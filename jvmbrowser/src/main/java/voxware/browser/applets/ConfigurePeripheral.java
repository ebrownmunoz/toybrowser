/**
 * 
 */
package voxware.browser.applets;

import com.voxware.browser.applets.AbstractApplet;
import com.voxware.browser.interpreter.Result;

/**
 * @author eric
 *
 */
public class ConfigurePeripheral extends AbstractApplet {

	/**
	 * For now, a dummy applet.
	 */
	public ConfigurePeripheral() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.voxware.browser.applets.AbstractApplet#run()
	 */
	@Override
	public Result run() {
		Result result = Result.Ok();
		result.setValue(true);
		return result;
	}

}
