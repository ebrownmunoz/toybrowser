package com.voxware.jvmbrowser.io;

import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.voxware.browser.io.Player;

public class URIPlayerSink implements Player<URI> {
	public URIPlayerSink() {
	}

	public Future<Void> play(URI output) {
		System.out.println("BytePlayer: playing URI" + output);

		return new Future<Void>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				return false;
			}

			@Override
			public boolean isCancelled() {
				return false;
			}

			@Override
			public boolean isDone() {
				return true;
			}

			@Override
			public Void get() throws InterruptedException, ExecutionException {
				return null;
			}

			@Override
			public Void get(long timeout, TimeUnit unit)
					throws InterruptedException, ExecutionException, TimeoutException {
				// TODO Auto-generated method stub
				return null;
			}
		}; // new Future<Status()
			
	} // play()

	@Override
	public void cancel() {
		
	}

	@Override
	public void setParameter(String name, Object value) {
	}

	@Override
	public Object getParameter(String name) {
		return null;
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

}
