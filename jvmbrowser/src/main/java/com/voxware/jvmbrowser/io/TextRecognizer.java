/**
 * 
 */
package com.voxware.jvmbrowser.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;

/**
 * @author eric
 *
 */
public class TextRecognizer implements Recognizer {
    private static final Logger LOG = LoggerFactory.getLogger(TextRecognizer.class);
    
    class TextFuture implements Future<RecognitionResult> {
        
        BlockingQueue<RecognitionResult> utterances = new ArrayBlockingQueue<RecognitionResult>(1);
        volatile boolean cancelled = false;
        volatile boolean done = false;
        
        public Thread inputThread = new Thread(new Runnable() {
            BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
            @Override
            public void run() {

                try {
                    
                    while(!buf.ready() && !done && !cancelled) {
                        synchronized(this) {
                            Thread.sleep(200);
                        }
                    }
                    
                    if (!cancelled && !done) {
                        done = true;
                        String line = buf.readLine();
                        utterances.put(RecognitionResult.success(line));                    
                    } else {
                        utterances.offer(RecognitionResult.event(Result.event("cancelled", "cancelled")));
                    }
                    
                } catch (IOException e) {
                    cancelled = true;
                    done = true;
                    LOG.warn("text browser", e);
                } catch (InterruptedException e) {
                    cancelled = true;
                    done = true;
                    LOG.warn("text browser", e);
                }
            }

        },

            "Text Input Thread");
        
        public TextFuture() {
            inputThread.start();
        }

        @Override
        public synchronized boolean cancel(boolean mayInterruptIfRunning) {
            if (! isDone()) {
                cancelled = true;
                done = true;
                return true;
            } else {
                return false;
            }
        }

        @Override
        public synchronized boolean isCancelled() {
            return cancelled;
        }

        @Override
        public synchronized boolean isDone() {
            return done;
        }

        @Override
        public synchronized RecognitionResult get() throws InterruptedException, ExecutionException {
            if (isCancelled()) {
                throw new CancellationException();
            } else {
                return utterances.take();
            }
        }

        @Override
        public synchronized RecognitionResult get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            if (isCancelled()) {
                throw new CancellationException();
            } else {
                RecognitionResult result = utterances.poll(timeout, unit);

                if (result != null) {
                    return result;
                } else {
                    throw new TimeoutException();
                }
            }
        }
        
        public synchronized void interrupt(RecognitionResult result) {
            done = true;
            utterances.add(result);
        }
        
    }

    private TextFuture future;

    /**
     * 
     */
    public TextRecognizer() {
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.io.Recognizer#recognize(java.lang.String)
     */
    @Override
    public Future<RecognitionResult> recognize(String rule) {
       this.future = new TextFuture();
       return future;
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.io.Recognizer#cancel()
     */
    @Override
    public void cancel() {
        future.cancelled = true;

    }

    /* (non-Javadoc)
     * @see com.voxware.browser.io.Recognizer#setParameter(java.lang.String, java.lang.Object)
     */
    @Override
    public void setParameter(String name, Object value) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.voxware.browser.io.Recognizer#getParameter(java.lang.String)
     */
    @Override
    public Object getParameter(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.io.Recognizer#sendInterrupt(com.voxware.browser.io.RecognitionResult)
     */
    @Override
    public void sendInterrupt(RecognitionResult rresult) {
        future.interrupt(rresult);

    }

	@Override
	public Future<RecognitionResult> recognizeWithInterruptsOnly() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void loadGrammar(String id, byte[] grammarBytes, boolean forceUpdate) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadVoiceModel(String id, byte[] voiBytes, boolean forceUpdate) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

}
