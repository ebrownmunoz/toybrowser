/**
 * 
 */
package com.voxware.jvmbrowser;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.applets.AppletRegistry;
import com.voxware.browser.applets.SingletonAppletFactory;
import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.BasicDocumentServer;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Interpreter;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.log.LogService;
import com.voxware.browser.model.Document;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;
import com.voxware.jvmbrowser.io.BytePlayerSink;
import com.voxware.jvmbrowser.io.StdoutTts;
import com.voxware.jvmbrowser.io.TextRecognizer;
import com.voxware.jvmbrowser.io.URIPlayerSink;

import voxware.browser.applets.ConfigurePeripheral;

/**
 * @author eric
 *
 */
public class JvmBrowser {

	private static final String  BOOT_URL_STRING = "vmserver/vxml/boot/voxboot.vxml";
	private static final String MAC_ID_STRING  = "00:00:00:00:00:00";
	private static final Object ENVIRONMENT_ID_STRING = "WH11";
	
	private URI rootURI;
	private URI startURI;
	private SessionContext sessionContext;
	private static Logger log = LoggerFactory.getLogger(JvmBrowser.class);
	
	
	private final DocumentParser parser = DaggerVxmlParser.create().getDocumentParser();
	private DocumentServer docserver;
	private IoManager iomanager;
	private Map<String, Object> properties;
	
	public JvmBrowser(Map<String, Object> properties, URI rootUri) throws URISyntaxException {
		
        docserver = new BasicDocumentServer(rootUri);

        iomanager = new IoManager();
        Player<String> tts = new StdoutTts();
        Player<byte[]> bytePlayer = new BytePlayerSink();
        Player<URI> uriPlayer = new URIPlayerSink();
 
        Recognizer recognizer = new TextRecognizer();

        iomanager.setAudioPlayer(new AudioPlayer(tts, bytePlayer, uriPlayer));
        iomanager.setRecognizer(recognizer);
        this.properties = properties;
        
	}
			
	public void run() throws URISyntaxException {
		Context context = Context.enter();
		
		URI boot_url = new URI (BOOT_URL_STRING);
		properties.put("macaddress", MAC_ID_STRING);
		properties.put("environmentid", ENVIRONMENT_ID_STRING);
		properties.put("languageId", "en-US");
		
		// session.memory.system.stats -- 
		// memstats.totalbytes - memstats.bytesfree"/> bytes (<value expr="memstats.percentused"/>%) 
		// of <value expr="memstats.totalbytes"/> total bytes</log>

		NativeObject system = new NativeObject();
		NativeObject memory = new NativeObject();
		NativeObject stats = new NativeObject();
		
		stats.put("totalbytes", stats, 10000);
		stats.put("bytesfree", stats, 2000);
		stats.put("percentused", stats, 10.2);
		
		memory.put("system", memory, system);
		system.put("stats", system, stats);
		
		properties.put("memory", memory);
        
        try {
        	sessionContext = new SessionContext(context, properties, iomanager, docserver);
        	sessionContext.getLogManager().register("appsrv", new LogService() {
				
				@Override
				public void send(String message) {
					log.info("APPSRV", message);
				}
			});
        	
        	AppletRegistry appletRegistry = new AppletRegistry();
        	
        	appletRegistry.registerApplet("voxware.browser.applets.ConfigurePeripheral",  
        			new ConfigurePeripheral());

        	Interpreter interpreter = new Interpreter(sessionContext, docserver, appletRegistry );
        	InputStream docStream = docserver.getDocument(boot_url);
        	Document document = parser.parse(sessionContext, boot_url, docStream);

        	interpreter.loadDocument(document);
            interpreter.processDocument();
        } catch (InterpreterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
        	Context.exit();
        }
	}

	/**
	 * @param args
	 * @throws URISyntaxException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws URISyntaxException, ParseException {
		
		CommandLine cl = getCommandLine(args);
		
		String[] myargs = cl.getArgs();
		
		if (myargs.length != 1) {
			System.err.println("Only one argument (URI) expected");
		} else {
			Map<String, Object> properties = new HashMap<String, Object>();
			URI rootUri = new URI(args[0]);
			
			JvmBrowser browser = new JvmBrowser(properties, rootUri);
			browser.run();
		}

	}


	private static CommandLine getCommandLine(String[] args) throws ParseException{
		Options options = new Options();
		Option helpOption = new Option("h", "Get Help");
		options.addOption(helpOption);
		
		CommandLine cl = new BasicParser().parse(options, args);
		
		return cl;
	}
}
