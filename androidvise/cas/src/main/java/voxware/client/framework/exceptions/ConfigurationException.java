package voxware.client.framework.exceptions;

public class ConfigurationException extends Exception {
  public ConfigurationException() {
    super();
  }

  public ConfigurationException(String msg) {
    super(msg);
  }
}
