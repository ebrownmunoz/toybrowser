package voxware.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Configuration {
    private static final Logger log = LoggerFactory.getLogger(Configuration.class);
    public static final String SERVER_URI = "serverIP";
    public static final String ENVIRONMENT = "environment";
    public static final String MAC_ADDRESS = "macAddress";
    public static final String LICENSE_KEY = "licenseKey";
    public static final String LOGICAL_NAME = "logicalName";
    public static final String LOG_LEVEL = "logLevel";
    public static final String CLIENT_GROUP = "clientGroup";
    public static final String CLIENT_TYPE = "clientType";
    public static final String OEM_VARIANT = "oemVariant";
    public static final String CFG_VARIANT = "cfgVariant";
    private static final Map<String, String> sharedPreferencesNameMap;
    static {
        Map<String, String> map = new HashMap<>();
        map.put(SERVER_URI, "com.voxware.browser.android.pref_root_uri");
        map.put(ENVIRONMENT, "com.voxware.browser.android.pref_environment");
        map.put(MAC_ADDRESS, "com.voxware.browser.android.pref_mac_address");
        map.put(LICENSE_KEY, "com.voxware.browser.android.pref_license_key");
        map.put(LOGICAL_NAME, "com.voxware.browser.android.pref_unit_id");
        //map.put(LOG_LEVEL, "com.voxware.browser.android.pref_log_level");
        // they are the same for now
        map.put(CLIENT_GROUP, "com.voxware.browser.android.pref_client_group");

        sharedPreferencesNameMap = Collections.unmodifiableMap(map);
    }
    private static Context defaultContext;

    public static void setDefaultContext(Context context) {
        defaultContext = context;
    }
    private final Context context;
    private final SharedPreferences sharedPreferences;

    public Configuration() {
        this(defaultContext);
    }

    public Configuration(Context context) {
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String get(final String name) {
        if (CLIENT_TYPE.equals(name)) {
            if (Build.MODEL.equals("MC32N0")) {
                return "Zebra MC32N0";
            } else {
                return Build.MANUFACTURER + " " + Build.MODEL;
            }
        } else if (OEM_VARIANT.equals(name)) {
            return "Android";
        } else if (CFG_VARIANT.equals(name)) {
            return "Flash";
        } else {
            String key = name;
            if (sharedPreferencesNameMap.containsKey(name)) {
                key = sharedPreferencesNameMap.get(name);
            }
            if (!sharedPreferences.contains(key)) {
                log.warn("Missing property: {} => {}", name, key);
            }
            if (name.equals(LOGICAL_NAME)) {
                String macAddress = sharedPreferences.getString("com.voxware.browser.android.pref_mac_address", null);
                // we lower case this for backwards compatibility
                macAddress = macAddress == null ? null : macAddress.toLowerCase();
                String logicalName = sharedPreferences.getString(key, macAddress);
                if (logicalName == null || logicalName.trim().isEmpty()) {
                    logicalName = macAddress;
                }
                return logicalName;
            } else {
                return sharedPreferences.getString(key, null);
            }
        }
    }

    public void set(String name, String value) {
        if (sharedPreferencesNameMap.containsKey(name)) {
            name = sharedPreferencesNameMap.get(name);
        }
        if (sharedPreferences.contains(name)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString(name, value);
            edit.apply();
        }
    }

}
