package voxware.vjml;

import java.lang.*;

public class VJMLNameConflictException extends VJMLException {

  public VJMLNameConflictException() {
    super();
  }

  public VJMLNameConflictException(Object offender) {
    super();
    this.offender = offender;
  }

  public VJMLNameConflictException(String message) {
    super(message);
  }

  public VJMLNameConflictException(Object offender, String message) {
    super(message);
    this.offender = offender;
  }

}
