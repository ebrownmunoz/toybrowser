package voxware.vjml;

import java.lang.*;
import java.util.*;

import voxware.util.*;

public class VJMLConduit {

  // VJMLConduit Exports (EXPORTED BY THE voxware.vjml PACKAGE)

   // Return the conduit's target IP address
  public String address() {
    return this.address;
  }

   // Return the number of VJMLChannels that share this conduit
  public int numChannels() {
    return channels.size();
  }

   // Enumerate the VJMLChannels that share this conduit
  public Enumeration channels() {
    return channels.elements();
  }

   // Return true iff the conduit's target IP address is reachable (e.g., not out of range)
  public boolean intact() {
    return this.intact;
  }

   // Install a VJMLListener, replacing and returning the existing one, if any (DEPRECATED)
  public VJMLListener listener(VJMLListener listener) {
    VJMLListener oldListener;
    try {
      oldListener = (VJMLListener)listeners.firstElement();
    } catch (NoSuchElementException e) {
      oldListener = null;
    }
    listeners.removeAllElements();
    if (listener != null) listeners.addElement(listener);
    return oldListener;
  }

   // Install a VJMLListener
  public void installListener(VJMLListener listener) {
    listeners.addElement(listener);
  }

   // Remove a VJMLListener
  public void removeListener(VJMLListener listener) {
    listeners.removeElement(listener);
  }

   // Flush out the conduit, discarding all the outgoing messages currently queued
  public synchronized void flush() {
    initializeAccumulator();
    pipeline.flush();
  }

   // Destroy this conduit and all of the channels that use it, removing it from the VJML
  public synchronized void destroy() {
     // Destroy all of the channels that use this conduit
    Enumeration enumeration = channels.elements();
    while (enumeration.hasMoreElements())
      ((VJMLChannel)enumeration.nextElement()).destroy();
     // Remove it from the VJML
    vjml.removeConduit(this.address);
     // Flush it out
    initializeAccumulator();
    pipeline.flush();
  }

  // VJMLConduit CLASS MEMBERS NOT EXPORTED BY THE voxware.vjml PACKAGE

  private VJML                    vjml;
  String                          address;        // The target IP address
  boolean                         taken;          // Whether the conduit's token is taken
  boolean                         intact;         // The target IP address is reachable (e.g., not out of range)
  private Vector                  channels;       // The channels that funnel through this conduit
  private Mailbox                 pipeline;       // Prioritized queue of outgoing VJMLSegmentedMessages awaiting transmission
  private Vector[]                accumulator;    // Marshalling queues of outgoing VJMLMessages awaiting encoding and transmission at batch priorities
  private VJMLSegmentedMessage[]  decoder;        // Array of concatenating decoders for incoming VJMLMessageSegments, indexed by priority
          Vector                  listeners;      // Vector of VJMLListeners to call on this conduit

  VJMLConduit(VJML vjml, String address) {
    this.vjml = vjml;
    this.address = address;
    taken = false;
    intact = true;
    channels = new Vector();
    pipeline = new Mailbox(vjml.numPriorities);
    accumulator = new Vector[vjml.numPriorities];
    initializeAccumulator();
    decoder = new VJMLSegmentedMessage[vjml.numPriorities];
    for (int i = 0; i < vjml.numPriorities; i++)
      decoder[i] = new VJMLSegmentedMessage();
    listeners = new Vector();
  }

   // Notify any listeners that messages have arrived, returning true iff there are listeners to notify
  boolean notifyListeners(VJMLChannel channel) {
    if (!listeners.isEmpty()) {
      Enumeration listenerEnum = listeners.elements();
      while (listenerEnum.hasMoreElements())
      ((VJMLListener)listenerEnum.nextElement()).message(channel);
      return true;
    } else {
      return false;
    }
  }

  private void initializeAccumulator() {
    for (int priority = 0; priority < vjml.numPriorities; priority++) accumulator[priority] = new Vector();
  }

   // Serialize and segment the given object, then queue the resulting VJMLSegmentedMessage for transmission
  void encode(Object object, int priority) {
    pipeline.insert(new VJMLSegmentedMessage(object, address, priority), priority);
  }

   // In priority order, serialize and segment the accumulated messages and queue the resulting VJMLSegmentedMessages for transmission
  void encode() {
    Vector messages;
    for (int priority = 0; priority < vjml.numPriorities; priority++) {
      messages = accumulator[priority];
      if (!messages.isEmpty()) {
        accumulator[priority] = new Vector();
        if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
          SysLog.println("VJMLConduit.encode(): cluster contains " + messages.size() + " messages");
         // Encode the cluster of messages in a VJMLSegmentedMessage as simply as possible
        Object object = (messages.size() > 1) ? messages : messages.firstElement();
        encode(object, priority);
      }
    }
  }

   // Return the decoded Object if the given VJMLMessageSegment terminates a valid VJMLSegmentedMessage; otherwise, return null
  Object decode(VJMLMessageSegment segment) {
    int priority = vjml.hardLimitedPriority(segment.priority);
    return decoder[priority].concatenateAndDecode(segment);
  }

  void address(String address) {
    this.address = address;
  }

  void intact(boolean intact) {
     // Report any change of state
    if (this.intact ^ intact) {
      VJMLException exception;
      if (this.intact) exception = new VJMLOutOfRangeException(this);
      else             exception = new VJMLBackInRangeException(this);
      vjml.exception(listeners, exception);
    }
    this.intact = intact;
  }

  void accumulate(Object object, int priority) {
    accumulator[priority].addElement(object);
  }

  synchronized void returnToken() {
    taken = false;
  }

  synchronized void removeAndReturnToken(int priority) {
    VJMLSegmentedMessage message = (VJMLSegmentedMessage)pipeline.peekNow(priority);
    if (message != null) {
      message.removeElementAt(0);
      if (message.isEmpty()) pipeline.removeNow(priority);
    }
    taken = false;
  }

  synchronized VJMLMessageSegment peekAndTakeToken() {
    VJMLMessageSegment segment = null;
    if (taken == false) {
      VJMLSegmentedMessage message = (VJMLSegmentedMessage)pipeline.peekNow();
      if (message != null) {
        segment = (VJMLMessageSegment)message.elementAt(0);
        taken = true;
      }
    }
    return segment;
  }

  synchronized void addChannel(VJMLChannel channel) {
    if (channels.indexOf(channel) < 0) channels.addElement(channel);
  }

  void removeChannel(VJMLChannel channel) {
    while (channels.removeElement(channel));
  }
}
