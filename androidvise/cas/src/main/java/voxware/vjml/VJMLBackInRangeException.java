package voxware.vjml;

import java.lang.*;

public class VJMLBackInRangeException extends VJMLException {

  public VJMLBackInRangeException(Object offender) {
    super(offender);
  }
}
