package voxware.vjml;

import java.lang.*;
import java.util.*;

import voxware.util.*;

public class VJMLChannel {

  // VJMLChannel Exports (EXPORTED BY THE voxware.vjml PACKAGE)

   // Return the channel's name
  public String name() {
    return name;
  }

   // Return the peer channel's IP address
  public String address() {
    return address;
  }

   // Return the VJMLConduit this channel uses
  public VJMLConduit conduit() {
    return this.conduit;
  }

   // Return the highest priority subject to message batching (default is VXML.batchThreshold())
  public int batchThreshold() {
    return batchThreshold;
  }

   // Set the highest priority subject to message batching (default is VXML.batchThreshold())
  public void batchThreshold(int priority) {
     // Setting this to vjml.numPriorities() defeats all message batching on this channel; setting it to 0 batches everything
    this.batchThreshold = priority;
  }

   // Report whether messages waiting at batch priorities tag along after any high priority messages (default is true)
  public boolean tagAlong() {
    return tagAlong;
  }

   // Set whether messages waiting at batch priorities tag along after any high priority messages (default is true)
  public void tagAlong(boolean tagAlong) {
    this.tagAlong = tagAlong;
  }

   // Return the highest priority message available in the in-box, blocking until one arrives
  public VJMLMessage getMessage() throws InterruptedException {
     // Should synchronize here, but can't without creating a possible deadlock condition
    VJMLMessage message = (VJMLMessage)inBox.remove();
    if (message != null && vjml != null) vjml.remove(message);
    return message;
  }

   // Return the highest priority message available in the in-box, blocking until one arrives or timeout is exceeded
  public VJMLMessage getMessage(long timeout) throws InterruptedException {
     // Should synchronize here, but can't without creating a possible deadlock condition
    VJMLMessage message = (VJMLMessage)inBox.remove(timeout);
    if (message != null && vjml != null) vjml.remove(message);
    return message;
  }

   // Return the highest priority message available in the in-box, or null if none is available
  public synchronized VJMLMessage getMessageNow() {
    VJMLMessage message = (VJMLMessage)inBox.removeNow();
    if (message != null && vjml != null) vjml.remove(message);
    return message;
  }

   // Return the oldest message available in the in-box at the given priority, or null if none is available
  public synchronized VJMLMessage getMessageNow(int priority) {
    VJMLMessage message = null;
    if (vjml != null) {
      priority = vjml.hardLimitedPriority(priority);
      message = (VJMLMessage)inBox.removeNow(priority);
      if (message != null) vjml.remove(message);
    }
    return message;
  }

   // Send a message at its internally specified priority
  public void putMessage(VJMLMessage message) {
    putMessage(message, message.priority);
  }

   // Send a message at the given priority
  public void putMessage(VJMLMessage message, int priority) {
    message.channel(name);
    if (vjml != null) {
      priority = vjml.hardLimitedPriority(priority);
      message.priority = priority;
      if (priority < batchThreshold) {
         // Send higher priority messages immediately
        vjml.send(message, conduit);
        if (tagAlong) vjml.send(conduit);
      } else {
         // Wait to batch lower priority ones
        conduit.accumulate(message, priority);
      }
    }
  }

   // Send all messages waiting in this channel's conduit, regardless of the outputInterval
  public void sendNow() {
    if (vjml != null) vjml.send(conduit);
  }

   // Return the total number of messages in the in-box
  public int messageCount() {
    return inBox.numobj();
  }

   // Return the total number of messages in the in-box at the given priority
  public int messageCount(int priority) {
    int numMessages = 0;
    if (vjml != null) {
      priority = vjml.hardLimitedPriority(priority);
      numMessages = inBox.numobj(priority);
    }
    return numMessages;
  }

   // Flush and return all messages currently in the channel's in-box
  public synchronized Vector flush() {
    Vector messages = inBox.flush();
    if (vjml != null && messages != null) vjml.remove(messages);
    return messages;
  }

   // Flush and return all messages currently in the channel's in-box at the specified priority
  public synchronized Vector flush(int priority) {
    Vector messages = null;
    if (vjml != null) {
      priority = vjml.hardLimitedPriority(priority);
      messages = inBox.flush(priority);
      if (messages != null) vjml.remove(messages);
    }
    return messages;
  }

   // Destroy this channel, flushing it out and removing it from the VJML
  public void destroy() {
    if (vjml != null) vjml.removeChannel(this.name);
    VJMLConduit conduit = this.conduit;
    if (conduit != null) {
      conduit.removeChannel(this);
      this.conduit = null;
    }
    this.flush();
    this.vjml = null;      // Orphan the channel
  }

   // Install a new VJMLListener
  public VJMLListener listener(VJMLListener listener) {
    VJMLListener oldListener = this.listener;
    this.listener = listener;
    return oldListener;
  }

  // VJMLChannel MEMBERS NOT EXPORTED BY THE voxware.vjml PACKAGE

  private String       name;
  private String       address;
  private VJML         vjml;
  private Mailbox      inBox;
  private VJMLConduit  conduit;
  private int          batchThreshold;
  private boolean      tagAlong;
  private VJMLListener listener;

   // Only VJML constructs VJMLChannels
  VJMLChannel(VJML vjml, String name, String address) {
    this.vjml = vjml;
    this.name = name;
    this.address = address;
    batchThreshold = vjml.batchThreshold;
    tagAlong = vjml.tagAlong;
    inBox = new Mailbox(vjml.numPriorities);
    if (SysLog.printLevel > 1 && SysLog.printMaskBitsSet(SysLog.VJML_MASK))
      SysLog.println("VJMLChannel: constructed channel (" + this.toString() + ") named \"" + name + "\" for address \"" + address + "\"");
  }

   // Set the vjml
  void vjml(VJML vjml) {
    this.vjml = null;
  }

   // Set the channel's conduit
  void conduit(VJMLConduit conduit) {
    this.conduit = conduit;
  }

   // Receive a VJMLMessage or Vector of VJMLMessages into the in-box (called by vjml.dispatch())
  synchronized void receive(Object object) {
    if (vjml != null) {
      if (object instanceof Vector) {
        Enumeration messages = ((Vector)object).elements();
        while (messages.hasMoreElements()) {
          VJMLMessage message = (VJMLMessage)messages.nextElement();
          message.priority = vjml.hardLimitedPriority(message.priority);
          inBox.insert(message, message.priority);
          vjml.insert(message);
        }
      } else if (object instanceof VJMLMessage) {
        VJMLMessage message = (VJMLMessage)object;
        message.priority = vjml.hardLimitedPriority(message.priority);
        inBox.insert(message, message.priority);
        vjml.insert(message);
      } else {
        vjml.exception(new RuntimeException("VJMLChannel.receive: UNEXPECTED " + object.getClass().getName() + " received"));
      }
    }
  }

   // Receive a lone VJMLMessage into the in-box (called by vjml.dispatch())
  synchronized void receive(VJMLMessage message) {
    if (vjml != null) {
      message.priority = vjml.hardLimitedPriority(message.priority);
      inBox.insert(message, message.priority);
      vjml.insert(message);
    }
  }

   // Notify the listener that a message has arrived
  boolean notifyListener() {
    VJMLListener thisListener = listener;
    if (thisListener != null) thisListener.message(this);
    return (thisListener != null);
  }
}
