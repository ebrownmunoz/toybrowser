package voxware.vjml;

import java.lang.*;
import java.util.*;
import java.net.InetAddress;

public class VJMLAttributes {

  int           numPriorities = VJMLMessage.DefaultNumPriorities;   // The number of priorities available
  int           defaultRcvPort = VJML.DefaultReceivePort;           // The default port number for both local and remote channels to receive messages on
  Integer       sendPort = null;                                    // The port to send messages on (the system assigns one if this is null)
  long          retryInterval = VJML.DefaultRetryInterval;          // Milliseconds between transmission attempts when the peer is inaccessible
  long          outputInterval = VJML.DefaultOutputInterval;        // Milliseconds between normal priority message transmission attempts
  int           batchThreshold = VJMLMessage.DefaultBatchThreshold; // The highest priority subject to message batching
  boolean       tagAlong = true;                                    // Whether sending higher priority messages provokes sending batched ones
  boolean       maintainAgeQueue = true;                            // Maintain a prioritized queue of incoming messages sorted by age within priorities
  InetAddress   inetAddress = null;                                 // The local internet address
  Vector        listeners = new Vector();                           // Vector of VJMLListeners
  ClassLoader   classLoader = null;                                 // Class loader to use during serialization/deserialization

  // VJMLAttributes Exports (EXPORTED BY THE voxware.vjml PACKAGE)

   // Construct a default VJMLAttributes
  public VJMLAttributes() {
  }

  public void numPriorities(int numPriorities) {
    this.numPriorities = numPriorities;
  }

  public int numPriorities() {
    return this.numPriorities;
  }

  public void inetAddress(InetAddress inetAddress) {
    this.inetAddress = inetAddress;
  }

  public InetAddress inetAddress() {
    return this.inetAddress;
  }

  public void defaultReceivePort(int defaultRcvPort) {
    this.defaultRcvPort = defaultRcvPort;
  }

  public int defaultReceivePort() {
    return this.defaultRcvPort;
  }

  public void sendPort(int sendPort) {
    this.sendPort = new Integer(sendPort);
  }

  public int sendPort() {
    return this.sendPort.intValue();
  }

  public void retryInterval(long retryInterval) {
    this.retryInterval = retryInterval;
  }

  public long retryInterval() {
    return this.retryInterval;
  }

  public void outputInterval(long outputInterval) {
    this.outputInterval = outputInterval;
  }

  public long outputInterval() {
    return this.outputInterval;
  }

  public void batchThreshold(int batchThreshold) {
    this.batchThreshold = batchThreshold;
  }

  public int batchThreshold() {
    return this.batchThreshold;
  }

  public void tagAlong(boolean tagAlong) {
    this.tagAlong = tagAlong;
  }

  public boolean tagAlong() {
    return this.tagAlong;
  }

  public void maintainAgeQueue(boolean maintainAgeQueue) {
    this.maintainAgeQueue = maintainAgeQueue;
  }

  public boolean maintainAgeQueue() {
    return this.maintainAgeQueue;
  }

   // DEPRECATED
  public void listener(VJMLListener listener) {
    this.listeners.addElement(listener);
  }

   // DEPRECATED
  public VJMLListener listener() {
    VJMLListener listener;
    try {
      listener = (VJMLListener)this.listeners.firstElement();
    } catch (NoSuchElementException exc) {
      listener = null;
    }
    return listener;
  }

  public void listeners(Vector listeners) {
    this.listeners = listeners;
  }

  public Vector listeners() {
    return this.listeners;
  }

  public void classLoader(ClassLoader classLoader) {
    this.classLoader = classLoader;
  }

  public ClassLoader classLoader() {
    return this.classLoader;
  }
}

