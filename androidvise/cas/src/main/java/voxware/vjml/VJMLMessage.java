package voxware.vjml;

import java.lang.*;
import java.io.*;

public class VJMLMessage implements Serializable {

  // VJMLMessage Exports (EXPORTED BY THE voxware.vjml PACKAGE)

   // The highest priority is always zero (see voxware.util.Mailbox)
  public static final int FirstPriority         = 0;
  public static final int HighestPriority       = FirstPriority;
  public static final int SecondPriority        = 1;
  public static final int HighPriority          = SecondPriority;
  public static final int ThirdPriority         = 2;
  public static final int NormalPriority        = ThirdPriority;
  public static final int FourthPriority        = 3;
  public static final int FifthPriority         = 4;
  public static final int SixthPriority         = 5;
  public static final int SeventhPriority       = 6;
  public static final int EighthPriority        = 7;
  public static final int NinthPriority         = 8;
  public static final int TenthPriority         = 9;

  public static final int DefaultPriority       = NormalPriority;
  public static final int DefaultBatchThreshold = NormalPriority;
  public static final int DefaultNumPriorities  = DefaultPriority + 1;

  public VJMLMessage(Object content) {
    this.content = content;
  }

  public VJMLMessage(Object content, int priority) {
    this.content = content;
    this.priority = priority;
  }

   // All fields are publicly read-only

  public Object content() {
    return content;
  }

  public String channel() {
    return channel;
  }

  public int priority() {
    return priority;
  }

  // VJMLMessage CLASS MEMBERS NOT EXPORTED BY THE voxware.vjml PACKAGE

  Object  content = null;              // Must implement Serializable
  String  channel = null;
  int     priority = DefaultPriority;

  void content(Object content) {
    this.content = content;
  }

  void channel(String channel) {
    this.channel = channel;
  }

  void priority(int priority) {
    this.priority = priority;
  }

  private void writeObject(ObjectOutputStream s) throws IOException {
     // Write the fields
    s.writeObject(content);
    s.writeObject(channel);
    s.writeInt(priority);
  }

  private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
     // Read the fields
    content = s.readObject();
    channel = (String) s.readObject();
    priority = s.readInt();
  }
}
