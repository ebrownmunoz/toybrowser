package voxware.vjml.net;

import java.lang.*;
import java.io.*;

class UDPPacketHdr implements Serializable {

  long crc;
  int  sn;
  int  ts;
  int  sts;

  static final int Success    = 0;
  static final int Duplicate  = 1;
  static final int Unreadable = 2;

  UDPPacketHdr(int sn, int ts) {
    this.crc = 0;
    this.sn = sn;
    this.ts = ts;
    this.sts = Success;
  }

  UDPPacketHdr(long crc, int sn, int ts) {
    this.crc = crc;
    this.sn = sn;
    this.ts = ts;
    this.sts = Success;
  }

  long crc() {
    return crc;
  }

  void crc(long crc) {
    this.crc = crc;
  }

  int sn() {
    return sn;
  }

  void sn(int sn) {
    this.sn = sn;
  }

  int ts() {
    return ts;
  }

  void ts(int ts) {
    this.ts = ts;
  }

  int sts() {
    return sts;
  }

  void sts(int sts) {
    this.sts = sts;
  }
}
  
