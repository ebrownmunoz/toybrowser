package voxware.vjml.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

class UDPSocket {
  private static final Logger log = LoggerFactory.getLogger(UDPSocket.class);
  private int          port = 0;
  private int          timeout = 0;
  private InetAddress  address = null;
  private DatagramSocket dgs = null;

  UDPSocket() throws SocketException {
    dgs = new DatagramSocket();
  }

  UDPSocket(int port) throws SocketException {
    dgs = new DatagramSocket(port);
  }

  UDPSocket(int port, InetAddress address) throws SocketException {
    dgs = new DatagramSocket(port, address);
  }
  
  void close() {
    if (dgs != null) {
      dgs.close();
    }
  }

  protected void finalize() throws Throwable {
  }

  void send(DatagramPacket p) throws IOException {
    if (dgs != null) {
      dgs.send(p);
    }
  }

  void receive(DatagramPacket p) throws IOException {
    if (dgs != null) {
      dgs.receive(p);
    }
  }

  InetAddress getLocalAddress() {
    if (dgs != null) {
      InetAddress ia = null;
      try {
        ia = InetAddress.getLocalHost();
      } catch (UnknownHostException e) {
        log.error("UDPSocket::getLocalAddress: ERROR getLocalHost", e);
      }
      return ia;
    }
    return address;
  }

  int getLocalPort() {
    if (dgs != null) {
      return dgs.getLocalPort();
    } else {
      return port;
    }
  }

  int getSoTimeout() throws SocketException {
    if (dgs != null) {
      return dgs.getSoTimeout();
    } else {
      return timeout;
    }
  }

  void setSoTimeout(int timeout) throws SocketException {
    if (dgs != null) {
      dgs.setSoTimeout(timeout);
    } else {
      if (timeout < 0)
        throw new SocketException("Invalid (negative) timeout value");
      this.timeout = timeout;
    }
  }

  private void setAddress(InetAddress address) {
    this.address = address;
  }
}
        
