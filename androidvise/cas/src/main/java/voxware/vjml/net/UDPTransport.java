package voxware.vjml.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import voxware.vjml.*;

class UDPSnWindow {
    private int[] window = null;

    static final int UDPSNWDfltLen = 64;

    UDPSnWindow(int windowLen) {
        window = new int[windowLen];
        initialize();
    }

    void initialize() {
        for (int i = 0; i < window.length; i++)
            window[i] = 0;
    }

    boolean isDuplicate(int sn) {
        if (sn == 0)
            return false;

        // Allow peers to restart - reinitialize for sn == 1
        if (sn == 1)
            initialize();
        int i = sn % window.length;
        if (window[i] == sn) {
            return true;
        } else {
            window[i] = sn;
            return false;
        }
    }
}

class UDPTransport {
    private static final Logger log = LoggerFactory.getLogger(UDPTransport.class);
    protected ClassLoader classLoader = null;
    protected UDPSocket recvSocket = null;
    protected UDPSocket sendSocket = null;
    protected Hashtable senderht = null;
    protected byte[] ackbuf = null;
    protected byte[] recvbuf = null;
    protected RTT rtt = null;
    protected int sendSn = 0;

    static final String LoopbackAddress = "127.0.0.1";

    static final int UDPTDfltPort = 4450;
    static final int UDPTDfltRBufLen = 2048;
    static final int UDPTDfltABufLen = 512;

    UDPTransport() throws IOException {
        recvSocket = new UDPSocket(UDPTDfltPort);
        sendSocket = new UDPSocket();
        classLoader = null;
        initialize();
    }

    UDPTransport(ClassLoader classLoader) throws IOException {
        recvSocket = new UDPSocket(UDPTDfltPort);
        sendSocket = new UDPSocket();
        this.classLoader = classLoader;
        initialize();
    }

    UDPTransport(int recvPort, ClassLoader classLoader) throws IOException {
        recvSocket = new UDPSocket(recvPort);
        sendSocket = new UDPSocket();
        this.classLoader = classLoader;
        initialize();
    }

    UDPTransport(int recvPort, InetAddress address, ClassLoader classLoader) throws IOException {
        recvSocket = new UDPSocket(recvPort, address);
        sendSocket = new UDPSocket();
        this.classLoader = classLoader;
        initialize();
    }

    UDPTransport(int sendPort, int recvPort, ClassLoader classLoader) throws IOException {
        recvSocket = new UDPSocket(recvPort);
        sendSocket = new UDPSocket(sendPort);
        this.classLoader = classLoader;
        initialize();
    }

    UDPTransport(int sendPort, int recvPort, InetAddress address, ClassLoader classLoader) throws IOException {
        recvSocket = new UDPSocket(recvPort, address);
        sendSocket = new UDPSocket(sendPort);
        this.classLoader = classLoader;
        initialize();
    }

    private void initialize() throws IOException {
        rtt = new RTT();
        recvbuf = new byte[UDPTDfltRBufLen];
        ackbuf = new byte[UDPTDfltABufLen];
        senderht = new Hashtable();
        //recvSocket.setSoTimeout(1000);
    }

    int parameter(int index) throws IllegalArgumentException {
        switch (index) {
            case VJMLTransport.RTTRxmitMax:
            case VJMLTransport.RTTRxmitMin:
            case VJMLTransport.RTTMaxRxmit:
                return rtt.parameter(index);
            default:
                throw new IllegalArgumentException("UDPTransport.parameter: ERROR - unknown parameter " + index);
        }
    }

    void parameter(int index, int value) throws IllegalArgumentException {
        switch (index) {
            case VJMLTransport.RTTRxmitMax:
            case VJMLTransport.RTTRxmitMin:
            case VJMLTransport.RTTMaxRxmit:
                rtt.parameter(index, value);
                break;
            default:
                throw new IllegalArgumentException("UDPTransport.parameter: ERROR - unknown parameter " + index);
        }
    }

    InetAddress localAddress() {
        return recvSocket.getLocalAddress();
    }

    int send(VJMLMessageSegment segment) {
        DatagramPacket sendPkt = null;
        DatagramPacket ackPkt = null;
        UDPPacketHdr sendHdr = null;
        UDPPacketHdr ackHdr = null;
        CRC32 crc = new CRC32();

        // Determine SN, ts and create UDPPacketHdr
        sendSn++;
        sendHdr = new UDPPacketHdr(sendSn, rtt.ts());

        String localAddr;
        InetAddress destAddr = null;
        int destPort = segment.port();
        try {
            destAddr = segment.inetAddress();
            // Hack to allow loopback testing
            if (destAddr.getHostAddress().equals(LoopbackAddress)) {
                localAddr = LoopbackAddress;
            } else {
                localAddr = recvSocket.getLocalAddress().getHostAddress();
                // Some JVM's (e.g., Blackdown) have a crippled InetAddress.getLocalHost() method that always returns the LoopbackAddress.
                // In this case send only the port number. The destination host will try to get the correct return address from the DatagramPacket.
                if (localAddr.equals(LoopbackAddress)) localAddr = "";
            }
            localAddr += ":" + recvSocket.getLocalPort();
        } catch (UnknownHostException e) {
            log.error("UDPTransport.send: ERROR getting destination inetaddress - " + e.toString(), e);
            return VJMLMessageSegment.JavaException;
        }

        // Serialize sendHdr and segment
        byte data[] = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            CheckedOutputStream cos = new CheckedOutputStream(baos, crc);
            DataOutputStream dos = new DataOutputStream(cos);

            // Serialize the segment
            cos.getChecksum().reset();
            if (segment.content != null) {
                dos.writeInt(segment.content.length);
                dos.write(segment.content, 0, segment.content.length);
            } else {
                dos.writeInt(0);
            }
            dos.writeUTF(localAddr);
            dos.writeByte(segment.status);
            dos.writeByte(segment.priority);
            dos.writeShort(segment.index);
            dos.flush();
            sendHdr.crc = cos.getChecksum().getValue();

            // Serialize the sendHdr
            dos.writeLong(sendHdr.crc);
            dos.writeInt(sendHdr.sn);
            dos.writeInt(sendHdr.ts);
            dos.writeInt(sendHdr.sts);
            dos.flush();
            dos.close();

            data = baos.toByteArray();
            log.debug("UDPTransport.send:");
            log.debug("  dest addr is " + segment.address());
            log.debug("  outbound CRC = " + sendHdr.crc);
            log.debug("  return addr is " + localAddr);
            log.debug("  packet data len = " + data.length);

        } catch (Exception e) {
            log.error("Error sending message", e);
            return VJMLMessageSegment.JavaException;
        }

        sendPkt = new DatagramPacket(data, data.length, destAddr, destPort);
        ackPkt = new DatagramPacket(ackbuf, ackbuf.length);

        rtt.reset();
        while (!Thread.interrupted()) {
            // Send Packet
            try {
                sendSocket.send(sendPkt);
                log.debug("UDPTransport.send: sent packet on port #" + sendSocket.getLocalPort());
            } catch (IOException ioe) {
                log.error("Error sending packet", ioe);
                return VJMLMessageSegment.NetworkFailure;
            }

            // Set timeout for receive
            try {
                int timeout = rtt.start();
                sendSocket.setSoTimeout(timeout);
                log.debug("UDPTransport.send: ack timeout = " + timeout);
            } catch (IOException ioe) {
                log.error("Error setting timeout", ioe);
                return VJMLMessageSegment.JavaException;
            }

            // Await acknowledgement
            try {
                do {
                    sendSocket.receive(ackPkt);
                    log.debug("UDPTransport.send: received ACK packet on port #" + sendSocket.getLocalPort());
                    try {
                        ByteArrayInputStream bais = new ByteArrayInputStream(ackPkt.getData());
                        DataInputStream dis = new DataInputStream(bais);
                        ackHdr = new UDPPacketHdr(0, 0);
                        ackHdr.crc = dis.readLong();
                        ackHdr.sn = dis.readInt();
                        ackHdr.ts = dis.readInt();
                        ackHdr.sts = dis.readInt();
                        dis.close();
                    } catch (Exception e) {
                        log.error("UDPTransport.send: ERROR - deserialization exception");
                        return VJMLMessageSegment.SerializeError;
                    }
                } while (ackHdr.sn() != sendHdr.sn());

                if (ackHdr.sts() != UDPPacketHdr.Success) {
                    if (ackHdr.sts() == UDPPacketHdr.Unreadable) {
                        // Resend the packet - the receiver couldn't read the contents
                        continue;
                    }
                    if (ackHdr.sts() == UDPPacketHdr.Duplicate) {
                        // A duplicate packet was detected by receiver.  Feign success
                        return VJMLMessageSegment.Success;
                    }
                }

                // Check the CRC32 and retry if there's a mismatch
                if (ackHdr.crc() != sendHdr.crc()) {
                    log.debug("UDPTransport.send: WARNING - i/b CRC mismatch, resending");
                    continue;
                } else {
                    break;
                }
            } catch (InterruptedIOException iie) {
                // InterruptedIOException is thrown when receive times out. Update RTO, give up when retries exceeded.
                if (rtt.timeout()) {
                    //!! Reinitialize RTT to start-up parameters here
                    rtt.init();

                    log.error("UDPTransport.send: ERROR - socket send timed out");
                    return VJMLMessageSegment.OutOfRange;
                }
            } catch (IOException ie) {
                return VJMLMessageSegment.JavaException;
            }

            log.warn("UDPTransport.send: timeout/retry");
        }
        rtt.stop(rtt.ts() - ackHdr.ts());


        log.debug("UDPTransport.send: send complete");
        return VJMLMessageSegment.Success;
    }

    VJMLMessageSegment receive() throws InterruptedIOException {
        CRC32 crc = new CRC32();
        DatagramPacket ackPkt = null;
        UDPPacketHdr recvHdr = null;
        VJMLMessageSegment segment = null;
        boolean duplicate = false;

        if (classLoader != null) {
            log.debug("UDPTransport.receive: using non-default classloader " + classLoader);
            Thread.currentThread().setContextClassLoader(classLoader);
        }

        while (!Thread.interrupted()) {
            InetAddress retAddr = null;
            int retPort = 0;

            try {
                // Receive datagram - this blocks, potentially for a long time...
                DatagramPacket recvPkt = new DatagramPacket(recvbuf, recvbuf.length);
                while (!Thread.interrupted()) {
                    try {
                        recvSocket.receive(recvPkt);
                        break;
                    } catch (SocketTimeoutException e) {
                        log.debug("UDPTransport.receive: receive timed out?");
                    }
                }
                if (Thread.interrupted()) {
                    log.debug("UDPTransport.receive: receive interrupted");
                    throw new InterruptedIOException();
                }

                log.debug("UDPTransport.receive: received msg of length " + recvPkt.getLength());

                // Obtain return address from datagram
                retAddr = recvPkt.getAddress();
                retPort = recvPkt.getPort();

                // Deserialize the message - we expect a VJMLMessageSegment followed by a UDPPacketHdr
                ByteArrayInputStream bais = new ByteArrayInputStream(recvPkt.getData());
                CheckedInputStream cis = new CheckedInputStream(bais, crc);
                DataInputStream dis = new DataInputStream(cis);

                // Read the segment
                byte[] content = null;
                cis.getChecksum().reset();
                int contentLen = dis.readInt();
                if (contentLen > 0) {
                    content = new byte[contentLen];
                    dis.readFully(content);
                }
                String address = dis.readUTF();
                int status = (int) dis.readByte();
                int priority = (int) dis.readByte();
                int index = (int) dis.readShort();
                long segmentCrc = cis.getChecksum().getValue();

                // Read the header
                recvHdr = new UDPPacketHdr(0, 0);
                recvHdr.crc = dis.readLong();
                recvHdr.sn = dis.readInt();
                recvHdr.ts = dis.readInt();
                recvHdr.sts = dis.readInt();

                dis.close();
                cis.close();
                bais.close();

                // If the address field in the VJMLMessageSegment contains only the port number, try to construct a return address from the datagram address
                if (address(address).length() == 0)
                    address = address(retAddr.getHostAddress()) + ":" + port(address);

                // See if the segment CRC32 matches the one in the packet header.
                boolean crcOk = (recvHdr.crc() == segmentCrc);
                boolean isDup = false;
                if (isDup = isDuplicate(address, recvHdr.sn()))
                    recvHdr.sts(UDPPacketHdr.Duplicate);
                recvHdr.crc(segmentCrc);

                // Echo the packet header w/updated CRC32
                byte data[] = null;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);
                dos.writeLong(recvHdr.crc);
                dos.writeInt(recvHdr.sn);
                dos.writeInt(recvHdr.ts);
                dos.writeInt(recvHdr.sts);
                dos.flush();
                dos.close();

                data = baos.toByteArray();
                log.debug("UDPTransport.receive:");
                log.debug("  inbound CRC = " + segmentCrc);
                log.debug("  ackPkt data len = " + data.length);
                log.debug("  return addr " + address);
                log.debug("  ackto addr " + retAddr.toString() + ":" + retPort);
                ackPkt = new DatagramPacket(data, data.length, retAddr, retPort);
                recvSocket.send(ackPkt);

                if (!crcOk) {
                    log.debug("UDPTransport.receive: segment CRC does not match header");
                    continue;
                } else if (isDup) {

                    log.debug("UDPTransport.receive: duplicate detected for sn " + recvHdr.sn() + ", continuing");
                    continue;
                } else {
                    segment = new VJMLMessageSegment(content, address, status, priority, index);
                    break;
                }
            } catch (InvalidClassException ice) {
            } catch (StreamCorruptedException sce) {
            } catch (OptionalDataException ode) {
                // The incoming packet data is unreadable.  Inform the sender.
                try {
                    DatagramPacket errPkt = null;
                    UDPPacketHdr errHdr = new UDPPacketHdr(0, 0, 0);
                    errHdr.sts(UDPPacketHdr.Unreadable);
                    byte data[] = null;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    DataOutputStream dos = new DataOutputStream(baos);
                    dos.writeLong(recvHdr.crc);
                    dos.writeInt(recvHdr.sn);
                    dos.writeInt(recvHdr.ts);
                    dos.writeInt(recvHdr.sts);
                    dos.flush();
                    dos.close();

                    data = baos.toByteArray();
                    errPkt = new DatagramPacket(data, data.length, retAddr, retPort);
                    recvSocket.send(errPkt);
                    log.debug("UDPTransport.receive: ERROR - serialization error, sender notified, continuing");
                    continue;
                } catch (Exception e) {
                    log.error("UDPTransport.receive: ERROR - exception reporting serialization error, continuing", e);
                    continue;
                }
            } catch (InterruptedIOException e) {
                log.error("UDPTransport.receive: InterruptedIOException", e);
                throw e;
            } catch (SocketException e) {
                log.error("UDPTransport.receive: SocketException", e);
                return null;
            } catch (Exception e) {
                log.error("UDPTransport.receive: ERROR - unexpected exception, continuing", e);
                continue;
            }
        }

        return segment;
    }

    private boolean isDuplicate(String address, int sn) {
        UDPSnWindow udpsnw = null;

        if ((udpsnw = (UDPSnWindow) senderht.get(address)) == null) {
            udpsnw = new UDPSnWindow(UDPSnWindow.UDPSNWDfltLen);
            senderht.put((Object) address, (Object) udpsnw);
            log.debug("UDPTransport.isDuplicate: added ht entry for " + address);
        }

        return udpsnw.isDuplicate(sn);
    }

    private static String address(String address) {

        int spos = address.lastIndexOf('/');
        int epos = address.indexOf(':');

        return (epos < 0) ? address.substring(spos + 1) : address.substring(spos + 1, epos);
    }

    private static String port(String address) {

        int spos = address.indexOf(':');

        return (spos < 0) ? "" : address.substring(spos + 1);
    }

    public void close() {
        log.info("Closing UDPTransport");
        sendSocket.close();
        recvSocket.close();
    }
}
