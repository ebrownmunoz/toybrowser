package voxware.app.remote;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CasServicesController extends AbstractCommandController {

  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  public CasServicesController() {
     setCommandClass(CasServicesCommand.class);
  }

  protected ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
    
    CasServicesCommand csc = (CasServicesCommand) command;
    csc.setApplicationContext(getApplicationContext());

    // log.info("handle: " + spc.getUserId() + "/" + spc.getApplicationId() + "/" + spc.getLanguageId() + "/" + spc.getEnvironmentId());

    csc.execute();

    log.info("handle: execute done");
    response.getWriter().write(csc.getResult());
    return null;
  }

  /*  protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws ServletException {
     // To enable conversion of a Multipart instance to byte[] we have to register a custom editor (in this case the ByteArrayMultipartFileEditor)
    binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    binder.registerCustomEditor(String.class, new URLEncodedUTF8StringPropertyEditor());
     // Now Spring knows how to handle multipart objects and convert them
  }
  */

}
