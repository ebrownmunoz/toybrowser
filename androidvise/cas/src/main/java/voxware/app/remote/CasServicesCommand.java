package voxware.app.remote;

import org.springframework.context.ApplicationContext;

import org.json.JSONObject;
import org.json.JSONException;

import com.voxware.remote.services.cas.ClientConfigurationDescriptor;
import com.voxware.remote.services.cas.ClientInterruptDescriptor;
import com.voxware.remote.services.cas.ClientStatusDescriptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.justobjects.pushlet.core.Config;
import voxware.app.beans.VJMLBean;
import voxware.app.utils.JSONDescriptorCodec;

import voxware.cas.commands.SourcedInterruptCommand;

import voxware.client.Configuration;
import voxware.client.framework.exceptions.ConfigurationException;

import voxware.util.Environment;
import voxware.vjml.VJMLChannel;
import voxware.vjml.VJMLMessage;
import voxware.vjml.VJMLNameConflictException;

public class CasServicesCommand {

    public static final String BrowserName = "browser";
    public static final String ClientGroupName = "clientGroup";
    public static final String ClientTypeName = "clientType";
    public static final String OemVariantName = "oemVariant";
    public static final String CfgVariantName = "cfgVariant";
    public static final String EnvironmentIdName = "environment";
    public static final String LicenseKeyName = "licenseKey";
    public static final String LogicalNameName = "logicalName";
    public static final String LogLevelName = "logLevel";
    public static final String ServerIPName = "serverIP";

    private static JSONDescriptorCodec codec = null;
    private ApplicationContext applicationContext;
    private String op;
    private String jsonObj;
    private String result = "";
    protected final Logger log = LoggerFactory.getLogger(getClass());

    public CasServicesCommand() {
    }

    public void execute() throws Exception {
        log.info("execute: CALLED");

        log.info("execute: received cmd w/op = \"" + op + "\"");
        log.info("execute:           jsonObj = \"" + jsonObj + "\"");

        if (op.equals("getClientConfiguration")) {
            ClientConfigurationDescriptor client = new ClientConfigurationDescriptor();

            try {
                Configuration config = new Configuration();
                JSONObject configuration = new JSONObject();
                String value = (String) config.get(BrowserName);
                // The value of the BrowserName property is assumed to be the serialization of a JSON object
                if (value != null) configuration.put(BrowserName, new JSONObject(value));
                value = (String) config.get(Configuration.CLIENT_GROUP);
                configuration.put(ClientGroupName, value);
                value = (String) config.get(Configuration.CLIENT_TYPE);
                configuration.put(ClientTypeName, value);
                value = (String) config.get(Configuration.OEM_VARIANT);
                configuration.put(OemVariantName, value);
                value = (String) config.get(Configuration.CFG_VARIANT);
                configuration.put(CfgVariantName, value);
                value = (String) config.get(Configuration.ENVIRONMENT);
                configuration.put(EnvironmentIdName, value);
                value = (String) config.get(Configuration.LICENSE_KEY);
                client.setLicenseKey(value);
                configuration.put(LicenseKeyName, value);
                value = (String) config.get(Configuration.LOGICAL_NAME);
                client.setLogicalName(value);
                configuration.put(LogicalNameName, value);
                value = (String) config.get(Configuration.LOG_LEVEL);
                configuration.put(LogLevelName, value);
                value = (String) config.get(Configuration.SERVER_URI);
                client.setServerIP(value);
                configuration.put(ServerIPName, value);
                client.setConfiguration(configuration.toString());
            } catch (JSONException je) {
                log.info("CasServicesImpl.read: Configuration retrieval failed due to " + je);
                result = "error: Configuration retrieval failed due to " + je;
            }

            result = getCodec().encode(client);
        } else if (op.equals("setClientConfiguration")) {
            ClientConfigurationDescriptor client = new ClientConfigurationDescriptor();
            if (!getCodec().decode(client, jsonObj)) {
                result = "error: could not decode ClientConfigurationDescriptor from JSON text \"" + jsonObj + "\"";
                return;
            }

            try {
                String source = client.getConfiguration();
                JSONObject configuration = (source != null) ? new JSONObject(source) : null;
                Configuration config = new Configuration();
                if (configuration != null || client.isMasked()) {
                    if (client.getLogicalName() != null)
                        config.set(LogicalNameName, client.getLogicalName());
                    if (client.getLicenseKey() != null)
                        config.set(LicenseKeyName, client.getLicenseKey());
                    if (client.getServerIP() != null)
                        config.set(ServerIPName, client.getServerIP());
                    // Values in the JSON configuration String take precedence
                    if (configuration != null) {
                        if (configuration.has(BrowserName))
                            setConfiguration(BrowserName, configuration, config);
                        if (configuration.has(ClientGroupName))
                            setConfiguration(ClientGroupName, configuration, config);
                        // these are constant in android
                        //if (configuration.has(ClientTypeName)) setConfiguration(ClientTypeName, configuration, config);
                        //if (configuration.has(OemVariantName)) setConfiguration(OemVariantName, configuration, config);
                        //if (configuration.has(CfgVariantName)) setConfiguration(CfgVariantName, configuration, config);
                        if (configuration.has(EnvironmentIdName))
                            setConfiguration(EnvironmentIdName, configuration, config);
                        if (configuration.has(LicenseKeyName))
                            setConfiguration(LicenseKeyName, configuration, config);
                        if (configuration.has(LogicalNameName))
                            setConfiguration(LogicalNameName, configuration, config);
                        // this has no effect on android
                        //if (configuration.has(LogLevelName)) setConfiguration(LogLevelName, configuration, config);
                        if (configuration.has(ServerIPName))
                            setConfiguration(ServerIPName, configuration, config);
                    }
                } else {
                    config.set(LogicalNameName, client.getLogicalName());
                    config.set(LicenseKeyName, client.getLicenseKey());
                    config.set(ServerIPName, client.getServerIP());
                }
            } catch (ConfigurationException ce) {
                result = "error: Configuration update failed due to " + ce;
            } catch (JSONException je) {
                result = "error: Configuration update failed due to " + je;
            }
            result = "status: ok";
        } else if (op.equals("createClientInterrupt")) {
            ClientInterruptDescriptor client = new ClientInterruptDescriptor();
            if (!getCodec().decode(client, jsonObj)) {
                result = "error: could not decode ClientInterruptDescriptor from JSON text \"" + jsonObj + "\"";
                return;
            }

            VJMLBean vjmlBean = (VJMLBean) applicationContext.getBean("vjml");
            SourcedInterruptCommand command = new SourcedInterruptCommand(client.getSource(), client.getContent());
            VJMLMessage message = new VJMLMessage(command, VJMLMessage.HighestPriority);

            // Get a VJML channel and send
            VJMLChannel channel = null;
            try {
                channel = vjmlBean.getAsyncChannel();
                channel.putMessage(message);
                channel.sendNow();
            } catch (VJMLNameConflictException vnce) {
                result = "error: CasServicesImpl.create(ClientInterruptDescriptor): channel NOT set up due to " + vnce;
                return;
            }
            result = "status: ok";
        } else if (op.equals("getClientStatus")) {
            ClientStatusDescriptor status = new ClientStatusDescriptor();

            Configuration config = new Configuration();
            status.setStatus("{ \"" + LogicalNameName + "\": \"" + (String) config.get(LogicalNameName) + "\" }");

            result = getCodec().encode(status);
        } else {
            result = "error: invalid operation \"" + op + "\"";
        }
    }

    protected void setConfiguration(String name, JSONObject json, Configuration config) throws ConfigurationException, JSONException {
        String value = json.getString(name);
        if (value.equals("null")) value = null;
        config.set(name, value);
    }

    private JSONDescriptorCodec getCodec() {
        if (codec == null) {
            codec = new JSONDescriptorCodec();
        }
        return codec;
    }

    // JavaBean Methods
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getJsonObj() {
        return jsonObj;
    }

    public void setJsonObj(String jsonObj) {
        this.jsonObj = jsonObj;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
