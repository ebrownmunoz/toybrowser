package voxware.app.commands;

public class PersonalizeOptionsCommand {

  private String mode = null;
  private String altVoice = null;
  private String scriptId = null;
  private String voicePrompted = "false";
  private String voiceControlled = "false";
  private String voiceAnnotated = "false";
  private String collectingAudio = "false";

  public PersonalizeOptionsCommand() {
  }

  public String getMode() {
    return this.mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public String getScriptId() {
    return this.scriptId;
  }

  public void setScriptId(String scriptId) {
    this.scriptId = scriptId;
  }

  public String getAltVoice() {
    return this.altVoice;
  }

  public void setAltVoice(String altVoice) {
    this.altVoice = altVoice;
  }

  public String getVoicePrompted() {
    return this.voicePrompted;
  }

  public void setVoicePrompted(String voicePrompted) {
    this.voicePrompted = voicePrompted;
  }

  public String getVoiceControlled() {
    return this.voiceControlled;
  }

  public void setVoiceControlled(String voiceControlled) {
    this.voiceControlled = voiceControlled;
  }

  public String getVoiceAnnotated() {
    return this.voiceAnnotated;
  }

  public void setVoiceAnnotated(String voiceAnnotated) {
    this.voiceAnnotated = voiceAnnotated;
  }

  public String getCollectingAudio() {
    return this.collectingAudio;
  }

  public void setCollectingAudio(String collectingAudio) {
    this.collectingAudio = collectingAudio;
  }

}
