package voxware.app.commands;

public class AdvancedOptionsCommand {

  private String enroll = "false";
  private String randomize = "false";
  private String environmentId;

  public AdvancedOptionsCommand() {
  }

  public String getEnroll() {
    return this.enroll;
  }

  public void setEnroll(String enroll) {
    this.enroll = enroll;
  }

  public String getRandomize() {
    return this.randomize;
  }

  public void setRandomize(String randomize) {
    this.randomize = randomize;
  }

  public String getEnvironmentId() {
    return this.environmentId;
  }

  public void setEnvironmentId(String environmentId) {
    this.environmentId = environmentId;
  }
}
