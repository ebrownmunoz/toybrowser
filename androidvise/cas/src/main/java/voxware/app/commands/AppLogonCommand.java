package voxware.app.commands;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppLogonCommand {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private String algorithm = "MD5";
    public static final int DEFAULT_RADIX = 36;

    private String userId = null;
    private String password = null;
    private String applicationId = null;
    private String languageId = null;
    private String action = null;
    private List applications = null;
    private String state = null;
    private String exitUrl = null;

    public AppLogonCommand() {
    }

    public String getAlgorithm() {
        return this.algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        if (algorithm == null || algorithm.equalsIgnoreCase("none")) {
            this.password = password;
            log.info("AppLogonCommand.setPassword: unhashed password = " + this.password);
        } else {
            try {
                this.password = hash(password, algorithm, DEFAULT_RADIX);
                log.info("AppLogonCommand.setPassword: Password.hash(****, " + algorithm + ", " + DEFAULT_RADIX + ") = " + this.password);
            } catch (Exception e) {
                log.info("AppLogonCommand.setPassword: " + e);
            }
        }
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List getApplications() {
        return applications;
    }

    public void setApplications(List applications) {
        this.applications = applications;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getExitUrl() {
        return exitUrl;
    }

    public void setExitUrl(String exitUrl) {
        this.exitUrl = exitUrl;
    }

    private String hash(String password, String algorithm, int radix) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = java.security.MessageDigest.getInstance(algorithm);
        byte[] bytes = password.getBytes("UTF8");
        digest.update(bytes, 0, bytes.length);
        return new BigInteger(digest.digest()).abs().toString(radix);
    }
}

