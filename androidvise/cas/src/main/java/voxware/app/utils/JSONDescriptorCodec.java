package voxware.app.utils;

import org.json.JSONObject;
import org.json.JSONException;

import com.voxware.remote.services.cas.ClientConfigurationDescriptor;
import com.voxware.remote.services.cas.ClientInterruptDescriptor;
import com.voxware.remote.services.cas.ClientStatusDescriptor;

public class JSONDescriptorCodec {
  public JSONDescriptorCodec() {}

  public String encode(ClientConfigurationDescriptor descriptor) {
    JSONObject o = new JSONObject();
    try {
      o.put("masked", descriptor.isMasked());
      o.put("logicalName", descriptor.getLogicalName());
      o.put("licenseKey", descriptor.getLicenseKey());
      o.put("serverIP", descriptor.getServerIP());
      o.put("configuration", descriptor.getConfiguration());
      o.put("ttsConfiguration", descriptor.getTtsConfiguration());
    } catch (JSONException je) {
      System.out.println("ERROR in CCD encode - JSONException!!");
      return null;
    }
    return o.toString();
  }

  public boolean decode(ClientConfigurationDescriptor descriptor, String jsonText) {
    JSONObject o = null;
    try {
     o = new JSONObject(jsonText);
    } catch (JSONException je) {
      System.out.println("ERROR in CCD encode - could not create json object from text \"" + jsonText + "\"");
      return false;
    }

    try {
      if (o.has("masked"))
        descriptor.setMasked(o.getBoolean("masked"));
      if (o.has("logicalName"))
        descriptor.setLogicalName(o.getString("logicalName"));
      if (o.has("licenseKey"))
        descriptor.setLicenseKey(o.getString("licenseKey"));
      if (o.has("serverIP"))
        descriptor.setServerIP(o.getString("serverIP"));
      if (o.has("configuration"))
        descriptor.setConfiguration(o.getString("configuration"));
      if (o.has("ttsConfiguration"))
        descriptor.setTtsConfiguration(o.getString("ttsConfiguration"));
    } catch (JSONException je) {
      System.out.println("ERROR in CCD decode - JSONException!!");
      return false;
    }
    return true;
  }

  public String encode(ClientInterruptDescriptor descriptor) {
    JSONObject o = new JSONObject();
    try {
      o.put("source", descriptor.getSource());
      o.put("content", descriptor.getContent());
    } catch (JSONException je) {
      System.out.println("ERROR in CID encode - JSONException!!");
      return null;
    }
    return o.toString();
  }

  public boolean decode(ClientInterruptDescriptor descriptor, String jsonText) {
    JSONObject o = null;
    try {
     o = new JSONObject(jsonText);
    } catch (JSONException je) {
      System.out.println("ERROR in CID decode - could not create json object from text \"" + jsonText + "\"");
      return false;
    }

    try {
      if (o.has("source"))
        descriptor.setSource(o.getString("source"));
      if (o.has("content"))
        descriptor.setContent(o.getString("content"));
    } catch (JSONException je) {
      System.out.println("ERROR in CID decode - JSONException!!");
      return false;
    }
    return true;
  }

  public String encode(ClientStatusDescriptor descriptor) {
    JSONObject o = new JSONObject();
    try {
      o.put("status", descriptor.getStatus());
    } catch (JSONException je) {
      System.out.println("ERROR in CSD encode - JSONException!!");
      return null;
    }
    return o.toString();
  }

  public boolean decode(ClientStatusDescriptor descriptor, String jsonText) {
    JSONObject o = null;
    try {
     o = new JSONObject(jsonText);
    } catch (JSONException je) {
      System.out.println("ERROR in CSD decode - could not create json object from text \"" + jsonText + "\"");
      return false;
    }

    try {
      if (o.has("status"))
        descriptor.setStatus(o.getString("status"));
    } catch (JSONException je) {
      System.out.println("ERROR in CSD decode - JSONException!!");
      return false;
    }
    return true;
  }
}
