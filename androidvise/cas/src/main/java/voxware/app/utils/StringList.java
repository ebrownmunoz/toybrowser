package voxware.app.utils;

import java.util.ArrayList;
import java.util.List;

public class StringList {

  protected List stringList = null;

  public StringList() {
  }

  public List getStringList() {
    return stringList;
  }

  public void setStringList(List stringList) {
    this.stringList = stringList;
  }

  public void add(Object o) {
    if (stringList == null) {
      stringList = new ArrayList();
    }
    stringList.add(o);
  }

  public String getConcatenation() {
    if (stringList == null || stringList.size() == 0) {
      return "";
    }

    StringBuffer buf = new StringBuffer("'").append((String)stringList.get(0)).append("'");
    for (int i = 1; i < stringList.size(); i++) {
      buf.append(",'").append((String)stringList.get(i)).append("'");
    }

    return buf.toString();
  }
}
