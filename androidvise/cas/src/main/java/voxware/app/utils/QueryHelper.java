package voxware.app.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.HashMap;
import java.util.StringTokenizer;

import voxware.cas.framework.*;
import voxware.cas.commands.*;

public class QueryHelper extends StringList {
  private String objName = null;        // Name of the result object the query produces
  private String string = null;         // The string submitted to the constructor
  private Map    map = null;            // Map of the NVP's

  public QueryHelper() {
    map = new HashMap();
  }

  public String getObjName() {
    return objName;
  }

  public void setObjName(String objName) {
    this.objName = objName;
  }

  public QueryHelper(String string) {
    this.string = string;
  }

  public QueryHelper(NameValuePairsCommand nvpCommand) {
    this();
    if (nvpCommand != null) {
      NameValuePair[] nvps = nvpCommand.getNameValuePairs();
      if (nvps != null) {
        for (int i = 0; i < nvps.length; i++) {
          NameValuePair nvp = nvps[i];
          map.put(nvp.name(), nvp.value());
        }
      }
    }
  }

  public Map getMap() {
    return map;
  }

  public void setMap(Map map) {
    this.map = map;
  }

  public void put(String name, String value) {
    map.put(name, value);
  }

  /** Parse method for handling JS-serialized lists of form ['string1','string2'] */
  public void parse() {
    if (string.startsWith("['")) {
      stringList = new ArrayList();
      String str = string.substring(string.indexOf("['")+2, string.indexOf("']"));
      StringTokenizer st = new StringTokenizer(str, "','");
      while (st.hasMoreTokens()) {
        String nt = st.nextToken();
        if (nt.equals(" "))
          continue;
        this.add(nt);
      }
    } else if (string.startsWith("[\"")) {
      stringList = new ArrayList();
      String str = string.substring(string.indexOf("[\"")+2, string.indexOf("\"]"));
      StringTokenizer st = new StringTokenizer(str, "\",\"");
      while (st.hasMoreTokens()) {
        String nt = st.nextToken();
        if (nt.equals(" "))
          continue;
        this.add(nt);
      }
    } else {
       // Not a square-brackets enclosed list of quoted items.  Punt!
      return;
    }
  }

  /**
   * Create an array of NameValuePairs containing the Map contents
   */
  public NameValuePair[] getArray() {
    NameValuePair[] nvps = null;

    if (map != null && map.size() > 0) {
      nvps = new NameValuePair[map.size()];
      /** Handle "query" specially - it has to be the first NameValuePair in the array */

      int i = 0;
      String query = null;

      // WARNING - nothing but Strings can be in this map!
      if ((query = (String)map.remove("query")) != null) {
        nvps[i++] = new NameValuePair("query", query);
      }

      Set keys = map.keySet();
      Iterator it = keys.iterator();
      while (it.hasNext()) {
        String key = (String) it.next();

        nvps[i++] = new NameValuePair(key, (String)map.get(key));
      }
    }
    return nvps;
  }
}
