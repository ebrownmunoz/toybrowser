package voxware.app.utils;

public class StringLiteralSupport {

  /**
   * Utilities for converting between Latin-1 Java(Script) String literals and Java(Script) Unicode Strings
   */

   // Return the Unicode String representation of the given Java(Script) String literal
  public static String unicodeFromStringLiteral(String literal) {
    int escape = literal.indexOf('\\');
    if (escape < 0) {
      return literal;
    } else {
      int literalEnd = literal.length();
      int codeEnd = escape + 2;     // Default escape sequence length is two
      char unicode = '\u0000';
      if (escape + 1 < literalEnd) {
        switch(literal.charAt(escape + 1)) {
          case 'u':
            if (escape + 5 < literalEnd) unicode = (char) Integer.parseInt(literal.substring(escape + 2, (codeEnd = escape + 6)), 16);  // unicode
            else return literal;
            break;
          case 'x':
            if (escape + 3 < literalEnd) unicode = (char) Integer.parseInt(literal.substring(escape + 2, (codeEnd = escape + 4)), 16);  // hex
            else return literal;
            break;
          case 'n':
            unicode = '\n'; // newline
            break;
          case 't':
            unicode = '\t'; // tab
            break;
          case 'b':
            unicode = '\b'; // backspace
            break;
          case 'r':
            unicode = '\r'; // return
            break;
          case 'f':
            unicode = '\f'; // form feed
            break;
          case '\\':
            unicode = '\\'; // backslash
            break;
          case '\'':
            unicode = '\''; // single quote
            break;
          case '\"':
            unicode = '\"'; // double quote
            break;
          default:
            unicode = literal.charAt(escape + 1);
        }
      } else {
        return literal;
      }
      return literal.substring(0, escape) + unicode + unicodeFromStringLiteral(literal.substring(codeEnd));
    }
  }

   // Append the Unicode String representation of the given Java(Script) String literal to the given StringBuffer
  public static StringBuffer appendUnicodeForStringLiteral(StringBuffer buffer, String literal) {
    return buffer.append(unicodeFromStringLiteral(literal));
  }

   // Return the Java(Script) String literal representation of the given Unicode String
  public static String stringLiteralFromUnicode(String unicode) {
    return appendStringLiteralForUnicode(new StringBuffer(unicode.length()), unicode).toString();
  }

   // Append the Java(Script) String literal representation of the given Unicode String to the given StringBuffer
  public static StringBuffer appendStringLiteralForUnicode(StringBuffer buffer, String unicode) {
    for (int index = 0; index < unicode.length(); index++) {
      char code = unicode.charAt(index);
      if (code < 0x80) {
        buffer.append(code);
      } else {
        if (code < 0x100) buffer.append("%u00");
        else if (code < 0x1000) buffer.append("%u0");
        else buffer.append("%u");
        buffer.append(Integer.toString(code, 16));
      }
    }
    return buffer;
  }

}
