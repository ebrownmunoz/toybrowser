package voxware.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Map;

import nl.justobjects.pushlet.core.Event;
import voxware.cas.commands.NameValuePairsCommand;
import voxware.cas.framework.Command;
import voxware.cas.framework.CommandHandler;
import voxware.cas.framework.exceptions.CommandException;

public class DisconnectHandler implements CommandHandler {
    protected final Logger log = LoggerFactory.getLogger(getClass());
    private String port = "";
    private String pushletSubject = null;
    private String pushletUri = null;
    private String method = null;
    private boolean sendUrl = false;

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPushletSubject() {
        return pushletSubject;
    }

    public void setPushletSubject(String pushletSubject) {
        this.pushletSubject = pushletSubject;
    }

    public String getPushletUri() {
        return pushletUri;
    }

    public void setPushletUri(String pushletUri) {
        this.pushletUri = pushletUri;
    }

    public boolean getSendUrl() {
        return sendUrl;
    }

    public void setSendUrl(boolean sendUrl) {
        this.sendUrl = sendUrl;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean isProperty(String value) {
        return value.startsWith("personalize.");
    }

    public void execute(Command command, Map model) throws CommandException {
    }

    public Object createEvent(Command command, Map model) throws CommandException {
        log.info("DisconnectHandler.createEvent CALLED");
        Event event = null;

        if (command instanceof NameValuePairsCommand) {
            event = Event.createDataEvent(pushletSubject);

            if (sendUrl) {
                // IP address of local host needed for pushed URL
                String methodParameter = "?method=" + method;

                event.setField("disconnect", "true");
                String url = new String( getPushletUri() + methodParameter);
                event.setField("url", url);
            }
        } else {
            log.error("DisconnectHandler: ERROR command " + command.getClass().getName() + " not an instance of NameValuePairsCommand");
        }

        return event;
    }
}
