/**
 * Controller for the Client/Voxmanager Logon Page
 */
 
package voxware.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import voxware.app.beans.ResponseObject;
import voxware.app.beans.UserContainer;
import voxware.app.beans.VJMLTransaction;
import voxware.app.beans.propertyeditors.UTF8StringPropertyEditor;
import voxware.app.commands.AppLogonCommand;
import voxware.app.utils.QueryHelper;

/**
 * @author Craig A. Vanderborgh
 */
public class LogonFormController extends SimpleFormController {

  protected final Logger log = LoggerFactory.getLogger(getClass());
  private static String[] USERS = { "pigpen", "charlie", "linus", "lucy" };
  private String launchView = null;
  private String personalizeView = null;
  private String personalizeOptionsView = null;
  private String vsoView = null;

  private int                     retries;
  public  int                  getRetries()             { return this.retries; }
  public  void                 setRetries(int retries)  { this.retries = retries; }

  private long           userRetryTimeout;
  public  long        getUserRetryTimeout()             { return this.userRetryTimeout; }
  public  void        setUserRetryTimeout(long timeout) { this.userRetryTimeout = timeout; }
  
  private long    applicationRetryTimeout;
  public  long getApplicationRetryTimeout()             { return this.applicationRetryTimeout; }
  public  void setApplicationRetryTimeout(long timeout) { this.applicationRetryTimeout = timeout; }
  

  public ModelAndView onSubmit(Object command) throws ServletException {
    log.info("onSubmit: CALLED");
    AppLogonCommand alc = (AppLogonCommand) command;

    /** Normal logon via the CAS GUI */
    ModelAndView mav = null;
    if (alc.getState() != null) {
      log.info("onSubmit: validated action is \"" + alc.getAction() + "\", state \"" + alc.getState() + "\"");
      /** Jump directly to pushlet, personalization already in progress */
      if (alc.getState().equals("running")) {
        log.info("onSubmit: going directly to personalization..");
        mav = new ModelAndView(new RedirectView(getPersonalizeView()));
      } else {
        if (alc.getAction().equals("personalize") || alc.getAction().equals("train") || alc.getAction().equals("test")) {
          log.info("onSubmit: going to personalize options page..");
          mav = new ModelAndView(new RedirectView(getPersonalizeOptionsView()));
        } else if (alc.getAction().equals("launch")) {
          log.info("onSubmit: going to launch page..");
          mav = new ModelAndView(new RedirectView(getLaunchView()));
        } else {
          /* Display form view page by default */
          log.info("onSubmit: WARNING defaulting to form view for unknown action \"" + alc.getAction() + "\"");
          mav = new ModelAndView(new RedirectView(getFormView()));
        }
      }
    } else {
      mav = new ModelAndView(new RedirectView(getSuccessView()));
      log.info("onSubmit: returning mav " + mav + ", validated action is \"" + alc.getAction() + "\"");
    }
 

   return mav;
  }

  protected Map referenceData(HttpServletRequest request) {
    Map m = new HashMap();
    m.put("hostinfo", new HostInfo(request));
    QueryHelper qh = new QueryHelper();
    qh.put("query", "users");
    qh.setObjName("users");
    VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
    vt.setRetries(retries, userRetryTimeout);
    ResponseObject response = null;
    try {
      response = vt.execute(qh);
      if (response.status()) {
        List users = (List) response.getMap().get("users");
         // Translate JavaScript serialization of user names into Unicode for display
        ArrayList displayList = new ArrayList(users.size());
        Iterator userit = users.iterator();
        while (userit.hasNext()) displayList.add(voxware.util.StringLiteralSupport.unicodeFromStringLiteral((String) userit.next()));
        m.put("users", displayList);
      } else {
        log.error("referenceData: ERROR " + response.getError() + " occured in VJMLTransaction");
      }
    } catch (Exception e) {
      log.error("referenceData: ERROR (no response) occured in VJMLTransaction", e);
    }
    return m;
  }

  protected Object formBackingObject(HttpServletRequest request) throws ServletException {
    Object fbo = getApplicationContext().getBean("appLogonCommand");

    log.info("formBackingObject: returning fbo " + fbo);
    return fbo;
  }

  protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
    log.info("initBinder: CALLED");
    binder.registerCustomEditor(String.class, new UTF8StringPropertyEditor());
  }

  protected void onBindAndValidate(HttpServletRequest request, Object command,
                                   BindException errors) throws Exception {
    log.info("onBindAndValidate: CALLED");
    AppLogonCommand alc = (AppLogonCommand) command;

    if (errors.getErrorCount() != 0) {
      log.info("onBindAndValidate: INFO called w/validation errors");
      return;
    }
 
    /**
     * This is where logon ACTUALLY HAPPENS.  If this routine is called and there
     * were no validation errors, we log on by obtaining the singleton
     * UserContainer - if it's available.
     *
     * If the UserContainer is unavailable, LOGON IS DISALLOWED
     */
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    if (!container.doLogon(request)) {
      log.info("onBindAndValidate: Logon fails due to existence of another active session");
      errors.rejectValue("userId", "error.session-unavail", "Cannot log on. Another user is currently logged on.");
    }

    /** Finally, transfer needed info from the command to the container */
    container.setUserId(alc.getUserId());
    container.setPassword(alc.getPassword());
    container.setApplications(alc.getApplications());
    container.setApplicationId(alc.getApplicationId());
    container.setLanguageId(alc.getLanguageId());
    container.setState(alc.getState());
    container.setExitUrl(alc.getExitUrl());
    log.info("onBindAndValidate: logging on " + alc.getUserId() + "/" + alc.getLanguageId());

    /** If not already extant, pre-populate list of available applications for use later */ 
    if (container.getApplications() == null) {
      QueryHelper qh = new QueryHelper();
      qh.put("query", "applications");
      qh.put("userId", container.getUserId());
      qh.put("password", container.getPassword());
 
      VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
      vt.setRetries(retries, applicationRetryTimeout);
      ResponseObject ro = vt.execute(qh);
      log.debug("onBindAndValidatet: applications txn.execute() returns!");
      if (ro.status()) {
        List apps = (List) ro.getMap().get("applications");
        if (apps.size() > 0) {
          log.info("onBindAndValidate: INFO there are " + apps.size() + " applications available for user \"" + container.getUserId() + "\"");
        } else {
          log.info("onBindAndValidate: INFO no applications available for user \"" + container.getUserId() + "\"");
        }
        container.setApplications(apps);
      }
    }
    log.info("onBindAndValidate: checkLogon " + container.checkLogon(request));
  }

  public String getLaunchView() {
    return launchView;
  }

  public void setLaunchView(String launchView) {
    this.launchView = launchView;
  }    

  public String getPersonalizeView() {
    return personalizeView;
  }

  public void setPersonalizeView(String personalizeView) {
    this.personalizeView = personalizeView;
  }

  public String getPersonalizeOptionsView() {
    return personalizeOptionsView;
  }

  public void setPersonalizeOptionsView(String personalizeOptionsView) {
    this.personalizeOptionsView = personalizeOptionsView;
  }

  public String getVsoView() {
    return vsoView;
  }

  public void setVsoView(String vsoView) {
    this.vsoView = vsoView;
  }
}
