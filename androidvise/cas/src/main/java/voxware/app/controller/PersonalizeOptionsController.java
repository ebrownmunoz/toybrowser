/**
 * Controller for the Client/Voxmanager Logon Page
 */
 
package voxware.app.controller;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import voxware.app.beans.UserContainer;
import voxware.app.commands.PersonalizeOptionsCommand;


/**
 * @author Craig A. Vanderborgh
 */
public class PersonalizeOptionsController extends MultiActionController {

  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String advancedOptionsView = null;
  private String formView = null;
  private String personalizeView = null;
  private String testView = null;

  public ModelAndView display(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("display: CALLED");
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");

    /** container.scripts is populated at authentication-time */

    ModelAndView mav = new ModelAndView(getFormView());
    setup(mav);

    return mav;
  }

   /** Add the default entries to model for the "Personalize Options" view */
  private void setup(ModelAndView mav) {
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    mav.addObject("scripts", container.getScripts());
  }
                              
  private void containerize(Object command) {
    log.info("containerize: CALLED");
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");

    PersonalizeOptionsCommand poc = (PersonalizeOptionsCommand) command;
    log.info("containerize: poc = " + poc);
    log.info("containerize: poc.getMode() = " + poc.getMode());

    container.setVoicePrompted(poc.getVoicePrompted().equalsIgnoreCase("true"));
    if (container.getVoicePrompted())
      log.info("Prompting by voice");
    container.setVoiceControlled(poc.getVoiceControlled().equalsIgnoreCase("true"));
    if (container.getVoiceControlled())
      log.info("Voice control enabled");
    container.setVoiceAnnotated(poc.getVoiceAnnotated().equalsIgnoreCase("true"));
    if (container.getVoiceAnnotated())
      log.info("Annotating by voice");
    container.setCollectingAudio(poc.getCollectingAudio().equalsIgnoreCase("true"));
    if (container.getCollectingAudio())
      log.info("Collecting audio data");

    container.setScriptId(poc.getScriptId());
    /*
    container.setWords(null);
    container.setPhrases(null);
    container.setEnroll(false);
    container.setRandomize(false);
    */   
  }

  public ModelAndView advanced(HttpServletRequest request, HttpServletResponse response, PersonalizeOptionsCommand command) throws ServletException {
    log.info("advanced: CALLED");
    containerize(command);

    return new ModelAndView(new RedirectView(getAdvancedOptionsView()));
  }

  public ModelAndView personalize(HttpServletRequest request, HttpServletResponse response, PersonalizeOptionsCommand command) throws ServletException {
    log.info("personalize: CALLED");
    containerize(command);

    return new ModelAndView(new RedirectView(getPersonalizeView()));
  }

  public ModelAndView test(HttpServletRequest request, HttpServletResponse response, PersonalizeOptionsCommand command) throws ServletException {
    log.info("personalize: CALLED");
    containerize(command);

    return new ModelAndView(new RedirectView(getTestView()));
  }


  /*
  public ModelAndView onSubmit(Object command) throws ServletException {

    log.info("onSubmit: CALLED");
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");

    PersonalizeOptionsCommand poc = (PersonalizeOptionsCommand) command;
    log.info("onSubmit: poc = " + poc);
    log.info("onSubmit: poc.getMode() = " + poc.getMode());

    container.setVoicePrompted(poc.getVoicePrompted().equalsIgnoreCase("true"));
    if (container.getVoicePrompted()) log.info("Prompting by voice");
    container.setVoiceControlled(poc.getVoiceControlled().equalsIgnoreCase("true"));
    if (container.getVoiceControlled()) log.info("Voice control enabled");
    container.setVoiceAnnotated(poc.getVoiceAnnotated().equalsIgnoreCase("true"));
    if (container.getVoiceAnnotated()) log.info("Annotating by voice");
    container.setCollectingAudio(poc.getCollectingAudio().equalsIgnoreCase("true"));
    if (container.getCollectingAudio()) log.info("Collecting audio data");

    container.setWords(null);
    container.setPhrases(null);
    container.setEnroll(false);
    container.setRandomize(false);

    ModelAndView mav = null;
    if (poc.getMode().equals("all")) {
      mav = new ModelAndView(new RedirectView(getPersonalizeView()));
      log.info("onSubmit: mav is " + mav + " for all-mode personalization");
    } else {
      mav = new ModelAndView(new RedirectView(getAdvancedOptionsView()));
      log.info("onSubmit: mav is " + mav + " for advanced-mode personalization");
    }
    return mav;
  }
  */



/*
  protected Object formBackingObject(HttpServletRequest request) throws ServletException {
    Object fbo = getApplicationContext().getBean("personalizeOptionsCommand");

    log.info("formBackingObject: returning fbo " + fbo);
    return fbo;
  }
*/

  public String getAdvancedOptionsView() {
    return advancedOptionsView;
  }

  public void setAdvancedOptionsView(String advancedOptionsView) {
    this.advancedOptionsView = advancedOptionsView;
  }

  public String getFormView() {
    return formView;
  }

  public void setFormView(String formView) {
    this.formView = formView;
  }

  public String getPersonalizeView() {
    return personalizeView;
  }

  public void setPersonalizeView(String personalizeView) {
    this.personalizeView = personalizeView;
  }

  public String getTestView() {
    return testView;
  }

  public void setTestView(String testView) {
    this.testView = testView;
  }
}
