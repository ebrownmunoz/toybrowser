/*
 * Controller for the Application Selection Page
 */
 
package voxware.app.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.UTFDataFormatException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.jetty.server.HttpOutput;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import voxware.cas.framework.Command;
import voxware.cas.commands.NameValuePairsCommand;

import voxware.app.utils.*;
import voxware.app.beans.*;
import voxware.app.beans.UserContainer;
import voxware.util.UTF8Support;


/**
 * @author Craig A. Vanderborgh
 */
public class ApplicationController extends MultiActionController {

  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String applicationsView = null;
  private String mainView = null;
  private String personalizeView = null;
  private String personalizeOptionsView = null;

  private int          retries;
  public  int       getRetries()             { return this.retries; }
  public  void      setRetries(int retries)  { this.retries = retries; }

  private long       applicationRetryTimeout;
  public  long    getApplicationRetryTimeout()             { return this.applicationRetryTimeout; }
  public  void    setApplicationRetryTimeout(long timeout) { this.applicationRetryTimeout = timeout; }
  
  private long    authenticationRetryTimeout;
  public  long getAuthenticationRetryTimeout()             { return this.authenticationRetryTimeout; }
  public  void setAuthenticationRetryTimeout(long timeout) { this.authenticationRetryTimeout = timeout; }
  
  public ModelAndView display(HttpServletRequest request, HttpServletResponse response) throws ServletException {

    log.info("display: CALLED");

    /** Get applications from UserContainer */
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");

    /**
     * COMPROMISE:  Due to timing problems in the browser, we currently do not request the latest
     *              apps list each time they're displayed to ensure the list is current.
     *              Instead, we cache them in the UserContainer, making a request for the apps
     *              list only if the container does not already contain a copy of the list.
     */
    if (container.getApplications() == null) {
      /** Use "applications" query to get list of available voice apps*/
      QueryHelper qh = new QueryHelper();
      qh.put("query", "applications");
      qh.put("userId", container.getUserId());
      qh.put("password", container.getPassword());
 
      VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
      vt.setRetries(retries, applicationRetryTimeout);
      ResponseObject ro = vt.execute(qh);
      log.debug("display: execute returns!");
      if (ro.status()) {
        List apps = (List) ro.getMap().get("applications");
        if (apps.size() > 0) {
          log.info("display: INFO there are " + apps.size() + " applications available for user \"" + container.getUserId() + "\"");
        } else {
          log.info("display: INFO no applications available for user \"" + container.getUserId() + "\"");
        }
        container.setApplications(apps);
      }
    }

    ModelAndView mav = new ModelAndView(getApplicationsView());
    setup(request, mav);
    return mav;
  }

  /** Add the default entries to model for the "Applications" view */
  private void setup(HttpServletRequest request, ModelAndView mav) {
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    mav.addObject("hostinfo", new HostInfo(request));
    List applications = container.getApplications();
     // Translate JavaScript serialization of application names into Unicode for display
    ArrayList displayList = new ArrayList(applications.size());
    Iterator applicationit = applications.iterator();
    while (applicationit.hasNext()) displayList.add(StringEscapeUtils.unescapeJava((String) applicationit.next()));
    mav.addObject("applications", displayList);
  }

  private ResponseObject authenticate(UserContainer container, String action) throws ServletException {

    /** Quadruplet authentication via VJML */
    log.info("authenticate: auth attempt using \"authentication\" query..");
    QueryHelper qh = new QueryHelper();
    qh.put("query", "authentication");
    qh.put("userId", container.getUserId());
    qh.put("password", container.getPassword());
    qh.put("applicationId", container.getApplicationId());
    qh.put("languageId", container.getLanguageId());
    qh.put("action", action);

    VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
    vt.setRetries(retries, authenticationRetryTimeout);
    ResponseObject response = vt.execute(qh);

    /* Caller must deal with errors */
    if (response.status()) {
      Map m = response.getMap();
      String state;

      if (m != null && (state = (String) m.get("state")) != null) {
        container.setState(state);
      }

      List scripts;
      scripts = (List) m.get("scripts");
      if (scripts != null) {
        log.info("authenticate: there are " + scripts.size() + " scripts");          
        container.setScripts(scripts);
      } else {
        log.info("authenticate: THERE ARE NO SCRIPTS");
      }
      log.info("authenticate: No script list in ro");

      container.setEnvironmentId((String) m.get("environmentId"));
    }

    return response;
  }

  public ModelAndView launch(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("launch: CALLED");

    /** Get the specfied application from the request, store in the container */
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    parseApplicationId(request, container);

    ResponseObject ro = authenticate(container, "launch");

    container.doLogoff(request);
    return new ModelAndView(new RedirectView(getMainView()));
  }

  public ModelAndView personalize(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("personalize: CALLED");
    
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    parseApplicationId(request, container);

    ResponseObject ro = authenticate(container, "train");

    ModelAndView mav = null;
    if (!ro.status()) {
      ResourceBundleBean bundleBean = (ResourceBundleBean) getApplicationContext().getBean("baseBundle");
      bundleBean.setLanguageId(container.getLanguageId());
      ResourceBundle bundle = bundleBean.getBundle();
      mav = new ModelAndView(getApplicationsView());
      mav.addObject("error", new String(bundle.getString(ro.getError()) + " " + container.getUserId() + "/" + container.getApplicationId()));
      log.info("personalize: authentication failed");
      setup(request, mav);
    } else {
      if (container.getState() != null && container.getState().equals("running")) {
        log.info("personalize: authentication ok, returning MAV for personalize");
        mav = new ModelAndView(new RedirectView(getPersonalizeView()));
      } else {
        log.info("personalize: authentication ok, returning MAV for personalize OPTIONS");
        mav = new ModelAndView(new RedirectView(getPersonalizeOptionsView()));
      }
    }

    return mav;
  }

  // Convenience routine to help with the case where applicationId is something like 
  // "charlie(en-US)", where the userId and languageId are conjoined.  This separates them, and sets
  // both the applicationId and languageId.
  private void parseApplicationId(HttpServletRequest request, UserContainer container) {
    String appname = request.getParameter("applicationId");
    try {
      if (appname != null) appname = UTF8Support.unicodeFromUTF8(appname);
    } catch (UTFDataFormatException udfe) {
      log.info("parseApplicationId: WARNING appname is not valid UTF-8");
    }
    if (appname == null || container == null) {
      log.info("parseApplicationId: WARNING appname/container unexpectedly NULL");
      return;
    }

    int lIdx, rIdx;
    if ((lIdx = appname.indexOf("(")) >= 0 && (rIdx = appname.indexOf(")")) >= 0) {
      container.setApplicationId(appname.substring(0, lIdx));
      container.setLanguageId(appname.substring(lIdx+1, rIdx));
      log.info("parseApplicationId: applicationId/language is \"" + 
                container.getApplicationId() + "/" + container.getLanguageId() + "\"");
    } else {
      container.setApplicationId(appname);
    }
  }

  public ModelAndView vso(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("vso: CALLED");

    /** Get the specfied application from the request, store in the container */
    log.info("vso: invoking personalization for VOICE SIGN-ON");
    
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    container.setApplicationId("control");
    authenticate(container, "train");
    return new ModelAndView(new RedirectView(getPersonalizeView()));
  }

  public ModelAndView logoff(HttpServletRequest request, HttpServletResponse response) throws ServletException {
     // Send a "logoff" Query to the VXML browser
    QueryHelper query = new QueryHelper();
    query.put("query", "logoff");
    VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
    vt.setRetries(0);
    ResponseObject queryResponse = vt.execute(query);
    log.info("logoff query " + (String) (queryResponse.status() ? "SUCCEEDED" : "FAILED"));
     // Disconnect
    return disconnect(request, response);
  } 

  public ModelAndView disconnect(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    container.doLogoff(request);
    return new ModelAndView(new RedirectView(getMainView()));
  } 

  public String getMainView() {
    return mainView;
  }

  public void setMainView(String mainView) {
    this.mainView = mainView;
  }

  public String getApplicationsView() {
    return applicationsView;
  }

  public void setApplicationsView(String applicationsView) {
    this.applicationsView = applicationsView;
  }

  public String getPersonalizeView() {
    return personalizeView;
  }

  public void setPersonalizeView(String personalizeView) {
    this.personalizeView = personalizeView;
  }    

  public String getPersonalizeOptionsView() {
    return personalizeOptionsView;
  }

  public void setPersonalizeOptionsView(String personalizeOptionsView) {
    this.personalizeOptionsView = personalizeOptionsView;
  }    
}
