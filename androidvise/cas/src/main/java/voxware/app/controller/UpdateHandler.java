package voxware.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.net.InetAddress;
import java.util.Map;
import java.util.ResourceBundle;

import nl.justobjects.pushlet.core.Event;
import voxware.app.beans.ResourceBundleBean;
import voxware.app.beans.UserContainer;
import voxware.app.utils.StringLiteralSupport;
import voxware.cas.commands.NameValuePairsCommand;
import voxware.cas.framework.Command;
import voxware.cas.framework.CommandHandler;
import voxware.cas.framework.NameValuePair;
import voxware.cas.framework.exceptions.CommandException;

public class UpdateHandler implements CommandHandler, ApplicationContextAware {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String port = "";
  private String pushletSubject = null;
  private String pushletUri = null;
  private String method = null;
  private boolean sendUrl = false;
  private ApplicationContext applicationContext = null;

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public String getPort() {
    return port;
  }

  public void setPort(String port) {
    this.port = port;
  }

  public String getPushletSubject() {
    return pushletSubject;
  }

  public void setPushletSubject(String pushletSubject) {
    this.pushletSubject = pushletSubject;
  }

  public String getPushletUri() {
    return pushletUri;
  }

  public void setPushletUri(String pushletUri) {
    this.pushletUri = pushletUri;
  }

  public boolean getSendUrl() {
    return sendUrl;
  }

  public void setSendUrl(boolean sendUrl) {
    this.sendUrl = sendUrl;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public boolean isProperty(String value) {
    return value.startsWith("personalize.");
  }

  public void execute(Command command, Map model) throws CommandException {
  }

  public Object createEvent(Command command, Map model) throws CommandException {
    Event event = null;

    if (command instanceof NameValuePairsCommand) {
      NameValuePairsCommand nvpCommand = (NameValuePairsCommand) command;      
      NameValuePair[] nameValuePairs = nvpCommand.getNameValuePairs();

      // Acquire resource bundle
      ResourceBundleBean rbb = (ResourceBundleBean) applicationContext.getBean("baseBundle");
      ResourceBundle bundle = rbb.getBundle();

      if (bundle == null) {
        log.error("ERROR no ResourceBundle (bundle), config problem??");
        throw new CommandException();
      }
       
      // The model can be populated here if need be.  Ajax training implementation does not require this though.
      event = Event.createDataEvent(pushletSubject);
      if (nameValuePairs != null) {
        for (int i = 0; i < nameValuePairs.length; i++) {
          NameValuePair nvp = nameValuePairs[i];
	  // log.info("nvp[" + i + "]: name \"" + nvp.name() + "\" value \"" + nvp.value() + "\" isProperty " + isProperty(nvp.value()));

          String value = null;
          if (isProperty(nvp.value()))
            value = bundle.getString(nvp.value());
          else
            value = nvp.value();
          event.setField(nvp.name(), StringLiteralSupport.stringLiteralFromUnicode(value));
        }
      }

      if (sendUrl) {
         // IP address of local host needed for pushed URL
        InetAddress ia = null;
        try {
          ia = InetAddress.getLocalHost();
        } catch (Exception e) {
          log.error("Error resolving localhost", e);
        }

        String methodParameter;
        if (method == null) {
           // The name of the method for the pushlet to invoke is the uncapitalized command name
          char[] cmdChars = nvpCommand.getCommand().toCharArray();
          cmdChars[0] = Character.toLowerCase(cmdChars[0]);
          methodParameter = "?method=" + new String(cmdChars);
        } else {
           // The name of the method is a bean property
          methodParameter = "?method=" + method;
        }

        String url = new String("http://" + ia.getHostAddress() + ":" + getPort() + getPushletUri() + methodParameter);
        event.setField("url", url);

        // Add the jsessionid to the event attributes
        UserContainer uc = (UserContainer) applicationContext.getBean("container");
        if (uc != null) {
          event.setField("sessionid", uc.getSessionId());
        }
      }
    } else {
      log.error("UpdateHandler: ERROR command " + command.getClass().getName() + " not an instance of NameValuePairsCommand");
    }

    return event;
  }
}
