/**
 * Controller for the VLS Client "Advanced Personalization" Page
 */
 
package voxware.app.controller;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import voxware.app.beans.ResponseObject;
import voxware.app.beans.UserContainer;
import voxware.app.beans.VJMLTransaction;
import voxware.app.commands.AdvancedOptionsCommand;
import voxware.app.utils.QueryHelper;


/**
 * @author Craig A. Vanderborgh
 */
public class AdvancedOptionsFormController extends SimpleFormController {

  protected final Logger log = LoggerFactory.getLogger(getClass());

  private static String[] VOCABULARY = { "grab", "1", "3", "confirm", "check", "5", "sample", "noise", "switch", "to", "special", "menu" };
  private List vocabulary;
  private static String[] SCRIPT = { "grab 1 3 confirm", "check 5 confirm", "sample noise", "switch to special menu" };
  private List script;
  private String personalizeView = null;
  private String testView = null;

  public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException {

    log.info("onSubmit: CALLED");
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");

    AdvancedOptionsCommand aoc = (AdvancedOptionsCommand) command;
    log.info("onSubmit: aoc = " + aoc);

    int[] wordIndices = getSelectionIndices(request, "selectedWords[]");
    log.info("onSubmit: wordIndices " + wordIndices);
    ArrayList words = null;
    if (wordIndices != null) {
      words = new ArrayList(wordIndices.length);
      for (int i = 0; i < wordIndices.length; i++) {
        String word = (String) vocabulary.get(wordIndices[i]);
        words.add(word);
        log.info("  word " + i + " = \"" + word + "\"");
      }
    }
    container.setWords(words);

    int[] phraseIndices = getSelectionIndices(request, "selectedPhrases[]");
    log.info("onSubmit: phraseIndices " + phraseIndices);
    ArrayList phrases = null;
    if (phraseIndices != null) {
      phrases = new ArrayList(phraseIndices.length);
      for (int i = 0; i < phraseIndices.length; i++) {
        String phrase = (String) script.get(phraseIndices[i]);
        phrases.add(phrase);
        log.info("  phrase " + i + " = \"" + phrase + "\"");
      }
    }
    container.setPhrases(phrases);

    container.setEnroll(aoc.getEnroll().equalsIgnoreCase("true"));
    if (container.getEnroll()) log.info("onSubmit: enrolling all selected words");

    container.setRandomize(aoc.getRandomize().equalsIgnoreCase("true"));
    if (container.getRandomize()) log.info("onSubmit: randomizing the order of training prompts");

    container.setEnvironmentId(aoc.getEnvironmentId());
    log.info("onSubmit: environmentId " + aoc.getEnvironmentId());

    ModelAndView mav = null;
    String method = request.getParameter("method");
    if (method == null || method.equalsIgnoreCase("personalize")) {
      mav = new ModelAndView(new RedirectView(getPersonalizeView()));
    } else {
      mav = new ModelAndView(new RedirectView(getTestView()));
    }


    return mav;
  }

  protected Map referenceData(HttpServletRequest request) {
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    Map m = new HashMap();
    m.put("hostinfo", new HostInfo(request));
    m.put("phrases", SCRIPT);
    QueryHelper qh = new QueryHelper();
    qh.put("query", "vocabulary");
    if (container.getScriptId() != null)
      qh.put("script", container.getScriptId());
    VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
    vt.setRetries(0);
    ResponseObject response = null;
    try {
      response = (ResponseObject) vt.execute(qh);
      if (response.status()) {
        vocabulary = (List) response.getMap().get("words");
         // Translate JavaScript serialization of word names into Unicode for display
        ArrayList wordList = new ArrayList(vocabulary.size());
        Iterator words = vocabulary.iterator();
        while (words.hasNext()) wordList.add(StringEscapeUtils.unescapeJava((String) words.next()));
        m.put("words", wordList);
        List environments = (List) response.getMap().get("environments");
         // Translate JavaScript serialization of environment names into Unicode for display
        ArrayList environmentList = new ArrayList(environments.size());
        Iterator environmentit = environments.iterator();
        while (environmentit.hasNext()) environmentList.add(StringEscapeUtils.unescapeJava((String) environmentit.next()));
        m.put("environments", environmentList);
      } else {
        log.error("referenceData: ERROR " + response.getError() + " occured in VJMLTransaction");
      }
    } catch (Exception e) {
      log.error("referenceData: ERROR (no response) occured in VJMLTransaction", e);
    }
    return m;
  }

  protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
    log.info("initBinder: CALLED");
     // Take out the selected words & phrases from binding.  SpringLite CAN'T SEEM TO BIND COLLECTIONS, so we have to handle these lists ourselves
    String[] allowed = { "selectedWords", "selectedPhrases", "enroll", "randomize", "environmentId" };
    binder.setAllowedFields(allowed);
  }

  protected Object formBackingObject(HttpServletRequest request) throws ServletException {
    Object fbo = getApplicationContext().getBean("advancedOptionsCommand");
    log.info("formBackingObject: returning fbo " + fbo);
    return fbo;
  }

  private int[] getSelectionIndices(HttpServletRequest request, String fieldName) {
    Object value = request.getParameterValues(fieldName);
    if (value == null || !(value instanceof String[]))
      return null;

    String[] strArray = (String[]) value;

    int[] intArray = new int[strArray.length];
    for (int i = 0; i < strArray.length; i++) intArray[i] = new Integer(strArray[i]).intValue();
    return intArray;
  }

  public String getPersonalizeView() {
    return personalizeView;
  }

  public void setPersonalizeView(String personalizeView) {
    this.personalizeView = personalizeView;
  }

  public String getTestView() {
    return testView;
  }

  public void setTestView(String testView) {
    this.testView = testView;
  }

}
