package voxware.app.controller;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import voxware.client.Configuration;
import voxware.client.framework.exceptions.ConfigurationException;

public class HostInfo {
    private String hostname = "noname";
    private String address = "0.0.0.0";

    public HostInfo(HttpServletRequest request) {
        setAddress(request.getLocalAddr());
        try {
            Configuration configuration = new Configuration();
            String logicalName = (String) configuration.get("logicalName");
            if (logicalName != null) {
                setHostname(logicalName);
            }
        } catch (Exception e) {
            // do nothing?
        }
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
