/*
 * Controller for the Personalize Page
 */
 
package voxware.app.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import voxware.cas.framework.Command;
import voxware.cas.commands.NameValuePairsCommand;

import voxware.app.utils.*;
import voxware.app.beans.*;
import voxware.app.commands.AppLogonCommand;
import voxware.app.validation.LogonValidator;

/**
 * @author Craig A. Vanderborgh
 */
public class PersonalizeController extends MultiActionController {

  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String applicationView = null;
  private String beginPersonalizeView = null;
  private String personalizeView = null;
  private String pushletView = null;
  private String exitView = null;

  protected ModelAndView trainOrTest(HttpServletRequest request, String action) throws ServletException {

    UserContainer container = (UserContainer) getApplicationContext().getBean("container");

    // Initialize the asynchronous channel, used for (some) web ui button presses and training page updates
    container.initAsyncChannel();
    
    // Assemble and send the StartTrainingCommand
    StringBuffer command = new StringBuffer("Action&action=").append(action);
    command.append("&applicationId=").append(container.getApplicationId());
    command.append("&enroll=").append(container.getEnroll());
    command.append("&randomize=").append(container.getRandomize());
    command.append("&voiceprompted=").append(container.getVoicePrompted());
    command.append("&voicecontrolled=").append(container.getVoiceControlled());
    command.append("&voiceannotated=").append(container.getVoiceAnnotated());
    command.append("&collectingaudio=").append(container.getCollectingAudio());
    if (container.getScriptId() != null)
      command.append("&script=").append(container.getScriptId());
    if (container.getEnvironmentId() != null)
      command.append("&environmentId=").append(container.getEnvironmentId());
    List words = container.getWords();
    if (words != null) {
      command.append("&words=[");
      Iterator iterator = words.iterator();
      if (iterator.hasNext()) command.append("'").append((String) iterator.next()).append("'");
      while (iterator.hasNext()) command.append(",'").append((String) iterator.next()).append("'");
      command.append("]");
      container.setWords(null);
    }
     // Restore the defaults
    container.setEnroll(false);
    container.setRandomize(false);
    container.setVoicePrompted(false);
    container.setVoiceControlled(true);
    container.setVoiceAnnotated(true);
    container.setCollectingAudio(false);
     // Send the command
    log.info(action + ": sending command \"" + command.toString() + "\" to the browser");
    NameValuePairsCommand nvpCommand = new NameValuePairsCommand(command.toString());
    container.sendCommand(nvpCommand);
    log.info("personalize.trainOrTest: COMMAND SENT");
    addReferenceData(request, container);
    ModelAndView mav = new ModelAndView(getPersonalizeView(), container);
    mav.addObject("sessionid", container.getSessionId());

    return mav;
  }

  public ModelAndView personalize(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("personalize: CALLED");
    return trainOrTest(request, "train");
  }

  public ModelAndView test(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("test: CALLED");
    return trainOrTest(request, "test");
  }

  public ModelAndView update(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    log.info("update: CALLED");

    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    addReferenceData(request, container);
    return new ModelAndView(getPersonalizeView(), container);
    /*
    String  viewName = null;
    if ((viewName = request.getParameter("view")) != null) { 

      log.info("update: view name is \"" + viewName + "\"");
      return new ModelAndView(viewName, container);
    } else {
      return null;
    }
    */
  }

  private void addReferenceData(HttpServletRequest request, UserContainer container) {
    container.put("hostinfo", new HostInfo(request));
    String application = container.getApplicationId();
    if (application == null) {
      container.put("application", "Voice Sign-On");
    } else {
      container.put("application", container.getApplicationId());
    }

    container.put("userId", container.getUserId());
    container.put("environment", container.getEnvironmentId());
  }

  public ModelAndView disconnect(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    UserContainer container = (UserContainer) getApplicationContext().getBean("container");
    ModelAndView mav = null;
    container.shutdownAsyncChannel();
    if (container.getExitUrl() == null || container.getExitUrl().length() <= 1) {
      log.info("disconnect: exit to application view (\"" + getApplicationView() + "\")");
      mav = new ModelAndView(new RedirectView(getApplicationView()));
    } else {
       // Send a "logoff" Query to the VXML browser
      QueryHelper query = new QueryHelper();
      query.put("query", "logoff");
      VJMLTransaction vt = (VJMLTransaction) getApplicationContext().getBean("txn");
      vt.setRetries(0);
      ResponseObject queryResponse = vt.execute(query);
      log.info("logoff query " + (String) (queryResponse.status() ? "SUCCEEDED" : "FAILED"));
      mav = new ModelAndView(getExitView());
      mav.addObject("exitUrl", (Object)container.getExitUrl());
    }
      
    return mav;
  } 

  public String getApplicationView() {
    return applicationView;
  }

  public void setApplicationView(String applicationView) {
    this.applicationView = applicationView;
  }    

  public String getPushletView() {
    return pushletView;
  }

  public void setPushletView(String pushletView) {
    this.pushletView = pushletView;
  }    

  public String getPersonalizeView() {
    return personalizeView;
  }

  public void setPersonalizeView(String personalizeView) {
    this.personalizeView = personalizeView;
  }    

  public String getExitView() {
    return exitView;
  }

  public void setExitView(String exitView) {
    this.exitView = exitView;
  } 
}
