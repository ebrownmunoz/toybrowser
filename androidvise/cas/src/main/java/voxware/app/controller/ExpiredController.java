/*
 * Controller for the Client/Voxmanager "Main Menu"
 */
 
package voxware.app.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Locale;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Craig A. Vanderborgh
 */
public class ExpiredController implements Controller {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String expiredView = null;

  public ModelAndView handleRequest(HttpServletRequest request,
                                    HttpServletResponse response) 
    throws ServletException, IOException {

    log.info("handleRequest CALLED");
    HostInfo hi = new HostInfo(request);
    return new ModelAndView(getExpiredView(), "hostinfo", hi);
  }

  public String getExpiredView() {
    return expiredView;
  }

  public void setExpiredView(String expiredView) {
    this.expiredView = expiredView;
  }
}
