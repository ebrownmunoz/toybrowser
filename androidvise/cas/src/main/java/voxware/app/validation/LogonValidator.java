package voxware.app.validation;

import org.springframework.validation.Validator;
import org.springframework.validation.Errors;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import voxware.app.beans.ResponseObject;
import voxware.app.beans.UserContainer;
import voxware.app.beans.VJMLTransaction;

import voxware.app.utils.QueryHelper;

import voxware.app.commands.AppLogonCommand;

import voxware.util.StringLiteralSupport;

public class LogonValidator implements Validator, ApplicationContextAware {

  /** Logger for this class and subclasses */
  protected ApplicationContext applicationContext = null;
  protected final Logger log = LoggerFactory.getLogger(getClass());

  private int                        retries;
  public  int                     getRetries()             { return this.retries; }
  public  void                    setRetries(int retries)  { this.retries = retries; }

  private long       applicationRetryTimeout;
  public  long    getApplicationRetryTimeout()             { return this.applicationRetryTimeout; }
  public  void    setApplicationRetryTimeout(long timeout) { this.applicationRetryTimeout = timeout; }

  private long    authenticationRetryTimeout;
  public  long getAuthenticationRetryTimeout()             { return this.authenticationRetryTimeout; }
  public  void setAuthenticationRetryTimeout(long timeout) { this.authenticationRetryTimeout = timeout; }

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public boolean supports(Class cls) {
    log.info("supports: cls " + cls + " getName() " + cls.getName());
    return cls.getName().equals("voxware.app.commands.AppLogonCommand");
  }

  /**
   * We have to handle 2 different cases in this validator:
   * 1. Normal GUI logon - we take (userId,password) and send query "applications" to the VXML browser.
   *    If we receive back a list of applications - even an empty list - logon is allowed.
   * 2. Voxmanager authentication - when the quadruplet (userId,password,applicationId,action) is complete
   *    we treat this as a Voxmanager authentication.  In this case we send query "authentication" to the VXML
   *    browser.  The VXML browser authenticates the entire quadruplet and we jump to the page required by "action".
   */
  public void validate(Object obj, Errors errors) {
    AppLogonCommand alc = (AppLogonCommand) obj;
    log.info("validate: CALLED with userId = " + alc.getUserId() + "(" + StringLiteralSupport.stringLiteralFromUnicode(alc.getUserId())
                        + "); password = " + alc.getPassword() + "(" + StringLiteralSupport.stringLiteralFromUnicode(alc.getPassword()) + ")");
    if (alc == null) {
      errors.rejectValue("password", "error.not-specified", null, "Password required.");
    } else {
      log.info("Validating AppLogonCommand...");

      // First, deal with the case where the userId and/or applicationId is something like
      // "charlie(en-US)", where the userId and languageId are conjoined.  Separate them now.
      String userId = alc.getUserId();
      int lIdx, rIdx;
      if ((lIdx = userId.indexOf("(")) >= 0 && (rIdx = userId.indexOf(")")) >= 0) {
        alc.setUserId(userId.substring(0, lIdx));
        alc.setLanguageId(userId.substring(lIdx+1, rIdx));
        log.info("validate: userId/languageId is \"" + alc.getUserId() + "/" + alc.getLanguageId() + "\"");
      }

      if (alc.getPassword() == null || alc.getPassword().length() <= 1) {
        errors.rejectValue("userId", "error.bad-password", "The password you gave is not valid. Please try again.");
        return;
      } else {
        /** If "application" is unspecified, treat this as a normal CAS GUI logon */
        if (alc.getApplicationId() == null) {
          log.info("validate: gui logon attempt, using \"applications\" query..");
          QueryHelper qh = new QueryHelper();
          qh.put("query", "applications");
          qh.put("userId", alc.getUserId());
          qh.put("password", alc.getPassword());
          qh.put("languageId", alc.getLanguageId());

          VJMLTransaction vt = (VJMLTransaction) applicationContext.getBean("txn");
          vt.setRetries(retries, applicationRetryTimeout);
          ResponseObject response = null;
          try {
            response = vt.execute(qh);
          } catch (Exception e) {
	    log.error("Error executing VJML transaction", e);
            e.printStackTrace();
          }

          if (response == null) {
        	  errors.reject("vjml", "Error connecting to client unit");
          } else if (response.status()) {
            List apps = (List) response.getMap().get("applications");
            if (apps.size() > 0) {
              alc.setApplications(apps);
            }

            log.info("AppLogon \"authenticate\" validates w/o errors");
          } else {
            if (response.getError().equals("error.bad-password")) {
              errors.rejectValue("userId", "error.bad-password", "The password you gave is not valid for this user. Please try again.");
            } else {
              errors.rejectValue("userId", response.getError(), "A communications error occurred");
            }
          }
        } else {
          /** If "application" is non-null, "action" must also be provided */
          if (alc.getAction() == null || alc.getAction().length() <= 1) {
            errors.rejectValue("action", "error.bad-action", "INTERNAL ERROR invalid action.");
            return;
          }

          /** Quintuplet authentication via VJML */
          log.info("validate: vm auth attempt, using \"authentication\" query..");
          QueryHelper qh = new QueryHelper();
          qh.put("query", "authentication");
          qh.put("userId", alc.getUserId());
          qh.put("password", alc.getPassword());
          qh.put("applicationId", alc.getApplicationId());
          qh.put("languageId", alc.getLanguageId());
          qh.put("action", alc.getAction());

          VJMLTransaction vt = (VJMLTransaction) applicationContext.getBean("txn");
          vt.setRetries(retries, authenticationRetryTimeout);
          ResponseObject response = null;
          try {
	    response = vt.execute(qh);
          } catch (Exception e) {
            log.error("Error", e);
            response = new ResponseObject("error.vjml-timeout");
          }

          if (response.status()) {
            /* Expect to find the training status, must be one of either "poised" or "running" */
            Map m = response.getMap();
            String state;
            if (m == null || (state = (String) m.get("state")) == null) {
              errors.rejectValue("state", "error.bad-status", "INTERNAL ERROR no status in auth response");
              return;
            }
            if (!state.equals("poised") && !state.equals("running")) {
              errors.rejectValue("state", "error.bad-status", "INTERNAL ERROR unknown state \"" + state + "\"");
              return;
            }
            alc.setState(state);
            log.info("AppLogon \"authenticate\" validates w/o errors, state is \"" + state + "\"");
             // Put the List of scripts into the container
            List scripts;
            UserContainer container = (UserContainer) applicationContext.getBean("container");
            scripts = (List) m.get("scripts");
            if (scripts != null) {
              log.info("authenticate: there are " + scripts.size() + " scripts");
              container.setScripts(scripts);
            } else {
              log.info("authenticate: THERE ARE NO SCRIPTS");
            }
            log.info("authenticate: No script list in ro");
             // Put the environmentId into the container
            container.setEnvironmentId((String) m.get("environmentId"));
          } else {
            if (response.getError().equals("error.bad-password")) {
              errors.rejectValue("userId", "error.bad-password", "The password you gave is not valid for this user. Please try again.");
            } else if (response.getError().equals("error.bad-application")) {
              errors.rejectValue("application", "error.bad-application", "The application specified is not valid for this user. Please try again.");
            } else if (response.getError().equals("error.bad-action")) {
              errors.rejectValue("action", "error.bad-action", "The specified action (\"" + alc.getAction() + "\") is invalid");
            } else if (response.getError().equals("error.bad-language")) {
              errors.rejectValue("languageId", "error.bad-language", "The specified language (\"" + alc.getLanguageId() + "\") is invalid");
            } else {
              errors.rejectValue("userId", response.getError(), "A communications error occurred");
            }
          }
        }
      }
    }
    log.info("validate: errors.getErrorCount() " + errors.getErrorCount());
  }
}
