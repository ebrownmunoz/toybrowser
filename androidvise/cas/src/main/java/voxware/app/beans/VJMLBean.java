package voxware.app.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.IOException;

import voxware.vjml.VJML;
import voxware.vjml.VJMLAttributes;
import voxware.vjml.VJMLChannel;
import voxware.vjml.VJMLNameConflictException;

public class VJMLBean implements InitializingBean {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String destAddr = "127.0.0.1";
  private String asyncChannelName = null;
  private VJMLChannel asyncChannel = null;
  private String syncChannelName = null;
  private VJMLChannel syncChannel = null;
  private VJML vjml = null;

  public void afterPropertiesSet() throws Exception {
    VJMLAttributes attributes = new VJMLAttributes();
    attributes.classLoader(this.getClass().getClassLoader());
    try {
      vjml = new VJML(attributes);
    } catch(IOException ioe) {
      log.error("Error starting VJML", ioe);
      throw ioe;
    }
  }

  public String getDestAddr() {
    return destAddr;
  }

  public void setDestAddr(String destAddr) {
    this.destAddr = destAddr;
  }

  public String getAsyncChannelName() {
    return syncChannelName;
  }

  public void setAsyncChannelName(String asyncChannelName) {
    this.asyncChannelName = null;
  }

  public String getSyncChannelName() {
    return syncChannelName;
  }

  public void setSyncChannelName(String syncChannelName) {
    this.syncChannelName = syncChannelName;
  }

  public VJMLChannel getAsyncChannel() throws VJMLNameConflictException {
    return getSyncChannel();
    /*
    if (asyncChannel == null) {
      asyncChannel = vjml.channel(asyncChannelName, destAddr);
    }
    return asyncChannel;
    */
  }

  public void shutdownAsyncChannel() {
    shutdownSyncChannel();
    /*
    if (asyncChannel != null) {
      asyncChannel.destroy();
      asyncChannel = null;
      } */
  }

  public VJMLChannel getSyncChannel() throws VJMLNameConflictException {
    if (syncChannel == null) {
      syncChannel = vjml.channel(syncChannelName, destAddr);
    }
    return syncChannel;
  }

  public void shutdownSyncChannel() {
    if (syncChannel != null) {
      syncChannel.destroy();
      syncChannel = null;
    }
  }

}
