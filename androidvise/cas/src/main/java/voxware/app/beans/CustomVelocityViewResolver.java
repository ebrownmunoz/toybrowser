package voxware.app.beans;

import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.velocity.VelocityView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

public class CustomVelocityViewResolver extends VelocityViewResolver {
  private String resourceBundleAttribute = null;
  private ResourceBundleBean resourceBundleBean = null;

  public void setResourceBundleAttribute(String resourceBundleAttribute) {
    this.resourceBundleAttribute = resourceBundleAttribute;
  }

  public String getResourceBundleAttribute() {
    return resourceBundleAttribute;
  }

  public void setResourceBundleBean(ResourceBundleBean resourceBundleBean) {
    this.resourceBundleBean = resourceBundleBean;
  }

  public ResourceBundleBean getResourceBundleBean() {
    return resourceBundleBean;
  }

  protected View loadView(String viewName, Locale locale) {
    CustomVelocityView view = (CustomVelocityView) super.loadView(viewName, locale);
    
    view.setResourceBundleAttribute(this.resourceBundleAttribute);
    view.setResourceBundleBean(this.resourceBundleBean);
    return view;
  }
}
