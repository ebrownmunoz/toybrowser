package voxware.app.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.justobjects.pushlet.core.Event;
import voxware.cas.commands.NameValuePairsCommand;
import voxware.cas.framework.CommandHandler;
import voxware.cas.framework.CommandNameHandlerMapping;
import voxware.vjml.VJMLChannel;
import voxware.vjml.VJMLMessage;


public class Nexus extends PushletContext implements Runnable {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private final int MAXTHREADSLEEPS = 80;
  private int threadSleepCnt = 0;
  private int threadSleepMillis = 250;
  private Thread thread = null;
  private VJMLChannel channel;
  private CommandNameHandlerMapping handlerMapping = null;
  private boolean running = false;

  public Nexus() {
    super(null);
    thread = new Thread(this, "Nexus-Runner");
    running = false;
    thread.start();
  }

  // PushletContext methods
  public void report(Throwable exception) {}
  public void event(Event event) {
    log.info("event: event " + event.toString());
    String cmdName = event.getField("cmd");
    if (cmdName == null) {
      log.error("Nexus.event: ERROR no cmd for event " + event.toString());
      return;
    }

    StringBuffer command = new StringBuffer(cmdName);

    /*!! Event has a lot of stuff in attribute map AppServerAccess doesn't like
    for (Iterator it = event.getFieldNames(); it.hasNext();) {
      String name = (String) it.next();
      if (name.equals("cmd")) {
        continue;
      } else {
        command.append("&").append(name).append("=").append(event.getField(name));
      }
      } */

    if (channel != null) {
      NameValuePairsCommand nvpCommand = new NameValuePairsCommand(command.toString());
      VJMLMessage message = new VJMLMessage(nvpCommand, VJMLMessage.HighestPriority);
      channel.putMessage(message);
      channel.sendNow();
      log.info("event: SENT message \"" + command.toString() + "\" on channel " + channel.name());
    } else {
      log.warn("event: WARNING - channel is null");
    }
  }

  public void heartbeat() {
  }

  public void start() {
    log.info("start: CALLED");
    channel.flush();
    running = true;
    synchronized (this) {
      notify();
    }
  }

  public void stop() {
    log.info("stop: CALLED");
    running = false;
  }

  // Runnable methods
  public void run() {
    log.info("run: STARTED");
    while (true) {
      if (running) {
        VJMLMessage vjmlMsg = channel.getMessageNow();
        if (vjmlMsg == null) {
          try {
            if (threadSleepCnt >= MAXTHREADSLEEPS) {
              threadSleepCnt = 0;

            } else {
              threadSleepCnt++;
              Thread.sleep(threadSleepMillis);
            }
          } catch (Exception e) {
          }
        } else {
          threadSleepCnt = 0;
          Object o = (Object) vjmlMsg.content();
          if (o instanceof NameValuePairsCommand) {
            NameValuePairsCommand nvpCommand = (NameValuePairsCommand) o;
            log.info("run: RECEIVED \"" + nvpCommand.toString() + "\" on channel " + channel.name());
        
            CommandHandler handler = handlerMapping.getHandler(nvpCommand.getCommand());
            if (handler == null) {
  	    // log.info("Nexus.run DEBUG handler for command \"" + nvpCommand.getCommand() + "\" is null");
            } else {
              try {
                Event event = null;
                event = (Event) handler.createEvent(nvpCommand, null);

                // Send event to pushlet (goes to HTML browser..)
                sendEvent(event);

              } catch (Exception e) {
                log.error("Error processing event", e);
              }
            }
            log.info("run: finished processing command \"" + nvpCommand.getCommand() + "\"");
          } else {
            log.warn("run: ERROR: unrecognized command type " + o.getClass().getName());
          }
        }
      } else {
        log.info("run: running " + running + ", going dormant..");
        synchronized (this) {
          while (!running) {
            try {
              wait();
            } catch (Exception e) {}
          }
        }
        log.info("run: AWAKENED running " + running);
        threadSleepCnt = 0;
      }
    }
  }

  // JavaBean Methods
  public VJMLChannel getChannel() {
    return channel;
  }

  public void setChannel(VJMLChannel channel) {
    this.channel = channel;
  }

  public int getThreadSleepMillis() {
    return threadSleepMillis;
  }

  public void setThreadSleepMillis(int threadSleepMillis) {
    this.threadSleepMillis = threadSleepMillis;
  }

  public void setHandlerMapping(CommandNameHandlerMapping handlerMapping) {
    this.handlerMapping = handlerMapping;
  }

  public CommandNameHandlerMapping getHandlerMapping() {
    return handlerMapping;
  }
}
