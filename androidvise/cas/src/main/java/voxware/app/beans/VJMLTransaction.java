package voxware.app.beans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;

import voxware.app.utils.StringList;
import voxware.app.utils.QueryHelper;

import voxware.vjml.*;
import voxware.cas.framework.*;
import voxware.cas.framework.exceptions.*;
import voxware.cas.commands.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VJMLTransaction implements ApplicationContextAware {

  protected final Logger log = LoggerFactory.getLogger(getClass());

  private ApplicationContext applicationContext = null;
  private long dfltTimeout;

  private int          retries;
  public  int       getRetries()             { return this.retries; }
  public  void      setRetries(int retries)  { this.retries = retries; }

  private long    retryTimeout;
  public  long getRetryTimeout()             { return this.retryTimeout; }
  public  void setRetryTimeout(long timeout) { this.retryTimeout = timeout; }
  
  public VJMLTransaction() {
  }

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public ResponseObject execute(QueryHelper qh) throws ServletException {
    return execute(qh.getArray(), qh.getObjName());
  }

  public ResponseObject execute(NameValuePair[] nvps, String objName) throws ServletException {

    log.info("execute: CALLED, application context " + applicationContext);
    ResponseObject response = new ResponseObject();
    VJMLBean vb = (VJMLBean) applicationContext.getBean("vjml");

    /** Find the query NVP, if any */
    String query = null;
    for (int i = 0; i < nvps.length; i++) {
      NameValuePair nvp = nvps[i];
      if (nvp.name().equals("query")) {
        query = nvp.value();
        break;
      }
    }
    boolean matches = (query == null);

    /** Set up the command and the channel */
    NameValuePairsCommand sendCmd = new NameValuePairsCommand("Query");
    sendCmd.setNameValuePairs(nvps);

    VJMLChannel channel = null;
    try {
      channel = vb.getSyncChannel();
      log.info("VJMLTransaction.execute: channel is " + channel);
    } catch (VJMLNameConflictException vnce) {
      log.error("execute: FATAL ERROR - VJMLNameConflictException (this should not ever happen)");
    }

     // Retry the specified number of times
    int countdown = retries;
    do {

      /** Send out the command on the channel */
      VJMLMessage send = new VJMLMessage(sendCmd, VJMLMessage.HighestPriority);
      channel.putMessage(send);
      channel.sendNow();
      log.info("VJMLTransaction.execute: SENT message \"" + sendCmd.toString() + "\" on channel " + channel.name());

      /** Await the appropriate response - this is a synchronous exchange */
      do {
        String queryResponse = null;
        VJMLMessage recv = null;

        try {
          recv = channel.getMessage((countdown > 0) ? retryTimeout : dfltTimeout);
          if (recv == null) {
            if (queryResponse != null) throw new ServletException("VJML timeout (nomatch): sent query " + query + " response " + queryResponse);
            else if (countdown > 0)    break;
            else                       throw new ServletException("VJML timeout (noreply): sent query " + query + ", no response");
          }
        } catch (InterruptedException ie) {
          log.warn("Caught InterruptedException", ie);
        }

        Object content = (Object) recv.content();
        if (content == null) {
          if (countdown > 0) break;
           // Return VJML timeout error
          response.setError("error.vjml-timeout");
          return response;
        }

        log.info("VJMLTransaction.execute: RECEIVED message \"" + content.toString() + "\" on channel " + channel.name());

        if (content instanceof NameValuePairsCommand) {
          NameValuePairsCommand nvpCommand = (NameValuePairsCommand) content;

          /** Expect a "QueryResponse" command */
          String rspCommand = nvpCommand.getCommand();
          if (rspCommand.equals("QueryResponse")) {

            /** Error Checking: Loop thru name/value pairs, watching for "response" and "error" */
            Map m = null;
            nvps = nvpCommand.getNameValuePairs();

            for (int i = 0; i < nvps.length; i++) {
              NameValuePair nvp = nvps[i];
              log.info("VJMLTransaction.execute: RESPONSE LOOP i " + i + " name " + nvp.name() + " value \"" + nvp.value() + "\"");
              if (nvp == null) {
                response.setError("error.null-response");
                log.error("execute: ERROR - null response received");
              } else if (nvp.name().equals("query")) {
                queryResponse = nvp.value();
                if (query != null && query.equals(queryResponse)) {
                  matches = true;
                } else if (query == null || !query.equals(queryResponse)) {
                  log.error("execute: ERROR - queryResponse \"" + queryResponse + "\" != query \"" + query + "\"");
                  matches = false;
                  break;
                }
              } else if (nvp.name().equals("error")) {
                log.error("execute: ERROR - \"" + nvp.value() + "\" in response");
                matches = true;
                response.setError(nvp.value());
              } else if (nvp.value().startsWith("['") || nvp.value().startsWith("[\"") || nvp.value().startsWith("[]")) {
                  QueryHelper qh = new QueryHelper(nvp.value());
                  qh.setObjName(nvp.name());
                  qh.parse();

                  /** Expect the "objName", if specified, given as constructor argument */
                  if (objName != null && !qh.getObjName().equals(objName)) {
                    log.info("execute: ERROR objName is " + qh.getObjName() + " expected " + objName);
                    response.setError("error.invalid-objname");
                  } else {
                    if (m == null) m = new HashMap();
                    m.put(nvp.name(), qh.getStringList());
                  }
              } else if (nvp.name().equals("languageId")) {
                ResourceBundleBean rbb = (ResourceBundleBean) applicationContext.getBean("baseBundle");
                rbb.setLanguageId(nvp.value());
              } else {
                /** Add the name-value pair to the map */
                if (m == null) m = new HashMap();
                m.put(nvp.name(), nvp.value());
              }
            }
            response.setMap(m);

          } else if (rspCommand.equals("Resend")) {

            /** Resend the command on the channel */
            channel.putMessage(send);
            channel.sendNow();
            log.info("VJMLTransaction.execute: RESENT message \"" + sendCmd.toString() + "\" on channel " + channel.name());

          } else {
            log.error("execute: ERROR received unexpected command \"" + nvpCommand.getCommand() + "\"");
            response.setError("error.unexpected-cmd");
            return response;
          }


        } else {
          response.setError("error.unexpected-cmd");
        }

      } while (!matches);

    } while (countdown-- > 0 && !matches);

    log.info("execute: return response " + response);
    return response;
  }
  
  public long getDfltTimeout() {
    return dfltTimeout;
  }

  public void setDfltTimeout(long dfltTimeout) {
    this.dfltTimeout = dfltTimeout;
  }

  public void setRetries(int retries, long timeout) {
    this.retries = retries;
    this.retryTimeout = timeout;
  }
}
