package voxware.app.beans;

import nl.justobjects.pushlet.core.PublishListener;
import nl.justobjects.pushlet.core.Event;
import nl.justobjects.pushlet.core.Dispatcher;

public abstract class PushletContext implements PublishListener {
  private String subject;

  public PushletContext(String subject) {
    this.subject = subject;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Event createEvent() {
    return Event.createDataEvent(subject);
  }

  public void sendEvent(Event event) {
    Dispatcher.getInstance().multicast(event);
  }

  public void register() {
    Dispatcher.getInstance().addListener(subject, this);
  }

  public void unregister() {
    Dispatcher.getInstance().removeListener(subject);
  }

  // PublishListener methods (callbacks)
  public void report(Throwable exception) {}
  public void event(Event event) {}
  public void heartbeat() {}

}