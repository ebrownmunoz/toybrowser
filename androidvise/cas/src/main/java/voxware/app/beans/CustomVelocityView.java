/*
 * Copyright 2002-2004 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

package voxware.app.beans;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.context.Context;
import org.apache.velocity.VelocityContext;
import org.springframework.web.servlet.view.velocity.VelocityView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 */
public class CustomVelocityView extends VelocityView {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private String resourceBundleAttribute = null;
  private ResourceBundleBean resourceBundleBean = null;

  public void setResourceBundleAttribute(String resourceBundleAttribute) {
    this.resourceBundleAttribute = resourceBundleAttribute;
  }

  public String getResourceBundleAttribute() {
    return resourceBundleAttribute;
  }

  public void setResourceBundleBean(ResourceBundleBean resourceBundleBean) {
    this.resourceBundleBean = resourceBundleBean;
  }

  public ResourceBundleBean getResourceBundleBean() {
    return resourceBundleBean;
  }

	protected void exposeHelpers(Context velocityContext, HttpServletRequest request) throws Exception {
    if (this.resourceBundleBean != null && this.resourceBundleAttribute != null) {
      velocityContext.put(resourceBundleAttribute, this.resourceBundleBean);
    }
  }
}
