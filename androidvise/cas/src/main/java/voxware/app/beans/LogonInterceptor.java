package voxware.app.beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Craig A. Vanderborgh
 */
public class LogonInterceptor extends HandlerInterceptorAdapter implements ApplicationContextAware {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private ApplicationContext applicationContext = null;
  private String expiredView = null;

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

 	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
 			throws Exception {
  		UserContainer container = (UserContainer) applicationContext.getBean("container");
    boolean loggedOn = container.checkLogon(request);
    log.info("INTERCEPTOR: checkLogon returns " + container.checkLogon(request));

    if (loggedOn) {
      // The following forces a RESET of the CAS session timer
      HttpSession session = request.getSession();
      if (session != null) {
        session.getServletContext();
      }
      return true;
    } else {
      throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView(getExpiredView())));
    }
  }

  public String getExpiredView() {
    return expiredView;
  }

  public void setExpiredView(String expiredView) {
    this.expiredView = expiredView;
  }
}
