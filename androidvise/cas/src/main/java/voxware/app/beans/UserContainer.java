package voxware.app.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import voxware.cas.framework.Command;
import voxware.cas.framework.CommandNameHandlerMapping;
import voxware.vjml.VJMLChannel;
import voxware.vjml.VJMLMessage;
import voxware.vjml.VJMLNameConflictException;

public class UserContainer extends HashMap implements ApplicationContextAware, HttpSessionBindingListener {

  protected final Logger log = LoggerFactory.getLogger(getClass());
  private ApplicationContext applicationContext = null;
  private String userId = null;
  private String password = null;
  private String applicationId = null;
  private String languageId = null;
  private String scriptId = null;
  private String environmentId = null;
  private String action = null;
  private String exitUrl = null;
  private List applications = null;
  private List scripts = null;
  private String state = null;
  private List words = null;
  private List phrases = null;
  private List environments = null;
  private boolean enroll = false;
  private boolean randomize = false;
  private boolean voicePrompted = false;
  private boolean voiceControlled = true;
  private boolean voiceAnnotated = true;
  private boolean collectingAudio = false;
  private HttpSession session = null;
  private String sessionId = null;
  private VJMLChannel asyncChannel = null;
  private Nexus nexus = null;
  private int maxInactiveInterval = -1;
  private boolean asyncInitialized = false;
  private CommandNameHandlerMapping handlerMapping = null;

  public void setHandlerMapping(CommandNameHandlerMapping handlerMapping) {
    this.handlerMapping = handlerMapping;
  }

  public CommandNameHandlerMapping getHandlerMapping() {
    return handlerMapping;
  }

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public boolean doLogon(HttpServletRequest request) throws ServletException {
    HttpSession session = request.getSession();
    String sessionId = session.getId();
    if (this.sessionId != null && !sessionId.equals(this.sessionId)) {
      /** Can't logon, another session w/different user exists */
      return false;
    }
    this.session = session;
    this.sessionId = sessionId;

    /* Set our own SPRING-CONFIGURED default (i.e. non-Ajax) session timeout */
    if (maxInactiveInterval > 0) {
      session.setMaxInactiveInterval(maxInactiveInterval);
    }

    /** 
     * Change to async channel management - init at personalize launch, 
     * shutdown at disconnect
     */

    /** Set "this" as a session attribute so we get session lifecycle callbacks 
     *   Yes, the Servlet API really does suck, doesn't it?? */
    session.setAttribute("container", this);
    return true;
  }

  public void doLogoff(HttpServletRequest request) {
    doLogoff(request.getSession());
  }

  public void doLogoff(HttpSession session) {
    if (session == null || this.sessionId == null) {
      log.info("doLogoff: Already logged off");
      return;
    }

    String sessionId = session.getId();
    if (sessionId.equals(this.sessionId)) {
      this.sessionId = null;
    }
    clear();

    //!! CAV We want to close out the async channel on timeout, right??
    shutdownAsyncChannel();
    userId = null;
    password = null;
    applicationId = null;
    applications = null;
    scriptId = null;
    scripts = null;
    environmentId = null;
    environments = null;
    languageId = null;
    state = null;
    session.invalidate();
  }

  public boolean checkLogon(HttpServletRequest request) {
    log.info("checkLogon: called - request = " + request.toString());
    if (this.sessionId == null)
      return false;

    /*!! This is disabled due to CE CAS bugs */
    return true;

    /** If the request sessionId matches the one in the container we are logged on */
    /*
    String sessionId = request.getSession().getId();
    log.info("checkLogon: sessionId = " + sessionId + "; this.sessionId = " + this.sessionId);
    if (sessionId.equals(this.sessionId)) {
      return true;
    }
    return false; */
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getLanguageId() {
    return languageId;
  }

  public void setLanguageId(String languageId) {
    this.languageId = languageId;
  }

  public String getScriptId() {
    return scriptId;
  }

  public void setScriptId(String scriptId) {
    this.scriptId = scriptId;
  }


  public String getEnvironmentId() {
    log.info("Getting environmentId (\"" + environmentId + "\")"); // DEBUG
    return environmentId;
  }

  public void setEnvironmentId(String environmentId) {
    log.info("Setting environmentId to \"" + environmentId + "\""); // DEBUG
    this.environmentId = environmentId;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public List getApplications() {
    return applications;
  }

  public void setApplications(List applications) {
    this.applications = applications;
  }

  public List getScripts() {
    return scripts;
  }

  public void setScripts(List scripts) {
    this.scripts = scripts;
  }

  public List getEnvironments() {
    return environments;
  }

  public void setEnvironments(List environments) {
    this.environments = environments;
  }

  public String getExitUrl() {
    return exitUrl;
  }

  public void setExitUrl(String exitUrl) {
    this.exitUrl = exitUrl;
  }

  public List getWords() {
    return words;
  }

  public void setWords(List words) {
    this.words = words;
  }

  public List getPhrases() {
    return phrases;
  }

  public void setPhrases(List phrases) {
    this.phrases = phrases;
  }

  public boolean getEnroll() {
    return enroll;
  }

  public void setEnroll(boolean enroll) {
    this.enroll = enroll;
  }

  public boolean getRandomize() {
    return randomize;
  }

  public void setRandomize(boolean randomize) {
    this.randomize = randomize;
  }

  public boolean getVoicePrompted() {
    return voicePrompted;
  }

  public void setVoicePrompted(boolean voicePrompted) {
    this.voicePrompted = voicePrompted;
  }

  public boolean getVoiceControlled() {
    return voiceControlled;
  }

  public void setVoiceControlled(boolean voiceControlled) {
    this.voiceControlled = voiceControlled;
  }

  public boolean getVoiceAnnotated() {
    return voiceAnnotated;
  }

  public void setVoiceAnnotated(boolean voiceAnnotated) {
    this.voiceAnnotated = voiceAnnotated;
  }

  public boolean getCollectingAudio() {
    return collectingAudio;
  }

  public void setCollectingAudio(boolean collectingAudio) {
    this.collectingAudio = collectingAudio;
  }

  public int getMaxInactiveInterval() {
    return maxInactiveInterval;
  }

  public void setMaxInactiveInterval(int maxInactiveInterval) {
    this.maxInactiveInterval = maxInactiveInterval;
  }

  public void initAsyncChannel() throws ServletException {
    log.info("initAsyncChannel: CALLED");
    VJMLBean vb = (VJMLBean) applicationContext.getBean("vjml");

    try {
      asyncChannel = vb.getAsyncChannel();
    } catch(VJMLNameConflictException vnce) {
      log.error("VJML name conflict", vnce);
      throw new ServletException("VJMLNameConflictException in initialize(), channel NOT set up");
    }

    // Test it!
    if (applicationContext == null)
      log.error("initAsyncChannel ERROR: application context is null!! (config problem?)");


    nexus = (Nexus) applicationContext.getBean("nexus");
    nexus.setChannel(asyncChannel);

    // Add the listeners
    nexus.register();
    nexus.start();
    asyncInitialized = true;

    // Clear HTTPSession timeout when possible
    if (session != null) {
      session.setMaxInactiveInterval(-1);
    }
    log.debug("initAsyncChannel: SETUP COMPLETE, destAddr " + vb.getDestAddr());
  }

  public void shutdownAsyncChannel() {
    if (asyncInitialized) {
      log.info("shutdownAsyncChannel DEBUG - terminating initialized async channel");
      if (asyncChannel != null) {
        nexus.unregister();
        nexus.stop();
      }
      // vb.shutdownAsyncChannel();
      nexus = null;
      asyncChannel = null;
      asyncInitialized = false;

      /* Restore non-Ajax session timeout */
      if (session != null && maxInactiveInterval > 0)
        session.setMaxInactiveInterval(maxInactiveInterval);
    }
  }

  // Send a command asynchronously (on asyncChannel)
  public void sendCommand(Command command) {
    VJMLMessage message = new VJMLMessage(command, VJMLMessage.HighestPriority);
    asyncChannel.putMessage(message);
    asyncChannel.sendNow();
    log.info("UserContainer.sendCommand: SENT message \"" + command.toString() + "\" on channel " + asyncChannel.name());
  }

  // VJMLListener Implementation

  /*
  // Report an exception
  public void exception(Throwable exception) {
    log.warn("UserContainer.exception: " + exception.toString());
  }

  // Report an incoming message
  public void message(VJMLChannel channel) {
    Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
    log.debug("UserContainer.message: message arrived on channel \"" + channel.name() + "\"");

    VJMLMessage message = null;
    try {
      // Remove and discard any messages on the channel but the LAST ONE
      while (channel.messageCount() > 1) {
        channel.getMessage();
      }
      message = channel.getMessage();
    } catch (InterruptedException ie) {
      ie.printStackTrace();
    }

    Object o = (Object) message.content();
    log.debug("UserContainer.message: DEBUG command is of type " + o.getClass().getName());
    if (o instanceof NameValuePairsCommand) {
      NameValuePairsCommand nvpCommand = (NameValuePairsCommand) o;
      log.info("UserContainer.message: RECEIVED message \"" + nvpCommand.toString() + "\" on channel " + channel.name());
        
      CommandHandler handler = handlerMapping.getHandler(nvpCommand.getCommand());
      if (handler == null) {
        log.debug("UserContainer.message DEBUG handler is null");
      } else {
        try {
          log.debug("UserContainer.message DEBUG executing handler..");
          handler.execute(nvpCommand, this);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      log.debug("CAS COMMAND HANDLED");
    } else {
      log.warn("UserContainer.message ERROR: unrecognized command type " + o.getClass().getName());
    }
    } */

  // HttpSessionBindingListener Implementation

  // The container calls this method when it is being unbound from the session
  public void valueUnbound(HttpSessionBindingEvent event) {
     // Perform resource cleanup
    log.info("UserContainer.valueUnbound: Unbinding ...");
    doLogoff(event.getSession());
  }

  // The container calls this method when it is being bound to the session
  public void valueBound(HttpSessionBindingEvent event) {
     // Do nothing
    log.info("UserContainer.valueBound: Binding ...");
  }
}
