package voxware.app.beans;

import java.io.InputStream;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceBundleBean implements ApplicationContextAware {
  protected final Logger log = LoggerFactory.getLogger(getClass());
  private ApplicationContext applicationContext = null;
  private String defaultResourceName = null;
  private String defaultLanguageId = null;
  private String languageId = null;
  private ResourceBundle bundle = null;

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

   // Get a language resource bundle
  public ResourceBundle getBundle(String languageId) { 
    return getBundle(null, languageId, null);
  }

  public ResourceBundle getBundle(String resourceName, String languageId) {
    return getBundle(resourceName, languageId, null);
  }

  public ResourceBundle getBundle(String resourceName, String languageId, String variant) {
    Locale locale = null;
    if (resourceName == null || resourceName.equals("null") || resourceName.equals("undefined")) resourceName = defaultResourceName;
    if (languageId == null || languageId.equals("null") || languageId.equals("undefined")) languageId = defaultLanguageId;
    if (variant == null || variant.equals("null") || variant.equals("undefined")) variant = null;

    if (languageId != null) {
      this.languageId = languageId;
      StringTokenizer tokenizer = new StringTokenizer(languageId, "-_");
      String language = (tokenizer.hasMoreTokens()) ? tokenizer.nextToken().toLowerCase() : null;
      String country = (tokenizer.hasMoreTokens()) ? tokenizer.nextToken().toUpperCase() : null;
      if (variant == null && tokenizer.hasMoreTokens()) variant = tokenizer.nextToken();  // Nonempty argument overrides token
      if (language != null) {
        if (country != null) {
          if (variant != null) locale = new Locale(language, country, variant);
          else                 locale = new Locale(language, country);
        } else {
          locale = new Locale(language);
        }
      }
    }
    if (locale == null) {
      locale = Locale.getDefault();
      this.languageId = locale.getLanguage() + "-" + locale.getCountry();
    }
    try {
      log.info("getBundle(): getting resource \"" + resourceName + "\" and locale " + locale);
      return ResourceBundle.getBundle(resourceName, locale, Thread.currentThread().getContextClassLoader()); // Throws MissingResourceException
    } catch (Exception e) {
      log.error("getBundle(): getting resource \"" + resourceName + "\" and locale " + locale + ": " + e);
      return ResourceBundle.getBundle(resourceName, Locale.getDefault(), Thread.currentThread().getContextClassLoader()); // Throws MissingResourceException
    }
  }

  // Concise getString() accessor method for use in VelocityContext
  public String msg(String key) {

    if (key == null) return null;

    String languageId = ((UserContainer) applicationContext.getBean("container")).getLanguageId();
    // log.info("msg: CALLED with key = " + key + "; bundle = " + bundle + "; languageId = " + languageId); // DEBUG
    setLanguageId(languageId);
    if (bundle == null) {
      getBundle();
      if (bundle == null)
        return null;
    }

    String retval = null;
    try {
      retval = bundle.getString(key);
      // log.info("msg: RETURNED " + retval + " for key = " + key + "; bundle = " + bundle + "; languageId = " + languageId); // DEBUG
    } catch (MissingResourceException mre) {
      log.error("msg: CAN'T FIND VALUE FOR KEY \"" + key + "\"");
    }
    return retval;
  }

  public ResourceBundle getBundle() {
    UserContainer container = (UserContainer) applicationContext.getBean("container");
    String languageId = ((UserContainer) applicationContext.getBean("container")).getLanguageId();
    if (languageId == null) {
      languageId = (defaultLanguageId == null) ? "en-US" : defaultLanguageId;
      log.info("getBundle: DEFAULTING to languageId \"" + languageId + "\"");
    }
    bundle = getBundle(languageId);
    return bundle;
  }

  public void setBundle(ResourceBundle bundle) {
    this.bundle = bundle;
  }

  public String getDefaultResourceName() {
    return defaultResourceName;
  }

  public void setDefaultResourceName(String defaultResourceName) {
    this.defaultResourceName = defaultResourceName;
  }

  public String getDefaultLanguageId() {
    return defaultLanguageId;
  }

  public void setDefaultLanguageId(String defaultLanguageId) {
    this.defaultLanguageId = defaultLanguageId;
  }

  public String getLanguageId() {
    return languageId;
  }

  public void setLanguageId(String languageId) {
     // Clear the cached bundle if new languageId is different
    if ((languageId == null && this.languageId != null) || (languageId != null && !languageId.equals(this.languageId))) {
      log.info("setLanguageId: CLEARING BUNDLE (current languageId = " + this.languageId + "; new languageId = " + languageId + ")");
      bundle = null;
    }
    this.languageId = languageId;
  }
}
