package voxware.app.beans.propertyeditors;

import voxware.util.UTF8Support;

import com.googlecode.openbeans.PropertyEditorSupport;
import java.io.UTFDataFormatException;

public class UTF8StringPropertyEditor extends PropertyEditorSupport {

  public void setAsText(String utf8String) {
    try {
      setValue(UTF8Support.unicodeFromUTF8(utf8String));
    } catch (UTFDataFormatException udfe) {
      setValue(utf8String);
    }
  }

}
