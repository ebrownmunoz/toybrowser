package voxware.app.beans;

import java.util.Map;
import java.util.HashMap;

public class ResponseObject {
  Map    map = null;
  String error = null;

  public ResponseObject() {
  }

  public ResponseObject(String error) {
    this.error = error;
  }

  public Map getMap() {
    return this.map;
  }

  public void setMap(Map map) {
    this.map = map;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public boolean status() {
    /** No error, no problem... */
    return error == null;
  }
}
