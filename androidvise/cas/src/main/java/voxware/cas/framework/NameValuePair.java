package voxware.cas.framework;

import java.io.IOException;
import java.io.Serializable;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class NameValuePair implements Serializable {
  private String name;
  private String value;

  public NameValuePair(String name, String value) {
    this.name = name;
    this.value = value;
  }

  public void name(String name) {
    this.name = name;
  }

  public String name() {
    return name;
  }

  public void value(String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }

  private void writeObject(ObjectOutputStream s) throws IOException {
     // Write the fields
    s.writeObject(name);
    s.writeObject(value);
  }

  private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
     // Read the fields
    name = (String) s.readObject();
    value = (String) s.readObject();
  }
}
