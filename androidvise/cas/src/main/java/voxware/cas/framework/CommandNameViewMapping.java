package voxware.cas.framework;

import java.util.Map;
import java.util.HashMap;

public class CommandNameViewMapping {
  Map commandMap = null;

  public Map getCommandMap() {
    return commandMap;
  }

  public void setCommandMap(Map commandMap) {
    this.commandMap = commandMap;
  }

  public String getView(String cmdName) {
    String view = null;

    view = (String) commandMap.get(cmdName);
    return view;
  }
}

  
