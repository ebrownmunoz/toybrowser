package voxware.cas.handlers;

import java.util.Map;

import voxware.cas.framework.Command;
import voxware.cas.framework.CommandHandler;
import voxware.cas.framework.exceptions.*;

public class DefaultHandler implements CommandHandler {
  public void execute(Command command, Map model) throws CommandException {
    System.out.println("DefaultHandler: processed execute() fo " + command.getClass().getName());
  }

  public Object createEvent(Command command, Map model) throws CommandException {
    System.out.println("DefaultHandler: processed createEvent() for " + command.getClass().getName());
    return null;
  }
}
