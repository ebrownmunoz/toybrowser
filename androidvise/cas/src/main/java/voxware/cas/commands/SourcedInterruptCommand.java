package voxware.cas.commands;

import voxware.cas.framework.Command;
import voxware.cas.framework.exceptions.*;

public class SourcedInterruptCommand implements Command {

  String source;
  Object content;

  public SourcedInterruptCommand() {
  }

  public SourcedInterruptCommand(String source) {
    this.source = source;
  }

  public SourcedInterruptCommand(String source, Object content) {
    this.source = source;
    this.content = content;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Object getContent() {
    return content;
  }

  public void setContent(Object content) {
    this.content = content;
  }

  public String toString() {
    return source + ":" + content.toString();
  }
}
