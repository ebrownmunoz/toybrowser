// Copyright (c) 2000 Just Objects B.V. <just@justobjects.nl>
// Distributable under LGPL license. See terms of license at gnu.org.
package nl.justobjects.pushlet.util;


/**
 * Logging utility.
 * TODO - use java.util Logging
 *
 * @author Just van den Broecke
 * @version $Id: Log.java,v 1.2 2005/02/21 11:15:59 justb Exp $
 */
public class Log {
  private static boolean debug = false;
  private static int LEVEL_NONE = 0;
  private static int LEVEL_FATAL = 1;
  private static int LEVEL_WARN = 2;
  private static int LEVEL_INFO = 3;
  private static int LEVEL_DEBUG = 4;

  /** Level intialized with default. */
  private static int level = LEVEL_NONE;

  /**
   * Log message for debug level.
   *
   * @param aMessage the message to be logged
   */
  static public void debug(String aMessage) {
    if (level < LEVEL_DEBUG) {
      return;
    }

    print("DEBUG", aMessage);
  }

  /**
   * Log message for info level.
   *
   * @param aMessage the message to be logged
   */
  static public void info(String aMessage) {
    if (level < LEVEL_INFO) {
      return;
    }

    print("INFO", aMessage);
  }

  /**
   * Log message for warning level.
   *
   * @param aMessage the message to be logged
   */
  static public void warn(String aMessage) {
    if (level < LEVEL_WARN) {
      return;
    }

    print("WARN", aMessage);
  }

  /**
   * Log message for warning level with exception.
   *
   * @param aMessage the message to be logged
   * @param aThrowable the exception
   */
  static public void warn(String aMessage, Throwable aThrowable) {
    if (level < LEVEL_WARN) {
      return;
    }

    warn(aMessage + " exception=" + aThrowable);
  }

  /**
   * Log message for fatal level.
   *
   * @param aMessage the message to be logged
   */
  static public void fatal(String aMessage) {
    if (level < LEVEL_FATAL) {
      return;
    }

    print("FATAL", aMessage);
  }

  /**
   * Log message (fatal level with exception).
   *
   * @param aMessage the message to be logged
   * @param aThrowable the exception
   */
  static public void fatal(String aMessage, Throwable aThrowable) {
    if (level < LEVEL_FATAL) {
      return;
    }

    fatal(aMessage + " exception=" + aThrowable);
  }

  /**
   * Set log level
   *
   * @param aLevel the message to be logged
   */
  static public void setLevel(int aLevel) {
    level = aLevel;
  }

  /**
   * Print message.
   *
   * @param aTag the log type
   * @param aMessage the message to be logged
   */
  static private void print(String aTag, String aMessage) {
    // For now...
    System.out.println("Pushlet[" + aTag + "] " + aMessage);
  }
}

/*
* $Log: Log.java,v $
* Revision 1.2  2005/02/21 11:15:59  justb
* support log levels
*
* Revision 1.1  2005/02/18 10:07:23  justb
* many renamings of classes (make names compact)
*
* Revision 1.7  2004/09/03 22:35:37  justb
* Almost complete rewrite, just checking in now
*
* Revision 1.6  2004/08/12 13:16:08  justb
* make debug flag false
*
* Revision 1.5  2004/03/10 14:01:55  justb
* formatting and *Subscriber refactoring
*
* Revision 1.4  2003/08/15 09:54:46  justb
* fix javadoc warnings
*
* Revision 1.3  2003/08/15 08:37:40  justb
* fix/add Copyright+LGPL file headers and footers
*
* Revision 1.2  2003/08/12 09:42:47  justb
* enhancements
*
* Revision 1.1  2003/08/12 08:46:00  justb
* cvs comment tags added
*
*
*/
