// Copyright (c) 2000 Just Objects B.V. <just@justobjects.nl>
// Distributable under LGPL license. See terms of license at gnu.org.
package nl.justobjects.pushlet.core;

import java.util.StringTokenizer;

import nl.justobjects.pushlet.util.Log;


/**
 * Represents client pushlet session state.
 *
 * @version $Id: Session.java,v 1.7 2005/02/28 15:58:05 justb Exp $
 * @author Just van den Broecke - Just Objects &copy;
 **/
public class Session implements Protocol, ConfigDefs {
  public static String[] FORCED_PULL_AGENTS = null;

  //!! CAV Config.getProperty(LISTEN_FORCE_PULL_AGENTS).split(",");
  static {
    StringTokenizer st = new StringTokenizer(Config.getProperty(LISTEN_FORCE_PULL_AGENTS), ",");
    FORCED_PULL_AGENTS = new String[st.countTokens()];
    for (int i = 0; i < FORCED_PULL_AGENTS.length; i++) {
      FORCED_PULL_AGENTS[i] = st.nextToken();
    }
  }
    
  private Controller controller;
  private Subscriber subscriber;
  private String userAgent;
  private long LEASE_TIME_MILLIS = Config.getLongProperty(SESSION_TIMEOUT_MINS) * 60 * 1000;
  private volatile long timeToLive = LEASE_TIME_MILLIS;
  private String address = "unknown";
  private String format = FORMAT_XML;
  private String id;

  /** Package-visible constructor. */
  Session(String anId) {
    id = anId;
    controller = new Controller(this);
    subscriber = new Subscriber(this);
  }

  /** Return (remote) Subscriber client's IP address. */
  public String getAddress() {
    return address;
  }

  /** Return command controller. */
  public Controller getController() {
    return controller;
  }

  /** Return Event format to send to client. */
  public String getFormat() {
    return format;
  }

  /** Return (remote) Subscriber client's unique id. */
  public String getId() {
    return id;
  }

  /** Return subscriber. */
  public Subscriber getSubscriber() {
    return subscriber;
  }

  /** Return remote HTTP User-Agent. */
  public String getUserAgent() {
    return userAgent;
  }

  /** Set address. */
  protected void setAddress(String anAddress) {
    address = anAddress;
  }

  /** Set event format to encode. */
  protected void setFormat(String aFormat) {
    format = aFormat;
  }

  /** Set client HTTP UserAgent. */
  public void setUserAgent(String aUserAgent) {
    userAgent = aUserAgent;
  }

  /** Decrease time to live. */
  public void age(long aDeltaMillis) {
    timeToLive -= aDeltaMillis;
  }

  /** Has session timed out? */
  public boolean isExpired() {
    return timeToLive <= 0;
  }

  /** Keep alive by resetting TTL. */
  public void kick() {
    timeToLive = LEASE_TIME_MILLIS;
  }

  public void start() {
    SessionManager.getInstance().addSession(this);
  }

  public void stop() {
    SessionManager.getInstance().removeSession(this);
  }

  /** Info. */
  public void info(String s) {
    Log.info("S-" + this + ": " + s);
  }

  /** Exceptional print util. */
  public void warn(String s) {
    Log.warn("S-" + this + ": " + s);
  }

  /** Exceptional print util. */
  public void debug(String s) {
    Log.debug("S-" + this + ": " + s);
  }

  public String toString() {
    return getAddress() + "[" + getId() + "]";
  }
}

/*
 * $Log: Session.java,v $
 * Revision 1.7  2005/02/28 15:58:05  justb
 * added SimpleListener example
 *
 * Revision 1.6  2005/02/28 12:45:59  justb
 * introduced Command class
 *
 * Revision 1.5  2005/02/28 09:14:55  justb
 * sessmgr/dispatcher factory/singleton support
 *
 * Revision 1.4  2005/02/25 15:13:01  justb
 * session id generation more robust
 *
 * Revision 1.3  2005/02/21 16:59:08  justb
 * SessionManager and session lease introduced
 *
 * Revision 1.2  2005/02/21 12:32:28  justb
 * fixed publish event in Controller
 *
 * Revision 1.1  2005/02/21 11:50:46  justb
 * ohase1 of refactoring Subscriber into Session/Controller/Subscriber
 *

 *
 */
