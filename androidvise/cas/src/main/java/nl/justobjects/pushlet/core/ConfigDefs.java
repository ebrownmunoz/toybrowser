// Copyright (c) 2000 Just Objects B.V. <just@justobjects.nl>
// Distributable under LGPL license. See terms of license at gnu.org.
package nl.justobjects.pushlet.core;


/**
 * Definition of config property strings.
 *
 * @version $Id: ConfigDefs.java,v 1.5 2005/02/28 09:14:55 justb Exp $
 * @author Just van den Broecke - Just Objects &copy;
 **/
public interface ConfigDefs {
  /** Session Manager */
  public static final String SESSION_MANAGER_CLASS = "sessionmanager.class";

  /** Dispatcher */
  public static final String DISPATCHER_CLASS = "dispatcher.class";

  /** Session management. */
  public static final String SESSION_ID_SIZE = "session.id.size";
  public static final String SESSION_TIMEOUT_MINS = "session.timeout.mins";
  public static final String SOURCES_ACTIVATE = "sources.activate";

  /** Logging */
  public static final String LOG_LEVEL = "log.level";

  /** Queues */
  public static final String QUEUE_SIZE = "queue.size";
  public static final String QUEUE_READ_TIMEOUT_MILLIS = "queue.read.timeout.millis";
  public static final String QUEUE_WRITE_TIMEOUT_MILLIS = "queue.write.timeout.millis";

  /** Listening modes. */
  public static final String LISTEN_FORCE_PULL_ALL = "listen.force.pull.all";
  public static final String LISTEN_FORCE_PULL_AGENTS = "listen.force.pull.agents";
  public static final String PULL_REFRESH_TIMEOUT_MILLIS = "pull.refresh.timeout.millis";
  public static final String PULL_REFRESH_WAIT_MIN_MILLIS = "pull.refresh.wait.min.millis";
  public static final String PULL_REFRESH_WAIT_MAX_MILLIS = "pull.refresh.wait.max.millis";
  public static final String POLL_REFRESH_TIMEOUT_MILLIS = "poll.refresh.timeout.millis";
  public static final String POLL_REFRESH_WAIT_MIN_MILLIS = "poll.refresh.wait.min.millis";
  public static final String POLL_REFRESH_WAIT_MAX_MILLIS = "poll.refresh.wait.max.millis";
}

/*
  * $Log: ConfigDefs.java,v $
  * Revision 1.5  2005/02/28 09:14:55  justb
  * sessmgr/dispatcher factory/singleton support
  *
  * Revision 1.4  2005/02/21 16:59:00  justb
  * SessionManager and session lease introduced
  *
  * Revision 1.3  2005/02/21 11:50:46  justb
  * ohase1 of refactoring Subscriber into Session/Controller/Subscriber
  *
  * Revision 1.2  2005/02/21 11:16:44  justb
  * add log level config prop
  *
  * Revision 1.1  2005/02/18 12:36:47  justb
  * changes for renaming and configurability
  *
  *
  *
  */
