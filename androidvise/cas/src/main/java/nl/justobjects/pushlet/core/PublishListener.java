package nl.justobjects.pushlet.core;


public interface PublishListener {
  public void report(Throwable exception);     // Report an exception
  public void event(Event event);              // Report an incoming message
  public void heartbeat();                     // Report incoming heartbeat
}