// Copyright (c) 2000 Just Objects B.V. <just@justobjects.nl>
// Distributable under LGPL license. See terms of license at gnu.org.
package nl.justobjects.pushlet.util;

import java.io.IOException;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;


/**
 * Utilities that interact with the underlying OS/JVM.
 *
 * @author Just van den Broecke
 * @version $Id: Sys.java,v 1.3 2006/05/06 00:06:28 justb Exp $
 */
public class Sys {
  /**
   * Replace characters having special meaning <em>inside</em> HTML tags
   * with their escaped equivalents, using character entities such as <tt>'&amp;'</tt>.
   *
   * <P>The escaped characters are :
   * <ul>
   * <li> <
   * <li> >
   * <li> "
   * <li> '
   * <li> \
   * <li> &
   * </ul>
   *
   * <P>This method ensures that arbitrary text appearing inside a tag does not "confuse"
   * the tag. For example, <tt>HREF='Blah.do?Page=1&Sort=ASC'</tt>
   * does not comply with strict HTML because of the ampersand, and should be changed to
   * <tt>HREF='Blah.do?Page=1&amp;Sort=ASC'</tt>. This is commonly seen in building
   * query strings. (In JSTL, the c:url tag performs this task automatically.)
   */
  static public String forHTMLTag(String aTagFragment) {
    final StringBuffer result = new StringBuffer();

    final StringCharacterIterator iterator = new StringCharacterIterator(aTagFragment);
    char character = iterator.current();
    while (character != CharacterIterator.DONE) {
      if (character == '<') {
        result.append("&lt;");
      } else if (character == '>') {
        result.append("&gt;");
      } else if (character == '\"') {
        result.append("&quot;");
      } else if (character == '\'') {
        result.append("&#039;");
      } else if (character == '\\') {
        result.append("&#092;");
      } else if (character == '&') {
        result.append("&amp;");
      } else {
        //the char is not a special one
        //add it to the result as is
        result.append(character);
      }

      character = iterator.next();
    }

    return result.toString();
  }

  /** Load properties file from classpath. */
  static public Properties loadPropertiesResource(String aResourcePath)
    throws IOException {
    try {
      // Use the class loader that loaded our class.
      // This is required where for reasons like security
      // multiple class loaders exist, e.g. BEA WebLogic.
      // Thanks to Lutz Lennemann 29-aug-2000.
      //!! CAV
      Object o = new Object();
      ClassLoader classLoader = o.getClass().getClassLoader();

      Properties properties = new Properties();

      // Try loading it.
      InputStream inputStream = classLoader.getResourceAsStream(aResourcePath);
      if (inputStream == null)
        throw new Throwable();
      properties.load(classLoader.getResourceAsStream(aResourcePath));
      return properties;
    } catch (Throwable t) {
      throw new IOException("failed loading Properties resource from " +
        aResourcePath);
    }
  }

  /** Shorthand for current time. */
  static public long now() {
    return System.currentTimeMillis();
  }

  private static Properties properties;
  public static Properties getProperties() {
    return Sys.properties;
  }

  public static void setProperties(Properties properties) {
    Sys.properties = properties;
  }
}
