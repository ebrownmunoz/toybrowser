// Copyright (c) 2000 Just Objects B.V. <just@justobjects.nl>
// Distributable under LGPL license. See terms of license at gnu.org.
package nl.justobjects.pushlet.core;

import nl.justobjects.pushlet.util.Log;
import nl.justobjects.pushlet.util.Sys;

import java.util.Properties;


/**
 * Loads and maintains overall configuration.
 *
 * @version $Id: Config.java,v 1.2 2006/05/06 00:10:11 justb Exp $
 * @author Just van den Broecke - Just Objects &copy;
 **/
public class Config implements ConfigDefs {
  private static final String PROPERTIES_RESOURCE = "pushlet.properties";
  private static Properties properties;

  /**
   * Initialize event sources from properties file.
   */
  public static void load() {
    // Load Event sources using properties file.
    Log.info("Config: loading");
    try {
      properties = Sys.loadPropertiesResource(PROPERTIES_RESOURCE);
    } catch (Throwable t) {
      Log.warn("Config: cannot find properties file: " + PROPERTIES_RESOURCE, t);
      properties = Sys.getProperties();
      return;
    }

    Log.info("Config: loaded values=" + properties);
  }

  public static String getProperty(String aName) {
    String value = properties.getProperty(aName);
    if (value == null) {
      throw new IllegalArgumentException("Unknown property: " + aName);
    }

    return value;
  }

  public static boolean getBoolProperty(String aName) {
    String value = getProperty(aName);
    try {
      return value.equals("true");
    } catch (Throwable t) {
      throw new IllegalArgumentException("Illegal property value: " + aName +
        " val=" + value);
    }
  }

  public static int getIntProperty(String aName) {
    String value = getProperty(aName);
    try {
      return Integer.parseInt(value);
    } catch (Throwable t) {
      throw new IllegalArgumentException("Illegal property value: " + aName +
        " val=" + value);
    }
  }

  public static long getLongProperty(String aName) {
    String value = getProperty(aName);
    try {
      return Long.parseLong(value);
    } catch (Throwable t) {
      throw new IllegalArgumentException("Illegal property value: " + aName +
        " val=" + value);
    }
  }

  public static boolean hasProperty(String aName) {
    return properties.containsKey(aName);
  }
}

/*
 * $Log: Config.java,v $
 * Revision 1.2  2006/05/06 00:10:11  justb
 * various chgs but not too serious...
 *
 * Revision 1.1  2005/02/18 12:36:47  justb
 * changes for renaming and configurability
 *

 *
 */
