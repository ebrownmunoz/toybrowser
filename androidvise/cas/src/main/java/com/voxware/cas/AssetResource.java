package com.voxware.cas;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;

import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.util.resource.Resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;


/**
 * Created by edh on 12/11/2015.
 */
public class AssetResource extends Resource {

    private final AssetManager assetManager;
    private final URI uri;
    private final boolean exists;

    public AssetResource(AssetManager assetManager, URI uri) throws IOException, URISyntaxException {
        this.assetManager = assetManager;
        this.uri = uri;
        this.exists = checkExists();
    }

    @Override
    public boolean isContainedIn(Resource r) throws MalformedURLException {
        if (r instanceof AssetResource) {
            AssetResource maybeParent = (AssetResource)r;
            String maybeParentPath = maybeParent.uri.getPath();
            if (maybeParent.uri.getScheme().equals(uri.getScheme()) && maybeParentPath.endsWith("/")) {
                return uri.getPath().startsWith(maybeParentPath);
            }
        }
        return false;
    }

    @Override
    public void release() {

    }

    @Override
    public boolean exists() {
        return exists;
    }

    protected boolean checkExists() {
        String path = uri.getPath();
        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        InputStream is = null;
        try {
            is = assetManager.open(path);
            return true;
        } catch (IOException e) {
            try {
                assetManager.list(path);
                return true;
            } catch (IOException e1) {
                return false;
            }
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    @Override
    public boolean isDirectory() {
        return uri.getPath().endsWith("/");
    }

    @Override
    public long lastModified() {
        return -1;
    }

    @Override
    public long length() {
        AssetFileDescriptor fd = null;
        try {
            String path = uri.getPath();
            if (path.startsWith("/")) {
                path = path.substring(1);
            }
            fd = assetManager.openFd(path);
            return fd.getLength();
        } catch (IOException e) {
            return -1;
        } finally {
            if (fd != null) {
                try {
                    fd.close();
                } catch (IOException e) {
                    // eat this
                }
            }
        }
    }

    @Override
    public URL getURL() {
        try {
            return uri.toURL();
        } catch (MalformedURLException e) {
            return null;
        }
    }

    @Override
    public File getFile() throws IOException {
        return null;
    }

    @Override
    public String getName() {
        String path = uri.getPath();
        int lastSlash = path.lastIndexOf('/');
        if (lastSlash != -1) {
            return path.substring(lastSlash + 1);
        } else {
            return path;
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return assetManager.open(uri.getPath());
    }

    @Override
    public OutputStream getOutputStream() throws IOException, SecurityException {
        throw new IOException( "Output not supported");
    }

    @Override
    public boolean delete() throws SecurityException {
        throw new SecurityException( "Delete not supported");
    }

    @Override
    public boolean renameTo(Resource dest) throws SecurityException {
        throw new SecurityException( "renameTo not supported");
    }

    @Override
    public String[] list() {
        try {
            return assetManager.list(uri.getPath());
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public Resource addPath(String path) throws IOException, MalformedURLException {
        try {
            return new AssetResource(assetManager, uri.resolve(path));
        } catch (URISyntaxException e) {
            MalformedURLException t =  new MalformedURLException("Error creating new path");
            t.initCause(e);
            throw t;
        }
    }
}
