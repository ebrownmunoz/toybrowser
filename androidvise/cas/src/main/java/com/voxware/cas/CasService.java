package com.voxware.cas;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Properties;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.justobjects.pushlet.util.Sys;
import voxware.client.Configuration;

public class CasService extends Service {
    public static final String KEYSTORE_NAME = "keystore";
    public static final String ANDROID_CONTEXT = "com.voxware.cas.CasService";

    private static Logger log = LoggerFactory.getLogger(CasService.class);
    private android.os.Handler handler;
    private Runnable runnable = new Runnable() {

        @Override
        public void run() {
            Server server = null;

            try {
                server = createServer();
                server.start();
            } catch (Exception e) {
                log.error("Failed to start CAS", e);
            }
            try {
                server.join();
            } catch (InterruptedException e) {
                try {
                    server.stop();
                    server.destroy();
                } catch (Exception e1) {
                    log.error("Error caught stopping server", e1);
                }
            }
        }
    };
    private Thread serverThread = new Thread(runnable, "CAS-Launcher");

    public class CasBinder extends Binder {
        public CasService getService() {
            return CasService.this;
        }
    }

    public CasService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PRNGFixes.apply();
        Properties properties = new Properties();
        InputStream is = null;
        try {
            is = getAssets().open("pushlet.properties");
            properties.load(is);
        } catch (IOException e) {

        } finally {
            IOUtils.closeQuietly(is);
        }
        Sys.setProperties(properties);
        Configuration.setDefaultContext(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new CasBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (serverThread.getState() == Thread.State.TERMINATED) {
            serverThread = new Thread(runnable);
        }
        if (serverThread.getState() == Thread.State.NEW) {
            serverThread.start();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        serverThread.interrupt();

        super.onDestroy();
    }

    protected Server createServer() throws IOException, URISyntaxException {
        Server server = new Server();
        server.setConnectors(createConnectors());
        server.setHandler(createHandler());
        return server;
    }

    protected Connector[] createConnectors() {
        Connector httpConnector = createHttpConnector();
        //Connector httpsConnector = createHttpsConnector();

        return new Connector[] {
            httpConnector,
            //httpsConnector
        };
    }

    protected Connector createHttpConnector() {
        SelectChannelConnector httpConnector = new SelectChannelConnector();
        httpConnector.setUseDirectBuffers(false);
        httpConnector.setPort(8080);
        return httpConnector;
    }

    protected Connector createHttpsConnector() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        SslContextFactory sslContextFactory = new SslContextFactory();
        InputStream in = openFileInput(KEYSTORE_NAME);
        KeyStore keyStore = KeyStore.getInstance("BKS");
        try {
            keyStore.load(in, getKeystorePassword().toCharArray());
        } finally {
            IOUtils.closeQuietly(in);
        }

        SslSelectChannelConnector httpsConnector = new SslSelectChannelConnector();
        httpsConnector.setUseDirectBuffers(false);
        httpsConnector.setPort(8443);
        return httpsConnector;
    }

    protected String getKeystorePassword() {
        return "voxware";
    }

    protected org.eclipse.jetty.server.Handler createHandler() throws IOException, URISyntaxException {
        HandlerCollection handlers = new HandlerCollection();
        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.addHandler(getLogHandler());
        contexts.addHandler(getWebAppContext());
        handlers.setHandlers(new Handler[] {contexts, new DefaultHandler()});
        return handlers;
    }

    protected WebAppContext getWebAppContext() throws IOException, URISyntaxException {
        File webAppDir = explodeWebApp();
        WebAppContext context = new WebAppContext();
        context.setContextPath("/");
        context.setTempDirectory(new File(getFilesDir(), "jetty/temp"));
        context.setExtractWAR(false);
        context.setCopyWebDir(false);
        context.setCopyWebInf(false);
        context.setClassLoader(new URLClassLoader(new URL[] {
                webAppDir.toURI().toURL(),
                new File(webAppDir, "WEB-INF/classes/").toURI().toURL()
        }, this.getClassLoader() ));
        //context.setBaseResource(new AssetResource(getAssets(), URI.create("asset:///webapp/")));
        context.setBaseResource(Resource.newResource(webAppDir));
        context.getServletContext().setAttribute(ANDROID_CONTEXT, this);
        return context;
    }

    protected File explodeWebApp() {
        File webAppDir = new File(getFilesDir(), "jetty/webapp/");
        if (!webAppDir.exists()) {
            webAppDir.mkdirs();
        } else {
            try {
                FileUtils.cleanDirectory(webAppDir);
            } catch (IOException e) {
                log.error("Error cleaning " + webAppDir.getAbsolutePath(), e);
            }
        }

        explodeContents(webAppDir.getParentFile(), new String[] {"webapp"});
        return webAppDir;
    }

    protected void explodeContents(File destination, String[] elements) {
        for (String element : elements) {
            InputStream is = null;
            FileOutputStream fos = null;
            try {
                is = getAssets().open(element);
                File target = new File(destination, element);
                File parent = target.getParentFile();
                parent.mkdirs();
                fos = new FileOutputStream(target);
                IOUtils.copy(is, fos);
            } catch (FileNotFoundException e) {
                // not a file, likely a directory
                try {
                    String[] dirElements = getAssets().list(element);
                    for (int i = 0; i < dirElements.length; i++) {
                        dirElements[i] = element + "/" + dirElements[i];
                    }
                    explodeContents(destination, dirElements);
                } catch (IOException e1) {
                    log.error("Error exploding dir " + element, e1);
                }
            } catch (IOException e) {
                // trouble copying the file
                log.error("Error exploding file " + element, e);
            } finally {
                IOUtils.closeQuietly(fos);
                IOUtils.closeQuietly(is);
            }
        }
    }

    protected Handler getLogHandler() throws IOException {
        ResourceHandler resourceHandler = new ResourceHandler();

        resourceHandler.setDirectoriesListed(true);
        File logDir = this.getExternalFilesDir("logs");
        resourceHandler.setBaseResource(Resource.newResource(logDir));
        ContextHandler resourceContextHandler = new ContextHandler();
        resourceContextHandler.setContextPath("/logs");
        resourceContextHandler.setHandler(resourceHandler);

        ServletContextHandler allLogsContextHandler = new ServletContextHandler();
        allLogsContextHandler.setContextPath("/logs");
        allLogsContextHandler.addServlet(new ServletHolder(new AllLogsServlet()), "/logs.zip");

        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] {resourceContextHandler, allLogsContextHandler});
        return contexts;
    }

    protected class AllLogsServlet extends HttpServlet {
        private ThreadLocal<byte[]> buffer = new ThreadLocal<byte[]>() {
            @Override
            protected byte[] initialValue() {
                return new byte[128 * 1024];
            }
        };
        public AllLogsServlet() {

        }

        @Override
        public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            File logDir = CasService.this.getExternalFilesDir("logs");
            try {
                File tempFile = File.createTempFile("log", ".zip");
                try {
                    FileInputStream fis = null;
                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(tempFile);
                        ZipOutputStream zos = new ZipOutputStream(fos);
                        File[] files = logDir.listFiles();
                        for (File file : files) {
                            writeFileToZip(zos, "", file);
                        }
                        zos.close(); // write the temp file
                        fis = new FileInputStream(tempFile);

                        resp.setContentType("application/zip");
                        resp.setHeader("Content-Disposition", "attachment; filename=\"logs.zip\"");
                        IOUtils.copyLarge(fis, resp.getOutputStream(), buffer.get());

                    } finally {
                        IOUtils.closeQuietly(fis);
                        // in case we didn't close it earlier
                        IOUtils.closeQuietly(fos);
                    }
                } finally {
                    tempFile.delete();
                }
            } finally {
                buffer.remove();
            }
        }

        private void writeFileToZip(ZipOutputStream zos, String parent, File file) throws IOException {
            if (file.isDirectory()) {
                ZipEntry entry = new ZipEntry(parent + file.getName() + "/");
                entry.setMethod(ZipEntry.STORED);
                entry.setSize(0);
                entry.setCrc(0);
                entry.setTime(file.lastModified());
                zos.putNextEntry(entry);
                zos.closeEntry();
                File[] subfiles = file.listFiles();
                for (File subfile : subfiles) {
                    writeFileToZip(zos, parent + file.getName() + "/", subfile);
                }
            } else {
                ZipEntry entry = new ZipEntry(parent + file.getName());
                entry.setTime(file.lastModified());
                zos.putNextEntry(entry);
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    IOUtils.copyLarge(fis, zos, buffer.get());
                } finally {
                    IOUtils.closeQuietly(fis);
                }
                zos.closeEntry();
            }
        }
    }
}
