package com.voxware.cas;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * Created by edh on 12/11/2015.
 */
public class AndroidXmlWebApplicationContext extends XmlWebApplicationContext {
    @Override
    protected void initBeanDefinitionReader(XmlBeanDefinitionReader beanDefinitionReader) {
        super.initBeanDefinitionReader(beanDefinitionReader);
        // this is needed for Android, since it always throws an exception if validating is true
        beanDefinitionReader.setValidating(false);
    }
}
