package com.voxware.remote.services.cas;

import java.io.Serializable;

public class ClientInterruptDescriptor implements Serializable {

  public ClientInterruptDescriptor() {}

  protected String     source;
  public    String  getSource()               { return this.source; }
  public    void    setSource(String source)  { this.source = source; }

  protected String    content;
  public    String getContent()               { return this.content; }
  public    void   setContent(String content) { this.content = content; }

  public String toString() {
    return super.toString() + "[" + source + "/" + content + "]";
  }
}
