package com.voxware.remote.services.cas;

import java.io.Serializable;

public class ClientStatusDescriptor implements Serializable {

  public ClientStatusDescriptor() {}

  protected String     status;
  public    String  getStatus()               { return this.status; }
  public    void    setStatus(String status)  { this.status = status; }

  public String toString() {
    return super.toString() + "[" + status + "]";
  }
}
