package com.voxware.remote.services;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class Result implements Serializable {

         Object     object = null;
  public Object  getObject()               { return this.object; }
  public void    setObject(Object object)  { this.object = object; }

         Map           map = null;
  public Map        getMap()               { return this.map; }
  public void       setMap(Map map)        { this.map = map; }

         List         list = null;
  public List      getList()               { return this.list; }
  public void      setList(List list)      { this.list = list; }

         String    message = null;
  public String getMessage()               { return message; }
  public void   setMessage(String message) { this.message = message; }

         String      error = null;
  public String   getError()               { return error; }
  public void     setError(String error)   { this.error = error; }

  public Result() {}

  public Result(Object object, String error) {
    this.object = object;
    this.error = error;
  }

  public boolean status() {
     // No error, no problem...
    return error == null;
  }

}

