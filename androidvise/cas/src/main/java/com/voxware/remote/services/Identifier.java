package com.voxware.remote.services;

import java.io.Serializable;

public class Identifier implements Serializable {

  public Identifier()                     {}
  public Identifier(Long id)              { this.id = id; }
  public Identifier(String name)          { this.name = name; }
  public Identifier(Long id, String name) { this.id = id; this.name = name; }

  protected Long        id = null;
  public    Long     getId()            { return id; }
  public    void     setId(Long id)     { this.id = id; }

  protected String    name;
  public    String getName()            { return name; }
  public    void   setName(String name) { this.name = name; }

   // This identifier matches another iff every non-null field in the other is equal to the corresponding field in this one
  public boolean matches(Identifier template) {
    return template == null || (template.id == null || template.id.equals(id)) && (template.name == null || template.name.equals(name));
  }

   // This identifier is matched by another iff every non-null field in it is equal to the corresponding field in the other
  public boolean isMatchedBy(Identifier other) {
    return other != null && other.matches(this);
  }

   // This identifier equals another iff their corresponding fields are equal
  public boolean equals(Identifier other) {
    return (other != null) &&
           (id == null && other.id == null || id != null && id.equals(other.id)) &&
           (name == null && other.name == null || name != null && name.equals(other.name));
  }

   // This identifier isEquivalentTo another iff they refer to the same thing
  public boolean isEquivalentTo(Identifier other) {
    return matches(other) && isMatchedBy(other);
  }

  public static boolean areEquivalent(Identifier one, Identifier other) {
    return one == null && other == null || one != null && one.isEquivalentTo(other);
  }

   // This identifier is complete iff it has both an id and a name
  public boolean isComplete() {
    return id != null && name != null;
  }

  public static boolean isComplete(Identifier identifier) {
    return identifier != null && identifier.isComplete();
  }

   // This identifier is null iff both its id and its name are null
  public boolean isNull() {
    return id == null && name == null;
  }

  public static boolean isNull(Identifier identifier) {
    return identifier == null || identifier.isNull();
  }

  public String toString() {
    if (id != null) {
      return "[" + id.toString() + "/" + name + "]";
    } else {
      return "[null/" + name + "]";
    }
  }

  public static String toString(Identifier id) {
    return (String)((id != null) ? id.toString() : "[null]");
  }

}
