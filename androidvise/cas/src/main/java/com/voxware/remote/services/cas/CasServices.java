package com.voxware.remote.services.cas;

import com.voxware.remote.services.Result;

public interface CasServices {

   // Basic Client Configuration Methods

  public Result read(ClientConfigurationDescriptor descriptor);
  public Result update(ClientConfigurationDescriptor descriptor);

   // Client Interrupt Methods

  public Result create(ClientInterruptDescriptor descriptor);

   // Client Status Methods

  public Result read(ClientStatusDescriptor descriptor);

}
