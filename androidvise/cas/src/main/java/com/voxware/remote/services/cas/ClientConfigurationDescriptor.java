package com.voxware.remote.services.cas;

import java.io.Serializable;

public class ClientConfigurationDescriptor implements Serializable {

  public ClientConfigurationDescriptor() {}

  protected boolean             masked;
  public    boolean           isMasked()               { return this.masked; }
  public    void             setMasked(boolean masked) { this.masked = masked; }

  protected String         logicalName;
  public    String      getLogicalName()               { return this.logicalName; }
  public    void        setLogicalName(String name)    { this.logicalName = name; }

  protected String          licenseKey;
  public    String       getLicenseKey()               { return this.licenseKey; }
  public    void         setLicenseKey(String key)     { this.licenseKey = key; }

  protected String            serverIP;
  public    String         getServerIP()               { return this.serverIP; }
  public    void           setServerIP(String ip)      { this.serverIP = ip; }

  protected String       configuration;
  public    String    getConfiguration()               { return this.configuration; }
  public    void      setConfiguration(String cfg)     { this.configuration = cfg; }

  protected String    ttsConfiguration;
  public    String getTtsConfiguration()               { return this.ttsConfiguration; }
  public    void   setTtsConfiguration(String cfg)     { this.ttsConfiguration = cfg; }

  public String toString() {
    return super.toString() + "[" + logicalName + "/" + licenseKey + "/" + serverIP + "]";
  }
}
