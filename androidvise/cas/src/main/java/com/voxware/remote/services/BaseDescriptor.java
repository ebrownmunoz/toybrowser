package com.voxware.remote.services;

import java.io.Serializable;
import java.util.Map;

public class BaseDescriptor implements Serializable {

  public BaseDescriptor() {}

  public BaseDescriptor(Identifier id) { this.id = id; }

  protected Identifier         id;
  public    Identifier      getId()                   { return id; }
  public    void            setId(Identifier id)      { this.id = id; }

  protected String    description;
  public    String getDescription()                   { return this.description; }
  public    void   setDescription(String description) { this.description = description; }

  protected boolean        masked;
  public    boolean      isMasked()                   { return this.masked; }
  public    void        setMasked(boolean masked)     { this.masked = masked; }

  protected String     subcommand;
  public    String  getSubcommand()                   { return this.subcommand; }
  public    void    setSubcommand(String subcommand)  { this.subcommand = subcommand; }

  public Map map() {
    return null;
  }

  public String cacheKey() {
    return new String("[" + getId().getName() + "]");
  }

  public String toString() {
    return getClass().getName() + Identifier.toString(getId());
  }

}
