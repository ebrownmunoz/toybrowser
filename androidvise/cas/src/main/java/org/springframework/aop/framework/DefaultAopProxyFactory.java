/*
 * Copyright 2002-2004 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 

package org.springframework.aop.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple implementation of AopProxyFactory
 * @author Rod Johnson
 * @version $Id: DefaultAopProxyFactory.java,v 1.2 2004/03/18 02:46:05 trisberg Exp $
 */
public class DefaultAopProxyFactory implements AopProxyFactory {
  protected final Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * @see org.springframework.aop.framework.AopProxyFactory#createAopProxy(org.springframework.aop.framework.AdvisedSupport)
	 */
	public AopProxy createAopProxy(AdvisedSupport advisedSupport) throws AopConfigException {
		boolean useCglib = advisedSupport.getOptimize() || advisedSupport.getProxyTargetClass() || advisedSupport.getProxiedInterfaces().length == 0;
		if (useCglib) {
                  log.info("DefaultAopProxyFactory.createAopProxy: ERROR attempted to use Cglib2");
                        System.out.println("DefaultAopProxyFactory FATAL ERROR: Cglib2 is NOT AVAILABLE IN SPRING LITE!");
			//!!CAV			return CglibProxyFactory.createCglibProxy(advisedSupport);
                        return null;
		}
		else {
                  log.info("DefaultAopProxyFactory.createAopProxy: creating JdkDynamicAopProxy..");
                  // Depends on whether we have expose proxy or frozen or static ts
                  JdkDynamicAopProxy aopProxy = new JdkDynamicAopProxy(advisedSupport);
                  //!! CAV log.info("DefaultAopProxyFactory.createAopProxy: JdkDynamicAopProxy CREATED");
                  return aopProxy;
		  //                  return new JdkDynamicAopProxy(advisedSupport);
		}
	}
	
	/**
	 * Inner class to just introduce a CGLIB dependency
	 * when actually creating a CGLIB proxy.
	 */
	private static class CglibProxyFactory {

		private static AopProxy createCglibProxy(AdvisedSupport advisedSupport) {
                        System.out.println("DefaultAopProxyFactory (2) FATAL ERROR: Cglib2 is NOT AVAILABLE IN SPRING LITE!");
			//!!CAV			return new Cglib2AopProxy(advisedSupport);
                        return null;
		}
	}

}
