/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_voxware_vise_AndroidAudioSource */

#ifndef _Included_com_voxware_vise_AndroidAudioSource
#define _Included_com_voxware_vise_AndroidAudioSource
#ifdef __cplusplus
extern "C" {
#endif
#undef com_voxware_vise_AndroidAudioSource_LOG2_PLACES
#define com_voxware_vise_AndroidAudioSource_LOG2_PLACES 2L
#undef com_voxware_vise_AndroidAudioSource_LOG2_PLACEMASK
#define com_voxware_vise_AndroidAudioSource_LOG2_PLACEMASK 3L
/*
 * Class:     com_voxware_vise_AndroidAudioSource
 * Method:    initialize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_voxware_vise_AndroidAudioSource_initialize
  (JNIEnv *, jobject);

/*
 * Class:     com_voxware_vise_AndroidAudioSource
 * Method:    destroy
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_voxware_vise_AndroidAudioSource_destroy
  (JNIEnv *, jobject);

/*
 * Class:     com_voxware_vise_AndroidAudioSource
 * Method:    npower
 * Signature: (Ljava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_com_voxware_vise_AndroidAudioSource_npower
  (JNIEnv *, jobject, jobject);

#ifdef __cplusplus
}
#endif
#endif
