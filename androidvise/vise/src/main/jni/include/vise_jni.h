#ifndef VISE_JNI_H
#define VISE_JNI_H

#include <stddef.h>

//#ifdef __cplusplus
//extern "C" {
//#endif

namespace java {
	namespace lang {
		extern struct _ClassCastException {
			jclass clazz;
		} ClassCastException;
		extern struct  _IllegalArgumentException {
			jclass clazz;
		} IllegalArgumentException;
		extern struct  _InterruptedException {
        	jclass clazz;
        } InterruptedException;
		extern struct  _NullPointerException {
			jclass clazz;
		} NullPointerException;
		extern struct _OutOfMemoryError {
			jclass clazz;
		} OutOfMemoryError;
		extern struct _RuntimeException {
			jclass clazz;
		} RuntimeException;
		extern struct _String {
			jclass clazz;
		} String;
		extern struct _Thread {
		    jclass    clazz;
		    jmethodID currentThread;
		    jmethodID isInterrupted;
		} Thread;
		extern struct _UnsupportedOperationException {
			jclass clazz;
		} UnsupportedOperationException;
	}
	namespace nio {
        extern struct _ByteBuffer {
            jclass    clazz;
            jmethodID asReadOnlyBuffer;
        } ByteBuffer;
	}
    namespace util {
        extern struct _List {
            jclass    clazz;
            jmethodID add__Object;
        } List;
        extern struct _ArrayList {
        	jclass clazz;
			jmethodID ctor;
        } ArrayList;
    }
}



namespace com {
	namespace voxware {
		namespace vise {
			extern struct _AndroidAudioSource {
				jclass clazz;
				jfieldID ptr;
			} AndroidAudioSource;
		}
	}
}

namespace voxware {
	namespace engine {
		namespace recognition {
			namespace sapivise {
				extern struct _RawResult {
					jclass   clazz;
					jfieldID accepted;
					jfieldID audio;
					jfieldID grammarName;
					jfieldID hostTrans;
					jfieldID nbestArray;
					jfieldID result;
					jfieldID training;
				} RawResult;
				extern struct _SapiVise {
					jclass   clazz;
					jfieldID recognizer;
					jfieldID floatVal;
					jfieldID intVal;
				} SapiVise;
				extern struct _SapiViseGrammar{
					jclass    clazz;
					jfieldID  grammar;
					jfieldID  sapivise;
					jfieldID  myName;// inherited from BaseGrammar
					jfieldID  defaultRuleName;
					jmethodID isResultAudioProvided;
					jmethodID isTrainingProvided;
				} SapiViseGrammar;
				extern struct _ViseEnrollStatus {
					jclass    clazz;
					jmethodID ctor_III;
				} ViseEnrollStatus;
				extern struct _ViseResult {
					jclass    clazz;
					jfieldID  result;
				} ViseResult;
				extern struct _ViseTrainer {
					jclass    clazz;
					jfieldID  nativePeer;
				} ViseTrainer;
			}
		}
	}
}

/*
typedef struct _JavaCache {
    struct {
        struct {
            struct {
                jclass clazz;
            } ClassCastException;
            struct {
            	jclass clazz;
            } IllegalArgumentException;
            struct {
            	jclass clazz;
            } NullPointerException;
            struct {
            	jclass clazz;
            } OutOfMemoryError;
            struct {
                jclass clazz;
            } RuntimeException;
            struct {
            	jclass clazz;
            } String;
            struct {
            	jclass clazz;
            } UnsupportedOperationException;
        } lang;
        struct {
            struct {
                jclass    clazz;
                jmethodID asReadOnlyBuffer;
            } ByteBuffer;
        } nio;
        struct {
            struct {
                jclass    clazz;
                jmethodID add__Object;
            } List;
            struct {
            	jclass clazz;
				jmethodID ctor;
            } ArrayList;
        } util;
    } java;
    struct {
        struct {
            struct {
                struct {
                    jclass   clazz;
                    jfieldID ptr;
                } AndroidAudioSource;
                struct {
                    jclass   clazz;
                    jfieldID ptr;
                } VISERecognizer;
                struct {
                    jclass    clazz;
                    jfieldID  ptr;
                } VISEGrammar;
            } vise;
        } voxware;
    } com;
    struct {
    	struct {
    		struct {
    			struct {
    				struct {
    					jclass   clazz;
    					jfieldID accepted;
    					jfieldID audio;
    					jfieldID grammarName;
    					jfieldID hostTrans;
    					jfieldID nbestArray;
    					jfieldID result;
    					jfieldID training;
    				} RawResult;
    				struct {
    					jclass   clazz;
    					jfieldID recognizer;
    					jfieldID floatVal;
    					jfieldID intVal;
    				} SapiVise;
    				struct {
    					jclass    clazz;
    					jmethodID isResultAudioProvided;
    					jmethodID isTrainingProvided;
    				} SapiViseGrammar;
    			} sapivise;
    		} recognition;
    	} engine;
    } voxware;
} JavaCache;
*/

jboolean JavaToUTF8String(JNIEnv *env, jstring src, char *dest, const size_t destLen);

//#ifdef __cplusplus
//}
//#endif

//extern JavaCache jni;

#endif
