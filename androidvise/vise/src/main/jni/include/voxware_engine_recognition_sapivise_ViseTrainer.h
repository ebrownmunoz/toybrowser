/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class voxware_engine_recognition_sapivise_ViseTrainer */

#ifndef _Included_voxware_engine_recognition_sapivise_ViseTrainer
#define _Included_voxware_engine_recognition_sapivise_ViseTrainer
#ifdef __cplusplus
extern "C" {
#endif
#undef voxware_engine_recognition_sapivise_ViseTrainer_DATAFILEERROR
#define voxware_engine_recognition_sapivise_ViseTrainer_DATAFILEERROR 100L
#undef voxware_engine_recognition_sapivise_ViseTrainer_NOTENOUGHDATA
#define voxware_engine_recognition_sapivise_ViseTrainer_NOTENOUGHDATA 101L
#undef voxware_engine_recognition_sapivise_ViseTrainer_VALUEOUTOFRANGE
#define voxware_engine_recognition_sapivise_ViseTrainer_VALUEOUTOFRANGE 102L
/*
 * Class:     voxware_engine_recognition_sapivise_ViseTrainer
 * Method:    n_initialize
 * Signature: (Ljava/nio/ByteBuffer;)Z
 */
JNIEXPORT jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1initialize
  (JNIEnv *, jobject, jobject);

/*
 * Class:     voxware_engine_recognition_sapivise_ViseTrainer
 * Method:    n_release
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1release
  (JNIEnv *, jobject);

/*
 * Class:     voxware_engine_recognition_sapivise_ViseTrainer
 * Method:    n_insertcorrection
 * Signature: (Ljava/lang/String;Lvoxware/engine/recognition/sapivise/ViseResult;)Z
 */
JNIEXPORT jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1insertcorrection
  (JNIEnv *, jobject, jstring, jobject);

/*
 * Class:     voxware_engine_recognition_sapivise_ViseTrainer
 * Method:    n_enrollword
 * Signature: ()Lvoxware/engine/recognition/sapivise/ViseEnrollStatus;
 */
JNIEXPORT jobject JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1enrollword
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
