#ifndef _CNISAPIVISE_HH_
#define _CNISAPIVISE_HH_

#include <jni.h>

// Verbex SAPI objects
#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "audiofile.hh"
#if defined(WINCEAUDIO)
#include "mmaudiosrc.hh"
#elif defined(OSSAUDIO)
#include "ossaudiosrc.hh"
#elif defined(INROADAUDIO)
#include "inroadaudiosrc.hh"
#elif defined(__ANDROID__)
#include "android/androidaudiosrc.hh"
#else
#include "nistaudio.hh"
#endif
#include "vbxenum.hh"
#include "viseobj.hh"
#include "phrase.hh"
#include "mbox.h"

#define DEBUG 1
#define SMAX  256

typedef struct NotifyMsg_s {
  DWORD                   flags;
  class CNISapiViseGram * gramPtr;
  LPUNKNOWN               results;
} NotifyMsg_t, NotifyMsg;

class CNISapiVise : public ISRNotifySink  {
public:
    // Constructor isn't actually called until the
    // allocate() method is called on the Java object.
    CNISapiVise();
   ~CNISapiVise(void);

    // Standard IUnkown members, common to OLE objects
    STDMETHODIMP         QueryInterface (REFIID, LPVOID FAR *);
    STDMETHODIMP_(ULONG) AddRef(void);
    STDMETHODIMP_(ULONG) Release(void);

    // Notifications Inherited from ISRNotifySink
    STDMETHOD  (AttribChanged) (DWORD);
    STDMETHOD  (Interference)  (QWORD, QWORD, DWORD);
    STDMETHOD  (Sound)         (QWORD, QWORD);
    STDMETHOD  (UtteranceBegin)(QWORD);
    STDMETHOD  (UtteranceEnd)  (QWORD, QWORD);
    STDMETHOD  (VUMeter)       (QWORD, WORD);        

    Bool Initialize(Bool voice);
    
    LPUNKNOWN       AudioUnknwnPtr;
    IAudio         *IAudioPtr;
    VbxEnum        *VbxEnumPtr;
    ISRAttributes  *ISRAttributesPtr;
    ISRCentral     *ISRCentralPtr;
    ISRFind        *ISRFindPtr;
    ISRSpeaker     *ISRSpeakerPtr;
    IVbxAudioFile  *IVbxAudioFilePtr;
    IVbxEngine     *IVbxEnginePtr;

    //jobject  JavaRec;          // instance associated with this obj
    MBOX_Att MboxAttr;
    MBOX     NotifyMbox;
    CLASS    NotifyClass;

    WORD     VU;
private:
    IMemory  allocator;
    ULONG    RefCnt;           // OLE reference counting
    
    // class SapiSRNotify *notify;
    Bool     success;
    DWORD    NotifySinkKey;    // Key received when registering notification
                              // sink, needed to unRegister().
        
    friend class SAPIGrammarImpl;
};


#endif // _CNISAPIVISE_HH_
