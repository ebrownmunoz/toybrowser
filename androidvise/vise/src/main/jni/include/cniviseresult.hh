#ifndef _JNISAPIVISERES_HH_
#define _JNISAPIVISERES_HH_

  // Verbex SAPI objects
#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "vbxenum.hh"
#include "viseobj.hh"
#include "visegram.hh"
#include "viseresults.hh"
#include "phrase.hh"

// CNI Inclusions
#include <jni.h>

#endif // _JNISAPIVISERES_HH_

