#ifndef _CNISAPIVISEENR_HH_
#define _CNISAPIVISEENR_HH_

#include <jni.h>

  // Verbex SAPI objects
#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "vbxenum.hh"
#include "viseobj.hh"
#include "visegram.hh"
#include "viseresults.hh"
#include "phrase.hh"

#endif // _CNISAPIVISEENR_HH_

