//
// Created by edh on 3/16/2016.
//

#ifndef ANDROIDVISE_COM_VOXWARE_VISE_NATIVELOGSOURCE_H
#define ANDROIDVISE_COM_VOXWARE_VISE_NATIVELOGSOURCE_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif
    void vbx_com_voxware_vise_NativeLogSource_bridge(const char * string);
    JNIEXPORT jstring JNICALL Java_com_voxware_vise_NativeLogSource_pumpMessage(JNIEnv *, jobject);
#ifdef __cplusplus
}
#endif

#endif //ANDROIDVISE_COM_VOXWARE_VISE_NATIVELOGSOURCE_H
