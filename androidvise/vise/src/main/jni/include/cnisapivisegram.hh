#ifndef _CNISAPIVISEGRAM_HH_
#define _CNISAPIVISEGRAM_HH_

#include <jni.h>

  // Verbex SAPI objects
#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "vbxenum.hh"
#include "viseobj.hh"
#include "phrase.hh"

#include "class.h"

class CNISapiViseGram : public ISRGramNotifySink {
  public:
    CNISapiViseGram(jobject javaGram, class CNISapiVise *rec);
   ~CNISapiViseGram();

    // Standard IUnknown members, common to OLE objects
    STDMETHODIMP         QueryInterface (REFIID, LPVOID FAR *);
    STDMETHODIMP_(ULONG) AddRef(void);
    STDMETHODIMP_(ULONG) Release(void);

    // Per-text-buffer Notifications inheritied from ITTSBufNotifySink
    STDMETHOD (BookMark)         (DWORD);
    STDMETHOD (Paused)           (void);
    STDMETHOD (PhraseFinish)     (DWORD, QWORD, QWORD, PSRPHRASE,	LPUNKNOWN);
    STDMETHOD (PhraseHypothesis) (DWORD, QWORD, QWORD, PSRPHRASE, LPUNKNOWN);
    STDMETHOD (PhraseStart)      (QWORD);
    STDMETHOD (ReEvaluate)       (LPUNKNOWN);
    STDMETHOD (Training)         (DWORD);
    STDMETHOD (UnArchive)        (LPUNKNOWN);

    // Non-SAPI methods
    void ProcessNotify(JNIEnv*, OBJT, jobject);

    ISRGramCommon   * ISRGramCommonPtr;
    IVbxGrammar     * IVbxGrammarPtr;
    LPUNKNOWN         GramUnknwnPtr;

    class   CNISapiVise *Owner;
    jobject JavaGram;
    char    ActiveRule[128];

  private:
    int RefCnt;
};


#endif // _CNISAPIVISEGRAM_HH_

