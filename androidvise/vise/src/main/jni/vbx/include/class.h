/* class.h - methods for classes and objects
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _CLASS_H_
#define _CLASS_H_

#include "pthr.h"
#include "types.h"
#include "critsect.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CLASS_s * CLASS;     /* Class */
typedef struct OBJT_s  * OBJT;      /* Object */

typedef struct CLASS_s {
  IMemory       * memory;
  OBJT            freeObjects;
  OBJT            objects;
  UInt            objectSize;
  UInt            numObjects;
  void         (* newFcn)(OBJT, Ptr);
  void         (* freeFcn)(OBJT, Ptr);
  Ptr             environment;
  MUTEX_DECLARE(mutex)  
} CLASS_t;

struct OBJT_s {
  union {
    CLASS         clas;
    OBJT          nextObject;
  }               pointer;
  Int             refCount;
#ifdef ALIGN64
  Int             duncel; /* Filler to align contents on 64-bit boundary */
#endif
#if defined(SPARC)
  Quad            contents[1];
#else
  Int             contents[1];  
#endif
};
typedef struct OBJT_s OBJT_t;

 /*** User Functions ***/
extern CLASS  CLASS_create(IMemory * memory, UInt numObjects, UInt sizeContents, void (* fcn)(OBJT obj, Ptr env), Ptr env);
extern Bool   CLASS_destroy(CLASS clas, void (* fcn)(OBJT obj, Ptr env), Ptr env);
extern UInt   CLASS_free(CLASS clas);
extern OBJT   OBJT_new(CLASS clas);
extern OBJT   OBJT_clone(OBJT object);
extern OBJT   OBJT_copy(OBJT object);
extern void   OBJT_free(OBJT object);

 /*** User Macros ***/
#define CLASS_setNewFcn(C,F)       ((C)->newFcn = (F))
#define CLASS_setFreeFcn(C,F)      ((C)->freeFcn = (F))
#define CLASS_setEnvironment(C,P)  ((C)->environment = (P))
#define CLASS_size(C)              ((C)->numObjects)
#define CLASS_objectSize(C)        ((C)->objectSize)
#define OBJT_size(O)               ((O)->pointer.clas->objectSize)
#define OBJT_contents(O)           ((O)->contents)
#define OBJT_class(O)              ((O)->pointer.clas)
#define OBJT_offset(T,F)           ((UInt)&(((T)NULL)->F))

#ifdef __cplusplus
}
#endif

#endif  /* _CLASS_H_ */
