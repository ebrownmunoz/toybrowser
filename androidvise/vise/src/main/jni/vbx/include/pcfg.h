/* pcfg.h - command line and configuration file processing
 *---------------------------------------------------------------------------*/

#ifndef _PCFG_
#define _PCFG_

#include <sys/types.h>
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PCFG_AssignmentOperator       "="
#define PCFG_CommentCharacter         ';'               /* The character that introduces comments in configuration files */
#define PCFG_NameTerminator           ':'               /* The character that terminates names in config files */

#define PCFG_DefaultTempDir           "/tmp"            /* The default repository for temporary files */
#define PCFG_ConfigFileEnv            "VOXWARE_PCFG"    /* The name of the default environment variable containing the config file name */
#define PCFG_ListSeparator            ", "              /* A string to separate strings in printouts of string lists */

#define PCFG_DefaultQuoteCharacters   "\"\'"            /* A list of the characters that can optionally delimit value fields in a config file */
#define PCFG_DefaultEscapeCharacter   '\\'              /* A character to escape quotes in quoted strings in a config file */

#define PCFG_DefaultNameLabel         "<name>"          /* The default label for the parameter name field in printouts */
#define PCFG_DefaultTypeLabel         "<type>"          /* The default label for the parameter type field in printouts */
#define PCFG_DefaultOptionLabel       "<option>"        /* The default label for the parameter command line option field in printouts */
#define PCFG_DefaultValueLabel        "<value>"         /* The default label for the parameter value field in printouts */
#define PCFG_DefaultDescriptionLabel  "<description>"   /* The default label for the parameter description field in printouts */
#define PCFG_DefaultPrefix            "  "              /* The default lead-in to a printout of parameters */
#define PCFG_DefaultEquals            " = "             /* The default string to show equality between parameters and their values in printouts */
#define PCFG_DefaultSeparator         "\n  "            /* The default separator between parameters in a parameter printout */

#define PCFG_DefaultTimeout           0                 /* The default maximum time allowed to establish a THTTP connection */

#define PCFG_Env                      '$'               /* The character that introduces an environment variable reference */
#define PCFG_Bra                      '{'               /* The (optional) start of an environment variable name (immediately following an env char) */
#define PCFG_Ket                      '}'               /* The end of an environment variable name (obligatory if the name started with a PCFG_Bra) */

#define PCFG_StringBufferLength       2048
#define PCFG_MaxNameLength            64
#define PCFG_MaxNumberHelps           64
#define PCFG_MaxNumberUnknowns        64

 /* PCFG types */
#define PCFG_TypeSize   8
#define PCFG_Type       0xff                            /* The portion of the type field containing the type index */

#define PCFG_NOTYPEP    0
#define PCFG_CHARP      (PCFG_NOTYPEP + 1)
#define PCFG_BYTEP      (PCFG_CHARP + 1)
#define PCFG_SHORTP     (PCFG_BYTEP + 1)
#define PCFG_INTP       (PCFG_SHORTP + 1)
#define PCFG_LONGP      (PCFG_INTP + 1)
#define PCFG_FLOATP     (PCFG_LONGP + 1)
#define PCFG_DOUBLEP    (PCFG_FLOATP + 1)
#define PCFG_STRP       (PCFG_DOUBLEP + 1)
#define PCFG_DATEP      (PCFG_STRP + 1)
#define PCFG_BOOLP      (PCFG_DATEP + 1)
#define PCFG_TRUEP      (PCFG_BOOLP + 1)                /* Legacy type, equivalent to BOOLP (which defaults to FALSE) */
#define PCFG_FALSEP     (PCFG_TRUEP + 1)                /* Legacy type, equivalent to BOOLP initialized to TRUE */
#define PCFG_HELPP      (PCFG_FALSEP + 1)
#define PCFG_NUMTYPES   (PCFG_HELPP + 1)

#define PCFG_isValidType(T)   (PCFG_NOTYPEP < (T & PCFG_Type) && (T & PCFG_Type) < PCFG_NUMTYPES)
#define PCFG_isIntegerType(T) (PCFG_BYTEP <= (T & PCFG_Type) && (T & PCFG_Type) <= PCFG_LONGP)
#define PCFG_isFloatType(T)   ((T & PCFG_Type) == PCFG_FLOATP || (T & PCFG_Type) == PCFG_DOUBLEP)
#define PCFG_isNumberType(T)  (PCFG_isIntegerType(T) || PCFG_isFloatType(T))

 /* PCFG Type Names */
extern const Char * PCFG_TypeNames[];

 /* A six-bit subfield specifying the base of the number system to use when displaying the parameter */
#define PCFG_BaseOffset PCFG_TypeSize
#define PCFG_BaseSize   6
#define PCFG_Base       (0x3f << PCFG_BaseOffset)
#define PCFG_Octal      (0x08 << PCFG_BaseOffset)
#define PCFG_Decimal    (0x0a << PCFG_BaseOffset)
#define PCFG_Hex        (0x10 << PCFG_BaseOffset)

 /* A one-bit subfield specifying whether the parameter is hidden */
#define PCFG_VeilOffset (PCFG_BaseSize + PCFG_BaseOffset)
#define PCFG_VeilSize   1
#define PCFG_Veil       (0x01 << PCFG_VeilOffset)

 /* A one-bit subfield specifying whether the parameter is a list */
#define PCFG_ListOffset  (PCFG_VeilSize + PCFG_VeilOffset)
#define PCFG_ListSize    1
#define PCFG_List        (0x01 << PCFG_ListOffset)

 /* A one-bit subfield specifying whether the parameter is modifiable */
#define PCFG_FixedOffset (PCFG_ListSize + PCFG_ListOffset)
#define PCFG_FixedSize   1
#define PCFG_Fixed       (0x01 << PCFG_FixedOffset)

 /* A PCFG Parameter */
typedef struct PCFG_Parameter_s * PCFG_Parameter;

 /* A PCFG_Parameter_Value */
typedef union PCFG_Parameter_Value_u * PCFG_Parameter_Value;

 /* A PCFG_Parameter_Evaluator */
typedef void (* PCFG_Parameter_Evaluator)(PCFG_Parameter, PCFG_Parameter_Value, Bool);

 /* The default PCFG_Parameter_Evaluators */
extern void           PCFG_Parameter_Evaluator_evaluateScalar(PCFG_Parameter parameter, PCFG_Parameter_Value value, Bool initial);
extern void           PCFG_Parameter_Evaluator_evaluateList(PCFG_Parameter parameter, PCFG_Parameter_Value value, Bool initial);

 /* A PCFG */
typedef struct PCFG_s * PCFG;

 /* Public PCFG_Parameter_Value Methods */
extern void           PCFG_Parameter_Value_free(PCFG_Parameter_Value value, Int type);

 /* Public PCFG_Parameter Methods */
extern PCFG_Parameter PCFG_Parameter_new(Char * name, Int type, Char * description, Char * option, Char * value, PCFG_Parameter_Evaluator evaluator);
extern void           PCFG_Parameter_free(PCFG_Parameter parameter);
extern void           PCFG_Parameter_freeAll(PCFG_Parameter parameter[]);
extern void           PCFG_Parameter_setEvaluator(PCFG_Parameter parameter, PCFG_Parameter_Evaluator evaluator);
extern PCFG_Parameter_Evaluator PCFG_Parameter_evaluator(PCFG_Parameter parameter);
extern Char *         PCFG_Parameter_name(PCFG_Parameter parameter);
extern Int            PCFG_Parameter_type(PCFG_Parameter parameter);
extern Bool           PCFG_Parameter_veiled(PCFG_Parameter parameter);
extern Bool           PCFG_Parameter_isList(PCFG_Parameter parameter);
extern Bool           PCFG_Parameter_fixed(PCFG_Parameter parameter);
extern Int            PCFG_Parameter_base(PCFG_Parameter parameter);
extern Char *         PCFG_Parameter_option(PCFG_Parameter parameter);
extern Char *         PCFG_Parameter_description(PCFG_Parameter parameter);
extern void           PCFG_Parameter_setConfigured(PCFG_Parameter parameter, Bool configured);
extern Bool           PCFG_Parameter_configured(PCFG_Parameter parameter);
extern void           PCFG_Parameter_devalue(PCFG_Parameter parameter);
extern Int            PCFG_Parameter_numValues(PCFG_Parameter parameter);
extern Int            PCFG_Parameter_numValuesAvailable(PCFG_Parameter parameter);
extern Int            PCFG_Parameter_rewind(PCFG_Parameter parameter);
extern PCFG_Parameter_Value PCFG_Parameter_value(PCFG_Parameter parameter);
extern Char *         PCFG_Parameter_stringValue(PCFG_Parameter parameter);
extern Char *         PCFG_Parameter_stringListValue(PCFG_Parameter parameter);
extern Char           PCFG_Parameter_charValue(PCFG_Parameter parameter);
extern Char           PCFG_Parameter_byteValue(PCFG_Parameter parameter);
extern Short          PCFG_Parameter_shortValue(PCFG_Parameter parameter);
extern Int            PCFG_Parameter_intValue(PCFG_Parameter parameter);
extern Quad           PCFG_Parameter_longValue(PCFG_Parameter parameter);
extern Float          PCFG_Parameter_floatValue(PCFG_Parameter parameter);
extern Double         PCFG_Parameter_doubleValue(PCFG_Parameter parameter);
extern Bool           PCFG_Parameter_booleanValue(PCFG_Parameter parameter);
extern Quad           PCFG_Parameter_dateValue(PCFG_Parameter parameter);
extern void           PCFG_Parameter_setValue(PCFG_Parameter parameter, Char * string);
extern void           PCFG_Parameter_setInitialValue(PCFG_Parameter parameter, Char * string);
extern Char *         PCFG_Parameter_appendToString(PCFG_Parameter parameter, Char * string, Char * fieldSeparator,
                                                    Bool showName, Int nameFieldLength, Bool showOption, Int optionFieldLength,
                                                    Char * equals, Bool showValue, Int valueFieldLength, Bool showDescription);
extern Char *         PCFG_Parameter_toString(PCFG_Parameter parameter);

 /* Public PCFG Methods */
extern PCFG           PCFG_new(PCFG_Parameter parameters[]);
extern void           PCFG_free(PCFG pcfg);
extern void           PCFG_setLabels(PCFG pcfg, Char * nameLabel, Char * typeLabel, Char * optionLabel, Char * valueLabel, Char * descriptionLabel);
extern void           PCFG_setDelimiters(PCFG pcfg, Char * prefix, Char * equals, Char * separator);
extern void           PCFG_setQuotes(PCFG pcfg, Char * quoteCharacters);
extern Char *         PCFG_getQuotes(PCFG pcfg);
extern void           PCFG_setEscape(PCFG pcfg, Char escapeCharacter);
extern Char           PCFG_getEscape(PCFG pcfg);
extern Bool           PCFG_setTempDir(PCFG pcfg, Char * path);
extern void           PCFG_setTimeout(PCFG pcfg, Int timeout);
extern Int            PCFG_timeout(PCFG pcfg);
extern Int            PCFG_maxNameLength(PCFG pcfg);
extern Int            PCFG_maxOptionLength(PCFG pcfg);
extern void           PCFG_setMaxValueFieldLength(PCFG pcfg, Int maxValueFieldLength);
extern Int            PCFG_maxValueLength(PCFG pcfg, Bool includeVeiled, Bool optionsOnly);
extern void           PCFG_setMaxValueLengthLimit(PCFG pcfg);
extern PCFG_Parameter PCFG_helpParameter(PCFG pcfg, Int argc, Char * argv[]);
extern void           PCFG_scanCommandLine(PCFG pcfg, Int argc, Char * argv[]);
extern void           PCFG_scanPartOfCommandLine(PCFG pcfg, Int argc, Char * argv[], Int ignoreFirst, Int ignoreLast);
extern FILE *         PCFG_openConfigFile(PCFG pcfg, Char * name, Char ** tempDir);
extern FILE *         PCFG_openConfigSource(PCFG pcfg, Char * name, Char ** tempDir);
extern Bool           PCFG_scanConfigFileSelectively(PCFG pcfg, FILE * file);
extern Bool           PCFG_scanConfigFile(PCFG pcfg, FILE * file);
extern PCFG_Parameter PCFG_parameter(PCFG pcfg, Char * name);
extern Bool           PCFG_setConfigured(PCFG pcfg, Char * name, Bool configured);
extern Bool           PCFG_configured(PCFG pcfg, Char * name);
extern Int            PCFG_numUnknownArgs(PCFG pcfg);
extern Char *         PCFG_unknownArg(PCFG pcfg, Int n);
extern Int            PCFG_indexOfUnknownArg(PCFG pcfg, Int n);
extern Char *         PCFG_unknownArgsToDefaultString(PCFG pcfg);
extern Char *         PCFG_unknownArgsToString(PCFG pcfg, Bool showIndices, Char * prefix, Char * separator);
extern void           PCFG_printOptions(PCFG pcfg, FILE * stream, Bool label, Bool showValues, Bool showDescriptions, Bool showVeiled);
extern void           PCFG_printParameters(PCFG pcfg, FILE * stream, Bool label,
                                           Bool showNames, Bool showOptions, Bool showValues, Bool showDescriptions, Bool showVeiled);
extern Char *         PCFG_toString(PCFG pcfg, Char * prefix, Char * separator);
extern void           PCFG_tokens(PCFG pcfg, Char * string, Char * tokens[], Int * length);

  /* Public PCFG Class Methods */
extern Bool           PCFG_startsWith(Char * string, Char * prefix);
extern Int            PCFG_matchLength(Char * string1, Char * string2);
extern Char *         PCFG_trim(Char * string);
extern Bool           PCFG_equals(Char * string1, Char * string2);
extern Bool           PCFG_assignment(Char * assignment, Char ** lhs, Char * operator, Char ** rhs);

#ifdef __cplusplus
}
#endif

#endif /* _PCFG_ */
