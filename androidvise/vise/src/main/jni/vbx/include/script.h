#ifndef _SCRIPT_H_
#define _SCRIPT_H_

#include "types.h"
#include "vfile.h"
#include "hyp.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct script_s *SCRIPT;

SCRIPT     SCRIPT_load(VFILE recFile, UInt vocabId);
void       SCRIPT_annihilate(SCRIPT script);
HYP        SCRIPT_entry(SCRIPT script, VFILE recFile, UInt idx);
UInt       SCRIPT_entryCnt(SCRIPT script);

#ifdef __cplusplus
}
#endif
#endif  /* _SCRIPT_H_ */
