/* llist.c - linked list facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _LLIST_H_
#define _LLIST_H_

#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* A Unique Key */
typedef struct llist_s * LLIST;

/* Public Methods */
extern LLIST  LLIST_create(Ptr contents);
extern void   LLIST_destroy(LLIST llist, void (* fcn)(Ptr contents, Ptr env), Ptr env);
extern LLIST  LLIST_append(LLIST llist, Ptr contents);
extern LLIST  LLIST_prepend(LLIST llist, Ptr contents);
extern LLIST  LLIST_first(LLIST llist);
extern LLIST  LLIST_last(LLIST llist);
extern LLIST  LLIST_next(LLIST llist);
extern LLIST  LLIST_previous(LLIST llist);
extern LLIST  LLIST_remove(LLIST llist);
extern Ptr    LLIST_contents(LLIST llist);
extern Ptr    LLIST_replaceContents(LLIST llist, Ptr contents);
extern LLIST  LLIST_find(LLIST llist, Ptr contents);
extern UInt   LLIST_length(LLIST llist);

#ifdef DEBUG
extern void   LLIST_dump(LLIST llist);
#endif

#ifdef __cplusplus
}
#endif
#endif /* _LLIST_H_ */
