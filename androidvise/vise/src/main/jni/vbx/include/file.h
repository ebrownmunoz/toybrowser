/* file.h - OS-independent file and directory access
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _FILE_H_
#define _FILE_H_

#include <stdio.h>
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

#define PATH_WindowsSeparator '\\'
#define PATH_NormalSeparator  '/'

#ifdef WIN32
#define PATH_Separator PATH_WindowsSeparator
#else
#define PATH_Separator PATH_NormalSeparator
#endif

 /* Determine whether a Char is a path separator */
#define PATH_isSeparator(C) ((C) == PATH_WindowsSeparator || (C) == PATH_NormalSeparator)
 /* Determine whether a Char* starts with a path separator */
#define PATH_isAbsolute(P)  ((P) != NULL && strlen(P) > 0 && PATH_isSeparator(*(P)))

 /* Allowed access arguments to FILEMAP_create() and values returned by FILEMAP_mode() */
#define FILEMAP_NOACCESS   0
#define FILEMAP_READONLY   1
#define FILEMAP_READWRITE  2

typedef struct filemap_s * FILEMAP;
typedef struct dirlist_s * DIRLIST;

extern Char *   PATH_localize(Char * path);
extern Char *   PATH_generalize(Char * path);
extern Char *   PATH_catToString(Char * string, Char * root, Char * extension);

extern Bool     FILE_appendStream(Char *srcName, FILE *targetFP);
extern Bool     FILE_copy(Char * oldName, Char * newName, Bool replace);
extern Bool     FILE_delete(Char * fileName);
extern Bool     FILE_exists(Char * fileName);
extern Bool     FILE_length(Char * fileName, UInt *length);
extern Bool     FILE_rename(Char * oldName, Char * newName, Bool replace);
extern Bool     FILE_write(const Char *fileName, const Char *mode, const Ptr data, UInt dataSize);

extern FILEMAP  FILEMAP_create(Char * fileName, Int access);
extern Bool     FILEMAP_synchronize(FILEMAP map, Char * region, UInt length);
extern Bool     FILEMAP_annihilate(FILEMAP map);
extern Char *   FILEMAP_map(FILEMAP map);
extern UInt     FILEMAP_length(FILEMAP map);
extern Char *   FILEMAP_name(FILEMAP map);
extern Int      FILEMAP_mode(FILEMAP map);

extern Bool     DIR_create(Char * dirName);
extern Bool     DIR_delete(Char * dirName);
extern Bool     DIR_makeAllInPath(Char * path);
extern Bool     DIR_copy(Char * oldDir, Char * newDir);
extern DIRLIST  DIRLIST_create(Char * dirName, Char * nameTemplate);
extern void     DIRLIST_annihilate(DIRLIST dirList);
extern UInt     DIRLIST_numEntries(DIRLIST dirList);
extern Char *   DIRLIST_fileNames(DIRLIST dirList, UInt * sizePtr);

#ifdef __cplusplus
}
#endif
#endif              /* ndef _FILE_H_ */
