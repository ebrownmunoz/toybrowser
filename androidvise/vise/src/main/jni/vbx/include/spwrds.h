/* spwrds.h - VISE Special Words
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _SPWRDS_H_
#define _SPWRDS_H_

#include "vocab.h"
#ifdef __cplusplus
extern "C" {
#endif

/* The map of the silence vocabulary */
extern vocmap_t silenceMap[];

/* N.B. If this is changed, spwrds.c must be changed correspondingly */
#define NUM_MULTISILENCES   9

/* The word Ids for silence words */
#define FIRST_MULTISILENCE  32497
#define SHORT_SILENCE       32509
#define SHORT_ADAPTIVE_SIL  32487
#define LONG_SILENCE        32510
#define LONG_ADAPTIVE_SIL   32488

/* The word Id of the joker wildcard */
#define JOKER_WID           32511

/* Word Ids for the special wildcards */
#define TRAINMULTIWC_WID    32512
#define ENROLLINITWC_WID    32513
#define ENROLLBODYWC_WID    32514

/* Special word classes */
#define SILENCE_CLASS       0
#define JOKER_CLASS         SILENCE_CLASS

#ifdef __cplusplus
}
#endif
#endif /* _SPWRDS_H_ */
