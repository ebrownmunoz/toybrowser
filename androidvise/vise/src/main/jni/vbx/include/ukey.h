/* ukey.h - unique key facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: ukey.h[1.0] Mon Apr  7 23:53:55 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _UKEY_H_
#define _UKEY_H_

#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* A Unique Key */
typedef struct ukey_s * UKEY;

/* A Pool of Unique Keys */
typedef struct ukeypool_s * UKEYPOOL;

/* A Key Registry */
typedef Quad UKEYREG, * UKEYREGP;

/* Public Methods */
extern UKEYPOOL UKEYPOOL_create(UInt initialSize);
extern void     UKEYPOOL_annihilate(UKEYPOOL pool);
extern UInt     UKEYPOOL_numKeysLeft(UKEYPOOL pool);
extern void     UKEYREG_reset(UKEYREGP registry);
extern UKEY     UKEY_reserve(UKEYPOOL pool);
extern void     UKEY_release(UKEY key);
extern void     UKEY_register(UKEYREGP registry, UKEY key);
extern void     UKEY_unregister(UKEYREGP registry, UKEY key);
extern Bool     UKEY_registered(UKEYREGP registry, UKEY key);

#ifdef DEBUG
extern void     UKEYPOOL_dump(UKEYPOOL pool);
#endif

#ifdef __cplusplus
}
#endif
#endif /* _UKEY_H_ */
