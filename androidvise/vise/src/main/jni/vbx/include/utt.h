/* utt.h - utterance acquisition/processing for the trainers
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 * Utterance structure and i/o routines for obtaining utterances
 *---------------------------------------------------------------------------*/

#ifndef _UTT_H_
#define _UTT_H_
#include <stdio.h>
#include "types.h"
#include "class.h"

#define   HEADERS   0
#define   TRANSFILE 1

typedef struct utt {
  Float  *cep;                          /* cepstrum for the utterance */
  Int     floatCnt;                     /* float count for cepstrum */
  Int     vQFrmCnt;                     /* VQ frame count for utterance */
  Char   *fname;                        /* utterance file name */
  Char   *trans;                        /* null terminated transcription */
  Ptr     vqobj;                        /* derived features & VQ results */
} utt_t, *UTT;

OBJT  UTT_create(void);
void  UTT_destroy(OBJT obj);
void  UTT_freeContents(OBJT obj);
Bool  UTT_init(Char *cepExt, Char *transFile);
void  UTT_load(OBJT obj, Char *file);
Char *UTT_getTransFrmFile(FILE *trfile, Char *fname);

#endif  /* _UTT_H_ */
