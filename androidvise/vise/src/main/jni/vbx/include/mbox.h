/* mbox.h - prioritized mailbox facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *
 * Modification history:
 * May  1997:  Rewritten to support priorities (based on PMBOX, created 1-23-97 by DCV)
 *---------------------------------------------------------------------------*/

#ifndef _MBOX_H_
#define _MBOX_H_

#include "pthr.h"
#include "critsect.h"
#include "limits.h"
#include "types.h"
#include "class.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MBOX_MINPRIORITY  UINT_MAX
#define MBOX_MAXPRIORITY  0

 /* Mailbox Attributes */
typedef struct MBOX_Att_s {
  IMemory   * memory;
  Uns         growQuantum;
  Uns         maxMessages;
  Uns         nPriorities;
} MBOX_Att_t;

typedef struct MBOX_Att_s * MBOX_Att;
typedef struct MBOX_s     * MBOX;

 /* Basic non-blocking single-thread mailbox operations */
extern MBOX_Att MBOX_createAttr(IMemory * memory, Uns growQuantum, Uns maxMessages, Uns nPriorities);
extern void     MBOX_destroyAttr(MBOX_Att attributes);
extern MBOX     MBOX_create(IMemory * memory, MBOX_Att attributes);
extern void     MBOX_destroy(MBOX mailbox);
extern Bool     MBOX_insertNow(MBOX mailbox, OBJT message);
extern Bool     MBOX_priorityInsertNow(MBOX mailbox, OBJT message, Uns priority);
extern OBJT     MBOX_removeNow(MBOX mailbox);
extern Uns      MBOX_numobj(MBOX mailbox);

 /* Synchronized multi-thread extensions */
extern Bool     MBOX_insert(MBOX mailbox, OBJT message);
extern Bool     MBOX_priorityInsert(MBOX mailbox, OBJT message, Uns priority);
extern Bool     MBOX_insertWithTimeout(MBOX mailbox, OBJT message, TIMESPEC * timeout);
extern Bool     MBOX_priorityInsertWithTimeout(MBOX mailbox, OBJT message, Uns priority, TIMESPEC * timeout);
extern OBJT     MBOX_remove(MBOX mailbox);
extern OBJT     MBOX_removeWithTimeout(MBOX mailbox, TIMESPEC * timeout);

#define MBOX_attrIMemory(ATTR)              ((ATTR)->memory)
#define MBOX_attrSetIMemory(ATTR, IMEM)     ((ATTR)->memory = (IMEM))
#define MBOX_attrGrowQuantum(ATTR)          ((ATTR)->growQuantum)
#define MBOX_attrSetGrowQuantum(ATTR, GQUA) ((ATTR)->growQuantum = (GQUA))
#define MBOX_attrMaxMessages(ATTR)          ((ATTR)->maxMessages)
#define MBOX_attrSetMaxMessages(ATTR, MAXM) ((ATTR)->maxMessages = (MAXM))
#define MBOX_attrNPriorities(ATTR)          ((ATTR)->nPriorities)
#define MBOX_attrSetNPriorities(ATTR, NPRI) ((ATTR)->nPriorities = (NPRI))

#ifdef __cplusplus
}
#endif
#endif
