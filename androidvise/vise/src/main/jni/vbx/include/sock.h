/*
 * sock.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _SOCK_H_
#define _SOCK_H_

#ifdef __cplusplus
extern "C" {
#endif

#define FAILURE -1
#define UNIX     0
#define TCP      1
#define MAXSOCKBUF 50000

/* Spec 1170 says in_addr_t is unsigned integral of exactly 32 bits */
#ifdef WIN32
typedef	unsigned int	in_addr_t;
#endif

typedef struct sock_s {
  Int type;                /* socket type: TCP or UNIX */
  Int bufSize;             /* size in bytes of socket buffer */
  UShort *port;            /* pointer to port number for TCP socket */
  Char *fname;             /* file name for UNIX socket */
} sock_t, *SOCK;

Int  SOCK_server(SOCK sock);
Int  SOCK_client(SOCK sock, Char *server);
Bool SOCK_close(Int sd, SOCK sock);
Bool SOCK_send(Int sd, Ptr buf, Int size, Bool block);
Bool SOCK_peek(Int sd, Ptr buf, Int size);
Bool SOCK_recv(Int sd, Ptr buf, Int size, Bool block);

#ifdef __cplusplus
}
#endif
#endif /* _SOCK_H_ */
