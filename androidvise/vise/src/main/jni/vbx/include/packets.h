/*
 * packets.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _PACKETS_H_
#define _PACKETS_H_

#ifdef RTS
#include "rtscfg.h"
#define NUM_COEFF    13
typedef  Float  Val;
#else
#include "mbox.h"
#include "cep.h"
#define NUM_COEFF    CEP_VECLEN
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_SAMPLES  480   /* 10 ms frame rate w/ 48 kHz sampling freq */

typedef Int MsgType;

/* the haphazard order of the entries in this
   enumeration is required for back compatability */

enum packets {
  ACCEPT,                 /* 0  Acceptance */
  BWD_RESULTS,            /* 1  Backward Result */
  CEP_FRAME,              /* 2  CEP Frame */
  CFG_PARAM,              /* 3  */                    /* Obsolete */
  FWD_CONF,               /* 4  */                    /* Obsolete */
  FWD_RESULTS,            /* 5  Forward Result */
  NBEST_CHOICE,           /* 6  */                    /* Obsolete */
  NBEST_RESULTS,          /* 7  Nbest Results */
  REC_LAUNCH,             /* 8  Recognition Launch */
  STRING,                 /* 9  Character String */
  STAT_MSG,               /* 10 Status Message */
  CEPWAV_FRAME,           /* 11 CEPWAV Frame */
  RESP_TIME,              /* 12 Response Time */
  DSP_CFG,                /* 13 DSP Configuration */
  DSP_FILT,               /* 14 DSP Mel Filter Config */
  DSP_ERR,                /* 15 DSP Error Data */
  SAVE_COUNT,             /* 16 Saved File Count */
  WAV_FRAME,              /* 17 WAV Frame */
  STEREO_WAV_FRAME,       /* 18 Stereo WAV Frame */
  DSP_GA,                 /* 19 DSP Gains & Attenuations */
  DSP_UF,                 /* 20 DSP Utterance Framing Cfg */
  SAPI_SERVICE            /* 21 SAPI Activation Request */
};

enum mboxes {
  MCT,                /* 0  FBS Main Control Thread mailbox */
  MSGOUT,             /* 1  FBS to HUB mailbox */
  DATAIN,             /* 2  FBS data mailbox */
  PRINT,              /* 3  FBS print mailbox */
  PRNMSG,             /* 4  HUB print mailbox */
  DATABUF,            /* 5  */                    /* Obsolete */
  DSP,                /* 6  DSP mailbox */
  FBSSOC_C,           /* 7  */                    /* Obsolete */
  FBSSOC_D,           /* 8  */                    /* Obsolete */
  GUI,                /* 9  GUI mailbox */
  RIT,                /* 10 HUB Recognition Interface Thread mailbox */
  RDDATA,             /* 11 HUB Read Data mailbox */
  MBOX_COUNT          /* 12 Total number of mailboxes */
};

#define HDR_LEN       6
#define STRLEN(s)     (strlen(s)+HDR_LEN)

#define MIN_LEN       8       /* minimum packet length */

#define CEP_LEN       62      /* HDR_LEN + sizeof(cep_t) */
#define STAT_LEN      8       /* HDR_LEN + sizeof(Short) */
#define CMD_LEN       8       /* HDR_LEN + ?? */
#define TIME_LEN      10      /* HDR_LEN + sizeof(Float) */
#define COUNT_LEN     10      /* HDR_LEN + sizeof(Int) */

#define DSP_CFG_LEN   118     /* HDR_LEN + sizeof(dspCfg_t) */
#define DSP_GA_LEN    26      /* HDR_LEN + sizeof(gainAttnCfg_t) */
#define DSP_UF_LEN    34      /* HDR_LEN + sizeof(uttFrm_t) */
#define DSP_ERR_LEN   10      /* HDR_LEN + sizeof(UInt) */

#define MAX_PACK_LEN  2000 /* 48kHz stereo WAV packets @ 100 frames/sec */
                              /*   ==> 1930 bytes/packet */

typedef struct acc_s {
  Bool accept;                /* TRUE iff result accepted */
  Int  nbest;                 /* Index of accepted result (FWD = 0) */
  Bool mod;                   /* TRUE iff result text has been edited */
  Char mtext[256];            /* Edited result string */
} acc_t, *ACC;
  
typedef struct cep_s {
  Int  seqNum;
  Val  data[NUM_COEFF];
} cep_t, *CEP;

typedef struct wav_s {
  Int  seqNum;
  Short data[1];
} wav_t, *WAV;

typedef struct stWav_s {
  Int seqNum;
  struct {
    Short left;
    Short right;
  } data [1];
} stWav_t, *STWAV;

typedef struct cepwav_s {
  Int seqNum;
  Val cep[NUM_COEFF];
  Short wav[1];
} cepwav_t, *CEPWAV;

#ifndef RTS
typedef struct ServiceReq_s {
  Ptr  reqObjPtr;             /* Service request object pointer */
  MBOX returnAddr;            /* Return address MBOX */
} ServiceReq_t, *SERVICEREQ;
#endif

/* for back compatibility with old packet stuff */

#define DSPSOC DSP

typedef acc_t accept_s;
typedef ACC accept_t;
typedef cep_t cepFrm_s;
typedef CEP cepFrm_t;

#ifdef __cplusplus
}
#endif
#endif /* _PACKETS_H_ */
