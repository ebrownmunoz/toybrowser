/* sockmap.h - defines the system-wide socket topology for our recognizer
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: sockmap.h[1.0] Mon Apr  7 23:53:49 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _SOCKMAP_H_
#define _SOCKMAP_H_
#include "types.h"
#include "packid.h"

#define  _SOCKADDR_LEN

enum sockets {DSP_H, HUB_D, HUB_FD, HUB_FC, HUB_G, FBS_HD, FBS_HC, GUI_H};

#define  MAXPIDS  10                    /* max packet id's */
#define  MAXSOCK  10                    /* max # sockets in the system */
#define  NONE    -1

Int   sox[MAXSOCK];                     /* socket array for system-wide use */
Int  *sockmap;                          /* maps packet id -> socket */

static UShort Ports[] = {0x0aaa, 0x0aab, 0x0aac, 0x0aad, 0x0aae,
                         0x0aaf, 0x0aba, 0x0abb, 0x0abc, 0x0abd};

#endif  /* _SOCKMAP_H_ */
