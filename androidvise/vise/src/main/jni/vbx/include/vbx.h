/* vbx.h - provides a humane system call error handling mechanism
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _VBX_H_
#define _VBX_H_

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include "types.h"
#if defined(__cplusplus)
extern "C" {
#endif

#define VBX_MAX_PRINT_LINE      1024
#define VBX_DFLT_PRINT_CEILING  0

/* Predefined print priorities for VBX_printIf() */
#define VBX_MAX_PRIO    0
#define VBX_HIGH_PRIO   2
#define VBX_HIMID_PRIO  3
#define VBX_MID_PRIO    4
#define VBX_LOMID_PRIO  5
#define VBX_LOW_PRIO    6
#define VBX_MIN_PRIO    8

extern void   VBX_init();
extern void   VBX_initStdOut();
extern void   VBX_print(const Char * format, ...);
extern void   VBX_printIf(Int priority, const Char * format, ...);
extern void   VBX_vprint(const Char * format, va_list args);
extern void   VBX_vprintIf(Int priority, const Char * format, va_list args);
extern void   VBX_setPrintFcn(void (* printFcn)(const Char * string));
extern void   VBX_setPrintCeiling(Int ceiling);
extern void   VBX_setStdOut(const Char * fileName, const Char * mode);
extern FILE * VBX_getStdOut();
extern FILE * VBX_sysOpen(const Char *path, const Char *mode);
extern Bool   VBX_sysRootDir(Char *dirBuf, Int *dirBufSize);

extern Char * _VBX_file_name(FILE * fs);
extern FILE * _VBX_fopen(const Char * fileName, const Char * mode, const Char * file, Int line);
extern FILE * _VBX_popen(const Char * fileName, const Char * mode, const Char * file, Int line);
extern FILE * _VBX_fopenp(const Char * dirl, const Char * fileName, const Char * mode, const Char * file, Int line);
extern Int    _VBX_fread(Char * ptr, Int size, Int cnt, FILE * stream, const Char * file, Int line);
extern void * _VBX_malloc(Int size, const Char * file, Int line);
extern void * _VBX_realloc(void * ptr, Int size, const Char * file, Int line);
extern void * _VBX_calloc(Int cnt, Int size, const Char * file, Int line);
extern void * _VBX_recalloc(void * ptr, Int cnt, Int size, const Char * file, Int line);
extern void * _VBX_2dcalloc(Int rcnt, Int ccnt, Int size, const Char * file, Int line);
extern void * _VBX_3dcalloc(Int lcnt, Int rcnt, Int ccnt, Int size, const Char * file, Int line);
extern Char * _VBX_salloc(const Char * str, const Char * file, Int line);
extern void   _VBX_exception(const Char * file, Int line, Int severity, const Char * format, ...);

#define VBX_fopen(name,mode)              _VBX_fopen((name),(mode),__FILE__,__LINE__)
#define VBX_popen(name,mode)              _VBX_popen((name),(mode),__FILE__,__LINE__)
#define VBX_fopenp(dirl,name,mode)        _VBX_fopenp((dirl),(name),(mode),__FILE__,__LINE__)
#define VBX_fread(ptr,size,cnt,stream)    _VBX_fread((ptr),(size),(cnt),(stream),__FILE__,__LINE__)
#define VBX_malloc(size)                  _VBX_malloc((size),__FILE__,__LINE__)
#define VBX_realloc(ptr,size)             _VBX_realloc((ptr),(size),__FILE__,__LINE__)
#define VBX_calloc(cnt,size)              _VBX_calloc((cnt),(size),__FILE__,__LINE__)
#define VBX_recalloc(ptr,cnt,size)        _VBX_recalloc((ptr),(cnt),(size),__FILE__,__LINE__)
#define VBX_2dcalloc(rcnt,ccnt,size)      _VBX_2dcalloc((rcnt),(ccnt),(size),__FILE__,__LINE__)
#define VBX_3dcalloc(lcnt,rcnt,ccnt,size) _VBX_3dcalloc((lcnt),(rcnt),(ccnt),(size),__FILE_,__LINE__)
#define VBX_salloc(str)                   _VBX_salloc((str),__FILE__,__LINE__)
#define VBX_free(ptr)                     if (ptr) { free(ptr); ptr = NULL; }

#define FATAL       4
#define SURVIVABLE  3
#define SEVERE      3
#define WARNING     2
#define INFORM      1

#define VBX_exception()   _VBX_exception(__FILE__,__LINE__,FATAL,"")
#define FATAL_BRA_        _VBX_exception(__FILE__,__LINE__,FATAL, 
#define SEVERE_BRA_       _VBX_exception(__FILE__,__LINE__,SEVERE, 
#define WARN_BRA_         _VBX_exception(__FILE__,__LINE__,WARNING,
#define INFO_BRA_         _VBX_exception(__FILE__,__LINE__,INFORM,
#define _KET      )
#define VBX_fatalIf(expr)   { Int v = expr; if (v) _VBX_exception(__FILE__,__LINE__,FATAL, "retval = %d; errno = %d", v, errno); }
#define VBX_errorIf(expr)   { Int v = expr; if (v) _VBX_exception(__FILE__,__LINE__,SEVERE, "retval = %d; errno = %d", v, errno); }
#define VBX_warningIf(expr) { Int v = expr; if (v) _VBX_exception(__FILE__,__LINE__,WARNING, "retval = %d; errno = %d", v, errno); }

#if defined(__cplusplus)
}
#endif
#endif /* _VBX_H_ */
