/* sox.h - Routines to handle creation and manipulation of sockets
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
#ifndef _SOX_H_
#define _SOX_H_

#include "pthr.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include "types.h"


#define SOX_clientAddr(s)   (s.clientAddr)
#define SOX_serverAddr(s)   (s.serverAddr)
#define SOX_fd(s)           (s.fd)

typedef struct sox_s {
  struct sockaddr_in serverAddr;
  struct sockaddr_in clientAddr;
  Int fd;
} sox_t, *SOX;


#ifdef __cplusplus
extern "C" {
#endif

Bool SOX_createTCP(SOX sox);
Bool SOX_close(SOX sox);
Int  SOX_configTCPClient(SOX sox, UInt server, Int serverPort);
Bool SOX_configTCPServer(SOX sox, Int serverPort, Int maxConn);
SOX  SOX_accept(SOX sox);
Bool SOX_connect(SOX sox, Int msec);
Bool SOX_getPeerName(SOX sox, struct sockaddr *sockaddr);
Bool SOX_isConnected(SOX sox);

#ifdef __cplusplus
}
#endif
#endif
