/* parcel.h -  Parcel generation and transmission
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
#ifndef _PARCEL_H_
#define _PARCEL_H_

#include "pthr.h"
#include "class.h"
#include "types.h"

#define  MAXIPLEN   (16)

/* Parcel status field codes */
#define PS_UNKNOWN        (0)
#define PS_INCOMINGMSG    (100)
#define PS_XMITSUCCESS    (101)
#define PS_XMITNETFAILURE (102)
#define PS_XMITOUTOFRANGE (103)
#define PS_XMITQOVERFLOW  (104)

enum parcel_types { CMD = 1, MSG };
enum cmd_parcels { ABORT = 1000, ACK, EOM, OAO };

typedef struct preamble_s {
  Int type;
  Int cmd;
  Int status;
  Int size;
} preamble_t, *PREAMBLE;

typedef struct address_s {
  UInt IP;
  Int  port;
} address_t, *ADDRESS;

typedef struct contents_t {
  Int   size;
  Char *data;
} contents_t, *CONTENTS;

typedef struct parcel_s {
  preamble_t preamble;
  address_t  dest;
  address_t  src;
  contents_t contents;
} parcel_t, *PARCEL;

typedef struct parcelBank_s {
  IMemory  allocator;
  CLASS    msgClass;
  CLASS    cmdClass;
} parcelBank_t, *PARCELBANK;

#ifdef __cplusplus
extern "C" {
#endif

PARCELBANK PARCEL_createBank(Int cmdParcelCnt, Int msgParcelCnt);
void       PARCEL_destroyBank(PARCELBANK bank);
PARCEL     PARCEL_new(PARCELBANK bank, Int parcelType);
void       PARCEL_free(PARCEL parcel);
PARCEL     PARCEL_recv(PARCELBANK bank, Int fd, Int msec);
Bool       PARCEL_send(Int fd, PARCEL parcel, Int msec);
Bool       PARCEL_sendCMD(Int fd, Int cmd, Int status, Int msec);
Bool       PARCEL_getDestIP(PARCEL parcel, Char *IP);
Bool       PARCEL_setDestIP(PARCEL parcel, Char *IP);
Bool       PARCEL_getSrcIP(PARCEL parcel, Char *IP);
Bool       PARCEL_setSrcIP(PARCEL parcel, Char *IP);

/* User Macros */
#define PARCEL_type(P)           ((P)->preamble.type)
#define PARCEL_cmd(P)            ((P)->preamble.cmd)
#define PARCEL_status(P)         ((P)->preamble.status)
#define PARCEL_size(P)           ((P)->preamble.size)
#define PARCEL_destIP(P)         ((P)->dest.IP)
#define PARCEL_destPort(P)       ((P)->dest.port)
#define PARCEL_data(P)           ((P)->contents.data)
#define PARCEL_dataSize(P)       ((P)->contents.size)
#define PARCEL_srcIP(P)          ((P)->src.IP)
#define PARCEL_srcPort(P)        ((P)->src.port)

#ifdef __cplusplus
}
#endif
#endif
