/* buff.h - circular, bi-directional buffer
 *----------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * HISTORY
 * 07-Dec-93 Created by Dave Vetter (dvetter@rad.verbex.com) from earlier
 *        code, also by Dave Vetter, originally written for the TMS320C30 DSP
 * 13-Sep-01 (dcv) Added BUF_readNow() and BUF_readnextNow()
 *
 * $__Header$
 *----------------------------------------------------------------------------*/

#ifndef _BUFF_H_
#define _BUFF_H_

#include "types.h"
#include "class.h"
#include "critsect.h"
#include "ifaces.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* Buffer Descriptor */
typedef struct BUFF_s * BUFF;

BUFF  BUF_create(IMemory * memory, size_t numElements);
void  BUF_destroy(BUFF buffer);
Int   BUF_extract(BUFF buffer, Ptr * extractBuf, SN firstSN, SN lastSN, UInt offset, Int objtBytes);
void  BUF_flush(BUFF buffer);
OBJT  BUF_readnext(BUFF buffer, SN * seqNumP);
OBJT  BUF_readnextNow(BUFF buffer, SN * seqNumP);
OBJT  BUF_read(BUFF buffer, SN * seqNumP);
OBJT  BUF_readNow(BUFF buffer, SN * seqNumP);
Bool  BUF_writenext(BUFF buffer, SN seqNum, OBJT object);
Bool  BUF_range(BUFF buffer, SN * firstSNP, SN * lastSNP);
Bool  BUF_setread(BUFF buffer, SN seqNum);

#ifdef __cplusplus
}
#endif
#endif  /* _BUFF_H_ */
