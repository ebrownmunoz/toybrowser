/* vlbldefs.h
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * Definitions of standard block ID's and related constants
 *
 * HISTORY
 * 07-Jul-96  Updated RRLee's old Windows version for use in the SAPI project
 * $Header: vlbldefs.h[1.2] Tue Jun  9 14:56:21 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef   _VLBLDEFS_H_
#define   _VLBLDEFS_H_

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Key Block Bits returned from GetLockAndMask */
#define   TR_KEYBIT_XV        0x00000001L
#define   TR_KEYBIT_SPEC2     0x00000002L
#define   TR_KEYBIT_SPEC3     0x00000004L
#define   TR_KEYBIT_SPEC31    0x00000008L
#define   TR_KEYBIT_VENDOR    0x00000010L

/* System Class:  These blocks exist in FILES ONLY and are not sent */
#define   TR_KEY                   100
#define   TR_VERSION               101

/* Application Class */
#define   TR_IDENT                 200
#define   TR_TEXT                  201
#define   TR_RESPONSE_TEMPLATE     202
#define   TR_CONTROL_TEMPLATE      203
#define   TR_SWITCH                204
#define   TR_PHONETICS             205
#define   TR_VPF                   206
#define   TR_DPF                   207
#define   TR_HELP                  208
#define   TR_KEY_MAP               209
#define   TR_DICT                  210

/* Vocabulary Class */
#define   TR_UPAT                  300
#define   TR_VOCABULARY_TRANS      301
#define   TR_SCRIPT                302
#define   TR_RECON                 303       /* never transferred */
#define   TR_CONTROL_PARSE_VOCAB   304
#define   TR_VOICE_PARSE_VOCAB     305
#define   TR_HOST_PARSE_VOCAB      306
#define   TR_DISPLAY_PARSE_VOCAB   307

/* Grammar Class */
#define   TR_GRAMMAR               400
#define   TR_GRAPH                 401
#define   TR_GRAMMAR_TRANS         402
#define   TR_CONTROL_PARSE         403
#define   TR_VOICE_PARSE           404
#define   TR_HOST_PARSE            405
#define   TR_DISPLAY_PARSE         406
#define   TR_FLM                   410
#define   TR_FUGRAM                411
#define   TR_FBGRAM                412



/* Generic Data Class */
#define   TR_JIN                   500
#define   TR_VISE                  501
#define   TR_RAM_SETUP             502
#define   TR_TASK                  503
#define   TR_SAMPLE                504
#define   TR_PHRASE                505
#define   TR_VSF                   506
#define   TR_SYSSETUP              508
#define   TR_INFO                  210


#ifdef __cplusplus
} 
#endif

#endif /* _VLBLDEFS_H */

