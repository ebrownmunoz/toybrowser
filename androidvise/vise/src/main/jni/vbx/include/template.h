/* template.h - Response template facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: template.h[1.0] Tue Jun  9 11:32:24 1998 dvetter@titan saved $
 *
 * DESCRIPTION:
 * Templates are sequences of wordId's and stateId's.  The high-order bit of 
 * stateId's will be set to distinguish words from states. PARSE_NULL_WORD (0)
 * terminates a template.
 *
 * WordId's are copied as is into the response sequence.  The branch labels
 * of single, unpaired stateId's are copied as well, as are all the
 * branch labels between paired stateId's
 *---------------------------------------------------------------------------*/
#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "vfile.h"
#include "types.h"
#include "tree.h"
#include "kb.h"

typedef UInt TemplateEl;

Ptr          TEMPLATE_load(VFILE recFile);
void         TEMPLATE_annihilate(Ptr ttbl);
TemplateEl * TEMPLATE_lookup(Ptr templateTablePtr, UShort templateIdx);
Bool         TEMPLATE_makeResponse(KB kb, UInt type, TemplateEl * template, VFILE recFile, 
                                   TREENODE parse, UInt vocabId, UInt grammarId, Char *buffer,
                                   UInt bufSize, UInt *sizeNeeded);
Bool         TEMPLATE_makeResponseHyp(KB kb, UInt type, VFILE recFile,
                                      HYP hyp, UInt vocabId, UInt grammarId,
                                      Char *buffer, UInt bufSize, UInt *sizeNeeded);

TREENODE     TEMPLATE_findState(TREENODE trace, UShort stateId);

#ifdef __cplusplus
}
#endif
#endif  /* _TEMPLATE_H_ */
