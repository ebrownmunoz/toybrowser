/* pid.h - access to process ID's
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _PID_H_
#define _PID_H_

#include "types.h"
#if defined(__cplusplus)
extern "C" {
#endif

extern unsigned int getpidbyname(char * name);
extern int          killprocess(unsigned int pid);

#if defined(__cplusplus)
}
#endif
#endif /* _PID_H_ */
