/* power - power and battery status facility
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (craigv)
 *
 * HISTORY
 *  09-Dec-02 Craig Vanderborgh (craigv@voxware.com)
 *      Prepared for checkin
 *---------------------------------------------------------------------------*/
#ifndef _POWER_H_
#define _POWER_H_
#include "pthr.h"

#ifdef __cplusplus
extern "C" {
#endif

Int POWER_getAvail();

#ifdef __cplusplus
}
#endif
#endif /* _POWER_H_ */
