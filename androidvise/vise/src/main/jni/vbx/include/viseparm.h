#ifndef VISEPARM_

/******************************************************************************
 * 
 * File:         viseparm.h
 *
 * Description:  Enumeration of the VISE parameters
 *
 *               NB: The parameters marked "must not move" must retain their
 *               current indices to preserve compatibility with earlier
 *               voice files.
 *
 *****************************************************************************/

#define VISEPARM_ 0

#include "vbxtypes.h"

#ifdef __cplusplus
extern "C" { 
#endif

typedef enum parid_e {
  NUMHIGH, FIRSTPARAM = NUMHIGH,
  NUMLOW, 
  MINAMPDUR,
  MINLOWAVG,
  STARTEXTRA,
  ENDEXTRA, 
  ACTIVATIONTHRESHOLD,
  DEACTIVATIONTHRESHOLD,
  LOOSEDPTHRESH, 
  TIGHTDPTHRESH,
  MUHTHAH,
  GRACEPERIOD,
  VERSION,                                             /* Must not move (12) */
  ARCMODELALLOC,
  GRAMMARMODELALLOC,
  KERNELMODELALLOC,
  WORDENTRYALLOC,
  WORDMODELALLOC,
  TRACEBACKPATHALLOC,
  TRACEBACKNODEALLOC,
  TRACEBACKFRAMEALLOC,
  JINBUFFERSIZE, FRAMEQLENGTH = JINBUFFERSIZE,
  FRAMEQFWDTOLERANCE,
  FRAMEQBWDTOLERANCE,
  JINCLASSSIZE, FRAMECLASSSIZE = JINCLASSSIZE,
  JINTOLERANCE, FRAMETOLERANCE = JINTOLERANCE,
  PATHSTACKSIZE,
  JINSCALE,                                            /* Must not move (27) */
  NOISEBITMASK,                                        /* Must not move (28) */
  WEIGHTNORM,
  FRAMESHIFT,                                          /* Must not move (30) */
  JINNUMCOMPONENTS, JINVECLENGTH = JINNUMCOMPONENTS,   /* Must not move (31) */
  FRAMEWIDTH,                                          /* Must not move (32) */
  SAMPLERATE,                                          /* Must not move (33) */
  SIGBITS,                                             /* Must not move (34) */
  AUDIOLEVEL,
  BOUPADLENGTH,
  EOUPADLENGTH,
  FEVERSION,                                           /* Must not move (38) */
  PADWORDINDEX,
  AUDQUEUELEN,
  MAXUNPROCESSEDFRAMES,
  UNPROCESSEDQUANTUM,
  NOISETRACKING,  LASTPARAM = NOISETRACKING,
  NPARAM, NOPARAMETER = NPARAM
} parid_t;

extern V_Char * VISEPARM_string(parid_t parameter);

#ifdef __cplusplus
}
#endif

#endif /* VISEPARM_ */
