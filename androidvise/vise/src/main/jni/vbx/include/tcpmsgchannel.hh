#ifndef _TCPMSGCHANNEL_HH_
#define _TCPMSGCHANNEL_HH_

#include "types.h"

// Voxware Infrastructure
#include "mbox.h"
#include "critsect.h"
#include "ifaces.h"
#include "parcel.h"

// Method return status codes
#define TCPMC_SUCCESS            (1)

#define TCPMCERR_UNDEFPARAM      (600)
#define TCPMCERR_INVALIDARG      (601)


// TCPMsgChannel Parameter Indices
#define TCPMC_MBOXGROWTHQNTM     (100)
#define TCPMC_MBOXMAXMSGS        (101)
#define TCPMC_MBOXNUMPRIOS       (102)
#define TCPMC_MSGPARCELS         (103)
#define TCPMC_CMDPARCELS         (104)
#define TCPMC_LISTENPORT         (105)
#define TCPMC_MAXCONN            (106)
#define TCPMC_RECVTIMEOUT        (107)
#define TCPMC_SENDTIMEOUT        (108)

typedef struct ConnDesc_s {
  Int    fd;
  void  *mc;
} ConnDesc_t, *CD;

// Definition of the TCPMsgChannel Object

class TCPMsgChannel {
  public:
    TCPMsgChannel();
   ~TCPMsgChannel();

    enum            { GrowthQuantumDflt = 50, MaxMsgsDflt = 50, NumPriosDflt = 1 };
    enum            { ConnTimeoutDflt = 120000, RecvTimeoutDflt = 1000, SendTimeoutDflt = 1000 };
    enum            { MsgParcelsDflt = 100, CmdParcelsDflt = 100 };
    enum            { ListenPortDflt = 5501, MaxConnDflt = 10 };

    void            Configure();
    Int             GetParameter(Int param, Int *valuePtr);
    Int             SetParameter(Int param, Int value);
    PARCEL          NewParcel(Int type);
    PARCEL          GetParcel();
    void            PutParcel(PARCEL parcel);

  protected:
    IMemory         Allocator;

    CRITSECT_DECLARE(CritSect);
    PARCELBANK      Bank;
    MBOX_Att        MBoxAttr;
    MBOX            RecvMBox;
    MBOX            SendMBox;
    pthread_t       ConnThread;
    pthread_t       SendThread;
    void            ConnectionListener();
    void            ParcelSender();

  // Parameters
    Int             ListenPort;
    Int             MaxConn;
    Int             ConnTimeout;
    Int             RecvTimeout;
    Int             SendTimeout;

  // Thread start routines are friends
  friend void TCPMsgChannel_connThread(TCPMsgChannel *);
  friend void TCPMsgChannel_sendThread(TCPMsgChannel *);
  friend void TCPMsgChannel_recvTask(CD);
};

typedef TCPMsgChannel * PTCPMsgChannel;

#endif  /* _TCPMSGCHANNEL_HH_ */
