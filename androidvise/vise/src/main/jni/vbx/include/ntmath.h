/* ntmath.h - header for DPML functions (needed in NT)
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: ntmath.h[1.0] Mon Apr  7 23:53:29 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _NTMATH_H_
#define _NTMATH_H_

#if defined(LINUX) || defined(AIX)
#ifdef __cplusplus
extern "C" {
#endif
extern float nearbyintf(float);
extern double nearbyint(double);
double nint(double x);
#define nint(x)            ((double)((int) (x + ((x < 0.) ? -0.5 : 0.5))))
#define nintf(x)           ((float)((int) (x + ((x < 0.) ? -0.5 : 0.5))))
#ifdef NEVER
#define nint(x)        (nearbyint(x))
#define nintf(x)       (nearbyintf(x))
#endif
#ifdef __cplusplus
}
#endif
#endif

#if defined(WIN32) && !defined(NEWLIB)
#if 0
#define UCHAR_MAX       255U          /* max value for an unsigned char */
#define UINT_MAX        4294967295U   /* max value for an unsigned int */
#define USHRT_MAX       65535U        /* max value for an unsigned short */
#endif

#ifdef __cplusplus
extern "C" {
#endif
#ifdef HAVE_DPML
float  frexpf(float x, int * y);
#define logb(x) _logb(x)
float  log10f(float x);
double nint(double x);
float  nintf(float x);
float  powf(float x, float y);
#else
double nint(double x);
#define nint(x)            ((double)((int) (x + ((x < 0.) ? -0.5 : 0.5))))
#define nintf(x)           ((float)((int) (x + ((x < 0.) ? -0.5 : 0.5))))
#define frexpf(x, y)       ((float) frexp((double) x, y))
#define logb(x)            (_logb(x))
#define log2(x)            (log(x) / log(2.0))
#define log10f(x)          ((float) log10((float) x))
#define powf(x, y)         ((float) pow((double) x, (double) y))
#endif
#ifdef __cplusplus
}
#endif
#endif

#if defined(VXWORKS) || defined(sol)
#ifdef __cplusplus
extern "C" {
#endif
double nint(double x);
#define nint(x)            ((double)((int) (x + ((x < 0.) ? -0.5 : 0.5))))
#define nintf(x)           ((float)((int) (x + ((x < 0.) ? -0.5 : 0.5))))
#define log2(x)            (log(x) / log(2.0))
#define log10f(x)          ((float) log10((float) x))
#define powf(x, y)         ((float) pow((double) x, (double) y))
#ifdef __cplusplus
}
#endif
#endif

#endif  /* _NTMATH_H_ */
