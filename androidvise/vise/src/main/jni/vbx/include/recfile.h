/* recfile.h - Recfile handling object which contains an IFile
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * 
 * Declarations for IFile-based object which does REC-file i/o
 *
 * Verbex Voice Systems (cav)
 * $Header: recfile.h[1.0] Thu Aug 27 13:20:00 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _RECFILE_H_
#define _RECFILE_H_

#include "ifaces.h"
#include "vbxtypes.h"
#include "vfile.h"
#include "types.h"

typedef struct recfile_s *RECFILE;

#undef  INTERFACE
#define INTERFACE RecFile

VBX_DECLARE_INTERFACE(RecFile) {
  IFile               iFile;
  struct recfile_s  * recfile;
};

#ifdef __cplusplus
extern "C" { 
#endif

#define RECFILE_SUCCESS           0
#define RECFILE_OPEN_FAILS        1
#define RECFILE_FILE_WRITE_ERROR  2
#define RECFILE_SEEK_ERROR        3

enum recfile_seeks   { RECFILE_SEEK_SET, RECFILE_SEEK_CUR, RECFILE_SEEK_END };
enum recfile_iomodes { RECFILE_READWRITE, RECFILE_READ, RECFILE_WRITE };
enum recfile_iotypes { RECFILE_STDIO, RECFILE_MEMSTREAM, RECFILE_FILESTREAM };

/* RECFILE-specific methods */
extern IFile  *RECFILE_create(IMemory *iMem, UInt ioType);
extern void    RECFILE_destroy(IFile *recfile);
extern Int     RECFILE_ioType(IFile *recfile);

#ifdef __cplusplus
}
#endif
#endif  /* _RECFILE_H_ */
