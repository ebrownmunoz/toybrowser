/****************************************************************************
 *
 *  ifacedef.h
 *
 *  Useful macros for declaring interfaces
 *
 ****************************************************************************/


#ifndef _IFACEDEF_H_
#define _IFACEDEF_H_

#ifdef __cplusplus
#define EXTERN_C    extern "C"
#else
#define EXTERN_C    extern
#endif

#define VBXRESULT  void

#define VBXMETHODCALLTYPE
#define VBXMETHODIMP            VBXRESULT VBXMETHODCALLTYPE
#define VBXMETHODIMP_(type)     type VBXMETHODCALLTYPE

/****************************************************************************
 *
 *      Interface Declaration
 *
 *      These macros enable you to declare interfaces such that a single
 *      declaration works for both C and C++.
 *
 *      Use VBX_DECLARE_INTERFACE(iface) to declare an interface that does
 *      not derive from a base interface.
 *
 *      Use VBX_DECLARE_INTERFACE_(iface, baseiface) to declare one that does.
 *
 *      A sample interface declaration:
 *
 *        #undef  INTERFACE
 *        #define INTERFACE IAudioIn
 *
 *        VBX_DECLARE_INTERFACE(IAudioIn) {
 *          // IAudioIn methods
 *          VBXMETHOD_(Bool, Read)    (VBXTHIS_ Short * samples, UInt numSamples) VBXPURE;
 *          VBXMETHOD(Start)          (VBXTHIS) VBXPURE;
 *          VBXMETHOD(Stop)           (VBXTHIS) VBXPURE;
 *        };
 *
 *      The resulting C++ expansion:
 *
 *        struct IAudioIn {
 *          virtual Bool      VBXMETHODCALLTYPE Read(Short * samples, UInt numSamples) = 0;
 *          virtual VBXRESULT VBXMETHODCALLTYPE Start(void) = 0;
 *          virtual VBXRESULT VBXMETHODCALLTYPE Stop(void) = 0;
 *        };
 *
 *      The resulting C expansion:
 *
 *        typedef struct IAudioIn IAudioIn;
 *        struct IAudioIn {
 *          Bool      (VBXMETHODCALLTYPE * Read)    (IAudioIn * This, Short * samples, UInt numSamples);
 *          VBXRESULT (VBXMETHODCALLTYPE * Start)   (IAudioIn * This);
 *          VBXRESULT (VBXMETHODCALLTYPE * Stop)    (IAudioIn * This);
 *        };
 */

#ifdef VBX_CPPIFACES

#define VBXMETHOD(method)                        virtual VBXRESULT VBXMETHODCALLTYPE method
#define VBXMETHOD_(type,method)                  virtual type VBXMETHODCALLTYPE method
#ifndef VBXPURE
#define VBXPURE                                  = 0
#endif
#define VBXTHIS_
#define VBXTHIS                                  void
#define VBX_DECLARE_INTERFACE(iface)             struct iface
#define VBX_DECLARE_INTERFACE_(iface, baseiface) struct iface : public baseiface

#else

#define VBXMETHOD(method)                        VBXRESULT (VBXMETHODCALLTYPE * method)
#define VBXMETHOD_(type,method)                  type (VBXMETHODCALLTYPE * method)
#define VBXPURE
#define VBXTHIS_                                 INTERFACE * This,
#define VBXTHIS                                  INTERFACE * This
#define VBX_DECLARE_INTERFACE(iface)             typedef struct iface iface; struct iface
#define VBX_DECLARE_INTERFACE_(iface, baseiface) VBX_DECLARE_INTERFACE(iface)

#endif

#endif     /* def _IFACEDEF_H_ */

