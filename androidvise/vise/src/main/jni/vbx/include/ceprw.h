/* ceprw.h - cepstral file i/o routines
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _CEPRW_H_
#define _CEPRW_H_

#include "types.h"
#include "sphdr.h"

#ifdef __cplusplus
extern "C" {
#endif

extern Bool  CEP_read(Char *fname, HDR hdr, Float **data, size_t * numFloats);
extern Bool  CEP_write(Char *fname, HDR hdr, Float *data, size_t   numFloats);
extern Int   CEP_peekLen(Char *fname, HDR hdr);

#ifdef __cplusplus
}
#endif
#endif              /* ndef _CEPRW_H_ */
