/* po.h - the Post Office cluster
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/

#ifndef _PO_H_
#define _PO_H_
#include "types.h"
#include "mbox.h"

#ifdef __cplusplus
extern "C" {
#endif
#define CEP_MBOXSIZE  10
#define MSG_MBOXSIZE  4
#define SIG_MBOXSIZE  1

extern MBOX * PO_mboxArr;

extern Bool  PO_create(IMemory * memory);
extern void  PO_destroy();
extern MBOX  PO_defineMBOX(Int idx, MBOX_Att attr);
extern Bool  PO_insert(Short mb, OBJT objt);
extern Bool  PO_priorityInsert(Short mb, OBJT objt, Uns priority);
extern Bool  PO_insertNow(Short mb, OBJT objt);
extern Bool  PO_priorityInsertNow(Short mb, OBJT objt, Uns priority);

#define PO_mbox(idx)   (PO_mboxArr[idx])

#ifdef __cplusplus
}
#endif
#endif  /* _PO_H_ */
