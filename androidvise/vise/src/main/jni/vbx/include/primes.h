/* primes.h - prime numbers
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: primes.h[1.0] Mon Apr  7 23:53:38 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _PRIMES_H_
#define _PRIMES_H_

#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

extern UInt * PRIMES_toN(UInt n, UInt * numPrimes);
extern UInt   PRIMES_next(UInt n);
extern UInt * PRIMES_firstN(UInt n, UInt * numPrimes);
extern UInt   PRIMES_nth(UInt n);

#ifdef __cplusplus
}
#endif
#endif /* _PRIMES_H_ */
