#ifndef _BUDS_H_
#define _BUDS_H_

#include "tree.h"

#ifdef __cplusplus
extern "C" {
#endif

/* The bud_pointer structure describes an element in a list of bud pointers.
   Buds are the tips of branches (nodes without daughters) in a tree.  */

typedef struct buds_s   * BUDS;
typedef struct budsEl_s * BUDSEL;

typedef struct budsEl_s {
  BUDSEL   prev;
  BUDSEL   next;
  TREENODE bud;
} budsEl_t;


BUDS   BUDS_create();
void   BUDS_destroy(BUDS buds);
Bool   BUDS_initialize(BUDS buds);
BUDSEL BUDS_elAlloc(BUDS buds);
void   BUDS_elFree(BUDS buds, BUDSEL bud);
Bool   BUDS_elInsert(BUDS buds, BUDSEL posBud, TREENODE node);
void   BUDS_elSet(BUDSEL bud, BUDSEL prev, BUDSEL next, TREENODE node);
Bool   BUDS_elAddDaughter(BUDS buds, TREE tree, ParsePos_t pos, BUDSEL motherBud, UShort stateId, UShort wordId);
UShort BUDS_maxUsedEl(BUDS buds);

#ifdef __cplusplus
}
#endif
#endif  /* _BUDS_H_ */
