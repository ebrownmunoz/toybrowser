#ifndef V_MTSC_

/******************************************************************************
 * 
 *  File: mtsc.h - header for the message test system
 *
 *****************************************************************************/

#define V_MTSC_

#include "vbxtypes.h"
#include "visemsg.h"
#include "ifaces.h"

  /* An MTSC */
typedef struct mtsc_s * MTSC;

  /* MTSC Methods */
extern MTSC     MTSC_Create(IMemory * memory, IMail * mail, VBXMSG sysmsg, V_Char * name);
extern void     MTSC_Destroy(MTSC mtsc);
extern V_Char * MTSC_Name(MTSC mtsc);
extern void     MTSC_Run(MTSC mtsc, void (* function)(MTSC));
extern void     MTSC_Function(MTSC mtsc);

#endif  /* defined V_MTSC_ */
