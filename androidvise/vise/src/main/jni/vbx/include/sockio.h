/* sockio.h - socket setup and i/o functions
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: sockio.h[1.0] Mon Apr  7 23:53:48 1997 dvetter@titan saved $
 * DESCRIPTION
 * Interface to sockio.c, primarily intended for packet.c
 *---------------------------------------------------------------------------*/

#ifndef _SOCKIO_H_
#define _SOCKIO_H_

#define FAILURE -1
#define UNIX     0
#define TCP      1
#define MAXSOCKBUF 50000

typedef struct sockdef_ {
  Int     typ;                /* socket type: TCP or UNIX */
  Int     bufsiz;             /* size in bytes of socket buffer */
  UShort *port;               /* pointer to port number for TCP socket */
  Char   *uname;              /* file name for UNIX domain socket */
} sockdef_s, *sockdef_t;

Bool sock_client(sockdef_t sock, Char *server);
Bool sock_close(Int sd, sockdef_t sock);
Bool sock_peek(Int sd, Char *buf, Int siz);
Bool sock_recv(Int sd, Char *buf, Int siz, Bool blk);
Bool sock_send(Int sd, Char *buf, Int siz, Bool blk);
Bool sock_server(sockdef_t sock);

#endif /* _SOCKIO_H_ */
