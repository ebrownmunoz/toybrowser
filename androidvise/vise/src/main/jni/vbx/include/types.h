/* types.h - typedefs for inter-architecture portability.
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _TYPES_H_
#define _TYPES_H_

#include "vbxtypes.h"

#if !defined(DEC) && \
    !defined(LINUX) && \
    !defined(AIX) && \
    !defined(usl) && \
    !defined(sun) && \
    !defined(VXWORKS) && \
    !defined(WIN32) && \
	!defined(__ANDROID__)
#define vbx
#endif

 /*** DEC Alpha Types ***/

#ifdef DEC
#ifndef Bool
typedef int            Bool;
#endif
#ifndef TRUE
enum    bools {FALSE, TRUE};
#endif
typedef char           Char;
typedef double         Double;
typedef float          Float;
typedef int            Int;
typedef int            Long;
typedef long           Quad;
#define QUAD_MAX       LONG_MAX
#define QUAD_MIN       LONG_MIN
#define QUADSIZE       8
#define strtoll(n,e,b) strtol(n,e,b)
#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "ld"
#endif
typedef char         * Ptr;
typedef short          Short;
typedef void           Void;
typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned int   ULong;
typedef unsigned short UShort;
#endif  /* def DEC */

 /*** LINUX and AIX Types ***/

#if defined(LINUX) || defined(AIX) || defined(__ANDROID__) || defined(ANDROID)
#if defined(X8664)
#ifndef Bool
typedef int            Bool;
#endif
/* GCJ is doing some screwy stuff  Overrule */
#ifdef  TRUE
#undef  TRUE
#undef  FALSE
#endif
enum    bools {FALSE, TRUE};
typedef char           Char;
typedef double         Double;
typedef float          Float;
typedef int            Int;
typedef int            Long;
typedef long           Quad;
#define QUAD_MAX       LONG_MAX
#define QUAD_MIN       LONG_MIN
#define QUADSIZE       8

#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "ld"
#endif
typedef char         * Ptr;
typedef short          Short;
typedef void           Void;
typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned int   ULong;
typedef unsigned short UShort;
#else
#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX  0x7fffffffffffffff
#define LONG_LONG_MIN  (-LONG_LONG_MAX-1L)
#endif
#ifndef Bool
typedef int            Bool;
#endif
/* GCJ is doing some screwy stuff  Overrule */
#ifdef  TRUE
#undef  TRUE
#undef  FALSE
#endif
enum    bools {FALSE, TRUE};

typedef char           Char;
typedef double         Double;
typedef float          Float;
typedef int            Int;
typedef int            Long;
typedef long long      Quad;
#if !defined(ANDROID) && !defined(__ANDROID__)
#define QUAD_MAX       LONG_LONG_MAX
#define QUAD_MIN       LONG_LONG_MIN
#endif
#define QUADSIZE       8
#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "lld"
#endif
typedef char         * Ptr;
typedef short          Short;
typedef void           Void;
typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned int   ULong;
typedef unsigned short UShort;
#endif  /* !defined(X86-64) */
#endif  /* defined(LINUX) || defined(AIX) */

 /*** VXWORKS Types ***/

#ifdef VXWORKS
#include "badmath.h"
#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX  0x7fffffffffffffff
#define LONG_LONG_MIN  0x8000000000000000
#endif
#ifndef Bool
typedef int            Bool;
#endif
#ifndef TRUE
enum    bools {FALSE, TRUE};
#endif
typedef char           Char;
typedef double         Double;
typedef float          Float;
typedef int            Int;
typedef int            Long;
typedef long long      Quad;
#define QUAD_MAX       LONG_LONG_MAX
#define QUAD_MIN       LONG_LONG_MIN
#define QUADSIZE       8
#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "lld"
#endif
typedef char         * Ptr;
typedef short          Short;
typedef void           Void;
typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned int   ULong;
typedef unsigned short UShort;
#endif  /* def VXWORKS */

 /*** WIN32 Types ***/

#ifdef WIN32

#ifndef Bool
typedef int            Bool;
#endif
/* GCJ is doing some screwy stuff  Overrule */
#ifdef TRUE
#undef TRUE
#undef FALSE
#define TRUE  1
#define FALSE 0
#endif
/*enum    bools {FALSE, TRUE}; */
typedef char           Char;
typedef double         Double;
#ifdef dblflt
typedef double         Float;
#else
typedef float          Float;
#endif
typedef int            Int;
typedef long           Long;
#if defined(GNUWINCE)
#define nint(x)        round(x)
#define nintf(x)       roundf(x)
typedef long long      Quad;
#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX  0x7fffffffffffffff
#define LONG_LONG_MIN  (-LONG_LONG_MAX-1L)
#endif
#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "lld"
#endif
#define QUAD_MAX       LONG_LONG_MAX
#define QUAD_MIN       LONG_LONG_MIN
#else
typedef __int64        Quad;
#define QUAD_MAX       _I64_MAX
#define QUAD_MIN       _I64_MIN
#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "I64d"
#endif
#endif
typedef char         * Ptr;
#if !defined(GNUWINCE)
typedef char         * caddr_t;
#endif
typedef short          Short;
typedef void           Void;

typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned long  ULong;
typedef unsigned short UShort;

#endif  /* def WIN32 */

 /*** USL Types ***/

#ifdef usl

typedef int            Bool;
#ifndef TRUE
enum    bools {FALSE, TRUE};
#endif
typedef char           Char;
typedef double         Double;
#ifdef dblflt
typedef double         Float;
#else
typedef float          Float;
#endif
typedef int            Int;
typedef long           Long;
typedef char         * Ptr;
typedef short          Short;
typedef void           Void;
typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned long  ULong;
typedef unsigned short UShort;

#endif  /* def usl */

 /*** SUN Solaris Types ***/

#ifdef sun
typedef int            Bool;
#ifndef TRUE
enum    bools {FALSE, TRUE};
#endif
#ifdef sol
#ifndef _QuadFmt_
#define _QuadFmt_
#define QuadFmt(x)     "%"#x "lld"
#endif
#endif
typedef char           Char;
typedef double         Double;
#ifdef dblflt 
typedef double         Float;
#else
typedef float          Float;
#endif
typedef int            Int;
typedef long           Long;
#ifdef sol
typedef long long      Quad;
#define QUAD_MAX       LONG_LONG_MAX
#define QUAD_MIN       LONG_LONG_MIN
#endif
typedef char         * Ptr;
typedef short          Short;
typedef void           Void;
typedef unsigned       Uns;
typedef unsigned char  UChar;
typedef unsigned int   UInt;
typedef unsigned long  ULong;
typedef unsigned short UShort;

#endif  /* def sun */

 /*** VBX Default Types ***/

#ifdef vbx
#define Void           V_Void;
#ifndef Bool
typedef V_Long         Bool;
#endif
#ifndef TRUE
enum    bools {FALSE, TRUE};
#endif
typedef V_Char         Char;
typedef V_Double       Double;
#ifdef dblflt
typedef V_Double       Float;
#else
typedef V_Real         Float;
#endif
typedef V_Long         Int;
typedef V_Long         Long;
typedef V_Char       * Ptr;
typedef V_Int          Short;
typedef V_ULong        Uns;
typedef V_Byte         UChar;
typedef V_ULong        UInt;
typedef V_ULong        ULong;
typedef V_Uns          UShort;
#endif  /* def vbx */


 /*** Useful Things ***/

typedef enum { BACKWARD = -1, UNTOWARD = 0, FORWARD = 1 } dir_t;
#define DIR_SYM(D)  (((D) == FORWARD) ? "FWD" : ((D) == BACKWARD) ? "BWD" : "NWD")

#define CAST(TYPIN, IN, TYPOUT, OUT) {  \
    union {TYPIN in; TYPOUT out;} tmp;  \
    tmp.in = (IN); (OUT) = tmp.out;     \
    }

#endif /* _TYPES_H_ */

