#ifndef _DATE_H_
#define _DATE_H_

#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

Bool DATE_getDefault(Char *dateBuf, Int dateBufLen);

#ifdef __cplusplus
}
#endif
#endif  /* ndef _DATE_H_ */
