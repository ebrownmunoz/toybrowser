/* vbxaudio.h - Audio abstraction 
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * Basic method of manipulating audio of the different kind
 *
 *---------------------------------------------------------------------------*/
#ifndef _VBXAUDIO_H_
#define _VBXAUDIO_H_

#ifdef __cplusplus
extern "C" { 
#endif /* __cplusplus */

#if defined(VXWORKS)

#include "vxWorks.h"
#include "msgQLib.h"
#include "drv/ioc/iocLib.h"
#include "drv/ioc/buttonLib.h"
#include "drv/wave/waveLib.h"
#include "drv/flash/rfncFlash.h"

#elif defined(GNUWINCE) || defined(LINUX) || defined(CYGWIN)

#if defined(CYGWIN) || defined(LINUX)
#include "mstypes.h"
#else
#include <sys/wcebase.h>
#include "vbxwave.h"
#endif
#include "mmsystem.h"

  /* Wave device handle access method */
extern HANDLE   vbxGetWaveHandle(void);
  /* Input */
extern MMRESULT vbxInOpen(LPHWAVEIN phwi, UINT uDeviceId, LPWAVEFORMATEX pwfx,
                           DWORD dwCallback, DWORD dwInstance, DWORD fdwOpen);
extern MMRESULT vbxInAddBuffer(HWAVEIN hwi, LPWAVEHDR pwh, UINT cbwh);
extern MMRESULT vbxInReset(HWAVEIN hwi);
extern MMRESULT vbxInStart(HWAVEIN hwi);
extern MMRESULT vbxInStop(HWAVEIN hwi);
extern MMRESULT vbxInPrepareHeader(HWAVEIN hwi, LPWAVEHDR pwh, UINT cbwh);
extern MMRESULT vbxInUnprepareHeader(HWAVEIN hwi, LPWAVEHDR pwh, UINT cbwh);
extern HWAVEIN  vbxGetWaveInHandle(void);
  /* Output */
extern MMRESULT vbxOutOpen(LPHWAVEOUT phwo, UINT uDeviceId, LPWAVEFORMATEX pwfx,
                           DWORD dwCallback, DWORD dwInstance, DWORD fdwOpen);
extern MMRESULT vbxOutPause(HWAVEOUT hwo);
extern MMRESULT vbxOutPrepareHeader(HWAVEOUT hwo, LPWAVEHDR pwh, UINT cbwh);
extern MMRESULT vbxOutUnprepareHeader(HWAVEOUT hwo, LPWAVEHDR pwh, UINT cbwh);
extern MMRESULT vbxOutReset(HWAVEOUT hwo);
extern MMRESULT vbxOutRestart(HWAVEOUT hwo);
extern MMRESULT vbxOutWrite(HWAVEOUT hwo, LPWAVEHDR pwh, UINT cbwh);
extern HWAVEOUT vbxGetWaveOutHandle(void);

#elif defined(LINUX)

extern Int vbxGetMixerFd(void);

#endif /* PLATFORM */

  /* OEM Info access */
extern const char * vbxGetOEM(void);
  /* Closing. vbxWaveHandleClose closes MixerFd (if open) in case of LINUX */
extern void     vbxWaveHandleClose(void);
extern Bool     vbxInClose(void);
extern Bool     vbxOutClose(void);
  /* Output Volume */
extern Bool     vbxOutGetVolume(Float * pdBVolume);
extern Bool     vbxOutSetVolume(Float dBVolume);
  /* Input Gain */
extern Bool     vbxInGetGain(Float * pdBGain);
extern void     vbxInSetGain(Float dBGain);
  /* Sidetone Attenuation */
extern Bool     vbxGetSidetoneGain(Float * pdBGain);
extern void     vbxSetSidetoneGain(Float dBGain);
  /* Meaningful only for XVOX and XVOX_LINUX platforms. Returns FALSE for any other */
extern Bool     readCodecRegister(UInt reg, UInt * pval);
extern Bool     writeCodecRegister(UInt reg, UInt val);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _VBXAUDIO_H_ */
