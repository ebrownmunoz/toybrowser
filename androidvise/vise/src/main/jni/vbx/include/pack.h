/*
 * pack.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _PACK_H_
#define _PACK_H_

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct pack_s {
  Short mb;              /* mailbox - must be first for MSG stuff to work */
  Short id;
  Short len;
  Char contents[1];
} pack_t, *PACK;

#define PACK_free(A)     free(A)
#define PACK_contents(A) ((A)->contents)
#define SIZEOF(t)        (sizeof(t)+HDR_LEN)

extern PACK  PACK_create(Short mb, Short id, Short len, Ptr contents);
extern Short PACK_destroy(PACK packet, Ptr *contents);
extern Bool  PACK_send(Int sd, PACK packet);
extern Bool  PACK_cmdSend(Int sd, Short mb, Short id);
extern Bool  PACK_avail(Int sd);
extern PACK  PACK_recv(Int sd);
extern Bool  PACK_mail(Short mb, PACK packet);
extern Bool  PACK_priorityMail(Short mb, PACK packet, Uns priority);
extern Bool  PACK_mailNow(Short mb, PACK packet);
extern Bool  PACK_priorityMailNow(Short mb, PACK packet, Uns priority);

/* for back compatibility with old packet stuff */

#define HDRLEN HDR_LEN
typedef pack_t packet_s;
typedef PACK packet_t;

packet_t pack_create(Short mb, Short id, Short len, caddr_t contents);
Int pack_destroy(packet_t packet, caddr_t *contents);
Bool pack_free(packet_t packet);
Bool pack_send(Int sd, packet_t packet);
Bool cmd_send(Int sd, Short mb, Short id);
Bool pack_avail(Int sd);
packet_t pack_recv(Int sd);
Bool pack_mail(Short mb, packet_t packet);

#ifdef __cplusplus
}
#endif
#endif  /* _PACK_H_ */
