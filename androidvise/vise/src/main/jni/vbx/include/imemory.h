#ifndef V_IMEMORY_

/******************************************************************************
 * 
 *  File: imemory.h - header for the memory allocator
 *
 *****************************************************************************/

#define V_IMEMORY_

#include "vbxtypes.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" { 
#endif

extern void * IMEM_calloc(IMemory * imemory, size_t numObj, size_t sizeObj, memtype_t type);
extern void   IMEM_free(IMemory * imemory, void * allocation);

#ifdef __cplusplus
}
#endif

#endif  /* defined V_IMEMORY_ */
