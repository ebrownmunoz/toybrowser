/* c.h - some useful macros
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: ceprw.h[2.2] Tue Feb 22 17:55:28 1994 craigv@noname saved $
 *---------------------------------------------------------------------------*/
#ifndef	_C_H_
#define	_C_H_

#include "types.h"

#ifndef MIN
#define	MIN(a,b) (((a)<(b))?(a):(b))
#endif
#ifndef MAX
#define	MAX(a,b) (((a)>(b))?(a):(b))
#endif

#ifndef	NULL
#define NULL	0
#endif

#define	CERROR		(-1)

#define	sizeofS(string)	(sizeof(string) - 1)
#define sizeofA(array)	(sizeof(array)/sizeof(array[0]))

#define caseE(enum_type)	case (Int)(enum_type)

#endif /* _C_H_ */
