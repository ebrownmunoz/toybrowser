/* pconf.h - command line argument processing ("parameter configuration")
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: pconf.h[1.0] Mon Apr  7 23:53:33 1997 dvetter@titan saved $
 * HISTORY
 * 16-Feb-01  Dave Vetter (davev@voxware.com)
 *      Added NABOOLP type
 * 26-Mar-93  Dave Vetter (dvetter@rad.verbex.com)
 *      Added function prototypes for export
 *---------------------------------------------------------------------------*/
#ifndef _PCONF_
#define _PCONF_

#include <sys/types.h>
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  NOTYPEP,
  BYTEP,
  SHORTP,
  INTP,
  LONGP,
  U_BYTEP,
  U_SHORTP,
  U_INTP,
  U_LONGP,
  FLOATP,
  DOUBLEP,
  BOOLP,
  CHARP,
  STRP,
  DATA_SRCP
} arg_t;

/* data source types */
typedef enum {
  SRC_NONE,
  SRC_HSA,
  SRC_VQFILE,
  SRC_CEPFILE,
  SRC_ADCFILE
} data_src_t;

typedef union _ptr {
  Char        *CharP;
  Char        *ByteP;
  UChar       *UByteP;
  Short       *ShortP;
  UShort      *UShortP;
  Int         *IntP;
  UInt        *UIntP;
  Long        *LongP;
  ULong       *ULongP;
  Float       *FloatP;
  Double      *DoubleP;
  Int         *BoolP;
  Char       **StringP;
  data_src_t  *DataSrcP;
} ptr_t;

/* External Representation of the PCONF table entries */
typedef struct pconf_config_s {
  Char        *LongName;  /* Long Name */
  Char        *Doc;       /* Documentation string */
  Char        *swtch;     /* Switch Name */
  arg_t        arg_type;  /* Argument Type */
  Ptr          var;       /* Pointer to the variable */
} config_t;

/* Internal Representation of the PCONF table entries */
typedef struct pconf_internalconfig_s {
  Char        *LongName;  /* Long Name */
  Char        *Doc;       /* Documentation string */
  Char        *swtch;     /* Switch Name */
  arg_t        arg_type;  /* Argument Type */
  ptr_t        var;       /* Pointer to the variable */
} Config_t;

extern Int   pconf(Int argc, Char * argv[], Config_t config_p[],
                   Char ** display, Char ** geometry, Char * (*GetDefault)());
extern Int   ppconf(Int argc, Char * argv[], Config_t config_p[],
                    Char ** display, Char ** geometry, Char * (*GetDefault)(),
                    Char last);
extern Int   fpconf(FILE * config_fp, Config_t config_p[],
                    Char ** display, Char ** geometry, Char * (*GetDefault)());

extern void  pvalues(Char * prog, Config_t cp[]);
extern void  pusage(Char * prog, Config_t cp[]);

#ifdef __cplusplus
}
#endif

#endif /* _PCONF_ */
