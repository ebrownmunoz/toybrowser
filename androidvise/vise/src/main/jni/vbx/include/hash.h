/* hash.h - hash table routines
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _HASH_H_
#define _HASH_H_

#include "types.h"
#include "list.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* Hash Table */
typedef struct hash_s * HASH;

/* Hash table methods */
extern HASH    HASH_create(Int initialSize, Int (* hash)(HASH ht, Ptr sym), Bool (* equals)(Ptr sym, Ptr hsym), void (* free)(Ptr value));
extern void    HASH_annihilate(HASH ht);
extern void    HASH_processSymbols(HASH ht, void (* fcn)(Ptr symbol));
extern void    HASH_setFactors(HASH ht, Float full, Float grow);
extern Bool    HASH_add(HASH ht, Ptr sym, Ptr val);
extern Bool    HASH_lookup(HASH ht, Ptr sym, Ptr * val);
extern Bool    HASH_modify(HASH ht, Ptr sym, Ptr val);
extern Int     HASH_used(HASH ht);
extern Ptr     HASH_firstValue(HASH ht, Ptr * symPtr);
extern Ptr     HASH_nextValue(HASH ht, Ptr * symPtr);
extern LIST    HASH_toObjList(HASH ht);

/* Integer hash functions */
extern Int     HASH_integer(HASH ht, Ptr sym);
extern Bool    HASH_integerEquals(Ptr sym, Ptr hsym);

/* String hash functions */
extern Int     HASH_string(HASH ht, Ptr sym);
extern Bool    HASH_stringEquals(Ptr sym, Ptr hsym);

#ifdef DEBUG
/* Debugging and statistics functions */
extern void    HASH_dump(HASH ht);
extern Int     HASH_numDeltaGreater(HASH ht, UInt floor);
extern Double  HASH_meanDepth(HASH ht);
#endif

#ifdef __cplusplus
}
#endif
#endif /* _HASH_H_ */
