/* msec.h - Simple routines for timing in milliseconds
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/

#ifndef _MSEC_H_
#define _MSEC_H_

#include "pthr.h"

#ifdef __cplusplus
extern "C" {
#endif

Int MSEC_getCurrentTime();

#ifdef __cplusplus
}
#endif
#endif
