#ifndef _SCLASS_H_
#define _SCLASS_H_

/* sclass - simple class without locking (performance!)
 *--------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * $__Header$
 *--------------------------------------------------------------------------*/

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NULL
#define NULL  0
#endif

typedef struct sclass_s *SCLASS;    /* Simple Class */
typedef struct item_s *ITEM;        /* ITEM */

struct sclass_s {
  ITEM(*newFcn) (SCLASS);
  void            (*freeFcn) (ITEM);
  ITEM            freeItems;
  ITEM            items;
  ITEM            lastItem;
  UInt            numItems;
  UInt            sizeItems;
  Bool            recycle;
};

struct item_s {
  SCLASS          sclass;
  ITEM            nextItem;
  Int             contents[1];
};

SCLASS          SC_new(UInt numItems, UInt sizeContents, Bool recycle);
void            SC_flush(SCLASS sclass);
void            SC_free(SCLASS sclass);
Int             SC_freeCount(SCLASS sclass);
ITEM            ITEM_new(SCLASS sclass);
void            ITEM_free(ITEM item);

#define ITEM_contents(I)        ((I)->contents)
#define ITEM_offset(T,F)        ((UInt)&(((T)NULL)->F))

#ifdef __cplusplus
}
#endif

#endif                                /* def _ITEM_H_ */
