/* wavrw.h - sample data file i/o routines
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (jrt)
 * $Header: wavrw.h[1.0] Mon Apr  7 23:54:00 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _WAVRW_H_
#define _WAVRW_H_

#include <stdio.h>
#include "types.h"
#include "sphdr.h"

#if defined(__cplusplus)
extern "C" {
#endif
#define  CH_CNT        1
#define  SMPL_RATE     16000
#define  SMPL_BYTE_FMT "01"
#define  SMPL_SIG_BITS 16
#define  ULAWSTR       "ulaw"

enum byteord {BIGEND, LITEND};
enum sampfmts {LINEAR, ULAW};

void WAV_addDfltHdrFields(HDR hdr);
Bool WAV_read(Char *fname, HDR hdr, Short **data, size_t *numShorts);
Bool WAV_write(Char *fname, HDR hdr, Short *data, size_t numShorts);
Bool WAV_wrtFrame(FILE *stream, Short *data, size_t numShorts);
Int  WAV_peekLen(Char *fname, HDR hdr);

#if defined(__cplusplus)
}
#endif

#endif              /* ndef _WAVRW_H_ */
