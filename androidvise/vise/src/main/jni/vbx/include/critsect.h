/* critsect.h - critical section abstraction for pthreads & WIN32
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 * CRITSECT should be used any time a mutex/cv pair is needed for
 * inter-thread synchronization; this will maintain portability between 
 * WIN32 and real programming environments.
 *---------------------------------------------------------------------------*/

#ifndef _CRITSECT_H_
#define _CRITSECT_H_

#include "pthr.h"
#include <errno.h>
#include "types.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CRITSECT_NOERROR   0
#define CRITSECT_TIMEDOUT  1
#define CRITSECT_INVALID   2
#define CRITSECT_NOMEMORY  3

#ifndef VBX_SINGLE_THREAD

#if defined(REAL_PTHREADS) || defined(GNUWINCE) || defined(VXWORKS)

    /**** Multithreaded Pthreads Version ****/

#define CRITSECT_ENTER(cs)   pthread_mutex_lock((cs)->mutexPtr)
#define CRITSECT_LEAVE(cs)   pthread_mutex_unlock((cs)->mutexPtr)
#define CRITSECT_SIGNAL(cs)  pthread_cond_signal(&cs->cv)

#define CRITSECT_TIMEDWAIT_ENTER(cs, wait, timedout, predicate)         \
{                                                                       \
  struct timespec abstime;                                              \
  Int    retval;                                                        \
  pthread_get_expiration_np(wait, &abstime);                            \
  pthread_mutex_lock(cs->mutexPtr);                                     \
  while (predicate && !timedout)                                        \
    timedout = pthread_cond_timedwait(&cs->cv, cs->mutexPtr, &abstime); \
  if (timedout) {                                                       \
    if      (timedout == ETIMEDOUT) timedout = CRITSECT_TIMEDOUT;       \
    else if (timedout == ENOMEM)    timedout = CRITSECT_NOMEMORY;       \
    else                            timedout = CRITSECT_INVALID;        \
  }                                                                     \
}

#define CRITSECT_WAIT_ENTER(cs, predicate) {                            \
  pthread_mutex_lock(cs->mutexPtr);                                     \
  while (predicate)                                                     \
    pthread_cond_wait(&cs->cv, cs->mutexPtr);                           \
}

typedef struct CritSect_s {
  pthread_mutex_t  * mutexPtr;
  pthread_cond_t     cv;
  IMemory          * memory;
} CritSect_t, *CRITSECT;
#endif

#if defined(WIN32) && !defined(REAL_PTHREADS) && !defined(GNUWINCE)

    /**** Multithreaded WIN32 Version ****/

#include <assert.h>
#include "vbx.h"

#define CRITSECT_ENTER(cs)   pthread_mutex_lock((cs)->mutexPtr)
#define CRITSECT_LEAVE(cs)   pthread_mutex_unlock((cs)->mutexPtr)
#define CRITSECT_SIGNAL(cs)  SetEvent((cs)->event)

#define CRITSECT_WAIT_ENTER(cs, predicate) {                          \
  int retval;                                                         \
  while (predicate) {                                                 \
    (void) WaitForSingleObject(cs->event, INFINITE);                  \
    if (predicate) {                                                  \
      ResetEvent(cs->event);                                          \
      Sleep(1);                                                       \
    }                                                                 \
  }                                                                   \
  retval = WaitForSingleObject(*cs->mutexPtr, INFINITE);              \
  assert(retval == WAIT_OBJECT_0 || retval == WAIT_ABANDONED);        \
  ResetEvent(cs->event);                                              \
}

#define CRITSECT_TIMEDWAIT_ENTER(cs, wait, timedout, predicate) {     \
  HANDLE handle[2] = {cs->event, *cs->mutexPtr};                      \
  Int retval;                                                         \
  Int msec = (wait)->tv_nsec / 1000000;                               \
  msec += (wait)->tv_sec * 1000;                                      \
  if (msec <= 0) msec = 1;                                            \
  timedout = CRITSECT_NOERROR;                                        \
  while (predicate && !timedout) {                                    \
    retval = WaitForSingleObject(cs->event, msec);                    \
    if (retval != 0 && retval != WAIT_OBJECT_0) {                     \
      if      (retval == WAIT_TIMEOUT)                                \
        timedout = CRITSECT_TIMEDOUT;                                 \
      else {			                                                       \
        timedout = CRITSECT_INVALID;                                  \
        VBX_print("  CRITSECT_TIMEDWAIT_ENTER: retval %d last error = %d\n", retval, GetLastError());\
      }                                                               \
    } else {                                                          \
      timedout = CRITSECT_NOERROR;                                    \
      if (predicate && !timedout) {                                   \
        ResetEvent(cs->event);                                        \
        Sleep(1);                                                     \
      }																                                               \
    }                                                                 \
  }                                                                   \
  if (!timedout && retval == WAIT_OBJECT_0) {                         \
    retval = WaitForSingleObject(*cs->mutexPtr, INFINITE);            \
  }                                                                   \
  ResetEvent(cs->event);                                              \
}																							 

typedef struct CritSect_s {
  pthread_mutex_t * mutexPtr;
  HANDLE            event;
  IMemory         * memory;
} CritSect_t, *CRITSECT;

#endif

CRITSECT CRITSECT_create(IMemory * memory, CRITSECT parent);
void     CRITSECT_destroy(CRITSECT cs);

#define CRITSECT_DECLARE(C)                CRITSECT  C;
#define CRITSECT_CREATE(C,M,P)             ((C) = CRITSECT_create(M, P))
#define CRITSECT_DESTROY(C)                if (C) CRITSECT_destroy(C)
#define CRITSECT_EXISTS(C)                 (C)
#define CRITSECT_ASSIGN(C,CS)              (C) = (CS)
#define TIMESPEC                           struct timespec
#define CRITSECT_SET_TIMESPEC(T,USEC)      (T).tv_sec = (USEC)/1000000, (T).tv_nsec = ((USEC)%1000000)*1000

#define MUTEX_DECLARE(M)                   pthread_mutex_t M;
#define MUTEX_INIT(MP, ATTR)               !pthread_mutex_init(MP, ATTR)
#define MUTEX_DESTROY(MP)                  ((MP) && !pthread_mutex_destroy(MP))
#define MUTEX_LOCK(MP)                     pthread_mutex_lock(MP);
#define MUTEX_UNLOCK(MP)                   pthread_mutex_unlock(MP);

#else /* defined VBX_SINGLE_THREAD */

    /**** Single-Threaded Version ****/

#define CRITSECT_DECLARE(C)
#define CRITSECT_CREATE(C,M,P)             TRUE
#define CRITSECT_DESTROY(C)
#define CRITSECT_EXISTS(C)                 TRUE
#define CRITSECT_ASSIGN(C,CS)
#define CRITSECT_ENTER(C)
#define CRITSECT_WAIT_ENTER(C,P)
#define CRITSECT_TIMEDWAIT_ENTER(C,W,T,P)  (T) = ((P) ? CRITSECT_TIMEDOUT : CRITSECT_NOERROR)
#define CRITSECT_LEAVE(C)
#define CRITSECT_SIGNAL(C)
#define TIMESPEC                           Int
#define CRITSECT_SET_TIMESPEC(T,USEC)      (T) = (USEC)

#define MUTEX_DECLARE(MP)
#define MUTEX_INIT(MP, ATTR)               TRUE
#define MUTEX_DESTROY(MP)                  TRUE
#define MUTEX_LOCK(MP)
#define MUTEX_UNLOCK(MP)

#endif /* not defined VBX_SINGLE_THREAD */

#ifdef __cplusplus
}
#endif
#endif  /* _CRITSECT_H_ */
