/******************************************************************************
 * 
 * File:         vbxtypes.h
 *
 * Description:  Basic Type and Constant Definitions for VISE
 *
 *****************************************************************************/

#ifndef _VBXTYPES_H
#define _VBXTYPES_H

#include <limits.h>

/*
 * Basic type definitions:
 * 
 * In order to preserve as much portability as possible, code should
 * refrain from relying on properties such as overflow and underflow
 * behavior.
 *
 * V_Byte   At least 8 bits, unsigned.
 * V_Int    At least 16 bits, two's compliment.
 * V_Uns    At least 16 bits, unsigned.
 * V_Long   At least 32 bits, two's compliment.
 * V_ULong  At least 32 bits, unsigned.
 * V_Void   The empty type
 * V_Real   At least single precision (32 bit) floating point
 * V_Double At least double precision (64 bit) floating point (unused)
 * V_Bool   Holds only TRUE (1) or FALSE (0)
 *
 * By definition, the sizeof(type) function returns the length of a type
 * in bytes, where a byte is defined as the smallest unit of addressable
 * memory (NOT necessarily 8 bits!).  Thus, sizeof() is useless for 
 * determining the sizes of the basic types in portable terms, so we must
 * define those sizes (XXXXSIZE) here in terms of 8 bit wide chunks.
 */

#define V_Void                void         /* The void return type for functions */

/* The Basic Types */

typedef char                  V_Char;      /* Character */
#define VCHARSIZE      1

typedef unsigned char         V_Byte;      /* Smallest unit of addressable memory */
#define VBYTESIZE      VCHARSIZE
#define VBYTEMAX       UCHAR_MAX

typedef short                 V_Int;       /* 16 (or more) bit signed integer */
#define VINTSIZE       2
#define VINTMAX        SHRT_MAX
#define VINTMIN        SHRT_MIN

typedef unsigned short        V_Uns;       /* 16 (or more) bit unsigned integer */
#define VUNSSIZE       VINTSIZE
#define VUNSMAX        USHRT_MAX

typedef int                   V_Long;      /* 32 bit signed integer */
#define VLONGSIZE      4
#define VLONGMAX       INT_MAX
#define VLONGMIN       INT_MIN

typedef unsigned int          V_ULong;     /* 32 bit unsigned integer */
#define VULONGSIZE     VLONGSIZE
#define VULONGMAX      UINT_MAX

typedef float                 V_Real;      /* 32 (or more) bit floating point number */
#define VREALSIZE      4

typedef double                V_Double;    /* 64 (or more) bit floating point number (unused) */
#define VDOUBLESIZE    8

typedef unsigned short        V_Bool;      /* TRUE or FALSE */
#define VBOOLSIZE      2

typedef V_Byte *              V_Ptr;       /* Generic pointer type */
typedef V_Int                 (*V_Fcn)();  /* Generic function type */

/*
 * Messages between the components of VISE are conceived of as streams
 * of atomic message elements (AME).  Whatever the physical implementation
 * of the communication system, such atomic message elements must LOGICALLY
 * be at least sixteen bits wide, but can be wider.  Their size, VAMESIZE,
 * is thus the LOGICAL width of the communications system.  IT MUST BE THE
 * SAME THROUGHOUT THE SYSTEM; two successive AMEs sent from one component
 * must not show up at another packed together in a larger AME; AMEs are
 * truly system-wide atoms.  VAMESIZE defines the width of an AME as a multiple
 * of eight-bit chunks.  IT MUST BE AT LEAST 2, AND IS THE SAME FOR EVERY
 * COMPONENT OF THE SYSTEM.
 */
#define VAMESIZE       2

/*
 * Messages appear to a given processor in the system as arrays of message
 * elements, of type V_Mel.  A V_Mel must be at least as large as an
 * AME (VMELSIZE >= VAMESIZE).  There is no advantage to its being larger
 * but sometimes it will have to be, simply because the processor cannot
 * address a smaller unit.  It does NOT have to be the same in all the
 * machines in the system.
 */
typedef V_Uns          V_Mel;
#define VMELSIZE       VUNSSIZE

  /* Boolean type values */
#ifndef TRUE
#define TRUE  ((V_Bool) 1)
#endif
#ifndef FALSE
#define FALSE ((V_Bool) 0)
#endif

#endif /* _VBXTYPES_H */
