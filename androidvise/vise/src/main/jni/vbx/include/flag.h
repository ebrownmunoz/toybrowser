/* flag.h - flag synchronization facility for threaded programs
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _FLAG_H_
#define _FLAG_H_

#include "pthr.h"
#include "types.h"
#include "critsect.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

 /* Flag */
typedef struct FLAG_s * FLAG;

 /* Flag Attributes */
typedef struct FLAG_Att_s {
  IMemory      * memory;    /* The memory allocator */
  Bool           notify;    /* Send the receiver notification in the mail */
  Short          mbId;      /* Id of receiver's mailbox */
  Short          packId;    /* Id of packet to send to receiver */
  Short          packLen;   /* Length of packet to send to receiver */
  Uns            priority;  /* Priority level of packet */
} FLAG_Att_t, * FLAG_Att;

 /* Flag operations */
extern FLAG_Att  FLAG_createAttr(IMemory * memory, Bool notify, Short mbId, Short packId, Short packLen, Uns priority);
extern void      FLAG_destroyAttr(FLAG_Att attributes);
extern FLAG      FLAG_create(IMemory * memory, FLAG_Att attributes);
extern void      FLAG_destroy(FLAG flag);
extern Bool      FLAG_state(FLAG flag);
extern Bool      FLAG_raise(FLAG flag);
extern Bool      FLAG_raiseAndWait(FLAG flag);
extern Bool      FLAG_raiseAndLinger(FLAG flag, TIMESPEC * timeout);
extern Bool      FLAG_lower(FLAG flag);
extern Bool      FLAG_waitAndLower(FLAG flag);
extern Bool      FLAG_lingerAndLower(FLAG flag, TIMESPEC * timeout);

#ifdef __cplusplus
}
#endif
#endif
