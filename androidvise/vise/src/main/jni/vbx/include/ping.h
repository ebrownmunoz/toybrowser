/* ping.h - Cross-platform infrastructure for doing an ICMP ping
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/

#ifndef _PING_H_
#define _PING_H_

#include "pthr.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PING_BUFSIZ     (1024)
#define PING_MSEC_DFLT  (1000)

Int  PING_open();
Bool PING_ping(Int fd, Char *host, Bool verbose, Int msec);
void PING_close(Int fd);

#ifdef __cplusplus
}
#endif
#endif
