/* dyn.h - dynamically load and link libraries
 *---------------------------------------------------------------------------*
 * Voxware, Inc. (dcv)
 *---------------------------------------------------------------------------*/

#ifndef _DYN_H_
#define _DYN_H_

#include <stdio.h>
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

#if   defined(WIN32)
#define DYN_MAX_NAME_LEN  512
typedef HANDLE DYN_Library;
typedef void * DYN_Symbol;
#elif defined(LINUX)
typedef void * DYN_Library;
typedef void * DYN_Symbol;
#endif

#if defined(WIN32) || defined(LINUX)
extern DYN_Library   DYN_open(const Char * filename);
extern Char        * DYN_error(void);
extern DYN_Symbol    DYN_symbol(DYN_Library library, Char * symbol);
extern Int           DYN_close(DYN_Library library);
#endif

#ifdef __cplusplus
}
#endif
#endif  /* ndef _DYN_H_ */
