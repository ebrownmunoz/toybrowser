#ifndef VISECMDS_

/******************************************************************************
 * 
 * File:         visecmds.h
 *
 * Description:  Enumeration of the VISE commands
 *
 *****************************************************************************/

#define VISECMDS_

#include "vbxtypes.h"

#ifdef __cplusplus
extern "C" { 
#endif

typedef enum visecmd_e {
  _QUERY_STATE,              /* command #0 */
  _LOAD_AND_GO,              /* command #1 */
  _RECOGNIZE,                /* command #2 */
  _TRAINWORD,                /* etc. */
  _SEGMENTWORD,
  _LISTEN,
  _VISEINIT,
  _WARMSTART,
  _DLOADGRAMMAR,
  _DELGRAMMAR,
  _DLOADWORD,                /* command #10 */
  _ULOADWORD,
  _DELWORD,
  _DLOADTEMPLATE,
  _ULOADTEMPLATE,
  _DLOADWILDCARD,
  _DLOADJIN,
  _ULOADJIN,
  _DLOADAMP,
  _ULOADAMP,
  _BEEP,                     /* command #20  Issue a beep */
  _SETPARAMETERS,            /* Set selected parameters */
  _ECHO,                     /* Echo received message verbatim */
  _VISEIDLE,                 /* Dummy command for abort response */
  _GETPARAMETERS,            /* Get VISE parameter block */
  _STARTLISTENING,           /* Start up the ADC and jin maker */
  _STOPLISTENING,            /* Shut off the ADC and jin maker */
  _SETTIME,                  /* Set the origin of the jin timer */
  _GETTIME,                  /* Get the current jin write time */
  _REPLACEWORD,              /* Replace one word with another */
  _GETAUDIOFRAMES,           /* command #30  Get frames of sample data */
  _PLAYDATA,                 /* Play back specified type of data */
  _SAMTOCELP,                /* Convert frames of samples into CELP set */
  _GETFACILITIES,            /* Have dispatcher report available facilities */
  _SETAUDIO,                 /* Set codec control mode parameters */
  _STARTTRAININGWORD,        /* Set up to accumulate training data for a word */
  _FINISHTRAININGWORD,       /* Train a word from accumulated training data */
  _ACOUSTICFEATURES,         /* A frame of acoustic features */
  _AUDIOFRAME,               /* A frame of audio sample data */
  _AMPLITUDE,                /* Amplitude of audio signal */
  _VUMETERON,
  _VUMETEROFF,               /* command #40 */
  _GETSEQUENCENUMBER,
  _DESTROY,
  _EXCEPTION,                /* Reports an exception */
  _ACKNOWLEDGMENT,           /* Acknowledges receipt of a command */
  _ABORT,
  _NOCOMMAND,
  _SOURCEINIT,
  _DLOADARC,                 /* Download and add an arc to a grammar */
  _DELARC,                   /* Delete an arc from a grammar */
  _ENROLLWORD,               /* Create a model for a word */
  _SETTIMEOUT,               /* Set the recognition timeout */
  _STARTCALIBRATION,         /* Start noise calibration mode in Front End */
  _STOPCALIBRATION,          /* Stop noise calibration mode in Front End */
  NUMVISECOMMANDS
} V_Cmd;

extern V_Char * VISECMD_string(V_Cmd command);

#ifdef __cplusplus
}
#endif

#endif /* VISECMDS_ */
