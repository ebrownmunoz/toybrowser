/* thrset.h - functions to manage sets of cloned threads
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _THRSET_H_
#define _THRSET_H_

#include "mbox.h"

/* Useful messages */
#define THR_START        ((OBJT) 1)
#define THR_STOP         ((OBJT) 2)
#define THR_DONE         ((OBJT) 3)
#define THR_RESET        ((OBJT) 4)

typedef pthread_attr_t *THR_Attr;
typedef pthread_t       THR;

/* The argument passed to the start function of each thread */
typedef struct thrset_arg_s {
  Int               index;  /* Index of the thread in the thread set */
  struct thrset_s * set;    /* The thread set this thread belongs to */
} thrset_arg_t, * THRSET_Arg;

/* A set of cloned threads */
typedef struct thrset_s {
  IMemory    * memory;      /* The memory allocator */
  Int          num;         /* Number of threads in the set */
  Char       * name;        /* The name of the thread set */
  THR_Attr     threadAttr;  /* The thread attributes */
  MBOX_Att     inBoxAttr;   /* The input mailbox attributes */
  pthread_t  * thread;      /* Array of threads */
  THRSET_Arg   arg;         /* Array of thread arguments */
  MBOX       * inBox;       /* Array of corresponding input mailboxes */
  MBOX         outBox;      /* The output mailbox */
  Ptr          environment; /* User-supplied environment structure */
} thrset_t, * THRSET;

extern THR_Attr  THR_createAttr(void);
extern void      THR_destroyAttr(THR_Attr attr);
extern void      THR_setAttr(THR_Attr attr, Int inheritSched, Int schedPolicy, Int priority, Int stackSize);
extern THRSET    THRSET_create(IMemory * memory, Char * name, THR_Attr threadAttr, MBOX_Att inBoxAttr, MBOX outBox);
extern void      THRSET_destroy(THRSET set);
extern void      THRSET_start(THRSET threads, Int num, pthread_startroutine_t startFcn, Ptr environment);
extern void      THRSET_stop(THRSET threads);

/* The Ith thread in set S */
#define THRSET_thread(S,I)            ((S)->thread[I])
/* Send the message M TO the Ith thread in set S */
#define THRSET_send(S,I,M)            MBOX_insert((S)->inBox[I], (OBJT) (M))
/* Send the message M FROM a thread in set S */
#define THRSET_thrSend(S,M)           MBOX_insert((S)->outBox, (OBJT) (M))
/* Receive a message FROM a thread in set S */
#define THRSET_recv(S)                MBOX_remove((S)->outBox)
#define THRSET_recvWithTimeout(S,T)   MBOX_removeWithTimeout((S)->outBox, (T))
/* Receive a message TO the Ith thread in set S */
#define THRSET_thrRecv(S,I)           MBOX_remove((S)->inBox[I])

/* The thread index */
#define THRSET_index(ARG)             (((THRSET_Arg) ARG)->index)
/* The thread set */
#define THRSET_set(ARG)               (((THRSET_Arg) ARG)->set)
/* The user-specified environment */
#define THRSET_environment(ARG)       (THRSET_set(ARG)->environment)

#endif  /* _THRSET_H_ */
