/******************************************************************************
 * 
 * File:         VISECOMP.H
 *
 * Description:  VISE Component Global Names
 *
 *****************************************************************************/

#ifndef VISECOMP_

#define VISECOMP_

#define  VISECOMP_FE        "FE"
#define  VISECOMP_SE        "SE"
#define  VISECOMP_RC        "RC"
#define  VISECOMP_RCP_MSG   "RCPM"
#define  VISECOMP_RCP_EVENT "RCPE"

/* Priorities definition */
enum priority_e {ABORT_PRIORITY, COMMAND_PRIORITY, DATA_PRIORITY, NUMPRIORITIES};

#endif
