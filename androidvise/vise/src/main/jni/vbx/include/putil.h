/* putil.h - functions for getting things into network-normal byte order
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: putil.h[1.0] Mon Apr  7 23:53:40 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _PUTIL_H_
#define _PUTIL_H_
#include "types.h"

Uns    htoni(Int val);
Int    ntohi(Uns val);
Uns    htonf(Float val);
Float  ntohf(Uns val);
UShort htonss(Short val);
Short  ntohss(UShort val);

#endif /* _PUTIL_H_ */
