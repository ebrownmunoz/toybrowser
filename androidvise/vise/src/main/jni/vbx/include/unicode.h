/* unicode.h - CString/Unicode interconversion utilities
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _UCODE_H_
#define _UCODE_H_

#include <stdio.h>
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

extern Bool     UCODE_getFromCString(const Char * string, UShort * unicode, UInt * size);
extern UShort * UCODE_fromCString(const Char * string, UInt * size);

extern Bool     UCODE_getCStringFrom(Char * string, const UShort * unicode, UInt size);
extern Char   * UCODE_toCString(const UShort * unicode, UInt size);

#ifdef __cplusplus
}
#endif
#endif              /* ndef _UCODE_H_ */
