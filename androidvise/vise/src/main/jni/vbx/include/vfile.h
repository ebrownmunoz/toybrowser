/* vfile.h
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * Recognizer/voice file parsing module
 *
 * HISTORY
 * 07-Jul-96 (cav) Updated RRLee's old version for use in the SAPI project
 * 17-Jan-97 (dcv) Added VSStreamData()
 * 04-Nov-97 (ret) Consolidated from CAV's RECFILE.
 *---------------------------------------------------------------------------*/
#ifndef _VFILE_H_
#define _VFILE_H_

#include "ifaces.h"
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* Silence vocabulary Id */
#define UPHEADSILCLASSID              0

#define VFILE_SUCCESS                 0
#define VFILE_ILLEGAL_MODE            1
#define VFILE_OPEN_FAILS              2
#define VFILE_ENOMEM                  3
#define VFILE_NO_FREE_HANDLES         4
#define VFILE_INVALID_HANDLE          5
#define VFILE_INVALID_MARK            6
#define VFILE_INVALID_STRUCTURE_TYPE  8
#define VFILE_INVALID_STRUCTURE_ATOM  9
#define VFILE_FILE_READ_ERROR         10
#define VFILE_END_OF_FILE             11
#define VFILE_INVALID_TEXT            12
#define VFILE_FILE_WRITE_ERROR        13
#define VFILE_BLOCK_CLOSED            14
#define VFILE_INVALID_VARIABLE_SIZE   15
#define VFILE_SEEK_ERROR              16

#ifndef countof
#define countof(A) (sizeof A / sizeof A[0])
#endif

/* RecFile Debugging Macros */

#define PRINT_FUPATHEAD(x) {                                                 \
  VBX_print("FUPATHEAD:\n");                                                 \
  VBX_print("  bVocabNameLength:     %d\n", (Int)(x)->bVocabNameLength);     \
  VBX_print("  bDiscreteMinCount:    %d\n", (Int)(x)->bDiscreteMinCount);    \
  VBX_print("  bContinuousMinCount:  %d\n", (Int)(x)->bContinuousMinCount);  \
  VBX_print("  wAlwaysFFFF:          %d\n", (Int)(x)->wAlwaysFFFF);          \
  VBX_print("  wAlwaysZero:          %d\n", (Int)(x)->wAlwaysZero);          \
  VBX_print("  lWildcardSum:         %d\n", (Int)(x)->lWildcardSum);         \
  VBX_print("  lWildcardSumSquares:  %d\n", (Int)(x)->lWildcardSumSquares);  \
  VBX_print("  wWildcardCount:       %d\n", (Int)(x)->wWildcardCount);       \
  VBX_print("  wScriptOrigin:        %d\n", (Int)(x)->wScriptOrigin);        \
  VBX_print("  wScriptOffset:        %d\n", (Int)(x)->wScriptOffset);        \
  VBX_print("  bDateLength:          %d\n", (Int)(x)->bDateLength);          \
  VBX_print("  bNameLength:          %d\n", (Int)(x)->bNameLength);          \
  VBX_print("  wNonAPSPassCount:     %d\n", (Int)(x)->wNonAPSPassCount);     \
  VBX_print("  wNumberPatterns:      %d\n", (Int)(x)->wNumberPatterns);      \
  VBX_print("  bNumberParms:         %d\n", (Int)(x)->bNumberParms);         \
}

#define PRINT_FUPATPARM(x) {                                                 \
  VBX_print("FUPATPARM:\n");                                                 \
  VBX_print("  bNumber:              %d\n", (Int)(x)->bNumber);              \
  VBX_print("  wValue:               %d\n", (Int)(x)->wValue);               \
  VBX_print("  bControl:             %d\n", (Int)(x)->bControl);             \
}

#define PRINT_FUPATE(x) {                                                    \
  VBX_print("FUPATE:\n");                                                    \
  VBX_print("  lSum:                 %d\n", (Int)(x)->lSum);                 \
  VBX_print("  lSquares:             %d\n", (Int)(x)->lSquares);             \
  VBX_print("  wCount:               %d\n", (Int)(x)->wCount);               \
  VBX_print("  wTrainingCount:       %d\n", (Int)(x)->wTrainingCount);       \
  VBX_print("  wTrainingSavedCount:  %d\n", (Int)(x)->wTrainingSavedCount);  \
  VBX_print("  bNKernWordMod:        %d\n", (Int)(x)->bNKernWordMod);        \
  VBX_print("  bTemplateCount:       %d\n", (Int)(x)->bTemplateCount);       \
  VBX_print("  bNameCount:           %d\n", (Int)(x)->bNameCount);           \
}

#define PRINT_FGRAPH(x) {                                                    \
  VBX_print("FGRAPH:\n");                                                    \
  VBX_print("  wGraphSize:           %d\n", (Int)(x)->wGraphSize);           \
  VBX_print("  wGrammarID:           %d\n", (Int)(x)->wGrammarID);           \
  VBX_print("  wNumberOfArcs:        %d\n", (Int)(x)->wNumberOfArcs);        \
  VBX_print("  wNumberOfNodes:       %d\n", (Int)(x)->wNumberOfNodes);       \
}

#define PRINT_FGRAPHARC(x) {                                                 \
  VBX_print("FGRAPHARC:\n");                                                 \
  VBX_print("  uSourceNode:          %d\n", (Int)(x)->uSourceNode);          \
  VBX_print("  uDestNode:            %d\n", (Int)(x)->uDestNode);            \
  VBX_print("  uNumberOfWords:       %d\n", (Int)(x)->uNumberOfWords);       \
}

#define PRINT_FLOFF(x) {                                                     \
  VBX_print("FLOFF: bLength: %d  lOffset: %d\n", (Int)(x)->bLength, (x)->lOffset); \
}

#define PRINT_FLOFFSTR(x, s) {                                               \
  Char buf[256];                                                             \
  PRINT_FLOFF(x);                                                            \
  if (VSGetText(s, x, (V_Byte *)buf) != VFILE_SUCCESS) {                        \
    WARN_BRA_ "Can't find FLOFF in stream %p", s _KET;                       \
  } else {                                                                   \
    VBX_print("FLOFFSTR: \"%s\"\n", buf);                                    \
  }                                                                          \
}

#define PRINT_FVOCABTRANS(x) {                                               \
  VBX_print("FVOCABTRANS:\n");                                               \
  VBX_print("  wNumberWords:         %d\n", (Int)(x)->wNumberWords);         \
  VBX_print("  wNumberDiscreteWords  %d\n", (Int)(x)->wNumberDiscreteWords); \
  VBX_print("  ");  PRINT_FLOFF(&x->loVocabularyName);                       \
  VBX_print("  ");  PRINT_FLOFF(&x->loHRFInitiator);                         \
  VBX_print("  ");  PRINT_FLOFF(&x->loHRFSeparator);                         \
  VBX_print("  ");  PRINT_FLOFF(&x->loHRFTerminator);                        \
  VBX_print("  ");  PRINT_FLOFF(&x->loVRFInitiator);                         \
  VBX_print("  ");  PRINT_FLOFF(&x->loVRFSeparator);                         \
  VBX_print("  ");  PRINT_FLOFF(&x->loVRFTerminator);                        \
  VBX_print("  ");  PRINT_FLOFF(&x->loDRFInitiator);                         \
  VBX_print("  ");  PRINT_FLOFF(&x->loDRFSeparator);                         \
  VBX_print("  ");  PRINT_FLOFF(&x->loDRFTerminator);                        \
  VBX_print("  wYesChoiceID:         %d\n", (Int)(x)->wYesChoiceID);         \
  VBX_print("  wNoChoiceID:          %d\n", (Int)(x)->wNoChoiceID);          \
  VBX_print("  wExitChoiceID:        %d\n", (Int)(x)->wExitChoiceID);        \
}

#define PRINT_FVOCABTRANSE(x) {                                              \
  VBX_print("FVOCABTRANSE:\n");                                              \
  VBX_print("  wMultipleTemplateID:  %d\n", (Int)(x)->wMultipleTemplateID);  \
  VBX_print("  bDiscreteWord:        %d\n", (Int)(x)->bDiscreteWord);        \
}
 
/*----------------------------------------------------------------------------*
 * Block data loaded to the recognizer is preceded by a size and checksum 
 * argument. This data is presented and received as bytes with big-endian 
 * byte order. The following three functions compute and manipulate the
 * checksum.
 *----------------------------------------------------------------------------*/

/* Checksum Structure used for Block Read/Writes */

typedef struct tVCHECKSUM {
  V_ULong     lBlockSize;          /* block size in bytes including checksum */
  V_Uns       wBlockChecksum;      /* block checksum */
} VCHECKSUM;

#define VCHECKSUM_SIZE  6          /* bytes in packed header */

/*----------------------------------------------------------------------------*
 * Initialize checksum structure. If pBuffer argument is NULL, *pChecksum is 
 * initialized to 0, else member values are unpacked from first VCHECKSUM_SIZE 
 * bytes of bufPtr
 *----------------------------------------------------------------------------*/

UShort VFChecksumOpen(VCHECKSUM *csPtr, UChar *bufPtr);
UShort VFChecksumContinue(VCHECKSUM *csPtr, UShort bufSiz, UChar *bufPtr);

/*----------------------------------------------------------------------------*
 * Adjust checksum for output.  Given a non-NULL, bufPtr, an ordered, packed 
 * copy of the checksum is written into VCHECKSUM_SIZE bytes of bufPtr
 *----------------------------------------------------------------------------*/

UShort VFChecksumClose(VCHECKSUM *csPtr, UChar *bufPtr);


/* Header Record for a block (largely hidden by the abstraction)  */
typedef struct tFBLOCKHEAD {
  V_Uns       wBlockID;                 /* TR_something                     */
  V_Uns       wClassID;
  VCHECKSUM   vck;
} FBLOCKHEAD;

#define RS_FBLOCKHEAD       0           /* structure reference ID           */

/* TR_KEY: the first block of a REC or VOI file */
typedef struct tFKEY {
  V_ULong     lMask;                    /* TR_KEYBIT_something combinations */
} FKEY;

#define RS_FKEY             1           /* structure reference ID           */

/* TR_VERSION: Actual length varies */
typedef struct tFSTRING {
  V_Byte      bStringLength;
  V_Byte      bStringData[255];
} FSTRING;

#define RS_FSTRING          2           /* structure reference ID          */

/* 1st member of a TR_SWITCH block */
typedef struct tFSWITCH {
  V_Uns       wNumberEntries;           /* number of FSWITCHE which follow */
} FSWITCH;

#define RS_FSWITCH          3           /* structure reference ID */

typedef struct tFSWITCHE {
  V_Uns       wSwitchNumber;
  V_Uns       wSwitchValue;
} FSWITCHE;

#define RS_FSWITCHE         4           /* structure reference ID          */

/* 1st member of a TR_UPAT block */
#define EMBEDDED_NAME_SIZE  38          /* maximum size of some names      */

typedef struct tFUPATHEAD {
  V_Byte      bVocabNameLength;
  V_Byte      bVocabName[EMBEDDED_NAME_SIZE];
  V_Byte      bGender;
  V_Byte      bDiscreteMinCount;
  V_Byte      bContinuousMinCount;
  V_Uns       wAlwaysFFFF;
  V_Uns       wAlwaysZero;
  V_ULong     lWildcardSum;             /* Sum of FUPAT.lSum    */
  V_ULong     lWildcardSumSquares;      /* Sum of FUPAT.lSquares  */
  V_Uns       wWildcardCount;           /* Sum of FUPAT.wTrainingCount */
  V_Uns       wScriptOrigin;
  V_Uns       wScriptOffset;
  V_Byte      bDateLength;
  V_Byte      bDate[EMBEDDED_NAME_SIZE];
  V_Byte      bTrainScheme;
  V_Byte      bNameLength;
  V_Byte      bName[EMBEDDED_NAME_SIZE];
  V_Byte      bReserved;
  V_Uns       wNonAPSPassCount;
  V_Uns       wNumberPatterns;          /* total number of words */
  V_Byte      bNumberParms;             /* number of FUPATPARMs */
} FUPATHEAD;

#define RS_FUPATHEAD        5           /* structure reference ID */

/* FUPATHEAD.bGender types */
#define UPAT_GENDER_FEMALE  1
#define UPAT_GENDER_MALE    2
#define UPAT_GENDER_UNISEX  3
#define UPAT_GENDER_GIRL    4
#define UPAT_GENDER_BOY     5
#define UPAT_GENDER_CHILD   6

/* FUPATHEAD.bTrainScheme values */
#define TRAIN_SCHEME_INCRM  0
#define TRAIN_SCHEME_BATCH  1
#define TRAIN_SCHEME_MIXED  2


/* These elements follow UPATHEAD */
typedef struct tFUPATPARM {
  V_Char      bNumber;
  V_Uns       wValue;
  V_Byte      bControl;                 /* !0 if VULCAN cares about it */ 
} FUPATPARM;

#define RS_FUPATPARM        6           /* structure reference ID     */

/* User Patterns have a special read method. See WORDSTAT */
typedef struct tFUPAT {
  V_ULong     lSum;                     /* weighted sum of scores for word */
  V_ULong     lSquares;                 /* weighted sum of squares of scores */
  V_Uns       wCount;                   /* weighted training count */
  V_Uns       wTrainingCount;           /* # of times word has been trained */
  V_Uns       wTrainingSavedCount;      /* internal count */
  V_Byte      bNKernWordMod;            /* word model kernel count */

  /* followed by bNKernWordMod * UPAT_WM_SIZE bytes of kernel count*/
  /* followed by BYTE template count */
  /* followed by bNKernWordMod*16 bytes of template */
  /* followed by BYTE word name size */
  /* followed by word name size bytes of word name */
} FUPAT;

#define UPAT_MAX_KERNELS   42
#define UPAT_NUM_PARMS     16
#define UPAT_WM_SIZE       2
#define UPAT_MAX_NAME      256

typedef struct tFUPATE {
  V_ULong     lSum;                     /* weighted sum of scores for word */
  V_ULong     lSquares;                 /* weighted sum of squares of scores */
  V_Uns       wCount;                   /* weighted training count */
  V_Uns       wTrainingCount;           /* # of times word has been trained */
  V_Uns       wTrainingSavedCount;      /* internal count */
  V_Byte      bNKernWordMod;            /* word model kernel count */
  /* followed by bNKernWordMod * UPAT_WM_SIZE bytes of kernel count*/
  V_Byte      bKernelDwells[UPAT_WM_SIZE * UPAT_MAX_KERNELS]; 
  /* followed by BYTE template count */
  V_Byte      bTemplateCount;
  /* followed by bNKernWordMod*16 bytes of template */
  V_Byte      bTemplate[UPAT_NUM_PARMS * UPAT_MAX_KERNELS];
  /* followed by BYTE word name size */
  V_Byte      bNameCount;
  /* followed by word name size bytes of word name */
  V_Byte      bName[UPAT_MAX_NAME];
} FUPATE;

/* Maximum Word Model Size (includes 255 char name) */

#define UPAT_MAX_SIZE  (2 * sizeof(V_ULong) + 3 * sizeof(V_Uns)\
 + (3  + (UPAT_WM_SIZE + UPAT_NUM_PARMS)* UPAT_MAX_KERNELS + 255) * sizeof(V_Byte))

#define RS_FUPATE           13          /* structure reference ID */

/* Length/Offset structure used to describe names in REC files */
typedef struct tFLOFF {
  V_Byte       bLength;
  V_ULong      lOffset;
} FLOFF;

/* First Element in TR_VOCABULARY_TRANS Block */
typedef struct  tFVOCABTRANS {
  V_Uns       wNumberWords;
  V_Uns       wNumberDiscreteWords;
  FLOFF       loVocabularyName;
  FLOFF       loHRFInitiator;
  FLOFF       loHRFSeparator;
  FLOFF       loHRFTerminator;
  FLOFF       loVRFInitiator;
  FLOFF       loVRFSeparator;
  FLOFF       loVRFTerminator;
  FLOFF       loDRFInitiator;
  FLOFF       loDRFSeparator;
  FLOFF       loDRFTerminator;
  V_Uns       wYesChoiceID;
  V_Uns       wNoChoiceID;
  V_Uns       wExitChoiceID;
} FVOCABTRANS;

#define RS_FVOCABTRANS      7           /* structure reference ID */

/* Subsequent Elements in Block */
typedef struct tFVOCABTRANSE {
  V_Uns       wMultipleTemplateID;
  V_Byte      bDiscreteWord;
  FLOFF       loWordName;
  FLOFF       loHostTranslation;
  FLOFF       loVoiceTranslation;
  FLOFF       loDisplayTranslation;
} FVOCABTRANSE;

#define RS_FVOCABTRANSE     8           /* structure reference ID */

/* First Element in TR_SCRIPT Block */
typedef struct tFSCRIPT {
  V_Uns       wEntryCnt;
  V_Byte      bDiscMinCnt;
  V_Byte      bContMinCnt;
} FSCRIPT;

#define RS_FSCRIPT          9           /* structure reference ID */

/* Subsequent Elements in Block */
typedef struct tFSCRIPTE {
  V_Uns       wGrammarID;
  V_Uns       wStartNode;
  V_Uns       wEndNode;
  V_Byte      bWordCnt;
} FSCRIPTE;

#define RS_FSCRIPTE         10          /* structure reference ID */

/* First Element in TR_GRAMMAR Block */
typedef struct tFGRAMMAR {
  V_Uns       wVocabulary;
  V_Uns       wDefaultCRTemplate;
  V_Uns       wDefaultHRTemplate;      
  FLOFF       bloGrammarName;
  V_Uns       wDefaultVRTemplate;      
  V_Uns       wDefaultDRTemplate;
} FGRAMMAR;

#define RS_FGRAMMAR         11          /* structure reference ID */

/* First Element in TR_GRAPH block */
typedef struct tFGRAPH {
    V_Uns     wGraphSize;
    V_Uns     wGrammarID;
    V_Uns     wNumberOfArcs;
    V_Uns     wNumberOfNodes;
} FGRAPH;

#define RS_GRAPH            12          /* structure reference ID */

/* First Element in TR_GRAM_NAMES Block */
typedef struct tFGENINFO {
  V_ULong     uGinclude;
  V_ULong     uAppName;
  V_ULong     uVersion;
  V_Uns       uNumIncludes;
  V_ULong     uIncludes;
  V_Uns       uNumGrams;
  V_Uns       uNumVocabs;
  V_Uns       uNumWords;
  V_Uns       uVISEsram;
  V_Uns       uVISETotal;
} FGENINFO;

#define RS_GENINFO  14

typedef struct tFGRAMINFO {
  V_ULong     uGramName;
  V_ULong     uSourceFile;
  V_ULong     uTimeDate;
  V_Uns       uInactive;
  V_Uns       uNumWords;
  V_Uns       uNumInstances;
  V_Uns       uComplexity;
} FGRAMINFO;
#define RS_GRAMINFO 15

typedef struct tFPHRGENINFO {
  V_Uns       uNumPhrases;
} FPHRGENINFO;

#define RS_PHRGENINFO   16

typedef struct tFPHRASEINFO {
  V_ULong     uPhraseHndl;
  V_Uns       uFileIndex;
  V_Uns       uFlags;
  V_Uns       uStage;
} FPHRASEINFO;

#define RS_PHRASEINFO   17

typedef struct tFLISTGENINFO {
  V_Uns       uNumLists;
} FLISTGENINFO;

#define RS_LISTGENINFO 18

typedef struct tFLISTINFO {
  V_ULong     uListHndl;
  V_Uns       uFlags;
  V_Uns       uStage;
  V_Uns       uMinCount;
  V_Uns       uMaxCount;
  V_ULong     uListPhrase;
  V_Uns       uListFileIndex;
  V_Uns       uNumElements;
  V_ULong     uFirstElement;
} FLISTINFO;

#define RS_LISTINFO 19

typedef struct tFGRAPHARC {
  V_Uns       uSourceNode;
  V_Uns       uDestNode;
  V_Uns       uNumberOfWords;
}  FGRAPHARC;

#define RS_GRAPHARC     20

typedef struct tFIDENT {
  FLOFF       bloApplicationName;
  V_Uns       wHostStartMsg;
  V_Uns       wVoiceStartMsg;
  V_Byte      bVCC;
  FLOFF       bloVersionName;
  V_ULong     ulVersionKey;
  V_Uns       wDisplayStartMsg;
  V_Uns       wFirstGrammar;
} FIDENT;

#define RS_IDENT     21

typedef struct  tFGRAMMARTRANS {
   V_Uns      wNumberWords;
   FLOFF      loHRFInitiator;
   FLOFF      loHRFSeparator;
   FLOFF      loHRFTerminator;
   FLOFF      loVRFInitiator;
   FLOFF      loVRFSeparator;
   FLOFF      loVRFTerminator;
   FLOFF      loDRFInitiator;
   FLOFF      loDRFSeparator;
   FLOFF      loDRFTerminator;
} FGRAMMARTRANS;

#define RS_FGRAMMARTRANS    22        /* structure reference ID */

typedef struct  tFGRAMMARTRANSE {
  V_Uns       wWordID;
  FLOFF       loHostTranslation;
  FLOFF       loVoiceTranslation;
  FLOFF       loDisplayTranslation;
} FGRAMMARTRANSE;

#define RS_FGRAMMARTRANSE   23        /* structure reference ID */

typedef struct tFGRAPHARCE {
  V_Uns       wWordID;
} FGRAPHARCE;

#define RS_GRAPHARCE        24        /* structure reference ID */

/* Generic V_Uns and V_ULong Readers */
typedef struct tFV_Uns {
  V_Uns       wWord;
} FV_Uns;

#define RS_FV_Uns           25

typedef struct tFV_ULong {
  V_ULong     lLong;
} FV_ULong;

#define RS_FV_ULong         26

typedef struct tFHINTGENINFO {
  V_Uns       uNumHints;
} FHINTGENINFO;

#define RS_FHINTGENINFO     27

typedef struct tFHINTINFOSTRUCT {
  V_Uns       uType;
  V_ULong     uHandleOffset;
  V_ULong     uHintOffset;
  V_ULong     uDescripOffset;
} FHINTINFOSTRUCT;

#define RS_FHINTINFOSTRUCT  28

typedef struct tPARSEHDR {
  V_Uns       wStateCount;
  V_ULong     lStateOffset;
} FPARSEHDR;

#define RS_FPARSEHDR        29
 
typedef struct tPARSEHDRE {
  V_ULong     lStateOffset;
} FPARSEHDRE;

#define RS_FPARSEHDRE       30

typedef struct tPARSEE {
  V_Uns       wStateData;
} FPARSEE;

#define RS_FPARSEE          31

typedef struct tTEMPLATEHDR {
  V_Uns       wTemplateCount;
  V_ULong     lTemplateOffset;
} FTEMPLATEHDR;

#define RS_FTEMPLATEHDR     32

typedef struct tTEMPLATEHDRE {
  V_ULong     lTemplateOffset;
} FTEMPLATEHDRE;

#define RS_FTEMPLATEHDRE    33

typedef struct tTEMPLATEE {
  V_Uns       wTemplateData;
} FTEMPLATEE;

#define RS_FTEMPLATEE       34

typedef struct tFLM {
  V_Uns       wVocabulary;
  V_Uns       wFirstSilence;
  V_ULong     lNumUnigrams;
  V_ULong     lNumBigrams;
  V_Uns       wNumNodes;
  V_Long      wLanguageWeight;
  V_Long      wUnigramWeight;
  V_Long      wWordInsertPenalty;
  V_Long      wPhoneInsertPenalty;
  V_Long      wSilInsertPenalty;
  V_Uns       wStartingWIID;
  V_Uns       wEndingWIID;
} FFLM;

#define RS_FFLM             35

typedef struct tUGRAM {
  V_Long      wProbability;
  V_Long      wBackoffWeight;
  V_Uns       wWordID;
  V_Uns       wNodeID;
  V_Uns       wNumSuccessors;
} FUGRAM;

#define RS_FUGRAM           36

typedef struct tBGRAM {
  V_Uns       wSuccessor;
  V_Long      wProbability;
} FBGRAM;

#define RS_FBGRAM           37

typedef struct tDICT {
  V_Uns       wNumEntries;
} FDICT;

#define RS_FDICT            38

typedef struct tDICTE {
  FLOFF       loWordName;
  FLOFF       loPhoneString;
  V_ULong     lAltPronProb;
  V_Uns       wUseContext;
} FDICTE;

#define RS_FDICTE           39

/* Used in VFExtractWordStatistics() */
typedef struct tWORDSTATS {
  V_Uns       wDataSize;
  V_Uns       wNameOffset;
  V_Byte      bNameSize;
} WORDSTATS;

/* VFILE file descriptor. */
typedef struct vfile_s * VFILE;

/* VFILE structure descriptor. */
typedef struct vfile_sdesc_s * VFILE_SDESC;

typedef struct vstream_s {
  Ptr         streamPtr;               /* voi/rec data stream origin */
  Ptr         currPosPtr;              /* current position in the stream */
  UInt        uMode;                   /* access mode */
  UInt        lFileSize;               /* size of file */
  Ptr         fileHandle;
  IMemory   * iMem;
} vstream_t, * VSTREAM;

/* VFILE file position descriptor. */
typedef V_ULong VFILE_MARK;

/* File Access/Control Routines */
VFILE    VFILE_open(IMemory * thisIMem, IFile * thisIFile, void * fileHandle, V_Uns mode);
V_Err    VFILE_rewind(VFILE vfile);
V_Err    VFILE_close(VFILE vfile);
IFile  * VFILE_interface(VFILE vfile);

/* Stream-specific routines */
Ptr      VFILE_streamData(VFILE vfile, UInt *dataSize);


/* Navigation Routines */
V_Err    VFILE_placeMark(VFILE vfile, VFILE_MARK * pMark);
V_Err    VFILE_gotoMark(VFILE vfile, VFILE_MARK * pMark);
V_ULong  VFILE_markDifference(VFILE_MARK * pMark1, VFILE_MARK * pMark2);
FBLOCKHEAD * VFILE_currentBlockHdr(VFILE vfile);
V_ULong  VFILE_currentBlockSize(VFILE vfile);

/* Input Routines */
V_Err    VFILE_getNextBlock(VFILE vfile, V_Uns * uBlockID, V_Uns * uClassID, V_ULong * ulSize);
V_Err    VFILE_readStructure(VFILE vfile, void * pStructure, V_Uns wCount, V_Uns sType);
V_Err    VFILE_readGeneralStructure(VFILE vfile, void * pStructure, V_Uns wCount, VFILE_SDESC pSDesc);
V_Err    VFILE_getText(VFILE vfile, FLOFF * pText, V_Byte * szBuffer);
V_Err    VFILE_readByte(VFILE vfile, V_ULong byteCount, V_Byte * pData);
V_Err    VFILE_extractWordStats(VFILE vfile, WORDSTATS * pwWordStats, V_Byte * buf);

/* Output Routines */
V_Err    VFILE_openBlock(VFILE vfile, V_Uns uBlockID, V_Uns uClassID);
V_Err    VFILE_openCurrentBlock(VFILE vfile);
V_Err    VFILE_closeBlock(VFILE vfile);
V_Err    VFILE_writeByte(VFILE vfile, V_ULong byteCount, V_Byte * pData);
V_Err    VFILE_writeStructure(VFILE vfile, void * pStructure, V_Uns wCnt, V_Uns wStructureType);
V_Err    VFILE_writeGeneralStructure(VFILE vfile, void * pStructure, V_Uns wCnt, VFILE_SDESC pSDesc);
V_Err    VFILE_insertStructure(VFILE vfile, void *pStructure, V_Uns wCnt, V_Uns wStructureType);
V_Err    VFILE_insertGeneralStructure(VFILE vfile, void *pStructure, V_Uns wCnt, VFILE_SDESC pSDesc);

#ifdef __cplusplus
}
#endif
#endif /* _VFILE_H_ */
