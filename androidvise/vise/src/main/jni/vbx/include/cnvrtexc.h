/* cnvrtexc.h - methods to convert HRESULTs to and from 16 bit VISE error codes
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * These macros convert all of the HRESULTs currently used by SAPI to and
 * from V_Err without loss.  They presuppose that success is zero in both
 * schemes.  A V_Err is actually an Int (32 bits), but its high-order 16 bits
 * must always be zero to permit sending it through the VBX messaging system.
 *
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _CNVRTEXC_H_
#define _CNVRTEXC_H_

#ifndef WIN32
#include "mstypes.h"
#endif
#include "vbxtypes.h"
#include "viseerr.h"

#define HRESULT_TO_VERR(H) (V_Err) (((((H) & 0x00070000) >> 3) | (H)) & 0x0000FFFF)
#define VERR_TO_HRESULT(V) (HRESULT) ((!(V)) ? (V) \
                                             : ((V) & 0x00008000) && !((V) & 0x00001000) ? (((V) & 0x0000E000) << 3) | ((V) & 0x00000FFF) | 0x80000000 \
                                                                                         : ((V) | 0x80000000))
#define VERR_IS_HRESULT(V) (!(V) || ((V) & 0x0000FFFF))

#endif /* _CNVRTEXC_H_ */
