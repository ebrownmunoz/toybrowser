#ifndef _ARCHIVE_H_
#define _ARCHIVE_H_

#include "types.h"
#include "sphdr.h"

#ifdef __cplusplus
extern "C" { 
#endif

Bool ARCHIVE_create(Char *archiveName, Char *srcDir);
Ptr  ARCHIVE_createMem(Char *archiveName, Char *targetDir, Int *byteCnt);
Bool ARCHIVE_extract(Ptr archive, Char *targetDir);
Bool ARCHIVE_load(Char *archiveFN, HDR hdr, Ptr *data, size_t *byteCnt);

#ifdef __cplusplus
}
#endif
#endif  /* ndef _ARCHIVE_H_ */
