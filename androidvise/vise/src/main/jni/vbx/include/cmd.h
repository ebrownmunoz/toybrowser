/*
 * cmd.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _CMD_H_
#define _CMD_H_

/* command codes */

/* the haphazard order of the entries in this
   enumeration is required for back compatability */

#ifdef __cplusplus
extern "C" {
#endif

enum commands {
  NODATA = 50,       /* 50  packetid > NODATA => dataless packet */
  ABORT,             /* 51  Abort processing this utterance */
  ABORT_ACK,         /* 52  Abort acknowledge */
  BWD_COMPLETE,      /* 53  Computation of backward results is complete */
  EXEC_BATCH,        /* 54  */                       /* Obsolete */
  DATA_START,        /* 55  Start sending data */
  EXEC_RECOGN,       /* 56  Start listening */
  DATA_STOP,         /* 57  Stop sending data */
  EXIT,              /* 58  Exit recognizer */
  FWD_COMPLETE,      /* 59  Computation of forward results is complete */
  NBEST_COMPLETE,    /* 60  Computation of nbest results is complete */
  REQBWD_RESULTS,    /* 61  Request backward results */
  REQFWD_RESULTS,    /* 62  Request forward results */
  REQNB_RESULTS,     /* 63  Request nbest results */
  REQUTT_FRAMES,     /* 64  */                       /* Obsolete */
  RDY_TO_LISTEN,     /* 65  Recognizer is ready to listen */
  RESET,             /* 66  Reset the DSP */
  CEP_START,         /* 67  Start sending CEP data from DSP */
  CEPWAV_START,      /* 68  Start sending CEPWAV data from DSP */
  FBS_READY,         /* 69  FBS is ready to listen */
  HOLD_PLAY,         /* 70  Pause transmission of play data to the DSP */
  RESUME_PLAY,       /* 71  Resume transmission of play data to the DSP */
  NO_HEAR,           /* 72  Recognizer did not hear utterance */
  HANG_UP,           /* 73  Hang up the telephone line */
  PICK_UP,           /* 74  Pick up the telephone line */
  RING_DETECT,       /* 75  Telephone ring is detected */
  UNDO,              /* 76  Undo (??) */
  WAV_START,         /* 77  Start sending WAV data from DSP */
  RESET_CODEC,       /* 78  Reset the CODEC */
  RESET_HIRSCH,      /* 79  Reset the Hirsch activity calculator */
  CALIBRATE_DONE,    /* 80  Calibration to background done */

  SAVECEP_ON = 101,  /* 101 Turn on saving of CEP data */
  SAVECEP_OFF,       /* 102 Turn off saving of CEP data */
  SAVEWAV_ON,        /* 103 Turn on saving of WAV data */
  SAVEWAV_OFF,       /* 104 Turn off saving of WAV data */
  LOG_ON,            /* 105 Turn on logging of recognition results */
  LOG_OFF,           /* 106 Turn off logging of recognition results */
  PROMPT_ON,         /* 107 Turn on prompting */
  PROMPT_OFF,        /* 108 Turn off prompting */
  EDIT_ENABLE,       /* 109 Enable (allow) user editing of results */
  EDIT_DISABLE,      /* 110 Disable (disallow) user editing of results */
  EDIT_ON,           /* 111 Turn on editing of results */
  EDIT_OFF,          /* 112 Turn off editing of results */

                     /* SAPI-oriented commands */
  PAUSE = 201,       /* 201 Pause a recognizer */
  RESUME             /* 202 Resume a recognizer */
};

/* status messages */
#define STAT_EOT -1

#ifdef __cplusplus
}
#endif
#endif  /* _CMD_H_ */
