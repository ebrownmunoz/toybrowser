#ifndef _PARSE_H_
#define _PARSE_H_

#include "types.h"
#include "hyp.h"
#include "vfile.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PARSE_NULL_ARC          0
#define PARSE_NULL_WORD         0

typedef enum { before, after } ParsePos_t;

/*---------------------------------------------------------------------------*
 * The successor structure describes the set of arcs connecting the current
 * state and the successor state in a parse table.  The arc field is
 * an exemplar of an arbitrarily large array of word_ID's, limited only by
 * the size of the vocabulary.  The last arc in a successor structure has
 * its high bit set.
 *
 * A PARSE_NULL_ARC arc indicates a NULL transition, one requiring no input.
 *
 * A PARSE_DEFAULT_ARC arc indicates a transition on any input; this makes
 * sense only for the last successor in a ParseState, and only as the
 * sole arc in a successor structure.
 *---------------------------------------------------------------------------*/

typedef struct {
  UShort state;  /* stateId */
  UShort arc[1]; /* wordId */
} ParseSuccessor_t, *SUCCESSOR;

/*---------------------------------------------------------------------------*
 * ParseState_t describes a state in the parse table.  The array declaration 
 * is phony; the array actually contains any number of successors.
 * A single successor with state = PARSE_TERMINAL_STATE marks a terminal
 * (acceptance) state; in this case, the parser returns the unique arc label.
 * The last successor state in a parse_state has its high bit set.
 *---------------------------------------------------------------------------*/

typedef struct {
  ParseSuccessor_t successor[1];
} ParseState_t, * PARSESTATE;

typedef struct parse_s      * PARSE;
typedef struct parseTable_s * PARSETABLE;

PARSE PARSE_create();
void  PARSE_destroy(PARSE parse);
Ptr   PARSE_loadTable(VFILE recFile);
void  PARSE_destroyTable(Ptr parse);
Bool  PARSE_parse(PARSE parse, HYP hyp, UInt maxWId, PARSETABLE parseTable, Ptr * parsePtr, UShort * index);

#ifdef __cplusplus
}
#endif
#endif  /* _PARSE_H_ */
