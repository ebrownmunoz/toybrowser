/* potbl.h - default attributes table for Post Office mailboxes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/

#ifndef _POTBL_H_
#define _POTBL_H_
#include "types.h"
#include "mbox.h"
#include "po.h"

/* Global default mailbox attributes table (ordered per mboxes enum in packets.h) */

MBOX_Att_t PO_attrTbl[] = {
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* MCT */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* MSGOUT */
  { NULL, CEP_MBOXSIZE, CEP_MBOXSIZE, 1 },       /* DATAIN */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* PRINT */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* PRNMSG */
  { NULL, CEP_MBOXSIZE, CEP_MBOXSIZE, 1 },       /* DATABUF */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* DSPSOC */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* FBSSOC_C */
  { NULL, CEP_MBOXSIZE, CEP_MBOXSIZE, 1 },       /* FBSSOC_D */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* GUI */
  { NULL, MSG_MBOXSIZE, MSG_MBOXSIZE, 1 },       /* RIT */
  { NULL, CEP_MBOXSIZE, CEP_MBOXSIZE, 1 }        /* RDDATA */
};

#endif /* _POTBL_H_ */
