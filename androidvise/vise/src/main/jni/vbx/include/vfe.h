/* vfe.h - VISE Feature Extractor Interface Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: vfe.h[1.0] Tue Jun  9 11:56:53 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _VFE_H_
#define _VFE_H_

#include "types.h"
#include "ifaces.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* VFE Descriptor */
typedef struct vfe_s * VFE;

/* VFE Methods */
extern VFE   VFE_create(IMemory * memory, IMail * mail, IAudioIn * audio);
extern void  VFE_destroy(VFE vfe);
extern Bool  VFE_startListening(VFE vfe, Int * status);
extern Bool  VFE_stopListening(VFE vfe, Int * status);
extern Bool  VFE_startCalibration(VFE vfe, Int maxFrames, Int * status);
extern Bool  VFE_stopCalibration(VFE vfe, Int * status);
extern Bool  VFE_getParameter(VFE vfe, UInt index, Int * value, Int * status);
extern Bool  VFE_setParameter(VFE vfe, UInt index, Int value, Int * status);
extern Bool  VFE_getSN(VFE vfe, SN * sn, Int * status);
extern Bool  VFE_turnVUMeterOn(VFE vfe, UInt receiverId, Int * status);
extern Bool  VFE_turnVUMeterOff(VFE vfe, Int * status);
extern Bool  VFE_getAudioData(VFE vfe, SN * startSN, SN *endSN, UShort ** data, Int * sampCnt, Int * status);

#ifdef __cplusplus
}
#endif
#endif /* _VFE_H_ */
