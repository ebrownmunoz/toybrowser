/* fname.h - figure out a valid file name for next file of a series
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: fname.h[1.0] Mon Apr  7 23:53:24 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _FNAME_H_
#define _FNAME_H_

#include "types.h"

Char *FN_last(Char *pfx, Char *ext, Char *dir);
Char *FN_next(Char *fname);
Int FN_num(Char *fname);

#endif              /* ndef _FNAME_H_ */
