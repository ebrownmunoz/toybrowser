/* pthr.h - provide pthreads-like environment on dec, sun, sco, sgi
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 * Many machines (e.g. Sun, SGI) do not have pthreads.  Here we provide a
 * "fake" pthreads environment on machines that do not have pthreads, and
 * the real thing on machines that do.  The end result is that there is no
 * conditional compilation needed in the REAL source code;  this construct
 * allow us to restrict threads-related conditional compilation to this 
 * include file.
 *---------------------------------------------------------------------------*/

#ifndef _PTHR_H_
#define _PTHR_H_

#ifndef VBX_SINGLE_THREAD
#if defined(ANDROID) || defined(__ANDROID__)
#ifndef pthreads
#define pthreads
#endif
#endif
#if defined(GNUWINCE)
#include <pthread.h>
#include "types.h"
#else
#if defined(MINGW32)
#include <windef.h>
#endif

#ifdef VXWORKS
#include "stdio.h"
#endif
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined(pthreads)
#if defined(DEC) || defined(sol)
#define REAL_PTHREADS
#endif
#if defined(LINUX) || defined(MINGW32) || defined(AIX) || defined(GNUWINCE) || defined (ANDROID) || defined(__ANDROID__)
#define REAL_PTHREADS
#include <time.h>
#endif
#endif

/* If we have honest-to-god pthreads ... */
#ifdef REAL_PTHREADS

#include <unistd.h>
#include <pthread.h>
#if !defined(GNUWINCE)
#if defined(DEC)
#define PRI_VBX_MIN                PRI_FIFO_MAX
#define PRI_VBX_MAX                PRI_FIFO_MIN
#else
Int pthread_delay_np(struct timespec *interval);
Int pthread_get_expiration_np(struct timespec *delta, struct timespec *abstime);
#endif
#if defined(LINUX) || defined(AIX) || defined(ANDROID) || defined(__ANDROID__)
#include <sys/resource.h>
#define PRI_FIFO_MAX               PRIO_MAX
#define PRI_FIFO_MIN               PRIO_MIN
#define PRI_VBX_MAX                PRI_FIFO_MAX
#define PRI_VBX_MIN                PRI_FIFO_MIN
#endif
#if defined(MINGW32)
  /*!! CAV Don't have these for Windows */
#define PRI_FIFO_MAX               47
#define PRI_FIFO_MIN               47
#endif
#if defined(WIN32)
#define PRI_VBX_MIN                PRI_OTHER_MAX
#define PRI_VBX_MAX                PRI_OTHER_MIN
#endif
#if defined(sol)
#include <sched.h>
#define PRI_VBX_MIN                PTHR_getMaxPrio()
#define PRI_VBX_MAX                PTHR_getMinPrio()
#define SOL_PRI_MAX                120
#define SOL_PRI_MIN                0
#endif

typedef void *(*pthread_startroutine_t)(void *);
Int  PTHR_getMaxPrio();
Int  PTHR_getMinPrio();
#endif  /* !def GNUWINCE */
#endif  /* def REAL_PTHREADS */

/* Verbex pthreads emulation for WIN32 */
#if defined(WIN32) && !defined(REAL_PTHREADS)
#if defined(UNDER_CE)
#include <wtypes.h>
#endif

#define PTHR_SUCCESS           0
#define PTHR_FAILURE          -1

/* WIN32 doesn't give us timespec, so we define it here */
#ifndef NEWLIB
#ifndef _TIMESPEC_T_
#define _TIMESPEC_T_
typedef struct timespec {
  unsigned long tv_sec;
  long          tv_nsec;
} timespec_t;
#endif
#endif

#define PRI_VBX_MAX            (THREAD_PRIORITY_HIGHEST)
#define PRI_VBX_MIN            (THREAD_PRIORITY_LOWEST)

typedef HANDLE           pthread_t;
typedef CRITICAL_SECTION pthread_mutex_t;
typedef Int              pthread_cond_attr_t;

enum {SIGNAL = 0, BROADCAST = 1, MAX_EVENTS = 2 };

typedef struct pthread_cond_s {
  /* Count of the number of waiters */
  UInt waitersCnt;
  CRITICAL_SECTION waitersCntMutex;

  /* Signal and broadcast event HANDLEs */
  HANDLE events[MAX_EVENTS];
} pthread_cond_t;

#endif

/* Verbex pthreads emulation for VxWorks */
#if defined(VXWORKS) && !defined(REAL_PTHREADS)
#include "errno.h"
#include "objLib.h"
#include "taskLib.h"
#include "sched.h"
#include "timers.h"
#include "msgQLib.h"
#include "semLib.h"

#define PTHR_SUCCESS           0
#define PTHR_FAILURE          -1

#define TICKNSEC               (4)
#define PRI_VBX_MAX            (0)
#define PRI_VBX_MIN            (255)
#define PRI_JNI_DFLT           (114)

typedef Int         pthread_t;
typedef Int         pthread_cond_attr_t;
typedef SEM_ID      pthread_mutex_t;
typedef struct pthread_cond_s {
  MSG_Q_ID waitersMsgQ;
  Bool defunct;
} pthread_cond_t;

Int  pthread_cond_init(pthread_cond_t *, const pthread_cond_attr_t *);
Int  pthread_cond_destroy(pthread_cond_t *);
Int  pthread_cond_wait(pthread_cond_t *, pthread_mutex_t *);
Int  pthread_cond_timedwait(pthread_cond_t *, pthread_mutex_t *, const struct timespec *);
Int  pthread_cond_signal(pthread_cond_t *);

pthread_t pthread_self();

Char *strdup(const Char *);

#endif

/* If we don't have real pthreads ... */
#ifndef REAL_PTHREADS

/* Define all the rest of the pthreads stuff */
#define PTHREAD_INHERIT_SCHED   0
#define PTHREAD_DEFAULT_SCHED   1

/*!! These look problematic for VxWorks !!*/
#define PRI_FIFO_MIN            14
#define PRI_FIFO_MAX            22
#define PRI_RR_MIN              16
#define PRI_RR_MAX              22
#define PRI_OTHER_MIN           16
#define PRI_OTHER_MAX           22

typedef struct PTHREAD_ATTR {
  Int  prio;
  Long stack;
  Int  options;
} pthread_attr_t;

#ifndef VXWORKS
typedef struct sched_param {
  int sched_priority;
} sched_param_t;

#define SCHED_FIFO              0
#define SCHED_RR                1
#define SCHED_OTHER             2
#endif

typedef Int    pthread_mutexattr_t;
typedef void (*pthread_startroutine_t)();

Int  pthread_attr_init(pthread_attr_t *);
Int  pthread_getschedpolicy(pthread_attr_t *, Int *);
Int  pthread_attr_setprio(pthread_attr_t *, Int);
Int  pthread_attr_setinheritsched(pthread_attr_t *, Int);
Int  pthread_attr_setschedparam(pthread_attr_t *, struct sched_param *);
Int  pthread_attr_setschedpolicy(pthread_attr_t *, Int);
Int  pthread_attr_setstacksize(pthread_attr_t *, Long);
Int  pthread_cancel(pthread_t);
Int  pthread_create(pthread_t *, pthread_attr_t *, pthread_startroutine_t, 
                    void *);
Int  pthread_delay_np(struct timespec *);
Int  pthread_detach(pthread_t);
void pthread_exit(void *);
Int  pthread_get_expiration_np(struct timespec *delta, struct timespec *abstime);
Int  pthread_join(pthread_t, void **);
Int  pthread_mutex_init(pthread_mutex_t *, pthread_mutexattr_t *);
Int  pthread_mutex_destroy(pthread_mutex_t *);
Int  pthread_mutex_lock(pthread_mutex_t *);
Int  pthread_mutex_unlock(pthread_mutex_t *);

#define DEFAULTSTACK            21120
#endif              /* ndef REAL_PTHREADS */

/* Define the stuff that's used in all thread implementations */
#if defined(VXWORKS)
#define PRI_VBX_MID             (114)
#else
#define PRI_VBX_MID             ((PRI_VBX_MAX + PRI_VBX_MIN) / 2)
#endif
#define PRI_VBX_HIGH            ((PRI_VBX_MAX + PRI_VBX_MID) / 2)
#define PRI_VBX_LOW             ((PRI_VBX_MIN + PRI_VBX_MID) / 2)

#ifdef __cplusplus
}
#endif
#endif              /* ndef VBX_SINGLE_THREAD */
#endif              /* ndef GNUWINCE */
#endif              /* ndef _PTHR_H_ */
