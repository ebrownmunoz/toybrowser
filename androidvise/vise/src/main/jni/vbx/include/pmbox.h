/* pmbox.h - prioritized mailbox facility for threaded programs
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: pmbox.h[1.0] Mon Apr  7 23:53:34 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _PMBOX_H_
#define _PMBOX_H_

#include "pthr.h"
#include "types.h"
#include "class.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct PMBOX_Att_s * PMBOX_Att;
typedef struct PMBOX_s     * PMBOX;

 /* Mailbox operations */
extern PMBOX_Att PMBOX_attributes(Uns maxMessages, Uns nPriorities);
extern PMBOX     PMBOX_new(PMBOX_Att attributes);
extern void      PMBOX_destroy(PMBOX mailbox);
extern Bool      PMBOX_insert(PMBOX mailbox, OBJT message, Uns priority);
extern Bool      PMBOX_insertNow(PMBOX mailbox, OBJT message, Uns priority);
extern Bool      PMBOX_insertWithTimeout(PMBOX mailbox, OBJT message, Uns priority, struct timespec * timeout);
extern OBJT      PMBOX_remove(PMBOX mailbox);
extern OBJT      PMBOX_removeNow(PMBOX mailbox);
extern OBJT      PMBOX_removeWithTimeout(PMBOX mailbox, struct timespec * timeout);
extern Uns       PMBOX_numobj(PMBOX mailbox);

#ifdef __cplusplus
}
#endif
#endif  /* _PMBOX_H_ */
