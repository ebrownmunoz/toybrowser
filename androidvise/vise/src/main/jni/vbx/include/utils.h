/* utils.h - general i/o and utility functions
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <sys/types.h>
#if defined(DEC)
#include <sys/times.h>
#endif
#include "types.h"
#include "vbx.h"

#define READERR        -1
#define WRITERR        -1
#define UTIL_PRINT_PRIO VBX_MID_PRIO

typedef struct timespec * TIME;
                                              
#ifdef __cplusplus
extern "C" {
#endif

extern Int    areadchar(Char *file, Char **data_ref, size_t *length_ref);
extern Int    areaddouble(Char *file, Double **data_ref, size_t *length_ref);
extern Int    areadfloat(Char *file, Float **data_ref, size_t *length_ref);
extern Int    areadint(Char *file, Int **data_ref, size_t *length_ref);
extern Int    areadshort(Char *file, Short **data_ref, size_t *length_ref);
extern Int    awritechar(Char *file, Char *data, size_t length);
extern Int    awritedouble(Char *file, Double *data, size_t length);
extern Int    awritefloat(Char *file, Float *data, size_t length);
extern Int    awriteint(Char *file, Int *data, size_t length);
extern Int    awriteshort(Char *file, Short *data, size_t length);
extern Int    cepReadBin(Float **buf, Int *len, Char *file);
extern Int    cepWriteBin(Float *buf, Int bytes, Char *file);
extern Double elapsedTime(TIME start, TIME end);
extern void   getMemoryStats(Long * totalPhysicalBytes, Long * physicalBytesFree, Int * percentUsed, Long * maxFreeBlockSize, Long * totalVirtualBytes, Long * virtualBytesFree);
extern void   getPagingStats(Long * reclaims, Long * faults, Long * swaps);
extern void   getTime(TIME val);
extern void   halt(Char * fmt, ...);
extern Long   maxResidentSetSize(void);
extern Char * nextToken(Char ** string, Char * delimiters);
extern Int    peek_length(Char *file);
extern Double processTime(void);
extern void   quit(Int status, Char * fmt, ...);
extern Int    nextPowerOfTwo(Int x);
extern Long   read_long(FILE *stream);
extern Int    read_long_array(FILE *stream, Long *base, Int length);
extern Char * stripID(Char * line, Int leftDelim, Int rightDelim);
extern void   swapShortBuf(Short *p, Long cnt);
extern void   swapLong(Long *intp);
extern void   swapLongBuf(Long *p, Long cnt);
extern Double systemTime(void);
extern Int    unlimit(void);
extern Int    write_long(FILE *stream, Long word);
extern Int    write_long_array(FILE *stream, Long *base, Int length);

#ifdef __cplusplus
}
#endif
#endif /* _UTILS_H_ */
