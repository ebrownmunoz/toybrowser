/* swap.h - in-place byte order conversion; nothing is returned
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _SWAP_H_
#define _SWAP_H_

#include "types.h"

#if (defined(__ANDROID__))
#include <endian.h>
#if _BYTE_ORDER == _LITTLE_ENDIAN
#define LITENDIAN
#else
#undef LITENDIAN
#endif
#endif
/* DEC and Intel are little-endian (the byte-order and address order are the same) */
/* SUN and Motorola are big-endian (the byte-order and address oreer are opposite) */

#if (defined(DEC) || defined(LITENDIAN))

#define SWAPBYTES

/* For a LITTLE ENDIAN reading from or writing to a BIG ENDIAN (vbx default) */
#define SWAPW(x) *(x) = ((0xff & (*(x))>>8) | (0xff00 & (*(x))<<8))
#define SWAPL(x) *(x) = ((0xff & (*(x))>>24) | (0xff00 & (*(x))>>8) |\
                         (0xff0000 & (*(x))<<8) | (0xff000000 & (*(x))<<24))
#define SWAPF(x) SWAPL((Int *) x)
#define SWAPP(x) SWAPL((Int *) x)
#define SWAPD(x) { Int *low = (Int *) (x), *high = (Int *) (x) + 1,\
                   temp; SWAPL(low);  SWAPL(high);\
                   temp = *low; *low = *high; *high = temp;}

/* For a LITTLE ENDIAN reading from or writing to a LITTLE ENDIAN */
#define BSWAPW(x)
#define BSWAPL(x)
#define BSWAPF(x)
#define BSWAPP(x)
#define PSWAPD(x)

#else

/* For a BIG ENDIAN reading from or writing to a BIG ENDIAN (vbx default) */
#define SWAPW(x)
#define SWAPL(x)
#define SWAPF(x)
#define SWAPP(x)
#define SWAPD(x)

/* For a BIG ENDIAN reading from or writing to a LITTLE ENDIAN */
#define BSWAPW(x) *(x) = ((0xff & (*(x))>>8) | (0xff00 & (*(x))<<8))
#define BSWAPL(x) *(x) = ((0xff & (*(x))>>24) | (0xff00 & (*(x))>>8) |\
                         (0xff0000 & (*(x))<<8) | (0xff000000 & (*(x))<<24))
#define BSWAPF(x) SWAPL((Int *) x)
#define BSWAPP(x) SWAPL((Int *) x)
#define BSWAPD(x) { Int *low = (Int *) (x), *high = (Int *) (x) + 1,\
                   temp; SWAPL(low);  SWAPL(high);\
                   temp = *low; *low = *high; *high = temp;}

#endif

#endif  /* _SWAP_H_ */

