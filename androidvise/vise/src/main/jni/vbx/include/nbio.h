/* nbio.h - Routines for doing non-blocking i/o on file descriptors
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 * DESCRIPTION
 *---------------------------------------------------------------------------*/

#ifndef _NBIO_H_
#define _NBIO_H_

#include "pthr.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ERROR           (-1)

#define NBIO_SELREAD    (0x0001)
#define NBIO_SELWRITE   (0x0010)
#define NBIO_SELEXC     (0x0100)

void NBIO_setblock(Int fd, Bool on);
Int  NBIO_select(Int fd, Int bitmask, Int msec);
Int  NBIO_read(Int fd, Char *bytes, Int byteCnt, Int msec);
Int  NBIO_write(Int fd, Char *bytes, Int byteCnt, Int msec);


#ifdef __cplusplus
}
#endif
#endif
