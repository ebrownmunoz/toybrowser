/*
 * sockets.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _SOCKETS_H_
#define _SOCKETS_H_

#include "types.h"
#include "packets.h"

enum sockets {
  DSP_H,    /* DSP side of HUB-DSP socket */
  HUB_D,    /* HUB side of HUB-DSP socket */
  HUB_FD,   /* */        /* Obsolete */
  HUB_FC,   /* */        /* Obsolete */
  HUB_G,    /* */        /* Obsolete */
  FBS_HD,   /* */        /* Obsolete */
  FBS_HC,   /* */        /* Obsolete */
  GUI_H     /* */        /* Obsolete */
};

#define  MAXSOCK  10                    /* max # sockets in the system */

Int sox[MAXSOCK];                       /* socket array for system-wide use */

static UShort Ports[] = {
  0x0aaa,   /* DSP side of HUB-DSP socket */
  0x0aab,   /* HUB side of HUB-DSP socket */
  0x0aac,   /* */        /* Obsolete */
  0x0aad,   /* */        /* Obsolete */
  0x0aae,   /* */        /* Obsolete */
  0x0aaf,   /* */        /* Obsolete */
  0x0aba,   /* */        /* Obsolete */
  0x0abb,   /* */        /* Obsolete */
  0x0abc,   /* */        /* Never Used */
  0x0abd    /* */        /* Never Used */
};

#endif  /* _SOCKETS_H_ */
