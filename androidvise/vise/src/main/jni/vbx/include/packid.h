/* packid.h - packet protocol for interprocess communication
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: packid.h[1.0] Mon Apr  7 23:53:32 1997 dvetter@titan saved $
 * DESCRIPTION
 * Packet definitions, routing enumerations, and all that...
 * OBSOLETE, replaced by packets.h
 *---------------------------------------------------------------------------*/

#ifndef _PACKID_H_
#define _PACKID_H_

#define NUM_COEFF 13
#define NUM_SAMPLES 160

typedef Int MsgType;   /* The packet id or command type */

enum packets {ACCEPT, BWD_RESULTS, CEP_FRAME, CFG_PARAM, FWD_CONF, FWD_RESULTS,
              NBEST_CHOICE, NBEST_RESULTS, REC_LAUNCH, STRING, STAT_MSG,
              CEPWAV_FRAME, RESP_TIME, DSP_GAIN, DSP_THRESH1, DSP_THRESH2,
              SAVE_COUNT, WAV_FRAME};
enum mboxes  {MCT, MSGOUT, DATAIN, PRINT, PRNMSG, DATABUF, DSPSOC, 
              FBSSOC_C, FBSSOC_D, GUI, RIT, RDDATA, MBOX_COUNT};

#define  CEP_LEN       62
#define  WAV_LEN       330       /* 160 samples / frame */
#define  CEPWAV_LEN    382       /* 160 samples / frame */
#define  BOO_LEN       8
#define  STAT_LEN      8
#define  CMD_LEN       8
#define  TIME_LEN      10
#define  GAIN_LEN      10
#define  THRESH_LEN    10
#define  COUNT_LEN     10

typedef struct accept {
  Bool  accept;
  Int   nbest;
  Bool  mod;
  Char  mtext[256];
} accept_s, *accept_t;
  
typedef struct cepFrm {
  Int   seqnum;
  Float data[NUM_COEFF];
} cepFrm_s, *cepFrm_t;

typedef struct wavFrm_s {
  Long seqNum;
  Float data[NUM_SAMPLES];
} wavFrm_t, *WAV;

typedef struct cepwav_s {
  Long seqNum;
  Float cep[NUM_COEFF];
  Short wav[NUM_SAMPLES];
} cepwav_t, *CEPWAV;

typedef struct cfgParam {
  Int   index;
  Int   type;
  union {
    Float  f;
    Int    i;
    Uns    u;
  } value;
  Char  text[256];
} cfgParam_s, *cfgParam_t;

#endif /* _PACKID_H_ */
