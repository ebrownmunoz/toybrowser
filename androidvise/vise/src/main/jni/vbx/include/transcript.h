/* transcript.h - module for reading and looking up transcriptions
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: transcript.h[1.0] Thu Aug 27 13:18:16 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _TRANSCRIPT_H_
#define _TRANSCRIPT_H_

#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

Bool   TRANSCRIPT_buildHashTbl(Char *fname, Char *sentDir);
Char * TRANSCRIPT_lookup(Char *key);
Bool   TRANSCRIPT_fromPath(Char *path, Char *trans, Int maxLen);

#ifdef __cplusplus
}
#endif
#endif  /* _TRANSCRIPT_H_ */
