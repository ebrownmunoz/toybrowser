/* urlcodec.h - URL encoding/decoding
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _URLCODEC_H_
#define _URLCODEC_H_

#include <stdio.h>
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

extern Char * URL_encode(const Char * plainText, Char * urlEncoding, Int maxEncodedLength);
extern Char * URL_decode(Char * plainText, const Char * urlEncoding, Int maxDecodedLength);

#ifdef __cplusplus
}
#endif
#endif  /* ndef _URLCODEC_H_ */
