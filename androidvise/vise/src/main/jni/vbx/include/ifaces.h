/****************************************************************************
 *
 *  ifaces.h
 *
 *  Public interface definitions for VISE
 *
 ****************************************************************************/


#ifndef _IFACES_H_
#define _IFACES_H_

#include "vbxtypes.h"
#include "ifacedef.h"
#include "viseerr.h"
#include "sn.h"

  /* Memory types */
typedef enum memtype_e { SLOW_MEMORY, FAST_MEMORY, MAIL_MEMORY } memtype_t;

#undef  INTERFACE
#define INTERFACE IMemory

VBX_DECLARE_INTERFACE(IMemory) {
  /* IMemory methods */
  VBXMETHOD_(void *, Calloc)   (VBXTHIS_ size_t numObj, size_t sizeObj, memtype_t type) VBXPURE;
  VBXMETHOD (Free)             (VBXTHIS_ void * allocation) VBXPURE;
};

#undef  INTERFACE
#define INTERFACE IMBox

VBX_DECLARE_INTERFACE(IMBox) {
  /* IMBox methods */
  VBXMETHOD (Destroy)          (VBXTHIS) VBXPURE;
  VBXMETHOD_(V_Bool, Send)     (VBXTHIS_ V_Uns mboxId, V_Uns * msg, V_ULong msgLen, V_Int priority, V_Err * status) VBXPURE;
  VBXMETHOD_(V_Bool, Receive)  (VBXTHIS_ V_Uns * mboxId, V_Uns ** msg, V_ULong * msgLen, V_Bool block) VBXPURE;
  VBXMETHOD_(V_Uns, NumMsgs)   (VBXTHIS) VBXPURE;
  VBXMETHOD (Flush)            (VBXTHIS) VBXPURE;
};

  /* Status codes reported by IMBox::Send() */
#define MBOX_SUCCESS      VISESUCCESS
#define MBOX_OUTOFMEMORY  OUTOFMEMORY
#define MBOX_NOSUCHMBOX   MAILNOSUCHMBOX
#define MBOX_BREAKDOWN    MAILBREAKDOWN

#undef  INTERFACE
#define INTERFACE IMail

VBX_DECLARE_INTERFACE(IMail) {
  /* IMail methods */
  VBXMETHOD_(IMBox *, CreateMBox) (VBXTHIS_ const V_Char * name, V_Int numPriorities, V_Err * status) VBXPURE;
  VBXMETHOD_(V_Uns, MailBoxId)    (VBXTHIS_ const V_Char * name, V_Err * status) VBXPURE;
};

  /* Status codes reported by IMail::CreateMBox() and IMail::MailBoxId() */
#define MAIL_SUCCESS      VISESUCCESS
#define MAIL_OUTOFMEMORY  OUTOFMEMORY
#define MAIL_MBOXEXISTS   MAILMBOXEXISTS
#define MAIL_NOSUCHMBOX   MAILNOSUCHMBOX
#define MAIL_BREAKDOWN    MAILBREAKDOWN

#undef  INTERFACE
#define INTERFACE IAudioIn

VBX_DECLARE_INTERFACE(IAudioIn) {
  /* IAudioIn methods */
  VBXMETHOD_(V_Bool, Init)     (VBXTHIS_ V_Uns rate, V_Uns size, V_Uns * bits) VBXPURE;
  VBXMETHOD_(V_Bool, Read)     (VBXTHIS_ V_Int * samples, V_Uns numSamples) VBXPURE;
  VBXMETHOD_(V_Bool, Start)    (VBXTHIS) VBXPURE;
  VBXMETHOD_(V_Bool, Stop)     (VBXTHIS) VBXPURE;
  VBXMETHOD_(V_Bool, LevelSet) (VBXTHIS_ V_Uns level) VBXPURE;
  VBXMETHOD_(V_Bool, LevelGet) (VBXTHIS_ V_Uns * level) VBXPURE;
  VBXMETHOD_(V_Bool, PosnGet)  (VBXTHIS_ SN * posn) VBXPURE;
  VBXMETHOD_(V_Bool, TotalGet) (VBXTHIS_ SN * posn) VBXPURE;
  VBXMETHOD_(V_Err,  Status)   (VBXTHIS) VBXPURE;
  VBXMETHOD_(V_Bool, DataAvailable) (VBXTHIS_ V_Long * bytesAvailable, V_Int * eof) VBXPURE;
};

#undef  INTERFACE
#define INTERFACE IFile

VBX_DECLARE_INTERFACE(IFile) {
  /* IFile methods */
  VBXMETHOD_(void *,  Open)    (VBXTHIS_ void *fileHandle, V_Uns mode) VBXPURE;
  VBXMETHOD_(V_Err,   Close)   (VBXTHIS_ void *desc) VBXPURE;
  VBXMETHOD_(V_ULong, Read)    (VBXTHIS_ void *desc, void *data, V_ULong size) VBXPURE;
  VBXMETHOD_(V_ULong, Write)   (VBXTHIS_ void *desc, void *data, V_ULong size) VBXPURE;
  VBXMETHOD_(V_Err,   Seek)    (VBXTHIS_ void *desc, V_Long offset, V_Uns type) VBXPURE;
  VBXMETHOD_(V_ULong, Tell)    (VBXTHIS_ void *desc) VBXPURE;
};

#endif     /* def _IFACES_H_ */
