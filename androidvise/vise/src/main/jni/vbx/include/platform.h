/*------------------------------------------------------------------------------
 * Defines OEM Platform Names
 *----------------------------------------------------------------------------*/
#ifndef _PLATFORM_H_
#define _PLATFORM_H_

/* Symbol */
#define SYM_PDT8000 "Symbol PDT8000 Series"
#define SYM_MC9000 "SYMBOL MC9000S"

/* Intermec */
#define ITC     "Intermec"

/* LXE */
#define LXE         "LXE_"

/* ADS */
#define ADS_XVOX    "XVOX"
#define XVOX_LINUX  "XVOX_LINUX"

/* ATLANTA */
#define ATLANTA     "Belgravium Atlanta 8000"

/* Original yellow box */
#define INROAD_BOX  "VLS310"

#endif /* _PLATFORM_H_ */
