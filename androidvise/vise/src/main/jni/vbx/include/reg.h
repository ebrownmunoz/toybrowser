/* reg.h - manipulate registry entries
 *---------------------------------------------------------------------------*
 * Voxware, Inc. (dcv)
 *---------------------------------------------------------------------------*/

#ifndef _REG_H_
#define _REG_H_

#include <stdio.h>
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

#ifdef WIN32
#if   defined(NEWLIB)
#include <sys/wceregistry.h>
#elif defined(UNDER_CE)
#include <winreg.h>
#endif
#define REG_NoType          REG_NONE
#define REG_String          REG_SZ
#define REG_EnvString       REG_EXPAND_SZ
#define REG_ByteArray       REG_BINARY
#define REG_Integer         REG_DWORD
#define REG_StringArray     REG_MULTI_SZ
#else
#define REG_NoType          0
#define REG_String          1
#define REG_EnvString       2
#define REG_ByteArray       3
#define REG_Integer         4
#define REG_StringArray     7
#endif

#define REG_MaxValueLength  512
#define REG_MaxRootLength   5
#define REG_MaxPathLength   512
#define REG_MaxNameLength   128

#define REG_DefaultArrayDelimiter ","       

#define REG_UntypedTypeName       "<untyped>"
#define REG_ByteArrayTypeName     "hex:"
#define REG_IntegerTypeName       "dword:"
#define REG_StringArrayTypeName   "array:"

#ifdef WIN32
#define REG_
#else
#endif

 /* A REG_Entry */
typedef struct REG_Entry_s * REG_Entry;

 /* REG_Entry constructors and destructors */
extern REG_Entry   REG_Entry_newFromComponents(Char * root, Char * path, Char * name, ULong type);
extern REG_Entry   REG_Entry_new(Char * path, ULong type);
extern void        REG_Entry_free(REG_Entry entry);

 /* Methods to read and write the underlying physical registry */
extern Bool        REG_Entry_read(REG_Entry entry);
extern Bool        REG_Entry_write(REG_Entry entry);
extern Bool        REG_Entry_remove(REG_Entry entry);
extern Bool        REG_Entry_save(REG_Entry entry);

 /* REG_Entry access methods */
extern Char *      REG_Entry_root(REG_Entry entry);
extern Bool        REG_Entry_setRoot(REG_Entry entry, Char * name);
extern Char *      REG_Entry_path(REG_Entry entry);
extern Bool        REG_Entry_setPath(REG_Entry entry, Char * path);
extern Char *      REG_Entry_name(REG_Entry entry);
extern Bool        REG_Entry_setName(REG_Entry entry, Char * name);
extern ULong       REG_Entry_type(REG_Entry entry);
extern Bool        REG_Entry_setType(REG_Entry entry, ULong type);
extern Bool        REG_Entry_isDir(REG_Entry entry);

 /* Methods for getting values */
extern ULong       REG_Entry_intValue(REG_Entry entry);
extern Char *      REG_Entry_stringValue(REG_Entry entry);
extern Char **     REG_Entry_stringArrayValue(REG_Entry entry, Int * size);
extern UChar *     REG_Entry_byteArrayValue(REG_Entry entry, Int * size);
extern REG_Entry * REG_Entry_entryArrayValue(REG_Entry entry, Int * size);
extern Char *      REG_Entry_appendValueToString(REG_Entry entry, Char * buffer, Char * separator);

 /* Methods for setting values */
extern Bool        REG_Entry_setIntValue(REG_Entry entry, ULong value);
extern Bool        REG_Entry_setIntValueFromString(REG_Entry entry, Char * value);
extern Bool        REG_Entry_setStringValue(REG_Entry entry, Char * value);
extern Bool        REG_Entry_setStringArrayValue(REG_Entry entry, Char ** value, Int size);
extern Bool        REG_Entry_setStringArrayValueFromString(REG_Entry entry, Char * value, Char * delimiters);
extern Bool        REG_Entry_setByteArrayValue(REG_Entry entry, UChar * value, Int size);
extern Bool        REG_Entry_setByteArrayValueFromString(REG_Entry entry, Char * value, Char * delimiters);
extern Bool        REG_Entry_setEntryArrayValue(REG_Entry entry, REG_Entry * value, Int size);

 /* Print method */
extern void        REG_Entry_print(REG_Entry entry, Char * prefix, Char * valEquals, Char * dirEquals, Char * separator, Char * suffix, Int maxDepth);

#ifdef __cplusplus
}
#endif
#endif  /* ndef _REG_H_ */
