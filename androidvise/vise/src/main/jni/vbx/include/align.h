/* align.h - NIST scoring module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: align.h[2.2] Thu Feb 24 00:24:01 1994 dvetter@noname saved $
 *---------------------------------------------------------------------------*/

#ifndef _ALIGN_H_
#define _ALIGN_H_

#include "types.h"

#define SL              120     /* words in a sentence              */
#define WC              40      /* characters in a word             */
#define SC              SL * (WC + 1)   /* characters in a sentence */

/* Error codes */
#define E_bad_ref_file          1
#define E_bad_hyp_file          2
#define E_no_ref_file           3
#define E_no_hyp_file           4
#define E_no_lexicon_file       5
#define E_bad_lexicon_file      6
#define E_unsorted_lexicon      7
#define E_no_hom_file           8
#define E_bad_hom_file          9
#define E_no_SM_file            10
#define E_bad_SM_file           11
#define E_bad_summary_file      12
#define E_too_many_chars        13
#define E_both_sents_int        14
#define E_ref_sent_int          15
#define E_hyp_sent_int          16
#define E_illegal_char          17
#define E_time_pairs            18

extern void ALIGN_init(Bool dbg, Bool readT, Bool useT, Bool homonyms);
extern void ALIGN_initSentence(void);
extern Int  ALIGN_sentences(Char * arg1[], Int n1, Char * arg2[], Int n2,
                            Char * refAligned, Char * hypAligned,
                            Char * s1[2 * SL], Char * s2[2 * SL],
                            Int s5[2 * SL],
                            Int inSplit[2 * SL], Int inMerge[2 * SL]);
extern Int  ALIGN_comp(Char * refStr,  Char * hypStr);
extern Bool ALIGN_loadLexicon(Char * lexFile);
extern Bool ALIGN_loadHomophones(Char * homonymFile);
extern Bool ALIGN_loadSplitsAndMerges(Char * smfile);
extern void ALIGN_checkLexicon(Int count, Char * wds[], Int * unknown);
extern void ALIGN_parse(Char * unaligned, Int * nWords, Char * words[],
                        Char ** sentenceID);
extern void ALIGN_parseTimes(Char * line, Int begin[SL], Int end[SL], Int n);
extern void ALIGN_recordErrors(Char * ref[], Char * hyp[], Int comparison[],
		                       Int inSplit[], Int inMerge[]);
extern void ALIGN_updateTotals(Int refUnknown, Int hypUnknown);
extern void ALIGN_printSentenceStats(Char * refAligned, Char * hypAligned,
                                     Char * refSentenceID, Char * hypSentenceID);
extern void ALIGN_printSummary(Char * refList, Char * hypList);
extern void ALIGN_perror(Int code, Int val, Char * s1, Char * s2);

#endif /* _ALIGN_H_ */
