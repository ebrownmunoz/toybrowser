#ifndef VBXMSG_

/******************************************************************************
 * 
 * File:        visemsg.h
 *
 * Description: Header file for Message System
 *
 *****************************************************************************/

#define VBXMSG_ 0

#include "vbxtypes.h"
#include "ifaces.h"
#include "sn.h"
#include "viseerr.h"

#ifdef __cplusplus
extern "C" { 
#endif

  /* The size of V_Int and V_Uns types in AME widths */
#define VINTSIZEINMELS   1
  /* The size of V_Long and V_ULong types in AME widths */
#define VLONGSIZEINMELS  (VLONGSIZE / VAMESIZE)
  /* The size of sequence numbers in AME widths */
#define VSNSIZEINMELS    (VSNSIZE / VAMESIZE)

  /* The Message Port Descriptor */
typedef struct vbxmsg_s * VBXMSG;

  /* Message Port Methods */
extern VBXMSG   MSG_create(IMemory *, IMBox *);
extern V_Void   MSG_destroy(VBXMSG);
extern V_Uns    MSG_numUnread(VBXMSG);
extern V_Long   MSG_numMelsRemaining(VBXMSG msg);

  /* Message Read/Write Methods */
extern V_Uns    MSG_correspondentId(VBXMSG);

  /* Message Read Methods */
extern V_Bool   MSG_openRead(VBXMSG msg, V_Uns * senderId, V_ULong * length, V_Int * cmd, V_Bool block, V_Err * status);
extern V_Bool   MSG_readInt(VBXMSG msg, V_Int * word);
extern V_Bool   MSG_readUns(VBXMSG msg, V_Uns * word);
extern V_Bool   MSG_readLong(VBXMSG msg, V_Long * word);
extern V_Bool   MSG_readULong(VBXMSG msg, V_ULong * word);
extern V_Bool   MSG_readMels(VBXMSG msg, V_Mel * mels, V_ULong count);
extern V_Bool   MSG_readSN(VBXMSG msg, V_SN * sn);
extern V_Bool   MSG_closeRead(VBXMSG msg, V_Err * status);

  /* Message Write Methods */
extern V_Bool   MSG_openWrite(VBXMSG msg, V_Uns receiverId, V_ULong length, V_Int cmd, V_Int priority, V_Err * status);
extern V_Bool   MSG_writeInt(VBXMSG msg, V_Int word);
extern V_Bool   MSG_writeUns(VBXMSG msg, V_Uns word);
extern V_Bool   MSG_writeLong(VBXMSG msg, V_Long word);
extern V_Bool   MSG_writeULong(VBXMSG msg, V_ULong word);
extern V_Bool   MSG_writeMels(VBXMSG msg, V_Mel * mels, V_ULong count);
extern V_Bool   MSG_writeSN(VBXMSG msg, V_SN sn);
extern V_Bool   MSG_closeWrite(VBXMSG msg, V_Err * status);

  /* For backwards compatibility */
#define MSG_writeWord(msg, word) VBXMSG_writeUns(msg, word)

#ifdef __cplusplus
}
#endif

#endif /* VBXMSG_ */
