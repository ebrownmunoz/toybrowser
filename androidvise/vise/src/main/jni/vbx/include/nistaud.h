/* nistaud.h - IAudioIn methods built on an NIST audio file source
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Component methods for building an IAudioIn to obtain data from an NIST audio
 * file source.
 *---------------------------------------------------------------------------*/

#ifndef _NISTAUD_H_
#define _NISTAUD_H_

#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

typedef struct NISTAudioIn * NISTAUDIN;

/* NISTAUDIN Methods */
extern NISTAUDIN NISTAUDIN_create(IMemory * imem);
extern void      NISTAUDIN_destroy(NISTAUDIN nistaudin);
extern Bool      NISTAUDIN_load(NISTAUDIN nistaudin, Char * fileName);
extern Bool      NISTAUDIN_setDefaultDir(NISTAUDIN nistaudin, Char * dirName);
extern Int       NISTAUDIN_sampleCount(NISTAUDIN nistaudin);
/* Do not free following returned pointers !!! */
extern Char *    NISTAUDIN_getFileName(NISTAUDIN nistaudin);
extern void *    NISTAUDIN_getHdrEntry(NISTAUDIN nistaudin, Char * entryName);
extern Int       NISTAUDIN_peakPower(NISTAUDIN nistaudin, Int frameLen,
                                Int frameShift, Int window, Int * gainShift);
extern Int       NISTAUDIN_setNoise(NISTAUDIN nistaudin, Char * fullFN);
extern Bool      NISTAUDIN_mix(NISTAUDIN nistaudin, Char * fileName,
                               Int noiPowerMix, Int milPre, Int milAdd, Int noiPowerAdd);
/* nistaudio & nistaudin helper functions */
extern Int       NISTAUDIN_getNoise(Char * fullFN, Short **noiBuf,
                                    Short **noiStart, Short **noiEnd,
                                    Int sampleRate);
extern void      NISTAUDIN_doMix(Short **data, Int *dataLen, Int noiPowerMix,
                                 Int numSamplesPre, Int numSamplesAdd, Int noiPowerAdd,
                                 Short **noiPtr, Short *noiStart,
                                 Short *noiEnd, Int noiPower);
#ifdef __cplusplus
}
#endif

#endif /* _NISTAUD_H_ */
