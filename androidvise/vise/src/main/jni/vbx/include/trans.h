#ifndef _TRANS_H_
#define _TRANS_H_

#include "types.h"
#include "hash.h"
#include "vfile.h"

#ifdef __cplusplus
extern "C" { 
#endif

typedef struct transent_s {
  UInt  wid;
  FLOFF hostTrans;
  FLOFF voiceTrans;
  FLOFF displayTrans;
} transent_t, *TRANSENT;

typedef struct delin_s {
  FLOFF initiator;
  FLOFF separator;
  FLOFF terminator;
} delin_t, *DELIN;

typedef struct trans_s {
  Ptr   controlParseTbl;
  Ptr   voiceParseTbl;
  Ptr   hostParseTbl;
  Ptr   displayParseTbl;
  DELIN HRDelin;
  DELIN VRDelin;
  DELIN DRDelin;
  HASH  widHT;                          /* Word Id hash of translations */
} trans_t, *TRANS;

DELIN DELIN_create();
void  DELIN_annihilate(DELIN delin);

TRANS TRANS_create();
void  TRANS_annihilate(TRANS trans);

TRANSENT TRANSENT_create(UInt wid, FLOFF hostTrans, FLOFF voiceTrans, FLOFF displayTrans);
void     TRANSENT_annihilate(TRANSENT transent);

#define DELIN_initiator(x)      ((x)->initiator)
#define DELIN_separator(x)      ((x)->separator)
#define DELIN_terminator(x)     ((x)->terminator)

#ifdef __cplusplus
}
#endif
#endif  /* _TRANS_H_ */
