/* siesta.h - provides a microsecond sleep in "any" machine environment
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _SIESTA_H_
#define _SIESTA_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef VBX_SINGLE_THREAD
#ifdef WIN32
#define siesta(x)    Sleep(x/1000)
#else
#include <poll.h>
#define siesta(x)    poll(NULL, 0, x/1000)
#endif
#else

#ifdef pthreads          /* Common Multithread Arch (CMA) POSIX 1003.4 */
#ifdef VXWORKS
#define siesta(x) \
{ struct timespec uts; \
  uts.tv_sec = x/1000000;\
  uts.tv_nsec = (x%1000000)*1000; \
  nanosleep(&uts, NULL); }
#else
#define siesta(x) \
{ struct timespec uts; \
  uts.tv_sec = x/1000000;\
  uts.tv_nsec = (x%1000000)*1000; \
  pthread_delay_np(&uts); }
#endif
#endif
#ifdef LWP               /* SunOS 4.1.3 Lightweight Processes threads */
#define siesta(x) \
{ struct timeval tv; \
  tv.tv_sec = x/1000000; \
  tv.tv_usec = x%1000000; \
  lwp_sleep(&tv); }
#endif
#ifdef sgi               /* Silicon Graphics (threads or not) */
#define siesta(x)  sginap(x/10000)
#endif

#endif

#ifdef __cplusplus
}
#endif
#endif              /* _SIESTA_H_ */

