/* list.h
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _LIST_H_
#define _LIST_H_

#include <stdio.h>
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct list_s {
   Int	   size;   /* Number entries in the list */
   Int	   inuse;  /* Number entries in use in list */
   Ptr   * list;   /* The list */
} list_t, * LIST;

extern LIST      LIST_new (void);
extern void      LIST_insert (LIST list, Ptr sym, Int idx);
extern Ptr       LIST_lookup (LIST list, Int idx);
extern void      LIST_append (LIST list, Ptr sym);
extern void      LIST_free (LIST list);
extern Int       LIST_index (LIST list, Ptr sym);
extern void      LIST_processElements(LIST list, void (* fcn)(Ptr elements));
extern Ptr     * LIST_array (LIST list);
extern Int       LIST_length (LIST list);
extern void      LIST_write (FILE * fs, LIST list);
extern void      LIST_read (FILE * fs, LIST list);
#ifdef DEBUG
extern void      LIST_dump(LIST list);
#endif
#ifdef __cplusplus
}
#endif

#endif /* _LIST_H_ */
