#ifndef V_IMAIL_

/******************************************************************************
 * 
 *  File: imemory.h - header for the memory allocator
 *
 *****************************************************************************/

#define V_IMAIL_

#include "vbxtypes.h"
#include "viseerr.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" { 
#endif

  /* IMBox Methods */
extern void    IMBOX_Destroy(IMBox * imbox);
extern V_Bool  IMBOX_Send(IMBox * imbox, V_Uns mboxId, V_Uns * msg, V_ULong msgLen, V_Int priority, V_Err * status);
extern V_Bool  IMBOX_Receive(IMBox * imbox, V_Uns * mboxId, V_Uns ** msg, V_ULong * msgLen, V_Bool block);
extern V_Uns   IMBOX_NumMsgs(IMBox * imbox);
extern void    IMBOX_Flush(IMBox * imbox);

  /* IMail Methods */
extern IMail * IMAIL_Create(IMemory * memory, V_Uns maxMsgs);
extern void    IMAIL_Destroy(IMail * imail);
extern IMBox * IMAIL_CreateMBox(IMail * imail, const V_Char * name, V_Int numPriorities, V_Err * status);
extern V_Uns   IMAIL_MailBoxId(IMail * imail, const V_Char * name, V_Err * status);

#ifdef __cplusplus
}
#endif

#endif  /* defined V_IMAIL_ */
