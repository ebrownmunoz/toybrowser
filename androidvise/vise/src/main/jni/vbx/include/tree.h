#ifndef _TREE_H_
#define _TREE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "types.h"
#include "parse.h"

/* The tree_node structure describes a node in a vrf_parse trace_tree.  The
   NFA simulator constructs this tree as it parses the recognition 
   sequence. */

typedef struct tree_s     * TREE;
typedef struct treeNode_s * TREENODE;

typedef struct treeNode_s {
  UShort     nodeStateId;
  UShort     branchWordId;
  TREENODE   motherPtr;
  UShort     daughterCount;
} treeNode_t;

TREE     TREE_create();
void     TREE_destroy(TREE tree);
Bool     TREE_initialize(TREE tree);
TREENODE TREE_nodeAlloc(TREE tree);
void     TREE_nodeFree(TREE tree, TREENODE oldNodePtr);
void     TREE_nodeSet(TREENODE  nodePtr, UShort stateId, UShort wordId, TREENODE mother, UShort daughters);
TREENODE TREE_nodeRemoveDaughter(TREE tree, TREENODE  daughterPtr);
void     TREE_prune(TREE tree, TREENODE nodePtr);
TREENODE TREE_invertStalk(TREENODE node);
UShort   TREE_maxUsedNodes(TREE tree);

#ifdef __cplusplus
}
#endif
#endif  /* _TREES_H_ */
