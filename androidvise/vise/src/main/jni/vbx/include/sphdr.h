/* sphdr.h - create and manipulate NIST compatible headers
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (jrt)
 * $__Header$
 * The SPHERE package from NIST was so atrociously written and non-portable
 * that we decided to completely re-implement it from scratch.  We have
 * not always provided the same functionality, so beware.
 *   Abbreviations used:
 *     hdr = header
 *     fld = field
 *     cmnt = comment
 *---------------------------------------------------------------------------*/

#ifndef _HDR_H_
#define _HDR_H_

#if defined(__cplusplus)
extern "C" {
#endif
#include "types.h"

typedef struct hdrFld_s {
  Char *name;
  Int type;
  void *data;
  Int length;
  Char *cmnt;
} hdrFld_t, * HDRF;

#define HDR_SIZE_MULT_MIN  8

typedef struct hdr_s {
  Int  numFlds;
  /* Header size will be multiple of this, which in turn
     must be multiple of HDR_SIZE_MULT_MIN */
  Int  sizeMult;
  HDRF *flds;
} hdr_t, * HDR;

#define PRINTSPHDR(H)  VBX_print("SPHDR: numFlds = %d; flds = %p\n", H->numFlds, H->flds)

#define INT_TYPE  0
#define REAL_TYPE 1
#define STR_TYPE  2

extern HDR    HDR_create(void);
extern Bool   HDR_isNIST(FILE *fp);
extern Bool   HDR_isNISTMem(Ptr buf);
extern Bool   HDR_setSizeMult(HDR hdr, Int sizeMult);
extern Bool   HDR_read(FILE *fp, HDR hdr);
extern Ptr    HDR_parse(Ptr archive, HDR hdr);
extern Bool   HDR_addFld(HDR hdr, const Char *name, Int type, const void *data, const Char *cmnt);
extern void   HDR_addCmnt(HDR hdr, Char *cmnt);
extern Int    HDR_numFlds(HDR hdr);
extern Bool   HDR_fldNames(HDR hdr, Int maxNames, Char *names[]);
extern Bool   HDR_fldCmnts(HDR hdr, Int maxCmnts, Char *cmnts[]);
extern Int    HDR_dataType(HDR hdr, Char *name);
extern void * HDR_data(HDR hdr, const Char *name, Int *length);
extern Char * HDR_cmnt(HDR hdr, Char *name);
extern Int    HDR_chgFldName(HDR hdr, Char *name, Char *newName);
extern Int    HDR_chgFldData(HDR hdr, Char *name, Int type, void *data);
extern Int    HDR_chgFldCmnt(HDR hdr, Char *name, Char *cmnt);
extern Int    HDR_delFld(HDR hdr, Char *name);
extern Int    HDR_delCmnt(HDR hdr, Char *cmnt);
extern void   HDR_clear(HDR hdr);
extern void   HDR_print(HDR hdr);
extern void   HDR_write(FILE *fp, HDR hdr);
extern Ptr    HDR_writeMem(HDR hdr, Int *hdrBytes);
extern void   HDR_free(HDR hdr);
extern void   HDR_copyData(FILE *fp, FILE *outfp);

#if defined(__cplusplus)
}
#endif
#endif /* ifndef _HDR_H_ */
