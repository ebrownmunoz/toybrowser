/* mac - get the "media access controller" (MAC) address
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (craigv)
 *
 * HISTORY
 *  09-Dec-02 Craig Vanderborgh (craigv@voxware.com)
 *      Prepared for checkin
 *---------------------------------------------------------------------------*/
#ifndef _MAC_H_
#define _MAC_H_
#include "pthr.h"

#ifdef __cplusplus
extern "C" {
#endif

Bool MAC_getHwAddr(Char *adapterHint, UChar *hwAddr, Int *hwAddrLen);

#ifdef __cplusplus
}
#endif
#endif /* _MAC_H_ */
