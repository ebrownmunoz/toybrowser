/* sn.h - sequence numbers
 *----------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * HISTORY
 * 28-Apr-95 Created by Dave Vetter (dvetter@rad.verbex.com)
 *
 * $__Header$
 *----------------------------------------------------------------------------*/
#ifndef _SN_H_
#define _SN_H_

#include <limits.h>
#include "types.h"

/* ---------------------------------------------------------------------------*
 * Valid SEQUENCE NUMBERS must be positive and monotonically increasing for a
 * given data stream.  Negative sequence numbers denote exception conditions,
 * and must be registered in iface.h.  At a rate of 100 frames per sec, 64-bit
 * sequence numbers (63 bits positive range) will suffice for over 2.9 billion
 * years, while 32-bits (31 bits positive range) will suffice for 5965 hours
 * (about 249 days), before wrapping around into the negative region.
 *---------------------------------------------------------------------------*/

#define _QUAD_SN_

  /* Sequence Number */
#ifndef _SNDEF_
 #define _SNDEF_
 #ifdef _QUAD_SN_
  typedef Quad sn_t, SN, V_SN;
  #define SN_MAX  QUAD_MAX
  #define SN_MIN  QUAD_MIN
  #define VSNSIZE 8
 #else
  typedef Long sn_t, SN, V_SN;
  #define SN_MAX  INT_MAX
  #define SN_MIN  INT_MIN
  #define VSNSIZE 4
 #endif
#endif

#endif  /* _SN_H_ */
