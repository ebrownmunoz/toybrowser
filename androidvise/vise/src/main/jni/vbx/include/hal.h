#ifndef _HAL_H_
#define _HAL_H_

#include "pthr.h"

#define MAXARGSTR (32)

typedef union ioctlarg_s {
  Float f;
  UInt  u;
  void *p;
  Char  str[MAXARGSTR];
} ioctlarg_t, *IOCTLARG;

typedef struct ioctlreg_s {
  UInt reg;
  UInt val;
} ioctlreg_t, *IOCTLREG;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

UInt HAL_ioctl(UInt cmd, IOCTLARG arg);
typedef UInt (*IOCTLFCN)(UInt cmd, IOCTLARG arg);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#define IOCTLMIN  AUD_IOCTLMIN
#define IOCTLMAX  MISC_IOCTLMAX

/* AUDio ioctls */
#define AUD_IOCTLMIN        AUD_GETOUTPUTGAIN
#define AUD_GETOUTPUTGAIN   (100)
#define AUD_SETOUTPUTGAIN   (101)
#define AUD_GETINPUTGAIN    (102)
#define AUD_SETINPUTGAIN    (103)
#define AUD_GETSIDETONEGAIN (104)
#define AUD_SETSIDETONEGAIN (105)
#define AUD_GETCODECREG     (106)
#define AUD_SETCODECREG     (107)
#define AUD_GETWAVEINHND    (108)
#define AUD_SETWAVEINHND    (109)
#define AUD_GETWAVEOUTHND   (110)
#define AUD_SETWAVEOUTHND   (111)
/*!! CAV: Now I don't think we'll do these */
#define AUD_GETWAVEFORMAT   (112)
#define AUD_SETWAVEFORMAT   (113)
#define AUD_IOCTLMAX        (199)

/* NETwork ioctls */
#define NET_IOCTLMIN        NET_GETMACADDR
#define NET_GETMACADDR      (201)
#define NET_IOCTLMAX        (299)

/* POWer management ioctls */
#define POW_IOCTLMIN        POW_GETPCTAVAIL
#define POW_GETPCTAVAIL     (301)
#define POW_IOCTLMAX        (399)

/* MISCellaneous ioctls */
#define MISC_IOCTLMIN       MISC_GETOEMSTRING
#define MISC_GETOEMSTRING   (401)
#define MISC_IOCTLMAX       (499)

#define EINVALIDARG     1   /* Invalid argument */
#define EUNSUPPORTED    2   /* Unsupported operation */
#define ELIBNOTFOUND    3   /* Dynamic lib not found */
#define EFCNNOTFOUND    4   /* Function not found in dynamic lib */
#define EFAIL           5   /* Operation failed */
#define ELIBFCNFAILED   6   /* Lib fcn failed */
#define ENOINIT         7   /* HAL library has not been initialized */

#endif /* _HAL_H_ */
