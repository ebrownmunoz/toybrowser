/* wrdmap.h - VISE Wordmap Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: wrdmap.h[1.0] Thu Apr 10 20:54:16 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _WRDMAP_H_
#define _WRDMAP_H_

#include "types.h"

/* Structure for mapping model names into word names */
typedef struct wrdmap_s * WRDMAP;

#ifdef __cplusplus
extern "C" { 
#endif

extern WRDMAP WRDMAP_create(void);
extern void   WRDMAP_annihilate(WRDMAP map);
extern Char * WRDMAP_name(WRDMAP map, Char * name);

#ifdef __cplusplus
}
#endif
#endif /* _WRDMAP_H_ */
