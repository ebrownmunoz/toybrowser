LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := vbxsys
# google recommends convenience variables to be prefixed with "MY_"
LOCAL_SRC_FILES := class.c critsect.c imail.c imemory.c mbox.c nbio.c pid.c pthr.c sox.c vbx.c visecmds.c viseerr.c visemsg.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include

include $(BUILD_STATIC_LIBRARY)
