#include "pthr.h"
#include <stdlib.h>
#include "types.h"
#include "vbx.h"
#include "vbxtypes.h"
#include "ifaces.h"
#include "imail.h"
#include "hash.h"
#include "mbox.h"
#include "critsect.h"

/******************************************************************************
 * 
 * File:        imail.c
 *
 * Description: The Mail System Interface Methods
 *
 * Operations:
 *
 *     // IMBox Methods
 *   void    IMBOX_Destroy(IMBox * imbox);
 *   V_Bool  IMBOX_Send(IMBox * imbox, V_Uns mboxId, V_Uns * msg, V_ULong msgLen, V_Int priority, V_Err * status);
 *   V_Bool  IMBOX_Receive(IMBox * imbox, V_Uns * mboxId, V_Uns ** msg, V_ULong * msgLen, V_Bool block);
 *   V_Uns   IMBOX_NumMsgs(IMBox * imbox);
 *   void    IMBOX_Flush(IMBox * imbox);
 *
 *     // IMail Methods
 *   IMail * IMAIL_Create(IMemory * memory, V_Uns maxMsgs);
 *   void    IMAIL_Destroy(IMail * imail);
 *   IMBox * IMAIL_CreateMBox(IMail * imail, V_Char * name, V_Int numPriorities, V_Err * status);
 *   V_Uns   IMAIL_MailBoxId(IMail * imail, V_Char * name, V_Err * status);
 *
 *****************************************************************************/

#define IMAIL_GROWTHQUANTUM  10
#define IMBOX_GROWTHQUANTUM  25

#ifdef VBXDEBUG
  #define VBX_DEBUG(P)  P
#else
  #define VBX_DEBUG(P)
#endif

#undef  INTERFACE
#define INTERFACE MyIMBox

VBX_DECLARE_INTERFACE(MyIMBox) {
  IMBox       parent;
  MBOX        mailbox;
  CRITSECT_DECLARE(busy)
  IMail     * imail;
  Char      * name;
  Uns         id;
};

#undef  INTERFACE
#define INTERFACE MyIMail

VBX_DECLARE_INTERFACE(MyIMail) {
  IMail       parent;
  IMemory   * memory;
  VBXMETHOD (Destroy)  (VBXTHIS) VBXPURE;
  CRITSECT_DECLARE(busy)
  HASH        nameHash;
  MBOX_Att    attributes;  
  MyIMBox  ** mailboxes;
  Uns         numAllocated;
};

typedef struct mboxmsg_s {
  V_Uns       id;
  V_ULong     length;
  V_Uns     * content;
} mboxmsg_t, * MBOXMSG;

#define MBOXMSG_PRINT(S,M) VBX_print("%s MBOXMSG @%p: id = %u, length = %u, content @%p\n", (S), (M), (M)->id, (Uns) ((M)->length), (M)->content)

/* Private Prototype */
static void  IMBOX_DestroyUnlocked(MyIMail  * myIMail, MyIMBox  * myIMBox);

/*****************************************************************************
 *
 * Name:     IMAIL_Create - create a mail system
 *
 ****************************************************************************/

    IMail *
IMAIL_Create(IMemory * memory, V_Uns maxMsgs)
{
  MyIMail * myIMail;
  Uns       growthQuantum = maxMsgs > IMBOX_GROWTHQUANTUM ? IMBOX_GROWTHQUANTUM : maxMsgs;

  if ((myIMail = (MyIMail *) memory->Calloc(memory, 1, sizeof(MyIMail), MAIL_MEMORY))) {
    myIMail->memory = memory;
    if (!(myIMail->nameHash = HASH_create(IMAIL_GROWTHQUANTUM, NULL, NULL, NULL)) ||
        !(myIMail->mailboxes = (MyIMBox **) memory->Calloc(memory, IMAIL_GROWTHQUANTUM, sizeof(MyIMBox *), MAIL_MEMORY)) ||
        !(myIMail->attributes = MBOX_createAttr(memory, growthQuantum, (Uns) maxMsgs, 1)) ||
        !(CRITSECT_CREATE(myIMail->busy, memory, NULL))) {
      IMAIL_Destroy((IMail *) myIMail);
      myIMail = NULL;
    } else {
      myIMail->parent.CreateMBox = IMAIL_CreateMBox;
      myIMail->parent.MailBoxId  = IMAIL_MailBoxId;
      myIMail->numAllocated = IMAIL_GROWTHQUANTUM;
    }
  }

  VBX_DEBUG(VBX_print("IMAIL_Create: created mail system @%p\n", myIMail));

  return((IMail *) myIMail);
}

/*****************************************************************************
 *
 * Name:     IMAIL_Destroy - destroy a mail system
 *
 ****************************************************************************/

    void
IMAIL_Destroy(IMail * imail)
{
  MyIMail  * myIMail = (MyIMail *) imail;
  CRITSECT_DECLARE(myCritsect)

  VBX_DEBUG(VBX_print("IMAIL_Destroy: destroying mail system @%p\n", myIMail));

  if (myIMail) {
    if (CRITSECT_EXISTS(myIMail->busy)) CRITSECT_ENTER(myIMail->busy);
    if (CRITSECT_EXISTS(myIMail->busy)) CRITSECT_ASSIGN(myCritsect, myIMail->busy);
    if (myIMail->mailboxes) {
      while (--myIMail->numAllocated) {
        if (myIMail->mailboxes[myIMail->numAllocated]) {
          VBX_DEBUG(VBX_print("IMAIL_Destroy: destroying mailbox %d @p\n", myIMail->numAllocated, myIMail->mailboxes[myIMail->numAllocated]->mailbox));
          IMBOX_DestroyUnlocked(myIMail, myIMail->mailboxes[myIMail->numAllocated]);
          /* myIMail->mailboxes[myIMail->numAllocated]->parent.Destroy((IMBox *) myIMail->mailboxes[myIMail->numAllocated]); */
        }
      }
      myIMail->memory->Free(myIMail->memory, myIMail->mailboxes);
    }
    if (myIMail->attributes) myIMail->memory->Free(myIMail->memory, myIMail->attributes);
    HASH_annihilate(myIMail->nameHash);
    myIMail->memory->Free(myIMail->memory, myIMail);
    if (CRITSECT_EXISTS(myIMail->busy)) CRITSECT_LEAVE(myCritsect);
    if (CRITSECT_EXISTS(myIMail->busy)) CRITSECT_DESTROY(myCritsect);
  }
}

/*****************************************************************************
 *
 * Name:     IMAIL_CreateMBox - create and register a mailbox
 *
 ****************************************************************************/

    IMBox *
IMAIL_CreateMBox(IMail * imail, const V_Char * name, V_Int numPriorities, V_Err * status)
{
  MyIMail  * myIMail = (MyIMail *) imail;
  MyIMBox ** newAllocation;
  MyIMBox  * myIMBox;
  Ptr        oldId;
  Uns        newId;

  CRITSECT_ENTER(myIMail->busy);

    /* Find the first unused (zero) element beyond the zero-th in the MyIMBox array */
  for (newId = 1; newId < myIMail->numAllocated && myIMail->mailboxes[newId]; newId++);

    /* Try to add or reinstate the name into the name hash table */
  if (HASH_lookup(myIMail->nameHash, (Ptr) name, &oldId)) {
      /* The name is already in the name hash table */
    if (oldId) {
        /* And has a valid (i.e., positive) id */
      *status = MAIL_MBOXEXISTS;
      CRITSECT_LEAVE(myIMail->busy);
      return(NULL);
    } else {
        /* But has no id, so give it the next available one */
      HASH_modify(myIMail->nameHash, (Ptr) name, (Ptr) (newId));
    }
  } else {
      /* The name is not in the name hash table */
    if (!HASH_add(myIMail->nameHash, (Ptr) name, (Ptr) (newId))) {
        /* It cannot be added, so memory must be exhausted */
      *status = MAIL_OUTOFMEMORY;
      CRITSECT_LEAVE(myIMail->busy);
      return(NULL);
    }
  }

    /* Expand the MyIMBox array if necessary */
  if (newId == myIMail->numAllocated) {
      /* The mailbox array needs to expand */
    myIMail->numAllocated += IMAIL_GROWTHQUANTUM;
    if ((newAllocation = (MyIMBox **) myIMail->memory->Calloc(myIMail->memory, myIMail->numAllocated, sizeof(MyIMBox *), MAIL_MEMORY))) {
      memcpy(newAllocation, myIMail->mailboxes, newId * sizeof(MyIMBox *));
      myIMail->memory->Free(myIMail->memory, myIMail->mailboxes);
      myIMail->mailboxes = newAllocation;
    } else {
      *status = MAIL_OUTOFMEMORY;
      CRITSECT_LEAVE(myIMail->busy);
      return(NULL);
    }
  }

    /* Create a new MyIMBox */
  MBOX_attrSetNPriorities(myIMail->attributes, (Uns) numPriorities);
  if ((myIMBox = myIMail->memory->Calloc(myIMail->memory, 1, sizeof(MyIMBox), MAIL_MEMORY)) &&
      (myIMBox->mailbox = MBOX_create(myIMail->memory, myIMail->attributes)) &&
      (CRITSECT_CREATE(myIMBox->busy, myIMail->memory, NULL)) &&
      (myIMBox->name = (Char *) myIMail->memory->Calloc(myIMail->memory, strlen(name) + 1, sizeof(Char), MAIL_MEMORY))) {
    *status = VISESUCCESS;
    myIMail->mailboxes[newId] = myIMBox;
    myIMBox->parent.Destroy = IMBOX_Destroy;
    myIMBox->parent.Send    = IMBOX_Send;
    myIMBox->parent.Receive = IMBOX_Receive;
    myIMBox->parent.NumMsgs = IMBOX_NumMsgs;
    myIMBox->parent.Flush   = IMBOX_Flush;
    myIMBox->imail = imail;
    myIMBox->id = newId;
    strcpy(myIMBox->name, name);
    CRITSECT_LEAVE(myIMail->busy);
    return((IMBox *) myIMBox);
  } else {
    *status = MAIL_OUTOFMEMORY;
    if (CRITSECT_EXISTS(myIMBox->busy)) CRITSECT_DESTROY(myIMBox->busy);
    MBOX_destroy(myIMBox->mailbox);
    if (myIMBox) myIMail->memory->Free(myIMail->memory, myIMBox);
    CRITSECT_LEAVE(myIMail->busy);
    return(NULL);
  }
}

/*****************************************************************************
 *
 * Name:     IMAIL_MailBoxId - return the identification number of a mailbox
 *
 ****************************************************************************/

    V_Uns
IMAIL_MailBoxId(IMail * imail, const V_Char * name, V_Err * status)
{
  MyIMail * myIMail = (MyIMail *) imail;
  Ptr       index;

    /* IMBox must have a positive index */
  if (HASH_lookup(myIMail->nameHash, (Ptr) name, &index) && index) {
    *status = MAIL_SUCCESS;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpointer-to-int-cast"
    return((V_Uns) index);
#pragma GCC diagnostic pop
  } else {
    *status = MAIL_NOSUCHMBOX;
    return((V_Uns) 0);
  }
}

/*****************************************************************************
 *
 * Name:     IMBOX_DestroyUnlocked - destroy and unregister an IMBox without
 *                                   locking the IMail->Busy CRITSECT
 *
 ****************************************************************************/

static void
IMBOX_DestroyUnlocked(MyIMail  * myIMail, MyIMBox  * myIMBox)
{
  CRITSECT_DECLARE(myCritsect)

    /* Remove the IMBox's name from the hash table */
  HASH_modify(myIMail->nameHash, (Ptr) myIMBox->name, (Ptr) NULL);
    /* Remove the IMBox from the IMBox array */
  myIMail->mailboxes[myIMBox->id] = NULL;

  CRITSECT_ENTER(myIMBox->busy);
    /* Destroy the components */
  myIMail->memory->Free(myIMail->memory, myIMBox->name);
  MBOX_destroy(myIMBox->mailbox);
  CRITSECT_ASSIGN(myCritsect, myIMBox->busy);
    /* Free the MyIMBox itself */
  myIMail->memory->Free(myIMail->memory, myIMBox);
  CRITSECT_LEAVE(myCritsect);
  CRITSECT_DESTROY(myCritsect);
}

/*****************************************************************************
 *
 * Name:     IMBOX_Destroy - destroy and unregister an IMBox
 *
 ****************************************************************************/

    void
IMBOX_Destroy(IMBox * imbox)
{
  MyIMBox  * myIMBox = (MyIMBox *) imbox;
  MyIMail  * myIMail = (MyIMail *) myIMBox->imail;

  CRITSECT_ENTER(myIMail->busy);
  IMBOX_DestroyUnlocked(myIMail, myIMBox);
  CRITSECT_LEAVE(myIMail->busy);
}

/*****************************************************************************
 *
 * Name:     IMBOX_Send - send a message from IMBox
 *
 ****************************************************************************/

    V_Bool
IMBOX_Send(IMBox * imbox, V_Uns mboxId, V_Uns * msg, V_ULong msgLen, V_Int priority, V_Err * status)
{
  MyIMBox * myIMBox = (MyIMBox *) imbox;
  MyIMail * myIMail = (MyIMail *) myIMBox->imail;
  MBOXMSG   message;

    /* Assemble the message */
  if ((message = (MBOXMSG) myIMail->memory->Calloc(myIMail->memory, 1, sizeof(mboxmsg_t), MAIL_MEMORY))) {
    message->id = myIMBox->id;
    message->content = msg;
    message->length = msgLen;
      /* If the intended receiver exists, try to send the message */
    if (myIMail->mailboxes[mboxId]) {
      /* VBX_DEBUG(MBOXMSG_PRINT("     IMBOX_Send", message)); */
      if (MBOX_priorityInsert(myIMail->mailboxes[mboxId]->mailbox, (OBJT) message, (Uns) priority))
        *status = MBOX_SUCCESS;
      else
        *status = MBOX_BREAKDOWN;
    } else {
      *status = MBOX_NOSUCHMBOX;
    }
      /* If send fails, free the message and its contents */
    if (*status) {
      if (message->content) myIMail->memory->Free(myIMail->memory, message->content);
      myIMail->memory->Free(myIMail->memory, message);
    }
  } else {
    *status = MBOX_OUTOFMEMORY;
  }

  return(!(V_Bool) *status);
}

/*****************************************************************************
 *
 * Name:     IMBOX_Receive - receive a message through IMBox
 *
 ****************************************************************************/

    V_Bool
IMBOX_Receive(IMBox * imbox, V_Uns * mboxId, V_Uns ** msg, V_ULong * msgLen, V_Bool block)
{
  MyIMBox * myIMBox = (MyIMBox *) imbox;
  MyIMail * myIMail = (MyIMail *) myIMBox->imail;
  MBOXMSG   message;

    /* Extract the message from the mailbox */
  if (block)
    message = (MBOXMSG) MBOX_remove(myIMBox->mailbox);
  else
    message = (MBOXMSG) MBOX_removeNow(myIMBox->mailbox);

  if (message) {
    *mboxId = message->id;
    *msg = message->content;
    *msgLen = message->length;
    /* VBX_DEBUG(MBOXMSG_PRINT("     IMBOX_Receive", message)); */
    myIMail->memory->Free(myIMail->memory, message);
    return(TRUE);
  } else {
    return(FALSE);
  }
}

/*****************************************************************************
 *
 * Name:     IMBOX_NumMsgs - return the number of unread messages in IMBox
 *
 ****************************************************************************/

    V_Uns
IMBOX_NumMsgs(IMBox * imbox)
{
  MyIMBox * myIMBox = (MyIMBox *) imbox;

  return((V_Uns) MBOX_numobj(myIMBox->mailbox));
}

/*****************************************************************************
 *
 * Name:     IMBOX_Flush - empty an IMBox
 *
 ****************************************************************************/

    void
IMBOX_Flush(IMBox * imbox)
{
  MyIMBox * myIMBox = (MyIMBox *) imbox;

  CRITSECT_ENTER(myIMBox->busy);
  while (NULL != MBOX_removeNow(myIMBox->mailbox));
  CRITSECT_LEAVE(myIMBox->busy);
}
