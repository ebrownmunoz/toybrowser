#include "pthr.h"
#include <stdlib.h>
#include "vbxtypes.h"
#include "ifaces.h"
#include "imemory.h"

/******************************************************************************
 * 
 * File:        imemory.c
 *
 * Description: The Memory Allocation Interface Methods
 *
 * Operations:
 *
 *     // Memory Allocation Methods
 *   void * IMEM_calloc(IMemory * imemory, size_t numObj, size_t sizeObj, memtype_t type);
 *   void   IMEM_free(IMemory * imemory, void * allocation);
 *
 *****************************************************************************/

/*****************************************************************************
 *
 * Name:     IMEM_calloc - allocate zeroed memory
 *
 ****************************************************************************/

    void *
IMEM_calloc(IMemory * imemory, size_t numObj, size_t sizeObj, memtype_t type)
{
  return(calloc(numObj, sizeObj));
}

/*****************************************************************************
 *
 * Name:     IMEM_free - free memory
 *
 ****************************************************************************/

    void
IMEM_free(IMemory * imemory, void * allocation)
{
  free(allocation);
}
