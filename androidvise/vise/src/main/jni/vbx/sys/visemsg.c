#include "pthr.h"
#include <string.h>
#include "ifaces.h"
#include "viseerr.h"
#include "visecmds.h"
#include "visemsg.h"
#include "class.h"
#include "sn.h"

#ifdef DEBUG
#include "vbx.h"
#include "visecmds.h"
#define VBX_DEBUG(P) P
#define SHOW_CLASS_SIZE 1
#else
#define VBX_DEBUG(P)
#endif

/******************************************************************************
 * 
 * File:        message.c
 *
 * Description: The Message System
 *
 * Operations:
 *
 *     // Message Port Methods
 *   VBXMSG   MSG_create(IMemory *, IMBox *);
 *   V_Void   MSG_destroy(VBXMSG);
 *   V_Uns    MSG_numUnread(VBXMSG);
 *
 *     // Message Read/Write Methods
 *   V_Uns    MSG_correspondentId(VBXMSG);
 *
 *     // Message Read Methods
 *   V_Bool   MSG_openRead(VBXMSG port, V_Uns * senderId, V_ULong * length, V_Int * cmd, V_Bool block, V_Err * status);
 *   V_Bool   MSG_readInt(VBXMSG port, V_Int * word);
 *   V_Bool   MSG_readUns(VBXMSG port, V_Uns * word);
 *   V_Bool   MSG_readLong(VBXMSG port, V_Long * word);
 *   V_Bool   MSG_readULong(VBXMSG port, V_ULong * word);
 *   V_Bool   MSG_readMels(VBXMSG port, V_Mel * mels, V_ULong count);
 *   V_Bool   MSG_readSN(VBXMSG port, V_SN * sn);
 *   V_Bool   MSG_closeRead(VBXMSG port, V_Err * status);
 *
 *     // Message Write Methods
 *   V_Bool   MSG_openWrite(VBXMSG port, V_Uns receiverId, V_ULong length, V_Int cmd, V_Int priority, V_Err * status);
 *   V_Bool   MSG_writeInt(VBXMSG port, V_Int word);
 *   V_Bool   MSG_writeUns(VBXMSG port, V_Uns word);
 *   V_Bool   MSG_writeLong(VBXMSG port, V_Long word);
 *   V_Bool   MSG_writeULong(VBXMSG port, V_ULong word);
 *   V_Bool   MSG_writeMels(VBXMSG port, V_Mel * mels, V_ULong count);
 *   V_Bool   MSG_writeSN(VBXMSG port, V_SN sn);
 *   V_Bool   MSG_closeWrite(VBXMSG port, V_Err * status);
 *
 *****************************************************************************/

/* NOTE: OBJT_SIZE is in units of V_Mel */
#define CLASS_SIZE       (768)
#define OBJT_SIZE        (128)
#define NOCLASS_MAGIC    (0xffffffff)

  /* The width of an Atomic Message Element (AME) in bits */
#define VAMESIZEINBITS   (VAMESIZE * 8)

  /* The magic longword */
#define MSGMAGICULONG    0x03020100UL
#define MSGMAGICMASK     0x000000ffUL

  /* Swap the two low-order bytes of a mel */
#define MSG_SWAPBYTES(MEL) {                                     \
  V_Mel loByte = (MEL) & ((V_Mel) 0x00ff);                       \
  V_Mel hiByte = (MEL) & ((V_Mel) 0xff00);                       \
  (MEL) = (MEL) & ~((V_Mel) 0xffff) | loByte << 8 | hiByte >> 8; \
}

#if (VLONGSIZEINMELS == 1)  /* If the message system is 32 bits wide */

    /* The bit mask for a 32-bit Atomic Message Element */
  #define VAMEMASK   0xffffffffUL

    /* Swap the two words in a 32-bit mel */
  #define MSG_SWAPWORDS(MEL) {                                           \
    V_Mel loWord = (MEL) & ((V_Mel) 0x0000ffff);                         \
    V_Mel hiWord = (MEL) & ((V_Mel) 0xffff0000);                         \
    (MEL) = (MEL) & ~((V_Mel) 0xffffffff) | loWord << 16 | hiWord >> 16; \
  }

    /* Unscramble a 32-bit mel */
  #define MSG_SWAPMEL(MEL, MAGIC)     \
    switch((MAGIC) & MSGMAGICMASK) {  \
      case 0:                         \
        break;                        \
      case 1:                         \
        MSG_SWAPBYTES(MEL);           \
        break;                        \
      case 2:                         \
        MSG_SWAPWORDS(MEL);           \
        break;                        \
      case 3:                         \
        MSG_SWAPBYTES(MEL);           \
        MSG_SWAPWORDS(MEL);           \
        MSG_SWAPBYTES(MEL);           \
        break;                        \
    }

#else                        /* The message system is only 16 bits wide */

  #define VAMEMASK   0xffffUL

    /* Unscramble a 16-bit mel */
  #define MSG_SWAPMEL(MEL, MAGIC)     \
    switch((MAGIC) & MSGMAGICMASK) {  \
      case 0:                         \
        break;                        \
      case 1:                         \
        MSG_SWAPBYTES(MEL);           \
        break;                        \
    }

#endif

pthread_key_t VBXMSG_key = (pthread_key_t)0;      /* Key for accessing the thread-specific MSG */
CLASS VBXMSG_dfltClass = NULL;        /* CLASS used for msgs <= OBJT_SIZE */

#if 0
/* Statistics Gathering Glop */
pthread_mutex_t mutex;
Int totalMsgs = 0;
Int maxMsgSize = 0;
Int l256cnt = 0;
Int l512cnt = 0;
Int l1024cnt = 0;
Int l2048cnt = 0;
Int l4096cnt = 0;
Int above4kcnt = 0;
#endif

  /* The Message Port Descriptor */
typedef struct vbxmsg_s {
  IMemory     * memory;   /* The memory allocator */
  IMBox       * mailbox;  /* The local mailbox */
} vbxmsg_t;

  /* The Message Descriptor */
typedef struct msg_s {
  V_Uns     mboxId;   /* The Id of the remote mailbox */
  V_Int     priority; /* The priority of the message */
  V_ULong   length;   /* The size of the message (in Mels) */
  V_ULong   melsDone; /* The number of Mels already read or written */
  V_ULong   magic;    /* The byte-swapping key */
  OBJT      objt;     /* OBJT content wrapper */
  V_Mel   * content;  /* Ptr to first element in the message (OBJT contents) */
} msg_t, *MSG;


#define MSG_PRINT(S,M,C)  VBX_print("%s VBXMSG @%p: %s (priority = %d, length = %u, magic = %8.8x, content @%p)\n", \
                                    (S), (M), (C), (Int) ((M)->priority), (Uns) ((M)->length), (Uns) ((M)->magic), ((M)->content))

/*****************************************************************************
 *
 * Name:     MSG_create - create a message port
 *
 ****************************************************************************/

    VBXMSG
MSG_create(IMemory * memory, IMBox * mailbox)
{
  OBJT o1, o2, o3;
  VBXMSG  port;
  /*!! CAV STARTUP SYNCH PROBLEM - WHAT TO DO?? */
		if (VBXMSG_key == (pthread_key_t)0) {
    if (pthread_key_create(&VBXMSG_key, NULL) != 0) {
      VBX_print("MSG_create: FATAL ERROR pthread_key_create() failed!");
						/* STATISTICS     pthread_mutex_init(&mutex, NULL); */
    }
    VBXMSG_dfltClass = CLASS_create(memory, CLASS_SIZE, OBJT_SIZE * VMELSIZE, NULL, NULL);
  }

  if (VBXMSG_dfltClass == NULL) {
    VBX_print("MSG_create: FATAL ERROR, CLASS IS NULL\n");
    return NULL;
  }

  port = (VBXMSG) memory->Calloc(memory, 1, sizeof(vbxmsg_t), MAIL_MEMORY);

  if (port) {
    port->memory = memory;
    port->mailbox = mailbox;
  }

  return(port);
}

/*****************************************************************************
 *
 * Name:     MSG_destroy - get rid of a message port
 *
 ****************************************************************************/

    V_Void
MSG_destroy(VBXMSG port)
{
  if (port) {
    port->memory->Free(port->memory, port);
  }
}

/*****************************************************************************
 *
 * Name:     MSG_numUnread - number of messages received but not yet read
 *
 ****************************************************************************/

    V_Uns
MSG_numUnread(VBXMSG port)
{
  return(port->mailbox->NumMsgs(port->mailbox));
}

/*****************************************************************************
 *
 * Name:     MSG_correspondentId - the Id of the remote mailbox
 *
 ****************************************************************************/

    V_Uns
MSG_correspondentId(VBXMSG port)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);
  return(msg->mboxId);
}

/*****************************************************************************
 *
 * Name:     MSG_numMelsRemaining - the number of mels unread or unwritten
 *
 ****************************************************************************/

    V_Long
MSG_numMelsRemaining(VBXMSG port)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);
  if (msg == NULL) {
    VBX_print("MSG_numMelsRemaining: ERROR msg is NULL");
    return(0);
  } else {
    return((V_Long) msg->length - (V_Long) msg->melsDone);
  }
}

/*****************************************************************************
 *
 * Name:     MSG_openRead - Open a message for reading
 *
 ****************************************************************************/

    V_Bool
MSG_openRead(VBXMSG port, V_Uns * senderId, V_ULong * length, V_Int * cmd, V_Bool block, V_Err * status)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);
  if (msg == NULL) {
    msg = (MSG) port->memory->Calloc(port->memory, 1, sizeof(msg_t), MAIL_MEMORY);
    if (pthread_setspecific(VBXMSG_key, (void *)msg) != 0) {
      VBX_print("MSG_openRead: pthread_setspecific FAILS for new msg\n");
    }
  }
  *senderId = 0;
  *length = 0;
  *cmd = _NOCOMMAND;

  if (msg->content) {
    *status = MESSAGEOPEN;

  } else if (port->mailbox->Receive(port->mailbox, senderId, &msg->objt, &msg->length, block)) {
    msg->content = (V_Mel *) &OBJT_contents(msg->objt);

    msg->mboxId = *senderId;
    msg->magic = MSGMAGICULONG;
    msg->melsDone = 0;

      /* Get the magic longword */
    MSG_readULong(port, &msg->magic);

      /* Get the command word */
    MSG_readInt(port, cmd);

    VBX_DEBUG(MSG_PRINT("    MSG_openRead", msg, VISECMD_string((V_Cmd) *cmd)));

    if (msg->melsDone > msg->length) {
      *length = (V_ULong) 0;
      *status = MESSAGETOOSHORT;
    } else {
        /* Adjust the reported length for the command word and the magic longword */
      *length = msg->length - msg->melsDone;
      *status = VISESUCCESS;
    }

  } else {
    *status = NOMESSAGES;
  }

  return(!(V_Bool) *status);
}

/*****************************************************************************
 *
 * Name:     MSG_readInt   - Read a V_Int from the message
 *           MSG_readUns   - Read a V_Uns from the message
 *           MSG_readLong  - Read a V_Long from the message
 *           MSG_readULong - Read a V_ULong from the message
 *           MSG_readSN    - Read a sequence number from the message
 *
 * Returns:  FALSE if there are not enough V_Mels still unread in the message
 *           to make up the requested type; TRUE otherwise.
 *
 * Requires: Must have called MSG_openRead() on the specified port
 *
 ****************************************************************************/

  /* An integer always fits in a mel */

    V_Bool
MSG_readInt(VBXMSG port, V_Int * word)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  if (msg->melsDone < msg->length) {
    MSG_SWAPMEL(msg->content[msg->melsDone], msg->magic);
    *word = (V_Int) msg->content[msg->melsDone++];
    return(TRUE);
  } else {
    msg->melsDone++;
    return(FALSE);
  }
}

    V_Bool
MSG_readUns(VBXMSG port, V_Uns * word)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  if (msg->melsDone < msg->length) {
    MSG_SWAPMEL(msg->content[msg->melsDone], msg->magic);
    *word = (V_Uns) msg->content[msg->melsDone++];
    return(TRUE);
  } else {
    msg->melsDone++;
    return(FALSE);
  }
}

  /* A long may or may not fit in a mel */

    V_Bool
MSG_readLong(VBXMSG port, V_Long * longWord)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  #if (VLONGSIZEINMELS == 1)
    if (msg->melsDone < msg->length) {
      MSG_SWAPMEL(msg->content[msg->melsDone], msg->magic);
      *longWord = (V_Long) msg->content[msg->melsDone++];
      return(TRUE);
    } else {
      msg->melsDone++;
      return(FALSE);
    }
  #else
    V_Int  melCount;
    if ((msg->melsDone + VLONGSIZEINMELS) <= msg->length) {
      for (melCount = 0; melCount < VLONGSIZEINMELS; melCount++) {
        MSG_SWAPMEL(msg->content[msg->melsDone + melCount], msg->magic);
        *longWord = *longWord << VAMESIZEINBITS  | msg->content[msg->melsDone + melCount] & VAMEMASK;
      }
      msg->melsDone += VLONGSIZEINMELS;
      return(TRUE);
    } else {
      msg->melsDone += VLONGSIZEINMELS;
      return(FALSE);
    }
  #endif
}

    V_Bool
MSG_readULong(VBXMSG port, V_ULong * longWord)
{
  V_Int  melCount;
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  *longWord = 0;
  #if (VLONGSIZEINMELS == 1)
    if (msg->melsDone < msg->length) {
      MSG_SWAPMEL(msg->content[msg->melsDone], msg->magic);
      *longWord = (V_ULong) msg->content[msg->melsDone++];
      return(TRUE);
    } else {
      msg->melsDone++;
      return(FALSE);
    }
  #else
    if ((msg->melsDone + VLONGSIZEINMELS) <= msg->length) {
      for (melCount = 0; melCount < VLONGSIZEINMELS; melCount++) {
        MSG_SWAPMEL(msg->content[msg->melsDone + melCount], msg->magic);
        *longWord = *longWord << VAMESIZEINBITS  | msg->content[msg->melsDone + melCount] & VAMEMASK;
      }
      msg->melsDone += VLONGSIZEINMELS;
      return(TRUE);
    } else {
      msg->melsDone += VLONGSIZEINMELS;
      return(FALSE);
    }
  #endif
}

  /* A sequence number may or may not fit in a mel */

    V_Bool
MSG_readSN(VBXMSG port, V_SN * sn)
{
  V_Int  melCount;
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);
  *sn = 0;

  #if (VSNSIZEINMELS == 1)
    if (msg->melsDone < msg->length) {
      *sn = (V_ULong) msg->content[msg->melsDone++];
      return(TRUE);
    } else {
      msg->melsDone++;
      return(FALSE);
    }
  #else
    if ((msg->melsDone + VSNSIZEINMELS) <= msg->length) {
      for (melCount = 0; melCount < VSNSIZEINMELS; melCount++) {
        MSG_SWAPMEL(msg->content[msg->melsDone + melCount], msg->magic);
        *sn = *sn << VAMESIZEINBITS  | msg->content[msg->melsDone + melCount] & VAMEMASK;
      }
      msg->melsDone += VSNSIZEINMELS;
      return(TRUE);
    } else {
      msg->melsDone += VSNSIZEINMELS;
      return(FALSE);
    }
  #endif
}

/*****************************************************************************
 *
 * Name:     MSG_readMels - Read an array of mels from the message
 *
 * Returns:  FALSE if there are not enough V_Mels still unread in the message
 *           to fulfill the request; TRUE otherwise.
 *
 * Requires: Must have called MSG_openRead() on the specified port
 *
 ****************************************************************************/

    V_Bool
MSG_readMels(VBXMSG port, V_Mel * mels, V_ULong count)
{
  V_Bool  status;
  V_Long  index;
  MSG     msg = (MSG) pthread_getspecific(VBXMSG_key);
  V_Long  room = (V_Long) msg->length - (V_Long) msg->melsDone;

  if (room >= (V_Long) count) {
    room = (V_Long) count;
    status = TRUE;
  } else {
    if (room < 0) room = 0;
    status = FALSE;
  }

    /* Copy as many of the mels as are available */
  for (index = 0; index < room; index++) {
    mels[index] = msg->content[msg->melsDone++];
    MSG_SWAPMEL(mels[index], msg->magic);
  }
    /* Just count the rest */
  msg->melsDone += count - (V_ULong) room;

  return(status);
}

/*****************************************************************************
 *
 * Name:     MSG_closeRead - Close a message just read
 *
 * Requires: Must have called MSG_openRead() on the specified port
 *
 ****************************************************************************/

    V_Bool
MSG_closeRead(VBXMSG port, V_Err * status)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

    /* If the message is still open */
  if (msg->content) {

    VBX_DEBUG(MSG_PRINT("    MSG_closeRead", msg, ""));

      /* Free the message contents */
    VBX_DEBUG(VBX_print("    MSG_closeRead: freeing OBJT 0x%x pointer.clas 0x%x\n", msg->objt, msg->objt->pointer.clas));
    if (msg->objt->pointer.clas == (CLASS)NOCLASS_MAGIC) {
      VBX_DEBUG(VBX_print("    MSG_closeRead: freeing malloced OBJT 0x%x\n", msg->objt));
      port->memory->Free(port->memory, msg->objt);
    } else {
      OBJT_free(msg->objt);
#if SHOW_CLASS_SIZE
      VBX_DEBUG(VBX_print("    MSG_closeRead: message class size = %d\n", CLASS_free(VBXMSG_dfltClass)));
#endif
    }

      /* Close the message */
    msg->objt = NULL;
    msg->content = NULL;

    if (msg->melsDone == msg->length) {
      *status = VISESUCCESS;
    } else if (msg->melsDone > msg->length) {
      *status = MESSAGEOVERFLOW;
      VBX_DEBUG(VBX_print("    MSG_closeRead: %d mels too many read\n", (Int) (msg->melsDone - msg->length)));
    } else {
      *status = MESSAGEINCOMPLETE;
      VBX_DEBUG(VBX_print("    MSG_closeRead: %d mels unread\n", (Int) (msg->length - msg->melsDone)));
    }

    /* Otherwise, report that it is already closed */
  } else {
    *status = MESSAGECLOSED;
  }

  return(!(V_Bool) *status);
}

/* Helper function to create an allocation with dummy OBJT struct wrapper */
OBJT MSG_callocObjt(VBXMSG port, Int length, Int size) {
  OBJT objt;
  UInt sizeObjt;
  Int sizeContents = length * size;

  sizeObjt = sizeof(OBJT_t) + sizeContents - sizeof(Int);
  objt = (OBJT) port->memory->Calloc(port->memory, 1, sizeObjt, MAIL_MEMORY);
  if (objt != NULL) {
    objt->pointer.clas = (CLASS) NOCLASS_MAGIC;
  }

  return(objt);
}

/*****************************************************************************
 *
 * Name:     MSG_openWrite - Open a message for writing
 *
 ****************************************************************************/

    V_Bool
MSG_openWrite(VBXMSG port, V_Uns receiverId, V_ULong length, V_Int cmd, V_Int priority, V_Err * status)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  if (msg == NULL) {
    msg = (MSG) port->memory->Calloc(port->memory, 1, sizeof(msg_t), MAIL_MEMORY);
    if (pthread_setspecific(VBXMSG_key, (void *)msg) != 0) {
      VBX_print("    MSG_openWrite: pthread_setspecific FAILS for new msg\n");
    }
  }

    /* Adjust the length to include the command word and the two magic words */
  length += 3;

#if 0
  pthread_mutex_lock(&mutex);
  totalMsgs++;
  if (length < 256) {
    l256cnt++;
		} else if (length < 512) {
    l512cnt++;
  } else if (length < 1024) {
    l1024cnt++;
  } else if (length < 2048) {
    l2048cnt++;
  } else if (length < 4096) {
    l4096cnt++;
  } else {
    above4kcnt++;
  }
  pthread_mutex_unlock(&mutex);
  if (!(totalMsgs % 500)) {
    VBX_print("MSG histo: msgs %d l256 %d l512 %d l1k %d l2k %d l4k %d >4k %d\n", totalMsgs, l256cnt, 
														l512cnt, l1024cnt, l2048cnt, l4096cnt, above4kcnt);
  }
  
  if (length >= 256) {
    VBX_print("MSG INFO: cmd %d length %d\n", cmd, length);
  }
#endif

    /* Check that the message is not already open */
  if (msg->content) {
    VBX_print("    MSG_openWrite: FAILED because message already open!\n");
    *status = MESSAGEOPEN;

  } else {
    if (length <= OBJT_SIZE) {
      msg->objt = OBJT_new(VBXMSG_dfltClass);
       /* Check for CLASS exhaustion - we can deal with it, but advise anyone watching! */
      if (msg->objt == NULL) {
        VBX_print("    MSG_openWrite: WARNING - dfltClass exhausted, reverting to malloc for all msgs!\n"); 
      }
    }

#if SHOW_CLASS_SIZE
    if (msg->objt != NULL) VBX_DEBUG(VBX_print("    MSG_openWrite: message class size = %d\n", CLASS_free(VBXMSG_dfltClass)));
#endif

    if (msg->objt == NULL) {
						msg->objt = MSG_callocObjt(port, length, VMELSIZE);
  				if (msg->objt == NULL)
        goto failure;
    }

    VBX_DEBUG(VBX_print("    MSG_openWrite: length %d msg->objt 0x%x msg->objt->pointer.clas 0x%x\n", length, msg->objt, msg->objt->pointer.clas));

    msg->content = (V_Mel *) &OBJT_contents(msg->objt);

      /* Initialize the message */
    msg->mboxId = receiverId;
    msg->priority = priority;
    msg->length = length;
    msg->melsDone = 0;

      /* Write magic longword and command (cannot fail) */
    MSG_writeULong(port, MSGMAGICULONG);
    MSG_writeInt(port, cmd);

    *status = VISESUCCESS;

    VBX_DEBUG(MSG_PRINT("    MSG_openWrite", msg, VISECMD_string((V_Cmd) cmd)));
    return(!(V_Bool) *status);
  }

	failure:
  VBX_print("MSG_openWrite: failed due to malloc failure!\n");
  msg->length = 0;
  msg->melsDone = 0;
  *status = OUTOFMEMORY;

  return(!(V_Bool) *status);
}

/*****************************************************************************
 *
 * Name:     MSG_writeInt   - Append a V_Int to a message
 *           MSG_writeUns   - Append a V_Uns to a message
 *           MSG_writeLong  - Append a V_Long to a message
 *           MSG_writeULong - Append a V_ULong to a message
 *
 * Returns:  FALSE if there are not enough V_Mels still unwritten in the
 *           message to hold the requested type; TRUE otherwise.
 *
 * Requires: Must have called MSG_openWrite() on the specified port
 *
 ****************************************************************************/

  /* An integer will always fit in a mel */

    V_Bool
MSG_writeInt(VBXMSG port, V_Int word)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  if (msg->melsDone < msg->length) {
    msg->content[msg->melsDone++] = (V_Mel) word;
    return(TRUE);
  } else {
    msg->melsDone++;
    return(FALSE);
  }
}

    V_Bool
MSG_writeUns(VBXMSG port, V_Uns word)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  if (msg->melsDone < msg->length) {
    msg->content[msg->melsDone++] = (V_Mel) word;
    return(TRUE);
  } else {
    msg->melsDone++;
    return(FALSE);
  }
}

  /* A long may or may not fit in a mel */

    V_Bool
MSG_writeLong(VBXMSG port, V_Long longWord)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  #if (VLONGSIZEINMELS == 1)
    if (msg->melsDone < msg->length) {
      msg->content[msg->melsDone++] = (V_Mel) longWord;
      return(TRUE);
    } else {
      msg->melsDone++;
      return(FALSE);
    }
  #else
    V_Int  melCount;
    if ((msg->melsDone + VLONGSIZEINMELS) <= msg->length) {
      for (melCount = VLONGSIZEINMELS; melCount--; ) {
        msg->content[msg->melsDone + melCount] = (V_Mel) (longWord & (V_Long) VAMEMASK);
        longWord = longWord >> VAMESIZEINBITS;
      }
      msg->melsDone += VLONGSIZEINMELS;
      return(TRUE);
    } else {
      msg->melsDone += VLONGSIZEINMELS;
      return(FALSE);
    }
  #endif
}

    V_Bool
MSG_writeULong(VBXMSG port, V_ULong longWord)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  #if (VLONGSIZEINMELS == 1)
    if (msg->melsDone < msg->length) {
      msg->content[msg->melsDone++] = (V_Mel) longWord;
      return(TRUE);
    } else {
      msg->melsDone++;
      return(FALSE);
    }
  #else
    V_Int  melCount;
    if ((msg->melsDone + VLONGSIZEINMELS) <= msg->length) {
      for (melCount = VLONGSIZEINMELS; melCount--; ) {
        msg->content[msg->melsDone + melCount] = (V_Mel) (longWord & (V_ULong) VAMEMASK);
        longWord = longWord >> VAMESIZEINBITS;
      }
      msg->melsDone += VLONGSIZEINMELS;
      return(TRUE);
    } else {
      msg->melsDone += VLONGSIZEINMELS;
      return(FALSE);
    }
  #endif
}

  /* A sequence number may or may not fit in a mel */

    V_Bool
MSG_writeSN(VBXMSG port, V_SN sn)
{
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

  #if (VSNSIZEINMELS == 1)
    if (msg->melsDone < msg->length) {
      msg->content[msg->melsDone++] = (V_Mel) sn;
      return(TRUE);
    } else {
      msg->melsDone++
      return(FALSE);
    }
  #else
    V_Int  melCount;
    if ((msg->melsDone + VSNSIZEINMELS) <= msg->length) {
      for (melCount = VSNSIZEINMELS; melCount--; ) {
        msg->content[msg->melsDone + melCount] = (V_Mel) (sn & (V_SN) VAMEMASK);
        sn = sn >> VAMESIZEINBITS;
      }
      msg->melsDone += VSNSIZEINMELS;
      return(TRUE);
    } else {
      msg->melsDone += VSNSIZEINMELS;
      return(FALSE);
    }
  #endif
}

/*****************************************************************************
 *
 * Name:     MSG_writeMels - Append an array of mels to a message
 *
 * Returns:  FALSE if there are not enough V_Mels still unwritten in the
 *           message to hold the array; TRUE otherwise.
 *
 * Requires: Must have called MSG_openWrite() on the specified port
 *
 ****************************************************************************/

    V_Bool
MSG_writeMels(VBXMSG port, V_Mel * mels, V_ULong count)
{
  V_Bool  status;
  V_Long  index;
  MSG     msg = (MSG) pthread_getspecific(VBXMSG_key);
  V_Long  room = (V_Long) msg->length - (V_Long) msg->melsDone;

  if (room >= (V_Long) count) {
    room = (V_Long) count;
    status = TRUE;
  } else {
    if (room < 0) room = 0;
    status = FALSE;
  }

    /* Write as many of the mels as will fit */
  for (index = 0; index < room; index++) {
    msg->content[msg->melsDone++] = mels[index];
  }
    /* Just count the rest */
  msg->melsDone += count - (V_ULong) room;

  return(status);
}

/*****************************************************************************
 *
 * Name:     MSG_closeWrite - Close a message just written
 *
 * Requires: Must have called MSG_openWrite() on the specified port
 *
 ****************************************************************************/

    V_Bool
MSG_closeWrite(VBXMSG port, V_Err * status)
{
  V_Err   mailStatus;
  V_Mel * content;
  MSG msg = (MSG) pthread_getspecific(VBXMSG_key);

    /* If the message is open */
  if (msg->content) {

    if (msg->melsDone == msg->length) {
      *status = VISESUCCESS;
    } else if (msg->melsDone < msg->length) {
        /* Fill out the message with zeros */
      memset((void *) (msg->content + msg->melsDone), 0, (size_t) ((msg->length - msg->melsDone) * VMELSIZE));
      *status = MESSAGEINCOMPLETE;
      VBX_DEBUG(VBX_print("    MSG_closeWrite: %d mels unread\n", (Int) (msg->length - msg->melsDone)));
    } else {
      *status = MESSAGEOVERFLOW;
    }

      /* Get a local pointer to the message contents */
				/*    content = msg->content; */
    content = (V_Mel *) msg->objt;

      /* Close the message before sending it, in case the receiver invalidates the message structure */
    msg->objt = NULL;
    msg->content = NULL;

    VBX_DEBUG(MSG_PRINT("    MSG_closeWrite", msg, ""));

      /* Send the message */
    port->mailbox->Send(port->mailbox, msg->mboxId, content, msg->length, msg->priority, &mailStatus);

      /* A Send() error takes precedence over length errors */
    if (mailStatus) *status = mailStatus;

    /* Otherwise, report that it is already closed */
  } else {
    *status = MESSAGECLOSED;
  }

  return(!(V_Bool) *status);
}
