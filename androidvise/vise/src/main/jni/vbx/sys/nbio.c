/* nbio.c - Routines for doing non-blocking i/o on file descriptors
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (cav)
 *---------------------------------------------------------------------------*/
#include "pthr.h"
#if defined(VXWORKS)
#include "ioLib.h"
#include "selectLib.h"
#else
#if defined(sol)
#define BSD_COMP
#endif
#if defined(DEC)
#include <sys/time.h>
#endif
#if defined(CYGWIN)
#include <sys/termios.h>
#endif
#include <sys/ioctl.h>
#include <sys/types.h>
#endif
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "nbio.h"
#include "msec.h"
#include "vbx.h"

#define  IORETRYMSEC   (250)

#ifdef VBXDEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

void NBIO_setblock(Int fd, Bool on)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the blocking attribute on the given file descriptor to enable
 * or disable non-blocking i/o.  The value of the "on" parameter
 * determines whether blocking is enabled;  if "on" is TRUE,
 * blocking is enabled.  Conversely, if "on" is FALSE, the file
 * descriptor is put in non-blocking mode.
 *---------------------------------------------------------------------------*/
{
  Int  status;
  Bool nonblock = !on;

  if (status = ioctl(fd, FIONBIO, (Int)&nonblock)) {
    VBX_print("setblock: ioctl ERROR, errno %d\n", errno);
  }
}

Int NBIO_select(Int fd, Int bitmask, Int msec)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * A wrapper routine to conceal the fairly ghastly details of using select
 * for the special case of a single file descriptor.  A select() with the
 * specified timeout called using the given file descriptor.  The "msec"
 * timeout is the desired timeout in milliseconds.  "bitmask" specifies
 * the types of file descriptor notifications that are desired.  
 * RETURN: A bitmask containing the notification(s) that awoke the select.
 *         Zero is returned if the select() timed out.
 *---------------------------------------------------------------------------*/
{
  fd_set *rPtr = NULL, *wPtr = NULL, *ePtr = NULL;
  fd_set  rfds, wfds, efds;
  struct  timeval tv;
  Int     nfds, secs;
  Int     rval = 0;

  /* Zero fd_sets and set pointers appropriately */
  if (bitmask & NBIO_SELREAD) {
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    rPtr = &rfds;
  }
  if (bitmask & NBIO_SELWRITE) {
    FD_ZERO(&wfds);
    FD_SET(fd, &wfds);
    wPtr = &wfds;
  }
  if (bitmask & NBIO_SELEXC) {
    FD_ZERO(&efds);
    FD_SET(fd, &efds);
    ePtr = &efds;
  }

  /* Set up the timeval structure */
  tv.tv_sec = msec / 1000;
  tv.tv_usec = msec % 1000 * 1000;

  nfds = select(fd + 1, rPtr, wPtr, ePtr, &tv);
  
  VBX_DEBUG(VBX_print("select() ret = %d, errno = %d\n", nfds, errno));

  if (nfds == 0) {
    VBX_DEBUG(VBX_print("NBIO_select: select() timed out (%d) errno = %d\n", msec));
    return 0;
  }

  if (rPtr && FD_ISSET(fd, rPtr)) rval |= NBIO_SELREAD;
  if (wPtr && FD_ISSET(fd, wPtr)) rval |= NBIO_SELWRITE;
  if (ePtr && FD_ISSET(fd, ePtr)) rval |= NBIO_SELEXC;
  return(rval);
}
    
Int NBIO_read(Int fd, Char *bytes, Int byteCnt, Int msec)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read byteCnt bytes to fd from the given buffer bytes in non-blocking
 * mode.  Note that this routine could also be used if fd is set to 
 * blocking mode.
 * RETURN:  Number of bytes read, or ERROR if timeout
 *---------------------------------------------------------------------------*/
{
  Bool timedout = FALSE;
  Int  bytesLeft = byteCnt;
  Int  bytesRead = 0;
  Int  nread, sstat;
  Int  then;

  then = MSEC_getCurrentTime();

  VBX_DEBUG(VBX_print("NBIO_read: read() %d 0x%x %d %d\n", fd, bytes, byteCnt, msec));
  while (bytesLeft) {
    nread = read(fd, bytes, bytesLeft);
    VBX_DEBUG(VBX_print("NBIO_read: read() nread = %d errno = %d\n", nread, errno));
    if (nread > 0) {
      bytesRead += nread;
      bytesLeft -= nread;
      bytes += nread;
    } else if (nread == 0) {
      /* We have encountered EOF.  Return number of bytes actually read */
      return(bytesRead);
    }

    if (nread == ERROR && bytesLeft) {
      if (errno == EWOULDBLOCK) {
        if (sstat = NBIO_select(fd, NBIO_SELREAD|NBIO_SELEXC, IORETRYMSEC)) {
          /* An exception has occurred, so punt! */
          if (sstat & NBIO_SELEXC) {
            return(ERROR);
          }
          if (sstat & NBIO_SELREAD) {
            continue;
          }
        }

        /* select() timed out.  Continue if time remains */
        if (MSEC_getCurrentTime() - then > msec) {
          VBX_DEBUG(VBX_print("NBIO_read: timed out after %d msec\n", msec));
          errno = ETIMEDOUT;
          return(ERROR);
        }
      } else {
        VBX_print("NBIO_read: read() failed, errno = %d\n", errno);
        return(ERROR);
      }
    }
  }
  return(bytesRead);
}
    
Int NBIO_write(Int fd, Char *bytes, Int byteCnt, Int msec)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write byteCnt bytes to fd from the given buffer bytes in non-blocking
 * mode.  Note that this routine could also be used if fd is set to 
 * blocking mode.
 * RETURN:  Number of bytes written, or ERROR if timeout
 *---------------------------------------------------------------------------*/
{
  Bool timedout = FALSE;
  Int  bytesWritten = 0;
  Int  bytesLeft = byteCnt;
  Int  nwritten, sstat;
  Int  then;

  then = MSEC_getCurrentTime();

  while (bytesLeft) {
    nwritten = write(fd, bytes, bytesLeft);
    VBX_DEBUG(VBX_print("NBIO_write: write() nwritten = %d errno = %d\n", nwritten, errno));
    if (nwritten > 0) {
      bytesLeft -= nwritten;
      bytes += nwritten;
    } else if (nwritten == 0) {
      /* Return the number of bytes actually written */
      return(nwritten);
    }

    if (nwritten == ERROR && bytesLeft) {
      if (errno == EWOULDBLOCK) {
        if (sstat = NBIO_select(fd, NBIO_SELWRITE|NBIO_SELEXC, IORETRYMSEC)) {
          /* An exception has occurred, so punt! */
          if (sstat & NBIO_SELEXC) {
            return(FALSE);
          }
        }
        if (sstat & NBIO_SELWRITE) {
          continue;
        }

        /* select() timed out or fd ready for writing.  Continue if time remains */
        if (MSEC_getCurrentTime() - then > msec) {
          VBX_DEBUG(VBX_print("NBIO_write: timed out after %d msec\n", msec));
          errno = ETIMEDOUT;
          return(FALSE);
        }
      } else {
        VBX_print("NBIO_write: write() failed, errno = %d\n", errno);
        return(ERROR);
      }
    }
  }
  return(TRUE);
}
           
  
  
