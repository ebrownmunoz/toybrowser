/* sox.c - Routines to handle creation and manipulation of sockets
 *---------------------------------------------------------------------------*
 * Voxware Incorporated (cav)
 *---------------------------------------------------------------------------*/
#include "pthr.h"
#if defined(VXWORKS)
#include "ioLib.h"
#include "sockLib.h"
#else
#include <sys/types.h>
#endif
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <string.h>

#include "nbio.h"
#include "sox.h"
#include "vbx.h"

#define IORETRYMSEC   (250)
#define LINGERTICKS   (75)

#if defined(VXWORKS)
#define SHUT_RD       (0)
#define SHUT_WR       (1)
#define SHUT_RDWR     (2)
#endif

#define VBXDEBUG
#ifdef VBXDEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

Bool SOX_createTCP(SOX sox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a SOX descriptor and obtain a TCP socket (SOCK_STREAM)
 *---------------------------------------------------------------------------*/
{
  Int fd;

  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == ERROR) {
    VBX_print("SOX_createTCP: ERROR - socket() fails, errno = %d\n", errno);
    return FALSE;
  }

  memset((Char *) sox, 0, sizeof(sox_t));
  sox->fd = fd;
  return((fd > 0) ? TRUE : FALSE);
}

Bool SOX_close(SOX sox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Shut down a SOX descriptor of any type
 *---------------------------------------------------------------------------*/
{
  Int rval;

  errno = 0;
  if (sox) {
    if (sox->fd) {
      rval = close(sox->fd);
      VBX_DEBUG(VBX_print("SOX_destroy: close() returns %d errno = %d\n",
                rval, errno));
      sox->fd = 0;
    }
  }
  return((rval == 0) ? TRUE : FALSE);
}
   
Bool SOX_configTCPClient(SOX sox, UInt server, Int serverPort)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Configure a SOX for use as a TCP client.  Use SOX_connect() to establish the
 * connection.
 *---------------------------------------------------------------------------*/
{
  Int sockAddrSize;
  Int optVal = 1;
  struct linger linger;

  Int fd = sox->fd;

  /* Specify the SO_REUSEADDR option  to bind a stream socket to a local  
   * port  that may be still bound to another stream socket that may be 
   * hanging around with a "zombie" protocol control block whose context
   * is  not yet freed from previous sessions */

  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *) &optVal,
                 sizeof(optVal)) == ERROR) {
    perror("SOX_configTCPClient: setsockopt SO_REUSEADDR failed");
    close(fd);
    return(FALSE);
  }

  /* Specify the TCP_NODELAY option for protocols such as the VJML
   * Protocol that require immediate delivery of many small messages. 
   *
   * By default VxWorks uses congestion avoidance algorithm for virtual  
   * terminal  protocols and  bulk  data  transfer protocols.  When the TCP_NODELAY 
   * option is turned on and there are segments to be sent out, TCP  bypasses the 
   * congestion avoidance algorithm and sends the segments out when there 
   * is enough space in the send window */

  if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *) &optVal,
                 sizeof(optVal)) == ERROR) {
    perror("SOX_configTCPClient: setsockopt TCP_NODELAY failed");
    close(fd);
    return(FALSE);
  }

  /* Specify the SO_KEEPALIVE option, and the transport protocol (TCP)  
   * initiates  a timer to detect a dead connection which prevents an
   * an application from  hanging on an invalid connection.
   */
  if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *) &optVal,
                 sizeof(optVal)) == ERROR) {
    perror("SOX_configTCPClient: setsockopt SO_KEEPALIVE failed");
    close(fd);
    return(FALSE);
  }

  linger.l_onoff = 1;
  linger.l_linger = LINGERTICKS;

  if (setsockopt(fd, SOL_SOCKET, SO_LINGER, (char *) &linger, sizeof(linger))
      == ERROR) {
    perror("SOX_configTCPClient: setsockopt SO_LINGER failed");
    close(fd);
    return(FALSE);
  }

  sockAddrSize = sizeof(struct sockaddr_in);
  memset((Char *) &sox->serverAddr, 0, sockAddrSize);
  sox->serverAddr.sin_family = AF_INET;
  sox->serverAddr.sin_port = htons(serverPort);
  sox->serverAddr.sin_addr.s_addr = server;

  return(TRUE);
}

Bool SOX_configTCPServer(SOX sox, Int serverPort, Int maxConn)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Configure a SOX for use as a TCP server.  Use SOX_connect() to establish the
 * connection.
 *---------------------------------------------------------------------------*/
{
  Int sockAddrSize;
  Int optVal = 1, fd;
  struct linger linger;

  fd = sox->fd;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *) &optVal,
                 sizeof(optVal)) == ERROR) {
    perror("SOX_configTCPServer: setsockopt SO_REUSEADDR failed");
    close(fd);
    return(FALSE);
  }

  if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *) &optVal,
                 sizeof(optVal)) == ERROR) {
    perror("SOX_configTCPServer: setsockopt SO_KEEPALIVE failed");
    close(fd);
    return(FALSE);
  }

  if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char *) &optVal,
                 sizeof(optVal)) == ERROR) {
    perror("SOX_configTCPServer: setsockopt TCP_NODELAY failed");
    close(fd);
    return(FALSE);
  }

  linger.l_onoff = 1;
  linger.l_linger = 0;

  if (setsockopt(fd, SOL_SOCKET, SO_LINGER, (char *) &linger, sizeof(linger))
      == ERROR) {
    perror("SOX_configTCPServer: setsockopt SO_LINGER failed");
    close(fd);
    return(FALSE);
  }

  sockAddrSize = sizeof(struct sockaddr_in);
  memset((Char *) &sox->serverAddr, 0, sockAddrSize);
  sox->serverAddr.sin_family = AF_INET;
  sox->serverAddr.sin_port = htons(serverPort);
  sox->serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sox->fd, (struct sockaddr *) &sox->serverAddr, sockAddrSize) == ERROR) {
    VBX_print("SOX_configTCPServer: ERROR - bind() failed, errno = %d\n", errno);
    return(FALSE);
  }

  if (listen(sox->fd, maxConn) == ERROR) {
    VBX_print("SOX_configTCPServer: ERROR - bind() failed, errno = %d\n", errno);
    return(FALSE);
  }

  return(TRUE);
}

#if 0
SOX SOX_accept(SOX sox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Accept a connection on the given SOX, returning a new SOX acceptiing
 * a connection from a remote client.  This function is intended for
 * use in BLOCKING mode!
 * RETURN:  New SOX descriptor if successful, NULL otherwise
 *---------------------------------------------------------------------------*/
{
  Int sockAddrLen;
  SOX newSox = SOX_createTCP();
  
  if (newSox == NULL) {
    VBX_print("SOX_accept: ERROR - cannot create new SOX!\n");
  }

  if ((SOX_fd(newSox) = accept(SOX_fd(sox), (struct sockaddr *)&newSox->clientAddr,
                               &sockAddrLen)) == ERROR) {
    VBX_print("SOX_accept:  accept() failed, errno = %d\n", errno);
    SOX_destroy(newSox);
  }

  return(newSox);
}
#endif

Bool SOX_connect(SOX sox, Int msec)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Connect the given SOX, which must be configured for TCPClient.
 * RETURN:  TRUE if successful within timeout (msec), FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  struct linger ling = { 1, 0 };
  Int rval;

  VBX_DEBUG(VBX_print("SOX_connect starts, sox = %p\n", sox));
  if ((rval = connect(sox->fd, (struct sockaddr *)&sox->serverAddr,
                      sizeof(struct sockaddr_in))) == ERROR) {
    VBX_DEBUG(VBX_print("SOX_connect: connect() returns %d, errno = %d\n", rval, errno));
    if (errno == EINPROGRESS || errno == EALREADY) {
      if (rval = NBIO_select(sox->fd, NBIO_SELWRITE|NBIO_SELEXC, msec)) {
        VBX_DEBUG(VBX_print("SOX_connect: complete, NBIO_select = %d\n", rval));

        /* We have to detect the "connection refused" case here */
        return(SOX_isConnected(sox));
      } else {
        /* Select timed out and we're still not connected */
        return(FALSE);
      }
    } else {
      VBX_print("SOX_connect: connect failed, errno = %d\n", errno);
      return(FALSE);
    }
  }

#if 0
  /* Set the socket's linger option so it will go away upon close */
  if (rval = setsockopt(SOX_fd(sox), SOL_SOCKET, SO_LINGER, 
                        (char *)&ling, sizeof(ling))) {
    VBX_print("SOX_connect: Could not set linger option! errno = %d\n", errno);
  }
#endif

  return(TRUE);
}

Bool SOX_isConnected(SOX sox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Determine whether the given SOX is connected
 * RETURN: TRUE if connected, FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  struct sockaddr sockAddr;
  
  return SOX_getPeerName(sox, &sockAddr);
}

Bool SOX_getPeerName(SOX sox, struct sockaddr *sockAddrPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the peername of a socket that is already connected
 * RETURN: TRUE if successful, FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  Int rval, sockAddrLen;
    
  if ((rval = getpeername(sox->fd, sockAddrPtr, &sockAddrLen)) == ERROR) {
    VBX_DEBUG(VBX_print("SOX_isConnected: getpeername failed, errno = %d\n", errno));
    return(FALSE);
  }
  
  return(TRUE);
}
