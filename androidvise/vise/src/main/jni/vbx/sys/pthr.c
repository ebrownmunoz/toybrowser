/* pthr.c - provide pthreads-like environment on dec, sun, sco, vxworks
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *---------------------------------------------------------------------------*/

#ifndef VBX_SINGLE_THREAD

#include "pthr.h"
#include "vbx.h"
#include "siesta.h"

#if !defined(DEC) && !defined(WIN32) && !defined(VXWORKS)
#include <poll.h>
#endif

#if defined(LINUX) || defined(sol) || defined(AIX) || defined(ANDROID) || defined(__ANDROID__)
#include <unistd.h>
#include <sched.h>
#include <sys/timeb.h>
#endif

#if defined(NEWLIB) || defined(MINGW32)
#include <sys/timeb.h>
#endif

#define  report()   return((errno == 0) ? 0 : -1)

#if defined(REAL_PTHREADS)

#if !defined(GNUWINCE)
#if !defined(DEC)
Int pthread_delay_np(struct timespec *interval)
{
  Int msec;

  msec = interval->tv_nsec / 1000000;                               \
  msec += interval->tv_sec * 1000;
  if (msec <= 0)
    msec = 1;
/*  VBX_print("pthread_delay_np() doing Sleep(%d)\n", msec); */
#if defined(WIN32)
  Sleep(msec);
#else
  usleep(msec * 1000);
#endif
}

Int pthread_get_expiration_np(struct timespec *delta, struct timespec *abstime)
{
#if defined(DEC) || defined(ANDROID) || defined(__ANDROID__)
  clock_gettime(CLOCK_REALTIME, abstime);
#else
  struct timeb   clockTime;

  ftime(&clockTime);
  abstime->tv_sec = clockTime.time;                         /* seconds */
  abstime->tv_nsec = (long) (clockTime.millitm * 1000000);  /* nanoseconds */
#endif

  abstime->tv_sec += delta->tv_sec;
  abstime->tv_nsec += delta->tv_nsec;
  if (abstime->tv_nsec + delta->tv_nsec > 1000000000) {
    abstime->tv_sec++;
    abstime->tv_nsec -= 1000000000;
  }
}

#endif

Int
PTHR_getMaxPrio()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the maximum scheduling priority within the current scheduling
 * policy.  Solaris is currently in a sorry state;  the POSIX interface for
 * setting policies/priorities for threads is provided but non-functional.
 *---------------------------------------------------------------------------*/
{
  struct sched_param sp;
  Int policy;
  Int rval = pthread_getschedparam(pthread_self(), &policy, &sp);

#if defined(sol)
  return SOL_PRI_MAX;
#else
  if (rval)
    WARN_BRA_ "pthread_getschedparam returns %d errno %d\n", rval, errno _KET;

#if defined(LINUX) || defined(AIX) || defined(MINGW32) || defined(ANDROID) || defined(__ANDROID__)
#if defined(CYGWIN)
  return sched_get_priority_max(SCHED_FIFO);
#else
  return PRI_FIFO_MAX;
#endif
#else
  switch (policy) {
  case SCHED_OTHER:
    return PRI_OTHER_MAX;
  case SCHED_FIFO:
    return PRI_FIFO_MAX;    
  case SCHED_RR:
    return PRI_RR_MAX;
  }
#endif
#endif
}

Int
PTHR_getMinPrio()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the minimum scheduling priority within the current scheduling
 * policy
 *---------------------------------------------------------------------------*/
{
  struct sched_param sp;
  Int policy;
  Int rval = pthread_getschedparam(pthread_self(), &policy, &sp);
      
#if defined(sol)
  return SOL_PRI_MIN;
#else
  if (rval)
    WARN_BRA_ "pthread_getschedparam returns %d errno %d\n", rval, errno _KET;

#if defined(LINUX) || defined(AIX) || defined(MINGW32) || defined(ANDROID) || defined(__ANDROID__)
#if defined(CYGWIN)
  return sched_get_priority_min(SCHED_FIFO);
#else
  return PRI_FIFO_MIN;
#endif
#else
  switch (policy) {
  case SCHED_OTHER:
    return PRI_OTHER_MIN;
  case SCHED_FIFO:
    return PRI_FIFO_MIN;    
  case SCHED_RR:
    return PRI_RR_MIN;
  }
#endif
#endif
}
  
#endif            /* !def GNUWINCE */ 
#endif            /* def REAL_PTHREADS */

#ifdef VXWORKS
/*---------------------------------------------------------------------------*
 * The following functions emulate pthreads routines as accurately as
 * possible by calling native VxWorks threads and synchronization primitives
 *---------------------------------------------------------------------------*/

#define PTHR_MAX_WAITERS 20

Int
pthread_cancel(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Request that the specified thread terminate execution
 *---------------------------------------------------------------------------*/
{
  return (taskDelete(thread) == OK) ? 0 : -1;
}

Int
pthread_cond_init(pthread_cond_t * cond , const pthread_cond_attr_t * attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a condition variable
 *---------------------------------------------------------------------------*/
{
  if (!cond)
    return PTHR_FAILURE;

  cond->defunct = TRUE;  
  if ((cond->waitersMsgQ = msgQCreate(PTHR_MAX_WAITERS, sizeof(Int),
                                     MSG_Q_FIFO)) == NULL)
    return PTHR_FAILURE;
  cond->defunct = FALSE;
  return PTHR_SUCCESS;
}

Int
pthread_cond_destroy(pthread_cond_t * cond)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy a condition variable
 *---------------------------------------------------------------------------*/
{
  Int tid;

  if (!cond)
    return PTHR_FAILURE;

  cond->defunct = TRUE;
  while (msgQNumMsgs(cond->waitersMsgQ) > 0) {
    if (msgQReceive(cond->waitersMsgQ, (char *)&tid, sizeof(Int),
                    WAIT_FOREVER) == ERROR) {
      VBX_print("ERROR: pthread_cond_signal can't read waiter tid\n");
      return PTHR_FAILURE;
    }
    if (taskResume(tid) == ERROR) {
      VBX_print("ERROR: pthread_cond_signal can't restart task %d\n", tid);
      return PTHR_FAILURE;
    }
  }
  
  if (msgQDelete(cond->waitersMsgQ) == ERROR) {
    VBX_print("ERROR: pthread_cond_destroy can't delete msgQ!\n");
    return PTHR_FAILURE;
  }

  return PTHR_SUCCESS;
}

Int
pthread_cond_wait(pthread_cond_t * cond , pthread_mutex_t * mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Wait on a condition variable
 *---------------------------------------------------------------------------*/
{
  Int tid, status;

  if (!cond || !mutex || cond->defunct)
    return PTHR_FAILURE;

  tid = taskIdSelf();  

  if (msgQSend(cond->waitersMsgQ, (char *) &tid, sizeof(Int), 
               WAIT_FOREVER, MSG_PRI_NORMAL) == ERROR) {
    VBX_print("ERROR: pthread_cond_wait can't place taskId on waiters msgQ\n");
    return PTHR_FAILURE;
  }

  if (pthread_mutex_unlock(mutex) != PTHR_SUCCESS) {
    VBX_print("ERROR: pthread_cond_wait can't unlock mutex %x\n", mutex);
    return PTHR_FAILURE;
  }

  if ((status = taskSuspend(tid)) != OK) {
    VBX_print("ERROR: pthread_cond_wait can't suspend task 5d\n", tid);
    return PTHR_FAILURE;
  }

  if (pthread_mutex_lock(mutex) != PTHR_SUCCESS) {
    VBX_print("ERROR: pthread_cond_wait can't reacquire mutex %x\n", mutex);
    return PTHR_FAILURE;
  }

  return PTHR_SUCCESS;
}

Int
pthread_cond_timedwait(pthread_cond_t * cond , pthread_mutex_t * mutex, 
                       const struct timespec * abstime)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Cop-out version of a timed condition variable wait
 *---------------------------------------------------------------------------*/
{
  Int tid, status;

  if (!cond || !mutex || !abstime)
    return PTHR_FAILURE;

  if (pthread_mutex_unlock(mutex) != PTHR_SUCCESS) {
    VBX_print("ERROR: pthread_cond_wait can't unlock mutex %x\n", mutex);
    return PTHR_FAILURE;
  }

  pthread_delay_np(abstime);

  if (pthread_mutex_lock(mutex) != PTHR_SUCCESS) {
    VBX_print("ERROR: pthread_cond_wait can't reacquire mutex %x\n", mutex);
    return PTHR_FAILURE;
  }

  return PTHR_SUCCESS;
}

Int
pthread_cond_signal(pthread_cond_t * cond)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Signal a condition variable
 *---------------------------------------------------------------------------*/
{
  Int tid, status;

  if (!cond || cond->defunct)
    return PTHR_FAILURE;

  if (msgQNumMsgs(cond->waitersMsgQ) > 0) {
    if (msgQReceive(cond->waitersMsgQ, (char *)&tid, sizeof(Int),
                    WAIT_FOREVER) == ERROR) {
      VBX_print("ERROR: pthread_cond_signal can't read waiter tid\n");
      return PTHR_FAILURE;
    }
    if (taskResume(tid) == ERROR) {
      VBX_print("ERROR: pthread_cond_signal can't restart task %d\n", tid);
      return PTHR_FAILURE;
    }
  }

  return PTHR_SUCCESS;
}

Int
pthread_create(pthread_t * thread, pthread_attr_t * attr,
               pthread_startroutine_t fcn, void * arg)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  pthread_attr_t  dfltAttr;
  Int             tid;

  if (!attr) {
    pthread_attr_init(&dfltAttr);
    attr = &dfltAttr;
  }

  VBX_errorIf((*thread = 
    taskSpawn(NULL, attr->prio, attr->options, attr->stack, (FUNCPTR) fcn, 
              (int) arg, 0, 0, 0, 0, 0, 0, 0, 0, 0)) == ERROR);
  return (*thread != ERROR) ? PTHR_SUCCESS : PTHR_FAILURE;
}

Int
pthread_delay_np(struct timespec *wait)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  nanosleep(wait, NULL);
  return PTHR_SUCCESS;
}

Int
pthread_detach(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Delete a thread object and deallocate its structures.  For the first
 * try with VxWorks, this is identical to pthread_cancel.
 *---------------------------------------------------------------------------*/
{
  return (taskDelete(thread) == OK) ? 0 : -1;
}

void
pthread_exit(void *status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exit self and return the specified status.  VxWorks does not allow any
 * status information to be returned.
 *---------------------------------------------------------------------------*/
{
  taskDelete(taskIdSelf());
}

Int pthread_get_expiration_np(struct timespec *delta, struct timespec *abstime)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add a specified interval to the current absolute time
 *---------------------------------------------------------------------------*/
{
  clock_gettime(CLOCK_REALTIME, abstime);

  abstime->tv_sec += delta->tv_sec;
  abstime->tv_nsec += delta->tv_nsec;
  if (abstime->tv_nsec + delta->tv_nsec > 1000000000) {
    abstime->tv_sec++;
    abstime->tv_nsec -= 1000000000;
  }
}

Int
pthread_getscheduler(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return sched_getscheduler(thread);
}

Int
pthread_join(pthread_t thread, void **status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Wait for the termination of the specified thread, and report its exit
 * status.  This is the initial implementation in support of VxWorks, and
 * it is not acceptable for release.
 *---------------------------------------------------------------------------*/
{
  STATUS retval;
  struct timespec ts = { 0, 100000000 };

  /* Poll for the task until it no longer exists */
  while (TRUE) {
    retval = taskIdVerify(thread);
    if (retval != OK)
      break;

    /* Sleep for 100 milliseconds before retry */
    nanosleep(&ts, NULL);
  }

  return PTHR_SUCCESS;
}
  
Int
pthread_mutex_init(pthread_mutex_t *mutex, pthread_mutexattr_t *attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
 *mutex = semBCreate(0, SEM_FULL);
  return (*mutex != NULL) ? PTHR_SUCCESS : PTHR_FAILURE;
}

Int
pthread_mutex_destroy(pthread_mutex_t *mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  STATUS retval;
  retval = semDelete(*mutex);
  return (retval == OK) ? PTHR_SUCCESS : PTHR_FAILURE;
}

Int
pthread_mutex_lock(pthread_mutex_t *mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  STATUS retval;
  retval = semTake(*mutex, WAIT_FOREVER);
  return (retval == OK) ? PTHR_SUCCESS : PTHR_FAILURE;
}

Int
pthread_mutex_unlock(pthread_mutex_t *mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return (semGive(*mutex) == OK) ? PTHR_SUCCESS : PTHR_FAILURE;
}

Int
pthread_getprio(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  STATUS retval;
  Int    prio;

  retval = taskPriorityGet(thread, &prio);
  return (retval == OK) ? prio : PTHR_FAILURE;
}

Int
pthread_setprio(pthread_t thread, Int priority)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  STATUS retval;
  retval = taskPrioritySet(thread, priority);
  return (retval == OK) ? PTHR_SUCCESS : PTHR_FAILURE;
}

pthread_t pthread_self()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return taskIdSelf();
}

/* Lastly, the PTHR module is initially providing several
 * functions missing from VxWorks */
Char *
strdup(const Char *str)
{
  Char *retstr;
  Int   len;

  len = strlen(str) + 1;
  retstr = calloc(1, strlen(str) + 1);
  VBX_errorIf(retstr == NULL);
  strncpy(retstr, str, len);
  return retstr;
}

#endif              /* def WIN32 */

#if defined(WIN32) && !defined(_POSIX_THREADS) && !defined(GNUWINCE)
/*---------------------------------------------------------------------------*
 * The following functions emulate pthreads routines as accurately as
 * possible by calling native WIN32 services
 *---------------------------------------------------------------------------*/

Int
pthread_cancel(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Request that the specified thread terminate execution
 *---------------------------------------------------------------------------*/
{
  return(TerminateThread(thread, 0) ? 0 : -1);
}

Int
pthread_cond_init(pthread_cond_t * cond , const pthread_cond_attr_t * attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a condition variable
 *---------------------------------------------------------------------------*/
{
  cond->waitersCnt = 0;
  InitializeCriticalSection(&cond->waitersCntMutex);

  /* Create an auto-reset event */
  cond->events[SIGNAL] = CreateEventW(NULL,  /* no security */
                                      FALSE, /* auto-reset event */
                                      FALSE, /* non-signaled initially */
                                      NULL); /* unnamed */

  /* Create a manual-reset event */
  cond->events[BROADCAST] = CreateEventW(NULL,  /* no security */
                                         TRUE,  /* manual-reset */
                                         FALSE, /* non-signaled initially */
                                         NULL); /* unnamed */
  return PTHR_SUCCESS;
}

Int
pthread_cond_destroy(pthread_cond_t * cond)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy a condition variable
 *---------------------------------------------------------------------------*/
{
  if (cond != NULL) {
    CloseHandle(cond->events[BROADCAST]);
    CloseHandle(cond->events[SIGNAL]);
    DeleteCriticalSection(&cond->waitersCntMutex);
  }

  return PTHR_SUCCESS;
}

Int
pthread_cond_wait(pthread_cond_t * cond , pthread_mutex_t * mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Wait on a condition variable
 *---------------------------------------------------------------------------*/
{
  Int result;
  Int lastWaiter;

  /* Avoid race conditions */
  EnterCriticalSection(&cond->waitersCntMutex);
  cond->waitersCnt++;
  LeaveCriticalSection(&cond->waitersCntMutex);

  /* It's ok to release the <external_mutex> here since Win32
   * manual-reset events maintain state when used with
   * <SetEvent>.  This avoids the "lost wakeup" bug... */
  LeaveCriticalSection(mutex);

  /* Wait for either event to become signaled due to <pthread_cond_signal>
   * being called or <pthread_cond_broadcast> being called. */
  result = WaitForMultipleObjects(2, cond->events, FALSE, INFINITE);

  EnterCriticalSection(&cond->waitersCntMutex);
  cond->waitersCnt--;
  lastWaiter = (result == WAIT_OBJECT_0 + BROADCAST && cond->waitersCnt == 0);
  LeaveCriticalSection(&cond->waitersCntMutex);

  /* Some thread called <pthread_cond_broadcast> */
  if (lastWaiter) {
    /* We're the last waiter to be notified or to stop waiting, so
     * reset the manual event */
    ResetEvent(cond->events[BROADCAST]);
  }

  /* Reacquire the mutex */
  EnterCriticalSection(mutex);

  return PTHR_SUCCESS;
}

Int
pthread_cond_timedwait(pthread_cond_t * cond , pthread_mutex_t * mutex, 
                       const struct timespec * abstime)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Timed condition variable wait
 *---------------------------------------------------------------------------*/
{
  Int result;
  Int lastWaiter;
  Int msec, time;
  
  msec = abstime->tv_nsec / 1000000;
  msec += abstime->tv_sec * 1000;

  /* Avoid race conditions */
  EnterCriticalSection(&cond->waitersCntMutex);
  cond->waitersCnt++;
  LeaveCriticalSection(&cond->waitersCntMutex);

  /* It's ok to release the <external_mutex> here since Win32
   * manual-reset events maintain state when used with
   * <SetEvent>.  This avoids the "lost wakeup" bug... */
  LeaveCriticalSection(mutex);

  /* Wait for either event to become signaled due to <pthread_cond_signal>
   * being called or <pthread_cond_broadcast> being called. */
  if ((msec == 0) && (abstime->tv_nsec > 0)) {
    time = 1;
  } else if (msec == 0) {
    time = INFINITE;
  } else {
    time = msec;
  }

  result = WaitForMultipleObjects(2, cond->events, FALSE, time);
  if (result == WAIT_TIMEOUT)
    errno = ETIMEDOUT;

  EnterCriticalSection(&cond->waitersCntMutex);
  cond->waitersCnt--;
  lastWaiter = (result == WAIT_OBJECT_0 + BROADCAST && cond->waitersCnt == 0);
  LeaveCriticalSection(&cond->waitersCntMutex);

  /* Some thread called <pthread_cond_broadcast> */
  if (lastWaiter) {
    /* We're the last waiter to be notified or to stop waiting, so
     * reset the manual event */
    ResetEvent(cond->events[BROADCAST]);
  }

  /* Reacquire the mutex */
  EnterCriticalSection(mutex);

  return (result == WAIT_TIMEOUT) ? PTHR_FAILURE : PTHR_SUCCESS;
}

Int
pthread_cond_signal(pthread_cond_t * cond)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Signal a condition variable
 *---------------------------------------------------------------------------*/
{
  Int haveWaiters = 0;

  /* Avoid race conditions */
  EnterCriticalSection(&cond->waitersCntMutex);
  haveWaiters = cond->waitersCnt > 0;
  LeaveCriticalSection(&cond->waitersCntMutex);

  if (haveWaiters)
    SetEvent(cond->events[SIGNAL]);
  return PTHR_SUCCESS;
}

Int
pthread_create(pthread_t * thread, pthread_attr_t * attr,
               pthread_startroutine_t fcn, void * arg)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  pthread_attr_t  dfltAttr;
  Int            tid;

  if (!attr) {
    pthread_attr_init(&dfltAttr);
    attr = &dfltAttr;
  }

  VBX_errorIf((*thread = 
    CreateThread(NULL, attr->stack, (LPTHREAD_START_ROUTINE) fcn, arg, 0, (DWORD *)&tid)) == NULL);
  VBX_errorIf(!SetThreadPriority(*thread, attr->prio));
  return (*thread != NULL) ? PTHR_SUCCESS : PTHR_FAILURE;
}

Int
pthread_delay_np(struct timespec *wait)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  Int msec = wait->tv_nsec / 1000000;
  msec += wait->tv_sec * 1000;
  Sleep(msec);
  return PTHR_SUCCESS;
}

Int
pthread_get_expiration_np(struct timespec *delta, struct timespec *abstime)
{
  struct timeb   clockTime;

  ftime(&clockTime);
  abstime->tv_sec = clockTime.time;                         /* seconds */
  abstime->tv_nsec = (long) (clockTime.millitm * 1000000);  /* nanoseconds */

  abstime->tv_sec += delta->tv_sec;
  abstime->tv_nsec += delta->tv_nsec;
  if (abstime->tv_nsec + delta->tv_nsec > 1000000000) {
    abstime->tv_sec++;
    abstime->tv_nsec -= 1000000000;
  }
}

Int
pthread_detach(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Delete a thread object and deallocate its structures
 *---------------------------------------------------------------------------*/
{
  return(CloseHandle(thread) ? 0 : -1);
}

void
pthread_exit(void *status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exit self and return the specified status.  Note that the status returned
 * is actually an integer, not an address, unlike REAL pthread_exit().
 *---------------------------------------------------------------------------*/
{
  ExitThread((Int) status);
}

Int
pthread_getscheduler(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return(SCHED_FIFO);
}

Int
pthread_join(pthread_t thread, void **status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Wait for the termination of the specified thread, and report its exit
 * status.  Note that the status is actually an integer, unlike a REAL join.
 *---------------------------------------------------------------------------*/
{
  Int retval, exitCode;

  retval = WaitForSingleObject(thread, INFINITE);
  GetExitCodeThread(thread, &exitCode);

  if (status != NULL) {
    *status = (void *) exitCode;
  }
  return (retval == WAIT_OBJECT_0) ? PTHR_SUCCESS : PTHR_FAILURE;
}
  
Int
pthread_mutex_init(pthread_mutex_t *mutex, pthread_mutexattr_t *attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (mutex != NULL) {
    InitializeCriticalSection(mutex);
    return PTHR_SUCCESS;
  } else {
    return PTHR_FAILURE;
  }
}

Int
pthread_mutex_destroy(pthread_mutex_t *mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (mutex != NULL) {
    DeleteCriticalSection(mutex);
    return PTHR_SUCCESS;
  } else {
    return PTHR_FAILURE;
  }
}

Int
pthread_mutex_lock(pthread_mutex_t *mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (mutex != NULL) {
    EnterCriticalSection(mutex);
    return PTHR_SUCCESS;
  } else {
    return PTHR_FAILURE;
  }
}

Int
pthread_mutex_unlock(pthread_mutex_t *mutex)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (mutex != NULL) {
    LeaveCriticalSection(mutex);
    return PTHR_SUCCESS;
  } else {
    return PTHR_FAILURE;
  }
}

Int
pthread_getprio(pthread_t thread)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return GetThreadPriority(thread);
}

Int
pthread_setprio(pthread_t thread, Int priority)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return SetThreadPriority(thread, priority) ? PTHR_SUCCESS : PTHR_FAILURE;
}

#endif              /* def WIN32 */

#ifndef REAL_PTHREADS

/*---------------------------------------------------------------------------*
 * The following pthreads routines are provided only by REAL pthreads
 * implementations
 *---------------------------------------------------------------------------*/

Int
pthread_attr_getschedpolicy(pthread_attr_t *attr, Int *policy)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return(0);
}

Int
pthread_attr_setschedpolicy(pthread_attr_t *attr, Int policy)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return(0);
}

Int
pthread_attr_init(pthread_attr_t *attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (attr) {
    attr->prio = PRI_VBX_MID;
    attr->stack = DEFAULTSTACK;
    return(PTHR_SUCCESS);
  }
  return(PTHR_FAILURE);  
}

Int
pthread_attr_setinheritsched(pthread_attr_t *attr, Int inherit)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return(0);
}

Int
pthread_attr_setprio(pthread_attr_t *attr, Int prio)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (attr) {
    attr->prio = prio;
    return(PTHR_SUCCESS);
  }
  return(PTHR_FAILURE);
}

Int
pthread_attr_getprio(pthread_attr_t *attr, Int *prio)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  if (attr) {
   *prio =  attr->prio;
    return(PTHR_SUCCESS);
  }
  return(PTHR_FAILURE);
}

Int
pthread_attr_setschedparam(pthread_attr_t *attr, struct sched_param *schedParam)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  return(0);
}

Int
pthread_attr_setstacksize(pthread_attr_t *attr, Long stacksize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  attr->stack = stacksize;
  return(0);
}

#endif              /* ndef REAL_PTHREADS */

#endif              /* ndef VBX_SINGLE_THREAD */
