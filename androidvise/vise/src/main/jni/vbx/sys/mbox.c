/*  mbox.c:  prioritized mailbox cluster
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *
 * DESCRIPTION
 *
 * The MBOX_* methods implement a simple prioritized mailbox facility in
 * which a mailbox contains nPriorities queues of up to maxMessages each.
 * An MBOX_insertXX call tries to insert a message into the queue belonging
 * to the requested priority, and blocks or fails when that queue is full.
 * A MBOX_removeXX call removes the oldest message from the highest
 * priority non-empty queue, and blocks or fails only when all of the queues
 * are empty.
 *
 * The initial allocation for each priority queue is growQuantum.  Each
 * queue grows as required, in increments of growQuantum, until its size 
 * reaches maxMessages, at which point the inserts will block or fail,
 * depending on the call.
 *
 * The highest priority is always 0; the lowest, nPriorities - 1.  However,
 * all positive priorities are valid, since all values greater than the lowest
 * priority map into the lowest.
 *
 * Giving MBOX_new a NULL attributes argument causes it to construct an
 * old-fashioned single-queue (single-priority, non-prioritized) mailbox,
 * in which the initial size of the queue is the maximum size, so that the
 * queue never grows.
 *
 * Compiling with VBX_SINGLE_THREAD defined creates a simpler version without
 * synchronization, for use within a single thread.
 * 
 * Functionality:
 *
 *   If VBX_SINGLE_THREAD is NOT defined:
 *
 *   attributes = MBOX_createAttr(IMemory * memory, Uns growQuantum, Uns maxMessages, Uns nPriorities);
 *                create a mailbox attributes block
 *                MBOX_destroyAttr(MBOX_Att attributes);
 *                destroy a mailbox attributes block
 *      mailbox = MBOX_create(IMemory * memory, MBOX_Att attributes);
 *                create a new prioritized mailbox
 *                MBOX_destroy(MBOX mailbox);
 *                destroy a mailbox
 *      success = MBOX_insert(MBOX mailbox, OBJT message);
 *                try to insert a message in a mailbox, blocking if full
 *      success = MBOX_priorityInsert(MBOX mailbox, OBJT message, Uns priority);
 *                try to insert a message in a mailbox, blocking if full
 *      success = MBOX_insertNow(MBOX mailbox, OBJT message);
 *                try to insert a message into a mailbox immediately
 *      success = MBOX_priorityInsertNow(MBOX mailbox, OBJT message, Uns priority);
 *                try to insert a message into a mailbox immediately
 *      success = MBOX_insertWithTimeout(MBOX mailbox, OBJT message, TIMESPEC * timeout)
 *                try to insert a message into a mailbox within a given period
 *      success = MBOX_priorityInsertWithTimeout(MBOX mailbox, OBJT message, Uns priority, TIMESPEC * timeout)
 *                try to insert a message into a mailbox within a given period
 *      message = MBOX_remove(MBOX mailbox);
 *                try to remove a message from a mailbox, blocking if empty
 *      message = MBOX_removeNow(MBOX mailbox);
 *                try to remove a message from a mailbox immediately
 *      message = MBOX_removeWithTimeout(MBOX mailbox, TIMESPEC * timeout)
 *                try to remove a message from a mailbox within a given period
 *      numobj  = MBOX_numobj(MBOX mailbox);
 *                report how many messages there are in a mailbox
 *
 *   If VBX_SINGLE_THREAD IS defined:
 *
 *   attributes = MBOX_attributes(Uns growQuantum, Uns maxMessages, Uns nPriorities);
 *                create a mailbox attributes block
 *      mailbox = MBOX_new(MBOX_Att attributes);
 *                create a new prioritized mailbox
 *      success = MBOX_insertNow(MBOX mailbox, OBJT message);
 *                try to insert a message into a mailbox immediately
 *      success = MBOX_priorityInsertNow(MBOX mailbox, OBJT message, Uns priority);
 *                try to insert a message into a mailbox immediately
 *      message = MBOX_removeNow(MBOX mailbox);
 *                try to remove a message from a mailbox immediately
 *      numobj  = MBOX_numobj(MBOX mailbox);
 *                report how many messages there are in a mailbox
 *
 * Modification history:
 *
 * Fall 1993:  Wrapped pthread_cond_wait's in a while () to circumvent weird 
 *             DECthreads condition variable behavior (cav)
 * Jan  1994:  Rewritten for SGI (cav)
 * May  1997:  Rewritten to support priorities (based on PMBOX, created 1-23-97 by DCV)
 * Oct  1997:  Added VBX_SINGLE_THREAD mode (dcv)
 *
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "class.h"
#include "critsect.h"
#include "ifaces.h"
#include "mbox.h"
#include "vbx.h"
#include <assert.h>

#ifdef VBXDEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

 /* Default mailbox attributes */
#define MBOX_DFLT_SIZE  3
#define MBOX_DFLT_LVLS  1

/* Mailbox priority level */
typedef struct PMBLVL_s {
  CRITSECT_DECLARE(notFull)
  Uns         numMessages;
  Uns         firstMessage;
  Uns         lastMessage;
  Uns         currentSize;
  OBJT      * storage;
} PMBLVL_t, * PMBLVL;

 /* Mailbox type */
typedef struct MBOX_s {
  CRITSECT_DECLARE(notEmpty)
  IMemory   * memory;
  PMBLVL      levels;
  Uns         numMessages;
  Uns         growQuantum;
  Uns         maxMessages;
  Uns         nPriorities;
} MBOX_t;

  /* Macro to insert a message into a mailbox at a given priority level */
#define MBOX_PUTMSG(mailbox, level, message) {                                 \
  if (level->numMessages >= level->currentSize) {                               \
    OBJT  * storage = level->storage;                                           \
    Uns     oldSize = level->currentSize;                                       \
    assert(level->lastMessage == level->firstMessage);                          \
    if ((level->currentSize += mailbox->growQuantum) > mailbox->maxMessages)    \
      level->currentSize = mailbox->maxMessages;                                \
    level->storage = (OBJT *) mailbox->memory->Calloc(mailbox->memory, level->currentSize, sizeof(OBJT), FAST_MEMORY); \
    VBX_DEBUG(VBX_print("  Enlarging MBOX level @%p: old (%d @%p), new (%d @%p)\n", level, oldSize, storage, level->currentSize, level->storage)); \
    memcpy(level->storage, storage, level->lastMessage * sizeof(OBJT));         \
    memcpy(level->storage + (level->currentSize - oldSize + level->firstMessage), \
           storage + level->firstMessage, (oldSize - level->firstMessage) * sizeof(OBJT)); \
    mailbox->memory->Free(mailbox->memory, storage);                            \
    level->firstMessage = level->currentSize - oldSize + level->firstMessage;   \
  }                                                                             \
  level->storage[level->lastMessage++] = message;                               \
  if (level->lastMessage == level->currentSize) level->lastMessage = 0;         \
  level->numMessages++;                                                         \
  mailbox->numMessages++;                                                       \
  CRITSECT_SIGNAL(mailbox->notEmpty);                                           \
}

#if 0
    printf("  Enlarging level @%p: old (%d @%p), new (%d @%p)\n", level, oldSize, storage, level->currentSize, level->storage); \
    printf("    Copying level @%p: %d objects (%d bytes) from old @%p to new @%p\n", level, level->lastMessage, \
           level->lastMessage * sizeof(OBJT), storage, level->storage); \
    printf("    Copying level @%p: %d objects (%d bytes) from old @%p to new @%p\n", level, \
           oldSize - level->firstMessage, (oldSize - level->firstMessage) * sizeof(OBJT), storage + level->firstMessage, \
           level->storage + (level->currentSize - oldSize + level->firstMessage)); \
    printf("    Old first message = %d, new = %d; old last message = %d, new = %d\n", level->firstMessage, \
           level->currentSize - oldSize + level->firstMessage, level->lastMessage, level->lastMessage); \

#endif

  /* Macro to extract a message from a mailbox */
#define MBOX_GETMSG(mailbox, message) {                                        \
  Uns      priority;                                                            \
  PMBLVL   level;                                                               \
  message = NULL;                                                               \
  for (priority = 0; priority < mailbox->nPriorities; priority++) {             \
    level = mailbox->levels + priority;                                         \
    if (level->numMessages) {                                                   \
      message = level->storage[level->firstMessage++];                          \
      if (level->firstMessage == level->currentSize) level->firstMessage = 0;   \
      level->numMessages--;                                                     \
      mailbox->numMessages--;                                                   \
      CRITSECT_SIGNAL(level->notFull);                                          \
      break;                                                                    \
    }                                                                           \
  }                                                                             \
}


MBOX_Att
MBOX_createAttr(IMemory * memory, Uns growQuantum, Uns maxMessages, Uns nPriorities)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new mailbox attributes block
 * Usage:     attributes = MBOX_createAttr(maxMessages);
 *
 *            MBOX_Att  attributes;     new attributes block
 *            Uns       maxMessages;    number of messages mailbox holds
 *            Uns       nPriorities;    number of priority levels
 *
 * Return:    attributes block, or NULL if insufficient memory
 *---------------------------------------------------------------------------*/
{
  MBOX_Att        attributes;

  /* Try to allocate space for the attributes block */
  attributes = (MBOX_Att) memory->Calloc(memory, 1, sizeof(MBOX_Att_t), SLOW_MEMORY);

  if (attributes) {
    attributes->memory = memory;
    attributes->growQuantum = growQuantum;
    attributes->maxMessages = maxMessages;
    attributes->nPriorities = nPriorities;
  }

  return attributes;
}


void
MBOX_destroyAttr(MBOX_Att attributes)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy a mailbox attributes block
 * Usage:     MBOX_destroyAttr(attributes);
 *
 *            MBOX_Att  attributes;     attributes block
 *
 * Return:    nothing
 *---------------------------------------------------------------------------*/
{
  if (attributes && attributes->memory)
    attributes->memory->Free(attributes->memory, attributes);
}


MBOX
MBOX_create(IMemory * memory, MBOX_Att attributes)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new mailbox of the specified size
 * Usage:    mailbox = MBOX_new(MBOX_Att attributes);
 *           MBOX   mailbox;           new mailbox
 *
 * Return:   the new mailbox, or NULL if insufficient memory
 *---------------------------------------------------------------------------*/
{
  MBOX   mailbox = (MBOX) NULL;
  Uns    growQuantum = MBOX_DFLT_SIZE;
  Uns    maxMessages = MBOX_DFLT_SIZE;
  Uns    nPriorities = MBOX_DFLT_LVLS;
  Uns    priority;
  Bool   success;

  if (attributes) {
    growQuantum = attributes->growQuantum;
    maxMessages = attributes->maxMessages;
    nPriorities = attributes->nPriorities;
  }
  if (!growQuantum) growQuantum = MBOX_DFLT_SIZE;
  if (!maxMessages) maxMessages = MBOX_DFLT_SIZE;
  if (!nPriorities) nPriorities = MBOX_DFLT_LVLS;

  /* If we can ... */
  if (  /* Get a memory allocator */
      memory &&
        /* Allocate space for the mailbox's descriptor */
      ((mailbox = (MBOX) memory->Calloc(memory, 1, sizeof(MBOX_t), FAST_MEMORY))) &&
        /* Remember where the allocator is */
      (mailbox->memory = memory) &&
        /* Create a critical section for the mailbox */
      (CRITSECT_CREATE(mailbox->notEmpty, memory, NULL)) &&
        /* Allocate space for the mailbox's levels */
      ((mailbox->levels = (PMBLVL) memory->Calloc(memory, nPriorities, sizeof(PMBLVL_t), FAST_MEMORY)))) {
    /* Set the mailbox parameters */
    mailbox->growQuantum = growQuantum;
    mailbox->maxMessages = maxMessages;
    mailbox->nPriorities = nPriorities;
    for (priority = 0; priority < nPriorities; priority++) {
      PMBLVL  level = mailbox->levels + priority;
        /* Create a critical section for the priority level */
      if (!(success = (Bool) (CRITSECT_CREATE(level->notFull, memory, mailbox->notEmpty)))) break;
        /* Allocate space for the priority level's storage */
      if (!(success = (Bool) (level->storage = (OBJT *) memory->Calloc(memory, growQuantum, sizeof(OBJT), FAST_MEMORY)))) break;
      VBX_DEBUG(VBX_print("MBOX_create: allocated %d bytes @%p for priority %d of mailbox @%p\n",
                           growQuantum * sizeof(OBJT), level->storage, priority, mailbox));
      level->currentSize = growQuantum;
      level->firstMessage = level->lastMessage = level->numMessages = 0;
    }
  } else {
    success = FALSE;
  }

  if (!success) {
    MBOX_destroy(mailbox);
    mailbox = (MBOX) NULL;
  }

  return mailbox;
}


void
MBOX_destroy(MBOX mailbox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy a prioritized mailbox
 * Usage:    MBOX_destroy(MBOX mailbox);
 *           MBOX    mailbox;           the mailbox
 *
 * Return:   void
 *---------------------------------------------------------------------------*/
{
  Uns      priority;

  if (mailbox && mailbox->memory) {
    if (mailbox->levels) {
      for (priority = 0; priority < mailbox->nPriorities; priority++) {
        CRITSECT_DESTROY(mailbox->levels[priority].notFull);
        if (mailbox->levels[priority].storage) {
          mailbox->memory->Free(mailbox->memory, mailbox->levels[priority].storage);
          VBX_DEBUG(VBX_print("MBOX_destroy: freed %d bytes @%p for priority %d of mailbox @%p\n",
                              mailbox->levels[priority].currentSize * sizeof(OBJT), mailbox->levels[priority].storage, priority, mailbox));
        }
      }
      mailbox->memory->Free(mailbox->memory, mailbox->levels);
    }
    CRITSECT_DESTROY(mailbox->notEmpty);
    mailbox->memory->Free(mailbox->memory, mailbox);
  }
}


Bool
MBOX_insert(MBOX mailbox, OBJT message)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a message with the lowest priority into a prioritized mailbox,
 * waiting until space at that priority is available
 *
 * Usage:    success = MBOX_insert(mailbox, message);
 *
 *           Bool    success;           TRUE
 *           MBOX    mailbox;           mailbox to insert the message into
 *           OBJT    message;           message to insert into the mailbox
 *
 * Return:   TRUE if successful
 *---------------------------------------------------------------------------*/
{
  PMBLVL   level = mailbox->levels;

  CRITSECT_WAIT_ENTER(level->notFull, level->numMessages >= mailbox->maxMessages);
  if (level->numMessages < mailbox->maxMessages) {
    MBOX_PUTMSG(mailbox, level, message);
    CRITSECT_LEAVE(level->notFull);
    return(TRUE);
  } else {
    CRITSECT_LEAVE(level->notFull);
    return(FALSE);
  }
}


Bool
MBOX_priorityInsert(MBOX mailbox, OBJT message, Uns priority)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a message with the given priority into a prioritized mailbox,
 * waiting until space at that priority is available
 *
 * Usage:    success = MBOX_priorityInsert(mailbox, message, priority);
 *
 *           Bool    success;           TRUE
 *           MBOX    mailbox;           mailbox to insert the message into
 *           OBJT    message;           message to insert into the mailbox
 *           Uns     priority;          priority level
 *
 * Return:   TRUE if successful
 *---------------------------------------------------------------------------*/
{
  PMBLVL   level;

  if (priority >= mailbox->nPriorities) priority = mailbox->nPriorities - 1;
  level = mailbox->levels + priority;

  CRITSECT_WAIT_ENTER(level->notFull, level->numMessages >= mailbox->maxMessages);
  if (level->numMessages < mailbox->maxMessages) {
    MBOX_PUTMSG(mailbox, level, message);
    CRITSECT_LEAVE(level->notFull);
    return(TRUE);
  } else {
    CRITSECT_LEAVE(level->notFull);
    return(FALSE);
  }
}


Bool
MBOX_insertNow(MBOX mailbox, OBJT message)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a message into a mailbox at lowest priority if space is available
 * Usage:    success = MBOX_insertNow(mailbox, message);
 *
 *           Bool    success;           TRUE if successful, FALSE otherwise
 *           MBOX    mailbox;           mailbox to insert the message into
 *           OBJT    message;           message to insert into the mailbox
 *
 * Return:   TRUE if message inserted; FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  PMBLVL   level = mailbox->levels;
  Bool     success;

  CRITSECT_ENTER(level->notFull);
  if (level->numMessages < mailbox->maxMessages) {
    MBOX_PUTMSG(mailbox, level, message);
    success = TRUE;
  } else {
    success = FALSE;
  }
  CRITSECT_LEAVE(level->notFull);

  return success;
}


Bool
MBOX_priorityInsertNow(MBOX mailbox, OBJT message, Uns priority)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a message into a mailbox if space is available
 * Usage:    success = MBOX_priorityInsertNow(mailbox, message, priority);
 *
 *           Bool    success;           TRUE if successful, FALSE otherwise
 *           MBOX    mailbox;           mailbox to insert the message into
 *           OBJT    message;           message to insert into the mailbox
 *           Uns     priority;          priority level
 *
 * Return:   TRUE if message inserted; FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  PMBLVL   level;
  Bool     success;

  if (priority >= mailbox->nPriorities) priority = mailbox->nPriorities - 1;
  level = mailbox->levels + priority;

  CRITSECT_ENTER(level->notFull);
  if (level->numMessages < mailbox->maxMessages) {
    MBOX_PUTMSG(mailbox, level, message);
    success = TRUE;
  } else {
    success = FALSE;
  }
  CRITSECT_LEAVE(level->notFull);

  return success;
}


Bool
MBOX_insertWithTimeout(MBOX mailbox, OBJT message, TIMESPEC * timeout)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a message into a mailbox, timing out if this cannot be done within
 * the timeout period.
 * Usage:    success = MBOX_insertWithTimeout(mailbox, message, timeout);
 *
 *           Bool     success;          TRUE if successful, FALSE otherwise
 *           MBOX     mailbox;          mailbox to insert the message into
 *           OBJT     message;          message to insert into the mailbox
 *           TIMESPEC timeout;          maximum time to wait
 *
 * Return:   TRUE if message is inserted; FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  PMBLVL   level = mailbox->levels;
  Int      timedOut = 0;

  CRITSECT_TIMEDWAIT_ENTER(level->notFull, timeout, timedOut, level->numMessages >= mailbox->maxMessages);
  if (!timedOut) MBOX_PUTMSG(mailbox, level, message);
  CRITSECT_LEAVE(level->notFull);

  return (!timedOut);
}


Bool
MBOX_priorityInsertWithTimeout(MBOX mailbox, OBJT message, Uns priority, TIMESPEC * timeout)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a message into a mailbox, timing out if this cannot be done within
 * the timeout period.
 * Usage:    success = MBOX_priorityInsertWithTimeout(mailbox, message, priority, timeout);
 *
 *           Bool     success;          TRUE if successful, FALSE otherwise
 *           MBOX     mailbox;          mailbox to insert the message into
 *           OBJT     message;          message to insert into the mailbox
 *           Uns      priority;         priority level
 *           TIMESPEC timeout;          maximum time to wait
 *
 * Return:   TRUE if message is inserted; FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  PMBLVL   level;
  Int      timedOut = 0;

  if (priority >= mailbox->nPriorities) priority = mailbox->nPriorities - 1;
  level = mailbox->levels + priority;

  CRITSECT_TIMEDWAIT_ENTER(level->notFull, timeout, timedOut, level->numMessages >= mailbox->maxMessages);
  if (!timedOut) MBOX_PUTMSG(mailbox, level, message);
  CRITSECT_LEAVE(level->notFull);

  return (!timedOut);
}


OBJT
MBOX_remove(MBOX mailbox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Remove a message from a prioritized mailbox, waiting if none is available
 * Usage:    message = MBOX_remove(mailbox);
 *
 *           OBJT    message;           message removed from the mailbox
 *           MBOX    mailbox;           mailbox to remove the message from
 *
 * Return:   message OBJT if message is removed
 *---------------------------------------------------------------------------*/
{
  OBJT     message = (OBJT) NULL;

  CRITSECT_WAIT_ENTER(mailbox->notEmpty, mailbox->numMessages == 0);
  if (mailbox->numMessages > 0)
    MBOX_GETMSG(mailbox, message);
  CRITSECT_LEAVE(mailbox->notEmpty);

  return message;
}


OBJT
MBOX_removeNow(MBOX mailbox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Remove a message from a prioritized mailbox if there is one in it
 * Usage:    message = MBOX_remove(mailbox);
 *
 *           OBJT    message;           message removed from the mailbox
 *           MBOX    mailbox;           mailbox to remove the message from
 *
 * Return:   message OBJT if message is removed; NULL otherwise
 *---------------------------------------------------------------------------*/
{
  OBJT     message = NULL;

  CRITSECT_ENTER(mailbox->notEmpty);
  if (mailbox->numMessages) MBOX_GETMSG(mailbox, message);
  CRITSECT_LEAVE(mailbox->notEmpty);

  return message;
}


OBJT
MBOX_removeWithTimeout(MBOX mailbox, TIMESPEC * timeout)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Remove a message from a prioritized mailbox, timing out if none is
 * available within the timeount period
 * Usage:    message = MBOX_removeWithTimeout(mailbox, timeout);
 *
 *           OBJT     message;          message removed from the mailbox
 *           MBOX     mailbox;          mailbox to remove the message from
 *           TIMESPEC timeout;          maximum time to wait
 *
 * Return:   message OBJT if message is removed, NULL if timeout
 *---------------------------------------------------------------------------*/
{
  OBJT     message = NULL;
  Int      timedOut = 0;

  CRITSECT_TIMEDWAIT_ENTER(mailbox->notEmpty, timeout, timedOut, mailbox->numMessages == 0);
  if (!timedOut) MBOX_GETMSG(mailbox, message);
  CRITSECT_LEAVE(mailbox->notEmpty);

  return message;
}


Uns
MBOX_numobj(MBOX mailbox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Determine how many objects there are in the mailbox
 * Usage:    numobj = MBOX_numobj(mailbox);
 *
 *           Uns     numobj;            current number of messages in mailbox
 *           MBOX    mailbox;           mailbox to examine
 *
 * Return:   current number of messages in mailbox;
 *---------------------------------------------------------------------------*/
{
  Uns      number;

  CRITSECT_ENTER(mailbox->notEmpty);
  number = mailbox->numMessages;
  CRITSECT_LEAVE(mailbox->notEmpty);

  return number;
}

