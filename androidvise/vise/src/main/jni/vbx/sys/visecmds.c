/* visecmds.c - VISE Commands
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * The VISE commands
 * 
 * HISTORY
 * 13-Oct-97  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "vbxtypes.h"
#include "visecmds.h"

/* Command names */
static V_Char * VISECMD_text[NUMVISECOMMANDS] = {
  "QUERY_STATE",
  "LOAD_AND_GO",
  "RECOGNIZE",
  "TRAINWORD",
  "SEGMENTWORD",
  "LISTEN",
  "VISEINIT",
  "WARMSTART",
  "DLOADGRAMMAR",
  "DELGRAMMAR",
  "DLOADWORD",
  "ULOADWORD",
  "DELWORD",
  "DLOADTEMPLATE",
  "ULOADTEMPLATE",
  "DLOADWILDCARD",
  "DLOADJIN",
  "ULOADJIN",
  "DLOADAMP",
  "ULOADAMP",
  "BEEP",
  "SETPARAMETERS",
  "ECHO",
  "VISEIDLE",
  "GETPARAMETERS",
  "STARTLISTENING",
  "STOPLISTENING",
  "SETTIME",
  "GETTIME",
  "REPLACEWORD",
  "RECORDSAM",
  "PLAYDATA",
  "SAMTOCELP",
  "GETFACILITIES",
  "SETAUDIO",
  "STARTTRAININGWORD",
  "FINISHTRAININGWORD",
  "ACOUSTICFEATURES",
  "AUDIOFRAME",
  "AMPLITUDE",
  "VUMETERON",
  "VUMETEROFF",
  "GETSEQUENCENUMBER",
  "DESTROY",
  "EXCEPTION",
  "ACKNOWLEDGMENT",
  "ABORT",
  "NOCOMMAND",
  "SOURCEINIT",
  "DLOADARC",
  "DELARC",
  "ENROLLWORD",
  "SETTIMEOUT",
  "STARTCALIBRATION",
  "STOPCALIBRATION"
};


V_Char *
VISECMD_string(V_Cmd command)
/**************************************************************************
 *
 * Name:     VISECMD_string - return the string for a VISE command
 *
 ***************************************************************************/
{
  if (0 > command || command >= NUMVISECOMMANDS)
    command = _NOCOMMAND;

  return VISECMD_text[command];
}
