#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#if defined(GNUWINCE)
#include <sys/wcebase.h>
#include <sys/wcetrace.h>
#include <sys/wceerror.h>
#include <sys/spawn.h>
#endif

#define BUFSIZE  256

void
atexitfcn(void)
{
#if defined(GNUWINCE)
  WCETRACE(WCE_IO, "getpidbyname: atexitfcn invoked");
#endif
}

unsigned int
getpidbyname(char * name)
{
  unsigned int pid = (unsigned int)-1;

#if defined(GNUWINCE)

  char buf[BUFSIZE], fmtbuf[BUFSIZE];
  wchar_t namew[BUFSIZE];
  PROCESSENTRY32 pe;
  HANDLE hnd;

  atexit(atexitfcn);

  if (name == NULL || strlen(name) == 0) {
    errno = EINVAL;
    return pid;
  }

  mbstowcs(namew, name, strlen(name)+1);

  hnd = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS|TH32CS_SNAPNOHEAPS, 0);
  if (hnd == (HANDLE)-1) {
    WCETRACE(WCE_IO, "getpidbyname: ERROR - could not create Toolhelp32Snapshot");
    errno = ENOMEM;
    return pid;
  }

  pe.dwSize = sizeof(PROCESSENTRY32);

  if (Process32First(hnd, &pe)) {
    do {
      if (wcscmp(namew, pe.szExeFile) == 0) {
        pid = pe.th32ProcessID;
        WCETRACE(WCE_IO, "getpidbyname: found pid %x for \"%s\"", pid, name);
        break;
      }
    } while (Process32Next(hnd, &pe));
  }

  CloseToolhelp32Snapshot(hnd);

  if (pid == (unsigned int)-1)
    WCETRACE(WCE_IO, "getpidbyname: no process found for \"%s\"", name);
#endif

  return(pid);
}

int
killprocess(unsigned int pid)
{
  int winerr = 0;

#if defined(GNUWINCE)
  if (TerminateProcess(_getchildhnd(pid), 1) != TRUE) {
    if ((winerr = GetLastError()) == ERROR_INVALID_HANDLE) {
      VBX_print("killprocess: warning - no process with pid = %#x\n");
    } else {
      VBX_print("killprocess: error - windows error = %d\n", winerr);
    }
  }
#endif

  return winerr;
}

