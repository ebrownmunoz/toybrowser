/* critsect.c - critical section abstraction
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * DESCRIPTION
 * WIN32 lacks a synchronization mechanism that is directly equivalent
 * to a mutex/condition variable pair.  This module provides a synchronization
 * object that allows us to abstract out this idiosyncracy of WIN32.
 * CRITSECT should be used any time a mutex/cv pair is needed for
 * inter-thread synchronization; this will maintain portability between 
 * WIN32 and real programming environments.
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#ifndef VBX_SINGLE_THREAD

#include "pthr.h"
#include <stdlib.h>
#include "types.h"
#include "critsect.h"
#include "ifaces.h"
#include "vbx.h"

/* A shared mutex (with reference count) */
typedef struct mutexShr_s {
  pthread_mutex_t  mutex;     /* MUST BE THE FIRST FIELD IN THIS STRUCTURE! */
  UInt             refCnt;
} mutexShr_t, * MUTEXSHR;

/* Macro to destroy a shared mutex if its decremented reference count is zero */

#define MUTEXSHR_DESTROY(MP, MEM)                                 \
  if (!(--(MP)->refCnt)) {                                        \
    VBX_errorIf(pthread_mutex_destroy((pthread_mutex_t *) MP));   \
    (MEM)->Free(MEM, MP);                                         \
  }

#if defined(REAL_PTHREADS) || defined(GNUWINCE) || defined(VXWORKS)

CRITSECT 
CRITSECT_create(IMemory * memory, CRITSECT partner)
{
  CRITSECT  critsect;
  MUTEXSHR  mutexShr;

  /* Allocate a new critsect */
  if (!memory || !(critsect = (CRITSECT) memory->Calloc(memory, 1, sizeof(CritSect_t), FAST_MEMORY)))
    return (CRITSECT) NULL;
  critsect->memory = memory;

  /* If there is a partner critsect, share its mutex */
  if (partner) {
    critsect->mutexPtr = partner->mutexPtr;

  /* Otherwise, create a new shared mutex */
  } else {
    /* Create it */
    if (!(mutexShr = (MUTEXSHR) memory->Calloc(memory, 1, sizeof(mutexShr_t), FAST_MEMORY))) {
      memory->Free(memory, critsect);
      return (CRITSECT) NULL;
    }
    /* Initialize it */
    if (pthread_mutex_init(&mutexShr->mutex, NULL)) {
      memory->Free(memory, critsect);
      memory->Free(memory, mutexShr);
      return (CRITSECT) NULL;
    }
    /* Have the new critsect share it */
    critsect->mutexPtr = &mutexShr->mutex;
  }

  /* Increment the reference count in the shared mutex */
  ((MUTEXSHR) critsect->mutexPtr)->refCnt++;

  /* Initialize the condition variable in the new critsect */
  if (pthread_cond_init(&critsect->cv, NULL)) {
    MUTEXSHR_DESTROY((MUTEXSHR) critsect->mutexPtr, memory);
    memory->Free(memory, critsect);
    critsect = (CRITSECT) NULL;
  }

  return critsect;
}

void
CRITSECT_destroy(CRITSECT critsect)
{
  VBX_errorIf(pthread_cond_destroy(&critsect->cv));
  MUTEXSHR_DESTROY((MUTEXSHR) critsect->mutexPtr, critsect->memory);
  critsect->memory->Free(critsect->memory, critsect);
}
#endif

#if defined(WIN32) && !defined(GNUWINCE) && !defined(REAL_PTHREADS)

CRITSECT 
CRITSECT_create(IMemory * memory, CRITSECT partner)
{
  CRITSECT  critsect;
  MUTEXSHR  mutexShr;

  /* Allocate a new critsect */
  if (!memory || !(critsect = (CRITSECT) memory->Calloc(memory, 1, sizeof(CritSect_t), FAST_MEMORY)))
    return (CRITSECT) NULL;
  critsect->memory = memory;

  /* If there is a partner critsect, share its mutex */
  if (partner) {
    critsect->mutexPtr = partner->mutexPtr;

  /* Otherwise, create a new shared mutex */
  } else {
    /* Create it */
    if (!(mutexShr = (MUTEXSHR) memory->Calloc(memory, 1, sizeof(mutexShr_t), FAST_MEMORY))) {
      memory->Free(memory, critsect);
      return (CRITSECT) NULL;
    }
    /* Initialize it */
    if (pthread_mutex_init(&mutexShr->mutex, NULL)) {
      memory->Free(memory, critsect);
      memory->Free(memory, mutexShr);
      return (CRITSECT) NULL;
    }
    /* Have the new critsect share it */
    critsect->mutexPtr = &mutexShr->mutex;
  }

  /* Increment the reference count in the shared mutex */
  ((MUTEXSHR) critsect->mutexPtr)->refCnt++;

  if (!(critsect->event = CreateEventW(NULL, TRUE, FALSE, NULL))) {
    MUTEXSHR_DESTROY((MUTEXSHR) critsect->mutexPtr, memory);
    memory->Free(memory, critsect);
    critsect = (CRITSECT) NULL;
  }

  return critsect;
}

void
CRITSECT_destroy(CRITSECT critsect)
{
  VBX_fatalIf(!CloseHandle(critsect->event));
  MUTEXSHR_DESTROY((MUTEXSHR) critsect->mutexPtr, critsect->memory);
  critsect->memory->Free(critsect->memory, critsect);
}

#endif /* ndef VBX_SINGLE_THREAD */

#endif
