/* class.c
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Routines for creating, annihilating and manipulating classes and objects
 * HISTORY
 * 07-Dec-93 Created by Dave Vetter (dvetter@rad.verbex.com) from earlier
 *        code, also by Dave Vetter, originally written for the TMS320C30 DSP
 * 28-Apr-95 Dave Vetter (dvetter@rad.verbex.com)
 *        Added arguments to CLASS_new() and CLASS_allocObj() to permit
 *        efficient creation of OBJTs with special allocation requirements.
 *        Added CLASS_free(), with similar de-allocation capabilities.
 *  4-May-95 Dave Vetter (dvetter@rad.verbex.com)
 *        Added OBJT_clone().
 * 22-May-95 Dave Vetter (dvetter@rad.verbex.com)
 *        Changed functionality of newFcn() and freeFcn() to permit OBJT_new()
 *        and OBJT_free() to operate efficiently on OBJTs with embedded
 *        allocations and OBJTs.  Added the environment Ptr.
 * 31-Oct-97 Dave Vetter
 *        Added VBX_SINGLE_THREAD mode and IMemory.  Changed CLASS_new and
 *        CLASS_free to CLASS_create and CLASS_destroy, respectively.
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"
#include <string.h>
#include "types.h"
#include "class.h"
#include "critsect.h"
#include "ifaces.h"
#include "vbx.h"

#define VBX_DEBUG(P)

static OBJT CLASS_allocObj(IMemory * memory, UInt numObjects, UInt sizeContents, void (* fcn)(OBJT obj, Ptr env), Ptr env);


static OBJT
CLASS_allocObj(IMemory * memory, UInt numObjects, UInt sizeContents, void (* fcn)(OBJT obj, Ptr env), Ptr env)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Allocate and link objects
 *---------------------------------------------------------------------------*/
{
  OBJT         firstObject, object;
  UInt         sizeObjects;

  /* Allocate space for the objects */
  sizeObjects = sizeof(OBJT_t) + sizeContents - sizeof(Int);

  /* Pad sizeObjects so that objects are 64-bit aligned on Alpha */
#ifdef ALIGN64
  if (sizeObjects & 0x7) sizeObjects = (sizeObjects & ~0x7) + 8;
#endif
  firstObject = object = (OBJT) memory->Calloc(memory, numObjects, sizeObjects, FAST_MEMORY);
  VBX_DEBUG(VBX_print("CLASS_allocObj: allocated %d OBJT of size %d @ %p\n", (Int) numObjects, (Int) sizeObjects, (Ptr) firstObject));

  /* Link the objects into a free list */
  while (--numObjects) {
    VBX_DEBUG(VBX_print("CLASS_allocObj: OBJT %d @ %p\n", (Int) numObjects, (Ptr) object));
    object->pointer.nextObject = (OBJT) ((Char *) object + sizeObjects);
    if (fcn) fcn(object, env);
    object = object->pointer.nextObject;
  }
  VBX_DEBUG(VBX_print("CLASS_allocObj: OBJT %d @ %p\n", (Int) numObjects, (Ptr) object));
  object->pointer.nextObject = NULL;
  if (fcn) fcn(object, env);

  return(firstObject);
}



CLASS
CLASS_create(IMemory * memory, UInt numObjects, UInt sizeContents, void (* fcn)(OBJT obj, Ptr env), Ptr env)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new class and set up a list of its initial free objects.  If fcn
 * is non-NULL, fcn(object, env) is called for each object in the new class.
 *---------------------------------------------------------------------------*/
{
  CLASS   clas;

  /* Allocate space for the class descriptor */
  if ((clas = (CLASS) memory->Calloc(memory, 1, sizeof(CLASS_t), FAST_MEMORY)) &&
      (clas->memory = memory) &&
      (MUTEX_INIT(&clas->mutex, NULL))) {
    /* Initialize the class */
    clas->freeObjects = clas->objects = CLASS_allocObj(memory, numObjects, sizeContents, fcn, env);
    clas->numObjects = numObjects;
    clas->objectSize = sizeContents;
    clas->newFcn = NULL;
    clas->freeFcn = NULL;
  } else {
    CLASS_destroy(clas, fcn, env);
    clas = (CLASS) NULL;
  }

  return(clas);
}


Bool
CLASS_destroy(CLASS clas, void (* fcn)(OBJT, Ptr env), Ptr env)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free a class, deallocating all of its memory.  If fcn is non-NULL,
 * apply it to each free object in the class.  Note that fcn MUST NOT
 * FREE THE OBJECTS THEMSELVES.  Its purpose is to facilitate freeing up
 * structures which are not in the objects themselves, but which the objects
 * point to.  Normally, these structures would have been created by a
 * non-NULL fcn argument to CLASS_new when the class was created.  The env
 * argument here, as in CLASS_new(), is an arbitrary environment argument
 * for fcn.
 *---------------------------------------------------------------------------*/
{
  OBJT  object;
  UInt  freeCount = 0;

  if (clas) {

    /* Count the free objects in the class */
    object = clas->freeObjects;
    while (object) {
      freeCount++;
      object = object->pointer.nextObject;
    }

    /* Return FALSE if not all of the objects in the class are free */
    if (freeCount != clas->numObjects)
      return(FALSE);

    /* Delete the mutex */
    if (!MUTEX_DESTROY(&clas->mutex))
      return(FALSE);

    /* If fcn is non-NULL, apply it to each object in the class */
    if (fcn) {
      object = clas->freeObjects;
      while (object) {
        fcn(object, env);
        object = object->pointer.nextObject;
      }
    }

    /* Free the objects */
    clas->memory->Free(clas->memory, clas->objects);

    /* Free the class descriptor */
    clas->memory->Free(clas->memory, clas);

  }

  return(TRUE);
}


UInt
CLASS_free(CLASS clas)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a new object of the given class
 *---------------------------------------------------------------------------*/
{
  OBJT  object;
  UInt  freeCount = 0;

  if (clas) {
    /* Count the free objects in the class */
    object = clas->freeObjects;
    while (object) {
      freeCount++;
      object = object->pointer.nextObject;
    }
  }

  return(freeCount);
}


OBJT
OBJT_new(CLASS clas)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a new object of the given class
 *---------------------------------------------------------------------------*/
{
  OBJT        object;
  
  MUTEX_LOCK(&clas->mutex);
  
  /* Get a free object */
  if (!clas->freeObjects)
    object = (OBJT) NULL;               /* no more objects */
  else {
    object = clas->freeObjects;        /* Get the first free object */
    clas->freeObjects = object->pointer.nextObject;
    object->pointer.clas = clas;
    object->refCount = 1;
  }

  /* If a special newFcn() exists, use it */
  if (clas->newFcn != NULL)
    clas->newFcn(object, clas->environment);
  
  MUTEX_UNLOCK(&clas->mutex);
  
  return(object);
}


OBJT
OBJT_clone(OBJT object)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new object with contents identical those of an old one
 *---------------------------------------------------------------------------*/
{
  CLASS clas;
  OBJT  clone;
  
  if (object) {
    clas = object->pointer.clas;
    clone = OBJT_new(clas);
    if (clone)
      memcpy(&clone->contents, &object->contents, clas->objectSize);
  } else {
    clone = NULL;
  }

  return(clone);
}


OBJT
OBJT_copy(OBJT object)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add a reference to an existing object
 *---------------------------------------------------------------------------*/
{
  CLASS clas;
  
  if (object) {
    clas = object->pointer.clas;
    
    MUTEX_LOCK(&clas->mutex);
    
    object->refCount++;
    
    MUTEX_UNLOCK(&clas->mutex);
  }
  return(object);
}


void
OBJT_free(OBJT object)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free up a copy of an object
 *---------------------------------------------------------------------------*/
{
  CLASS clas;
  
  if (object) {
    clas = object->pointer.clas;
    
    MUTEX_LOCK(&clas->mutex);
  
    /* If the object is referred to elsewhere, do not free it */
    if (!(--object->refCount)) {
      
      /* If a special freeFcn() exists for this class, use it */
      if (clas->freeFcn != NULL)
        clas->freeFcn(object, clas->environment);
        
      /* Return the object to the CLASS's freelist */
      object->pointer.nextObject = clas->freeObjects;
      clas->freeObjects = object;
    }
    
    MUTEX_UNLOCK(&clas->mutex);
  }
}
