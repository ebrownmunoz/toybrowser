/* file.c - OS-independent file and directory access module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * HISTORY
 *  9-Dec-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#if defined(VXWORKS)
#include "pthr.h"
#include "dirent.h"
#include "stat.h"
#include "fcntl.h"
#else
#include "pthr.h"
#include <fcntl.h>
#ifdef NEWLIB
#include "sys/wcefile.h"
#include "sys/wcememory.h"
#endif
#ifdef MINGW32
#include <windows.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#if !defined(sol) && !defined(WIN32) && !defined(CYGWIN)
#if defined(ANDROID)
#include <dirent.h>
#else
#include <sys/dir.h>
#endif
#else
#define _POSIX_PTHREAD_SEMANTICS
#endif
#ifndef WIN32
#include <sys/mman.h>
#include <fnmatch.h>
#endif
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#endif

#include <string.h>
#include "types.h"
#include "file.h"
#include "vbx.h"

#define BLKSIZ  16384

#ifdef DEBUG
#define VBX_DEBUG(P)     P
#else
#define VBX_DEBUG(P)
#endif

typedef struct filemap_s {
#ifdef WIN32
  HANDLE   file;
  HANDLE   mapping;
#else
  Int      file;
#endif
  Char   * map;
  Char   * fileName;
  UInt     length;
  Int      mode;
} filemap_t;

typedef struct dirent dirent_t, * DIRENT;
typedef DIR  * DIRSEARCH;
#define  DIRLIST_FILENAME_LENGTH(dep)  (strlen((dep)->d_name))
#define  DIRLIST_FILENAME(dep)         ((dep)->d_name)
#define  DIRLIST_FILENAME_FREE(name)

#define DIRLIST_ALLOC  20

typedef struct dirlist_s {
  DIRENT  entries;
  UInt    size;
} dirlist_t;

Char *
PATH_localize(Char * path)
/**------------------------------------------------------------*
 * Translate all path separators to PATH_Separator and
 * return the path. THIS METHOD MODIFIES path.
 *------------------------------------------------------------**/
{
  Char * character;

  if (path != NULL)
    for (character = path; *character != '\0'; character++)
      if (PATH_isSeparator(*character)) *character = PATH_Separator;

  return path;
}

Char *
PATH_generalize(Char * path)
/**------------------------------------------------------------*
 * Translate all path separators to PATH_NormalSeparator
 * and return the path. THIS METHOD MODIFIES path.
 *------------------------------------------------------------**/
{
  Char * character;

  if (path != NULL)
    for (character = path; *character != '\0'; character++)
      if (PATH_isSeparator(*character)) *character = PATH_NormalSeparator;

  return path;
}

Char *
PATH_catToString(Char * string, Char * root, Char * extension)
/**------------------------------------------------------------*
 * Concatenate the root and extension path components in the
 * string buffer and return the buffer. If neither component is
 * NULL or empty, the root does not end in a PATH_Separator
 * and the extension is not absolute, insert a
 * PATH_Separator between the root and extension.
 * THE STRING BUFFER MUST BE LONG ENOUGH TO HOLD THE RESULT.
 *------------------------------------------------------------**/
{
  Char * origin = string;

  if (root != NULL) {
    strcpy(string, root);
    /* INFO_BRA_ "PATH_catToString: %s -> %s, length = %d", root, origin, strlen(root) _KET; */
    string += strlen(root);
  }

  if (extension != NULL) {
    /* Insert a PATH_Separator if neither component is NULL and neither the root ends nor
       the extension begins with a PATH_Separator */
    if (string != origin && !PATH_isAbsolute(string - 1) && !PATH_isAbsolute(extension)) {
      sprintf(string, "%c%s", PATH_Separator, extension);
      /* INFO_BRA_ "PATH_catToString: inserted \'%c\'; string = %s", PATH_Separator, string _KET; */
    } else {
      strcpy(string, extension);
      /* INFO_BRA_ "PATH_catToString: no \'%c\' inserted; string = %s", PATH_Separator, string _KET; */
    }
  }

  /* INFO_BRA_ "PATH_catToString: %s + %s -> %s", root, extension, origin _KET; */

  return origin;
}

Bool
FILE_appendStream(Char * srcName, FILE * targetFP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write the contents of the file named by srcName into the target file
 * starting at the current position
 *---------------------------------------------------------------------------*/
{
  FILE  * srcFP;
  Ptr     buffer;
  UInt    nRead, nWritten;
  Bool    success = FALSE;

  if (targetFP != NULL) {
    if ((srcFP = fopen(srcName, "rb"))) {
      if (srcFP != NULL) {
        if (buffer = (Ptr) calloc(1, BLKSIZ)) {
          do {
            nRead = fread(buffer, 1, BLKSIZ, srcFP);
            if (nRead)
              nWritten = fwrite(buffer, 1, nRead, targetFP);
          } while (nRead > 0);
          success = TRUE;
          free(buffer);
        }
      }
      fclose(srcFP);
    }
  }
  return(success);
}

Bool
FILE_copy(Char * oldName, Char * newName, Bool replace)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Makes a copy of the file named oldName in a file named newName.  An
 * existing file named newName is overwritten only if the replace argument
 * is TRUE;  otherwise the function fails.
 *---------------------------------------------------------------------------*/
{
  FILE  * old, * new;
  Ptr     buffer;
  UInt    nRead, nWritten;
  Bool    success = FALSE;

#ifdef VXWORKS
  Int     fd;
  if (replace || ((fd = open(newName, O_RDONLY, 0444)) == ERROR)) {
#else
  if (replace || access(newName, F_OK)) {
#endif
    if ((old = fopen(oldName, "rb"))) {
      if ((new = fopen(newName, "wb"))) {
        if (buffer = (Ptr) calloc(1, BLKSIZ)) {
          success = TRUE;
          do {
            nRead = fread(buffer, 1, BLKSIZ, old);
            if (nRead > 0 && !(success = (nRead == fwrite(buffer, 1, nRead, new))))
              break;
          } while (nRead > 0);
          free(buffer);
        }
        fclose(new);
      }
      fclose(old);
    }
  } else {
#ifdef VXWORKS
    close(fd);
#endif
  }
  return(success);
}

Bool
FILE_delete(Char * fileName)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Deletes a closed file from a directory.
 *---------------------------------------------------------------------------*/
{
  return(!remove(fileName));
}

Bool
FILE_exists(Char * fileName)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reports whether or not a file exists
 *---------------------------------------------------------------------------*/
{
#if defined(VXWORKS)
  Int fd;
  fd = open(fileName, O_RDONLY, 0444);
  if (fd != ERROR) {
    close(fd);
    return TRUE;
  } else {
    return FALSE;
  }
#else
  return(!access(fileName, F_OK));
#endif
}

Bool
FILE_length(Char * fileName, UInt *length)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reports whether or not a file exists
 *---------------------------------------------------------------------------*/
{
  FILE *fp;
  int   rval;
  fp = VBX_fopen(fileName, "r");
  rval = fseek(fp, 0, SEEK_END);
  if (rval) {
   *length = 0;
    return(FALSE);
  }
 *length = (UInt) ftell(fp);
  return(TRUE);
}

Bool
FILE_rename(Char * oldName, Char * newName, Bool replace)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Renames the file named oldName to newName.  An existing file named newName
 * is deleted only if replace is TRUE;  otherwise the function fails.
 *---------------------------------------------------------------------------*/
{
#ifdef VXWORKS
  Int fd;
  if (replace || ((fd = open(newName, O_RDONLY, 0444)) == ERROR)) {
    return(!rename(oldName, newName));
  } else {
    close(fd);
    return(FALSE);
  }
#else
  if (replace || access(newName, F_OK)) {
#if defined(MINGW32)
    FILE_delete(newName);
#endif
    return(!rename(oldName, newName));
  } else {
    return(FALSE);
  }
#endif
}

Bool
FILE_write(const Char *fileName, const Char *mode, const Ptr data, UInt dataSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Robustly create a file at the specified path and write "data" as its
 * contents
 *---------------------------------------------------------------------------*/
{
  Bool status = FALSE;
  Int  rval;
  FILE *fp = NULL;
  UInt  totBytesWritten = 0, bytesWritten = 0;

  if (!fileName || !strlen(fileName)) {
    VBX_print("FILE_write: ERROR - %s fileName argument\n",
               fileName ? "zero length" : "NULL");
    return(FALSE);
  }

  if (!mode || !strlen(mode)) {
    VBX_print("FILE_write: ERROR - %s mode argument\n",
               mode ? "zero length" : "NULL");
    return(FALSE);
  }

  if (!data) {
    VBX_print("FILE_write: ERROR - data argument is NULL\n");
    return(FALSE);
  }

  fp = fopen(fileName, mode);
  if (!fp) {
    VBX_print("FILE_write: ERROR - fopen() failed, errno = %d\n", errno);
    perror("FILE_write: fopen() errno info - ");
    return(FALSE);
  }

  while (totBytesWritten < dataSize) {
    bytesWritten = fwrite(data + totBytesWritten, 1, 
                          dataSize - totBytesWritten, fp);
    if (bytesWritten == 0 && (dataSize - totBytesWritten > 0)) {
      VBX_print("FILE_write: ERROR - fwrite() failed, errno = %d\n", errno);
      perror("FILE_write: fwrite() errno info -");
      status = FALSE;
      break;
    }

    totBytesWritten += bytesWritten;
    if (totBytesWritten == dataSize)
      status = TRUE;
  }

  rval = fclose(fp);
  if (rval) {
    VBX_print("FILE_write: ERROR - fclose() failed, errno = %d\n", errno);
    perror("FILE_write: fclose() errno info -");
    return(FALSE);
  }

  return(status);
}

FILEMAP
FILEMAP_create(Char * fileName, Int access)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Open the file and map it into the process' address space
 *---------------------------------------------------------------------------*/
{
  FILEMAP  map = NULL;
  Int      rlen;
#ifdef WIN32
  UInt     fileAccess, mapAccess, viewAccess;
#if defined(UNDER_CE) || defined(NEWLIB)
  WCHAR   *fileNameW;
#endif
#else
  Int      fileAccess, mapAccess;
#endif

  if (fileName == NULL || strlen(fileName) == 0) {
    SEVERE_BRA_ "FILEMAP_create: ERROR - invalid/NULL fileName" _KET;
    return(NULL);
  }

  if (map = (FILEMAP) calloc(sizeof(filemap_t), 1)) {
    map->fileName = VBX_salloc(fileName);
    map->mode = access;
#ifdef WIN32
    if (access == FILEMAP_READWRITE) {
      fileAccess = GENERIC_READ | GENERIC_WRITE;
      mapAccess = PAGE_READWRITE;
      viewAccess = FILE_MAP_ALL_ACCESS;
    } else {
      fileAccess = GENERIC_READ;
      mapAccess = PAGE_READONLY;
      viewAccess = FILE_MAP_READ;
    }

#if defined(UNDER_CE) || defined(NEWLIB)
    fileNameW = (WCHAR *) calloc((strlen(fileName) + 1) * sizeof(WCHAR), 1);
    mbstowcs(fileNameW, fileName, strlen(fileName) + 1);
    map->file = CreateFileForMappingW(fileNameW, fileAccess, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    VBX_DEBUG(VBX_print("INFO: CreateFileForMapping returns %8.8x (%d)\n", map->file, map->file == INVALID_HANDLE_VALUE));
    VBX_free(fileNameW);
#else
    map->file = CreateFile(fileName, fileAccess, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
#endif
    if (map->file != INVALID_HANDLE_VALUE) {
      if ((map->length = GetFileSize(map->file, NULL)) > 0 &&
          (map->mapping = CreateFileMappingW(map->file, NULL, mapAccess, 0, 0, NULL))) {
        if (!(map->map = (Char *) MapViewOfFile(map->mapping, viewAccess, 0, 0, 0))) {
          SEVERE_BRA_ "FILEMAP_create: cannot map view of file \"%s\"", fileName _KET;
          CloseHandle(map->mapping);
          CloseHandle(map->file);
          VBX_free(map);
        }
      } else {
        CloseHandle(map->file);
#else
    if (access == FILEMAP_READWRITE) {
      fileAccess = O_RDWR;
    } else {
      fileAccess = O_RDONLY;
    }
    map->file = open(fileName, fileAccess, 0);
    if (map->file != -1) {
#if defined(VXWORKS)
      if ((map->length = (UInt) lseek(map->file, 0, SEEK_END)) == (UInt) -1 ||
          lseek(map->file, 0, SEEK_SET) == -1 ||
          (map->map = (Char *) calloc(1, map->length)) == NULL ||
          (rlen = read(map->file, map->map, map->length)) != map->length) {
#else
      if (access == FILEMAP_READWRITE)
        mapAccess = PROT_READ | PROT_WRITE;
      else
        mapAccess = PROT_READ;
      if ((map->length = (UInt) lseek(map->file, 0, SEEK_END)) == (UInt) -1 ||
#if defined(sol)
          (map->map = (Char *) mmap(NULL, map->length, mapAccess, MAP_SHARED, map->file, (off_t) 0)) == (Char *) -1) {
#elif defined(LINUX) || defined(ANDROID) || defined(__ANDROID__)
          (map->map = (Char *) mmap(NULL, map->length, mapAccess, MAP_FILE | MAP_SHARED, map->file, (off_t) 0)) == (Char *) -1) {
#else
          (map->map = (Char *) mmap(NULL, map->length, mapAccess, MAP_FILE | MAP_VARIABLE | MAP_SHARED, map->file, (off_t) 0)) == (Char *) -1) {
#endif
#endif
        close(map->file);
#endif
#ifdef WIN32
        VBX_DEBUG(VBX_print("file = %d length = %d mapping = %x map = %x rlen = %d\n", map->file, map->length, map->mapping, map->map, rlen));
#else
        VBX_DEBUG(VBX_print("length = %d map = %x\n", map->length, map->map));
#endif
        SEVERE_BRA_ "FILEMAP_create: cannot create map object for file \"%s\" (%d)", fileName, map->length _KET;
        VBX_free(map);
      }
    } else {
      SEVERE_BRA_ "FILEMAP_create: cannot open file \"%s\" for mapping", fileName _KET;
      VBX_free(map);
    }
  }

  return map;
}

Bool
FILEMAP_synchronize(FILEMAP map, Char * region, UInt length)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Post to the underlying file any changes in the given region of the map.
 * If region is NULL, the entire map is posted.
 *---------------------------------------------------------------------------*/
{
  Bool  success = FALSE;

  if (map) {
    if (!region) {
      region = map->map;
      length = map->length;
    }

    if (map->map <= region && (region + length) <= (map->map + map->length)) {
#ifdef WIN32
      if (FlushViewOfFile(region, length))
#else
#ifdef VXWORKS
      if (lseek(map->file, region - map->map, SEEK_SET) == 0 &&
          write(map->file, region, length) == length)
#else
      if (!msync(region, length, MS_SYNC))
#endif
#endif
        success = TRUE;
      else
        SEVERE_BRA_ "FILEMAP_synchronize: cannot synchronize region [%p, %p] of map of file \"%s\"",
                    region, region + length, map->fileName _KET;
    } else {
      SEVERE_BRA_ "FILEMAP_synchronize: region [%p, %p] not in map [%p, %p] of file \"%s\"",
                  region, region + length, map->map, map->map + map->length, map->fileName _KET;
    }
  }

  return success;
}

Bool
FILEMAP_annihilate(FILEMAP map)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate the map, freeing up the address space it occupies, posting all
 * changes to the underlying file, and closing the file.
 *---------------------------------------------------------------------------*/
{
  Bool  success = FALSE;
  Bool  retval;

  if (map) {
    VBX_free(map->fileName);
#ifdef WIN32
    if (!(retval = UnmapViewOfFile(map->map))) {
      VBX_DEBUG(VBX_print("FILEMAP_annihilate: UnmapViewOfFile(%x) failed\n"));
    }
    success = retval;
    if (!(retval = CloseHandle(map->mapping))) {
      VBX_DEBUG(VBX_print("FILEMAP_annihilate: CloseHandle for map->mapping (%x) failed\n"));
    }
    success &= retval;
#if !defined(UNDER_CE) && !defined(NEWLIB)
    /* NOTE: DO NOT call CloseHandle for map->file on CE.  Refer to M$ docs */
    if (!(retval = CloseHandle(map->file))) {
      VBX_DEBUG(VBX_print("FILEMAP_annihilate: CloseHandle for map->file (%x) failed\n"));
    }
    success &= retval;
#endif
#else
#ifdef VXWORKS
    if (lseek(map->file, 0, SEEK_SET) == 0 &&
        write(map->file, map->map, map->length) == map->length &&
        close(map->file) != ERROR) {
      if (map->map != NULL)
        VBX_free(map->map);
      success = TRUE;
    }
#else
    success = !msync(map->map, map->length, MS_SYNC) && !munmap(map->map, map->length) && !close(map->file);
#endif
#endif
    free(map);
  }

  return success;
}

Char *
FILEMAP_map(FILEMAP map)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a pointer to the file map
 *---------------------------------------------------------------------------*/
{
  if (map)
    return map->map;
  else
    return NULL;
}

UInt
FILEMAP_length(FILEMAP map)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the length of the file map
 *---------------------------------------------------------------------------*/
{
  if (map)
    return map->length;
  else
    return 0;
}

Char *
FILEMAP_name(FILEMAP map)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the name of the mapped file
 *---------------------------------------------------------------------------*/
{
  if (map)
    return map->fileName;
  else
    return NULL;
}

Int
FILEMAP_mode(FILEMAP map)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the access mode of the map
 *---------------------------------------------------------------------------*/
{
  if (map)
    return map->mode;
  else
    return FILEMAP_NOACCESS;
}

Bool
DIR_create(Char * dirName)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create the specified directory.  Returns TRUE if successful, 
 * FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  Int rval;

#if defined(WIN32) && !defined(NEWLIB)
  rval = _mkdir(dirName);
#elif defined(VXWORKS)
  rval = mkdir(dirName);
#else
  rval = mkdir(dirName, S_IRWXU | S_IRWXG | S_IXOTH);
#endif
  if (rval == 0 || rval == EEXIST) {
    return TRUE;
  } else {
    WARN_BRA_ "DIR_create: can't create \"%s\", %s", dirName, strerror(errno) _KET;
    return FALSE;
  }
}

Bool
DIR_makeAll(Char * path)
/**------------------------------------------------------------*
 * Create all the directories in the specified path
 * NOT YET IMPLEMENTED
 *------------------------------------------------------------**/
{
  return FALSE;
}

Bool
DIR_delete(Char * dirName)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Delete the specified directory and anything in it.  Returns TRUE if 
 * successful, FALSE otherwise.
 * RESTRICTION:  This routine will fail if the specified directory has subdirs
 *---------------------------------------------------------------------------*/
{
  Int     rval;
  UInt    dirSize, fileNameLen;
  DIRLIST dirList;
  Char   *fileName, *fileNameList = NULL;
  Char    filePath[256];
  
  if (dirList = DIRLIST_create(dirName, "*")) {
    
    /* Create a list of filenames */
    fileNameList = DIRLIST_fileNames(dirList, &dirSize);
    fileName = fileNameList;
    while (fileNameLen = strlen(fileName)) {
      sprintf(filePath, "%s/%s", dirName, fileName);
      
      if (!FILE_delete(filePath)) {
        VBX_free(fileNameList);
        DIRLIST_annihilate(dirList);
        return(FALSE);
      }
      fileName += fileNameLen + 1;
    }
    VBX_free(fileNameList);
    DIRLIST_annihilate(dirList);
  }

#if defined(WIN32) && !defined(NEWLIB)
  rval = _rmdir(dirName);
#else
  rval = rmdir(dirName);
#endif
  if (rval == 0) {
    return TRUE;
  } else {
    WARN_BRA_ "DIR_delete: can't delete \"%s\", %s", dirName, strerror(errno) _KET;
    return(FALSE);
  }
}

Bool
DIR_copy(Char * oldDir, Char * newDir)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Copies the contents of directory oldDir into new directory newDir.
 * Subdirectories of oldDir are IGNORED.  Absolute path for both directories
 * is REQUIRED.  Returns TRUE upon successfully copying the contents of oldDir,
 * FALSE otherwise.
 *---------------------------------------------------------------------------*/
{
  Int     rval;
  UInt    dirSize, fileNameLen;
  DIRLIST dirList;
  Char   *fileName, *fileNameList = NULL;
  Char    oldFilePath[256], newFilePath[256];
  
  if (dirList = DIRLIST_create(oldDir, "*")) {
    if (!DIR_create(newDir)) {
      WARN_BRA_ "DIR_copy: can't create new directory \"%s\"", newDir _KET;
      DIRLIST_annihilate(dirList);
      return(FALSE);
    }
    
    /* Create a list of filenames */
    fileNameList = DIRLIST_fileNames(dirList, &dirSize);
    fileName = fileNameList;
    while (fileNameLen = strlen(fileName)) {
      sprintf(oldFilePath, "%s/%s", oldDir, fileName);
      sprintf(newFilePath, "%s/%s", newDir, fileName);
      if (!FILE_copy(oldFilePath, newFilePath, TRUE)) {
        VBX_free(fileNameList);
        DIRLIST_annihilate(dirList);
        return(FALSE);
      }
      fileName += fileNameLen + 1;
    }
    VBX_free(fileNameList);
    DIRLIST_annihilate(dirList);
  }
  return TRUE;
}

DIRLIST
DIRLIST_create(Char * dirName, Char * nameTemplate)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a DIRLIST containing an array of directory entry structures for the
 * files in the directory named dirName whose names satisfy the nameTemplate.
 * Returns NULL if an error occurs, or an empty DIRLIST if there are no such
 * files.
 *---------------------------------------------------------------------------*/
{
  DIRLIST     dirList;
  DIRSEARCH   search;
  DIRENT      newArray, entryPtr = NULL;
  Int         listAlloc = 0;   /* The number of entries allocated */
  Int         entryCount = 0;  /* Count of entries found, index of next entry to find */
  Bool        success = FALSE;

  if (dirList = (DIRLIST) malloc(sizeof(dirlist_t))) {
    listAlloc = DIRLIST_ALLOC;
    if ((dirList->entries = (DIRENT) malloc((size_t) (listAlloc * sizeof(dirent_t))))) {
#if defined(VXWORKS) || defined(NEWLIB) || defined (MINGW32)
      if (success = ((search = opendir(dirName)) != (DIRSEARCH) -1) && (entryPtr = readdir(search))) {
#else
      if ((success = ((search = opendir(dirName)) != (DIRSEARCH) -1) && !readdir_r(search, dirList->entries + entryCount, &entryPtr) && entryPtr)) {
#endif

        do {
#if defined(VXWORKS) || defined(WIN32)
          if (TRUE) {
#else
          if (!fnmatch(nameTemplate, (dirList->entries + entryCount)->d_name, FNM_PATHNAME | FNM_PERIOD)) {
#endif
            /* We've found one that matches */
            if (++entryCount >= listAlloc) {
              listAlloc += DIRLIST_ALLOC;
              if (success = (Bool) (newArray = (DIRENT) realloc(dirList->entries, (size_t) (listAlloc * sizeof(dirent_t)))))
                dirList->entries = newArray;
              else
                entryCount--;
            }
          }
#if defined(VXWORKS) || defined(NEWLIB) || defined(MINGW32)
        } while (success && (entryPtr = readdir(search)));
#else
        } while (success && !readdir_r(search, dirList->entries + entryCount, &entryPtr) && entryPtr);
#endif
        closedir(search);
      }
    }

    if (success) {
      /* Shrink the array of entries to its final size */
      if (entryCount) {
        if (newArray = (DIRENT) realloc(dirList->entries, (size_t) (entryCount * sizeof(dirent_t))))
          dirList->entries = newArray;
      } else {
        VBX_free(dirList->entries);
        dirList->entries = NULL;
      }
      dirList->size = (UInt) (entryCount);
    } else {
      /* Annihilate whatever has been created */
      DIRLIST_annihilate(dirList);
      dirList = NULL;
    }
  } else {
    VBX_free(dirList);
  }

  return(dirList);
}


void
DIRLIST_annihilate(DIRLIST dirList)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a list of directory entries
 *---------------------------------------------------------------------------*/
{
  if (dirList) {
    VBX_free(dirList->entries);
    free(dirList);
  }
}


UInt
DIRLIST_numEntries(DIRLIST dirList)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the number of entries in dirList
 *---------------------------------------------------------------------------*/
{
  if (dirList)
    return(dirList->size);
  else
    return(0);
}


Char *
DIRLIST_fileNames(DIRLIST dirList, UInt * sizePtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a NULL-terminated string containing a NULL-terminated file name
 * string for each directory entry in DIRLIST.  THE CALLER IS RESPONSIBLE FOR
 * DEALLOCATING THE RETURNED STRING OF STRINGS.
 *---------------------------------------------------------------------------*/
{
  UInt    index;
  Char  * namePtr, * nameList = NULL, * entryStr = NULL;

  *sizePtr = 0;

  if (dirList) {

    /* First compute the length of the list string */
    for (index = 0; index < dirList->size; index++) {
      *sizePtr += DIRLIST_FILENAME_LENGTH(dirList->entries + index) + 1;
    }
    /* Allow for the final NULL string */
    (*sizePtr)++;

    /* Allocate the list string */
    if (nameList = (Char *) malloc(*sizePtr)) {

      /* Then assemble it */
      for (namePtr = nameList, index = 0;
           index < dirList->size;
           namePtr += DIRLIST_FILENAME_LENGTH(dirList->entries + index) + 1, index++) {
        entryStr = DIRLIST_FILENAME(dirList->entries + index);
        strcpy(namePtr, entryStr);
        DIRLIST_FILENAME_FREE(entryStr);
      }
      /* Terminate it with a NULL string */
      *namePtr = '\0';
    }
  }

  return nameList;
}

