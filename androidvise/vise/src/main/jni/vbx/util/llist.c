/* llist.c - linked list facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * The LLIST_* methods implement a simple doubly-linked list facility.
 *
 * HISTORY
 * 21-May-97  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "vbx.h"
#include "llist.h"

/* A linked list */
typedef struct llist_s {
  Ptr    contents;
  LLIST  next;
  LLIST  previous;
} llist_t;


LLIST
LLIST_create(Ptr contents)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates and returns a single-element list containing contents.
 *---------------------------------------------------------------------------*/
{
  LLIST  llist = (LLIST) calloc(1, sizeof(llist_t));

  if (llist) llist->contents = contents;

  return(llist);
}


void
LLIST_destroy(LLIST llist, void (* fcn)(Ptr contents, Ptr env), Ptr env)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilates the list containing llist, de-allocating its memory.  If fcn
 * is not NULL, the method applies it to the contents of each element before
 * annihilating the element.  Typically, fcn would destroy the contents.  The
 * env argument is an arbitrary environment pointer for fcn.  If fcn is NULL,
 * nothing is done to the contents; in particular, they are NOT destroyed.
 *---------------------------------------------------------------------------*/
{
  LLIST  next;

  llist = LLIST_first(llist);

  while (llist) {
    if (fcn) fcn(llist->contents, env);
    next = llist->next;
    free(llist);
    llist = next;
  }
}


LLIST
LLIST_append(LLIST llist, Ptr contents)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates a list element containing contents, links it into llist as
 * llist's successor, and returns it.  If llist is NULL, this method is
 * equivalent to LLIST_create(contents).
 *---------------------------------------------------------------------------*/
{
  LLIST  next = (LLIST) calloc(1, sizeof(llist_t));

  if (next) {
    next->contents = contents;
    next->previous = llist;
    if (llist) {
      next->next = llist->next;
      llist->next = next;
    }
  }

  return(next);
}


LLIST
LLIST_prepend(LLIST llist, Ptr contents)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates a list element containing contents, links it into llist as
 * llist's predecessor, and returns it.  If llist is NULL, this method is
 * equivalent to LLIST_create(contents).
 *---------------------------------------------------------------------------*/
{
  LLIST  previous = (LLIST) calloc(1, sizeof(llist_t));

  if (previous) {
    previous->contents = contents;
    previous->next = llist;
    if (llist) {
      previous->previous = llist->previous;
      llist->previous = previous;
    }
  }

  return(previous);
}


LLIST
LLIST_first(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the first element of the list, the only one with no predecessor
 *---------------------------------------------------------------------------*/
{
  if (llist)
    while (llist->previous)
      llist = llist->previous;

  return(llist);
}


LLIST
LLIST_last(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the last element of the list, the only one with no successor
 *---------------------------------------------------------------------------*/
{
  if (llist)
    while (llist->next)
      llist = llist->next;

  return(llist);
}


LLIST
LLIST_next(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the next element of the list, if any
 *---------------------------------------------------------------------------*/
{
  return(llist ? llist->next : NULL);
}


LLIST
LLIST_previous(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the previous element of the list, if any
 *---------------------------------------------------------------------------*/
{
  return(llist ? llist->previous : NULL);
}


LLIST
LLIST_remove(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Removes the element at llist from the list, returning the next element.
 * This method DOES NOT DESTROY the element at llist; it just extracts it
 * from the list as a single-element list in its own right.  To destroy the
 * extracted element, call LLIST_destroy() on it.
 *---------------------------------------------------------------------------*/
{
  LLIST  next;

  if (llist) {
    if (llist->previous)
      llist->previous->next = llist->next;
    if (llist->next)
      llist->next->previous = llist->previous;
    next = llist->next;
    llist->previous = NULL;
    llist->next = NULL;
  }

  return(next);
}


Ptr
LLIST_contents(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the contents of the element at llist, or NULL if none.
 *---------------------------------------------------------------------------*/
{
  return(llist ? llist->contents : NULL);
}


Ptr
LLIST_replaceContents(LLIST llist, Ptr contents)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Replaces the contents of llist with contents, returning the old contents
 *---------------------------------------------------------------------------*/
{
  Ptr  oldContents = NULL;

  if (llist) {
    oldContents = llist->contents;
    llist->contents = contents;
  }

  return(oldContents);
}


LLIST
LLIST_find(LLIST llist, Ptr contents)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the first element of llist with the given contents, or NULL if
 * there is no such element
 *---------------------------------------------------------------------------*/
{
  llist = LLIST_first(llist);
  while (llist && llist->contents != contents)
    llist = llist->next;

  return(llist);
}


UInt
LLIST_length(LLIST llist)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the number of elements in llist
 *---------------------------------------------------------------------------*/
{
  UInt  count = 0;

  llist = LLIST_first(llist);
  while (llist) {
    llist = llist->next;
    count++;
  }

  return(count);
}
