#include "pthr.h"

/* #define NDEBUG */

#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "vbx.h"
#include "urlcodec.h"


 /* Return true iff the given plaintext char is "safe": [A-Za-z0-9\-_\.\*]. Such characters are their own url encoding. */
#define isSafe(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z') || ((c) >= '0' && (c) <= '9') || (c) == '-' || (c) == '_' || (c) == '.' || (c) == '*')

/* Return true iff the given encoded char is the same as its plaintext. */
#define isPlain(c) (((c) != '%') && ((c) != '+'))

Char *
URL_encode(const Char * plainText, Char * urlEncoding, Int maxEncodedLength)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * URL-encode the given plainText string. Returns the encoding, or NULL if
 * the encoding's length exceeds the given maximum
 *---------------------------------------------------------------------------*/
{
  Int  txtLength = strlen(plainText);
  Int  txtStart = 0;
  Int  txtIndex = 0;
  Int  encIndex = 0;
  Char character;
  while (TRUE) {
     /* Append the longest immediately subsequent substring containing only "safe" characters */
    txtStart = txtIndex;
    while (txtIndex < txtLength && isSafe(plainText[txtIndex])) txtIndex++;
    strncpy(&urlEncoding[encIndex], &plainText[txtStart], txtIndex - txtStart);
    encIndex += txtIndex - txtStart;
    if (encIndex + 1 >= maxEncodedLength) {
      urlEncoding[maxEncodedLength - 1] = '\0';
      return (txtIndex >= txtLength) ? urlEncoding : NULL;
    } else if (txtIndex >= txtLength) {
      urlEncoding[encIndex] = '\0';
      /* VBX_print("URL_encode: %s ==> %s\n", plainText, urlEncoding); */
      return urlEncoding;
    } else if (plainText[txtIndex] == ' ') {
       /* Append a plus sign in place of the space character */
      urlEncoding[encIndex++] = '+';
      txtIndex++;
    } else {
      while (txtIndex < txtLength && (character = plainText[txtIndex]) != ' ' && !isSafe(character)) {
         /* Append the byte encoded as a "%xy" String */
        if (encIndex + 3 >= maxEncodedLength || sprintf(&urlEncoding[encIndex], "%%%2.2x", (((Int) character) & 0xff)) != 3) {
          urlEncoding[encIndex] = '\0';
          return NULL;
        }
        encIndex += 3;
        txtIndex++;
      }
    }
  }
}

Char *
URL_decode(Char * plainText, const Char * urlEncoding, Int maxDecodedLength)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Decode the given URL-encoded string to a plainText string. Returns the
 * plaintext, or NULL if there is a decoding problem.
 *---------------------------------------------------------------------------*/
{
  Int encLength = strlen(urlEncoding);
  Int encStart = 0;
  Int encIndex = 0;
  Int txtIndex = 0;
  Int character;
  while (TRUE) {
     /* Get and append a substring containing only plainText characters */
    encStart = encIndex;
    while (encIndex < encLength && txtIndex < maxDecodedLength && isPlain(urlEncoding[encIndex])) encIndex++;
    strncpy(&plainText[txtIndex], &urlEncoding[encStart], encIndex - encStart);
    txtIndex += encIndex - encStart;
    if (txtIndex + 1 >= maxDecodedLength) {
      plainText[maxDecodedLength - 1] =  '\0';
      return (encIndex >= encLength) ? plainText : NULL;
    } else if (encIndex >= encLength) {
      plainText[txtIndex] = '\0';
      /* VBX_print("URL_decode: %s <== %s\n", plainText, urlEncoding); */
      return plainText;
    } else if (urlEncoding[encIndex] == '+') {
       /* Append a space character in place of the plus sign */
      plainText[txtIndex++] = ' ';
      encIndex++;
    } else {
      while (encIndex < encLength && urlEncoding[encIndex] == '%') {
         /* Decode the "%xy" string as a byte */
        if (txtIndex + 1 >= maxDecodedLength || sscanf(&urlEncoding[encIndex], "%%%2x", &character) != 1) {
          plainText[txtIndex] = '\0';
          return NULL;
        }
        plainText[txtIndex++] = (Char) (character & 0xff);
        encIndex += 3;
      }
    }
  }
}

