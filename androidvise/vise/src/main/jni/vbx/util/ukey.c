/* ukey.c - unique key facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * The UKEY_* methods implement a simple registration scheme to permit (say)
 * multiple objects to share a common object which any of them may change.
 * The sharing objects acquire unique keys (UKEYs) from a pool (UKEYPOOL) of
 * such by calling UKEY_reserve(), and can then they register with the shared
 * object by calling UKEY_register() with that key.  Objects can "unregister"
 * all the others by calling UKEYREG_reset(), and any object which has
 * acquired a UKEY can determine whether it is still registered by calling
 * UKEY_registered().  When a sharing object no longer needs a key, it can
 * return it to the pool by calling UKEY_release(), BUT MUST ALSO MAKE SURE
 * TO UNREGISTER IT BY CALLING UKEY_unregister() ON EVERY REGISTRY IT HAS
 * REGISTERED IT WITH.
 *
 * HISTORY
 * 12-Jan-97  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "types.h"
#include "vbx.h"
#include "primes.h"
#include "ukey.h"

/* A Unique Key */
typedef struct ukey_s {
  union {
    UKEY     nextKey;
    UKEYPOOL keyPool;
  }        link;
  UInt     value;
} ukey_t;

/* A Pool of Unique Keys */
typedef struct ukeypool_s {
  UKEY     nextKey;
  UKEY     keys;
  UInt     numKeysLeft;
} ukeypool_t;

/* A Key Registry */
typedef struct ukeyreg_s {
  Quad     registry;
} ukeyreg_t;


UKEYPOOL
UKEYPOOL_create(UInt initialSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns a new UKEY pool containing approximately initialSize UKEYs,
 * or NULL if it cannot allocate memory for the pool.
 *---------------------------------------------------------------------------*/
{
  UKEYPOOL   thisPool;
  UInt       numPrimes;
  UInt     * prime, * primes;
  UKEY       key;

  if ((thisPool = (UKEYPOOL) calloc(1, sizeof(ukeypool_t)))) {
    if ((prime = primes = PRIMES_firstN(initialSize, &numPrimes)) &&
        (key = thisPool->nextKey = thisPool->keys = (UKEY) calloc(--numPrimes, sizeof(ukey_t)))) {
      thisPool->numKeysLeft = numPrimes;
      /* Make a linked list of primes greater than 1 */
      while (TRUE) {
        key->value = *++prime;
        if (!(--numPrimes)) break;
        key->link.nextKey = key + 1;
        key++;
      }
      key->link.nextKey = NULL;
    } else {
      VBX_free(thisPool->keys);
      VBX_free(thisPool);
    }
    VBX_free(primes);
  }

  return(thisPool);
}

void
UKEYPOOL_annihilate(UKEYPOOL pool)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilates a UKEY pool
 *---------------------------------------------------------------------------*/
{
  if (pool) {
    VBX_free(pool->keys);
    free(pool);
  }
}


UInt
UKEYPOOL_numKeysLeft(UKEYPOOL pool)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the number of keys left (unreserved) in a UKEY pool
 *---------------------------------------------------------------------------*/
{
  if (pool)
    return(pool->numKeysLeft);
  else
    return(0);
}


void
UKEYREG_reset(UKEYREGP registry)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Empty a registry
 *---------------------------------------------------------------------------*/
{
  if (registry) *registry = (UKEYREG) 1;
}


UKEY
UKEY_reserve(UKEYPOOL pool)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns a unique key (UKEY) from a UKEY pool, or NULL if none can be had
 *---------------------------------------------------------------------------*/
{
  UKEY  thisKey = NULL;

  if (pool && pool->nextKey) {
    thisKey = pool->nextKey;
    pool->nextKey = thisKey->link.nextKey;
    thisKey->link.keyPool = pool;
    pool->numKeysLeft--;
  } else {
    assert(!pool->numKeysLeft);
  }

  return(thisKey);
}


void
UKEY_release(UKEY key)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Releases a UKEY, returning it to the pool
 *---------------------------------------------------------------------------*/
{
  UKEYPOOL  pool;

  if (key) {
    pool = key->link.keyPool;
    key->link.nextKey = pool->nextKey;
    pool->nextKey = key;
    pool->numKeysLeft++;
  }
}


void
UKEY_register(UKEYREGP registry, UKEY key)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Registers a UKEY in a UKEY registry if it is not already registered there
 *---------------------------------------------------------------------------*/
{
  if (registry && key && !UKEY_registered(registry, key))
    *registry *= key->value;
}


void
UKEY_unregister(UKEYREGP registry, UKEY key)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Unregisters a UKEY from a UKEY registry if it is registered there
 *---------------------------------------------------------------------------*/
{
#ifdef VXWORKS
  Quad quo, rem;
  if (registry && key && UKEY_registered(registry, key)) {
    lldivmod(*registry, (Quad) key->value, &quo, &rem);
   *registry = quo;
  }
#else
  if (registry && key && UKEY_registered(registry, key))
    *registry /= key->value;
#endif
}


Bool
UKEY_registered(UKEYREGP registry, UKEY key)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns TRUE if the UKEY is in the registry, FALSE otherwise
 *---------------------------------------------------------------------------*/
{
#ifdef VXWORKS
  Quad quo, rem;
  if (registry && key) {
    lldivmod(*registry, (Quad) key->value, &quo, &rem);
    return(!((Int)rem));
  } else
    return(FALSE);
#else
  if (registry && key)
    return(!(*registry % key->value));
  else
    return(FALSE);
#endif
}

#ifdef DEBUG

void
UKEYPOOL_dump(UKEYPOOL pool)
{
  UKEY  key;
  UInt  keyCount = 0;

  assert(pool);

  VBX_print("UKEYPOOL_dump(%p):\n", pool);

  key = pool->nextKey;
  while (key) {
    VBX_print("%8u", key->value);
    key = key->link.nextKey;
    keyCount++;
    if (!(keyCount % 10)) VBX_print("\n");
  }
  if (keyCount % 10) VBX_print("\n");

  VBX_print("End of UKEYPOOL_dump(%p); %u keys available\n", pool, keyCount);
}



#endif
