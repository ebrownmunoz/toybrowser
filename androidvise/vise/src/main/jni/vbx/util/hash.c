/* hash.c - hash table
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * The HASH_* methods implement a simple hashing scheme in which values
 * (pointers) are associated with symbols (also pointers).  Methods exist
 * for adding (symbol, value) pairs to the table and later looking them up,
 * the only limitation being that the symbol and value CANNOT BOTH BE NULL.
 * Entries in the hash table cannot be deleted. The hash table grows to
 * accommodate an arbitary number of entries, but if the final size is
 * known in advance, it is most efficient to create the table with that
 * number of entries.
 *
 * The interpretation of the symbols is up to the user, who passes hashing
 * and comparison functions to the creation function, thereby determining how
 * the resulting table will interpret the symbols.  Pre-packaged hashing and
 * comparison functions are provided for interpreting the symbols either as
 * signed integers or as null-terminated strings.  The latter is the default
 * interpretation; passing NULL functions to the creation function will
 * elicit this behavior.  The user is free to create other such functions on
 * the model of these, as needed.
 *
 * The interpretation of the values is likewise up to the user, who can cast
 * them externally to any type which will fit in a pointer type.
 *
 * The last argument to the creation function is an annihilation function
 * for whatever the values point to.  It is called by the hashtable
 * annihilation function on each active value.  It may be NULL, in which case
 * nothing is done to whatever the values point to, if anything.
 *
 * Defining DELTA_HASH creates a somewhat more complex and efficient hashing
 * scheme, but it is doubtful that the additional efficiency suffices to
 * warrant the increased complexity.
 *
 * HISTORY
 *  6-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 * 12-Jan-97  Dave Vetter (dvetter@verbex.com)
 *      Added HASH_modify()
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "vbx.h"
#include "utils.h"
#ifdef VXWORKS
#include "/vobs/core_vob/vbx/include/list.h"
#else
#include "list.h"
#endif
#include "hash.h"

/* #define DELTA_HASH */

#define DFLT_FULL_FACTOR  ((Float) 2.0)
#define DFLT_GROW_FACTOR  ((Float) 3.0)

/* Hashtable entry */
typedef struct htent_s {
  Ptr   sym;                            /* Hashed symbol */
  Ptr   val;                            /* Associated value */
#ifdef DELTA_HASH
  Int   del;                            /* Delta to next key to try */
#endif
} htent_t, * HTENT;

/* Hashtable */
typedef struct hash_s {
  HTENT  table;                         /* The table entries */
  Int    size;                          /* The size of the table */
  Int    inuse;                         /* Number entries in use */
  Int    (* hash)(HASH ht, Ptr sym);    /* The hash function */
  Bool   (* equals)(Ptr sym, Ptr hsym); /* The comparison function */
  void   (* free)(Ptr value);           /* Value annihilator */
  Float  fullFactor;
  Float  growFactor;
  UInt   entry;                         /* Table iterator */
#ifdef DEBUG
  Int    depth;
  Int    tries;
#endif
} hash_t;

static Bool HASH_in(HASH ht, Ptr sym, Ptr val);
static Bool HASH_key(HASH ht, Ptr sym, Int * keyPtr);


Int
HASH_integer(HASH ht, Ptr sym)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Integer hash function
 *---------------------------------------------------------------------------*/
{
#ifdef VXWORKS
  Quad rem, quo;
  lldivmod((Quad) abs((Int) sym) * (Quad) 3, (Quad) (ht->size), &quo, &rem);
  return((Int) rem);
#else
  return((Int) (((Quad) abs((Int) sym) * (Quad) 3) % (Quad) ht->size));
#endif
}


Bool
HASH_integerEquals(Ptr sym, Ptr hsym)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Integer comparison function
 *---------------------------------------------------------------------------*/
{
  return((Int) sym == (Int) hsym);
}


Int
HASH_string(HASH ht, Ptr sym)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * String (default) hash function
 *---------------------------------------------------------------------------*/
{
  Char  * cp = (Char *) sym;
  Int     key = 0;
  Int     i = -1;

  do key += *cp++ << (0xF & i--);
  while (*cp);

  key %= ht->size;
  if (key < 0) key += ht->size;

  return(key);
}


Bool
HASH_stringEquals(Ptr sym, Ptr hsym)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * String (default) comparison function
 *---------------------------------------------------------------------------*/
{
  return(!strcmp(sym, hsym));
}


HASH
HASH_create(Int initialSize, Int (* hash)(HASH ht, Ptr sym),
            Bool (* equals)(Ptr sym, Ptr hsym), void (* free)(Ptr value))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates and returns a new hashtable.  The minimal possible size is passed
 * in as a parameter.  The nearest prime larger than that number is used as
 * the actual size.
 *---------------------------------------------------------------------------*/
{
  HASH  ht;

  ht = (HASH) VBX_calloc(1, sizeof(hash_t));
  if (hash)
    ht->hash = hash;
  else
    ht->hash = HASH_string;
  if (equals)
    ht->equals = equals;
  else
    ht->equals = HASH_stringEquals;
  ht->free = free;
  ht->fullFactor = DFLT_FULL_FACTOR;
  ht->growFactor = DFLT_GROW_FACTOR;
  if (initialSize < 1) initialSize = 1;
  ht->size = (Int) PRIMES_next((UInt) (ht->fullFactor * initialSize) + 1);
  ht->inuse = 0;
  ht->table = (HTENT) VBX_calloc((size_t) ht->size, sizeof(htent_t));
  ht->entry = 0;
#ifdef DEBUG
  ht->depth = 0;
  ht->tries = 0;
#endif
  return (ht);
}


void
HASH_annihilate(HASH ht)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilates a hash table
 *---------------------------------------------------------------------------*/
{
  Int  i;

  if (ht) {
    /* If there is a free() function, use it to free the values */
    if (ht->free)
      for (i = 0; i < ht->size; i++)
        if (ht->table[i].val)
          ht->free(ht->table[i].val);
    VBX_free(ht->table);
    VBX_free(ht);
  }
}


void
HASH_processSymbols(HASH ht, void (* fcn)(Ptr symbol))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Apply the specified fcn to all the symbols in the hash table.  For example,
 * if fcn() is free(), this frees the memory that the symbol pointers
 * reference (always presupposing that the symbols are true pointers, as they
 * are when hashing strings, but not when hashing integers).
 *---------------------------------------------------------------------------*/
{
  Int  i;

  if (!fcn) fcn = (void (*)(Ptr)) free;

  if (ht && fcn)
    for (i = 0; i < ht->size; i++)
      if (ht->table[i].sym)
        fcn(ht->table[i].sym);
}


void
HASH_setFactors(HASH ht, Float full, Float grow)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the factors which determine when and by how much the hash table will
 * be expanded
 *---------------------------------------------------------------------------*/
{
  assert(ht && full >= (Float) 1.0 && grow >= (Float) 1.0);

  ht->fullFactor = full;
  ht->growFactor = grow;
}


Bool
HASH_add(HASH ht, Ptr sym, Ptr val)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns FALSE if 'sym' is already in the table with a value different from
 * 'val'.  Otherwise, returns TRUE, and inserts 'sym' into the table with
 * the value 'val' if it's not already there.   This function differs from
 * HASH_in() in making sure that the hash table is large enough to accept
 * further entries efficiently, expanding it if it is not.
 *---------------------------------------------------------------------------*/
{
  Bool     success;
  assert(ht && (sym || val));

  /* Try to hash 'sym' into ht */
  if (success = HASH_in(ht, sym, val)) {
    /* If the hash table is now too full, make a new, larger one */
    if ((Int) (ht->fullFactor * ht->inuse) + 1 >= ht->size) {
      Int      i;
      Int      oldSize = ht->size;
      HTENT    table, oldTable = ht->table;

      ht->size = (Int) PRIMES_next((UInt) (ht->growFactor * ht->inuse));
      ht->inuse = 0;
      ht->table = (HTENT) VBX_calloc((size_t) ht->size, sizeof(htent_t));

      /* Create a new hash table from the old one */
      for (table = oldTable, i = 0; i < oldSize; i++, table++)
        if ((table->sym || table->val) && !HASH_in(ht, table->sym, table->val))
          FATAL_BRA_ "Can't expand inconsistent hashtable" _KET;

      /* Delete the old hash table */
      VBX_free(oldTable);

#ifdef DEBUG
      VBX_print("HASH_add: Expanded hash table @%p\n", ht);
#endif
    }
  }

  return(success);
}


static Bool
HASH_key(HASH ht, Ptr sym, Int * keyPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Look up 'sym' in 'ht'. If 'sym' is found report its key and return TRUE;
 * if not, return FALSE and report the key at which it should be inserted.
 *---------------------------------------------------------------------------*/
{
  Int      key;
#ifdef DELTA_HASH
  Int      delta = 0;
#endif

  assert(ht);

  /* Calculate the initial key */
  key = ht->hash(ht, sym);

  /* If the hash table doesn't exist, report the key */
  if (ht->size == 0) {
    *keyPtr = key;
    return(FALSE);
  }

#ifdef DEBUG
  ht->tries++;
#endif

  while (TRUE) {

#ifdef DEBUG
    ht->depth++;
#endif
    /* If this entry is not in use, report its key */
    if (!ht->table[key].sym && !ht->table[key].val) {
      assert(ht->size > key && key >= 0);
      *keyPtr = key;
#ifdef DELTA_HASH
      /* Normalize delta to the interval (0, ht->size - 1) */
      while (delta < 0) delta += ht->size;
      delta %= ht->size;
      /* Put it into the appropriate entry */
      if ((key -= delta) < 0) {
        key += ht->size;
        delta -= ht->size;
      }
      assert(ht->size > key && key >=0 && ht->size > delta && delta > -ht->size);
      ht->table[key].del = delta;
#endif
      return(FALSE);
    }

    /* If this entry is the one sought, report its key */
    if (ht->equals(sym, ht->table[key].sym)) {
      *keyPtr = key;
      return(TRUE);
    }

    /* Otherwise, increment the key and try again */
#ifdef DELTA_HASH
    if (ht->table[key].del) {
      /* The delta field is not zero, so use it to get the next key */
      key += ht->table[key].del;
      /* Increment delta only if a previous delta field was zero */
      if (delta) delta += ht->table[key].del;
    } else {
      /* The delta field is zero, so go to the next entry */
      delta++;
      key++;
      if (key >= ht->size) key -= ht->size;
    }
#else
    key++;
    if (key >= ht->size) key -= ht->size;
#endif
  }
}


Bool
HASH_lookup(HASH ht, Ptr sym, Ptr * val)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Look up 'sym' in 'ht'. If 'sym' is found report its associated value in
 * *val and return TRUE;  if not, return FALSE and report the key in *val.
 *---------------------------------------------------------------------------*/
{
  Int      key;

  /* If this entry is the one sought, report its value in *val */

  if (HASH_key(ht, sym, &key)) {
    if (val) *val = ht->table[key].val;
    return(TRUE);

  /* Otherwise, report its key in *val */
  } else {
    if (val) *val = (Ptr) key;
    return(FALSE);
  }
}


Bool
HASH_modify(HASH ht, Ptr sym, Ptr val)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Look up 'sym' in 'ht'. If 'sym' is found, change its associated value to
 * val and return TRUE;  if not, return FALSE.  Note that this is just the
 * opposite of HASH_add(), which adds a symbol only if it is not already
 * present.
 *---------------------------------------------------------------------------*/
{
  Int      key;

  /* If this entry is the one sought, change its value to val */
  if (HASH_key(ht, sym, &key)) {
    ht->table[key].val = val;
    return(TRUE);
  } else {
    return(FALSE);
  }
}


Int
HASH_used(HASH ht)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the number of entries in use in the hash table
 *---------------------------------------------------------------------------*/
{
  assert(ht);
  return (ht->inuse);
}


static Bool
HASH_in(HASH ht, Ptr sym, Ptr val)
/*---------------------------------------------------------------------------*
 * DESRIPTION
 * Returns FALSE if 'sym' is already in the table with a value different from
 * 'val'.  Otherwise, returns TRUE, and inserts 'sym' into the table with
 * the value 'val' if it's not already there.  This function does not check to
 * see if a new entry will fit in the table.
 *---------------------------------------------------------------------------*/
{
  Int      key;

  assert(ht && (sym || val));

  if (HASH_key(ht, sym, &key)) {
    /* Lookup finds sym, so value must equal that already in table */
    return(val == ht->table[key].val);
  } else {
    /* Lookup fails to find sym, so key is the hash index */
    ht->table[key].sym = sym;
    ht->table[key].val = val;
    ht->inuse++;
    return(TRUE);
  }
}


Ptr
HASH_firstValue(HASH ht, Ptr * symPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the value of the first (physically, not chronologically) entry that
 * is in use in the table, and reports the corresponding symbol if symPtr is
 * not null.
 *---------------------------------------------------------------------------*/
{
  assert(ht);

  ht->entry = 0;
  return(HASH_nextValue(ht, symPtr));
}

Ptr
HASH_nextValue(HASH ht, Ptr * symPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the value of the next (physically, not chronologically) entry that
 * is in use in the table, and reports the corresponding symbol if symPtr is
 * not null.
 *---------------------------------------------------------------------------*/
{
  Ptr     symbol = NULL, value = NULL;

  assert(ht);

  while ((Int) ht->entry < ht->size) {
    symbol = ht->table[ht->entry].sym;
    value = ht->table[ht->entry++].val;
    if (symbol || value) break;
  }

  if (symPtr)
    *symPtr = symbol;

  return(value);
}


LIST
HASH_toObjList(HASH ht)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a list of the symbols in hash table 'ht' indexed by their values.
 * Call this only when the values in ht are confined to integers smaller
 * than the number of active entries in ht;  i.e., when ht is essentially a
 * scrambled array.
 *---------------------------------------------------------------------------*/
{
  Int             i;
  LIST            list;

  assert(ht);
  list = LIST_new();
  for (i = 0; i < ht->size; i++)
    if (ht->table[i].sym) {
      assert((Int) ht->table[i].val < ht->inuse);
      LIST_insert (list, ht->table[i].sym, (Int) ht->table[i].val);
    }
  assert(LIST_length(list) == ht->inuse);
  return (list);
}


#ifdef DEBUG

void
HASH_dump(HASH ht)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Dump the hash table.
 *---------------------------------------------------------------------------*/
{
  Int      i;

  assert(ht);
  VBX_print("HASH_dump() of %p; %d entries, %d in use\n",
              ht, ht->size, ht->inuse);
#ifdef DELTA_HASH
  VBX_print("    Symbol     Value    Delta\n");
#else
  VBX_print("    Symbol     Value\n");
#endif
  for (i = 0; i < ht->size; i++)
    if (ht->table[i].sym || ht->table[i].val)
#ifdef DELTA_HASH
      VBX_print("  %8p  %8p    %5d\n",
                ht->table[i].sym, ht->table[i].val, ht->table[i].del);
#else
      VBX_print("  %8p  %8p\n",
                ht->table[i].sym, ht->table[i].val);
#endif
    else
      VBX_print("    UNUSED\n");
}


Int
HASH_numDeltaGreater(HASH ht, UInt floor)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the number of entries with delta greater than the specified floor
 *---------------------------------------------------------------------------*/
{
  Int     i, delta, count = 0;

  assert(ht);

#ifdef DELTA_HASH
  for (i = 0; i < ht->size; i++) {
    delta = ht->table[i].del;
    while (delta < 0) delta += ht->size;
    if (delta > floor) count++;
  }
#endif

  return(count);
}


Double
HASH_meanDepth(HASH ht)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the average depth of searches since the table was created
 *---------------------------------------------------------------------------*/
{
  return((Double) ht->depth / (Double) ht->tries);
}

#endif                          /* def DEBUG */
