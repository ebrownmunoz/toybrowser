/*
 * sphdr.c
 * speech header resources
 * modified from sphere code obtained from NIST
 * Verbex Voice Systems (jrt)
 */

static char AtFSid[]= "$__Header$";
#include "pthr.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "vbx.h"
#include "sphdr.h"

/*
 * Abbreviations used:
 *   hdr = header
 *   fld = field
 *   cmnt = comment
 */

#define ALLOCATION_QUANTUM 1024
#define HDR_ID_SIZE        8
#define NIST_LABEL         "NIST_1A"
#define HDR_SIZE_SIZE      8
#define HDR_SIZE_MULT_DFLT ALLOCATION_QUANTUM
#define HDR_TERMINATOR     "end_head"
#define COMMENT_CHAR       ';'
#define PAD_CHAR           ' '
#define HDR_NO_DATA        "<none>"

static Char *HDR_createStr(Int size);
static void  HDR_freeFld(HDRF fld);
static Bool  HDR_parseBuf(HDR hdr, Ptr hdrBuf, Ptr endOfBuf);
static Ptr   HDR_parseLine(HDR hdr, Ptr startOfLine);
static Bool  HDR_valid(Int type);

HDR
HDR_create(void)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a blank speech header structure.
 *---------------------------------------------------------------------------*/
{
  HDR hdr;
  
  if (hdr = (HDR)calloc(1,sizeof(hdr_t))) {
    hdr->flds = NULL;
    hdr->numFlds = 0;
    hdr->sizeMult = HDR_SIZE_MULT_DFLT;
  }

  return hdr;
}

Bool
HDR_isNIST(FILE *fp)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Determine if the file has a NIST speech header.
 *
 * The file pointer will be returned to the beginning of the file.
 *---------------------------------------------------------------------------*/
{
  Char hdrID[HDR_ID_SIZE];
  Char hdrSizeStr[HDR_SIZE_SIZE];
  Int hdrSize;
  
  if (HDR_ID_SIZE != fread(hdrID, sizeof(Char), HDR_ID_SIZE, fp) || HDR_SIZE_SIZE != fread(hdrSizeStr, sizeof(Char), HDR_SIZE_SIZE, fp)) {
    SEVERE_BRA_ "File read error\n" _KET;
    return FALSE;
  }

  if (fseek(fp, 0, SEEK_SET) != 0) {
    SEVERE_BRA_ "File seek error\n" _KET;
    return FALSE;
  }

  return (hdrID[HDR_ID_SIZE-1] != '\n' ||
          strncmp(hdrID, NIST_LABEL, HDR_ID_SIZE-1) ||
          hdrSizeStr[HDR_SIZE_SIZE-1] != '\n' ||
          (hdrSize = atoi(hdrSizeStr)) <= 0 ||
          hdrSize % HDR_SIZE_MULT_MIN != 0) ? FALSE : TRUE;
}

Bool
HDR_isNISTMem(Ptr buf)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Determine if "buf" is starts with a NIST header
 *
 * The file pointer will be returned to the beginning of the file.
 *---------------------------------------------------------------------------*/
{
  Bool rval;

  rval = (strncmp(buf, NIST_LABEL, strlen(NIST_LABEL)) == 0);
  return(rval);
}

Bool
HDR_setSizeMult(HDR hdr, Int sizeMult)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set value, which defines Header granularity.
 * Must be multiple of HDR_SIZE_MULT_MIN, for HDR_isNIST() could work.
 *---------------------------------------------------------------------------*/
{
  if (!hdr || sizeMult <= 0 || sizeMult % HDR_SIZE_MULT_MIN != 0)
    return FALSE;

  hdr->sizeMult = sizeMult;

  return TRUE;
}

Bool
HDR_read(FILE *fp, HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read header from the file into the speech header structure.
 * Speech header structure must have previously been created with HDR_create.
 *---------------------------------------------------------------------------*/
{
  Char hdrID[HDR_ID_SIZE];
  Char hdrSizeStr[HDR_SIZE_SIZE];
  Int hdrSize;
  Ptr hdrBuf;
  
  if (fp == NULL) {
    SEVERE_BRA_ "Bad file pointer\n" _KET;
    return FALSE;
  }
  if (ftell(fp) != 0) {
    SEVERE_BRA_ "Not at start of file\n" _KET;
    return FALSE;
  }
  if (hdr == NULL) {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
    return FALSE;
  }
  if (HDR_ID_SIZE != fread(hdrID, sizeof(Char), HDR_ID_SIZE, fp) || HDR_SIZE_SIZE != fread(hdrSizeStr, sizeof(Char), HDR_SIZE_SIZE, fp)) {
    SEVERE_BRA_ "File read error\n" _KET;
    return FALSE;
  }
  if (hdrID[HDR_ID_SIZE-1] != '\n' || strncmp(hdrID, NIST_LABEL, HDR_ID_SIZE-1)) {
    SEVERE_BRA_ "Bad header label\n" _KET;
    return FALSE;
  }
  if (hdrSizeStr[HDR_SIZE_SIZE-1] != '\n' || (hdrSize = atoi(hdrSizeStr)) <= HDR_ID_SIZE + HDR_SIZE_SIZE || hdrSize % HDR_SIZE_MULT_MIN != 0) {
    SEVERE_BRA_ "Bad header size\n" _KET;
    return FALSE;
  }

  hdrSize -= (HDR_ID_SIZE + HDR_SIZE_SIZE);
  if (!(hdrBuf = HDR_createStr(hdrSize))) {
    SEVERE_BRA_ "Can't allocate header of size %d\n", hdrSize _KET;
    return FALSE;
  }

  if (hdrSize != fread(hdrBuf, sizeof(Char), hdrSize, fp)) {
    SEVERE_BRA_ "File read error\n" _KET;
    free(hdrBuf);
    return FALSE;
  }

  hdrBuf[hdrSize] = '\0';

  if (hdr->numFlds != 0) 
    HDR_clear(hdr);

  if (!HDR_parseBuf(hdr, hdrBuf, hdrBuf + hdrSize)) {
    free(hdrBuf);
    HDR_clear(hdr);
    return FALSE;
  }

  free(hdrBuf);
  return TRUE;
}

Ptr
HDR_parse(Ptr stream, HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read header from an in-core stream into the given speech header structure.
 * Speech header structure must have previously been created with HDR_create.
 * A pointer to the start of data following the well formed header is returned.
 * If stream is headerless, it is returned as is with supplied header cleared,
 * which means caller's default setting. For example current WAV_read default:
 * linear, 16 bits, LITENDIAN byte order.
 *---------------------------------------------------------------------------*/
{
  Char hdrID[HDR_ID_SIZE];
  Char hdrSizeStr[HDR_SIZE_SIZE];
  Int hdrSize;
  Ptr hdrBuf, streamPtr = stream;
  
  if (hdr == NULL) {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
    return NULL;
  }

  if (hdr->numFlds != 0)
    HDR_clear(hdr);

  memcpy(hdrID, streamPtr, HDR_ID_SIZE);
  streamPtr += HDR_ID_SIZE;
  memcpy(hdrSizeStr, streamPtr, HDR_SIZE_SIZE);
  streamPtr += HDR_SIZE_SIZE;

  if (hdrID[HDR_ID_SIZE-1] != '\n' || strncmp(hdrID, NIST_LABEL, HDR_ID_SIZE-1))
    return stream; /* Headerless stream */

  if (hdrSizeStr[HDR_SIZE_SIZE-1] != '\n' || (hdrSize = atoi(hdrSizeStr)) <= HDR_ID_SIZE + HDR_SIZE_SIZE || hdrSize % HDR_SIZE_MULT_MIN != 0) {
    SEVERE_BRA_ "Bad header size\n" _KET;
    return NULL;
  }

  hdrSize -= (HDR_ID_SIZE + HDR_SIZE_SIZE);
  if (!(hdrBuf = HDR_createStr(hdrSize))) {
    SEVERE_BRA_ "Can't allocate header of size %d\n", hdrSize _KET;
    return NULL;
  }

  memcpy(hdrBuf, streamPtr, hdrSize);
  streamPtr += hdrSize;

  hdrBuf[hdrSize] = '\0';

  if (!HDR_parseBuf(hdr, hdrBuf, hdrBuf + hdrSize)) {
    free(hdrBuf);
    HDR_clear(hdr);
    return NULL;
  }

  free(hdrBuf);
  return streamPtr;
}

Bool
HDR_addFld(HDR hdr, const Char *name, Int type, const void *data, const Char *cmnt)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add a field to the speech header structure.
 * Always pass a pointer to the data regardless of the data type.
 * If no comment is desired then set 'cmnt' to NULL.
 *---------------------------------------------------------------------------*/
{
  Int i;
  HDRF newFld;
  HDRF *newFlds;
  
  if (hdr == NULL) {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
    return FALSE;
  }
  if (name == NULL) {
    SEVERE_BRA_ "Bad field name pointer\n" _KET;
    return FALSE;
  }
  if (!isalpha(*name)) {
    SEVERE_BRA_ "Bad field name syntax\n" _KET;
    return FALSE;
  }
  for (i=1; name[i] != '\0'; i++)
    if (!isalnum(name[i]) && name[i] != '_') {
      SEVERE_BRA_ "Bad name syntax\n" _KET;
      return FALSE;
    }
  if (data == NULL) {
    SEVERE_BRA_ "Bad data pointer\n" _KET;
    return FALSE;
  }
  if (!HDR_valid(type)) {
    SEVERE_BRA_ "Bad data type\n" _KET;
    return FALSE;
  }
  newFlds = (HDRF *)calloc(hdr->numFlds+1, sizeof(HDRF));
  newFld = (HDRF)calloc(1, sizeof(hdrFld_t));
  if (newFlds == NULL || newFld == NULL) {
    SEVERE_BRA_ "Out of memory\n" _KET;
    return FALSE;
  }
  newFld->name = (Char *)malloc(strlen(name) + 1);
  if (newFld->name == NULL) {
    SEVERE_BRA_ "Out of memory\n" _KET;
    return FALSE;
  }
  strcpy(newFld->name, name);

  newFld->type = type;
  switch(type) {
    case INT_TYPE:
      newFld->length = sizeof(Int);
      newFld->data = (void *)calloc(1, sizeof(Int));
      if (newFld->data == NULL) {
        SEVERE_BRA_ "Out of memory\n" _KET;
        return FALSE;
      }
      *(Int *)(newFld->data) = *(Int *)data;
      break;
    case REAL_TYPE:
      newFld->length = sizeof(Float);
      newFld->data = (void *)calloc(1, sizeof(Float));
      if (newFld->data == NULL) {
        SEVERE_BRA_ "Out of memory\n" _KET;
        return FALSE;
      }
      *(Float *)(newFld->data) = *(Float *)data;
      break;
    default:
      if (newFld->length = strlen(data)) {
        newFld->data = (Char *)malloc(newFld->length + 1);
        if (newFld->data == NULL) {
          SEVERE_BRA_ "Out of memory\n" _KET;
          return FALSE;
        }
        strcpy(newFld->data, data);
      } else {
        newFld->length = strlen(HDR_NO_DATA);
        newFld->data = (Char *)malloc(newFld->length + 1);
        if (newFld->data == NULL) {
          SEVERE_BRA_ "Out of memory\n" _KET;
          return FALSE;
        }
        strcpy(newFld->data, HDR_NO_DATA);
      }
  }
  if (cmnt != NULL) {
    newFld->cmnt = (Char *)malloc(strlen(cmnt) + 1);
    if (newFld->cmnt == NULL) {
      SEVERE_BRA_ "Out of memory\n" _KET;
      return FALSE;
    }
    strcpy(newFld->cmnt, cmnt);
  }
  else newFld->cmnt = NULL;
  for (i=0; i < hdr->numFlds; i++) newFlds[i] = hdr->flds[i];
  newFlds[i] = newFld;
  if (hdr->flds != NULL) free(hdr->flds);
  hdr->flds = newFlds;
  hdr->numFlds++;
  return TRUE;
}

void
HDR_addCmnt(HDR hdr, Char *cmnt)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add a field consisting of only a comment to the speech header structure.
 *---------------------------------------------------------------------------*/
{
  Int i;
  HDRF newFld;
  HDRF *newFlds;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  if (cmnt == NULL) FATAL_BRA_ "Bad comment pointer" _KET;
  newFlds = (HDRF *)VBX_calloc(hdr->numFlds+1, sizeof(HDRF));
  newFld = (HDRF)VBX_calloc(1, sizeof(hdrFld_t));
  newFld->name = NULL;
  newFld->type = 0;
  newFld->data = NULL;
  newFld->length = 0;
  newFld->cmnt = VBX_salloc(cmnt);
  for (i=0; i < hdr->numFlds && hdr->flds[i]->name == NULL; i++)
    newFlds[i] = hdr->flds[i];
  newFlds[i] = newFld;
  for ( ; i < hdr->numFlds; i++) newFlds[i+1] = hdr->flds[i];
  if (hdr->flds != NULL) free(hdr->flds);
  hdr->flds = newFlds;
  hdr->numFlds++;
}

Int
HDR_numFlds(HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the number of fields in the speech header structure.
 *---------------------------------------------------------------------------*/
{
  if (hdr)
    return hdr->numFlds;

  return -1;
}

Bool
HDR_fldNames(HDR hdr, Int maxNames, Char *names[])
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Put the names of the fields in the speech header structure into 'names'.
 * No more than maxNames names will be extracted.
 *---------------------------------------------------------------------------*/
{
  Int i;
  
  if (hdr) {
    for (i=0; i < hdr->numFlds && i < maxNames; i++)
      names[i] = hdr->flds[i]->name;
    return TRUE;
  }

  return FALSE;
}

Bool
HDR_fldCmnts(HDR hdr, Int maxCmnts, Char *cmnts[])
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Put the comments of the fields in the speech header structure into 'cmnts'.
 * No more than maxCmnts comments will be extracted.
 *---------------------------------------------------------------------------*/
{
  Int i;
  
  if (hdr) {
    for (i=0; i < hdr->numFlds && i < maxCmnts; i++)
      cmnts[i] = hdr->flds[i]->cmnt;
    return TRUE;
  }

  return FALSE;
}

Int
HDR_dataType(HDR hdr, Char *name)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the data type of the data in the field with name 'name' in the
 *   speech header structure.
 * If no field matches 'name', -1 is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  
  if (hdr) {
    if (name) {
      while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
      if (i < hdr->numFlds)
        return hdr->flds[i]->type;
    } else {
      SEVERE_BRA_ "Bad name pointer\n" _KET;
    }
  } else {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
  }
  return -1;
}

void *
HDR_data(HDR hdr, const Char *name, Int *length)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a pointer to the data in the field with name 'name' in the speech
 *   header structure.
 * If no field matches 'name', NULL is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  
  if (hdr) {
    if (name) {
      while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
      if (i < hdr->numFlds) {
        *length = hdr->flds[i]->length;
        return hdr->flds[i]->data;
      }
    } else {
      SEVERE_BRA_ "Bad name pointer\n" _KET;
    }
  } else {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
  }
  return NULL;
}

Char *
HDR_cmnt(HDR hdr, Char *name)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a pointer to the comment in the field with name 'name' in the speech
 *   header structure.
 * If no field matches 'name', NULL is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  
  if (hdr) {
    if (name) {
      while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
      if (i < hdr->numFlds)
        return hdr->flds[i]->cmnt;
    } else {
      SEVERE_BRA_ "Bad name pointer\n" _KET;
    }
  } else {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
  }
  return NULL;
}

Int
HDR_chgFldName(HDR hdr, Char *name, Char *newName)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Change the name of the field with name 'name' to 'newName' in the speech
 *   header structure.
 * If no field matches 'name', -1 is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  if (name == NULL || newName == NULL) FATAL_BRA_ "Bad name pointer" _KET;
  while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
  if (i < hdr->numFlds) {
    free(hdr->flds[i]->name);
    hdr->flds[i]->name = VBX_salloc(newName);
    return 0;
  }
  return -1;
}

Int
HDR_chgFldData(HDR hdr, Char *name, Int type, void *data)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Change the data in the field with name 'name' in the speech header
 *   structure.
 * Always pass a pointer to the data regardless of the data type.
 * If no field matches 'name', -1 is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  if (name == NULL) FATAL_BRA_ "Bad name pointer" _KET;
  if (data == NULL) FATAL_BRA_ "Bad data pointer" _KET;
  if (!HDR_valid(type)) FATAL_BRA_ "Bad data type" _KET;
  while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
  if (i < hdr->numFlds) {
    hdr->flds[i]->type = type;
    free(hdr->flds[i]->data);
    switch(type) {
      case INT_TYPE:
        hdr->flds[i]->length = sizeof(Int);
        hdr->flds[i]->data = (void *)VBX_calloc(1, sizeof(Int));
        *(Int *)(hdr->flds[i]->data) = *(Int *)data;
        break;
      case REAL_TYPE:
        hdr->flds[i]->length = sizeof(Float);
        hdr->flds[i]->data = (void *)VBX_calloc(1, sizeof(Float));
        *(Float *)(hdr->flds[i]->data) = *(Float *)data;
        break;
      default:
        if (strlen(data)) {
          hdr->flds[i]->length = strlen(data);
          hdr->flds[i]->data = VBX_salloc(data);
        } else {
          hdr->flds[i]->length = strlen(HDR_NO_DATA);
          hdr->flds[i]->data = VBX_salloc(HDR_NO_DATA);
        }
    }
    return 0;
  }
  return -1;
}

Int
HDR_chgFldCmnt(HDR hdr, Char *name, Char *cmnt)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Change the comment in the field with name 'name' in the speech header
 *   structure.
 * If no comment is desired then set 'cmnt' to NULL.
 * If no field matches 'name', -1 is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  if (name == NULL) FATAL_BRA_ "Bad name pointer" _KET;
  while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
  if (i < hdr->numFlds) {
    if (hdr->flds[i]->cmnt != NULL) free(hdr->flds[i]->cmnt);
    if (cmnt == NULL) hdr->flds[i]->cmnt = NULL;
    else hdr->flds[i]->cmnt = VBX_salloc(cmnt);
    return 0;
  }
  return -1;
}

Int
HDR_delFld(HDR hdr, Char *name)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Delete the field with name 'name' from the speech header structure.
 * If no field matches 'name', -1 is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  Int j;
  HDRF *newFlds;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  if (name == NULL) FATAL_BRA_ "Bad name pointer" _KET;
  if (hdr->numFlds != 0) {
    while (i < hdr->numFlds && (hdr->flds[i]->name == NULL || strcmp(name, hdr->flds[i]->name) != 0)) i++;
    if (i < hdr->numFlds) {
      if (hdr->numFlds == 1) {
        HDR_clear(hdr);
      } else {
        newFlds = (HDRF *)VBX_calloc(hdr->numFlds-1, sizeof(HDRF));
        j = i;
        for (i=0; i < j; i++) newFlds[i] = hdr->flds[i];
        HDR_freeFld(hdr->flds[i]);
        for (i++; i < hdr->numFlds; i++) newFlds[i-1] = hdr->flds[i];
        free(hdr->flds);
        hdr->flds = newFlds;
        hdr->numFlds--;
      }
      return 0;
    }
  }
  return -1;
}

Int
HDR_delCmnt(HDR hdr, Char *cmnt)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Delete the field consisting of only a comment with comment 'cmnt' from the
 *   speech header structure.
 * If no comment matches 'cmnt', -1 is returned.
 *---------------------------------------------------------------------------*/
{
  Int i=0;
  Int j;
  HDRF *newFlds;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  if (cmnt == NULL) FATAL_BRA_ "Bad comment pointer" _KET;
  if (hdr->numFlds != 0) {
    while (i < hdr->numFlds && (hdr->flds[i]->name != NULL || hdr->flds[i]->cmnt == NULL || strcmp(cmnt, hdr->flds[i]->cmnt) != 0)) i++;
    if (i < hdr->numFlds) {
      if (hdr->numFlds == 1) {
        HDR_clear(hdr);
      } else {
        newFlds = (HDRF *)VBX_calloc(hdr->numFlds-1, sizeof(HDRF));
        j = i;
        for (i=0; i < j; i++) newFlds[i] = hdr->flds[i];
        HDR_freeFld(hdr->flds[i]);
        for (i++; i < hdr->numFlds; i++) newFlds[i-1] = hdr->flds[i];
        free(hdr->flds);
        hdr->flds = newFlds;
        hdr->numFlds--;
      }
      return 0;
    }
  }
  return -1;
}

void
HDR_clear(HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Delete all the fields in the speech header structure.
 *---------------------------------------------------------------------------*/
{
  Int i;
  
  if (hdr) {
    for (i=0; i < hdr->numFlds; i++)
      HDR_freeFld(hdr->flds[i]);
    VBX_free(hdr->flds);
    hdr->numFlds = 0;
  } else {
    WARN_BRA_ "NULL header pointer\n" _KET;
  }
}

void
HDR_print(HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Print out all the fields in the speech header structure to the screen.
 *---------------------------------------------------------------------------*/
{
  Int i;
  
  if (hdr == NULL) {
    SEVERE_BRA_ "Bad header pointer\n" _KET;
    return;
  }
  printf("\n\nNumber of fields in header: %d\n\n", hdr->numFlds);
  for (i=0; i < hdr->numFlds; i++)
    if (hdr->flds[i]->name == NULL) {
      printf(";%s\n", hdr->flds[i]->cmnt);
    } else {
      switch (hdr->flds[i]->type) {
        case INT_TYPE:
          printf("%s -i %d",
                 hdr->flds[i]->name,
                 *(Int *)hdr->flds[i]->data);
          break;
        case REAL_TYPE:
          printf("%s -r %f",
                 hdr->flds[i]->name,
                 *(Float *)hdr->flds[i]->data);
          break;
        default:
          printf("%s -s%d %s",
                 hdr->flds[i]->name,
                 hdr->flds[i]->length,
                 (Char *)hdr->flds[i]->data);
      }
      if (hdr->flds[i]->cmnt != NULL)
        printf(" ;%s\n", hdr->flds[i]->cmnt);
      else
        printf("\n");
    }
  printf("\n\n");
}

void
HDR_write(FILE *fp, HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write the speech header structure to the file in NIST format.
 *---------------------------------------------------------------------------*/
{
  Int i;
  Int numBytes;
  
  if (fp == NULL) FATAL_BRA_ "Bad file pointer" _KET;
  if (ftell(fp) != 0) FATAL_BRA_ "Not at start of file" _KET;
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;
  clearerr(fp);
  if (fseek(fp, HDR_ID_SIZE + HDR_SIZE_SIZE, SEEK_SET) != 0)
    FATAL_BRA_ "File seek error" _KET;
  for (i = 0; i < hdr->numFlds; i++) {
    if (hdr->flds[i]->name == NULL) {
      if (fprintf(fp, ";%s\n", hdr->flds[i]->cmnt) < 0)
        FATAL_BRA_ "File write error" _KET;
    } else {
      switch (hdr->flds[i]->type) {
        case INT_TYPE:
          if (fprintf(fp, "%s -i %d", hdr->flds[i]->name, *(Int *)hdr->flds[i]->data) < 0)
            FATAL_BRA_ "File write error" _KET;
          break;
        case REAL_TYPE:
          if (fprintf(fp, "%s -r %f", hdr->flds[i]->name, *(Float *)hdr->flds[i]->data) < 0)
            FATAL_BRA_ "File write error" _KET;
          break;
        default:
          if (fprintf(fp, "%s -s%d %s", hdr->flds[i]->name, hdr->flds[i]->length, (Char *)hdr->flds[i]->data) < 0)
            FATAL_BRA_ "File write error" _KET;
      }
      if (hdr->flds[i]->cmnt != NULL) {
        if (fprintf(fp, " ;%s\n", hdr->flds[i]->cmnt) < 0)
          FATAL_BRA_ "File write error" _KET;
      } else {
        if (fprintf(fp, "\n") < 0)
          FATAL_BRA_ "File write error" _KET;
      }
    }
  }
  if (fprintf(fp, "%s\n", HDR_TERMINATOR) < 0) FATAL_BRA_ "File write error" _KET;
  if (fputc('\n', fp) == (Int) NULL) FATAL_BRA_ "File write error" _KET;
  numBytes = ftell(fp);
  i = numBytes % hdr->sizeMult;
  if (i > 0) {
    i = hdr->sizeMult - i;
    numBytes += i;
    while (--i > 0)
      if (fputc(PAD_CHAR, fp) == (Int) NULL) FATAL_BRA_ "File write error" _KET;
    if (fputc('\n', fp) == (Int) NULL) FATAL_BRA_ "File write error" _KET;
  }
  if (numBytes != ftell(fp)) FATAL_BRA_ "File write error" _KET;
  if (fseek(fp, 0, SEEK_SET) != 0) FATAL_BRA_ "File seek error" _KET;
  if (fprintf(fp, "%*s\n", HDR_ID_SIZE-1, NIST_LABEL) < 0 ||
      fprintf(fp, "%*d\n", HDR_SIZE_SIZE-1, numBytes) < 0 ||
      fflush(fp) != 0) FATAL_BRA_ "File write error" _KET;
  if (fseek(fp, numBytes, SEEK_SET) != 0) FATAL_BRA_ "File seek error" _KET;
}

Ptr
HDR_writeMem(HDR hdr, Int *hdrBytes)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write the speech header structure to the file in NIST format.
 * PLEASE NOTE:  "Header Comment" functionality is not supported here!
 *---------------------------------------------------------------------------*/
{
  Int   i, len;
  Int   blksiz = ALLOCATION_QUANTUM, byteCnt = 0;
  Char *fldBuf;
  Ptr   memBlk, mp;
  
  if (hdr == NULL) FATAL_BRA_ "Bad header pointer" _KET;

  memBlk = (Ptr) VBX_calloc(ALLOCATION_QUANTUM, 1);
  fldBuf = (Ptr) VBX_calloc(ALLOCATION_QUANTUM, 1);

  /* Leave room at start of allocation for header info */
  byteCnt += HDR_ID_SIZE + HDR_SIZE_SIZE;
  mp = memBlk + byteCnt;
  
  for (i = 0; i < hdr->numFlds; i++) {
    if (hdr->flds[i]->name == NULL) {
      if (sprintf(fldBuf, ";%s\n", hdr->flds[i]->cmnt) < 0)
        FATAL_BRA_ "Memory write error" _KET;
    } else {
      switch (hdr->flds[i]->type) {
        case INT_TYPE:
          if (sprintf(fldBuf, "%s -i %d\n", hdr->flds[i]->name, *(Int *)hdr->flds[i]->data) < 0)
            FATAL_BRA_ "Memory write error" _KET;
          break;
        case REAL_TYPE:
          if (sprintf(fldBuf, "%s -r %f\n", hdr->flds[i]->name, *(Float *)hdr->flds[i]->data) < 0)
            FATAL_BRA_ "Memory write error" _KET;
          break;
        default:
          if (sprintf(fldBuf, "%s -s%d %s\n", hdr->flds[i]->name, hdr->flds[i]->length, (Char *)hdr->flds[i]->data) < 0)
            FATAL_BRA_ "Memory write error" _KET;
      }
    }

    /* Check allocation before writing token value */
    if (blksiz - byteCnt < strlen(fldBuf)) {
      blksiz += ALLOCATION_QUANTUM;
      if ((memBlk = (Ptr) realloc(memBlk, blksiz)) == NULL)
        FATAL_BRA_ "realloc (%d) failed", blksiz _KET;
      mp = memBlk + byteCnt;             /* Get to end of last write */
    }
    len = strlen(fldBuf);
    memcpy(mp, fldBuf, len);
    mp += len;
    byteCnt += len;
  }
  
  /* Check allocation before writing HDR_TERMINATOR */
  if (blksiz - byteCnt < strlen(HDR_TERMINATOR) + 1) {
    blksiz += ALLOCATION_QUANTUM;
    if ((memBlk = (Ptr) realloc(memBlk, blksiz)) == NULL)
      FATAL_BRA_ "realloc (%d) failed", blksiz _KET;
    mp = memBlk + byteCnt;             /* Get to end of last write */
  }

  if (sprintf(fldBuf, "%s\n", HDR_TERMINATOR) < 0)
    FATAL_BRA_ "Memory write error" _KET;
  len = strlen(fldBuf);
  memcpy(mp, fldBuf, len);
  byteCnt += len;
  mp += len;
  
  len = byteCnt % hdr->sizeMult;
  if (len > 0) {
    len = hdr->sizeMult - len;
    memset(mp, PAD_CHAR, len - 1);
    mp += len - 1;
    *mp = '\n';
    byteCnt += len;
  }
 *hdrBytes = byteCnt;

  /* Update the header */
  mp = memBlk;
  sprintf(fldBuf, "%*s\n%*d\n", HDR_ID_SIZE-1, NIST_LABEL, HDR_SIZE_SIZE-1, *hdrBytes);
  memcpy(mp, fldBuf, HDR_ID_SIZE + HDR_SIZE_SIZE);
  VBX_free(fldBuf);
  return memBlk;
}


void
HDR_free(HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free the space allocated for the header structure and all its fields.
 *---------------------------------------------------------------------------*/
{
  if (hdr) {
    HDR_clear(hdr);
    free(hdr);
  } else {
    WARN_BRA_ "NULL header pointer\n" _KET;
  }
}

void
HDR_copyData(FILE *fp, FILE *outfp)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Copy file 'fp' to file 'outfp'.
 *---------------------------------------------------------------------------*/
{
  Int c;
  
  if (fp == NULL || outfp == NULL) FATAL_BRA_ "Bad file pointer" _KET;
  while ((c = fgetc(fp)) != EOF)
    if (fputc(c,outfp) == EOF) FATAL_BRA_ "File write error" _KET;
  if (ferror(fp)) FATAL_BRA_ "File read error" _KET;
}

static Char *
HDR_createStr(Int size)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Allocate space for a string of size 'size', with room for a trailing '\0'.
 *---------------------------------------------------------------------------*/
{
  if (size > 0)
    return calloc(size+1, sizeof(Char));

  return NULL;
}

static void
HDR_freeFld(HDRF fld)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free the space allocated for the header field.
 *---------------------------------------------------------------------------*/
{
  if (fld != NULL) {
    if (fld->name != NULL) free(fld->name);
    if (fld->data != NULL) free(fld->data);
    if (fld->cmnt != NULL) free(fld->cmnt);
    free(fld);
  }
}

static Bool
HDR_parseBuf(HDR hdr, Ptr hdrBuf, Ptr endOfBuf)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Parse the fields in 'hdrBuf' and put into the speech header structure.
 *---------------------------------------------------------------------------*/
{
  size_t  remaining;
  Ptr     startOfLine;
  
  startOfLine = hdrBuf;
  while (startOfLine < endOfBuf) {
    remaining = endOfBuf - startOfLine;
    if (remaining < strlen(HDR_TERMINATOR)) {
      SEVERE_BRA_ "Bad header end" _KET;
      return FALSE;
    }
    if (*startOfLine == COMMENT_CHAR) {
      while (startOfLine < endOfBuf && *startOfLine != '\n') startOfLine++;
      if (startOfLine < endOfBuf) startOfLine++;
    } else {
      if (isalpha(*startOfLine)) {
        if (strncmp(startOfLine, HDR_TERMINATOR, strlen(HDR_TERMINATOR)) == 0 &&
            (remaining == strlen(HDR_TERMINATOR) || *(startOfLine+strlen(HDR_TERMINATOR)) == ' ' || *(startOfLine+strlen(HDR_TERMINATOR)) == '\n')) {
          return TRUE;
        } else {
          startOfLine = HDR_parseLine(hdr, startOfLine);
          if (NULL == startOfLine) return FALSE;
        }
      } else {
        SEVERE_BRA_ "Bad header line" _KET;
        return FALSE;
      }
    }
  }
  SEVERE_BRA_ "Bad header end" _KET;
  return FALSE;
}

static Ptr
HDR_parseLine(HDR hdr, Ptr startOfLine)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Parse a line starting at 'startOfLine' and put into a field in the speech
 *   header structure.
 * Returns a pointer to the beginning of the next line in the buffer.
 *---------------------------------------------------------------------------*/
{
  Int i;
  Ptr index;
  Char *Cmnt;
  Ptr spacePtr;
  Int intData;
  Float realData;
  Int type;
  void *data;
  Ptr endOfStr;
  Int strLen;
  
  index = startOfLine;
  spacePtr = strchr(index, ' ');
  if (spacePtr == NULL) {
    SEVERE_BRA_ "Bad header field name\n" _KET;
    return NULL;
  }
  while (isalnum(*index) || *index == '_') index++;
  if (index != spacePtr) {
    SEVERE_BRA_ "Bad header field name\n" _KET;
    return NULL;
  }
  *index = '\0';
  index++;
  if (*index != '-') {
    SEVERE_BRA_ "Bad header field descriptor\n" _KET;
    return NULL;
  }
  spacePtr = strchr(index, ' ');
  if (spacePtr == NULL) {
    SEVERE_BRA_ "Bad header field descriptor\n" _KET;
    return NULL;
  }
  index++;
  switch (*index) {
    case 'i':
      type = INT_TYPE;
      index++;
      if (index != spacePtr) {
        SEVERE_BRA_ "Bad header int type descriptor\n" _KET;
        return NULL;
      }
      index++;
      if (!isdigit(*index) && *index != '+' && *index != '-') {
        SEVERE_BRA_ "Bad header int data\n" _KET;
        return NULL;
      }
      intData = atoi(index);
      data = &intData;
      if (*index == '+' || *index == '-') index++;
      if (!isdigit(*index)) {
        SEVERE_BRA_ "Bad header int data\n" _KET;
        return NULL;
      }
      while (isdigit(*index)) index++;
      break;
    case 'r':
      type = REAL_TYPE;
      index++;
      if (index != spacePtr) {
        SEVERE_BRA_ "Bad header real type descriptor\n" _KET;
        return NULL;
      }
      index++;
      if (!isdigit(*index) && *index != '+' && *index != '-') {
        SEVERE_BRA_ "Bad header real data\n" _KET;
        return NULL;
      }
      realData = (Float) atof(index);
      data = &realData;
      if (*index == '+' || *index == '-') index++;
      if (!isdigit(*index)) {
        SEVERE_BRA_ "Bad header real data\n" _KET;
        return NULL;
      }
      while (isdigit(*index)) index++;
      if (*index != '.') {
        SEVERE_BRA_ "Bad header real data\n" _KET;
        return NULL;
      }
      index++;
      if (!isdigit(*index)) {
        SEVERE_BRA_ "Bad header real data\n" _KET;
        return NULL;
      }
      while (isdigit(*index)) index++;
      break;
    case 's':
      type = STR_TYPE;
      index++;
      if (!isdigit(*index)) {
        SEVERE_BRA_ "Bad header str length\n" _KET;
        return NULL;
      }
      strLen = atoi(index);
      while (isdigit(*index)) index++;
      if (index != spacePtr) {
        SEVERE_BRA_ "Bad header str length\n" _KET;
        return NULL;
      }
      index++;
      data = index;
      for (i = 0; i < strLen && *index != '\n' && *index != '\0'; i++)
        index++;
      if (i < strLen || *index == '\0') {
        SEVERE_BRA_ "Short header string\n" _KET;
        return NULL;
      }
      endOfStr = index;
      break;
    default:
      SEVERE_BRA_ "Bad data type character\n" _KET;
      return NULL;
  }

  while (*index == ' ') index++;
  if (*index != '\n' && *index != COMMENT_CHAR) {
    SEVERE_BRA_ "Bad end of header line\n" _KET;
    return NULL;
  }

  if (*index == COMMENT_CHAR) {
    index++;
    Cmnt = index;
    while (*index != '\n' && *index != '\0') index++;
    if (*index == '\0') {
      SEVERE_BRA_ "Bad end of header comment\n" _KET;
      return NULL;
    }
    *index = '\0';
  } else {
    Cmnt = NULL;
  }

  if (type == STR_TYPE) *endOfStr = '\0';

  if (!HDR_addFld(hdr, startOfLine, type, data, Cmnt))
    return NULL;

  index++;
  return(index);
}

static Bool
HDR_valid(Int type)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return TRUE if the type is a valid data type.  Otherwise return FALSE.
 *---------------------------------------------------------------------------*/
{
  if (type == INT_TYPE || type == REAL_TYPE || type == STR_TYPE) return TRUE;
  else return FALSE;
}
