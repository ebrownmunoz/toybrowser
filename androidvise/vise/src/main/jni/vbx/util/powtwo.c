/* powtwo.c
 *----------------------------------------------------------------------------*
 */

static char AtFSid[]= "$Header: powtwo.c[1.0] Fri Apr 11 00:04:00 1997 dvetter@titan saved $";
#include "pthr.h"

#include <stdlib.h>
#include "types.h"
#include "utils.h"
#include "vbx.h"


Int
nextPowerOfTwo(Int x)
/*----------------------------------------------------------------------------*
 * DESCRIPTION
 * For a given positive integer x, this function returns the least power of
 * two greater than or equal to x.
 */
{
  Int  power = 0;

  x--;
  while (x) {
    x >>= 1;
    power++;
  }
  return(1 << power);
}
