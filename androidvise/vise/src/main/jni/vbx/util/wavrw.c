/*
 *  wavrw.c:  sample data file i/o
 *  Verbex Voice Systems (jrt)
 */

static char AtFSid[]= "$__Header$";

#include "pthr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "types.h"
#include "file.h"
#include "sphdr.h"
#include "utils.h"
#include "vbx.h"
#include "ulaw.h"
#include "wavrw.h"


/* Note: SWAPW is for big-endian machines, SWAPX for little-endian */
#if (!defined(DEC) && !defined(LITENDIAN))
#define SWAPW(x)   *(x) = ((0xff & (*(x))>>8) | (0xff00 & (*(x))<<8))
#define SWAPX(x)
#else
#define SWAPW(x)
#define SWAPX(x)   *(x) = ((0xff & (*(x))>>8) | (0xff00 & (*(x))<<8))
#endif

#define BLKSIZ      80000
#define FILETYPE   "Sampled Data"
#define WAVRW_VERS "2.3"

static Bool WAV_exists(Char *fName);

Bool
WAV_read(Char * fname, HDR hdr, Short ** data, size_t * numShorts)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read the sampled data from a file.  If the file has a NIST speech header,
 * the header data will be put into hdr, otherwise hdr will be cleared.
 * Result shall always be a sequence of 16-bit linear samples appropriately
 * byte ordered.
 *---------------------------------------------------------------------------*/
{
  FILE   *fp;
  Char   *cp;
  UChar  *memBlk, *dp;
  Short  *sp;
  Int     sampleFmt = LINEAR, byteCnt = 2, *ip;
  Int     byteOrd = LITEND;  /* Default to little-endian (DEC and Intel) */
  Int     blksiz = BLKSIZ, nread, len, length;
  size_t  i;

  if (fname == NULL) FATAL_BRA_ "Bad file name" _KET;

  if (!WAV_exists(fname))
    return(FALSE);
  fp = VBX_fopen(fname, "rb");

  /* Get its length */
  if (fseek(fp, 0, SEEK_END)) FATAL_BRA_ "File seek error\n" _KET;
  if ((length = ftell(fp)) <= 0) FATAL_BRA_ "File length error\n" _KET;
  if (fseek(fp, 0, SEEK_SET)) FATAL_BRA_ "File seek error\n" _KET;

  if (HDR_isNIST(fp)) {
    if (hdr == NULL) hdr = HDR_create();
    else HDR_clear(hdr);
    HDR_read(fp, hdr);
  }

  /* Get the sample properties from the header */
  if (hdr != NULL) {
    /* Determine sample coding (ulaw or 16 bit linear only) */
    if (cp = HDR_data(hdr, "sample_coding", &len))
      sampleFmt = (strncmp(cp, ULAWSTR, len) == 0) ? ULAW : LINEAR;
    /* Determine sample size (1 or 2 bytes only */
    if (ip = HDR_data(hdr, "sample_sig_bits", &len))
      byteCnt = (*ip == 8) ? 1 : 2;
    /* Determine byte order (little- or big-endian) */
    if (cp = HDR_data(hdr, "sample_byte_format", &len))
      byteOrd = (strncmp(cp, "01", len) == 0) ? LITEND : BIGEND;
  }

  memBlk = (UChar *) VBX_calloc(length, 1);
  *numShorts = fread(memBlk, byteCnt, length/byteCnt, fp);

  switch (sampleFmt) {
  case ULAW:
   *data = (Short *) VBX_calloc(*numShorts, sizeof(Short));
    for (i = 0; i < *numShorts; i++) {
      (*data)[i] = mulaw[(UInt)memBlk[i]];
	 }
    free(memBlk);
    break;
  case LINEAR:
  default:
    switch (byteCnt) {
    case 1:
      FATAL_BRA_ "8 bit linear format not supported yet" _KET;
      break;
    case 2:
      sp = *data = (Short *) memBlk;
      if (byteOrd == LITEND) {
        for (i = 0; i < *numShorts; i++, sp++) SWAPW(sp); 
      } else {
        for (i = 0; i < *numShorts; i++, sp++) SWAPX(sp); 
      }
      break;
    }
  }
  if (fclose(fp)) FATAL_BRA_ "fclose(%s) failed", fname _KET;
  return(TRUE);
}  

Bool
WAV_write(Char * fname, HDR hdr, Short * data, size_t numShorts)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write sampled data to a file.  A NIST speech header will be put at the
 * beginning of the file.  Header fields for "wavrw_version" and "file_type"
 * will be added to hdr (or updated if such fields already exist).
 *---------------------------------------------------------------------------*/
{
  FILE * fp;
  size_t i;
  
  if (fname == NULL) FATAL_BRA_ "Bad file name" _KET;
  fp = VBX_fopen(fname, "wb");
  if (hdr != NULL) {
    WAV_addDfltHdrFields(hdr);
    HDR_write(fp, hdr);
    HDR_delFld(hdr, "wavrw_version");
  }
  for (i=0; i < numShorts; i++)
    SWAPW(&data[i]);
  if (fwrite(data, sizeof(Short), numShorts, fp) != numShorts)
    FATAL_BRA_ "fwrite(%s) failed", fname _KET;
  if (fclose(fp)) FATAL_BRA_ "fclose(%s) failed", fname _KET;
  return(TRUE);
}

Bool
WAV_wrtFrame(FILE * fp, Short * data, size_t numShorts)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write a sample frame to the specified stream.
 *---------------------------------------------------------------------------*/
{
  size_t  i;

  for (i=0; i < numShorts; i++) 
    SWAPW(&data[i]);
  if (fwrite(data, sizeof(Short), numShorts, fp) != numShorts)
    FATAL_BRA_ "fwrite failed" _KET;
  return(TRUE);
}  

Int
WAV_peekLen(Char * fname, HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the number of elements of sampled data from the file.  If the file has
 * a NIST speech header, the header data will be put into hdr, otherwise hdr
 * will be cleared.
 *---------------------------------------------------------------------------*/
{
  FILE * fp;
  size_t numShorts = 0;
  Short  data;

  if (fname == NULL) FATAL_BRA_ "Bad file name" _KET;
  fp = VBX_fopen(fname, "rb");
  if (HDR_isNIST(fp)) {
    if (hdr == NULL) hdr = HDR_create();
    else HDR_clear(hdr);
    HDR_read(fp, hdr);
  }
  while(fread(&data, sizeof(Short), 1, fp)) numShorts++;
  if (fclose(fp)) FATAL_BRA_ "fclose(%s) failed", fname _KET;
  return(numShorts);
}

static Bool
WAV_exists(Char * fName)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Check if the given file exists.
 *---------------------------------------------------------------------------*/
{
  Char *fileName;
#ifdef vms
  Char vmsName[256];
  Char *fnPtr, *fnPtr2;
  
  fnPtr = fName;
  sprintf(vmsName, "%s", "dka0:[");
  if (*fnPtr == '/') {
    fnPtr++;
    if ((fnPtr2 = strchr(fnPtr, '/')) == NULL) strcat(vmsName, "000000]");
  } else if (strncmp(fnPtr, "../", 3)) {
    fnPtr += 3;
    if ((fnPtr2 = strchr(fnPtr, '/')) == NULL) strcat(vmsName, "-]");
    else strcat(vmsName, "-.");
  } else {
    if (strncmp(fnPtr, "./", 2)) fnPtr += 2;
    if ((fnPtr2 = strchr(fnPtr, '/')) == NULL) strcat(vmsName, "]");
    else strcat(vmsName, ".");
  }
  while (fnPtr2 != NULL) {
    strncat(vmsName, fnPtr, fnPtr2 - fnPtr);
    fnPtr = fnPtr2 + 1;
    if ((fnPtr2 = strchr(fnPtr, '/')) == NULL) strcat(vmsName, "]");
    else strcat(vmsName, ".");
  }
  strcat(vmsName, fnPtr);
  fileName = vmsName;
#else
  fileName = fName;
#endif

  return FILE_exists(fileName);
}

void
WAV_addDfltHdrFields(HDR hdr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write the NIST file fields necessary to describe our standard 16-bit linear
 * little endian sample format
 *---------------------------------------------------------------------------*/
{
  Int   value;

  HDR_delFld(hdr, "channel_count");
  HDR_delFld(hdr, "sample_byte_format");
  HDR_delFld(hdr, "sample_n_bytes");
  HDR_delFld(hdr, "sample_sig_bits");
  HDR_delFld(hdr, "sample_coding");
  HDR_delFld(hdr, "file_type");
  HDR_delFld(hdr, "wavrw_version");
  value = CH_CNT;
  HDR_addFld(hdr, "channel_count", INT_TYPE, &value, NULL);
  HDR_addFld(hdr, "sample_byte_format", STR_TYPE, SMPL_BYTE_FMT, NULL);
  value = SMPL_SIG_BITS;
  HDR_addFld(hdr, "sample_sig_bits", INT_TYPE, &value, NULL);
  HDR_addFld(hdr, "file_type", STR_TYPE, FILETYPE, NULL);
  HDR_addFld(hdr, "wavrw_version", STR_TYPE, WAVRW_VERS, NULL);
}
