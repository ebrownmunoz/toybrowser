LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := vbxutil
# google recommends convenience variables to be prefixed with "MY_"
LOCAL_SRC_FILES := file.c hash.c list.c llist.c powtwo.c primes.c sphdr.c ukey.c urlcodec.c wavrw.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include

include $(BUILD_STATIC_LIBRARY)
