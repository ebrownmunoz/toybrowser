/* primes.c
 *----------------------------------------------------------------------------*
 */

static char AtFSid[]= "$Header: primes.c[1.0] Fri Apr 11 00:04:03 1997 dvetter@titan saved $";
#include "pthr.h"

#include <stdlib.h>
#include <math.h>
#include "types.h"
#include "vbx.h"
#include "primes.h"

static Bool * sieve(UInt max);

static Bool *
sieve(UInt n)
/*----------------------------------------------------------------------------*
 * DESCRIPTION
 * For a given natural number n, this function returns an array of n
 * Bools in which an element is TRUE iff its array index is prime.
 *----------------------------------------------------------------------------*/
{
  UInt    i, j;
  UInt    maxFactor;
  Bool  * a;          /* a[i] = FALSE if i is composite, TRUE if i is prime. */

  maxFactor = (UInt) sqrt((Double) n);

  a = (Bool *) VBX_calloc(n + 1, sizeof(Bool));
  for (i = 1; i <= n; a[i++] = TRUE);
  for (i = 2; i <= maxFactor; i++)
    if (a[i])
      for (j = 2 * i; j <= n; j += i)
        a[j] = FALSE;

  return(a);
}


UInt *
PRIMES_toN(UInt n, UInt * numPrimes)
/*----------------------------------------------------------------------------*
 * DESCRIPTION
 * For a given natural number n, this function returns the array of all primes
 * less than or equal to n, and reports its size.
 *----------------------------------------------------------------------------*/
{
  UInt    i, j;
  Bool  * a;         /* a[i] = FALSE if i is composite, TRUE if i is prime. */
  UInt  * primes;

  if (n < 1) n = 1;

  a = sieve(n);

  /* Count the primes and allocate an array for them */
  for (i = 1, j = 0; i <= n; i++) if (a[i]) j++;
  *numPrimes = j;
  primes = (UInt *) VBX_calloc(j, sizeof(UInt));

  /* Fill the array */
  for (i = 1, j = 0; i <= n; i++) if (a[i]) primes[j++] = i;

  free(a);
  return(primes);
}


UInt
PRIMES_next(UInt n)
/*----------------------------------------------------------------------------*
 * DESCRIPTION
 * For a given natural number n, this function returns the least prime
 * number greater than or equal to n. (Used for the hash table size).
 *----------------------------------------------------------------------------*/
{
  UInt    i;
  UInt    maxResult;
  Bool  * a;         /* a[i] = FALSE if i is composite, TRUE if i is prime. */

  if (n < 1) n = 1;
  maxResult = 2 * n;

  a = sieve(maxResult);

  for (i = n; !a[i]; i++);

  free(a);
  return(i);
}


UInt *
PRIMES_firstN(UInt n, UInt * numPrimes)
/*----------------------------------------------------------------------------*
 * DESCRIPTION
 * For a given natural number n, this function returns an array of
 * at least the first n primes, and reports its size.
 *----------------------------------------------------------------------------*/
{
  UInt    sieveSize = (UInt) ((log((Double) (n + 3)) + 1.0) * ((Double) (n + 3)));
  UInt  * primes = PRIMES_toN(sieveSize, numPrimes);

  while (*numPrimes <= n) {
    sieveSize *= 1.1;
    VBX_free(primes);
    primes = PRIMES_toN(sieveSize, numPrimes);
  }

  return primes;
}


UInt
PRIMES_nth(UInt n)
/*----------------------------------------------------------------------------*
 * DESCRIPTION
 * For a given natural number n, this function returns the nth prime, where
 * 1 is the zero-th prime
 *----------------------------------------------------------------------------*/
{
  UInt    numPrimes;
  UInt    prime;
  UInt  * primes = PRIMES_firstN(n, &numPrimes);

  prime = primes[n];
  VBX_free(primes);

  return prime;
}
