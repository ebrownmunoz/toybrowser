/* list.c - creation and maintenance of lists
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * HISTORY
 * 25-Mar-93 Dave Vetter (dvetter@rad.verbex.com)
 *      ANSIfied and extracted prototypes
 * 14-Oct-92 Eric Thayer (eht+@cmu.edu) Carnegie Mellon University
 *      Added formal declaration for sym in LIST_append().
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "types.h"
#include "list.h"
#include "vbx.h"

#define LIST_GROWTH_QUANTUM 64

LIST
LIST_new(void)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Currently all we do is VBX_calloc
 *---------------------------------------------------------------------------*/
{
  return ((LIST) VBX_calloc(1, sizeof(list_t)));
}


void
LIST_insert(LIST list, Ptr sym, Int idx)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  Int  oldSize;

  assert(list);

   /* Make sure the new entry fits in the List */
  oldSize = list->size;
  if (idx >= list->size) {
    list->size = idx + LIST_GROWTH_QUANTUM;
    list->list = (Ptr *) VBX_realloc((Char *) list->list, sizeof(Ptr) * list->size);
     /* Zero the expansion area */
    memset(&list->list[oldSize], 0, (list->size - oldSize) * sizeof(Ptr));
  }
  
   /* Every entry before the last is considered to be in use */
  if (idx >= list->inuse)
    list->inuse = idx + 1;

   /* Put 'sym' into the list */
  list->list[idx] = sym;
}


Ptr
LIST_lookup(LIST list, Int idx)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  assert((list != NULL) && (idx < list->size) && (idx >= 0));
  return (list->list[idx]);
}


void
LIST_append(LIST list, Ptr sym)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add sym to list at the inuse position and increment inuse.
 *---------------------------------------------------------------------------*/
{
  LIST_insert(list, sym, list->inuse);
}


void
LIST_free(LIST list)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free the list list.
 * NB: This routine does not free the list descriptor itself.
 *---------------------------------------------------------------------------*/
{
  assert(list);
  if (list->list) {
    free(list->list);
    list->list = NULL;
  }
  list->size = 0;
  list->inuse = 0;
}


Int
LIST_index(LIST list, Ptr sym)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  Int             i;

  assert(list);
  for (i = 0; i < list->inuse; i++)
    if (sym == list->list[i])
      return (i);
  FATAL_BRA_ "LIST_index() failed on %d", sym _KET;
}


void
LIST_processElements(LIST list, void (* fcn)(Ptr elements))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Apply the specified fcn to all the elements in the list.  
 * If fcn() is NULL, assumes that the non-null elements are
 * pointers, and frees the memory that the elements reference.
 *---------------------------------------------------------------------------*/
{
  Int  idx;

  if (list)
    for (idx = 0; idx < list->inuse; idx++) {
      if (fcn) fcn(list->list[idx]);
      else if (list->list[idx]) free(list->list[idx]);
    }
}


Ptr        *
LIST_array(LIST list)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  assert(list);
  return (list->list);
}


Int
LIST_length(LIST list)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  assert(list);
  return (list->inuse);
}


void
LIST_write(FILE * fs, LIST list)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  assert(list);
  fwrite(&list->inuse, sizeof(Int), 1, fs);
  fwrite(list->list, sizeof(Ptr), list->inuse, fs);
}


void
LIST_read(FILE * fs, LIST list)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  assert(list);
  fread(&list->inuse, sizeof(Int), 1, fs);
  list->list = (Ptr *) VBX_calloc(list->inuse, sizeof(Ptr));
  list->size = list->inuse;
  fread(list->list, sizeof(Ptr), list->inuse, fs);
}

#ifdef DEBUG

void
LIST_dump(LIST list)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  Int             i;

  assert(list);
  VBX_print("LIST_dump() of 0x%08x; %d entries, %d in use\n", list, list->size, list->inuse);
  VBX_print("       idx  object\n");
  for (i = 0; i < list->inuse; i++)
    VBX_print("%10d  0x%08x\n", i, list->list[i]);
}

#endif
