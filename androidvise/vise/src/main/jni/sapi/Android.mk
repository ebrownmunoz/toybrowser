LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := vbxaudio
LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/vbxaudio/*.c) $(wildcard $(LOCAL_PATH)/vbxaudio/*.cc)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/../vbx/include $(LOCAL_PATH)/../fe/include $(LOCAL_PATH)/../ms/include

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := sapivise
LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/vise/*.c) $(wildcard $(LOCAL_PATH)/vise/*.cc)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/../vbx/include $(LOCAL_PATH)/../fe/include $(LOCAL_PATH)/../vise2/include $(LOCAL_PATH)/../ms/include
LOCAL_STATIC_LIBRARIES := fe vbxaudio vbxsys vbxutil vulcan vvisese

include $(BUILD_STATIC_LIBRARY)
