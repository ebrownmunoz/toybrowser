#ifndef _VBXENGINE_HH_
#define _VBXENGINE_HH_

#include "vbxcom.hh"

// Parameter indices
#define VISE_NHYPOTHESES  100
#define VISE_ABSWILDCARD  101
#define VISE_RELWILDCARD  102
#define VISE_FEGAINSHIFT  103
#define VISE_JINCLASSIZE  104
#define VISE_QUEUELENGTH  105
#define VISE_QUEUEFWDTOL  106
#define VISE_QUEUEBWDTOL  107
#define VISE_FRBUFLENGTH  108
#define VISE_BOUPADLNGTH  109
#define VISE_EOUPADLNGTH  110
#define VISE_FRAMEIDLEMS  111
#define VISE_PRINTFRAMES  112
#define VISE_PRINTSTATUS  113

#define VISE_SPEAKERPATH  200

// IVbxEngine
#undef  INTERFACE
#define INTERFACE   IVbxEngine

//!! FIX THIS - it's fudged
DEFINE_GUID(IID_IVbxEngine, 38, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

DECLARE_INTERFACE_(IVbxEngine, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)  (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)   (THIS) PURE;
  STDMETHOD_(ULONG, Release)  (THIS) PURE;

  // IVbxEngine members
  STDMETHOD (SetParam)        (THIS_ DWORD param, INT value) PURE;
  STDMETHOD (GetParam)        (THIS_ DWORD param, PINT pValue) PURE;
  STDMETHOD (SetParam)        (THIS_ DWORD param, PCSTR value) PURE;
  STDMETHOD (GetParam)        (THIS_ DWORD param, PSTR value) PURE;
};

typedef IVbxEngine *PIVbxEngine;

#endif /* _VBXENGINE_HH_ */

