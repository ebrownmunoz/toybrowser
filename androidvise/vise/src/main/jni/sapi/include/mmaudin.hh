/* mmaudin.hh - IAudioIn methods built on an MMAudio source
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Component methods for building an IAudioIn to obtain data from an OLE
 * MMAudio source.
 *
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _MMAUDIN_HH_
#define _MMAUDIN_HH_

#include "vbxcom.hh"
#include "mmaudin.h"

/* MMAUDIN Descriptor */
typedef struct mmaudin_s * MMAUDIN;

/* MMAUDIN Methods */
extern MMAUDIN  MMAUDIN_create(LPUNKNOWN audioUnknown);
extern void     MMAUDIN_destroy(MMAUDIN mmaudin);

#endif /* _MMAUDIN_HH_ */
