#ifndef _VISEFACTORY_HH_
#define _VISEACTORY_HH_

#include "vbxcom.hh"

class ViseClassFactory : public IClassFactory {
  public:
    ViseClassFactory();
   ~ViseClassFactory();

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IClassFactory members
    STDMETHODIMP              CreateInstance(LPUNKNOWN, REFIID, LPVOID *);
    STDMETHODIMP              LockServer(BOOL);
}

typedef ViseClassFactory *PViseClassFactory;

#endif  /* _VISEFACTORY_HH_ */
