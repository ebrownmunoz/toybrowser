#ifndef _VISEKB_HH_
#define _VISEKB_HH_

#include "types.h"
#include "ekb.h"
#include "kb.h"
#include "vfile.h"
#include "critsect.h"
#include "ukey.h"
#include "file.h"
#include "hash.h"
#include "ifaces.h"
#include "speaker.h"

// COM infrastructure
#include "vbxcom.hh"

class VISESpkrDirObj;
typedef VISESpkrDirObj * PVISESpkrDirObj;


  // Generic VISE Stream

class VISEStreamObj {
  public:
    VISEStreamObj(void);
    virtual ~VISEStreamObj(void);

    STDMETHODIMP_(ULONG)  AddRef(void);
    STDMETHODIMP_(ULONG)  Release(void);

    IFile               * File(void);
    VFILE                 Stream(void);

  protected:
    ULONG                 RefCnt;
    IMemory               Allocator;
    IFile               * IFilePtr;
    VFILE                 StreamPtr;

    CRITSECT_DECLARE(RefCntCritSect)
};

typedef VISEStreamObj * PVISEStreamObj;


  // VISE RecStream (Grammar)

class VISERecStreamObj : public VISEStreamObj {
  public:
    VISERecStreamObj(void);
    VISERecStreamObj(SDATA);
   ~VISERecStreamObj(void);

    Bool                  Init(EKB ekb);
    KB                    Kb(void);

    IFile               * RecFile(void);
    SDATA                 Grammar;

  private:
    KB                    KbPtr;
};

typedef VISERecStreamObj * PVISERecStreamObj;


  // VISE VoiStream (Speaker Profile)

class VISEVoiStreamObj : public VISEStreamObj {
  public:
    VISEVoiStreamObj(Char * name, PVISESpkrDirObj dirPtr);
   ~VISEVoiStreamObj(void);

    PVISESpkrDirObj       Directory();
    void                  Update(void);
    SPKR                  Speaker(void);
    virtual Bool          LoadModels(Char * fileName, EKB ekb) = 0;
    virtual void          UnloadModels(void) = 0;
    virtual Bool          SaveModels(UInt vocabId, UKEY key) = 0;

    Bool                  ReplaceContents;
    Bool                  RestoreContents;
    Bool                  RemoveContents;
    UKEYREG               Registry;
    SPKR                  SpkrProfile;
    Char                * SpkrName;

  protected:
    PVISESpkrDirObj       SpkrDirObjPtr;

  
};

typedef VISEVoiStreamObj * PVISEVoiStreamObj;

 // The VISEVoiStreamObj Constructor
extern PVISEVoiStreamObj  VISE_createVoiStreamObj(Char * name, PVISESpkrDirObj dirPtr);


  // VISE KB Object (Reconciled Grammar/Speaker Pair)

class VISEKbObj {
  public:
    VISEKbObj(EKB, PVISERecStreamObj, PVISEVoiStreamObj, Char *, UKEYPOOL);
   ~VISEKbObj(void);

    STDMETHODIMP_(ULONG)  AddRef(void);
    STDMETHODIMP_(ULONG)  Release(void);

    KB                    Kb(void);
    PVISERecStreamObj     RecStream(void);
    PVISEVoiStreamObj     VoiStream(void);
    Char                * SpkrName(void);
    Bool                  IsMadeOf(PVISERecStreamObj, PVISEVoiStreamObj, Char *);
    Bool                  SaveModels(UInt vocabId);
    Bool                  Update(void);

  protected:
    ULONG                 RefCnt;

  private:
    IMemory               Allocator;
    PVISERecStreamObj     RecStreamPtr;
    PVISEVoiStreamObj     VoiStreamPtr;
    Char                * SpkrNamePtr;
    UKEY                  Key;

    CRITSECT_DECLARE(RefCntCritSect)
};

typedef VISEKbObj * PVISEKbObj;


  // VISE Directory Object

class VISESpkrDirObj {
  public:
    VISESpkrDirObj(const Char * dirName, const Char * extension, Char backupTag, Char temporaryTag, EKB ekb);
   ~VISESpkrDirObj(void);

    STDMETHODIMP_(ULONG)  AddRef(void);
    STDMETHODIMP_(ULONG)  Release(void);

    Char                * Names(UInt * numNamesPtr, UInt * listLengthPtr);
    Char                * FilePathName(const Char * name);
    EKB                   EngineKB(void);
    PVISEVoiStreamObj     CreateVoiStream(Char * name);
    PVISEVoiStreamObj     Stream(Char * name);
    Bool                  RemoveStream(Char * name);
    Bool                  HasAnEntry(Char * name);
    Bool                  RemoveEntry(Char * name);
    Bool                  RestoreEntry(Char * name);
    Char                * DirPath(void);
    DIRLIST               DirList(Char * tmplt);

    virtual Ptr           ReadEntry(Char * name, UInt * dataSizePtr) = 0;
    virtual Bool          WriteEntry(Char * name, Ptr data, UInt dataSize) = 0;
    virtual Bool          ReplaceContents(Char * name) = 0;
    virtual Bool          RemoveContents(Char * name) = 0;
    virtual Bool          RestoreContents(Char * name) = 0;
    virtual Bool          CloneEntry(const Char * sourceName, const Char * cloneName) = 0;

  protected:
    ULONG                 RefCnt;
    IMemory               Allocator;
    Char                * TaggedPathName(Char * name, Char tag);
    virtual Bool          BackupContents(Char * name) = 0;
    Char                * Path;
    Char                * Extension;
    Char                  BackupTag;
    Char                  TemporaryTag;
    Char                * Template;
    HASH                  Streams;
    EKB                   EKbPtr;

    CRITSECT_DECLARE(RefCntCritSect)
};

typedef VISESpkrDirObj  * PVISESpkrDirObj;

 // The VISESpkrDirObj Constructor
extern PVISESpkrDirObj  VISE_createSpkrDirObj(Char * dirName, EKB ekb);

#endif /* _VISEKB_HH_ */
    
