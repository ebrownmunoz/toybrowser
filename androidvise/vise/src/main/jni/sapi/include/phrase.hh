#ifndef _PHRASE_HH_
#define _PHRASE_HH_

#include "vbxcom.hh"

class SAPIPhrase {
  public:
    SAPIPhrase();
    SAPIPhrase(PSRPHRASE);
   ~SAPIPhrase(void);

    PSRPHRASE       SRPhrase();
    PSRWORD         FirstSRWord();
    PSRWORD         NextSRWord();
    void            AppendSRWord(PSRWORD);
    void            AppendWordString(Char *, Int);
    unsigned int    SRPhraseSize();
    unsigned int    NumWords();
    char          * Text();
    void            Print();

  protected:
    unsigned int    CMNumWords;
    PSRPHRASE       SRPhrasePtr;
    char          * NextSRWordPtr;  
};

typedef SAPIPhrase  * PSAPIPhrase;

#endif  /* _PHRASE_HH_ */
