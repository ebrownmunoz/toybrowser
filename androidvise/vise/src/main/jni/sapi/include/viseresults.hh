#ifndef _VISERES_HH_
#define _VISERES_HH_

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ifaces.h"
#include "mbox.h"
#include "class.h"
#include "sn.h"

// VULCAN Inclusions
#include "hyp.h"

// COM Infrastructure
#include "vbxcom.hh"

class VISEEngineObj;
typedef VISEEngineObj         * PVISEEngineObj;
class VISEGrammarObj;
typedef VISEGrammarObj        * PVISEGrammarObj;
class VISEKbObj;
typedef VISEKbObj             * PVISEKbObj;


// ISRResAudio Interface

class VISEISRResAudio : ISRResAudio {
  public:
    VISEISRResAudio(LPVOID, LPUNKNOWN);
   ~VISEISRResAudio(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResAudio
    STDMETHODIMP              GetWAV(PSDATA);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown
};

typedef VISEISRResAudio * PVISEISRResAudio;


// ISRResAudioEx Interface

class VISEISRResAudioEx : ISRResAudioEx {
  public:
    VISEISRResAudioEx(LPVOID, LPUNKNOWN);
   ~VISEISRResAudioEx(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResAudio
    STDMETHODIMP              GetWAV(PSDATA, QWORD, QWORD);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown
};

typedef VISEISRResAudioEx * PVISEISRResAudioEx;

// ISRResBasic Interface

class VISEISRResBasic : ISRResBasic {
  public:
    VISEISRResBasic(LPVOID, LPUNKNOWN);
   ~VISEISRResBasic(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResBasic
    STDMETHODIMP              PhraseGet(DWORD, PSRPHRASE, DWORD,  DWORD *);
    STDMETHODIMP              Identify(GUID *);
    STDMETHODIMP              TimeGet(PQWORD, PQWORD);
    STDMETHODIMP              FlagsGet(DWORD, DWORD *);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown
};

typedef VISEISRResBasic * PVISEISRResBasic;

// ISRResCorrection Interface

class VISEISRResCorrection : ISRResCorrection {
  public:
    VISEISRResCorrection(LPVOID, LPUNKNOWN);
   ~VISEISRResCorrection();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResCorrection
    STDMETHODIMP              Correction(PSRPHRASE, WORD);
    STDMETHODIMP              Validate(WORD);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown    
};

typedef VISEISRResCorrection * PVISEISRResCorrection;

// ISRResGraph Interface

class VISEISRResGraph : ISRResGraph {
  public:
    VISEISRResGraph(LPVOID, LPUNKNOWN);
   ~VISEISRResGraph();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResGraph
    STDMETHODIMP              BestPathPhoneme(DWORD, DWORD *, DWORD, DWORD *);
    STDMETHODIMP              BestPathWord(DWORD, DWORD *, DWORD, DWORD *);
    STDMETHODIMP              GetPhonemeNode(DWORD, PSRRESPHONEMENODE, PWCHAR, PCHAR);
    STDMETHODIMP              GetWordNode(DWORD, PSRRESWORDNODE, PSRWORD, DWORD, DWORD *);
    STDMETHODIMP              PathScorePhoneme(DWORD *, DWORD, LONG *);
    STDMETHODIMP              PathScoreWord(DWORD *, DWORD, LONG *);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown    
};

typedef VISEISRResGraph * PVISEISRResGraph;

// ISRResMemory Interface

class VISEISRResMemory : ISRResMemory {
  public:
    VISEISRResMemory(LPVOID, LPUNKNOWN);
   ~VISEISRResMemory();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResMemory
    STDMETHODIMP              Free(DWORD);
    STDMETHODIMP              Get(DWORD *, DWORD *);
    STDMETHODIMP              LockGet(BOOL *);
    STDMETHODIMP              LockSet(BOOL);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown    
};

typedef VISEISRResMemory * PVISEISRResMemory;

// ISRResNotifySink Interface

class VISEISRResNotifySink : public ISRGramNotifySink {
  public:
    VISEISRResNotifySink(LPVOID, LPUNKNOWN);
   ~VISEISRResNotifySink();

    // Delegate to the container Unknown, if any
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResNotifySink
    STDMETHODIMP              BookMark(DWORD);
    STDMETHODIMP              Paused();
    STDMETHODIMP              PhraseFinish(DWORD, QWORD, QWORD, PSRPHRASE, LPUNKNOWN);
    STDMETHODIMP              PhraseHypothesis(DWORD, QWORD, QWORD, PSRPHRASE, LPUNKNOWN);
    STDMETHODIMP              PhraseStart(QWORD);
    STDMETHODIMP              ReEvaluate(LPUNKNOWN);
    STDMETHODIMP              Training(DWORD);
    STDMETHODIMP              UnArchive(LPUNKNOWN);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown    
};

typedef VISEISRResNotifySink * PVISEISRResNotifySink;


// ISRResTranslate Interface

class VISEISRResTranslate : ISRResTranslate {
  public:
    VISEISRResTranslate(LPVOID, LPUNKNOWN);
   ~VISEISRResTranslate();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRResTranslate
    STDMETHODIMP              Translate(DWORD, DWORD, PSTR, DWORD, DWORD *);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown    
};

typedef VISEISRResTranslate * PVISEISRResTranslate;


// Definition of the VISE Results SAPI Object

class VISEResultsObj : public IUnknown {
  public:
    VISEResultsObj(IMemory *, PVISEGrammarObj, PVISEKbObj, UInt, PVISEEngineObj);
   ~VISEResultsObj();

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    PVISEGrammarObj           Grammar(void);
    PVISEKbObj                Kb(void);
    PISRGRAMNOTIFYSINK        NotifySink(void);
    Char                    * RuleName(void);
    void                      SetRuleName(Char *);
    PVISEEngineObj            EngineObject(void);
    Ptr                       AcousticFeatures(SN * first, SN * last, UInt * numFrames);
    void                      SetAcousticFeatures(Ptr frames, SN first, SN last, UInt numFrames);
    Ptr                       AudioData(SN * first, SN * last, UInt * sampCnt);
    void                      SetAudioData(OBJT audioData, SN first, SN last, UInt sampCnt);
    HRESULT                   CreateWAV(PSDATA pWav, SN first, SN last);
    Int                       AudioSampRate(void);
    void                      AudioSampRate(Int);
    Float                     AGCsub(void);
    void                      SetAGCsub(Float);
    HYP                       Hypotheses(void);
    void                      SetHypotheses(HYP);
    UInt                      NBest(void);
    PSRPHRASE                 Transcription(void);
    void                      Transcription(PSRPHRASE);

  protected:
    ULONG                     RefCnt;
    LPUNKNOWN                 UnknwnPtr;

    PVISEISRResAudio          VISEISRResAudioPtr;
    PVISEISRResAudioEx        VISEISRResAudioExPtr;
    PVISEISRResBasic          VISEISRResBasicPtr;
    PVISEISRResCorrection     VISEISRResCorrectionPtr;
    PVISEISRResGraph          VISEISRResGraphPtr;
    PVISEISRResMemory         VISEISRResMemoryPtr;
    PVISEISRResTranslate      VISEISRResTranslatePtr;

    UInt                      GrammarId;               // "Rule"
    UInt                      VocabId;                 // "Grammar"

    // Results Object Internals
  private:
    Bool                      Built;
    void                      Build();
    IMemory                 * Allocator;
    PVISEEngineObj            EngineObjPtr;

    PVISEGrammarObj           GrammarPtr;              // The "parent" grammar object
    PISRGRAMNOTIFYSINK        NotifySinkPtr;
    PVISEKbObj                KbPtr;                   // The reconciled knowledge base
    Char                    * CMRuleName;

    PSRPHRASE                 CMTranscription;         // Verified result
    HYP                       CMHypotheses;
    UInt                      CMNBest;

    Ptr                       FeatureFrames;
    SN                        FirstFeatSN;
    SN                        LastFeatSN;
    UInt                      FeatFrameCnt;

    OBJT                      CMAudioData;
    Int                       CMAudioSampRate;
    SN                        FirstAudioSN;
    SN                        LastAudioSN;
    UInt                      AudioSampCnt;

    Float                     AgcSub;

    CRITSECT_DECLARE(RefCntCritSect)

  // Interfaces are friends
  friend class VISEISRResAudio;
  friend class VISEISRResAudioEx;
  friend class VISEISRResBasic;
  friend class VISEISRResCorrection;
  friend class VISEISRResGraph;
  friend class VISEISRResMemory;
  friend class VISEISRResTranslate;
};

typedef VISEResultsObj * PVISEResultsObj;

#endif  /* _VISERES_HH_ */
