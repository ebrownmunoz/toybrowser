#ifndef _VBXENUM_HH_
#define _VBXENUM_HH_

#include "vbxcom.hh"
#include "vbxsapi.hh"

// ISRFind interface

class VbxISRFind : public ISRFind {
  public:
    VbxISRFind(LPVOID, LPUNKNOWN);
   ~VbxISRFind(void);

    // These IUnknown members delegate to outer Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRFind
    STDMETHODIMP              Find(PSRMODEINFO, PSRMODEINFORANK, PSRMODEINFO);
    STDMETHODIMP              Select(GUID, PISRCENTRAL *, LPUNKNOWN);

  private:
    ULONG     RefCnt;         // interface reference count
    LPVOID    ObjPtr;         // back pointer to the containing object
    LPUNKNOWN UnknwnPtr;      // pointer to outer Unknown
};

typedef VbxISRFind *PVbxISRFind;

// ISREnum interface

class VbxISREnum : public ISREnumA, public ISREnumW {
  public:
    VbxISREnum(LPVOID, LPUNKNOWN);
   ~VbxISREnum(void);

    // These IUnknown members delegate to outer Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISREnum
    STDMETHODIMP              Next(ULONG, PSRMODEINFOA, ULONG *);
    STDMETHODIMP              Next(ULONG, PSRMODEINFOW, ULONG *);
    STDMETHODIMP              Skip(ULONG);
    STDMETHODIMP              Reset(void);
    STDMETHODIMP              Clone(PISRENUMA *);
    STDMETHODIMP              Clone(PISRENUMW *);
    STDMETHODIMP              Select(GUID, PISRCENTRALA *, LPUNKNOWN);
    STDMETHODIMP              Select(GUID, PISRCENTRALW *, LPUNKNOWN);

  private:
    ULONG     RefCnt;         // interface reference count
    LPVOID    ObjPtr;         // back pointer to the containing object
    LPUNKNOWN UnknwnPtr;      // pointer to outer Unknown
};

class   VISEObj;
typedef VISEObj    *PVISEObj;
typedef VbxISREnum *PVbxISREnum;



class VbxEnum : public IUnknown {
  public:
    VbxEnum(LPUNKNOWN);
    VbxEnum(VbxEnum *);
   ~VbxEnum(void);

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // Non-SAPI Methods
    Char                     *CfgFilePath(void);
  protected:
    ULONG                     RefCnt;
    LPUNKNOWN                 UnknwnPtr;
    PVbxISREnum               VbxISREnumPtr;
    PVbxISRFind               VbxISRFindPtr;
    PVISEObj                  VISEObjPtr;
    Int                       ArrayPosn;
    Char                      CMCfgFilePath[256];

  // Interfaces are friends
  friend class VbxISREnum;
  friend class VbxISRFind;
};

typedef VbxEnum *PVbxEnum;

#endif /* _VBXENUM_HH_ */


