#ifndef _VISETRAIN_HH_
#define _VISETRAIN_HH_

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "critsect.h"
#include "ifaces.h"
#include "imemory.h"
#include "mbox.h"
#include "sn.h"

// VULCAN Inclusions
#include "hyp.h"

// COM Infrastructure
#include "vbxcom.hh"

// Verbex SAPI Objects
#include "phrase.hh"
#include "visekb.hh"

class VISEEngineObj;
typedef VISEEngineObj  * PVISEEngineObj;
class VISEResultsObj;
typedef VISEResultsObj * PVISEResultsObj;

// IVbxSpkrTrain Interface

class VISEIVbxSpkrTrain : IVbxSpkrTrain {
  public:
    VISEIVbxSpkrTrain(LPVOID, LPUNKNOWN);
   ~VISEIVbxSpkrTrain(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IVbxSpkrTrain
    STDMETHODIMP              GetTrainer(LPUNKNOWN *);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown
};

typedef VISEIVbxSpkrTrain * PVISEIVbxSpkrTrain;

// IVbxTrain Interface

class VISEIVbxTrain : IVbxTrain {
  public:
    VISEIVbxTrain(LPVOID, LPUNKNOWN);
   ~VISEIVbxTrain(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IVbxTrain
    STDMETHODIMP              EnrollWord(PSDATA, PSDATA, DWORD *, DWORD *);
    STDMETHODIMP              RetranscribeWord(PSDATA, PSDATA, DWORD *, DWORD *);
    STDMETHODIMP              SetParameter(DWORD);
    STDMETHODIMP              SetParameter(DWORD, INT);
    STDMETHODIMP              GetParameter(DWORD, PINT);
    STDMETHODIMP              InsertCorrection(LPUNKNOWN, PSRPHRASE, WORD);
    STDMETHODIMP              InsertValidation(LPUNKNOWN, WORD);
    STDMETHODIMP              TrainWords(PSDATA, PSDATA);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown
};

typedef VISEIVbxTrain * PVISEIVbxTrain;

// Definition of the VISE Training SAPI Object

class VISETrainingObj : public IUnknown {
  public:
    VISETrainingObj();
   ~VISETrainingObj();

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // Non-SAPI Methods
    PVISEEngineObj            EngineObject(void);
    PVISEKbObj                Kb(void);
    PVISEGrammarObj           Grammar(void);
    Int                       FramesPerKernel(void);  
    Int                       UttCnt(void);
    Int                       UttsPerModel(void);
    PSAPIPhrase               EnrollWords(void);
    PVISEResultsObj           FirstResultsObject(void);
    PVISEResultsObj           NextResultsObject(void);
    PVISEResultsObj           RemoveResultsObject(void);

  protected:
    ULONG                     RefCnt;
    LPUNKNOWN                 UnknwnPtr;

    PVISEIVbxTrain            VISEIVbxTrainPtr;

    // Training Object Internals
    PVISEEngineObj            EngineObjPtr;
    PVISEKbObj                CMKbPtr;
    PVISEGrammarObj           CMGrammarObjPtr;

    DWORD                     CMFramesPerKernel;
    Int                       CMUttCnt;
    DWORD                     CMUttsPerModel;

    PSAPIPhrase               CMEnrollWordsPtr;
    LLIST                     ResultsList;
    SN                        LastStartSN;
    IMemory                   Allocator;

    CRITSECT_DECLARE(RefCntCritSect)

  // Interfaces are friends
  friend class VISEIVbxTrain;
};

typedef VISETrainingObj * PVISETrainingObj;
#endif  /* _VISETRAIN_HH_ */
