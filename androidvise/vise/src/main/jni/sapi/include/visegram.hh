#ifndef _VISEGRAM_HH_
#define _VISEGRAM_HH_

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ifaces.h"
#include "llist.h"
#include "mbox.h"

// VULCAN Inclusions
#include "kb.h"

// COM infrastructure
#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "visekb.hh"
#include "visemessage.hh"


// ISRGramCommon Interface

class VISEISRGramCommon : ISRGramCommon {
  public:
    VISEISRGramCommon(LPVOID, LPUNKNOWN);
   ~VISEISRGramCommon(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRGramCommon
    STDMETHODIMP              Activate(HWND, BOOL, PCSTR);
    STDMETHODIMP              Archive(BOOL, PVOID, DWORD, DWORD *);
    STDMETHODIMP              BookMark(QWORD, DWORD);
    STDMETHODIMP              Deactivate(PCSTR);
    STDMETHODIMP              DeteriorationGet(DWORD *, DWORD *, DWORD *);
    STDMETHODIMP              DeteriorationSet(DWORD, DWORD, DWORD);
    STDMETHODIMP              TrainDlg(HWND, PCSTR);
    STDMETHODIMP              TrainPhrase(DWORD, PSDATA);
    STDMETHODIMP              TrainQuery(DWORD *);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown
    MBOX_Att                  RAAttributes;
    MBOX                      ReturnAddress;
};

typedef VISEISRGramCommon * PVISEISRGramCommon;

// ISRGramCFG Interface

class VISEISRGramCFG : ISRGramCFG {
  public:
    VISEISRGramCFG(LPVOID, LPUNKNOWN);
   ~VISEISRGramCFG(void);

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRGramCFG
    STDMETHODIMP              LinkQuery(PCSTR, BOOL *);
    STDMETHODIMP              ListAppend(PCSTR, SDATA);
    STDMETHODIMP              ListGet(PCSTR, PSDATA);
    STDMETHODIMP              ListRemove(PCSTR, SDATA);
    STDMETHODIMP              ListSet(PCSTR, SDATA);
    STDMETHODIMP              ListQuery(PCSTR, BOOL *);

  private:
    ULONG                     RefCnt;         // Interface reference count
    LPVOID                    ObjPtr;         // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // Pointer to outer Unknown    
};

typedef VISEISRGramCFG * PVISEISRGramCFG;


// IVbxGrammar interface - this is a Verbex extension

class VISEIVbxGrammar : public IVbxGrammar {
  public:
    VISEIVbxGrammar(LPVOID, LPUNKNOWN);
   ~VISEIVbxGrammar();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IVbxGrammar Members
    STDMETHODIMP              Parse(PCSTR vocabName, PCSTR phrase, PSTR * gramNames, DWORD * gramNamesLen);
    STDMETHODIMP              Tokenize(PCSTR vocabName, PCSTR phrase, PSRPHRASE srPhrasePtr, DWORD phraseSize, DWORD * sizeNeededPtr);
    STDMETHODIMP              StartTrainingPass(PCSTR rule);
    STDMETHODIMP              EndTrainingPass(PCSTR rule, Bool finalPass);
    STDMETHODIMP              EndTrainingPass(PCSTR rule, Char ** words, Bool finalPass);
    STDMETHODIMP              SaveModels(PCSTR rule, Int minTrainingCount);
    STDMETHODIMP              SaveModels(PCSTR rule, Char ** words, Int minTrainingCount);
    STDMETHODIMP              SetTranslationKind(DWORD kind);
    STDMETHODIMP              Translate(DWORD kind, PCSTR rule, PSRPHRASE srPhrasePtr, PSTR buf, DWORD bufSize, DWORD * sizeNeededPtr);
    STDMETHODIMP              Translate(DWORD kind, PCSTR rule, PCSTR plaintext, PSTR buf, DWORD bufSize, DWORD * sizeNeededPtr);
    STDMETHODIMP              QueryGrammarName(PSTR nameBuf, DWORD bufSize, DWORD * sizeNeeded);
    STDMETHODIMP              QueryDefaultRuleName(PSTR nameBuf, DWORD bufSize, DWORD * sizeNeeded);
    STDMETHODIMP              QueryRuleNames(PSTR * ruleNamesBuf, DWORD * bufSize);
    STDMETHODIMP              RemoveNotifySink(void);

  private:
    ULONG                     RefCnt;             // Interface reference count
    LPVOID                    ObjPtr;             // Back pointer to the object
    LPUNKNOWN                 UnknwnPtr;          // Pointer to outer Unknown
    MBOX_Att                  RAAttributes;
    MBOX                      ReturnAddress;

    Bool                      LoadGrammars(Bool); // Partially load a KB from a Rec Stream       
    KB                        KbPtr;              // Partial KB (without models) for parsing and translating
};

typedef VISEIVbxGrammar * PVISEIVbxGrammar;


// Definition of the VISE Grammar SAPI Object

class VISEGrammarObj : public IUnknown {
  public:
    VISEGrammarObj(ISRGramNotifySink *, PVISERecStreamObj, IMemory * allocator, MBOX controlMBox);
   ~VISEGrammarObj();

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    ISRGramNotifySink       * NotifySink(void);
    PVISERecStreamObj         RecStream(void);
    MBOX                      ControlMBox(void);

  protected:
    ULONG                     RefCnt;
    LPUNKNOWN                 UnknwnPtr;

    PVISEISRGramCommon        VISEISRGramCommonPtr;
    PVISEISRGramCFG           VISEISRGramCFGPtr;
    PVISEIVbxGrammar          VISEIVbxGrammarPtr;

  private:
    IMemory                 * Allocator;
    LLIST                     ResultsObjects;
    ISRGramNotifySink       * NotifySinkPtr;
    PVISERecStreamObj         RecStreamPtr;
    MBOX                      CMControlMBox;
    UInt                      KbTransType;
    
    void                      EnqueueResults(PVISEResultsObj);
    void                      DequeueResults(PVISEResultsObj);

    CRITSECT_DECLARE(RefCntCritSect)

  // Results objects are friends
  friend class VISEResultsObj;

  // Interfaces are friends
  friend class VISEISRGramCommon;
  friend class VISEISRCentral;
  friend class VISEISRSpeaker;
  friend class VISEIVbxGrammar;
};

typedef VISEGrammarObj * PVISEGrammarObj;

#endif  /* _VISEGRAM_HH_ */
