#ifndef _ANDROID_AUDIOSRC_HH
#define _ANDROID_AUDIOSRC_HH

#include <stdlib.h>
#include <pthread.h>
#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "vbxwave.h"

#include "SLES/OpenSLES.h"
#include "SLES/OpenSLES_Android.h"
#include <atomic>

#define MAX_NUMBER_INPUT_DEVICES 3

// forward declarations
class AndroidAudioSrc;

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _AudioBuffer {
    size_t bufferSize;
    volatile BOOL   bufferSubmitted;
    volatile int    bytesRead;
    void * bytes;
} AudioBuffer;

void RecordEventCallback(SLRecordItf, void*, SLuint32);
void BufferQueueCallback(SLAndroidSimpleBufferQueueItf, void*);

#ifdef __cplusplus
}
#endif
/**
 * IAudio interface to OpenSL ES
 * Should really avoid the COM look at some point, but beyond the scope of this port
 */
class AndroidIAudio : public IAudio {
	public:
        // These IUnknown methods delegate to the container Unknown
        STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
        STDMETHODIMP_(ULONG)      AddRef();
        STDMETHODIMP_(ULONG)      Release();

        // IAudio
        STDMETHODIMP              Flush();
        STDMETHODIMP              GetGain(FLOAT *);
        STDMETHODIMP              SetGain(FLOAT);
        STDMETHODIMP              Reset();
        STDMETHODIMP              Ioctl(DWORD, PVOID);
        STDMETHODIMP              LevelGet(DWORD *);
        STDMETHODIMP              LevelSet(DWORD);
        /**
         * Registers an IAudioSourceNotifySink with the audio source
         */
        STDMETHODIMP              PassNotify(PVOID, IID);
        STDMETHODIMP              PosnGet(PQWORD);
        STDMETHODIMP              Claim();
        STDMETHODIMP              UnClaim();
        STDMETHODIMP              Start();
        STDMETHODIMP              Stop();
        STDMETHODIMP              TotalGet(PQWORD);
        STDMETHODIMP              ToFileTime(PQWORD, FILETIME *);
        STDMETHODIMP              WaveFormatGet(PSDATA);
        STDMETHODIMP              WaveFormatSet(SDATA);

    protected:
		AndroidIAudio(AndroidAudioSrc *, LPUNKNOWN);
    	~AndroidIAudio(void);
	private:
		ULONG            refCnt;
		AndroidAudioSrc *objPtr;
		LPUNKNOWN        unknownPtr;
	// this allows AndroidAudioSrc to invoke the constructor/destructor
	friend class AndroidAudioSrc;
};

class AndroidIAudioSource : public IAudioSource {
    public:
        // These IUnknown members delegate to outer Unknown
        STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
        STDMETHODIMP_(ULONG)      AddRef();
        STDMETHODIMP_(ULONG)      Release();

        // IAudioSource
        STDMETHODIMP              DataAvailable(DWORD *, BOOL *);
        /**
         * Reads audio data into the buffer
         * Returns AUDERR_NOTENOUGHDATA if less bytes were read than were requested
         */
        STDMETHODIMP              DataGet(PVOID, DWORD, DWORD *);
    protected:
        AndroidIAudioSource(AndroidAudioSrc*, LPUNKNOWN);
       ~AndroidIAudioSource();
    private:
        ULONG            refCnt;
        AndroidAudioSrc *objPtr;
        LPUNKNOWN        unknownPtr;
    friend class AndroidAudioSrc;
};

class AndroidAudioSrc : public IUnknown {
	public:
		AndroidAudioSrc(LPUNKNOWN, DESTROYFCN);
		~AndroidAudioSrc();
	    // IUnknown does not delegate
        STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
        STDMETHODIMP_(ULONG)      AddRef();
        STDMETHODIMP_(ULONG)      Release();

        // Non-Micro$oft Methods
        Bool                      Initialize(Int, Int);
        Bool                      Open(WAVEFORMATEX *);
        Bool                      Close();
        Bool                      ShutDown();

        // actual callback handler for various things
        void recordEventCallback(SLuint32 recordEvent);
        void bufferQueueCallback();
	protected:
        ULONG                   refCnt;
        LPUNKNOWN               unknownPtr;
        DESTROYFCN              destroyFcn;
        IAudioSourceNotifySink *notifySink;
        BOOL                    deviceOpen;
		void                   *waveFormatEx;
		size_t                  waveFormatExLen;

        AndroidIAudio          *androidIAudioPtr;
        AndroidIAudioSource    *androidIAudioSourcePtr;
        // slCreateEngine
		SLObjectItf sl;
        // actual engine interface
		SLEngineItf engine;
        //SLAudioIODeviceCapabilitiesItf devCaps;
        SLAndroidConfigurationItf androidConfiguration;
        SLDeviceVolumeItf deviceVolume;

        SLObjectItf recorderObject;
        SLRecordItf recorder;

        SLint32                numInputs;
        SLuint32               inputDeviceIds[MAX_NUMBER_INPUT_DEVICES];
        SLAudioInputDescriptor audioInputDescriptor;

        SLDataSource           audioSource;
        SLDataLocator_IODevice audioInputDeviceLocator;

        SLDataSink                             audioSink;
        SLDataLocator_AndroidSimpleBufferQueue audioInputBuffersLocator;
        SLDataFormat_PCM                       audioInputFormat;
        SLAndroidSimpleBufferQueueItf          bufferQueue;

        volatile int state = 0; // 0 is stopped, 1 is started
        int64_t totalBytesSent;
        size_t bufferSize;
        int numberOfBuffers;
        // index of next buffer to read, mod with numberOfBuffers to get real index
        volatile int nextReadBuffer;
        // index of last buffer read from BufferQueue, mod with numberOfBuffers to get real index
        volatile int lastReceivedBuffer;
        // index of next buffer to submit, mod with numberOfBuffers to get real index
        volatile int nextSubmitBuffer;
        // ptr to array of AudioBuffer*
        AudioBuffer **buffers;
        pthread_mutex_t bufferMutex;
        std::atomic_int submittedBuffers;

        friend class AndroidIAudio;
        friend class AndroidIAudioSource;
        friend void RecordEventCallback(SLRecordItf, void*, SLuint32);
        friend void BufferQueueCallback(SLAndroidSimpleBufferQueueItf, void*);
};
#endif
