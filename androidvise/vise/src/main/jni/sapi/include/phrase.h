#ifndef _PHRASE_H_
#define _PHRASE_H_

#include "types.h"
#include "hyp.h"
#include "vocab.h"

#define LAST_WORD  -1

#ifdef __cplusplus
extern "C" {
#endif

typedef struct phrase_s *PHRASE;
typedef struct phrase_s {
  Ptr       sapiPhrase;                 /* the real Microsoft SAPI phrase */
  Int       sizeWord;                   /* size of each word in the phrase */
  Int       maxStrLen;                  /* allocation size for SRWORD text */
  Int       maxWrds;                    /* allocation size for SRWORDs */
  Ptr       wrdPtr;                     /* current write pointer */
  Int       wrdCnt;                     /* current word count */
  Bool      ownAllocation;              /* do we manage SRPHRASE allocation? */
  Bool    (*addWrdFcn)(PHRASE, Int, Char *);      /* add a word to this phrase */
  void    (*initFcn)(PHRASE);                     /* initialize this phrase */
} phrase_t;

PHRASE   PHRASE_create(Int maxStrLen, Int maxWrds);
PHRASE   PHRASE_encap(Ptr phrasePtr);
void     PHRASE_destroy(PHRASE phrase);
void     PHRASE_init(PHRASE phrase);
void     PHRASE_print(Ptr phrase);
void     PHRASE_concat(Ptr phrasePtr, Char *buf);
Bool     PHRASE_addWord(PHRASE phrase, Int wordId, Char *wordTxt);
Int      PHRASE_fromHypothesis(HYP hyp, PHRASE phrase, VOCAB vocab);

#ifdef __cplusplus
}
#endif

#endif  /* _PHRASE_H_ */
