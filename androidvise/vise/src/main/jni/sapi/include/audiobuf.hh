#ifndef _AUDIOBUF_HH_
#define _AUDIOBUF_HH_

#include "vbxcom.hh"
#include "class.h"
#include "buff.h"
#include "sn.h"
#include "ifaces.h"

/* AudioFrame structure exported for clients */
typedef struct audioframe_s {
  Int   byteCnt;
  UChar bytes[1];
} audioframe_t, *AUDIOFRAME;

// IAudioSource interface

class VBXIAudioSource : public IAudioSource {
  public:
    VBXIAudioSource(LPVOID, LPUNKNOWN);
   ~VBXIAudioSource(void);

    // These IUnknown members delegate to outer Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IAudioSource
    STDMETHODIMP              DataAvailable(DWORD *, BOOL *);
    STDMETHODIMP              DataGet(PVOID, DWORD, DWORD *);

  private:
    ULONG     RefCnt;         // interface reference count
    LPVOID    ObjPtr;         // back pointer to the object (???)
    LPUNKNOWN UnknwnPtr;      // pointer to outer Unknown
};

typedef VBXIAudioSource *PVBXIAudioSource;

// IAudioDest interface

class VBXIAudioDest : public IAudioDest {
  public:
    VBXIAudioDest(LPVOID, LPUNKNOWN);
   ~VBXIAudioDest(void);

    // These IUnknown members delegate to outer Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IAudioDest
    STDMETHODIMP              FreeSpace(DWORD *, BOOL *);
    STDMETHODIMP              DataSet(PVOID, DWORD);
    STDMETHODIMP              BookMark(DWORD);

  private:
    ULONG     RefCnt;         // interface reference count
    LPVOID    ObjPtr;         // back pointer to the object (???)
    LPUNKNOWN UnknwnPtr;      // pointer to outer Unknown
};

typedef VBXIAudioDest *PVBXIAudioDest;

class AudioBuf : public IUnknown {

  public:
    AudioBuf(Int objtSize, Int bufSize);
   ~AudioBuf(void);
    OBJT                      ObjtNew();
    void                      SetEof(Bool);
    OBJT                      RetrieveObjt();
    void                      SubmitObjt(OBJT);
    void                      Flush();
    enum                      { LINEAR16, ULAW8 };

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);
 
  protected:
    ULONG                     RefCnt;
    LPUNKNOWN                 UnknwnPtr;
    PVBXIAudioSource          VBXIAudioSrcPtr;
    PVBXIAudioDest            VBXIAudioDestPtr;
    BUFF                      Buff;
    CLASS                     Class;
    Int                       AudioLen;
    Int                       AudioPosn;
    Quad                      TotalBytes;
    AUDIOFRAME                AudioFrame;
    UChar                    *AudioPtr;
    Int                       BufSize;
    Bool                      Eof;

    OBJT                      CurrentObjt;
    Int                       ObjtSamplesRemaining;
    Int                       ObjtSize;
    SN                        WriteSN;
    Int                       SampleBytes;
    pthread_mutex_t           Mutex;
    IMemory                   Allocator;

  // Interfaces are friends
  friend class VBXIAudioSource;
  friend class VBXIAudioDest;
  friend class InroadIAudio;
  friend class MMIAudio;
  friend class NISTInIAudio;
  friend class OssIAudio;
  friend class OssDestIAudio;
  friend class WINVCIAudio;
  friend class DialogicIAudio;
};

typedef AudioBuf *PAudioBuf;

#endif /* _AUDIOBUF_HH_ */
