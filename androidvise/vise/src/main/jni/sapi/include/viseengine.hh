#ifndef _VISEENGINE_HH_
#define _VISEENGINE_HH_

#include <limits.h>
#include <float.h>
#include <math.h>
#include "types.h"
#include "sn.h"

// VULCAN Inclusions
#include "vocab.h"
#include "ekb.h"
#include "kb.h"

// FE Inclusions
#include "mmaudin.h"
#include "vfe.h"

// Verbex Infrastructure
#include "ifaces.h"
#include "visemsg.h"
#include "mbox.h"
#include "vbxparms.h"

// COM Infrastructure
#include "vbxcom.hh"
#include "vbxsapi.hh"

#define ENROLLMENTGRAMMARNAME     "ANY_WORD"
#define CALIBRATIONGRAMMARNAME    "CALIBRATION"

 // VISEEngineObj uses these ludicrously large equivalent threshold values to mark threshold parameters as uninitialized
#define VISE_UNINITIALIZED_INTEGER_THRESHOLD  37 * (Int) VISE_SCALEFACTOR_DEFAULT
#define VISE_UNINITIALIZED_FLOAT_THRESHOLD    SCALEDLOGINTEGER_TO_LINEARFLOAT(VISE_UNINITIALIZED_INTEGER_THRESHOLD)
#define SRWARN_UNINITIALIZED_PARAMETER        SRWARNING(0xFF)

// Macros to convert between frames and milliseconds (FRAMEPERIOD is defined in vviseengine.cc and fviseengine.cc)
#define VISE_FRAMES_TO_MILLISECONDS(FRAMES)   ((Int)floor((Double)FRAMES * (Double)FRAMEPERIOD + (Double)0.5))
#define VISE_MILLISECONDS_TO_FRAMES(MILLIS)   ((Int)floor((Double)MILLIS / (Double)FRAMEPERIOD + (Double)0.5))

#define VISE_MAXFRAMECNT_DEFAULT  2000         // Frames
#define LS_MIN_DEFAULT            40           // Frames
#define IW_MAX_DEFAULT            30           // Frames

#define VISE_AUDQUEUELEN_DEFAULT  0            // Frames
#define VISE_BOUSILMSECS_DEFAULT  0
#define VISE_EOUSILMSECS_DEFAULT  0

#define VISE_SAMPSIGBITS_MINIMUM  0
#define VISE_SAMPSIGBITS_DEFAULT  13
#define VISE_SAMPSIGBITS_MAXIMUM  16
#define VISE_SAMPSPERSEC_DEFAULT  0
#define VISE_PRINTSTATUS_DEFAULT  VBX_VERBOSE

class VISEObj;
typedef VISEObj            * PVISEObj;
class VISEGrammarRequest;
typedef VISEGrammarRequest * PVISEGrammarRequest;
class VISESpeakerProfile;
typedef VISESpeakerProfile * PVISESpeakerProfile;
class VISEResultsObj;
typedef VISEResultsObj     * PVISEResultsObj;
class VISEKbObj;
typedef VISEKbObj          * PVISEKbObj;
class VISETrainingObj;
typedef VISETrainingObj    * PVISETrainingObj;

// Engine command priorities
typedef enum sapipriority_e {
  SAPI_TRAINPRIORITY = MBOX_MAXPRIORITY,
  SAPI_FORCEPRIORITY,
  SAPI_ABORTPRIORITY,
  SAPI_RECOGPRIORITY,
  SAPI_DEACTPRIORITY = SAPI_RECOGPRIORITY,
  SAPI_CALIBPRIORITY = SAPI_RECOGPRIORITY,
  SAPI_BATCHPRIORITY = SAPI_RECOGPRIORITY,
  SAPI_FINISPRIORITY,
  SAPI_NUMPRIORITIES
} sapipriority_t;

  // Definition of the VISE Engine Object

class VISEEngineObj {
  public:
    VISEEngineObj(IMemory * allocator, IMail * mail, PVISEObj controlObjPtr, LPUNKNOWN audioUnknwnPtr);
    virtual ~VISEEngineObj();

    virtual Bool          Abort() = 0;
    virtual STDMETHODIMP  Calibrate(PVISEResultsObj results);
    STDMETHODIMP          Deactivate();
    STDMETHODIMP          Activate(PVISEResultsObj results, PVISEGrammarRequest rule);
    STDMETHODIMP          Recognize(PVISEResultsObj results, PVISEGrammarRequest rule);
    virtual STDMETHODIMP  Force(PVISEResultsObj results);
    virtual STDMETHODIMP  EnrollWord(PVISETrainingObj trainer);
    virtual STDMETHODIMP  RetranscribeWord(PVISETrainingObj trainer);
    virtual STDMETHODIMP  Train(PVISEResultsObj results);
    virtual STDMETHODIMP  StartTrainingPass(PVISEResultsObj results);
    virtual STDMETHODIMP  EndTrainingPass(PVISEResultsObj results, Bool finalPass);
    virtual STDMETHODIMP  EndTrainingPass(PVISEResultsObj results, Char ** words, Bool finalPass);
    virtual STDMETHODIMP  SaveModels(PVISEResultsObj results, Int minTrainingCount);
    virtual STDMETHODIMP  SaveModels(PVISEResultsObj results, Char ** words, Int minTrainingCount);
    virtual STDMETHODIMP  SetParameter(DWORD param) = 0;
    virtual STDMETHODIMP  SetParameter(DWORD param, INT value) = 0;
    virtual STDMETHODIMP  GetParameter(DWORD param, PINT pValue) = 0;
    virtual STDMETHODIMP  SetParameter(DWORD param, FLOAT value) = 0;
    virtual STDMETHODIMP  GetParameter(DWORD param, PFLOAT pValue) = 0;
    STDMETHODIMP          SetSpeaker(PVISESpeakerProfile speaker);
    virtual STDMETHODIMP  SetTimeout(UINT timeout);
    STDMETHODIMP          SourceAddRef();
    STDMETHODIMP          SourceRelease();
    VFE                   Source();
    Int                   MaxSigBitsDefault;

  protected:
    virtual STDMETHODIMP  SetEngineDefaults(void) = 0;

    PVISEObj              ControlObjPtr;
    MBOX                  ControlMBox;

    IMemory             * Allocator;
    IMail               * Mail;

    IAudio              * IAudioPtr;
    IAudioSource        * IAudioSourcePtr;
    MMAudioIn             MMAudIn;
    IAudioIn            * AudioIn;              // The Audio Interface

    EKB                   EKb;                  // The Engine KB

    VFE                   VFeat;                // The Feature Extractor

    STDMETHODIMP          SourceStart(SN * sn);
    STDMETHODIMP          SourceStop(SN * sn);
    UInt                  SourceRefCnt;
    Int                   FeVersion;

    MBOX                  EngineMBox;           // The engine thread's command mailbox
    pthread_t             EngineThread;
    virtual void          EngineMain() = 0;

    Int                   AudioQLen;
    Int                   BOUSilMSecs;
    Int                   EOUSilMSecs;
    Int                   MaxSigBits;
    Int                   SampsPerSec;
    Int                   MaxFrameCount;
    UInt                  LSMinDwell;
    UInt                  IWMaxDwell;

    Int                   PrintStats;

     // Variables used by VISE_engineThread
    PVISEKbObj            KbObjPtr;
    KB                    Kb;
    UInt                  VocabId;
    VOCAB                 Vocabulary;
    UInt                  GrammarId;
    Bool                  BatchTrain;

  // Thread start routines are friends
  friend void VISE_engineThread(VISEEngineObj *);
};

typedef VISEEngineObj * PVISEEngineObj;

// The engine thread routine
extern void  VISE_engineThread(PVISEEngineObj);

// The engine constructor
extern PVISEEngineObj  VISE_createEngine(IMemory * allocator, IMail * mail, PVISEObj controlObjPtr, LPUNKNOWN audioUnknwnPtr);

#endif  /* _VISEENGINE_HH_ */
