#ifndef _VISEPHRASE_HH_
#define _VISEPHRASE_HH_

#include "types.h"
#include "vbxcom.hh"
#include "hyp.h"
#include "kb.h"
#include "vfile.h"
#include "vocab.h"

#include "phrase.hh"

class VISESAPIPhrase: public SAPIPhrase {
  public:
    VISESAPIPhrase(HYP, VOCAB);
    VISESAPIPhrase(HYP, KB, VFILE, UInt, UInt, UInt);
    VISESAPIPhrase(PSRPHRASE, VOCAB);
    VISESAPIPhrase(PCSTR, VOCAB);
    VISESAPIPhrase(PCSTR, UInt);

    HYP     Hypothesis();
};

typedef VISESAPIPhrase  * PVISESAPIPhrase;

#endif  /* _VISEPHRASE_HH_ */
