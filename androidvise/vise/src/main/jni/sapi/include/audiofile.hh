#ifndef _AUDIOFILE_HH_
#define _AUDIOFILE_HH_

#include "vbxcom.hh"

// IVbxAudioFile
#undef  INTERFACE
#define INTERFACE   IVbxAudioFile

DEFINE_GUID(IID_IVbxAudioFile, 39, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

DECLARE_INTERFACE_(IVbxAudioFile, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)  (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)   (THIS) PURE;
  STDMETHOD_(ULONG, Release)  (THIS) PURE;

  // IVbxAudioFile members
  STDMETHOD (Read)            (THIS_ PCHAR fname) PURE;
//  STDMETHOD (Write)           (THIS_ PCHAR fname) PURE;
  STDMETHOD (SetDefaultDir)   (THIS_ PCHAR dirName) PURE;
  STDMETHOD (GetFileName)     (THIS_ PCHAR fname) PURE;
};

typedef IVbxAudioFile *PIVbxAudioFile;

#endif /* _AUDIOFILE_HH_ */

