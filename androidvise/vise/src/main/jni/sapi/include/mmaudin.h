/* mmaudin.h - IAudioIn methods built on an MMAudio source
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Component methods for building an IAudioIn to obtain data from an OLE
 * MMAudio source.
 *
 * Verbex Voice Systems (dcv)
 * $Header: mmaudin.h[1.0] Tue Jun  9 11:54:31 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _MMAUDIN_H_
#define _MMAUDIN_H_

#include "sn.h"
#include "ifaces.h"
#include "vbxtypes.h"
#include "viseerr.h"

#undef  INTERFACE
#define INTERFACE MMAudioIn

typedef struct mmaudin_s * MMAUDIN;

VBX_DECLARE_INTERFACE(MMAudioIn) {
  IAudioIn           iAudioIn;
  struct mmaudin_s * mmaudin;
};

#ifdef __cplusplus
extern "C" { 
#endif

extern V_Bool MMAUDIN_Init(IAudioIn * This, V_Uns rate, V_Uns size, V_Uns * bits);
extern V_Bool MMAUDIN_Read(IAudioIn * This, V_Int * samples, V_Uns numSamples);
extern V_Bool MMAUDIN_Start(IAudioIn * This);
extern V_Bool MMAUDIN_Stop(IAudioIn * This);
extern V_Bool MMAUDIN_LevelSet(IAudioIn * This, V_Uns level);
extern V_Bool MMAUDIN_LevelGet(IAudioIn * This, V_Uns * level);
extern V_Bool MMAUDIN_PosnGet(IAudioIn * This, SN * posn);
extern V_Err  MMAUDIN_Status(IAudioIn * This);
extern V_Bool MMAUDIN_TotalGet(IAudioIn * This, SN * currentSN);
extern V_Bool MMAUDIN_DataAvailable(IAudioIn * This, V_Long * bytesAvailable, V_Int * eof);

#ifdef __cplusplus
}
#endif

#endif /* _MMAUDIN_H_ */
