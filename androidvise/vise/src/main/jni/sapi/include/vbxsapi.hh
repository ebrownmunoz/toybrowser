#ifndef _VBXSAPI_HH_
#define _VBXSAPI_HH_

#include "vbxcom.hh"
#include "vbxparms.h"

// GUID's for Verbex SAPI Objects

// Define GUID's for the Verbex engines and their "modes"
DEFINE_GUID(CLSID_VerbexVise,   0x39a1e272, 0x5092, 0x11d0, 0xa9, 0x94, 0x00, 0x00, 0xc0, 0x7d, 0xce, 0xaf);
DEFINE_GUID(CLSID_VerbexFlexVise,   0x39a1e272, 0x5092, 0x11d0, 0xa9, 0x94, 0x00, 0x00, 0xc0, 0x7d, 0xce, 0xb0);
DEFINE_GUID(IID_VerbexViseMode, 0x00000029, 0x0001, 0x0002, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46);
DEFINE_GUID(IID_VerbexFlexViseMode, 0x00000029, 0x0001, 0x0002, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x47);

// CLSID: VerbexMMAudioSource {2B6A110E-61CF-11d0-8BC2-E270470E384D}
DEFINE_GUID(CLSID_VerbexMMAudioSource, 
0x2b6a110e, 0x61cf, 0x11d0, 0x8b, 0xc2, 0xe2, 0x70, 0x47, 0xe, 0x38, 0x4d);

// CLSID: VerbexMMAudioDest {2B6A110F-61CF-11d0-8BC2-E270470E384D}
DEFINE_GUID(CLSID_VerbexMMAudioDest, 
0x2b6a110f, 0x61cf, 0x11d0, 0x8b, 0xc2, 0xe2, 0x70, 0x47, 0xe, 0x38, 0x4d);

// CLSID: VerbexWinvcAudioSource {2B6A11A0-61CF-11d0-8BC2-E270470E384D}
DEFINE_GUID(CLSID_VerbexWinvcAudioSource, 
0x2b6a11a0, 0x61cf, 0x11d0, 0x8b, 0xc2, 0xe2, 0x70, 0x47, 0xe, 0x38, 0x4d);

// CLSID: VerbexWinvcAudioDest {2B6A11A1-61CF-11d0-8BC2-E270470E384D}
DEFINE_GUID(CLSID_VerbexWinvcAudioDest, 
0x2b6a11a1, 0x61cf, 0x11d0, 0x8b, 0xc2, 0xe2, 0x70, 0x47, 0xe, 0x38, 0x4d);

// CLSID: VerbexNistAudioSource {2B6A11A2-61CF-11d0-8BC2-E270470E384D}
DEFINE_GUID(CLSID_VerbexNistAudioSource, 
0x2b6a11a2, 0x61cf, 0x11d0, 0x8b, 0xc2, 0xe2, 0x70, 0x47, 0xe, 0x38, 0x4d);

typedef void (*DESTROYFCN)(void);

// IVbxEngine
#undef  INTERFACE
#define INTERFACE   IVbxEngine

DEFINE_GUID(IID_IVbxEngine, 38, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

DECLARE_INTERFACE_(IVbxEngine, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)    (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)     (THIS) PURE;
  STDMETHOD_(ULONG, Release)    (THIS) PURE;

  // IVbxEngine members
  STDMETHOD (SetParameter)      (THIS_ DWORD param) PURE;
  STDMETHOD (SetParameter)      (THIS_ DWORD param, INT value) PURE;
  STDMETHOD (GetParameter)      (THIS_ DWORD param, PINT pValue) PURE;
  STDMETHOD (SetParameter)      (THIS_ DWORD param, FLOAT value) PURE;
  STDMETHOD (GetParameter)      (THIS_ DWORD param, PFLOAT pValue) PURE;
  STDMETHOD (SetParameter)      (THIS_ DWORD param, PCSTR value) PURE;
  STDMETHOD (GetParameter)      (THIS_ DWORD param, PSTR value) PURE;
  STDMETHOD (SetTimeout)        (THIS_ UINT timeout) PURE;
};

typedef IVbxEngine * PIVbxEngine;

// IVbxGrammar
#undef  INTERFACE
#define INTERFACE   IVbxGrammar

// Translation constants (types of translation supported by the interface)
#define  SRGRMTRKIND_HOST             0
#define  SRGRMTRKIND_VOICE            1
#define  SRGRMTRKIND_DISPLAY          2

DEFINE_GUID(IID_IVbxGrammar, 39, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

DECLARE_INTERFACE_(IVbxGrammar, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)    (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)     (THIS) PURE;
  STDMETHOD_(ULONG, Release)    (THIS) PURE;

  // IVbxGrammar members
  STDMETHOD (Parse)             (THIS_ PCSTR vocabName, PCSTR phrase, PSTR * gramNames, DWORD * gramNamesLen) PURE;
  STDMETHOD (Tokenize)          (THIS_ PCSTR vocabName, PCSTR phrase, PSRPHRASE srPhrasePtr, DWORD phraseSize, DWORD * sizeNeededPtr) PURE;
  STDMETHOD (StartTrainingPass) (THIS_ PCSTR rule) PURE;
  STDMETHOD (EndTrainingPass)   (THIS_ PCSTR rule, BOOL finalPass) PURE;
  STDMETHOD (EndTrainingPass)   (THIS_ PCSTR rule, Char ** words, Bool finalPass) PURE;
  STDMETHOD (SaveModels)        (THIS_ PCSTR rule, Int minTrainingCount) PURE;
  STDMETHOD (SaveModels)        (THIS_ PCSTR rule, Char ** words, Int minTrainingCount) PURE;
  STDMETHOD (SetTranslationKind)(THIS_ DWORD kind) PURE;
  STDMETHOD (Translate)         (THIS_ DWORD kind, PCSTR rule, PSRPHRASE srPhrasePtr, PSTR buf, DWORD bufSize, DWORD * sizeNeededPtr) PURE;
  STDMETHOD (Translate)         (THIS_ DWORD kind, PCSTR rule, PCSTR plainText, PSTR buf, DWORD bufSize, DWORD * sizeNeededPtr) PURE;
  STDMETHOD (QueryGrammarName)  (THIS_ PSTR nameBuf, DWORD bufSize, DWORD * sizeNeeded) PURE;
  STDMETHOD (QueryDefaultRuleName)  (THIS_ PSTR nameBuf, DWORD bufSize, DWORD * sizeNeeded) PURE;
  STDMETHOD (QueryRuleNames)    (THIS_ PSTR * ruleNamesBuf, DWORD * bufSize) PURE;
  STDMETHOD (RemoveNotifySink)  (THIS) PURE;
};

typedef IVbxGrammar * PIVbxGrammar;

// IVbxSpkrTrain
#undef  INTERFACE
#define INTERFACE   IVbxSpkrTrain

DEFINE_GUID(IID_IVbxSpkrTrain, 40, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

DECLARE_INTERFACE_(IVbxSpkrTrain, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)    (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)     (THIS) PURE;
  STDMETHOD_(ULONG, Release)    (THIS) PURE;

  // IVbxSpkrTrain members
  STDMETHOD (GetTrainer)        (THIS_ LPUNKNOWN *ppUnknwnTrainer) PURE;
};

typedef IVbxSpkrTrain * PIVbxSpkrTrain;

// IVbxTrain
#undef  INTERFACE
#define INTERFACE   IVbxTrain

DEFINE_GUID(IID_IVbxTrain, 41, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

// Parameter constants (parameters that can be set via the interface)
#define SRTRAIN_UTTSPERMODEL          0
#define SRTRAIN_FRAMESPERKERNEL       1

DECLARE_INTERFACE_(IVbxTrain, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)    (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)     (THIS) PURE;
  STDMETHOD_(ULONG, Release)    (THIS) PURE;

  // IVbxTrain members
  STDMETHOD (EnrollWord)        (THIS_ PSDATA pWords, PSDATA pEnrolled, DWORD *dwCount, DWORD *dwNeeded) PURE;
  STDMETHOD (RetranscribeWord)  (THIS_ PSDATA pWords, PSDATA pEnrolled, DWORD *dwCount, DWORD *dwNeeded) PURE;
  STDMETHOD (SetParameter)      (THIS_ DWORD dwParam) PURE;
  STDMETHOD (SetParameter)      (THIS_ DWORD dwParam, INT value) PURE;
  STDMETHOD (GetParameter)      (THIS_ DWORD dwParam, PINT pValue) PURE;
  STDMETHOD (InsertCorrection)  (THIS_ LPUNKNOWN pUnknwnResult, PSRPHRASE pTranscription, WORD wConfidence) PURE;
  STDMETHOD (InsertValidation)  (THIS_ LPUNKNOWN pUnknwnResult, WORD wConfidence) PURE;
  STDMETHOD (TrainWords)        (THIS_ PSDATA pWords, PSDATA pTrained) PURE;
};

typedef IVbxTrain * PIVbxTrain;

// ISRResTranslate
#undef  INTERFACE
#define INTERFACE   ISRResTranslate

// Translation constants (types of translation supported by the interface)
#define  SRRESTRKIND_HOST             0
#define  SRRESTRKIND_VOICE            1
#define  SRRESTRKIND_DISPLAY          2

DEFINE_GUID(IID_ISRResTranslate, 42, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);

DECLARE_INTERFACE_(ISRResTranslate, IUnknown) {
  // IUnknown members
  STDMETHOD (QueryInterface)    (THIS_ REFIID riid, LPVOID *ppvObj) PURE;
  STDMETHOD_(ULONG, AddRef)     (THIS) PURE;
  STDMETHOD_(ULONG, Release)    (THIS) PURE;

  // ISRResTranslate members
  STDMETHOD (Translate)         (THIS_ DWORD transKind, DWORD rank, PSTR buf, DWORD bufSize, DWORD *sizeNeededPtr) PURE;
};

typedef ISRResTranslate * PISRResTranslate;

#endif /* _VBXSAPI_HH_ */
