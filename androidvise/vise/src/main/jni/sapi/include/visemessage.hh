#ifndef _VISEMSG_HH_
#define _VISEMSG_HH_

// Verbex C Infrastructure
#include "types.h"
#include "mbox.h"
#include "vbx.h"

// COM infrastructure
#include "vbxcom.hh"


typedef enum SAPIVISECmd_e {
  SAPI_ABORT,
  SAPI_ABORTED,
  SAPI_ACTIVATE,
  SAPI_ACTIVATED,
  SAPI_CALIBRATE,
  SAPI_CALIBRATED,
  SAPI_DEACTIVATE,
  SAPI_ENDTRAININGPASS,
  SAPI_ENROLLWORD,
  SAPI_EXIT,
  SAPI_LISTUNMODELLEDWORDS,
  SAPI_MISSINGMODELS,
  SAPI_NULLRESULTS,
  SAPI_PAUSE,
  SAPI_RECOGNIZE,
  SAPI_RECOGNIZED,
  SAPI_RESUME,
  SAPI_RETRANSCRIBE,
  SAPI_SAVEMODELS,
  SAPI_SETSIGBITS,
  SAPI_SETSPEAKER,
  SAPI_STARTTRAININGPASS,
  SAPI_TRAIN,
  NUMSAPIVISECMDS
} SAPIVISECmd;

extern const V_Char * SAPIVISECmd_string(SAPIVISECmd command);


class VISEGrammarObj;
typedef VISEGrammarObj * PVISEGrammarObj;
class VISEResultsObj;
typedef VISEResultsObj * PVISEResultsObj;
class VISEVoiStreamObj;
typedef VISEVoiStreamObj  * PVISEVoiStreamObj;


class VISEReply {
  public:
    VISEReply(SAPIVISECmd, Ptr, HRESULT);
   ~VISEReply(void);

    SAPIVISECmd        Service(void);
    void               SetService(SAPIVISECmd);
    HRESULT            HResult(void);
    void               SetHResult(HRESULT);
    Ptr                Results(void);

  protected:
    Ptr                CMResults;

  private:
    SAPIVISECmd        CMService;
    HRESULT            CMHResult;
};

typedef VISEReply * PVISEReply;


class VISEServiceRequest {
  public:
    VISEServiceRequest(SAPIVISECmd, Ptr, MBOX);
   ~VISEServiceRequest(void);

    SAPIVISECmd        Service(void);
    void               SetService(SAPIVISECmd);
    MBOX               ReturnAddress(void);
    void               SetReturnAddress(MBOX);
    Ptr                Argument(void);

  protected:
    Ptr                CMArgument;

  private:
    SAPIVISECmd        CMService;
    MBOX               CMReturnAddress;
};

typedef VISEServiceRequest * PVISEServiceRequest;


class VISEGramResRequest : public VISEServiceRequest {
  public:
    VISEGramResRequest(SAPIVISECmd, LPUNKNOWN, const Char * , Bool, Int, MBOX);
    VISEGramResRequest(SAPIVISECmd, LPUNKNOWN, const Char * , Bool, MBOX);
   ~VISEGramResRequest(void);

    Char             * RuleName(void);
    void               SetRuleName(Char *);
    Bool               AutoPause(void);
    Bool               BoolParam(void);
    Int                IntParam(void);
    Char            ** WordNames(void);
    void               SetWordNames(Char **);

  private:
    Char             * CMRuleName;
    Bool               CMAutoPause;
    Int                CMCounter;
    Char            ** CMWordNames;
};

typedef VISEGramResRequest * PVISEGramResRequest;


class VISEGrammarRequest : public VISEGramResRequest {
  public:
    VISEGrammarRequest(SAPIVISECmd, PVISEGrammarObj, const Char *, Bool, Int, MBOX);
    VISEGrammarRequest(SAPIVISECmd, PVISEGrammarObj, const Char *, Bool, MBOX);
   ~VISEGrammarRequest(void);

    PVISEGrammarObj    GrammarObject(void);
};

typedef VISEGrammarRequest * PVISEGrammarRequest;


class VISEResultsRequest : public VISEGramResRequest  {
  public:
    VISEResultsRequest(SAPIVISECmd, PVISEResultsObj, const Char *, Bool, Int, MBOX);
    VISEResultsRequest(SAPIVISECmd, PVISEResultsObj, const Char *, Bool, MBOX);
   ~VISEResultsRequest(void);

    PVISEResultsObj    ResultsObject(void);
};

typedef VISEResultsRequest * PVISEResultsRequest;


class VISESpeakerProfile : public VISEServiceRequest {
  public:
    VISESpeakerProfile(SAPIVISECmd, PVISEVoiStreamObj, const Char *, Bool, MBOX);
   ~VISESpeakerProfile(void);

    PVISEVoiStreamObj  VoiStream(void);
    Char             * SpkrName(void);
    Bool               Lock(void);

  private:
    Char             * CMSpkrName;
    Bool               CMLock;
};

typedef VISESpeakerProfile * PVISESpeakerProfile;


#endif /* _VISEMSG_HH_ */
    
