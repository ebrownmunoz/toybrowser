#ifndef _VISENOTIFY_HH_
#define _VISENOTIFY_HH_

#include "types.h"

// Verbex Infrastructure
#include "critsect.h"
#include "ifaces.h"
#include "visemsg.h"
#include "vfe.h"

// COM Infrastructure
#include "vbxcom.hh"
#include "vbxsapi.hh"

  // Definition of the VISE Notification Object

class VISENotifyObj {
  public:
    VISENotifyObj(IMemory * allocator, IMail * mail, ISRNotifySink * notifier, VFE source);
   ~VISENotifyObj();

    STDMETHODIMP    SetParameter(DWORD param);
    STDMETHODIMP    SetParameter(DWORD param, INT value);
    STDMETHODIMP    GetParameter(DWORD param, PINT pValue);

  private:
    IMemory       * Allocator;
    IMail         * Mail;
    ISRNotifySink * NotifySinkPtr;
    VFE             VFeat;

    CRITSECT_DECLARE(CritSect)

    IMBox         * NotifyMBox;
    VBXMSG          NotifyMsg;
    UInt            NotifyMBoxId;
    pthread_t       NotifyThread;
    void            NotifyMain();

    Int             VUEnabled;
    Int             VUScaleFactor;
    Int             VUOffset;

  // Thread start routines are friends
  friend void VISE_notifyThread(VISENotifyObj *);
};

typedef VISENotifyObj * PVISENotifyObj;

#endif  /* _VISENOTIFY_HH_ */
