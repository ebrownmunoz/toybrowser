#ifndef _VISEOBJ_HH_
#define _VISEOBJ_HH_

#include "types.h"

// VULCAN Inclusions
#include "ekb.h"

// Verbex Infrastructure
#include "critsect.h"
#include "hash.h"
#include "ifaces.h"
#include "mbox.h"
#include "ukey.h"

// COM Infrastructure
#include "vbxcom.hh"
#include "vbxsapi.hh"

class VISEEngineObj;
typedef VISEEngineObj      * PVISEEngineObj;
class VISENotifyObj;
typedef VISENotifyObj      * PVISENotifyObj;
class VISEGrammarRequest;
typedef VISEGrammarRequest * PVISEGrammarRequest;
class VISESpeakerProfile;
typedef VISESpeakerProfile * PVISESpeakerProfile;
class VISEKbObj;
typedef VISEKbObj          * PVISEKbObj;
class VISESpkrDirObj;
typedef VISESpkrDirObj     * PVISESpkrDirObj;
class VISEIVbxSpkrTrain;
typedef VISEIVbxSpkrTrain  * PVISEIVbxSpkrTrain;


  // VISEISRCentral Interface

class VISEISRCentral : public ISRCentralA, public ISRCentralW {
  public:
    VISEISRCentral(LPVOID, LPUNKNOWN);
   ~VISEISRCentral();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRCentral Members
    STDMETHODIMP              ModeGet(PSRMODEINFOA);
    STDMETHODIMP              ModeGet(PSRMODEINFOW);
    STDMETHODIMP              GrammarLoad(SRGRMFMT, SDATA, PVOID, IID, LPUNKNOWN *);
    STDMETHODIMP              Pause();
    STDMETHODIMP              PosnGet(PQWORD);
    STDMETHODIMP              Resume();
    STDMETHODIMP              ToFileTime(PQWORD, FILETIME *);
    STDMETHODIMP              Register(PVOID, IID, DWORD *);
    STDMETHODIMP              UnRegister(DWORD);

  private:
    ULONG                     RefCnt;         // interface reference count
    LPVOID                    ObjPtr;         // back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // pointer to outer Unknown
};

typedef VISEISRCentral *PVISEISRCentral;


  // ISRAttributes Interface

class VISEISRAttributes : public ISRAttributes {
  public:
    VISEISRAttributes(LPVOID, LPUNKNOWN);
   ~VISEISRAttributes();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRAttributes Members
    STDMETHODIMP              AutoGainEnableGet(DWORD *);
    STDMETHODIMP              AutoGainEnableSet(DWORD);
    STDMETHODIMP              EchoGet(BOOL *);
    STDMETHODIMP              EchoSet(BOOL);
    STDMETHODIMP              EnergyFloorGet(WORD *);
    STDMETHODIMP              EnergyFloorSet(WORD);
    STDMETHODIMP              MicrophoneGet(PSTR, DWORD, DWORD *);
    STDMETHODIMP              MicrophoneSet(PCSTR);
    STDMETHODIMP              RealTimeGet(DWORD *);
    STDMETHODIMP              RealTimeSet(DWORD);
    STDMETHODIMP              SpeakerGet(PSTR, DWORD, DWORD *);
    STDMETHODIMP              SpeakerSet(PCSTR);
    STDMETHODIMP              TimeOutGet(DWORD *, DWORD *);
    STDMETHODIMP              TimeOutSet(DWORD, DWORD);
    STDMETHODIMP              ThresholdGet(DWORD *);
    STDMETHODIMP              ThresholdSet(DWORD);

  private:
    ULONG                     RefCnt;         // interface reference count
    LPVOID                    ObjPtr;         // back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // pointer to outer Unknown
};

typedef VISEISRAttributes *PVISEISRAttributes;


  // ISRDialogs interface

class VISEISRDialogs : public ISRDialogs {
  public:
    VISEISRDialogs(LPVOID, LPUNKNOWN);
   ~VISEISRDialogs();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

   // ISRDialogs members
    STDMETHODIMP              AboutDlg(HWND, PCSTR);
    STDMETHODIMP              GeneralDlg(HWND, PCSTR);
    STDMETHODIMP              LexiconDlg(HWND, PCSTR);
    STDMETHODIMP              TrainMicDlg(HWND, PCSTR);
    STDMETHODIMP              TrainGeneralDlg(HWND, PCSTR);

  private:
    ULONG                     RefCnt;         // interface reference count
    LPVOID                    ObjPtr;         // back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // pointer to outer Unknown
    MBOX_Att                  RAAttributes;
    MBOX                      ReturnAddress;
};

typedef VISEISRDialogs * PVISEISRDialogs;


  // ISRSpeaker interface

class VISEISRSpeaker : public ISRSpeaker {
  public:
    VISEISRSpeaker(LPVOID, LPUNKNOWN);
   ~VISEISRSpeaker();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // ISRSpeaker Members
    STDMETHODIMP              Delete(PCSTR);
    STDMETHODIMP              Enum(PSTR *, DWORD *);
    STDMETHODIMP              Merge(PCSTR, PVOID, DWORD);
    STDMETHODIMP              New(PCSTR);
    STDMETHODIMP              Query(PSTR, DWORD, DWORD *);
    STDMETHODIMP              Read(PCSTR, PVOID *, DWORD *);
    STDMETHODIMP              Revert(PCSTR);
    STDMETHODIMP              Select(PCSTR, BOOL);
    STDMETHODIMP              Write(PCSTR, PVOID, DWORD);

  private:
    ULONG                     RefCnt;         // interface reference count
    LPVOID                    ObjPtr;         // back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // pointer to outer Unknown
    MBOX_Att                  RAAttributes;
    MBOX                      ReturnAddress;
};

typedef VISEISRSpeaker * PVISEISRSpeaker;


  // IVbxEngine interface - this is a Verbex extension

class VISEIVbxEngine : public IVbxEngine {
  public:
    VISEIVbxEngine(LPVOID, LPUNKNOWN);
   ~VISEIVbxEngine();

    // These IUnknown methods delegate to the container Unknown
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    // IVbxEngine Members
    STDMETHODIMP              SetParameter(DWORD param);
    STDMETHODIMP              SetParameter(DWORD param, INT value);
    STDMETHODIMP              GetParameter(DWORD param, PINT pValue);
    STDMETHODIMP              SetParameter(DWORD param, FLOAT value);
    STDMETHODIMP              GetParameter(DWORD param, PFLOAT pValue);
    STDMETHODIMP              SetParameter(DWORD param, PCSTR value);
    STDMETHODIMP              GetParameter(DWORD param, PSTR value);
    STDMETHODIMP              SetTimeout(UINT timeout);

  private:
    ULONG                     RefCnt;         // interface reference count
    LPVOID                    ObjPtr;         // back pointer to the object
    LPUNKNOWN                 UnknwnPtr;      // pointer to outer Unknown
    MBOX_Att                  RAAttributes;
    MBOX                      ReturnAddress;
};

typedef VISEIVbxEngine * PVISEIVbxEngine;


  // Definition of the VISE SAPI Object

class VISEObj : public IUnknown {
  public:
    VISEObj(LPUNKNOWN, LPUNKNOWN, PCSTR);
   ~VISEObj();

    // IUnknown does not delegate
    STDMETHODIMP              QueryInterface(REFIID, LPVOID *);
    STDMETHODIMP_(ULONG)      AddRef(void);
    STDMETHODIMP_(ULONG)      Release(void);

    EKB                       EngineKB();           // The Engine KB
    Bool                      Pause();
    Bool                      Resume();
    STDMETHODIMP              Activated(HRESULT);
    STDMETHODIMP              Calibrated(HRESULT);
    STDMETHODIMP              Recognized(HRESULT);

  protected:
    ULONG                     RefCnt;
    LPUNKNOWN                 UnknwnPtr;

    PVISEISRCentral           VISEISRCentralPtr;
    PVISEISRAttributes        VISEISRAttributesPtr;
    PVISEISRDialogs           VISEISRDialogsPtr;
    PVISEISRSpeaker           VISEISRSpeakerPtr;
    PVISEIVbxEngine           VISEIVbxEnginePtr;
    PVISEIVbxSpkrTrain        VISEIVbxSpkrTrainPtr;

  private:
    IMemory                   Allocator;
    IMail                   * Mail;

    LPUNKNOWN                 AudioUnknwnPtr;
    IAudio                  * IAudioPtr;

    PVISEEngineObj            EngineObjPtr;         // The Engine Object

    EKB                       EKb;                  // The Engine KB

    MBOX                      ControlMBox;
    pthread_t                 ControlThread;
    void                      ControlMain();

    Int                       PauseCount;
    CRITSECT                  PauseCountCritsect;

    ISRNotifySink           * NotifySinkPtr;
    CRITSECT                  NotifySinkCritsect;
    PVISENotifyObj            NotifyObjPtr;         // The VU Meter Notification Object

    PVISEGrammarRequest       ActiveRulePtr;
    PVISESpeakerProfile       ActiveSpkrPtr;
    PVISEKbObj                ActiveKbPtr;
    STDMETHODIMP              LaunchRecognition(Bool);
    void                      ActiveKbUpdate(PVISEGrammarRequest);
    HASH                      SpkrDirPool;
    PVISESpkrDirObj           SpkrDirPtr;
    UKEYPOOL                  KeyPool;

    Int                       MaxNumHyps;

    CRITSECT_DECLARE(RefCntCritSect)

  // Interfaces are friends
  friend class VISEISRCentral;
  friend class VISEISRAttributes;
  friend class VISEISRDialogs;
  friend class VISEISRSpeaker;
  friend class VISEIVbxEngine;
  friend class VISEIVbxSpkrTrain;

  // Thread start routines are friends
  friend void VISEOBJ_controlThread(VISEObj *);
};

typedef VISEObj *PVISEObj;

#endif  /* _VISEOBJ_HH_ */
