#include "pthr.h"
#if defined(CELIB)
#include "celib.h"
#endif
#include <stdio.h>
#include <string.h>

// Verbex SAPI Objects
#include "viseobj.hh"
#include "viseengine.hh"
#include "visephrase.hh"
#include "visegram.hh"
#include "visekb.hh"
#include "visemessage.hh"
#include "visenotify.hh"
#include "visetrain.hh"
#include "viseresults.hh"

// VULCAN Inclusions
#include "vocab.h"
#include "vfile.h"
#include "syntax.h"
#include "ekb.h"
#include "kb.h"
#include "vbxtypes.h"
#include "viseparm.h"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "hash.h"
#include "ifaces.h"
#include "imail.h"
#include "imemory.h"
#include "mbox.h"
#include "critsect.h"
#include "ukey.h"

#ifdef DEBUG
#define VBX_DEBUG(P)              P
#define VBX_IF_NOT_DO(C,P)        if (!(C)) (P)
#define VBX_VERBOSE               TRUE
#define VBX_FATALSTATUS(R,S)      { Int v = (R); if (R) FATAL_BRA_ "condition = %d; status = %s", v, VISEERR_string(S) _KET; }
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)        C
#define VBX_VERBOSE               FALSE
#define VBX_FATALSTATUS(R,S)
#endif

#define VISE_LOGFILENAME_DEFAULT  "visedbg.log"
#define VISE_SPEAKERNAME_DEFAULT  "Default"

#define VISE_MAXPATHSTRLEN        256
#define VISE_NHYPOTHESES_MINIMUM  0
#define VISE_NHYPOTHESES_DEFAULT  5
#define VISE_NHYPOTHESES_MAXIMUM  32

#define UKEYPOOLSIZE_DEFAULT      100
#define SPDIRPOOLSIZE_DEFAULT     5

// An arbitrary key
#define NOTIFY_SINK_KEY           713

// Internal function prototypes
void VISEOBJ_controlThread(VISEObj *);

VISEObj::VISEObj(LPUNKNOWN unknwnPtr, LPUNKNOWN audioUnknwnPtr, PCSTR cfgFileName):
RefCnt(0),
UnknwnPtr(unknwnPtr),
AudioUnknwnPtr(audioUnknwnPtr),
IAudioPtr(NULL),
PauseCount(0),
NotifySinkPtr(NULL),
NotifyObjPtr(NULL),
ActiveRulePtr(NULL),
ActiveSpkrPtr(NULL),
ActiveKbPtr(NULL),
SpkrDirPtr(NULL),
SpkrDirPool(NULL),
MaxNumHyps(VISE_NHYPOTHESES_DEFAULT)
{
  LPUNKNOWN unknwn = (UnknwnPtr == NULL) ? (LPUNKNOWN) this : UnknwnPtr;

   // Get an IAudio
  if (AudioUnknwnPtr)
    VBX_fatalIf(AudioUnknwnPtr->QueryInterface(IID_IAudio, (PVOID *) &IAudioPtr));
   // Instantiate the memory allocation interface
  Allocator.Calloc = IMEM_calloc;
  Allocator.Free = IMEM_free;

   // Instantiate the CRITSECT for RefCnt
  CRITSECT_CREATE(RefCntCritSect, &Allocator, NULL);

   // Instantiate the mail system
  Mail = IMAIL_Create(&Allocator, VUNSMAX);
  VBX_fatalIf(!Mail);

   // Create and load the engine knowledge base
  EKb = EKB_create();
  VBX_fatalIf(!EKb);
  EKB_load(EKb, (Ptr) cfgFileName, VBX_VERBOSE);

   // Create a pool of unique keys
  KeyPool = UKEYPOOL_create(UKEYPOOLSIZE_DEFAULT);
  VBX_fatalIf(!KeyPool);

   // Create the pool of speaker directories
  SpkrDirPool = HASH_create(SPDIRPOOLSIZE_DEFAULT, NULL, NULL, NULL);
  VBX_fatalIf(!SpkrDirPool);

   // Create the critical sections
  VBX_fatalIf(!CRITSECT_CREATE(NotifySinkCritsect, &Allocator, NULL));
  VBX_fatalIf(!CRITSECT_CREATE(PauseCountCritsect, &Allocator, NULL));

   // Set up the control mailbox
  VBX_fatalIf(!(ControlMBox = MBOX_create(&Allocator, (MBOX_Att) NULL)));

   // Instantiate the engine object
  VBX_fatalIf(!(EngineObjPtr = VISE_createEngine(&Allocator, Mail, this, AudioUnknwnPtr)));

   // Start the control thread
  VBX_fatalIf(pthread_create(&ControlThread, NULL, (pthread_startroutine_t) VISEOBJ_controlThread, (void *) this));
  VBX_DEBUG(VBX_print("VISEObj::VISEObj: control thread started\n"));

   // Instantiate the contained interfaces
  VBX_fatalIf(!(VISEISRCentralPtr = new VISEISRCentral(this, unknwn)));
  VBX_fatalIf(!(VISEISRAttributesPtr = new VISEISRAttributes(this, unknwn)));
  VBX_fatalIf(!(VISEISRDialogsPtr = new VISEISRDialogs(this, unknwn)));
  VBX_fatalIf(!(VISEISRSpeakerPtr = new VISEISRSpeaker(this, unknwn)));
  VBX_fatalIf(!(VISEIVbxEnginePtr = new VISEIVbxEngine(this, unknwn)));
  VBX_fatalIf(!(VISEIVbxSpkrTrainPtr = new VISEIVbxSpkrTrain(this, unknwn)));

  VBX_DEBUG(VBX_print("VISEObj::VISEObj: DONE\n"));
}

VISEObj::~VISEObj()
{
  void  * status;

   // Shut down the control thread
  PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_EXIT, NULL, NULL);
  VBX_fatalIf(!MBOX_insert(ControlMBox, (OBJT) serviceReqPtr));
  VBX_DEBUG(VBX_print("  VISEObj::~VISEObj: sent SAPI_EXIT to ControlThread\n"));
  pthread_join(ControlThread, &status);

   // Free the engine object
  delete EngineObjPtr;

   // Release the notification sink
  CRITSECT_ENTER(NotifySinkCritsect);
  if (NotifySinkPtr) 
    NotifySinkPtr->Release();
  CRITSECT_LEAVE(NotifySinkCritsect);

   // Free the contained interfaces
  VBX_DEBUG(VBX_print("  VISEObj::~VISEObj(): freeing contained interfaces\n"));
  delete VISEISRCentralPtr;
  delete VISEISRAttributesPtr;
  delete VISEISRDialogsPtr;
  delete VISEISRSpeakerPtr;
  delete VISEIVbxEnginePtr;
  delete VISEIVbxSpkrTrainPtr;

  VBX_DEBUG(VBX_print("  VISEObj::~VISEObj(): annihilating speaker pool objects\n"));
   // Annihilate the pool of unique keys
  UKEYPOOL_annihilate(KeyPool);

   // Annihilate the pool of speaker directories
  HASH_processSymbols(SpkrDirPool, NULL);
  HASH_annihilate(SpkrDirPool);

   // Release the SpkrDir object, if any
  if (SpkrDirPtr) SpkrDirPtr->Release();

   // Destroy the engine knowledge base
  EKB_destroy(EKb);

  VBX_DEBUG(VBX_print("  VISEObj::~VISEObj(): destroying mailboxes\n"));
  MBOX_destroy(ControlMBox);

  VBX_DEBUG(VBX_print("  VISEObj::~VISEObj(): destroying critical sections\n"));
  CRITSECT_destroy(NotifySinkCritsect);
  CRITSECT_destroy(PauseCountCritsect);

   // Destroy the mail system
  VBX_DEBUG(VBX_print("  VISEObj::~VISEObj(): destroying mail system\n"));
  IMAIL_Destroy(Mail);

   // Destroy the RefCnt CRITSECT
  CRITSECT_DESTROY(RefCntCritSect);

  if (IAudioPtr) {
    // Release the IAudio interface
    VBX_DEBUG(VBX_print("  VISEObj::~VISEObj(): releasing the IAudio interface\n"));
    IAudioPtr->Release();
  }
}

STDMETHODIMP
VISEObj::QueryInterface(REFIID riid, LPVOID *ppv)
{
 *ppv = NULL;

   // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown))
    *ppv = (LPVOID) this;
  else if (IsEqualIID(riid, IID_ISRCentralA))
    *ppv = VISEISRCentralPtr;
  else if (IsEqualIID(riid, IID_ISRCentralW))
    *ppv = VISEISRCentralPtr;
  else if (IsEqualIID(riid, IID_ISRAttributes))
    *ppv = VISEISRAttributesPtr;
  else if (IsEqualIID(riid, IID_ISRDialogs))
    *ppv = VISEISRDialogsPtr;
  else if (IsEqualIID(riid, IID_ISRSpeaker))
    *ppv = VISEISRSpeakerPtr;
  else if (IsEqualIID(riid, IID_IVbxEngine))
    *ppv = VISEIVbxEnginePtr;
  else if (IsEqualIID(riid, IID_IVbxSpkrTrain))
    *ppv = VISEIVbxSpkrTrainPtr;

   // Update the reference count here
  if (*ppv != NULL) {
    ((LPUNKNOWN) *ppv)->AddRef();
    return NOERROR;
  }
  return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG)
VISEObj::AddRef()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEObj::AddRef(): RefCnt for VISEObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISEObj::Release()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEObj::Release(): RefCnt for VISEObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) delete this;
  return refCnt;
}

EKB
VISEObj::EngineKB()
{
  return EKb;
}

Bool
VISEObj::Pause()
{
  Bool paused;

  CRITSECT_ENTER(PauseCountCritsect);
  paused = !PauseCount++;
  VBX_DEBUG(VBX_print("  %s (%d) after Pause\n", PauseCount ? "PAUSED" : "NOT PAUSED", PauseCount));
  CRITSECT_LEAVE(PauseCountCritsect);

   // Returns TRUE iff PauseCount goes from 0 to 1
  return paused;
}

Bool
VISEObj::Resume()
{
  Bool resumed;

  CRITSECT_ENTER(PauseCountCritsect);
  resumed = PauseCount ? !--PauseCount : FALSE;
  VBX_DEBUG(VBX_print("  %s (%d) after Resume\n", PauseCount ? "PAUSED" : "NOT PAUSED", PauseCount));
  CRITSECT_LEAVE(PauseCountCritsect);

   // Returns TRUE iff PauseCount goes from 1 to 0
  return resumed;
}

STDMETHODIMP
VISEObj::Activated(HRESULT result)
{
  PVISEServiceRequest  serviceReqPtr = new VISEServiceRequest(SAPI_ACTIVATED, (Ptr) result, NULL);
  VBX_DEBUG(VBX_print("  VISEObj::Activated() sending %s to ControlThread\n", SAPIVISECmd_string(SAPI_ACTIVATED)));
  MBOX_insert(ControlMBox, (OBJT) serviceReqPtr);
  return NOERROR;
}

STDMETHODIMP
VISEObj::Calibrated(HRESULT result)
{
  PVISEServiceRequest  serviceReqPtr = new VISEServiceRequest(SAPI_CALIBRATED, (Ptr) result, NULL);
  VBX_DEBUG(VBX_print("  VISEObj::Calibrated() sending %s to ControlThread\n", SAPIVISECmd_string(SAPI_CALIBRATED)));
  MBOX_insert(ControlMBox, (OBJT) serviceReqPtr);
  return NOERROR;
}

STDMETHODIMP
VISEObj::Recognized(HRESULT result)
{
  PVISEServiceRequest  serviceReqPtr = new VISEServiceRequest(SAPI_RECOGNIZED, (Ptr) result, NULL);
  VBX_DEBUG(VBX_print("  VISEObj::Recognized() sending %s to ControlThread\n", SAPIVISECmd_string(SAPI_RECOGNIZED)));
  MBOX_insert(ControlMBox, (OBJT) serviceReqPtr);
  return NOERROR;
}


  // Methods for VISEISRCentral

VISEISRCentral::VISEISRCentral(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRCentral::~VISEISRCentral()
{
}

STDMETHODIMP
VISEISRCentral::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRCentral::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRCentral::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRCentral::ModeGet(PSRMODEINFOA modeInfoPtr)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRCentral::ModeGet(PSRMODEINFOW modeInfoPtr)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRCentral::GrammarLoad(SRGRMFMT fmt, SDATA grammar, PVOID notifyIfacePtr, IID notifyIfaceIID, LPUNKNOWN *unknwnPtr)
{
  PVISEObj  objPtr = (PVISEObj) ObjPtr;
  IMemory * allocator = &objPtr->Allocator;

   // Check the type of the grammar
  if (fmt != SRGRMFMT_CFGNATIVE)
    return SRERR_GRAMMARWRONGTYPE;

   // Check that the proffered interface is a notification interface
  if (notifyIfacePtr && !IsEqualIID(notifyIfaceIID, IID_ISRGramNotifySink)) 
    return SRERR_INVALIDINTERFACE;

   // Construct a Recognition Stream to hold the grammar and create the grammar object

  PVISERecStreamObj  recStreamPtr = new VISERecStreamObj(grammar);
  if (!recStreamPtr) return E_OUTOFMEMORY;
  if (!recStreamPtr->Init(objPtr->EKb)) return SRERR_GRAMMARERROR;
  PVISEGrammarObj grammarObjPtr = new VISEGrammarObj((ISRGramNotifySink *) notifyIfacePtr, recStreamPtr, allocator, objPtr->ControlMBox);

  *unknwnPtr = (LPUNKNOWN) grammarObjPtr;

  if (!grammarObjPtr) return E_OUTOFMEMORY;
  grammarObjPtr->AddRef();

  return SRERR_NONE;
}

STDMETHODIMP
VISEISRCentral::Pause()
{
  PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_PAUSE, NULL, NULL);
  if (MBOX_insert(((PVISEObj) ObjPtr)->ControlMBox, (OBJT) serviceReqPtr)) {
    VBX_DEBUG(VBX_print("  ISRCentral::Pause: sent SAPI_PAUSE to ControlThread\n"));
    return SRERR_NONE;
  } else {
    VBX_DEBUG(VBX_print("  ISRCentral::Pause: can't send SAPI_PAUSE to ControlThread\n"));
    return E_UNEXPECTED;
  }
}

STDMETHODIMP
VISEISRCentral::PosnGet(PQWORD posnPtr)
{
  PVISEObj   objPtr = (PVISEObj) ObjPtr;
  IAudio   * audioPtr = objPtr->IAudioPtr;

  return (audioPtr) ? audioPtr->PosnGet(posnPtr) : SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRCentral::Resume()
{
  PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_RESUME, NULL, NULL);
  if (MBOX_insert(((PVISEObj) ObjPtr)->ControlMBox, (OBJT) serviceReqPtr)) {
    VBX_DEBUG(VBX_print("  ISRCentral::Resume: sent SAPI_RESUME to ControlThread\n"));
    return SRERR_NONE;
  } else {
    VBX_DEBUG(VBX_print("  ISRCentral::Resume: can't send SAPI_RESUME to ControlThread\n"));
    return E_UNEXPECTED;
  }
}

STDMETHODIMP
VISEISRCentral::ToFileTime(PQWORD quadPtr, FILETIME *ftPtr)
{
  PVISEObj   objPtr = (PVISEObj) ObjPtr;
  IAudio   * audioPtr = objPtr->IAudioPtr;

  return (audioPtr) ? audioPtr->ToFileTime(quadPtr, ftPtr) : SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRCentral::Register(PVOID notifyIfacePtr, IID notifyIfaceIID, DWORD *key)
{
  HRESULT  result = SRERR_NONE;
  PVISEObj objPtr = (PVISEObj) ObjPtr;

  CRITSECT_ENTER(objPtr->NotifySinkCritsect);
  if (!objPtr->NotifySinkPtr) {
    if (IsEqualIID(notifyIfaceIID, IID_ISRNotifySink)) {
      objPtr->NotifySinkPtr = (ISRNotifySink *) notifyIfacePtr;
      objPtr->NotifySinkPtr->AddRef();
      objPtr->NotifyObjPtr = new VISENotifyObj(&objPtr->Allocator, objPtr->Mail, objPtr->NotifySinkPtr, objPtr->EngineObjPtr->Source());
      *key = NOTIFY_SINK_KEY;
    } else {
      result = SRERR_INVALIDINTERFACE;
    }
  }
  CRITSECT_LEAVE(objPtr->NotifySinkCritsect);

  return result;
}

STDMETHODIMP
VISEISRCentral::UnRegister(DWORD key)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;

  if (key == NOTIFY_SINK_KEY) {
    if (objPtr->NotifySinkPtr) {
      CRITSECT_ENTER(objPtr->NotifySinkCritsect);
      if (objPtr->NotifyObjPtr != NULL) {
        delete objPtr->NotifyObjPtr;
        objPtr->NotifyObjPtr = NULL;
      }
      objPtr->NotifySinkPtr->Release();
      objPtr->NotifySinkPtr = NULL;
      CRITSECT_LEAVE(objPtr->NotifySinkCritsect);
    }
    return SRERR_NONE;
  } else {
    return SRERR_INVALIDKEY;
  }
}


  // ISRAttributes Interface

VISEISRAttributes::VISEISRAttributes(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
  return;
}

VISEISRAttributes::~VISEISRAttributes()
{
}

STDMETHODIMP
VISEISRAttributes::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRAttributes::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRAttributes::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRAttributes::AutoGainEnableGet(DWORD *gainPtr)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::AutoGainEnableSet(DWORD gain)
{
  // PVISEObj objPtr = (PVISEObj) ObjPtr;
  // CRITSECT_ENTER(objPtr->NotifySinkCritsect);
  // if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_AUTOGAINENABLE);
  // CRITSECT_LEAVE(objPtr->NotifySinkCritsect);

  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::EchoGet(BOOL *echo)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::EchoSet(BOOL echo)
{
  // PVISEObj objPtr = (PVISEObj) ObjPtr;
  // CRITSECT_ENTER(objPtr->NotifySinkCritsect);
  // if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_ECHO);
  // CRITSECT_LEAVE(objPtr->NotifySinkCritsect);

  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::EnergyFloorGet(WORD *eFloorPtr)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::EnergyFloorSet(WORD eFloor)
{
  // PVISEObj objPtr = (PVISEObj) ObjPtr;
  // CRITSECT_ENTER(objPtr->NotifySinkCritsect);
  // if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_ENERGYFLOOR);
  // CRITSECT_LEAVE(objPtr->NotifySinkCritsect);

  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::MicrophoneGet(PSTR micNameBuf, DWORD micNameBufLen, 
                                 DWORD *micNameBufMinLen)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::MicrophoneSet(PCSTR micNameBuf)
{
  // PVISEObj objPtr = (PVISEObj) ObjPtr;
  // CRITSECT_ENTER(objPtr->NotifySinkCritsect);
  // if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_MICROPHONE);
  // CRITSECT_LEAVE(objPtr->NotifySinkCritsect);

  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::RealTimeGet(DWORD *rtFractionPtr)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::RealTimeSet(DWORD rtFraction)
{
  // PVISEObj objPtr = (PVISEObj) ObjPtr;
  // CRITSECT_ENTER(objPtr->NotifySinkCritsect);
  // if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_REALTIME);
  // CRITSECT_LEAVE(objPtr->NotifySinkCritsect);

  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRAttributes::SpeakerGet(PSTR spkrNameBuf, DWORD spkrNameBufLen, DWORD *spkrNameMinLen)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  return objPtr->VISEISRSpeakerPtr->Query(spkrNameBuf, spkrNameBufLen, spkrNameMinLen);
}

STDMETHODIMP
VISEISRAttributes::SpeakerSet(PCSTR spkrNameBuf)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  return objPtr->VISEISRSpeakerPtr->Select(spkrNameBuf, FALSE);
}

STDMETHODIMP
VISEISRAttributes::TimeOutGet(DWORD *initialTO, DWORD *finalTO)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  INT      value;
  HRESULT  hresult;

  if ((hresult = objPtr->EngineObjPtr->GetParameter(VISE_SILMAXDWELL, &value)) == SRERR_NONE) {
    *initialTO = (DWORD) value;
    hresult = objPtr->EngineObjPtr->GetParameter(VISE_ENDMINDWELL, &value);
    *finalTO = (DWORD) value;
  }

  return hresult;
}

STDMETHODIMP
VISEISRAttributes::TimeOutSet(DWORD initialTO, DWORD finalTO)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  hresult;
  
  if (((hresult = objPtr->EngineObjPtr->SetParameter(VISE_ENDMINDWELL, (INT) finalTO)) == SRERR_NONE) &&
      ((hresult = objPtr->EngineObjPtr->SetParameter(VISE_SILMAXDWELL, (INT) initialTO)) == SRERR_NONE)) {
    CRITSECT_ENTER(objPtr->NotifySinkCritsect);
    if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_TIMEOUT);
    CRITSECT_LEAVE(objPtr->NotifySinkCritsect);
  }

  return hresult;
}

STDMETHODIMP
VISEISRAttributes::ThresholdGet(DWORD * threshold)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  INT      threshInt;

  HRESULT  hresult = objPtr->EngineObjPtr->GetParameter(VISE_WCTHRESHOLD, &threshInt);
  *threshold = threshInt;
  return hresult;
}

STDMETHODIMP
VISEISRAttributes::ThresholdSet(DWORD threshold)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  hresult;

  if (threshold > 100) { 
    hresult = SRERR_INVALIDPARAM;
  } else if ((hresult = objPtr->EngineObjPtr->SetParameter(VISE_WCTHRESHOLD, (INT) threshold)) == SRERR_NONE) {
    CRITSECT_ENTER(objPtr->NotifySinkCritsect);
    if (objPtr->NotifySinkPtr) objPtr->NotifySinkPtr->AttribChanged(ISRNSAC_THRESHOLD);
    CRITSECT_LEAVE(objPtr->NotifySinkCritsect);
  }

  return hresult;
}


  // ISRDialogs Interface

VISEISRDialogs::VISEISRDialogs(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{ 
  IMemory * allocator = &((PVISEObj) ObjPtr)->Allocator;
  VBX_fatalIf(!(RAAttributes = MBOX_createAttr(allocator, 1, 1, 1)));
  VBX_fatalIf(!(ReturnAddress = MBOX_create(allocator, RAAttributes)));
}

VISEISRDialogs::~VISEISRDialogs()
{
  MBOX_destroy(ReturnAddress);
  VBX_free(RAAttributes);
}

STDMETHODIMP
VISEISRDialogs::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRDialogs::AddRef(void)
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRDialogs::Release(void)
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRDialogs::AboutDlg(HWND, PCSTR)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRDialogs::GeneralDlg(HWND, PCSTR)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRDialogs::LexiconDlg(HWND, PCSTR)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRDialogs::TrainMicDlg(HWND, PCSTR)
{
  HRESULT  hresult;
  PVISEObj objPtr = (PVISEObj) ObjPtr;

   // If the source of Jin data has been initialized, calibrate
  if (objPtr->ActiveSpkrPtr) {
     // Send a request for calibration to the control object
    PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_CALIBRATE, NULL, ReturnAddress);
    if (MBOX_insert(objPtr->ControlMBox, (OBJT) serviceReqPtr)) {
      VBX_DEBUG(VBX_print("  ISRDialogs::TrainMicDlg: sent SAPI_CALIBRATE to ControlThread\n"));
       // Await confirmation
      hresult = (HRESULT) MBOX_remove(ReturnAddress);
      VBX_DEBUG(VBX_print("  ISRDialogs::TrainMicDlg: received hresult %d from SAPI_CALIBRATE from Engine\n", hresult));
    } else {
      VBX_DEBUG(VBX_print("  ISRDialogs::TrainMicDlg: can't send SAPI_CALIBRATE to ControlThread\n"));
      hresult = E_UNEXPECTED;
    }

   // Otherwise, the source is not initialized, so calibration is not possible
  } else {
    hresult = SRERR_NOUSERSELECTED;
  }

  return hresult;
}

STDMETHODIMP
VISEISRDialogs::TrainGeneralDlg(HWND, PCSTR)
{
  return SRERR_NOTSUPPORTED;
}


  // ISRSpeaker Interface

VISEISRSpeaker::VISEISRSpeaker(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
  IMemory * allocator = &((PVISEObj) ObjPtr)->Allocator;
  VBX_fatalIf(!(RAAttributes = MBOX_createAttr(allocator, 1, 1, 1)));
  VBX_fatalIf(!(ReturnAddress = MBOX_create(allocator, RAAttributes)));
}

VISEISRSpeaker::~VISEISRSpeaker()
{
  MBOX_destroy(ReturnAddress);
  VBX_free(RAAttributes);
}

STDMETHODIMP
VISEISRSpeaker::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRSpeaker::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRSpeaker::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRSpeaker::Delete(PCSTR spkrName)
{
  return (((PVISEObj) ObjPtr)->SpkrDirPtr->RemoveEntry((Char *) spkrName) ? SRERR_NONE : SRERR_INVALIDPARAM);
}

STDMETHODIMP
VISEISRSpeaker::Enum(PSTR * enumBuffer, DWORD * bufferSize)
{
  *enumBuffer = ((PVISEObj) ObjPtr)->SpkrDirPtr->Names(NULL, (UInt *) bufferSize);

  return ((*enumBuffer) ? SRERR_NONE : E_OUTOFMEMORY);
}

STDMETHODIMP
VISEISRSpeaker::Merge(PCSTR spkrName, PVOID buffer, DWORD bufferSize)
{
  HRESULT hresult;

  PVISESpkrDirObj spkrDirPtr = ((PVISEObj) ObjPtr)->SpkrDirPtr;
  VBX_fatalIf(!spkrDirPtr);

   // If the speaker is unknown, return an exception
  if (!spkrDirPtr->HasAnEntry((Char *) spkrName))
    hresult = SRERR_NOUSERSELECTED;
   // Otherwise, currently just try to replace the old entry with the new one
  else if (spkrDirPtr->WriteEntry((Char *) spkrName, (Ptr) buffer, (UInt) bufferSize))
    hresult = SRERR_NONE;
   // If this fails, return an error
  else
    hresult = E_UNEXPECTED;

  return hresult;
}

STDMETHODIMP
VISEISRSpeaker::New(PCSTR spkrName)
{
  HRESULT hresult;

  PVISESpkrDirObj spkrDirPtr = ((PVISEObj) ObjPtr)->SpkrDirPtr;
  VBX_fatalIf(!spkrDirPtr);

   // Make sure the speaker doesn't already exist
  if (spkrDirPtr->HasAnEntry((Char *) spkrName))
    hresult = SRERR_SPEAKEREXISTS;
   // Otherwise, try to clone the default speaker profile
  else if (spkrDirPtr->CloneEntry(VISE_SPEAKERNAME_DEFAULT, (Char *) spkrName))
    hresult = SRERR_NONE;
   // If this fails, return an error
  else
    hresult = E_UNEXPECTED;

  return hresult;
}

STDMETHODIMP
VISEISRSpeaker::Query(PSTR spkrNameBuf, DWORD spkrNameBufLen, DWORD *spkrNameMinLen)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  UInt     nameLen;

  if (objPtr->ActiveSpkrPtr) {
    nameLen = strlen(objPtr->ActiveSpkrPtr->SpkrName());
    if (nameLen < spkrNameBufLen) {
      strcpy(spkrNameBuf, objPtr->ActiveSpkrPtr->SpkrName());
      return SRERR_NONE;
    } else {
      if (spkrNameMinLen)
        *spkrNameMinLen = nameLen;
      return SRERR_VALUEOUTOFRANGE;
    }
  } else {
    strcpy(spkrNameBuf, "");
    return SRERR_NOUSERSELECTED;
  }
}

STDMETHODIMP
VISEISRSpeaker::Read(PCSTR spkrName, PVOID * bufferPtr, DWORD * bufferSizePtr)
{
  *bufferPtr = (PVOID) ((PVISEObj) ObjPtr)->SpkrDirPtr->ReadEntry((Char *) spkrName, (UInt *) bufferSizePtr);
  return (*bufferPtr ? SRERR_NONE : SRERR_NOUSERSELECTED);
}

STDMETHODIMP
VISEISRSpeaker::Revert(PCSTR spkrName)
{
  return (((PVISEObj) ObjPtr)->SpkrDirPtr->RestoreEntry((Char *) spkrName) ? SRERR_NONE : SRERR_NOUSERSELECTED);
}

STDMETHODIMP
VISEISRSpeaker::Select(PCSTR spkrName, BOOL fLock)
{
  HRESULT  hresult;
  PVISEObj objPtr = (PVISEObj) ObjPtr;

   // If the requested speaker is the active speaker, do nothing
  if (objPtr->ActiveSpkrPtr && !strcmp(objPtr->ActiveSpkrPtr->SpkrName(), spkrName)) {
    hresult = SRERR_NONE;

   // Otherwise ...
  } else {
     // Get the voice stream for the speaker
    VBX_DEBUG(VBX_print("  ISRSpeaker::Select: Getting a voice stream object for \"%s\"\n", spkrName));
    PVISEVoiStreamObj  voiStreamObjPtr = objPtr->SpkrDirPtr->CreateVoiStream((Char *) spkrName);
    if (voiStreamObjPtr) {
       // Create a speaker profile object
      VBX_DEBUG(VBX_print("  ISRSpeaker::Select: creating VISESpeakerProfile for \"%s\"\n", spkrName));
      PVISESpeakerProfile spkrProfilePtr = new VISESpeakerProfile(SAPI_SETSPEAKER, voiStreamObjPtr, (Char *) spkrName, fLock, ReturnAddress);
       // Send it to RIT
      if (MBOX_insert(objPtr->ControlMBox, (OBJT) spkrProfilePtr)) {
        VBX_DEBUG(VBX_print("  ISRSpeaker::Select: sent SAPI_SETSPEAKER to ControlThread for speaker \"%s\"\n", spkrName));
         // Await confirmation
        hresult = (HRESULT) MBOX_remove(ReturnAddress);
        VBX_DEBUG(VBX_print("  ISRSpeaker::Select: received confirmation of SAPI_SETSPEAKER from Engine for speaker \"%s\"\n", spkrName));
      } else {
        VBX_DEBUG(VBX_print("  ISRSpeaker::Select: can't send SAPI_SETSPEAKER to ControlThread for speaker \"%s\"\n", spkrName));
        hresult = E_UNEXPECTED;
      }
    } else {
      VBX_DEBUG(VBX_print("  ISRSpeaker::Select: can't get a voice stream object for \"%s\"\n", spkrName));
      hresult = SRERR_NOUSERSELECTED;
    }
  }

  return hresult;
}

STDMETHODIMP
VISEISRSpeaker::Write(PCSTR spkrName, PVOID buffer, DWORD bufferSize)
{
  HRESULT hresult;
  PVISESpkrDirObj spkrDirPtr = ((PVISEObj) ObjPtr)->SpkrDirPtr;
  VBX_fatalIf(!spkrDirPtr);

   // If the speaker already has an entry in the speaker directory, return an exception
  if (spkrDirPtr->HasAnEntry((Char *) spkrName))
    hresult = SRERR_SPEAKEREXISTS;
   // Otherwise, try to create a new entry and write the contents of the buffer into it
  else if (spkrDirPtr->WriteEntry((Char *) spkrName, (Ptr) buffer, (UInt) bufferSize))
    hresult = SRERR_NONE;
   // If this fails, return an error
  else
    hresult = E_UNEXPECTED;

  return hresult;
}


  // IVbxEngine Interface

VISEIVbxEngine::VISEIVbxEngine(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
  IMemory * allocator = &((PVISEObj) ObjPtr)->Allocator;
  VBX_fatalIf(!(RAAttributes = MBOX_createAttr(allocator, 1, 1, 1)));
  VBX_fatalIf(!(ReturnAddress = MBOX_create(allocator, RAAttributes)));
}

VISEIVbxEngine::~VISEIVbxEngine()
{
  MBOX_destroy(ReturnAddress);
  VBX_free(RAAttributes);
}

STDMETHODIMP
VISEIVbxEngine::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEIVbxEngine::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEIVbxEngine::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEIVbxEngine::SetParameter(DWORD param)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_NHYPOTHESES:
    result = SetParameter(VISE_NHYPOTHESES, VISE_NHYPOTHESES_DEFAULT);
    break;
  case VISE_SAMPSIGBITS:
    result = SetParameter(VISE_SAMPSIGBITS, objPtr->EngineObjPtr->MaxSigBitsDefault);
    break;
  case VISE_FRAMEIDLEMS:
    result = SetParameter(VISE_FRAMEIDLEMS, 0);
    break;
  default:
    if ((result = objPtr->EngineObjPtr->SetParameter(param)) == SRERR_INVALIDPARAM)
      if (objPtr->NotifyObjPtr) result = objPtr->NotifyObjPtr->SetParameter(param);
  }
  return result;
}

STDMETHODIMP
VISEIVbxEngine::SetParameter(DWORD param, INT value)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_NHYPOTHESES:
    if (value < VISE_NHYPOTHESES_MINIMUM || value > VISE_NHYPOTHESES_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      objPtr->MaxNumHyps = value;
    break;
  case VISE_SAMPSIGBITS: {
    PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_SETSIGBITS, (Ptr) value, ReturnAddress);
    if (MBOX_insert(objPtr->ControlMBox, (OBJT) serviceReqPtr)) {
      VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: sent SAPI_SETSIGBITS to ControlThread\n"));
       // Await confirmation
      result = (HRESULT) MBOX_remove(ReturnAddress);
      VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: received confirmation of SAPI_SETSIGBITS from Engine for MaxSigBits = %d\n", value));
    } else {
      VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: can't send SAPI_SETSIGBITS to ControlThread\n"));
      result = E_UNEXPECTED;
    }
    break;
  }
  case VISE_FRAMEIDLEMS:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    if ((result = objPtr->EngineObjPtr->SetParameter(param, value)) == SRERR_INVALIDPARAM)
      if (objPtr->NotifyObjPtr) result = objPtr->NotifyObjPtr->SetParameter(param, value);
  }
  return result;
}

STDMETHODIMP
VISEIVbxEngine::GetParameter(DWORD param, PINT pValue)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_NHYPOTHESES:
    *pValue = objPtr->MaxNumHyps;
    break;
  case VISE_FRAMEIDLEMS:
    *pValue = 0;
    break;
  default:
    if ((result = objPtr->EngineObjPtr->GetParameter(param, pValue)) == SRERR_INVALIDPARAM)
      if (objPtr->NotifyObjPtr) result = objPtr->NotifyObjPtr->GetParameter(param, pValue);
  }
  return result;
}
    
STDMETHODIMP
VISEIVbxEngine::SetParameter(DWORD param, FLOAT value)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  default:
    result = objPtr->EngineObjPtr->SetParameter(param, value);
  }
  return result;
}

STDMETHODIMP
VISEIVbxEngine::GetParameter(DWORD param, PFLOAT pValue)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  default:
    result = objPtr->EngineObjPtr->GetParameter(param, pValue);
  }
  return result;
}
    
STDMETHODIMP
VISEIVbxEngine::SetParameter(DWORD param, PCSTR spkrDir)
{
  PVISEObj         objPtr = (PVISEObj) ObjPtr;
  HRESULT          result = SRERR_NONE;
  PVISESpkrDirObj  oldSpkrDirPtr;

  switch (param) {
  case VISE_SPEAKERPATH:
    if (spkrDir) {
      VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: setting SPEAKERPATH to \"%s\"\n", spkrDir)); 
      oldSpkrDirPtr = objPtr->SpkrDirPtr;
      if (!HASH_lookup(objPtr->SpkrDirPool, (Ptr) spkrDir, (Ptr *) &objPtr->SpkrDirPtr)) {
        VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: Creating a new speaker directory object for \"%s\"\n", spkrDir));
        objPtr->SpkrDirPtr = VISE_createSpkrDirObj((Char *) spkrDir, objPtr->EKb);
        VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: objPtr->SpkrDirPtr @%p\n", objPtr->SpkrDirPtr));
        VBX_fatalIf(!objPtr->SpkrDirPtr);
        objPtr->SpkrDirPtr->AddRef();
        if (oldSpkrDirPtr) oldSpkrDirPtr->Release();
        Char * spkrDirDup = strdup(spkrDir);
        VBX_fatalIf(!spkrDirDup);
        VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: hashing spkrDirDup \"%s\"\n", spkrDirDup));
        VBX_fatalIf(!HASH_add(objPtr->SpkrDirPool, (Ptr) spkrDirDup, (Ptr) objPtr->SpkrDirPtr));
      } else {
        VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: A speaker directory object already exists for \"%s\"\n", spkrDir));
      }
    } else {
      VBX_DEBUG(VBX_print("  IVbxEngine::SetParameter: ERROR - specified SPEAKERPATH is NULL\n")); 
      result = SRERR_VALUEOUTOFRANGE;
    }
    break;
  case VISE_DFLTSPEAKER:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}

STDMETHODIMP
VISEIVbxEngine::GetParameter(DWORD param, PSTR spkrDir)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_SPEAKERPATH:
    strcpy(spkrDir, objPtr->SpkrDirPtr->DirPath());
    break;
  case VISE_DFLTSPEAKER:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}

STDMETHODIMP
VISEIVbxEngine::SetTimeout(UINT timeout)
{
  PVISEObj objPtr = (PVISEObj) ObjPtr;
  HRESULT hres = objPtr->EngineObjPtr->SetTimeout(timeout);
  return hres;
}

void
VISEOBJ_controlThread(VISEObj * viseObj)
{
  viseObj->ControlMain();
}

void
VISEObj::ControlMain()
{
  PVISEServiceRequest     serviceReqPtr = NULL;
  PVISEGrammarRequest     grammarReqPtr = NULL;
  PVISEResultsRequest     resultsReqPtr = NULL;
  PVISEResultsObj         resultsObjPtr;
  PVISESpeakerProfile     newSpeakerPtr = NULL;
  PSDATA                  prompts = NULL;
  SAPIVISECmd             service;
  MBOX                    returnAddress = NULL;
  MBOX                    activateRetAddr = NULL;
  MBOX                    calibrateRetAddr = NULL;
  HRESULT                 hresult = SRERR_NONE;
  void                  * status;


  VBX_print("ControlThread is ALIVE\n");

  while (TRUE) {
    serviceReqPtr = (PVISEServiceRequest) MBOX_remove(ControlMBox);
    returnAddress = serviceReqPtr->ReturnAddress();
    
    switch (service = serviceReqPtr->Service()) {

    case SAPI_ACTIVATE:

      grammarReqPtr = (PVISEGrammarRequest) serviceReqPtr;
      activateRetAddr = returnAddress;

      VBX_DEBUG(VBX_print("  ControlThread: received SAPI_ACTIVATE for rule @%p named = \"%s\"\n", grammarReqPtr, grammarReqPtr->RuleName()));

       // If there is currently no active rule, install the one specified in this request
      if (ActiveRulePtr == NULL) {
        ActiveRulePtr = grammarReqPtr;
        VBX_DEBUG(VBX_print("  ControlThread: SAPI_ACTIVATE set active rule to \"%s\"\n", grammarReqPtr->RuleName()));
         // Make sure the current KB is up-to-date
        ActiveKbUpdate(ActiveRulePtr);
        VBX_DEBUG(VBX_print("  ControlThread: ActiveKbUpdate returned, ActiveRulePtr = %p, ActiveKbPtr = %p\n", ActiveRulePtr, ActiveKbPtr));
         // If no rule is specified, use the "Start" rule
        if (!ActiveRulePtr->RuleName()) {
        	VBX_DEBUG(VBX_print("  ControlThread: ActiveRulePtr has no name\n"));
          ActiveRulePtr->SetRuleName(SYNTAX_name(KB_grammar(ActiveKbPtr->Kb(), KB_firstGrammarId(ActiveKbPtr->Kb()))));
          VBX_DEBUG(VBX_print("  ControlThread: Updated ActiveRulePtr name\n"));
        }
        VBX_DEBUG(VBX_print("  ControlThread: ActiveRulePtr name is %s\n", ActiveRulePtr->RuleName()));
         // If the requested rule is not the enrollment and calibration rule and is not a member of the current KB, return an exception
        if (strcmp(grammarReqPtr->RuleName(), ENROLLMENTGRAMMARNAME) && strcmp(grammarReqPtr->RuleName(), CALIBRATIONGRAMMARNAME) &&
            !KB_grammarByName(ActiveKbPtr->Kb(), grammarReqPtr->RuleName())) {
          hresult = SRERR_INVALIDRULE;
          VBX_DEBUG(VBX_print("  ControlThread: SAPI_ACTIVATE requested rule \"%s\" not in grammar\n", grammarReqPtr->RuleName()));
          ActiveRulePtr = NULL;
          delete grammarReqPtr;
        } else {
        	VBX_DEBUG(VBX_print("  ControlThread: SAPI_ACTIVATE requested rule \"%s\" is in grammar\n", grammarReqPtr->RuleName()));
          (void) EngineObjPtr->SourceAddRef();
          hresult = SRERR_NONE;
        }

       // Otherwise, there is already a rule active, so return an exception
      } else {
         // If no rule is specified, use the "Start" rule of the currently active Kb
        if (!grammarReqPtr->RuleName())
          grammarReqPtr->SetRuleName(SYNTAX_name(KB_grammar(ActiveKbPtr->Kb(), KB_firstGrammarId(ActiveKbPtr->Kb()))));
        if (strcmp(grammarReqPtr->RuleName(), ActiveRulePtr->RuleName()) || grammarReqPtr->GrammarObject() != ActiveRulePtr->GrammarObject())
          hresult = SRERR_TOOMANYGRAMMARS;
        else
          hresult = SRERR_RULEALREADYACTIVE;
        VBX_DEBUG(VBX_print("  ControlThread: SAPI_ACTIVATE attempted activation for rule \"%s\" with rule \"%s\" already active\n",
                            grammarReqPtr->RuleName(), ActiveRulePtr->RuleName()));
        delete grammarReqPtr;
      }

       // Report only failure here; SAPI_ACTIVATED will report success, if any
      if (hresult) {
        if (activateRetAddr) {
          MBOX_insert(activateRetAddr, (OBJT) hresult);
          activateRetAddr = NULL;
        }
      } else {
    	  VBX_DEBUG(VBX_print("  ControlThread: no errors to report, calling LaunchRecognition(TRUE);\n"));
         // Launch recognition via activation, thus requiring a SAPI_ACTIVATED acknowledgement
        LaunchRecognition(TRUE);
      }

      break;

    case SAPI_ACTIVATED:

      hresult = (HRESULT) serviceReqPtr->Argument();

      VBX_DEBUG(VBX_print("  ControlThread: SAPI_ACTIVATED (hresult = %8.8x)\n", hresult));

       // If the activation failed for any reason, remove the active rule
      if (hresult) {
        delete ActiveRulePtr;
        ActiveRulePtr = NULL;
      }

       // Report success or failure to the activator, if any
      if (activateRetAddr) {
        MBOX_insert(activateRetAddr, (OBJT) hresult);
        activateRetAddr = NULL;
      }

      delete serviceReqPtr;
      break;

    case SAPI_DEACTIVATE:

      grammarReqPtr = (PVISEGrammarRequest) serviceReqPtr;

      VBX_DEBUG(VBX_print("  ControlThread: received SAPI_DEACTIVATE for rule named = \"%s\"\n", grammarReqPtr->RuleName()));

       // If the currently active rule is the one specified in this request, deactivate it
      if (ActiveRulePtr != NULL &&
          (grammarReqPtr->RuleName() ||
           (ActiveKbPtr &&
            (grammarReqPtr->SetRuleName(SYNTAX_name(KB_grammar(ActiveKbPtr->Kb(), KB_firstGrammarId(ActiveKbPtr->Kb())))), grammarReqPtr->RuleName()))) &&
          !strcmp(grammarReqPtr->RuleName(), ActiveRulePtr->RuleName()) &&
          grammarReqPtr->GrammarObject() == ActiveRulePtr->GrammarObject()) {
        delete ActiveRulePtr;
        ActiveRulePtr = NULL;
        VBX_DEBUG(VBX_print("  ControlThread: SAPI_DEACTIVATE deleted active rule \"%s\"\n", grammarReqPtr->RuleName()));

         // Since there are now no active rules, deactivate the engine
        hresult = EngineObjPtr->Deactivate();

       // Otherwise, return an exception
      } else {
        VBX_DEBUG(VBX_print("  ControlThread: SAPI_DEACTIVATE attempted deactivation on inactive rule \"%s\"\n", grammarReqPtr->RuleName()));
        hresult = SRERR_RULENOTACTIVE;
      }
      delete grammarReqPtr;

      if (returnAddress) MBOX_insert(returnAddress, (OBJT) hresult);

      break;

    case SAPI_CALIBRATE:

      calibrateRetAddr = returnAddress;

       // Make sure the current KB is up-to-date
      ActiveKbUpdate(ActiveRulePtr);

       // Construct a results object for the engine to fill
      resultsObjPtr = new VISEResultsObj(&Allocator, NULL, ActiveKbPtr, MaxNumHyps, EngineObjPtr);

       // Request calibration
      hresult = EngineObjPtr->Calibrate(resultsObjPtr);

       // Report only failure here; SAPI_CALIBRATED will report success
      if (hresult && calibrateRetAddr) {
        MBOX_insert(calibrateRetAddr, (OBJT) hresult);
        calibrateRetAddr = NULL;
      }

      delete serviceReqPtr;
      break;

    case SAPI_CALIBRATED:

      hresult = (HRESULT) serviceReqPtr->Argument();

      VBX_DEBUG(VBX_print("  ControlThread: SAPI_CALIBRATED (hresult = %8.8x)\n", hresult));

      if (calibrateRetAddr) {
        MBOX_insert(calibrateRetAddr, (OBJT) hresult);
        calibrateRetAddr = NULL;
      }

      delete serviceReqPtr;
      break;

    case SAPI_LISTUNMODELLEDWORDS: {

      VBX_DEBUG(VBX_print("  ControlThread: SAPI_LISTUNMODELLEDWORDS\n"));

      prompts = (PSDATA) VBX_calloc(1, sizeof(SDATA));

       // Make sure there is an active speaker
      if (ActiveSpkrPtr) {
        PVISEGrammarObj  grammarObj = (PVISEGrammarObj) serviceReqPtr->Argument();
        PVISEKbObj       kbObjPtr;
         // Use the active KB if it is appropriate
        if (ActiveKbPtr && ActiveKbPtr->IsMadeOf(grammarObj->RecStream(), ActiveSpkrPtr->VoiStream(), ActiveSpkrPtr->SpkrName()))
          kbObjPtr = ActiveKbPtr;
         // Otherwise, make one up propter hoc
        else
          kbObjPtr = new VISEKbObj(EKb, grammarObj->RecStream(), ActiveSpkrPtr->VoiStream(), ActiveSpkrPtr->SpkrName(), KeyPool);
        kbObjPtr->AddRef();

         // Get the KB
        KB  kbPtr = kbObjPtr->Kb();;

         // Grammar ID is first grammar from KB (FIX THIS!!)
        UInt  grammarId = KB_firstGrammarId(kbPtr);
        UInt  vocabId = SYNTAX_vocabularyId(KB_grammar(kbPtr, grammarId));
        VBX_DEBUG(VBX_print("  ControlThread::SAPI_LISTUNMODELLEDWORDS: first vocab Id = %d, first grammar Id = %d\n", vocabId, grammarId));

         // Get the vocabulary
        VOCAB  vocab = KB_vocabulary(kbPtr, vocabId);
        VBX_fatalIf(vocab == NULL);

         // Allocate space for the list of unmodelled words
        const Int promptListGrowthQntm = 1024;
        UInt       promptListSize = promptListGrowthQntm; 
        Ptr       promptList;

        promptList = (Ptr) VBX_calloc(1, promptListSize);

         // Assemble the list of unmodelled words
        UInt     wid;
        Int      totalBytes = 0;
        Ptr      phrasePtr = promptList;
        VOCITER  vociter = VOCITER_create(vocab);
        while ((wid = VOCITER_nextWordId(vociter))) {
          if (!VOCAB_modelId(vocab, wid)) {
            Char * wordName = VOCAB_wordNameFromId(vocab, wid);

             // Construct an SRPHRASE containing the word
            PVISESAPIPhrase  sapiPhrasePtr = new VISESAPIPhrase(wordName, wid);

             // Reallocate if necessary
            if (totalBytes + sapiPhrasePtr->SRPhraseSize() > promptListSize) {
              promptList = (Ptr) VBX_realloc(promptList, promptListSize + promptListGrowthQntm);
              promptListSize += promptListGrowthQntm;
              phrasePtr = promptList + totalBytes;     // Because promptList may have changed
            }

            Int  phraseBytes = sapiPhrasePtr->SRPhraseSize();
            totalBytes += phraseBytes;
            memcpy(phrasePtr, sapiPhrasePtr->SRPhrase(), phraseBytes);
            phrasePtr += phraseBytes;

            delete sapiPhrasePtr;
          }
        }
        VOCITER_destroy(vociter);

        if (totalBytes == 0) {
          VBX_free(promptList);
          promptList = NULL;
        }

        prompts->pData = promptList;
        prompts->dwSize = totalBytes;

        kbObjPtr->Release();
        hresult = SRERR_NONE;

       // Else, no speaker is active, so nothing can be done
      } else {
        hresult = SRERR_NOUSERSELECTED;
      }

       // Create and send a reply
      PVISEReply  replyPtr = new VISEReply(SAPI_LISTUNMODELLEDWORDS, (Ptr) prompts, hresult);
      if (returnAddress) MBOX_insert(returnAddress, (OBJT) replyPtr);

      delete serviceReqPtr;
      break;
    }      
    case SAPI_SETSIGBITS:

      VBX_DEBUG(VBX_print("  ControlThread: received SAPI_SETSIGBITS\n"));

      hresult = EngineObjPtr->SetParameter(VISE_SAMPSIGBITS, (Int) serviceReqPtr->Argument());

      if (returnAddress) MBOX_insert(returnAddress, (OBJT) hresult);

      delete serviceReqPtr;
      break;
      
    case SAPI_SETSPEAKER:

      newSpeakerPtr = (PVISESpeakerProfile) serviceReqPtr;

      VBX_DEBUG(VBX_print("  ControlThread: received SAPI_SETSPEAKER\n"));

      hresult = EngineObjPtr->SetSpeaker(newSpeakerPtr);

      VBX_DEBUG(VBX_print("  ControlThread: hresult = %d\n", hresult));
      if (hresult == SRERR_NONE) {
        delete ActiveSpkrPtr;
        ActiveSpkrPtr = newSpeakerPtr;
         // Make sure the current KB (if any) is up-to-date
        ActiveKbUpdate(ActiveRulePtr);
        CRITSECT_ENTER(NotifySinkCritsect);
        if (NotifySinkPtr) NotifySinkPtr->AttribChanged(ISRNSAC_SPEAKER);
        CRITSECT_LEAVE(NotifySinkCritsect);
      } else {
        delete serviceReqPtr;
      }

      if (returnAddress) MBOX_insert(returnAddress, (OBJT) hresult);

      break;
      
    case SAPI_STARTTRAININGPASS:
    case SAPI_ENDTRAININGPASS:
    case SAPI_SAVEMODELS:

      grammarReqPtr = (PVISEGrammarRequest) serviceReqPtr;

      VBX_DEBUG(VBX_print("  ControlThread: received %s for rule @%p named = \"%s\"\n",\
                          SAPIVISECmd_string(service), grammarReqPtr, grammarReqPtr->RuleName() ? grammarReqPtr->RuleName() : "(NULL)"));

#ifndef NOBATCHTRAINING
       // If there is currently no active rule, use the one specified in this request
      if (!ActiveRulePtr) {
         // Make sure the current KB is up-to-date
        ActiveKbUpdate(grammarReqPtr);
         // If no rule is specified, use the "Start" rule
        if (!grammarReqPtr->RuleName())
          grammarReqPtr->SetRuleName(SYNTAX_name(KB_grammar(ActiveKbPtr->Kb(), KB_firstGrammarId(ActiveKbPtr->Kb()))));
       // Otherwise, if no rule is specified, use the currently active rule
      } else if (!grammarReqPtr->RuleName()) {
        grammarReqPtr->SetRuleName(ActiveRulePtr->RuleName());
      }

       // If the rule is not a member of the current KB, return an exception
      if (!KB_grammarByName(ActiveKbPtr->Kb(), grammarReqPtr->RuleName())) {
        hresult = SRERR_INVALIDRULE;
        VBX_DEBUG(VBX_print("  ControlThread: %s requested rule \"%s\" not in grammar\n", SAPIVISECmd_string(service), grammarReqPtr->RuleName()));
      } else {
         // If there is currently an active rule, and ...
        if (ActiveRulePtr &&
           // either the new request is from a different grammar, or ...
          (grammarReqPtr->GrammarObject() != ActiveRulePtr->GrammarObject() ||
           // the requested rule and the active rule do not share a vocabulary
          SYNTAX_vocabularyId(KB_grammarByName(ActiveKbPtr->Kb(), grammarReqPtr->RuleName())) !=
            SYNTAX_vocabularyId(KB_grammarByName(ActiveKbPtr->Kb(), ActiveRulePtr->RuleName())))) {
          hresult = SRERR_TOOMANYGRAMMARS;
          VBX_DEBUG(VBX_print("  ControlThread: %s requested rule \"%s\" with rule \"%s\" in another vocabulary already active\n",\
               SAPIVISECmd_string(service), grammarReqPtr->RuleName(), ActiveRulePtr->RuleName()));
        } else {
           // Construct a results object with zero-best
          resultsObjPtr = new VISEResultsObj(&Allocator, grammarReqPtr->GrammarObject(), ActiveKbPtr, 0, EngineObjPtr);
          resultsObjPtr->SetRuleName(grammarReqPtr->RuleName());
          switch (service) {
            case SAPI_STARTTRAININGPASS:
              hresult = EngineObjPtr->StartTrainingPass(resultsObjPtr);
              break;
            case SAPI_ENDTRAININGPASS:
               // BoolParam tells if it is final pass
              hresult = EngineObjPtr->EndTrainingPass(resultsObjPtr, grammarReqPtr->WordNames(), grammarReqPtr->BoolParam());
              break;
            case SAPI_SAVEMODELS:
               // IntParam means minTrainingCount for a model to be uploaded and saved
              hresult = EngineObjPtr->SaveModels(resultsObjPtr, grammarReqPtr->WordNames(), grammarReqPtr->IntParam());
              break;
          }

        }
      }
#else
      hresult = SRERR_NOTSUPPORTED;
#endif
      delete grammarReqPtr;

      if (returnAddress) MBOX_insert(returnAddress, (OBJT) hresult);

      break;

    case SAPI_NULLRESULTS:

       // Make sure that the active (default) KB is up-to-date
      ActiveKbUpdate(ActiveRulePtr);
       // Construct a skeletal results object
      resultsObjPtr = new VISEResultsObj(&Allocator, NULL, ActiveKbPtr, 0, EngineObjPtr);
       // Send a SAPI_NULLRESULTS reply containing the results object to returnAddress
      resultsReqPtr = new VISEResultsRequest(SAPI_NULLRESULTS, resultsObjPtr, "", FALSE, NULL);
      VBX_DEBUG(VBX_print("  ControlThread: resultsObj %p KB %p\n", resultsObjPtr, resultsObjPtr->Kb()->Kb()));
      if (returnAddress) MBOX_insert(returnAddress, (OBJT) resultsReqPtr);

      delete serviceReqPtr;
      break;

    case SAPI_PAUSE:

      if (Pause()) EngineObjPtr->Abort();
      VBX_DEBUG(VBX_print("  ControlThread: PauseCount %d after SAPI_PAUSE\n", PauseCount));

      delete serviceReqPtr;
      break;

    case SAPI_RESUME:

      Resume();
      VBX_DEBUG(VBX_print("  ControlThread: PauseCount %d after SAPI_RESUME\n", PauseCount));

      LaunchRecognition(FALSE);

      delete serviceReqPtr;
      break;

    case SAPI_RECOGNIZED:

      hresult = (HRESULT) serviceReqPtr->Argument();

      VBX_DEBUG(VBX_print("  ControlThread: SAPI_RECOGNIZED, PauseCount %d (hresult = %8.8x)\n", PauseCount, hresult));

      LaunchRecognition(FALSE);

      delete serviceReqPtr;
      break;

    case SAPI_ABORTED:

      VBX_DEBUG(VBX_print("  ControlThread: SAPI_ABORTED, PauseCount %d\n", PauseCount));

      delete serviceReqPtr;
      break;

    case SAPI_EXIT:

      VBX_DEBUG(VBX_print("  ControlThread: SAPI_EXIT exiting\n"));

      if (ActiveKbPtr) ActiveKbPtr->Release();
      delete ActiveRulePtr;
      delete ActiveSpkrPtr;
      delete serviceReqPtr;

      VBX_print("ControlThread has shut down\n");
      pthread_exit(NULL);

      break;

    default:

      delete serviceReqPtr;
      VBX_DEBUG(VBX_print("  ControlThread: unknown service (%d) requested\n", service));
    }
  }
}


STDMETHODIMP
VISEObj::LaunchRecognition(Bool activate)
{
  HRESULT  result;

   // Request recognition if there is an active rule and the engine is not paused
  if (ActiveRulePtr != NULL && !PauseCount) {
	VBX_DEBUG(VBX_print("  ControlThread: Launch conditions met, ActiveRule = %p, PauseCount = %d\n", ActiveRulePtr, PauseCount));
     // Construct a results object for the engine to fill
    PVISEResultsObj resultsObjPtr = new VISEResultsObj(&Allocator, ActiveRulePtr->GrammarObject(), ActiveKbPtr, MaxNumHyps, EngineObjPtr);
    resultsObjPtr->SetRuleName(ActiveRulePtr->RuleName());
     // Send an activation or recognition request to the engine
    if (activate)
      result = EngineObjPtr->Activate(resultsObjPtr, ActiveRulePtr);
    else
      result = EngineObjPtr->Recognize(resultsObjPtr, ActiveRulePtr);
  } else {
    result = SRERR_NONE;
    VBX_DEBUG(VBX_print("  ControlThread: Launch conditions not met, ActiveRule = %p, PauseCount = %d\n", ActiveRulePtr, PauseCount));
  }

  return(result);
}


void
VISEObj::ActiveKbUpdate(PVISEGrammarRequest rulePtr)
{
  PVISERecStreamObj  recStreamPtr = NULL;

  if (rulePtr) {
    recStreamPtr = rulePtr->GrammarObject()->RecStream();
  } else if (ActiveKbPtr && ActiveKbPtr->RecStream() && ActiveKbPtr->RecStream()->RecFile()) {
    recStreamPtr = ActiveKbPtr->RecStream();
  }
  VBX_DEBUG(VBX_print("  ControlThread: recStreamPtr is %p, ActiveSpkrPtr = %p\n", recStreamPtr, ActiveKbPtr));

  if (ActiveSpkrPtr && !(ActiveKbPtr && ActiveKbPtr->IsMadeOf(recStreamPtr, ActiveSpkrPtr->VoiStream(), ActiveSpkrPtr->SpkrName()))) {
    if (ActiveKbPtr) ActiveKbPtr->Release();
    ActiveKbPtr = new VISEKbObj(EKb, recStreamPtr, ActiveSpkrPtr->VoiStream(), ActiveSpkrPtr->SpkrName(), KeyPool);
    ActiveKbPtr->AddRef();
  }
  VBX_DEBUG(if (ActiveSpkrPtr && ActiveKbPtr) \
              VBX_print("  ControlThread: ActiveKbUpdate(ActiveKb = %p; RecStream = %p; VoiStream = %p; SpkrName = \"%s\"; ActiveRule = \"%s\")\n", \
                        ActiveKbPtr, ActiveKbPtr->RecStream(), ActiveKbPtr->VoiStream(), ActiveKbPtr->SpkrName(), \
                        (rulePtr && rulePtr->RuleName()) ? rulePtr->RuleName() : "(NULL)"));
}
