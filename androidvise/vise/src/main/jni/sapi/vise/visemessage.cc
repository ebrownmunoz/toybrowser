#include "pthr.h"
#include <string.h>

// Verbex SAPI Objects
#include "visegram.hh"
#include "visekb.hh"
#include "visemessage.hh"
#include "viseresults.hh"

// Verbex C Infrastructure
#include "types.h"
#include "mbox.h"
#include "vbx.h"


const Char *
SAPIVISECmd_string(SAPIVISECmd command)
{
  const Char * cmdString;

  switch (command) {
    case SAPI_ABORT:                cmdString = "SAPI_ABORT";                break;
    case SAPI_ABORTED:              cmdString = "SAPI_ABORTED";              break;
    case SAPI_ACTIVATE:             cmdString = "SAPI_ACTIVATE";             break;
    case SAPI_ACTIVATED:            cmdString = "SAPI_ACTIVATED";            break;
    case SAPI_CALIBRATE:            cmdString = "SAPI_CALIBRATE";            break;
    case SAPI_CALIBRATED:           cmdString = "SAPI_CALIBRATED";           break;
    case SAPI_DEACTIVATE:           cmdString = "SAPI_DEACTIVATE";           break;
    case SAPI_ENDTRAININGPASS:      cmdString = "SAPI_ENDTRAININGPASS";      break;
    case SAPI_ENROLLWORD:           cmdString = "SAPI_ENROLLWORD";           break;
    case SAPI_RETRANSCRIBE:         cmdString = "SAPI_RETRANSCRIBE";         break;
    case SAPI_EXIT:                 cmdString = "SAPI_EXIT";                 break;
    case SAPI_LISTUNMODELLEDWORDS:  cmdString = "SAPI_LISTUNMODELLEDWORDS";  break;
    case SAPI_MISSINGMODELS:        cmdString = "SAPI_MISSINGMODELS";        break;
    case SAPI_NULLRESULTS:          cmdString = "SAPI_NULLRESULTS";          break;
    case SAPI_PAUSE:                cmdString = "SAPI_PAUSE";                break;
    case SAPI_RECOGNIZE:            cmdString = "SAPI_RECOGNIZE";            break;
    case SAPI_RECOGNIZED:           cmdString = "SAPI_RECOGNIZED";           break;
    case SAPI_RESUME:               cmdString = "SAPI_RESUME";               break;
    case SAPI_SAVEMODELS:           cmdString = "SAPI_SAVEMODELS";           break;
    case SAPI_SETSIGBITS:           cmdString = "SAPI_SETSIGBITS";           break;
    case SAPI_SETSPEAKER:           cmdString = "SAPI_SETSPEAKER";           break;
    case SAPI_STARTTRAININGPASS:    cmdString = "SAPI_STARTTRAININGPASS";    break;
    case SAPI_TRAIN:                cmdString = "SAPI_TRAIN";                break;
    default:                        cmdString = "";                          break;
  }

  return cmdString;
}


  // VISE Reply
VISEReply::VISEReply(SAPIVISECmd service, Ptr results, HRESULT hresult):
CMService(service),
CMResults(results),
CMHResult(hresult)
{
}

VISEReply::~VISEReply(void)
{
}

SAPIVISECmd
VISEReply::Service(void)
{
  return CMService;
}

void
VISEReply::SetService(SAPIVISECmd service)
{
  CMService = service;
}

HRESULT
VISEReply::HResult(void)
{
  return CMHResult;
}

void
VISEReply::SetHResult(HRESULT hresult)
{
  CMHResult = hresult;
}

Ptr
VISEReply::Results(void)
{
  return CMResults;
}


  // VISE Service Request Methods

VISEServiceRequest::VISEServiceRequest(SAPIVISECmd service, Ptr argument, MBOX returnAdress):
CMArgument(argument),
CMService(service),
CMReturnAddress(returnAdress)
{
}

VISEServiceRequest::~VISEServiceRequest(void)
{
}

SAPIVISECmd
VISEServiceRequest::Service(void)
{
  return CMService;
}

void
VISEServiceRequest::SetService(SAPIVISECmd service)
{
  CMService = service;
}

MBOX
VISEServiceRequest::ReturnAddress(void)
{
  return CMReturnAddress;
}

void
VISEServiceRequest::SetReturnAddress(MBOX returnAdress)
{
  CMReturnAddress = returnAdress;
}

Ptr
VISEServiceRequest::Argument(void)
{
  return CMArgument;
}


  // VISE Grammar and Results Request Methods

VISEGramResRequest::VISEGramResRequest(SAPIVISECmd service, LPUNKNOWN iunknown, const Char * ruleName, Bool autoPause, Int count, MBOX returnAddress):
VISEServiceRequest(service, (Ptr) iunknown, returnAddress),
CMRuleName(NULL),
CMAutoPause(autoPause),
CMCounter(count),
CMWordNames(NULL)
{
  if (CMArgument) ((LPUNKNOWN) CMArgument)->AddRef();
  if (ruleName) CMRuleName = VBX_salloc(ruleName);
}

VISEGramResRequest::VISEGramResRequest(SAPIVISECmd service, LPUNKNOWN iunknown, const Char * ruleName, Bool autoPause, MBOX returnAddress):
VISEServiceRequest(service, (Ptr) iunknown, returnAddress),
CMRuleName(NULL),
CMAutoPause(autoPause),
CMCounter(0),
CMWordNames(NULL)
{
  if (CMArgument) ((LPUNKNOWN) CMArgument)->AddRef();
  if (ruleName) CMRuleName = VBX_salloc(ruleName);
}

VISEGramResRequest::~VISEGramResRequest(void)
{
  if (CMArgument) ((LPUNKNOWN) CMArgument)->Release();
  VBX_free(CMRuleName);
}

Char *
VISEGramResRequest::RuleName(void)
{
  return CMRuleName;
}

void
VISEGramResRequest::SetRuleName(Char * ruleName)
{
  VBX_free(CMRuleName);
  CMRuleName = VBX_salloc(ruleName);
}

Char **
VISEGramResRequest::WordNames(void)
{
  return CMWordNames;
}

void
VISEGramResRequest::SetWordNames(Char ** wordNames)
{
  CMWordNames = wordNames;
}

Bool
VISEGramResRequest::AutoPause(void)
{
  return CMAutoPause;
}

 // Alias to call above to avoid confusion
Bool
VISEGramResRequest::BoolParam(void)
{
  return CMAutoPause;
}

Int
VISEGramResRequest::IntParam(void)
{
  return CMCounter;
}


  // VISE Grammar Request Methods

VISEGrammarRequest::VISEGrammarRequest(SAPIVISECmd service, PVISEGrammarObj grammarObj, const Char * ruleName, Bool autoPause, Int count, MBOX returnAddress):
VISEGramResRequest(service, (LPUNKNOWN) grammarObj, ruleName, autoPause, count, returnAddress)
{
}

VISEGrammarRequest::VISEGrammarRequest(SAPIVISECmd service, PVISEGrammarObj grammarObj, const Char * ruleName, Bool autoPause, MBOX returnAddress):
VISEGramResRequest(service, (LPUNKNOWN) grammarObj, ruleName, autoPause, returnAddress)
{
}

VISEGrammarRequest::~VISEGrammarRequest(void)
{
}

PVISEGrammarObj
VISEGrammarRequest::GrammarObject(void)
{
  return (PVISEGrammarObj) CMArgument;
}


  // VISE Results Request Methods

VISEResultsRequest::VISEResultsRequest(SAPIVISECmd service, PVISEResultsObj resultsObj, const Char * ruleName, Bool autoPause, Int count, MBOX returnAddress):
VISEGramResRequest(service, (LPUNKNOWN) resultsObj, ruleName, autoPause, count, returnAddress)
{
}

VISEResultsRequest::VISEResultsRequest(SAPIVISECmd service, PVISEResultsObj resultsObj, const Char * ruleName, Bool autoPause, MBOX returnAddress):
VISEGramResRequest(service, (LPUNKNOWN) resultsObj, ruleName, autoPause, returnAddress)
{
}

VISEResultsRequest::~VISEResultsRequest(void)
{
}

PVISEResultsObj
VISEResultsRequest::ResultsObject(void)
{
  return (PVISEResultsObj) CMArgument;
}


  // Speaker Profile Methods

VISESpeakerProfile::VISESpeakerProfile(SAPIVISECmd service, PVISEVoiStreamObj voiStream, const Char * spkrName, Bool lock, MBOX returnAddress):
VISEServiceRequest(service, (Ptr) voiStream, returnAddress),
CMSpkrName(NULL),
CMLock(lock)
{
  if (voiStream) voiStream->AddRef();
  if (spkrName) CMSpkrName = VBX_salloc(spkrName);
}

VISESpeakerProfile::~VISESpeakerProfile(void)
{
  if (CMArgument) ((PVISEVoiStreamObj) CMArgument)->Release();
  VBX_free(CMSpkrName);
}

PVISEVoiStreamObj
VISESpeakerProfile::VoiStream(void)
{
  return (PVISEVoiStreamObj) CMArgument;
}

Char *
VISESpeakerProfile::SpkrName(void)
{
  return CMSpkrName;
}

Bool
VISESpeakerProfile::Lock(void)
{
  return CMLock;
}
