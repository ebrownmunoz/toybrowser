#include "pthr.h"
#if defined(CELIB)
#include "celib.h"
#endif
#include <string.h>

// Verbex SAPI Objects
#include "visekb.hh"

// VULCAN Inclusions
#include "ekb.h"
#include "kb.h"
#include "sn.h"
#include "speaker.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "recfile.h"
#include "viseerr.h"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ukey.h"
#include "file.h"
#include "hash.h"
#include "ifaces.h"
#include "imemory.h"
#include "llist.h"

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#endif


  // Generic VISE Stream Methods

VISEStreamObj::VISEStreamObj(void):
RefCnt(0),
IFilePtr(NULL),
StreamPtr(NULL)
{
  Allocator.Calloc = IMEM_calloc;
  Allocator.Free   = IMEM_free;

  CRITSECT_CREATE(RefCntCritSect, &Allocator, NULL);
}

VISEStreamObj::~VISEStreamObj(void)
{
  CRITSECT_DESTROY(RefCntCritSect);
}

STDMETHODIMP_(ULONG)
VISEStreamObj::AddRef(void)
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEStreamObj::AddRef(): RefCnt for VISEStreamObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISEStreamObj::Release(void)
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEStreamObj::Release(): RefCnt for VISEStreamObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) delete this;
  return refCnt;
}

IFile *
VISEStreamObj::File(void)
{
  return IFilePtr;
}

VFILE
VISEStreamObj::Stream(void)
{
  return StreamPtr;
}


 // VISE RecStream Methods

VISERecStreamObj::VISERecStreamObj(void):
VISEStreamObj(),
KbPtr(NULL)
{
  Grammar.dwSize = 0;
  Grammar.pData = NULL;
}

VISERecStreamObj::VISERecStreamObj(SDATA data):
VISEStreamObj(),
KbPtr(NULL)
{
  VBX_DEBUG(VBX_print("  VISERecStreamObj::VISERecStreamObj: constructing new RecStream object @%p\n", this));
   // Make a local copy of the grammar
  if (data.dwSize) {
    Grammar.dwSize = data.dwSize;
    Grammar.pData = (PVOID) malloc(data.dwSize);
    memcpy(Grammar.pData, data.pData, data.dwSize);
     // Open it as a stream
    IFilePtr = RECFILE_create(&Allocator, RECFILE_MEMSTREAM);
    StreamPtr = VFILE_open(&Allocator, IFilePtr, (void *) &Grammar, RECFILE_READ);
  } else {
    Grammar.dwSize = 0;
    Grammar.pData = NULL;
  }
}

VISERecStreamObj::~VISERecStreamObj(void)
{
  VBX_DEBUG(VBX_print("  VISERecStreamObj::~VISERecStreamObj: destroying RecStream object @%p\n", this));
  KB_destroy(KbPtr);
   // Close the stream
  if (StreamPtr) VFILE_close(StreamPtr);
  if (IFilePtr) RECFILE_destroy(IFilePtr);
   // Get rid of the local copy of the grammar
  VBX_free(Grammar.pData);
}

Bool
VISERecStreamObj::Init(EKB ekb)
{
  KbPtr = KB_create(ekb);
  VBX_fatalIf(!KbPtr);

  return(KB_load(KbPtr, StreamPtr, VBX_VERBOSE));
}

KB
VISERecStreamObj::Kb(void)
{
  return KbPtr;
}

IFile *
VISERecStreamObj::RecFile(void)
{
  return IFilePtr;
}


 // VISE VoiStream Methods

VISEVoiStreamObj::VISEVoiStreamObj(Char * name, PVISESpkrDirObj dirPtr):
VISEStreamObj(),
SpkrDirObjPtr(dirPtr),
SpkrProfile(NULL),
ReplaceContents(FALSE),
RestoreContents(FALSE),
RemoveContents(FALSE),
SpkrName(NULL)
{
  VBX_DEBUG(VBX_print("  VISEVoiStreamObj::VISEVoiStreamObj: constructing new VoiStream object @%p for speaker \"%s\"\n", this, name));
  UKEYREG_reset(&Registry);
  if (name) SpkrName = VBX_salloc(name);
  if (SpkrDirObjPtr) SpkrDirObjPtr->AddRef();
}

VISEVoiStreamObj::~VISEVoiStreamObj(void)
{
  if (RemoveContents) {
    VBX_DEBUG(VBX_print("  VISEVoiStreamObj::~VISEVoiStreamObj: removing underlying files for \"%s\" from the directory\n", SpkrName));
    SpkrDirObjPtr->RemoveContents(SpkrName);
  }

  if (IFilePtr) RECFILE_destroy(IFilePtr);
  if (SpkrDirObjPtr) SpkrDirObjPtr->Release();

  VBX_free(SpkrName);
}

void
VISEVoiStreamObj::Update(void)
{
  if (RestoreContents) {
    VBX_DEBUG(VBX_print("  VISEVoiStreamObj::Update: restoring contents of \"%s\"\n", SpkrName));
    UnloadModels();
    SpkrDirObjPtr->RestoreContents(SpkrName);
    Char * fileName = SpkrDirObjPtr->FilePathName(SpkrName);
    LoadModels(fileName, SpkrDirObjPtr->EngineKB());
    VBX_free(fileName);
    RestoreContents = FALSE;
  }

  if (ReplaceContents) {
    VBX_DEBUG(VBX_print("  VISEVoiStreamObj::Update: replacing contents of \"%s\"\n", SpkrName));
    UnloadModels();
    if (!SpkrDirObjPtr->ReplaceContents(SpkrName))
      SEVERE_BRA_ "VISEVoiStreamObj::Update can't replace contents of \"%s\"", SpkrName _KET;
    Char * fileName = SpkrDirObjPtr->FilePathName(SpkrName);
    LoadModels(fileName, SpkrDirObjPtr->EngineKB());
    VBX_free(fileName);
    ReplaceContents = FALSE;
  }
}

SPKR
VISEVoiStreamObj::Speaker(void)
{
  return SpkrProfile;
}

PVISESpkrDirObj
VISEVoiStreamObj::Directory(void)
{
  return SpkrDirObjPtr;
}


  // VISE Knowledge Base Methods

VISEKbObj::VISEKbObj(EKB ekb, PVISERecStreamObj recStream, PVISEVoiStreamObj voiStream, Char * spkrName, UKEYPOOL keyPool):
RefCnt(0),
RecStreamPtr(recStream),
VoiStreamPtr(voiStream),
SpkrNamePtr(VBX_salloc(spkrName))
{
  if (!voiStream) FATAL_BRA_ "VISEKbObj: Missing Voi Stream" _KET;

  Allocator.Calloc = IMEM_calloc;
  Allocator.Free   = IMEM_free;

  CRITSECT_CREATE(RefCntCritSect, &Allocator, NULL);

   // Make a default RecStream if none is provided
  if (!RecStreamPtr) {
    RecStreamPtr = new VISERecStreamObj();
    RecStreamPtr->Init(ekb);
  }
  RecStreamPtr->AddRef();
  VoiStreamPtr->AddRef();

  Key = UKEY_reserve(keyPool);
  if (!Key) FATAL_BRA_ "VISEKbObj: Cannot reserve a UKEY" _KET;

  VBX_DEBUG(VBX_print("  VISEKbObj: constructing new KB object @%p\n", this));

  (void) Update();
}

VISEKbObj::~VISEKbObj(void)
{
  VBX_DEBUG(VBX_print("  VISEKbObj::~VISEKbObj: destroying KB object @%p\n", this));

  UKEY_unregister(&VoiStreamPtr->Registry, Key);
  UKEY_release(Key);
  if (RecStreamPtr) RecStreamPtr->Release();
  VoiStreamPtr->Release();
  VBX_free(SpkrNamePtr);

  CRITSECT_DESTROY(RefCntCritSect);
}

STDMETHODIMP_(ULONG)
VISEKbObj::AddRef(void)
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEKbObj::AddRef(): RefCnt for VISEKbObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISEKbObj::Release(void)
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEKbObj::Release(): RefCnt for VISEKbObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) delete this;
  return refCnt;
}

KB
VISEKbObj::Kb(void)
{
  return RecStreamPtr->Kb();
}

PVISERecStreamObj
VISEKbObj::RecStream(void)
{
  return RecStreamPtr;
}

PVISEVoiStreamObj
VISEKbObj::VoiStream(void)
{
  return VoiStreamPtr;
}

Char *
VISEKbObj::SpkrName(void)
{
  return SpkrNamePtr;
}

Bool
VISEKbObj::IsMadeOf(PVISERecStreamObj recStreamPtr, PVISEVoiStreamObj voiStreamPtr, Char * spkrName)
{
  return(this->RecStream() == recStreamPtr && this->VoiStream() == voiStreamPtr && !strcmp(this->SpkrName(), spkrName));
}

Bool
VISEKbObj::SaveModels(UInt vocabId)
{
  return VoiStreamPtr->SaveModels(vocabId, Key);
}

Bool
VISEKbObj::Update(void)
{
  VBX_DEBUG(VBX_print("  VISEKBObj::Update: calling VoiStream::Update\n"));

   // Update the voice stream object
  VoiStreamPtr->Update();

   // Do nothing if up-to-date
  if (UKEY_registered(&VoiStreamPtr->Registry, Key)) return FALSE;

  VBX_DEBUG(VBX_print("  VISEKbObj::Update: constructing KB object @%p for speaker \"%s\"\n", this, SpkrNamePtr));

   // Register with the voice stream
  UKEY_register(&VoiStreamPtr->Registry, Key);

   // Reconcile the rec (speaker-independent/application-dependent) and voi (speaker-dependent) data
  return(KB_reconcile(RecStreamPtr->Kb(), VoiStreamPtr->Speaker()));
}


  // VISE Speaker Directory Methods

VISESpkrDirObj::VISESpkrDirObj(const Char * dirName, const Char * extension, Char backupTag, Char temporaryTag, EKB ekb):
RefCnt(0),
Streams(NULL),
BackupTag(backupTag),
TemporaryTag(temporaryTag),
EKbPtr(ekb)
{
  VBX_fatalIf(!dirName);

  Allocator.Calloc = IMEM_calloc;
  Allocator.Free   = IMEM_free;

  CRITSECT_CREATE(RefCntCritSect, &Allocator, NULL);
  
  // Construct the directory path name
  UInt     nameLen = strlen(dirName);
  Path = (Char *) malloc(nameLen + 2);
  VBX_fatalIf(!Path);
  strcpy(Path, dirName);
  if (nameLen) {
    Char     c = dirName[nameLen - 1];
    if (c != '/' && c != '\\')
      strcat(Path, "/");
  } else {
    strcat(Path, "/");
  }

  // Construct the file name extension
  if (extension && strlen(extension)) {
    Extension = (Char *) malloc(strlen(extension) + 2);
    VBX_fatalIf(!Extension);
    if (extension[0] == '.')
      strcpy(Extension, extension);
    else
      sprintf(Extension, ".%s", extension);
  } else {
#pragma GCC diagnostic ignored "-Wwrite-strings"
    Extension = "";
#pragma GCC diagnostic pop
  }
  VBX_DEBUG(VBX_print("  VISESpkrDirObj::VISESpkrDirObj: constructing file name template\n"));

  // Construct the file name template
  Template = (Char *) malloc(strlen(Extension) + 2);
  VBX_fatalIf(!Template);
  sprintf(Template, "*%s", Extension);

  // Construct a list of the names
  UInt   numNames;
  Char * names = Names(&numNames, NULL);
  VBX_fatalIf(!names);

  // Hash a NULL stream for each speaker
  Streams = HASH_create(numNames, NULL, NULL, NULL);
  VBX_fatalIf(!Streams);
  Char * name = names;
  while ((nameLen = strlen(name))) {
    if (!HASH_lookup(Streams, name, NULL)) {
      Char * dupName = strdup(name);
      HASH_add(Streams, dupName, (Ptr) NULL);
    }
    name += nameLen + 1;
  }

  VBX_free(names);

  VBX_DEBUG(VBX_print("  VISESpkrDirObj::VISESpkrDirObj: constructing new Directory object @%p\n", this));
}

VISESpkrDirObj::~VISESpkrDirObj(void)
{
  VBX_DEBUG(VBX_print("  VISESpkrDirObj::~VISESpkrDirObj: destroying Directory object @%p\n", this));

  HASH_processSymbols(Streams, NULL);
  HASH_annihilate(Streams);
  if (strlen(Extension)) VBX_free(Extension);
  VBX_free(Template);
  VBX_free(Path);

  CRITSECT_DESTROY(RefCntCritSect);
}

STDMETHODIMP_(ULONG)
VISESpkrDirObj::AddRef(void)
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISESpkrDirObj::AddRef(): RefCnt for VISESpkrDirObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISESpkrDirObj::Release(void)
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISESpkrDirObj::Release(): RefCnt for VISESpkrDirObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) delete this;
  return refCnt;
}

EKB
VISESpkrDirObj::EngineKB(void)
{
  return EKbPtr;
}

Char *
VISESpkrDirObj::Names(UInt * numNamesPtr, UInt * listLengthPtr)
{
  UInt     dirSize, numNames = 0, listLength = 1;
  DIRLIST  dirList;
  Char *   fileNameList = NULL;
  Char *   nameList = NULL;

  // Construct a DIRLIST of files
  dirList = DIRLIST_create(Path, Template);
  if (dirList) {
    // Construct a list of file names
    fileNameList = DIRLIST_fileNames(dirList, &dirSize);
    if (fileNameList) {
      // Extract from it a list of names
      UInt    fileNameLen, nameLen;
      Char  * extStart, * fileName = fileNameList, * name = fileNameList;
      while ((fileNameLen = strlen(fileName))) {
        if ((extStart = strrchr(fileName, '.'))) *extStart = '\0';
        nameLen = strlen(fileName);
        memmove(name, fileName, nameLen + 1);
        fileName += fileNameLen + 1;
        name += nameLen + 1;
        listLength += nameLen + 1;
        numNames++;
      }
      *name = '\0';
    }
  }
  DIRLIST_annihilate(dirList);

  nameList = (Char *) malloc(listLength);

  if (nameList) {
    if (fileNameList)
      memmove(nameList, fileNameList, listLength);
    else
      nameList[0] = '\0';
    if (numNamesPtr) *numNamesPtr = numNames;
    if (listLengthPtr) *listLengthPtr = listLength;
  } else {
    if (numNamesPtr) *numNamesPtr = 0;
    if (listLengthPtr) *listLengthPtr = 0;
  }
  VBX_free(fileNameList);

  return nameList;
}

Char *
VISESpkrDirObj::FilePathName(const Char * name)
{
  Char * fileName = (Char *) malloc(strlen(Path) + strlen(name) + strlen(Extension) + 1);
  if (fileName) {
    if (*name == '/' || *name == '\\')
      sprintf(fileName, "%s%s", name, Extension);
    else
      sprintf(fileName, "%s%s%s", Path, name, Extension);
  }
  return fileName;
}

PVISEVoiStreamObj
VISESpkrDirObj::CreateVoiStream(Char * name)
{
  PVISEVoiStreamObj   voiStreamObjPtr;

  // If the stream hash table has no entry for the name, put a NULL one there
  if (!HASH_lookup(Streams, name, (Ptr *) &voiStreamObjPtr)) {
    VBX_DEBUG(VBX_print("  DirObj::CreateVoiStream: There is no entry for \"%s\" in the stream hash table\n", name));
    Char * dupName = strdup(name);
    if (!dupName || !HASH_add(Streams, dupName, NULL)) {
      VBX_DEBUG(VBX_print("  DirObj::CreateVoiStream: Can't hash entry for \"%s\"\n", name));
      return NULL;
    }
  // Else, return the entry if it is non-NULL
  } else {
    if (voiStreamObjPtr) {
      VBX_DEBUG(VBX_print("  DirObj::CreateVoiStream: A voice stream object already exists for \"%s\"\n", name));
      return voiStreamObjPtr;
    } else {
      VBX_DEBUG(VBX_print("  DirObj::CreateVoiStream: There is a NULL entry for \"%s\" in the stream hash table\n", name));
    }
  }

  // No stream object exists for the name, so construct one from the underlying file

  // Check that there is a file for the name
  Char * fileName = FilePathName(name);
  if (!fileName || !FILE_exists(fileName)) {
    VBX_DEBUG(VBX_print("  DirObj::CreateVoiStream: There is no file named \"%s\" in directory \"%s\"\n", fileName, Path));
    VBX_free(fileName);
    return NULL;
  }

  if (!BackupContents(name)) {
    VBX_free(fileName);
    return NULL;
  }

  // Construct a stream object for the contents
  voiStreamObjPtr = VISE_createVoiStreamObj(name, this);
  VBX_DEBUG(VBX_print("  VISEVoiStreamObj: created %p\n", voiStreamObjPtr));
  if (!voiStreamObjPtr->LoadModels(fileName, EKbPtr)) {
    VBX_DEBUG(VBX_print("  DirObj::CreateVoiStream: Can't load the stream object for \"%s\" from file \"%s\"\n", name, fileName));
    delete voiStreamObjPtr;
    VBX_free(fileName);
    return NULL;
  }
  VBX_free(fileName);

  // Insert the stream object into the hash table entry
  VBX_fatalIf(!HASH_modify(Streams, name, (Ptr) voiStreamObjPtr));

  // Return the new stream object
  return voiStreamObjPtr;
}

PVISEVoiStreamObj
VISESpkrDirObj::Stream(Char * name)
{
  PVISEVoiStreamObj   voiStreamObjPtr;

  // If the stream hash table has no entry for the name, return NULL
  if (!HASH_lookup(Streams, name, (Ptr *) &voiStreamObjPtr))
    voiStreamObjPtr = NULL;

  return voiStreamObjPtr;
}

Bool
VISESpkrDirObj::RemoveStream(Char * name)
{
  VBX_DEBUG(VBX_print("  DirObj::RemoveStream: Removing stream object for speaker \"%s\"\n", name));
  return HASH_modify(Streams, name, NULL);
}

Bool
VISESpkrDirObj::HasAnEntry(Char * name)
{
  Char * fileName = FilePathName(name);
  Bool  hasOne = (fileName != NULL && FILE_exists(fileName));
  VBX_free(fileName);

  return hasOne;
}

Bool
VISESpkrDirObj::RemoveEntry(Char * name)
{
  PVISEVoiStreamObj  streamObj = Stream(name);

  // If there is an associated stream object, ask it to delete the contents
  if (streamObj) {
    streamObj->RemoveContents = TRUE;
    return TRUE;
  // Otherwise, delete the files here
  } else {
    return RemoveContents(name);
  }
}

Bool
VISESpkrDirObj::RestoreEntry(Char * name)
{
  PVISEVoiStreamObj  streamObj = Stream(name);

  // If there is an associated stream object, ask it to restore the contents
  if (streamObj) {
    streamObj->RestoreContents = TRUE;
    return TRUE;
  // Otherwise, do the restoration here
  } else {
    return RestoreContents(name);
  }
}

Char *
VISESpkrDirObj::DirPath(void)
{
  return Path;
}

DIRLIST
VISESpkrDirObj::DirList(Char * tmplt)
{
  if (!tmplt) tmplt = Template;
  return DIRLIST_create(Path, tmplt);
}

Char *
VISESpkrDirObj::TaggedPathName(Char * name, Char tag)
{
  Char * fileName = (Char *) malloc(strlen(Path) + strlen(name) + strlen(Extension) + 2);
  if (fileName) {
    if (*name == '/' || *name == '\\')
      sprintf(fileName, "%s%s%c", name, Extension, tag);
    else
      sprintf(fileName, "%s%s%s%c", Path, name, Extension, tag);
  }
  return fileName;
}
