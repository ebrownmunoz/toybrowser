#include "pthr.h"
#include <stdio.h>
#include <string.h>
#if !defined(VXWORKS) && !defined(CELIB)
#include <wchar.h>
#endif

// Verbex SAPI Objects
#include "vbxenum.hh"
#include "viseobj.hh"

// Verbex C Infrastructure
#include "types.h"
#include "file.h"
#include "vbx.h"

#ifdef DEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

#ifdef VXWORKS
#define VISE_CFGFILE_DEFAULT  "/sys/flexvise.cfg"
#define VISE_CFGFILE_DFLTDIR  "/sys"          // Default directory for config files
#else
#define VISE_CFGFILE_DEFAULT  "flexvise.cfg"  // Search working directory for this
#define VISE_CFGFILE_DFLTDIR  "kb"            // Default directory for config files
#endif
#define VISE_CFGFILE_TELE2000 "tele2000.cfg"  // Default if "flexvise.cfg" not found

#define PRINT_GUID(_fp, _guid) VBX_print("%8.8x-%4.4x-%4.4x-%2.2x%2.2x-%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x",    \
                                       _guid.Data1, _guid.Data2, _guid.Data3, _guid.Data4[0], _guid.Data4[1], \
                                       _guid.Data4[2], _guid.Data4[3], _guid.Data4[4], _guid.Data4[5],        \
                                       _guid.Data4[6], _guid.Data4[7])

// Internal function prototypes
static void CreateModeInfo(PSRMODEINFOA modeInfoPtr);
static void CreateModeInfo(PSRMODEINFOW modeInfoPtr);

VbxEnum::VbxEnum(LPUNKNOWN unknwnPtr):
RefCnt(0),
UnknwnPtr(unknwnPtr),
VbxISREnumPtr(NULL),
VbxISRFindPtr(NULL),
VISEObjPtr(NULL),
ArrayPosn(0)
{
  if (!UnknwnPtr) UnknwnPtr = this;

  // Instantiate the contained interfaces
  VbxISREnumPtr = new VbxISREnum(this, UnknwnPtr);
  VbxISRFindPtr = new VbxISRFind(this, UnknwnPtr);
}

VbxEnum::VbxEnum(PVbxEnum enumPtr):
RefCnt(0),
VbxISREnumPtr(NULL),
VbxISRFindPtr(NULL),
VISEObjPtr(NULL)
{
  UnknwnPtr = this;
  ArrayPosn = enumPtr->ArrayPosn;

  // Instantiate the contained interfaces
  VbxISREnumPtr = new VbxISREnum(this, UnknwnPtr);
  VbxISRFindPtr = new VbxISRFind(this, UnknwnPtr);
}

VbxEnum::~VbxEnum(void)
{
  VBX_DEBUG(VBX_print("  VbxEnum::~VbxEnum(): destroying the VBX enumerator\n"));

  // Free the contained interfaces
  delete VbxISREnumPtr;
  delete VbxISRFindPtr;
}

STDMETHODIMP
VbxEnum::QueryInterface(REFIID riid, LPVOID *ppv)
{
 *ppv = NULL;

  VBX_DEBUG(VBX_print("VbxEnum::QueryInterface CALLED\n"));

  // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown))
    *ppv = (LPVOID) this;
  else if (IsEqualIID(riid, IID_ISREnumA))
    *ppv = VbxISREnumPtr;
  else if (IsEqualIID(riid, IID_ISREnumW))
    *ppv = VbxISREnumPtr;
  else if (IsEqualIID(riid, IID_ISRFind))
    *ppv = VbxISRFindPtr;

  // Update the reference count here
  if (*ppv != NULL) {
    ((LPUNKNOWN) *ppv)->AddRef();
    VBX_DEBUG(VBX_print(" QI SUCCESS\n"));
    return SRERR_NONE;
  }

  VBX_DEBUG(VBX_print(" QI FAILURE\n"));

  return SRERR_INVALIDINTERFACE;
}

STDMETHODIMP_(ULONG)
VbxEnum::AddRef()
{
  VBX_DEBUG(VBX_print("  VbxEnum::AddRef(): RefCnt for VbxEnum @%p = %d\n", this, RefCnt + 1));
  return ++RefCnt;
}

STDMETHODIMP_(ULONG)
VbxEnum::Release()
{
  ULONG retval;

  retval = --RefCnt;
  VBX_DEBUG(VBX_print("  VbxEnum::Release(): RefCnt for VbxEnum @%p = %d\n", this, retval));
  if (RefCnt == 0) {
    delete this;
  }
  return retval;
}

Char *
VbxEnum::CfgFilePath()
{
  // If "flexvise.cfg" exists in the current working directory, use that
  sprintf(CMCfgFilePath, "%s", VISE_CFGFILE_DEFAULT);
  if (FILE_exists(CMCfgFilePath))
    return CMCfgFilePath;

  // Otherwise, unconditionally return the default
  sprintf(CMCfgFilePath, "%s/%s", VISE_CFGFILE_DFLTDIR, VISE_CFGFILE_TELE2000);
  return CMCfgFilePath;
}

// Methods for ISREnum

VbxISREnum::VbxISREnum(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VbxISREnum::~VbxISREnum()
{
}

STDMETHODIMP
VbxISREnum::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VbxISREnum::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VbxISREnum::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VbxISREnum::Next(ULONG reqModeCnt, PSRMODEINFOA modeArray, ULONG *modeCnt)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_DEBUG(VBX_print("VbxEnumA::Next CALLED ... "));

  if (reqModeCnt <= 0 || modeArray == NULL || modeCnt == NULL)
    return SRERR_INVALIDPARAM;

  if (objPtr->ArrayPosn == 0) {
    VBX_DEBUG(VBX_print("calling CreateModeInfoA ... \n"));
    CreateModeInfo(modeArray);

    VBX_DEBUG(VBX_print(" EngineID:     "));
    VBX_DEBUG(PRINT_GUID(fp, modeArray->gEngineID));

    objPtr->ArrayPosn++;
   *modeCnt = 1;
  } else {
    objPtr->ArrayPosn++;
   *modeCnt = 0;
  }
    
  VBX_DEBUG(VBX_print(" ... provided %d entries\n", *modeCnt));

  if (*modeCnt == reqModeCnt)
    return S_OK;
  else
    return S_FALSE;
}

STDMETHODIMP
VbxISREnum::Next(ULONG reqModeCnt, PSRMODEINFOW modeArray, ULONG *modeCnt)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_DEBUG(VBX_print("VbxEnumW::Next CALLED ... "));

  if (reqModeCnt <= 0 || modeArray == NULL || modeCnt == NULL)
    return SRERR_INVALIDPARAM;

  if (objPtr->ArrayPosn == 0) {
    VBX_DEBUG(VBX_print("calling CreateModeInfoW ... \n"));
    CreateModeInfo(modeArray);

    VBX_DEBUG(VBX_print(" EngineID:     "));
    VBX_DEBUG(PRINT_GUID(fp, modeArray->gEngineID));

    objPtr->ArrayPosn++;
   *modeCnt = 1;
  } else {
    objPtr->ArrayPosn++;
   *modeCnt = 0;
  }
    
  VBX_DEBUG(VBX_print(" ... provided %d entries\n", *modeCnt));

  if (*modeCnt == reqModeCnt)
    return S_OK;
  else
    return S_FALSE;
}

STDMETHODIMP
VbxISREnum::Skip(ULONG skipCnt)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_DEBUG(VBX_print("VbxEnum::Skip CALLED\n"));

  objPtr->ArrayPosn += skipCnt;  
  return SRERR_NONE;
}

STDMETHODIMP
VbxISREnum::Clone(ISREnumA **enumPtrPtr)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_DEBUG(VBX_print("VbxEnumA::Clone CALLED\n"));

  PVbxEnum clonePtr = new VbxEnum(objPtr);
  *enumPtrPtr = (ISREnumA *) clonePtr->VbxISREnumPtr;
  ((LPUNKNOWN) *enumPtrPtr)->AddRef();
  return SRERR_NONE;
}

STDMETHODIMP
VbxISREnum::Clone(ISREnumW **enumPtrPtr)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_DEBUG(VBX_print("VbxEnumW::Clone CALLED\n"));

  PVbxEnum clonePtr = new VbxEnum(objPtr);
  *enumPtrPtr = (ISREnumW *) clonePtr->VbxISREnumPtr;
  ((LPUNKNOWN) *enumPtrPtr)->AddRef();
  return SRERR_NONE;
}

STDMETHODIMP
VbxISREnum::Select(GUID modeID, PISRCENTRALA *isrCentralPtrPtr, LPUNKNOWN audioUnknownPtr)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_print("VbxISREnnum::Select: CALLED\n");

  objPtr->VISEObjPtr = new VISEObj(NULL, audioUnknownPtr, objPtr->CfgFilePath());

  VBX_print("VbxISREnnum::Select: VISEObjPtr @%p\n", objPtr->VISEObjPtr);
  VBX_fatalIf(objPtr->VISEObjPtr == NULL);
  VBX_fatalIf(objPtr->VISEObjPtr->QueryInterface(IID_ISRCentral, (PVOID *) isrCentralPtrPtr));
  // AddRef on ISRCentral already done by QueryInterface
  VBX_print("VbxISREnnum::Select: DONE\n");
  return SRERR_NONE;
}

STDMETHODIMP
VbxISREnum::Select(GUID modeID, PISRCENTRALW *isrCentralPtrPtr, LPUNKNOWN audioUnknownPtr)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  VBX_DEBUG(VBX_print("VbxEnumW::Select CALLED\n"));

  objPtr->VISEObjPtr = new VISEObj(NULL, audioUnknownPtr, objPtr->CfgFilePath());
  VBX_fatalIf(objPtr->VISEObjPtr == NULL);
  VBX_fatalIf(objPtr->VISEObjPtr->QueryInterface(IID_ISRCentral, (PVOID *) isrCentralPtrPtr));
  // AddRef on ISRCentral already done by QueryInterface
  return SRERR_NONE;
}

STDMETHODIMP
VbxISREnum::Reset(void)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  objPtr->ArrayPosn = 0;
  return SRERR_NONE;
}

// Methods for ISRFind

VbxISRFind::VbxISRFind(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VbxISRFind::~VbxISRFind()
{
}

STDMETHODIMP
VbxISRFind::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VbxISRFind::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VbxISRFind::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VbxISRFind::Find(PSRMODEINFO infoPtr, PSRMODEINFORANK infoRankPtr, PSRMODEINFO infoFoundPtr)
{
  VBX_DEBUG(VBX_print("VbxISRFind::Find calling CreateModeInfo\n"));

  CreateModeInfo(infoFoundPtr);

  return SRERR_NONE;
}

STDMETHODIMP
VbxISRFind::Select(GUID modeID, PISRCENTRAL *isrCentralPtrPtr, LPUNKNOWN audioUnknownPtr)
{
  PVbxEnum objPtr = (PVbxEnum) ObjPtr;

  objPtr->VISEObjPtr = new VISEObj(NULL, audioUnknownPtr, objPtr->CfgFilePath());
  VBX_fatalIf(objPtr->VISEObjPtr == NULL);
  VBX_fatalIf(objPtr->VISEObjPtr->QueryInterface(IID_ISRCentral, (PVOID *) isrCentralPtrPtr));
  // AddRef on ISRCentral already done by QueryInterface
  return SRERR_NONE;
}

static void 
CreateModeInfo(PSRMODEINFOA modeInfoPtr)
{
#ifdef WIN32
  ((PSRMODEINFOW) modeInfoPtr)->gEngineID = CLSID_VerbexVise;
  ((PSRMODEINFOW) modeInfoPtr)->gModeID = IID_VerbexViseMode;
  wcscpy((wchar_t *) ((PSRMODEINFOW) modeInfoPtr)->szMfgName, L"Verbex Voice Systems");
  wcscpy((wchar_t *) ((PSRMODEINFOW) modeInfoPtr)->szProductName, L"SAPI VISE v0.3");
  wcscpy((wchar_t *) ((PSRMODEINFOW) modeInfoPtr)->szModeName, L"Continuous VISE");
  ((PSRMODEINFOW) modeInfoPtr)->language.LanguageID = 0x0409;  // = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)
  ((PSRMODEINFOW) modeInfoPtr)->language.szDialect[0] = L'\0';
  ((PSRMODEINFOW) modeInfoPtr)->dwSequencing = SRSEQUENCE_CONTINUOUS;
  ((PSRMODEINFOW) modeInfoPtr)->dwMaxWordsVocab = 10000;
  ((PSRMODEINFOW) modeInfoPtr)->dwMaxWordsState = 10000;
  ((PSRMODEINFOW) modeInfoPtr)->dwGrammars = SRGRAM_CFG;
  ((PSRMODEINFOW) modeInfoPtr)->dwFeatures = SRFEATURE_INDEPSPEAKER | SRFEATURE_TRAINWORD | SRFEATURE_WILDCARD | SRFEATURE_GRAMRECURSIVE;
  ((PSRMODEINFOW) modeInfoPtr)->dwInterfaces = 0;
  ((PSRMODEINFOW) modeInfoPtr)->dwEngineFeatures = 0;
#else
  modeInfoPtr->gEngineID = CLSID_VerbexVise;
  modeInfoPtr->gModeID = IID_VerbexViseMode;
  strcpy(modeInfoPtr->szMfgName, "Verbex Voice Systems");
  strcpy(modeInfoPtr->szProductName, "SAPI VISE v0.3");
  strcpy(modeInfoPtr->szModeName, "Continuous VISE");
  modeInfoPtr->language.LanguageID = 0x0409;  // = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)
  modeInfoPtr->language.szDialect[0] = '\0';
  modeInfoPtr->dwSequencing = SRSEQUENCE_CONTINUOUS;
  modeInfoPtr->dwMaxWordsVocab = 10000;
  modeInfoPtr->dwMaxWordsState = 10000;
  modeInfoPtr->dwGrammars = SRGRAM_CFG;
  modeInfoPtr->dwFeatures = SRFEATURE_INDEPSPEAKER | SRFEATURE_TRAINWORD | SRFEATURE_WILDCARD | SRFEATURE_GRAMRECURSIVE;
  modeInfoPtr->dwInterfaces = 0;
  modeInfoPtr->dwEngineFeatures = 0;
#endif
}

static void 
CreateModeInfo(PSRMODEINFOW modeInfoPtr)
{
  modeInfoPtr->gEngineID = CLSID_VerbexVise;
  modeInfoPtr->gModeID = IID_VerbexViseMode;
#ifndef VXWORKS
  wcscpy((wchar_t *) modeInfoPtr->szMfgName, L"Verbex Voice Systems");
  wcscpy((wchar_t *) modeInfoPtr->szProductName, L"SAPI VISE v0.3");
  wcscpy((wchar_t *) modeInfoPtr->szModeName, L"Continuous VISE");
#endif
  modeInfoPtr->language.LanguageID = 0x0409;  // = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)
  modeInfoPtr->language.szDialect[0] = L'\0';
  modeInfoPtr->dwSequencing = SRSEQUENCE_CONTINUOUS;
  modeInfoPtr->dwMaxWordsVocab = 10000;
  modeInfoPtr->dwMaxWordsState = 10000;
  modeInfoPtr->dwGrammars = SRGRAM_CFG;
  modeInfoPtr->dwFeatures = SRFEATURE_INDEPSPEAKER | SRFEATURE_TRAINWORD | SRFEATURE_WILDCARD | SRFEATURE_GRAMRECURSIVE;
  modeInfoPtr->dwInterfaces = 0;
  modeInfoPtr->dwEngineFeatures = 0;
}
