#include "pthr.h"
#include <sys/types.h>
#include <limits.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

 // Verbex SAPI Objects
#include "viseengine.hh"
#include "visenotify.hh"
#include "viseobj.hh"
#include "visephrase.hh"
#include "visekb.hh"
#include "visemessage.hh"
#include "visetrain.hh"
#include "viseresults.hh"

 // FE Inclusions
#include "mmaudin.hh"
#include "mmaudin.h"
#include "feversion.h"

 // VULCAN Inclusions
#include "speaker.h"
#include "vbxtypes.h"
#include "vfe.h"
#include "viseerr.h"
#include "viseparm.h"

 // Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ifaces.h"
#include "mbox.h"
#include "siesta.h"
#include "sn.h"
#include "viseerr.h"
#include "visemsg.h"
#include "cnvrtexc.h"

#ifdef DEBUG
#define VBX_DEBUG(P)              P
#define VBX_IF_NOT_DO(C,P)        if (!(C)) (P)
#define VBX_VERBOSE               TRUE
#define VBX_FATALSTATUS(R,S)      { Int v = (R); if (R) FATAL_BRA_ "condition = %d; status = %s", v, VISEERR_string(S) _KET; }
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)        C
#define VBX_VERBOSE               FALSE
#define VBX_FATALSTATUS(R,S)
#endif

#define DEACTIVATION_WAIT         20000            // Deactivation wait in microseconds

#define SMAX                      256

#define ENGINEMBOX_DEPTH          4

VISEEngineObj::VISEEngineObj(IMemory * allocator, IMail * mail, PVISEObj controlObjPtr, LPUNKNOWN audioUnknwnPtr):
Allocator(allocator),
AudioQLen(VISE_AUDQUEUELEN_DEFAULT),
IAudioSourcePtr(NULL),
IAudioPtr(NULL),
BOUSilMSecs(VISE_BOUSILMSECS_DEFAULT),
EOUSilMSecs(VISE_EOUSILMSECS_DEFAULT),
Mail(mail),
ControlObjPtr(controlObjPtr),
VocabId(0),
Vocabulary(NULL),
FeVersion(FE_BADVERSION),
SourceRefCnt(0),
MaxFrameCount(VISE_MAXFRAMECNT_DEFAULT),
LSMinDwell(LS_MIN_DEFAULT),
IWMaxDwell(IW_MAX_DEFAULT),
MaxSigBits(VISE_SAMPSIGBITS_DEFAULT),
MaxSigBitsDefault(VISE_SAMPSIGBITS_DEFAULT),
SampsPerSec(VISE_SAMPSPERSEC_DEFAULT),
PrintStats(VISE_PRINTSTATUS_DEFAULT)
{
   // If audioUnknwnPtr is not NULL, try to get the audio source interfaces
  if (audioUnknwnPtr &&
      !audioUnknwnPtr->QueryInterface(IID_IAudioSource, (PVOID *) &IAudioSourcePtr) &&
      !audioUnknwnPtr->QueryInterface(IID_IAudio, (PVOID *) &IAudioPtr) &&
      (MMAudIn.mmaudin = MMAUDIN_create(audioUnknwnPtr))) {
	  VBX_print("VISEEngineObj::VISEEngineObj(): enabling VOICE recognition\n");
    AudioIn = (IAudioIn *) &MMAudIn;
    AudioIn->Init = MMAUDIN_Init;
    AudioIn->Read = MMAUDIN_Read;
    AudioIn->Start = MMAUDIN_Start;
    AudioIn->Stop = MMAUDIN_Stop;
    AudioIn->LevelSet = MMAUDIN_LevelSet;
    AudioIn->LevelGet = MMAUDIN_LevelGet;
    AudioIn->PosnGet = MMAUDIN_PosnGet;
    AudioIn->TotalGet = MMAUDIN_TotalGet;
    AudioIn->Status = MMAUDIN_Status;
    AudioIn->DataAvailable = MMAUDIN_DataAvailable;
  } else {
    if (IAudioSourcePtr) IAudioSourcePtr->Release();
    if (IAudioPtr)       IAudioPtr->Release();
    AudioIn = NULL;
    VBX_print("VISEEngineObj::VISEEngineObj(): defaulting to TEXT recognition\n");
  }

   // Instantiate the Front End
  VBX_fatalIf(!(VFeat = VFE_create(Allocator, Mail, AudioIn)));

   // Set up the mailboxes
  MBOX_Att  mboxAttr = MBOX_createAttr(Allocator, ENGINEMBOX_DEPTH, ENGINEMBOX_DEPTH, SAPI_NUMPRIORITIES);
  VBX_fatalIf(!mboxAttr);
  VBX_fatalIf(!(EngineMBox = MBOX_create(Allocator, mboxAttr)));
  MBOX_destroyAttr(mboxAttr);
}

VISEEngineObj::~VISEEngineObj()
{
   // Get rid of the source object
  VBX_DEBUG(VBX_print("  VISEEngineObj::~VISEEngineObj(): annihilating source objects\n"));
  VFE_destroy(VFeat);
  MMAUDIN_destroy(MMAudIn.mmaudin);

   // Release the audio source interfaces
  VBX_DEBUG(VBX_print("  VISEEngineObj::~VISEEngineObj(): releasing the audio source interfaces\n"));
  if (IAudioSourcePtr) IAudioSourcePtr->Release();
  if (IAudioPtr)       IAudioPtr->Release();

   // Destroy the command mailbox
  VBX_DEBUG(VBX_print("  VISEEngineObj::~VISEEngineObj(): destroying mailboxes\n"));
  MBOX_destroy(EngineMBox);
}

STDMETHODIMP
VISEEngineObj::SetSpeaker(PVISESpeakerProfile speaker)
{
  HRESULT  hresult;

   // Get the appropriate version of front end from the new speaker's profile
  Int  newFeVersion = SPKR_feVersion(speaker->VoiStream()->Speaker());

   // If there is a consistent version
  if (newFeVersion < 0) {
    VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker cannot get a consistent front-end version number from the speaker profile\n"));
    hresult = SRERR_NOUSERSELECTED;
  } else if (FeVersion == newFeVersion) {
    VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker found front-end version already set to the desired value (%d)\n", FeVersion));
    hresult = SRERR_NONE;
  } else if (SourceRefCnt) {
    VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker cannot reset busy source\n"));
    hresult = SRERR_WAVEDEVICEBUSY;
  } else {
    VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker: old FeVersion = %d; new FeVersion = %d\n", FeVersion, newFeVersion));
    Int  status;
    VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker calling VFE_setParameter\n"));
    if (VFE_setParameter(VFeat, FEVERSION, newFeVersion, &status)) {
      FeVersion = newFeVersion;
      VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker set front-end attributes for FeVersion %d\n", FeVersion));
      hresult = SetEngineDefaults();
    } else if (newFeVersion > 10000 && VFE_setParameter(VFeat, FEVERSION, newFeVersion-10000, &status)) {
      FeVersion = newFeVersion-10000;
      VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker set front-end attributes for FeVersion %d\n", FeVersion));
      hresult = SetEngineDefaults();
    } else {
      VBX_DEBUG(VBX_print("  VISEEngineObj::SetSpeaker cannot find the requested FeVersion (%d)\n", newFeVersion));
      hresult = SRERR_WAVEFORMATNOTSUPPORTED;
    }
  }

  return hresult;
}

STDMETHODIMP
VISEEngineObj::Deactivate()
{
  MBOX                 returnAddress = MBOX_create(Allocator, NULL);
  PVISEServiceRequest  serviceReqPtr = new VISEServiceRequest(SAPI_DEACTIVATE, NULL, returnAddress);
  if (MBOX_priorityInsert(EngineMBox, (OBJT) serviceReqPtr, SAPI_DEACTPRIORITY)) {
    while (TRUE) {
      Abort();
      if (MBOX_removeNow(returnAddress))
        break;
      siesta(DEACTIVATION_WAIT);
      VBX_DEBUG(VBX_print("  VISEEngineObj::Deactivate() waiting for acknowledgement of deactivation\n"));
    }
    VBX_DEBUG(VBX_print("  VISEEngineObj::Deactivate() aborted engine\n"));
  } else {
    delete serviceReqPtr;
  }
  MBOX_destroy(returnAddress);

   // Release the data source and stop it if it has no other active clients
  SN  currentSN;
  return SourceStop(&currentSN);
}

STDMETHODIMP
VISEEngineObj::Calibrate(PVISEResultsObj results)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::Activate(PVISEResultsObj results, PVISEGrammarRequest rule)
{
   // Encapsulate the arguments in a SAPI_ACTIVATE results service request
  PVISEResultsRequest  resultsReqPtr = new VISEResultsRequest(SAPI_ACTIVATE, results, rule->RuleName(), rule->AutoPause(), (MBOX) NULL);
   // Send the request to the engine at SAPI_RECOGPRIORITY
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_RECOGPRIORITY)) {
    return SRERR_NONE;
    VBX_DEBUG(VBX_print("  VISEEngineObj::Activate: Requesting recognition from active rule %p for results %p\n", rule, results));
  } else {
    delete resultsReqPtr;
    return E_UNEXPECTED;
  }
}

STDMETHODIMP
VISEEngineObj::Recognize(PVISEResultsObj results, PVISEGrammarRequest rule)
{
   // Encapsulate the arguments in a SAPI_RECOGNIZE results service request
  PVISEResultsRequest  resultsReqPtr = new VISEResultsRequest(SAPI_RECOGNIZE, results, rule->RuleName(), rule->AutoPause(), (MBOX) NULL);
   // Send the request to the engine at SAPI_RECOGPRIORITY
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_RECOGPRIORITY)) {
    return SRERR_NONE;
    VBX_DEBUG(VBX_print("  VISEEngineObj::Recognize: Requesting recognition from active rule %p for results %p\n", rule, results));
  } else {
    delete resultsReqPtr;
    return E_UNEXPECTED;
  }
}

STDMETHODIMP
VISEEngineObj::Force(PVISEResultsObj results)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::EnrollWord(PVISETrainingObj trainer)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::RetranscribeWord(PVISETrainingObj trainer)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::Train(PVISEResultsObj results)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::StartTrainingPass(PVISEResultsObj results)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::EndTrainingPass(PVISEResultsObj results, Bool finalPass)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::EndTrainingPass(PVISEResultsObj results, Char ** words, Bool finalPass)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::SaveModels(PVISEResultsObj results, Int minTrainingCount)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::SaveModels(PVISEResultsObj results, Char ** words, Int minTrainingCount)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::SetTimeout(UINT timeout)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEEngineObj::SourceAddRef()
{
  SourceRefCnt++;
  return SRERR_NONE;
}

STDMETHODIMP
VISEEngineObj::SourceRelease()
{
  if (SourceRefCnt) SourceRefCnt--;
  return SRERR_NONE;
}

VFE
VISEEngineObj::Source()
{
  return VFeat;
}

STDMETHODIMP
VISEEngineObj::SourceStart(SN * sn)
{
  HRESULT  hresult;
  Int      status;

   // Start the data source (or let it continue running) if the source reference count is non-zero
  if ((hresult = VFE_getSN(VFeat, sn, &status) ? SRERR_NONE : E_UNEXPECTED)) {
    VBX_DEBUG(VBX_print("  SourceStart: could not get sequence number (status = %s)\n", VISEERR_string((V_Err) status)));
  } else if ((hresult = SourceRefCnt ? SRERR_NONE : E_UNEXPECTED)) {
    VBX_DEBUG(VBX_print("  SourceStart: unreferenced source not started\n"));
  } else if (!VFE_startListening(VFeat, &status)) {
    hresult = VERR_IS_HRESULT((V_Err) status) ? VERR_TO_HRESULT((V_Err) status) : E_UNEXPECTED;
    VBX_DEBUG(VBX_print("  SourceStart: SourceStart failed (hresult = %ld)\n", hresult));
  } else {
    VBX_DEBUG(VBX_print("  SourceStart: started the source\n"));
  }

  return hresult;
}

STDMETHODIMP
VISEEngineObj::SourceStop(SN * sn)
{
  HRESULT  hresult = SRERR_NONE;
  Int      status;

   // Stop the data source if decrementing the source reference count takes it from 1 to 0
  if (!SourceRefCnt)
    VBX_DEBUG(VBX_print("  SourceStop: found the source already stopped\n"));
  else if (SourceRefCnt > 1)
    VBX_DEBUG(VBX_print("  SourceStop: found that the source still has %u other client(s)\n", SourceRefCnt - 1));
  else if ((hresult = VFE_stopListening(VFeat, &status) ? SRERR_NONE : E_UNEXPECTED))
    VBX_DEBUG(VBX_print("  SourceStop: SourceStop failed (hresult = %ld)\n", hresult));
  else
    VBX_DEBUG(VBX_print("  SourceStop: stopped the source\n"));

  if (!hresult && SourceRefCnt) SourceRefCnt--;

  if ((hresult = VFE_getSN(VFeat, sn, &status) ? SRERR_NONE : E_UNEXPECTED))
    VBX_DEBUG(VBX_print("  SourceStop: could not get sequence number (status = %s)\n", VISEERR_string((V_Err) status)));

  return hresult;
}
