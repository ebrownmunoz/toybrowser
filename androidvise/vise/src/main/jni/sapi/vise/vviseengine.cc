#include "pthr.h"
#include <sys/types.h>
#include <limits.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

 // Verbex SAPI Objects
#include "viseengine.hh"
#include "visenotify.hh"
#include "viseobj.hh"
#include "visephrase.hh"
#include "visekb.hh"
#include "visemessage.hh"
#include "visetrain.hh"
#include "viseresults.hh"
#include "vbxwave.h"

 // VULCAN Inclusions
#include "vocab.h"
#include "syntax.h"
#include "models.h"
#include "ekb.h"
#include "kb.h"
#include "hyp.h"
#include "spwrds.h"
#include "vbxtypes.h"
#include "vfe.h"
#include "vvise.h"
#include "visecomp.h"
#include "viseerr.h"
#include "viseparm.h"

#include "feversion.h"

 // Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ifaces.h"
#include "mbox.h"
#include "siesta.h"
#include "sn.h"
#include "viseerr.h"
#include "visemsg.h"

 // The DEBUG macros cause the VxWorks compiler to crash with an internal error...
#ifdef DEBUG
#define VISEENG_DEBUG
#endif

#ifdef VISEENG_DEBUG
#define VBX_DEBUG(P)              P
#define VBX_IF_NOT_DO(C,P)        if (!(C)) (P)
#define VBX_VERBOSE               TRUE
#define VBX_FATALSTATUS(R,S)      { Int v = (R); if (R) FATAL_BRA_ "condition = %d; status = %s", v, VISEERR_string(S) _KET; }
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)        C
#define VBX_VERBOSE               FALSE
#define VBX_FATALSTATUS(R,S)
#endif

#define ELAPSED_TIME(S,E)   (((E).tv_sec - (S).tv_sec) * 1000 + ((E).tv_usec - (S).tv_usec) / 1000)

// Time mesurement:
//  struct timeval startTime;
//  struct timeval endTime;
//  gettimeofday(&startTime, NULL);
//  CALIBRATION
//  gettimeofday(&endTime, NULL);
//  VBX_print("Calibration took = %ld milliseconds\n", ELAPSED_TIME(startTime, endTime));

 // A variant of a word (for enrollment)
typedef struct variant_s {
  Char *  name;
  UInt    numModels;
  UInt *  wids;
} variant_t, * VARIANT;

 // Some engine parameter defaults for supported VVISE Front End versions (for BATCH mode trained Voice Files !!!)
typedef struct feEngine_s {
  Int     feVersion;
  Int     absWCOffset;
  Int     relWCOffset;
  Int     muthaThreshold;
  Int     looseThreshold;
  Int     tightThreshold;
} feEngine_t;

 // FEVersion                 absWC  relWC  mutha  loose  tight
static feEngine_t feParTbl[] = {
  { FE_VISE11KHz,                70,    60,  1100,  1400,   700   },
  { FE_VISE10KHz,                60,    65,  1000,  1300,   700   },
  { FE_VISE8KHz,                 50,    60,   900,  1200,   600   },
  { FE_VISE11KHzNSS,            200,   120,  1200,  1500,   700   },
  { FE_VISE8KHzNSS,             150,   100,  1100,  1400,   700   },
  { FE_BADVERSION,                0,     0,     0,     0,     0   }
};

  // A VISE Engine Object
class VVISEEngineObj : VISEEngineObj {
  public:
    VVISEEngineObj(IMemory * allocator, IMail * mail, PVISEObj controlObjPtr, LPUNKNOWN audioUnknwnPtr);
   ~VVISEEngineObj();

    Bool                  Abort(void);
    STDMETHODIMP          Calibrate(PVISEResultsObj results);
    STDMETHODIMP          Force(PVISEResultsObj results);
    STDMETHODIMP          EnrollWord(PVISETrainingObj trainer);
    STDMETHODIMP          Train(PVISEResultsObj results);
    STDMETHODIMP          StartTrainingPass(PVISEResultsObj results);
    STDMETHODIMP          EndTrainingPass(PVISEResultsObj results, Bool finalPass);
    STDMETHODIMP          EndTrainingPass(PVISEResultsObj results, Char ** words, Bool finalPass);
    STDMETHODIMP          SaveModels(PVISEResultsObj results, Int minTrainingCount);
    STDMETHODIMP          SaveModels(PVISEResultsObj results, Char ** words, Int minTrainingCount);
    STDMETHODIMP          SetParameter(DWORD param);
    STDMETHODIMP          SetParameter(DWORD param, INT value);
    STDMETHODIMP          GetParameter(DWORD param, PINT pValue);
    STDMETHODIMP          SetParameter(DWORD param, FLOAT value);
    STDMETHODIMP          GetParameter(DWORD param, PFLOAT pValue);
    STDMETHODIMP          SetTimeout(UINT maxFrames);
    Bool                  doCalibration(SN startSN, SN endSN);

  private:
    STDMETHODIMP          SetEngineDefaults(void);
    void                  EngineMain(void);
    Bool                  EngineLoad(void);
    Bool                  EngineLoadJoker(void);

    VVISE                 VVise;                // The Search Engine

    IMBox               * AbortMBox;            // Mailbox for aborting SE
    VBXMSG                AbortPort;
    VBXMSG                TimeoutPort;

    Int                   FrameQLength;
    Int                   FrameQFWDTolerance;
    Int                   FrameQBWDTolerance;
    Int                   BOUPadLength;
    Int                   EOUPadLength;

     // Class members used by VISE_engineThread
    Bool                  EngineKbUpdate(PVISEResultsObj);
    VOCAB                 Silences;

    Int                   TrainingAvg;
    Int                   AbsoluteWC;
    Bool                  AbsoluteWCSet;
    Int                   RelativeWC;
    Bool                  RelativeWCSet;
    Int                   RelativeWCTrainCorrection;
    Int                   Muthah;
    Bool                  MuthahSet;
    Int                   BeamWidth;
    Bool                  BeamWidthSet;
    Int                   BOUBeamWidth;
    Bool                  BOUBeamWidthSet;
    Int                   MaxUnprocessedFrames;
    Int                   UnprocessedFrQuantum;
    Int                   PhraseTrainWeight;
    Int                   BatchTrainWeight;
    Int                   DfltTrainScore;
    Int                   DfltTrainCount;
    Int                   CalibrationLength;
    Int                   CalibrationWeight;
    Int                   CalibrationWCRel;
    Int                   CalibrationWCAbs;
    Int                   EnrollInitWCRel;
    Int                   EnrollInitWCAbs;
    Int                   EnrollInitWCCount;
    Int                   EnrollBodyWCRel;
    Int                   EnrollBodyWCAbs;
    Int                   EnrollBodyWCMinDwell;
    Int                   EnrollBodyWCMaxDwell;
    Int                   EnrollMinKernel;
    Int                   EnrollMaxKernel;
    Int                   EnrollMinDwell;
    Int                   EnrollMaxDwell;
    Float                 EnrollTolerance;
    Int                   SilAdaptWeight;
    Int                   SilAdaptOffset;
};
typedef VVISEEngineObj * PVVISEEngineObj;

#define FRAMEPERIOD               ((Double) 12.8)  // Should be able to get from FE
#define BYTESPERFRAME_MINIMUM     204              // At 8 KHz

#define VISE_QUEUELENGTH_MINIMUM  1
#define VISE_QUEUELENGTH_DEFAULT  2000
#define VISE_QUEUEFWDTOL_MINIMUM  0
#define VISE_QUEUEFWDTOL_DEFAULT  2
#define VISE_QUEUEBWDTOL_MINIMUM  0
#define VISE_QUEUEBWDTOL_DEFAULT  1
#define VISE_BOUPADLNGTH_MINIMUM  0
#define VISE_BOUPADLNGTH_DEFAULT  0
#define VISE_BOUPADLNGTH_MAXIMUM  BYTESPERFRAME_MINIMUM - 1
#define VISE_EOUPADLNGTH_MINIMUM  0
#define VISE_EOUPADLNGTH_DEFAULT  0
#define VISE_EOUPADLNGTH_MAXIMUM  BYTESPERFRAME_MINIMUM - 1

#define VISE_TRAINWEIGHT_MINIMUM  0                // New data replaces old model
#define VISE_TRAINWEIGHT_DEFAULT  6
#define VISE_TRAINWEIGHT_MAXIMUM  255              // Old model is unchanged
#define VISE_BATCHWEIGHT_MINIMUM  0                // New pass average replaces old model
#define VISE_BATCHWEIGHT_DEFAULT  0
#define VISE_BATCHWEIGHT_MAXIMUM  255              // 1 => new pass average and old model have equal weight
#define VISE_DFLTTRCOUNT_MINIMUM  0
#define VISE_DFLTTRCOUNT_DEFAULT  1
#define VISE_DFLTTRSCORE_MINIMUM  0
#define VISE_DFLTTRSCORE_DEFAULT  200
#define VISE_DFLTTRSCORE_MAXIMUM  1000
#define VISE_MAXUNPRFRMS_DEFAULT  6000
#define VISE_UNPRQUANTUM_DEFAULT  300
#define VISE_BATCHGROWTH_DEFAULT  80
#define VISE_BATCH_RELATIVE_CORR  0                // This value is added to the RelativeWC if Voice File is batch trained (default)
#define VISE_INCRM_RELATIVE_CORR  10               // This value is added to the RelativeWC if Voice File is incrementally trained
#define VISE_NOISETRKING_DEFAULT  0

 // Silence adaptation parameters
#define VISE_SILADAPTWGT_MINIMUM  0
#define VISE_SILADAPTWGT_DEFAULT  5
#define VISE_SILADAPTWGT_MAXIMUM  100
#define VISE_SILADAPTLAG_MINIMUM  0
#define VISE_SILADAPTLAG_DEFAULT  10
#define ADAPTATIONWEIGHTNORM      256 * VISE_SILADAPTWGT_MAXIMUM

 // Calibration parameters
#define CALIBRATIONLENGTH_DEFAULT 19
#define CALIBRATIONWEIGHT_DEFAULT 3
#define CALIBRATIONWCREL_DEFAULT  1000
#define CALIBRATIONWCABS_DEFAULT  500

 // Enrollment parameters
#define ENROLLINITWCREL_MINIMUM   0
#define ENROLLINITWCREL_DEFAULT   150
#define ENROLLINITWCABS_MINIMUM   0
#define ENROLLINITWCABS_DEFAULT   250
#define ENROLLINITWCMAXDW_DEFAULT 1
#define ENROLLINITWCCOUNT_MINIMUM 0
#define ENROLLINITWCCOUNT_DEFAULT 9
#define ENROLLBODYWCREL_MINIMUM   0
#define ENROLLBODYWCREL_DEFAULT   150
#define ENROLLBODYWCABS_MINIMUM   0
#define ENROLLBODYWCABS_DEFAULT   150
#define ENROLLBODYWCMINDW_MINIMUM 1
#define ENROLLBODYWCMINDW_DEFAULT 11
#define ENROLLBODYWCMAXDW_DEFAULT (MAXDWELL - ENROLLINITWCMAXDW_DEFAULT * ENROLLINITWCCOUNT_DEFAULT)
#define ENROLLMINKERNEL_MINIMUM   1
#define ENROLLMINKERNEL_DEFAULT   5
#define ENROLLMAXKERNEL_DEFAULT   25
#define ENROLLMINDWELL_MINIMUM    1
#define ENROLLMINDWELL_DEFAULT    1
#define ENROLLMAXDWELL_DEFAULT    10
#define ENROLLTOLERANCE_MINIMUM   0
#define ENROLLTOLERANCE_DEFAULT   40
#define ENROLLTOLERANCE_DIVISOR   100
 // Following is equal to ENROLL_MAXUTTERANCES in vise2/.../enroll.c
#define ENROLLMAXMODELS           100
#define ENROLLMAXVARIANTS         100
#define VARIANTDELIMITER          '_'


// Construct a VVISEEngineObj masquerading as a VISEEngineObj
PVISEEngineObj
VISE_createEngine(IMemory * allocator, IMail * mail, PVISEObj controlObjPtr, LPUNKNOWN audioUnknwnPtr)
{
  return((PVISEEngineObj) new VVISEEngineObj(allocator, mail, controlObjPtr, audioUnknwnPtr));
}

VVISEEngineObj::VVISEEngineObj(IMemory * allocator, IMail * mail, PVISEObj controlObjPtr, LPUNKNOWN audioUnknwnPtr):
VISEEngineObj(allocator, mail, controlObjPtr, audioUnknwnPtr),
TrainingAvg(0),
AbsoluteWC(VISE_UNINITIALIZED_INTEGER_THRESHOLD),
AbsoluteWCSet(FALSE),
RelativeWC(VISE_UNINITIALIZED_INTEGER_THRESHOLD),
RelativeWCSet(FALSE),
RelativeWCTrainCorrection(VISE_BATCH_RELATIVE_CORR),
Muthah(VISE_UNINITIALIZED_INTEGER_THRESHOLD),
MuthahSet(FALSE),
BeamWidth(VISE_UNINITIALIZED_INTEGER_THRESHOLD),
BeamWidthSet(FALSE),
BOUBeamWidth(VISE_UNINITIALIZED_INTEGER_THRESHOLD),
BOUBeamWidthSet(FALSE),
FrameQLength(VISE_QUEUELENGTH_DEFAULT),
FrameQFWDTolerance(VISE_QUEUEFWDTOL_DEFAULT),
FrameQBWDTolerance(VISE_QUEUEBWDTOL_DEFAULT),
BOUPadLength(VISE_BOUPADLNGTH_DEFAULT),
EOUPadLength(VISE_EOUPADLNGTH_DEFAULT),
MaxUnprocessedFrames(VISE_MAXUNPRFRMS_DEFAULT),
UnprocessedFrQuantum(VISE_UNPRQUANTUM_DEFAULT),
PhraseTrainWeight(VISE_TRAINWEIGHT_DEFAULT),
BatchTrainWeight(VISE_BATCHWEIGHT_DEFAULT),
DfltTrainScore(VISE_DFLTTRSCORE_DEFAULT),
DfltTrainCount(VISE_DFLTTRCOUNT_DEFAULT),
CalibrationLength(CALIBRATIONLENGTH_DEFAULT),
CalibrationWeight(CALIBRATIONWEIGHT_DEFAULT),
CalibrationWCRel(CALIBRATIONWCREL_DEFAULT),
CalibrationWCAbs(CALIBRATIONWCABS_DEFAULT),
EnrollInitWCRel(ENROLLINITWCREL_DEFAULT),
EnrollInitWCAbs(ENROLLINITWCABS_DEFAULT),
EnrollInitWCCount(ENROLLINITWCCOUNT_DEFAULT),
EnrollBodyWCRel(ENROLLBODYWCREL_DEFAULT),
EnrollBodyWCAbs(ENROLLBODYWCABS_DEFAULT),
EnrollBodyWCMinDwell(ENROLLBODYWCMINDW_DEFAULT),
EnrollBodyWCMaxDwell(ENROLLBODYWCMAXDW_DEFAULT),
EnrollMinKernel(ENROLLMINKERNEL_DEFAULT),
EnrollMaxKernel(ENROLLMAXKERNEL_DEFAULT),
EnrollMinDwell(ENROLLMINDWELL_DEFAULT),
EnrollMaxDwell(ENROLLMAXDWELL_DEFAULT),
EnrollTolerance((Float) ENROLLTOLERANCE_DEFAULT / (Float) ENROLLTOLERANCE_DIVISOR),
SilAdaptWeight(VISE_SILADAPTWGT_DEFAULT),
SilAdaptOffset(VISE_SILADAPTLAG_DEFAULT)
{
   // Create a mailbox for asynchronous access to the search engine
  V_Err  vstatus;
  AbortMBox = Mail->CreateMBox(Mail, "SE_ABORT", NUMPRIORITIES, &vstatus);
  VBX_FATALSTATUS(!AbortMBox, vstatus);
   // Create a message port for aborting the engine object
  AbortPort = MSG_create(Allocator, AbortMBox);
   // Create a message port for setting the engine object's timeout
  TimeoutPort = MSG_create(Allocator, AbortMBox);

   // Instantiate VVISE (thereby instantiating the Search Engine)
  VVise = VVISE_create(Allocator, Mail);
  VBX_fatalIf(!VVise);

   // Set up engine for regular or text mode according to value of AudioIn
  VVISE_setTextMode(VVise, NULL == AudioIn);

   // Download default parameters
  if (!VVISE_setParameter(VVise, NULL, FRAMEQLENGTH, FrameQLength) ||
      !VVISE_setParameter(VVise, NULL, FRAMEQFWDTOLERANCE, FrameQFWDTolerance) ||
      !VVISE_setParameter(VVise, NULL, FRAMEQBWDTOLERANCE, FrameQBWDTolerance) ||
      !VVISE_setParameter(VVise, NULL, BOUPADLENGTH, BOUPadLength) ||
      !VVISE_setParameter(VVise, NULL, EOUPADLENGTH, EOUPadLength) ||
      !VVISE_setParameter(VVise, NULL, MAXUNPROCESSEDFRAMES, MaxUnprocessedFrames) ||
      !VVISE_setParameter(VVise, NULL, UNPROCESSEDQUANTUM, UnprocessedFrQuantum) ||
      !VVISE_setParameter(VVise, NULL, FRAMECLASSSIZE, MaxUnprocessedFrames + FrameQLength + 1) ||
      !VVISE_setParameter(VVise, NULL, WEIGHTNORM, ADAPTATIONWEIGHTNORM))
    FATAL_BRA_ "Can't set VISE parameter (status = %s)", VISEERR_string(VVISE_status(VVise)) _KET;

   // Enforce default sigbits on Front End
  if (SRERR_NONE != SetParameter(VISE_SAMPSIGBITS))
    VBX_print("  VISEEngineObj::VISEEngineObj: ERROR setting default sigBits");
   // Start the engine thread
  VBX_fatalIf(pthread_create(&EngineThread, NULL, (pthread_startroutine_t) VISE_engineThread, (void *) this));
}

VVISEEngineObj::~VVISEEngineObj()
{
  void  * status;

   // Abort the engine
  Abort();

   // Shut down the engine thread
  PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_EXIT, NULL, NULL);
  VBX_fatalIf(!MBOX_priorityInsert(EngineMBox, (OBJT) serviceReqPtr, SAPI_FINISPRIORITY));
  VBX_DEBUG(VBX_print("  VISEEngineObj::~VISEEngineObj: sent SAPI_EXIT to EngineThread\n"));
  pthread_join(EngineThread, &status);

   // Destroy VVISE (thereby destroying the Search Engine)
  VBX_DEBUG(VBX_print("  VVISEEngineObj::~VVISEEngineObj(): destroying VVISE\n"));
  VVISE_destroy(VVise);

   // Destroy the engine object's abort message port
  MSG_destroy(AbortPort);

   // Destroy the engine object's timeout message port
  MSG_destroy(TimeoutPort);
}

HRESULT
VVISEEngineObj::SetEngineDefaults()
{
  HRESULT hresult = SRERR_NONE;

  if (VALID_FE_VISE(FeVersion)) {
    Int versionIndex;
    for (versionIndex = 0; feParTbl[versionIndex].feVersion != FE_BADVERSION; versionIndex++)
      if (FeVersion == feParTbl[versionIndex].feVersion) break;
    if (feParTbl[versionIndex].feVersion != FE_BADVERSION) {
      if (!AbsoluteWCSet)   AbsoluteWC = feParTbl[versionIndex].absWCOffset;
      if (!RelativeWCSet)   RelativeWC = feParTbl[versionIndex].relWCOffset;
      if (!MuthahSet)       Muthah = feParTbl[versionIndex].muthaThreshold;
      if (!BeamWidthSet)    BeamWidth = feParTbl[versionIndex].looseThreshold;
      if (!BOUBeamWidthSet) BOUBeamWidth = feParTbl[versionIndex].tightThreshold;
      VBX_DEBUG(VBX_print("  VVISEEngineObj::SetEngineDefaults: AWC = %d, RWC = %d, M = %d, BW = %d, BOUBW = %d\n",
                                                                AbsoluteWC, RelativeWC, Muthah, BeamWidth, BOUBeamWidth));
    } else {
      hresult = SRERR_VALUEOUTOFRANGE;
    }
  }

  return hresult;
}

STDMETHODIMP
VVISEEngineObj::SetParameter(DWORD param)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_SILMAXDWELL:
    result = SetParameter(VISE_SILMAXDWELL, VISE_FRAMES_TO_MILLISECONDS(IW_MAX_DEFAULT));
    break;
  case VISE_MAXDURATION:
    result = SetParameter(VISE_MAXDURATION, VISE_FRAMES_TO_MILLISECONDS(VISE_MAXFRAMECNT_DEFAULT));
    break;
  case VISE_MAXFRAMECNT:
    result = SetParameter(VISE_MAXFRAMECNT, VISE_MAXFRAMECNT_DEFAULT);
    break;
  case VISE_ENDMINDWELL:
    result = SetParameter(VISE_ENDMINDWELL, VISE_FRAMES_TO_MILLISECONDS(LS_MIN_DEFAULT));
    break;
  case VISE_ABSWILDCARD:
    result = SetParameter(VISE_ABSWILDCARD, VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_RELWILDCARD:
    result = SetParameter(VISE_RELWILDCARD, VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_MUTHATHRESH:
    result = SetParameter(VISE_MUTHATHRESH, VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_LOOSETHRESH:
    result = SetParameter(VISE_LOOSETHRESH, VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_TIGHTTHRESH:
    result = SetParameter(VISE_TIGHTTHRESH, VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_AUDQUEUELEN:
    result = SetParameter(VISE_AUDQUEUELEN, VISE_FRAMES_TO_MILLISECONDS(VISE_AUDQUEUELEN_DEFAULT));
    break;
  case VISE_BOUSILMSECS:
    result = SetParameter(VISE_BOUSILMSECS, VISE_BOUSILMSECS_DEFAULT);
    break;
  case VISE_EOUSILMSECS:
    result = SetParameter(VISE_EOUSILMSECS, VISE_EOUSILMSECS_DEFAULT);
    break;
  case VISE_SAMPSIGBITS:
    result = SetParameter(VISE_SAMPSIGBITS, MaxSigBitsDefault);
    break;
  case VISE_NOISETRKING:
    result = SetParameter(VISE_NOISETRKING, VISE_NOISETRKING_DEFAULT);
    break;
  case VISE_SAMPSPERSEC:
    result = SetParameter(VISE_SAMPSPERSEC, 0);
    break;
  case VISE_MAXUNPRFRMS:
    result = SetParameter(VISE_MAXUNPRFRMS, VISE_MAXUNPRFRMS_DEFAULT);
    break;
  case VISE_UNPRQUANTUM:
    result = SetParameter(VISE_UNPRQUANTUM, VISE_UNPRQUANTUM_DEFAULT);
    break;
  case VISE_QUEUELENGTH:
    result = SetParameter(VISE_QUEUELENGTH, VISE_QUEUELENGTH_DEFAULT);
    break;
  case VISE_QUEUEFWDTOL:
    result = SetParameter(VISE_QUEUEFWDTOL, VISE_QUEUEFWDTOL_DEFAULT);
    break;
  case VISE_QUEUEBWDTOL:
    result = SetParameter(VISE_QUEUEBWDTOL, VISE_QUEUEBWDTOL_DEFAULT);
    break;
  case VISE_BOUPADLNGTH:
    result = SetParameter(VISE_BOUPADLNGTH, VISE_FRAMES_TO_MILLISECONDS(VISE_BOUPADLNGTH_DEFAULT));
    break;
  case VISE_EOUPADLNGTH:
    result = SetParameter(VISE_EOUPADLNGTH, VISE_FRAMES_TO_MILLISECONDS(VISE_EOUPADLNGTH_DEFAULT));
    break;
  case VISE_PRINTFRAMES:
    result = SetParameter(VISE_PRINTFRAMES, 0);
    break;
  case VISE_PRINTSTATUS:
    result = SetParameter(VISE_PRINTSTATUS, VISE_PRINTSTATUS_DEFAULT);
    break;
  case VISE_TRAINWEIGHT:
    result = SetParameter(VISE_TRAINWEIGHT, VISE_TRAINWEIGHT_DEFAULT);
    break;
  case VISE_BATCHWEIGHT:
    result = SetParameter(VISE_BATCHWEIGHT, VISE_BATCHWEIGHT_DEFAULT);
    break;
  case VISE_DFLTTRCOUNT:
    result = SetParameter(VISE_DFLTTRCOUNT, VISE_DFLTTRCOUNT_DEFAULT);
    break;
  case VISE_DFLTTRSCORE:
    result = SetParameter(VISE_DFLTTRSCORE, VISE_DFLTTRSCORE_DEFAULT);
    break;
  case VISE_ENRINIWCREL:
    result = SetParameter(VISE_ENRINIWCREL, ENROLLINITWCREL_DEFAULT);
    break;
  case VISE_ENRINIWCABS:
    result = SetParameter(VISE_ENRINIWCABS, ENROLLINITWCABS_DEFAULT);
    break;
  case VISE_ENRINICOUNT:
    result = SetParameter(VISE_ENRINICOUNT, ENROLLINITWCCOUNT_DEFAULT);
    break;
  case VISE_ENRBODWCREL:
    result = SetParameter(VISE_ENRBODWCREL, ENROLLBODYWCREL_DEFAULT);
    break;
  case VISE_ENRBODWCABS:
    result = SetParameter(VISE_ENRBODWCABS, ENROLLBODYWCABS_DEFAULT);
    break;
  case VISE_ENRBODMINDW:
    result = SetParameter(VISE_ENRBODMINDW, ENROLLBODYWCMINDW_DEFAULT);
    break;
  case VISE_ENRBODMAXDW:
    result = SetParameter(VISE_ENRBODMAXDW, ENROLLBODYWCMAXDW_DEFAULT);
    break;
  case VISE_ENRMINKERNL:
    result = SetParameter(VISE_ENRMINKERNL, ENROLLMINKERNEL_DEFAULT);
    break;
  case VISE_ENRMAXKERNL:
    result = SetParameter(VISE_ENRMAXKERNL, ENROLLMAXKERNEL_DEFAULT);
    break;
  case VISE_ENRMINDWELL:
    result = SetParameter(VISE_ENRMINDWELL, ENROLLMINDWELL_DEFAULT);
    break;
  case VISE_ENRMAXDWELL:
    result = SetParameter(VISE_ENRMAXDWELL, ENROLLMAXDWELL_DEFAULT);
    break;
  case VISE_ENRTOLERNCE:
    result = SetParameter(VISE_ENRTOLERNCE, ENROLLTOLERANCE_DEFAULT);
    break;
  case VISE_CALIBLENGTH:
    result = SetParameter(VISE_CALIBLENGTH, CALIBRATIONLENGTH_DEFAULT);
    break;
  case VISE_CALIBWEIGHT:
    result = SetParameter(VISE_CALIBWEIGHT, CALIBRATIONWEIGHT_DEFAULT);
    break;
  case VISE_CALIBRWCREL:
    result = SetParameter(VISE_CALIBRWCREL, CALIBRATIONWCREL_DEFAULT);
    break;
  case VISE_CALIBRWCABS:
    result = SetParameter(VISE_CALIBRWCABS, CALIBRATIONWCABS_DEFAULT);
    break;
  case VISE_SILADAPTWGT:
    result = SetParameter(VISE_SILADAPTWGT, VISE_SILADAPTWGT_DEFAULT);
    break;
  case VISE_SILADAPTLAG:
    result = SetParameter(VISE_SILADAPTLAG, VISE_SILADAPTLAG_DEFAULT);
    break;
  default:
    result = SetParameter(param, 0);
  }
  return result;
}

STDMETHODIMP
VVISEEngineObj::SetParameter(DWORD param, INT value)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_SILMAXDWELL:
    IWMaxDwell = (UInt) VISE_MILLISECONDS_TO_FRAMES(value);
    break;
  case VISE_MAXDURATION:
    value = (INT) VISE_MILLISECONDS_TO_FRAMES(value);
  case VISE_MAXFRAMECNT:
    MaxFrameCount = value;
    break;
  case VISE_ENDMINDWELL:
    LSMinDwell = (UInt) VISE_MILLISECONDS_TO_FRAMES(value);
    break;
  case VISE_ABSWILDCARD:
    AbsoluteWC = value;
    AbsoluteWCSet = (value != VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_RELWILDCARD:
    RelativeWC = value;
    RelativeWCSet = (value != VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_MUTHATHRESH:
    Muthah = value;
    MuthahSet = (value != VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_LOOSETHRESH:
    BeamWidth = value;
    BeamWidthSet = (value != VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_TIGHTTHRESH:
    BOUBeamWidth = value;
    BOUBeamWidthSet = (value != VISE_UNINITIALIZED_INTEGER_THRESHOLD);
    break;
  case VISE_AUDQUEUELEN:
    if (value < 0) {
      result = SRERR_VALUEOUTOFRANGE;
    } else {
      if (SourceRefCnt) {
        VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_AUDQUEUELEN cannot reset busy Audio source\n"));
        result = SRERR_WAVEDEVICEBUSY;
      } else {
        value = (INT) VISE_MILLISECONDS_TO_FRAMES(value);
        Int status;
        if (!VFE_setParameter(VFeat, AUDQUEUELEN, value, &status)) {
          VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_AUDQUEUELEN cannot set AudioQLen: status = %s %d\n", VISEERR_string((V_Err) status)));
          result = E_UNEXPECTED;
        } else {
          VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_AUDQUEUELEN set AudioQLen to %d\n", value));
          AudioQLen = value;
          result = SRERR_NONE;
        }
      }
    }
    break;
  case VISE_BOUSILMSECS:
    BOUSilMSecs = value;
    break;
  case VISE_EOUSILMSECS:
    EOUSilMSecs = value;
    break;
  case VISE_SAMPSIGBITS:
    if (value < VISE_SAMPSIGBITS_MINIMUM || value > VISE_SAMPSIGBITS_MAXIMUM) {
      result = SRERR_VALUEOUTOFRANGE;
    } else {
      if (SourceRefCnt) {
        VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_SAMPSIGBITS cannot reset busy Jin source\n"));
        result = SRERR_WAVEDEVICEBUSY;
      } else {
         // Initialize the feature extractor with the new gain MaxSigBits
        Int  status;
        if (!VFE_setParameter(VFeat, SIGBITS, value, &status)) {
          VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_SAMPSIGBITS cannot set MaxSigBits: status = %s %d\n", VISEERR_string((V_Err) status)));
          result = E_UNEXPECTED;
        } else {
          VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_SAMPSIGBITS set MaxSigBits to %d\n", value));
          MaxSigBits = value;
          result = SRERR_NONE;
        }
      }
    }
    break;
  case VISE_NOISETRKING:
    if (SourceRefCnt) {
      VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_NOISETRKING cannot reset busy Jin source\n"));
      result = SRERR_WAVEDEVICEBUSY;
    } else {
       // Set FE NOISETRACKING parameter
      Int  status;
      if (!VFE_setParameter(VFeat, NOISETRACKING, value, &status)) {
        VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_NOISETRACKING cannot set parameter: status = %s %d\n", VISEERR_string((V_Err) status)));
        result = E_UNEXPECTED;
      } else {
        VBX_DEBUG(VBX_print("  EngineObj::SetParameter: VISE_NOISETRACKING parameter set to %d\n", value));
        result = SRERR_NONE;
      }
    }
    break;
  case VISE_SAMPSPERSEC:
    result = SRERR_NOTSUPPORTED;
    break;
  case VISE_MAXUNPRFRMS:
    MaxUnprocessedFrames = value;
    result = SRERR_NOTSUPPORTED;
    break;
  case VISE_UNPRQUANTUM:
    UnprocessedFrQuantum = value;
    result = SRERR_NOTSUPPORTED;
    break;
  case VISE_QUEUELENGTH:
    if (value < VISE_QUEUELENGTH_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      FrameQLength = value;
    break;
  case VISE_QUEUEFWDTOL:
    if (value < VISE_QUEUEFWDTOL_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      FrameQFWDTolerance = value;
    break;
  case VISE_QUEUEBWDTOL:
    if (value < VISE_QUEUEBWDTOL_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      FrameQBWDTolerance = value;
    break;
  case VISE_BOUPADLNGTH:
    if ((value = (Int) VISE_MILLISECONDS_TO_FRAMES(value)) < VISE_BOUPADLNGTH_MINIMUM || value > VISE_BOUPADLNGTH_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      BOUPadLength = value;
    break;
  case VISE_EOUPADLNGTH:
    if ((value = (Int) VISE_MILLISECONDS_TO_FRAMES(value)) < VISE_EOUPADLNGTH_MINIMUM || value > VISE_EOUPADLNGTH_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EOUPadLength = value;
    break;
  case VISE_PRINTFRAMES:
    result = SRERR_NOTSUPPORTED;
    break;
  case VISE_PRINTSTATUS:
    PrintStats = value;
    break;
  case VISE_TRAINWEIGHT:
    if (value < VISE_TRAINWEIGHT_MINIMUM || value > VISE_TRAINWEIGHT_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      PhraseTrainWeight = value;
    break;
  case VISE_BATCHWEIGHT:
    if (value < VISE_BATCHWEIGHT_MINIMUM || value > VISE_BATCHWEIGHT_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      BatchTrainWeight = value;
    break;
  case VISE_DFLTTRCOUNT:
    if (value < VISE_DFLTTRCOUNT_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      DfltTrainCount = value;
    break;
  case VISE_DFLTTRSCORE:
    if (value < VISE_DFLTTRSCORE_MINIMUM || value > VISE_DFLTTRSCORE_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      DfltTrainScore = value;
    break;
  case VISE_ENRINIWCREL:
    if (value < ENROLLINITWCREL_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollInitWCRel = value;
    break;
  case VISE_ENRINIWCABS:
    if (value < ENROLLINITWCABS_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollInitWCAbs = value;
    break;
  case VISE_ENRINICOUNT:
    if (value < ENROLLINITWCCOUNT_MINIMUM || value * ENROLLINITWCMAXDW_DEFAULT > MAXDWELL - EnrollBodyWCMaxDwell)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollInitWCCount = value;
    break;
  case VISE_ENRBODWCREL:
    if (value < ENROLLBODYWCREL_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollBodyWCRel = value;
    break;
  case VISE_ENRBODWCABS:
    if (value < ENROLLBODYWCABS_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollBodyWCAbs = value;
    break;
  case VISE_ENRBODMINDW:
    if (value < ENROLLBODYWCMINDW_MINIMUM || value > EnrollBodyWCMaxDwell)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollBodyWCMinDwell = value;
    break;
  case VISE_ENRBODMAXDW:
    if (value < EnrollBodyWCMinDwell || value > MAXDWELL - ENROLLINITWCMAXDW_DEFAULT * EnrollInitWCCount)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollBodyWCMaxDwell = value;
    break;
  case VISE_ENRMINKERNL:
    if (value < ENROLLMINKERNEL_MINIMUM || value > EnrollMaxKernel)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollMinKernel = value;
    break;
  case VISE_ENRMAXKERNL:
    if (value < EnrollMinKernel || value * EnrollMaxDwell > MAXDWELL)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollMaxKernel = value;
    break;
  case VISE_ENRMINDWELL:
    if (value < ENROLLMINDWELL_MINIMUM || value > EnrollMaxDwell)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollMinDwell = value;
    break;
  case VISE_ENRMAXDWELL:
    if (value * EnrollMaxKernel > MAXDWELL)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollMaxDwell = value;
    break;
  case VISE_ENRTOLERNCE:
    if (value < ENROLLTOLERANCE_MINIMUM || value > ENROLLTOLERANCE_DIVISOR)
      result = SRERR_VALUEOUTOFRANGE;
    else
      EnrollTolerance = (Float) value / (Float) ENROLLTOLERANCE_DIVISOR;
    break;
  case VISE_CALIBLENGTH:
    CalibrationLength = value;
    break;
  case VISE_CALIBWEIGHT:
    CalibrationWeight = value;
    break;
  case VISE_CALIBRWCREL:
    CalibrationWCRel = value;
    break;
  case VISE_CALIBRWCABS:
    CalibrationWCAbs = value;
    break;
  case VISE_SILADAPTWGT:
    if (value < VISE_SILADAPTWGT_MINIMUM || value > VISE_SILADAPTWGT_MAXIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      SilAdaptWeight = value;
    break;
  case VISE_SILADAPTLAG:
    if (value < VISE_SILADAPTLAG_MINIMUM)
      result = SRERR_VALUEOUTOFRANGE;
    else
      SilAdaptOffset = value;
    break;
  case VISE_FRONTENDVER:
  case VISE_WCTHRESHOLD:
  case VISE_MAXRESLTLEN:
  case VISE_SHOWFRMDATA:
  case VISE_STARTSHOWFR:
  case VISE_FINALSHOWFR:
  case VISE_SHOWALLTBKS:
  case VISE_SHOWFINALTB:
  case VISE_SHOWTBSTRNG:
  case VISE_MAXNODDWELL:
  case VISE_FORWARDONLY:
  case VISE_VSBEAMWIDTH:
  case VISE_NPBEAMWIDTH:
  case VISE_NWBEAMWIDTH:
  case VISE_CHANSPERFRM:
  case VISE_MAXCPFLBINS:
  case VISE_METTHRCOUNT:
  case VISE_CMTTHRCOUNT:
  case VISE_NMTTHRCOUNT:
  case VISE_USEPROFILER:
  case VISE_USEBACKOFFP:
  case VISE_SHOWALTPRON:
  case VISE_FORCEFINISH:
  case VISE_NBSTFWDTOLR:
  case VISE_NBSTBWDTOLR:
  case VISE_NBSTSTKSIZE:
  case VISE_NBSTPRUNSIZ:
  case VISE_PATHTBLSIZE:
  case VISE_SHOWCEPDATA:
  case VISE_SHOWFEATDAT:
  case VISE_SHOWVQFRAME:
  case VISE_SHOWEPFRAME:
  case VISE_USECEPPACKS:
  case VISE_EPCLASSSIZE:
  case VISE_EPTHREADCNT:
  case VISE_NPRECOMPFWD:
  case VISE_NPRECOMPBWD:
  case VISE_USEWILDCARD:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}

STDMETHODIMP
VVISEEngineObj::GetParameter(DWORD param, PINT pValue)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_SILMAXDWELL:
    *pValue = (INT) VISE_FRAMES_TO_MILLISECONDS(IWMaxDwell);
    break;
  case VISE_MAXDURATION:
    *pValue = (INT) VISE_FRAMES_TO_MILLISECONDS(MaxFrameCount);
    break;
  case VISE_MAXFRAMECNT:
    *pValue = (INT) MaxFrameCount;
    break;
  case VISE_ENDMINDWELL:
    *pValue = (INT) VISE_FRAMES_TO_MILLISECONDS(LSMinDwell);
    break;
  case VISE_ABSWILDCARD:
    *pValue = AbsoluteWC;
    if (!AbsoluteWCSet) result = SRWARN_UNINITIALIZED_PARAMETER;
    break;
  case VISE_RELWILDCARD:
    *pValue = RelativeWC;
    if (!RelativeWCSet) result = SRWARN_UNINITIALIZED_PARAMETER;
    break;
  case VISE_MUTHATHRESH:
    *pValue = Muthah;
    if (!MuthahSet) result = SRWARN_UNINITIALIZED_PARAMETER;
    break;
  case VISE_LOOSETHRESH:
    *pValue = BeamWidth;
    if (!BeamWidthSet) result = SRWARN_UNINITIALIZED_PARAMETER;
    break;
  case VISE_TIGHTTHRESH:
    *pValue = BOUBeamWidth;
    if (!BOUBeamWidthSet) result = SRWARN_UNINITIALIZED_PARAMETER;
    break;
  case VISE_AUDQUEUELEN:
    *pValue = (INT) VISE_FRAMES_TO_MILLISECONDS(AudioQLen);
    break;
  case VISE_BOUSILMSECS:
    *pValue = BOUSilMSecs;
    break;
  case VISE_EOUSILMSECS:
    *pValue = EOUSilMSecs;
    break;
  case VISE_SAMPSIGBITS: {
    Int status;
     // Get the parameter from the feature extractor, if possible
    if (VFE_getParameter(VFeat, SIGBITS, pValue, &status)) {
      MaxSigBits = *pValue;
     // Otherwise, just report the local value
    } else {
      *pValue = MaxSigBits;
    }
    break;
  }
  case VISE_SAMPSPERSEC: {
    Int status;
     // Get the parameter from the feature extractor, if possible
    if (VFE_getParameter(VFeat, SAMPLERATE, pValue, &status)) {
      SampsPerSec = *pValue;
     // Otherwise, just report the local value
    } else {
      *pValue = SampsPerSec;
    }
    break;
  }
  case VISE_NOISETRKING: {
    Int status;
    *pValue = 0;
    if (!VFE_getParameter(VFeat, NOISETRACKING, pValue, &status))
      VBX_DEBUG(VBX_print("  EngineObj::GetParameter: VISE_NOISETRACKING cannot get parameter: status = %s %d\n", VISEERR_string((V_Err) status)));
    break;
  }
  case VISE_MAXUNPRFRMS:
    *pValue = MaxUnprocessedFrames;
    break;
  case VISE_UNPRQUANTUM:
    *pValue = UnprocessedFrQuantum;
    break;
  case VISE_QUEUELENGTH:
    *pValue = FrameQLength;
    break;
  case VISE_QUEUEFWDTOL:
    *pValue = FrameQFWDTolerance;
    break;
  case VISE_QUEUEBWDTOL:
    *pValue = FrameQBWDTolerance;
    break;
  case VISE_BOUPADLNGTH:
    *pValue = BOUPadLength;
    break;
  case VISE_EOUPADLNGTH:
    *pValue = EOUPadLength;
    break;
  case VISE_PRINTFRAMES:
    *pValue = 0;
    result = SRERR_NOTSUPPORTED;
    break;
  case VISE_PRINTSTATUS:
    *pValue = PrintStats;
    break;
  case VISE_TRAINWEIGHT:
    *pValue = PhraseTrainWeight;
    break;
  case VISE_BATCHWEIGHT:
    *pValue = BatchTrainWeight;
    break;
  case VISE_DFLTTRCOUNT:
    *pValue = DfltTrainCount;
    break;
  case VISE_DFLTTRSCORE:
    *pValue = DfltTrainScore;
    break;
  case VISE_ENRINIWCREL:
    *pValue = EnrollInitWCRel;
    break;
  case VISE_ENRINIWCABS:
    *pValue = EnrollInitWCAbs;
    break;
  case VISE_ENRINICOUNT:
    *pValue = EnrollInitWCCount;
    break;
  case VISE_ENRBODWCREL:
    *pValue = EnrollBodyWCRel;
    break;
  case VISE_ENRBODWCABS:
    *pValue = EnrollBodyWCAbs;
    break;
  case VISE_ENRBODMINDW:
    *pValue = EnrollBodyWCMinDwell;
    break;
  case VISE_ENRBODMAXDW:
    *pValue = EnrollBodyWCMaxDwell;
    break;
  case VISE_ENRMINKERNL:
    *pValue = EnrollMinKernel;
    break;
  case VISE_ENRMAXKERNL:
    *pValue = EnrollMaxKernel;
    break;
  case VISE_ENRMINDWELL:
    *pValue = EnrollMinDwell;
    break;
  case VISE_ENRMAXDWELL:
    *pValue = EnrollMaxDwell;
    break;
  case VISE_ENRTOLERNCE:
    *pValue = (INT) (EnrollTolerance * (Float) 100);
    break;
  case VISE_CALIBLENGTH:
    *pValue = CalibrationLength;
    break;
  case VISE_CALIBWEIGHT:
    *pValue = CalibrationWeight;
    break;
  case VISE_CALIBRWCREL:
    *pValue = CalibrationWCRel;
    break;
  case VISE_CALIBRWCABS:
    *pValue = CalibrationWCAbs;
    break;
  case VISE_SILADAPTWGT:
    *pValue = SilAdaptWeight;
    break;
  case VISE_SILADAPTLAG:
    *pValue = SilAdaptOffset;
    break;
  case VISE_FRONTENDVER:
    *pValue = FeVersion;
    break;
  case VISE_TRAINSCHEME:
      *pValue = (Vocabulary) ? MODELS_trainScheme(VOCAB_models(Vocabulary)) : -1;
    break;
  case VISE_WCTHRESHOLD:
  case VISE_MAXRESLTLEN:
  case VISE_SHOWFRMDATA:
  case VISE_STARTSHOWFR:
  case VISE_FINALSHOWFR:
  case VISE_SHOWALLTBKS:
  case VISE_SHOWFINALTB:
  case VISE_SHOWTBSTRNG:
  case VISE_MAXNODDWELL:
  case VISE_FORWARDONLY:
  case VISE_VSBEAMWIDTH:
  case VISE_NPBEAMWIDTH:
  case VISE_NWBEAMWIDTH:
  case VISE_CHANSPERFRM:
  case VISE_MAXCPFLBINS:
  case VISE_METTHRCOUNT:
  case VISE_CMTTHRCOUNT:
  case VISE_NMTTHRCOUNT:
  case VISE_USEPROFILER:
  case VISE_USEBACKOFFP:
  case VISE_SHOWALTPRON:
  case VISE_FORCEFINISH:
  case VISE_NBSTFWDTOLR:
  case VISE_NBSTBWDTOLR:
  case VISE_NBSTSTKSIZE:
  case VISE_NBSTPRUNSIZ:
  case VISE_PATHTBLSIZE:
  case VISE_SHOWCEPDATA:
  case VISE_SHOWFEATDAT:
  case VISE_SHOWVQFRAME:
  case VISE_SHOWEPFRAME:
  case VISE_USECEPPACKS:
  case VISE_EPCLASSSIZE:
  case VISE_EPTHREADCNT:
  case VISE_NPRECOMPFWD:
  case VISE_NPRECOMPBWD:
  case VISE_USEWILDCARD:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}

STDMETHODIMP
VVISEEngineObj::SetParameter(DWORD param, FLOAT value)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_VSBEAMWIDTH:
  case VISE_NPBEAMWIDTH:
  case VISE_NWBEAMWIDTH:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}

STDMETHODIMP
VVISEEngineObj::GetParameter(DWORD param, PFLOAT pValue)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_VSBEAMWIDTH:
  case VISE_NPBEAMWIDTH:
  case VISE_NWBEAMWIDTH:
    result = SRERR_NOTSUPPORTED;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}

Bool
VVISEEngineObj::Abort()
{
  return VVISE_abort(VVise, AbortPort);
}

STDMETHODIMP
VVISEEngineObj::Calibrate(PVISEResultsObj results)
{
  HRESULT  hresult;

   // Make sure Source is not running
  if (SourceRefCnt) {
    VBX_DEBUG(VBX_print("  EngineObj::Calibrate: cannot calibrate busy FE\n"));
    return SRERR_WAVEDEVICEBUSY;
  }
   // Encapsulate the results object in a SAPI_CALIBRATE results service request
  PVISEResultsRequest  resultsReqPtr = new VISEResultsRequest(SAPI_CALIBRATE, results, "", FALSE, NULL);
   // Reference the data source
  SourceRefCnt++;
   // Send the request to the engine
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_CALIBPRIORITY)) {
    hresult = SRERR_NONE;
    VBX_DEBUG(VBX_print("  VVISEEngineObj::Calibrate: sent SAPI_CALIBRATE to EngineThread in request @%p for results @%p\n", resultsReqPtr, results));
  } else {
    delete resultsReqPtr;
    SourceRefCnt--;
    hresult = E_UNEXPECTED;
  }

  return hresult;
}

STDMETHODIMP
VVISEEngineObj::Force(PVISEResultsObj results)
{
   // Encapsulate the results object in a SAPI_RECOGNIZE results service request
  PVISEResultsRequest resultsReqPtr = new VISEResultsRequest(SAPI_RECOGNIZE, results, NULL, FALSE, (MBOX) NULL);
   // Send the request to the engine at SAPI_FORCEPRIORITY
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_FORCEPRIORITY)) {
    VBX_DEBUG(VBX_print("  VISEEngineObj::Force sent SAPI_RECOGNIZE to EngineThread\n"));
    return SRERR_NONE;
  } else {
    delete resultsReqPtr;
    return E_UNEXPECTED;
  }
}

STDMETHODIMP
VVISEEngineObj::EnrollWord(PVISETrainingObj trainer)
{
  HRESULT  hresult;

   // Encapsulate the training object in a SAPI_ENROLLWORD service request
  MBOX                returnAddress = MBOX_create(Allocator, NULL);
  PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_ENROLLWORD, (Ptr) trainer, returnAddress);
   // Send the request to the engine at SAPI_FORCEPRIORITY
  if (MBOX_priorityInsert(EngineMBox, (OBJT) serviceReqPtr, SAPI_FORCEPRIORITY)) {
    VBX_DEBUG(VBX_print("  VVISEEngineObj::EnrollWord: sent SAPI_ENROLLWORD to EngineThread\n"));
     // Wait for the reply; this is a synchronous operation
    hresult = (HRESULT) MBOX_remove(returnAddress);
  } else {
    delete serviceReqPtr;
    VBX_DEBUG(VBX_print("  VVISEEngineObj::EnrollWord: FAILURE sending service request to Engine object\n"));
    hresult =  E_UNEXPECTED;
  }
  MBOX_destroy(returnAddress);

  return hresult;
}

STDMETHODIMP
VVISEEngineObj::Train(PVISEResultsObj results)
{
   // Encapsulate the training object in a SAPI_TRAIN service request
  PVISEResultsRequest resultsReqPtr = new VISEResultsRequest(SAPI_TRAIN, results, NULL, FALSE, (MBOX) NULL);
   // Send the request to the engine at SAPI_FORCEPRIORITY
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_TRAINPRIORITY)) {
    VBX_DEBUG(VBX_print("  VVISEEngineObj::Train: sent SAPI_TRAIN to EngineThread\n"));
    return SRERR_NONE;
  } else {
    delete resultsReqPtr;
    return E_UNEXPECTED;
  }
}

STDMETHODIMP
VVISEEngineObj::StartTrainingPass(PVISEResultsObj results)
{
  HRESULT  hresult;

   // Encapsulate the arguments in a SAPI_STARTTRAININGPASS results service request
  MBOX                 returnAddress = MBOX_create(Allocator, NULL);
  PVISEResultsRequest  resultsReqPtr = new VISEResultsRequest(SAPI_STARTTRAININGPASS, results, results->RuleName(), FALSE, returnAddress);
   // Send the request to the engine
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_BATCHPRIORITY)) {
    VBX_DEBUG(VBX_print("  VVISEEngineObj::StartTrainingPass: rule \"%s\" for results %p\n",  results->RuleName(), results));
     // Wait for the reply; this is a synchronous operation
    hresult = (HRESULT) MBOX_remove(returnAddress);
  } else {
    delete resultsReqPtr;
    hresult = E_UNEXPECTED;
  }
  MBOX_destroy(returnAddress);

  return hresult;
}

STDMETHODIMP
VVISEEngineObj::EndTrainingPass(PVISEResultsObj results, Bool finalPass)
{
  return EndTrainingPass(results, NULL, finalPass);
}

STDMETHODIMP
VVISEEngineObj::EndTrainingPass(PVISEResultsObj results, Char ** words, Bool finalPass)
{
  HRESULT  hresult;

   // Encapsulate the arguments in a SAPI_ENDTRAININGPASS results service request
  MBOX                 returnAddress = MBOX_create(Allocator, NULL);
  PVISEResultsRequest  resultsReqPtr = new VISEResultsRequest(SAPI_ENDTRAININGPASS, results, results->RuleName(), finalPass, returnAddress);
  resultsReqPtr->SetWordNames(words);
   // Send the request to the engine
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_BATCHPRIORITY)) {
    VBX_DEBUG(VBX_print("  VVISEEngineObj::EndTrainingPass: rule \"%s\" for results %p\n",  results->RuleName(), results));
     // Wait for the reply; this is a synchronous operation
    hresult = (HRESULT) MBOX_remove(returnAddress);
  } else {
    delete resultsReqPtr;
    hresult = E_UNEXPECTED;
  }
  MBOX_destroy(returnAddress);

  return hresult;
}

STDMETHODIMP
VVISEEngineObj::SaveModels(PVISEResultsObj results, Int minTrainingCount)
{
  return SaveModels(results, NULL, minTrainingCount);
}

STDMETHODIMP
VVISEEngineObj::SaveModels(PVISEResultsObj results, Char ** words, Int minTrainingCount)
{
  HRESULT  hresult;

   // Encapsulate the arguments in a SAPI_SAVEMODELS results service request
  MBOX                 returnAddress = MBOX_create(Allocator, NULL);
  PVISEResultsRequest  resultsReqPtr = new VISEResultsRequest(SAPI_SAVEMODELS, results, results->RuleName(), FALSE, minTrainingCount, returnAddress);
  resultsReqPtr->SetWordNames(words);
   // Send the request to the engine
  if (MBOX_priorityInsert(EngineMBox, (OBJT) resultsReqPtr, SAPI_BATCHPRIORITY)) {
    VBX_DEBUG(VBX_print("  VVISEEngineObj::SaveModels: rule \"%s\" for results %p\n",  results->RuleName(), results));
     // Wait for the reply; this is a synchronous operation
    hresult = (HRESULT) MBOX_remove(returnAddress);
  } else {
    delete resultsReqPtr;
    hresult = E_UNEXPECTED;
  }
  MBOX_destroy(returnAddress);

  return hresult;
}

STDMETHODIMP
VVISEEngineObj::SetTimeout(UINT timeout)
{
  UInt maxFrames = (UInt) VISE_MILLISECONDS_TO_FRAMES(timeout);

  return VVISE_setTimeout(VVise, TimeoutPort, maxFrames) ? (SRERR_NONE) : (SRERR_INVALIDMODE);
}

void
VISE_engineThread(VISEEngineObj * viseEngineObj)
{
  viseEngineObj->EngineMain();
}

void
VVISEEngineObj::EngineMain()
{
  PVISEServiceRequest   serviceReqPtr = NULL;
  PVISEResultsRequest   resultsReqPtr = NULL;
  PVISEResultsObj       resultsObjPtr = NULL;
  SAPIVISECmd           service;
  MBOX                  returnAddress = NULL;

  UInt                  nBest = 1;

  Ptr                   acousticFeatures;
  SN                    startSN = 0;
  SN                    endSN = 0;
  SN                    hypStartSN;
  Int                   snStep = 1;
  UInt                  numFrames = 0;
  Bool                  live = TRUE;
  Bool                  abortable = TRUE;
  Int                   rstatus;
  HYP                   hyp = NULL;
  Bool                  firstPass = FALSE;
  Bool                  finalPass = FALSE;
  Bool                  calibrateViaActivate;
  Char **               wordNames = NULL;
  HRESULT               hresult = SRERR_NONE;

  V_Err                 vstatus = VISESUCCESS;

   // Get VVISE
  VVISE  vvise = VVise;

  VBX_print("EngineThread is ALIVE\n");

   // Initialize the knowledge base variables
  KbObjPtr = NULL;
  Kb = NULL;
  VocabId = 0;
  Vocabulary = NULL;
  Silences = NULL;
  GrammarId = 0;
  BatchTrain = FALSE;

  while (TRUE) {

    serviceReqPtr = (PVISEServiceRequest) MBOX_remove(EngineMBox);
    returnAddress = serviceReqPtr->ReturnAddress();
  
    switch (service = serviceReqPtr->Service()) {

    case SAPI_ACTIVATE:
    case SAPI_RECOGNIZE:
    case SAPI_TRAIN:
    case SAPI_CALIBRATE: {

      resultsReqPtr = (PVISEResultsRequest) serviceReqPtr;

      VBX_DEBUG(VBX_print("  EngineThread: received %s\n", SAPIVISECmd_string(service)));

       // Get the originating results object
      resultsObjPtr = resultsReqPtr->ResultsObject();

      calibrateViaActivate = (resultsObjPtr->RuleName() != NULL &&
                              0 == strcmp(resultsObjPtr->RuleName(), CALIBRATIONGRAMMARNAME)) ? TRUE : FALSE;

       // Download parameters that may have changed (has no effect until SOURCESTART or WARMSTART)
      if (!VVISE_setParameter(vvise, NULL, FRAMEQLENGTH, FrameQLength) ||
          !VVISE_setParameter(vvise, NULL, FRAMEQFWDTOLERANCE, FrameQFWDTolerance) ||
          !VVISE_setParameter(vvise, NULL, FRAMEQBWDTOLERANCE, FrameQBWDTolerance) ||
          !VVISE_setParameter(vvise, NULL, BOUPADLENGTH, BOUPadLength) ||
          !VVISE_setParameter(vvise, NULL, EOUPADLENGTH, EOUPadLength))
        FATAL_BRA_ "Can't set VISE parameter (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;

       // Download the required vocabulary if necessary; otherwise, just download the silences, wildcard and thresholds, which may have changed
      if (!EngineKbUpdate(resultsObjPtr)) {
        VBX_DEBUG(VBX_print("  EngineThread: Vocabulary %d, containing rule \"%s\", is already loaded\n", VocabId, resultsObjPtr->RuleName()));
        if (!VVISE_downloadWildcard(vvise, JOKER_WID, JOKER_WID, 1, 1, RelativeWC + RelativeWCTrainCorrection, TrainingAvg + AbsoluteWC))
          FATAL_BRA_ "Can't adjust the wildcard (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        VBX_DEBUG(VBX_print("  EngineThread: downloaded wildcard (Rel = %d, DAbs = %d, Abs = %d)\n",
                            RelativeWC + RelativeWCTrainCorrection, AbsoluteWC, TrainingAvg + AbsoluteWC));
        VOCAB_setModelDwells(Silences, LONG_SILENCE, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10));
        VOCAB_setModelDwells(Silences, LONG_ADAPTIVE_SIL, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10));
        if (!VVISE_downloadModel(vvise, Silences, LONG_SILENCE, LONG_SILENCE) ||
            !VVISE_downloadModel(vvise, Silences, LONG_ADAPTIVE_SIL, LONG_ADAPTIVE_SIL))
          FATAL_BRA_ "Can't adjust the long silences (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        if (!VVISE_setThresholds(vvise, (UInt) BeamWidth, (UInt) BeamWidth, (UInt) BeamWidth, (UInt) BOUBeamWidth, (UInt) Muthah))
          FATAL_BRA_ "Can't set VISE thresholds (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
      } else if (BatchTrain) {
        SEVERE_BRA_ "User Vocabulary reloaded amid Batch Training !!!\n" _KET;
      }

       // Get current SN increment as 2*frameShift
      FEVER_getFrameShift(FeVersion, &snStep);
      snStep *= 2;

       // If there is no data in the results object, the engine must get its acoustic data from the audio source
      if (live = !(acousticFeatures = resultsObjPtr->AcousticFeatures(&startSN, &endSN, &numFrames))) {
        if (service == SAPI_CALIBRATE || calibrateViaActivate) {
          Int status;
          if (NULL == AudioIn) {
            VBX_DEBUG(VBX_print("  EngineThread: No calibration in text mode !!!\n"));
            ControlObjPtr->Calibrated(SRERR_NONE);
            delete resultsReqPtr;
            break;
          }
          if (!VFE_startCalibration(VFeat, CalibrationLength * NUM_MULTISILENCES + 1, &status)) {
            VBX_DEBUG(VBX_print("  EngineThread: cannot set the source in calibration mode (status = %d)\n", status));
            ControlObjPtr->Calibrated(E_UNEXPECTED);
            delete resultsReqPtr;
            break;
          }
        }
        if ((hresult = SourceStart(&startSN))) {  // The starting frame is the next frame the source can produce
          Int status;
          VBX_DEBUG(VBX_print("  EngineThread: cannot start the source (hresult = %8.8x)\n", hresult));
          SourceStop(&startSN);
           // An activation requires disconfirmation
          if (service == SAPI_ACTIVATE) ControlObjPtr->Activated(hresult);
          if (service == SAPI_CALIBRATE || calibrateViaActivate) VFE_stopCalibration(VFeat, &status);
          if (service == SAPI_CALIBRATE) ControlObjPtr->Calibrated(E_UNEXPECTED);
          delete resultsReqPtr;
          break;
        } else {
          endSN = - (V_SN) 1;                     // The ending frame is whenever it is
          VBX_DEBUG(VBX_print("  EngineThread: set up for live data acquisition\n"));
        }
       // Otherwise, it will get its acoustic data from the results object
      } else {
        if (!VVISE_downloadJin(vvise, (JIN2FR) acousticFeatures, &startSN, &endSN, &numFrames))
          FATAL_BRA_ "Can't download jin data (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        VBX_DEBUG(VBX_print("  EngineThread: set up to use feature data stored in the results object\n"));
      }

       // Get the actual starting SN and report it to the application (should do this in recog, but can't yet)
      VBX_DEBUG(VBX_print("  EngineThread: starting SN = " QuadFmt(-) "\n", startSN));
      if (resultsObjPtr->NotifySink()) resultsObjPtr->NotifySink()->PhraseStart(startSN);

       // If this is a REQUEST FOR RECOGNITION, DO RECOGNITION
      if (service == SAPI_RECOGNIZE || service == SAPI_ACTIVATE) {

         // An activation requires confirmation
        if (service == SAPI_ACTIVATE) ControlObjPtr->Activated(SRERR_NONE);

        UInt  temporaryGrammarId;

         // If a transcription exists, force the recognition to produce the transcription
        if (resultsObjPtr->Transcription()) {
          GrammarId = temporaryGrammarId = KB_unusedGrammarId(Kb);
          PVISESAPIPhrase  sapiPhrasePtr = new VISESAPIPhrase(resultsObjPtr->Transcription(), Vocabulary);
          HYP              transHyp = sapiPhrasePtr->Hypothesis();
          delete sapiPhrasePtr;
          if (!VVISE_downloadForcingGrammar(vvise, transHyp, temporaryGrammarId, Vocabulary))
            FATAL_BRA_ "Can't download forcing grammar (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
          HYP_destroy(transHyp);
          if (!live) {
             // Shorten mindwell for Long Silence for nonlive forced recognition to allow the last word to last longer
            VOCAB_setModelDwells(Silences, LONG_SILENCE, (UChar) 0, (UChar) (LSMinDwell / 2), (UChar) (LSMinDwell + 10));
            VVISE_downloadModel(vvise, Silences, LONG_SILENCE, LONG_SILENCE);
             // Restore
            VOCAB_setModelDwells(Silences, LONG_SILENCE, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10));
          }
          nBest = 1;
         // Otherwise, if the enrollment grammar is called for, download it
        } else if (!strcmp(resultsObjPtr->RuleName(), ENROLLMENTGRAMMARNAME)) {
          GrammarId = temporaryGrammarId = KB_unusedGrammarId(Kb);
          if (!VVISE_downloadWildcard(vvise, ENROLLINITWC_WID, ENROLLINITWC_WID, 1, ENROLLINITWCMAXDW_DEFAULT, EnrollInitWCRel, EnrollInitWCAbs) ||
              !VVISE_downloadWildcard(vvise, ENROLLBODYWC_WID, ENROLLBODYWC_WID, EnrollBodyWCMinDwell, EnrollBodyWCMaxDwell, EnrollBodyWCRel, EnrollBodyWCAbs))
            FATAL_BRA_ "Can't download enrollment wildcards (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
          if (!VVISE_downloadEnrollmentGrammar(vvise, temporaryGrammarId, (UInt) EnrollInitWCCount))
            FATAL_BRA_ "Can't download enrollment grammar (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
          nBest = 1;
         // Otherwise, use the current grammar
        } else {
          temporaryGrammarId = 0;
          nBest = resultsObjPtr->NBest();
          if (nBest < 1) nBest = 1;
        }

        if (calibrateViaActivate) {
          VBX_DEBUG(VBX_print("  EngineThread::Calibration via SAPI_ACTIVATE\n"));
          VBX_fatalIf(!live || !AudioIn);
           // CALIBRATE
          doCalibration(startSN, endSN);
          hyp = NULL;
        } else {
           // RECOGNIZE
          VBX_DEBUG(VBX_print("  EngineThread: recognition started with LSMinDwell = %d, RelWC = %d, AbsWC = %d\n", LSMinDwell, RelativeWC, AbsoluteWC));
          hyp = VVISE_recognize(vvise, GrammarId, startSN, endSN, MaxFrameCount, live, nBest, PrintStats);
          rstatus = VVISE_status(vvise);
          VBX_DEBUG(VBX_print("  EngineThread: VVISE_recognize completed, rstatus=%d\n", rstatus));

           // If forced or enrolling, delete the grammar
          if (temporaryGrammarId && !VVISE_deleteGrammar(vvise, temporaryGrammarId))
            FATAL_BRA_ "Can't delete forcing or wildcard grammar (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;

           // If aborted, clean up and quit
          if (rstatus == ABORTED) {
            HYP_destroy(hyp);
             // Get rid of the results request, thereby releasing the results object
            delete resultsReqPtr;
            break;
          }

           // Report abnormal return here
          if (rstatus != VISESUCCESS && rstatus != RECOGTIMEOUT)
            VBX_print("  EngineThread: ERROR (code=%d) in VVISE_recognize - %s\n", rstatus, VISEERR_string(VVISE_status(vvise)));

           // Train adaptive silences, if possible
          VBX_DEBUG(VBX_print("  EngineThread: Training adaptive silences\n"));
          if (SilAdaptWeight && rstatus == VISESUCCESS && live && AudioIn) {
            WRDHYP  word;
            Int     offset = SilAdaptOffset;
            if (((word = HYP_findWord(hyp, LONG_ADAPTIVE_SIL)) || (word = HYP_findWord(hyp, LONG_SILENCE))) && offset < WRDHYP_numFrames(word)) {
              SN  snBegin = WRDHYP_startSN(word) + snStep * offset;
              UInt  weight = ((VISE_SILADAPTWGT_MAXIMUM - SilAdaptWeight) * ADAPTATIONWEIGHTNORM) / VISE_SILADAPTWGT_MAXIMUM;
              if (VVISE_trainWord(vvise, LONG_ADAPTIVE_SIL, Silences, snBegin, WRDHYP_endSN(word), weight, 0, VBX_VERBOSE) &&
                  VVISE_uploadModel(vvise, Silences, LONG_ADAPTIVE_SIL, FALSE) &&
                  VOCAB_copyModel(Silences, LONG_ADAPTIVE_SIL, SHORT_ADAPTIVE_SIL, FALSE) &&
                  VVISE_downloadModel(vvise, Silences, SHORT_ADAPTIVE_SIL, SHORT_ADAPTIVE_SIL))
                VBX_DEBUG(VBX_print("  EngineThread::SAPI_RECOGNIZE trained adaptive silences\n"));
              else
                VBX_print("  EngineThread::SAPI_RECOGNIZE can't train adaptive silences\n");
            }
          }
          VBX_DEBUG(VBX_print("  EngineThread: Recognize block DONE\n"));
        }
       // Otherwise, if it is a REQUEST FOR TRAINING, DO TRAINING
      } else if (service == SAPI_TRAIN) {
        VBX_DEBUG(VBX_print("  EngineThread::SAPI_TRAIN\n"));
        VBX_fatalIf(live || !AudioIn);
        hyp = NULL;

         // TRAIN
        VVISE_trainVocabulary(vvise, resultsObjPtr->Hypotheses(), Vocabulary, PhraseTrainWeight);
        VVISE_trainVocabulary(vvise, resultsObjPtr->Hypotheses(), Silences, VISE_TRAINWEIGHT_MAXIMUM);
         // Save results only for pure incremental training
        if (!BatchTrain) {
          VVISE_uploadHypModels(vvise, Vocabulary, resultsObjPtr->Hypotheses(), FALSE);
          VVISE_uploadHypModels(vvise, Silences, resultsObjPtr->Hypotheses(), FALSE);
           // Copy the long silence into the first multisilence to improve framing
          VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, FIRST_MULTISILENCE, FALSE), \
                        VBX_print("  EngineThread: training cannot copy long silence to first multi-silence\n"));
          KbObjPtr->SaveModels(VocabId);
          KbObjPtr->SaveModels((UInt) UPHEADSILCLASSID);
        }
        VBX_DEBUG(VBX_print("  EngineThread::SAPI_TRAIN completed\n"));

       // Otherwise, it must be a REQUEST FOR CALIBRATION, so do that
      } else {
        VBX_DEBUG(VBX_print("  EngineThread::SAPI_CALIBRATE\n"));
        VBX_fatalIf(!live || !AudioIn);
         // CALIBRATE
        doCalibration(startSN, endSN);
        hyp = NULL;
      }

       // Pause if auto-paused
      if (resultsReqPtr->AutoPause()) {
        VBX_IF_NOT_DO(Abort(), VBX_print("  EngineThread: AbortEngine fails\n"));
        ControlObjPtr->Pause();
      }

      if (hyp || calibrateViaActivate) {
         // Send notification (SHOULD SEND PhraseHypothesis IF WE GET SUBOPTIMAL TRACEBACK !! FIX !!)
        VBX_DEBUG(VBX_print("  EngineThread: hyp=0x%x\n", hyp));
        PVISESAPIPhrase sapiPhrasePtr = NULL;
        if (hyp) {
          if (PrintStats) HYP_print(hyp, Vocabulary, Silences);
          HYP_boundaries(hyp, &hypStartSN, &endSN);
          VBX_DEBUG(VBX_print("  EngineThread: Hypothesis: startSN = " QuadFmt(-) ", endSN = " QuadFmt(-) "\n", hypStartSN, endSN));
          sapiPhrasePtr = new VISESAPIPhrase(hyp, Vocabulary);
          VBX_DEBUG(sapiPhrasePtr->Print());
          VBX_DEBUG(Char * sapiText = sapiPhrasePtr->Text());
          VBX_DEBUG(VBX_print("  EngineThread: Result from SAPIPhrase: \"%s\"\n", sapiText));
          VBX_DEBUG(VBX_free(sapiText));
        }
         // Pass hyp and the jin data to the results object, which takes responsibility for annihilating them
        resultsObjPtr->SetHypotheses(hyp);

        if (hyp) {
           // Put in jin data only if it was live and substantial
          if (live && AudioIn) {
             // Step back 10 frames, or as many as possible. Empty hyp returns hypStartSN == 0
            if (hypStartSN == 0 || (hypStartSN -= snStep * 10) > startSN) startSN = hypStartSN;

            if ((acousticFeatures = (Ptr) VVISE_uploadJin(vvise, &startSN, &endSN, &numFrames))) {
              VBX_DEBUG(VBX_print("  EngineThread: ResultsObj @%p got AF @%p with startSN = " QuadFmt(-) ", endSN = " QuadFmt(-) "\n",
                                resultsObjPtr, acousticFeatures, startSN, endSN));
              resultsObjPtr->SetAcousticFeatures(acousticFeatures, startSN, endSN, numFrames);
            } else {
              VBX_DEBUG(VBX_print("  EngineThread: can't upload the jin data (status = %s)\n", VISEERR_string(VVISE_status(vvise))));
            }
          } else {
            startSN = hypStartSN;
          }
        }
         // If AudioQ has been configured and we are live, populate the results object with the audio data for this utterance
        if (live && AudioIn && AudioQLen > 0) {
          SN      sampStartSN = 0, sampEndSN = 0, sourceSN = 0;
          Int     sampCnt = 0;
          UShort *sampData = NULL;
          Int     astatus;

           // Stop the source for the duration of this operation
          if (SourceRefCnt && (hresult = SourceStop(&sourceSN))) VBX_print("  EngineThread: WARNING - SourceStop FAILS (hresult = %x)\n", hresult);

          if (SourceRefCnt == 0) {
            VBX_DEBUG(VBX_print("  EngineThread: Extracting sample data...\n"));
            if (!VFE_getAudioData(VFeat, &sampStartSN, &sampEndSN, &sampData, &sampCnt, &astatus) || sampData == NULL) {
              VBX_DEBUG(VBX_print("  EngineThread: can't upload the sample data from VFE (astatus = %d)\n", astatus));
            } else {
              VBX_DEBUG(VBX_print("  EngineThread: uploaded sample data @%p with startSN = " QuadFmt(-) ", endSN = " QuadFmt(-) ", and sampCnt = %d\n",
                                  sampData, sampStartSN, sampEndSN, sampCnt));
              resultsObjPtr->SetAudioData((OBJT)sampData, sampStartSN, sampEndSN, sampCnt);

               // Record the sample rate, if possible
              if (IAudioPtr != NULL) {
                SDATA sdata = { NULL, 0 };
                if (hresult = IAudioPtr->WaveFormatGet(&sdata)) {
                  VBX_print("  EngineThread: ERROR - IAudio::WaveFormatGet FAILS hresult = %x\n", hresult);
                } else {
                  if (sdata.pData != NULL && sdata.dwSize > 0) {
                    PWAVEFORMATEX waveFormat = (PWAVEFORMATEX) sdata.pData;                   
                    resultsObjPtr->AudioSampRate(waveFormat->nSamplesPerSec);
                    VBX_free(sdata.pData);
                  } else {
                    VBX_print("  EngineThread: ERROR - IAudio::WaveFormatGet produced NULL/zero result\n");
                  }
                }
              }
            }
          } else {
            VBX_print("  EngineThread: ERROR - SourceRefCnt unexpectedly nonzero (%d), data collection FAILS!\n", SourceRefCnt);
          }
        }
 
        VBX_DEBUG(VBX_print("  EngineThread: Calling PhraseFinish\n"));
        if (hyp) {
          resultsObjPtr->NotifySink()->PhraseFinish(rstatus != VISESUCCESS ? ISRNOTEFIN_THISGRAMMAR : ISRNOTEFIN_RECOGNIZED | ISRNOTEFIN_THISGRAMMAR,
                                                  startSN, endSN, sapiPhrasePtr->SRPhrase(), (LPUNKNOWN) resultsObjPtr);
          delete sapiPhrasePtr;
        } else if (calibrateViaActivate) {
          resultsObjPtr->NotifySink()->PhraseFinish(ISRNOTEFIN_THISGRAMMAR, startSN, endSN, NULL, (LPUNKNOWN) resultsObjPtr);
        }
      }

       // Stop source and calibration, if calibration of any kind is running
      if (service == SAPI_CALIBRATE || calibrateViaActivate) {
        SN  currentSN;
        Int status;
        SourceStop(&currentSN);
        VFE_stopCalibration(VFeat, &status);
      }

       // Send notification if auto-paused (the specification asserts we must do this AFTER sending PhraseFinish)
      if (resultsReqPtr->AutoPause()) {
        resultsObjPtr->NotifySink()->Paused();
        VBX_DEBUG(VBX_print("  EngineThread: called NotifySink()->Paused() after %s\n", SAPIVISECmd_string(service)));
       // Otherwise, notify the Control object of completion
      } else if (live) {
        if (service == SAPI_CALIBRATE) {
          ControlObjPtr->Calibrated(SRERR_NONE);
        } else {
          ControlObjPtr->Recognized(SRERR_NONE);
        }
      }

       // Get rid of the results request, thereby releasing the results object
      VBX_DEBUG(VBX_print("EngineThread: deleting VISEResultRequest pointer %p\n", resultsReqPtr));
      delete resultsReqPtr;

      VBX_DEBUG(VBX_print("  EngineThread: exiting %s\n", SAPIVISECmd_string(service)));

      break;
    }

    case SAPI_ENROLLWORD: {

      VBX_fatalIf(!AudioIn);
       // Baseline implementation SUPPORTS "multiple word models" now !!
      VBX_DEBUG(VBX_print("  EngineThread: received SAPI_ENROLLWORD\n"));
      VISETrainingObj * trainingObjPtr = (VISETrainingObj *) serviceReqPtr->Argument();
      VBX_fatalIf(!trainingObjPtr);

       // Bring VISE up to date
      if ((resultsObjPtr = trainingObjPtr->FirstResultsObject()) && !EngineKbUpdate(resultsObjPtr)) {
         // Do the things you need to do if the vocab is NOT downloaded
        VOCAB_setModelDwells(Silences, LONG_SILENCE, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10));
        VOCAB_setModelDwells(Silences, LONG_ADAPTIVE_SIL, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10));
        if (!VVISE_downloadModel(vvise, Silences, LONG_SILENCE, LONG_SILENCE) ||
            !VVISE_downloadModel(vvise, Silences, LONG_ADAPTIVE_SIL, LONG_ADAPTIVE_SIL))
          FATAL_BRA_ "Can't adjust the long silences (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        if (!VVISE_setThresholds(vvise, (UInt) BeamWidth, (UInt) BeamWidth, (UInt) BeamWidth, (UInt) BOUBeamWidth, (UInt) Muthah))
          FATAL_BRA_ "Can't set VISE thresholds (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
      } else if (resultsObjPtr && BatchTrain) {
        SEVERE_BRA_ "User Vocabulary reloaded for Enrollment amid Batch Training !!!\n" _KET;
      }

       // Estimate the size of JIN queue required to hold all the results
      Int  totalFrames = 0;
      for (resultsObjPtr = trainingObjPtr->FirstResultsObject();  resultsObjPtr != NULL; resultsObjPtr = trainingObjPtr->NextResultsObject())
        if (resultsObjPtr->AcousticFeatures(&startSN, &endSN, &numFrames))
          totalFrames += (Int) numFrames;
       // Enlarge the JIN queue if necessary
      if (totalFrames > FrameQLength) {
        FrameQLength = totalFrames;
        if (!VVISE_setParameter(vvise, NULL, FRAMEQLENGTH, FrameQLength))
          FATAL_BRA_ "Can't set VISE parameter (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        if (!VVISE_sourceInit(vvise))
          FATAL_BRA_ "Can't initialize VISE source (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        if (PrintStats) VBX_print("  IVbxTrain::EnrollWord enlarged the frame queue to %d frames\n", FrameQLength);
      }

       // VISE should now be ready for enrollment

       // STEP 1: Perform a VVISE_listen on each utterance in the training object to determine the extent of the speech in the utterance

      Int amplitude;
      Int uttCnt;
      ENRUTT enrUtt = (ENRUTT) VBX_calloc(trainingObjPtr->UttCnt(), sizeof(enrutt_t));
      Int idx;
      for (idx = 0, resultsObjPtr = trainingObjPtr->FirstResultsObject();  resultsObjPtr != NULL;) {
        acousticFeatures = resultsObjPtr->AcousticFeatures(&enrUtt[idx].startSN, &enrUtt[idx].endSN, &enrUtt[idx].frameCnt);
        VBX_DEBUG(VBX_print("SAPI_ENROLLWORD: idx %d; Frames: @%p; startSN = " QuadFmt(-) ", endSN = " QuadFmt(-) ", frameCnt = %d\n",
                            idx, acousticFeatures, enrUtt[idx].startSN, enrUtt[idx].endSN, enrUtt[idx].frameCnt));

         // Hack: "The Box" returns several garbage frames at the start of each utterance; here, we skip these until we determine where they come from.
        if (enrUtt[idx].startSN + 10 * 282 < enrUtt[idx].endSN) {
          enrUtt[idx].startSN += 10 * 282;
          VBX_DEBUG(VBX_print("SAPI_ENROLLWORD:  invoked enrollment startSN hack, startSN = " QuadFmt(-) " for utterance idx\n", enrUtt[idx].startSN, idx));
        }

        if (!VVISE_downloadJin(vvise, (JIN2FR) acousticFeatures, &enrUtt[idx].startSN, &enrUtt[idx].endSN, &enrUtt[idx].frameCnt)) {
          FATAL_BRA_ "Can't download jin data (status = %s)", VISEERR_string(VVISE_status(vvise)) _KET;
        } else if (!VVISE_listen(vvise, &enrUtt[idx], enrUtt[idx].frameCnt, FALSE, TRUE, &amplitude)) {
          VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_listen failed (status = %s)\n", VISEERR_string(VVISE_status(vvise)));
          resultsObjPtr = trainingObjPtr->RemoveResultsObject();
          continue;
        } else if (enrUtt[idx].wordStartSN == enrUtt[idx].startSN) {
          VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_listen could not find start of word within listening period\n");
          resultsObjPtr = trainingObjPtr->RemoveResultsObject();
          continue;
        } else if (enrUtt[idx].wordEndSN == enrUtt[idx].endSN) {
          VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_listen could not find end of word within listening period\n");
          resultsObjPtr = trainingObjPtr->RemoveResultsObject();
          continue;
        } else if (enrUtt[idx].wordFrameCnt < EnrollMinKernel) {
          VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_listen found less than %d frames\n", EnrollMinKernel);
          resultsObjPtr = trainingObjPtr->RemoveResultsObject();
          continue;
        } else if (enrUtt[idx].wordFrameCnt > MAXDWELL) {
          VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_listen found more than %d frames\n", MAXDWELL);
          resultsObjPtr = trainingObjPtr->RemoveResultsObject();
          continue;
        } else {
          VBX_DEBUG(VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_listen succeeded (startSN = " QuadFmt(-) ", endSN = " QuadFmt(-) " amplitude = %d)\n", 
                              enrUtt[idx].wordStartSN, enrUtt[idx].wordEndSN, amplitude));
          resultsObjPtr = trainingObjPtr->NextResultsObject();
          idx++;
        }
      }
      uttCnt = idx;

       // Determine the number of variants and models and fill in the array of wordIds to enroll
      VARIANT variants = (VARIANT) VBX_calloc(ENROLLMAXVARIANTS, sizeof(variant_t));
      UInt numVariants = 0;
      PSRWORD srWordPtr = trainingObjPtr->EnrollWords()->FirstSRWord();
      VBX_fatalIf(!srWordPtr);

       // If the wordId in the PSRWORD is -1, look up the correct wordIds by name in the knowledge base
      if (srWordPtr->dwWordNum == -1) {
        Char * baseWordName = srWordPtr->szWord;
        Int  baseWordLength = strlen(baseWordName);
        UInt wid;
        VOCITER  vociter = VOCITER_create(Vocabulary);
        while ((wid = VOCITER_nextWordId(vociter))) {
          Char * wordName = VOCAB_wordNameFromId(Vocabulary, wid);
          if (!strncmp(wordName, baseWordName, baseWordLength) && (baseWordLength == strlen(wordName) || wordName[baseWordLength] == VARIANTDELIMITER)) {
             // Find the variant in the array of variants already found, or, if no such variant is found, create a new one
            Int variantIndex;
            for (variantIndex = 0; variantIndex < numVariants; variantIndex++) if (!strcmp(wordName, variants[variantIndex].name)) break;
            if (variantIndex == numVariants) {
              numVariants++;
              variants[variantIndex].name = wordName;
              variants[variantIndex].numModels = 1;
              variants[variantIndex].wids = (UInt *) VBX_calloc(ENROLLMAXMODELS, sizeof(UInt));
              variants[variantIndex].wids[0] = wid;
            } else {
              variants[variantIndex].wids[variants[variantIndex].numModels++] = wid;
            }
          }
        }
         // If no matching wordId found, punt RIGHT NOW!
        if (!numVariants) {
          hresult = SRERR_INVALIDPARAM;
          delete serviceReqPtr;
          VBX_free(enrUtt);
          VBX_free(variants);
          VBX_print("  EngineThread::SAPI_ENROLLWORD found no variants for base word \"%s\"\n", baseWordName);
          MBOX_insert(returnAddress, (OBJT) hresult);
          break;
        } else {
          VBX_print("  EngineThread::SAPI_ENROLLWORD found %d variants for base word \"%s\"\n", numVariants, baseWordName); // DEBUG
        }
       // Otherwise, use the wordIds supplied
      } else {
        UInt numModels = trainingObjPtr->EnrollWords()->NumWords();
        for (Int modelIndex = 0; modelIndex < numModels; modelIndex++) {
          VBX_fatalIf(!srWordPtr);
          Char * wordName = srWordPtr->szWord;
          UInt wid = (UInt)srWordPtr->dwWordNum;
           // Find the variant in the array of variants already found, or, if no such variant is found, create a new one
          Int variantIndex;
          for (variantIndex = 0; variantIndex < numVariants; variantIndex++) if (!strcmp(wordName, variants[variantIndex].name)) break;
          if (variantIndex == numVariants) {
            numVariants++;
            variants[variantIndex].name = wordName;
            variants[variantIndex].numModels = 1;
            variants[variantIndex].wids = (UInt *) VBX_calloc(ENROLLMAXMODELS, sizeof(UInt));
            variants[variantIndex].wids[0] = wid;
          } else {
            variants[variantIndex].wids[variants[variantIndex].numModels++] = wid;
          }
          srWordPtr = trainingObjPtr->EnrollWords()->NextSRWord();
        }
      }

       // Find the variant with the greatest number of models
      Int maxIndex = 0;
      for (Int index = 0; index < numVariants; index++) if (variants[index].numModels > variants[maxIndex].numModels) maxIndex = index;
      Int numModels = variants[maxIndex].numModels;
      UInt * wids = variants[maxIndex].wids;
      VBX_print("  EngineThread::SAPI_ENROLLWORD: variant %d (\"%s\") has the most models (%d)\n", maxIndex, variants[maxIndex].name, numModels); // DEBUG

      Int minNumUtts = numModels * trainingObjPtr->UttsPerModel();
      if (numModels > minNumUtts) minNumUtts = numModels;

       // Quit if there are not enough utterances
      if (uttCnt < minNumUtts) {
        hresult = SRERR_NOTENOUGHDATA;
        VBX_print("  EngineThread::SAPI_ENROLLWORD completed STEP 1 with only %d of the %d utterances required\n", uttCnt, minNumUtts);
      } else {
        VBX_DEBUG(VBX_print("  EngineThread::SAPI_ENROLLWORD completed STEP 1 with %d usable utterances\n", uttCnt));

         // STEP 2: Prune utterances that do not meet length tolerance criteria

        Int  i, j;

         // Sort the ENRUTT array in order of increasing word duration using a bubble sort
        Int       unsorted;
        enrutt_t  etemp;
        for (i = 0, unsorted = uttCnt; i < uttCnt - 1; i++, unsorted--) {
          for (j = 0; j < unsorted - 1; j++) {
            if (enrUtt[j].wordFrameCnt > enrUtt[j+1].wordFrameCnt) {
              etemp = enrUtt[j+1];
              enrUtt[j+1] = enrUtt[j];
              enrUtt[j] = etemp;
            }
          }
        }

        Int medianDuration = enrUtt[uttCnt/2].wordFrameCnt;
        Int delta = (Int)(medianDuration * EnrollTolerance);
        UInt usableUtts = 0;

        VBX_DEBUG(VBX_print("  Median word duration is %d frames. Sorted ENRUTT array follows:\n", medianDuration));
        for (i = 0; i < uttCnt; i++) {
          VBX_DEBUG(VBX_print("  " QuadFmt(-) " " QuadFmt(-) " " QuadFmt(-) " " QuadFmt(-) " wordFrameCnt %d frameCnt %d\n", 
                              enrUtt[i].startSN, enrUtt[i].wordStartSN, enrUtt[i].wordEndSN,
                              enrUtt[i].endSN, enrUtt[i].wordFrameCnt, enrUtt[i].frameCnt));
          if (enrUtt[i].wordFrameCnt < medianDuration - delta || enrUtt[i].wordFrameCnt > medianDuration + delta)
            enrUtt[i].wordFrameCnt = 0;
          else
            usableUtts++;
        }

        if (usableUtts < minNumUtts) {
          VBX_print("  EngineThread::SAPI_ENROLLWORD: not enough samples (%d) for enrollment, need at least %d\n", usableUtts, minNumUtts);
          delete serviceReqPtr;
          VBX_free(enrUtt);
          for (Int variantIndex = 0; variantIndex < numVariants; variantIndex++) VBX_free(variants[variantIndex].wids);
          VBX_free(variants);
          hresult = SRERR_NOTENOUGHDATA;       
          VBX_print("  EngineThread::SAPI_ENROLLWORD did not complete, hresult = %x\n", hresult);
          MBOX_insert(returnAddress, (OBJT) hresult);
          break;
        }

        for (i = j = 0; i < uttCnt; i++) {
          if (enrUtt[i].wordFrameCnt > 0) {
            if (i != j) enrUtt[j] = enrUtt[i];
            j++;
          }
        }

        VBX_DEBUG(VBX_print("  EngineThread::SAPI_ENROLLWORD completed STEP 2, %d utterances survived pruning\n", usableUtts));

         // STEP 3: 

         // Do the enrollment
        VBX_DEBUG(VBX_print("SAPI_ENROLLWORD: fpk %d maxk %d mink %d nmod %d nutts %d\n", 
                  trainingObjPtr->FramesPerKernel(), EnrollMaxKernel, EnrollMinKernel, numModels,
                  usableUtts));
        Bool rval = VVISE_enrollWord(vvise, Vocabulary, trainingObjPtr->FramesPerKernel(), EnrollMaxKernel, EnrollMinKernel, numModels, wids, usableUtts, enrUtt);
        
        if (!rval && VVISE_status(vvise) != VISESUCCESS) {
          if (PrintStats) VBX_print("  EngineThread::SAPI_ENROLLWORD VVISE_enrollWord failed; vstatus = %s\n", VISEERR_string(VVISE_status(vvise)));
          hresult = SRERR_VALUEOUTOFRANGE;
        } else {

           // Upload all models from VISE with their dwells
          for (Int modelIndex = 0; modelIndex < numModels; modelIndex++) {
            if (!VVISE_uploadModel(vvise, Vocabulary, wids[modelIndex], TRUE)) {
              FATAL_BRA_ "  EngineThread::SAPI_ENROLLWORD can't perform VVISE_uploadModel for wid %d vstatus = %s", wids[modelIndex],
                VISEERR_string(VVISE_status(vvise)) _KET;
            } else { // Reset training history in enrolled model
              FUPATE model;
              if (VOCAB_getModel(Vocabulary, wids[modelIndex], &model)) {
                model.lSum = 0L;
                model.lSquares = 0L;
                model.wCount = 0;
                model.wTrainingCount = 0;
                model.wTrainingSavedCount = 0;
                VOCAB_putModel(Vocabulary, wids[modelIndex], &model);
              }
            }
          }

           // Copy models into all variants as needed and download to VISE with their dwells
          for (Int variantIndex = 0; variantIndex < numVariants; variantIndex++) {
            if (variantIndex != maxIndex) {
              for (Int modelIndex = 0; modelIndex < variants[variantIndex].numModels; modelIndex++) {
                if (!VOCAB_copyModel(Vocabulary, wids[modelIndex], variants[variantIndex].wids[modelIndex], TRUE)) {
                  FATAL_BRA_ "  EngineThread::SAPI_ENROLLWORD can't perform VOCAB_copyModel from wid %d to wid %d",
                    wids[modelIndex], variants[variantIndex].wids[modelIndex] _KET;
                } else if (!VVISE_downloadModel(vvise, Vocabulary, variants[variantIndex].wids[modelIndex], variants[variantIndex].wids[modelIndex])) {
                  FATAL_BRA_ "  EngineThread::SAPI_ENROLLWORD can't perform VVISE_downloadModel for wid %d vstatus = %s",
                    variants[variantIndex].wids[modelIndex], VISEERR_string(VVISE_status(vvise)) _KET;
                } else {
                  VBX_print("SAPI_ENROLLWORD successfully downloaded the model for \"%s(%d)\" (wid = %d) to \"%s(%d)\" (wid = %d)\n",
                            variants[maxIndex].name, modelIndex, wids[modelIndex],
                            variants[variantIndex].name, modelIndex, variants[variantIndex].wids[modelIndex]); // DEBUG
                }
              }
            }
          }

           // Save the models
          if (!KbObjPtr->SaveModels(VocabId)) {
            delete serviceReqPtr;
            VBX_free(enrUtt);
            for (Int variantIndex = 0; variantIndex < numVariants; variantIndex++) VBX_free(variants[variantIndex].wids);
            VBX_free(variants);
            hresult = SRERR_DATAFILEERROR;
            SEVERE_BRA_ "  EngineThread::SAPI_ENROLLWORD - SaveModels FAILED!" _KET;
            MBOX_insert(returnAddress, (OBJT) hresult);
            break;
          }

           // Reload the grammars to pick up the new models
          UInt  numGrammars, numGrammarsDone;
          VOCAB_grammars(Vocabulary, &numGrammars);
          if (numGrammars) {
            if ((numGrammarsDone = VVISE_deleteGrammars(vvise, Vocabulary)) != numGrammars)
              FATAL_BRA_ "  EngineThread::SAPI_ENROLLWORD can delete only %d of the %d grammars", numGrammarsDone, numGrammars _KET;
            if ((numGrammarsDone = VVISE_downloadGrammars(vvise, Vocabulary, VBX_VERBOSE)) != numGrammars)
              FATAL_BRA_ "  EngineThread::SAPI_ENROLLWORD can download only %d of the %d grammars", numGrammarsDone, numGrammars _KET;
          }

           // Reload the Joker wildcard to pick up the new words
          if (!EngineLoadJoker())
            FATAL_BRA_ "  EngineThread::SAPI_ENROLLWORD cannot download the joker wildcard" _KET;

          hresult = SRERR_NONE;
        }
      }

      VBX_free(enrUtt);
      for (Int variantIndex = 0; variantIndex < numVariants; variantIndex++) VBX_free(variants[variantIndex].wids);
      VBX_free(variants);
      delete serviceReqPtr;

       // Now we are done.  Send a reply, as this service request expects one (synchronous op)
      MBOX_insert(returnAddress, (OBJT) hresult);
      VBX_DEBUG(VBX_print("  EngineThread::SAPI_ENROLLWORD done, hresult = %x\n", hresult));
      break;
    }

    case SAPI_STARTTRAININGPASS: {

      VBX_fatalIf(!AudioIn);

      resultsReqPtr = (PVISEResultsRequest) serviceReqPtr;

      VBX_DEBUG(VBX_print("  EngineThread: received SAPI_STARTTRAININGPASS\n"));

      if (!BatchTrain) {
         // This is the very beginning of batch training. Make sure Vocabulary if any is reloaded with initializeDwells == TRUE
        if (KbObjPtr) {
          KbObjPtr->Release();
          KbObjPtr = NULL;
        }
        firstPass = TRUE;
        BatchTrain = TRUE;
      }
       // Get the originating results object
      resultsObjPtr = resultsReqPtr->ResultsObject();

       // Download the required vocabulary if it isn't already there
      VBX_IF_NOT_DO(!EngineKbUpdate(resultsObjPtr), VBX_print("  EngineThread: SAPI_STARTTRAININGPASS downloaded new vocabulary\n"));

       // Initialize the training data
      (void) VVISE_reinitializeModels(vvise, Vocabulary, firstPass);
      (void) VVISE_reinitializeModels(vvise, Silences, FALSE);

       // Get rid of the results request, thereby releasing the results object
      delete resultsReqPtr;

       // Now we are done.  Send a reply, as this service request expects one (synchronous op)
      MBOX_insert(returnAddress, (OBJT) SRERR_NONE);
      VBX_DEBUG(VBX_print("  EngineThread: training pass started\n"));
      break;
    }

    case SAPI_ENDTRAININGPASS: {

      VBX_fatalIf(!AudioIn);

      resultsReqPtr = (PVISEResultsRequest) serviceReqPtr;

      finalPass = resultsReqPtr->BoolParam();
      wordNames = resultsReqPtr->WordNames();

      VBX_DEBUG(VBX_print("  EngineThread: received SAPI_ENDTRAININGPASS\n"));

       // Get the originating results object
      resultsObjPtr = resultsReqPtr->ResultsObject();

       // Download the required vocabulary if it isn't already there. Too bad if it happens
      VBX_IF_NOT_DO(!EngineKbUpdate(resultsObjPtr), VBX_print("  ERROR: EngineThread: SAPI_ENDTRAININGPASS downloaded new vocabulary !!!\n"));

       // Replace the old models with new ones based on the training data. Adjust dwells of real words only on final pass
      VBX_DEBUG(VBX_print("  EngineThread: adjusting selected models\n"));
      (void) VVISE_adjustSelectedModels(vvise, Vocabulary, wordNames, BatchTrainWeight, finalPass);
      VBX_DEBUG(VBX_print("  EngineThread: adjustedSelectedModels\n"));
      (void) VVISE_adjustModels(vvise, Silences, BatchTrainWeight, FALSE);
      VBX_DEBUG(VBX_print("  EngineThread: adjustedModels\n"));

      if (firstPass) {
         // Have the most trained Long Silence used on following passes
        firstPass = FALSE;
        VVISE_uploadVocab(vvise, Silences, FALSE);
        VOCENT_useBestModel(VOCAB_entryFromId(Silences, LONG_SILENCE));
      }
       // Get rid of the results request, thereby releasing the results object
      VBX_DEBUG(VBX_print("  EngineThread: deleting resultsReqPtr\n"));
      delete resultsReqPtr;
      VBX_DEBUG(VBX_print("  EngineThread: deleted resultsReqPtr\n"));
       // Now we are done.  Send a reply, as this service request expects one (synchronous op)
      MBOX_insert(returnAddress, (OBJT) SRERR_NONE);
      VBX_DEBUG(VBX_print("  EngineThread: training pass completed\n"));
      break;
    }

    case SAPI_SAVEMODELS: {

      VBX_fatalIf(!AudioIn);

      resultsReqPtr = (PVISEResultsRequest) serviceReqPtr;

      Int minTrainingCount = resultsReqPtr->IntParam();
      // finalPass = resultsReqPtr->BoolParam(); Ignore this here, presuming it's set in SAPI_ENDTRAININGPASS
      wordNames = resultsReqPtr->WordNames();

      VBX_DEBUG(VBX_print("  EngineThread: received SAPI_SAVEMODELS\n"));

       // Get the originating results object
      resultsObjPtr = resultsReqPtr->ResultsObject();

       // Download the required vocabulary if it isn't already there. Too bad if it happens
      VBX_IF_NOT_DO(!EngineKbUpdate(resultsObjPtr), VBX_print("  ERROR: EngineThread: SAPI_SAVEMODELS downloaded new vocabulary !!!\n"));

       // Upload the models
      VVISE_uploadSelectedModels(vvise, Vocabulary, wordNames, TRUE, minTrainingCount);
      VVISE_uploadVocab(vvise, Silences, FALSE);

       // Duplicate the Long Silence models having the most training
      VOCENT_useBestModel(VOCAB_entryFromId(Silences, LONG_SILENCE));

       // Copy the long silence into all others. (MS1 was modified due to forcing grammar. SS picks up some transitions ...
      VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, SHORT_SILENCE, FALSE),\
                    VBX_print("  EngineThread::SAPI_SAVEMODELS cannot copy long silence to short silence\n"));
      VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, LONG_ADAPTIVE_SIL, FALSE),\
                    VBX_print("  EngineThread::SAPI_SAVEMODELS cannot copy long silence to long adaptive silence\n"));
      VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, SHORT_ADAPTIVE_SIL, FALSE),\
                    VBX_print("  EngineThread::SAPI_SAVEMODELS cannot copy long silence to short adaptive silence\n"));
       // Copy the long silence into all multisilences
      UInt  wid;
      UInt  wordCount;
      for (wordCount = 1, wid = FIRST_MULTISILENCE; wordCount <= NUM_MULTISILENCES; wordCount++, wid++) {
        VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, wid, FALSE),\
                      VBX_print("  EngineThread::SAPI_SAVEMODELS cannot copy long silence to multisilence #%d\n", wordCount));
      }

       // Mark models training scheme as BATCH
      MODELS_setSchemeBatch(VOCAB_models(Vocabulary));

       // Save the models into the voice stream
      KbObjPtr->SaveModels(VocabId);
      KbObjPtr->SaveModels((UInt) UPHEADSILCLASSID);

      if (finalPass) {
         // Set in SAPI_ENDTRAININGPASS
         // For undertrained models we need to restore their expanded dwells constraints
         // THIS IMPLIES that SAPI_SAVEMODELS is the LAST operation of the Batch Training !!!
        VVISE_downloadVocab(vvise, Vocabulary, FALSE, VBX_VERBOSE);
        BatchTrain = FALSE;
        finalPass = FALSE;
      }

       // Get rid of the results request, thereby releasing the results object
      delete resultsReqPtr;

       // Now we are done.  Send a reply, as this service request expects one (synchronous op)
      MBOX_insert(returnAddress, (OBJT) SRERR_NONE);
      VBX_DEBUG(VBX_print("  EngineThread: models saved to voice stream and file\n"));
      break;
    }

    case SAPI_DEACTIVATE: {

      VBX_DEBUG(VBX_print("  EngineThread: received SAPI_DEACTIVATE\n"));
      delete serviceReqPtr;

      if (returnAddress) MBOX_insert(returnAddress, (OBJT) TRUE);

      break;
    }

    case SAPI_EXIT: {

      VBX_DEBUG(VBX_print("  EngineThread: exiting\n"));
      delete serviceReqPtr;

       // Release the KB object, if any
      if (KbObjPtr) KbObjPtr->Release();

      VBX_print("EngineThread has shut down\n");
      pthread_exit(NULL);

      break;
    }

    case SAPI_ABORT: {

      delete serviceReqPtr;

      Abort();
      VBX_DEBUG(VBX_print("  EngineThread: aborted\n"));      

      break;
    }

    default: {

      SEVERE_BRA_ "  EngineThread: unknown service (%d) requested\n", service _KET;
      delete serviceReqPtr;

      Abort();

      break;
    }
    }

     // Deallocate the array of word names, if any
     /* EH: let caller deal with it
    if (wordNames != NULL) {
      Int i = 0;
      for (i = 0; wordNames[i]; i++) free(wordNames[i]);
      VBX_free(wordNames);
    }
    */
  }
}

Bool
VVISEEngineObj::EngineKbUpdate(PVISEResultsObj resultsObjPtr)
{
  Bool        downloadVocab = FALSE;
  PVISEKbObj  newKbObjPtr;
  UInt        newVocabId;

   // Find out what KB to use and replace the old KB with the new if they differ
  newKbObjPtr = resultsObjPtr->Kb();
  if (KbObjPtr != newKbObjPtr) {
    VBX_DEBUG(VBX_print("  EngineObj::EngineKbUpdate: old KbObjPtr = %p; new KbObjPtr = %p\n", KbObjPtr, newKbObjPtr));
    if (KbObjPtr) KbObjPtr->Release();
    newKbObjPtr->AddRef();
    KbObjPtr = newKbObjPtr;
    downloadVocab = TRUE;
  }

   // Update the KB if its constituent voice stream has changed
  if (KbObjPtr->Update())
    downloadVocab = TRUE;
  Kb = KbObjPtr->Kb();

   // Find out what vocabulary to use
  if (resultsObjPtr->RuleName()) {
    SYNTAX  grammar;
     // If neither the wildcard grammar nor calibration, get the appropriate vocabulary; otherwise, just use the current one
    if (strcmp(resultsObjPtr->RuleName(), ENROLLMENTGRAMMARNAME) && strcmp(resultsObjPtr->RuleName(), CALIBRATIONGRAMMARNAME)) {
      grammar = KB_grammarByName(Kb, resultsObjPtr->RuleName());
      VBX_fatalIf(!grammar);
      GrammarId = SYNTAX_id(grammar);
      newVocabId = SYNTAX_vocabularyId(grammar);
    } else if (VocabId) {
      newVocabId = VocabId;
    } else {
      grammar = KB_grammar(Kb, KB_firstGrammarId(Kb));
      VBX_fatalIf(!grammar);
      GrammarId = SYNTAX_id(grammar);
      newVocabId = SYNTAX_vocabularyId(grammar);
    }
  } else {
    newVocabId = VocabId;
  }

   // Replace the old vocabulary with the new if they differ
  if (!VocabId || VocabId != newVocabId) {
    VBX_DEBUG(VBX_print("  EngineObj::EngineKbUpdate: GrammarId = %u; old VocabId = %u; new VocabId = %u\n", GrammarId, VocabId, newVocabId));
    VocabId = newVocabId;
    downloadVocab = TRUE;
  }

  Vocabulary = VocabId ? KB_vocabulary(Kb, VocabId) : NULL;
  Silences = KB_vocabulary(Kb, (UInt) UPHEADSILCLASSID);

   // Download the new vocabulary if required
  if (downloadVocab) {
    VBX_DEBUG(VBX_print("  EngineObj::EngineKbUpdate: Downloading vocabulary %u, containing rule \"%s\"\n", VocabId, resultsObjPtr->RuleName() ? resultsObjPtr->RuleName() : "(NULL)"));
    if (!EngineLoad())
      VBX_print("  EngineObj::EngineKbUpdate: ERROR - EngineLoad FAILED\n");
  }

  return(downloadVocab);
}

Bool
VVISEEngineObj::EngineLoad()
{
  UInt   numModels;
  Bool   result;

   // Induce VISE amnesia
  if (!VVISE_warmstart(VVise)) {
    VBX_print("  EngineObj::EngineLoad: Can't WARMSTART VISE (status = %s)\n", VISEERR_string(VVISE_status(VVise)));
    result = FALSE;

   // Set thresholds
  } else if (!VVISE_setThresholds(VVise, (UInt) BeamWidth, (UInt) BeamWidth, (UInt) BeamWidth, (UInt) BOUBeamWidth, (UInt) Muthah)) {
    VBX_print("  EngineObj::EngineLoad: Can't set VISE thresholds (status = %s)\n", VISEERR_string(VVISE_status(VVise)));
    result = FALSE;

   // Download the word models and templates for the specified vocabulary, if any
  } else if (Vocabulary && (numModels = VVISE_downloadVocab(VVise, Vocabulary, BatchTrain, VBX_VERBOSE)) != VOCAB_numModels(Vocabulary)) {
    VBX_print("  EngineObj::EngineLoad: Can only download %u of the %u models in the user's vocabulary (status = %s)\n",
                        numModels, VOCAB_numModels(Vocabulary), VISEERR_string(VVISE_status(VVise)));
    result = FALSE;

   // Set LongSilence dwells which may have changed
  } else if (!VOCAB_setModelDwells(Silences, LONG_SILENCE, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10)) ||
             !VOCAB_setModelDwells(Silences, LONG_ADAPTIVE_SIL, (UChar) 0, (UChar) LSMinDwell, (UChar) (LSMinDwell + 10))) {
    VBX_print("  EngineObj::EngineLoad: Can't set LongSilence dwells\n");

    result = FALSE;

   // Download the silence word models and templates
  } else if ((numModels = VVISE_downloadVocab(VVise, Silences, FALSE, VBX_VERBOSE)) != VOCAB_numModels(Silences)) {
    VBX_print("  EngineObj::EngineLoad: Can only download %u of the %u silence models (status = %s)\n",
                        numModels, VOCAB_numModels(Silences), VISEERR_string(VVISE_status(VVise)));
    result = FALSE;

   // Download the Joker wildcard
  } else if (!EngineLoadJoker()) {
    VBX_print("  EngineObj::EngineLoad: Can't load the joker\n");
    result = FALSE;
    
   // Download the grammars for the specified vocabulary, if any
  } else if (Vocabulary) {
    UInt   numGrammars;
    (void) VOCAB_grammars(Vocabulary, &numGrammars);
    if (!numGrammars) {
      VBX_print("  EngineObj::EngineLoad: There are no grammars for vocabulary %u in recstream\n", VocabId);
      result = FALSE;
    } else if (numGrammars != VVISE_downloadGrammars(VVise, Vocabulary, VBX_VERBOSE)) {
      VBX_print("  EngineObj::EngineLoad: Cannot download all %u of the grammars for vocabulary %u (status = %s)\n",
                          numGrammars, VocabId, VISEERR_string(VVISE_status(VVise)));
      result = FALSE;
    } else {
      VBX_DEBUG(VBX_print("%u grammars loaded for vocabulary %u from recStream\n", numGrammars, VocabId));
      result = TRUE;
    }

   // There are no grammars to load
  } else {
    result = TRUE;
  }

  return result;
}

Bool
VVISEEngineObj::EngineLoadJoker()
{
  UInt  wcSum = 0;
  UInt  wcSumSq = 0;
  UInt  wcCount = 0;

  if (Vocabulary) VOCAB_setTrainingDefault(Vocabulary, (UInt) DfltTrainScore, (UInt) DfltTrainCount);
  VOCAB_setTrainingDefault(Silences, (UInt) DfltTrainScore, (UInt) DfltTrainCount);

   // If we can't compute the accumulated score over the models for the specified vocabulary, get it from the models header
  if (Vocabulary && !VOCAB_getTraining(Vocabulary, &wcSum, &wcSumSq, &wcCount, TRUE)) {
    MODELS_getTraining(VOCAB_models(Vocabulary), &wcSum, &wcSumSq, &wcCount, TRUE);
    VBX_DEBUG(VBX_print("  EngineObj::EngineLoadJoker: (from Vocabulary header) WCSum = %d; WCSumSq = %d; WCCount = %d\n", wcSum, wcSumSq, (Int) wcCount));
  }

   // Compute the average per-frame score from training
  VBX_DEBUG(VBX_print("  EngineObj::EngineLoadJoker: WCSum = %d; WCSumSq = %d; WCCount = %d\n", wcSum, wcSumSq, (Int) wcCount));
  TrainingAvg = wcCount && wcSum ? wcSum / wcCount : (DfltTrainCount ? DfltTrainScore / DfltTrainCount : DfltTrainScore);

   // Lift it up, if it is BATCH trained, since AbsoluteWC was tuned for Incremental training, which renders larger mean
  if (Vocabulary && MODELS_trainScheme(VOCAB_models(Vocabulary)) > 0) {
    TrainingAvg += VISE_BATCHGROWTH_DEFAULT;
    RelativeWCTrainCorrection = VISE_BATCH_RELATIVE_CORR;
  } else {
    RelativeWCTrainCorrection = VISE_INCRM_RELATIVE_CORR;
  }
   // Override a ridiculous training average
  if (TrainingAvg > VISE_DFLTTRSCORE_MAXIMUM) {
    VBX_DEBUG(VBX_print("  EngineObj::EngineLoadJoker: overriding an absurd average training score (%d)\n", TrainingAvg));
    TrainingAvg = DfltTrainCount ? DfltTrainScore / DfltTrainCount : DfltTrainScore;
  }

   // In text mode we have exact match in "training"
  if (NULL == AudioIn)
    TrainingAvg = 0;

   // Download the wildcard
  UInt  absWCThresh = TrainingAvg + AbsoluteWC;
  UInt  relWCThresh = RelativeWCTrainCorrection + RelativeWC;
  VBX_DEBUG(VBX_print("  EngineObj::EngineLoadJoker: Downloading the joker model (rel = %d, abs = %u)\n", relWCThresh, absWCThresh));
  if (!VVISE_downloadWildcard(VVise, JOKER_WID, JOKER_WID, 1, 1, relWCThresh, absWCThresh)) {
    VBX_print("  EngineObj::EngineLoadJoker: Can't download the wildcard (status = %s)\n", VISEERR_string(VVISE_status(VVise)));
    return FALSE;
  } else {
    return TRUE;
  }
}

Bool
VVISEEngineObj::doCalibration(SN startSN, SN endSN)
{
  enrutt_t  utt;
  // Calibration Hack:  "The Box" is returning several garbage frames at the start of each utterance.
  //                    Here, we're going to skip these frames until we determine where they come from.

   // Get VVISE
  VVISE  vvise = VVise;
  utt.startSN = startSN + 10 * 282;
  utt.endSN = endSN;
  if (!VVISE_listen(vvise, &utt, CalibrationLength * NUM_MULTISILENCES + 1, TRUE, FALSE, NULL)) {
    VBX_print("  VVISEEngineObj::doCalibration: VVISE_listen failed (status = %s)\n", VISEERR_string(VVISE_status(vvise)));
    return FALSE;
  } else {
    VBX_DEBUG(VBX_print("  VVISEEngineObj::doCalibration: VVISE_listen succeeded (startSN = " QuadFmt(-) ", endSN = " QuadFmt(-) ")\n",
                        utt.wordStartSN, utt.wordEndSN));

    // Train the long silence
    VBX_IF_NOT_DO(VVISE_trainWord(vvise, LONG_SILENCE, Silences, utt.wordStartSN, utt.wordEndSN, CalibrationWeight, 0, PrintStats),\
                  VBX_print("  VVISEEngineObj::doCalibration: VVISE_trainWord failed on LONG_SILENCE (status = %s)\n", VISEERR_string(VVISE_status(vvise))));
    VVISE_uploadVocab(vvise, Silences, FALSE);

    // Copy the long silence into the short and adaptive silences
    VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, LONG_ADAPTIVE_SIL, FALSE),\
                  VBX_print("  VVISEEngineObj::doCalibration: cannot copy long silence to long adaptive silence\n"));
    VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, SHORT_SILENCE, FALSE),\
                  VBX_print("  VVISEEngineObj::doCalibration: cannot copy long silence to short silence\n"));
    VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, SHORT_ADAPTIVE_SIL, FALSE),\
                  VBX_print("  VVISEEngineObj::doCalibration: cannot copy long silence to short adaptive silence\n"));

    // Copy the long silence into all multisilences
    UInt  wid;
    UInt  wordCount;
    for (wordCount = 1, wid = FIRST_MULTISILENCE; wordCount <= NUM_MULTISILENCES; wordCount++, wid++) {
      VBX_IF_NOT_DO(VOCAB_copyModel(Silences, LONG_SILENCE, wid, FALSE),\
                    VBX_print("  VVISEEngineObj::doCalibration: cannot copy long silence to multisilence #%d\n", wordCount));
    }

    // Download the silence vocabulary to make VISE aware of the copies
    VBX_IF_NOT_DO(VVISE_downloadVocab(vvise, Silences, FALSE, VBX_VERBOSE),\
                        VBX_print("  VVISEEngineObj::doCalibration: cannot download the silences after calibration\n"));
  }
  return TRUE;
}
