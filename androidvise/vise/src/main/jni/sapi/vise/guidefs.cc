#ifndef _GUIDEFS_HH_
#define _GUIDEFS_HH_

#define INITGUID  1
#include "pthr.h"
#include "vbxcom.hh"
#include "vbxenum.hh"
#include "viseobj.hh"
#include "visegram.hh"
#include "visetrain.hh"
#include "audiofile.hh"
#include "speech.h"

#endif
