#include "pthr.h"
#include <stdio.h>
#include <string.h>

 // Verbex SAPI Objects
#include "visegram.hh"
#include "viseresults.hh"
#include "visephrase.hh"

 // VISE infrastructure
#include "ifaces.h"
#include "vfile.h"

 // VULCAN Inclusions
#include "kb.h"
#include "script.h"
#include "vocab.h"
#include "syntax.h"
#include "script.h"

 // Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "critsect.h"
#include "mbox.h"
#include "llist.h"

#ifdef DEBUG
#define GRAMDEBUG
#endif
#ifdef GRAMDEBUG
#define VBX_DEBUG(P)  P
#define VBX_VERBOSE   TRUE
#else
#define VBX_DEBUG(P)
#define VBX_VERBOSE   FALSE
#endif

VISEGrammarObj::VISEGrammarObj(ISRGramNotifySink *notifySinkPtr, PVISERecStreamObj recStream, IMemory * allocator, MBOX controlMBox):
RefCnt(0),
NotifySinkPtr(notifySinkPtr),
RecStreamPtr(recStream),
Allocator(allocator),
ResultsObjects(NULL),
CMControlMBox(controlMBox),
KbTransType(KB_DISPLAY_RESPONSE)
{
  UnknwnPtr = this;

  CRITSECT_CREATE(RefCntCritSect, Allocator, NULL);

   // Instantiate the contained interfaces
  VISEISRGramCommonPtr = new VISEISRGramCommon(this, UnknwnPtr);
  VISEISRGramCFGPtr = new VISEISRGramCFG(this, UnknwnPtr);
  VISEIVbxGrammarPtr = new VISEIVbxGrammar(this, UnknwnPtr);

   // Assure that the referenced objects don't abandon us
  if (NotifySinkPtr) NotifySinkPtr->AddRef();
  VBX_fatalIf(!RecStreamPtr);
  RecStreamPtr->AddRef();

  VBX_DEBUG(VBX_print("  VISEGrammarObj::VISEGrammarObj(): (@%p) created with notification sink @%p and rec stream @%p\n", this, NotifySinkPtr, RecStreamPtr));
}

VISEGrammarObj::~VISEGrammarObj()
{  
  VBX_DEBUG(VBX_print("  VISEGrammarObj::~VISEGrammarObj(): (@%p) freeing contained interfaces\n", this));

   // Delete the contained interfaces
  delete VISEISRGramCommonPtr;
  delete VISEISRGramCFGPtr;
  delete VISEIVbxGrammarPtr;

   // Release the referenced objects
  VBX_DEBUG(VBX_print("  VISEGrammarObj::~VISEGrammarObj(): (@%p) releasing notification sink @%p\n", this, NotifySinkPtr));
  if (NotifySinkPtr) NotifySinkPtr->Release();
  VBX_DEBUG(VBX_print("  VISEGrammarObj::~VISEGrammarObj(): (@%p) releasing recognition stream @%p\n", this, RecStreamPtr));
  if (RecStreamPtr) RecStreamPtr->Release();

  VBX_DEBUG(VBX_print("  VISEGrammarObj::~VISEGrammarObj(): (@%p) releasing critsect @%p\n", this, RefCntCritSect));
  CRITSECT_DESTROY(RefCntCritSect);
}

STDMETHODIMP
VISEGrammarObj::QueryInterface(REFIID riid, LPVOID *ppv)
{
  *ppv = NULL;

   // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown))
    *ppv = (LPVOID) this;
  else if (IsEqualIID(riid, IID_ISRGramCommon))
    *ppv = VISEISRGramCommonPtr;
  else if (IsEqualIID(riid, IID_ISRGramCFG))
    *ppv = VISEISRGramCFGPtr;
  else if (IsEqualIID(riid, IID_IVbxGrammar))
    *ppv = VISEIVbxGrammarPtr;

   // Update the reference count here
  if (*ppv != NULL) {
    ((LPUNKNOWN) *ppv)->AddRef();
    return SRERR_NONE;
  }
  return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG)
VISEGrammarObj::AddRef()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEGrammarObj::AddRef(): RefCnt for VISEGrammarObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISEGrammarObj::Release()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEGrammarObj::Release(): RefCnt for VISEGrammarObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) {
    delete this;
    VBX_DEBUG(VBX_print("  VISEGrammarObj::Release(): VISEGrammarObj successfully destroyed\n"));
  }
  return refCnt;
}

void
VISEGrammarObj::EnqueueResults(PVISEResultsObj viseResultsObjPtr)
{
  (void) AddRef();
   // Enqueue the results object at the beginning of the grammar's list of results objects
  ResultsObjects = LLIST_prepend(ResultsObjects, (Ptr) viseResultsObjPtr);
  VBX_DEBUG(VBX_print("  VISEGrammarObj::EnqueueResults: (@%p) enqueued results object @%p (%u objects now queued)\n",
                      this, viseResultsObjPtr, LLIST_length(ResultsObjects)));
}

void
VISEGrammarObj::DequeueResults(PVISEResultsObj viseResultsObjPtr)
{
  LLIST  llist = LLIST_find(ResultsObjects, (Ptr) viseResultsObjPtr);
  if (ResultsObjects == llist) ResultsObjects = LLIST_next(llist);
  (void) LLIST_remove(llist);
  LLIST_destroy(llist, NULL, NULL);
  VBX_DEBUG(VBX_print("  VISEGrammarObj::DequeueResults: (@%p) dequeued results object @%p (%u objects now queued)\n",
                      this, viseResultsObjPtr, LLIST_length(ResultsObjects)));
  (void) Release();
}

ISRGramNotifySink *
VISEGrammarObj::NotifySink(void)
{
  return NotifySinkPtr;
}

PVISERecStreamObj
VISEGrammarObj::RecStream(void)
{
  return RecStreamPtr;
}

MBOX
VISEGrammarObj::ControlMBox(void)
{
  return CMControlMBox;
}


  // Methods for VISEISRGramCommon

VISEISRGramCommon::VISEISRGramCommon(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
  IMemory * allocator = ((PVISEGrammarObj) ObjPtr)->Allocator;
  VBX_fatalIf(!(RAAttributes = MBOX_createAttr(allocator, 1, 1 ,1)));
  VBX_fatalIf(!(ReturnAddress = MBOX_create(allocator, RAAttributes)));
}

VISEISRGramCommon::~VISEISRGramCommon()
{
  MBOX_destroy(ReturnAddress);
  VBX_free(RAAttributes);
}

STDMETHODIMP
VISEISRGramCommon::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRGramCommon::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRGramCommon::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRGramCommon::Activate(HWND hwnd, BOOL autoPause, PCSTR rule)
{
  PVISEGrammarObj objPtr = (PVISEGrammarObj) ObjPtr;

  if (hwnd) return SRERR_NOTSUPPORTED;

  PVISEGrammarRequest gramReqPtr = new VISEGrammarRequest(SAPI_ACTIVATE, objPtr, (Char *) rule, autoPause, ReturnAddress);
  if (!MBOX_insert(objPtr->CMControlMBox, (OBJT) gramReqPtr))
    return E_UNEXPECTED;
  VBX_DEBUG(VBX_print("  GramCommon: sent SAPI_ACTIVATE to Engine for rule \"%s\"\n", rule));

  HRESULT hresult = (HRESULT) MBOX_remove(ReturnAddress);
  VBX_DEBUG(VBX_print("  GramCommon: received confirmation of SAPI_ACTIVATE from Engine for rule \"%s\"\n", rule));
  return hresult;
}

STDMETHODIMP
VISEISRGramCommon::Archive(BOOL archiveResults, PVOID buf, DWORD bufSize, DWORD *bufMinLen)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCommon::BookMark(QWORD time, DWORD bookMarkID)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCommon::Deactivate(PCSTR rule)
{
  PVISEGrammarObj objPtr = (PVISEGrammarObj) ObjPtr;
  PVISEGrammarRequest gramReqPtr = new VISEGrammarRequest(SAPI_DEACTIVATE, objPtr, (Char *) rule, FALSE, ReturnAddress);
  if (!MBOX_insert(objPtr->CMControlMBox, (OBJT) gramReqPtr))
    return E_UNEXPECTED;
  VBX_DEBUG(VBX_print("  GramCommon: sent SAPI_DEACTIVATE to Engine for rule \"%s\"\n", rule));

  HRESULT hresult = (HRESULT) MBOX_remove(ReturnAddress);
  VBX_DEBUG(VBX_print("  GramCommon: received confirmation of SAPI_DEACTIVATE from Engine for rule \"%s\", hres = %x\n", rule, hresult));
  return hresult;
}

STDMETHODIMP
VISEISRGramCommon::DeteriorationGet(DWORD *memThresh, DWORD *timeThresh, DWORD *objCntThresh)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCommon::DeteriorationSet(DWORD memThresh, DWORD timeThresh, DWORD objCntThresh)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCommon::TrainDlg(HWND hwnd, PCSTR title)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCommon::TrainPhrase(DWORD extent, PSDATA prompts)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;
  
  if (gramObjPtr == NULL)
    return E_NOINTERFACE;

  if (extent == SRTQEX_REQUIRED) {

    PVISEReply      replyPtr;
    PSDATA          wordListPtr;

    PVISEServiceRequest serviceReqPtr = new VISEServiceRequest(SAPI_LISTUNMODELLEDWORDS, (Ptr) gramObjPtr, ReturnAddress);
    if (!MBOX_insert(gramObjPtr->CMControlMBox, (OBJT) serviceReqPtr))
      return E_UNEXPECTED;
    VBX_DEBUG(VBX_print("  GramCommon::TrainPhrase sent SAPI_LISTUNMODELLEDWORDS to ControlThread\n"));

    if ((replyPtr = (PVISEReply) MBOX_remove(ReturnAddress)) == NULL || replyPtr->Service() != SAPI_LISTUNMODELLEDWORDS) {
      return E_UNEXPECTED;
    } else if ((wordListPtr = (PSDATA) replyPtr->Results())) {
      VBX_DEBUG(VBX_print("  GramCommon::TrainPhrase received list of unmodelled words from ControlThread\n"));
      if (prompts) {
        prompts->pData = wordListPtr->pData;
        prompts->dwSize = wordListPtr->dwSize;
      }
      free(wordListPtr);
      return SRERR_NONE;
    }
    HRESULT  hresult = replyPtr->HResult();
    delete replyPtr;
    return hresult;

  } else if (extent == SRTQEX_RECOMMENDED) {

    SCRIPT          script;
    UInt            vocabId, grammarId;
    HYP             entry;
    KB              kbPtr;
    Ptr             promptBlk;
    Ptr             entryPtr;
    Int             bytes, totalBytes;
    Int             i, entryCnt, promptBlkSize = 0;

    const Int promptBlkGrowthQntm = 2048;

     // Load up our very own KB using the rec stream
    kbPtr = KB_create(NULL);
    VFILE recFile = gramObjPtr->RecStream()->Stream();
    VBX_fatalIf(!recFile);
    if (!KB_load(kbPtr, recFile, FALSE) ||
        !KB_loadResponseData(kbPtr, recFile, FALSE)) {
      KB_destroy(kbPtr);
      return SRERR_GRAMMARERROR;
    }

     // Grammar ID is first grammar from KB (FIX THIS!!)
    grammarId = KB_firstGrammarId(kbPtr);
    vocabId = SYNTAX_vocabularyId(KB_grammar(kbPtr, grammarId));
    VBX_DEBUG(VBX_print("  GramCommon: first vocab Id = %d\n", vocabId));
    VBX_DEBUG(VBX_print("  GramCommon: first grammar Id = %d\n", grammarId));
    script = SCRIPT_load(recFile, vocabId);
    if (script == NULL) {
      KB_destroy(kbPtr);
      return SRERR_GRAMMARERROR;
    }

     // Get the vocabulary
    VOCAB vocabulary;
    vocabulary = KB_vocabulary(kbPtr, vocabId);
    VBX_fatalIf(vocabulary == NULL);

    entryCnt = SCRIPT_entryCnt(script);
    if (!entryCnt) {
      KB_destroy(kbPtr);
      return SRERR_GRAMMARERROR;
    }

    entryPtr = promptBlk = (Ptr) VBX_calloc(1, promptBlkGrowthQntm);
    promptBlkSize =  promptBlkGrowthQntm; 

    for (totalBytes = i = 0; i < entryCnt; i++) {

      entry = SCRIPT_entry(script, recFile, i);
      if (entry == NULL)
        continue;

       // Construct an SRPHRASE containing the words in the phrase
      PVISESAPIPhrase sapiPhrasePtr;
      HYP_getSyntaxData(entry, &grammarId, NULL, NULL);
      sapiPhrasePtr = new VISESAPIPhrase(entry, kbPtr, recFile, vocabId, grammarId, gramObjPtr->KbTransType);

       // Reallocate if necessary
      if (totalBytes + sapiPhrasePtr->SRPhraseSize() > promptBlkSize) {
        promptBlk = (Ptr) VBX_realloc(promptBlk, promptBlkSize + promptBlkGrowthQntm);
        promptBlkSize += promptBlkGrowthQntm;
        entryPtr = promptBlk + totalBytes;     // General case: promptBlk will CHANGE!!
      }

      totalBytes += bytes = sapiPhrasePtr->SRPhraseSize();
      memcpy(entryPtr, sapiPhrasePtr->SRPhrase(), bytes);
      entryPtr += bytes;

       // Clean up for this entry
      HYP_destroy(entry);
      delete sapiPhrasePtr;
    }

     // Clean up the SCRIPT
    SCRIPT_annihilate(script);

    if (totalBytes == 0) {
      VBX_free(promptBlk);
      prompts->dwSize = 0;
      KB_destroy(kbPtr);
      return SRERR_GRAMMARERROR;
    }

    prompts->pData = promptBlk;
    prompts->dwSize = totalBytes;
    KB_destroy(kbPtr);

  } else {
    return SRERR_INVALIDPARAM;
  }

  return SRERR_NONE;
}

STDMETHODIMP
VISEISRGramCommon::TrainQuery(DWORD *trainingType)
{
  return SRERR_NOTSUPPORTED;
}


  // Methods for VISEISRGramCFG

VISEISRGramCFG::VISEISRGramCFG(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRGramCFG::~VISEISRGramCFG()
{
}

STDMETHODIMP
VISEISRGramCFG::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRGramCFG::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRGramCFG::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRGramCFG::LinkQuery(PCSTR pszLinkName, BOOL * fExist)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCFG::ListAppend(PCSTR pszListName, SDATA dWord)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCFG::ListGet(PCSTR pszListName, PSDATA pdWord)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCFG::ListRemove(PCSTR pszListName, SDATA dWord)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCFG::ListSet(PCSTR pszListName, SDATA dWord)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRGramCFG::ListQuery(PCSTR pszListName, BOOL * fExist)
{
  return SRERR_NOTSUPPORTED;
}


  // IVbxGrammar Interface

VISEIVbxGrammar::VISEIVbxGrammar(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr),
KbPtr(NULL)
{
  IMemory * allocator = ((PVISEGrammarObj) ObjPtr)->Allocator;
  VBX_fatalIf(!(RAAttributes = MBOX_createAttr(allocator, 1, 1 ,1)));
  VBX_fatalIf(!(ReturnAddress = MBOX_create(allocator, RAAttributes)));
}

VISEIVbxGrammar::~VISEIVbxGrammar()
{
  MBOX_destroy(ReturnAddress);
  VBX_free(RAAttributes);
  KB_destroy(KbPtr);
}

STDMETHODIMP
VISEIVbxGrammar::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEIVbxGrammar::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEIVbxGrammar::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

Bool
VISEIVbxGrammar::LoadGrammars(Bool loadTranslations)
{
  VFILE   recogStream = ((PVISEGrammarObj) ObjPtr)->RecStreamPtr->Stream();
  KbPtr = KB_create(NULL);

   // Load the speaker independent, application dependent knowledge base
  if (!KB_load(KbPtr, recogStream, VBX_VERBOSE)) {
    VBX_DEBUG(VBX_print("VISEIVbxGrammar::LoadGrammars: Cannot load the knowledge base\n"));

   // Optionally load the response translation tables
  } else if (loadTranslations && !KB_loadResponseData(KbPtr, recogStream, VBX_VERBOSE)) {
    VBX_DEBUG(VBX_print("VISEIVbxGrammar::LoadGrammars: Cannot load the translation tables\n"));

  } else {
    VBX_DEBUG(VBX_print("  VISEIVbxGrammar::LoadGrammars: Application \"%s\" loaded\n", KB_name(KbPtr)));
    return TRUE;
  }

  KB_destroy(KbPtr);
  KbPtr = NULL;
  return FALSE;
}

STDMETHODIMP
VISEIVbxGrammar::Parse(PCSTR vocabName, PCSTR phrase, PSTR * gramNames, DWORD * gramNamesLen)
{
  SYNTAX  * grammars;                    // The array of currently usable grammars
  SYNTAX  * gramPtr;                     // Pointer for grammar manipulation
  UInt      numGrammars;                 // The number of grammars in the vocabulary
  Char    * grammarName, * grammarNames;
  VOCAB     vocabulary = NULL;
  HRESULT   result = SRERR_NONE;
  Int       nameLength, length;

   // Load the grammars if necessary
  if (!KbPtr) LoadGrammars(FALSE);

   // Allocate a buffer (containing a NULL grammar name) for the grammar names
  grammarNames = (Char *) VBX_calloc(length = 1, sizeof(Char));

   // If a vocabulary is specified, use it
  if (vocabName) {
    if (!(vocabulary = KB_vocabularyByName(KbPtr, (Char *) vocabName)))
      result = SRERR_INVALIDPARAM;
   // Otherwise, use the vocabulary for the first rule
  } else if (!(vocabulary = KB_vocabulary(KbPtr, SYNTAX_vocabularyId(KB_grammar(KbPtr, KB_firstGrammarId(KbPtr)))))) {
    result = E_UNEXPECTED;
  }

  if (vocabulary != NULL && (gramPtr = grammars = VOCAB_parse(vocabulary, (Char *) phrase, &numGrammars))) {
     // Put each (NULL-terminated) grammar name into the buffer
    while (numGrammars--) {
      grammarName = SYNTAX_name(*gramPtr++);
      nameLength = (Int) (strlen(grammarName) + 1);
      grammarNames = (Char *) VBX_realloc(grammarNames, length + nameLength);
      memcpy(grammarNames + length - 1, grammarName, nameLength);
      length += nameLength;
    }
     // Put a NULL grammar name at the end of the buffer
    grammarNames[length - 1] = '\0';
    VBX_free(grammars);
  }

  *gramNames = grammarNames;
  *gramNamesLen = length;

  return SRERR_NONE;
}

STDMETHODIMP
VISEIVbxGrammar::Tokenize(PCSTR vocabName, PCSTR phrase, PSRPHRASE srPhrasePtr, DWORD phraseSize, DWORD * sizeNeededPtr)
{
  VOCAB    vocabulary = NULL;
  HRESULT  result = SRERR_NONE;

   // Load the grammars if necessary
  if (!KbPtr) LoadGrammars(FALSE);

   // If a vocabulary is specified, use it
  if (vocabName) {
    if (!(vocabulary = KB_vocabularyByName(KbPtr, (Char *) vocabName)))
      result = SRERR_INVALIDPARAM;
   // Otherwise, use the vocabulary for the first rule
  } else if (!(vocabulary = KB_vocabulary(KbPtr, SYNTAX_vocabularyId(KB_grammar(KbPtr, KB_firstGrammarId(KbPtr)))))) {
    result = E_UNEXPECTED;
  }

  if (vocabulary) {
     // Construct an SRPHRASE containing the words in the phrase
    PVISESAPIPhrase sapiPhrasePtr = new VISESAPIPhrase(phrase, vocabulary);
    *sizeNeededPtr = sapiPhrasePtr->SRPhraseSize();
    VBX_DEBUG(VBX_print("  VISEIVbxGrammar::Tokenize: SAPI phrase length = %u\n", *sizeNeededPtr));
     // sapiPhrasePtr->Print();  // DEBUG
    if (*sizeNeededPtr && *sizeNeededPtr <= phraseSize)
      memcpy(srPhrasePtr, sapiPhrasePtr->SRPhrase(), *sizeNeededPtr);
    else
      result = SRERR_VALUEOUTOFRANGE;
    delete sapiPhrasePtr;
  }

  return result;
}

STDMETHODIMP
VISEIVbxGrammar::StartTrainingPass(PCSTR rule)
{
  PVISEGrammarObj objPtr = (PVISEGrammarObj) ObjPtr;
  PVISEGrammarRequest gramReqPtr = new VISEGrammarRequest(SAPI_STARTTRAININGPASS, objPtr, (Char *) rule, FALSE, ReturnAddress);
  if (!MBOX_insert(objPtr->CMControlMBox, (OBJT) gramReqPtr))
    return E_UNEXPECTED;
  VBX_DEBUG(VBX_print("  GramCommon: sent SAPI_STARTTRAININGPASS to Engine for rule \"%s\"\n", rule ? rule : "(NULL)"));

  HRESULT hresult = (HRESULT) MBOX_remove(ReturnAddress);
  VBX_DEBUG(VBX_print("  VISEIVbxGrammar::StartTrainingPass: received confirmation of SAPI_STARTTRAININGPASS from Engine for rule \"%s\"\n", rule ? rule : "(NULL)"));
  return hresult;
}

STDMETHODIMP
VISEIVbxGrammar::EndTrainingPass(PCSTR rule, Bool finalPass)
{
  return EndTrainingPass(rule, NULL, finalPass);
}

STDMETHODIMP
VISEIVbxGrammar::EndTrainingPass(PCSTR rule, Char ** words, Bool finalPass)
{
  PVISEGrammarObj objPtr = (PVISEGrammarObj) ObjPtr;
  PVISEGrammarRequest gramReqPtr = new VISEGrammarRequest(SAPI_ENDTRAININGPASS, objPtr, (Char *) rule, finalPass, ReturnAddress);
  gramReqPtr->SetWordNames(words);
  if (!MBOX_insert(objPtr->CMControlMBox, (OBJT) gramReqPtr))
    return E_UNEXPECTED;
  VBX_DEBUG(VBX_print("  GramCommon: sent SAPI_ENDTRAININGPASS to Engine for rule \"%s\"\n", rule ? rule : "(NULL)"));

  HRESULT hresult = (HRESULT) MBOX_remove(ReturnAddress);
  VBX_DEBUG(VBX_print("  VISEIVbxGrammar::EndTrainingPass: received confirmation of SAPI_ENDTRAININGPASS from Engine for rule \"%s\"\n", rule ? rule : "(NULL)"));
  return hresult;
}

STDMETHODIMP
VISEIVbxGrammar::SaveModels(PCSTR rule, Int minTrainingCount)
{
  return SaveModels(rule, NULL, minTrainingCount);
}

STDMETHODIMP
VISEIVbxGrammar::SaveModels(PCSTR rule, Char ** words, Int minTrainingCount)
{
  PVISEGrammarObj objPtr = (PVISEGrammarObj) ObjPtr;
  PVISEGrammarRequest gramReqPtr = new VISEGrammarRequest(SAPI_SAVEMODELS, objPtr, (Char *) rule, FALSE, minTrainingCount, ReturnAddress);
  gramReqPtr->SetWordNames(words);
  if (!MBOX_insert(objPtr->CMControlMBox, (OBJT) gramReqPtr))
    return E_UNEXPECTED;
  VBX_DEBUG(VBX_print("  GramCommon: sent SAPI_SAVEMODELS to Engine for rule \"%s\"\n", rule ? rule : "(NULL)"));

  HRESULT hresult = (HRESULT) MBOX_remove(ReturnAddress);
  VBX_DEBUG(VBX_print("  VISEIVbxGrammar::SaveModels: received confirmation of SAPI_SAVEMODELS from Engine for rule \"%s\"\n", rule ? rule : "(NULL)"));
  return hresult;
}

STDMETHODIMP
VISEIVbxGrammar::SetTranslationKind(DWORD kind)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;

  VBX_fatalIf(gramObjPtr == NULL);

  switch (kind) {
  case SRRESTRKIND_HOST:
    gramObjPtr->KbTransType =  KB_HOST_RESPONSE;
    break;
  case SRRESTRKIND_VOICE:
    gramObjPtr->KbTransType = KB_VOICE_RESPONSE;
    break;
  case SRRESTRKIND_DISPLAY:
    gramObjPtr->KbTransType = KB_DISPLAY_RESPONSE;
    break;
  default:
    return SRERR_VALUEOUTOFRANGE;
  }

  return SRERR_NONE;
}


STDMETHODIMP
VISEIVbxGrammar::Translate(DWORD transKind, PCSTR rule, PSRPHRASE srPhrasePtr, PSTR buf, DWORD bufSize, DWORD *sizeNeededPtr)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;
  HRESULT         hresult = SRERR_NOTSUPPORTED;
  Bool            success;
  UInt            grammarId;
  SYNTAX          grammar;
  UInt            vocabId;
  VOCAB           vocab;

   // Load the kb if necessary
  if (!KbPtr) LoadGrammars(TRUE);

  VFILE recFile = gramObjPtr->RecStream()->Stream();
  VBX_fatalIf(!recFile);

   // Get the grammar Id of the specified rule, or the Id of the starting grammar if no rule is specified
  if (rule && strlen(rule)) {
    if ((success = (grammar = KB_grammarByName(KbPtr, (Char *) rule)) != NULL))
      grammarId = SYNTAX_id(grammar);
  } else {
    grammarId = KB_firstGrammarId(KbPtr);
    success = TRUE;
  }
  VBX_DEBUG(VBX_print("  IVbxGrammar::Translate: grammar Id = %d\n", grammarId));

   // Get the Id of the vocabulary that contains the specified grammar
  if (!(success = success && (grammar = KB_grammar(KbPtr, grammarId)) != NULL)) {
    hresult = SRERR_INVALIDRULE;
  } else {
    vocabId = SYNTAX_vocabularyId(grammar);
    VBX_DEBUG(VBX_print("  IVbxGrammar::Translate: vocab Id = %d\n", vocabId));
     // Get the vocabulary that contains the specified grammar
    if (!(success = (vocab = KB_vocabulary(KbPtr, vocabId)) != NULL)) {
      hresult = SRERR_INVALIDRULE;
    } else {

       // Construct a HYP containing the words in the phrase
      PVISESAPIPhrase  sapiPhrasePtr = new VISESAPIPhrase(srPhrasePtr, vocab);
       // sapiPhrasePtr->Print();  // DEBUG
      HYP  hyp = sapiPhrasePtr->Hypothesis();

      switch (transKind) {
        case SRRESTRKIND_HOST:
          success = KB_response(KbPtr, KB_HOST_RESPONSE, hyp, recFile, vocabId, grammarId, buf, bufSize, (UInt *) sizeNeededPtr);
          break;
        case SRRESTRKIND_VOICE:
          success = KB_response(KbPtr, KB_VOICE_RESPONSE, hyp, recFile, vocabId, grammarId, buf, bufSize, (UInt *) sizeNeededPtr);
          break;
        case SRRESTRKIND_DISPLAY:
          success = KB_response(KbPtr, KB_DISPLAY_RESPONSE, hyp, recFile, vocabId, grammarId, buf, bufSize, (UInt *) sizeNeededPtr);
          break;
        default:
          success = FALSE;
          hresult = SRERR_NOTSUPPORTED;
      }

      HYP_destroy(hyp);
      delete sapiPhrasePtr;
    }
  }
  
  if (success)
    hresult = SRERR_NONE;
  return hresult;
}


STDMETHODIMP
VISEIVbxGrammar::Translate(DWORD transKind, PCSTR rule, PCSTR plaintext, PSTR buf, DWORD bufSize, DWORD *sizeNeededPtr)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;
  HRESULT         hresult = SRERR_NOTSUPPORTED;
  Bool            success;
  UInt            grammarId;
  SYNTAX          grammar;
  UInt            vocabId;
  VOCAB           vocab;

   // Load the kb if necessary
  if (!KbPtr) LoadGrammars(TRUE);

  VFILE recFile = gramObjPtr->RecStream()->Stream();
  VBX_fatalIf(!recFile);

   // Get the grammar Id of the specified rule, or the Id of the starting grammar if no rule is specified
  if (rule && strlen(rule)) {
    if ((success = (grammar = KB_grammarByName(KbPtr, (Char *) rule)) != NULL))
      grammarId = SYNTAX_id(grammar);
  } else {
    grammarId = KB_firstGrammarId(KbPtr);
    success = TRUE;
  }
  VBX_DEBUG(VBX_print("  IVbxGrammar::Translate: grammar Id = %d\n", grammarId));

   // Get the Id of the vocabulary that contains the specified grammar
  if (!(success = success && (grammar = KB_grammar(KbPtr, grammarId)) != NULL)) {
    hresult = SRERR_INVALIDRULE;
  } else {
    vocabId = SYNTAX_vocabularyId(grammar);
    VBX_DEBUG(VBX_print("  IVbxGrammar::Translate: vocab Id = %d\n", vocabId));
     // Get the vocabulary that contains the specified grammar
    if (!(success = (vocab = KB_vocabulary(KbPtr, vocabId)) != NULL)) {
      hresult = SRERR_INVALIDRULE;
    } else {

       // Construct a HYP containing the words in the phrase
      PVISESAPIPhrase  sapiPhrasePtr = new VISESAPIPhrase(plaintext, vocab);
       // sapiPhrasePtr->Print();  // DEBUG
      HYP  hyp = sapiPhrasePtr->Hypothesis();

      switch (transKind) {
        case SRRESTRKIND_HOST:
          success = KB_response(KbPtr, KB_HOST_RESPONSE, hyp, recFile, vocabId, grammarId, buf, bufSize, (UInt *) sizeNeededPtr);
          break;
        case SRRESTRKIND_VOICE:
          success = KB_response(KbPtr, KB_VOICE_RESPONSE, hyp, recFile, vocabId, grammarId, buf, bufSize, (UInt *) sizeNeededPtr);
          break;
        case SRRESTRKIND_DISPLAY:
          success = KB_response(KbPtr, KB_DISPLAY_RESPONSE, hyp, recFile, vocabId, grammarId, buf, bufSize, (UInt *) sizeNeededPtr);
          break;
        default:
          success = FALSE;
          hresult = SRERR_NOTSUPPORTED;
      }

      HYP_destroy(hyp);
      delete sapiPhrasePtr;
    }
  }
  
  if (success)
    hresult = SRERR_NONE;
  return hresult;
}

STDMETHODIMP
VISEIVbxGrammar::QueryGrammarName(PSTR nameBuf, DWORD bufSize, DWORD * sizeNeeded)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;

   // Load the kb if necessary
  if (!KbPtr) LoadGrammars(TRUE);
  VBX_fatalIf(!KbPtr || !nameBuf || !sizeNeeded);

 *sizeNeeded = strlen(KB_name(KbPtr)) + 1;

  if (*sizeNeeded > bufSize) {
    return SRERR_VALUEOUTOFRANGE;
  } 

  strcpy(nameBuf, KB_name(KbPtr));
  return SRERR_NONE;
}

STDMETHODIMP
VISEIVbxGrammar::QueryDefaultRuleName(PSTR nameBuf, DWORD bufSize, DWORD * sizeNeeded)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;

   // Load the kb if necessary
  if (!KbPtr) LoadGrammars(TRUE);
  VBX_fatalIf(!KbPtr || !nameBuf || !sizeNeeded);

 *sizeNeeded = strlen(SYNTAX_name(KB_grammar(KbPtr, KB_firstGrammarId(KbPtr)))) + 1;

  if (*sizeNeeded > bufSize) {
    return SRERR_VALUEOUTOFRANGE;
  } 

  strcpy(nameBuf, SYNTAX_name(KB_grammar(KbPtr, KB_firstGrammarId(KbPtr))));
  return SRERR_NONE;
}

STDMETHODIMP
VISEIVbxGrammar::QueryRuleNames(PSTR * ruleNamesBuf, DWORD * bufSize)
{
  SYNTAX    grammar = NULL;
  Int       i;
  Ptr       namesBlk;
  Ptr       entryPtr;
  Int       bytes, totalBytes;
  Int       entryCnt, namesBlkSize = 0;

  // Load the kb if necessary
  if (!KbPtr) LoadGrammars(TRUE);

  const Int namesBlkGrowthQuantum = 512;

  entryPtr = namesBlk = (Ptr) VBX_calloc(1, namesBlkGrowthQuantum);
  namesBlkSize =  namesBlkGrowthQuantum;

  for (totalBytes = i = 0;; i++) {
    grammar = KB_grammar(KbPtr, (UInt) i);
    if (grammar == NULL) {
      if (i == 0)
        continue;
      else
        break;
    }

     // Reallocate if necessary, making sure that there is room for the extra terminating null
    bytes = strlen(SYNTAX_name(grammar)) + 1;
    while ((totalBytes + bytes + 1) > namesBlkSize) {
      namesBlk = (Ptr) VBX_realloc(namesBlk, namesBlkSize + namesBlkGrowthQuantum);
      namesBlkSize += namesBlkGrowthQuantum;
      entryPtr = namesBlk + totalBytes;   // In general, namesBlk will CHANGE!!
    }

    totalBytes += bytes;
    strncpy(entryPtr, SYNTAX_name(grammar), bytes);
    entryPtr += bytes;
  }

 *entryPtr = '\0';
 *bufSize = totalBytes + 1;
 *ruleNamesBuf = namesBlk;
  return SRERR_NONE;
}
    
STDMETHODIMP
VISEIVbxGrammar::RemoveNotifySink(void)
{
  PVISEGrammarObj gramObjPtr = (PVISEGrammarObj) ObjPtr;
  if (gramObjPtr->NotifySinkPtr) gramObjPtr->NotifySinkPtr->Release();
  gramObjPtr->NotifySinkPtr = NULL;
  return SRERR_NONE;
}
