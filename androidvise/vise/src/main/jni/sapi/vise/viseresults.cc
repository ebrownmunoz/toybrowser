#include "pthr.h"
#include <stdio.h>
#include <string.h>

// Verbex SAPI Objects
#include "viseengine.hh"
#include "visegram.hh"
#include "visemessage.hh"
#include "visephrase.hh"
#include "viseresults.hh"

// VISE infrastructure
#include "vfile.h"

// VULCAN Inclusions
#include "vocab.h"
#include "syntax.h"
#include "models.h"
#include "kb.h"
#include "hyp.h"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "critsect.h"
#include "ifaces.h"
#include "mbox.h"
#include "sphdr.h"
#include "wavrw.h"
#include "sn.h"

#ifdef DEBUG
#define VISERES_DEBUG
#endif

#ifdef VISERES_DEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

#define SMAX  512

VISEResultsObj::VISEResultsObj(IMemory * allocator, PVISEGrammarObj grammarObjPtr, PVISEKbObj kbObjPtr, UInt nBest, PVISEEngineObj engineObjPtr):
RefCnt(0),
Allocator(allocator),
EngineObjPtr(engineObjPtr),
CMTranscription(NULL),
CMHypotheses(NULL),
GrammarPtr(grammarObjPtr),
NotifySinkPtr(NULL),
KbPtr(kbObjPtr),
VISEISRResAudioPtr(NULL),
VISEISRResAudioExPtr(NULL),
VISEISRResBasicPtr(NULL),
VISEISRResCorrectionPtr(NULL),
VISEISRResGraphPtr(NULL),
VISEISRResMemoryPtr(NULL),
VISEISRResTranslatePtr(NULL),
CMRuleName(NULL),
Built(FALSE),
CMNBest(nBest),
FeatureFrames(NULL),
FirstFeatSN(0),
LastFeatSN(0),
FeatFrameCnt(0),
FirstAudioSN(0),
LastAudioSN(0),
AudioSampCnt(0),
AgcSub((Float)0.),
CMAudioSampRate(0),
CMAudioData(NULL)
{
  CRITSECT_CREATE(RefCntCritSect, Allocator, NULL);

  if (GrammarPtr) GrammarPtr->AddRef();
  if (KbPtr) KbPtr->AddRef();
  UnknwnPtr = this;

  VBX_DEBUG(VBX_print("  VISEResultsObj: constructing new Results object @%p\n", this));
}

VISEResultsObj::~VISEResultsObj()
{  
  VBX_DEBUG(VBX_print("  VISEResultsObj: destroying Results object @%p\n", this));

  // Delete any contained interfaces
  delete VISEISRResAudioPtr;
  delete VISEISRResAudioExPtr;
  delete VISEISRResBasicPtr;
  delete VISEISRResCorrectionPtr;
  delete VISEISRResGraphPtr;
  delete VISEISRResMemoryPtr;
  delete VISEISRResTranslatePtr;

  // Deallocate strings
  VBX_free(CMRuleName);

  // Annihilate any hypotheses
  HYP_destroy(CMHypotheses);

  // Deallocate any feature frames
  if (FeatureFrames) Allocator->Free(Allocator, FeatureFrames);

  // Deallocate the audio data
  // VBX_free(CMAudioData);
  OBJT_free(CMAudioData);

  // Deallocate the transcription
  VBX_free(CMTranscription);

  // Dequeue if necessary
  if (Built) GrammarPtr->DequeueResults(this);

  // Release the referenced objects
  if (NotifySinkPtr) {
    NotifySinkPtr->Release();
    VBX_DEBUG(VBX_print("  VISEResultsObj::~VISEResultsObj: (@%p) released NotifySinkPtr @%p\n", this, NotifySinkPtr));
  }
  if (GrammarPtr) {
    GrammarPtr->Release();
    VBX_DEBUG(VBX_print("  VISEResultsObj::~VISEResultsObj: (@%p) released GrammarPtr\n", this));
  }
  if (KbPtr) {
    KbPtr->Release();
    VBX_DEBUG(VBX_print("  VISEResultsObj::~VISEResultsObj: (@%p) released KbPtr\n", this));
  }

  CRITSECT_DESTROY(RefCntCritSect);

  VBX_DEBUG(VBX_print("  VISEResultsObj::~VISEResultsObj: (@%p) released critsect\n", this));

}

STDMETHODIMP
VISEResultsObj::QueryInterface(REFIID riid, LPVOID *ppv)
{
  *ppv = NULL;

  // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown)) {
    *ppv = (LPVOID) this;
  } else if (IsEqualIID(riid, IID_ISRResAudio)) {
    if (!VISEISRResAudioPtr)
      VISEISRResAudioPtr = new VISEISRResAudio(this, UnknwnPtr);
    *ppv = VISEISRResAudioPtr;
  } else if (IsEqualIID(riid, IID_ISRResAudioEx)) {
    if (!VISEISRResAudioExPtr)
      VISEISRResAudioExPtr = new VISEISRResAudioEx(this, UnknwnPtr);
    *ppv = VISEISRResAudioExPtr;
  } else if (IsEqualIID(riid, IID_ISRResBasic)) {
    if (!VISEISRResBasicPtr)
      VISEISRResBasicPtr = new VISEISRResBasic(this, UnknwnPtr);
    *ppv = VISEISRResBasicPtr;
  } else if (IsEqualIID(riid, IID_ISRResCorrection)) {
    if (!VISEISRResCorrectionPtr)
      VISEISRResCorrectionPtr = new VISEISRResCorrection(this, UnknwnPtr);
    *ppv = VISEISRResCorrectionPtr;
  } else if (IsEqualIID(riid, IID_ISRResGraph)) {
    if (!VISEISRResGraphPtr)
      VISEISRResGraphPtr = new VISEISRResGraph(this, UnknwnPtr);
    *ppv = VISEISRResGraphPtr;
  } else if (IsEqualIID(riid, IID_ISRResMemory)) {
    if (!VISEISRResMemoryPtr)
      VISEISRResMemoryPtr = new VISEISRResMemory(this, UnknwnPtr);
    *ppv = VISEISRResMemoryPtr;
  } else if (IsEqualIID(riid, IID_ISRResTranslate)) {
    if (!VISEISRResTranslatePtr)
      VISEISRResTranslatePtr = new VISEISRResTranslate(this, UnknwnPtr);
    *ppv = VISEISRResTranslatePtr;
  }

  if (*ppv != NULL) {
    // Update the reference count
    ((LPUNKNOWN) *ppv)->AddRef();
    // Build the results object if it's not yet built
    if (!Built) Build();
    return NOERROR;
  }
  return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG)
VISEResultsObj::AddRef()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEResultsObj::AddRef(): RefCnt for VISEResultsObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISEResultsObj::Release()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISEResultsObj::Release(): RefCnt for VISEResultsObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) { 
    delete this;
    VBX_DEBUG(VBX_print("  VISEResultsObj::Release(): successfully destroyed\n"));
  }
  return refCnt;
}

void
VISEResultsObj::Build()
{
  if (!Built) {
    if (GrammarPtr) GrammarPtr->EnqueueResults(this);
    CMNBest  = HYP_numAlternatives(CMHypotheses);
    if ((GrammarId = SYNTAX_id(KB_grammarByName(KbPtr->Kb(), CMRuleName))))
      VocabId = SYNTAX_vocabularyId(KB_grammar(KbPtr->Kb(), GrammarId));
    else
      VocabId = 0;
    Built = TRUE;
  }
}
    
PVISEGrammarObj
VISEResultsObj::Grammar(void)
{
  return GrammarPtr;
}

PVISEKbObj
VISEResultsObj::Kb(void)
{
  return KbPtr;
}

PISRGRAMNOTIFYSINK
VISEResultsObj::NotifySink(void)
{
  if (NotifySinkPtr)
    return NotifySinkPtr;
  else
    return GrammarPtr ? (PISRGRAMNOTIFYSINK) GrammarPtr->NotifySinkPtr : NULL;
}

Char *
VISEResultsObj::RuleName(void)
{
  return CMRuleName;
}

void
VISEResultsObj::SetRuleName(Char * ruleName)
{
  VBX_free(CMRuleName);
  CMRuleName = VBX_salloc(ruleName);
}

Ptr
VISEResultsObj::AcousticFeatures(SN * first, SN * last, UInt * frameCnt)
{
  *first = FirstFeatSN;
  *last = LastFeatSN;
  *frameCnt = FeatFrameCnt;
  return FeatureFrames;
}

void
VISEResultsObj::SetAcousticFeatures(Ptr frames, SN first, SN last, UInt frameCnt)
{
  VBX_DEBUG(VBX_print("  VISEResultsObj::SetAcousticFeatures: In ResultsObj @%p set AF @%p\n", this, frames));
   // Deallocate any existing feature frames
  if (FeatureFrames) Allocator->Free(Allocator, FeatureFrames);

  FeatureFrames = frames;
  FirstFeatSN = first;
  LastFeatSN = last;
  FeatFrameCnt = frameCnt;
}

Ptr
VISEResultsObj::AudioData(SN * first, SN * last, UInt * sampCnt)
{
  Ptr ptr = NULL;
  *first = FirstAudioSN;
  *last = LastAudioSN;
  *sampCnt = AudioSampCnt;
  if (CMAudioData != NULL)
    ptr = (Ptr) &OBJT_contents(CMAudioData);
  return ptr;
}

void
VISEResultsObj::SetAudioData(OBJT audioData, SN first, SN last, UInt sampCnt)
{
  VBX_DEBUG(VBX_print("  VISEResultsObj::SetAudioData: In ResultsObj @%p set AD @%p\n", this, audioData));
   // Deallocate any existing audio data
  //VBX_free(CMAudioData);
  OBJT_free(CMAudioData);

  CMAudioData = audioData;
  FirstAudioSN = first;
  LastAudioSN = last;
  AudioSampCnt = sampCnt;
}

HRESULT
VISEResultsObj::CreateWAV(PSDATA pWav, SN first, SN last)
{
  Ptr  sampData = NULL;
  Ptr  wav = NULL;
  SN   uttFirstSN = 0, uttLastSN = 0;
  Int  sampCnt = 0, hdrSize = 0;
  
  if (pWav == NULL) return E_INVALIDARG;

  sampData = AudioData(&uttFirstSN, &uttLastSN, (UInt *)&sampCnt);
  if (sampData == NULL || first < uttFirstSN || last > uttLastSN) return SRERR_NOTENOUGHDATA;
  
   // The sampData variable points to the contents of an OBJT obtained from a CLASS maintained in FE.
   // Each such OBJT is capable of holding the full contents of the audio buffer, plus 1024 bytes reserved for a NIST header.
  wav = (Ptr) sampData;

  HDR hdr = HDR_create();
  if (!hdr) return E_OUTOFMEMORY;
  HDR_setSizeMult(hdr, HDR_SIZE_MULT_MIN);
  if (HDR_isNISTMem(wav)) {
     // The header has already been created and the audio samples have been moved accordingly
    hdrSize = HDR_parse(wav, hdr) - wav;
    HDR_free(hdr);
    VBX_print("ISRResAudio::CreateWAV: WARNING - Multiple calls to GetWAV or CreateWAV yield the same result, regardless of the parameters");
  } else {
     // Populate the NIST header
    HDR_addFld(hdr, "created_by", STR_TYPE, (Char *)"SAPIVISE v1.0", NULL);
    WAV_addDfltHdrFields(hdr);
    HDR_addFld(hdr, "sample_rate", INT_TYPE, &CMAudioSampRate, NULL);
    sampCnt = abs((Int)(first - last)) / 2;
    HDR_addFld(hdr, "sample_count", INT_TYPE, &sampCnt, NULL);
    HDR_addFld(hdr, "sample_coding", STR_TYPE, (Char *)"pcm", NULL);

    // Try to obtain the 0th NBest recognition result and record it
    ISRResBasic * ISRResBasicPtr = NULL;
    PSRPHRASE     srPhrasePtr = NULL;
    PSAPIPhrase   sapiPhrasePtr = NULL;
    ULong         needed;

    if (SRERR_NONE == QueryInterface(IID_ISRResBasic, (PVOID *) &ISRResBasicPtr)) {
      Char * buf = (Char *) calloc(1, SMAX);
      if (!buf) {
        HDR_free(hdr);
        return E_OUTOFMEMORY;
      }
      srPhrasePtr = (PSRPHRASE) buf;
      if (SRERR_NONE == ISRResBasicPtr->PhraseGet(0, srPhrasePtr, (UInt) SMAX, &needed)) {
        sapiPhrasePtr = new SAPIPhrase(srPhrasePtr);
        Char *sapiText = sapiPhrasePtr->Text();
        HDR_addFld(hdr, "nbest0", STR_TYPE, sapiText, NULL);
        VBX_free(sapiText);
        delete sapiPhrasePtr;
      } else {
        HDR_addFld(hdr, "nbest0", STR_TYPE, (Char *)"<none>", NULL);
      }
      VBX_free(buf);
    }

    if (ISRResBasicPtr != NULL)
     ISRResBasicPtr->Release();

    Ptr hdrPtr = HDR_writeMem(hdr, &hdrSize);
    HDR_free(hdr);

     // Allocate the waveform to be returned
    SN wavSize = last - first;
    if (wavSize < 0) {
      VBX_free(hdrPtr);
      return SRERR_INVALIDPARAM;
    }

     // Move the audio samples to a position immediately following the allocation for the header
    int offset = first - uttFirstSN;
    offset -= offset % 2;
    memmove(wav + hdrSize, sampData + offset, last - first);
     // Copy in the NIST header
    memcpy(wav, hdrPtr, hdrSize);
    VBX_free(hdrPtr);
  }

  pWav->pData = (PVOID) wav;
  pWav->dwSize = last - first + hdrSize;
  return SRERR_NONE;
}
  
Float
VISEResultsObj::AGCsub(void)
{
  return AgcSub;
}

void
VISEResultsObj::SetAGCsub(Float agcSub)
{
  AgcSub = agcSub;
}

Int
VISEResultsObj::AudioSampRate(void)
{
  return CMAudioSampRate;
}

void
VISEResultsObj::AudioSampRate(Int audioSampRate)
{
  CMAudioSampRate = audioSampRate;
}

HYP
VISEResultsObj::Hypotheses(void)
{
  return CMHypotheses;
}

void
VISEResultsObj::SetHypotheses(HYP hypotheses)
{
  HYP_destroy(CMHypotheses);
  CMHypotheses = hypotheses;
}

UInt
VISEResultsObj::NBest(void)
{
  return CMNBest;
}

PSRPHRASE
VISEResultsObj::Transcription(void)
{
  return CMTranscription;
}

void
VISEResultsObj::Transcription(PSRPHRASE srPhrasePtr)
{
  if (srPhrasePtr) {
    // Deallocate the previous transcription
    VBX_free(CMTranscription);

    // Allocate memory for the SRPHRASE, and copy srPhrase into it
    CMTranscription = (PSRPHRASE) VBX_calloc(srPhrasePtr->dwSize, sizeof(Char));
    memcpy(CMTranscription, srPhrasePtr, srPhrasePtr->dwSize);
  }
}



PVISEEngineObj
VISEResultsObj::EngineObject(void)
{
  return EngineObjPtr;
}

  // Methods for VISEISRResAudio

VISEISRResAudio::VISEISRResAudio(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResAudio::~VISEISRResAudio()
{
}

STDMETHODIMP
VISEISRResAudio::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResAudio::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResAudio::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResAudio::GetWAV(PSDATA pWav)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;
  PVISEEngineObj  engineObj = resultsObj->EngineObject();
  HRESULT hresult = SRERR_NONE;
  SN      audFirstSN = 0, audLastSN = 0;
  Ptr     sampData = NULL;
  Int     bouSilenceLength = 0, eouSilenceLength = 0;
  Bool    error = FALSE;
  UInt    sampCnt = 0;
  
  if (pWav == NULL)
    return E_INVALIDARG;

  sampData  = resultsObj->AudioData(&audFirstSN, &audLastSN, &sampCnt);
  if (sampData == NULL)
    return SRERR_NOTENOUGHDATA;

  if (hresult = engineObj->GetParameter(VISE_BOUSILMSECS, &bouSilenceLength)) {
    VBX_print("ISRResAudio::GetWAV: WARNING - GetParameter for VISE_BOUSILMSECS FAILS (%x)\n", hresult);
    error = TRUE;
  }
  if (hresult = engineObj->GetParameter(VISE_EOUSILMSECS, &eouSilenceLength)) {
    VBX_print("ISRResAudio::GetWAV: WARNING - GetParameter for VISE_EOUSILMSECS FAILS (%x)\n", hresult);
    error = TRUE;
  }

  SN hypFirstSN = 0, hypLastSN = 0;
  SN firstSN = 0, lastSN = 0;
  if (resultsObj->CMHypotheses) HYP_boundaries(resultsObj->CMHypotheses, &hypFirstSN, &hypLastSN);
  VBX_DEBUG(VBX_print("VISEISRResAudio::GetWAV:\n"));
  VBX_DEBUG(VBX_print("  Audio Queue: audFirstSN = " QuadFmt(-) ", audLastSN = " QuadFmt(-) "\n", audFirstSN, audLastSN));
  VBX_DEBUG(VBX_print("  Hypothesis:  hypFirstSN = " QuadFmt(-) ", hypLastSN = " QuadFmt(-) "\n", hypFirstSN, hypLastSN));
  VBX_DEBUG(VBX_print("  Padding:     bouSilenceLength %d msec; eouSilenceLength %d msec\n", bouSilenceLength, eouSilenceLength));

  if (resultsObj->CMAudioSampRate == 0) {
    VBX_print("ISRResAudio::GetWAV: WARNING - audio sample rate is zero\n");
    error = TRUE;
  }

   // Try to constrain to the desired SN range
  if (!error) {
    bouSilenceLength = (bouSilenceLength * resultsObj->CMAudioSampRate * 2) / 1000;  // Convert milliseconds to bytes
    firstSN = hypFirstSN - bouSilenceLength;
    if (firstSN < audFirstSN) {
      VBX_DEBUG(VBX_print("ISRResAudio::GetWAV: can't accomodate %d bytes of BOU silence; first SN is " QuadFmt(-) "\n", bouSilenceLength, audFirstSN));
      firstSN = audFirstSN;
    }
    eouSilenceLength = (eouSilenceLength * resultsObj->CMAudioSampRate * 2) / 1000;  // Convert milliseconds to bytes
    lastSN = hypLastSN + eouSilenceLength;
    if (lastSN > audLastSN) {
      VBX_DEBUG(VBX_print("ISRResAudio::GetWAV: can't accomodate %d bytes of EOU silence; last SN is " QuadFmt(-) "\n", eouSilenceLength, audLastSN));
      lastSN = audLastSN;
    }
    if (firstSN >= lastSN) {
      VBX_DEBUG(VBX_print("ISRResAudio::GetWAV: WARNING - firstSN (" QuadFmt(-) ") >= lastSN (" QuadFmt(-) ")\n", firstSN, lastSN));
      error = TRUE;
    }
  }

   // If something has gone wrong, just return all of the available data
  if (error) {
    firstSN = audFirstSN;
    lastSN = audLastSN;
  }

  // this causes the full audio to be saved regardless
  firstSN = audFirstSN;
  lastSN = audLastSN;
   
  VBX_DEBUG(VBX_print("VISEISRResAudio::GetWAV: firstSN = " QuadFmt(-) ", lastSN = " QuadFmt(-) "\n", firstSN, lastSN));
  return resultsObj->CreateWAV(pWav, firstSN, lastSN);
}

  // Methods for VISEISRResAudioEx

VISEISRResAudioEx::VISEISRResAudioEx(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResAudioEx::~VISEISRResAudioEx()
{
}

STDMETHODIMP
VISEISRResAudioEx::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResAudioEx::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResAudioEx::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResAudioEx::GetWAV(PSDATA pWav, QWORD startSN, QWORD endSN)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;
  
  if (pWav == NULL)
    return E_INVALIDARG;

  return resultsObj->CreateWAV(pWav, startSN, endSN);
}

  // Methods for VISEISRResBasic

VISEISRResBasic::VISEISRResBasic(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResBasic::~VISEISRResBasic()
{
}

STDMETHODIMP
VISEISRResBasic::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResBasic::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResBasic::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResBasic::PhraseGet(DWORD dwRank, PSRPHRASE pSRPhrase, DWORD dwPhraseSize,  DWORD * pdwPhraseNeeded)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;
  HRESULT         status;

  if (pSRPhrase == NULL)
    return SRERR_INVALIDPARAM;

  pSRPhrase->dwSize = 0;
  if (resultsObj->CMHypotheses) {
    if (dwRank < resultsObj->CMNBest) {

      PVISESAPIPhrase sapiPhrasePtr  = new VISESAPIPhrase(&resultsObj->CMHypotheses[dwRank], KB_vocabulary(resultsObj->KbPtr->Kb(), resultsObj->VocabId));
      UInt            sapiPhraseSize = sapiPhrasePtr->SRPhraseSize();

      *pdwPhraseNeeded = sapiPhraseSize;
      if (sapiPhraseSize <= dwPhraseSize) {
        // VBX_DEBUG(VBX_print("  PhraseGet: Copying SRPhraseSize (%d) bytes into SRPhraseBuffer\n", sapiPhraseSize));
        memcpy(pSRPhrase, sapiPhrasePtr->SRPhrase(), sapiPhraseSize);
        status = SRERR_NONE;
      } else {
        // VBX_DEBUG(VBX_print("  PhraseGet: SRPhraseSize (%d) > SRPhraseBufferSize (%d)\n", sapiPhraseSize, dwPhraseSize));
        status = SRERR_INVALIDPARAM;
      }
      delete sapiPhrasePtr;
    } else {
      // VBX_DEBUG(VBX_print("  PhraseGet: dwRank (%d) >= NBest (%d)\n", dwRank, resultsObj->CMNBest));
      *pdwPhraseNeeded = 0;
      status = SRERR_VALUEOUTOFRANGE;
    }
  } else {
    *pdwPhraseNeeded = 0;
    status = SRERR_NOTENOUGHDATA;
  }

  return status;
}

STDMETHODIMP
VISEISRResBasic::Identify(GUID * pGUID)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResBasic::TimeGet(PQWORD pqTimeStampBegin, PQWORD pqTimeStampEnd)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;
  SN              start, end;

  if (resultsObj->CMHypotheses) {
    HYP_boundaries(resultsObj->CMHypotheses, &start, &end);
    if (pqTimeStampBegin)
      *pqTimeStampBegin = start;
    if (pqTimeStampEnd)
      *pqTimeStampEnd = end;
    return SRERR_NONE;
  } else {
    return SRERR_NOTENOUGHDATA;
  }
}

STDMETHODIMP
VISEISRResBasic::FlagsGet(DWORD dwRank, DWORD * pdwFlags)
{
  return SRERR_NOTSUPPORTED;
}


  // Methods for VISEISRResCorrection

VISEISRResCorrection::VISEISRResCorrection(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResCorrection::~VISEISRResCorrection()
{
}

STDMETHODIMP
VISEISRResCorrection::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResCorrection::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResCorrection::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResCorrection::Correction(PSRPHRASE pSRPhrase, WORD wConfidence)
{
  PVISEResultsObj objPtr = (PVISEResultsObj) ObjPtr;

   // Annihilate the (incorrect) hypotheses
  objPtr->SetHypotheses(NULL);

  if (!objPtr->FeatureFrames)
    return SRERR_NOTENOUGHDATA;

   // Copy the (correct) transcription into the results object
  if (!pSRPhrase) return E_INVALIDARG;
  objPtr->Transcription(pSRPhrase);

   // Put a results notification sink into the results object, if it is not there
  if (!objPtr->NotifySinkPtr) {
    objPtr->NotifySinkPtr = (PISRGRAMNOTIFYSINK) new VISEISRResNotifySink(NULL, NULL);
    objPtr->NotifySinkPtr->AddRef();
  }

  return objPtr->EngineObjPtr->Force(objPtr);
}

STDMETHODIMP
VISEISRResCorrection::Validate(WORD wConfidence)
{
  PVISEResultsObj objPtr = (PVISEResultsObj) ObjPtr;

  return objPtr->EngineObjPtr->Train(objPtr);
}


  // Methods for VISEISRResGraph

VISEISRResGraph::VISEISRResGraph(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResGraph::~VISEISRResGraph()
{
}

STDMETHODIMP
VISEISRResGraph::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResGraph::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResGraph::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResGraph::BestPathPhoneme(DWORD dwRank, DWORD * padwPath, DWORD dwPathSize, DWORD * pdwPathNeeded)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResGraph::BestPathWord(DWORD dwRank, DWORD * padwPath, DWORD dwPathSize, DWORD * pdwPathNeeded)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResGraph::GetPhonemeNode(DWORD dwPhonemeNode, PSRRESPHONEMENODE pNode, PWCHAR cIPA, PCHAR cEngine)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResGraph::GetWordNode(DWORD dwWordNode, PSRRESWORDNODE pNode, PSRWORD pSRWord, DWORD dwMemSize, DWORD * pdwMemNeeded)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResGraph::PathScorePhoneme(DWORD * paNodes, DWORD dwNumNodes, LONG * plScore)
{
  // For now, this method works only by defaulting to the best path
  if (paNodes) {
    return SRERR_NOTSUPPORTED;
  } else if (plScore) {
    HYP hyp = ((PVISEResultsObj) ObjPtr)->Hypotheses();
    if (hyp) {
      *plScore = HYP_totalScore(hyp);
      return SRERR_NONE;
    } else {
      return SRERR_NOTENOUGHDATA;
    }
  } else {
    return SRERR_INVALIDPARAM;
  }
}

STDMETHODIMP
VISEISRResGraph::PathScoreWord(DWORD * paNodes, DWORD dwNumNodes, LONG * plScore)
{
  // For now, this method works only by defaulting to the best path
  if (paNodes) {
    return SRERR_NOTSUPPORTED;
  } else if (plScore) {
    HYP hyp = ((PVISEResultsObj) ObjPtr)->Hypotheses();
    if (hyp) {
      *plScore = HYP_totalScore(hyp);
      return SRERR_NONE;
    } else {
      return SRERR_NOTENOUGHDATA;
    }
  } else {
    return SRERR_INVALIDPARAM;
  }
}


  // Methods for VISEISRResMemory

VISEISRResMemory::VISEISRResMemory(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResMemory::~VISEISRResMemory()
{
}

STDMETHODIMP
VISEISRResMemory::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResMemory::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResMemory::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResMemory::Free(DWORD dwKind)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;

  if (dwKind & SRRESMEMKIND_AUDIO) resultsObj->SetAudioData(NULL, 0, 0, 0);
  if (dwKind & SRRESMEMKIND_CORRECTION) resultsObj->SetAcousticFeatures(NULL, 0, 0, 0);
  if (dwKind & SRRESMEMKIND_EVAL);
  if (dwKind & SRRESMEMKIND_PHONEMEGRAPH);
  if (dwKind & SRRESMEMKIND_WORDGRAPH);

  return SRERR_NONE;
}

STDMETHODIMP
VISEISRResMemory::Get(DWORD * pdwKind, DWORD * pdwMemory)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;
  SN   first;
  SN   last;
  UInt size;

  *pdwKind = 0;
  *pdwMemory = sizeof(VISEResultsObj);

  if (resultsObj->CMAudioData) {
    *pdwKind |= SRRESMEMKIND_AUDIO;
    resultsObj->AudioData(&first, &last, &size);
    *pdwMemory += last - first;
  }
  if (resultsObj->FeatureFrames) {
    *pdwKind |= SRRESMEMKIND_CORRECTION;
    resultsObj->AcousticFeatures(&first, &last, &size);
     // For allocation estimation purposes, the feature frame is assumed to be 16 bytes long
    *pdwMemory += size * 16;
  }

  return SRERR_NONE;
}

STDMETHODIMP
VISEISRResMemory::LockGet(BOOL * pfLock)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResMemory::LockSet(BOOL fLock)
{
  return SRERR_NOTSUPPORTED;
}


  // VISEISRResNotifySink methods

VISEISRResNotifySink::VISEISRResNotifySink(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
  VBX_DEBUG(VBX_print("  VISEISRResNotifySink: constructing new results notification sink @%p\n", this));
}

VISEISRResNotifySink::~VISEISRResNotifySink()
{
  VBX_DEBUG(VBX_print("  VISEISRResNotifySink: destroying results notification sink @%p\n", this));
}

STDMETHODIMP
VISEISRResNotifySink::QueryInterface(REFIID riid, LPVOID *ppv)
// This doesn't really need to be implemented, since it's never called
{
  if (UnknwnPtr) {
    return UnknwnPtr->QueryInterface(riid, ppv);
  } else {
    *ppv = NULL;

    // Always return our IUnknown for IID_IUnknown
    if (IsEqualIID(riid, IID_IUnknown))
      *ppv = (LPVOID) this;
    else if (IsEqualIID(riid, IID_ISRGramNotifySink))
      *ppv = (LPVOID) this;

    // Update the reference count here
    if (*ppv != NULL) {
      ((LPUNKNOWN) *ppv)->AddRef();
      return NOERROR;
    }
    return E_NOINTERFACE;
  }
}

STDMETHODIMP_(ULONG)
VISEISRResNotifySink::AddRef()
{
  ++RefCnt;
  if (UnknwnPtr) return UnknwnPtr->AddRef();
  else return RefCnt;
}

STDMETHODIMP_(ULONG)
VISEISRResNotifySink::Release()
{
  ULONG refCnt;

  refCnt = --RefCnt;
  if (UnknwnPtr) {
    return UnknwnPtr->Release();
  } else {
    if (refCnt == 0) delete this;
    return refCnt;
  }
}

STDMETHODIMP
VISEISRResNotifySink::BookMark(DWORD dwBookMarkID)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResNotifySink::Paused()
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResNotifySink::PhraseFinish(DWORD flags, QWORD startTime, QWORD endTime, PSRPHRASE srPhrase, LPUNKNOWN results)
{
  VBX_DEBUG(VBX_print("  ResNotifySink::PhraseFinish: StartByte = %ld, EndByte = %ld\n", startTime, endTime));

  ISRResCorrection * ISRResCorrectionPtr;
  VBX_fatalIf(results->QueryInterface(IID_ISRResCorrection, (PVOID *) &ISRResCorrectionPtr));
  VBX_fatalIf(ISRResCorrectionPtr->Validate(SRCORCONFIDENCE_VERY));
  ISRResCorrectionPtr->Release();

  return NOERROR;
}

STDMETHODIMP
VISEISRResNotifySink::PhraseHypothesis(DWORD, QWORD, QWORD, PSRPHRASE, LPUNKNOWN)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResNotifySink::PhraseStart(QWORD qTimeStampBegin)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResNotifySink::ReEvaluate(LPUNKNOWN)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResNotifySink::Training(DWORD dwTrain)
{
  return SRERR_NOTSUPPORTED;
}

STDMETHODIMP
VISEISRResNotifySink::UnArchive(LPUNKNOWN)
{
  return SRERR_NOTSUPPORTED;
}


  // Methods for VISEISRResTranslate

VISEISRResTranslate::VISEISRResTranslate(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEISRResTranslate::~VISEISRResTranslate()
{
}

STDMETHODIMP
VISEISRResTranslate::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEISRResTranslate::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEISRResTranslate::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEISRResTranslate::Translate(DWORD transKind, DWORD rank, PSTR buf, DWORD bufSize, DWORD * sizeNeededPtr)
{
  PVISEResultsObj resultsObj = (PVISEResultsObj) ObjPtr;
  HRESULT         status = SRERR_NOTSUPPORTED;
  Bool            rval = FALSE;

  if (resultsObj->CMHypotheses) {
    if (rank < resultsObj->CMNBest) {
      switch (transKind) {
      case SRRESTRKIND_HOST:
        rval = KB_response(resultsObj->KbPtr->Kb(), KB_HOST_RESPONSE, &resultsObj->CMHypotheses[rank],
                           resultsObj->KbPtr->RecStream()->Stream(),
                           resultsObj->VocabId, resultsObj->GrammarId, buf, bufSize, (UInt *) sizeNeededPtr);
        break;
      case SRRESTRKIND_VOICE:
        rval = KB_response(resultsObj->KbPtr->Kb(), KB_VOICE_RESPONSE, &resultsObj->CMHypotheses[rank],
                           resultsObj->KbPtr->RecStream()->Stream(),
                           resultsObj->VocabId, resultsObj->GrammarId, buf, bufSize, (UInt *) sizeNeededPtr);
        break;
      case SRRESTRKIND_DISPLAY:
        rval = KB_response(resultsObj->KbPtr->Kb(), KB_DISPLAY_RESPONSE, &resultsObj->CMHypotheses[rank],
                           resultsObj->KbPtr->RecStream()->Stream(),
                           resultsObj->VocabId, resultsObj->GrammarId, buf, bufSize, (UInt *) sizeNeededPtr);
        break;
      default:
        status = SRERR_NOTSUPPORTED;
      }
    } else {
      *sizeNeededPtr = 0;
      status = SRERR_VALUEOUTOFRANGE;
    }
  } else {
    *sizeNeededPtr = 0;
    status = SRERR_NOTENOUGHDATA;
  }
  
  if (rval)
    status = SRERR_NONE;
  return status;
}
