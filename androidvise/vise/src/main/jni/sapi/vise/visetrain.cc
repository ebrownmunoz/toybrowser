#include "pthr.h"
#include <stdio.h>
#include <string.h>

// Verbex SAPI Objects
#include "viseengine.hh"
#include "visegram.hh"
#include "visemessage.hh"
#include "visephrase.hh"
#include "viseresults.hh"
#include "visetrain.hh"

// VISE infrastructure
#include "vfile.h"

// VULCAN Inclusions
#include "vocab.h"
#include "syntax.h"
#include "models.h"
#include "hyp.h"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ifaces.h"
#include "mbox.h"
#include "sn.h"

#define DFLT_UTTSPERMODEL 2
#define DFLT_FRMSPERKERNL 3

#ifdef DEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

VISETrainingObj::VISETrainingObj():
RefCnt(0L),
EngineObjPtr(NULL),
CMUttCnt(0),
CMUttsPerModel(DFLT_UTTSPERMODEL),
CMFramesPerKernel(DFLT_FRMSPERKERNL),
CMKbPtr(NULL),
CMGrammarObjPtr(NULL),
CMEnrollWordsPtr(NULL),
ResultsList(NULL),
LastStartSN(0),
VISEIVbxTrainPtr(NULL)
{
  UnknwnPtr = this;

  VBX_DEBUG(VBX_print("  VISETrainingObj: constructing new Training object @%p\n", this));

  // Instantiate the memory allocation interface
  Allocator.Calloc = IMEM_calloc;
  Allocator.Free = IMEM_free;

  // Instantiate the CRITSECT for RefCnt
  CRITSECT_CREATE(RefCntCritSect, &Allocator, NULL);
}

VISETrainingObj::~VISETrainingObj()
{
  delete VISEIVbxTrainPtr;
  delete CMEnrollWordsPtr;

  if (ResultsList) {
    LLIST     llist;
    LPUNKNOWN resultsPtr;
    llist = LLIST_first(ResultsList);
    if (llist == NULL)
      FATAL_BRA_ "VISETrainingObj::~VISETrainingObj: ERROR - unexpected NULL LLIST\n" _KET;

    do {
      resultsPtr = (LPUNKNOWN) LLIST_contents(llist);
      if (resultsPtr) {
        resultsPtr->Release();
      } else {
        VBX_print("VISETrainingObj::~VISETrainingObj: ERROR - unexpected NULL VISEResults\n");
      }
    } while ((llist = LLIST_next(llist)) != NULL);
    LLIST_destroy(ResultsList, NULL, NULL);
  }

  // Destroy the RefCnt CRITSECT
  CRITSECT_DESTROY(RefCntCritSect);
}

STDMETHODIMP
VISETrainingObj::QueryInterface(REFIID riid, LPVOID *ppv)
{
  *ppv = NULL;

  // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown)) {
    *ppv = (LPVOID) this;
  } else if (IsEqualIID(riid, IID_IVbxTrain)) {
    if (!VISEIVbxTrainPtr)
      VISEIVbxTrainPtr = new VISEIVbxTrain(this, UnknwnPtr);
   *ppv = VISEIVbxTrainPtr;
  }

  if (*ppv != NULL) {
    // Update the reference count
    ((LPUNKNOWN) *ppv)->AddRef();
    return NOERROR;
  }
  return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG)
VISETrainingObj::AddRef()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = ++RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISETrainingObj::AddRef(): RefCnt for VISETrainingObj @%p = %d\n", this, refCnt));
  return refCnt;
}

STDMETHODIMP_(ULONG)
VISETrainingObj::Release()
{
  CRITSECT_ENTER(RefCntCritSect);
  ULONG  refCnt = --RefCnt;
  CRITSECT_LEAVE(RefCntCritSect);
  VBX_DEBUG(VBX_print("  VISETrainingObj::Release(): RefCnt for VISETrainingObj @%p = %d\n", this, refCnt));
  if (refCnt == 0) delete this;
  return refCnt;
}

PVISEEngineObj
VISETrainingObj::EngineObject(void)
{
  return EngineObjPtr;
}

PVISEKbObj
VISETrainingObj::Kb(void)
{
  return CMKbPtr;
}

PVISEGrammarObj
VISETrainingObj::Grammar(void)
{
  return CMGrammarObjPtr;
}

Int
VISETrainingObj::UttCnt(void)
{
  return CMUttCnt;
}

Int
VISETrainingObj::UttsPerModel(void)
{
  return CMUttsPerModel;
}

Int
VISETrainingObj::FramesPerKernel(void)
{
  return CMFramesPerKernel;
}

PSAPIPhrase
VISETrainingObj::EnrollWords(void)
{
  return CMEnrollWordsPtr;
}

PVISEResultsObj
VISETrainingObj::FirstResultsObject()
{
  PVISEResultsObj resultsPtr;

  LLIST llist = LLIST_first(ResultsList);
  if (llist == NULL)
    return NULL;
  ResultsList = llist;
  resultsPtr = (PVISEResultsObj) LLIST_contents(llist);
  return resultsPtr;
}

PVISEResultsObj
VISETrainingObj::NextResultsObject()
{
  PVISEResultsObj resultsPtr;

  LLIST llist = LLIST_next(ResultsList);
  if (llist == NULL)
    return NULL;
  ResultsList = llist;
  resultsPtr = (PVISEResultsObj) LLIST_contents(llist);
  return resultsPtr;
}


PVISEResultsObj
VISETrainingObj::RemoveResultsObject()
{
  PVISEResultsObj resultsPtr;

  LLIST prevllist = LLIST_previous(ResultsList);
  LLIST nextllist = LLIST_remove(ResultsList);
  if (ResultsList) {
    if ((resultsPtr = (PVISEResultsObj) LLIST_contents(ResultsList)))
      resultsPtr->Release();
    LLIST_destroy(ResultsList, NULL, NULL);
    CMUttCnt--;
  }
  ResultsList = nextllist ? nextllist : prevllist;
  resultsPtr = nextllist ? (PVISEResultsObj) LLIST_contents(nextllist) : NULL;
  return resultsPtr;
}

  // Methods for VISEIVbxSpkrTrain

VISEIVbxSpkrTrain::VISEIVbxSpkrTrain(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEIVbxSpkrTrain::~VISEIVbxSpkrTrain()
{
}

STDMETHODIMP
VISEIVbxSpkrTrain::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEIVbxSpkrTrain::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEIVbxSpkrTrain::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEIVbxSpkrTrain::GetTrainer(LPUNKNOWN *unknwnTrainerPtrPtr)
{
  PVISETrainingObj VISETrainingObjPtr;
  HRESULT          rval;

  VBX_fatalIf(!(VISETrainingObjPtr = new VISETrainingObj()));

  rval = VISETrainingObjPtr->QueryInterface(IID_IUnknown, (LPVOID *)unknwnTrainerPtrPtr);

  if (FAILED(rval)) {
    VBX_print("VISEIVbxSpkrTrain::GetTrainer: ERROR - QI fails for IID_IUnknown\n");
    delete VISETrainingObjPtr;
   *unknwnTrainerPtrPtr = NULL;
  }

  return rval;
}

  // Methods for VISEIVbxTrain

VISEIVbxTrain::VISEIVbxTrain(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VISEIVbxTrain::~VISEIVbxTrain()
{
}

STDMETHODIMP
VISEIVbxTrain::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VISEIVbxTrain::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VISEIVbxTrain::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VISEIVbxTrain::EnrollWord(PSDATA wordsPtr, PSDATA enrolledPtr, DWORD *countPtr, DWORD *neededPtr)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;
  PSRWORD          srWordPtr;
  PSAPIPhrase      sapiPhrasePtr;

  VBX_DEBUG(VBX_print("VISEIVbxTrain::EnrollWord: starting..\n"));

  if (enrolledPtr != NULL)
    enrolledPtr->pData = NULL;            // Initially pessimistic

  if (wordsPtr != NULL) {
    // return E_INVALIDARG;
    // If reenrollment is needed, this data is the only way to enforce multiple model enrollment,
    // because CMEnrollWordsPtr would contain only one SRWORD structure with SRWORD->dwWordNum == firstWordId.
    // First check if all wordNames here are the same as objPtr->CMEnrollWordsPtr->FirstSRWord()->szWord, and 
    // dwWordNum are unique (for this assume dwWordNum coming in ascending order !!!),
    // then replace objPtr->CMEnrollWordsPtr with new SAPIPhrase built on basis of wordsPtr->pData

    Char * wordName = objPtr->CMEnrollWordsPtr->FirstSRWord()->szWord;
    PSAPIPhrase newEnrollWordsPtr = new SAPIPhrase((PSRPHRASE)wordsPtr->pData);
    if (!newEnrollWordsPtr || newEnrollWordsPtr->NumWords() != 1 ||
        strcmp(wordName, newEnrollWordsPtr->FirstSRWord()->szWord)) {
      delete newEnrollWordsPtr;
      return E_INVALIDARG;
    }
    Int         phraseLength = newEnrollWordsPtr->SRPhraseSize();
    DWORD       bytesRemaining = wordsPtr->dwSize - phraseLength;
    Char      * phrasePtr = (Char *) wordsPtr->pData + phraseLength;
    DWORD       previousWid = newEnrollWordsPtr->FirstSRWord()->dwWordNum;

    while (bytesRemaining) {
      sapiPhrasePtr = new SAPIPhrase((PSRPHRASE) phrasePtr);
      if (!sapiPhrasePtr || sapiPhrasePtr->NumWords() != 1 ||
          sapiPhrasePtr->FirstSRWord()->dwWordNum <= previousWid ||
          strcmp(wordName, sapiPhrasePtr->FirstSRWord()->szWord)) {
        delete sapiPhrasePtr;
        delete newEnrollWordsPtr;
        return E_INVALIDARG;
      }
      previousWid = sapiPhrasePtr->FirstSRWord()->dwWordNum;
      newEnrollWordsPtr->AppendSRWord(sapiPhrasePtr->FirstSRWord());
      phraseLength = sapiPhrasePtr->SRPhraseSize();
      phrasePtr += phraseLength;
      bytesRemaining -= phraseLength;
      delete sapiPhrasePtr;
    }

    // Replace
    delete objPtr->CMEnrollWordsPtr;
    objPtr->CMEnrollWordsPtr = newEnrollWordsPtr;
  }

  if (!objPtr->ResultsList) {
    VBX_print("  VISEIVbxTrain::EnrollWord: DEBUG: NULL ResultsList, giving up!\n");
    return SRERR_NOTENOUGHDATA;
  }

  // Fill in return values
  *countPtr = objPtr->CMUttCnt;
  *neededPtr = objPtr->CMEnrollWordsPtr->NumWords() * objPtr->CMUttsPerModel;

  // We want all utterances to pass initial scrutiny of enrollment. Bad ones will be rejected, which affects data collection.
  if (*countPtr > 1) {
    // Consistency check: make sure that the contained results objects all reference the same word (in text form)
    *countPtr = 1;

    PVISEResultsObj resultsPtr;
    LLIST llist = LLIST_first(objPtr->ResultsList);
    resultsPtr = (PVISEResultsObj) LLIST_contents(llist);

    while ((llist = LLIST_next(llist)) != NULL) {
      resultsPtr = (PVISEResultsObj) LLIST_contents(llist);
      VBX_fatalIf(resultsPtr == NULL);
      sapiPhrasePtr = new SAPIPhrase(resultsPtr->Transcription());
      srWordPtr = sapiPhrasePtr->FirstSRWord();
      VBX_fatalIf(!srWordPtr);
    
      if (strcmp(objPtr->CMEnrollWordsPtr->FirstSRWord()->szWord, srWordPtr->szWord) != 0) {
        VBX_DEBUG(VBX_print("  VISEIVbxTrain::EnrollWord: invalid word in results object\n"));
        delete sapiPhrasePtr;
        return SRERR_NOTENOUGHDATA;
      }
      delete sapiPhrasePtr;
      (*countPtr)++;
    }
  }

  // Have the engine enroll the word
  VBX_DEBUG(VBX_print("VISEIVbxTrain::EnrollWord: CALLING engine EnrollWord..\n"));
  HRESULT  hresult = objPtr->EngineObjPtr->EnrollWord(objPtr);
  VBX_DEBUG(VBX_print("VISEIVbxTrain::EnrollWord: hresult = %x\n", hresult));

  if (hresult == SRERR_NONE) {
    if (enrolledPtr != NULL) {  
    enrolledPtr->pData = (PVOID) VBX_calloc(1, objPtr->CMEnrollWordsPtr->SRPhraseSize());
      memcpy(enrolledPtr->pData, objPtr->CMEnrollWordsPtr->SRPhrase(), objPtr->CMEnrollWordsPtr->SRPhraseSize());
      enrolledPtr->dwSize = objPtr->CMEnrollWordsPtr->SRPhraseSize();
    }
  } else {
    if (hresult == SRERR_NOTENOUGHDATA)
      *countPtr = objPtr->UttCnt();
  }

  return hresult;
}

STDMETHODIMP
VISEIVbxTrain::RetranscribeWord(PSDATA wordsPtr, PSDATA enrolledPtr, DWORD *countPtr, DWORD *neededPtr)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;

  if (enrolledPtr != NULL)
    enrolledPtr->pData = NULL;            // Initially pessimistic
  if (wordsPtr != NULL)
    return E_INVALIDARG;

  if (!objPtr->ResultsList) {
    VBX_print("  VISEIVbxTrain::RetranscribeWord: DEBUG: NULL ResultsList, giving up!\n");
    return SRERR_NOTENOUGHDATA;
  }

  // See if we have enough utterances for enrollment, regardless
  *countPtr = objPtr->CMUttCnt;
  *neededPtr = objPtr->CMEnrollWordsPtr->NumWords() * objPtr->CMUttsPerModel;
  if (*countPtr < *neededPtr) {
    VBX_DEBUG(VBX_print("  VISEIVbxTrain::RetranscribeWord: too few utterances for enrollment (%d), needed %d\n", objPtr->CMUttCnt, *neededPtr));
    return SRERR_NOTENOUGHDATA;
  }

 *countPtr = 1;

  // Consistency check: make sure that the contained results objects all reference the same word (in text form)
  PSRWORD         srWordPtr;
  PSAPIPhrase     sapiPhrasePtr;
  PVISEResultsObj resultsPtr;
  LLIST llist = LLIST_first(objPtr->ResultsList);
  resultsPtr = (PVISEResultsObj) LLIST_contents(llist);

  while ((llist = LLIST_next(llist)) != NULL) {
    resultsPtr = (PVISEResultsObj) LLIST_contents(llist);
    VBX_fatalIf(resultsPtr == NULL);
    sapiPhrasePtr = new SAPIPhrase(resultsPtr->Transcription());
    srWordPtr = sapiPhrasePtr->FirstSRWord();
    VBX_fatalIf(!srWordPtr);
    
    if (strcmp(objPtr->CMEnrollWordsPtr->FirstSRWord()->szWord, srWordPtr->szWord) != 0) {
      VBX_DEBUG(VBX_print("  VISEIVbxTrain::RetranscribeWord: invalid word in results object\n"));
      delete sapiPhrasePtr;
      return SRERR_NOTENOUGHDATA;
    }
    delete sapiPhrasePtr;
    (*countPtr)++;
  }

  // Have the engine enroll the word
  HRESULT  hresult = objPtr->EngineObjPtr->RetranscribeWord(objPtr);

  if (hresult == SRERR_NONE) {
    if (enrolledPtr != NULL) {  
      enrolledPtr->pData = (PVOID) VBX_calloc(1, objPtr->CMEnrollWordsPtr->SRPhraseSize());
      memcpy(enrolledPtr->pData, objPtr->CMEnrollWordsPtr->SRPhrase(), objPtr->CMEnrollWordsPtr->SRPhraseSize());
    }
  } else {
    if (hresult == SRERR_NOTENOUGHDATA)
      *countPtr = objPtr->UttCnt();
  }

  return hresult;
}

STDMETHODIMP
VISEIVbxTrain::SetParameter(DWORD param)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case SRTRAIN_UTTSPERMODEL:
    result = SetParameter(SRTRAIN_UTTSPERMODEL, DFLT_UTTSPERMODEL);
    break;
  case SRTRAIN_FRAMESPERKERNEL:
    result = SetParameter(SRTRAIN_FRAMESPERKERNEL, DFLT_FRMSPERKERNL);
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
 
  return result;
}

STDMETHODIMP
VISEIVbxTrain::SetParameter(DWORD param, INT value)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case SRTRAIN_UTTSPERMODEL:
    objPtr->CMUttsPerModel = value;
    break;
  case SRTRAIN_FRAMESPERKERNEL:
    objPtr->CMFramesPerKernel = value;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
 
  return result;
}

STDMETHODIMP
VISEIVbxTrain::GetParameter(DWORD param, PINT valuePtr)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case SRTRAIN_UTTSPERMODEL:
   *valuePtr = objPtr->CMUttsPerModel;
    break;
  case SRTRAIN_FRAMESPERKERNEL:
   *valuePtr =  objPtr->CMFramesPerKernel;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }

  return result;
}

STDMETHODIMP
VISEIVbxTrain::InsertCorrection(LPUNKNOWN unknwnPtr, PSRPHRASE transPtr, WORD confidence)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;
  PVISEResultsObj  resultsPtr = (PVISEResultsObj) unknwnPtr;
  PSRWORD          currWordPtr, srWordPtr;

  if (!resultsPtr || !transPtr)
    return E_INVALIDARG;

  // The transcription must consist of a SINGLE WORD to be valid
  PSAPIPhrase sapiPhrasePtr = new SAPIPhrase(transPtr);

  if (!sapiPhrasePtr || sapiPhrasePtr->NumWords() != 1) {
    VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: invalid NumWords (%d), giving up!\n", sapiPhrasePtr->NumWords()));
    delete sapiPhrasePtr;
    return E_INVALIDARG;
  } else {
    Char *text = NULL;
    text = sapiPhrasePtr->Text();
    VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: transcription is \"%s\"\n", text));
    VBX_free(text);
  }

  // If this is the first call, just save sapiPhrasePtr (this is done later..)
  // Otherwise, determine whether this insertion is for a different wordId than
  // previous insertions.  If so, add new word to the list of enrollment words
  Bool contains = FALSE;
  Int  i;

  if (objPtr->CMEnrollWordsPtr != NULL) {
    currWordPtr = sapiPhrasePtr->FirstSRWord();

    for (i = 0, srWordPtr = objPtr->CMEnrollWordsPtr->FirstSRWord(); (i < objPtr->CMEnrollWordsPtr->NumWords()) && (srWordPtr != NULL); i++) {
      if (currWordPtr->dwWordNum == srWordPtr->dwWordNum) {
        contains = TRUE;
        break;
      }
      srWordPtr = objPtr->CMEnrollWordsPtr->NextSRWord();
    }

    if (!contains) {
      VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: INFO: appending DIFFERENT word to transcription\n"));
      objPtr->CMEnrollWordsPtr->AppendSRWord(currWordPtr);
    } else {
      VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: INFO: transcription already contains word\n"));
    }
  }

  // Ensure that the Mailbox, Kb, and Grammar match those of the first insertion
  if (objPtr->CMUttCnt == 0) {
    objPtr->EngineObjPtr    = resultsPtr->EngineObject();
    objPtr->CMKbPtr         = resultsPtr->Kb();
    objPtr->CMGrammarObjPtr = resultsPtr->Grammar();
  } else {
    if (objPtr->EngineObjPtr != resultsPtr->EngineObject()) {
      VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: mailbox mismatch, giving up!\n"));
      delete sapiPhrasePtr;
      return E_INVALIDARG;
    } else if (objPtr->CMKbPtr != resultsPtr->Kb()) {
      VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: KB mismatch, giving up!\n"));
      delete sapiPhrasePtr;
      return E_INVALIDARG;
    } else if (objPtr->CMGrammarObjPtr != resultsPtr->Grammar()) {
      VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: Grammar mismatch, giving up!\n"));
      delete sapiPhrasePtr;
      return E_INVALIDARG;
    }
  }

  // Be sure that the recognition hypothesis contains valid start/ending SN's and non-zero numFrames
  SN   firstSN, lastSN;
  UInt numFrames;
  Ptr acousticFeatures = resultsPtr->AcousticFeatures(&firstSN, &lastSN, &numFrames);
  if ((firstSN == 0 && lastSN == 0) || numFrames == 0) {
    VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: Results object has invalid AcousticFeatures, giving up!\n"));
    delete sapiPhrasePtr;
    return E_INVALIDARG;
  }

  // This results object is good enough to insert; make "sapiPhrasePtr" the transcription if this has not already been done
  if (objPtr->CMEnrollWordsPtr == NULL)
    objPtr->CMEnrollWordsPtr = sapiPhrasePtr;
  else
    delete sapiPhrasePtr;

  // Copy the (correct) transcription into the results object
  VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: Results object PASSED first consistency tests\n"));
  resultsPtr->AddRef();
  resultsPtr->Transcription(transPtr);

  // Annihilate the (incorrect) hypotheses
  resultsPtr->SetHypotheses(NULL);

  // Append this results object to VISETrainingObj's linked list
  VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: inserting resultsObj @%p with AF @%p and firstSN = " QuadFmt(-) ", lastSN = " QuadFmt(-) "\n", resultsPtr, acousticFeatures, firstSN, lastSN));
  //!! SORT BASED ON STARTING SN
  PVISEResultsObj prevResultsPtr;
  LLIST llist, nextLlist;
  if ((llist = LLIST_last(objPtr->ResultsList)) == NULL) {
    VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: ResultsList is NULL\n"));
    objPtr->ResultsList = LLIST_create((Ptr) resultsPtr);
  } else {
    SN   firstSN, lastSN, resultsFirstSN;
    UInt frameCnt;

    VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: ResultsList exists, finding insertion location\n"));
    prevResultsPtr = (PVISEResultsObj) LLIST_contents(llist);
    (void) resultsPtr->AcousticFeatures(&resultsFirstSN, &lastSN, &frameCnt);
    (void) prevResultsPtr->AcousticFeatures(&firstSN, &lastSN, &frameCnt);

    if (resultsFirstSN > firstSN) {
      do {
        nextLlist = LLIST_next(llist);
        if (nextLlist == NULL)
          break;
        llist = nextLlist;
        prevResultsPtr = (PVISEResultsObj) LLIST_contents(llist);
        (void) prevResultsPtr->AcousticFeatures(&firstSN, &lastSN, &frameCnt);
      } while (resultsFirstSN > firstSN);

      LLIST_append(llist, (Ptr) resultsPtr);
    } else {
      LLIST_prepend(llist, (Ptr) resultsPtr);
    }
  }

  objPtr->CMUttCnt++;
  VBX_DEBUG(VBX_print("  VISEIVbxTrain::InsertCorrection: complete, UttCnt = %d\n", objPtr->CMUttCnt));  
  return SRERR_NONE;
}

STDMETHODIMP
VISEIVbxTrain::InsertValidation(LPUNKNOWN unknwnPtr, WORD confidence)
{
  PVISETrainingObj objPtr = (PVISETrainingObj) ObjPtr;
  PVISEResultsObj  resultsPtr = (PVISEResultsObj) unknwnPtr;

  if (!resultsPtr)
    return E_INVALIDARG;

  resultsPtr->AddRef();

  // Append this results object to VISETrainingObj's linked list !! FIX !! Must keep sorted!!
  LLIST_append(objPtr->ResultsList, (Ptr) resultsPtr);
  return SRERR_NONE;
}

STDMETHODIMP
VISEIVbxTrain::TrainWords(PSDATA, PSDATA)
{
  return SRERR_NOTSUPPORTED;
}
