#include "pthr.h"
#include <stdio.h>

// Verbex SAPI Objects
#include "visenotify.hh"

// VULCAN Inclusions
#include "vfe.h"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ifaces.h"
#include "critsect.h"
#include "siesta.h"
#include "visecomp.h"
#include "viseerr.h"
#include "visemsg.h"
#include "visecmds.h"

#ifdef DEBUG
#define VBX_DEBUG(P)              P
#define VBX_IF_NOT_DO(C,P)        if (!(C)) (P)
#define VBX_VERBOSE               TRUE
#define VBX_FATALSTATUS(R,S)      { Int v = (R); if (R) FATAL_BRA_ "condition = %d; status = %s", v, VISEERR_string(S) _KET; }
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)        C
#define VBX_VERBOSE               FALSE
#define VBX_FATALSTATUS(R,S)
#endif

#define VUENABLED_DEFAULT         0
#define VUSCALEFACTOR_DEFAULT     512
#define VUOFFSET_DEFAULT          128

// Internal function prototypes
void VISE_notifyThread(PVISENotifyObj);

VISENotifyObj::VISENotifyObj(IMemory * allocator, IMail * mail, ISRNotifySink * notifier, VFE source):
Allocator(allocator),
Mail(mail),
NotifySinkPtr(notifier),
VFeat(source),
NotifyMBox(NULL),
NotifyMsg(NULL),
NotifyThread((pthread_t)NULL),
VUEnabled(VUENABLED_DEFAULT),
VUScaleFactor(VUSCALEFACTOR_DEFAULT),
VUOffset(VUOFFSET_DEFAULT)
{
  CRITSECT_CREATE(CritSect, Allocator, NULL);

   // Create a message port and mailbox for front end VU meter output
  V_Err  vstatus;
  NotifyMBox = Mail->CreateMBox(Mail, "NOT", NUMPRIORITIES, &vstatus);
  VBX_FATALSTATUS(!NotifyMBox, vstatus);
  NotifyMBoxId = Mail->MailBoxId(Mail, "NOT", &vstatus);
  VBX_FATALSTATUS(!NotifyMBoxId, vstatus);
  VBX_fatalIf((NotifyMsg = MSG_create(Allocator, NotifyMBox)) == NULL);

   // Start up the notification thread
  VBX_fatalIf(pthread_create(&NotifyThread, NULL, (pthread_startroutine_t) VISE_notifyThread, (void *) this));
}

VISENotifyObj::~VISENotifyObj()
{
   // Shut down the notification thread
  V_Err   vstatus;
  VBX_fatalIf(!NotifyMsg);
  if (!MSG_openWrite(NotifyMsg, NotifyMBoxId, 0, _ABORT, ABORT_PRIORITY, &vstatus))
    FATAL_BRA_ "VISEObj::~VISEObj: can't send abort to NotifyThread!!!\n" _KET;
  if (!MSG_closeWrite(NotifyMsg, &vstatus))
    FATAL_BRA_ "VISEObj::~VISEObj: can't send abort to NotifyThread!!!\n" _KET;
  void  * status;
  pthread_join(NotifyThread, &status);

   // Destroy the message port and mailbox
  MSG_destroy(NotifyMsg);
  NotifyMBox->Destroy(NotifyMBox);

  CRITSECT_DESTROY(CritSect);
}

STDMETHODIMP
VISENotifyObj::SetParameter(DWORD param)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_VUSCALEMULT:
    result = SetParameter(VISE_VUSCALEMULT, VUSCALEFACTOR_DEFAULT);
    break;
  case VISE_VUOFFSETADD:
    result = SetParameter(VISE_VUOFFSETADD, VUOFFSET_DEFAULT);
    break;
  case VISE_VUONOFFSWCH:
    result = SetParameter(VISE_VUONOFFSWCH, VUENABLED_DEFAULT);
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }

  return result;
}

STDMETHODIMP
VISENotifyObj::SetParameter(DWORD param, INT value)
{
  HRESULT  result = SRERR_NONE;

  CRITSECT_ENTER(CritSect);
  switch (param) {
  case VISE_VUSCALEMULT:
    VUScaleFactor = value;
    break;
  case VISE_VUOFFSETADD:
    VUOffset = value;
    break;
  case VISE_VUONOFFSWCH:
    Int vstatus;
    if (value) {
      if (!VUEnabled && VFE_turnVUMeterOn(VFeat, NotifyMBoxId, &vstatus)) {
        VUEnabled = 1;
      } else if (!VUEnabled) {
        VBX_print("VISENotifyObj::SetParameter: SEVERE ERROR - can't turn VU meter on (status = %d)\n", vstatus);
        result = SRERR_INVALIDMODE;
      }
    } else {
      if (VUEnabled && VFE_turnVUMeterOff(VFeat, &vstatus)) {
        VUEnabled = 0;
      } else if (VUEnabled) {
        VBX_print("VISENotifyObj::SetParameter: SEVERE ERROR - can't turn VU meter off (status = %d)\n", vstatus);
        result = SRERR_INVALIDMODE;
      }
    }
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  CRITSECT_LEAVE(CritSect);

  return result;
}

STDMETHODIMP
VISENotifyObj::GetParameter(DWORD param, PINT pValue)
{
  HRESULT  result = SRERR_NONE;

  switch (param) {
  case VISE_VUSCALEMULT:
    *pValue = VUScaleFactor;
    break;
  case VISE_VUOFFSETADD:
    *pValue = VUOffset;
    break;
  case VISE_VUONOFFSWCH:
    *pValue = VUEnabled;
    break;
  default:
    result = SRERR_INVALIDPARAM;
  }
  return result;
}
    
void
VISE_notifyThread(PVISENotifyObj viseNotifyObj)
{
  viseNotifyObj->NotifyMain();
}

void
VISENotifyObj::NotifyMain()
{
  Bool                    vuStatus;
  V_Uns                   senderId;
  V_ULong                 length;
  V_Int                   command;
  V_Err                   status, cstatus;
  V_Uns                   amplitude;
  Int                     vuLevel;

   // By default the VU meter is off

  while (TRUE) {
    /* Get message, blocking on mailbox */
    MSG_openRead(NotifyMsg, &senderId, &length, &command, TRUE, &status);

    switch (command) {
    case _AMPLITUDE:
      if (!MSG_readUns(NotifyMsg, &amplitude))
        FATAL_BRA_ "VISENotifyObj::NotifyMain: MSG_readUns FAILS for amplitude read\n" _KET;
      MSG_closeRead(NotifyMsg, &status);
      if (NotifySinkPtr != NULL) {
        CRITSECT_ENTER(CritSect);
        vuLevel = amplitude - VUOffset;
        if (vuLevel < 0) vuLevel = 0;
        vuLevel *= VUScaleFactor;
        CRITSECT_LEAVE(CritSect);
        NotifySinkPtr->VUMeter(1, vuLevel);
      }
      break;

    case _ABORT:
      MSG_closeRead(NotifyMsg, &status);
      VFE_turnVUMeterOff(VFeat, &vuStatus);
      pthread_exit(NULL);
      break;
    }
  }
}
