#include "pthr.h"
#include <string.h>

// Verbex SAPI Objects
#include "visekb.hh"

// VULCAN Inclusions
#include "ekb.h"
#include "kb.h"
#include "models.h"
#include "sn.h"
#include "speaker.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "recfile.h"
#include "viseerr.h"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "ukey.h"
#include "file.h"
#include "hash.h"
#include "ifaces.h"
#include "imemory.h"
#include "llist.h"

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#endif

#define SPKRFILEEXT_DEFAULT       "voi"
#define SPKRBACKUPTAG_DEFAULT     '~'
#define SPKRTEMPTAG_DEFAULT       '+'

  // VISE VoiStream (Speaker Profile)

class VVISEVoiStreamObj : public VISEVoiStreamObj {
  public:
    VVISEVoiStreamObj(Char * name, PVISESpkrDirObj dirPtr);
   ~VVISEVoiStreamObj(void);

    Bool                  LoadModels(Char * fileName, EKB ekb);
    void                  UnloadModels(void);
    Bool                  SaveModels(UInt vocabId, UKEY key);

  private:
    Bool                  MapFile(Char * fileName);
    void                  UnMapFile(void);
    FILEMAP               FileMap;
};

typedef VVISEVoiStreamObj * PVVISEVoiStreamObj;

 // Construct a VVISEVoiStreamObj masquerading as a VISEVoiStreamObj
PVISEVoiStreamObj
VISE_createVoiStreamObj(Char * name, PVISESpkrDirObj dirPtr)
{
  return((PVISEVoiStreamObj) new VVISEVoiStreamObj(name, dirPtr));
}

  // VISE Directory Object

class VVISESpkrDirObj: public VISESpkrDirObj {
  public:
    VVISESpkrDirObj(const Char * dirName, const Char * extension, Char backupTag, Char temporaryTag, EKB ekb);
   ~VVISESpkrDirObj(void);

    Ptr                   ReadEntry(Char * name, UInt * dataSizePtr);
    Bool                  WriteEntry(Char * name, Ptr data, UInt dataSize);
    VFILE                 OpenEntryForWrite(Char * name);
    Bool                  CloseEntry(Char * name, VFILE vfile);
    Bool                  ReplaceContents(Char * name);
    Bool                  RemoveContents(Char * name);
    Bool                  RestoreContents(Char * name);
    Bool                  CloneEntry(const Char * sourceName, const Char * cloneName);

  protected:
    Bool                  BackupContents(Char * name);
};

typedef VVISESpkrDirObj * PVVISESpkrDirObj;

 // Construct a VVISESpkrDirObj masquerading as a VISESpkrDirObj
PVISESpkrDirObj
VISE_createSpkrDirObj(Char * dirName, EKB ekb)
{
  return((PVISESpkrDirObj) new VVISESpkrDirObj(dirName, SPKRFILEEXT_DEFAULT, SPKRBACKUPTAG_DEFAULT, SPKRTEMPTAG_DEFAULT, ekb));
}


 // VVISE VoiStream Methods

VVISEVoiStreamObj::VVISEVoiStreamObj(Char * name, PVISESpkrDirObj dirPtr):
VISEVoiStreamObj(name, dirPtr),
FileMap(NULL)
{
}

VVISEVoiStreamObj::~VVISEVoiStreamObj(void)
{
  VBX_DEBUG(VBX_print("  VISEVoiStreamObj::~VISEVoiStreamObj: destroying VoiStream object @%p\n", this));

  Update();

  if (SpkrDirObjPtr) SpkrDirObjPtr->RemoveStream(SpkrName);

  UnloadModels();
}

Bool
VVISEVoiStreamObj::MapFile(Char * fileName)
{
  if (IFilePtr == NULL)
    IFilePtr = RECFILE_create(&Allocator, RECFILE_MEMSTREAM);

   // Map the file
  if ((FileMap = FILEMAP_create(fileName, FILEMAP_READWRITE))) {
    SDATA sdata;
    sdata.pData  = (PVOID) FILEMAP_map(FileMap);
    sdata.dwSize = FILEMAP_length(FileMap);
    if (StreamPtr = VFILE_open(&Allocator, IFilePtr, (void *) &sdata, RECFILE_READWRITE))
      return(TRUE);
    else
      VBX_DEBUG(VBX_print("  VVISEVoiStreamObj::MapFile(): Can't open the stream \"%s\"\n", fileName));
  } else {
    VBX_DEBUG(VBX_print("  VVISEVoiStreamObj::MapFile(): Can't map the file \"%s\"\n", fileName));
  }

   // Unsuccessful returns
  UnMapFile();
  return FALSE;
}

void
VVISEVoiStreamObj::UnMapFile(void)
{
  if (StreamPtr) (void) VFILE_close(StreamPtr);
  StreamPtr = NULL;
  if (FileMap && !FILEMAP_annihilate(FileMap))
    SEVERE_BRA_ "VVISEVoiStreamObj::UnMapFile() Can't destroy filemap" _KET;
  FileMap = NULL;
}

void
VVISEVoiStreamObj::UnloadModels(void)
{
  VBX_DEBUG(VBX_print("  VVISEVoiStreamObj::UnloadModels(): unloading models  @%p for \"%s\"\n", SpkrProfile, SpkrName));
  UKEYREG_reset(&Registry);
  SPKR_destroy(SpkrProfile);
  SpkrProfile = NULL;
  UnMapFile();
}

Bool
VVISEVoiStreamObj::LoadModels(Char * fileName, EKB ekb)
{
  UKEYREG_reset(&Registry);

  if (MapFile(fileName)) {
    VBX_DEBUG(VBX_print("  VVISEVoiStreamObj::LoadModels(): loading VVISEVoiStreamObj @%p from mapped file \"%s\" of %u bytes at %p\n",\
                        this, FILEMAP_name(FileMap), FILEMAP_length(FileMap), FILEMAP_map(FileMap)));
    if ((SpkrProfile = SPKR_create(ekb)) != NULL && SPKR_load(SpkrProfile, StreamPtr, VBX_VERBOSE)) {
      return TRUE;
    } else {
      UnloadModels();
      VBX_DEBUG(VBX_print("  VVISEVoiStreamObj::Load(): Can't create model repository for the stream \"%s\"\n", fileName));
    }
  }

   // Unsuccessful return
  return FALSE;
}

Bool
VVISEVoiStreamObj::SaveModels(UInt vocabId, UKEY key)
{
  Bool   success;
  Char * fileName;

   // If not all the models for this vocabulary are in the stream, create a new voice file and update the stream
  if (MODELS_mustSave(MODSET_models(SpkrProfile, vocabId))) {

    VFILE vfile;

    if (!(success = (vfile = ((PVVISESpkrDirObj) Directory())->OpenEntryForWrite(SpkrName)) != NULL))
      SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't open temporary file for \"%s\"", SpkrName _KET;
    else if (!MODSET_save(SpkrProfile, vfile, VBX_VERBOSE))
      SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't save models for \"%s\" (status = %d)",SpkrName, VISEERR_string(MODSET_status(SpkrProfile)) _KET;
    else if (!((PVVISESpkrDirObj) Directory())->CloseEntry(SpkrName, vfile))
      SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't close temporary file for \"%s\"", SpkrName _KET;
    else {

       // Lock the models set, since it is invalid while remapping the voice file
      MODSET_lock(SpkrProfile);
      UnMapFile();
      if (!(success = SpkrDirObjPtr->ReplaceContents(SpkrName))) {
        SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't replace file for \"%s\"", SpkrName _KET;
      } else if (!(success = (fileName = SpkrDirObjPtr->FilePathName(SpkrName)) != NULL)) {
        SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't create file name for \"%s\"", SpkrName _KET;
      } else if (!(success = MapFile(fileName))) {
        SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't map file for \"%s\"", fileName _KET;
      } else {
        VBX_free(fileName);
        MODSET_setVoiFile(SpkrProfile, StreamPtr);
      }

      UKEYREG_reset(&Registry);
      UKEY_register(&Registry, key);
      MODSET_unlock(SpkrProfile);
    }

  // Otherwise, since all the models for this vocabulary are already in the stream, just synchronize
  } else if (!(success = FILEMAP_synchronize(FileMap, NULL, 0))) {
    SEVERE_BRA_ "VVISEVoiStreamObj::SaveModels can't synchronize file map" _KET;
  }

  VBX_DEBUG(VBX_print("  VVISEVoiStreamObj::SaveModels: Models %ssaved for speaker \"%s\"\n", (success ? "" : "NOT "), SpkrName));

  return success;
}


  // VISE Speaker Directory Methods

VVISESpkrDirObj::VVISESpkrDirObj(const Char * dirName, const Char * extension, Char backupTag, Char temporaryTag, EKB ekb):
VISESpkrDirObj(dirName, extension, backupTag, temporaryTag, ekb)
{
}

VVISESpkrDirObj::~VVISESpkrDirObj(void)
{
}

Ptr
VVISESpkrDirObj::ReadEntry(Char * name, UInt * dataSizePtr)
{
  Ptr data, streamData;

  // If there is an associated stream object, get the data from it
  PVISEVoiStreamObj streamObj;
  if (streamObj = Stream(name)) {
    streamData = VFILE_streamData(streamObj->Stream(), dataSizePtr);
    data = (Ptr) malloc(*dataSizePtr);
    memcpy(data, streamData, *dataSizePtr);

  // Otherwise, get the data from the file
  } else {
    Char * fileName = FilePathName(name);
    IFile *recFile = RECFILE_create(&Allocator, RECFILE_FILESTREAM);
    if (recFile == NULL) FATAL_BRA_ "RECFILE_create returned NULL" _KET;      
    VFILE vFile = VFILE_open(&Allocator, recFile, fileName, RECFILE_READ);
    if (vFile == NULL) FATAL_BRA_ "VFILE_open FAILED for \"%s\"\n", fileName _KET;
    streamData = VFILE_streamData(vFile, dataSizePtr);
    data = (Ptr) malloc(*dataSizePtr);
    memcpy(data, streamData, *dataSizePtr);
    VFILE_close(vFile);
    RECFILE_destroy(recFile);
    VBX_free(fileName);
  }

  return data;
}

Bool
VVISESpkrDirObj::WriteEntry(Char * name, Ptr data, UInt dataSize)
{
  PVISEVoiStreamObj  streamObj = Stream(name);
  Bool               status = FALSE;

  // Write the data into a temporary file, creating the file if necessary
  Char * tempName = TaggedPathName(name, TemporaryTag);
  if (tempName == NULL)
    return status;

  status = FILE_write(tempName, "wb", data, dataSize);
  free(tempName);
  if (!status)
    return(FALSE);

  // If there is an associated stream object, ask it to replace the file
  if (streamObj) {
    streamObj->ReplaceContents = TRUE;
    return TRUE;
  // Otherwise, replace the file here
  } else {
    return ReplaceContents(name);
  }
}

VFILE
VVISESpkrDirObj::OpenEntryForWrite(Char * name)
{
  Char  * tempName;
  IFile * ifile;
  VFILE   vfile = NULL;

  // Open a temporary file
  if (!(tempName = TaggedPathName(name, TemporaryTag)) ||
      !(ifile = RECFILE_create(&Allocator, RECFILE_STDIO)) ||
      !(vfile = VFILE_open(&Allocator, ifile, tempName, RECFILE_WRITE)))
    if (ifile) RECFILE_destroy(ifile);
  VBX_free(tempName);
  return vfile;
}

Bool
VVISESpkrDirObj::CloseEntry(Char * name, VFILE vfile)
{
  IFile             * ifile;
  PVISEVoiStreamObj   streamObj = Stream(name);
  
  if (vfile &&
      (ifile = VFILE_interface(vfile)) &&
      VFILE_SUCCESS == VFILE_close(vfile)) {
    RECFILE_destroy(ifile);
    // If there is an associated stream object, ask it to replace the file
    if (streamObj) {
      streamObj->ReplaceContents = TRUE;
      return TRUE;
    // Otherwise, replace the file here
    } else {
      return ReplaceContents(name);
    }
  } else {
    return FALSE;
  }
}

Bool
VVISESpkrDirObj::ReplaceContents(Char * name)
{
  PVISEVoiStreamObj   streamObj = Stream(name);
  
  // Move the temporary file to the file
  Char * fileName = FilePathName(name);
  Char * tempName = TaggedPathName(name, TemporaryTag);
  Bool   success = fileName && tempName && FILE_rename(tempName, fileName, TRUE);
  VBX_free(tempName);
  VBX_free(fileName);

  if (success && streamObj)
    streamObj->ReplaceContents = FALSE;

  return success;
}

Bool
VVISESpkrDirObj::RemoveContents(Char * name)
{
  // Remove the working file, if any
  Char * fileName = FilePathName(name);
  Bool   success = fileName && FILE_delete(fileName);
  VBX_free(fileName);

  // Remove the backup file, if any
  Char * backupName = TaggedPathName(name, BackupTag);
  if (backupName) (void) FILE_delete(backupName);
  VBX_free(backupName);

  // Remove the temporary file, if any
  Char * tempName = TaggedPathName(name, TemporaryTag);
  if (tempName) (void) FILE_delete(tempName);
  VBX_free(tempName);

  return success;
}

Bool
VVISESpkrDirObj::RestoreContents(Char * name)
{
  // Copy the backup file to the file
  Char * fileName = FilePathName(name);
  Char * backupName = TaggedPathName(name, BackupTag);
  Bool   success = fileName && backupName && FILE_copy(backupName, fileName, TRUE);
  VBX_free(backupName);
  VBX_free(fileName);

  return success;
}

Bool
VVISESpkrDirObj::CloneEntry(const Char * sourceName, const Char * cloneName)
{
  Bool  success;

  Char * fullSourceName = FilePathName(sourceName);
  Char * fullCloneName = FilePathName(cloneName);

  // Check that there is such a source file
  if (!(success = fullSourceName && FILE_exists(fullSourceName))) {
    VBX_DEBUG(VBX_print("  DirObj::CloneEntry: There is no file named \"%s\"\n", fullSourceName));

  // Copy the source file to the clone, but do not overwrite an existing one
  } else if (!(success = fullCloneName && FILE_copy(fullSourceName, fullCloneName, FALSE))) {
    VBX_DEBUG(VBX_print("  DirObj::CloneEntry: Can't make a clone of \"%s\" named \"%s\"\n", fullSourceName, fullCloneName));
  }

  VBX_free(fullCloneName);
  VBX_free(fullSourceName);

  return success;
}

Bool
VVISESpkrDirObj::BackupContents(Char * name)
{
  Bool   result;

  // Make a backup copy of the file, overwriting any existing one
  Char * fileName = FilePathName(name);
  Char * backupName = TaggedPathName(name, BackupTag);
  if (!fileName || !backupName || !FILE_copy(fileName, backupName, TRUE)) {
    VBX_DEBUG(VBX_print("  SpkrDirObj::BackupContents: Can't make a backup named \"%s\"\n", backupName));
    result = FALSE;
  } else {
    result = TRUE;
  }
  VBX_free(backupName);
  VBX_free(fileName);

  return(result);
}
