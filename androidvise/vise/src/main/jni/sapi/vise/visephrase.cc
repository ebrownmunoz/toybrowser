#include "pthr.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>

/* Verbex utilities */
#include "types.h"
#include "vbx.h"
#include "hyp.h"
#include "vocab.h"

#include "visephrase.hh"


VISESAPIPhrase::VISESAPIPhrase(HYP hyp, VOCAB vocab):
SAPIPhrase(NULL)
{
  PSRWORD   wordPtr;
  UInt      wordSize;
  UInt      length;
  UInt      wordCount;
  WRDHYP    word;
  Char    * wordName;

  UInt      srPhraseLen = (UInt) ((PSRPHRASE) NULL)->abWords;
  UInt      srWordLen   = (UInt) ((PSRWORD) NULL)->szWord;

  // VBX_print("  VISESAPIPhrase: srPhraseLen = %d, srWordLen   = %d\n", srPhraseLen, srWordLen); // DEBUG

  // Compute the length of the area needed to store the words
  length = 0;
  for (word = HYP_words(hyp, &wordCount); wordCount--; word++)
    // If the word is in the vocabulary, include its length
    if (wordName = VOCAB_wordNameFromId(vocab, WRDHYP_wid(word))) {
      CMNumWords++;
      wordSize = srWordLen + strlen(wordName) + 1;
      // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
      if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
      length += wordSize;
    }

  // Construct the SRPHRASE only if there are words to put into it
  if (length) {
    // Add in the length of the empty SRPHRASE
    length += srPhraseLen;

    // Allocate memory for the SRPHRASE
    SRPhrasePtr = (PSRPHRASE) VBX_calloc(length, sizeof(Char));
    SRPhrasePtr->dwSize = length;
    // VBX_print("  VISESAPIPhrase: allocated SRPhrasePtr of length %d @ %p for %u words\n", length, SRPhrasePtr, CMNumWords); // DEBUG

    // Put the words into the SRPHRASE
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
    for (word = HYP_words(hyp, &wordCount); wordCount--; word++)
      // If the word is in the vocabulary, append it
      if (wordName = VOCAB_wordNameFromId(vocab, WRDHYP_wid(word))) {
        wordSize = srWordLen + strlen(wordName) + 1;
        // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
        if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
        wordPtr = (PSRWORD) NextSRWordPtr;
        wordPtr->dwSize = wordSize;
        wordPtr->dwWordNum = WRDHYP_wid(word);
        strcpy(wordPtr->szWord, wordName);
        NextSRWordPtr += wordSize;
        length -= wordSize;
      }
    length -= srPhraseLen;

    VBX_fatalIf(length); // DEBUG

    // Initialize the word pointer
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
  }
}


VISESAPIPhrase::VISESAPIPhrase(HYP hyp, KB kb, VFILE recFile, UInt vocabId, UInt grammarId, UInt transKind):
SAPIPhrase(NULL)
{
  PSRWORD   wordPtr;
  UInt      wordSize;
  UInt      length;
  UInt      wordCount;
  WRDHYP    word;
  Char      wordName[256];
  FLOFF   * floffPtr;
  Bool      rval;

  UInt      srPhraseLen = (UInt) ((PSRPHRASE) NULL)->abWords;
  UInt      srWordLen   = (UInt) ((PSRWORD) NULL)->szWord;

  // VBX_print("  VISESAPIPhrase: srPhraseLen = %d, srWordLen   = %d\n", srPhraseLen, srWordLen); // DEBUG

  // Compute the length of the area needed to store the words
  length = 0;
  for (word = HYP_words(hyp, &wordCount); wordCount--; word++) {
    // If the word is in the vocabulary, include its length
    if (floffPtr = KB_translateWordId(kb, transKind, WRDHYP_wid(word), vocabId, grammarId)) {
      rval = VFILE_getText(recFile, floffPtr, (UChar *)wordName);
      if (rval == VFILE_SUCCESS) {
        CMNumWords++;
        wordSize = srWordLen + strlen(wordName) + 1;
        // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
        if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
        length += wordSize;
      } else {
        WARN_BRA_ "Can't find translation text in rec file" _KET;
        continue;
      }
    }
  }

  // Construct the SRPHRASE only if there are words to put into it
  if (length) {
    // Add in the length of the empty SRPHRASE
    length += srPhraseLen;

    // Allocate memory for the SRPHRASE
    SRPhrasePtr = (PSRPHRASE) VBX_calloc(length, sizeof(Char));
    SRPhrasePtr->dwSize = length;
    // VBX_print("  VISESAPIPhrase: allocated SRPhrasePtr of length %d @ %p for %u words\n", length, SRPhrasePtr, CMNumWords); // DEBUG

    // Put the words into the SRPHRASE
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
    for (word = HYP_words(hyp, &wordCount); wordCount--; word++) {
      // If the word is in the vocabulary, append it
      if (floffPtr = KB_translateWordId(kb, transKind, WRDHYP_wid(word), vocabId, grammarId)) {
        rval = VFILE_getText(recFile, floffPtr, (UChar *)wordName);
        if (rval == VFILE_SUCCESS) {
          wordSize = srWordLen + strlen(wordName) + 1;
          // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
          if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
          wordPtr = (PSRWORD) NextSRWordPtr;
          wordPtr->dwSize = wordSize;
          wordPtr->dwWordNum = WRDHYP_wid(word);
          strcpy(wordPtr->szWord, wordName);
          NextSRWordPtr += wordSize;
          length -= wordSize;
        } else {
          WARN_BRA_ "Can't find translation text in rec file" _KET;
          continue;
        }  
      }
    }

    length -= srPhraseLen;

    VBX_fatalIf(length); // DEBUG

    // Initialize the word pointer
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
  }  
}


VISESAPIPhrase::VISESAPIPhrase(PSRPHRASE srPhrase, VOCAB vocab):
SAPIPhrase(srPhrase)
{
  PSRWORD  wordPtr;
  VOCENT   token;

  if (vocab)
    for (wordPtr = FirstSRWord(); wordPtr != NULL; wordPtr = NextSRWord())
      if (wordPtr->dwWordNum == 0 && (token = VOCAB_entryFromName(vocab, wordPtr->szWord)) != NULL)
        wordPtr->dwWordNum = (UInt) VOCENT_firstWordId(token);

  // Re-initialize the word pointer
  NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
}


VISESAPIPhrase::VISESAPIPhrase(PCSTR text, VOCAB vocab):
SAPIPhrase(NULL)
{
  VOCENT  * tokens;
  UInt      wordCount;
  UInt      wordIndex;

  PSRWORD   wordPtr;
  UInt      wordSize;
  UInt      length;
  Char    * wordName;

  UInt      srPhraseLen = (UInt) ((PSRPHRASE) NULL)->abWords;
  UInt      srWordLen   = (UInt) ((PSRWORD) NULL)->szWord;

  // VBX_print("  VISESAPIPhrase: srPhraseLen = %d, srWordLen   = %d\n", srPhraseLen, srWordLen); // DEBUG

  // Tokenize the given phrase
  if ((tokens = VOCAB_tokens(vocab, (Char *) text, &wordCount))) {

    // Compute the size of the SRPHRASE
    length = srPhraseLen;
    for (wordIndex = 0; wordIndex < wordCount; wordIndex++)
      if ((wordName = VOCENT_wordName(tokens[wordIndex]))) {
        CMNumWords++;
        wordSize = srWordLen + strlen(wordName) + 1;
        // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
        if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
        length += wordSize;
      }

    // Allocate memory for the SRPHRASE
    SRPhrasePtr = (PSRPHRASE) VBX_calloc(length, sizeof(Char));
    SRPhrasePtr->dwSize = length;
    // VBX_print("  VISESAPIPhrase: allocated SRPhrasePtr of length %d @ %p for %u words\n", length, SRPhrasePtr, CMNumWords); // DEBUG

    // Put the words into the SRPHRASE
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
    for (wordIndex = 0; wordIndex < wordCount; wordIndex++)
      if ((wordName = VOCENT_wordName(tokens[wordIndex]))) {
        wordSize = srWordLen + strlen(wordName) + 1;
        // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
        if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
        wordPtr = (PSRWORD) NextSRWordPtr;
        wordPtr->dwSize = wordSize;
        wordPtr->dwWordNum = (UInt) VOCENT_firstWordId(tokens[wordIndex]);
        strcpy(wordPtr->szWord, wordName);
        NextSRWordPtr += wordSize;
        length -= wordSize;
      }
    length -= srPhraseLen;

    VBX_free(tokens);
    VBX_fatalIf(length); // DEBUG

    // Initialize the word pointer
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
  }
}


VISESAPIPhrase::VISESAPIPhrase(PCSTR wordName, UInt wordId):
SAPIPhrase(NULL)
{
  if (wordName) {
    CMNumWords = 1;
     // Compute the size of the SRPHRASE
    UInt  srWordLen   = (UInt) ((PSRWORD) NULL)->szWord;
    UInt  wordSize = srWordLen + strlen(wordName) + 1;
     // Pad wordSize so that SRWORDs are 64-bit aligned on the Alpha
    if (wordSize & 0x7) wordSize = (wordSize & ~0x7) + 8;
    UInt  srPhraseLen = (UInt) ((PSRPHRASE) NULL)->abWords;
    UInt  length = srPhraseLen + wordSize;

     // Allocate memory for the SRPHRASE
    SRPhrasePtr = (PSRPHRASE) VBX_calloc(length, sizeof(Char));
    SRPhrasePtr->dwSize = length;

     // Put the word into the SRPHRASE
    PSRWORD  wordPtr = (PSRWORD) SRPhrasePtr->abWords;
    wordPtr->dwSize = wordSize;
    wordPtr->dwWordNum = wordId;
    strcpy(wordPtr->szWord, wordName);
  }

   // Initialize the word pointer
  NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
}


HYP
VISESAPIPhrase::Hypothesis(void)
{
  HYP      hyp = NULL;
  PSRWORD  word;
  UInt     index;

  if ((word = FirstSRWord()) != NULL && (hyp = HYP_create(1)) != NULL && HYP_setWordCount(hyp, CMNumWords)) {
    for (index = 0; word != NULL; word = NextSRWord())
      HYP_setWId(hyp, index++, word->dwWordNum);
  } else {
    HYP_destroy(hyp);
    hyp = NULL;
  }

  return(hyp);
}
