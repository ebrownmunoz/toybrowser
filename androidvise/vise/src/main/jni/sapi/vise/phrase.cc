#include "pthr.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

/* Verbex utilities */
#include "types.h"
#include "vbx.h"

#include "phrase.hh"


SAPIPhrase::SAPIPhrase():
CMNumWords(0),
SRPhrasePtr(NULL),
NextSRWordPtr(NULL)
{
  // Allocate memory for a new, empty SRPHRASE
  SRPhrasePtr = (PSRPHRASE) calloc(1, sizeof(SRPHRASE));
  assert(SRPhrasePtr);

  SRPhrasePtr->dwSize = sizeof(DWORD);
}


SAPIPhrase::SAPIPhrase(PSRPHRASE srPhrasePtr):
CMNumWords(0),
SRPhrasePtr(NULL),
NextSRWordPtr(NULL)
{
  if (srPhrasePtr && srPhrasePtr->dwSize) {
    // Allocate memory for the SRPHRASE, and copy srPhrase into it
    SRPhrasePtr = (PSRPHRASE) calloc(srPhrasePtr->dwSize, sizeof(Char));
    assert(SRPhrasePtr);
    memcpy(SRPhrasePtr, srPhrasePtr, srPhrasePtr->dwSize);

    // Count the SRWORDs in the SRPHRASE
    PSRWORD   wordPtr;
    UInt      byteCount = (UInt) ((PSRPHRASE) NULL)->abWords;
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
    while (byteCount < SRPhrasePtr->dwSize) {
      CMNumWords++;
      wordPtr = (PSRWORD) NextSRWordPtr;
      byteCount += wordPtr->dwSize;
      NextSRWordPtr = ((Ptr)NextSRWordPtr + wordPtr->dwSize);
    }

    // Initialize the word pointer
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
  }
}


SAPIPhrase::~SAPIPhrase()
{
  VBX_free(SRPhrasePtr);
}


PSRPHRASE
SAPIPhrase::SRPhrase()
{
  return SRPhrasePtr;
}


PSRWORD
SAPIPhrase::FirstSRWord()
{
  // Initialize the word pointer
  if (SRPhrasePtr)
    NextSRWordPtr = (Ptr) SRPhrasePtr->abWords;
  else
    NextSRWordPtr = (Ptr) NULL;

  return NextSRWord();
}


PSRWORD
SAPIPhrase::NextSRWord()
{
  PSRWORD  wordPtr;

  if (NextSRWordPtr) {
    wordPtr = (PSRWORD) NextSRWordPtr;
    NextSRWordPtr = ((Ptr)NextSRWordPtr + wordPtr->dwSize);
    if ((Ptr) NextSRWordPtr - (Ptr) SRPhrasePtr >= SRPhrasePtr->dwSize)
      NextSRWordPtr = (Ptr) NULL;
  } else {
    wordPtr = (PSRWORD) NULL;
  }

  return wordPtr;
}


void
SAPIPhrase::AppendSRWord(PSRWORD wordPtr)
{
  if (wordPtr) {
    // Reallocate to accomodate an additional word
    SRPhrasePtr = (PSRPHRASE) VBX_recalloc(SRPhrasePtr, SRPhrasePtr->dwSize + wordPtr->dwSize, sizeof(Char));
    memcpy((Ptr)SRPhrasePtr + SRPhrasePtr->dwSize, wordPtr, wordPtr->dwSize);

    // Update phrase size and word count
    CMNumWords++;
    SRPhrasePtr->dwSize += wordPtr->dwSize;
  }
}


void
SAPIPhrase::AppendWordString(Char *string, Int wordNum)
{
  if (string) {
    // Create an SRWORD representation of the string,
    // then use AppendSRWord to do the job
    Int wordLen = sizeof(DWORD) + sizeof(DWORD) + strlen(string) + 1;
    PSRWORD srWordPtr = (PSRWORD) VBX_calloc(1, wordLen);
    srWordPtr->dwSize = wordLen;
    srWordPtr->dwWordNum = wordNum;
    strcpy(srWordPtr->szWord, string);

    AppendSRWord(srWordPtr);
    VBX_free(srWordPtr);
  }
}

UInt
SAPIPhrase::SRPhraseSize()
{
  if (SRPhrasePtr)
    return SRPhrasePtr->dwSize;
  else
    return 0;
}


UInt
SAPIPhrase::NumWords()
{
  return CMNumWords;
}


Char *
SAPIPhrase::Text()
{
  // Allocate enough space for the text string
  Char  * text = (Char *) VBX_malloc((size_t) (SRPhraseSize() + 1));
  text[0] = '\0';

  // Put the words (if any) into it
  if (SRPhrasePtr) {
    PSRWORD  wordPtr = (PSRWORD) SRPhrasePtr->abWords;
    UInt  wordCount = CMNumWords;
    if (wordCount) {
      strcat(text, wordPtr->szWord);
      while (--wordCount) {
        strcat(text, " ");
        wordPtr = (PSRWORD) (((Ptr) wordPtr) + wordPtr->dwSize);
        strcat(text, wordPtr->szWord);
      }
    }
  }

  return text;
}


void
SAPIPhrase::Print()
{
  PSRWORD   wordPtr;
  UInt      byteCount;
  Ptr       bytePtr;

  VBX_print("  SAPI PHRASE:\n");
  VBX_print("            WORD   WordID   Offset\n");

  if (SRPhrasePtr) {
    bytePtr = (Ptr) &SRPhrasePtr->abWords[0];
    for (byteCount = sizeof(DWORD); byteCount < SRPhrasePtr->dwSize; byteCount++) {
      wordPtr = (PSRWORD) bytePtr;
      if (wordPtr->szWord != NULL) {
        VBX_print("%16s    %4d     %4d\n", wordPtr->szWord, wordPtr->dwWordNum, (Ptr) bytePtr - (Ptr) SRPhrasePtr);
      } else {
        VBX_print("         <null>     %4d     %4d\n", wordPtr->szWord, wordPtr->dwWordNum, (Ptr) bytePtr - (Ptr) SRPhrasePtr);
      }
      byteCount += wordPtr->dwSize;
      bytePtr += wordPtr->dwSize;
    }
  }
}
