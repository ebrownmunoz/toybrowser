#include "pthr.h"
#include <stdio.h>

#include "types.h"
#include "siesta.h"
#include "vbx.h"

// Verbex SAPI Objects
#include "visefactory.hh"
#include "vbxenum.hh"

#ifdef NEVER
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

static Int EnumObjCount = 0;
static Int DllRefCount = 0;

ViseClassFactory::ViseClassFactory():
RefCnt(0),
UnknwnPtr(NULL)
{
}

ViseClassFactory::~ViseClassFactory()
{
}

STDMETHODIMP
ViseClassFactory::QueryInterface(REFIID riid, LPVOID *ppv)
{
 *ppv = NULL;

  // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown) || IsEqualIID(riid, IID_IClassFactory))
    *ppv = (LPVOID) this;

  // Update the reference count here
  if (*ppv != NULL) {
    ((LPUNKNOWN) *ppv)->AddRef();
    return NOERROR;
  }
  return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG)
ViseClassFactory::AddRef()
{
  VBX_DEBUG(VBX_print("  ViseClassFactory::AddRef(): RefCnt for ViseClassFactory @%p = %d\n", this, RefCnt + 1));
  return ++RefCnt;
}

STDMETHODIMP_(ULONG)
ViseClassFactory::Release()
{
  ULONG retval;

  retval = --RefCnt;
  VBX_DEBUG(VBX_print("  ViseClassFactory::Release(): RefCnt for ViseClassFactory @%p = %d\n", this, retval));
  if (RefCnt == 0)
    delete this;
  return retval;
}

STDMETHODIMP
ViseClassFactory::CreateInstance(LPUNKNOWN unknwnPtr, REFIID riid, LPVOID *ppv)
{
  PVbxEnum viseEnumPtr;
  HRESULT  rval;

 *ppv = NULL;
  rval = E_OUTOFMEMORY;

  if (unknwnPtr != NULL && !IsEqualIID(riid, IID_IUnknown)) {
    return CLASS_E_NOAGGREGATION;
  }

  viseEnumPtr = new VbxEnum(unknwnPtr);

  if (viseEnumPtr == NULL)
    return rval;

  rval = viseEnumPtr->QueryInterface(riid, ppv);

  if (FAILED(rval))
    delete viseEnumPtr;
  else
    EnumObjCount++;

  return rval;
}

STDMETHODIMP
ViseClassFactory::LockServer(Bool lock)
{
  if (lock)
    DllRefCount++;
  else
    DllRefCount--;

  return NOERROR;
}

#ifdef WIN32

STDAPI
DllCanUnloadNow(void)
{
  SCODE sc;

  sc = ((EnumObjCount == 0) && (DllRefCount == 0)) ? S_OK : S_FALSE;
  return ResultFromScode(sc);
}

#define PRINT_GUID(_guid) fprintf(fp, "%8.8x-%4.4x-%4.4x-%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x\n", \
                          _guid.Data1, _guid.Data2, _guid.Data3, _guid.Data4[0], _guid.Data4[1],      \
                          _guid.Data4[2], _guid.Data4[3], _guid.Data4[4], _guid.Data4[5],             \
                          _guid.Data4[6], _guid.Data4[7])

STDAPI
DllGetClassObject(REFCLSID rclsid, REFIID riid, PVOID *ppv)
{
  ViseClassFactory  *objPtr;
  HRESULT            rval;

  VBX_DEBUG(FILE *fp = fopen("c:\\com.out", "w"));
  VBX_DEBUG(fprintf(fp, "DllGetClassObject CALLED\n"));
  if (!IsEqualIID(rclsid, CLSID_VerbexVise) &&
      !IsEqualIID(rclsid, CLSID_VerbexFlexVise)) {
    VBX_DEBUG(fprintf(fp, "CLSID match FAILED\n"));
    VBX_DEBUG(fprintf(fp, "rclsid:           "));
    VBX_DEBUG(PRINT_GUID(rclsid));
    VBX_DEBUG(fprintf(fp, "CLSID_VerbexVise: "));
    VBX_DEBUG(PRINT_GUID(CLSID_VerbexVise));
    VBX_DEBUG(fclose(fp));
    return ResultFromScode(E_FAIL);
  }

  objPtr = new ViseClassFactory();

  if (objPtr == NULL)
    return ResultFromScode(E_OUTOFMEMORY);

  rval = objPtr->QueryInterface(riid, ppv);

  if (FAILED(rval)) {
    VBX_DEBUG(fprintf(fp, "QueryInterface FAILED\n"));
    VBX_DEBUG(fclose(fp));
    delete objPtr;
  }

  VBX_DEBUG(fclose(fp));
  return rval;
}

#endif
