/* mmaudin.cc - IAudioIn methods built on an MMAudio source
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 23-Oct-97  Dave Vetter (dave@verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <string.h>

#include "vbxcom.hh"
#include "vbxwave.h"
#include "mmaudin.hh"
#include "mmaudin.h"
#include "siesta.h"
#include "sn.h"
#include "types.h"
#include "vbx.h"
#include "viseerr.h"
#include "cnvrtexc.h"

#ifdef VBXDEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

#define SIGBITS_DEFAULT     12
#define SAMPLESIZE_DEFAULT  2
#define SAMPLERATE_DEFAULT  8000

/* A MMAUDIN descriptor */
typedef struct mmaudin_s {
  LPUNKNOWN      audioUnknown;     /* Micro$oft MMAudio unknown */
  IAudio       * iAudioPtr;        /* Micro$oft MMAudio Interfaces */
  IAudioSource * iAudioSourcePtr;
  UInt           sampleSize;       /* The number of 8-bit chunks in a sample */
  UInt           sampleRate;       /* The sample rate in Hz */
  UInt           byteRate;         /* The byte rate in Hz */
  HRESULT        status;
} mmaudin_t;

void
MMAUDIN_destroy(MMAUDIN mmaudin)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy a MMAUDIN object
 *---------------------------------------------------------------------------*/
{
  if (mmaudin) {
    if (mmaudin->iAudioSourcePtr) mmaudin->iAudioSourcePtr->Release();
    if (mmaudin->iAudioPtr)      mmaudin->iAudioPtr->Release();
    free(mmaudin);
  }
}


MMAUDIN
MMAUDIN_create(LPUNKNOWN audioUnknown)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a MMAUDIN object
 *---------------------------------------------------------------------------*/
{
  MMAUDIN  mmaudin;
  HRESULT  hresult;

  if (!(mmaudin = (MMAUDIN) VBX_calloc(1, sizeof(mmaudin_t))) ||
      !(mmaudin->audioUnknown = audioUnknown) ||
      (audioUnknown->QueryInterface(IID_IAudio, (PVOID *) &mmaudin->iAudioPtr)) ||
      (audioUnknown->QueryInterface(IID_IAudioSource, (PVOID *) &mmaudin->iAudioSourcePtr))) {
    MMAUDIN_destroy(mmaudin);
    mmaudin = NULL;
  } else {
    mmaudin->sampleSize = SAMPLESIZE_DEFAULT;
    mmaudin->sampleRate = SAMPLERATE_DEFAULT;
    mmaudin->byteRate = SAMPLESIZE_DEFAULT * SAMPLERATE_DEFAULT;
  }

  return(mmaudin);
}

V_Bool
MMAUDIN_Init(IAudioIn * This, V_Uns rate, V_Uns size, V_Uns * bits)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn    * audioIn = (MMAudioIn *) This;
  IAudio       * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  MMAUDIN        mmaudin = audioIn->mmaudin;
  WAVEFORMATEX   waveFormat;
  HRESULT        hresult;

  mmaudin->sampleRate = (UInt) rate;
  mmaudin->byteRate = mmaudin->sampleRate * mmaudin->sampleSize;

  // Set sample rate and format
  memset(&waveFormat, 0, sizeof(WAVEFORMATEX));
  waveFormat.wFormatTag = WAVE_FORMAT_PCM;
  waveFormat.nChannels  = (WORD) 1;
  waveFormat.nSamplesPerSec = (DWORD) rate;
  waveFormat.nAvgBytesPerSec = (DWORD) (mmaudin->byteRate);
  waveFormat.nBlockAlign = (WORD) mmaudin->sampleSize;
  waveFormat.wBitsPerSample = (WORD) (8 * mmaudin->sampleSize);
  waveFormat.cbSize = (WORD) 0;

  // VBXCEP13 waveformat has been specified
  if (size == sizeof(Float))
    waveFormat.wFormatTag = WAVE_FORMAT_VBXCEP13;

  SDATA   waveFormatData = { (PVOID) &waveFormat, (DWORD) sizeof(WAVEFORMATEX) };

  VBX_DEBUG(VBX_print("Wave device audio format (input):\n"));
  VBX_DEBUG(VBX_print("  wFormatTag:      %d\n", waveFormat.wFormatTag));
  VBX_DEBUG(VBX_print("  nChannels:       %d\n", waveFormat.nChannels));
  VBX_DEBUG(VBX_print("  nSamplesPerSec:  %d\n", waveFormat.nSamplesPerSec));
  VBX_DEBUG(VBX_print("  nAvgBytesPerSec: %d\n", waveFormat.nAvgBytesPerSec));
  VBX_DEBUG(VBX_print("  nBlockAlign:     %d\n", waveFormat.nBlockAlign));
  VBX_DEBUG(VBX_print("  wBitsPerSample:  %d\n", waveFormat.wBitsPerSample));
  VBX_DEBUG(VBX_print("  cbSize:          %d\n", waveFormat.cbSize));

  // Set the WAVEFORMAT of the audio source
  if (hresult = iAudioPtr->WaveFormatSet(waveFormatData)) {
    VBX_DEBUG(VBX_print("  MMAUDIN_Init: cannot set waveformat (hresult = %8.8x)\n", hresult));
  // Flush the audio source
  } else if (hresult = iAudioPtr->Flush()) {
    VBX_DEBUG(VBX_print("  MMAUDIN_Init: cannot Flush audio source (hresult = %8.8x)\n", hresult));
  } else {
    hresult = SRERR_NONE;
  }

  // Return the default number of significant bits
  *bits = SIGBITS_DEFAULT;

  mmaudin->status = hresult;
  return(!hresult);
}


V_Bool
MMAUDIN_Read(IAudioIn * This, V_Int * samples, V_Uns numSamples)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn    * audioIn = (MMAudioIn *) This;
  IAudioSource * iAudioSourcePtr = audioIn->mmaudin->iAudioSourcePtr;
  MMAUDIN        mmaudin = audioIn->mmaudin;
  HRESULT        hresult;
  DWORD          bytesRequested = (DWORD) (numSamples * mmaudin->sampleSize);
  DWORD          bytesAvailable, value;
  BOOL           eof;

  while (!(hresult = iAudioSourcePtr->DataAvailable(&bytesAvailable, &eof)) &&
         !eof &&
         (bytesAvailable < bytesRequested)) {
    value = 1000000 * (bytesRequested - bytesAvailable) / mmaudin->byteRate;
    VBX_DEBUG(VBX_print("MMAUDIN_Read: hresult %x eof %d available %d requested %d siesta %d\n",
               hresult, eof, bytesAvailable, bytesRequested, value));
    siesta(value);
  }

  hresult = iAudioSourcePtr->DataGet(samples, bytesRequested, &bytesAvailable);

  mmaudin->status = hresult;
  return(!hresult && (bytesAvailable == bytesRequested));
}


V_Bool
MMAUDIN_Start(IAudioIn * This)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;
  IAudio    * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  HRESULT     hresult;

  if ((hresult = iAudioPtr->Start())) {
    VBX_DEBUG(VBX_print("  MMAUDIN_Start: IAudio::Start failed (hresult = %8.8x)\n", hresult));
  } else {
    VBX_DEBUG(SN  currentSN);
    VBX_DEBUG(iAudioPtr->TotalGet((PQWORD) &currentSN));
    VBX_DEBUG(VBX_print("  MMAUDIN_Start: started data acquisition through iAudioPtr @%p at SN %ld\n", iAudioPtr, currentSN));
  }

  audioIn->mmaudin->status = hresult;
  return(!hresult);
}


V_Bool
MMAUDIN_Stop(IAudioIn * This)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;
  IAudio    * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  HRESULT     hresult;

  if ((hresult = iAudioPtr->Stop())) {
    VBX_DEBUG(VBX_print("  MMAUDIN_Stop: IAudio::Stop failed (hresult = %8.8x)\n", hresult));
  } else if ((hresult = iAudioPtr->Flush())) {
    VBX_DEBUG(VBX_print("  MMAUDIN_Stop: IAudio::Flush failed (hresult = %8.8x)\n", hresult));
  } else {
    VBX_DEBUG(SN  currentSN);
    VBX_DEBUG(iAudioPtr->TotalGet((PQWORD) &currentSN));
    VBX_DEBUG(VBX_print("  MMAUDIN_Stop: stopped data acquisition at SN %ld\n", currentSN));
  }

  audioIn->mmaudin->status = hresult;
  return(!hresult);
}


V_Bool
MMAUDIN_LevelSet(IAudioIn * This, V_Uns level)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;
  IAudio    * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  DWORD       dwlevel = level << 16 | level;
  HRESULT     hresult = iAudioPtr->LevelSet(dwlevel);

  audioIn->mmaudin->status = hresult;
  return(!hresult);
}


V_Bool
MMAUDIN_LevelGet(IAudioIn * This, V_Uns * level)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;
  IAudio    * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  DWORD       dwlevel;
  HRESULT     hresult = iAudioPtr->LevelGet(&dwlevel);

  *level = hresult ? 0 : (V_Uns) (dwlevel & 0x00ff);

  audioIn->mmaudin->status = hresult;
  return(!hresult);
}


V_Bool
MMAUDIN_PosnGet(IAudioIn * This, SN * posn)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;
  IAudio    * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  HRESULT     hresult = iAudioPtr->PosnGet((PQWORD) posn);

  audioIn->mmaudin->status = hresult;
  return(!hresult);
}


V_Err
MMAUDIN_Status(IAudioIn * This)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;

  return(HRESULT_TO_VERR(audioIn->mmaudin->status));
}

V_Bool
MMAUDIN_TotalGet(IAudioIn * This, SN * currentSN)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn * audioIn = (MMAudioIn *) This;
  IAudio    * iAudioPtr = audioIn->mmaudin->iAudioPtr;
  HRESULT     hresult = iAudioPtr->TotalGet((PQWORD) currentSN);

  VBX_DEBUG(if (hresult) VBX_print("  MMAUDIN_TotalGet: IAudio::TotalGet failed (hresult = %8.8x)\n", hresult));

  audioIn->mmaudin->status = hresult;

  return(!hresult);
}

V_Bool
MMAUDIN_DataAvailable(IAudioIn * This, V_Long * bytesAvailable, V_Int * eof)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  MMAudioIn    * audioIn = (MMAudioIn *) This;
  IAudioSource * iAudioSourcePtr = audioIn->mmaudin->iAudioSourcePtr;
  MMAUDIN        mmaudin = audioIn->mmaudin;
  HRESULT        hresult;
  BOOL           boolEof;
  DWORD          value, bytesRequested = 320;
  DWORD          bytesAvail;

  while (!(hresult = iAudioSourcePtr->DataAvailable(&bytesAvail, &boolEof)) &&
         !boolEof &&
         (bytesAvail < bytesRequested)) {
    value = 1000000 * (bytesRequested - bytesAvail) / mmaudin->byteRate;
    VBX_DEBUG(VBX_print("MMAUDIN_DataAvailable: hresult %x eof %d available %d requested %d siesta %d\n",
               hresult, eof, bytesAvail, bytesRequested, value));
    siesta(value);
  }
 *bytesAvailable = (V_Long) bytesAvail;
 *eof = (V_Int) boolEof;

  mmaudin->status = hresult;
  return(!hresult);
}
