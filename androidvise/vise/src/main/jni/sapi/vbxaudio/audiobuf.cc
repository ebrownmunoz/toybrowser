#include "pthr.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

// Verbex SAPI Objects
#include "audiobuf.hh"

// Verbex C Infrastructure
#include "types.h"
#include "vbx.h"
#include "class.h"
#include "buff.h"
#include "ifaces.h"
#include "imemory.h"
#include "sn.h"

#ifdef DEBUG
#define VBX_DEBUG(P)
#else
#define VBX_DEBUG(P)
#endif

#define DEBUG_PRINT_PRIO  VBX_LOW_PRIO

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif
#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

AudioBuf::AudioBuf(Int bufSize, Int objtSize):
RefCnt(0),
VBXIAudioSrcPtr(NULL),
VBXIAudioDestPtr(NULL),
Buff(NULL),
Class(NULL),
CurrentObjt(NULL),
Eof(FALSE),
AudioPosn(0),
AudioLen(0),
TotalBytes((Quad) 0),
BufSize(bufSize),
ObjtSize(objtSize),
SampleBytes(sizeof(Short)),
WriteSN(0)
{
  LPUNKNOWN unknwn = (LPUNKNOWN) this;

  // Instantiate the memory allocation interface
  Allocator.Calloc = IMEM_calloc;
  Allocator.Free = IMEM_free;

  VBX_fatalIf(pthread_mutex_init(&Mutex, NULL));
  VBX_fatalIf(ObjtSize <= 0);
  Class = CLASS_create(&Allocator, BufSize + 1, ObjtSize + sizeof(audioframe_t), NULL, NULL);
  Buff = BUF_create(&Allocator, BufSize);

  // Instantiate the contained interfaces
  VBXIAudioSrcPtr = new VBXIAudioSource(this, unknwn);
  VBXIAudioDestPtr = new VBXIAudioDest(this, unknwn);

  VBX_DEBUG(VBX_print("AudioBuf@%p::AudioBuf created\n", this));
}

AudioBuf::~AudioBuf()
{
  VBX_DEBUG(VBX_print("AudioBuf@%p::~AudioBuf destroying\n", this));
  if (CurrentObjt != NULL)
    OBJT_free(CurrentObjt);
  BUF_flush(Buff);
  BUF_destroy(Buff);
  CLASS_destroy(Class, NULL, NULL);
  // Free the contained interfaces
  delete VBXIAudioSrcPtr;
  delete VBXIAudioDestPtr;
  VBX_DEBUG(VBX_print("AudioBuf@%p::~AudioBuf destroyed\n", this));
}

STDMETHODIMP
AudioBuf::QueryInterface(REFIID riid, LPVOID *ppv)
{
 *ppv = NULL;

  // Always return our IUnknown for IID_IUnknown
  if (IsEqualIID(riid, IID_IUnknown))
    *ppv = (LPVOID) this;
  else if (IsEqualIID(riid, IID_IAudioSource))
    *ppv = VBXIAudioSrcPtr;
  else if (IsEqualIID(riid, IID_IAudioDest)) {
    *ppv = VBXIAudioDestPtr;
  }

  // Update the reference count here
  if (*ppv != NULL) {
    ((LPUNKNOWN) *ppv)->AddRef();
    return NOERROR;
  }
  return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG)
AudioBuf::AddRef()
{
  VBX_DEBUG(VBX_print("  AudioBuf@%p::AddRef(): RefCnt = %d\n", this, RefCnt + 1));
  return ++RefCnt;
}

STDMETHODIMP_(ULONG)
AudioBuf::Release()
{
  ULONG retval;

  retval = --RefCnt;
  VBX_DEBUG(VBX_print("  AudioBuf@%p::Release(): RefCnt = %d\n", this, retval));
  if (RefCnt == 0) {
    delete this;
  }
  return retval;
}

OBJT
AudioBuf::ObjtNew()
{
  return OBJT_new(Class);
}  

void
AudioBuf::SetEof(Bool eof)
{
  pthread_mutex_lock(&Mutex);
  Eof = eof;
  pthread_mutex_unlock(&Mutex);
  VBX_DEBUG(VBX_print("  AudioBuf@%p::SetEof %s\n", this, Eof ? "TRUE" : "FALSE"));
}
  
OBJT
AudioBuf::RetrieveObjt()
{
  OBJT objt = NULL;
  SN   sn;

  // Check out the object pointer and ensure it's non-NULL
  if ((objt = BUF_readnextNow(Buff, &sn)) != NULL) {
    pthread_mutex_lock(&Mutex);

    // Update our internal bean counting
    AUDIOFRAME frame = (AUDIOFRAME) &OBJT_contents(objt);
    AudioLen -= frame->byteCnt / SampleBytes;
    pthread_mutex_unlock(&Mutex);
  }

  return(objt);
}

void
AudioBuf::SubmitObjt(OBJT objt)
{
  AUDIOFRAME frame = NULL;

  // Check out the object pointer and ensure it's non-NULL
  if (objt == NULL || (frame = (AUDIOFRAME) &OBJT_contents(objt)) == NULL) {
    VBX_print("AudioBuf@%p::SubmitObjt ERROR - encountered NULL objt/contents\n", this);
    return;
  }

  // If no wraparound, increment sample and byte counts
  if (BUF_writenext(Buff, WriteSN++, objt)) {
    pthread_mutex_lock(&Mutex);

    // Update our internal bean counting
    AudioLen += frame->byteCnt / SampleBytes;
    TotalBytes += frame->byteCnt;
    pthread_mutex_unlock(&Mutex);
    VBX_DEBUG(VBX_print("AudioBuf@%p::SubmitObjt %d bytes\n", this, frame->byteCnt));
  }
}

void
AudioBuf::Flush()
{
  BUF_flush(Buff);
  pthread_mutex_lock(&Mutex);
  AudioPosn = AudioLen = 0;
  if (CurrentObjt != NULL)
    OBJT_free(CurrentObjt);
  CurrentObjt = NULL;
  ObjtSamplesRemaining = 0;
  AudioFrame = NULL;
  AudioPtr = NULL;
  pthread_mutex_unlock(&Mutex);
}

// Methods for VBXIAudioSource interface
// This is the Generic interface provided to the engine

VBXIAudioSource::VBXIAudioSource(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VBXIAudioSource::~VBXIAudioSource()
{
}

STDMETHODIMP
VBXIAudioSource::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VBXIAudioSource::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VBXIAudioSource::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VBXIAudioSource::DataAvailable(DWORD *countPtr, BOOL *eofPtr)
//----------------------------------------------------------------------------/
// DESCRIPTION
// Return (in countPtr) the number of samples this object is currently capable
// of providing
//----------------------------------------------------------------------------/
{  
  AudioBuf *objPtr = (AudioBuf *) ObjPtr;

  if (!countPtr) return AUDERR_INVALIDPARAM;

  pthread_mutex_lock(&objPtr->Mutex);
 *countPtr = (DWORD)(objPtr->AudioLen - objPtr->AudioPosn) * 2;
  if (*countPtr < 0) 
    *countPtr = 0;
  *eofPtr = objPtr->Eof;
  pthread_mutex_unlock(&objPtr->Mutex);

  VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataAvailable: %d bytes available; eof = %d\n", objPtr, *countPtr, *eofPtr));

  return AUDERR_NONE;
}

STDMETHODIMP
VBXIAudioSource::DataGet(PVOID bufPtr, DWORD bytesReq, DWORD *bytesCopied)
//----------------------------------------------------------------------------/
// DESCRIPTION
// Copy the requested samples into the buffer at bufPtr, and set *sampCopied
// to the number of samples copied (which may be less than sampReq).  If 
// less than the requested number are copied, return AUDERR_NOTENOUGHDATA.
//----------------------------------------------------------------------------/
{
  AudioBuf   *objPtr = (AudioBuf *) ObjPtr;
  Char       *outPtr = (Char *) bufPtr;

  Int        sampStillNeeded;
  SN         sn;
  HRESULT    result;
  Int        sampCopied, sampReq = bytesReq / 2;

  if (!outPtr || !bytesCopied) return AUDERR_INVALIDPARAM;
 *bytesCopied = sampCopied = 0;

  // First time (and post flush) initialization
  if (!objPtr->CurrentObjt) {
    objPtr->CurrentObjt = BUF_readnext(objPtr->Buff, &sn);
    objPtr->AudioFrame = (AUDIOFRAME) &OBJT_contents(objPtr->CurrentObjt);
    objPtr->AudioPtr = objPtr->AudioFrame->bytes;
    objPtr->ObjtSamplesRemaining = objPtr->AudioFrame->byteCnt / objPtr->SampleBytes;
  }

  // Limit the copy count to what is actually available
  pthread_mutex_lock(&objPtr->Mutex);
  if (sampReq <= objPtr->AudioLen - objPtr->AudioPosn) {
    result = AUDERR_NONE;
  } else {
    sampReq = (Int) (objPtr->AudioLen - objPtr->AudioPosn);
    result = AUDERR_NOTENOUGHDATA;
  }
  pthread_mutex_unlock(&objPtr->Mutex);

  if (sampReq) {
    VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: %d samples requested\n", objPtr, sampReq));
    pthread_mutex_lock(&objPtr->Mutex);
    VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: mutex acquired\n", objPtr));
    while ((sampStillNeeded = sampReq - sampCopied) > 0) {
      VBX_fatalIf(objPtr->ObjtSamplesRemaining < 0);
      if (sampStillNeeded <= objPtr->ObjtSamplesRemaining) {
  
        // CurrentObjt has sufficient data to provide what is required
        memcpy(outPtr, objPtr->AudioPtr, sampStillNeeded * objPtr->SampleBytes);
        objPtr->ObjtSamplesRemaining -= sampStillNeeded;
        objPtr->AudioPtr += sampStillNeeded * objPtr->SampleBytes;
        objPtr->AudioPosn += sampStillNeeded;
        sampCopied = sampReq;
       *bytesCopied = sampCopied * 2;
      } else {
        // Not enough data in CurrentObjt;  copy what's left, then get the next OBJT
        memcpy(outPtr, objPtr->AudioPtr, objPtr->ObjtSamplesRemaining * objPtr->SampleBytes);
        outPtr += objPtr->ObjtSamplesRemaining * objPtr->SampleBytes;
        sampCopied += objPtr->ObjtSamplesRemaining;
       *bytesCopied += 2 * objPtr->ObjtSamplesRemaining;
        objPtr->AudioPosn += objPtr->ObjtSamplesRemaining;
     
        // Release the AudioBuf mutex to avoid a potential deadlock manipulating the buffer or class
        pthread_mutex_unlock(&objPtr->Mutex);
        VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: mutex temporarily relinquished\n", objPtr));
        OBJT_free(objPtr->CurrentObjt);
        objPtr->CurrentObjt = NULL;
        VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: reading next OBJT from buffer\n", objPtr));
        objPtr->CurrentObjt = BUF_readnext(objPtr->Buff, &sn);
        VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: OBJT with sn = %ld read from audio buffer\n", objPtr, sn));
        // Re-acquire the AudioBuf mutex
        pthread_mutex_lock(&objPtr->Mutex);
        VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: mutex re-acquired\n", objPtr));
        objPtr->AudioFrame = (AUDIOFRAME) &OBJT_contents(objPtr->CurrentObjt);
        objPtr->AudioPtr = objPtr->AudioFrame->bytes;
        objPtr->ObjtSamplesRemaining = objPtr->AudioFrame->byteCnt / objPtr->SampleBytes;
      }
    }
    pthread_mutex_unlock(&objPtr->Mutex);
    VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: mutex relinquished\n", objPtr));
  }

  VBX_DEBUG(VBX_print("  VBXIAudioSource@%p::DataGet: got %d of %d bytes requested; %d samples still available\n",
            objPtr, *bytesCopied, bytesReq, objPtr->AudioLen - objPtr->AudioPosn));

  return result;
}

// Methods for VBXIAudioDest interface
// This is the Generic interface utlized for audio rendering

VBXIAudioDest::VBXIAudioDest(LPVOID objPtr, LPUNKNOWN unknwnPtr):
RefCnt(0),
ObjPtr(objPtr),
UnknwnPtr(unknwnPtr)
{
}

VBXIAudioDest::~VBXIAudioDest()
{
}

STDMETHODIMP
VBXIAudioDest::QueryInterface(REFIID riid, LPVOID *ppv)
{
  return UnknwnPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG)
VBXIAudioDest::AddRef()
{
  ++RefCnt;
  return UnknwnPtr->AddRef();
}

STDMETHODIMP_(ULONG)
VBXIAudioDest::Release()
{
  --RefCnt;
  return UnknwnPtr->Release();
}

STDMETHODIMP
VBXIAudioDest::FreeSpace(DWORD *countPtr, BOOL *eofPtr)
//----------------------------------------------------------------------------/
// DESCRIPTION
// Return (in countPtr) the number of samples this object is currently capable
// of providing.  Here we have to "extend" the API a little.  In a slight
// (but essential!) departure from the M$ spec, we use eofPtr to report whether
// whether the waveOut device has played out all of the pending bytes.
//----------------------------------------------------------------------------/
{  
  AudioBuf *objPtr = (AudioBuf *) ObjPtr;
  if (!countPtr) return AUDERR_INVALIDPARAM;

  pthread_mutex_lock(&objPtr->Mutex);
 *countPtr = (DWORD)(objPtr->ObjtSize * (objPtr->BufSize - 1) - objPtr->AudioLen * objPtr->SampleBytes);
  if (*countPtr < 0) 
    *countPtr = 0;

 *eofPtr = objPtr->Eof;
  pthread_mutex_unlock(&objPtr->Mutex);

  VBX_DEBUG(VBX_print("  VBXIAudioDest@%p::FreeSpace: %d bytes available; eof = %d\n", objPtr, *countPtr, *eofPtr));

  return AUDERR_NONE;
}

STDMETHODIMP
VBXIAudioDest::DataSet(PVOID bufPtr, DWORD byteCnt)
//----------------------------------------------------------------------------/
// DESCRIPTION
// Write the requested samples into the AudioBuf from bufPtr.  Returns
// AUDERR_NOTENOUGHDATA if there is insufficient space in the AudioBuf
// to accomodate the write request.
// NOTE: This seems like a silly error code to use for the failure cases, but
//       Uncle Bill has not provided us with sensible AUDERRs
//----------------------------------------------------------------------------/
{
  AudioBuf  *objPtr = (AudioBuf *) ObjPtr;
  Char      *outPtr = (Char *) bufPtr;
  Int        writeBytesPending = byteCnt;
  Int        freeBytes, writeBytes;
  Int        objtSize;
  Bool       eof;
  OBJT       objt = NULL;
  AUDIOFRAME frame = NULL;
  HRESULT    result;
 
  if (!bufPtr || !byteCnt)
   return AUDERR_INVALIDPARAM;

  // Limit the write to the available free AudioBuf space
  result = FreeSpace((DWORD *)&freeBytes, (BOOL *)&eof);
  if (result) {
    VBX_print("VBXIAudioDest@%p::DataSet: ERROR can't get FreeSpace (result %x)\n", objPtr, result);
    return AUDERR_NOTENOUGHDATA;
  }
  
  if (writeBytesPending > freeBytes) {
    VBX_print("VBXIAudioDest@%p::DataSet: ERROR insufficient FreeSpace for write (%d > %d)\n", objPtr, writeBytesPending, freeBytes);
    return AUDERR_NOTENOUGHDATA;
  }

  objtSize = objPtr->ObjtSize;
  do {
    writeBytes = (writeBytesPending < objtSize) ? writeBytesPending : objtSize;
    objt = objPtr->ObjtNew();
    if (objt == NULL) {
      VBX_print("VBXIAudioDest@%p::DataSet: HOLY COW [ran out of OBJTs in write loop] TILT!\n", objPtr);
      return AUDERR_NOTENOUGHDATA;
    }

    frame = (AUDIOFRAME) &OBJT_contents(objt);
    frame->byteCnt = writeBytes;
    memcpy(frame->bytes, outPtr, writeBytes);
    writeBytesPending -= writeBytes;
    outPtr += writeBytes;
    objPtr->SubmitObjt(objt);
    objPtr->SetEof(FALSE);
  } while (writeBytesPending > 0);

  return AUDERR_NONE;
}

STDMETHODIMP
VBXIAudioDest::BookMark(DWORD bookMark)
//----------------------------------------------------------------------------/
// DESCRIPTION
// This is not a book, therefore we do not need a "book mark"
//----------------------------------------------------------------------------/
{
  return AUDERR_NOTSUPPORTED;
}
