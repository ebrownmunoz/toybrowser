#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <android/log.h>
#include <pthread.h>
#include <time.h>
#include <atomic>
#include "SLES/OpenSLES.h"
#include "SLES/OpenSLES_Android.h"
#include "android/androidaudiosrc.hh"
#include "vbx.h"


#define MODULENAME "AndroidAudio"

// 1 sample of 16-bit audio!
#define MIN_BUFFER_SIZE 2
#define INPUT_LEFT_CHANNEL 0
#define INPUT_RIGHT_CHANNEL 1

#define INPUT_CHANNEL INPUT_LEFT_CHANNEL

#ifdef LOG_AUDIO
#define AUDIO_FILE "/storage/sdcard1/Download/androidaudio.raw"
#endif

static int eraseFile = 1;
/**
 * Callback function thunk for record events
 */
void RecordEventCallback(SLRecordItf recorder, void *pContext, SLuint32 recordEvent) {
	((AndroidAudioSrc*)pContext)->recordEventCallback(recordEvent);
}

/**
 * Callback function thunk for buffer full event
 */
void BufferQueueCallback(SLAndroidSimpleBufferQueueItf bufferQueue, void *pContext) {
    ((AndroidAudioSrc*)pContext)->bufferQueueCallback();
}

/*
void * allocateWithWriteBarrier(size_t bufferSize) {
	sysconf(_SC_PAGE_SIZE);
}
*/

AndroidIAudio::AndroidIAudio(AndroidAudioSrc *_objPtr, LPUNKNOWN _unknownPtr):
	refCnt(0),
	objPtr(_objPtr),
	unknownPtr(_unknownPtr) {
}

AndroidIAudio::~AndroidIAudio() {
}

STDMETHODIMP AndroidIAudio::QueryInterface(REFIID riid, LPVOID *ppv) {
	return unknownPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG) AndroidIAudio::AddRef() {
	++refCnt;
	return unknownPtr->AddRef();
}

STDMETHODIMP_(ULONG) AndroidIAudio::Release() {
	--refCnt;
	return unknownPtr->Release();
}

STDMETHODIMP AndroidIAudio::Flush() {
    VBX_print("[DEBUG] AndroidIAudio::Flush(): called");

    if (objPtr->deviceOpen) {
        objPtr->Close();
        objPtr->Open((WAVEFORMATEX *)objPtr->waveFormatEx);
        return AUDERR_NONE;
    }

    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudio::GetGain(FLOAT *gain) {
    if (nullptr == gain) {
        return AUDERR_INVALIDPARAM;
    }
    return AUDERR_NOTSUPPORTED;
};

STDMETHODIMP AndroidIAudio::SetGain(FLOAT) {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::Reset() {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::Ioctl(DWORD cmd, PVOID arg) {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::LevelGet(DWORD *level) {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::LevelSet(DWORD level) {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::PassNotify(PVOID notifySink, IID iid) {
    if (nullptr != notifySink) {
        if (!IsEqualIID(iid, IID_IAudioSourceNotifySink) ) {
            return AUDERR_INVALIDNOTIFYSINK;
        }
        ((IAudioSourceNotifySink *)notifySink)->AddRef();
    }

    if (objPtr->notifySink) {
        IAudioSourceNotifySink * old = objPtr->notifySink;
        objPtr->notifySink = (IAudioSourceNotifySink*)notifySink;
        old->Release();
    }
    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudio::PosnGet(PQWORD pos) {
    DWORD       count = 0;

   // If device is not configured
    if (!objPtr->deviceOpen) {
        return AUDERR_NEEDWAVEFORMAT;
    }

    pthread_mutex_lock(&objPtr->bufferMutex);
    // we don't want to use a recursive mutex, so just replicate the counting code here
    // objPtr->androidIAudioSourcePtr->DataAvailable(&count, nullptr);
    for (int i = 0; i < objPtr->numberOfBuffers; i++) {
        if (!objPtr->buffers[i]->bufferSubmitted) {
            count += objPtr->buffers[i]->bufferSize - objPtr->buffers[i]->bytesRead;
        }
    }
    *pos = objPtr->totalBytesSent + count;
    pthread_mutex_lock(&objPtr->bufferMutex);
    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudio::Claim() {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::UnClaim() {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::Start() {
    SLresult result;
    VBX_print("[INFO] AndroidIAudio::Start(): called");
    if (!objPtr->deviceOpen) {
        VBX_print("[ERROR] AndroidIAudio::Start(): device was not open");
        return AUDERR_NEEDWAVEFORMAT;
    }

    result = (*objPtr->recorder)->SetRecordState(objPtr->recorder, SL_RECORDSTATE_RECORDING);
    if (SL_RESULT_SUCCESS != result) {
        VBX_print("[ERROR] AndroidIAudio::Start(): Failed to set record state to RECORDING: %d", result);
        return FALSE;
    }
    if (objPtr->notifySink) {
        objPtr->notifySink->AudioStart();
    }

    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudio::Stop() {
    SLresult result;
    VBX_print("[INFO] AndroidIAudio::Stop(): called, %lld bytes returned", objPtr->totalBytesSent);
    if (!objPtr->deviceOpen) {
        VBX_print("AndroidIAudio::Stop(): device was not open");
        return AUDERR_NEEDWAVEFORMAT;
    }
    result = (*objPtr->recorder)->SetRecordState(objPtr->recorder, SL_RECORDSTATE_STOPPED);
    if (SL_RESULT_SUCCESS != result) {
        VBX_print("[ERROR] AndroidIAudio::Stop(): Failed to set record state to STOPPED: %d", result);
        return FALSE;
    }
    VBX_print("[DEBUG] AndroidIAudio::Stop(): SetRecordState() returned");
    if (objPtr->notifySink) {
        objPtr->notifySink->AudioStop(IANSRSN_INACTIVE);
    }

    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudio::TotalGet(PQWORD quadPtr) {
    if (!objPtr->deviceOpen) {
    	return AUDERR_NEEDWAVEFORMAT;
    }
	*quadPtr = objPtr->totalBytesSent;
    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudio::ToFileTime(PQWORD quadPtr, FILETIME *ft) {
    return AUDERR_NOTSUPPORTED;
}

STDMETHODIMP AndroidIAudio::WaveFormatGet(PSDATA sdata) {
    if (nullptr == sdata) {
        return AUDERR_INVALIDPARAM;
    }
    if (objPtr->waveFormatEx && objPtr->waveFormatExLen) {
        sdata->pData = malloc(objPtr->waveFormatExLen);
        memcpy(sdata->pData, objPtr->waveFormatEx, objPtr->waveFormatExLen);
        sdata->dwSize = objPtr->waveFormatExLen;
        return AUDERR_NONE;
    } else {
        return AUDERR_NEEDWAVEFORMAT;
    }
}

STDMETHODIMP AndroidIAudio::WaveFormatSet(SDATA sdata) {
    if (objPtr->deviceOpen) {
        objPtr->Close();
    }
    if (!objPtr->Open((WAVEFORMATEX *)(sdata.pData))) {
        return E_FAIL;
    } else {
        // copy the WAVEFORMATEX structure
        if (objPtr->waveFormatEx) {
            free(objPtr->waveFormatEx);
        }
        objPtr->waveFormatExLen = sdata.dwSize;
        objPtr->waveFormatEx = malloc((size_t)sdata.dwSize);
        memcpy(objPtr->waveFormatEx, sdata.pData, sdata.dwSize);
        return AUDERR_NONE;
    }
}
/**********************************************************************************************
 * AndroidIAudioSource class implementation
 */
AndroidIAudioSource::AndroidIAudioSource(AndroidAudioSrc *_objPtr, LPUNKNOWN _unknownPtr) :
    refCnt(0),
    objPtr(_objPtr),
    unknownPtr(_unknownPtr) {
}

AndroidIAudioSource::~AndroidIAudioSource() {

}

STDMETHODIMP AndroidIAudioSource::QueryInterface(REFIID riid, LPVOID *ppv) {
    return unknownPtr->QueryInterface(riid, ppv);
}

STDMETHODIMP_(ULONG) AndroidIAudioSource::AddRef() {
    refCnt++;
    return unknownPtr->AddRef();
}

STDMETHODIMP_(ULONG) AndroidIAudioSource::Release() {
    refCnt--;
    return unknownPtr->Release();
}

STDMETHODIMP AndroidIAudioSource::DataAvailable(DWORD *count, BOOL *eof) {
    if (*eof) {
    	*eof = FALSE;
    }
	if (count) {
        (*count) = 0;
        pthread_mutex_lock(&objPtr->bufferMutex);
        for (int i = 0; i < objPtr->numberOfBuffers; i++) {
            if (!objPtr->buffers[i]->bufferSubmitted) {
                (*count) += objPtr->buffers[i]->bufferSize - objPtr->buffers[i]->bytesRead;
            }
        }
        pthread_mutex_unlock(&objPtr->bufferMutex);
        //__android_log_print(ANDROID_LOG_DEBUG, "AndroidIAudio", "DataAvailable: %d bytes", *count);
    }

    return AUDERR_NONE;
}

STDMETHODIMP AndroidIAudioSource::DataGet(PVOID buffer, DWORD bytesRequested, DWORD *bytesCopied) {
    if (!buffer || bytesRequested < 0 || !bytesCopied) {
        return AUDERR_INVALIDPARAM;
    }

    *bytesCopied = 0;
    if (0 == bytesRequested) {
        return AUDERR_NONE;
    }

    pthread_mutex_lock(&objPtr->bufferMutex);

    while (*bytesCopied < bytesRequested) {
        // the buffers are in accessed in a circular method, so mod the index by the total number of buffers
        AudioBuffer *curr = objPtr->buffers[objPtr->nextReadBuffer % objPtr->numberOfBuffers];
        if (!curr->bufferSubmitted && (curr->bytesRead < curr->bufferSize)) {
            int bytesRemaining = curr->bufferSize - curr->bytesRead;
            int bytesToCopy = 0;
            if (((*bytesCopied) + bytesRemaining) < bytesRequested) {
                // we don't have enough in the current buffer, copy all the remaining bytes
                bytesToCopy = bytesRemaining;
            } else {
                // we have enough, copy just the needed amount
                bytesToCopy = bytesRequested - (*bytesCopied);
            }
            memcpy((int8_t*)buffer + *bytesCopied, ((int8_t*)curr->bytes + curr->bytesRead), bytesToCopy);
            curr->bytesRead += bytesToCopy;
            (*bytesCopied) += bytesToCopy;
            if (curr->bytesRead == curr->bufferSize) {
                // we've exhausted the current buffer, advance to the next buffer
                objPtr->nextReadBuffer++;
            }
        } else {
            // we've exhausted all buffers, there's just nothing left to read right now
            objPtr->totalBytesSent += (*bytesCopied);
            pthread_mutex_unlock(&objPtr->bufferMutex);
            return AUDERR_NOTENOUGHDATA;
        }
    }
    objPtr->totalBytesSent += (*bytesCopied);
    pthread_mutex_unlock(&objPtr->bufferMutex);
    // we don't bother doing any submitting of buffers to the BufferQueue like in MMAudioSrc
    // there's a buffer callback that will do that for us
    return AUDERR_NONE;
}

/**********************************************************************************************
 * AndroidAudioSrc class implementation
 */
AndroidAudioSrc::AndroidAudioSrc(LPUNKNOWN _unknownPtr, DESTROYFCN _destroyFcn):
	refCnt(0),
	unknownPtr(_unknownPtr),
	destroyFcn(_destroyFcn),
	notifySink(nullptr),
	deviceOpen(FALSE),
	waveFormatEx(nullptr),
    waveFormatExLen(0),
	numInputs(MAX_NUMBER_INPUT_DEVICES),
	sl(nullptr),
	engine(nullptr),
//	devCaps(nullptr),
	recorderObject(nullptr),
	recorder(nullptr),
	audioSource({nullptr, nullptr}),
	audioInputDeviceLocator({SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT, SL_DEFAULTDEVICEID_AUDIOINPUT, nullptr}),
	audioSink({&audioInputBuffersLocator,&audioInputFormat}),
	buffers(nullptr),
	bufferSize(0),
	numberOfBuffers(0),
    totalBytesSent((int64_t) 0) {

	LPUNKNOWN unknwn = (nullptr == unknownPtr) ? (LPUNKNOWN) this : unknownPtr;
	pthread_mutex_init(&bufferMutex, nullptr);
	androidIAudioPtr = new AndroidIAudio(this, unknwn);
  	androidIAudioSourcePtr = new AndroidIAudioSource(this, unknwn);
};

AndroidAudioSrc::~AndroidAudioSrc() {
    delete androidIAudioSourcePtr;
    delete androidIAudioPtr;
    pthread_mutex_destroy(&bufferMutex);
};

STDMETHODIMP AndroidAudioSrc::QueryInterface(REFIID riid, LPVOID *ppv) {
    *ppv = nullptr;
    if (IsEqualIID(riid, IID_IUnknown)) {
        *ppv = this;
        this->AddRef();
    } else if (IsEqualIID(riid, IID_IAudioSource)) {
        *ppv = androidIAudioSourcePtr;
        androidIAudioSourcePtr->AddRef();
    } else if (IsEqualIID(riid, IID_IAudio)) {
        *ppv = androidIAudioPtr;
        androidIAudioPtr->AddRef();
    } else {
        return E_NOINTERFACE;
    }
    return AUDERR_NONE;
}

STDMETHODIMP_(ULONG) AndroidAudioSrc::AddRef() {
    refCnt++;
}

STDMETHODIMP_(ULONG) AndroidAudioSrc::Release() {
    refCnt--;
}

Bool AndroidAudioSrc::Initialize(Int objtSize, Int bufSize) {
	SLEngineOption engineOptions[] = {
			(SLuint32) SL_ENGINEOPTION_THREADSAFE,
			(SLuint32) SL_BOOLEAN_TRUE
	};

	SLuint32 inputDeviceIds;
	SLint32 numInputs = MAX_NUMBER_INPUT_DEVICES;

	SLresult result;

	if (objtSize < MIN_BUFFER_SIZE || bufSize < 1) {
		VBX_print("androidaudio.cpp: Invalid arguments to Initialize: (%d, %d)", objtSize, bufSize);
		return FALSE;
	}

	bufferSize = objtSize;
	numberOfBuffers = bufSize;
	buffers = (AudioBuffer**)calloc(bufSize, sizeof(AudioBuffer*));

	if (nullptr == buffers) {
	    VBX_print("androidaudio.cpp: buffers allocation failed: %d", errno);
		return FALSE;
	}

	for (int i = 0; i < numberOfBuffers; i++) {
		buffers[i] = (AudioBuffer*)malloc(sizeof(AudioBuffer));
		if (nullptr == buffers[i]) {
		    VBX_print("androidaudio.cpp: buffer record allocation (%d/%d) failed: %d", i, numberOfBuffers, errno);
			for (int j = 0; j < i; j++) {
			    free(buffers[j]->bytes);
				free(buffers[j]);
			}
			free(buffers);
			return FALSE;
		} else {
		    buffers[i]->bytes = (void*)malloc(bufferSize);
		    if (nullptr == buffers[i]->bytes) {
                VBX_print("androidaudio.cpp: buffer allocation (%d/%d) failed: %d", i, numberOfBuffers, errno);
                for (int j = 0; j < i; j++) {
                    free(buffers[j]->bytes);
                    free(buffers[j]);
                }
                free(buffers);
                return FALSE;
            } else {
                buffers[i]->bufferSize = bufferSize;
                buffers[i]->bufferSubmitted = FALSE;
                buffers[i]->bytesRead = bufferSize;
            }
        }
	}

	result = slCreateEngine(&sl, 1, engineOptions, 0, nullptr, nullptr);
	if (SL_RESULT_SUCCESS != result) {
	    VBX_print("androidaudio.cpp: Failed to slCreateEngine: %d", result);
		return FALSE;
	}

	result = (*sl)->Realize(sl, SL_BOOLEAN_FALSE);
	if (SL_RESULT_SUCCESS != result) {
	    VBX_print("androidaudio.cpp: Failed to Realize engine: %d", result);
		return FALSE;
	}

	result = (*sl)->GetInterface(sl, SL_IID_ENGINE, (void*)&engine);
	if (SL_RESULT_SUCCESS != result) {
	    VBX_print("androidaudio.cpp: Failed to get ENGINE interface: %d", result);
		return FALSE;
	}

	// we might need this to query the capabilities of an audio input device
//	result = (*sl)->GetInterface(sl, SL_IID_AUDIOIODEVICECAPABILITIES, (void*)&devCaps);
//	if (SL_RESULT_SUCCESS != result) {
//	    VBX_print("androidaudio.cpp: Failed to get AUDIOIODEVICECAPABILITIES interface: %d", result);
//		return FALSE;
//	}
    return TRUE;
}

Bool AndroidAudioSrc::Open(WAVEFORMATEX * waveFormat) {
	const SLInterfaceID additionalInterfaces[3] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE, SL_IID_ANDROIDCONFIGURATION};
    const SLboolean requiredInterfaces[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    SLresult result;

    VBX_print("androidaudio.cpp: Opening audio device...");
    if (nullptr == waveFormat) {
        VBX_print("androidaudio.cpp: waveFormat was nullptr");
        return FALSE;
    }
    if (deviceOpen) {
    	VBX_print("androidaudio.cpp: Device already open, closing...");
        this->Close();
    }

	// set up the audio source
	audioInputDeviceLocator.locatorType = SL_DATALOCATOR_IODEVICE;
	audioInputDeviceLocator.deviceType = SL_IODEVICE_AUDIOINPUT;
	audioInputDeviceLocator.deviceID = SL_DEFAULTDEVICEID_AUDIOINPUT;
	audioInputDeviceLocator.device = nullptr;
	audioSource.pLocator = (void*)&audioInputDeviceLocator;
	audioSource.pFormat = nullptr;

	// set up the audio sink
	audioInputBuffersLocator.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
	audioInputBuffersLocator.numBuffers = numberOfBuffers;
	audioInputFormat.formatType = SL_DATAFORMAT_PCM;
	audioInputFormat.numChannels = waveFormat->nChannels;
	switch (waveFormat->nSamplesPerSec) {
		case 8000:
			audioInputFormat.samplesPerSec = SL_SAMPLINGRATE_8;
			break;
		case 11025:
			audioInputFormat.samplesPerSec = SL_SAMPLINGRATE_11_025;
			break;
		case 16000:
			audioInputFormat.samplesPerSec = SL_SAMPLINGRATE_16;
			break;
		default:
    	    VBX_print("androidaudio.cpp: Unsupported sampling rate: %d", waveFormat->nSamplesPerSec);
    		return FALSE;
	}
	switch (waveFormat->wBitsPerSample) {
		case 8:
			audioInputFormat.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_8;
			audioInputFormat.containerSize = SL_PCMSAMPLEFORMAT_FIXED_8;
			break;
		case 16:
			audioInputFormat.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
			audioInputFormat.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
			break;
		default:
			return FALSE;
	}

	audioInputFormat.channelMask = SL_SPEAKER_FRONT_LEFT;
	audioInputFormat.endianness = SL_BYTEORDER_LITTLEENDIAN;
	if ((audioInputFormat.numChannels != 1) || (audioInputFormat.samplesPerSec != SL_SAMPLINGRATE_11_025) || (audioInputFormat.bitsPerSample != SL_PCMSAMPLEFORMAT_FIXED_16)) {
		VBX_print("androidaudio.cpp: channel is %d", audioInputFormat.numChannels);
		VBX_print("androidaudio.cpp: sampling rate is %d", audioInputFormat.samplesPerSec);
		VBX_print("androidaudio.cpp: sample size is %d", audioInputFormat.bitsPerSample);
	}
	audioSink.pLocator = (void*)&audioInputBuffersLocator;
	audioSink.pFormat = (void*)&audioInputFormat;

	// get the recorder object, but we need the ANDROIDSIMPLEBUFFERQUEUE interface also
	result = (*engine)->CreateAudioRecorder(engine, &recorderObject, &audioSource, &audioSink, 2, additionalInterfaces, requiredInterfaces);
	if (SL_RESULT_SUCCESS != result) {
	    VBX_print("androidaudio.cpp: Failed to create audio recorder: %d", result);
		return FALSE;
	}

    result = (*recorderObject)->GetInterface(recorderObject, SL_IID_ANDROIDCONFIGURATION, (void*)&androidConfiguration);
    if (SL_RESULT_SUCCESS != result) {
        VBX_print("androidaudio.cpp: Failed to acquire AndroidConfiguration interface: %d", result);
        return FALSE;
    }

#ifdef USE_SL_ANDROID_RECORDING_PRESET_VOICE_RECOGNITION
    SLint32 recordingPreset = SL_ANDROID_RECORDING_PRESET_VOICE_RECOGNITION;
    result = (*androidConfiguration)->SetConfiguration(androidConfiguration, SL_ANDROID_KEY_RECORDING_PRESET, &recordingPreset, sizeof(SLint32));
    if (SL_RESULT_SUCCESS != result) {
        VBX_print("androidaudio.cpp: Failed to set recorder configuration to VOICE_RECOGNITION: %d", result);
        return FALSE;
    }
#endif

	result = (*recorderObject)->Realize(recorderObject, SL_BOOLEAN_FALSE);
	if (SL_RESULT_SUCCESS != result) {
	    VBX_print("androidaudio.cpp: Failed to realize audio recorder: %d", result);
		return FALSE;
	}

	result = (*recorderObject)->GetInterface(recorderObject, SL_IID_RECORD, (void*)&recorder);
	if (SL_RESULT_SUCCESS != result) {
		VBX_print("androidaudio.cpp: Failed to get the recorder interface: %d", result);
		return FALSE;
	}

    // just in case it doesn't come stopped

    SLuint32 state = SL_RECORDSTATE_STOPPED;
    result = (*recorder)->GetRecordState(recorder, &state);
    if (SL_RESULT_SUCCESS != result) {
        VBX_print("androidaudio.cpp: Failed to get the record state of the recorder");
        return FALSE;
    }
    if (SL_RECORDSTATE_STOPPED != state) {
        VBX_print("androidaudio.cpp: Recorder state is not stopped, but is %d", state);
        result = (*recorder)->SetRecordState(recorder, SL_RECORDSTATE_STOPPED);
        if (SL_RESULT_SUCCESS != result) {
            VBX_print("androidaudio.cpp: Failed to stop the recorder");
            return FALSE;
        }
    }

	result = (*recorderObject)->GetInterface(recorderObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, (void*)&bufferQueue);
	if (SL_RESULT_SUCCESS != result) {
		VBX_print("androidaudio.cpp: Failed to get the simple buffer queue interface");
		return FALSE;
	}

    submittedBuffers.store(0);

    for (int i = 0; i < numberOfBuffers; i++) {
    	// scrub the buffers just in case
    	memset(buffers[i]->bytes, 0, buffers[i]->bufferSize);
    	buffers[i]->bytesRead = 0;
	    (*bufferQueue)->Enqueue(bufferQueue, buffers[i]->bytes, bufferSize);
	    buffers[i]->bufferSubmitted = TRUE;
        submittedBuffers++;
	}

    nextReadBuffer = 0;
	lastReceivedBuffer = 0;
	nextSubmitBuffer = 0;

	(*bufferQueue)->RegisterCallback(bufferQueue, &BufferQueueCallback, this);
	(*recorder)->RegisterCallback(recorder, &RecordEventCallback, this);

    deviceOpen = TRUE;
    VBX_print("androidaudio.cpp: Audio device opened");
	return TRUE;
};

Bool AndroidAudioSrc::Close() {
    if (deviceOpen) {
        VBX_print("[DEBUG] androidaudio.cpp: Audio device closing");
        struct timespec ts;
        (*recorder)->SetRecordState(recorder, SL_RECORDSTATE_STOPPED);
        int numSubmitted = submittedBuffers.load();
        if (numSubmitted > 0) {
            // bufferSize / 2 == samples
            // 11025 samples per second, 11.025 samples per millis
            // bufferSize / 22.05 == buffer length in millis
            ts.tv_sec = 0;
            ts.tv_nsec = ((bufferSize / 22) * 1000) * numberOfBuffers;
            nanosleep(&ts, NULL);
        }
        (*bufferQueue)->Clear(bufferQueue);
        (*recorderObject)->Destroy(recorderObject);
        recorder = nullptr;
        recorderObject = nullptr;
        deviceOpen = FALSE;
        VBX_print("[DEBUG] androidaudio.cpp: Audio device closed");
    }
    return TRUE;
}

Bool AndroidAudioSrc::ShutDown() {
    for (int i = 0; i < numberOfBuffers; i++) {
        free(buffers[i]);
    }
    free(buffers);
}

void AndroidAudioSrc::recordEventCallback(SLuint32 recordEvent) {
    VBX_print("androidaudio.cpp: recordEvent was called with: %d", recordEvent);
}
/**
 * Called after each buffer is fully filled
 *
 */
void AndroidAudioSrc::bufferQueueCallback() {
    SLAndroidSimpleBufferQueueState bufferQueueState;
    SLresult result;
    //VBX_print("androidaudio.cpp: bufferQueueCallback was called");
    result = (*bufferQueue)->GetState(bufferQueue, &bufferQueueState);
    if (SL_RESULT_SUCCESS != result) {
        VBX_print("androidaudio.cpp: GetState failed: %d", result);
        return;
    }
    //VBX_print("androidaudio.cpp: count: %d, playIndex: %d", bufferQueueState.count, bufferQueueState.index);
#ifdef LOG_AUDIO
    FILE *log = eraseFile ? fopen(AUDIO_FILE, "wb") : fopen(AUDIO_FILE, "ab");
    eraseFile = 0;
    if (!log) {
    	VBX_print("androidaudio.cpp: Failed to open audio log(%d)", errno);
    }
#endif
    // process all the filled buffers
    for (; lastReceivedBuffer < bufferQueueState.index; lastReceivedBuffer++) {
        AudioBuffer *buffer = buffers[lastReceivedBuffer % numberOfBuffers];
        buffer->bytesRead = 0;
#ifdef LOG_AUDIO
        if (log) {
			if (fwrite(buffer->bytes, sizeof(char), buffer->bufferSize, log) != buffer->bufferSize) {
			    VBX_print("androidaudio.cpp: Failed to write audio log(%d)", ferror);
			}
        }
#endif
        buffer->bufferSubmitted = FALSE;
        submittedBuffers--;
    }
#ifdef LOG_AUDIO
    if (log) {
    	fflush(log);
    	fclose(log);
    }
#endif
    // enqueue any newly emptied buffers
    for (AudioBuffer *buffer = buffers[nextSubmitBuffer % numberOfBuffers];
            (buffer->bytesRead == buffer->bufferSize) && !buffer->bufferSubmitted;
            nextSubmitBuffer++) {
        memset(buffer->bytes, 0, bufferSize);
        buffer->bufferSubmitted = TRUE;
        (*bufferQueue)->Enqueue(bufferQueue, buffer->bytes, buffer->bufferSize);
        submittedBuffers++;
    }
}
