//
// Created by edh on 3/16/2016.
//

#include "com_voxware_vise_NativeLogSource.h"
#include <queue>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <jni.h>
#include "vise_jni.h"

static std::queue<char*> logElements;
static std::mutex list_guard;
static std::condition_variable isEmpty;


void vbx_com_voxware_vise_NativeLogSource_bridge(const char * string) {
    std::lock_guard<std::mutex> lock(list_guard);
    char * copy = strdup(string);
    logElements.push(copy);
    isEmpty.notify_all();
}

using namespace java::lang;

jstring JNICALL Java_com_voxware_vise_NativeLogSource_pumpMessage(JNIEnv *env, jobject thiz) {
    std::unique_lock<std::mutex> lock(list_guard);
    jobject currentThread = env->CallStaticObjectMethod(Thread.clazz, Thread.currentThread);
    while (logElements.size() == 0 && !(env->CallBooleanMethod(currentThread, Thread.isInterrupted))) {
        if (isEmpty.wait_for(lock, std::chrono::milliseconds(1000)) == std::cv_status::timeout) {
            if (env->CallBooleanMethod(currentThread, Thread.isInterrupted)) {
                return NULL;
            }
        }
    }
    if (env->CallBooleanMethod(currentThread, Thread.isInterrupted)) {
        env->ThrowNew(InterruptedException.clazz, "Interrupted");
        return NULL;
    }
    char * nString = logElements.front();
    logElements.pop();
    jstring ret = env->NewStringUTF(nString);
    free(nString);
    return ret;
}
