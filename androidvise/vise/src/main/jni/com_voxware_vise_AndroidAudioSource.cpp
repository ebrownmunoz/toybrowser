#include <jni.h>
#include <unistd.h>
#include <android/log.h>

#include "vbxcom.hh"
#include "vbxsapi.hh"
#include "vbx.h"

#include "android/androidaudiosrc.hh"
#include <com_voxware_vise_AndroidAudioSource.h>
#include <vise_jni.h>

#define SAMPLESIZE_DEFAULT  (2)
#define OBJT_SIZE           (2048)
#define BUF_SIZE            (60)

#define VBX_print(...) __android_log_print(ANDROID_LOG_ERROR, "com_voxware_vise_AndroidAudioSource.cpp", __VA_ARGS__)

using namespace java::lang;
using java::nio::ByteBuffer;
using com::voxware::vise::AndroidAudioSource;

struct com_voxware_vise_AndroidAudioSource_np {
    AndroidAudioSrc *src;
    IAudio          *iAudioPtr;
    IAudioSource    *iAudioSourcePtr;
};

/*
 * Class:     com_voxware_vise_AndroidAudioSource
 * Method:    initialize
 * Signature: ()V
 */
void JNICALL Java_com_voxware_vise_AndroidAudioSource_initialize(JNIEnv *env, jobject thiz) {
    HRESULT hres;
    VBX_init();
    com_voxware_vise_AndroidAudioSource_np *np = new com_voxware_vise_AndroidAudioSource_np();

    np->src = new AndroidAudioSrc(nullptr, nullptr);
    hres = np->src->QueryInterface(IID_IAudio, (PVOID *)&np->iAudioPtr);
    if (hres) {
        VBX_print("main: QI for IID_IAudio failed, hres %x\n", hres);
        delete np->src;
        free(np);
        env->ThrowNew(ClassCastException.clazz, "Not IAudio");
        return;
    }
    hres = np->src->QueryInterface(IID_IAudioSource, (PVOID *)&np->iAudioSourcePtr);
    if (hres) {
        __android_log_print(ANDROID_LOG_ERROR, "VISE.cpp", "main: QI for IID_IAudio failed, hres %x\n", hres);
        VBX_print("main: QI for IID_IAudioSource failed, hres %x\n", hres);
        delete np->src;
        free(np);
        env->ThrowNew(ClassCastException.clazz, "Not IAudioSource");
        return;
    }
    if (!np->src->Initialize(OBJT_SIZE, BUF_SIZE)) {
        delete np->src;
        free(np);
        env->ThrowNew(RuntimeException.clazz, "Error calling Initialize");
        return;
    }

    jobject buffer = env->NewDirectByteBuffer(np, sizeof(com_voxware_vise_AndroidAudioSource_np));
    jobject roBuffer = env->CallObjectMethod(buffer, ByteBuffer.asReadOnlyBuffer);
    env->SetObjectField(thiz, AndroidAudioSource.ptr, roBuffer);
}

/*
 * Class:     com_voxware_vise_AndroidAudioSource
 * Method:    destroy
 * Signature: ()V
 */
void JNICALL Java_com_voxware_vise_AndroidAudioSource_destroy(JNIEnv *env, jobject thiz) {
    com_voxware_vise_AndroidAudioSource_np *np = (com_voxware_vise_AndroidAudioSource_np *)env->GetDirectBufferAddress(env->GetObjectField(thiz, AndroidAudioSource.ptr));
    np->iAudioSourcePtr->Release();
    np->iAudioPtr->Release();
    np->src->ShutDown();
    delete np->src;
    delete np;
    env->SetObjectField(thiz, AndroidAudioSource.ptr, NULL);
}

jint JNICALL Java_com_voxware_vise_AndroidAudioSource_npower(JNIEnv *env, jobject thiz, jobject buffer) {
    HRESULT     hres;
    DWORD       samplesAvalable;
    BOOL        eof;
    Int         sampleCount, i;
    DWORD       bytesAvail, bytesCopied;

    com_voxware_vise_AndroidAudioSource_np *np = (com_voxware_vise_AndroidAudioSource_np *)env->GetDirectBufferAddress(env->GetObjectField(thiz, AndroidAudioSource.ptr));
    int16_t *sampBuf = (int16_t*)env->GetDirectBufferAddress(buffer);

    WAVEFORMATEX waveFormat;
    waveFormat.wFormatTag = WAVE_FORMAT_PCM;
    waveFormat.nChannels  = (WORD) 1;
    waveFormat.nSamplesPerSec = (DWORD) 11025;
    waveFormat.nAvgBytesPerSec = (DWORD) 11025 * SAMPLESIZE_DEFAULT;
    waveFormat.nBlockAlign = (WORD) SAMPLESIZE_DEFAULT;
    waveFormat.wBitsPerSample = (WORD) (8 * SAMPLESIZE_DEFAULT);
    waveFormat.cbSize = (WORD) 0;

    SDATA   waveFormatData = { (PVOID) &waveFormat, (DWORD) sizeof(WAVEFORMATEX) };

    hres = np->iAudioPtr->WaveFormatSet(waveFormatData);
    if (hres) {
        VBX_print("main: WaveFormatSet failed, hres %x\n", hres);
        env->ThrowNew(RuntimeException.clazz, "Unsupported format?");
        return -1;
    }

    VBX_print("Performing Start");
    hres = np->iAudioPtr->Start();
    if (FAILED(hres)) {
        VBX_print("main: IAudio::Start FAILED - abort\n");
        env->ThrowNew(RuntimeException.clazz, "Failed to start?");
        return -1;
    }
    sampleCount = 0;
    samplesAvalable = 0;

    for (i = 0; i < 5; i++) {
        sleep(1);
        np->iAudioSourcePtr->DataAvailable(&bytesAvail, &eof);
        np->iAudioSourcePtr->DataGet(sampBuf + sampleCount, 22050, &bytesCopied);
        sampleCount += bytesCopied / 2;
        samplesAvalable += bytesAvail / 2;
    //    VBX_print("bytesAvail: %d bytesCopied: %d eof: %d\n", bytesAvail, bytesCopied, eof);
    }

    VBX_print(" Obtained %d samples. Performing Stop\n", samplesAvalable);
    np->iAudioPtr->Stop();
    np->iAudioPtr->Flush();

    return sampleCount;
}
