#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_alg.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_mem.h"
#include "visemsg.h"
#include "v_par.h"

#ifdef DEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/**************************************************************************
 *
 * Name:      LISTEN -- Locate a word in the acoustic feature data
 *
 * Returns:   Status of reply message
 *
 * Requires:  The function WARMSTART must have been called before this
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                        Type
 *
 *              wait for data?                  V_Uns
 *              find a word?                    V_Uns
 *              word start time                 V_SN
 *              word end time                   V_SN
 *              maximum number of frames        V_ULong
 *
 * Modifies:  jin object
 *
 * Effect:    In the "wait" mode, this function waits for input audio
 *            data if the feature extractor is running and the data
 *            is not already available.
 *
 *            In the "non-wait" mode, the data is assumed already to exist
 *            in the jin buffer.
 *
 *            The beginning and end of a word utterance is detected
 *            based on amplitude.  The average volume level and the times
 *            of the detected beginning and ending of the word are returned
 *            in the reply:
 *
 *              Contents                        Type
 *
 *              volume level                    V_Int
 *              word starting sequence number   V_SN
 *              word ending sequence number     V_SN
 *              number of frames                V_ULong
 *
 *            The status in the reply will be:
 *
 *              VISESUCCESS      if successful,
 *              FINDWORDFAILURE  if a word was requested and cannot be found,
 *              LISTENTOOLONG    if listening for too long a duration,
 *              CANTREADAMP      if amplitude data cannot be read.
 *
 * Implementation:
 *
 *            If in "wait" mode,
 *             if sampling, wait for jin data
 *             if not, quit if jin data not available
 *
 *            If in "find word" mode,
 *              find word boundaries using jin[0] between the
 *              specified starting and ending sequence numbers
 *
 *            Compose a return message containing:
 *             volume level of utterance
 *             sequence number at which requested data starts
 *             sequence number at which requested data ends
 *             the number of frames in the requested data
 *
 * Uses:      JINSRC_canread -- tell whether jin is available
 *            findword -- detect word boundaries based on jin[0]
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\LISTEN.C_V  $
 * 
 *    Rev 3.8   20 Mar 1992 12:40:42   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.7   12 Nov 1991 15:47:30   DCV
 * Now expects findword() to return standard VISE status codes.
 * 
 *    Rev 3.6   08 Oct 1991 10:09:00   DCV
 * Modified for continuous feature extraction.
 * 
 *    Rev 3.5   14 Aug 1991 16:09:10   DCV
 * Added JINBUFFERSIZE to VISE parameter block
 * 
 *    Rev 3.4   02 Aug 1991 10:20:42   DCV
 * Installed new message system
 * 
 *    Rev 3.3   18 Jul 1991 16:14:52   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   07 Nov 1990 16:20:10   DCV
 * Modified MMM_xxx calls for new mem.c
 * 
 *    Rev 3.1   13 Sep 1990 13:47:52   DCV
 * First version with individual header files
 * 
 *    Rev 3.0   05 Sep 1990  9:05:52   DCV
 * First SPOX revision
 * 
 *    Rev 1.1   24 Jul 1990 14:09:30   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:12:58   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
LISTEN(VISE vise)
{
  V_Uns   live = 0;
  V_Uns   find = 0;
  V_SN    startSN = 0;
  V_SN    endSN = 0;
  V_ULong numFrames = 0;
  V_Int   volume = 0;
  V_SN    wordStart = (V_SN) 0;
  V_SN    wordEnd = (V_SN) 0;
  V_SN    sn;
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status;

  /* Get the parameters from the command message */
  MSG_readUns(vise->incoming, &live);
  MSG_readUns(vise->incoming, &find);
  MSG_readSN(vise->incoming, &startSN);
  MSG_readSN(vise->incoming, &endSN);
  MSG_readULong(vise->incoming, &numFrames);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

  /* Quit if this is not a live request and the requested frames are unavailable */
  if (!(V_Bool) live && !JINSRC_canread(vise->jinSource, startSN, endSN))
    VISE_EXIT(CANTREADFRAME);

  /* Initialize the jin padder */
  if ((status = JINSRC_pad(vise->jinSource)))
    VISE_EXIT(status);

  if (numFrames == 0 || numFrames > MAXLISTENFRAMES) numFrames = MAXLISTENFRAMES;
  /* Since first JINSRC_read is special */
  vise->maxFrames = numFrames - 1;

  /* This is a TEMPORARY fix pending resolution of the whole wraparound issue */
  if (endSN < (V_SN) 0) endSN = SN_MAX;

  /* Pull first requested frame into the frame queue */
  if (!JINSRC_read(vise->jinSource, &startSN, NULL, &status)) {
    VISE_EXIT(status);
  } else {
    sn = startSN;
  }

  /* Pull the rest of the requested frames into the frame queue */
  while ((sn < endSN) && (sn = -1)) {
    if (!JINSRC_read(vise->jinSource, &sn, NULL, &status))
      VISE_EXIT(status);
    /* If the specified period has been exhausted, quit */
    if (vise->maxFrames && !--vise->maxFrames) {
      endSN = sn;
      break;
    }
  }

  status = findword(vise, startSN, endSN, &wordStart, &wordEnd, &volume);

  VBX_DEBUG(VBX_print("  LISTEN: FindWord Status = %d; wordStart = " QuadFmt(-) "; wordEnd = " QuadFmt(-) "\n", status, wordStart, wordEnd));

  if (!(V_Bool) find && (status == VISESUCCESS || status == CANTFINDWORD)) {
    wordStart = startSN;
    wordEnd = endSN;
    status = VISESUCCESS;
  }

  numFrames = (V_ULong) JINSRC_numframes(vise->jinSource, &wordStart, &wordEnd);

  VISE_EXIT(status);

  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId,
                    (V_ULong) (3 * VINTSIZEINMELS + VLONGSIZEINMELS + 2 * VSNSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _LISTEN);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_writeInt(vise->outgoing, volume);
    MSG_writeSN(vise->outgoing, wordStart);
    MSG_writeSN(vise->outgoing, wordEnd);
    MSG_writeULong(vise->outgoing, numFrames);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

