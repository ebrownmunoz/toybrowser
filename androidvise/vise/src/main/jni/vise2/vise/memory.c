#include "vise.h"
#include <stdlib.h>
#include <string.h>
#include "v_mem.h"
#include "v_par.h"
#include <assert.h>
#include "ifaces.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)   P
#else
#define VBX_DEBUG(P)
#endif

/******************************************************************************
 * 
 * Name:        mem.c
 *
 * Description: Memory management for VISE.
 *
 * Operations:
 *
 *  1. MEM_create()    - create a memory allocator
 *
 *  2. MEM_destroy()   - destroy a memory allocator
 *
 *  3. MEM_free()      - deallocate all memory in a given class
 *
 *  4. MEM_size()      - return the number of bytes allocated to a class of memory
 *
 *  5. MEM_alloc()     - allocate a number of contiguous blocks of memory
 *
 *  6. MEM_alloclist() - allocate and link a number of contiguous blocks of memory
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\MEM.C_V  $
 * 
 *    Rev 3.9   29 Aug 1991 14:34:12   DCV
 * Fixed sporadic DRAM sizing error due to hardware refresh
 * 
 *    Rev 3.8   14 Aug 1991 16:12:30   DCV
 * Fixed MEM_alloc to use any available memory when memory of the requested
 *  type is exhausted.
 * Added MEM_alloclist() to allocate NULL-terminated linked lists.
 * Added FASTMEM and SLOWMEM to VISE parameters block.
 * 
 *    Rev 3.7   18 Jul 1991 16:15:50   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.6   12 Jun 1991 14:01:12   DCV
 * Eliminated SCOREKEEPING ifdefs
 * 
 *    Rev 3.5   28 Feb 1991 15:14:26   DCV
 * Reversed arguments in call to MEM_aliased to avoid
 * bus capacitance problem in a 6000 without DRAM
 * 
 *    Rev 3.4   13 Dec 1990 18:32:44   DCV
 * Corrected 7000 configuration error in MEM_find()
 * 
 *    Rev 3.3   07 Nov 1990 16:17:12   DCV
 * Rewritten for all 6000 and 7000 configurations (without SPOX dependency)
 * 
 *    Rev 3.2   25 Sep 1990 10:41:48   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 3.1   13 Sep 1990 13:50:48   DCV
 * First version with individual header files
 * 
 *    Rev 3.0   05 Sep 1990  9:07:36   DCV
 * First SPOX revision
 * 
 *    Rev 1.2   24 Jul 1990 14:10:10   DCV
 * Changed maximum number of word entries to 25000 in 7000 LV
 * 
 *    Rev 1.1   19 Jul 1990 18:02:30   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:16:14   DCV
 * Initial revision.
 *
 *****************************************************************************/

#define ALLOCLIST_QUANTUM 25

/* Allocation descriptor */
typedef struct memalloc_s * MEMALLOC;
typedef struct memalloc_s {
  MEMALLOC   next;
  V_Ptr      alloc;
  V_Char   * caller;   /* The name of the calling fcn */
  V_ULong    size;
} memalloc_t;

/* Allocation class descriptor */
typedef struct memcfg_s {
  IMemory  * imem;   /* The memory allocation interface */
  memalloc_t list;   /* The list of allocation descriptors for this class */
  MEMALLOC   next;   /* The next available allocation descriptor in the list */
  MEMALLOC   last;   /* The last segment of the list of allocation descriptors allocated */
  V_ULong    size;   /* The number of bytes allocated to this class */
} memcfg_t;

/* Private function prototype */
static V_Void  MEM_deallocateClass(MEMCFG cfg, mem_t memType);

/******************************************************************************
 * 
 * Name:     MEM_create() - create a memory allocator
 *
 * Returns:  A pointer to the system memory configuration descriptor
 *     
 * Requires: Nothing
 *
 * Modifies: Memory configuration block
 *
 * Effect:   Sets up the memory configuration block
 *
 * Implementation:
 *
 *           Allocate the array of allocation class descriptors and
 *           return a pointer to it
 * 
 * Dependency:
 *
 * Uses:     IMemory->Calloc()
 *
 *****************************************************************************/

MEMCFG
MEM_create(IMemory * imem)
{
  MEMCFG  cfg  = (MEMCFG) imem->Calloc(imem, NUM_MEM_TYPES, sizeof(memcfg_t), FAST_MEMORY);
  mem_t   type = NUM_MEM_TYPES;

  if (cfg) {
    while (type--) {
      cfg[type].imem = imem;
      /* The next available allocation descriptor is the first one in the list */
      cfg[type].next = &cfg[type].list;
    }
  }

  return(cfg);
}

/******************************************************************************
 * 
 * Name:     MEM_destroy() - destroy a memory allocator
 *
 * Returns:  Nothing
 *     
 * Requires: Nothing
 *
 * Modifies: Memory configuration block
 *
 * Effect:   Deallocates all memory allocated by the allocator 
 *           and destroys the allocator
 *
 * Implementation:
 *
 *    Deallocate all memory in all classes allocated by this allocator
 *    Deallocate the array of allocation class descriptors
 * 
 * Dependency:
 *
 * Uses:     MEM_free()
 *           IMemory->Free()
 *
 *****************************************************************************/

V_Void
MEM_destroy(MEMCFG cfg)
{
  if (cfg) {
    MEM_free(cfg, ALL_MEM);
    cfg->imem->Free(cfg->imem, cfg);
  }
}

/******************************************************************************
 * 
 * Name:     MEM_free - free all the memory in a given allocation class
 *
 * Call:     MEM_free(cfg, memtype);
 *           N.B. memtype may be any individual type, or ALL_MEM
 *
 * Returns:  Nothing
 *     
 * Requires: A memory allocator
 *
 * Modifies: Internal memory allocation
 *
 * Effect:   Deallocates the types of memory specified
 *
 * Implementation:
 * 
 * Dependency: 
 *
 * Uses:     MEM_deallocateClass()
 *
 *****************************************************************************/

V_Void
MEM_free(MEMCFG cfg, mem_t memType)
{
  if (memType == ALL_MEM) {
    while (memType--) MEM_deallocateClass(cfg, memType);
  } else {
    MEM_deallocateClass(cfg, memType);
  }
}

static V_Void
MEM_deallocateClass(MEMCFG cfg, mem_t memType)
/******************************************************************************
 * DESCRIPTION
 * Deallocates a class of memory, thereby also deallocating the allocation
 * lists, which are of the same class.  memType must be an individual type.
 *****************************************************************************/
{
  MEMALLOC  next, nextPlusOne;

  /* MEM_printCfg(cfg, memType, "start of MEM_deallocateClass"); */ /* DEBUG */

  cfg += memType;

  /* Free each allocation in the list for this class */
  next = &cfg->list;
  while (next != cfg->next) {
    /* Need nextPlusOne, because the IMemory->Free() may deallocate *next */
    nextPlusOne = next->next;
    if (next->alloc) cfg->imem->Free(cfg->imem, next->alloc);
    next = nextPlusOne;
  }
  cfg->next = &cfg->list;
  cfg->list.next = (MEMALLOC) NULL;

  /* Free the final list segment, if any */
  if (cfg->last) {
    cfg->imem->Free(cfg->imem, cfg->last);
    cfg->last = (MEMALLOC) NULL;
  }

  cfg->size = 0;
}

/******************************************************************************
 * 
 * Name:     MEM_size - return the number of bytes allocated to a class
 *
 * Call:     MEM_size(cfg, memtype);
 *
 * Returns:  The number of bytes allocated to the given class of memory
 *     
 * Requires: A memory allocator
 *
 * Modifies: Nothing
 *
 * Effect:  
 *
 * Implementation:
 * 
 * Dependency: 
 *
 * Uses:     Nothing
 *
 *****************************************************************************/

V_ULong
MEM_size(MEMCFG cfg, mem_t memType)
{
  assert(memType < NUM_MEM_TYPES);
  return(cfg[memType].size);
}

/******************************************************************************
 * 
 * Name:     MEM_alloc - Allocate a block of memory
 *
 * Call:     ptr = MEM_alloc(size, count, memType);
 *
 * Returns:  A pointer to the allocated block, or NULL if out of memory
 *     
 * Requires: 
 *
 * Modifies: The memory configuration
 *
 * Effect:
 *
 * Implementation:
 *
 * Dependency:
 *
 * Uses:     IMemory->Calloc()
 *
 *****************************************************************************/

V_Ptr
MEM_alloc(MEMCFG cfg, V_ULong blkSize, V_ULong blkCount, mem_t memType, V_Char * caller)
  /* cfg  -  memory configuration block */
  /* blkSize -  size of block */
  /* blkCount -  how many blocks to allocate */
  /* memType -  type of memory to allocate (FAST, SLOW, ...) */
  /* caller   -   name of calling fcn */
{
  MEMCFG    thisCfg;
  MEMALLOC  newListSeg;
  V_Ptr     thisAlloc;
  V_ULong   size = blkSize * blkCount;

#if 0
#ifdef ALIGN64
  /* Align on 8-byte boundaries */
  size = (size + (V_ULong) 7) & ~((V_ULong) 7);
#endif
#endif

  /* Get the configuration descriptor for the requested allocation class */
  assert(memType < NUM_MEM_TYPES);
  thisCfg = cfg + memType;

  /* If the list of allocation descriptors for this class has only one member left, add another list segment to it */
  if (!thisCfg->next->next) {
    /* Make the remaining member succeed itself, so that thisCfg->next will still point to it after MEM_alloclist returns */
    thisCfg->next->next = thisCfg->next;
    VBX_DEBUG(VBX_print("  MEM_alloc() extending allocation descriptor list for %s\n", MEMTYPESTR(memType)));
    /* If we can allocate memory for a new segment of the list, then extend the list */
    if ((newListSeg = (MEMALLOC) MEM_alloclist(cfg, (V_ULong) sizeof(memalloc_t), ALLOCLIST_QUANTUM, memType, "MEM_alloc"))) {
      /* Make the allocation descriptor for the most recently allocated segment of the list its own last member */
      thisCfg->next->alloc = (V_Ptr) thisCfg->last;
      /* Make the new segment the most recently allocated segment */
      thisCfg->last = newListSeg;
      /* Link the new segment onto the end of the old list */
      thisCfg->next->next = newListSeg;
      /* Make the first allocation descriptor in the new segment the next one to use */
      thisCfg->next = thisCfg->next->next;
    /* Otherwise, the enterprise is hopeless */
    } else {
      return(NULL);
    }
  }

  /* Allocate the requested memory */
  thisAlloc = size ? (V_Ptr) thisCfg->imem->Calloc(thisCfg->imem, (size_t) blkCount, (size_t) blkSize, memType) : NULL;
  /* Give the allocation a descriptor, even if the allocation is legitimately NULL */
  if (thisAlloc || !size) {
    thisCfg->next->alloc = thisAlloc;
    thisCfg->next->size = size;
    thisCfg->next->caller = caller;
    thisCfg->size += size;
    thisCfg->next = thisCfg->next->next;
  }
  /* MEM_printCfg(cfg, memType, "end of MEM_alloc"); */ /* DEBUG */
  return(thisAlloc);
}

/******************************************************************************
 * 
 * Name:     MEM_alloclist - Allocate a NULL-terminated list of objects
 *
 * Call:     ptr = MEM_alloclist(cfg, size, count, memType, caller);
 *
 * Returns:  A pointer to the head of the list, or NULL if out of memory
 *     
 * Requires: 
 *
 * Modifies: The memory configuration
 *
 * Effect:
 *
 * Implementation:
 *
 *           Allocate an array of count objects of size size.
 *           If the allocation fails
 *             Return NULL;
 *           Else
 *             Link the objects into a NULL terminated list.
 *             Return a pointer to the head of the allocated list.
 * 
 * Dependency:
 *
 * Uses:     MEM_alloc()
 *
 *****************************************************************************/

V_Ptr
MEM_alloclist(MEMCFG cfg, V_ULong objSize, V_ULong objCount, mem_t memType, V_Char * caller)
  /* cfg  - memory configuration block */
  /* objSize - size of objects */
  /* objCount - the number of objects to allocate */
  /* memType - FAST_MEM, SLOW_MEM, ... */
  /* caller   -   name of calling fcn */
{
  V_Ptr * nextObj;
  V_Ptr * lastObj;
  V_Ptr * headObj;

  if ((headObj = (V_Ptr *) MEM_alloc(cfg, objSize, objCount, memType, caller)) != NULL) {
      /* Link the new objects into a NULL-terminated list,
         using the first element as a pointer to the successor */
    nextObj = headObj;
    lastObj = (V_Ptr *) ((V_Ptr) headObj + (objCount - 1) * objSize);
    while (nextObj < lastObj) {
      *nextObj = (V_Ptr) ((V_Ptr) nextObj + objSize);
      nextObj = (V_Ptr *) ((V_Ptr) nextObj + objSize);
    }
    *lastObj = NULL;
  }

  return((V_Ptr) headObj);
}

#ifdef VBXDEBUG

V_Void
MEM_printCfg(MEMCFG cfg, mem_t memType, V_Char * label)
/******************************************************************************
 * DESCRIPTION
 *****************************************************************************/
{
  MEMALLOC  next;
  UInt      index = 0;

  cfg += memType;

  /* Print each allocation in the list for this class */
  next = &cfg->list;
  VBX_print("  MEMORY DESCRIPTOR for %s at %p (%s)\n", MEMTYPESTR(memType), cfg, label);
  VBX_print("   Allocation List (@%p)\n", next);
  VBX_print("     Idx    Address    Next      Alloc   Size        Caller\n");
  while (next != cfg->next) {
    VBX_print("    %4d %c %09p %09p %09p %-11u %s\n",
              ++index, (next == cfg->last) ? '>' : ' ', next, next->next, next->alloc, next->size, next->caller);
    next = next->next;
  }
  VBX_print("     ---   %09p %09p %09p\n", next, next->next, next->alloc);
  VBX_print("                      Total allocation = %ld bytes\n", cfg->size);
  VBX_print("  END of Memory Descriptor\n");
}

#endif
