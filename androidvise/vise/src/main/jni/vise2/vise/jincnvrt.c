#include "vise.h"
#include "jincnvrt.h"


/***************************************************************************
 *
 * Name:      jin_from_jin2 - unpack jin from jin2
 *
 * Returns:   nothing
 *
 * Requires:  The jin components must be unsigned 8-bit quantities.
 *            Regardless of byte order (little-endian or big-endian), the
 *            lower-order jin component must be in the low-order byte of
 *            the jin2 word.
 *
 * Modifies:  dest
 *
 * Effect:    Unpacks jin data from jin2 representation
 *              (2 8-bit components in each 16-bit shortword)
 *            to jin representation
 *              (1 8-bit component in each 8-bit byte)
 *
 * Implementation:
 *
 **************************************************************************/

  V_Void
jin_from_jin2(jin dest, jin2 src)
{
  jinel  * jinPtr = (jinel *) dest;
  jin2el * jin2Ptr = (jin2el *) src;
  V_Int    i = SIZE_OF_JIN2_ARRAY;

  while (i--) {
    *jinPtr++ = *jin2Ptr & 0x00ff;
    *jinPtr++ = *jin2Ptr++ >> 8 & 0x00ff;
  }
}

/**************************************************************************
 *
 * Name:      jin2_from_jin - pack jin into jin2
 *
 * Returns:   nothing
 *
 * Requires:  The jin components must be unsigned 8-bit quantities
 *
 * Modifies:  dest
 *
 * Effect:    Packs jin data to jin2 representation
 *              (2 8-bit components in each 16-bit shortword)
 *            from jin representation
 *              (1 8-bit component in each 8-bit byte)
 *
 *            Regardless of byte order (little-endian or big-endian), the
 *            lower-order jin component goes into the low-order byte of
 *            the jin2 word.
 *
 * Implementation:
 *
 **************************************************************************/

  V_Void
jin2_from_jin(jin2 dest, jin src)
{
  jinel  * jinPtr = (jinel *) src;
  jin2el * jin2Ptr = (jin2el *) dest;
  V_Int    i = SIZE_OF_JIN2_ARRAY;

  while (i--) {
    *jin2Ptr = *(jinPtr + 1);
    *jin2Ptr = *jin2Ptr << 8 | *jinPtr;
    jinPtr += 2;
    jin2Ptr++;
  }
}

/**************************************************************************
 *
 * Name:      jin_from_jin4 - unpack jin from jin4
 *
 * Returns:   nothing
 *
 * Requires:  The jin components must be unsigned 8-bit quantities.
 *            Regardless of byte order (little-endian or big-endian), the
 *            lower-order jin components must be in the lower-order bytes
 *            of the jin4 word.
 *
 * Modifies:  dest
 *
 * Effect:    Unpacks jin data from jin4 representation
 *              (4 8-bit components in each 32-bit longword)
 *            to jin representation
 *              (1 8-bit component in each 8-bit byte)
 *
 * Implementation:
 *
 **************************************************************************/

  V_Void
jin_from_jin4(jin dest, jin4 src)
{
  jinel  * jinPtr = (jinel *) dest;
  jin4el * jin4Ptr = (jin4el *) src;
  V_Int    i = SIZE_OF_JIN4_ARRAY;

  while (i--) {
    *jinPtr++ = *jin4Ptr & 0x000000ffUL;
    *jinPtr++ = *jin4Ptr >> 8 & 0x000000ffUL;
    *jinPtr++ = *jin4Ptr >> 16 & 0x000000ffUL;
    *jinPtr++ = *jin4Ptr++ >> 24 & 0x000000ffUL;
  }
}

/**************************************************************************
 *
 * Name:      jin4_from_jin - pack jin into jin4
 *
 * Returns:   nothing
 *
 * Requires:  The jin components must be unsigned 8-bit quantities
 *
 * Modifies:  dest
 *
 * Effect:    Packs jin data to jin4 representation
 *              (4 8-bit components in each 32-bit longword)
 *            from jin representation
 *              (1 8-bit component in each 8-bit byte)
 *
 *            Regardless of byte order (little-endian or big-endian), the
 *            lower-order jin components go into the lower-order bytes of
 *            the jin4 word.
 *
 * Implementation:
 *
 **************************************************************************/

 V_Void
jin4_from_jin(jin4 dest, jin src)
{
  jinel  * jinPtr = (jinel *) src;
  jin4el * jin4Ptr = (jin4el *) dest;
  V_Int    i = SIZE_OF_JIN4_ARRAY;

  while (i--) {
    *jin4Ptr = *(jinPtr + 3);
    *jin4Ptr = *jin4Ptr << 8 | *(jinPtr + 2);
    *jin4Ptr = *jin4Ptr << 8 | *(jinPtr + 1);
    *jin4Ptr = *jin4Ptr << 8 | *jinPtr;
    jinPtr += 4;
    jin4Ptr++;
  }
}

/**************************************************************************
 *
 * Name:      jin2_from_jin4 - unpack jin2 from jin4
 *
 * Returns:   nothing
 *
 * Requires:  The jin components must be unsigned 8-bit quantities.
 *            Regardless of byte order (little-endian or big-endian), the
 *            lower-order jin components must be in the lower-order bytes
 *            of the jin4 word.
 *
 * Modifies:  dest
 *
 * Effect:    Unpacks jin data from jin4 representation
 *              (4 8-bit components in each 32-bit longword)
 *            to jin2 representation
 *              (2 8-bit components in each 16-bit shortword)
 *
 * Implementation:
 *
 **************************************************************************/

  V_Void
jin2_from_jin4(jin2 dest, jin4 src)
{
  jin2el * jin2Ptr = (jin2el *) dest;
  jin4el * jin4Ptr = (jin4el *) src;
  V_Int    i = SIZE_OF_JIN4_ARRAY;

  while (i--) {
    *jin2Ptr++ = *jin4Ptr & 0x0000ffffUL;
    *jin2Ptr++ = *jin4Ptr++ >> 16 & 0x0000ffffUL;
  }
}

/**************************************************************************
 *
 * Name:      jin4_from_jin2 - packs jin2 into jin4
 *
 * Returns:   nothing
 *
 * Requires:  The jin components must be unsigned 8-bit quantities
 *
 * Modifies:  dest
 *
 * Effect:    Packs jin data to jin4 representation
 *              (4 8-bit components in each 32-bit longword)
 *            from jin representation
 *              (2 8-bit components in each 16-bit shortword)
 *
 *            Regardless of byte order (little-endian or big-endian), the
 *            lower-order jin components go into the lower-order bytes of
 *            the jin4 word.
 *
 * Implementation:
 *
 **************************************************************************/

  V_Void
jin4_from_jin2(jin4 dest, jin2 src)
{
  jin2el * jin2Ptr = (jin2el *) src;
  jin4el * jin4Ptr = (jin4el *) dest;
  V_Int    i = SIZE_OF_JIN4_ARRAY;

  while (i--) {
    *jin4Ptr = *(jin2Ptr + 1);
    *jin4Ptr = *jin4Ptr << 16 | *jin2Ptr;
    jin2Ptr += 2;
    jin4Ptr++;
  }
}

