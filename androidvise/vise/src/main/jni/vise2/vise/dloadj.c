#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "visemsg.h"
#include <assert.h>

/**************************************************************************
 *
 * Name:      DLOADJIN - Upload jin data
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                         Type
 *
 *              Starting sequence number         V_SN
 *              Ending sequence number           V_SN
 *              Number of frames                 V_Uns
 *              For each frame:
 *                Sequence number                V_SN
 *                Jin data (as a jin2r)          V_Uns * SIZE_OF_JIN2_ARRAY
 *
 * Modifies:  The message
 *
 * Effect:    Downloads the given jin frames into the frame queue,
 *            resetting it if the download overlaps with the frames
 *            currently in the queue.
 *
 * Implementation:
 *
 * Uses:      MSG_xxx -- Message functions
 *            JINSRC_resetQueue
 *            JINSRC_insert
 *
 **************************************************************************/

  V_Err
DLOADJIN(VISE vise)
{
  V_SN    startSN, endSN, sn;
  V_Uns   numFrames;
  jin2r   features;
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status, istatus;

  /* Get the requested limiting sequence numbers */
  MSG_readSN(vise->incoming, &startSN);
  MSG_readSN(vise->incoming, &endSN);
  MSG_readUns(vise->incoming, &numFrames);

  /* Insert the first frame into the frame queue, resetting it if necessary */
  istatus = VISESUCCESS;
  if (numFrames-- &&
      MSG_readSN(vise->incoming, &sn) &&
      MSG_readMels(vise->incoming, (V_Mel *) &features, (V_ULong) SIZE_OF_JIN2_ARRAY) &&
      !JINSRC_insert(vise->jinSource, &sn, &features, &istatus)) {
    if (istatus != CANTWRITEFRAME ||
        !JINSRC_resetQueue(vise->jinSource) ||
        !JINSRC_insert(vise->jinSource, &sn, &features, &istatus))
      VISE_EXIT(istatus);
  }

  /* Insert the rest of the frames into it */
  while (numFrames-- &&
         MSG_readSN(vise->incoming, &sn) &&
         MSG_readMels(vise->incoming, (V_Mel *) &features, (V_ULong) SIZE_OF_JIN2_ARRAY) &&
         JINSRC_insert(vise->jinSource, &sn, &features, &istatus));

  assert(sn == endSN);

  MSG_closeRead(vise->incoming, &status);

  VISE_EXIT(istatus != VISESUCCESS ? istatus : status);

  /* Exception handling */
  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DLOADJIN);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
