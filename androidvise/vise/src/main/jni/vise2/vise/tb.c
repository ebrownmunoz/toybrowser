#include "pthr.h"
#include "vise.h"
#include "v_tb.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"

/**************************************************************************
 *
 * Name:        tb -- Traceback Cluster
 *
 * Description: The traceback cluster implements a single abstract
 *              traceback tree object which is used in determining the
 *              results of the dynamic programmming recognition
 *              process.  A traceback tree consists of a list of
 *              traceback frames, doubly-linked in order of increasing
 *              time of creation.  Frames can be added only at the more
 *              recent end of the list, but they can be removed from
 *              anywhere on the list.
 *
 *              The traceback cluster gives special status to three of
 *              the frames in the traceback tree:  the next frame to
 *              be used, the oldest unpruned frame, and the youngest
 *              frame containing an "immortal" traceback node.  The
 *              first two are maintained by the traceback cluster
 *              itself;  the last will someday be maintained by
 *              partial_traceback(), which will identify "immortal"
 *              nodes.
 *      
 * Operations:
 *
 *              tb_init(vise); -- Initialize the traceback object.
 *
 *              tb_addframe(vise, nodelist, nodecount, time, sn, sumbestscores, sigmagmin);
 *                -- Add a frame to the traceback tree.
 *
 *              tb_removeframe(vise, frame); -- Remove a frame from the traceback tree.
 *
 *              tb_nextframe(vise); -- Return the next available frame in the traceback tree.
 *
 *              tb_prune(vise); -- Prune the traceback tree.
 *
 *                tbn         nodelist; -- A list of traceback nodes
 *                V_Int      nodecount; -- The number of nodes in a nodelist
 *                V_Time          time; -- Creation time of a frame
 *                SN                sn; -- Sequence number of the frame
 *                tbf            frame; -- A traceback frame
 *                V_Long sumbestscores; -- Sum of previous best scores
 *                V_Int      sigmagmin; -- The renormalized sum of globalmins
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\TB.C_V  $
 * 
 *    Rev 3.8   18 Aug 1992 11:11:36   DCV
 * Fixed bug in tb_addframe making
 * tb_prune fail and Vise asleep.
 * Initialize tbffirstnode to NULL. 
 * 
 *    Rev 3.7   20 Mar 1992 12:59:58   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   29 Jan 1992 12:56:42   DCV
 * Changed basic types and modified tb_init() to return pointer to first tbf.
 * 
 *    Rev 3.5   20 Sep 1991 09:57:12   DCV
 * Added code to put the sum of best path scores in the tbf of a new frame.
 * 
 *    Rev 3.4   18 Jul 1991 16:22:50   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.3   13 Jun 1991 15:07:06   DCV
 * Eliminated superfluous call to tbf_decnodecount().
 * 
 *    Rev 3.2   12 Jun 1991 14:35:32   DCV
 * Rewritten to support N-best path reporting.
 * 
 *    Rev 3.1   25 Sep 1990 10:43:46   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 3.0   13 Sep 1990 13:59:04   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:16:28   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:21:10   DCV
 * Initial revision.
 *
 **************************************************************************/

/***************************************************************************
 *
 * Name:      tb_init - Initialize traceback object
 *
 * Call:      firstframe = tb_init(vise);
 *
 *              VISE      vise; -- The VISE descriptor
 *              tbf firstframe; -- The first frame in the traceback tree
 *
 * Returns:   A tbf
 *
 * Requires:  tbf_init() must already have been called.
 *
 * Modifies:  The traceback object
 *
 * Effect:    Initializes the traceback object.
 *
 * Implementation:
 *
 *            Allocate a new frame, if possible, otherwise return NULL.
 *            Set the tbnextframe, tbmatureframe and tblastimmortal
 *            fields in tb to the new frame.
 *            Set the tbfsuccessor, tbfpredecessor and tbffirstnode
 *            fields in the new frame to NULL.
 *            Initialize the accumulated globalmin to zero.
 *            Return the new frame.
 *
 * Uses:      tbf_alloc
 *            tbf_setsuccessor
 *            tbf_setfirstnode
 *            tbf_setglobalmin
 *            tbf_setpredecessor
 *
 **************************************************************************/

  tbf
tb_init(VISE vise)
{
  tbf newframe;

   /* Get an empty traceback frame */
  if ((newframe = tbf_alloc(vise)) != NULL) {

    vise->tbtree.tbnextframe = vise->tbtree.tbmatureframe = vise->tbtree.tblastimmortal = newframe;

    tbf_setsuccessor(newframe, NULL);
    tbf_setfirstnode(newframe, NULL);
    tbf_setglobalmin(newframe, (V_Long) 0);
    tbf_setsumbestscores(newframe, (V_Long) 0);
    tbf_setpredecessor(newframe, NULL);
  }

  return(newframe);
}

/* End of tb_init() */

/**************************************************************************
 *
 * Name:      tb_addframe - Add a frame to the trackback tree
 *
 * Call:      nextframe = tb_addframe(vise, nodelist, nodecount, time, sn, sbstscrs, sgmin);
 *
 *              tbf   nextframe; -- A traceback frame
 *              VISE       vise; -- The VISE descriptor
 *              tbn    nodelist; -- List of nodes to put in the frame
 *              V_Int nodecount; -- The number of nodes in the list
 *              V_Time     time; -- The creation time of the new frame
 *              V_SN         sn; -- The sequence number of the new frame
 *              V_Long sbstscrs; -- Sum of best scores
 *              V_Int     sgmin; -- Sum of renormalized globalmins
 *
 * Returns:   New empty frame, or NULL if none available
 *
 * Requires:  tb_init must be called prior to invoking tb_addframe.
 *
 * Modifies:  The traceback object
 *
 * Effect:    Puts the nodelist and time into tbnextframe (which was
 *            allocated by the last call to tb_addframe() or tb_init()
 *            but is still empty) of the traceback tree, and makes it
 *            the current frame.  Then allocates a new (empty) frame,
 *            if possible, makes it the successor to the old next (now
 *            current) frame, makes it the new next frame, and returns
 *            it to the caller.
 *
 * Implementation:
 *
 *            Put the nodelist, nodecount, time and sigmaglobalmin
 *            into tbnextframe (which was allocated by the last call
 *            to tb_addframe() or tb_init() but is still empty) of
 *            the traceback tree.
 *
 *            Allocate a new (empty) frame, if possible, make it the
 *            successor to the old next frame, make it the new next
 *            frame, and return it to the caller.
 *
 * Uses:      tbf_setinfo
 *            tbf_alloc
 *            tbf_setsuccessor
 *            tbf_setpredecessor
 *            tbf_setfirstnode
 *
 **************************************************************************/

  tbf
tb_addframe(VISE vise, tbn nodelist, V_Int nodecount, V_Time time, V_SN sn, V_Long sumbestscores, V_Long sigmaglobalmin)
{
  tbf newframe = vise->tbtree.tbnextframe;

   /* Fill in the fields of current next frame */
  tbf_setinfo(newframe, nodelist, nodecount, time, sn, sumbestscores, sigmaglobalmin);

   /* Allocate a new next frame, and make it successor to current one */
  newframe = tbf_alloc(vise);
  tbf_setsuccessor(vise->tbtree.tbnextframe, newframe);

   /* Link the new next frame immediately after the current one */
  if (newframe != NULL) {
    tbf_setpredecessor(newframe, vise->tbtree.tbnextframe);
    tbf_setsuccessor(newframe, NULL);
    tbf_setfirstnode(newframe, NULL);
  }

   /* Make the new frame the current next frame */
  vise->tbtree.tbnextframe = newframe;

  return(newframe);
 }

/* End of tb_addframe() */

/**************************************************************************
 *
 * Name:      tb_nextframe - Get the next frame in the trackback tree
 *
 * Call:      nextframe = tb_nextframe(vise);
 *
 *              tbf   nextframe; -- A traceback frame
 *              VISE       vise; -- The VISE descriptor
 *
 * Returns:   The next frame in the traceback tree
 *
 * Requires:  tb_init must be called prior to invoking tb_nextframe.
 *
 * Modifies:  The traceback object
 *
 * Effect:    Returns tbnextframe.
 *
 * Implementation:
 *
 *            Return tbnextframe
 *
 * Uses:    
 *
 **************************************************************************/

  tbf
tb_nextframe(VISE vise)
{
  return(vise->tbtree.tbnextframe);
}

/* End of tb_nextframe() */

/**************************************************************************
 *
 * Name:      tb_removeframe - Remove a frame from the trackback tree
 *
 * Call:      tb_removeframe(vise, frame);
 *
 *              VISE    vise; -- The VISE descriptor
 *              tbf    frame; -- The traceback frame to be removed
 *
 * Returns:   Nothing
 *
 * Requires:  The frame must exist and have no nodes in its nodelist
 *
 * Modifies:  The traceback object
 *
 * Effect:    Removes the frame from the traceback tree and returns
 *            it to the list of free traceback frames.
 *
 * Implementation:
 *
 *            Make the frame's successor the successor to its
 *            predecessor.
 *
 *            Make the frame's predecessor the predecessor to its
 *            successor.
 *
 *            Return the frame to the free list.
 *
 * Uses:      tbf_successor
 *            tbf_setsuccessor
 *            tbf_predecessor
 *            tbf_setpredecessor
 *            tbf_free
 *
 **************************************************************************/

  V_Void
tb_removeframe(VISE vise, tbf frame)
{
  tbf followingframe = tbf_successor(frame);
  tbf precedingframe = tbf_predecessor(frame);

  if (precedingframe != NULL)
   tbf_setsuccessor(precedingframe, followingframe);

  if (followingframe != NULL)
    tbf_setpredecessor(followingframe, precedingframe);

  tbf_free(vise, frame);
}

/* End of tb_removeframe() */

/**************************************************************************
 *
 * Name:      tb_prune - prune the traceback tree
 *
 * Call:      tb_prune(vise, time);
 *
 *              VISE    vise; -- The VISE descriptor
 *              V_Time  time; -- The current frame time
 *
 * Returns:   V_Void
 *
 * Requires:  tb_init must have been invoked prior to any invocation
 *            of tb_prune.
 *
 * Modifies:  The traceback tree
 *
 * Effect:    Prunes the traceback tree, iterating over the oldest
 *            unpruned frame in the tree if it is older than the maximum
 *            word length, MAXDWELL, deleting any path elements no longer
 *            having descendants, deleting any traceback nodes which no
 *            longer contain any path elements still having descendants,
 *            and deleting the frame itself if as a result it no longer
 *            contains any nodes.  It recursively deletes any ancestral
 *            path elements which lose all their descendants as a result
 *            of this pruning operation, as well as their nodes and
 *            frames as appropriate.
 *
 * Implementation:
 *
 * Uses:      tbp_childcount - get number of children of a tb node.
 *            tbp_decchildcount - decrement child count of a tb node.
 *            tbp_parent - get parent of a traceback node.
 *            tbp_successor - get next traceback node.
 *            tbp_setsuccessor - set traceback node's next pointer.
 *
 ************************************************************************/

  V_Void
tb_prune(VISE vise, V_Time time)
{
  tbn  tbnode;
  tbp  tbpath;
  tbn  parentnode;
  tbf  parentframe;
  tbf  matureframe;
  tbn  nextnode;
  tbp  nextpath;
  tbn  freedtbns;   /* List of free traceback nodes    */
  tbp  freedtbps;   /* List of free traceback path elements */

  freedtbns = vise->tbnPool.freeentries;
  freedtbps = vise->tbpPool.freeentries;

   /* Get the most recent mature frame of the traceback tree */
  if ((matureframe = vise->tbtree.tbmatureframe) == NULL) return;

   /* Make sure it is really mature enough to prune */
  if ((time - tbf_time(matureframe)) <= MAXDWELL) return;

   /* Get the list of nodes reached in this frame */
  nextnode = tbf_firstnode(vise->tbtree.tbmatureframe);

   /* Move to the next potentially mature frame */
  vise->tbtree.tbmatureframe = tbf_successor(matureframe);

   /* Iterate over nodes reached in this frame, pruning paths that have no surviving children, nodes to which no surviving paths lead,
    and the frame itself if no nodes survive. */
  while (nextnode != NULL) {
     /* Advance nextnode to its successor, saving a working copy */
    nextnode = tbn_successor(tbnode = nextnode);
     /* Get the best path to the working node */
    nextpath = tbn_bestpath(tbnode);
     /* Iterate over the paths to this node, pruning each back to the youngest path element with surviving children. */
    while (nextpath != NULL) {
       /* Advance nextpath to the next best path, saving a copy */
      nextpath = tbp_successor(tbpath = nextpath);
       /* Delete the path element if it has no surviving children */
      if (tbp_childcount(tbpath) == 0) {
         /* Delete the working path element */
        tbp_setsuccessor(tbpath, freedtbps);
        freedtbps = tbpath;
        vise->tbpPool.used--;
         /* Delete the node if no surviving paths reach it */
        if (!tbn_decpathcount(tbnode)) {
          tbn_setsuccessor(tbnode, freedtbns);
          freedtbns = tbnode;
          vise->tbnPool.used--;
           /* Delete the frame if no nodes are reached in it */
          if (!tbf_decnodecount(matureframe))
            tb_removeframe(vise, matureframe);
        }
         /* Trace back along this path, pruning ancestral path elements until one with surviving children is found. */
        while ((tbpath = tbp_parent(tbpath)) != NULL) {
          if (tbp_decchildcount(tbpath) != 0) break;
           /* Get parent and node of the working path element */
          parentnode = tbp_node(tbpath);
           /* Delete the working path element */
          tbp_setsuccessor(tbpath, freedtbps);
          freedtbps = tbpath;
          vise->tbpPool.used--;
           /* Delete path's node if no surviving paths reach it */
          if (!tbn_decpathcount(parentnode)) {
             /* Get the frame containing the node */
            parentframe = tbn_frame(parentnode);
             /* Delete the node */
            tbn_setsuccessor(parentnode, freedtbns);
            freedtbns = parentnode;
            vise->tbnPool.used--;
             /* Delete the frame if no nodes are reached in it */
            if (!tbf_decnodecount(parentframe))
              tb_removeframe(vise, parentframe);
          }
        } /* End of trace back along path */
      }
    } /* End of loop over path elements in nodes */
  } /* End of loop over nodes in mature frame */

   /* Save the updated free lists */
  vise->tbnPool.freeentries = freedtbns;
  vise->tbpPool.freeentries = freedtbps;
}

/* End of tb_prune() */

/* End of tb.c */
