#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_mem.h"
#include "visemsg.h"
#include "v_par.h"
#include "v_pat.h"
#include "v_syn.h"
#include "v_voc.h"

/**************************************************************************
 *
 * Name:      WARMSTART -- Initialize the VISE clusters
 *
 * Returns:   Status of reply message
 *
 * Requires:  The VISEINIT function must already have been called
 *
 * Modifies:  Everything
 *
 * Effect:    Initializes VISE data objects.  All data previously
 *            resident in VISE is lost.
 *
 * Implementation:
 *
 *            Initialize the MEM cluster, the VOC cluster, the SYN cluster,
 *            the PATH cluster and the Jin source.
 *
 * Uses:      voc_init()
 *            syn_init()
 *            amp_init()
 *            path_init()
 *            JINSRC_init()
 *            MSG_xxx()
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\WARMSTRT.C_V  $
 * 
 *    Rev 3.4   20 Mar 1992 13:12:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.3   08 Oct 1991 10:16:08   DCV
 * Now makes sure the feature extraction process is off before re-starting.
 * 
 *    Rev 3.2   02 Aug 1991 11:36:32   DCV
 * Installed new message system
 * 
 *    Rev 3.1   18 Jul 1991 16:28:22   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 14:05:08   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:20:16   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:37:44   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
WARMSTART(VISE vise)
{
  V_Err  status, vstatus;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);

  MSG_closeRead(vise->incoming, &status);

   /* Initialize the memory allocator */
  MEM_free(vise->memCfg, FAST_MEM);
  MEM_free(vise->memCfg, SLOW_MEM);

   /* Initialize the components which DO NOT require memory allocation */
  syn_init(vise);
  voc_init(vise);

   /* Initialize those which DO */
  if (JINSRC_init(vise->jinSource) && (path_init(vise)))
    vstatus = VISESUCCESS;
  else
    vstatus = OUTOFMEMORY;

  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _WARMSTART);
    MSG_writeInt(vise->outgoing, (V_Int) vstatus);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
