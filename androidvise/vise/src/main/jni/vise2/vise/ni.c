#include "pthr.h"
#include "vise.h"
#include "v_ni.h"
#include "v_tbp.h"
#include "v_wm.h"

/**************************************************************************
 *
 *	Name:			ni -- Node Instance Cluster
 *
 *	Description:
 *
 *					The node instance cluster implements the node instance
 *					objects, which contain the grammar-level node information
 *					during recognition.  A node instance object has a seed
 *					score, which is the score that words on arcs originating
 *					at that node inherit, a seed traceback node, which is also
 *					inherited and which henceforth refers to this node at this
 *					time (such that any path containing this will have gone
 *					through this node at this time), a node id number, and
 *					a list of the ni_numPaths best paths reaching this node
 *					at this time.  The path list is maintained as an ordered
 *					array of inherited path objects, each consisting of a
 *					score, a traceback node which refers to the instance of
 *					the grammar node and time at which the specified word's
 *					arc originated, and an id number for the word which
 *					resulted in that score.
 *
 *	Operations:
 *
 *		ni_nodeinit(node, nodeid) - Initilize a node and assign it an
 *			identification number.
 *
 *		nodeid = ni_id(node) - Get node identification number.
 *
 *		ni_putpathlist(node, tbpath) - Give node an inherited pathlist.
 *
 *		tbpath = ni_pathlist(node) - Get the inherited pathlist of the node.
 *
 *		ni_putseed(node, score, tbpath) - Assert new seed data for the node.
 *
 *		ni_getseed(node, &score, &tbpath) - Get the seed data for the node.
 *
 *		ni_setworstscore(node, score) -- Assert a new worst score for the
 *													node.
 *
 *		worstscore = ni_worstscore(node) -- Get the worst inherited score
 *														for the node.
 *
 *		ni_setseedscore(node, score) -- Assert a new seed score for the
 *													node.
 *
 *		seedscore = ni_seedscore(node) -- Get the seed score for the node.
 *
 *		seedpath = ni_seedpath(node) -- Get the seed path for the node.
 *
 *					ni		node;		--	A node instance
 *					V_NId	nodid;	--	Node id
 *					V_Int	score;	--	The score for a path to the node.
 *					tbn	tbnode;	--	Traceback node identifying the previous
 *											node and time
 *					tbp	tbpath;	--	Traceback path
 *					V_WId	wid;		--	The identification number of a word
 *
 *	$Log:   C:\USR\SRC\VISE\C\VCS\NI.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:45:28   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   29 Jan 1992 15:08:58   DCV
 * Changed basic types
 * 
 *    Rev 3.5   12 Nov 1991 16:22:04   DCV
 * Major revision which eliminates the inherited path array in favor of
 *   a list of inherited paths (tbps).  Eliminates the numpaths global,
 *   ni_insertpath(), ni_insertpaths(), ni_resetpaths(), ni_putinfo(),
 *   ni_getinfo(), and other functions which accessed elements in the
 *   ip array.  New functions ni_pathlist(), ni_putpathlist(),
 *   ni_setworstscore() and ni_worstscore() access the new fields which
 *   replace the old ip array.  ni_nodeinit() replaces ni_putid().
 * 
 * 
 *    Rev 3.4   20 Sep 1991 17:22:08   DCV
 * New version using DGNs to distinguish between paths.  Doesn't yet
 * distinguish between paths with same contents in different order.
 * Replaced ni_insertpath() (which becomes static) with ni_insertpaths().
 * 
 *    Rev 3.3   24 Jul 1991 14:15:12   DCV
 * Added code to deal with equivalence classes of words
 * 
 *    Rev 3.2   18 Jul 1991 16:16:48   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.1   12 Jun 1991 14:07:26   DCV
 * Rewritten to support N-best path reporting
 * 
 *    Rev 3.0   13 Sep 1990 13:51:58   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:11:36   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:23:30   DCV
 * Initial revision.
 *
 *************************************************************************/

/**************************************************************************
 *
 *	Name:			ni_nodeinit - Initialize a node instance
 *
 *	Calling Sequence:
 *
 *					ni_nodeinit(node, nodeid)
 *
 *					ni		node		--	A node instance object.
 *					V_NId	nodeid	--	A node identification number.
 *
 *	Returns:
 *
 *	Requires:	The node instance must not be NULL.
 *
 *	Modifies:	The node instance.
 *
 *	Effect:		Associates the specified identification number with the
 *					given node instance and NULLs its pathlist
 *
 *	Implementation:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef	NOMACROS
	V_Void
ni_nodeinit(ni node, V_NId nodeid)
	{
	node->niid = nodeid;
	node->nipathlist = NULL;
	node->niworstscore = V_INFINITY;
	node->niseedscore = V_INFINITY;
	node->niseedpath = NULL;
	}
#endif

/***************************************************************************
 *
 *	Name:			ni_id - Get node id number of node instance
 *
 *	Calling Sequence:
 *
 *					nodeid = ni_id(node)
 *
 *					ni 	 node 	-- A node instance object.
 *					V_NId nodeid	-- A node identification number.
 *
 *	Returns:		The identification number for the given  node  instance
 *					object.
 *
 *	Requires:	A node identification number must have previously	been
 *					asserted for this node with the ni_nodeinit function.
 *
 *	Modifies:
 *
 *	Effect:		Returns the node identification number  for	the  given
 *					node instance object.
 *
 *	Implementation:
 *
 *	Uses:			
 *
 **************************************************************************/

#ifdef	NOMACROS
	V_NId
ni_id(ni node)
	{
	return(node->niid);
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_putpathlist - Give the node an inherited pathlist.
 *
 *	Call:			ni_putpathlist(node, tbpath)
 *
 *					ni		node	--	A node instance object
 *					tbp tbpath	-- A list of traceback path elements.
 *
 *	Returns:		Nothing
 *
 *	Requires:	
 *
 *	Modifies:	The nipathlist field in the node instance object
 *
 *	Effect:		Sets the nipathlist field in the node to tbpath
 *
 *	Implementation:
 *
 *					Set node->nipathlist to tbpath
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	V_Void
ni_putpathlist(ni node, tbp tbpath)
	{
	node->nipathlist = tbpath;
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_pathlist(ni node) - Get the inherited pathlist of the node.
 *
 *	Call:			tbpath = ni_pathlist(node)
 *
 *					ni		node	--	A node instance object
 *					tbp tbpath	-- A list of traceback path elements.
 *
 *	Returns:		The list of inherited traceback path elements
 *
 *	Requires:	
 *
 *	Modifies:	Nothing
 *
 *	Effect:		Returns the list of inherited traceback path elements
 *
 *	Implementation:
 *
 *					Return node->nipathlist
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	tbp
ni_pathlist(ni node)
	{
	return(node->nipathlist);
	}
#endif

/***************************************************************************
 *
 *	Name:			ni_putseed - Put seed info in node instance
 *
 *	Calling Sequence:
 *
 *					ni_putseed(node, score, tbpath);
 *
 *					ni		node;		--	The node instance which
 *											receives a new seed.
 *					V_Int	score;	--	The new seed score it receives.
 *					tbp	tbpath;	--	The new traceback path it recieves.
 *
 *	Returns:
 *
 *	Requires:
 *
 *	Modifies:	The node instance.
 *
 *	Effect:		Replaces the given node instance object's current	seed
 *					score  and	traceback path with the specified score and
 *					traceback path.
 *
 *	Implementation:
 *
 *					Put specified score in node's seed-score field
 *					Put specified traceback path in tb-path field
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
	V_Void
ni_putseed(ni node, V_Int score, tbp tbpath)
	{
	node->niseedscore = score;
	node->niseedpath	= tbpath;
	}
#endif

/*************************************************************************
 *
 *	Name:			ni_getseed - Get seed info from node instance
 *
 *	Calling Sequence:
 *
 *					ni_getseed(node, score, tbpath);
 *
 *					ni 	  node;	-- A node instance object.
 *					V_Int	*score;	-- Its current score.
 *					tbp  *tbpath;	-- And its traceback path.
 *
 *	Returns:
 *
 *	Requires:	A seed must have been previously asserted for the given
 *					node with the function ni_putseed.
 *
 *	Modifies:
 *
 *	Effect:		The seed score and traceback path for the node instance
 *					object  is	returned.	This	is the information that a
 *					word will inherit if it is on an arc  which	originates
 *					at the given node.
 *
 *	Implementation:
 *
 *					Return the seed-score field of the given node instance object.
 * 				Return the seed-traceback path field of the given node
 *							instance object.
 *
 *	Uses:			
 *
 *************************************************************************/

#ifdef NOMACROS
	V_Void
ni_getseed(ni node, V_Int *pscore, tbp *ptbpath)
	{
	*pscore	= node->niseedscore;
	*ptbpath = node->niseedpath;
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_setworstscore - Set the worst inherited score of a node
 *
 *	Calling Sequence:
 *
 *					ni_setworstscore(node, score)
 *
 *					ni		node	--	A node instance object
 *					V_Int	score	--	The score of the worst path to the node
 *
 *	Returns:		Nothing
 *
 *	Requires:	
 *
 *	Modifies:	The worst inherited score in the node instance object
 *
 *	Effect:		Sets the niworstscore field in the node to score
 *
 *	Implementation:
 *
 *					Set node->niworstscore to score
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	V_Void
ni_setworstscore(ni node, V_Int score)
	{
	node->niworstscore = score;
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_worstscore -- Get the worst inherited score for the node
 *
 *	Calling Sequence:
 *
 *					score = ni_worstscore(node);
 *
 *					ni		node	--	A node instance object
 *					V_Int	score	--	A path score
 *
 *	Returns:		The worst inherited score for the given node
 *
 *	Requires:	The worst score must have been set previously by
 *					ni_nodeinit() or ni_setworstscore()
 *
 *	Modifies:
 *
 *	Effect:		Returns the worst inherited score for the given node.
 *
 *	Implementation:
 *
 *					Return the worstscore field of the given node instance
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	V_Int
ni_worstscore(ni node)
	{
		return(node->niworstscore);
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_setseedscore - Set the seed score of a node instance
 *
 *	Calling Sequence:
 *
 *					ni_setseedscore(node, score)
 *
 *					ni		node	--	A node instance object
 *					V_Int	score	--	The score for the path to the node
 *
 *	Returns:		Nothing
 *
 *	Requires:	
 *
 *	Modifies:	The niseedscore field in the node instance object
 *
 *	Effect:		Sets the niseedscore field in the node to score
 *
 *	Implementation:
 *
 *					Set node->niseedscore to score
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	V_Void
ni_setseedscore(ni node, V_Int score)
	{
	node->niseedscore = score;
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_seedscore -- Get the seed score for the node
 *
 *	Calling Sequence:
 *
 *					score = ni_seedscore(node);
 *
 *					ni		node	--	A node instance object
 *					V_Int	score	--	A path score
 *
 *	Returns:		The seed score for the given node
 *
 *	Requires:	A seed score must previously have been asserted for
 *					the given node with ni_setseedscore() or ni_putseed().
 *
 *	Modifies:
 *
 *	Effect:		Returns the seedscore for the given node.
 *
 *	Implementation:
 *
 *					Return seedscore field of given node object
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	V_Int
ni_seedscore(ni node)
	{
	return(node->niseedscore);
	}
#endif

/**************************************************************************
 *
 *	Name:			ni_seedpath -- Get the seed path for the node
 *
 *	Calling Sequence:
 *
 *					path = ni_seedpath(node);
 *
 *					ni		node	--	A node instance object
 *					tbp	path	--	A traceback path
 *
 *	Returns:		The seed path for the given node.
 *
 *	Requires:	A seed path must previously have been asserted for
 *					the given node with ni_setseedpath() or ni_putseed().
 *
 *	Modifies:
 *
 *	Effect:		Returns the seedpath for the given node.
 *
 *	Implementation:
 *
 *					Return seedpath field of given node object
 *
 *	Uses:			
 *
 ***************************************************************************/

#ifdef NOMACROS
	tbp
ni_seedpath(ni node)
	{
	return(node->niseedpath);
	}
#endif

