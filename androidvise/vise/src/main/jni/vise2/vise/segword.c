#include "pthr.h"
#include <stdlib.h> /* Needed to get div() and the div_t typedef */
#include "vbx.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_alg.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_voc.h"
#include "v_wm.h"


#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/**************************************************************************
 *
 * Name:      SEGMENTWORD -- Segment word and make templates
 *
 * Returns:   Status of reply message
 *
 * Requires:  The function WARMSTART must have been called before this
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                                  Type
 *
 *              word id                                   V_Uns
 *              word starting sequence number             V_SN
 *              word ending sequence number               V_SN
 *              training weighting factor                 V_Int
 *
 * Modifies:   The templates for the specified word.
 *
 * Effect:     The specified region of jin  data  is segmented  in a
 *             balanced way among the kernels of the given word. Then
 *             the template for each kernel is modified to reflect the
 *             new data.  The weighting factor determines the importance
 *             of the old template means in making the new template.
 *
 * Implementation:
 *
 *             get id for word
 *             get number of kernels
 *             get the weighting factor
 *             for each kernel,
 *               Determine how many jin frames should be assigned to it.
 *               Adjust its template using the assigned jin frames,
 *                 weighting it according to the weight.
 *             send VISESUCCESS
 *
 *             Exception handling:
 *
 *             if specified jin data does not exist
 *               send CANTREADFRAME
 *             if specified word does not exist
 *               send WORDNOTINVOCAB
 *             if word is a wildcard or word is uninitialized and weight nonzero
 *               send WORDHASNOTEM
 *             if jin duration exceeds MAXDWELL
 *               send WORDDURATIONTOOL0NG
 *             if jin duration is less than number of kernels in word
 *               send WORDDURATIONTOOSHORT
 *             if adjustem() fails
 *               send the error code it returns
 *
 *             The number of frames alloted to each kernel is
 *             essentially the quotient of the number of frames and
 *             the number of kernels in the word.  The allotment of
 *             frames to kernels is done so as to distribute
 *             throughout the word the kernels getting one more frame
 *             than some of the others.  This is done by having the
 *             number of frames for each successive kernel be the
 *             quotient resulting from a denominator of twice the
 *             number of kernels, and a numerator of twice the number
 *             of frames plus the preceding remainder, with the
 *             initially used remainder being the number of kernels.
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\SEGWORD.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:54:00   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   22 Nov 1991 10:42:44   DCV
 * Modified for new hybrid version of adjustTemplate()
 * 
 *    Rev 3.5   14 Aug 1991 16:35:00   DCV
 * Changed call to jin_canread()
 * 
 *    Rev 3.4   02 Aug 1991 11:30:38   DCV
 * Installed new message system
 * 
 *    Rev 3.3   18 Jul 1991 16:21:08   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   27 Feb 1991 11:06:44   DCV
 * Changed for floating-point weighting scheme
 * Changed flow for readability (to parallel trainwrd.c)
 * 
 *    Rev 3.1   10 Jan 1991 15:21:04   DCV
 * Consolidated template updating in a single call to adjustTemplate()
 * 
 *    Rev 3.0   13 Sep 1990 13:56:10   DCV
 * First version with individual header files
 * 
 *    Rev 1.2   27 Jul 1990 14:52:06   DCV
 * Includes <stdlib.h> to get the div_t typedef
 * 
 *    Rev 1.1   24 Jul 1990 14:15:36   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990  9:58:36   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
SEGMENTWORD(VISE vise)
{
  V_Uns   wid;             /* word id                             */
  V_SN    startSN;         /* number of first frame to segment    */
  V_SN    endSN;           /* number of last frame to segment     */
  V_SN    snReq, sn;       /* sequence number of current frame    */
  wm      word;            /* word model                          */
  km      kernel;          /* current kernel to process           */
  kt      ktrainer;        /* kernel training object              */
  V_Uns   weight;          /* weighting factor for template means */
  V_Int   duration;        /* length jin data                     */
  V_Int   numKernels;      /* number of kernels in the word model */
  V_Int   denominator;     /* denominator in division             */
  V_Uns   frameCount;      /* number of jin frames for a kernel   */
  V_Int   remainder;       /* remainder in division               */
  V_Int   wstatus;         /* word status                         */
  div_t   d, div();
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status;

   /* Get the command arguments */
  MSG_readUns(vise->incoming, &wid);
  MSG_readSN(vise->incoming, &startSN);
  MSG_readSN(vise->incoming, &endSN);
  MSG_readUns(vise->incoming, &weight);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

  if (!(word = voc_getword(vise, (V_WId) wid)))
    VISE_EXIT(WORDNOTINVOCAB);

  if (!JINSRC_canread(vise->jinSource, startSN, endSN))
    VISE_EXIT(CANTREADFRAME);

  if ((duration = (V_Int) JINSRC_numframes(vise->jinSource, &startSN, &endSN)) > MAXDWELL)
    VISE_EXIT(WORDDURATIONTOOLONG);

  if ((numKernels = wm_numkernels(word)) > duration)
    VISE_EXIT(WORDDURATIONTOOSHORT);

  wstatus = wm_status(word);

  if ((wstatus != TEMPLATES) && ((wstatus != UNINITIALIZED) || (weight != 0)))
    VISE_EXIT(WORDHASNOTEM);

  /* Initially, remainder is the number of kernels in the word model */
  remainder = numKernels;
  denominator = numKernels << 1;

  /* Iterate over all kernels in the word model */
  snReq = sn = startSN;
  for (kernel = wm_firstkernel(word), ktrainer = wm_firstkt(word); kernel != NULL; kernel = km_nextkernel(kernel)) {
    d = div((duration << 1) + remainder, denominator);
    frameCount = (V_Uns) d.quot;
    remainder = d.rem;

    /* Adjust the kernel's template with new data */
    if ((vise_status = adjustTemplate(vise, kernel, ktrainer, &snReq, frameCount, weight)) != VISESUCCESS)
      VISE_EXIT(vise_status);

    /* Update the observed min/max dwells */
    if (ktrainer) {
      kt_adjustobsdwells(ktrainer, frameCount);
      ktrainer = kt_nextkt(ktrainer);
    }

    /* Remember the SN, and get the next frame, regardless of SN */
    sn = snReq;
    snReq = -1;
  }
  VBX_DEBUG(if (sn != endSN) VBX_print("Segmentword::adjusttemplate: startSN = %ld, sn = %ld, endSN = %ld\n", startSN, sn, endSN));

  wm_setstatus(word, TEMPLATES);
  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _SEGMENTWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
