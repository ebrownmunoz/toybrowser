#include "pthr.h"
#include "vise.h"
#include "v_km.h"
#include "v_tem.h"

/**************************************************************************
 *
 *	Name:				km -- Kernel Model Cluster
 *
 *	Description:	The kernel model cluster implements the abstract kernel
 *						model objects of which word models are constructed. 
 *						A kernel model object contains dwell information
 *						indicating how long its associated sound may last and a
 *						template object which represents the sound itself.
 *
 *	Operations:
 *
 *		1.	km_template(kernel); -- Get template object from kernel.
 *
 *		2.	km_puttemplate(kernel, template); -- Put template object into kernel.
 *
 *		3.	km_dwells(kernel, &mindwell, &optdwell); -- Get minimum and
 *			optional dwell values from kernel.
 *
 *		4.	km_putdwells(kernel, mindwell, optdwell); --  Put new minimum
 *			and optional dwell values into kernel.
 *
 *		5.	km_nextkernel(kernel); -- Get the next kernel in kernel list.
 *
 *		6.	km_putnextkernel(kernel, nextkernel); -- Link to the next kernel.
 *
 *					km		kernel;			- Kernel model object
 *					tem	template;		- Template object
 *					V_Int	mindwell;		- Minimum dwell constraint.
 *					V_Int	optdwell;		- Optional dwell constraint.
 *					km		nextkernel;		- Kernel model object
 *
 *	Computer:	TMS320C30	(6000 and 7000)
 *
 * Unit Test:	kmt.ct
 *
 *	$Log:   C:\USR\SRC\VISE\C\VCS\KM.C_V  $
 * 
 *    Rev 3.3   20 Mar 1992 12:40:20   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.2   29 Jan 1992 14:51:56   DCV
 * Changed basic types
 * 
 *    Rev 3.1   18 Jul 1991 16:14:16   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 13:47:28   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:09:00   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:11:08   DCV
 * Initial revision.
 *
 **************************************************************************/

/***************************************************************************
 *
 *	Name:			km_template - Get template from kernel model
 *
 *	Calling Sequence:
 *
 *					template = km_template(kernel);
 *
 *					km	 kernel;		--	given kernel model.
 *					tem template;	--	associated template of kernel.
 *
 *	Returns:		the template object associated with the given kernel object.
 *
 *
 *	Requires:	the given kernel should have	a	template  before	this
 *					function  is  invoked  upon  it.   See  description  of
 *					km_puttemplate  for  more   information	 about	placing
 *					templates in kernels.
 *
 *	Modifies:
 *
 *	Effect:		Returns the template object  associated  with  a  given
 *					kernel model object by the function km_puttemplate.	This
 *					does not remove the template object from the kernel.
 *
 *	Implementation:
 *
 *					Return template associated with given kernel
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS
	tem
km_template(km	kernel)
	{
	return(&(kernel->kmtemplate));		/*	return kernel's template			*/
	}
#endif

/**************************************************************************
 *
 *	Name:			km_puttemplate -- Put template into kernel
 *
 *	Calling Sequence:
 *
 *					km_puttemplate(kernel, template);
 *
 *					km  kernel;		--	kernel which gets a new template.
 *					tem template;	--	the template which the kernel gets.
 *
 *	Returns:
 *
 *	Requires:
 *
 *	Modifies:	kernel
 *
 *	Effect:		The given template object is associated with the given
 *					kernel model object.  Subsequent invocation of the
 *					function km_template will return the given template.
 *
 *	Implementation:
 *
 *					Associate given template with given kernel
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef UNUSED
#ifdef	NOMACROS
	V_Void
km_puttemplate(km	kernel, tem template)
	{
	kernel->kmtemplate = *template; /* assign template object to kernel */
	}
#endif
#endif

/**************************************************************************
 *
 *	Name:			km_dwells -- Get dwell values from kernel model
 *
 *	Calling Sequence:
 *
 *					km_dwells(kernel, mindwell, optdwell);
 *
 *					km		 kernel;		--	kernel to be accessed for information.
 *					V_Int *mindwell;	--	minimum dwell value constraint.
 *					V_Int *optdwell;	--	optional dwell value constraint.
 *
 *	Returns:
 *
 *	Requires:	Minimum and optional dwell values must already be
 *					established for the given kernel using the function
 *					km_putdwells or else the results returned will be
 *					meaningless.
 *
 *	Modifies:	*mindwell, *optdwell
 *
 *	Effect:		This function returns the minimum and optional dwell
 *					values  associated  with  the given kernel model object
 *					via the function km_putdwells.
 *
 *	Implementation:
 *
 *					return minimum and optional dwell values associated
 *					with kernel
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef	NOMACROS
	V_Void
km_dwells(km kernel, V_Int *mindwell, V_Int *optdwell)
	/* km		 kernel		-	kernel to be accessed for information.	*/
	/* V_Int *mindwell	-	minimum dwell value constraint.			*/
	/* V_Int *optdwell	-	optional dwell value constraint.			*/
	{
	*mindwell = kernel->kmmindwell; /* mindwell and optdwell values assigned*/
	*optdwell = kernel->kmoptdwell; /* to arguments passed						*/
	}
#endif

/**************************************************************************
 *
 *	Name:			km_putdwells -- Associate dwells with kernel
 *
 *	Calling Sequence:
 *
 *					km_putdwells(kernel, mindwell, optdwell);
 *
 *					km		 kernel;		--	Kernel in which to store
 *												the information.
 *					V_Int mindwell;	--	Minimum dwell time constraint.
 *					V_Int optdwell;	--	Optional additional dwell time constraint.
 *
 *	Returns:		V_Void
 *
 *	Requires:
 *
 *	Modifies:	kernel
 *
 *	Effect:		Stores the minimum dwell and optional additional  dwell
 *					time constraint information in the given kernel.
 *
 *	Implementation:
 *
 *					Associate dwell info with kernel
 *
 *	Dependency:
 *
 *	Uses:
 *
 ************************************************************************/

#ifdef	NOMACROS
	V_Void
km_putdwells(km kernel, V_Int mindwell, V_Int optdwell)
	/* km		 kernel	-	Kernel in which to store the information.	*/
	/* V_Int mindwell	-	Minimum dwell time constraint.				*/
	/* V_Int optdwell	-	Optional additional dwell time constraint.*/
	{
	kernel->kmmindwell = mindwell;	/*	assign mindwell and optdwell args	*/
	kernel->kmoptdwell = optdwell;	/*	to kernel									*/
	}
#endif

/***************************************************************************
 *
 *	Name:			km_nextkernel - Get next kernal from kernel model
 *
 *	Calling Sequence:
 *
 *					nextkernel = km_nextkernel(kernel);
 *
 *					km	kernel;		--	the given kernel model.
 *					km	nextkernel;	--	the next kernel model.
 *
 *	Returns:		The next kernel model in the kernel model list.
 *
 *
 *	Requires:	The next kernel field must have been set with a call to
 *					km_putnextkernel().
 *
 *	Modifies:
 *
 *	Effect:		Returns the next kernel model in the list of kernel models
 *					containing the current one.  If the current model is the
 *					last in the list, returns NULL.
 *
 *	Implementation:
 *
 *					Return kmnextkernel field from the given kernel
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS
	km
km_nextkernel(km	kernel)
	{
	return(kernel->kmnextkernel);		/*	return nextkernel	*/
	}
#endif

/**************************************************************************
 *
 *	Name:			km_putnextkernel -- Put nextkernel into kernel
 *
 *	Calling Sequence:
 *
 *					km_putnextkernel(kernel, nextkernel);
 *
 *					km kernel;		--	kernel which gets a new nextkernel.
 *					km nextkernel;	-- the kernel to link in.
 *
 *	Returns:
 *
 *	Requires:
 *
 *	Modifies:	kernel
 *
 *	Effect:		The nextkernel is appended to the list following the
 *					current one.  Whatever followed previously is lost.
 *
 *	Implementation:
 *
 *					Set the kmnextkernel field in kernel to nextkernel.
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef UNUSED
	V_Void
km_putnextkernel(km	kernel, tem nextkernel)
	{
	kernel->kmnextkernel = nextkernel; /* link nextkernel to kernel */
	}
#endif

/* End of km.c */
