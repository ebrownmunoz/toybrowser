#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "visemsg.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/**************************************************************************
 *
 * Name:      SETTIMEOUT -- Set the maximum frame count for recognition
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                       Type
 *
 *              Maximum frame count            V_ULong
 *
 * Modifies:  The maximum frame count in the VISE descriptor
 *
 * Effect:    Sets the maximum frame count in the VISE descriptor
 *
 * Implementation:
 *
 *            Get the frame count from the command message
 *            Set the frame count in the VISE descriptor
 *
 * Uses:      MSG_xxx -- message routines
 *
 ***************************************************************************/

  V_Err
SETTIMEOUT(VISE vise)
{
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  /* Get the maximum frame count from the command message */
  if (!MSG_readULong(vise->incoming, &vise->maxFrames))
    VISE_EXIT(MESSAGETOOSHORT);

  VBX_DEBUG(VBX_print("SETTIMEOUT() set vise->maxFrames = %u\n", vise->maxFrames));

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _SETTIMEOUT);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
