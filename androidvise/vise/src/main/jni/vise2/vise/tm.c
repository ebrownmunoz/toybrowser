#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include <stdlib.h>
#include "v_add.h"
#include "v_tem.h"
#include "v_tm.h"
#include "v_wm.h"

/*****************************************************************************
 *
 *  Name:     tm_init() -- Initialize the template matcher (per frame)
 *
 *  Call:     tm_init(vise, jinframe, time, oldbestscore);
 *
 *              VISE    vise;
 *              jin     jinframe;
 *              time_t  time;
 *              int_t   oldbestscore;
 *
 *  Returns:  nothing
 *
 *  Requires:
 *
 *  Modifies: vise->temMatcher
 *
 *  Effect:  Initializes the template matcher with the given jin data.
 *           The time is saved and used to assure that no template is
 *           matched more than once for a given frame. The oldbestscore
 *           is used to normalize the template match costs.
 *
 *  Implementation:
 *
 *****************************************************************************/

  V_Void
tm_init(VISE vise, jin jinframe, V_Time time, V_Int oldbestscore)
{
  vise->temMatcher.time = time;
  vise->temMatcher.oldbestscore = -oldbestscore;
  vise->temMatcher.status = TEMPLATES;
  vise->temMatcher.globalmin = V_INFINITY;
  vise->temMatcher.jinframe = * jinframe;
}

/******************************************************************************
 *
 *  Name:     tm_setstatus() -- Set the status of the template matcher
 * 
 *  Call:     tm_setstatus(vise, status);
 *
 *              VISE   vise;
 *              V_Int  status;
 *
 *  Returns:  nothing
 *
 *  Requires:
 *
 *  Modifies: vise->temMatcher.status
 *
 *  Effect:   Sets the status of the template in the template matcher.
 *
 *  Implementation:
 *
 *****************************************************************************/

#ifdef UNUSED
  V_Void
tm_setstatus(VISE vise, V_Int status)
{
  vise->temMatcher.status = status;
}
#endif

/******************************************************************************
 *
 *  Name:     tm_globalmin(
 *
 *  Call:     min = tm_globalmin(vise);
 *
 *              V_Int  min; -- the current global minimum score
 *              VISE   vise;
 *
 *  Returns:  globalmin
 *
 *  Requires:
 *
 *  Modifies: vise->temMatcher.status
 *
 *  Effect:   Returns the minimum cost found for this jin frame so far.
 *
 *  Implementation:
 *
 *****************************************************************************/

  V_Int
tm_globalmin(VISE vise)
{
  return(vise->temMatcher.globalmin);
}

/******************************************************************************
 *
 *  Name:     tm_match() -- Distance between template and jin frame
 *
 *  Call:     cost = tm_match(vise, template);
 *
 *              V_Int  cost;
 *              VISE   vise;
 *              tem    template;
 *
 *  Returns:  The cost of matching the template with the current frame
 *
 *  Requires: tm_init() must have been called with the current frame data
 *
 *  Modifies: vise->temMatcher.globalmin
 *
 *  Effect:   This code calculates a distance (or cost) of matching the
 *            current acoustic frame (jin data) and the specified acoustic
 *            template.  The metric used is the sum of the absolute
 *            differences between corresponding jin and template parameters.
 *
 *  Implementation:
 *
 *****************************************************************************/

  V_Int
tm_match(VISE vise, tem template)
{
  V_Int   index;
  V_Long   cost;
  jinel * jinptr;
#if (VBYTESIZE != 1)
  jinr    temjin;
#endif
  jinel * temptr;
  V_Uns   relcost;
  V_Uns   abscost;
  V_Int   limit;

   /* If the template is not a wildcard, get the cost of the match */
  if (vise->temMatcher.status == TEMPLATES) {

     /* If we've already done this match, just return the cost already stored in the template */
    if (vise->temMatcher.time == template->temtime) {
      return(template->temcost);

     /* Otherwise, compute the cost now */
    } else {
       /* Set the template's time to the matcher's time */
      template->temtime = vise->temMatcher.time;
       /* Compute the sum of absolute values of differences */
      cost = vise->temMatcher.oldbestscore;
#if (VBYTESIZE == 1)
       /* jinpack is already unpacked, so do nothing */
      temptr = (&template->temparam.jin)->jincomponents;
#else
      jin_from_jinpack(&temjin, &template->temparam.jin);
      temptr = temjin.jincomponents;
#endif
      jinptr = (vise->temMatcher.jinframe).jincomponents;
      for (index = NUMCOMPONENTS; index--;) {
       if (*temptr > *jinptr)
        cost = (cost + *temptr++) - *jinptr++;
       else
        cost = (cost - *temptr++) + *jinptr++;
       }
       /* Set the template's matching cost */
      if (cost > V_INFINITY) cost = V_INFINITY;
      template->temcost = cost;
       /* Update globalmin */
      if (cost < vise->temMatcher.globalmin)
       vise->temMatcher.globalmin = cost;
       /* Return the newly computed cost */
      return(cost);
    }

   /* Otherwise, compute the wildcard cost */
  } else {
    tem_getwildcard(template, &relcost, &abscost);
     /* Compute raw wildcard cost (relative wc + globalmin) */
    cost = add_inf((V_Int) relcost, vise->temMatcher.globalmin);
     /* Compute hard limit on wildcard cost (absolute wc) */
    limit = add_inf((V_Int) abscost, vise->temMatcher.oldbestscore);
     /* Final wildcard cost is the minimum of the two */
    return(cost > limit ? limit : cost);
  }
}

