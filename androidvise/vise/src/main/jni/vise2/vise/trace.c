#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_alg.h"
#include "v_div.h"
#include "v_pat.h"
#include "v_tbp.h"

/**************************************************************************
 *
 * Name:        Trace -- Trace back along a path through the grammar
 *
 * Description: The traceback package contains routines which perform
 *              high-level operations on the traceback object.
 *
 * Operations:  trace() - trace back to record recognized utterance.
 *
 *              tracelength() - trace back to count the words on a path.
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\TRACE.C_V  $
 * 
 *    Rev 3.5   20 Mar 1992 13:02:20   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.4   29 Jan 1992 15:28:18   DCV
 * Changed basic types
 * 
 *    Rev 3.3   20 Sep 1991 09:46:42   DCV
 * Added code to compute the absolute path costs from the renormalized
 *  costs now stored in tbps, using best path sums from tbfs.
 * 
 *    Rev 3.2   01 Aug 1991 17:17:28   DCV
 * Added call to path_flush() in trace()
 * Added tracelength() function
 * 
 *    Rev 3.1   12 Jun 1991 14:49:18   DCV
 * Rewritten for new traceback tree structure to support N-best path and
 *   globalmin reporting.
 * 
 *    Rev 3.0   13 Sep 1990 13:59:54   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:17:26   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:26:12   DCV
 * Initial revision.
 *
 **************************************************************************/

/**************************************************************************
 *
 * Name:      trace -- Perform forced traceback on a path
 *
 * Call:      outcome = trace(vise, termnode)
 *
 *            VISE vise;     -- A VISE descriptor
 *            tbp  termnode; -- Traceback path element that terminates utterance
 *            V_Bool outcome;
 *
 * Returns:   TRUE if traceback is successful and FALSE if traceback
 *            exceeds the capacity of the path object.
 *
 * Requires:  tb_init() must have been invoked prior to any invocation of trace().
 *
 * Modifies:  the path object
 *
 * Effect:    Performs a forced traceback operation through the
 *            traceback tree object starting at the given path element,
 *            which is assumed to be the final element in a path through
 *            the utterance.  This "reads out" the results of the dynamic
 *            programming part of the recognition process.  Modifies the
 *            path object such that it contains the word id numbers,
 *            average cost per frame, average global minimum cost, and
 *            starting and ending sequence numbers for each word in the
 *            utterance as recognized by the dynamic programming.
 *
 * Implementation:
 *
 *            WHILE current path element has a parent DO
 *              Get information from the current path element;
 *              Compute the word's score and duration;
 *              Insert the word id, starting SN, endingSN,
 *                average cost per frame in the word and
 *                average globalmin per frame in the word
 *                into the path;
 *              IF path insertion fails THEN
 *                RETURN FALSE;
 *              END;
 *              Current path element = parent;
 *            END;
 *            RETURN TRUE;
 *
 * Uses:      path_flush  - Empty the path object
 *            path_push   - Insert info into path object
 *            tbp_info  - Get traceback path element information
 *            tbp_sn    - Sequence number of a traceback path element
 *            tbp_cost  - Cost of a traceback path element
 *            tbp_globalmin - Accumulated global min for a path
 *            tbp_parent  - Parent of a traceback path element
 *
 **************************************************************************/

  V_Bool
trace(VISE vise, tbp path)
{
  V_Time  starttime;   /* The starting frame index of the word */
  V_Time  endtime;     /* The ending frame index of the word */
  V_Time  wordlength;  /* The length of the word in frames */
  V_SN    startsn;     /* The starting SN of the word */
  V_SN    endsn;       /* The ending SN of the word */
  V_Int   cost;        /* Cost of path to this path element */
  tbn     node;        /* Traceback node containing this path */
  V_WId   wid;         /* Word identification number */
  tbp     parent;      /* Parent of current path element */
  V_ULong dgn;         /* DGN of path to this element */
  V_Uns   length;      /* Length of path to this element */

   /* Make sure the path stack is empty */
  path_flush(vise);

   /* Iterate back through the list of ancestral traceback path elements,
      stopping when an element with no parent is reached */
  while (tbp_parent(path) != NULL) {
     /* Get information about the current traceback path element */
    tbp_info(path, &node, &cost, &wid, &parent, &dgn, &length);
    starttime = tbp_time(parent);
    startsn = tbp_sn(parent);
    endtime = tbp_time(path);
    endsn = tbp_sn(path);
    wordlength = endtime - starttime;

     /* Calculate the average cost per frame for this word:

           cost of path to path element - cost of path to parent
      cost = -----------------------------------------------------
              time of path element - time of parent   */

     /* Push the recognized word onto the path */
    if (!path_push(vise, wid, (V_Uns) wordlength, startsn, endsn - 1,
                   rndiv32((V_Long) cost + tbp_sumbestscores(path) - (V_Long) tbp_cost(parent) - tbp_sumbestscores(parent), wordlength),
                   rndiv32(tbp_globalmin(path) + tbp_sumbestscores(path) - tbp_globalmin(parent) - tbp_sumbestscores(parent), wordlength)))
      return (FALSE);

    path = parent;
  }
  return (TRUE);
}

/**************************************************************************
 *
 * Name:      tracelength -- Count the words in a path
 *
 * Call:      numwords = tracelength(termnode)
 *
 *            tbp   termnode; -- Traceback path element terminating the utterance
 *            V_Int numwords; -- The number of words in the path
 *
 * Returns:   The number of words on the path
 *
 * Requires:  tb_init() must have been invoked prior to any invocation
 *            of tracelength().
 *
 * Modifies:  numwords
 *
 * Effect:    Performs a forced traceback operation through the
 *            traceback tree object starting at the given path element,
 *            which is assumed to be the final element in a path through
 *            the utterance.  This "reads out" the results of the dynamic
 *            programming part of the recognition process, counting the
 *            number of words on the path, but does not modify the path
 *            object.
 *
 * Implementation:
 *
 *            Set the count of the number of words in the path to zero.
 *            WHILE current path element has a parent
 *              Increment the word count.
 *              Current path element = parent.
 *            RETURN the word count.
 *
 * Uses:      tbp_parent - Parent of a traceback path element.
 *
 **************************************************************************/

  V_Int
tracelength(tbp path)
{
  V_Int numWords = 0; /* Count of the number of words on the path */

   /* If the path is not null, count back through the list of ancestral
      traceback path elements, stopping when an element with no parent
      is reached */
  if (path != NULL)
    while ((path = tbp_parent(path)) != NULL)
      numWords++;

  return(numWords);
}

/* End of trace.c */

