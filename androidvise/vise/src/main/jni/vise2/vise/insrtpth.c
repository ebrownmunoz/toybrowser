#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_alg.h"
#include "v_ni.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:   insertpaths - Merge source node paths into destination node
 *
 * Calling Sequence:
 *
 *     outcome = insertpaths(vise, dstni, score, srcpath, wordmodel, worstscore, maxpaths);
 *
 *     V_Bool    outcome; -- TRUE or FALSE
 *                  VISE          vise; -- The VISE descriptor
 *     V_Int * worstscore; -- The score of the worst path now in the destination node's path list
 *     ni           dstni; -- The node instance object which gets the resulting merged path list
 *     V_Int        score; -- The score of the best new path through the source and the given word
 *     tbp        srcpath; -- The best traceback path element in the source node's path list
 *     wm   wordmodel; -- The word model for the last word on the paths through the source
 *     V_Uns   maxpaths; -- The maximum number of alternative paths to retain in the path list
 *
 * Returns:  TRUE if insertion succeeds (i.e., there is enough memory
 *     for the new tbf's required);  FALSE otherwise.
 *
 * Requires: The caller should assure that score is better than the
 *     score of the worst path already in the destination node's
 *     inherited path list.
 *
 * Modifies: The path list in the destination node instance object.
 *
 * Effect:  Merges the path information from the pathlist of the source
 *     node, extending the paths through the given word, into the
 *     destination node's inherited path list, maintaining that
 *     list in order of increasing cost and assuring that no two
 *     inherited paths have the same DGN.
 *
 * Implementation:
 *
 * Dependency: 
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\INSRTPTH.C_V  $
 * 
 *    Rev 1.4   07 Feb 1994 15:25:58   DCV
 * srcpathDGN and dstpathDGN are now arrays
 * of V_ULong not V_WId. Previous didn't work
 * on Intel 16-bit platform in multiple result
 * mode.
 * 
 *    Rev 1.3   20 Mar 1992 12:38:00   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.2   29 Jan 1992 14:37:00   DCV
 * Changed basic types
 * 
 *    Rev 1.1   13 Nov 1991 11:02:36   DCV
 * Changed MAX_PATHS to MAXPATHS
 * 
 *    Rev 1.0   13 Nov 1991 10:55:30   DCV
 * Initial Version
 * 
 *************************************************************************/

 V_Bool
insertpaths(VISE vise, ni dstni, V_Int score, tbp srcpath, wm wordmodel, V_Int *worstscore, V_Uns maxpaths)
 {
 tbp         dstpath;             /* Current path to the destination node   */
 tbp         nextdstpath;            /* Next best path to destination node      */
 V_Int     i;
 tbp      tbpath;           /* A traceback path element      */
 V_Int  numpaths;          /* Current # of paths in new dest path list  */
 V_Int  numfromsrc;          /* # of paths from source list in new list  */
 V_Int  numfromdst;          /* # of paths from old dest list in new list */
 V_ULong     srcpathDGN[MAXPATHS]; /* DGNs for paths from src         */
 V_ULong     dstpathDGN[MAXPATHS]; /* DGNs for paths from old dst list          */
 V_ULong     pathDGN;       /* The degenerate Goedel number of a path    */
 V_Int  deltascore;          /* Incremental cost of last word    */
 V_Uns  pathInc;       /* Increase in path length (0 or 1)    */
 V_Uns  wordClass;          /* The class prime of the last word    */
 V_WId  wordID;           /* The word ID of the last word     */

 deltascore = score - tbp_cost(srcpath);

 if (wordmodel == NULL) {
  wordClass = 1;
  wordID = NULLWORDID;
  pathInc = 0;
  }
 else {
  wordClass = wm_class(wordmodel);
  wordID = wm_id(wordmodel);
   /* If the word linking the source and destination nodes is
    "significant", extending a path from the source to the
    destination node will lengthen that path by one word;
    otherwise, the path will not lengthen. */
  pathInc = (V_Uns)(wordClass > 1);
  }

 dstpath = ni_pathlist(dstni);

  /* If the best path through the source and the given word is
   better than the best path in the old destination pathlist,
   insert it as the new best path to the destination: */
 if (score < (dstpath ? tbp_cost(dstpath) : V_INFINITY)) {
   /* Compute the DGN of the path through the source */
  pathDGN = (tbp_DGN(srcpath) * wordClass) & 0x00FFFFFF;
   /* If new source and old destination paths have different
    DGNs, insert the new source path as a new path at the
    head of the destination path list;  otherwise, replace
    the old destination path with the new source path. */ 
  if ((dstpath == NULL) || (tbp_DGN(dstpath) != pathDGN)) {
   if ((tbpath = tbp_alloc(vise)) == NULL) return(FALSE);
   tbp_setsuccessor(tbpath, dstpath);
   ni_putpathlist(dstni, tbpath);
   dstpath = tbpath;
   }
  tbp_setinfo(dstpath, NULL, score, wordID, srcpath, pathDGN,
             tbp_length(srcpath) + pathInc);
   /* Point source path pointer at second best path to source */
  srcpath = tbp_successor(srcpath);
   /* The best new destination path comes from the source */
  srcpathDGN[0] = pathDGN;
  numfromsrc = 1;
  numfromdst = 0;
  }
  /* Otherwise, the best path to the destination remains the same */
 else {
  dstpathDGN[0] = tbp_DGN(dstpath);
  numfromsrc = 0;
  numfromdst = 1;
  }

  /* At this point, dstpath is the first tbp in the destination
   node's list (and will definitely remain so), and srcpath is
   the second or first tbp in the source node's list, depending
   on whether the first did or did not become the first in the
   destination list.  In either case, the list of paths to the
   destination node contains exactly one validated path at this
   point, be it the best path from the source or the old best
   destination path */
 numpaths = 1;

  /* Merge destination paths and paths from source through word
   into the new destination path list: */
 nextdstpath = tbp_successor(dstpath);
 while (numpaths < maxpaths) {

   /* If there are no more paths to the destination node */
  if (nextdstpath == NULL) {
    /* If there are no more paths to the source node */
   if (srcpath == NULL) break;
   }
   /* Otherwise, there is another path to the destination node */
  else {
    /* If there are no more paths to the source node, or if
     the next best path through the source is no better
     than the next destination path, see if the latter
     should be retained */
   if ((srcpath == NULL) ||
    (tbp_cost(srcpath) + deltascore >= tbp_cost(nextdstpath))) {
     /* CASE OF POSSIBLE INSERTION FROM DESTINATION pathlist:
      See if the DGN of the next destination path is the
      same as the DGN of a source path already added to
      the destination path list */
    pathDGN = tbp_DGN(nextdstpath);
    for (i = 0; i < numfromsrc; i++)
     if (pathDGN == srcpathDGN[i]) break;
     /* If not, retain the next destination path */
    if (i == numfromsrc) {
     dstpathDGN[numfromdst++] = pathDGN;
      /* Make it the new destination path */
     dstpath = nextdstpath;
      /* Bump the destination path count */     
     numpaths++;
     }
     /* Otherwise, discard it */
    else {
     tbp_setsuccessor(dstpath, tbp_successor(nextdstpath));
     tbp_free(vise, nextdstpath);
     }
     /* Advance to the next destination path */
    nextdstpath = tbp_successor(dstpath);
    continue;
    }
   }

   /* CASE OF POSSIBLE INSERTION FROM SOURCE pathlist:
    At this point, it is known that there is another path to the
    source node, one which is better than the next path (if any)
    to the destination.  Find out whether or not to extend and
    insert it by seeing if its extended DGN is the same as the
    DGN of a destination path already in the new destination
    path list */
  pathDGN = (tbp_DGN(srcpath) * wordClass) & 0x00FFFFFF;
  for (i = 0; i < numfromdst; i++)
   if (pathDGN == dstpathDGN[i]) break;
   /* If not, insert a copy of the extended source path
    into the new destination path list */
  if (i == numfromdst) {
    /* Add source path DGN to source path DGN array */
   srcpathDGN[numfromsrc++] = pathDGN;
    /* Insert source path into the destination pathlist */
   if ((tbpath = tbp_alloc(vise)) == NULL) return(FALSE);
   tbp_setsuccessor(tbpath, nextdstpath);
   tbp_setsuccessor(dstpath, tbpath);
   dstpath = tbpath;
   tbp_setinfo(dstpath, NULL, tbp_cost(srcpath) + deltascore,
            wordID, srcpath, pathDGN,
            tbp_length(srcpath) + pathInc);
    /* Bump the destination path count */     
   numpaths++;
   }
   /* Advance to the next source path */
  srcpath = tbp_successor(srcpath);

  } /* End of loop over paths */

  /* If the destination list is full, discard the rest of it */
 if (numpaths == maxpaths) {
  nextdstpath = tbp_successor(dstpath);
   /* Terminate new destination path list with current dstpath */
  tbp_setsuccessor(dstpath, NULL);
   /* Return the score of the worst path */
  *worstscore = tbp_cost(dstpath);
   /* Discard the rest of the new destination path list */
  while ((dstpath = nextdstpath) != NULL) {
   nextdstpath = tbp_successor(dstpath);
   tbp_free(vise, dstpath);
   }
  }
 else
  *worstscore = V_INFINITY;

 return(TRUE);

 } /* End of insertpaths() */
