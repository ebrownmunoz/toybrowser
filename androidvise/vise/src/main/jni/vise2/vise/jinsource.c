#include "pthr.h"
#include <stdlib.h>
#include "sn.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_par.h"
#include "v_jin.h"
#include "v_km.h"
#include "v_tem.h"
#include "visemsg.h"

/* OOPS Inclusions */
#include "iface.h"
#include "iattr.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "jinmsg.h"
#include "objpad.h"
#include "objsnk.h"
#include "objsrc.h"
#include "queue.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

/**************************************************************************
 *
 * Name:         jin -- Jin source cluster
 *
 * Description:  The jin source cluster provides methods for accessing
 *               and buffering the incoming jin frames
 *
 * Operations:
 *
 *      jinsrc = JINSRC_create(vise) -- Create a jin source
 *
 *     success = JINSRC_init(jinsrc) -- Initialize a jin source
 *
 *               JINSRC_destroy(jinsrc) -- Destroy a jin source
 *
 *     success = JINSRC_extract(jinsrc, &sn, jin2parms, &exception) -- Extract
 *                 a frame from the queue if the frame is there; fail
 *                 otherwise.
 *
 *     success = JINSRC_insert(jinsrc, &sn, jin2parms, &exception)  -- Insert
 *                 a frame into the queue
 *
 *      status = JINSRC_pad(jinsrc) -- Initialize the frame padder
 *
 *     success = JINSRC_read(jinsrc, &sn,  parameters, &overwritten, &exception)
 *                 Read a frame of acoustic parameters from the jin buffer,
 *                 causing it to acquire the frame from its source if it does
 *                 not already contain the frame.
 *
 *     success = JINSRC_canread(jinsrc, startSN, endSN) --  Are there frames
 *                 of acoustic parameters in the frame buffer for the
 *                 specified range of sequence numbers?
 *
 *   numframes = JINSRC_numframes(jinsrc, &startSN, &endSN) -- The number of
 *                 frames in the frame buffer that have sequence numbers
 *                 in the specified range (startSN <= sn <= endSN).
 *
 *     success = JINSRC_resetQueue(jinsrc) -- flush out the frame queue
 *
 *     success = JINSRC_writeEXC(jinsrc, exception) -- write an exception
 *                 frame into the JINSRC
 *
 *     success = JINSRC_writeFrame(jinsrc, message) -- write a Jin frame
 *                 from a message into the JINSRC
 *
 *     JINSRC    jinsrc      -- a jin source descriptor
 *     VISE      vise        -- the VISE descriptor
 *     V_SN      sn          -- sequence number of desired frame
 *     V_SN      startSN     -- starting sequence number of a region
 *     V_SN      endSN       -- ending sequence number of a region
 *     jin       parameters  -- frame of acoustic parameters
 *     jin2      jin2parms   -- frame of acoustic parameters in message
 *     V_Uns     overwritten -- number of frames overwritten
 *     V_Err     exception   -- VISE error code
 *     V_Bool    success     -- TRUE or FALSE
 *     V_SN      numframes   -- the number of frames in the region
 *     VBXMSG    message     -- a VBX message port
 *
 **************************************************************************/

/* Jin Source Descriptor */
typedef struct jinsrc_s {
  VISE         vise;
  IGEN         igenJinAttr;
  IJINATTR     iJinAttr;
  IGEN         igenJinMsg;
  ISRC         isrcJinMsg;
  IJINMSG      iJinMsg;
  IGEN         igenSilPad;
  ISRC         isrcSilPad;
  IGEN         igenFrameQ;
  IQUEUE       iqueueFrameQ;
  IQSINK       iqsinkFrameQ;
  ISRC         isrcFrameQ;
  IGEN         igenEngine;
  IOBJSINK     iEngine;
  IGEN         igenFrameQSink;
  IOBJSINK     iFrameQSink;
  IGEN         igenJinSource;
  IOBJSOURCE   iJinSource;
  ISRC         isrcJinSource;
  IGEN         igenJinMsgAttr;
  IJINMSGATTR  iJinMsgAttr;
  IGEN         igenSilPadAttr;
  IPADATTR     iSilPadAttr;
  CLASS        silPadFrames;
  OBJT         silPadFrame;
  IGEN         igenFrameQAttr;
  IQATTR       iFrameQAttr;
} jinsrc_t;


/***************************************************************************
 *
 * Name:      JINSRC_create - Create a jin source
 *
 * Returns:   A jin source descriptor, or NULL if none can be created
 *
 * Requires:  A valid VISE descriptor
 *
 * Modifies:
 *
 * Effect:    Creates and initializes a jin source
 *
 * Uses:
 *
 **************************************************************************/

  JINSRC
JINSRC_create(VISE vise)
{
  JINSRC  jinsrc = (JINSRC) vise->memory->Calloc(vise->memory, 1, sizeof(jinsrc_t), FAST_MEMORY);

  if ( jinsrc &&
        /* Remember where VISE is */
      (jinsrc->vise = vise) &&
        /* Create an attributes object for the Jin frames */
      (jinsrc->igenJinAttr = JINATTR_create(vise->memory)) &&
        /* Get its IJINATTR (control) interface */
      (jinsrc->iJinAttr = (IJINATTR) IGEN_interface(jinsrc->igenJinAttr, IJINATTRID)) &&
        /* Create a JINMSG to get jin frames from VISE's incoming message port */
      (jinsrc->igenJinMsg = JINMSG_create(vise)) &&
        /* Get its IJINMSG (control) interface */
      (jinsrc->iJinMsg = (IJINMSG) IGEN_interface(jinsrc->igenJinMsg, IJINMSGID)) &&
        /* Get its ISRC (output) interface */
      (jinsrc->isrcJinMsg = (ISRC) IGEN_interface(jinsrc->igenJinMsg, ISRCID)) &&
        /* Create an OBJPAD, passing it the JINMSG's ISRC as source */
      (jinsrc->igenSilPad = OBJPAD_create(vise->memory, jinsrc->isrcJinMsg)) &&
        /* Get its ISRC (output) interface */
      (jinsrc->isrcSilPad = (ISRC) IGEN_interface(jinsrc->igenSilPad, ISRCID)) &&
        /* Create a frame queue, passing it the SILPAD's ISRC as source */
      (jinsrc->igenFrameQ = QUEUE_create(vise->memory, jinsrc->isrcSilPad)) &&
        /* Get its IQUEUE (control) interface */
      (jinsrc->iqueueFrameQ = (IQUEUE) IGEN_interface(jinsrc->igenFrameQ, IQUEUEID)) &&
        /* Get its IQSINK (injection) interface */
      (jinsrc->iqsinkFrameQ = (IQSINK) IGEN_interface(jinsrc->igenFrameQ, IQSINKID)) &&
        /* Get its ISRC (output) interface */
      (jinsrc->isrcFrameQ = (ISRC) IGEN_interface(jinsrc->igenFrameQ, ISRCID)) &&
        /* Create an OBJSINK for the engine, passing it the frame QUEUE's ISRC as source */
      (jinsrc->igenEngine = OBJSINK_create(vise->memory, jinsrc->isrcFrameQ)) &&
        /* Get its IOBJSINK (control) interface */
      (jinsrc->iEngine = (IOBJSINK) IGEN_interface(jinsrc->igenEngine, IOBJSINKID)) &&
        /* Create an OBJSINK, passing it the frame queue's IQUEUE as source */
      (jinsrc->igenFrameQSink = OBJSINK_create(vise->memory, (ISRC) jinsrc->iqueueFrameQ)) &&
        /* Get an interface to the OBJSINK */
      (jinsrc->iFrameQSink = (IOBJSINK) IGEN_interface(jinsrc->igenFrameQSink, IOBJSINKID)) &&
        /* Create an OBJSOURCE for injecting frames into the frame queue */
      (jinsrc->igenJinSource = OBJSOURCE_create(vise->memory)) &&
        /* Get its IOBJSOURCE (control) interface */
      (jinsrc->iJinSource = (IOBJSOURCE) IFACE_interface((IFACE) jinsrc->igenJinSource, IOBJSOURCEID)) &&
        /* Get its ISRC (output) interface */
      (jinsrc->isrcJinSource = (ISRC) IGEN_interface((IFACE) jinsrc->igenJinSource, ISRCID)) &&
        /* Make the IQSINK the ISNK for the OBJSOURCE's ISRC */
      (ISRC_setSink(jinsrc->isrcJinSource, (ISNK) jinsrc->iqsinkFrameQ)) &&
        /* Make the OBJSOURCE's ISRC the ISRC for the IQSINK */
      (ISNK_setSource((ISNK) jinsrc->iqsinkFrameQ, jinsrc->isrcJinSource)) &&
        /* Create an attributes object for the Jin message reader */
      (jinsrc->igenJinMsgAttr = JINMSGATTR_create(vise->memory)) &&
        /* Get its IJINMSGATTR (control) interface */
      (jinsrc->iJinMsgAttr = (IJINMSGATTR) IGEN_interface(jinsrc->igenJinMsgAttr, IJINMSGATTRID)) &&
        /* Create a default attributes object for the silence padder */
      (jinsrc->igenSilPadAttr = PADATTR_create(vise->memory)) &&
        /* Get its IPADATTR (control) interface */
      (jinsrc->iSilPadAttr = (IPADATTR) IGEN_interface(jinsrc->igenSilPadAttr, IPADATTRID)) &&
        /* Create a CLASS of silence padding objects */
      (jinsrc->silPadFrames = CLASS_create(vise->memory, 1, SIZE_OF_JIN_ARRAY, NULL, NULL)) &&
        /* Instantiate the silence padding object */
      (jinsrc->silPadFrame = OBJT_new(jinsrc->silPadFrames)) &&
        /* Create a default queue attributes object for the frame queue */
      (jinsrc->igenFrameQAttr = QATTR_create(vise->memory)) &&
        /* Get its IQATTR (control) interface */
      (jinsrc->iFrameQAttr = (IQATTR) IGEN_interface(jinsrc->igenFrameQAttr, IQATTRID)) &&
        /* Initialize the JINSRC */
      JINSRC_init(jinsrc)
     ) {
    return(jinsrc);
  } else {
    JINSRC_destroy(jinsrc);
    return(NULL);
  }
}


/***************************************************************************
 *
 * Name:      JINSRC_init - initialize a jin source
 *
 * Returns:   TRUE if successful, FALSE otherwise
 *
 * Requires:  A valid JINSRC
 *
 * Modifies:  The JINSRC, if any
 *
 * Effect:    Initializes a jin source
 *
 * Uses:
 *
 **************************************************************************/

  V_Bool
JINSRC_init(JINSRC jinsrc)
{
  Int  param;

   /* Make sure there is a jinsrc to initialize */
  if (!jinsrc)
    VBX_print("  JINSRC_init: FAILED (no jinsrc specified)\n");
   /* Set the length of a Jin vector */
  else if (!IJINATTR_setVecLen(jinsrc->iJinAttr, (Int) param_get(jinsrc->vise, JINVECLENGTH)))
    VBX_print("  JINSRC_init: FAILED (can't set jin vector length)\n");
   /* Set the Jin frame attributes in the OBJSOURCE */
  else if (!IOBJSOURCE_setAttributes(jinsrc->iJinSource, (IATTR) jinsrc->iJinAttr))
    VBX_print("  JINSRC_init: FAILED (can't set jin frame attributes in the OBJSOURCE)\n");
   /* Set the size of the class of Jin vector objects */
  else if (!IJINMSGATTR_setClassSize(jinsrc->iJinMsgAttr, (Uns) param_get(jinsrc->vise, JINCLASSSIZE)))
    VBX_print("  JINSRC_init: FAILED (can't set the size of the class of Jin vector objects)\n");
   /* Set the Jin frame attributes in the JINMSG */
  else if (!IJINMSGATTR_setFrameAttr(jinsrc->iJinMsgAttr, jinsrc->iJinAttr))
    VBX_print("  JINSRC_init: FAILED (can't set the Jin frame attributes in the JINMSG)\n");
   /* Set the maximum allowable sequence number gap */
  else if (!IJINMSGATTR_setMaxGapLen(jinsrc->iJinMsgAttr, (param = (Int) param_get(jinsrc->vise, JINTOLERANCE)) < 0 ? SN_MAX / 2 : 1024 * (SN) param))
    VBX_print("  JINSRC_init: FAILED (can't set the maximum allowable sequence number gap)\n");
   /* Set the maximum number of bytes to discard to reach the desired frame */
  else if (!IJINMSGATTR_setMaxReject(jinsrc->iJinMsgAttr, (param = (Int) param_get(jinsrc->vise, JINTOLERANCE)) < 0 ? SN_MAX / 2 : 1024 * (SN) param))
    VBX_print("  JINSRC_init: FAILED (can't set the maximum number of bytes to discard to reach the desired frame)\n");
   /* Set the growth quantum for the FIFO of unprocessed frames */
  else if (!IJINMSGATTR_setGrowQuant(jinsrc->iJinMsgAttr, (Uns) param_get(jinsrc->vise, UNPROCESSEDQUANTUM)))
    VBX_print("  JINSRC_init: FAILED (can't set the growth quantum for the FIFO of unprocessed frames)\n");
   /* Set the maximum size of the FIFO of unprocessed frames */
  else if (!IJINMSGATTR_setMaxLatent(jinsrc->iJinMsgAttr, (Uns) param_get(jinsrc->vise, MAXUNPROCESSEDFRAMES)))
    VBX_print("  JINSRC_init: FAILED (can't set the maximum size of the FIFO of unprocessed frames)\n");
   /* Set the length of the frame queue */
  else if (!IQATTR_setSize(jinsrc->iFrameQAttr, param_get(jinsrc->vise, FRAMEQLENGTH)))
    VBX_print("  JINSRC_init: FAILED (can't set the length of the frame queue)\n");
   /* Set the tolerance for forward sequence number gaps */
  else if (!IQATTR_setFwdTol(jinsrc->iFrameQAttr, param_get(jinsrc->vise, FRAMEQFWDTOLERANCE )))
    VBX_print("  JINSRC_init: FAILED (can't set the tolerance for forward sequence number gaps)\n");
   /* Set the tolerance for backward sequence number gaps */
  else if (!IQATTR_setBwdTol(jinsrc->iFrameQAttr, param_get(jinsrc->vise, FRAMEQBWDTOLERANCE )))
    VBX_print("  JINSRC_init: FAILED (can't set the tolerance for backward sequence number gaps)\n");
   /* Initialize the QUEUE object with (must do BEFORE initializing JINMSG) */
  else if (!IGEN_initialize(jinsrc->igenFrameQ, (IATTR) jinsrc->iFrameQAttr))
    VBX_print("  JINSRC_init: FAILED (can't initialize the QUEUE object)\n");
   /* Initialize the PADOBJ */
  else if (!IGEN_initialize(jinsrc->igenSilPad, (IATTR) jinsrc->iSilPadAttr))
    VBX_print("  JINSRC_init: FAILED (can't initialize the PADOBJ)\n");
   /* Initialize the JINMSG object */
  else if (!IGEN_initialize(jinsrc->igenJinMsg, (IATTR) jinsrc->iJinMsgAttr))
    VBX_print("  JINSRC_init: FAILED (can't initialize the JINMSG object)\n");
  else
    return(TRUE);

  return(FALSE);
}


/***************************************************************************
 *
 * Name:      JINSRC_destroy - Destroy a jin source
 *
 * Returns:   Nothing
 *
 * Requires:  Nothing
 *
 * Modifies:  The jin source, if any
 *
 * Effect:    Destroys a jin source
 *
 * Uses:      CLASS_destroy()
 *            IGEN_annihilate()
 *            OBJT_free()
 *
 **************************************************************************/

void
JINSRC_destroy(JINSRC jinsrc)
{
  if (jinsrc) {
    /* Destroy in reverse order so that all OBJTs in the CLASS of jinframes can be freed */
    IGEN_annihilate(jinsrc->igenFrameQSink);
    IGEN_annihilate(jinsrc->igenEngine);
    IGEN_annihilate(jinsrc->igenFrameQ);
    IGEN_annihilate(jinsrc->igenFrameQAttr);
    IGEN_annihilate(jinsrc->igenSilPad);
    IGEN_annihilate(jinsrc->igenSilPadAttr);
    IGEN_annihilate(jinsrc->igenJinMsg);
    IGEN_annihilate(jinsrc->igenJinMsgAttr);
    IGEN_annihilate(jinsrc->igenJinSource);
    IGEN_annihilate(jinsrc->igenJinAttr);
    OBJT_free(jinsrc->silPadFrame);
    CLASS_destroy(jinsrc->silPadFrames, NULL, NULL);
    jinsrc->vise->memory->Free(jinsrc->vise->memory, jinsrc);
  }
}


/***************************************************************************
 *
 * Name:      JINSRC_pad - Initialize the jin padder
 *
 * Returns:   Status of initialization (V_Err)
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  The jin padding object
 *
 * Effect:    Initializes the jin padding object with the BOUPADLENGTH,
 *            EOUPADLENGTH, and PADWORDINDEX parameters.
 *
 * Uses:      IGEN_initialize()
 *            IPADATTR_setBOUPadLen()
 *            IPADATTR_setEOUPadLen()
 *            IPADATTR_setPadObject()
 *            km_template()
 *            memcpy()
 *            param_get()
 *            tem_read()
 *            voc_getword()
 *            wm_firstkernel()
 *            wm_status()
 *
 **************************************************************************/

  V_Err
JINSRC_pad(JINSRC jinsrc)
{
  wm        word;             /* word model */
  jinr      padfr;
  jinpackr  padpackfr;

  /* Make sure that a word with the given id exists */
  if (!(word = voc_getword(jinsrc->vise, (V_WId) param_get(jinsrc->vise, PADWORDINDEX))))
    return(WORDNOTINVOCAB);

  /* Make sure the word has templates */
  if (wm_status(word) != TEMPLATES)
    return(WORDHASNOTEM);

  /* Get the template for the first kernel */
  tem_read(km_template(wm_firstkernel(word)), &padfr);
  jinpack_from_jin(&padpackfr, &padfr);

  /* Set up the pad attributes object */
  memcpy(&OBJT_contents(jinsrc->silPadFrame), &padpackfr, sizeof(jinpackr));
  IPADATTR_setPadObject(jinsrc->iSilPadAttr, jinsrc->silPadFrame);
  IPADATTR_setBOUPadLen(jinsrc->iSilPadAttr, (SN) param_get(jinsrc->vise, BOUPADLENGTH));
  IPADATTR_setEOUPadLen(jinsrc->iSilPadAttr, (SN) param_get(jinsrc->vise, EOUPADLENGTH));

  /* Initialize the padder */
  IGEN_initialize(jinsrc->igenSilPad, (IATTR) jinsrc->iSilPadAttr);

  VBX_DEBUG(VBX_print("  JINSRC_pad: wid = %d, bou = %d, eou = %d\n",
                      param_get(jinsrc->vise, PADWORDINDEX), param_get(jinsrc->vise, BOUPADLENGTH), param_get(jinsrc->vise, EOUPADLENGTH)));

  return(VISESUCCESS);
}


/***************************************************************************
 *
 * Name:      JINSRC_read - Read a frame of acoustic parameters from the source
 *
 * Returns:   TRUE if successful, FALSE otherwise.  If the result is FALSE,
 *            the status argument points to the VISE exception code giving
 *            the reason for the failure;  otherwise, it points to
 *            VISESUCCESS.
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  The source
 *
 * Effect:    Reads one frame of acoustic parameters from the frame
 *            queue, reporting its sequence number and whether
 *            queue overrun or EOU have occurred.  If features
 *            is not NULL, copies the frame into features as jin.
 *
 * Uses:      IFACE_error()
 *            IOBJSINK_getFrame()
 *            jin_from_jinpack
 *            OBJT_contents()
 *
 **************************************************************************/

  V_Bool
JINSRC_read(JINSRC jinsrc, V_SN * sn, jin features, V_Err * status)
{
  OBJT    jinobject;
  EXC     exception;

  VBX_DEBUG(exception = EXC_NONE);

  if (IOBJSINK_getFrame(jinsrc->iEngine, FORWARD, sn, &jinobject)) {
    if (features)
      jin_from_jinpack(features, (jinpack) &OBJT_contents(jinobject));
    OBJT_free(jinobject);
    VBX_DEBUG(if (features) PRINTJIN16("  JINSRC_read                   ", *sn, ((jinel *) features)));
    *status = VISESUCCESS;
  } else {
    /* Get the EXC and convert it to a V_Err */
    exception = IFACE_error((IFACE) jinsrc->iEngine);
    switch (exception) {
      case EXC_ABORT:
        *status = ABORTED; break;
      case EXC_CANTACKNOWLEDGE:
        *status = OUTOFMEMORY; break;
      case EXC_CANTGETFEATURE:
        *status = CANTREADFRAME; break;
      case EXC_EOU:
        *status = RECOGTIMEOUT; break;
      case EXC_FEATCLASSEMPTY:
        *status = SYSLIMITREACHED; break;
      case EXC_OUTOFMEMORY:
        *status = OUTOFMEMORY; break;
      case EXC_SEQNUMGAP:
        *status = CANTREADFRAME; break;
      case EXC_SNWRAPAROUND:
        if (!JINSRC_init(jinsrc))
          *status = OUTOFMEMORY;
        else
          *status = SNWRAPAROUND;
        break;
      case EXC_UNEXPECTED:
      default:
        *status = UNKNOWNERROR; break;
    }
  }

  VBX_DEBUG(if (exception) VBX_print("  JINSRC_read: exception = %d, vise status = %s\n", (Int) exception, VISEERR_string(*status)));
  return(*status == VISESUCCESS);
}


/***************************************************************************
 *
 * Name:      JINSRC_writeFrame - copy a frame from a message into a JINSRC
 *
 * Returns:   TRUE if successful, FALSE otherwise
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *            The message must have been opened for reading and the next
 *            message element to read must be the sequence number.  The
 *            features must be in jin2 format.
 *
 * Modifies:  The JINSRC
 *
 * Effect:    Puts a Jin frame into the JINSRC
 *
 * Uses:      IJINMSG_writeFrame()
 *
 *************************************************************************/

  V_Bool
JINSRC_writeFrame(JINSRC jinsrc, VBXMSG message)
{
  return((V_Bool) IJINMSG_writeFrame(jinsrc->iJinMsg, message));
}


/***************************************************************************
 *
 * Name:      JINSRC_writeEXC - write an EXC into a JINSRC
 *
 * Returns:   TRUE if successful, FALSE otherwise
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  The JINSRC
 *
 * Effect:    Puts an EXC frame into the JINSRC
 *
 * Uses:      IJINMSG_writeEXC()
 *
 *************************************************************************/

  V_Bool
JINSRC_writeEXC(JINSRC jinsrc, V_Err exception)
{
  EXC  exc;

  if (exception == ENDOFDATA)
    exc = EXC_EOU;
  else if (exception == SNWRAPAROUND)
    exc = EXC_SNWRAPAROUND;
  else
    return(FALSE);

  return((V_Bool) IJINMSG_writeEXC(jinsrc->iJinMsg, exc));
}


/***************************************************************************
 *
 * Name:      JINSRC_canread - Do frames of acoustic features exist
 *                  for the given range of sequence numbers?
 *
 * Returns:   TRUE if so, FALSE otherwise
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  Nothing
 *
 * Effect:    Determines if frames of acoustic features exist in the
 *            jin buffer for a specified region of sequence numbers
 *
 * Uses:      IQUEUE_range()
 *
 *************************************************************************/

  V_Bool
JINSRC_canread(JINSRC jinsrc, V_SN startSN, V_SN endSN)
{
  V_SN   minSN;
  V_SN   maxSN;
  V_Bool success = FALSE;

  if (IQUEUE_range(jinsrc->iqueueFrameQ, &minSN, &maxSN)) {
    success = (minSN <= startSN && endSN <= maxSN);
    VBX_DEBUG(VBX_print("  JINSRC_canread: %ld <= %ld && %ld <= %ld\n", minSN, startSN, endSN, maxSN));
  } else {
    VBX_DEBUG(VBX_print("  JINSRC_canread: exception = %d\n", IFACE_error((IFACE) jinsrc->iqueueFrameQ)));
  }

  return(success);
}

/***************************************************************************
 *
 * Name:      JINSRC_numframes - returns the number of frames in the frame
 *            buffer that have sequence numbers in the specified range
 *
 * Returns:   The number of frames in the specified region,
 *            including the starting and ending frames
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  Replaces startSNP and endSNP with the nearest extant
 *            sequence numbers in the specified range.  Does not
 *            modify the frame queue.
 *
 * Effect:
 *
 * Uses:      IQUEUE_frameCount()
 *
 *************************************************************************/

  V_SN
JINSRC_numframes(JINSRC jinsrc, V_SN * startSNP, V_SN * endSNP)
{
  V_SN  numframes = (V_SN) 0;

  if (!IQUEUE_frameCount(jinsrc->iqueueFrameQ, startSNP, endSNP, &numframes))
    VBX_DEBUG(VBX_print("  JINSRC_numframes: exception = %d\n", IFACE_error((IFACE) jinsrc->iqueueFrameQ)));

  return(numframes);
}


/***************************************************************************
 *
 * Name:      JINSRC_extract - Read a frame of acoustic parameters from the
 *            frame queue
 *
 * Returns:   TRUE if successful, FALSE otherwise.  If the result is FALSE,
 *            the status argument points to the VISE exception code giving
 *            the reason for the failure;  otherwise, it points to
 *            VISESUCCESS.
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  The IQUEUE interface to the frame queue
 *
 * Effect:    Reads one frame of acoustic parameters from the frame
 *            queue, reporting its sequence number.  If features
 *            is not NULL, copies the frame into features as jin2.
 *
 * Uses:      IFACE_error()
 *            IOBJSINK_getFrame()
 *            jin_from_jin2()
 *            jin2_from_jinpack()
 *            JINMSG_newJinFrame()
 *            OBJT_contents()
 *            OBJT_free()
 *
 **************************************************************************/

  V_Bool
JINSRC_extract(JINSRC jinsrc, V_SN * sn, jin2 features, V_Err * status)
{
  OBJT    jinobject;
  EXC     exception;

  VBX_DEBUG(jinr  jinframe);
  VBX_DEBUG(exception = EXC_NONE);

  if (IOBJSINK_getFrame(jinsrc->iFrameQSink, FORWARD, sn, &jinobject)) {
    if (features)
      jin2_from_jinpack(features, (jinpack) &OBJT_contents(jinobject));
    OBJT_free(jinobject);
    VBX_DEBUG(if (features) { jin_from_jin2(&jinframe, features); PRINTJIN16("  JINSRC_extract                ", *sn, ((jinel *) &jinframe)); });
    *status = VISESUCCESS;
  } else {
    /* Get the EXC and convert it to a V_Err */
    exception = IFACE_error((IFACE) jinsrc->iFrameQSink);
    switch (exception) {
      case EXC_QUNINIT:
      case EXC_NOTINQUEUE:
      case EXC_QEMPTY:
      case EXC_NOPREVIOUSSN:
        *status = CANTREADFRAME; break;
      default:
        *status = UNKNOWNERROR; break;
    }
  }

  VBX_DEBUG(if (exception) VBX_print("  JINSRC_extract: exception = %d, vise status = %s\n", (Int) exception, VISEERR_string(*status)));
  return(*status == VISESUCCESS);
}


/***************************************************************************
 *
 * Name:      JINSRC_insert - Insert a frame of acoustic parameters into the
 *            frame queue
 *
 * Returns:   TRUE if successful, FALSE otherwise.  If the result is FALSE,
 *            the status argument points to the VISE exception code giving
 *            the reason for the failure;  otherwise, it points to
 *            VISESUCCESS.
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  The frame queue
 *
 * Effect:    Inserts one frame of acoustic parameters into the frame
 *            queue, at the given sequence number.
 *
 * Uses:      IFACE_error()
 *            IOBJSOURCE_putFrame()
 *            jin_from_jin2()
 *            jinpack_from_jin2()
 *            JINMSG_newJinFrame()
 *            OBJT_contents()
 *
 **************************************************************************/

  V_Bool
JINSRC_insert(JINSRC jinsrc, V_SN * sn, jin2 features, V_Err * status)
{
  OBJT    jinobject;
  EXC     exception;

  VBX_DEBUG(jinr  jinframe);
  VBX_DEBUG(exception = EXC_NONE);

  if (jinobject = IJINMSG_newJinFrame(jinsrc->iJinMsg)) {
    jinpack_from_jin2((jinpack) &OBJT_contents(jinobject), features);
    if (IOBJSOURCE_putFrame(jinsrc->iJinSource, FORWARD, sn, jinobject)) {
      VBX_DEBUG(if (features) { jin_from_jin2(&jinframe, features); PRINTJIN16("  JINSRC_insert                 ", *sn, ((jinel *) &jinframe)); });
      *status = VISESUCCESS;
    } else {
      OBJT_free(jinobject);
      /* Get the EXC and convert it to a V_Err */
      exception = IFACE_error((IFACE) jinsrc->iJinSource);
      switch (exception) {
        case EXC_NOPREVIOUSSN:
        case EXC_CANTPUTSN:
        default:
          *status = CANTWRITEFRAME; break;
      }
    }
  } else {
    exception = EXC_FEATCLASSEMPTY;
    *status = SYSLIMITREACHED;
  }

  VBX_DEBUG(if (exception) VBX_print("  JINSRC_extract: exception = %d, vise status = %s\n", (Int) exception, VISEERR_string(*status)));
  return(*status == VISESUCCESS);
}


/***************************************************************************
 *
 * Name:      JINSRC_resetQueue - Clear and reset the frame queue
 *
 * Returns:   TRUE if successful, FALSE otherwise.
 *
 * Requires:  JINSRC_create must have been called to create the JINSRC
 *
 * Modifies:  The frame queue
 *
 * Effect:    Clears and resets the frame queue, freeing all the frame
 *            objects it contains
 *
 * Uses:      IQUEUE_reset()
 *
 **************************************************************************/

  V_Bool
JINSRC_resetQueue(JINSRC jinsrc)
{
  return(IQUEUE_reset(jinsrc->iqueueFrameQ));
}
