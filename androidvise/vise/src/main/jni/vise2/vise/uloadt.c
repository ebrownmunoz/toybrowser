#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_tem.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      ULOADTEMPLATE -- Upload templates for one word
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                         Type
 *
 *              Word identification number       V_Uns
 *
 * Modifies:  The message buffer
 *
 * Effect:    Uploads the set of templates for one word.
 *
 *            A template consists of a mean value for each acoustic
 *            parameter.  The templates are sent in the following
 *            format:
 *
 *              Contents                         Type
 *
 *              Word identification number       V_Uns
 *              Number of acoustic kernels       V_Uns
 *              For each acoustic kernel:
 *                For each acoustic parameter:
 *                  Mean                         1/2 of V_Uns
 *
 * Implementation:
 *
 *            Get the word id in the message buffer.
 *            Get the number of kernels for the word.
 *            Copy the number of kernels to the message buffer.
 *            For each kernel,
 *             Copy the kernel's template to the message buffer.
 *            Send VISESUCCESS.
 *
 *            Exception handling:
 *            If the word is not in the vocabulary,
 *             Send WORDNOTINVOCAB.
 *            If the word has no templates,
 *             Send WORDHASNOTEM.
 *
 * Uses:
 *
 *            voc_getword -- get word from vocabulary
 *            wm_status -- get the status of a word
 *            wm_firstkernel -- returns first kernel from word model
 *            km_gettemplate -- returns template from kernel
 *            tem_read  -- returns acoustic parameters from template
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\ULOADT.C_V  $
 * 
 *    Rev 3.3   20 Mar 1992 13:04:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.2   02 Aug 1991 11:33:56   DCV
 * Installed new message system
 * 
 *    Rev 3.1   18 Jul 1991 16:26:04   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 14:01:30   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:18:48   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:31:42   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
ULOADTEMPLATE(VISE vise)
{
  wm     word;      /* word model */
  km     kernel;    /* kernel model */
  jinr   jinData;
  V_Uns  msgData[SIZE_OF_JIN2_ARRAY];
  V_Int  numKernels;
  V_Int  i;
  V_Uns  wid;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  /* Get the word Id */
  MSG_readUns(vise->incoming, &wid);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

   /* Make sure that a word with the given id exists */
  if (!(word = voc_getword(vise, (V_WId) wid)))
    VISE_EXIT(WORDNOTINVOCAB);

   /* Make sure the word has templates to upload */
  if (wm_status(word) != TEMPLATES)
    VISE_EXIT(WORDHASNOTEM);

  numKernels = wm_numkernels(word);

  /* Construct the reply */
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) ((4 + numKernels * SIZE_OF_JIN2_ARRAY) * VINTSIZEINMELS),
                    _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ULOADTEMPLATE);
    MSG_writeInt(vise->outgoing, VISESUCCESS);

    MSG_writeUns(vise->outgoing, (V_Uns) wid);
    MSG_writeUns(vise->outgoing, (V_Uns) numKernels);

    /* Write out all the templates of the word */
    for (kernel = wm_firstkernel(word); kernel != NULL;  kernel = km_nextkernel(kernel)) {
      tem_read(km_template(kernel), &jinData);
      jin2_from_jin((jin2) msgData, &jinData);
      for (i = 0; i < SIZE_OF_JIN2_ARRAY; i++)
        MSG_writeUns(vise->outgoing, msgData[i]);
    }

    /* Close the reply message */
    MSG_closeWrite(vise->outgoing, &status);
  }

  /* Normal return */
  return(status);

  /* Exception handling */
  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ULOADWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}



