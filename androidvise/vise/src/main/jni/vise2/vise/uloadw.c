#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      ULOADWORD - Upload a word model
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                         Type
 *
 *              word identification number       V_Uns
 *
 * Modifies:  The message
 *
 * Effect:    Uploads the word model for the specified  word.  The
 *            word model is represented in the reply message in the
 *            following format:
 *
 *              Contents                         Type
 *
 *              Word identification number       V_Uns
 *              Word class identification prime  V_Uns
 *              Number of acoustic kernels       V_Uns
 *              Repeat for each acoustic kernel:
 *                Minimum duration               V_Uns
 *                Maximum duration               V_Uns
 *
 * Implementation:
 *
 *            Get the word id
 *            Get the word class id prime
 *            Get the number of kernels
 *            Store the number of kernels in the return message
 *            For each kernel in the word
 *              Get mindwell and optdwell
 *              Calculate maxdwell = mindwell + optdwell
 *              Store mindwell and maxdwell in return message
 *            Set message length
 *            Send VISESUCCESS
 *
 *            Exception handling:
 *
 *            If the word is not in the vocabulary,
 *              Send WORDNOTINVOCAB
 *
 * Uses:
 *
 *            voc_getword -- Get word model from vocabulary
 *            wm_firstkernel -- Get first kernel in word model
 *            wm_numkernels -- Get number of kernels in word model
 *            km_dwells -- Get dwell constraints for kernel
 *            MSG_xxx -- Message functions
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\ULOADW.C_V  $
 * 
 *    Rev 3.4   20 Mar 1992 13:05:16   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.3   24 Sep 1991 16:03:52   DCV
 * Returns the word class id prime as the second element of the reply.
 * 
 *    Rev 3.2   02 Aug 1991 11:34:40   DCV
 * Installed new message system
 * 
 *    Rev 3.1   18 Jul 1991 16:26:38   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 14:01:06   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:19:02   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:32:48   DCV
 * Initial revision.
 *
 **************************************************************************/

  V_Err
ULOADWORD(VISE vise)
{
  wm      word;
  km      kernel;
  V_Int   minDwell, optDwell, numKernels;
  V_Uns   wid;
  V_Uns   class;
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status;

  /* Get the word Id */
  MSG_readUns(vise->incoming, &wid);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

   /* Make sure there is a word model to upload */
  if (!(word = voc_getword(vise, (V_WId)wid)))
    VISE_EXIT(WORDNOTINVOCAB);

  class = wm_class(word);
  numKernels = wm_numkernels(word);

  /* Construct the reply */
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) ((numKernels * 2 + 5) * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ULOADWORD);
    MSG_writeInt(vise->outgoing, VISESUCCESS);

    MSG_writeUns(vise->outgoing, (V_Uns) wid);
    MSG_writeUns(vise->outgoing, (V_Uns) class);
    MSG_writeUns(vise->outgoing, (V_Uns) numKernels);

    for (kernel = wm_firstkernel(word); kernel != NULL; kernel = km_nextkernel(kernel)) {
      km_dwells(kernel, &minDwell, &optDwell);
      MSG_writeUns(vise->outgoing, (V_Uns) minDwell);
      MSG_writeUns(vise->outgoing, (V_Uns) (minDwell + optDwell));
    }

    /* Close the reply message */
    MSG_closeWrite(vise->outgoing, &status);
  }

  /* Normal return */
  return(status);

  /* Exception handling */
  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ULOADWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
