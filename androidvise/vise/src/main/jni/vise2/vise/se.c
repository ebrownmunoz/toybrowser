#include "vise.h"
#include "viseerr.h"
#include "visemsg.h"
#include "visecomp.h"
#include "class.h"
#include "v_cmd.h"
#include "v_jin.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_vise.h"
#include "se.h"
#include "ifaces.h"

/***************************************************************************
 * 
 * Name:     se.c - the VISE Search Engine
 *
 * Operations:
 *
 *   SE    SE_create(IMail * mail, IMemory * memory, V_Int * status);
 *   void  SE_destroy(SE se);
 *   void  SE_run(SE se);
 *
 ***************************************************************************/

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P) (P)
#else
#define VBX_DEBUG(P)
#endif

/* Private prototype */
static void  SE_destroyDescriptor(SE se);

/***************************************************************************
 *
 * Name:     SE_create - VISE Search Engine Constructor
 *
 * Effect:   Instantiates the VISE Search Engine and hence
 *           must be called before any other VISE functions.
 *
 ***************************************************************************/

SE
SE_create(IMail * mail, IMemory * memory, V_Err * status)
{
  SE        se;
  V_Err     vstatus = OUTOFMEMORY;

  if (  /* Instantiate the Search Engine */
      (se = (SE) memory->Calloc(memory, 1, sizeof(vise_t), FAST_MEMORY)) && (se->memory = memory) &&
        /* Create the memory allocator */
      (se->memCfg = MEM_create(memory)) &&
        /* Initialize the parameters */
      (param_init(se)) &&
        /* Create a mailbox */
      (se->mailbox = mail->CreateMBox(mail, VISECOMP_SE, NUMPRIORITIES, &vstatus)) &&
        /* Get the mailbox's identifier */
      (se->myMBoxId = mail->MailBoxId(mail, VISECOMP_SE, &vstatus)) &&
        /* Create a read message port for the mailbox */
      (se->incoming = MSG_create(se->memory, se->mailbox))  &&
        /* Create a write message port for the mailbox */
      (se->outgoing = MSG_create(se->memory, se->mailbox)) &&
        /* Failure of JINSRC_create() is always due to lack of memory */
      (vstatus = OUTOFMEMORY) &&
        /* Create a source of feature frames (jin data) */
      (se->jinSource = JINSRC_create(se))
     ) {
    se->mail = mail;
  } else {
    SE_destroyDescriptor(se);
    se = NULL;
  }

  *status = (V_Int) vstatus;
  return se;
}


/**************************************************************************
 *
 * Name:     SE_destroy
 *
 * Effect:   Destroys an instance of the VISE Search Engine
 *           It may be called safely with a NULL argument
 *
 ***************************************************************************/

void
SE_destroy(SE se)
{
  IMBox  * mailbox;
  VBXMSG   message;
  V_Int    command, dstatus;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    status;

  if (se) {
    /* If the Search Engine is running, send a _DESTROY command to it */
    if (se->rcMBoxId) {
      if ((mailbox = se->mail->CreateMBox(se->mail, VISECOMP_SE "_DESTROY", 1, &status))) {
        if (message = MSG_create(se->memory, mailbox)) {
          if (MSG_openWrite(message, se->myMBoxId, (V_ULong) 0, _DESTROY, COMMAND_PRIORITY, &status) &&
              MSG_closeWrite(message, &status) &&
              MSG_openRead(message, &senderId, &length, &command, TRUE, &status)) {
            MSG_readInt(message, &command);
            MSG_readInt(message, &dstatus);
            MSG_closeRead(message, &status);
          }
          MSG_destroy(message);
        }
        mailbox->Destroy(mailbox);
      }
      /* If _DESTROY reaches the Search Engine, SE_run destroys the SE descriptor; otherwise, do it here */
      if (status) SE_destroyDescriptor(se);
    /* Otherwise, just destroy the SE descriptor */
    } else {
      SE_destroyDescriptor(se);
    }
  }
}


/**************************************************************************
 *
 * Name:     SE_destroyDescriptor
 *
 * Effect:   Destroys an SE descriptor
 *
 ***************************************************************************/

static void
SE_destroyDescriptor(SE se)
{
  if (se) {
    JINSRC_destroy(se->jinSource);
    MSG_destroy(se->outgoing);
    MSG_destroy(se->incoming);
    MEM_destroy(se->memCfg);
    se->memory->Free(se->memory, se);
  }
}


/**************************************************************************
 *
 * Name:     SE_run
 *
 * Effect:   Runs an instance of the VISE Search Engine
 *
 ***************************************************************************/

void
SE_run(SE se)
{
  V_Err  status;

  if (se) {
    /* Try to get the Id of the Recognition Control Process' mailbox */
    if ((se->rcMBoxId = se->mail->MailBoxId(se->mail, VISECOMP_RC, &status))) {
      /* If successful, enter the Search Engine's dispatch loop */
      VBX_DEBUG(VBX_print("SE_run: Search Engine started\n"));
      status = VISE_dispatch(se);
      VBX_DEBUG(VBX_print("SE_run: Search Engine exited with status = %s\n", VISEERR_string(status)));
      /* Destroy the descriptor */
      SE_destroyDescriptor(se);
      VBX_DEBUG(VBX_print("SE_run: Search Engine stopped\n"));
    } else {
      VBX_DEBUG(VBX_print("SE_run: cannot get the id of RCP's mailbox (status = %s)\n", VISEERR_string(status)));
    }
  }
}
