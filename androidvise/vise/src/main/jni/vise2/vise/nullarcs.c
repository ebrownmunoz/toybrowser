#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_ai.h"
#include "v_alg.h"
#include "v_gi.h"
#include "v_ni.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"

/**************************************************************************
 *
 * Name:      nullarcs -- Process null arcs of grammar
 *
 * Returns:   VISESUCCESS or CANTALLOCATETBP
 *
 * Requires:  The null arcs must be ordered such that any node which
 *            appears as both a source and a destination must never
 *            appear as a source before any appearance as a
 *            destination.  Null arcs must precede all non-null arcs
 *            in the grammar.
 *
 * Modifies:  The traceback tree and the seed space of the grammar nodes
 *
 * Effect:    Processes the null arcs of the given grammar instance.
 *            Null arcs are inherited across in zero time.  This
 *            function performs that inheritance, maintaining the
 *            dynamic programming invariants where several null
 *            arcs have a particular node as their destination.
 *            This function propagates path information from the
 *            inherited path list of the source node to that of
 *            destination node.
 *
 * Implementation:
 *
 * Uses:      ai_sourcenode -- Get source node of arc
 *            ai_destinationode -- Get destination node of arc
 *            gi_firstarc -- Get first arc in grammar
 *            gi_lastarc -- Get last arc in grammar
 *            ai_firstword -- Test arc for nullness
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\NULLARCS.C_V  $
 * 
 *    Rev 3.6   20 Mar 1992 12:46:22   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.5   12 Nov 1991 11:40:50   DCV
 * Rewrote to call insertpaths() and to propogate from inherited
 *   pathlist to inherited pathlist
 * 
 *    Rev 3.4   20 Sep 1991 09:32:52   DCV
 * Replaced bit-masked word class mechanism with DGNs
 * 
 *    Rev 3.3   30 Aug 1991 16:52:44   DCV
 * Added code to create new destination tbn when none exists.
 * Added code to make tbps copied from the source node point
 *   to the destination.
 * Added code to decrement the path count when excess tbps are pruned.
 * 
 *    Rev 3.2   24 Jul 1991 14:16:24   DCV
 * Added code to deal with equivalence classes of words
 * 
 *    Rev 3.1   12 Jun 1991 14:08:50   DCV
 * Rewritten to support N-best path reporting
 * 
 *    Rev 3.0   13 Sep 1990 13:52:40   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:12:16   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:24:50   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
nullarcs(VISE vise, gi grammar, V_Uns maxpaths)
  /* gi grammar - the active grammar instance */
{
  tbp  srcpath;          /* Head of list of paths to source node      */
  tbp  dstpath;          /* Head of list of paths to destination node */
  ni   srcni;            /* Source node of arc                        */
  ni   dstni;            /* Destination or destination node of arc    */
  ai   arc;              /* An arc instance in the active grammar     */
  ai   firstnonnullarc;  /* Sentinel arc in grammar                   */

  firstnonnullarc = gi_firstnonnullarc(grammar);

  /* Merge source paths of null arcs into their destination paths */
  for (arc = gi_firstarc(grammar); arc < firstnonnullarc; arc++) {
    /* Get source and destination grammar node instances */
    srcni = ai_sourcenode(arc);
    dstni = ai_destinationnode(arc);

    /* If there are paths to the source node, and the score of the best one is better than the score of the worst path
       to the destination node, insert source paths into the destination path list and then prune null words from
       the paths so they won't appear in the traceback. */
    if (((srcpath = ni_pathlist(srcni)) != NULL) && (tbp_cost(srcpath) < ni_worstscore(dstni))) {
      if (insertpaths(vise, dstni, tbp_cost(srcpath), srcpath, NULL, &(dstni->niworstscore), maxpaths)) {
        for (dstpath = ni_pathlist(dstni); dstpath != NULL; dstpath = tbp_successor(dstpath))
          if (tbp_wid(dstpath) == NULLWORDID) {
            tbp_setwid(dstpath, tbp_wid(tbp_parent(dstpath)));
            tbp_setparent(dstpath, tbp_parent(tbp_parent(dstpath)));
          }
      } else {
        return(CANTALLOCATETBP);
      }
    }
  }

  return(VISESUCCESS);
} /* End of nullarcs() */

