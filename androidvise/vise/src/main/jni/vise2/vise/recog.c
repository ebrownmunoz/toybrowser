#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_fyi.h"
#include "v_alg.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "visemsg.h"
#include "v_par.h"
#include "v_pat.h"
#include "v_syn.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"

#ifdef VBXDEBUG
#include "vbx.h"
#include "v_mem.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/**************************************************************************
 *
 * Name:     RECOGNIZE -- Recognize an utterance
 *
 * Returns:  Status of reply message
 *
 * Requires: WARMSTART must already have been called
 *
 *           The format of the command message must be as follows:
 *
 *             Contents                                Type
 *
 *             grammar id                              V_Uns
 *             wait for feature data?                  V_Uns
 *             recognition starting sequence number    V_SN
 *             recognition ending sequence number      V_SN
 *             maximum number of frames                V_ULong
 *             number of paths to trace back           V_Uns
 *
 * Modifies: Everything.
 *
 * Effect:   Recognizes an utterance and reports the result of the
 *           recognition.  If the wait parameter is set to TRUE, the
 *           recognizer will wait for feature data if the feature
 *           extractor is running, otherwise it assumes the frames
 *           are available in the feature frame buffer.
 *
 *           RECOGNIZE returns from a successful recognition (or any
 *           recognition where traceback is possible) with the word
 *           ids, start and end frames, and scores for each word
 *           recognized in the following format:
 *
 *             Contents                                Type
 *
 *             for each trace result
 *               number of words recognized            V_Uns
 *               for each recognized word:
 *                 word id                             V_Uns
 *                 number of frames                    V_Uns
 *                 starting sequence number of word    V_SN
 *                 ending sequence number of word      V_SN
 *                 average score per frame in word     V_Int
 *                 average global min score per frame  V_Int
 *               incremental path cost                 V_Int
 *
 *           The scores represent the average cost per frame, so
 *           the lower a score, the better the match.  Note that
 *           silences, wildcards, and other non-word "words" are
 *           treated just as any other word, and will be returned as
 *           part of the utterance.
 *
 *           A trace result with zero words and no incremental
 *           path cost terminates the message.
 *
 * Implementation:
 *
 *           Get the arguments of the recognition command.
 *
 *           If this is a request not to wait for feature data or
 *           a request to wait and the feature extractor is off,
 *           and the requested frames are not available
 *            Send a CANTREADFRAME reply.
 *
 *           Activate the grammar.  If the grammar cannot be activated,
 *            Send the error code as a reply.
 *
 *           Perform the recognition loop:
 *           For each frame in the recognition time period
 *           (or until end-of-utterance or a recognition error occurs),
 *            If in the wait recognition mode,
 *             Jump start the signal processing if necessary.
 *            Get the next jin frame.
 *            Perform dynamic programming.
 *            Process grammar nodes and update the history network.
 *
 *           The recognition loop has terminated.
 *           Stop the interrupt routine that is collecting adc samples.
 *
 *           If the outcome of the recognition loop is ABORTED,
 *            Send an ABORTED reply.
 *
 *           If word traceback is possible,
 *            If word traceback is succesful,
 *             Record the recognized utterance.
 *            Else
 *             Send a TRACEBACKPATHTOOLONG reply.
 *
 *           Reply with the outcome of the recognition loop:
 *            If successful recognition, send VISESUCCESS with results.
 *            If recognition timeout, send RECOGTIMEOUT with results
 *             if they are available.
 *            If jin buffer overflow, send JINOVERFLOW.
 *            If sample buffer overflow, send SAMOVERFLOW.
 *            If partial traceback overflow, send PARTIALTBERROR.
 *
 * Uses:     fe_running - Is the feature extractor on?
 *           JINSRC_canread - Does jin buffer contain frames for a given time period?
 *           JINSRC_read - read a frame from the jin buffer
 *           JINSRC_pad - initialize the jin padding object
 *           path_pop - get the next recognized word from the path object
 *           syn_activategrammar - activate the recognition grammar
 *           recloop - recognition loop
 *           tracelength - count the words on a path
 *           trace - perform forced traceback
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\RECOG.C_V  $
 * 
 *    Rev 3.11   20 Mar 1992 12:50:06   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.10   12 Nov 1991 15:28:20   DCV
 * Expects standard VISE status codes from syn_activategrammar(), and
 *   passes them to VULCAN if they indicate failure of some kind.
 * Now passes numpaths to recloop() instead of to syn_activategrammar().
 * Reports TRACEBACKPATHTOOLONG instead of TRACEBACKERROR when the
 *   traceback path is too long for the traceback path buffer.
 * 
 *    Rev 3.9   08 Oct 1991 10:10:30   DCV
 * Modified for continuous feature extraction.
 * 
 *    Rev 3.8   20 Sep 1991 09:44:00   DCV
 * Added code to compute unrenormalized path costs from the renormalized costs
 *  now stored in tbps plus the sum of best path costs now stored in tbfs.
 * 
 *    Rev 3.7   14 Aug 1991 16:27:44   DCV
 * Removed MEM_init() (it moved to the main dispatch loop)
 * 
 *    Rev 3.6   02 Aug 1991 11:29:50   DCV
 * Installed new message system
 * 
 *    Rev 3.5   18 Jul 1991 16:20:26   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.4   12 Jun 1991 14:27:38   DCV
 * Added N-best path and globalmin reporting
 * 
 *    Rev 3.3   20 Dec 1990 14:35:04   DCV
 * Added calls to implement FYI parameter reporting
 * 
 *    Rev 3.2   07 Nov 1990 16:22:18   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.1   25 Sep 1990 10:42:36   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 3.0   13 Sep 1990 13:55:08   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:14:34   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990  9:34:32   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
RECOGNIZE(VISE vise)
{
  V_Uns    grammarID;    /* Grammar id                          */
  V_Uns    live;         /* Live recognition?                   */
  V_Uns    numFrames;    /* The number of frames in a word      */
  V_SN     startSN;      /* Recognition or word starting SN     */
  V_SN     endSN;        /* Recognition or word ending SN       */
  V_Uns    numPaths;     /* Number of alternate paths to report */
  V_ULong  maxFrames;    /* Maximum number of acoustic frames   */
  tbn      terminal;     /* Terminal node of utterance          */
  tbp      path;         /* Last element on path to trace back  */
  V_Long   pathCost;     /* Total cost of path to trace back    */
  V_Long   prevPathCost; /* Total cost of path last traced back */
  V_Int    numWords;     /* The number of words on a path       */
  V_WId    wid;          /* Id of a recognized word             */
  V_Int    globalMin;    /* Avg global minimum frame cost       */
  V_Int    frameCost;    /* Avg frame cost in a word            */
  V_ULong  msgLen;       /* The length of the reply message     */
  V_Uns    senderId = MSG_correspondentId(vise->incoming);
  V_Err    vise_status, vstatus, cstatus;

  FYI_INIT(vise);

  /* Initialize the message length to the length of the FYI parameter
     area, plus 1 for the 0 which will terminate the entire message */
  msgLen = FYI_MSGSIZE(vise) + 3 * VINTSIZEINMELS;

  /* Get the recognition command arguments */
  MSG_readUns(vise->incoming, &grammarID);
  MSG_readUns(vise->incoming, &live);
  MSG_readSN(vise->incoming, &startSN);
  MSG_readSN(vise->incoming, &endSN);
  MSG_readULong(vise->incoming, &maxFrames);
  MSG_readUns(vise->incoming, &numPaths);

  if (!MSG_closeRead(vise->incoming, &vstatus)) {
    VBX_DEBUG(VBX_print("  VISE::RECOGNIZE exiting immediately with status %s\n", VISEERR_string(vstatus)));
    VISE_EXIT(vstatus);
  }

  if (maxFrames == 0 || maxFrames > MAXLISTENFRAMES) maxFrames = MAXLISTENFRAMES;
  vise->maxFrames = maxFrames;

  VBX_DEBUG(VBX_print("  VISE::RECOGNIZE(grammarId = %u, live = %d, startSN = %ld, endSN = %ld, maxFrames = %u, nbest = %u)\n",\
                                         grammarID,      live,      startSN,       endSN,       maxFrames,      numPaths));

  /* Quit if this is not a live request and the required frames are unavailable */
  if (!(V_Bool) live && !JINSRC_canread(vise->jinSource, startSN, endSN))
    VISE_EXIT(CANTREADFRAME);

  /* Initialize the jin padder */
  if ((vstatus = JINSRC_pad(vise->jinSource)))
    VISE_EXIT(vstatus);

  /* Get the exact starting sequence number */
  if (!JINSRC_read(vise->jinSource, &startSN, NULL, &vstatus))
    VISE_EXIT(vstatus);

  VBX_DEBUG(VBX_print("  VISE::RECOGNIZE: actual startSN = %ld\n", startSN));

  /* Activate the grammar */
  if ((vstatus = syn_activategrammar(vise, (V_GId) grammarID)) != VISESUCCESS)
    VISE_EXIT(vstatus);

  /* Enter the recognition loop */
  vstatus = recloop(vise, startSN, endSN, &terminal, numPaths, (V_Bool) live);

  /* Control passes to this point when an "unusual" event indicates that
     the recognition should be terminated.  End of utterance may have
     been detected, a timeout may have occurred, or one of several error
     conditions may have been discovered. */

  VBX_DEBUG(VBX_print("  VISE::RECOGNIZE: recloop returns %s\n", VISEERR_string(vstatus)));

  /* Save FYI parameters */
  FYI_ASSIGNP(FYI_HANDLE(vise, FYI_FASTMEM),=,(V_Int) ((MEM_size(vise->memCfg, FAST_MEM) + 1023) / 1024));
  FYI_ASSIGNP(FYI_HANDLE(vise, FYI_SLOWMEM),=,(V_Int) ((MEM_size(vise->memCfg, SLOW_MEM) + 1023) / 1024));

  /* Initialize the reply length to the length of the FYI parameter
     area, which may now have changed, plus 1 for the command, 1 for
     the status, and 1 for the 0 which will terminate the message */
  msgLen = FYI_MSGSIZE(vise) + 3 * VINTSIZEINMELS;

  /* If the recognition loop was aborted, quit and send ABORTED */
  if (vstatus == ABORTED)
    VISE_EXIT(vstatus);

  /* If no path reaches the terminal traceback node, quit
     and send whatever status the recognition loop returned */
  if ((terminal == NULL) || ((path = tbn_bestpath(terminal)) == NULL))
    VISE_EXIT(vstatus);

  /* This is now the normal case, where traceback can at least
     be attempted, even if a timeout or other exceptional condition
     has occurred.  The only possibility of further trouble is
     if some path exceeds the maximum allowable path size, in which
     case that path is discarded and a TRACEBACKPATHTOOLONG status
     is sent, though other paths may still be reported */

  /* Compute the length of the reply message by looping over
     successively worse paths until there are no more, counting
     the number of words in each path.  If there are too many
     words on a path to fit on the path stack, skip that path.
     Otherwise, increase the length of the message accordingly.
     There are 6 message elements (wid, numframes, starttime,
     endtime, framecost, globalmin) per word per path, plus 2
     elements (number of words, incremental cost) per path */
  do {
    if ((numWords = tracelength(path)) > path_size(vise))
      vstatus = TRACEBACKPATHTOOLONG;
    else msgLen += numWords * (4 * VINTSIZEINMELS + 2 * VSNSIZEINMELS) + 2 * VINTSIZEINMELS;
  }
  /* Get the terminal path element in the next best path, if any */
  while ((path = tbp_successor(path)) != NULL);

  /* Construct the reply */
  if (MSG_openWrite(vise->outgoing, senderId, msgLen, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &cstatus)) {
    MSG_writeInt(vise->outgoing, _RECOGNIZE);
    MSG_writeInt(vise->outgoing, (V_Int) vstatus);
    FYI_WRITEMSG(vise);

    path = tbn_bestpath(terminal);
    prevPathCost = (V_Long) tbp_cost(path) + tbp_sumbestscores(path);

    /* Loop over successively worse paths until there are no more,
       tracing back along those paths which fit on the path stack
       and sending their contents in the reply message. */
    do {
      /* If this path fits on the path stack: */
      if (trace(vise, path)) {
        /* Put the number of words on this path in the message */
        MSG_writeUns(vise->outgoing, (V_Uns) tracelength(path));
        /* Put the recognized utterance into the message */
        while (path_pop(vise, &wid, &numFrames, &startSN, &endSN, &frameCost, &globalMin)) {
          MSG_writeUns(vise->outgoing, (V_Uns) wid);
          MSG_writeUns(vise->outgoing, (V_Uns) numFrames);
          MSG_writeSN(vise->outgoing, (V_SN) startSN);
          MSG_writeSN(vise->outgoing, (V_SN) endSN);
          MSG_writeInt(vise->outgoing, (V_Int) frameCost);
          MSG_writeInt(vise->outgoing, (V_Int) globalMin);
        }
        /* Append the incremental path cost to the message */
        pathCost = (V_Long) tbp_cost(path) + tbp_sumbestscores(path);
        MSG_writeInt(vise->outgoing, (V_Int) (pathCost - prevPathCost));
        prevPathCost = pathCost;
      }
    }
    /* Get the terminal path element in the next best path, if any */
    while ((path = tbp_successor(path)) != NULL);

    /* Set # of words on next best path to zero to terminate the message */
    MSG_writeInt(vise->outgoing, (V_Int) 0);
    /* Close the reply message */
    MSG_closeWrite(vise->outgoing, &cstatus);
  }

  /* VBX_DEBUG(MEM_printCfg(vise->memCfg, FAST_MEM, "end of RECOGNIZE")); */

  /* Normal return */
  return(cstatus);

  /* Exception handling */
  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) msgLen, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &cstatus)) {
    MSG_writeInt(vise->outgoing, _RECOGNIZE);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    FYI_WRITEMSG(vise);
    /* Set number of words on (non-existent) next best path to zero to terminate the message */
    MSG_writeUns(vise->outgoing, (V_Uns) 0);
    MSG_closeWrite(vise->outgoing, &cstatus);
  }
  return(cstatus);
}

