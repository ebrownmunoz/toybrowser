#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "visemsg.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      DLOADWILDCARD - Download wildcard parameters for one word
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                     Type
 *
 *              Word identification number   V_Uns
 *              Relative Threshold           V_Uns
 *              Absolute Threshold           V_Uns
 *
 * Modifies:  The wildcard parameters for the given word
 *
 * Effect:    Downloads the wildcard parameters for a one kernel
 *            word.  The wildcard parameters consist of a relative
 *            threshold, which indicates how much worse than the best
 *            scoring template the wildcard should "match", and an
 *            absolute threshold which sets a limit on how bad that
 *            match score can get.
 *
 *            Wildcard parameters can only be downloaded for words
 *            which have not yet been initialized or for words that
 *            have previously been initialized as wildcards.
 *            Wildcards can not be downloaded over templates, nor
 *            vise versa.
 *
 * Implementation:
 *
 *            Get the word id
 *            Get the relative threshold
 *            Get the absolute threshold
 *            Place the thresholds in the appropriate word model
 *            Send a VISESUCCESS reply.
 *
 *            Exception handling:
 *            If the word is not in the vocabulary,
 *              Send a WORDNOTINVOCAB reply.
 *            If the number of kernels or the word is not 1,
 *              Send a TOOMANYKERNELS reply.
 *            If the word already has templates
 *              Send a WORDHASTEMPLATES reply.
 *
 * Uses:      voc_getword -- Get a word from the vocabulary
 *            wm_status -- Get the status for a word
 *            wm_setstatus -- Set the status for a word
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\DLOADWC.C_V  $
 * 
 *    Rev 3.2   20 Mar 1992 12:33:14   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.1   02 Aug 1991 10:14:50   DCV
 * Installed new message system
 * 
 *    Rev 3.0   13 Sep 1990 13:41:46   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:05:28   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:49:38   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
DLOADWILDCARD(VISE vise)
{
  V_Uns  wid;         /* The word id */
  wm     word;        /* The corresponding word model */
  V_Uns  relThresh;   /* The relative wildcard threshold */
  V_Uns  absThresh;   /* The absolute wildcard threshold */
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  /* Get the word id and the wildcard thresholds */
  MSG_readUns(vise->incoming, &wid);
  MSG_readUns(vise->incoming, &relThresh);
  MSG_readUns(vise->incoming, &absThresh);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

  if (!(word = voc_getword(vise, (V_WId)wid)))
    VISE_EXIT(WORDNOTINVOCAB);

  if (wm_status(word) == TEMPLATES)
    VISE_EXIT(WORDHASTEMPLATES);

  if (wm_numkernels(word) != 1)
    VISE_EXIT(TOOMANYKERNELS);

  wm_setstatus(word, WILDCARD);
  wm_setwildcard(word, relThresh, absThresh);

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DLOADWILDCARD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
