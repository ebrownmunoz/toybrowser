#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_add.h"
#include "v_alg.h"
#include "v_ki.h"
#include "v_tm.h"

/*****************************************************************************
 *  
 * File:			PROCESSK.C
 *
 * Description:
 *
 *					Kernel-level dynamic programming
 *
 * Operations:	processkernel()
 *
 * Computer:	80X86
 *
 * Unit Test:	
 *
 * $Log$
 * 
 ****************************************************************************/

/*****************************************************************************
 *
 * Name: 		processkernel - Dynamic programming for one kernel
 *
 * Call:			processkernel(vise, kernel,kernelscore,inscore,intbpath,bestscore);
 *
 *                  VISE          vise; -- The VISE descriptor
 *					ki	        kernel;	-- Kernel instance to be processed
 *					V_Int  kernelscore;	-- Score of Self-Loop of Kernel TBP
 *					V_Int      inscore;	-- Inherited score for kernel
 *					tbp	      intbpath;	-- Traceback path for this score
 *					V_Int  * bestscore;	-- Running minimum score
 *
 * Returns:		nothing
 *
 * Requires:	The current time and jinframe must already be loaded
 *					into the tm cluster.
 *
 * Modifies:	kernel, bestscore
 *
 * Effect:		Performs dynamic programming on a single kernel instance
 *					object for a single frame of acoustic parameter data.
 *
 * Implementation:
 *
 *					The kernel level dynamic programming iterates over all
 *					internal nodes of the given kernel instance object.
 *					Each node consists of a score and an associated
 *					traceback path.  A single template object represents
 *					the sound associated with all of the nodes of the
 *					kernel.  The cost of the kernel's template relative to
 *					the current frame of jin data is determined, and is
 *					used to update the scores of the internal nodes of the
 *					kernel instance. Each node in the kernel besides the
 *					first and last ones inherits the score of its
 *					predecessor.  The first node unconditionally inherits
 *					from the 'inscore' parameter, which is then normalized
 *					just as the other nodes.  The last node in the kernel
 *					is special in that it has an implicit self-loop.  It
 *					can either inherit from its predecessor or from itself.
 *					Each time the self-loop node inherits from its
 *					predecessor, the dwell counter for the kernel is reset
 *					to the optdwell constraint.  Each time the self loop
 *					node inherits from itself, the kernel dwell counter is
 *					decremented.  Any time that the dwellcounter reaches 0,
 *					the self loop node must inherit from its predecessor.
 *					Once a node has inherited a score, it is normalized
 *					with the difference between the cost of the current jin
 *					frame relative to the kernel's template, and the
 *					minimum global score from the last frame of processing.
 *					The traceback path associated with the score is
 *					inherited also, but is not changed in any way.
 *
 *					Kernel nodes are processed last to first, so that the
 *					score inherited by a node at time t is a function of
 *					the score of its predecessor at time t-1.
 *
 *					Perform template match to get cost of this kernel with
 *						respect to the current jin frame, and normalize
 *						cost by subtracting out the best path score from
 *						previous frame.
 *
 *					Get traceback path for last knode in kernel (score is
 *						argument kernelscore)
 *					Get minimum dwell for kernel
 *					Get current dwell count for self loop node of kernel
 *					Decrement dwell counter
 *					If the self loop score is less than its predecessor's
 *					  (either previous knode, or input score), AND the
 *					  dwellcount is not less than 0
 *						Self loop node inherits from itself instead of
 *						  its predecessor
 *						Score of self loop becomes its previous score
 *						  plus normalized cost via infinity addition.
 *						Not necessary to re-inherit tbpath
 *						Update minimum score encountered if necessary
 *					Else
 *						Self loop node inherits from its predecessor
 *						Score of self loop becomes predecessor's score
 *						  plus the normalized cost via infinity addition
 *						Inherit the traceback path from previous knode
 *						Set dwell count equal to optional dwell for kernel
 *						Update minimum score encountered if necessary
 *
 *					For the second to last to the second knode in the kernel
 *						Inherit the score from previous knode
 *						Add normalized cost to score using infinity
 *						  addition
 *						Inherit the traceback path from previous knode
 *						Update minimum score encountered if necessary
 *
 *					Inherit score and traceback path into first knode from
 *						the calling parameters if not already done
 *					Add normalized cost to score using infinity addition
 *					Update minimum score encountered if necessary
 *
 * Computer:	80X86
 *
 * Dependency:	KI (kernel instance) and KND (kernel internal nodes) 
 *					representations are exposed here.
 *
 * Uses:	 		tm_match -	Determine cost of a frame of acoustic
 *									parameters relative to a template.
 *
 ****************************************************************************/

	V_Void
processkernel(VISE vise, ki kernel, V_Int kernelscore,	V_Int inscore, tbp intbpath, V_Int * bestscore)
	{
	V_Int	cost;				/* Returned by tm_match(template) */
	V_Int	dwellcount;
	V_Int	mindwell;
	V_Int	optdwell;
	V_Int	score;
	tbp		tbpath;
	KND		kndptr;
	KND		prvkndptr;

	cost = tm_match(vise, ki_template(kernel));

	ki_getdwells(kernel, &mindwell, & optdwell, & dwellcount);

		/*	FIRST, deal with the loop (last) knode */

		/* If there is only one knode in this kernel */
	if (mindwell == 1) {
			/* The predecessor's score and tbpath are inscore and intbpath */
		score  = inscore;
		tbpath = intbpath;
		}
	else	{
			/*	Otherwise, they come from the penultimate knode in this kernel */
		kndptr = ki_lastknode(kernel) - 1;
		score  = kndptr->kiscore;
		tbpath = kndptr->kitbpath;
		}

		/* Point kndptr at the loop knode */
	kndptr = ki_lastknode(kernel);

		/* If (kernelscore (score of loop knode) <= predecessor's score)
			and (optional dwell is not exhausted) */
	if ((kernelscore <= score) && (dwellcount--)) {
			/* Loop knode inherits from itself */
			/* Inherit previous loop knode score + cost */
		score = add_inf(kernelscore, cost);
		}
	else {
			/* Otherwise, loop knode inherits from its predecessor */
			/*	Inherit predecessor's score + cost */
		score = add_inf(score, cost);
			/* Inherit predecessor's tbpath unchanged */
		kndptr->kitbpath = tbpath;
			/* Reset dwellcount */
		dwellcount = optdwell;
		}
		/* Inherit the score */
	kndptr->kiscore = score;
		/* Update running best score */
	if (score < *bestscore) *bestscore = score;

	
		/* NOW, deal with the other knodes (except the first) in reverse */

		/* Point kndptr at the penultimate knode */
	kndptr--;
		/* Point prvkndptr at the antepenultimate knode */
	prvkndptr = kndptr - 1;

	while (kndptr > ki_firstknode(kernel)) {
			/* Each knode inherits from the previous knode unconditionally */
			/* Inherit the previous knode's score + cost */
		score = add_inf(prvkndptr->kiscore, cost);
		kndptr->kiscore = score;
			/* Inherit predecessor's tbpath unchanged */
		kndptr->kitbpath = prvkndptr->kitbpath;
			/* Update running best score */
		if (score < *bestscore) *bestscore = score;
		kndptr--;
		prvkndptr--;
		}

		/* FINALLY, deal with the first knode if we haven't already done so */

		/* If there is more than one knode in this kernel */
	if (mindwell > 1) {
			/* We have not yet processed the first knode, which must inherit
				score and tbpath unconditionally from the arguments */
			/*	Inherit inscore + cost */
		score = add_inf(inscore, cost);
		kndptr->kiscore = score;
			/*	Inherit intbpath unchanged */
		kndptr->kitbpath = intbpath;
			/* Update running best score */
		if (score < *bestscore) *bestscore = score;
		}

		/* Update the dwellcount */
	ki_putdwellcount(kernel, dwellcount);
	}


