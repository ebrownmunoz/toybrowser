#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_am.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_gm.h"
#include "visemsg.h"
#include "v_syn.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      DELWORD -- Delete a word from the vocabulary
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                       Type
 *
 *              Word identification number     V_Uns
 *
 * Modifies:  the vocabulary object
 *
 * Effect:    Deletes the specified word from the vocabulary, if possible.
 *            Sends a VISESUCCESS reply if successful,
 *            Sends a WORDNOTINVOCAB reply if word is not in vocabulary,
 *            Sends a WORDINAGRAM reply if word is used by some grammars.
 *
 * Implementation:
 *
 *            Get the word id from the command message
 *            Delete the word from the voc object
 *
 * Uses:      syn_firstgrammar - return first grammar in syntax (syn.c)
 *            syn_nextgrammar  - return next grammar in syntax (syn.c)
 *            gm_firstarc - return first arc in grammar (gm.c)
 *            gm_nextarc - return next arc in grammar  (gm.c)
 *            am_firstword - return first word on arc  (am.c)
 *            am_nextword - return next word on arc (am.c)
 *            wm_id - return id of word (wm.c)
 *            voc_getword - get a word from the vocabulary  (voc.c)
 *            voc_deleteword - delete word from vocabulary (voc.c)
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\DELWOR.C_V  $
 * 
 *    Rev 3.2   20 Mar 1992 12:27:08   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.1   02 Aug 1991 09:58:28   DCV
 * Installed new message system
 * 
 *    Rev 3.0   13 Sep 1990 13:37:22   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:03:24   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:36:04   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
DELWORD(VISE vise)
{
  V_Uns  wid;
  gm     grm;
  am     arc;
  wm     word;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  /* Get the wid from the command message */
  if (!MSG_readUns(vise->incoming, &wid))
    VISE_EXIT(MESSAGETOOSHORT);

  /* Search the syntax to insure the word is not used in any grammar */
  if ((word = voc_getword(vise, (V_WId) wid))) {
    for (grm = syn_firstgrammar(vise); grm; grm = syn_nextgrammar(vise))
      for (arc = gm_firstarc(grm); arc; arc = gm_nextarc(grm))
        for (word = am_firstword(arc); word; word = am_nextword(arc))
          if (wm_id(word) == (V_WId) wid)
            VISE_EXIT(WORDINAGRAM);

    /* OK to delete word */
    voc_deleteword(vise, (V_WId) wid);
    VISE_EXIT(VISESUCCESS);
  } else {
    VISE_EXIT(WORDNOTINVOCAB);
  }

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DELWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
