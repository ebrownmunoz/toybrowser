#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_tem.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      DLOADTEMPLATE -- Download templates for one word
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                         Type
 *
 *              Word identification number       V_Uns
 *              Number of acoustic kernels       V_Uns
 *              For each acoustic kernel:
 *                For each acoustic parameter:
 *                  Mean                         1/2 of V_Uns
 *
 *
 * Modifies:  The template for each kernel of the word.
 *
 * Effect:    Downloads the set of templates for one word.  A
 *            template consists of a mean value for each acoustic
 *            parameter.  Any previous data in the word's templates
 *            are overwritten.
 *
 * Implementation:
 *
 *            Get the word id.
 *            Get the number of kernels.
 *            For each kernel,
 *              Load the kernel's template.
 *            Indicate that the word has templates.
 *            Send a VISESUCCESS reply.
 *
 *            Exception handling:
 *            If the word is not in the vocabulary,
 *              Send a WORDNOTINVOCAB reply.
 *            If the word is a wildcard,
 *              Send a WORDISAWILDCARD reply.
 *            If the number of templates is wrong,
 *              Send a NUMTEMWRONG reply.
 *
 * Uses:      voc_getword -- get word from vocabulary
 *            wm_status -- is a word a wildcard?
 *            wm_setstatus -- indicate that the word has templates
 *            wm_firstkernel -- returns first kernel from word model
 *            km_gettemplate -- returns template from kernel
 *            tem_write -- write template
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\DLOADT.C_V  $
 * 
 *    Rev 3.4   20 Mar 1992 12:32:28   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.3   08 Oct 1991 09:49:20   DCV
 * Can now deal with word models having no kernels -- marks words as having
 *   templates only if one or more kernel templates are downloaded.
 * 
 *    Rev 3.2   02 Aug 1991 10:05:44   DCV
 * Installed new message system
 * 
 *    Rev 3.1   18 Jul 1991 16:02:24   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 13:39:48   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:04:52   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:46:08   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
DLOADTEMPLATE(VISE vise)
{
  wm     word;          /* Word model */
  km     kernel;        /* Kernel model */
  V_Uns  wid;           /* Word id of word to receive the templates */
  V_Uns  numTemplates;  /* The number of templates to be downloaded */
  V_Uns  i;
  V_Uns  msgData[SIZE_OF_JIN2_ARRAY];
  jinr   jinData;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

   /* Get the wid and number of templates */
  if (!MSG_readUns(vise->incoming, &wid) ||
      !MSG_readUns(vise->incoming, &numTemplates))
    VISE_EXIT(MESSAGETOOSHORT);

   /* Make sure there is a word model for these templates */
  if (!(word = voc_getword(vise, (V_WId)wid)))
    VISE_EXIT(WORDNOTINVOCAB);

   /* Make sure it is not a wildcard */
  if (wm_status(word) == WILDCARD)
    VISE_EXIT(WORDISAWILDCARD);

   /* Make sure it has the right number of templates */
  if (numTemplates != wm_numkernels(word))
    VISE_EXIT(NUMTEMWRONG);

   /* Load all the templates */
  for (kernel = wm_firstkernel(word); kernel != NULL; kernel = km_nextkernel(kernel)) {
    for (i = 0; i < SIZE_OF_JIN2_ARRAY; i++)
      if (!MSG_readUns(vise->incoming, &msgData[i]))
        VISE_EXIT(MESSAGETOOSHORT);
    jin_from_jin2(&jinData, (jin2) msgData);
    tem_write(km_template(kernel), &jinData);
  }

   /* Indicate that the word has templates */
  if (numTemplates) wm_setstatus(word, TEMPLATES);

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DLOADTEMPLATE);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
