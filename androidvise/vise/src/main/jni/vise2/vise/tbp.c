#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"

/**************************************************************************
 *
 * Name:    tbp -- The Traceback Path Element Cluster
 *
 * Description: The traceback path element cluster implements the
 *      abstract traceback path elements which populate a
 *      traceback node.  A traceback path element represents a
 *      single path terminating at that node at a particular
 *      moment (the creation time of the traceback frame
 *      containing the node) in time.
 *
 *      Each traceback path element has a parent, a cost, and
 *      a word id, which are respectively the preceding path
 *      element on the path, the absolute cost of the path so
 *      far, and the word identification number of the last word
 *      on the path.  While a path element "knows" who its
 *      parent is, it does not have any information about its
 *      children other than the number of children it has.
 *      This information is kept up to date by processes
 *      outside of the cluster.  A path element also has a
 *      successor path element, which links elements together
 *      into pathlists in a traceback node, in the order of
 *      increasing path cost.  The successor to a path element
 *      is thus not the next element on the path, but rather
 *      the end of the next best path to this node.  A path
 *      element also has a link to the traceback node which
 *      contains it.
 *
 * Operations:
 *
 *      tbp_init(); -- Initialize the traceback path
 *       element cluster.
 *
 *      tbp_alloc(vise); -- Allocate a traceback path element.
 *
 *      tbp_free(path); -- Deallocate a traceback path
 *       element.
 *
 *      tbp_used(); -- Return the number of traceback path
 *       elements currently in use.
 *
 *      tbp_setinfo(path, node, cost, wid, parent, dgn, length); --
 *       Initialize a traceback path element with a traceback
 *       node, cost, wordid, parent tbn, dgn and length.
 *
 *      tbp_info(path, &node, &cost, &wid, &parent, &dgn, &length);
 *       Get a traceback path element's characterization
 *       information.
 *
 *      tbp_setsuccessor(path, successor); -- Make successor
 *       the successor to path.
 *
 *      tbp_successor(path); -- Get the successor of a
 *       traceback path element.
 *
 *      tbp_setnode(path, node) -- Set the traceback node
 *       of a traceback path element.
 *
 *        tbp_node(path) -- Get the traceback node containing
 *       the given traceback path element.
 *
 *      tbp_cost(path); -- Get the absolute path cost of the
 *       path ending at the given traceback path element.
 *
 *      tbp_wid(path); -- Get the id of the last word on
 *       the path leading to this path element.
 *
 *      tbp_parent(path); -- Get the parent path element of
 *       the given traceback path element.
 *
 *      tbp_orphan(path); -- Remove a path element's
 *       knowledge of its parent.
 *
 *      tbp_childcount(path); -- Get the number of children
 *       for a traceback path element.
 *
 *        tbp_incchildcount(path); -- Increment the child count
 *       of a traceback path element.
 *
 *        tbp_decchildcount(path); -- Decrement the child count
 *       of a traceback path element.
 *
 *      tbp_globalmin(path); -- Get the globalmin of a
 *       traceback path element.
 *
 *      tbp_sumbestscores(path); -- Get the sum of all previous
 *       best frame scores.
 *
 *      tbp_time(path); -- Get the creation time of a
 *       traceback path element.
 *
 *      tbp_sn(path); -- Get the sequence number of a
 *       traceback path element.
 *
 *      tbp_copy(src, dst) -- Copy one traceback path
 *       element into another.
 *
 *                      VISE     vise; -- a VISE descriptor
 *      tbp    path; -- a given traceback path element
 *      tbp successor; -- a traceback path element
 *      tbn   node; -- a traceback node
 *      V_Time   time; -- the creation time of the traceback frame
 *                      V_SN       sn; -- the sequence number of the traceback frame
 *      V_Long   cost; -- the cost of the path through that path
 *      V_WId   wid; -- the word id associated with the traceback path
 *      tbp    parent; -- the parent path of the traceback path
 *      tbp  src, dst; -- traceback path elements
 *
 * Computer:  TMS320C30
 *
 * Unit Test:  
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\TBP.C_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:01:10   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Jan 1992 12:59:10   DCV
 * Changed basic types
 * 
 *    Rev 1.5   20 Sep 1991 10:04:32   DCV
 * Added DGN and pathlength fields to tbps, and changed the pathcost field
 *  to a renormalized cost (sumbestscores is now available in the tbf).
 * 
 *    Rev 1.4   30 Aug 1991 17:05:20   DCV
 * tbp_copy() no longer copies tbpnode and tbpchildcount.  It now
 *   sets tbpchildcount to zero.
 * 
 *    Rev 1.3   23 Aug 1991 09:52:06   DCV
 * tbp_alloc() now increments tbpused.
 * 
 *    Rev 1.2   14 Aug 1991 16:41:38   DCV
 * Made TRACEBACKPATHALLOC a VISE parameter.
 * Now calls MEM_alloclist() to allocate tbp objects.
 * 
 *    Rev 1.1   18 Jul 1991 16:24:48   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.0   12 Jun 1991 14:43:02   DCV
 * Initial Version
 * 
 ***************************************************************************/

/***************************************************************************
 *
 * Name:    tbp_init -- Initialize the Traceback Path Element Cluster
 *
 * Calling Sequence:
 *
 *      tbp_init(vise);
 *
 *                      VISE  vise; -- The VISE descriptor
 *
 * Returns:
 *
 * Requires:  The memory for traceback path elements must have been
 *      deallocated.
 *
 * Modifies:  The traceback path element cluster
 *
 * Effect:   Initializes the traceback path element cluster.
 *
 * Implementation:
 *
 *      Set the vise->tbpPool.freeentries pointer to NULL.
 *      Set vise->tbpPool.used to zero.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

 V_Void
tbp_init(VISE vise)
 {
 vise->tbpPool.freeentries = NULL;
 vise->tbpPool.used = 0;
 }

/* end tbp_init */

/**************************************************************************
 *
 * Name:    tbp_alloc -- Traceback Path Allocation Function
 *
 * Calling Sequence:
 *
 *      path = tbp_alloc(vise);
 *
 *      tbp path; -- newly allocated traceback path object
 *                      VISE  vise; -- The VISE descriptor
 *
 * Returns:   A newly allocated and uninitialized traceback path,
 *      or NULL if all available traceback paths are already
 *      allocated.
 *
 *
 * Requires:  tbn_init must be called before invoking tbn_alloc.
 *
 * Modifies:  The traceback path element cluster
 *
 * Effect:   Returns a new traceback path object, or NULL if none
 *      are available.  Traceback paths can be returned to free
 *      storage with the tbp_free function.
 *
 *
 * Implementation:
 *
 *      If no free traceback path element exists,
 *       Make some if possible or return NULL
 *       Link them into a NULL-terminated free list.
 *
 *       Get an traceback path element off of the free list.
 *       Increment the used path count.
 *       Return the traceback path.
 *
 * Dependency:
 *
 * Uses:    param_get()
 *      MEM_alloclist()
 *
 **************************************************************************/

 tbp
tbp_alloc(VISE vise)
 {
 tbp nextFreeTBP;

  /* If no free traceback path elements exist, make some */
 if (vise->tbpPool.freeentries == NULL &&
  (vise->tbpPool.freeentries =
          (tbp) MEM_alloclist(vise->memCfg, (V_ULong) sizeof(tbp_t), (V_ULong) param_get(vise, TRACEBACKPATHALLOC), FAST_MEM, "tbp_alloc tbp")) == NULL)
        return(NULL);

  /* At least one free traceback path element now exists; get it */
 nextFreeTBP = vise->tbpPool.freeentries;
 vise->tbpPool.freeentries = vise->tbpPool.freeentries->tbpsuccessor;
 vise->tbpPool.used++;

 return(nextFreeTBP);
 }

/* end tbp_alloc */

/***************************************************************************
 *
 * Name:    tbp_free -- Deallocate a traceback path
 *
 * Calling Sequence:
 *
 *      tbp_free(vise, path)
 *
 *                      VISE  vise; -- The VISE descriptor
 *      tbp path; -- The path which is to be deallocated
 *
 * Returns:
 *
 *
 * Requires:  tbp_init must be called before invoking tbp_free.
 *      The path element must not be NULL.
 *
 *
 * Modifies:  The traceback path element cluster
 *
 *
 * Effect:   Returns the given traceback path element to free storage
 *
 *
 * Implementation:
 *
 *      Point the given path's successor pointer at the free list.
 *      Point the free list at the given path.
 *      Decrement the used path element count.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_free(VISE vise, tbp path)
 {
 path->tbpsuccessor = vise->tbpPool.freeentries;
 vise->tbpPool.freeentries = path;
 vise->tbpPool.used--;
 }

/* end of tbp_free */

#endif

/***************************************************************************
 *
 * Name:    tbp_used -- Return the number of tbps in use
 *
 * Calling Sequence:
 *
 *      numused = tbp_used(vise);
 *
 *      V_Int numused;
 *                      VISE  vise; -- The VISE descriptor
 *
 * Returns:   The number of tbps currently in use
 *
 * Requires:  tbp_init() must have been called previously
 *
 * Modifies:  Nothing
 *
 * Effect:
 *
 * Implementation:
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Int
tbp_used(VISE vise)
 {
 return(vise->tbpPool.used);
 }

/* end tbp_used */

#endif

/**************************************************************************
 *
 * Name:    tbp_setinfo -- Initialize traceback path element
 *
 * Calling Sequence:
 *
 *      tbp_setinfo(path, node, cost, wid, parent, dgn, length);
 *
 *      tbp  path; -- A traceback path element.
 *      tbn    node; -- The node this tbp belongs to.
 *      V_Int  cost; -- The cost of the path to that node.
 *      V_WId   wid; -- The word id number for the path.
 *      tbp  parent; -- The traceback path from which
 *            the given path has inherited.
 *      V_ULong  dgn; -- The dgn of the path to this element
 *      V_Uns  length; -- The length of the path to this element
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies:  path element
 *
 * Effect:   Places the given node, cost, word id number,
 *      parent, dgn and length information into the given
 *      traceback path element.  Sets the child count to zero.
 *
 * Implementation:
 *
 *      node  ==> pathid field of path
 *      cost  ==> cost field of path
 *      wid  ==> wid field of path
 *      parent ==> parent field of path
 *      dgn  ==> dgn field of path
 *      length ==> length field of path
 *      0   ==> child count field of path
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_setinfo(tbp path, tbn node,
     V_Int cost, V_WId wid, tbp parent, V_ULong dgn, V_Uns length)
 /* tbp  path  - Path to receive new data    */
 /* tbn  node  - The node this tbp belongs to   */
 /* V_Int  cost  - Cost of path to this path     */
 /* V_WId  wid  - Node's Word Identification Number */
 /* tbp  parent - Nodes's parent        */
 /* V_ULong dgn  - Nodes's dgn         */
 /* V_Uns  length - Nodes's path length      */
 {
 path->tbpnode   = node;
 path->tbpcost   = cost;
 path->tbpwid   = wid;
 path->tbpchildcount = 0;
 path->tbpparent  = parent;
 path->tbpDGN   = dgn;
 path->tbplength  = length;
 }

/* end tbp_setinfo */

#endif

/**************************************************************************
 *
 * Name:    tbp_info -- Get traceback path element's information
 *
 * Calling Sequence:
 *
 *      tbp_info(path, node, cost, wid, parent, dgn, length);
 *
 *      tbp   path; -- A traceback path object.
 *      tbn    *node; -- The node this tbp belongs to.
 *      V_Int  *cost; -- Its path cost.
 *      V_WId   *wid; -- Its associated word id number.
 *      tbp  *parent; -- Its parent traceback path.
 *      V_ULong  *dgn; -- The dgn of the path to this element
 *      V_Uns  *length; -- The length of the path to this element
 *
 * Returns:
 *
 * Requires:  tbp_setinfo must be called for a traceback path element
 *      prior to any invocation of tbp_info on that element.
 *
 * Modifies:  node, cost, wid, parent, dgn and length
 *
 * Effect:   Returns the traceback node, renormalized path cost,
 *      word id, parent traceback path element, DGN and path
 *      length for a given traceback path element.
 *
 * Implementation:
 *
 *      path's traceback node      ==> *node
 *      path's path cost       ==> *cost
 *      path's word identification number ==> *wid
 *      path's parent traceback path   ==> *parent
 *      path's DGN         ==> *dgn
 *      path's length        ==> *length
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_info(tbp path, tbn *node,
   V_Int *cost, V_WId *wid, tbp *parent, V_ULong *dgn, V_Uns *length)
 /* tbp  path  - Path we're extracting info from    */
 /* tbn  *node  - Associated traceback node     */ 
 /* V_Int  *cost  - Absolute cost of path to this path  */
 /* V_WId  *wid  - Word Indentification return parameter */
 /* tbp  *parent - Parent value return parameter    */
 /* V_ULong *dgn  - Nodes's dgn          */
 /* V_Uns  *length - Nodes's path length       */
 {
 *node   = path->tbpnode;
 *cost   = path->tbpcost;
 *wid   = path->tbpwid;
 *parent = path->tbpparent;
 *dgn   = path->tbpDGN;
 *length = path->tbplength;
 }

/* end tbp_info */

#endif

/***************************************************************************
 *
 * Name:    tbp_setsuccessor -- Set successor for path element
 *
 * Calling Sequence:
 *
 *      tbp_setsuccessor(predecessor, successor);
 *
 *      tbp predecessor, successor;  -- traceback path elements
 *      to be linked
 *
 * Returns:
 *
 *
 * Requires:
 *
 *
 * Modifies:  predecessor
 *
 *
 * Effect:   Makes successor the successor of predecessor
 *
 *
 * Implementation:
 *
 *      copy successor to predecessor's successor field
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_setsuccessor(tbp predecessor, tbp successor)
 {
 predecessor->tbpsuccessor = successor;
 }

/* end of tbp_setsuccessor */

#endif

/**************************************************************************
 *
 * Name:    tbp_successor -- Get successor of a path element
 *
 * Calling Sequence:
 *
 *      successor = tbp_successor(predecessor);
 *
 *      tbp successor, predecessor;
 *
 * Returns:   The successor to the given predecessor path element
 *
 *
 * Requires:  A successor should have already been established with
 *      the tbp_setsuccessor function.
 *
 *
 * Modifies:
 *
 *
 * Effect:   Returns the successor of the given path element.
 *
 *
 * Implementation:
 *
 *      Return successor field of predecessor path element
 *
 * Dependency:
 *
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 tbp
tbp_successor(tbp predecessor)
 {
 return( predecessor->tbpsuccessor );
 }

/* end of tbp_successor */

#endif

/***************************************************************************
 *
 * Name:    tbp_setnode -- Set the node of a traceback path element
 *
 * Calling Sequence:
 *
 *      tbp_setnode(path, node);
 *
 *      tbp path;
 *      tbn node;
 *
 * Returns:   Nothing
 *
 * Requires:  tbp_init() must have been called previously
 *
 * Modifies:  The traceback path element
 *
 * Effect:   The node pointer in the traceback path element is
 *      set to node.
 *
 * Implementation:
 *
 *      Set tbpnode to node in the traceback path element.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_setnode(tbp path, tbn node)
 {
 path->tbpnode = node;
 }

/* end tbp_setnode */

#endif

/***************************************************************************
 *
 * Name:    tbp_node -- Get node containing a traceback path element
 *
 * Calling Sequence:
 *
 *      node = tbp_node(path);
 *
 *      tbp path; - A traceback path element
 *      tbn node; - The node containing that element
 *
 * Returns:   The traceback node containing the specified path element
 *
 * Requires:  tbp_init() must have been called previously
 *
 * Modifies:  node
 *
 * Effect:   Returns the node containing the traceback path element
 *
 * Implementation:
 *
 *      Return the tbpnode field of the traceback path element.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 tbn
tbp_node(tbp path)
 {
 return(path->tbpnode);
 }

/* end tbp_node */

#endif

/**************************************************************************
 *
 * Name:    tbp_cost -- Get the path cost of traceback path element
 *
 * Calling Sequence:
 *
 *      cost = tbp_cost(path);
 *
 *      V_Int cost; - Renormalized path cost of traceback path
 *      tbp path; - A traceback path element
 *
 * Returns:   The path cost of the traceback path
 *
 * Requires:  A cost must be asserted for a particular traceback path
 *      element with tbp_setinfo prior to invoking tbp_cost
 *      for that path element.
 *
 * Modifies:
 *
 * Effect:   Returns the most recently asserted cost for the given
 *      traceback path element.
 *
 * Implementation:
 *
 *      Return the cost field of the given traceback path element
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 V_Int
tbp_cost(tbp path)
 /* tbp  path - Path we're getting cost from */
 {
 return(path->tbpcost);
 }

/* end tbp_cost */

#endif

/**************************************************************************
 *
 * Name:    tbp_sumbestscores -- Get the sum of best frame scores
 *
 * Calling Sequence:
 *
 *      sum = tbp_sumbestscores(path);
 *
 *      V_Long  sum; - Sum of best scores so far
 *      tbp  path; - A traceback path element
 *
 * Returns:   The sum of all previous best frame scores
 *
 * Requires:  The path element must belong to a frame
 *
 * Modifies:
 *
 * Effect:   Returns the sumbestscores
 *
 * Implementation:
 *
 *      Return the sumbestscores field of the given traceback
 *      frame containing the node containing this path element
 *
 * Dependency:
 *
 * Uses:    tbf_sumbestscores
 *      tbn_frame
 *
 *************************************************************************/

#ifdef NOMACROS

  V_Long
tbp_sumbestscores(tbp path)
{
  return(tbf_sumbestscores(tbn_frame(path->tbpnode)));
}

/* end tbp_sumbestscores */

#endif

/***************************************************************************
 *
 * Name:    tbp_wid -- Get the Id of the last word on this path
 *
 * Calling Sequence:
 *
 *      wordid = tbp_wid(path);
 *
 *      V_WId wordid;
 *
 *      tbp path;
 *
 * Returns:   
 *
 * Requires:  tbp_init() must have been called previously
 *
 * Modifies:  
 *
 * Effect:   
 *
 * Implementation:
 *
 *      The id of the associated word ==> wordid
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_WId
tbp_wid(tbp path)
 {
 path->tbpwid;
 }

/* end tbp_wid */

#endif

/**************************************************************************
 *
 * Name:    tbp_parent -- Get parent path of traceback path element
 *
 * Calling Sequence:
 *
 *      parent = tbp_parent(path);
 *
 *      tbp parent; -- The parent path element of the given
 *           traceback path element.
 *      tbp path; -- The given traceback path element.
 *
 * Returns:   The parent path element of the given traceback path
 *      element.
 *
 * Requires:  A parent path must have been asserted for the given
 *      path element with tbp_setinfo prior to invoking
 *      tbp_parent.
 *
 * Modifies:
 *
 * Effect:   Returns the most recently asserted parent path element
 *      for the given traceback path element.
 *
 * Implementation:
 *
 *      Return parent field of the given traceback path
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 tbp
tbp_parent(tbp path)
 /* tbp path - Path whose parent we're seeking  */
 {
 return(path->tbpparent);
 }

/* end tbp_parent */

#endif

/**************************************************************************
 *
 * Name:    tbp_orphan -- Remove knowledge of parent
 *
 * Calling Sequence:
 *
 *      tbp_orphan(path);
 *
 *      tbp path; -- A traceback path element
 *
 * Returns:
 *
 *
 * Requires:  path element must not be NULL.
 *
 *
 * Modifies:  path element
 *
 *
 * Effect:   Removes the given path's knowledge of its parent.
 *      tbp_info on path will henceforth return NULL for
 *      the parent.
 *
 *
 * Implementation:
 *
 *      Place NULL in path's parent field
 *
 * Dependency:
 *
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_orphan(tbp path)
 {
 path->tbpparent = NULL;
 }

/* end of tbp_orphan */

#endif

/**************************************************************************
 *
 * Name:    tbp_childcount -- Child count of traceback path element
 *
 * Calling Sequence:
 *
 *      numberchildren = tbp_childcount(path);
 *
 *      V_Int numberchildren; -- Number of children of path.
 *      tbp path;       -- A traceback path element.
 *
 * Returns:   The number of children of the given traceback path
 *      element.
 *
 *
 * Requires:  tbp_setinfo must be invoked for a particular traceback
 *      path element prior to invoking tbp_childcount on that
 *      path.
 *
 * Modifies:
 *
 * Effect:   Returns the number of children of the given traceback
 *      path element.
 *
 * Implementation:
 *
 *      Return childcount field of the traceback path element
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Int
tbp_childcount (tbp path)
 /* tbp path - Path we're getting childcount from */
 {
 return(path->tbpchildcount);
 }

/* end tbp_childcount */

#endif

/**************************************************************************
 *
 * Name:    tbp_incchildcount -- Increment child count
 *
 * Calling Sequence:
 *
 *      num = tbp_incchildcount(path);
 *
 *      V_Int num;  -- New childcount
 *      tbp path; -- A traceback path element.
 *
 * Returns:   The new childcount
 *
 *
 * Requires:  tbp_setinfo must have been invoked upon a particular
 *      traceback path element prior to invoking
 *      tbp_incchildcount on that path.
 *
 * Modifies:  path
 *
 * Effect:   Increments the child count of the given traceback path
 *      element.
 *
 * Implementation:
 *
 *      childcount = childcount + 1
 *      return childcount
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 V_Int
tbp_incchildcount(tbp path)
 /* tbp  path - Path whose child we're incrementing */
 {
 return (++path->tbpchildcount);
 }

/* end tbp_incchildcount */

#endif

/**************************************************************************
 *
 * Name:    tbp_decchildcount -- Decrement the path's child count
 *
 * Calling Sequence:
 *
 *      num = tbp_decchildcount(path);
 *
 *      V_Int num; -- New childcount.
 *      tbp path;  -- A traceback path element.
 *
 * Returns:   The decremented childcount.
 *
 *
 * Requires:  tbp_setinfo must have been called prior to invoking
 *      tbp_decchildcount.
 *
 * Modifies:  path
 *
 * Effect:   Unconditionally decrements the child count of a traceback
 *      path element.
 *
 * Implementation:
 *
 *      If childcount field of path > 0
 *        Decrement childcount field
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Int
tbp_decchildcount(tbp path)
 /* tbp  path - Path whose childcount we're decrementing */
 {
 return (--path->tbpchildcount);
 }

/* end tbp_decchildcount */

#endif

/**************************************************************************
 *
 * Name:    tbp_globalmin -- Get globalmin for a traceback path element
 *
 * Calling Sequence:
 *
 *      globalmin = tbp_globalmin(path);
 *
 *      V_Long globalmin; -- The globalmin on a traceback path
 *      tbp    path; -- A traceback path element
 *
 * Returns:   The globalmin for the traceback path element
 *
 * Requires:  A traceback node and frame containing this element
 *      must exist.
 *
 * Modifies:  Nothing
 *
 * Effect:   Returns the globalmin field of the traceback frame
 *      containing the given traceback path element.
 *
 * Implementation:
 *
 *      Return the globalmin field of the traceback frame
 *      containing the traceback node which contains the
 *      given traceback path element.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Long
tbp_globalmin(tbp path)
{
  return(tbf_globalmin(tbn_frame(path->tbpnode)));
}

/* end tbp_globalmin */

#endif

/**************************************************************************
 *
 * Name:    tbp_time -- Get the frame time of a traceback element
 *
 * Calling Sequence:
 *
 *      time = tbp_time(path);
 *
 *      V_Time time; -- The creation time of a traceback frame
 *      tbp  path; -- A traceback path element
 *
 * Returns:   The creation time of the traceback path element
 *
 * Requires:  A traceback node and frame containing this element
 *      must exist.
 *
 * Modifies:  Nothing
 *
 * Effect:   Returns the creation time of the given traceback
 *      path element.
 *
 * Implementation:
 *
 *      Return the time field of the traceback frame containing
 *      the traceback node which contains the given traceback
 *      path element.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Time
tbp_time(tbp path)
 /* tbp path - the traceback element whose frame time is desired */
 {
 return(tbf_time(tbn_frame(path->tbpnode)));
 }

/* end tbp_time */

#endif

/**************************************************************************
 *
 * Name:    tbp_sn -- Get the sequence number of a traceback element
 *
 * Calling Sequence:
 *
 *      sn = tbp_sn(path);
 *
 *      V_SN   sn; -- The sequence number of a traceback frame
 *      tbp  path; -- A traceback path element
 *
 * Returns:   The sequence number of the traceback path element
 *
 * Requires:  A traceback node and frame containing this element
 *      must exist.
 *
 * Modifies:  Nothing
 *
 * Effect:   Returns the sequence number of the given traceback
 *      path element.
 *
 * Implementation:
 *
 *      Return the sn field of the traceback frame containing
 *      the traceback node which contains the given traceback
 *      path element.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_SN
tbp_sn(tbp path)
 /* tbp path - the traceback element whose frame sn is desired */
 {
 return(tbf_sn(tbn_frame(path->tbpnode)));
 }

/* end tbp_sn */

#endif

/***************************************************************************
 *
 * Name:    tbp_copy -- Copy one tbp into another
 *
 * Calling Sequence:
 *
 *      tbp_copy(src, dst);
 *
 *      tbp src, dst;
 *
 * Returns:   Nothing
 *
 * Requires:  tbp_init() must have been called previously
 *
 * Modifies:  The destination tbp
 *
 * Effect:   See implementation.
 *
 * Implementation:
 *
 *      The wid, cost, parant, dgn and length fields are
 *      copied from the source tbp into the corresponding
 *      fields of the destination tbp.  The childcount of
 *      the destination tbp is set to zero.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Void
tbp_copy(tbp srctbp, tbp dsttbp)
 {
 dsttbp->tbpwid    = srctbp->tbpwid;
 dsttbp->tbpchildcount = 0;
 dsttbp->tbpcost   = srctbp->tbpcost;
 dsttbp->tbpparent   = srctbp->tbpparent;
 dsttbp->tbpDGN    = srctbp->tbpDGN;
 dsttbp->tbplength   = srctbp->tbplength;
 }

/* end tbp_copy */

#endif

/* END TBP CLUSTER */
