#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      REPLACEWORD -- Replace one word model with another
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                                Type
 *
 *              Old word identification number          V_Uns
 *              New word identification number          V_Uns
 *              New word class identification prime     V_Uns
 *              Number of acoustic kernels in new word  V_Uns
 *              For each acoustic kernel:
 *                Minimum duration                      V_Uns
 *                Maximum duration                      V_Uns
 *
 * Modifies:  The wm object and the km object
 *
 * Effect:    Deletes the old word model and its associated template,
 *            and downloads the word model information for the new word,
 *            including the minimum and maximum dwells of all the kernels
 *            which comprise the new word model, but not the new word's
 *            templates.
 *
 * Implementation:
 *
 *            Get the old word id
 *            Get the new word id
 *            Get the new word class id prime
 *            Get the new number of kernels.
 *
 *            if there is an old word model
 *              Delete it and create a new word model at the same location
 *            otherwise
 *              Create a new word model
 *            For each kernel in the new word
 *              put in minimum and optional dwell
 *                 (optional dwell = max dwell - min dwell)
 *            Send a VISESUCCESS reply.
 *
 *            Exception handling:
 *            if the new word has too many kernels,
 *              send a TOOMANYKERNELS reply.
 *            if the duration of the new word > MAXDWELL,
 *              send a WORDDURATIONTOOLONG reply.
 *            if the minimum dwell = 0  for any new kernel,
 *              send a BADDWELLS reply.
 *            if the minimum dwell > maximum dwell for any new kernel,
 *              send a BADDWELLS reply.
 *
 * Uses:      voc_getword  -- find a word entry in the vocabulary
 *            voc_createword -- create a word model
 *            wm_allocate -- allocate kernel models for a word
 *            wm_deallocate -- deallocate kernel models for a word
 *            wm_firstkernel -- return first kernel of word model
 *            km_putdwells -- add dwells to kernel model
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\REPLACEW.C_V  $
 * 
 *    Rev 1.2   20 Mar 1992 12:50:34   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.1   12 Nov 1991 15:45:02   DCV
 * Changed return status code VOCABFULL to CANTALLOCATEVOCWORD.
 * 
 *    Rev 1.0   08 Oct 1991 10:42:58   DCV
 * Initial Version
 * 
 *    Rev 3.4   24 Sep 1991 16:02:28   DCV
 * Now expects a class id prime as the second message element.
 * 
 *    Rev 3.3   20 Sep 1991 17:27:10   DCV
 * Assigns a unique prime (word class) to each word based on its id, except
 * that all words with id >= 1000 are assigned to class 1.
 * 
 *    Rev 3.2   02 Aug 1991 10:13:50   DCV
 * Installed new message system
 * 
 *    Rev 3.1   18 Jul 1991 16:05:40   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 13:41:26   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:05:10   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:48:14   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
REPLACEWORD(VISE vise)
{
  V_Uns  nKernels;            /* Number of kernels in word      */
  V_Uns  minDwell, maxDwell;  /* Dwell constraints for a kernel */
  V_Uns  oldWid;              /* Old word identification number */
  V_Uns  newWid;              /* New word identification number */
  V_Uns  class;               /* New word class number          */
  V_Int  duration;            /* Word duration                  */
  wm     word;                /* The word model (old and new)   */
  km     kernel;              /* Kernel model                   */
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  /* Get the old and new wid, new class prime and number of kernels */
  if (!MSG_readUns(vise->incoming, &oldWid) ||
      !MSG_readUns(vise->incoming, &newWid) ||
      !MSG_readUns(vise->incoming, &class) ||
      !MSG_readUns(vise->incoming, &nKernels))
    VISE_EXIT(MESSAGETOOSHORT);

  /* Make sure the number of kernels is not too large */
  if (NUMKERNEL < nKernels)
    VISE_EXIT(TOOMANYKERNELS);

  /* If there is a word model for the old word ... */
  if ((word = voc_getword(vise, (V_WId) oldWid))) {
    if (newWid != oldWid || class != wm_class(word) || nKernels != wm_numkernels(word)) {
       /* Deallocate the kernel models for the old word */
      wm_deallocate(vise, word);
       /* Allocate kernel models for the new word */
      if (!(wm_allocate(vise, word, (V_WId) newWid, class, (V_Int) nKernels)))
        VISE_EXIT(CANTALLOCATEVOCWORD);
    }
  } else {
    /* Allocate a word model for the new word */
    if (!(word = voc_createword(vise, (V_WId) newWid, class, (V_Int) nKernels)))
      VISE_EXIT(CANTALLOCATEVOCWORD);
  }

  /* The normal case */
  duration = 0;
  for (kernel = wm_firstkernel(word); kernel != NULL; kernel = km_nextkernel(kernel)) {

    if (!MSG_readUns(vise->incoming, &minDwell) ||
        !MSG_readUns(vise->incoming, &maxDwell)) {
      voc_deleteword(vise, (V_WId) newWid);
      VISE_EXIT(MESSAGETOOSHORT);
    }

    duration += maxDwell;

    if ((maxDwell < minDwell) || (minDwell == 0)) {
      voc_deleteword(vise, (V_WId) newWid);
      VISE_EXIT(BADDWELLS);
    }

    if (MAXDWELL < duration) {
      voc_deleteword(vise, (V_WId) newWid);
      VISE_EXIT(WORDDURATIONTOOLONG);
    }

    km_putdwells(kernel, (V_Int)minDwell, (V_Int)(maxDwell - minDwell));
  }

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _REPLACEWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
