#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_ki.h"
#include "v_km.h"
#include "v_mem.h"
#include "v_tem.h"

/**************************************************************************
 *
 * Name:    ki - Kernel Instance Cluster
 *
 * Description: The kernel instance cluster implements the abstract
 *      kernel instance objects of which word instances are
 *      constructed.  A kernel instance object contains its
 *      corresponding kernel model,  as well as "scratchpad"
 *      information used for performing recognition. This
 *      information includes the minimum and optional dwell
 *      times and template inherited from its corresponding
 *      kernel model object, and mindwell sets of scores and
 *      traceback nodes, one set for each of the internal nodes
 *      ("knode") in the kernel instance.
 *
 * Operations:
 *
 *  1. result = ki_init(vise, kndcount); -- Kernel instance cluster init
 *
 *  2. kndcount = ki_knodecount(vise); - Count of knodes allocated.
 *
 *  3. ki_allocate(vise, kerninstance, mindwell); --  Allocate a set of
 *   internal knodes for a kernel instance.
 *
 *  4. ki_knodeinit(kerninstance); -- Initialize the knode scores of
 *   the kernel instance.
 *
 *  5. ki_putmodel(vise, kerninstance,  model);  -- Associate a  kernel
 *   instance with a model.
 *
 *  6. ki_putdwellcount(kerninstance, dwellcount);  -- Put a  new
 *   dwellcount into the kernel instance.
 *
 *  7. ki_getdwells(kerninstance, &mindwell, &optdwell, &dwellcount);
 *   --  Get minimum and optional dwell from the kernel instance
 *   along with the current dwellcount in the self loop knode.
 *
 *  8. ki_firstknode(kerninstance);  -- Get kernel instance internal
 *   knode array.
 *
 *    9. ki_lastknode(kerninstance); -- Get a pointer to the
 *   last knode for the kernel.
 *
 *   10. ki_template(kerninstance); -- Get the template corresponding
 *   to the kernel instance.
 *
 *
 *                  VISE        vise;
 *     V_Bool   result;
 *     V_ULong kndcount;
 *     wm     model;
 *     V_Int mindwell, optdwell, dwellcount, score;
 *     tbn    tbnode;
 *     ki kerninstance;
 *
 * Computer: TMS320C30
 *
 * Unit Test: kit.ct
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\KI.C_V  $
 * 
 *    Rev 3.6   20 Mar 1992 12:39:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.5   12 Nov 1991 18:23:32   DCV
 * Eliminated ki_getscore(), which has never been used.
 * Changed ki_knodeinit() to initialize kitbpath instead of kitbnode,
 *   which it replaces.
 * 
 *    Rev 3.4   08 Oct 1991 10:00:20   DCV
 * Added ki_knodecount() to report number of knodes allocated since last ki_init().
 * Changed ki_allocate() to allocate knodes from free memory if none remain
 *   in the knode pool.
 * 
 *    Rev 3.3   18 Jul 1991 16:13:38   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   12 Jun 1991 13:44:14   DCV
 * Eliminated erroneous inclusion of v_tbn.h
 * 
 *    Rev 3.1   07 Nov 1990 16:11:16   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:47:12   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:08:46   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:09:26   DCV
 * Initial revision.
 *
 ***************************************************************************/

/**************************************************************************
 *
 * Name:   ki_init - Kernel instance cluster initialization
 *
 * Calling Sequence:
 *
 *     result = ki_init(vise, kndcount);
 *
 *     V_Bool result;  Was memory allocated for the knodes?
 *                  VISE    vise;       A VISE descriptor
 *     V_ULong kndcount; The number of knodes to allocate
 *
 * Returns:  TRUE or FALSE
 *
 * Requires: Memory for all instance objects must have been deallocated
 *
 * Modifies: The kernel instance cluster
 *
 * Effect:  Initializes the kernel instance cluster
 *
 * Implementation:
 *
 *     Allocate fast memory for kernel traceback node instances,
 *      or return FALSE.
 *     Set first-knode pointer to origin of new memory area.
 *     Set last-knode pointer to end of new memory area.
 *
 * Uses:   MEM_alloc()
 *
 **************************************************************************/

 V_Bool
ki_init(VISE vise, V_ULong kndcount)
 {
 vise->knPool.freeknode = (KND) MEM_alloc(vise->memCfg, (V_ULong) sizeof(knd_t), kndcount, FAST_MEM, "ki_init knd");
 if (vise->knPool.freeknode == NULL && kndcount)
   return(FALSE);
 vise->knPool.freecount = kndcount;
    vise->knPool.knodecount = 0;
 return(TRUE);
 }

/**************************************************************************
 *
 * Name:    ki_knodecount - Count of knodes allocated
 *
 * Calling Sequence:
 *
 *      kndcount = ki_knodecount(vise);
 *
 *      V_ULong kndcount; -- The number of knodes allocated
 *                      VISE        vise; -- The VISE descriptor
 *
 * Returns:   Count of knodes allocated
 *
 * Requires:  The grammar must have been activated since being
 *      initialized
 *
 * Modifies:  Nothing
 *
 * Effect:   Returns a count of the knodes allocated
 *
 * Implementation:
 *
 * Uses:
 *
 *************************************************************************/

 V_ULong
ki_knodecount(VISE vise)
 {
 return(vise->knPool.knodecount);
 }

/**************************************************************************
 *
 * Name:   ki_allocate - Allocate kernel internal knodes
 *
 * Calling Sequence:
 *
 *     ki_allocate(vise, kerninstance, mindwell);
 *
 *                  VISE    vise;           --  A VISE descriptor
 *     ki  kerninstance; -- A kernel instance object
 *     V_Int mindwell;  -- The number of knodes to
 *             associate with the kerninstance
 *
 *
 * Returns:  TRUE if successful, FALSE if not:  kernel nodes exhausted.
 *
 * Requires: This function should only be invoked for a particular
 *     kernel once between invocations of ki_init.  Mindwell
 *     must be greater than zero.
 *
 * Modifies: kerninstance
 *
 * Effect:  Allocates mindwell kernel internal knodes and places
 *     them in the given kernel instance object.
 *
 * Implementation:
 *
 *     If there's room in the free knode pool
 *      Set first knode pointer to current free knodes pointer
 *      Advance free knodes pointer by mindwell knodes
 *     Else
 *      Allocate mindwell knodes from free memory
 *      Set first knode pointer to the new allocation
 *      Return FALSE if allocation is NULL
 *
 *     Set last knode pointer to first + mindwell - 1
 *     Return TRUE
 *
 * Uses:   MEM_alloc()
 *
 **************************************************************************/

 V_Bool
ki_allocate(VISE vise, ki kerninstance, V_Int mindwell)
 {
  /* If there's room left in the normally allocated knode pool */
 if ((V_ULong) mindwell <= vise->knPool.freecount) {
   /* Allocate the knodes from there */
  kerninstance->kifirstknode = vise->knPool.freeknode;
  vise->knPool.freeknode += mindwell;
  vise->knPool.freecount -= mindwell;
 } else {
   /* Else try to allocate them from free memory */
  kerninstance->kifirstknode = (KND) MEM_alloc(vise->memCfg, (V_ULong) sizeof(knd_t), (V_ULong) mindwell, FAST_MEM, "ki_allocate knd");
  if (kerninstance->kifirstknode == NULL) return(FALSE);
 }
 kerninstance->kilastknode = kerninstance->kifirstknode + mindwell - 1;
    vise->knPool.knodecount += mindwell;
 return(TRUE);
 }

/***************************************************************************
 *
 * Name:   ki_knodeinit - Kernel node initialization
 *
 * Calling Sequence:
 *
 *     ki_knodeinit(kerninstance);
 *
 *     ki kerninstance; -- a kernel instance object
 *
 * Returns:
 *
 * Requires: kernel instance allocation of knodes with ki_allocate
 *
 * Modifies: kernel instance knode scores
 *
 * Effect:  This function initializes the knode scores in the given
 *     kernel instance to V_INFINITY.
 *
 * Implementation:
 *
 *     for all knodes in the kernel instance
 *      set the score to V_INFINITY
 *
 * Uses:
 *
 **************************************************************************/

 V_Void
ki_knodeinit(ki kerninstance)
 {
 KND knodepointer;

 knodepointer = kerninstance->kifirstknode;

 while ( knodepointer <= kerninstance->kilastknode ) 
  {
  knodepointer->kiscore = V_INFINITY;
  knodepointer->kitbpath = NULL;
  knodepointer++;
  }
 }

/***************************************************************************
 *
 * Name:   ki_putmodel - Associate a kernel instance and model
 *
 * Calling Sequence:
 *
 *     ki_putmodel(vise, kerninstance, model);
 *
 *     VISE       vise; -- The VISE descriptor
 *     ki kerninstance; -- A kernel instance object.
 *     km        model; -- Its corresponding kernel model object.
 *
 * Returns:  TRUE if successful, or FALSE if unsuccessful:  kernel
 *     nodes exhausted (unable to ki_allocate()).
 *
 * Requires:
 *
 * Modifies: kerninstance
 *
 * Effect:  Associates the given kernel instance object with the
 *     given kernel model object.  The minimun dwell from the
 *     kernel model is used to determine the kernel nodes
 *     array size.
 *
 * Implementation:
 *
 *     Get template from kernel model
 *     Place in kernel instance
 *     Initialize the template to have cost 0 and time -1
 *     Get minimum and optional dwell from kernel model
 *     Set dwellcount to 0
 *     Place in kernel instance
 *     Allocate the kernel nodes array
 *      Return failure if unsuccessful allocation
 *
 * Uses:
 *
 *     km_dwells -- Get the dwell constraints from a kernel.
 *     tem_pcost -- Set the cost and time of a template
 *     km_gettemplate -- Get the template from a kerne mdel
 *     ki_allocate -- Allocate knodes for a kernel instance
 *
 **************************************************************************/

 V_Bool
ki_putmodel(VISE vise, ki kiptr, km model)
 {
 kiptr->kitemplate = km_template(model);
 km_template(model)->temcost = 0;
 km_template(model)->temtime = (V_Time) -1;

 km_dwells(model, &kiptr->kimindwell, &kiptr->kioptdwell);

 kiptr->kidwellcount = kiptr->kioptdwell;

 return(ki_allocate(vise, kiptr, kiptr->kimindwell));
 }

/**************************************************************************
 *
 * Name:     ki_putdwellcount - Put dwellcount into kern_instance
 *
 * Calling Sequence:
 *
 *     ki_putdwellcount(kerninstance, dwellcount);
 *
 *     ki   kerninstance;  -- A kernel instance object.
 *     V_Int dwellcount;  -- Self loop countdown
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies: kerninstance
 *
 * Effect:  Changes the dwell count of the given kernel instance
 *     object to the value specified.
 *
 * Implementation:
 *
 *     Copy dwellcount into kernel object.
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
 V_Void
ki_putdwellcount(ki kerninstance, V_Int dwellcount)
 {
 kerninstance->kidwellcount = dwellcount;
 }
#endif

/**************************************************************************
 *
 * Name:   ki_getdwells - Get dwell constraints and dwell count
 *
 * Calling Sequence:
 *
 *     ki_getdwells( kerninstance, &mindwell, &optdwell, &dwellcount);
 *
 *     ki   kerninstance; -- A kernel instance object.
 *     V_Int mindwell;  -- The minimum dwell for the kernel.
 *     V_Int optdwell;  -- The optional additional dwell time.
 *     V_Int dwellcount; -- The current dwell count for
 *              the kernel.
 *
 * Returns:
 *
 * Requires: The kernel instance must already be associated with a
 *     kernel model. See ki_putmodel.
 *
 * Modifies: *mindwell, *optdwell, *dwellcount
 *
 * Effect:  Returns the minimum dwell time and the   optional
 *     additional dwell time constraints for the given kernel.
 *     Also returns the kernel's current dwell time counter.
 *     Returns all results in the corresponding call-by-result
 *     parameters.
 *
 * Implementation:
 *
 *     Get minimum and optional dwell constraints from
 *       the kernel instance
 *     Place them in the corresponding calling parameters
 *     Get the dwell count from the kernel instance
 *     Place it in the corresponding calling parameter
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
 V_Void
ki_getdwells(ki kerninstance,
     V_Int *pmindwell, V_Int *poptdwell, V_Int *pdwellcount)
 {
 *pmindwell  = kerninstance->kimindwell;
 *pdwellcount = kerninstance->kidwellcount;
 *poptdwell  = kerninstance->kioptdwell;
 }
#endif

/***************************************************************************
 *
 * Name:   ki_firstknode - Get kernel internal knodes array
 *
 * Calling Sequence:
 *
 *     knodes = ki_firstknode(kerninstance);
 *
 *     ki  kern__instance; -- A kernel instance object.
 *     knd        * knodes; -- An array of knodes:
 *
 * Returns:  A pointer to the knodes array for the given kernel
 *     instance.
 *
 * Requires: The kernel instance must already be associated with a
 *     set of knodes.  See ki_allocate.  The actual parameter
 *     nodes array must be of length mindwell or greater.
 *
 * Modifies:
 *
 * Effect:  Returns a pointer to the knodes array of the given
 *     kernel instance object.  Note that this constitutes a
 *     representation exposure, and is being done for
 *     efficiency reasons. Be careful.
 *
 * Implementation:
 *
 *     Return the contents of the firstknode pointer
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
 KND
ki_firstknode(ki kerninstance)
 {
 return(kerninstance->kifirstknode);
 }
#endif

/***************************************************************************
 *
 * Name:   ki_lastknode - Get last internal knode
 *
 * Calling Sequence:
 *
 *     knode = ki_lastknode(kerninstance);
 *
 *     ki  kern__instance; -- A kernel instance object.
 *     knd         * knode; -- A pointer to a knode
 *
 * Returns:  A pointer to the last knode in the array for the given
 *     kernel instance.
 *
 * Requires: The kernel instance must already be associated with a
 *     set of knodes.  See ki_allocate.  The actual parameter
 *     nodes array must be of length mindwell or greater.
 *
 * Modifies:
 *
 * Effect:  Returns a pointer to the last knode for the given
 *     kernel instance object.  Note that this constitutes a
 *     representation exposure, and is being done for
 *     efficiency reasons. Be careful.
 *
 * Implementation:
 *
 *     Return the lastknode pointer
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
 KND
ki_lastknode(ki kerninstance)
 {
 return(kerninstance->kilastknode);
 }
#endif

/**************************************************************************
 *
 * Name:   ki_template - Get template from kernel instance
 *
 * Calling Sequence:
 *
 *     template = ki_template(kern_instance);
 *
 *     ki   kern_instance; -- A kernel instance object.
 *     tem  template;   -- The corresponding template object.
 *
 * Returns:  A template object
 *
 * Requires: The given kernel instancee object must already be
 *     associated with a kernel model.  See ki_putmodel.
 *     A template object must already be part of the kernel
 *     instance's corresponding kernel model object.
 *
 * Modifies:
 *
 * Effect:  Returns the template of the kernel instance.
 *
 * Implementation:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
 tem
ki_template(ki kerninstance)
 {
 return(kerninstance->kitemplate);
 }
#endif
