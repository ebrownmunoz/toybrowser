/* jinmsg.c - a jin frame source which reads from VISE's incoming VBXMSG
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 20-Oct-97  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdlib.h>
#include <string.h>
#include "vbx.h"
#include "class.h"
#include "vise.h"
#include "v_cmd.h"
#include "v_vise.h"
#include "iattr.h"
#include "jinattr.h"
#include "jinmsg.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "mbox.h"
#include "sn.h"
#include "types.h"
#include "vbxtypes.h"
#include "visemsg.h"
#include <assert.h>

#ifdef VBXDEBUG
#include "v_jin.h"
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

#define DFLT_MAXFRAMES  (SN_MAX / 2);
#define DFLT_MAXGAPLEN  (SN_MAX / 2);
#define DFLT_GROWQUANT  1
#define DFLT_MAXLATENT  1

/* The Class of JINMSG Interface Objects */
static iclass_t IJINMSG_Class = {IJINMSGID};

/* The JINMSG Interface */
typedef struct ijinmsg_s {
  iface_t  iface;
} ijinmsg_t;

/* The Class of JINMSGATTR (JINMSG Attributes) Interface Objects */
static iclass_t IJINMSGATTR_Class = {IJINMSGATTRID};

/* JINMSG attributes */
typedef struct jinmsgattr_s {
  Uns        classSize;     /* The size of the CLASS of jin frame objects */
  IJINATTR   frameAttr;     /* The attributes of a Jin frame */
  SN         maxReject;     /* The maximum number of bytes getFrame will consume to reach the requested frame */
  SN         maxGapLen;     /* The maximum SN gap getFrame will allow */
  Uns        maxLatent;     /* Maximum size (in frames) of the mailbox (poste restante) for unprocessed frames */
  Uns        growQuant;     /* Allocation quantum (in frames) for the poste restante mailbox */
} jinmsgattr_t, * JINMSGATTR;

/* A JINMSG descriptor */
typedef struct jinmsg_s {
  IMemory  * memory;        /* The memory allocator */
  ISRC       isrc;          /* JINMSG's (jin) source interface (output) */
  IGEN       igenattr;      /* Interface to the jin attributes block */
  IJINMSG    ijinmsg;       /* JINMSG's external interface */
  VISE       vise;          /* The VISE descriptor */
  Uns        classSize;     /* The size of the CLASS of jin frame OBJTs */
  CLASS      frames;        /* The CLASS of jin frame OBJTs */
  MBOX_Att_t postRAttr;     /* The attributes of the mailbox (poste restante) for unprocessed frames */
  MBOX       posteRest;     /* The mailbox (poste restante) for unprocessed frames */
  Int        jinVecLen;     /* The number of coefficients in a jin vector */
  SN         fwdTol;
  SN         fwdTolOff;
  SN         bwdTol;
  SN         bwdTolOff;
} jinmsg_t, * JINMSG;

/* A Sequence-numbered JIN Frame */
typedef jinpackfr_t jinframe_t, * JINFRAME;

/* SN Macros */
#define INFWDTOL(D)  ((D) < 0 ? (D) < jinmsg->fwdTolOff : (D) <= jinmsg->fwdTol)
#define INBWDTOL(D)  ((D) > 0 ? (D) > jinmsg->bwdTolOff : (D) >= jinmsg->bwdTol)

/* Private functions */

static IJINMSG  IJINMSG_create(IGEN igen);

static Bool  JINMSG_annihilate(IGEN igen);
static Bool  JINMSG_initialize(IGEN igen, IATTR iattr);
static Bool  JINMSG_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  JINMSG_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static IJINMSGATTR  IJINMSGATTR_create(IGEN igen);


/*** JINMSG Methods ***/

IGEN
JINMSG_create(VISE vise)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a JINMSG object
 *---------------------------------------------------------------------------*/
{
  JINMSG  jinmsg = (JINMSG) vise->memory->Calloc(vise->memory, 1, sizeof(jinmsg_t), OOPS_MEMORY);
  IGEN    igen = IGEN_create(vise->memory, (OBJ) jinmsg);
  ISRC    isrc = ISRC_create(igen);
  IGEN    igenattr = JINATTR_create(vise->memory);
  IJINMSG ijinmsg = IJINMSG_create(igen);

  /* Initialize the object */
  jinmsg->memory = vise->memory;
  jinmsg->isrc = isrc;
  jinmsg->igenattr = igenattr;
  jinmsg->vise = vise;
  jinmsg->ijinmsg = ijinmsg;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, JINMSG_annihilate);
  IGEN_setInitializer(igen, JINMSG_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, JINMSG_getFrame);
  ISRC_setFrameAvailable(isrc, JINMSG_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, IJINATTRID));

  return(igen);
}


Bool
IJINMSG_writeFrame(IJINMSG ijinmsg, VBXMSG message)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Put a feature frame into the poste restante mailbox
 * The message must have been opened for reading and the next message element
 * to read must be the sequence number.  The features must be in jin2 format.
 *---------------------------------------------------------------------------*/
{
  JINMSG    jinmsg;
  OBJT      object;
  JINFRAME  frame;
  jin2r     frame2r;
  V_Int     i;
  V_Err     status;
  Bool      result = FALSE;

  jinmsg = ((JINMSG) IFACE_object((IFACE) ijinmsg));

  if ((object = OBJT_new(jinmsg->frames))) {
    frame = (JINFRAME) &OBJT_contents(object);
    if (MSG_readSN(message, &frame->sn)) {
      for (i = 0; i < jinmsg->jinVecLen / 2; i++) {
        if (!MSG_readUns(message, &frame2r.jin2components[i]))
          break;
      }
      if (i == jinmsg->jinVecLen / 2) {
        jinpack_from_jin2(&frame->jin, &frame2r);
        if (MBOX_insertNow(jinmsg->posteRest, object))
          result = TRUE;
        VBX_DEBUG(PRINTJIN16("  IJINMSG_writeFrame", frame->sn, ((jinel *) &frame->jin)));
      }
    }
  }
  MSG_closeRead(message, &status);
  if (!result && object) OBJT_free(object);

  return(result);
}

Bool
IJINMSG_writeEXC(IJINMSG ijinmsg, EXC exception)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write an EXC frame into the poste restante mailbox
 *---------------------------------------------------------------------------*/
{
  JINMSG    jinmsg;
  OBJT      object;
  JINFRAME  frame;

  jinmsg = ((JINMSG) IFACE_object((IFACE) ijinmsg));

  if ((object = OBJT_new(jinmsg->frames))) {
    frame = (JINFRAME) &OBJT_contents(object);
    frame->sn = exception;
    if (MBOX_insertNow(jinmsg->posteRest, object)) {
      VBX_DEBUG(VBX_print("  IJINMSG_writeEXC (%d)\n", exception));
      return(TRUE);
    } else {
      OBJT_free(object);
    }
  }

  return(FALSE);
}

OBJT
IJINMSG_newJinFrame(IJINMSG ijinmsg)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a new jin frame object from the CLASS of them that JINMSG maintains
 * Returns NULL if none are available
 *---------------------------------------------------------------------------*/
{
  JINMSG    jinmsg;

  jinmsg = ((JINMSG) IFACE_object((IFACE) ijinmsg));

  return(OBJT_new(jinmsg->frames));
}


/*** IJINMSG METHODS ***/

static IJINMSG
IJINMSG_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IJINMSG interface
 *---------------------------------------------------------------------------*/
{
  IJINMSG  ijinmsg;

  if ((ijinmsg = (IJINMSG) IFACE_calloc((IFACE) igen, 1, sizeof(ijinmsg_t)))) {
    IFACE_setClass((IFACE) ijinmsg, &IJINMSG_Class);
    IFACE_setObject((IFACE) ijinmsg, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) ijinmsg, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) ijinmsg);
  }

  return(ijinmsg);
}


/*** GENERIC METHODS ***/

static Bool
JINMSG_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a jinmsg object
 * This supplements the generic IGEN_annihilate() by annihilating the
 * CLASS of jin frame OBJTs and the jin attributes block
 *---------------------------------------------------------------------------*/
{
  JINMSG jinmsg = (JINMSG) IFACE_object((IFACE) igen);

  /* Flush out and destroy the poste restante mailbox */
  if (jinmsg->posteRest) {
    OBJT  object;
    while ((object = MBOX_removeNow(jinmsg->posteRest)))
      OBJT_free(object);
    MBOX_destroy(jinmsg->posteRest);
    VBX_DEBUG(VBX_print("JINMSG_annihilate: destroyed the poste restante mailbox @%p\n", jinmsg->posteRest));
  }

  /* Annihilate the CLASS of jin frame OBJTs */
  if (jinmsg->frames && !CLASS_destroy(jinmsg->frames, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }
  VBX_DEBUG(VBX_print("JINMSG_annihilate: destroyed CLASS of %u jinframes @%p\n", jinmsg->classSize, jinmsg->frames));

  /* Annihilate the jin attributes object */
  if (!IGEN_annihilate(jinmsg->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) jinmsg->igenattr));
    return(FALSE);
  }

  return(TRUE);
}


static Bool
JINMSG_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a jinmsg according to a jinmsg attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) iattr);
  JINMSG      jinmsg = (JINMSG) IFACE_object((IFACE) igen);
  IJINATTR    ijinattr;
  ISNK        sink;
  OBJT        object;
  Int         oldJinVecLen;

  /* Flush out the poste restante mailbox */
  if (jinmsg->posteRest)
    while ((object = MBOX_removeNow(jinmsg->posteRest)))
      OBJT_free(object);

  /* Get the attributes */
  jinmsg->fwdTol    =   (jinmsgattr->maxGapLen);
  VBX_DEBUG(VBX_print("JINMSG_initialize: jinmsg->fwdTol    = %ld\n", jinmsg->fwdTol));
  jinmsg->fwdTolOff = SN_MIN + jinmsg->fwdTol;
  VBX_DEBUG(VBX_print("JINMSG_initialize: jinmsg->fwdTolOff = %ld\n", jinmsg->fwdTolOff));
  jinmsg->bwdTol    = - (jinmsgattr->maxReject);
  VBX_DEBUG(VBX_print("JINMSG_initialize: jinmsg->bwdTol    = %ld\n", jinmsg->bwdTol));
  jinmsg->bwdTolOff = SN_MAX + jinmsg->bwdTol;
  VBX_DEBUG(VBX_print("JINMSG_initialize: jinmsg->bwdTolOff = %ld\n", jinmsg->bwdTolOff));
  IJINATTR_getVecLen(jinmsgattr->frameAttr, &jinmsg->jinVecLen);
  VBX_DEBUG(VBX_print("JINMSG_initialize: maxReject = %ld, maxGapLen = %ld, jinVecLen = %d\n",
                      jinmsgattr->maxReject, jinmsgattr->maxGapLen, jinmsg->jinVecLen));

  /* Get the vecLen in the jin attributes block (in ISRC) */
  if (!ISRC_getAttributes(jinmsg->isrc, (IATTR *) &ijinattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) jinmsg->isrc));
    return(FALSE);
  }
  IJINATTR_getVecLen(ijinattr, &oldJinVecLen);

  /* Make sure the class of jinframes is at least large enough to fill the poste restante mailbox */
  if (jinmsgattr->classSize < jinmsgattr->maxLatent)
    jinmsgattr->classSize = jinmsgattr->maxLatent + 1;
  VBX_DEBUG(VBX_print("JINMSG_initialize: jinmsg->classSize = %u\n", jinmsg->classSize));

  /* If there is no CLASS of jinframes, or if the jinVecLen or the classSize has changed, (re)create the CLASS */
  if (!jinmsg->frames || jinmsg->jinVecLen != oldJinVecLen || jinmsgattr->classSize != jinmsg->classSize) {
    VBX_DEBUG(VBX_print("JINMSG_initialize: (re)creating the CLASS of jinframes (old Class size = %u)\n", jinmsg->classSize));
    if (jinmsg->frames && !CLASS_destroy(jinmsg->frames, NULL, NULL)) {
      IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
      return(FALSE);
    }
    jinmsg->classSize = jinmsgattr->classSize;
    
    jinmsg->frames = CLASS_create(jinmsg->memory, (UInt) jinmsg->classSize, sizeof(jinframe_t), NULL, NULL);
    VBX_DEBUG(VBX_print("JINMSG_initialize: new CLASS of %u jinframes @%p\n", jinmsg->classSize, jinmsg->frames));
    if (!jinmsg->frames) {
      IFACE_setError((IFACE) igen, EXC_CANTCREATECLASS);
      return(FALSE);
    }
  }

  /* If there is no poste restante mailbox, or if the jinVecLen or the poste restante parameters have changed, (re)create the mailbox */
  if (!jinmsg->posteRest || jinmsg->jinVecLen != oldJinVecLen ||
      jinmsgattr->growQuant != MBOX_attrGrowQuantum(&jinmsg->postRAttr) ||
      jinmsgattr->maxLatent != MBOX_attrMaxMessages(&jinmsg->postRAttr)) {
    VBX_DEBUG(VBX_print("JINMSG_initialize: (re)creating the poste restante mailbox @%p\n", jinmsg->posteRest));
    if (jinmsg->posteRest) MBOX_destroy(jinmsg->posteRest);
    MBOX_attrSetIMemory(&jinmsg->postRAttr, jinmsg->memory);
    MBOX_attrSetNPriorities(&jinmsg->postRAttr, 1);
    MBOX_attrSetGrowQuantum(&jinmsg->postRAttr, jinmsgattr->growQuant);
    MBOX_attrSetMaxMessages(&jinmsg->postRAttr, jinmsgattr->maxLatent);
    jinmsg->posteRest = MBOX_create(jinmsg->memory, &jinmsg->postRAttr);
    VBX_DEBUG(VBX_print("JINMSG_initialize: new poste restante mailbox @%p\n", jinmsg->posteRest));
    if (!jinmsg->posteRest) {
      IFACE_setError((IFACE) igen, EXC_OUTOFMEMORY);
      return(FALSE);
    }
  }

  /* If the jinVecLen has changed, promulgate the change to the sink */
  if (oldJinVecLen != jinmsg->jinVecLen) {
    IJINATTR_setVecLen(ijinattr, jinmsg->jinVecLen);

    /* Get the sink associated with the source (output) interface */
    sink = ISRC_sink(jinmsg->isrc);
    /* If there is none, register an exception */
    if (!sink) {
      IFACE_setError((IFACE) igen, EXC_NOSNK);
      return(FALSE);
    }

    /* Pass the jin attributes on to the sink, if possible */
    if (ISNK_setAttributes(sink, (IATTR) ijinattr)) {
      /* If successfully passed, return TRUE */
      return(TRUE);
    } else {
      /* Otherwise, register the exception and return FALSE */
      IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
      return(FALSE);
    }
  } else {
    return(TRUE);
  }
}


/*** SOURCE METHODS ***/

static Bool
JINMSG_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a jin frame via the source (output) interface.  If this is a
 * request for a particular SN, end-of-utterance is ignored if encountered
 * before that SN is reached; otherwise, it is reported as an exception.
 *---------------------------------------------------------------------------*/
{
  JINMSG    jinmsg;
  VBXMSG    message;
  V_Bool    found;
  OBJT      object;
  V_Int     command;
  V_SN      sn, targetSN, snDiff;
  jin2r     frame2r;
  JINFRAME  frame;
  V_Err     status;
  V_Int     viseerror;
  EXC       exception;
  V_Int     i;

  assert(isrc && snP);

  jinmsg = (JINMSG) IFACE_object((IFACE) isrc);
  message = jinmsg->vise->incoming;

  if (outP) *outP = NULL;

  /* Can't go BACKWARD */
  if (dir == BACKWARD) {
    IFACE_setError((IFACE) isrc, EXC_NOBACKWARD);
    return(FALSE);
  }

  /* Set the target to the desired SN, which is negative if the next available SN will do */
  targetSN = *snP;

  /* Read without blocking from the poste restante mailbox until the desired or a greater SN is reached  */
  while (found = ((object = MBOX_removeNow(jinmsg->posteRest)) != NULL)) {
    frame =  (JINFRAME) &OBJT_contents(object);
    *snP = frame->sn;
    /* If this is an exception frame, ... */
    if (*snP < 0) {
      OBJT_free(object);
      /* Ignore EOU if seeking a specific SN */
      if (*snP == (V_SN) EXC_EOU && targetSN >= 0) {
        continue;
      /* Otherwise, report the exception */
      } else {
        VBX_DEBUG(VBX_print("  JINMSG_getFrame: got EXC %d from PR\n", (Int) *snP));
        IFACE_setError((IFACE) isrc, (EXC) *snP);
        return FALSE;
      }
    /* Otherwise, if any SN will do, use this one */
    } else if (targetSN < 0) {
      break;
    /* Otherwise, we're looking for a specific target SN */
    } else {
      snDiff = *snP - targetSN;
      /* If the current SN is at the target or less than maxGapLen beyond it, use it */
      if (INFWDTOL(snDiff)) {
        break;
      /* Otherwise, if it is less than maxReject before it, reject it and keep on looking */
      } else if (INBWDTOL(snDiff)) {
        OBJT_free(object);
        continue;
      /* Otherwise, the current SN is outside of the acceptable range */
      } else {
        OBJT_free(object);
        IFACE_setError((IFACE) isrc, EXC_SEQNUMGAP);
        VBX_DEBUG(VBX_print("  JINMSG_getFrame SN gap (sn = %d, target = %d) from PR\n", (Int) *snP, (Int) targetSN));
        return(FALSE);
      }
    }
  }

  /* If we've found the frame we want ... */
  if (found) {
    /* If requested, pass the frame object to the caller */
    if (outP) {
      VBX_DEBUG(PRINTJIN16("  JINMSG_getFrame got from PR", frame->sn, ((jinel *) &frame->jin)));
      *outP = object;
    /* Otherwise, throw the frame object away */
    } else {
      OBJT_free(object);
    }
  }
    
  /* If we've found the frame we want, we need only peek at the message port (using a non-blocking read)
     to see if we've received an _ABORT;  otherwise, we've emptied the poste restante mailbox without
     finding the desired frame, so we'll have to loop and block on the message port until we get it */

  object = NULL;
  while ((status = VISE_busyDispatch(jinmsg->vise, &command, !found)) == VISESUCCESS) {
    if (command == _ACOUSTICFEATURES) {
      if (MSG_readSN(message, &sn)) {
        /* If any available SN will do, then this one will */
        if (targetSN < 0) targetSN = sn;
        snDiff = sn - targetSN;
        /* If the current SN is not at the target or less than maxGapLen beyond it ... */
        if (!INFWDTOL(snDiff)) {
          /* If the current SN is less than maxReject before it, reject it and keep on looking */
          if (INBWDTOL(snDiff)) {
            MSG_closeRead(message, &status);
            continue;
          /* Otherwise, the current SN is outside of the acceptable range */
          } else {
            MSG_closeRead(message, &status);
            if (found && outP) OBJT_free(*outP);
            IFACE_setError((IFACE) isrc, EXC_SEQNUMGAP);
            VBX_DEBUG(VBX_print("  JINMSG_getFrame SN gap (sn = %d, target = %d, btol = %d, ftol = %d) from FE\n",
                                (Int) sn, (Int) targetSN, (Int) jinmsg->fwdTol, (Int) -jinmsg->bwdTol));
            return(FALSE);
          }
        }
        /* Create a frame object with the desired features */
        /* If an unused jinframe OBJT is available, fill it with the jin data */
        if ((object = OBJT_new(jinmsg->frames))) {
          frame = (JINFRAME) &OBJT_contents(object);
          frame->sn = sn;
          for (i = 0; i < jinmsg->jinVecLen / 2; i++) {
            if (!MSG_readUns(message, &frame2r.jin2components[i]))
              break;
          }
          if (i < jinmsg->jinVecLen / 2) {
            OBJT_free(object);
          } else {
            jinpack_from_jin2(&frame->jin, &frame2r);
            /* If we've already found something in poste restante, mail this frame there */
            if (found) {
              if (!MBOX_insertNow(jinmsg->posteRest, object)) {
                OBJT_free(object);
                if (outP) OBJT_free(*outP);
                MSG_closeRead(message, &status);
                IFACE_setError((IFACE) isrc, EXC_QUEUEFULL);
                return(FALSE);
              }
              VBX_DEBUG(PRINTJIN16("  JINMSG_getFrame put into PR", frame->sn, ((jinel *) &frame->jin)));
            /* Otherwise, report it directly to the caller */
            } else if (outP) {
              *snP = sn;
              *outP = object;
              VBX_DEBUG(PRINTJIN16("  JINMSG_getFrame got from FE", frame->sn, ((jinel *) &frame->jin)));
            }
          }
        /* Otherwise, report that the class of jinframe objects is empty */
        } else {
          MSG_closeRead(message, &status);
          if (found && outP) OBJT_free(*outP);
          IFACE_setError((IFACE) isrc, EXC_FEATCLASSEMPTY);
          return(FALSE);
        }
      }
      if (MSG_closeRead(message, &status)) {
        IFACE_setError((IFACE) isrc, EXC_NONE);
        return(TRUE);
      } else {
        if (found && outP) OBJT_free(*outP);
        IFACE_setError((IFACE) isrc, EXC_CANTGETFEATURE);
        return(FALSE);
      }
    } else if (command == _EXCEPTION) {
      MSG_readInt(message, &viseerror);
      MSG_closeRead(message, &status);
      /* Ignore EODATA if seeking a specific SN */
      if (viseerror == ENDOFDATA && targetSN >= 0) {
        continue;
      /* Otherwise, report the exception */
      } else {
        exception = viseerror == ENDOFDATA ? EXC_EOU : viseerror == SNWRAPAROUND ? EXC_SNWRAPAROUND : EXC_UNEXPECTED;
        /* If a frame was found in poste restante, put an exception frame there */
        if (found) {
          /* If a jinframe OBJT is available, mark it as exceptional and put it in poste restante */
          if ((object = OBJT_new(jinmsg->frames))) {
            frame = (JINFRAME) &OBJT_contents(object);
            frame->sn = (V_SN) exception;
            if (!MBOX_insertNow(jinmsg->posteRest, object)) {
              OBJT_free(object);
              if (outP) OBJT_free(*outP);
              IFACE_setError((IFACE) isrc, EXC_QUEUEFULL);
              return(FALSE);
            }
            VBX_DEBUG(VBX_print("  JINMSG_getFrame: put EXC %d into PR\n", (Int) exception));
          /* Otherwise, report that the class of jinframe objects is empty */
          } else {
            if (outP) OBJT_free(*outP);
            IFACE_setError((IFACE) isrc, EXC_FEATCLASSEMPTY);
            return(FALSE);
          }
        /* Otherwise, report the exception directly to the caller */
        } else {
          VBX_DEBUG(VBX_print("  JINMSG_getFrame: got EXC %d from FE\n", (Int) exception));
          IFACE_setError((IFACE) isrc, exception);
          return(FALSE);
        }
      }
    } else {
      /* The message is already closed and acknowledged */
      VBX_DEBUG(VBX_print("  JINMSG_getFrame: received %s command\n", VISECMD_string((V_Cmd) command)));
      if (found && outP) OBJT_free(*outP);
      IFACE_setError((IFACE) isrc, command == _ABORT ? EXC_ABORT : EXC_UNEXPECTED);
      return(FALSE);
    }
  }

  /* Reaching here means we've found the frame we want in the poste restante mailbox and
     did not succeed in reading anything from the message port */

  if (status != VISESUCCESS && status != NOMESSAGES) {
    VBX_DEBUG(VBX_print("  JINMSG_getFrame: could not acknowledge inappropriate %s command\n", VISECMD_string((V_Cmd) command)));
    /* Any other exception means acknowledgement of an inappropriate command was impossible */
    if (outP) OBJT_free(*outP);
    IFACE_setError((IFACE) isrc, EXC_CANTACKNOWLEDGE);
    return(FALSE);
  }

  IFACE_setError((IFACE) isrc, EXC_NONE);
  return(TRUE);
}


static Bool
JINMSG_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of a jin frame via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(JINMSG_getFrame(isrc, dir, snP, NULL));
}


/*** JINMSGATTR Methods ***/

IGEN
JINMSGATTR_create(IMemory * memory)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a JINMSG attributes object
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR   jinmsgattr = (JINMSGATTR) memory->Calloc(memory, 1, sizeof(jinmsgattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(memory, (OBJ) jinmsgattr);
  IJINMSGATTR  ijinmsgattr = IJINMSGATTR_create(igen);

  /* Set defaults */
  jinmsgattr->maxReject = DFLT_MAXFRAMES;
  jinmsgattr->maxGapLen = DFLT_MAXGAPLEN;
  jinmsgattr->growQuant = DFLT_GROWQUANT;
  jinmsgattr->maxLatent = DFLT_MAXLATENT;

  return(igen);
}


/*** IJINMSGATTR METHODS ***/

static IJINMSGATTR
IJINMSGATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IJINMSGATTR interface
 *---------------------------------------------------------------------------*/
{
  IJINMSGATTR  ijinmsgattr;

  if ((ijinmsgattr = (IJINMSGATTR) IFACE_calloc((IFACE) igen, 1, sizeof(ijinmsgattr_t)))) {
    IFACE_setClass((IFACE) ijinmsgattr, &IJINMSGATTR_Class);
    IFACE_setObject((IFACE) ijinmsgattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) ijinmsgattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) ijinmsgattr);
  }

  return(ijinmsgattr);
}


Bool
IJINMSGATTR_setClassSize(IJINMSGATTR ijinmsgattr, Uns classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of jin frames
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr;

  assert(ijinmsgattr);
  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) ijinmsgattr);

  jinmsgattr->classSize = classSize;

  return(TRUE);
}


Bool
IJINMSGATTR_setFrameAttr(IJINMSGATTR ijinmsgattr, IJINATTR ijinattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the attributes of a Jin frame
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr;

  assert(ijinmsgattr);
  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) ijinmsgattr);

  jinmsgattr->frameAttr = ijinattr;

  return(TRUE);
}


Bool
IJINMSGATTR_setMaxGapLen(IJINMSGATTR ijinmsgattr, SN maxGapLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the maximum allowable sequence number gap
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr;

  assert(ijinmsgattr);
  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) ijinmsgattr);

  jinmsgattr->maxGapLen = maxGapLen;

  return(TRUE);
}


Bool
IJINMSGATTR_setMaxReject(IJINMSGATTR ijinmsgattr, SN maxReject)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the maximum number of bytes to discard to reach the desired frame
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr;

  assert(ijinmsgattr);
  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) ijinmsgattr);

  jinmsgattr->maxReject = maxReject;

  return(TRUE);
}


Bool
IJINMSGATTR_setGrowQuant(IJINMSGATTR ijinmsgattr, Uns growQuant)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the growth quantum for the poste restante mailbox
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr;

  assert(ijinmsgattr);
  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) ijinmsgattr);

  jinmsgattr->growQuant = growQuant;

  return(TRUE);
}


Bool
IJINMSGATTR_setMaxLatent(IJINMSGATTR ijinmsgattr, Uns maxLatent)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the maximum size of the mailbox (poste restante) for unprocessed frames
 *---------------------------------------------------------------------------*/
{
  JINMSGATTR  jinmsgattr;

  assert(ijinmsgattr);
  jinmsgattr = (JINMSGATTR) IFACE_object((IFACE) ijinmsgattr);

  jinmsgattr->maxLatent = maxLatent;

  return(TRUE);
}
