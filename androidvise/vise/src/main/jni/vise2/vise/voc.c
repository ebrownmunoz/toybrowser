#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:    voc -- Vocabulary Cluster
 *
 * Description: The vocabulary cluster implements a single  abstract
 *      vocabulary object.  This  object contains all of the
 *      word models known to the system at any one time.
 *
 * Operations:
 *
 *    1. voc_init(vise); -- Initialize the vocabulary.  Deletes  all word
 *    models in the vocabulary.
 *
 *    2. voc_createword(vise, wordid, class, numkernels); -- Creates a new
 *    word model  in  the  vocabulary  with the specified identification
 *    number and number of kernels.  Returns the newly created word
 *    object.
 *
 *    3. voc_deleteword(vise, wordid); -- Removes the specified  word  model
 *    from the vocabulary.
 *
 *    4. voc_getword(vise, wordid); -- Returns the specified word model if  it
 *    exists in the vocabulary.
 *
 *                      VISE        vise; -- A VISE descriptor
 *      V_WId     wordid; -- Word identification number
 *      V_Uns    class; -- Word class number
 *      V_Int numkernels; -- Number of kernels in word model
 *
 * Computer: TMS320C30
 *
 * Unit Test: voct.ct
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\VOC.C_V  $
 * 
 *    Rev 3.8   20 Mar 1992 13:09:46   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.7   29 Jan 1992 13:14:06   DCV
 * Changed basic types
 * 
 *    Rev 3.6   20 Sep 1991 09:53:20   DCV
 * Added a word class field to word models
 * 
 *    Rev 3.5   14 Aug 1991 16:44:46   DCV
 * Made WORDMODELALLOC a VISE parameter.
 * Now calls MEM_alloclist() to allocate word model objects
 * 
 *    Rev 3.4   22 Jul 1991 16:29:02   DCV
 * Eliminated unused variable in voc_createword()
 * 
 *    Rev 3.3   18 Jul 1991 16:27:52   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   12 Jun 1991 13:39:56   DCV
 * Eliminated voccurrentword from voc since it is not used.
 * 
 *    Rev 3.1   07 Nov 1990 16:40:16   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 14:04:14   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:19:22   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:35:24   DCV
 * Initial revision.
 *
 *************************************************************************/

/* 
 * The words in the vocabulary are linked together in two singly linked
 * lists.  One list contains all the currently allocated words and one
 * contains all of the currently unallocated words.  This facilitates
 * storage management, as the available word model structures are always
 * kept easily accessible.
 *
 * The elements of the linked lists, or vocabulary entries, are structures
 * which each consist of a single word model and a single pointer to the
 * next vocabulary entry. The word model representation, wm_t, is defined
 * to be the structure which contains the word model information in the
 * word model cluster. A word model object, or wm, is defined to be a
 * pointer to a wm_t.  This is the only information about word models that
 * is available to the vocabulary cluster.
 */

/* The Vocabulary Entry Descriptor */
typedef struct vocwrd_s {
 VOCWRD nextentry;    /* Next vocabulary entry */
 wm_t wordmodel;    /* Word model descriptor */
} vocwrd_t;

/************************************************************************
 *
 * Name:    voc_init - Initialize the vocabulary
 *
 * Calling Sequence:
 *
 *      voc_init(vise);
 *
 *                      VISE    vise; -- The VISE descriptor
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies:  The vocabulary object
 *
 * Effect:   Initializes the vocabulary object. Deletes all  words
 *      in  the vocabulary.  Any word objects obtained through
 *      the vocabulary are invalidated and should not  be used
 *      henceforth.
 *
 * Implementation:
 *
 *
 *      For all entries in entry array
 *       Place vocabulary entry on deallocated list
 *
 *      Initialize word model cluster
 *
 * Dependency:
 *
 * Uses:
 *
 *       -  wm_init -- Initializes word model cluster
 *
 *************************************************************************/

 V_Void
voc_init(VISE vise)
 {
  /* Empty the vocabulary */
 vise->vocab.freeword = vise->vocab.firstword = NULL;

  /* Deallocate all word model entries */
 wm_init(vise);
 }


/*************************************************************************
 *
 * Name:      voc_createword - Create a new word
 *
 * Calling Sequence:
 *
 *            word = voc_createword(vise, wid, class, numkernels);
 *
 *            wm  word;         -- the newly created word model
 *            VISE    vise;     -- a VISE descriptor
 *            V_WId wid;        -- word indentification number
 *            V_Uns class;      -- word class number
 *            V_Int numkernels; -- number of kernels in word
 *
 * Returns:   A newly created word model consisting of the specified
 *            number of (uninitialized) kernels, or NULL if there
 *            is insufficient room to create the new word entry.
 *
 *
 * Requires:  The vocabulary must be initialized with voc_init before
 *            any invocations of voc_createword can be made.  It must
 *            be true that voc_getword(wid) returns NULL prior to invocation
 *            of voc_createword(wid).  The function does not check
 *            whether the word is already present.
 *
 * Modifies:  the vocabulary object
 *
 * Effect:    Creates a new word model with the specified number  of
 *            kernels, and returns it to the caller.
 *
 * Implementation:
 *
 *            If there are more word entries on the free list
 *       Allocate word model of first entry on the free list
 *         with the specified number of kernels
 *       If allocation unsuccessful
 *         Return NULL
 *       Else
 *        Take first entry off the free list
 *        Place it on the allocated list
 *        Return new word model
 *      Else
 *       Return NULL
 *
 * Dependency:
 *
 * Uses:
 *      param_get   -- get a VISE parameter
 *      wm_allocate -- allocate a word model with a specified number of kernels
 *
 *************************************************************************/

  wm
voc_createword(VISE vise, V_WId wid, V_Uns class, V_Int numkernels)
  /* V_WId wid   - word identification number */
  /* V_Uns class   - word class number    */
  /* V_Int numkernels - number of kernels in word */
{
  VOCWRD nextFreeWM;

   /* If no free word models exist, make some */
  if (vise->vocab.freeword == NULL &&
      (vise->vocab.freeword =
        (VOCWRD) MEM_alloclist(vise->memCfg, (V_ULong) sizeof(vocwrd_t), (V_ULong) param_get(vise, WORDMODELALLOC), SLOW_MEM, "voc_createword wm")) == NULL)
    return(NULL);

   /* At least one free word model now exists; get it */
  nextFreeWM = vise->vocab.freeword;
  vise->vocab.freeword = vise->vocab.freeword->nextentry;

   /* Fill it with kernel models if possible */
  if (!wm_allocate(vise, &nextFreeWM->wordmodel, wid, class, numkernels))
    return(NULL);

   /* Prepend it to the vocabulary's list of word models */
  nextFreeWM->nextentry = vise->vocab.firstword;
  vise->vocab.firstword = nextFreeWM;

   /* Return the new word model */
  return(&nextFreeWM->wordmodel);
}

/***************************************************************************
 *
 * Name:    voc_deleteword - Delete word from vocabulary
 *
 * Calling Sequence:
 *
 *      voc_deleteword(vise, wordid);
 *
 *                      VISE    vise; -- The VISE descriptor
 *      V_WId wordid; -- word identification code
 *
 * Returns:   TRUE if the word is successfully deleted or  FALSE  if
 *      the word is not found in the vocabulary.
 *
 * Requires:  The vocabulary must be initialized with voc_init  prior
 *      to any invocation of voc_deleteword
 *
 * Modifies:  The vocabulary object
 *
 * Effect:   Deletes specified  word  from  vocabulary  if it   is
 *      present.   Returns  an error indication if the word is
 *      not found.
 *
 * Implementation:
 *
 *      For all entries in vocabulary
 *       If wordid of word model equals specified wordid
 *        Splice entry out of allocated list
 *        Append entry onto deallocated list
 *        Deallocate associated word model
 *        Return TRUE
 *      If specified word not found
 *       Return FALSE
 *
 * Dependency:
 *
 * Uses:    wm_id -- Get word identification number
 *      wm_deallocate -- Remove kernel models from word model
 *
 **************************************************************************/


 V_Bool
voc_deleteword(VISE vise, V_WId wordid)
 {
 VOCWRD currptr, prevptr;

 for ( currptr = vise->vocab.firstword, prevptr = NULL;
   currptr != NULL;
   prevptr = currptr, currptr = currptr->nextentry)
  {
  if (wm_id(&(currptr->wordmodel)) == wordid)
   {
   if (prevptr == NULL) /* First Word! */
    vise->vocab.firstword = currptr->nextentry;
   else
    prevptr->nextentry = currptr->nextentry;

   currptr->nextentry = vise->vocab.freeword;
   vise->vocab.freeword = currptr;
   wm_deallocate(vise, &((vise->vocab.freeword)->wordmodel));
   return (TRUE);
   }
  }
 return (FALSE);
 }


/**************************************************************************
 *
 * Name:    voc_getword - Get the specified word from a vocabulary
 *
 * Calling Sequence:
 *
 *      word = voc_getword(vise, wordid);
 *
 *      wm      word; -- Word model returned from vocabulary
 *                      VISE    vise; -- The VISE descriptor
 *      V_WId wordid; -- Word identification number
 *
 * Returns:   the word model object  from  the  vocabulary  with  the
 *      specified  word  identification number, or NULL if no
 *      such word currently exists.
 *
 *
 * Requires:  The vocabulary must be initialized with voc_init  prior
 *      to any invocation of voc_getword.
 *
 * Modifies:
 *
 * Effect:   Returns the  specified  word  model  object  from  the
 *      vocabulary.
 *
 * Implementation:
 *
 *      For all entries in the vocabulary
 *        If word model id equals word identification number
 *         return(word)
 *      If specified word not found
 *         return NULL
 *
 * Dependency:
 *
 * Uses:     -  wm_id - Get word identification number
 *
 *************************************************************************/

 wm
voc_getword(VISE vise, V_WId wordid)
 {
 VOCWRD vp;

 for (vp = vise->vocab.firstword; vp != NULL; vp = vp->nextentry )
  if (wm_id(&(vp->wordmodel)) == wordid)
   return (&(vp->wordmodel));

 return (NULL);
 }


