#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_ai.h"
#include "v_fyi.h"
#include "v_gi.h"
#include "v_gm.h"
#include "v_ki.h"
#include "v_mem.h"
#include "v_ni.h"
#include "v_nm.h"
#include "v_par.h"
#include "v_syn.h"
#include "v_tb.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"
#include "v_wi.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:   syn -- Syntax Cluster
 *
 * Description:
 *
 *   The  syntax  cluster  implements  the single  abstract
 *   syntax  object  which  contains all  of the  grammars
 *   currently known to the system  as well as  a  single
 *   active  grammar  instance.   At any one time, only one
 *   grammar may be active
 *
 * Operations:
 *
 *   1. syn_init(vise) -- initialize the syntax object.
 *
 *   2. (gm) syn_creategrammar(vise, id) -- add a new grammar to the
 *      syntax and assign it the given identification number.
 *
 *   3. (gm) syn_getgrammar(vise, id); -- return the specified grammar
 *      model.
 *
 *   4. (V_Bool) syn_deletegrammar(vise, id) -- delete the specified grammar
 *      from the syntax.
 *
 *   5. (gm) syn_firstgrammar(vise); -- return the first grammar model in
 *      the syntax.
 *
 *   6. (gm) syn_nextgrammar(vise); -- return the next grammar model in
 *      the syntax.
 *
 *   7. (V_Int) syn_activategrammar(vise, id); -- create a single grammar
 *      instance corresponding to the grammar model specified.  The
 *      previous active grammar is obliterated by this operation.
 *
 *   8. (gi) syn_getactive(vise) -- return the active grammar instance.
 *
 *        VISE         vise; -- The VISE descriptor
 *        V_Int numbernodes; -- Number of nodes in the grammar
 *        V_GId          id; -- Grammar identification number
 *        V_Int    numpaths; -- Number of paths to trace back
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\SYN.C_V  $
 * 
 *    Rev 3.9   20 Mar 1992 12:54:30   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * Moved initialization of traceback structures to syn_activategrammar()
 * 
 *    Rev 3.8   22 Nov 1991 11:59:04   DCV
 * Eliminated incorrect but harmless initialization of start node's
 *   worst inherited score
 * 
 *    Rev 3.7   12 Nov 1991 16:00:20   DCV
 * Eliminated the numpaths argument, since it is no longer needed to size
 *   inherited path arrays.
 * Now sets the pointer to the first non-null arc in the grammar instance,
 *   and will not activate a grammar consisting only of null arcs.
 * Now returns standard VISE status codes.
 * Causes the start node to inherit a zero-cost path.
 * 
 *    Rev 3.6   08 Oct 1991 10:24:54   DCV
 * Changed syn_activategrammar() to ignore uninitialized words
 * Changed syn_activategrammar() to set the knode and kernel instance counts in the
 *   grammar model to the actual number of knodes and kernel instances allocated
 *   during activation.
 * 
 *    Rev 3.5   20 Sep 1991 09:54:58   DCV
 * Added code to initialize DGNs to 1 and pathlengths to 0 in iprs
 * 
 *    Rev 3.4   14 Aug 1991 16:36:48   DCV
 * Made GRAMMARMODELALLOC a VISE parameter
 * 
 *    Rev 3.3   18 Jul 1991 16:21:58   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   12 Jun 1991 14:34:30   DCV
 * Modified to support N-best path reporting.
 * 
 *    Rev 3.1   07 Nov 1990 16:36:22   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:58:46   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:16:08   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:19:28   DCV
 * Initial revision.
 *
 *************************************************************************/

/* A Syntax Entry Descriptor */
typedef struct synent_s {
  SYNENT   nextgrammar;   /* Next syntax entry */
  gm_t    grammarmodel;   /* Grammar model  */
} synent_t;

#define GM_SIZE sizeof(synent_t)

/**************************************************************************
 *
 * Name:     syn_init - Initialize Syntax Object
 *
 * Call:     syn_init(VISE vise);
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies: The syntax object
 *
 * Effect:   Initializes the syntax object.  All grammars within the
 *           syntax object are deleted.
 *
 * Implementation:
 *
 *           Initialize grammar pointers
 *
 *           For all grammar model entries in the syntax array
 *             Link grammar model entry to list of free entries
 *
 *           Initialize grammar model cluster
 *
 * Uses:     gm_init() -- initialize the grammar model cluster
 *
 ************************************************************************/

  V_Void
syn_init(VISE vise)
{
  vise->syntax.firstgrammar = vise->syntax.currentgrammar = vise->syntax.freegrammars = NULL;
  gm_init(vise);
}

/*************************************************************************
 *
 * Name:     syn_creategrammar -- Create new grammar in syntax
 *
 * Call:     grammar = syn_creategrammar(vise, id, numbernodes);
 *
 *             gm        grammar; -- The newly created grammar model
 *             VISE         vise; -- A VISE descriptor
 *             V_GId          id; -- Identification number of grammar
 *             V_Int numbernodes; -- Number of nodes in grammar
 *
 * Returns:  The newly created grammar,
 *           OR if there is not sufficient room for the grammar NULL
 *           is returned.
 *
 * Requires: The syntax should not already contain a grammar with
 *           the  specified identification number. syn_init must be
 *           invoked prior to any invocation of syn_creategrammar.
 *
 * Modifies: the syntax object
 *
 * Effect:   Adds a new grammar model object to the  syntax object.
 *           This grammar has the identification number and number
 *           of nodes  specified in  the calling parameters.   No
 *           checking  is  done  by this function to insure that the
 *           identification number is unique.
 *
 * Implementation:
 *
 *           If no free grammar model entry return NULL
 *
 *           Put the id and node count into the grammar
 *           Take an entry off the free list
 *
 *           Place it on the allocated list
 *
 * Uses:     gm_putinfo
 *           param_get -- get a VISE parameter
 *           MEM_alloclist -- allocate a linked list of objects
 *
 ************************************************************************/

  gm
syn_creategrammar(VISE vise, V_GId id)
{
  SYNENT  nextFreeGM;
  gm      grammar;

   /* If no free grammar model entries exist, make some */
  if (vise->syntax.freegrammars == NULL &&
      !(vise->syntax.freegrammars =
           (SYNENT) MEM_alloclist(vise->memCfg, GM_SIZE, (V_ULong) param_get(vise, GRAMMARMODELALLOC), SLOW_MEM, "syn_creategrammar synent")))
    return(NULL);

   /* At least one free grammar model entry now exists; get it */
  nextFreeGM = vise->syntax.freegrammars;
  vise->syntax.freegrammars = nextFreeGM->nextgrammar;

   /* Initialize the new grammar model */
  grammar = &(nextFreeGM->grammarmodel);
  gm_putinfo(grammar, id);

   /* Prepend it to the syntax's list of grammar models */
  nextFreeGM->nextgrammar = vise->syntax.firstgrammar;
  vise->syntax.firstgrammar = nextFreeGM;

   /* Return the new grammar model */
  return(grammar);
}

/**************************************************************************
 *
 * Name:     syn_getgrammar -- Get specified grammar model
 *
 * Call:     grammar = syn_getgrammar(vise, id);
 *
 *             gm grammar;  -- grammar object returned
 *             VISE  vise;  -- the VISE descriptor
 *             V_GId   id;  -- id number for grammar model
 *
 * Returns:  The  grammar  model object  in the  syntax  with  the
 *           specified
 *
 * Requires: syn_init must be invoked prior  to any  invocation  of
 *           syn_getgrammar.
 *
 * Modifies:
 *
 * Effect:   Returns the specified grammar, if it exists.
 *
 * Implementation:
 *
 *           For all grammar models in syntax
 *             If grammar identification number == id
 *               Return grammar
 *           If specified grammar not found
 *             Return NULL
 *
 * Uses:
 *
 **************************************************************************/

  gm
syn_getgrammar(VISE vise, V_GId id)
{
  SYNENT  scan;

  for (scan = vise->syntax.firstgrammar; scan != NULL; scan = scan->nextgrammar)
    if (gm_id(&(scan->grammarmodel)) == id)
      return(&(scan->grammarmodel));

  return(NULL);
}

/***************************************************************************
 *
 * Name:     syn_firstgrammar - Return first grammar model
 *
 * Call:     grammar = syn_firstgrammar(vise);
 *
 *             gm grammar; -- The first grammar model object in the syntax.
 *             VISE  vise; -- the VISE descriptor
 *
 * Returns:  The first grammar in the syntax, or NULL if the  syntax
 *           does not contain any grammars.
 *
 * Requires: syn_init must be invoked prior  to any  invocation  of
 *           syn_firstgrammar.
 *
 * Modifies: The syntax object
 *
 * Effect:   Returns the first grammar in the syntax, if any  exist.
 *           Resets  the syntax grammar model iterator such that the
 *           next call to  syn_nextgrammar will return  the  second
 *           grammar in the syntax.
 *
 * Implementation:
 *
 *           Sets the current grammar model pointer to the
 *             next-entry field of the entry pointed to by
 *             the first-grammar pointer
 *           Return the grammar model associated with the entry
 *             pointed to by the first-grammar pointer
 *
 * Uses:
 *
 **************************************************************************/

  gm
syn_firstgrammar(VISE vise)
{
  SYNENT  first;

  if ((first = vise->syntax.firstgrammar) == NULL) return(NULL);

  vise->syntax.currentgrammar = first->nextgrammar;
  return(&(first->grammarmodel));
}

/**************************************************************************
 *
 * Name:     syn_nextgrammar - Syntax grammar model iterator
 *
 * Call:     grammar = syn_nextgrammar(vise);
 *
 *             gm grammar; -- next grammar in syntax
 *             VISE  vise; -- the VISE descriptor
 *
 * Returns:  The next grammar model object in the syntax object,  or
 *           NULL if there are none left.
 *
 * Requires: syn_init must be invoked prior  to any  invocation  of
 *           syn_nextgrammar.
 *
 * Modifies: The syntax object
 *
 * Effect:   Returns the next grammar model  object  in  the  syntax
 *           object  if there is one, and updates the syntax grammar
 *           iterator to point to the next grammar in the syntax.
 *
 * Implementation:
 *
 *           If current-grammar entry pointer == NULL
 *             Return NULL
 *           else
 *             save current-grammar entry
 *             set current-grammar entry pointer to
 *               next field of saved entry
 *             Return grammar model of saved entry
 *
 * Uses:
 *
 *************************************************************************/

  gm
syn_nextgrammar(VISE vise)
{
  SYNENT  next;

  if ((next = vise->syntax.currentgrammar) == NULL) return(NULL);

  vise->syntax.currentgrammar = next->nextgrammar;
  return(&(next->grammarmodel));
}

/**************************************************************************
 *
 * Name:     syn_activategrammar - Activate a grammar
 *
 * Call:     outcome = syn_activategrammar(vise, id);
 *
 *             V_Int  outcome; -- Returned status code
 *             VISE      vise; -- The VISE dsecriptor
 *             V_GId       id; -- Identification number of grammar
 *
 * Returns:  A VISE status code.
 *
 * Requires: syn_init must be invoked prior  to any  invocation  of
 *           syn_activategrammar.
 *
 * Modifies: The syntax object.
 *
 *
 * Effect:   Builds  a  new  active grammar instance,  using  the
 *           specified  grammar  as a model. The previous active
 *           grammar, if any, is obliterated. The  node  instance
 *           scores are set to initial values.
 *
 * Implementation:
 *
 *           For all node models in the grammar model (last to first!)
 *             add a node instance with same id to grammar instance
 *             score of best path = 0 if node id = 0
 *             score of all other paths = V_INFINITY
 *             wordid = V_INFINITY
 *             tracebacknode = NULL
 *
 *           For all arc models in the grammar model
 *             add an arc instance to the grammar instance
 *               with corresponding source and destination nodes
 *             For all word models in the arc model
 *               add a word instance to the arc instance
 *                 (which replicates word model into word instance)
 *
 * Uses:     syn_getgrammar -- get specified grammar
 *           gi_init -- Initialize a grammar instance
 *           gi_addnode -- add a node to a grammar instance
 *           gi_getnode -- get a node instance
 *           gi_addarc -- add an arc to a grammar instance
 *           gm_firstarc -- get first arc in grammar
 *           gm_nextarc -- get next arc in grammar
 *           gm_nicount-- get node count from grammar model
 *           ni_putid -- put node id into node instance
 *           ni_putbestscore -- put into node instance: best path score
 *           tbp_setinfo -- put info into a traceback path element
 *           ni_putpathlist -- put a pathlist into a grammar node
 *
 *************************************************************************/

  V_Int
syn_activategrammar(VISE vise, V_GId id)
{
  gi     grminst;
  gm     grmmod;
  ni     nodeinstance;
  V_Int  nodecount;
  tbp    startpath;
  am     arcmodel;
  ai     arcinstance;
  ni     srcni, destni;
  wm     wordmodel;
  V_Int  status;

  if ((grmmod = syn_getgrammar(vise, id)) == NULL)
    return(GRAMMARNOTINSYNTAX);

  if (!(grminst = gi_init(vise, grmmod)))
    return(OUTOFMEMORY);

   /* First initialize the node instances */
  for (nodecount = gm_nicount(grmmod); nodecount--; )
    if ((nodeinstance = gi_addnode(grminst, nodecount)) == NULL)
      return(BADNODECOUNT);

   /* Initialize the dynamic programming data objects */
  tbf_init(vise);
  tbn_init(vise);
  tbp_init(vise);
  if (tb_init(vise) == NULL) return(CANTINITIALIZETBTREE);

   /* Create a path to the start node with zero cost */
  if ((startpath = tbp_alloc(vise)) == NULL) return(CANTALLOCATETBP);
  tbp_setinfo(startpath, NULL, 0, V_INFINITY, NULL, (V_ULong)1, (V_Uns)0);
  tbp_setsuccessor(startpath, NULL);
  ni_putpathlist(nodeinstance, startpath);

   /* Then do the arc instances */
  for (arcmodel = gm_firstarc(grmmod); arcmodel != NULL; arcmodel = gm_nextarc(grmmod)) {
    if ((srcni = gi_getnode(grminst, nm_nid(am_sourcenode(arcmodel)))) == NULL)
      return(BADNODEID);

    if ((destni = gi_getnode(grminst, nm_nid(am_destinationnode(arcmodel)))) == NULL)
      return(BADNODEID);

    if ((arcinstance = gi_addarc(grminst, srcni, destni)) == NULL)
      return(BADARCCOUNT);

     /* If the arc is a null arc */
    if ((wordmodel = am_firstword(arcmodel)) == NULL)
       /* Then it is not the first non-null arc in the grammar */
      gi_setfirstnonnullarc(grminst, gi_lastarc(grminst));
    else
       /* Otherwise, add all the initialized words on the arc */
      do {
        if (wm_status(wordmodel) != UNINITIALIZED) {
          status = ai_addword(vise, arcinstance, wordmodel);
          if (status != VISESUCCESS) return(status);
         }
        }
      while ((wordmodel = am_nextword(arcmodel)) != NULL);
  }

   /* Don't activate a grammar containing nothing but null arcs */
  if (gi_firstnonnullarc(grminst) == gi_lastarc(grminst))
    return(ALLARCSNULL);

   /* Update the knode and kernel instance totals for the grammar
    so no special supplemental allocations will be required the
    next time it is activated, unless more words are initialized */
  gm_setkndcount(grmmod, ki_knodecount(vise));
  gm_setkicount(grmmod, wi_kernelcount(vise));

  return(VISESUCCESS);
}

/**************************************************************************
 *
 * Name:     syn_getactive - Get the active grammar
 *
 * Call:     grammar = syn_getactive(vise);
 *
 *             gi     grammar; -- The active grammar instance
 *             VISE      vise; -- The VISE dsecriptor
 *
 * Returns:  the active grammar instance object
 *
 * Requires: previous call to syn_activate
 *
 * Modifies:
 *
 * Effect:   Returns the current active grammar object
 *
 * Implementation:
 *
 *           Return the active grammar object field of the syntax object
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

  gi
syn_getactive(VISE vise)
{
  return(&vise->syntax.activegrammar);
}

#endif

/**************************************************************************
 *
 * Name:     syn_deletegrammar - Delete grammar from syntax
 *
 * Call:     syn_deletegrammar(vise, id);
 *
 *             VISE    vise;  -- The VISE dsecriptor
 *             V_GId     id;  -- grammar identification number.
 *
 * Returns:  TRUE if the grammar is successfully deleted or FALSE if
 *           the grammar is not found in the syntax.
 *
 * Requires: syn_init must be invoked prior  to any  invocation  of
 *           syn_deletegrammar.
 *
 * Modifies: The syntax object
 *
 * Effect:   Deletes the specified grammar from the syntax if it  is
 *           present.   Returns  an error code if the grammar is not
 *           found.
 *
 * Implementation:
 *
 *           First, check if the id == the id of the current grammar
 *             If it does, then make the current grammar the one after
 *             the current grammar.
 *
 *           For all entries in the syntax
 *             If grammar id of grammar model == specified id
 *               Splice entry out of allocated list
 *               Append entry onto deallocated list
 *               Deallocate associated grammar model
 *               Return sucess indication
 *           If specified grammar not found
 *             Return failure indication
 *
 * Uses:     gm_id
 *           syn_nextgrammar
 *           gm_deallocate -- deallocate the arcs in a grammar;
 *
 **************************************************************************/

  V_Bool
syn_deletegrammar(VISE vise, V_GId id)
{
  SYNENT  scan;
  SYNENT  last;
  gm      grammar;

  last = NULL;

  if (vise->syntax.currentgrammar != NULL)
    if (gm_id( &((vise->syntax.currentgrammar)->grammarmodel)) == id)
      syn_nextgrammar(vise);

  for (scan = vise->syntax.firstgrammar; scan != NULL; scan = scan->nextgrammar) {
    if (gm_id(grammar = &(scan->grammarmodel)) == id) {
       /* Deallocate all arcs in the grammar */
      gm_deallocate(vise, grammar);

       /* Remove this entry from the active list */
      if (last != NULL)
        last->nextgrammar = scan->nextgrammar;
      else
        vise->syntax.firstgrammar = scan->nextgrammar;

       /* Link this entry into the free list */
      scan->nextgrammar = vise->syntax.freegrammars;
      vise->syntax.freegrammars = scan;
      return(TRUE);
    }
    last = scan;
  }

  return(FALSE);
}
