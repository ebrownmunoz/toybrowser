#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "visemsg.h"
#include "v_par.h"
#include "version.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/*****************************************************************************
 *
 * PARAMETER Cluster - Get/Set VISE parameters.
 *
 * Contents:
 * 
 *  V_Bool param_init(vise) - initialize the parameter facility
 *    Returns TRUE if successful, FALSE otherwise
 *
 *  V_Bool param_set(vise, index, value) - set parameter "index" to "value"
 *    Returns TRUE if parameter was set, FALSE if index was invalid
 *
 *  param_t param_get(vise, index) - get value of parameter "index"
 *    Actually implemented as a macro. DOES NOT CHECK ARGUMENT!
 *
 *  V_Err SETPARAMETERS(VISE, message) - Set specified parameters
 *    Returns status of reply
 *
 *  V_Err GETPARAMETERS(VISE, message) - Get entire parameter block from VISE
 *    Returns status of reply
 *
 *****************************************************************************/

  /* Parameters of the system */
#define DEFAULT_VISEVERSION            VISEVERSION /* From version.h (READ ONLY) */

  /* Parameters of findword() */
#define DEFAULT_NUMHIGH                5     /* Number of high jin[0] in findword */
#define DEFAULT_NUMLOW                 5     /* Number of low jin[0] in findword */
#define DEFAULT_MINAMPDUR              5     /* Number of successive jin[0] above threshold needed for start or end of utterance in findword */
#define DEFAULT_MINLOWAVG              -70   /* Minimum allowable findword low average */
#define DEFAULT_STARTEXTRA             0     /* Extra bytes to add at the start */
#define DEFAULT_ENDEXTRA               0     /* Extra bytes to add at the end */

  /* Parameters of recloop() */
#define DEFAULT_ACTIVATIONTHRESHOLD    1200  /* Activation threshold */
#define DEFAULT_DEACTIVATIONTHRESHOLD  DEFAULT_ACTIVATIONTHRESHOLD  /* Must equal activation threshold */
#define DEFAULT_LOOSEDPTHRESH          DEFAULT_ACTIVATIONTHRESHOLD  /* Must equal activation threshold */
#define DEFAULT_TIGHTDPTHRESH          600   /* Pre-Speech (BOU) DP threshold */
#define DEFAULT_MUHTHAH                700   /* Maximum joker node value */
#define DEFAULT_GRACEPERIOD            50    /* Framecount of best score at wildcard node that elicits BOU thresholding */

  /* Parameters of recognition command */
#define DEFAULT_PATHSTACKSIZE          200   /* Maximum number of words in an utterance */

  /* Parameters of the Feature Extractor */
#define DEFAULT_JINNUMCOMPONENTS       NUMCOMPONENTS  /* From v_jin.h (READ ONLY) */
#define DEFAULT_JINSCALE               0     /* Maximum jin parameter value */
#define DEFAULT_NOISEBITMASK           0     /* Noise bit mask for samples */
#define DEFAULT_FRAMESHIFT             128   /* Shift from one frame to next */
#define DEFAULT_FRAMEWIDTH             256   /* FRAMELENGTH in v_dsp.h */
#define DEFAULT_SAMPLERATE             10000 /* Default sampling rate */
#define DEFAULT_SIGBITS                12    /* Number of significant bits in an audio sample */
#define DEFAULT_AUDIOLEVEL             0     /* Default audio level setting */
#define DEFAULT_FEVERSION              FRONTEND  /* From version.h (READ ONLY) */
#define DEFAULT_NOISETRACKING          0     /* Default Backup Noise Tracking is OFF */

  /* Data Collection Parameters */
#define DEFAULT_AUDQUEUELEN            0

  /* Parameters of the source objects */
#define DEFAULT_MAXUNPROCESSEDFRAMES   1000  /* Maximum number of unprocessed jin frames retained (max poste restante size) */
#define DEFAULT_UNPROCESSEDQUANTUM     1000  /* Allocation quantum for mailbox (poste restante) for unprocessed jin frames */
#define DEFAULT_FRAMEQLENGTH           100   /* Maximum number of processed jin frames retained */
#define DEFAULT_FRAMEQFWDTOLERANCE     2     /* Frame Queue forward tolerance (in bytes of sampled data) */
#define DEFAULT_FRAMEQBWDTOLERANCE     0     /* Frame Queue backward tolerance (in bytes of sampled data) */
#define DEFAULT_JINCLASSSIZE           DEFAULT_FRAMEQLENGTH + DEFAULT_MAXUNPROCESSEDFRAMES + 1
#define DEFAULT_JINTOLERANCE           -1    /* Maximum sequence number gap (in kilobytes, or SN_MAX/2 if less than zero) */
#define DEFAULT_BOUPADLENGTH           0     /* Maximum number of silence padding frames at BOU */
#define DEFAULT_EOUPADLENGTH           0     /* Maximum number of silence padding frames at EOU */
#define DEFAULT_PADWORDINDEX           32510 /* LONG_SILENCE -- Silence padding frame */

  /* Parameters of memory system */
#define DEFAULT_ARCMODELALLOC          10    /* Arc Models per allocation */
#define DEFAULT_GRAMMARMODELALLOC      3     /* Grammar Models per allocation */
#define DEFAULT_KERNELMODELALLOC       500   /* Kernel Models per allocation */
#define DEFAULT_WORDENTRYALLOC         100   /* Word entries per allocation */
#define DEFAULT_WORDMODELALLOC         50    /* Word models per allocation */
#define DEFAULT_TRACEBACKPATHALLOC     1000  /* Path Elements per allocation */
#define DEFAULT_TRACEBACKNODEALLOC     1000  /* Traceback Nodes per allocation */
#define DEFAULT_TRACEBACKFRAMEALLOC    100   /* Traceback Frames per allocation */

  /* Parameters of adjustTemplate() */
#define DEFAULT_WEIGHTNORM             16384 /* Training weight normalization */

static param_t parameters[NPARAM] = {
  DEFAULT_NUMHIGH,
  DEFAULT_NUMLOW,
  DEFAULT_MINAMPDUR,
  DEFAULT_MINLOWAVG,
  DEFAULT_STARTEXTRA,
  DEFAULT_ENDEXTRA,
  DEFAULT_ACTIVATIONTHRESHOLD,
  DEFAULT_DEACTIVATIONTHRESHOLD,
  DEFAULT_LOOSEDPTHRESH,
  DEFAULT_TIGHTDPTHRESH,
  DEFAULT_MUHTHAH,
  DEFAULT_GRACEPERIOD,
  DEFAULT_VISEVERSION,
  DEFAULT_ARCMODELALLOC,
  DEFAULT_GRAMMARMODELALLOC,
  DEFAULT_KERNELMODELALLOC,
  DEFAULT_WORDENTRYALLOC,
  DEFAULT_WORDMODELALLOC,
  DEFAULT_TRACEBACKPATHALLOC,
  DEFAULT_TRACEBACKNODEALLOC,
  DEFAULT_TRACEBACKFRAMEALLOC,
  DEFAULT_FRAMEQLENGTH,
  DEFAULT_FRAMEQFWDTOLERANCE,
  DEFAULT_FRAMEQBWDTOLERANCE,
  DEFAULT_JINCLASSSIZE,
  DEFAULT_JINTOLERANCE,
  DEFAULT_PATHSTACKSIZE,
  DEFAULT_JINSCALE,
  DEFAULT_NOISEBITMASK,
  DEFAULT_WEIGHTNORM,
  DEFAULT_FRAMESHIFT,
  DEFAULT_JINNUMCOMPONENTS,
  DEFAULT_FRAMEWIDTH,
  DEFAULT_SAMPLERATE,
  DEFAULT_SIGBITS,
  DEFAULT_AUDIOLEVEL,
  DEFAULT_BOUPADLENGTH,
  DEFAULT_EOUPADLENGTH,
  DEFAULT_FEVERSION,
  DEFAULT_PADWORDINDEX,
  DEFAULT_AUDQUEUELEN,
  DEFAULT_MAXUNPROCESSEDFRAMES,
  DEFAULT_UNPROCESSEDQUANTUM,
  DEFAULT_NOISETRACKING
};

/**************************************************************************
 *
 * param_init - initialize the parameter facility
 *
 *  The parameters are stored in STATIC_MEM to prevent warmstart() from
 *  wiping them out.  Hence param_init() must be called in VISE_create().
 *
 ***************************************************************************/

  V_Bool
param_init(VISE vise)
{
  V_Int  paramId;

  vise->parameters = (param_t *) MEM_alloc(vise->memCfg, (V_ULong) sizeof(param_t), (V_ULong) NPARAM, STATIC_MEM, "param_init");
  if (vise->parameters) {
    paramId = NPARAM;
    VBX_DEBUG(VBX_print("  VISE Parameters:\n"));
    while (paramId--) {
      vise->parameters[paramId] = parameters[paramId];
      VBX_DEBUG(VBX_print("   %3.3d %9.9d %9.9d\n", paramId, vise->parameters[paramId], parameters[paramId]));
    }
  }

  return((V_Bool)(vise->parameters != NULL));
}

/**************************************************************************
 *
 * param_set - set a parameter to a specified value
 *
 ***************************************************************************/

  V_Bool
param_set(VISE vise, parid_t index, param_t value)
{
  if (index >= FIRSTPARAM && index <= LASTPARAM) {
    vise->parameters[(V_Int) index] = value;
    return(TRUE);
  } else {
    return(FALSE);
  }
}

/**************************************************************************
 *
 *  param_get - get the value of a parameter
 *
 ***************************************************************************/

#ifdef NOMACROS
  param_t
param_get(VISE vise, parid_t index)
{
  if (index >= FIRSTPARAM && index <= LASTPARAM)
    return(vise->parameters[(V_Int) index]);
  else
    return((param_t) 0);
}
#endif

/**************************************************************************
 *
 *  SETPARAMETERS - the VISE command to set VISE parameters
 *
 *  Description:  In response to a command containing a list of
 *                (index, value) pairs, SETPARAMETERS sets the value of
 *                the parameter corresponding to each valid index to the
 *                specified value.  The command may safely contain a null
 *                list, a list with indices out of range, or a list with
 *                duplicate indices.  If any indices are out of range,
 *                SETPARAMETERS reports BADPARINDEX, but still sets the
 *                rest.  If there are duplicate indices, the value paired
 *                with the last occurence is used.
 *
 ***************************************************************************/

  V_Err
SETPARAMETERS(VISE vise)
{
  V_Uns  parmIndex = NPARAM;     /* index of current parameter */
  V_Int  parmValue = 0;          /* value to set current parameter to */
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  vise_status = VISESUCCESS;
  while (MSG_readUns(vise->incoming, &parmIndex) && MSG_readInt(vise->incoming, &parmValue))
    if (!param_set(vise, (parid_t) parmIndex, (param_t) parmValue))
      vise_status = BADPARINDEX;

  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _SETPARAMETERS);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

/**************************************************************************
 *
 *  GETPARAMETERS - the VISE command to get VISE parameters
 *
 *  Description:  In response to a command message containing a list of
 *                the indices of the desired parameters, GETPARAMETERS
 *                sends a reply containing one (index, value) pair for
 *                each valid index requested.  The command may safely
 *                contain a null list, a list with indices out of range,
 *                or a list with duplicate indices.  If any index is out
 *                of range, GETPARAMETERS reports BADPARINDEX, but still
 *                gets the rest.
 *
 ***************************************************************************/

  V_Err
GETPARAMETERS(VISE vise)
{
  V_Uns  parmIndex;     /* parameter index */
  V_Bool requested[NPARAM];
  V_Uns  numRequested;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  for (parmIndex = 0; parmIndex < NPARAM; parmIndex++)
    requested[parmIndex] = FALSE;

  vise_status = VISESUCCESS;
  while (MSG_readUns(vise->incoming, &parmIndex)) {
    if (parmIndex >= FIRSTPARAM && parmIndex <= LASTPARAM)
      requested[parmIndex] = TRUE;
    else
      vise_status = BADPARINDEX;
  }
  MSG_closeRead(vise->incoming, &status);

  numRequested = 0;
  for (parmIndex = 0; parmIndex < NPARAM; parmIndex++)
    if (requested[parmIndex]) numRequested++;
  
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) ((numRequested * 2 + 2) * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _GETPARAMETERS);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    for (parmIndex = 0; parmIndex < NPARAM; parmIndex++)
      if (requested[parmIndex]) {
        MSG_writeUns(vise->outgoing, parmIndex);
        MSG_writeInt(vise->outgoing, (V_Int) param_get(vise, (parid_t) parmIndex));
      }
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

