#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_alg.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_div.h"
#include "v_jin.h"
#include "v_ki.h"
#include "v_km.h"
#include "v_mem.h"
#include "visemsg.h"
#include "v_tm.h"
#include "v_voc.h"
#include "v_wi.h"
#include "v_wm.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/* Private prototypes */
static V_Void wordtrace(V_Int (*buffer)[MAXDWELL], wm, V_Int, V_Int *);
static V_Void writrace(V_Int (*buffer)[MAXDWELL], wi, V_Time);

/**************************************************************************
 *
 * Name:      TRAINWORD - Train one word
 *
 * Returns:   Status of reply message
 *
 * Requires:  The function WARMSTART must have been called before this
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                                  Type
 *
 *              word id                                   V_Uns
 *              word start time                           V_SN
 *              word end time                             V_SN
 *              training weighting factor                 V_Int
 *              maximum allowable recognition word score  V_Int
 *
 * Modifies:  The specified word's templates
 *
 * Effect:    Trains the specified word from the region of the acoustic
 *            parameter buffer defined by the specified start and end
 *            times. Training is performed by doing word-level dynamic 
 *            programming.  The templates for the specified word are
 *            updated.  The weighting factor is used to weight old
 *            training data against new training data.
 *
 * Implementation:
 *
 *            Get id of word to train
 *            Get maximum allowable training score
 *            Get the weighing factor
 *
 *            Get word model
 *            Get word instance and number of kernels for word
 *            Wake up all the kernels in the word
 *
 *            For each feature frame,
 *              run the word level dynamic programming
 *              write traceback information
 *
 *            If a good training score results,
 *              Perform kernel-level traceback
 *              For each kernel,
 *                adjust the template, using the new feature data
 *              send VISESUCCESS
 *            else
 *              send BADSCORE
 *
 *            Exception handling:
 *            if word is not in the vocabulary,
 *              send WORDNOTINVOCAB.
 *            if duration of utterance > maximum allowed,
 *              send WORDDURATIONTOOLONG.
 *            if duration of utterance < number of kernels,
 *              send WORDDURATIONTOOSHORT.
 *            if the feature data cannot be read,
 *              send CANTREADFRAME
 *            if trying to train a word with no templates,
 *              send WORDHASNOTEM.
 *            if adjustTemplate() fails
 *              send the error code it returns.
 *
 *            The traceback buffer a local data structure used by TRAINWORD's
 *            subroutines writrace and wordtrace.  It contains one word for
 *            each frame for each kernel, in which writrace writes the dwell
 *            for the given kernel for the given frame.  wordtrace then determines
 *            the best path by tracing back from the entry for the last kernel
 *            for the last frame.  The frame number is the more rapidly varying
 *            index for this array.  The kernels appear in reverse order, with
 *            the dwells for the last kernel at the start of the buffer.
 *
 *            processword is a routine that is shared with the recognition
 *            dynamic programming.  Here a word instance for the word involved
 *            is created, and processword is invoked each frame with an initial
 *            seedscore of zero, and with a seedscore of V_INFINITY thereafter.
 *            processword requires the global minimum score, the time (frame
 *            number), and the feature frame be loaded into the tm cluster each
 *            frame.
 *
 *            A check is made that the average score per frame for the best
 *            path found by the dynamic programming is no greater than a
 *            specified value.  It is possible that the dwell constraints could
 *            cause score worse than in the original recognition (in somewhat
 *            subtle ways). They could even result in a bad traceback.  The
 *            latter is indicated by a score of infinity in the last kernel,
 *            implying that the traceback path leads to a time after the start
 *            time, and thus the first kernel was seeded with V_INFINITY.
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\TRAINWRD.C_V  $
 * 
 *    Rev 3.10   20 Mar 1992 13:03:20   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.9   22 Nov 1991 10:46:52   DCV
 * Modified for new hybrid version of adjustTemplate()
 * 
 *    Rev 3.8   08 Oct 1991 10:32:58   DCV
 * Starting and ending times are now signed quantities.
 * Now checks to make sure it has read each frame of jin data properly.
 * 
 *    Rev 3.7   14 Aug 1991 16:43:10   DCV
 * Removed MEM_init() (to main dispatch loop).
 * 
 *    Rev 3.6   02 Aug 1991 11:31:22   DCV
 * Installed new message system
 * 
 *    Rev 3.5   18 Jul 1991 16:25:28   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.4   27 Feb 1991 11:12:18   DCV
 * Changed for floating-point weighting scheme
 * Changed for readability (to parallel segword.c)
 * 
 *    Rev 3.3   10 Jan 1991 15:22:42   DCV
 * Consolidated template updating in a single call to adjustTemplate()
 * 
 *    Rev 3.2   07 Nov 1990 16:37:52   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.1   25 Sep 1990 10:44:52   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 3.0   13 Sep 1990 14:00:10   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:17:46   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:27:48   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
TRAINWORD(VISE vise)
{
  V_Uns   wid;                 /* word id                          */
  V_SN    startSN;             /* starting SN of utterance         */
  V_SN    endSN;               /* ending SN of utterance           */
  V_SN    snReq, sn;           /* sequence number of current frame */
  V_Uns   weight;              /* weight of old template means     */
  V_Uns   maxScore;            /* maximum allowable score          */
  V_Int   numKernels;          /* number of kernels                */
  V_Int   numKnodes;           /* number of kernel traceback nodes */
  V_Time  starttime;           /* arbitrary starting frame id      */
  V_Time  toff;                /* frame id offset                  */
  V_Int   duration;            /* length of utterance              */
  wm      word;                /* word model object                */
  wi_t    wdInstRep;           /* word instance representation     */
  V_Int   bestLastScore;       /* minimum score in last kernel     */
  V_Long  totScore;            /* accumulated total score          */
  V_Int   wordMin;             /* minimum score for word           */
  V_Int   score;               /* score for final kernel           */
  V_Int   seedScore;           /* score with which to seed         */
  ki      finalKernel;         /* last kernel instance in word     */
  km      kernel;              /* kernel model object              */
  kt      ktrainer;            /* kernel training object           */
  V_Int * dwell;               /* dwell iterator                   */
  jinr    featureFrame;        /* Jin frame                        */
  V_Int   dwells[NUMKERNEL];   /* kernel dwells                    */
  V_Int   (*trace)[MAXDWELL];  /* Traceback buffer                 */
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status;

  /* Get the trainword command arguments */
  MSG_readUns(vise->incoming, &wid);
  MSG_readSN(vise->incoming, &startSN);
  MSG_readSN(vise->incoming, &endSN);
  MSG_readUns(vise->incoming, &weight);
  MSG_readUns(vise->incoming, &maxScore);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

  if (!(word = voc_getword(vise, (V_WId) wid)))
    VISE_EXIT(WORDNOTINVOCAB);

  if (!JINSRC_canread(vise->jinSource, startSN, endSN))
    VISE_EXIT(CANTREADFRAME);

  if ((duration = (V_Int) JINSRC_numframes(vise->jinSource, &startSN, &endSN)) > MAXDWELL)
    VISE_EXIT(WORDDURATIONTOOLONG);

  if ((numKernels = wm_numkernels(word)) > duration)
    VISE_EXIT(WORDDURATIONTOOSHORT);

  if (wm_status(word) != TEMPLATES)
    VISE_EXIT(WORDHASNOTEM);

  /* Accumulate the total number of kernel traceback nodes required */
  numKnodes = 0;
  for (kernel = wm_firstkernel(word); kernel != NULL; kernel = km_nextkernel(kernel))
    numKnodes += km_mindwell(kernel);

  if (numKnodes > duration)
    VISE_EXIT(WORDDURATIONTOOSHORT);

  VBX_DEBUG(VBX_print("  Trainword: duration = %d; numKernels = %d; numKnodes = %d\n", duration, numKernels, numKnodes));

  /* Set up an area for the dynamic programming data objects */
  trace = (V_Int(*)[MAXDWELL]) MEM_alloc(vise->memCfg, MAXDWELL * sizeof(V_Int), NUMKERNEL, FAST_MEM, "TRAINWORD traceback buffer");

  wi_init(vise, (V_ULong) numKernels);
  ki_init(vise, (V_ULong) numKnodes);
  wi_replicate(vise, &wdInstRep, word);

  /* Wake all kernels of word. */
  while( numKernels-- )
    finalKernel = wi_wakekernel(&wdInstRep);

  wordMin = 0;
  tm_setstatus(vise, TEMPLATES);
  seedScore = 0;
  totScore = 0L;

  /* Create a starting frame index */
  starttime = (V_Time) (startSN & (SN) 8191);
  snReq = startSN;
  for (toff = 0; toff < (V_Uns) duration; toff++) {
    if (!JINSRC_read(vise->jinSource, &snReq, &featureFrame, &status))
      VISE_EXIT(status);
    /* The superfluous and misleading cast of the sum of V_Time types as V_Time circumvents a bug in the M$ compiler */
    tm_init(vise, &featureFrame, (V_Time) (starttime + toff), wordMin);

    VBX_DEBUG(VBX_print("    Trainword: doing DP on feature frame @%ld\n", snReq));

    /* Perform dynamic programming on the frame. */
    processword(vise, &wdInstRep, seedScore, NULL, &wordMin, &bestLastScore);

    /* Record kernel-level traceback information. */
    writrace(trace, &wdInstRep, toff);
    seedScore = V_INFINITY;
    totScore += (V_Long) wordMin;

    /* Remember the SN, and get the next frame, regardless of SN */
    sn = snReq;
    snReq = -1;
  }
  VBX_DEBUG(if (sn != endSN) VBX_print("  Trainword::processword: startSN = %ld, sn = %ld, endSN = %ld\n", startSN, sn, endSN));

  /* Get the score for the final kernel */
  score = finalKernel->kilastknode->kiscore;

  /* Check that the average frame score for the training
  recognition is less than the maximum allowed */
  totScore += (V_Long) (score - wordMin);
  if (score >= V_INFINITY || rndiv32(totScore, duration) > maxScore) {
    VBX_DEBUG(VBX_print("  TRAINWORD: BADSCORE (avg frameScore (%d) > maxScore (%u) over %d frames)\n",
                        rndiv32(totScore, duration), maxScore, duration));
    VISE_EXIT(BADSCORE);
  }

  /* Perform kernel-level traceback */
  wordtrace(trace, word, duration, dwell = dwells);

  /* Iterate over all kernels in the word model */
  snReq = sn = startSN;
  for (kernel = wm_firstkernel(word), ktrainer = wm_firstkt(word); kernel != NULL; kernel = km_nextkernel(kernel), dwell++) {

    /* Adjust the kernel's template and accumulator using the new acoustic data from the word level traceback */
    if ((status = adjustTemplate(vise, kernel, ktrainer, &snReq, *dwell, weight)) != VISESUCCESS)
      VISE_EXIT(status);
    VBX_DEBUG(VBX_print("    Trainword: doing adjustment on feature frame @%ld\n", snReq));

    /* Update the observed min/max dwells */
    if (ktrainer) {
      kt_adjustobsdwells(ktrainer, *dwell);
      ktrainer = kt_nextkt(ktrainer);
    }

    /* Remember the SN, and get the next frame, regardless of SN */
    sn = snReq;
    snReq = -1;
  }
  VBX_DEBUG(if (sn != endSN) VBX_print("  Trainword::adjusttemplate: startSN = %ld, sn = %ld, endSN = %ld\n", startSN, sn, endSN));

  VISE_EXIT(VISESUCCESS);

  VBX_DEBUG(MEM_printCfg(vise->memCfg, FAST_MEM, "end of TRAINWORD"));

  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _TRAINWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

/**************************************************************************
 *
 * Name:      STARTTRAININGWORD -- initialize a word's training data accumulator
 *
 *            Initializes the training data for a subsequent training pass
 *
 * Returns:   FALSE if reply is impossible, TRUE otherwise
 *
 * Requires:  The function WARMSTART must have been called before this one,
 *            and the word model and its templates should already have been
 *            downloaded.
 *
 *            The format of the message should be as follows:
 *
 *              Contents           Type
 *
 *              word id            V_Uns
 *
 * Modifies:  The specified word's accumulated training data
 *
 * Effect:    Clears the accumulated training data for a word
 *
 * Implementation:
 *
 *            Initialize the training data for each kernel in the word
 *
 *
 **************************************************************************/

  V_Err
STARTTRAININGWORD(VISE vise)
{
  V_Uns  wid;        /* word id                */
  wm     word;       /* word model object      */
  km     kernel;     /* kernel model object    */
  kt     ktrainer;   /* kernel training object */
  V_Int  minDwell;
  V_Int  optDwell;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

   /* Get the command argument */
  MSG_readUns(vise->incoming, &wid);

  MSG_closeRead(vise->incoming, &status);
  if (status)
    VISE_EXIT(status);

  if ((word = voc_getword(vise, (V_WId) wid)) == NULL)
    VISE_EXIT(WORDNOTINVOCAB);

  if (wm_status(word) != TEMPLATES)
    VISE_EXIT(WORDHASNOTEM);

   /* Thus we have real word with wm_numkernels(word) > 0 */

  if (!wm_firstkt(word)) {
     /* This is first pass of batch training */
    if (!wm_allocatekt(vise, word))
      VISE_EXIT(OUTOFMEMORY);
    vise->firstPass = TRUE;
  }

  for (ktrainer = wm_firstkt(word); ktrainer != NULL; ktrainer = kt_nextkt(ktrainer)) {
    kt_settrainingcount(ktrainer, 0);
    kt_clearaccumulator(ktrainer);
     /* Initialize for extremum search */
    kt_putobsdwells(ktrainer, MAXDWELL, 1);
  }

  vise->batchTrain = TRUE;

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _STARTTRAININGWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

/**************************************************************************
 *
 * Name:      FINISHTRAININGWORD -- train a word with its accumulated data
 *
 * Returns:   FALSE if reply is impossible, TRUE otherwise
 *
 * Requires:  The function WARMSTART must have been called before this one
 *
 *            The format of the message should be as follows:
 *
 *              Contents           Type
 *
 *              word id            V_Uns
 *              old data weight    V_Uns
 *              train dwells?      V_Uns
 *
 * Modifies:  The specified word's template
 *
 * Effect:  
 *
 * Implementation:
 *
 **************************************************************************/

  V_Err
FINISHTRAININGWORD(VISE vise)
{
  V_Uns   wid;                 /* word id                              */
  V_Uns   oldWeight;           /* old data percentage in final mix     */
  V_Uns   trainDwells;         /* train dwells?                        */
  wm      word;                /* word model object                    */
  km      kernel;              /* kernel model object                  */
  kt      ktrainer;            /* kernel training object               */
  tem     template;            /* kernel template                      */
  jinr    featureVector;       /* buffer for unpacked feature data     */
  V_Long  newWeight;           /* mixing weight for new feature values */
  V_Int   i, k;
  V_Int   minDwl, maxDwl;
  V_Int   minDwell[NUMKERNEL]; /* adjusted min dwells                  */
  V_Int   maxDwell[NUMKERNEL]; /* adjusted max dwells                  */
  V_Int   avgMaxDwl, reservDwl, sumMaxDwl, numOverDwl;
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status;
  V_Long  oldNumber;

   /* Get the command arguments */
  MSG_readUns(vise->incoming, &wid);
  MSG_readUns(vise->incoming, &oldWeight);
  MSG_readUns(vise->incoming, &trainDwells);

  MSG_closeRead(vise->incoming, &status);
  if (status)
    VISE_EXIT(status);

  if ((word = voc_getword(vise, (V_WId) wid)) == NULL)
    VISE_EXIT(WORDNOTINVOCAB);

  if (wm_status(word) != TEMPLATES)
    VISE_EXIT(WORDHASNOTEM);

  if (oldWeight > 100)
    VISE_EXIT(BADWEIGHTFACTOR);

  avgMaxDwl = MAXDWELL / wm_numkernels(word);
  sumMaxDwl = numOverDwl = reservDwl = 0;
  for (kernel = wm_firstkernel(word), ktrainer = wm_firstkt(word), k = 0;
       kernel != NULL && ktrainer != NULL;
       kernel = km_nextkernel(kernel), ktrainer = kt_nextkt(ktrainer), k++) {

    if ((newWeight = kt_trainingcount(ktrainer)) == 0)
      VISE_EXIT(CANTADJUSTTEMPLATE);

    if (oldWeight < 100) {
      /* Let some new data in */
      oldNumber = (oldWeight * newWeight) / (100 - oldWeight);

      template = km_template(kernel);

      /* Unpack the old template vector */
      tem_read(template, &featureVector);

      /* Mix the old template vector with the newly accumulated feature data */
      for (i = 0; i < NUMCOMPONENTS; i++)
        jin_setcomponent(&featureVector, i, (V_Byte)ldiv32(kt_accumulator(ktrainer, i) + (V_Long)jin_getcomponent(&featureVector, i) * oldNumber, newWeight + oldNumber));

      tem_write(template, &featureVector);
    }

    if (trainDwells) {
      kt_getobsdwells(ktrainer, &minDwl, &maxDwl);
      minDwell[k] =     (minDwl + 1) / 2;
      maxDwl = (3 * maxDwl + 1) / 2;
      if (maxDwl > MAXKERNELDWELL) maxDwl = MAXKERNELDWELL;
      maxDwell[k] = maxDwl;
      sumMaxDwl += maxDwl;
       /* Count long and leftover from short */
      if (maxDwl > avgMaxDwl)
        numOverDwl++;
      else
        reservDwl += (avgMaxDwl - maxDwl);
    }
  }

   /* Final dwells adjustment */
  if (trainDwells) {
    if (numOverDwl)
      reservDwl /= numOverDwl;
     /* Force minimum minDwell on the first and last kernel, which may be silence */
    minDwell[0] = 1;
    minDwell[wm_numkernels(word) - 1] = 1;
    for (kernel = wm_firstkernel(word), k = 0; kernel != NULL;
         kernel = km_nextkernel(kernel), k++) {
      if (sumMaxDwl > MAXDWELL && maxDwell[k] > avgMaxDwl)
        maxDwell[k] = avgMaxDwl + reservDwl;
      if (maxDwell[k] > MAXKERNELDWELL)
        maxDwell[k] = MAXKERNELDWELL;
      if (minDwell[k] > maxDwell[k])
        minDwell[k] = maxDwell[k];
      if (minDwell[k] < 1)
        minDwell[k] = 1;
      km_putdwells(kernel, minDwell[k], maxDwell[k] - minDwell[k]);
    }
  }

  vise->batchTrain = FALSE;
  vise->firstPass = FALSE;

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _FINISHTRAININGWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

/**************************************************************************
 *
 * Name:      WORDTRACE -- kernel-level traceback for word
 *
 * Returns:   nothing
 *
 * Requires:  Traceback information must be in the traceback buffer.
 *            Dwell buffer must be at least as long as there are
 *            kernels in the word.
 *
 * Modifies:
 *
 * Effect:    Reads the kernel level segmentation of the word out of
 *            the traceback buffer.  The information is summarized in
 *            the dwell buffer in the form of durations for each kernel.
 *
 * Implementation:
 *
 *            For each kernel
 *              trace back to get the dwell
 *
 **************************************************************************/

  static V_Void
wordtrace(V_Int (*trace)[MAXDWELL], wm wordMod, V_Int duration, V_Int *dwell)
  /* V_Int   (*trace)[MAXDWELL] - traceback buffer */
  /* wm      wordMod            - word model */
  /* V_Int   duration           - the number of frames processed for the word */
  /* V_Int * dwell              - dwell counts of the kernels    */
{
  V_Int   frameNumber;  /* feature frame iterator */
  V_Int * dwellPtr;

  frameNumber = duration - 1;

  /* The kernel-level traceback buffer stores the last kernel for a 
     word first, while the dwells are returned so that the first dwell
     is for the first kernel */
  dwellPtr = &dwell[wm_numkernels(wordMod)];

  while (dwellPtr > &dwell[0]) {
    *--dwellPtr = (*(trace++))[frameNumber];
    frameNumber -= *dwellPtr;
  }
}

/**************************************************************************
 *
 * Name:      WRITRACE -- Write durations for traceback
 *
 * Returns:   nothing
 *
 * Requires:  Dynamic programming for the current frame must already have
 *            been completed for the word instance
 *
 * Modifies:  trace
 *
 * Effect:    The duration in frames of each kernel is written to the
 *            corresponding entry of the trace
 *
 * Implementation:
 *
 *            for each kernel starting from the last
 *              write the traceback information for the kernel
 *
 **************************************************************************/

  static V_Void
writrace(V_Int (*trace)[MAXDWELL], wi wordInst, V_Time time)
  /* V_Int (*trace)[MAXDWELL] - traceback buffer */
  /* wi  wordInst             - word instance to process */
  /* V_Time time              - current frame number */
{
  ki     curKer;         /* The current kernel */
  V_Int  wiActiveCount;
  V_Int  minDwell;       /* The minimum required dwell          */
  V_Int  optDwell;       /* Optional additional dwell permitted */
  V_Int  dwellCnt;       /* The current dwell count for kernel  */

  if ((wiActiveCount = wi_numactive(wordInst)) != 0) {
    for (curKer = wi_lastkernel(wordInst); wiActiveCount--; curKer-- ) {
      ki_getdwells(curKer, &minDwell, &optDwell, &dwellCnt);
      (*(trace++))[time] = optDwell - dwellCnt + minDwell;
    }
  }
}
