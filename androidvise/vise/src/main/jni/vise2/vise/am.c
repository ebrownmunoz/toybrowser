#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_am.h"
#include "v_mem.h"
#include "v_nm.h"
#include "v_par.h"
#include "v_wm.h"

/**************************************************************************
 *
 *	Name:				am -- Arc Model Cluster
 *
 *	Description:	The arc model	cluster	implements	the  abstract	arc
 *						model objects which populate the grammar model.  An arc
 *						model represents	a	directed  transition  between  two
 *						nodes  in  a  finite state grammar.  The arc is labeled
 *						with a list of word models.  The finite state  automata
 *						corresponding	to  the	grammar can make the transition
 *						represented when it recognizes an occurrence of one  of
 *						the word models which label the arc.
 *
 *	Operations:
 *
 *	1.	am_init(vise)
 *		Initialize arc model cluster.
 *
 *	2.	am_arcinit(arcmodel)
 *		Initialize an arc model.
 *
 *	3.	node = am_sourcenode(arcmodel)
 *		Return the source node for	the given arc.
 *
 *	4.	node = am_destinationnode(arcmodel)
 *		Return the destination node for the given arc.
 *
 *	5.	outcome = am_addword(vise, arcmodel, word)
 *		Add another word model to the given arc's word list.
 *
 *	6.	word = am_firstword(arcmodel)
 *		Return the first word model associated with the given arc.
 *
 *	7.	word = am_nextword(arcmodel)
 *		Return the next word model associated with the given arc.
 *
 *	8.	am_deallocate(vise, arcmodel)
 *		Return the words on the arc to the free list.
 *
 *          VISE  vise      --  The VISE descriptor
 *			am	  arcmodel	--	An arc model object
 *			wm	  word		--	A word model object
 *			nm	  node		--	A node model object
 *
 *	Computer:	TMS320C30 and iAPX86
 *
 *	Unit Test:	amtest.ct
 *
 *	$Log:   C:\USR\SRC\VISE\C\VCS\AM.C_V  $
 * 
 *    Rev 3.5   20 Mar 1992 12:25:00   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.4   29 Jan 1992 14:21:00   DCV
 * Changed basic types
 * Changed am_addword() to return pointer to wdent instead of bool_t
 * 
 *    Rev 3.3   14 Aug 1991 15:55:48   DCV
 * Now uses alloclist() to allocate word entry objects
 * 
 *    Rev 3.2   18 Jul 1991 15:44:20   DCV
 * Implemented dynamic memory allocation
 * 
 *    Rev 3.1   07 Nov 1990 16:03:04   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:43:42   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:00:42   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:27:22   DCV
 * Initial revision.
 *
 *************************************************************************/

/* Word Entry (Word on Arc) Descriptor */
typedef struct wdent_s {
	WDENT	nextword;		/* Next word entry on arc */
	wm 		thisword;		/* Word on arc			  */
} wdent_t;

/*************************************************************************
 *
 *	Name:			am_init - Initialize arc model cluster.
 *
 *	Calling Sequence:
 *
 *					am_init(VISE vise);
 *
 *	Returns:		V_Void
 *
 *	Requires:
 *
 *	Modifies:	The arc model cluster.
 *
 *	Effect:		Initializes the arc model cluster.	This operation must
 *					be  invoked  prior  to	the invocation of any other arc
 *					model operation.	Any arc models  in  existence  before
 *					the  invocation  of this function are destroyed by this
 *					operation.
 *
 *	Implementation:
 *
 *					Empty the free list
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

	V_Void
am_init(VISE vise)
	{
	vise->waPool.freewdent = NULL;
	}

/**************************************************************************
 *
 *	Name:			am_arcinit - Initialize an arc model
 *
 *	Calling Sequence:
 *
 *					am_arcinit(arcmodel)
 *
 *					am arcmodel -- arc model to initialize
 *
 *	Returns:		V_Void
 *
 *	Requires:	The arc model cluster has been initialized.
 *
 *	Modifies:	The arc model.
 *
 *	Effect:		Initializes the given arc model.
 *
 *	Implementation:
 *
 *					clear arc model word pointers
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS
	V_Void
am_arcinit(am arcmodel)
	{
	arcmodel->amfirstword = NULL;
	arcmodel->amcurrentword	= NULL;
	arcmodel->amlastword	= NULL;
	}
#endif

/*************************************************************************
 *
 *	Name:			am_sourcenode - Get source node for arc model
 *
 *	Calling Sequence:
 *
 *					node = am_sourcenode(arcmodel)
 *
 *					am arcmodel	-- arc model object
 *					nm node		-- source node of the arc
 *
 *	Returns:		A node model corresponding to the source	node	of  the
 *					given arc model.
 *
 *	Requires:	The arc model has been initialized.
 *
 *	Modifies:
 *
 *	Effect:		Returns the node model corresponding to the source node
 *					of the given arc model.
 *
 *	Implementation:
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/
#ifdef	NOMACROS

	nm
am_sourcenode(am arcmodel)
	{
	return(&(arcmodel->amsource));
	}

#endif

/*************************************************************************
 *
 *	Name:			am_destinationnode - Get destination node for arc model
 *
 *	Calling Sequence:
 *
 *					node = am_destinationnode(arcmodel)
 *
 *					am	arcmodel	-- arc model object
 *					nm	node		-- destination node of the arc
 *
 *	Returns:		A node model corresponding to the destination  node  of
 *					the given arc model.
 *
 *	Requires:	The arc model has been initialized.
 *
 *	Modifies:
 *
 *	Effect:		Returns the node model corresponding to the destination
 *					node of the given arc model.
 *
 *	Implementation:
 *
 *	Dependency:
 *
 *	Uses:
 *
 ***************************************************************************/
#ifdef	NOMACROS

	nm
am_destinationnode(am arcmodel)
	{
	return(&(arcmodel->amdestination));
	}

#endif

/***************************************************************************
 *
 *	Name:			am_addword - Add word to arc model
 *
 *	Calling Sequence:
 *
 *					result = am_addword(vise, arcmodel, word);
 *
 *					WDENT	result	  -- pointer to new word entry
 *                  VISE    vise      -- VISE descriptor
 *					am		arcmodel  -- arc model object
 *					wm		word	  -- word model object to add
 *
 *	Returns:		Pointer to new word entry if word was successfully added,
 *					or NULL if there was no room for the additional word.
 *
 *	Requires:	The arc model and  the	arc  model	cluster	have	been
 *					initialized.
 *
 *	Modifies:	The arc model
 *
 *	Effect:		Adds	the  given	word	model  to  the  list  of  words
 *					belonging to the given arc model.
 *
 *	Implementation:
 *
 *					If no free word model entries exist,
 *						Try to make some.
 *						If we cannot, return FALSE.
 *
 *					Get next word entry off of list of free entries.
 *					Place word model in new word entry.
 *					Place word entry on the arc model's list of words.
 *					Return TRUE.
 *
 *	Dependency:
 *
 *	Uses:			MEM_alloclist -- allocate a linked list of objects
 *					param_get -- get a VISE parameter
 *
 **************************************************************************/

	WDENT
am_addword(VISE vise, am arcmodel, wm word)
	{
	WDENT	nextFreeWDENT;

		/* If no free word model entries exist, make some */
	if (vise->waPool.freewdent == NULL &&
		(vise->waPool.freewdent =
          (WDENT) MEM_alloclist(vise->memCfg, sizeof(wdent_t), (V_ULong) param_get(vise, WORDENTRYALLOC), SLOW_MEM, "am_addword wdent")) == NULL)
        return(NULL);

		/*	At least one free word model entry exists; get it */
	nextFreeWDENT = vise->waPool.freewdent;
	vise->waPool.freewdent = nextFreeWDENT->nextword;

		/* Fill in the word model */
	nextFreeWDENT->thisword = word;

		/*	Append it to the arc model's list of word entries */
	if (arcmodel->amfirstword != NULL)
		(arcmodel->amlastword)->nextword = nextFreeWDENT;
	else
		arcmodel->amcurrentword = arcmodel->amfirstword	= nextFreeWDENT;
	arcmodel->amlastword = nextFreeWDENT;
	nextFreeWDENT->nextword = NULL;

	return(nextFreeWDENT);
	}

/***************************************************************************
 *
 *	Name:			am_firstword - Get first word on an arc
 *
 *	Calling Sequence:
 *
 *					word = am_firstword(arcmodel)
 *
 *					am	arcmodel	-- arc model object
 *					wm	word		-- first word model object
 *
 *	Returns:		The first word model on the given arc model, or NULL if
 *					there are no word models on the arc.
 *
 *	Requires:	The arc model and  the	arc  model	cluster	have	been
 *					initialized.
 *
 *	Modifies:
 *
 *	Effect:		Returns the first word model associated  with  the  arc
 *					model,  if	there  are any.  Updates the arc model word
 *					iterator such that  the  next  call  to  am_nextword	will
 *					return the second word model in the arc model.
 *
 *	Implementation:
 *
 *					If the arc has a word in its word list,
 *						Return the word.
 *					Else
 *						Return NULL.
 *
 *	Dependency:
 *
 *	Uses:			am_nextword
 *
 *************************************************************************/

	wm
am_firstword(am arcmodel)
	{
	arcmodel->amcurrentword = arcmodel->amfirstword;
	return(am_nextword(arcmodel));
	}

/**************************************************************************
 *
 *	Name:			am_nextword - Get next word on an arc
 *
 *	Calling Sequence:
 *
 *					word = am_nextword(arcmodel)
 *
 *					am	arcmodel	-- arc model object
 *					wm	word		-- next word model object
 *
 *	Returns:		The next word model on the given arc model, or NULL  if
 *					there are no word models left.
 *
 *	Requires:	The arc model and  the	arc  model	cluster	have	been
 *					initialized.  The am_firstword function has been called.
 *
 *	Modifies:	The arc model
 *
 *	Effect:		Returns the next word model  associated  with  the  arc
 *					model.
 *
 *	Implementation:
 *
 *					If the arc has another word on its word list,
 *						Return the word.
 *					Else
 *						Return NULL.
 *
 *	Dependency:
 *
 *	Uses:
 *
 ***************************************************************************/

	wm
am_nextword(am arcmodel)
	{
	WDENT nextentry;

	if ((nextentry = arcmodel->amcurrentword) != NULL)
		{
		arcmodel->amcurrentword = nextentry->nextword;
		return (nextentry->thisword);
		}
	else
		return(NULL);
	}

/*************************************************************************
 *
 *	Name:			am_deallocate - Deallocate the words on an arc
 *
 *	Calling Sequence:
 *
 *					am_deallocate(vise, arcmodel)
 *
 *                  VISE     vise;  -- the VISE descriptor
 *					am	 arcmodel;	-- arc model object
 *
 *	Returns:		V_Void
 *
 *	Requires:	The arc model and  the	arc  model	cluster	have	been
 *					initialized.
 *
 *	Modifies:	The arc model and free list of word entries
 *
 *	Effect:		Deallocate all words on an arc.
 *
 *	Implementation:
 *
 *					If the arc has words,
 *						Place the words on the free list.
 *					Reinitialize the arc.
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

	V_Void
am_deallocate(VISE vise, am arcmodel)
	{
	if (arcmodel->amfirstword != NULL)
		{
		(arcmodel->amlastword)->nextword = vise->waPool.freewdent;
		vise->waPool.freewdent = arcmodel->amfirstword;
		}
	am_arcinit(arcmodel);
	}


