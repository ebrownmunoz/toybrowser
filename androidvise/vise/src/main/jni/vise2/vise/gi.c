#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_ai.h"
#include "v_gi.h"
#include "v_gm.h"
#include "v_ki.h"
#include "v_mem.h"
#include "v_ni.h"
#include "v_wi.h"

/**************************************************************************
 *
 * Name:    gi -- Grammar Instance Cluster
 *
 * Description: The  grammar  instance cluster implements a single
 *      abstract  grammar  instance object, which is the object
 *      used for the representation of the active  grammar. A
 *      grammar instance consists of a set of arc instances and
 *      a set of node instances.
 *
 * Operations:
 *
 *  1. result = gi_init(vise, grammar, grammarmodel) 
 *   Initialize the grammar instance object.
 *
 *  2. node = gi_addnode(grammar, nodeid)
 *   Add an initialized node instance to the grammar.
 *
 *  3. arc = gi_addarc(grammar, source, destination)
 *   Add an arc instance, with source and destination nodes, 
 *   to the grammar instance.
 *
 *  4. node = gi_getnode(grammar, nodeid)
 *   Get the node instance in the grammar with the specified 
 *   identification number.
 *
 *  5. node = gi_firstnode(grammar)
 *   Get the first node instance  in the grammar instance.
 *
 *  6. node = gi_lastnode(grammar)
 *   Get the last node instance in the grammar instance.
 *
 *  7. arc = gi_firstarc(grammar)
 *   Get the first arc instance in the grammar instance.
 *
 *  8. arc = gi_lastarc(grammar)
 *   Get the last arc instance in the grammar instance.
 *
 *     V_Bool result   -- TRUE or FALSE
 *     gi   grammar   -- A grammar instance object.
 *     gm   grammarmodel -- A grammar model object.
 *     ni   node    -- A node instance object.
 *     ai   arc    -- An arc instance object.
 *     V_NId  nodeid   -- Node identification number.
 *     ni   source   -- Source node of arc.
 *     ni   destination  -- Destination node of arc.
 *
 * Computer: TMS320C30 and iAPX86
 *
 * Unit Test: gitest.ct
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\GI.C_V  $
 * 
 *    Rev 3.5   20 Mar 1992 12:35:14   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.4   29 Jan 1992 14:45:34   DCV
 * Changed basic types; added inclusion of v_ki.h
 * 
 *    Rev 3.3   08 Nov 1991 12:29:26   DCV
 * Eliminated inherited path structures in node instances.
 * Inherited paths now handled with lists of path elements.
 * Added gifirstnonnullarc to prevent arcs with no initialized words
 *   from aliasing as null arcs.
 * 
 *    Rev 3.2   18 Jul 1991 16:10:02   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.1   07 Nov 1990 16:07:18   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:45:02   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:07:16   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:59:08   DCV
 * Initial revision.
 *
 **************************************************************************/

/***************************************************************************
 *
 * Name:   gi_init - Initialize the active grammar instance object
 *
 * Calling Sequence:
 *
 *     grammar = gi_init(vise, grammarmodel)
 *
 *                  VISE    vise;           -- a VISE descriptor
 *     gi   grammar;  -- grammar instance object
 *     gm   grammarmodel; -- grammar model object
 *
 * Returns:  TRUE if successful, FALSE if insufficient memory
 *
 * Requires: Memory for all instance objects must have been deallocated
 *
 * Modifies: The grammar instance object.
 *
 * Effect:  Initializes the active grammar instance object.  Memory
 *     is allocated for nodes, arcs, word instances, kernel
 *     instances and kernel traceback nodes per the information
 *     in the grammar model.
 *
 * Implementation:
 *
 *     Set up the arc instance array for the grammar,
 *      or return FALSE.
 *     Compute the size of the node instances.
 *     Set up the node instance array for the grammar,
 *      or return FALSE.
 *     Initialize the arc instance,
 *          word instance,
 *          and kernel instance clusters,
 *      or return FALSE.
 *     Return TRUE.
 *
 * Dependency:
 *
 * Uses:   ai_init   --  initialize the arc instance cluster
 *     wi_init   --  initialize the word instance cluster
 *     ki_init   -- initialize the kernel instance cluster
 *     MEM_alloc  --  allocate memory
 *     gm_aicount  -- count of arc instances in grammar
 *     gm_nicount  -- count of node instances in grammar
 *     gm_wicount  -- count of word instances in grammar
 *     gm_kicount  -- count of kernel instances in grammar
 *     gm_kndcount  -- count of kernel traceback node instances
 *
 **************************************************************************/

 gi
gi_init(VISE vise, gm grmmod)
 {
    gi grminst = &vise->syntax.activegrammar;

  /* Allocate memory for the array of node instances */
 grminst->ginodes = (ni) MEM_alloc(vise->memCfg, NI_SIZE, gm_nicount(grmmod), FAST_MEM, "gi_init ni");
 if (grminst->ginodes == NULL)
      return(NULL);

  /* Allocate memory for the array of arc instances */
 grminst->giarcs = (ai) MEM_alloc(vise->memCfg, AI_SIZE, gm_aicount(grmmod), FAST_MEM, "gi_init ai");
 if (grminst->giarcs == NULL)
      return(NULL);

  /* Initialize the node instances of the grammar */
 grminst->gifreeni = grminst->ginodes;
 grminst->gimaxni = grminst->ginodes + gm_nicount(grmmod);

  /* Initialize the arc instances of the grammar */
 grminst->gifirstnonnullai = grminst->gifreeai = grminst->giarcs;
 grminst->gimaxai = grminst->giarcs + gm_aicount(grmmod);

  /* Initialize the arc, word and kernel instance clusters */
 if (!ai_init(vise, gm_wicount(grmmod)) || !wi_init(vise, gm_kicount(grmmod)) || !ki_init(vise, gm_kndcount(grmmod)))
      grminst = NULL;

    return grminst;
    }

/****************************************************************************
 *
 * Name:   gi_addnode - Add a node instance to a grammar
 *
 * Calling Sequence:
 *
 *     gi_addnode(grammar, nodeID)
 *     gi   grammar -- A grammar instance object.
 *     V_NId nodeID -- A grammar node ID.
 *
 * Returns:  An initialized node instance, or NULL if  none  are
 *     available.
 *
 * Requires: A call to gi_init must preceed any call to gi_addnode for
 *     a particular grammar instance.
 *
 * Modifies: The grammar instance.
 *
 * Effect:  Adds a new node instance  object to  the
 *     given grammar instance object.
 *
 * Implementation:
 *
 *     If a free node instance exists,
 *      Initialize it.
 *       Advance last-node pointer.
 *       Return node instance.
 *     Else
 *       Return NULL.
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
 ni
gi_addnode(gi grammar, V_NId nodeID)
 {
 return(grammar->gifreeni < grammar->gimaxni) ?
     (ni_nodeinit(grammar->gifreeni, nodeID),
               grammar->gifreeni++) : NULL);
 }
#endif

/***************************************************************************
 *
 * Name:   gi_addarc - Add an arc instance to a grammar
 *
 * Calling Sequence:
 *
 *     arc = gi_addarc(grammar, source, destination)
 *
 *     ai arc   -- Newly added arc instance object.
 *     gi grammar  -- A grammar instance object.
 *     ni source  -- Source node instance object.
 *     ni destination -- Destination node instance object.
 *
 * Returns:  The arc instance of the added arc or NULL  if  no more
 *     arc instances can be allocated.
 *
 * Requires: A call to gi_init must preceed any call to  gi_addarc
 *     for a particular grammar instance.
 *
 * Modifies: The grammar instance.
 *
 * Effect:  Adds an arc instance, with source and destination node
 *     instances, to the given grammar instance.
 *
 * Implementation:
 *
 *     If a free arc instance exists,
 *      Put source and destination node instances in arc instance.
 *       Advance last-arc pointer.
 *       Return arc instance.
 *     Else
 *       Return NULL.
 *
 * Uses:   - ai_arcinit -- initialize an arc instance object
 *
 **************************************************************************/

#ifdef NOMACROS
 ai
gi_addarc(gi grammar, ni source, ni destination)
 {
 return(grammar->gifreeai < grammar->gimaxai ?
     (ai_arcinit(grammar->gifreeai, source, destination),
               grammar->gifreeai++) : NULL );
 }
#endif

/***************************************************************************
 *
 * Name:   gi_firstarc - Get first arc instance in grammar
 *
 * Calling Sequence:
 *
 *     arc = gi_firstarc(grammar);
 *
 *     ai arc;  -- First arc instance in the grammar instance.
 *     gi grammar; -- A grammar instance object.
 *
 * Returns:  The first arc instance in the grammar instance, or NULL
 *     if there are no arc instances in the grammar instance.
 *
 * Requires: A call to gi_init must precede any calls to gi_firstarc for
 *     a particular grammar instance.
 *
 * Modifies: The grammar instance.
 *
 * Effect:  Returns the first arc instance in the grammar  instance
 *     object  and  resets the  grammar instance arc iterator
 *     such that the next invocation of  gi_narc  will  return
 *     the second arc instance in the grammar.
 *
 * Implementation:
 *
 *     If the grammar instance has an arc,
 *       Set current arc pointer to the
 *        second arc in the grammar.
 *       Return the first arc instance.
 *     Else
 *       Return NULL.
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
 ai
gi_firstarc(gi grammar)
 {
 return( grammar->giarcs );
 }


 ai
gi_lastarc(gi grammar)
 {
 return( grammar->gifreeai );
 }
#endif

/***************************************************************************
 *
 * Name:   gi_firstnode - Get first node instance in grammar
 *
 * Calling Sequence:
 *
 *     node = gi_firstnode(grammar);
 *
 *     ni node;   -- First node instance in the grammar instance.
 *     gi grammar;  -- A grammar instance object.
 *
 * Returns:  The first node instance in  the grammar instance,  or
 *     NULL if  there  are  no  node instances in the grammar
 *     instance.
 *
 * Requires: A call to gi_init must precede any calls  to  gi_firstnode
 *     for a particular grammar instance.
 *
 * Modifies: The grammar instance.
 *
 * Effect:  Returns the first node instance in the grammar instance
 *     object  and  resets the grammar instance node iterator
 *     such that the next invocation of gi_nnode  will  return
 *     the second node instance in the grammar.
 *
 * Implementation:
 *
 *     If the grammar instance has an node,
 *       Set current node pointer to
 *        second node in the grammar.
 *       Return the first node instance.
 *     Else
 *       Return NULL.
 *
 * Uses:
 *
 ***************************************************************************/

#ifdef NOMACROS
 ni
gi_firstnode(gi grammar)
 {
 return( grammar->ginodes );
 }

 ni
gi_lastnode(gi grammar)
 {
 return( grammar->gifreeni );
 }
#endif

/***************************************************************************
 *
 * Name:   gi_getnode - Get node in the grammar
 *
 * Calling Sequence:
 *
 *     node = gi_getnode(grammar, nodeid);
 *
 *     ni  node  -- The specified node instance object.
 *     gi  grammar -- The given grammar instance object.
 *     V_NId nodeid -- The id number of the desired node.
 *
 * Returns:  The node instance object in the given grammar  instance
 *     object  with  the  specified identification number, or
 *     NULL if there are no nodes  in  the  grammar  with  the
 *     given identification number.
 *
 * Requires: A call to gi_init must preceed any call to gi_getnode for
 *     a particular grammar instance.
 *
 * Modifies:
 *
 * Effect:  Returns the the node instance in the given grammar with
 *     the  specified  identification  number,  if it  can be
 *     found.
 *
 * Implementation:
 *
 *     For all nodes instances in the grammar instance,
 *       If node identification number == id,
 *        Return node.
 *
 *     If node not found,
 *       Return NULL
 *
 * Uses:   - gi_firstnode -- get first node instance in grammar instance
 *     - gi_lastnode -- get last node instance in grammar instance
 *     - ni_id -- get id of node instance
 *
 **************************************************************************/

 ni
gi_getnode(gi grammar, V_NId nodeid)
 {
 ni node;

 node = gi_lastnode(grammar) - 1 - nodeid;
 if (nodeid == ni_id(node))
  return(node);
 else
  return(NULL);
 }
