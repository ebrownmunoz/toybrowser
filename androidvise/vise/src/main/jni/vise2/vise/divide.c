#include "pthr.h"
#include "vise.h"
#include "v_div.h"

/**************************************************************************
 *
 *	Name: 			X86_DIV.C
 *
 *	Description:	Division cluster
 *
 *	Operations:		rndiv32(n, d);
 *
 **************************************************************************/

	V_Int
rndiv32(V_Long n, V_Int d)
	{
	V_Long quotient;

	quotient = (n << 1) / (V_Long)d;
	if (quotient > 0) quotient++;
	return((V_Int)quotient >> 1);
	}

        V_Int
ldiv32(V_Long n, V_Long d)
	{
	V_Long quotient;

	quotient = (n << 1) / d;
	if (quotient > 0) quotient++;
	return((V_Int)quotient >> 1);
	}
