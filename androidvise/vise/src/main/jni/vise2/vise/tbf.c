#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_tbf.h"

/**************************************************************************
 *
 * Name:        tbf -- The Traceback Frame Cluster
 *
 * Description: The traceback frame cluster implements the abstract
 *              traceback frame objects which define the layers of the
 *              traceback tree.  A traceback frame contains a traceback
 *              node for each grammar node which is reached by a finite
 *              cost path absorbing all the frames of speech data up to
 *              and including the data for that frame.
 *
 *              Each traceback frame object contains a creation time, a
 *              sum of the global minimum frame costs for all frames up
 *              to and including this frame, and a list of the traceback
 *              nodes reached in this frame.  In addition, each traceback
 *              frame is linked to the preceding and following traceback
 *              frames in the traceback tree, whose creation times are
 *              respectively earlier and later than its own.
 *
 * Operations:  tbf_init(vise); -- Initialize the traceback frame cluster.
 *
 *              tbf_alloc(vise); -- Allocate a traceback frame.
 *
 *              tbf_free(vise, frame); -- Deallocate a traceback frame.
 *
 *              tbf_used(vise); -- Return the number of traceback frames currently in use.
 *
 *              tbf_setinfo(frame, firstnode, nodecount, time, sn, sumbestscores, globalmin);
 *                Initialize a traceback frame with its first traceback node, its time
 *                of creation and the accumulated global minimum cost.
 *
 *              tbf_info(frame, &firstnode, &nodecount, &time, &sn, &sumbestscores, &globalmin);
 *                Get traceback frame characterization information
 *
 *              tbf_setsuccessor(frame, successor); -- Make successor the successor to frame.
 *
 *              tbf_successor(frame); -- Get a frame's successor.
 *
 *              tbf_setfirstnode(frame); -- Set the traceback node
 *                element terminating the best node to a traceback node.
 *
 *              tbf_firstnode(frame); -- Get the traceback node element
 *                terminating the best node to a traceback node.
 *
 *              tbf_setpredecessor(frame, predecessor) -- Make predecessor
 *                the traceback frame immediately preceding frame.
 *
 *              tbf_predecessor(frame); -- Get the traceback frame
 *                immediately preceding the given traceback frame.
 *
 *              tbf_setnodecount(frame, nodecount); -- Set the number
 *                of nodes reached in a traceback frame.
 *
 *              tbf_nodecount(frame); -- Get the number of nodes reached in a traceback frame.
 *
 *              tbf_incnodecount(frame); -- Increment the node count of a traceback frame.
 *
 *              tbf_decnodecount(frame); -- Decrement the node count of a traceback frame.
 *
 *              tbf_settime(frame, time); -- Set the creation time of a traceback frame.
 *
 *              tbf_setsn(frame, sn); -- Set the sequence number of the traceback frame
 *
 *              tbf_time(frame); -- Get the creation time of a traceback frame.
 *
 *              tbf_sn(frame); -- Get the sequence number of a traceback frame.
 *
 *              tbf_setglobalmin(frame, globalmin); -- Set the globalmin field in the traceback frame.
 *
 *              tbf_globalmin(frame); -- Get the globalmin from the traceback frame.
 *
 *                VISE        vise; -- a VISE descriptor
 *                tbf        frame; -- a given traceback frame
 *                tbf  predecessor; -- a traceback frame
 *                tbf   succcessor; -- a traceback frame
 *                tbp    firstnode; -- a traceback node
 *                V_Int  nodecount; -- count of nodes in a frame
 *                V_Time      time; -- creation time of a traceback frame
 *                V_SN          sn; -- sequence number of a traceback frame
 *                V_Long globalmin; -- sum of globalmin so far
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\TBF.C_V  $
 * 
 *    Rev 1.5   20 Mar 1992 13:00:36   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.4   29 Jan 1992 12:58:12   DCV
 * Changed basic types
 * 
 *    Rev 1.3   20 Sep 1991 10:01:44   DCV
 * Added a sumbestscores field to tbfs, so that the cost field in tbps
 *  can now remain renormalized until traceback.
 * 
 *    Rev 1.2   14 Aug 1991 16:38:06   DCV
 * Made TRACEBACKFRAMEALLOC a VISE parameter.
 * Now calls MEM_alloclist() to allocate tbf objects.
 * 
 *    Rev 1.1   18 Jul 1991 16:23:26   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.0   12 Jun 1991 14:39:34   DCV
 * Initial Version
 * 
 ***************************************************************************/

/***************************************************************************
 *
 * Name:    tbf_init -- Initialize Traceback Frame Cluster
 *
 * Calling Sequence:
 *
 *      tbf_init(vise);
 *
 *                      VISE vise; -- a VISE descriptor
 *
 * Returns:
 *
 * Requires:  The memory for traceback frames must have been deallocated
 *
 * Modifies:  The traceback frame cluster
 *
 * Effect:   Initializes the traceback frame cluster.
 *
 * Implementation:
 *
 *      Set the vise->tbfPool.freeentries pointer to point to NULL.
 *      Set vise->tbfPool.used to zero.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

  V_Void
tbf_init(VISE vise)
{
  vise->tbfPool.freeentries = NULL;
  vise->tbfPool.used = 0;
}

/* end tbf_init */

/**************************************************************************
 *
 * Name:    tbf_alloc -- Traceback Frame Allocation Function
 *
 * Calling Sequence:
 *
 *      frame = tbf_alloc(vise);
 *
 *      tbf frame; -- newly allocated traceback frame
 *                      VISE vise; -- a VISE descriptor
 *
 * Returns:   A newly allocated and uninitialized traceback frame,
 *      or NULL if all available traceback frames are already
 *      allocated.
 *
 * Requires:  tbf_init must be called before invoking tbf_alloc.
 *
 * Modifies:  The traceback frame cluster
 *
 * Effect:   Returns a new traceback frame object, or NULL if none
 *      are available.  Traceback frames can be returned to free
 *      storage with the tbf_free function.
 *
 * Implementation:
 *
 *      If no free traceback frame exists,
 *       Make some if possible or return NULL
 *       Link them into a NULL-terminated free list.
 *
 *       Get an traceback frame off of the free list.
 *       Increment the used frame count.
 *       Return the traceback frame.
 *
 * Dependency:
 *
 * Uses:    param_get()
 *      MEM_alloclist()
 *
 **************************************************************************/

  tbf
tbf_alloc(VISE vise)
{
  tbf nextFreeTBF;

   /* If no free traceback frames exist, make some */
  if (vise->tbfPool.freeentries == NULL &&
      (vise->tbfPool.freeentries =
           (tbf) MEM_alloclist(vise->memCfg, (V_ULong) sizeof(tbf_t), (V_ULong) param_get(vise, TRACEBACKFRAMEALLOC), FAST_MEM, "tbf_alloc tbf")) == NULL)
    return(NULL);

   /* At least one free traceback frame now exists; get it */
  nextFreeTBF = vise->tbfPool.freeentries;
  vise->tbfPool.freeentries = vise->tbfPool.freeentries->tbfsuccessor;

  vise->tbfPool.used++;
  return(nextFreeTBF);
}

/* end tbf_alloc */

/***************************************************************************
 *
 * Name:    tbf_free -- Deallocate a traceback frame
 *
 * Calling Sequence:
 *
 *      tbf_free(vise, frame)
 *
 *                      VISE vise; -- a VISE descriptor
 *      tbf frame; -- The frame which is to be deallocated
 *
 * Returns:
 *
 *
 * Requires:  tbf_init must be called before invoking tbf_free.
 *      The frame must not be NULL.
 *
 * Modifies:  The traceback frame cluster
 *
 * Effect:   Returns the traceback frame to free storage
 *
 * Implementation:
 *
 *      Point the frame's successor pointer at the free list.
 *      Point the free list at the given frame.
 *      Decrement the used frame count.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_free(VISE vise, tbf frame)
{
  frame->tbfsuccessor = vise->tbfPool.freeentries;
  vise->tbfPool.freeentries = frame;
  vise->tbfPool.used--;
}

/* end of tbf_free */

#endif

/***************************************************************************
 *
 * Name:    tbf_used -- Return the number of tbfs in use
 *
 * Calling Sequence:
 *
 *      numused = tbf_used(vise);
 *
 *      V_Int numused;
 *                      VISE       vise; -- a VISE descriptor
 *
 * Returns:   The number of tbfs currently in use
 *
 * Requires:  tbf_init() must have been called previously
 *
 * Modifies:  Nothing
 *
 * Effect:
 *
 * Implementation:
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Int
tbf_used(VISE vise)
{
  return(vise->tbfPool.used);
}

/* end tbf_used */

#endif

/**************************************************************************
 *
 * Name:    tbf_setinfo -- Initialize a traceback frame
 *
 * Calling Sequence:
 *
 *      tbf_setinfo(frame, firstnode, nodecount, time, sn, sumbestscores, globalmin);
 *
 *      tbf            frame; -- A traceback frame
 *      tbp        firstnode; -- The first node in this frame
 *      V_Int      nodecount; -- The number of nodes in this frame
 *      V_Time          time; -- The creation time of this frame
 *      V_SN              sn; -- The sequence number of this frame
 *      V_Long sumbestscores;
 *      V_Long     globalmin; -- The sum of globalmin for this frame
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies:  frame
 *
 * Effect:   Places the given firstnode, nodecount, time and globalmin
 *      information into the traceback frame.
 *
 * Implementation:
 *
 *      firstnode  ==> firstnode field of frame
 *      nodecount ==> nodecount filed of frame
 *      time  ==> time field of frame
 *                      sn          ==> sequence number field of frame
 *      globalmin ==> globalmin field of frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setinfo(tbf frame, tbn firstnode, V_Int nodecount, V_Time time, V_SN sn, V_Long sumbestscores, V_Long globalmin)
{
  frame->tbffirstnode = firstnode;
  frame->tbfnodecount = nodecount;
  frame->tbftime = time;
  frame->tbfsn = sn;
  frame->tbfsumbestscores = sumbestscores;
  frame->tbfglobalmin = globalmin;
}

/* end tbf_setinfo */

#endif

/**************************************************************************
 *
 * Name:    tbf_info -- Get traceback frame information
 *
 * Calling Sequence:
 *
 *      tbf_info(frame, firstnode, nodecount, time, sn, globalmin);
 *
 *      tbf             frame;  - A traceback frame
 *      tbp        *firstnode; - The first node in this frame
 *      V_Int      *nodecount; - The number of nodes in this frame
 *      V_Time          *time; - The index of this frame
 *      V_SN              *sn; - The sequence number of this frame
 *      V_Long *sumbestscores;
 *      V_Long     *globalmin; - The sum of globalmin for this frame
 *
 * Returns:
 *
 * Requires:  tbf_setinfo must have been called for the traceback
 *            frame prior to invoking tbf_info on that frame.
 *
 * Modifies:  firstnode, time, globalmin
 *
 * Effect:    Returns the firstnode, frame and globalmin for a
 *            given traceback frame.
 *
 * Implementation:
 *
 *      frame's firstnode   ==> firstnode
 *      frame's nodecount   ==> nodecount
 *      frame's index    ==> time
 *      frame's sequence number     ==> sn
 *      frame's globalmin   ==> globalmin
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_info(tbf frame, tbn * firstnode, V_Int * nodecount, V_Time * time, V_SN * sn, V_Long * sumbestscores, V_Long * globalmin)
{
  *firstnode = frame->tbffirstnode;
  *nodecount = frame->tbfnodecount;
  *time = frame->tbftime;
  *sn = frame->tbfsn;
  *sumbestscores = frame->tbfsumbestscores;
  *globalmin = frame->tbfglobalmin;
}

/* end tbf_info */

#endif

/***************************************************************************
 *
 * Name:    tbf_setsuccessor -- Set successor of a traceback frame
 *
 * Calling Sequence:
 *
 *      tbf_setsuccessor(frame, successor);
 *
 *      tbf frame, successor; -- traceback frames to be linked
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies:  frame
 *
 * Effect:   Makes successor the successor of frame
 *
 * Implementation:
 *
 *      copy successor to frame's successor field
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setsuccessor(tbf frame, tbf successor)
{
  frame->tbfsuccessor = successor;
}

/* end of tbf_setsuccessor */

#endif

/**************************************************************************
 *
 * Name:    tbf_successor -- Get the successor of a traceback frame
 *
 * Calling Sequence:
 *
 *      successor = tbf_successor(frame);
 *
 *      tbf successor, frame;
 *
 * Returns:   The successor to the given frame
 *
 * Requires:  A successor should have already been established with
 *      the tbf_setsuccessor function.
 *
 * Modifies:
 *
 * Effect:   Returns the successor of the given frame.
 *
 * Implementation:
 *
 *      Return successor field of frame
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

  tbf
tbf_successor(tbf frame)
{
  return(frame->tbfsuccessor);
}

/* end of tbf_successor */

#endif

/***************************************************************************
 *
 * Name:    tbf_setfirstnode -- Set the first node in a frame
 *
 * Calling Sequence:
 *
 *      tbf_setfirstnode(frame, firstnode);
 *
 *      tbf frame;   - A traceback frame
 *      tbn firstnode; - A traceback node
 *
 * Returns:   Nothing
 *
 * Requires:  tbf_init() must have been called previously
 *
 * Modifies:  The traceback frame
 *
 * Effect:   Sets the firstnode field in the traceback frame.
 *
 * Implementation:
 *
 *      Set tbffirstnode to firstnode in the traceback frame.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setfirstnode(tbf frame, tbn firstnode)
{
  frame->tbffirstnode = firstnode;
}

/* end tbf_setfirstnode */

#endif

/**************************************************************************
 *
 * Name:    tbf_firstnode -- Get the first node in a traceback frame
 *
 * Calling Sequence:
 *
 *      firstnode = tbf_firstnode(frame);
 *
 *      tbn firstnode; -- The first node in the traceback frame
 *      tbf frame;  -- The traceback frame
 *
 * Returns:   The last traceback node element on the best node to the
 *      given traceback frame.
 *
 * Requires:  A firstnode must have been asserted for the given frame
 *      with tbf_setinfo or tbf_setfirstnode before invoking
 *      tbf_firstnode.
 *
 * Modifies:
 *
 * Effect:   Returns the most recently asserted firstnode for the
 *      given traceback frame.
 *
 * Implementation:
 *
 *      Return firstnode field of the given traceback frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  tbn
tbf_firstnode(tbf frame)
{
  return(frame->tbffirstnode);
}

/* end tbf_firstnode */

#endif

/***************************************************************************
 *
 * Name:    tbf_setpredecessor -- Set the predecessor of a frame
 *
 * Calling Sequence:
 *
 *      tbf_setpredecessor(frame, predecessor);
 *
 *      tbf frame;   -- The given traceback frame
 *      tbf predecessor; -- The predecessor of the given frame
 *
 * Returns:   Nothing
 *
 * Requires:  tbf_init() must have been called previously
 *
 * Modifies:  The traceback frame
 *
 * Effect:   Sets the predecessor pointer in the traceback frame.
 *
 * Implementation:
 *
 *      Set tbfpredecessor to predecessor in the traceback frame.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setpredecessor(tbf frame, tbf predecessor)
{
  frame->tbfpredecessor = predecessor;
}

/* end tbf_setpredecessor */

#endif

/**************************************************************************
 *
 * Name:    tbf_predecessor -- Get the predecessor of a frame
 *
 * Calling Sequence:
 *
 *      predecessor = tbf_predecessor(frame);
 *
 *      tbf predecessor; -- The predecessor of the given frame
 *      tbf frame;   -- The given traceback frame
 *
 * Returns:   The predecessor of a given traceback frame
 *
 * Requires:  A predecessor must have been asserted for the given
 *      frame with tbf_setinfo or tbf_setpredecessor before
 *      invoking tbf_predecessor.
 *
 * Modifies:
 *
 * Effect:   Returns the most recently asserted predecessor for the
 *      given traceback frame.
 *
 * Implementation:
 *
 *      Return predecessor field of the given traceback frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  tbf
tbf_predecessor(tbf frame)
{
  return(frame->tbfpredecessor);
}

/* end tbf_predecessor */

#endif

/***************************************************************************
 *
 * Name:    tbf_setnodecount -- Set the node count of a frame
 *
 * Calling Sequence:
 *
 *      tbf_setnodecount(frame, nodecount);
 *
 *      tbf frame;  -- A traceback frame
 *      V_Int nodecount; -- The number of nodes in this frame
 *
 * Returns:   Nothing
 *
 * Requires:  tbf_init() must have been called previously
 *
 * Modifies:  The traceback frame
 *
 * Effect:   Sets the nodecount in the traceback frame.
 *
 * Implementation:
 *
 *      Set tbfnodecount to nodecount in the traceback frame.
 *
 * Dependency:  
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setnodecount(tbf frame, V_Int nodecount)
{
  frame->tbfnodecount = nodecount;
}

/* end tbf_setnodecount */

#endif

/**************************************************************************
 *
 * Name:    tbf_nodecount -- Node count of traceback frame
 *
 * Calling Sequence:
 *
 *      numnodes = tbf_nodecount(frame);
 *
 *      V_Int numnodes; -- The number of nodes in the frame
 *      tbf frame;  -- A traceback frame
 *
 * Returns:   The number of nodes in the given traceback frame.
 *
 * Requires:  tbf_setinfo must be invoked on a traceback
 *      frame before invoking tbf_nodecount on it.
 *
 * Modifies:
 *
 * Effect:   Returns the number of nodes in the given traceback frame.
 *
 * Implementation:
 *
 *      Return nodecount field of traceback frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Int
tbf_nodecount (tbf frame)
{
  return(frame->tbfnodecount);
}

/* end tbf_nodecount */

#endif

/**************************************************************************
 *
 * Name:    tbf_incnodecount -- Increment node count of a frame
 *
 * Calling Sequence:
 *
 *      num = tbf_incnodecount(frame);
 *
 *      V_Int num; -- New nodecount
 *      tbf frame; -- A traceback frame
 *
 * Returns:   The new nodecount
 *
 *
 * Requires:  The nodecount of the frame must have been set using
 *      tbf_setinfo or tbf_setnodecount before invoking
 *      tbf_incnodecount on that frame.
 *
 * Modifies:  frame
 *
 * Effect:   Increments the node count of a traceback frame.
 *
 * Implementation:
 *
 *      nodecount = nodecount + 1
 *      return nodecount
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

  V_Int
tbf_incnodecount(tbf frame)
{
  return(++frame->tbfnodecount);
}

/* end tbf_incnodecount */

#endif

/**************************************************************************
 *
 * Name:    tbf_decnodecount -- Decrement the node count of a frame
 *
 * Calling Sequence:
 *
 *      num = tbf_decnodecount(frame);
 *
 *      V_Int num; -- New nodecount
 *      tbf frame; -- A traceback frame
 *
 * Returns:   The decremented nodecount
 *
 *
 * Requires:  The nodecount of the frame must have been set using
 *      tbf_setinfo or tbf_setnodecount before invoking
 *      tbf_decnodecount on that frame.
 *
 * Modifies:  frame
 *
 * Effect:   Unconditionally decrements the node count of a traceback
 *      frame.
 *
 * Implementation:
 *
 *      nodecount = nodecount - 1
 *      return nodecount
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Int
tbf_decnodecount(tbf frame)
{
  return(--frame->tbfnodecount);
}

/* end tbf_decnodecount */

#endif

/**************************************************************************
 *
 * Name:    tbf_settime -- Set the index of a traceback frame
 *
 * Calling Sequence:
 *
 *      tbf_settime(frame, time);
 *
 *      tbf  frame; -- A traceback frame
 *      V_Time  time; -- The index of the traceback frame
 *
 * Returns:   Nothing
 *
 * Requires:  The frame cannot be NULL
 *
 * Modifies:  The traceback frame
 *
 * Effect:   Sets the index of the traceback frame.
 *
 * Implementation:
 *
 *      time ==> the time field of the frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_settime(tbf frame, V_Time time)
{
  frame->tbftime = time;
}

/* end tbf_settime */

#endif

/**************************************************************************
 *
 * Name:    tbf_setsn -- Set the sequence number of a traceback frame
 *
 * Calling Sequence:
 *
 *      tbf_setsn(frame, sn);
 *
 *      tbf  frame; -- A traceback frame
 *      V_SN    sn; -- The sequence number of the traceback frame
 *
 * Returns:   Nothing
 *
 * Requires:  The frame cannot be NULL
 *
 * Modifies:  The traceback frame
 *
 * Effect:   Sets the sequence number of the traceback frame.
 *
 * Implementation:
 *
 *      sn ==> the sn field of the frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setsn(tbf frame, V_SN sn)
{
  frame->tbfsn = sn;
}

/* end tbf_setsn */

#endif

/**************************************************************************
 *
 * Name:    tbf_time -- Get the index of a traceback frame
 *
 * Calling Sequence:
 *
 *      time = tbf_time(frame);
 *
 *      V_Time  time; -- The index of the traceback frame
 *      tbf  frame; -- A traceback frame
 *
 * Returns:   The index of the traceback frame
 *
 * Requires:  The frame cannot be NULL
 *
 * Modifies:  Nothing
 *
 * Effect:   Returns the index of the traceback frame.
 *
 * Implementation:
 *
 *      Return the time field of the traceback frame.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Time
tbf_time(tbf frame)
{
  return(frame->tbftime);
}

/* end tbf_time */

#endif

/**************************************************************************
 *
 * Name:      tbf_sn -- Get the sequence number of a traceback frame
 *
 * Call:      sn = tbf_sn(frame);
 *
 *            V_SN    sn; -- The sequence number of the traceback frame
 *            tbf  frame; -- A traceback frame
 *
 * Returns:   The sequence number of the traceback frame
 *
 * Requires:  The frame cannot be NULL
 *
 * Modifies:  Nothing
 *
 * Effect:    Returns the sequence number of the traceback frame.
 *
 * Implementation:
 *
 *            Return the sn field of the traceback frame.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_SN
tbf_sn(tbf frame)
{
  return(frame->tbfsn);
}

/* end tbf_sn */

#endif

/**************************************************************************
 *
 * Name:      tbf_setglobalmin -- Set the globalmin of a traceback frame
 *
 * Call:      tbf_setglobalmin(frame, globalmin);
 *
 *            tbf         frame; -- A traceback frame
 *            V_Long  globalmin; -- The globalmin of the traceback frame
 *
 * Returns:   Nothing
 *
 * Requires:  The frame cannot be NULL
 *
 * Modifies:  The traceback frame
 *
 * Effect:    Sets the globalmin of the traceback frame.
 *
 * Implementation:
 *
 *            globalmin ==> the globalmin field of the frame
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Void
tbf_setglobalmin(tbf frame, V_Long globalmin)
{
  frame->tbfglobalmin = globalmin;
}

/* end tbf_setglobalmin */

#endif

/**************************************************************************
 *
 * Name:      tbf_globalmin -- Get the globalmin of a traceback frame
 *
 * Call:      globalmin = tbf_globalmin(frame);
 *
 *            V_Long  globalmin; -- The globalmin of the traceback frame
 *            tbf         frame; -- A traceback frame
 *
 * Returns:   The globalmin of the traceback frame
 *
 * Requires:  The frame cannot be NULL
 *
 * Modifies:  Nothing
 *
 * Effect:    Returns the globalmin of the traceback frame.
 *
 * Implementation:
 *
 *            Return the globalmin field of the traceback frame.
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

  V_Long
tbf_globalmin(tbf frame)
{
  return(frame->tbfglobalmin);
}

/* end tbf_globalmin */

#endif

/* END TBN CLUSTER */
