#include "vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_mem.h"
#include "visemsg.h"
#include "v_par.h"
#include "v_vise.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/* Private prototype */
static V_Err  VISE_Acknowledge(VISE vise, V_Int command, V_Err status);

/**************************************************************************
 *
 * Name:     VISE_dispatch - VISE Command Dispatcher
 *
 * Returns:  VISE exception that caused exit
 *
 * Requires: The message system must be properly initialized
 *
 * Modifies: Anything and everything
 *
 * Effect:   This function is the toplevel dispatcher for VISE.
 *           It opens messages, invokes the appropriate function
 *           to deal with the messages and re-initializes the
 *           the scratchpad memory resource when the function
 *           returns.  It is the responsibility of the called
 *           function to read the message, take the appropriate
 *           action and send a reply.  If the called function is
 *           is able to send a reply, it returns TRUE even when
 *           it is unsuccessful; otherwise, it returns FALSE.
 *
 * Implementation:
 *
 *           Loop forever, 
 *             Waiting for an incoming message,
 *             Dispatching to the appropriate function
 *             Re-initializing fast memory
 *           Until receiving _DESTROY or failing to acknowledge
 *
 * Computer: Any
 *
 * Uses:
 *           MSG_openRead - receive a message (visemsg.c)
 *           MSG_closeRead - discard a received message (visemsg.c)
 *           MSG_openWrite - open a message for writing (visemsg.c)
 *           MSG_writeInt - write a V_Int into a message (visemsg.c)
 *           MSG_closeWrite - send a message (visemsg.c)
 *           MSG_openWrite - send a message (visemsg.c)
 *           DLOADARC - Down load and add an arc to a grammar (dloadarc.c)
 *           DLOADGRAMMAR - Down load a grammar (dloadg.c)
 *           DLOADTEMPLATE - Down load templates for a word (dloadt.c)
 *           DLOADWORD - Down load a word model (dloadw.c)
 *           DLOADWILDCARD - Down load a wildcard (dloadwc.c)
 *           ULOADTEMPLATE - Upload templates for a word
 *           ULOADWORD - Upload a word model
 *           DELARC - Delete an arc from a grammar (delarc.c)
 *           DELGRAMMAR - Delete grammar (delgra.c)
 *           DELWORD - Delete word (delwor.c)
 *           WARMSTART - Reinitialize VISE (warmstrt.c)
 *           GETPARAMETERS - Get VISE parameters (param.c)
 *           SETPARAMETERS - Set VISE parameters (param.c)
 *           RECOGNIZE - Recognize an utterance (recog.c)
 *           ENROLLWORD - Create a model for a word (enroll.c)
 *           TRAINWORD - Train a word incrementally and accumulate data (trainwrd.c)
 *           STARTTRAININGWORD - Start accumulating training data (trainwrd.c)
 *           FINISHTRAININGWORD - Train using accumulated data (trainwrd.c)
 *           SEGMENTWORD - Segment a word (segword.c)
 *           LISTEN - Collect acoustic feature data for an interval (listen.c)
 *           REPLACEWORD - Replace one word with another (replacew.c)
 *           SOURCEINIT - Initialize the Jin source (srcinit.c)
 *
 ***************************************************************************/

  V_Err
VISE_dispatch(VISE vise)
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    status;

    /* Open the next VISE command */
  while (MSG_openRead(vise->incoming, &senderId, &length, &command, TRUE, &status)) {

    VBX_DEBUG(VBX_print("  VISE_dispatch: received %s command from sender %d\n", VISECMD_string((V_Cmd) command), senderId));

    if (command == _DESTROY) {

        /* Do whatever cleanup is required */

        /* Acknowledge _DESTROY and exit immediately */
      status = VISE_Acknowledge(vise, _DESTROY, VISESUCCESS);

        /* The VISE descriptor may now be invalid */

      break;
    }

      /* Dispatch to the function indicated by command identifier */
    switch (command) {
      case _WARMSTART:
        status = WARMSTART(vise);
        break;
      case _RECOGNIZE:
        status = RECOGNIZE(vise);
        break;
      case _ENROLLWORD:
        status = ENROLLWORD(vise);
        break;
      case _TRAINWORD:
        status = TRAINWORD(vise);
        break;
      case _STARTTRAININGWORD:
        status = STARTTRAININGWORD(vise);
        break;
      case _FINISHTRAININGWORD:
        status = FINISHTRAININGWORD(vise);
        break;
      case _DLOADARC:
        /* status = DLOADARC(vise); */
        status = NOTSUPPORTED;
        break;
      case _DELARC:
        /* status = DELARC(vise); */
        status = NOTSUPPORTED;
        break;
      case _DLOADGRAMMAR:
        status = DLOADGRAMMAR(vise);
        break;
      case _DELGRAMMAR:
        status = DELGRAMMAR(vise);
        break;
      case _DLOADJIN:
        status = DLOADJIN(vise);
        break;
      case _ULOADJIN:
        status = ULOADJIN(vise);
        break;
      case _DLOADWORD:
        status = DLOADWORD(vise);
        break;
      case _ULOADWORD:
        status = ULOADWORD(vise);
        break;
      case _DELWORD:
        status = DELWORD(vise);
        break;
      case _DLOADTEMPLATE:
        status = DLOADTEMPLATE(vise);
        break;
      case _ULOADTEMPLATE:
        status = ULOADTEMPLATE(vise);
        break;
      case _DLOADWILDCARD:
        status = DLOADWILDCARD(vise);
        break;
      case _SETPARAMETERS:
        status = SETPARAMETERS(vise);
        break;
      case _GETPARAMETERS:
        status = GETPARAMETERS(vise);
        break;
      case _REPLACEWORD:
        status = REPLACEWORD(vise);
        break;
      case _SEGMENTWORD:
        status = SEGMENTWORD(vise);
        break;
      case _LISTEN:
        status = LISTEN(vise);
        break;
      case _VISEINIT:
        status = VISE_Acknowledge(vise, command, VISESUCCESS);
        break;
      case _SOURCEINIT:
        status = SOURCEINIT(vise);
        break;
      case _SETTIMEOUT:
        status = VISE_Acknowledge(vise, command, UNTIMELYCOMMAND);
        break;
      case _ABORT:
        status = VISE_Acknowledge(vise, command, UNTIMELYCOMMAND);
        break;
      case _ACOUSTICFEATURES:
        /* No acknowledgment expected */
        if (JINSRC_writeFrame(vise->jinSource, vise->incoming)) {
          status = VISESUCCESS;
        } else {
          VBX_DEBUG(VBX_print("VISE_dispatch: ignoring JINSRC_writeFrame failure (overflow)\n"));
          status = VISESUCCESS;
        }
        break;
      case _EXCEPTION:
        /* No acknowledgment expected */
        MSG_readInt(vise->incoming, &command);
        MSG_closeRead(vise->incoming, &status);
        if ((command == ENDOFDATA || command == SNWRAPAROUND) && !JINSRC_writeEXC(vise->jinSource, (V_Err) command))
          status = OUTOFMEMORY;
        else
          status = VISESUCCESS;
        break;
      default:
        status = VISE_Acknowledge(vise, command, ILLEGALCOMMAND);
    }

      /* Deallocate the fast memory */
    MEM_free(vise->memCfg, FAST_MEM);

      /* Give up if the command function could not send a reply */
    if (status) {
      VBX_DEBUG(VBX_print("VISE_dispatch: reply status = %s\n", VISEERR_string(status)));
      break;
    }
  }

  return(status);
}


/**************************************************************************
 *
 * Name:     VISE_busyDispatch - process commands while busy
 *
 * Returns:  A VISE exception
 *
 * Requires: The message system must be properly initialized
 *
 * Effect:   This function takes the place of the toplevel dispatcher when
 *           VISE is busy consuming acoustic data.  When it encounters an
 *           _ACOUSTICFEATURES or _EXCEPTION command, it reports receiving
 *           the command and returns VISESUCCESS, but does not close the
 *           message or acknowledge it.  IN THESE TWO CASES THE CALLER MUST
 *           CLOSE THE INCOMING MESSAGE after reading its contents.  When
 *           it encounters an _ABORT command, it acknowledges it and returns
 *           VISESUCCESS.  It accepts _SETPARAMETERS, _GETPARAMETERS and
 *           _SETTIMEOUT, and executes them.  Otherwise, it ignores command
 *           messages, just sending an acknowledgment of having received
 *           them back to their sender with an UNTIMELYCOMMAND or
 *           ILLEGALCOMMAND status and looping back for the next command.
 *           If it fails trying to send an acknowledgment, it returns the
 *           status of the failure.
 *
 *           If the loopAndBlock argument is TRUE, the function blocks until
 *           it gets a message on vise->incoming, and loops back for the next
 *           message until it receives an _ACOUSTICFEATURES, _EXCEPTION or
 *           _ABORT command.  Otherwise, it neither blocks nor loops, but
 *           returns immediately, reporting whatever command it has received,
 *           or NOMESSAGES if it has received none.
 *
 ***************************************************************************/

  V_Err
VISE_busyDispatch(VISE vise, V_Int * command, V_Bool loopAndBlock)
{
  V_Uns    senderId;
  V_ULong  length;
  V_Bool   success;
  V_Err    status = VISESUCCESS;

    /* Open the next VISE command */
  while ((success = MSG_openRead(vise->incoming, &senderId, &length, command, loopAndBlock, &status))) {

    VBX_DEBUG(VBX_print("  VISE_busyDispatch(%s): received %s command from sender %d\n",
                        loopAndBlock ? "blocked" : "unblocked", VISECMD_string((V_Cmd) *command), senderId));

    if (*command == _ACOUSTICFEATURES || *command == _EXCEPTION) {
      break;
    } else if (*command == _ABORT) {
      status = VISE_Acknowledge(vise, *command, VISESUCCESS);
      break;
    } else if (*command == _SETTIMEOUT) {
      status = SETTIMEOUT(vise);
    } else if (*command == _SETPARAMETERS) {
      status = SETPARAMETERS(vise);
    } else if (*command == _GETPARAMETERS) {
      status = GETPARAMETERS(vise);
    } else if (0 <= *command && *command < NUMVISECOMMANDS) {
      status = VISE_Acknowledge(vise, *command, UNTIMELYCOMMAND);
    } else {
      status = VISE_Acknowledge(vise, *command, ILLEGALCOMMAND);
    }
    if (status || !loopAndBlock) break;

  }

  if (status) VBX_DEBUG(VBX_print("  VISE_busyDispatch(%s): reply status = %s\n",
                                  loopAndBlock ? "blocked" : "unblocked", VISEERR_string(status)));

  return(status);
}


/**************************************************************************
 *
 * Name:     VISE_Acknowledge - send an acknowledgement to the sender
 *
 ***************************************************************************/

  static V_Err
VISE_Acknowledge(VISE vise, V_Int command, V_Err status)
{
  V_Uns  source = MSG_correspondentId(vise->incoming);
  V_Err  vstatus;

  MSG_closeRead(vise->incoming, &vstatus);
  if (MSG_openWrite(vise->outgoing, source, (V_ULong) 2, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeInt(vise->outgoing, command);
    MSG_writeInt(vise->outgoing, (V_Int) status);
    MSG_closeWrite(vise->outgoing, &vstatus);
  }

  return(vstatus);
}
