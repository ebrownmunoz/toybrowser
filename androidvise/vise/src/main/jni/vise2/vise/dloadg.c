#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_am.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_gm.h"
#include "v_km.h"
#include "v_mem.h"
#include "visemsg.h"
#include "v_syn.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      DLOADGRAMMAR -- Download a grammar
 *
 * Returns:   Status of reply message
 *
 * Requires:  The WARMSTART function must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                       Type
 *
 *              Grammar ID                     V_Uns
 *              Number of arcs                 V_Uns
 *              Number of nodes                V_Uns
 *              For each arc:
 *                Start node number            V_Uns
 *                End node number              V_Uns
 *                Number of words on arc       V_Uns
 *                For each word on arc:
 *                  Word id                    V_Uns
 *
 *            In addition to this format all arcs are ordered:  Null
 *            arcs come before nonnull arcs. Null arcs are ordered
 *            such that if a node k is both a start node and an end
 *            node, then the arc with end node k preceeds the arc
 *            with start node k.  Loops of null arcs are forbidden.
 *
 *            Nodes must be numbered consecutively.  If a grammar has
 *            n nodes, then the start node must be 0, and the end
 *            node of the grammar must be n-1.
 *
 *            Finally, wildcard words may not be on the same arc as
 *            non-wildcard words.  All arcs containing wildcard words
 *            must come last in the grammar description.
 *
 * Modifies:  The syntax object, the gm object, the am object.
 *
 * Effect:    Downloads the given grammar and assigns it the
 *            specified grammar identification number.  If an error
 *            is detected, the grammar is not accepted and an error
 *            code is returned.
 *
 *            DLOADGRAMMAR sends back the number of missing words
 *            used in the grammar:
 *
 *              Contents                       Type
 *
 *              Number of missing words        V_Uns
 *
 * Implementation:
 *
 * Uses:      syn_creategrammar -- adds grammar to syntax
 *            syn__deletegrammar -- deletes grammar from syntax
 *            syn_getgrammar -- returns grammar from grammar id
 *            syn_activategrammar-- activates a grammar
 *            gm_addarc -- adds arc to grammar
 *            gm_removearc -- removes an arc from a grammar
 *            am_addword -- adds word to arc
 *            voc_gword -- returns word from word id
 *            wm_status -- check status of word
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\DLOADG.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:31:26   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   12 Nov 1991 15:53:20   DCV
 * Now expects standard VISE status codes from syn_activategrammar().
 * Made status codes appropriate fro dynamic memory allocation scheme.
 * 
 *    Rev 3.5   08 Oct 1991 09:43:06   DCV
 * Modified to tolerate uninitialized words and report how many there are.
 * 
 *    Rev 3.4   09 Aug 1991 17:47:54   DCV
 * Now reports which allocation attempt fails
 * 
 *    Rev 3.3   02 Aug 1991 10:02:30   DCV
 * Installed new message system
 * 
 *    Rev 3.2   18 Jul 1991 16:00:38   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.1   07 Nov 1990 16:06:18   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:38:52   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:04:08   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:41:36   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
DLOADGRAMMAR(VISE vise)
{
  km       kernel;         /* Kernel model object                       */
  wm       word;           /* Word model object                         */
  am       arc;            /* Arc model object                          */
  gm       grammar;        /* New grammar model object                  */
  V_Uns    nWordsOnArc;    /* The number of words on an arc             */
  V_Uns    wordID;         /* The id of a word on an arc                */
  V_ULong  nKnodes;        /* Number of kernel traceback nodes required */
  V_ULong  nKernels;       /* Number of kernel models in the grammar    */
  V_ULong  nWordEntries;   /* Number of words on arcs in the grammar    */
  V_Uns    srcNode;        /* The source node of an arc                 */
  V_Uns    dstNode;        /* The destination node of an arc            */
  V_ULong  nWordsMissing;  /* The number of missing words               */
  V_Uns    wordIndex;
  V_Uns    wordCount;
  V_Uns    nArcs;          /* Number of arcs in the grammar             */
  V_Uns    nNodes;         /* Number of nodes in the grammar            */
  V_Uns    grammarId;      /* Grammar identification number             */
  V_Uns    arcCount;
  V_Uns    senderId = MSG_correspondentId(vise->incoming);
  V_Err    vise_status, status;

   /* Initialize counts */
  nKnodes = nKernels = nWordEntries = nWordsMissing = arcCount = 0;

   /* Get the grammar-wide parameters */
  if (!MSG_readUns(vise->incoming, &grammarId) ||
      !MSG_readUns(vise->incoming, &nArcs) ||
      !MSG_readUns(vise->incoming, &nNodes))
    VISE_EXIT(MESSAGETOOSHORT);

   /* Check that the grammar does not already exist */
  if (syn_getgrammar(vise, (V_GId) grammarId))
    VISE_EXIT(GRAMMARINSYNTAX);

   /* Create a grammar model with the given id, if there is room */
  if (!(grammar = syn_creategrammar(vise, (V_GId) grammarId)))
    VISE_EXIT(CANTALLOCATEGM);

   /* Get the parameters of each arc */
  while (nArcs--) {
    if (!MSG_readUns(vise->incoming, &srcNode) ||
        !MSG_readUns(vise->incoming, &dstNode) ||
        !MSG_readUns(vise->incoming, &nWordsOnArc)) {
      syn_deletegrammar(vise, (V_GId) grammarId);
      VISE_EXIT(MESSAGETOOSHORT);
    }

    if ((arc = gm_addarc(vise, grammar, (V_NId) srcNode, (V_NId) dstNode)) != NULL) {
      for (wordIndex = wordCount = 0; wordIndex < nWordsOnArc; wordIndex++) {
         /* Get the word model for the next word on the arc */
        if (!MSG_readUns(vise->incoming, &wordID)) {
          syn_deletegrammar(vise, (V_GId) grammarId);
          VISE_EXIT(MESSAGETOOSHORT);
        }
        if ((word = voc_getword(vise, (V_WId) wordID))) {
          wordCount++;
           /* If the word model has been initialized */
          if (wm_status(word) != UNINITIALIZED) {
             /* Accumulate its kernel count into the grammar's total */
            nKernels += wm_numkernels(word);
             /* Accumulate the knode count for each kernel in the word into the grammar's total */
            for ( kernel = wm_firstkernel(word); kernel != NULL; kernel = km_nextkernel(kernel))
              nKnodes += km_mindwell(kernel);
          } else {
             /* Else increment the count of uninitialized words */
            nWordsMissing++;
          }
           /* Add the word model to the arc, if possible */
          if (!am_addword(vise, arc, word)) {
            syn_deletegrammar(vise, (V_GId) grammarId);
            VISE_EXIT(CANTALLOCATEWDENT);
          }
        } else {
           /* This word is not in the vocabulary */
          nWordsMissing++;
        }
      }
       /* If the arc is not null but all its word models are missing, remove it */
      if (nWordsOnArc && !wordCount) {
        gm_removearc(vise, grammar, arc);
      } else {
        arcCount++;
        nWordEntries += wordCount;
      }
    } else { /* !(arc = gm_addarc(grammar, ...) ) */
      syn_deletegrammar(vise, (V_GId) grammarId);
      VISE_EXIT(CANTALLOCATEAM);
    }
  }

   /* Set the counts of grammar elements */
  gm_setaicount(grammar, (V_ULong) arcCount);
  gm_setnicount(grammar, (V_ULong) nNodes);
  gm_setkndcount(grammar, nKnodes);
  gm_setkicount(grammar, nKernels);
  gm_setwicount(grammar, nWordEntries);

   /* Try to activate the grammar */
  if ((status = syn_activategrammar(vise, (V_GId) grammarId)) != VISESUCCESS)
    syn_deletegrammar(vise, (V_GId) grammarId);

  VISE_EXIT(status);

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (3 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DLOADGRAMMAR);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_writeUns(vise->outgoing, (V_Uns) nWordsMissing);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

