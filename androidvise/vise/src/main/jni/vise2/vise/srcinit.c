#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "v_jin.h"
#include "visecomp.h"
#include "visemsg.h"

/**************************************************************************
 *
 * Name:      SOURCEINIT -- Initialize the Jin Source
 *
 * Returns:   Status of reply message
 *
 * Requires:  param_init() must have been called
 *
 * Modifies:  The Jin source
 *
 * Effect:    Initializes Jin source objects according to the source
 *            parameters.  All data in the poste restante mailbox and the
 *            frame queue are lost.
 *
 * Implementation:
 *
 *            Call JINSRC_init() to initialize the Jin source
 *
 * Uses:      JINSRC_init()
 *
 * 19 Nov 1997 Created by DCV
 *
 *************************************************************************/

  V_Err
SOURCEINIT(VISE vise)
{
  V_Err  status, vstatus;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);

  MSG_closeRead(vise->incoming, &status);

   /* Initialize the Jin source */
  if (JINSRC_init(vise->jinSource))
    vstatus = VISESUCCESS;
  else
    vstatus = OUTOFMEMORY;

  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _SOURCEINIT);
    MSG_writeInt(vise->outgoing, (V_Int) vstatus);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
