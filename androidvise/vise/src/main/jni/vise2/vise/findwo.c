#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_jin.h"
#include "v_alg.h"
#include "v_par.h"
#include "v_div.h"

/******************************************************************************
 * 
 * File:    FINDWO.C - Locate word boundaries based on acoustic data
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\FINDWO.C_V  $
 * 
 *    Rev 3.3   20 Mar 1992 12:34:26   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.2   29 Jan 1992 14:41:08   DCV
 * Changed basic types
 * Replaced call to rndiv16() with call to rndiv32()
 * 
 *    Rev 3.1   12 Nov 1991 15:50:06   DCV
 * Now returns standard VISE status codes.
 * 
 *    Rev 3.0   13 Sep 1990 13:42:22   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:06:30   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:53:40   DCV
 * Initial revision.
 *
 *****************************************************************************/

#define MAX_NUMHIGH 32   /* Size of high and low amplitude buffers */
#define MAX_NUMLOW  32

#define HIGHAMP     255  /* highest jin[0] value */
#define LOWAMP      0    /* lowest jin[0] value  */

/* Private prototypes */
static V_Int  mean(V_Int *, V_Int);
static V_Err  searchJZero(VISE, V_SN, V_SN, V_Int, V_SN *, V_SN *);
static V_Err  findHighLow(VISE, V_SN, V_SN, V_Int *, V_Int, V_Int *, V_Int);
#ifdef NOMACROS
static V_Int  thresh(V_Int, V_Int);
#else
#define       thresh(h,l)  rndiv32((V_Long) (4 * (l)) + (h), 5)
#endif

/**************************************************************************
 *
 * Name:      findword -- find the beginning and the end of utterance
 *
 * Returns:   VISESUCCESS   -- if word boundaries are found
 *            CANTREADAMP   -- if the given times are improper
 *            CANTREADFRAME -- if read from frame queue fails
 *            CANTFINDWORD  -- if word boundary detection fails
 *
 * Requires:
 *
 * Modifies:  *wordStart, *wordEnd, *volume
 *
 * Effect:    Finds the beginning and end of a word based upon the
 *            amplitude data between the specified times. The word
 *            boundaries are returned in the result parameters.
 *            The volume parameter returns the average high amplitude.
 *
 * Implementation:
 *
 *            Find several highest and lowest amplitude values (findHighLow())
 *            Compute averages of the highest and lowest amplitudes (mean())
 *            Compute threshold value from these averages (thresh())
 *            Find first string of MINAMPDUR amplitudes at or above
 *              the threshold, giving start frame. (searchJZero())
 *            Find last string of MINAMPDUR amplitudes at or above
 *              the threshold, giving end frame. (searchJZero())
 *
 *            Return appropriate values in call-by-reference parameters
 *
 * Uses:      (All defined locally)
 *            findHighLow - find several largest and smallest amplitudes
 *            mean - compute average value of a vector.
 *            thresh - compute threshold from average high and low values
 *            searchJZero - search for start and end frames.
 *
 **************************************************************************/

  V_Err
findword(VISE vise, V_SN startSN, V_SN endSN, V_SN * wordStart, V_SN * wordEnd, V_Int * volume)
  /* V_SN    startSN   - beginning of period of interest */
  /* V_SN    endSN     - endof period of interest        */
  /* V_SN  * wordStart - time of word start              */
  /* V_SN  * wordEnd   - time of word end                */
  /* V_Int * volume    - loudness of word                */
{
  V_Int  highBuf[MAX_NUMHIGH]; /* buffer of largest amplitudes */
  V_Int  lowBuf[MAX_NUMLOW];   /* buffer of smallest amplitudes */
  V_Err  status;

  /* Initialize return parameters */
  *wordStart = (V_SN) 0;
  *wordEnd = (V_SN) 0;
  *volume = 0;

  if (!JINSRC_canread(vise->jinSource, startSN, endSN))
    return(CANTREADAMP);

  if ((status = findHighLow(vise, startSN, endSN, highBuf, param_get(vise, NUMHIGH), lowBuf, param_get(vise, NUMLOW))) != VISESUCCESS)
    return(status);

  *volume = mean(highBuf, param_get(vise, NUMHIGH));

  return(searchJZero(vise, startSN, endSN, thresh(*volume, mean(lowBuf, param_get(vise, NUMLOW))), wordStart, wordEnd));
 }

/**************************************************************************
 *
 * Name:      thresh -- Compute Findword Threshold
 *
 * Returns:   threshold value
 *
 * Requires:  high >= low
 *
 * Modifies:
 *
 * Effect:    A word boundary threshold value is computed and returned
 *
 * Implementation:
 *
 *            Return (((4 * lowaverage) + hiaverage) / 5), rounded to
 *            the nearest integer.
 *
 * Uses:      rndiv32 - rounded division function
 *
 **************************************************************************/

#ifdef NOMACROS

  static V_Int
thresh(V_Int high, V_Int low)
{
  return (rndiv32((V_Long)((4 * low) + high), 5));
}

#endif

/******************************************************************************
 * 
 * Name:      mean() - find the mean value of a vector.
 *
 * Returns:   The average value of the vector.
 *     
 * Requires:  Nothing.
 *
 * Modifies:  Nothing.
 *
 * Effect:    The mean value of a vector of integers is calculated.
 *
 * Implementation:
 *
 *            Form the sum of all of the elements in the vector.
 *            Return the sum divided by the length of the vector.
 * 
 * Uses:      rndiv32() - rounded integer division.
 *
 *****************************************************************************/

  static V_Int
mean(V_Int * data, V_Int size)
{
  V_Long  acc;
  V_Int   n;

  acc = 0L;
  for (n = 0; n < size; ++n) 
    acc += (V_Long) *data++;
  return (rndiv32(acc, size));
}

/**************************************************************************
 *
 * Name:      searchJZero() - Search for start and end frames of a word.
 *
 * Returns:   TRUE if start and end of word are successfully found,
 *            FALSE otherwise.
 *
 * Requires:  amp_canread(startime, endSN) must be true.
 *
 * Modifies:  *startword, *wordEnd
 *
 * Effect:    Searches the specified region of the amplitude buffer
 *            for a word.  If both the start and end of a word are
 *            found, values for the parameters wordStart and wordEnd
 *            are saved and TRUE is returned. Otherwise, result
 *            parameters are not modified and FALSE is returned.
 *
 * Implementation:
 *
 *            "wordStart" is the time of first amplitude of the first
 *            string  of MINAMPDUR amplitudes all of which are at or
 *            above the threshold value.  The start value returned is
 *            STARTEXTRA frames preceding this, but not less than 0.
 *
 *            "wordEnd" is the time of the last amplitude of the last
 *            string  of MINAMPDUR amplitudes all of which are at or
 *            above the threshold value.  The end value  returned  is
 *            ENDEXTRA frames after this, but not more than the total
 *            number of frames.
 *
 *            Search for first occurence of MINAMPDUR amplitudes
 *            at or above threshold when found set start value
 *            and initial end value.
 *
 *            if (not found) return false
 *
 *            search through end of amplitudes
 *
 *            When MINAMPDUR amplitudes at or above
 *            threshold found, set new end value.
 *
 *            adjust end value
 *            return true
 *
 * Uses:      amp_rnextamplitude -- return next amplitude
 *
 *************************************************************************/

  static V_Err
searchJZero (VISE vise, V_SN startSN, V_SN endSN, V_Int threshold, V_SN * wordStart, V_SN * wordEnd)
 /* V_SN    startSN   - beginning SN for search  */
 /* V_SN    endSN     - ending SN for search     */
 /* V_Int   threshold - threshold value          */
 /* V_SN  * wordStart - start frame of utterance */
 /* V_SN  * wordEnd   - end frame of utterance   */
{
  V_Int   highcount;  /* Count of successive values of jin[0] at or above threshold */
  V_Int   jzero;      /* Current frame's jin[0] value */
  V_SN    start, end; /* local word boundaries */
  V_SN    sn;         /* Current frame counter */
  jinr    frame;      /* The current jin frame */
  V_Err   status;

  highcount = 0;

  /* Search for the start */
  sn = startSN;
  do {
    if (!JINSRC_read(vise->jinSource, &sn, &frame, &status)) return(status);
    jzero = jin_getcomponent(&frame, 0);

    if (jzero >= threshold) {
      if (!highcount)
        start = sn;
      if (++highcount >= param_get(vise, MINAMPDUR))
        break;
    /* Restart search for word beginning */
    } else {
      highcount = 0;
    }
  } while ((sn < endSN) && (sn = -1));

  if (sn < endSN) {

    /* Adjust the starting sequence number */
    start -= param_get(vise, STARTEXTRA);
    if (start < startSN) start = startSN;

    end = sn;

    /* Search for the end */
    do {
      if (!JINSRC_read(vise->jinSource, &sn, &frame, &status)) return(status);
      jzero = jin_getcomponent(&frame, 0);

      if (threshold <= jzero) {
        /* If at or above threshold, increment highcount */
        ++highcount;
        /* If enough successive jzeros above threshold, we've found a new end */
        if (highcount >= param_get(vise, MINAMPDUR))
          end = sn;
      } else {
        /* If jzero is below the threshold, set highcount to 0 */
        highcount = 0;
      }
    } while ((sn < endSN) && (sn = -1));

    /* Adjust ending sequence number */
    end += param_get(vise, ENDEXTRA);
    if (end > endSN) end = endSN;

    /* Report the start and end frames */
    *wordStart = start;
    *wordEnd = end;

    return (VISESUCCESS);
  } else {
    return (CANTFINDWORD);
  }
}


/**************************************************************************
 *
 * Name:      findHighLow -- Find high and low jzeros
 *
 * Returns:   V_Void
 *
 * Requires:  numHigh and numLow must be positive
 *
 * Modifies:  highBuf and lowBuf
 *
 * Effect:    This function finds the extrema of jin[0] in a region in
 *            the frame buffer between the given start SN and end SN,
 *            inclusive.  highBuf contains the NUMHIGH largest values
 *            in ascending order. lowBuf contains the NUMLOW smallest
 *            values in descending order.
 *
 * Implementation:
 *
 *            "highBuf" is initialized with values which are all the
 *            lowest possible jzero, and "lowBuf" is initialized
 *            with values which are the highest possible jzero.
 *
 *            for each jzero between the start and end times
 *              if jzero larger than smallest in highBuf
 *                put jzero into appropriate place in highBuf
 *              endif
 *              if jzero smaller than largest in lowBuf
 *                put jzero into appropriate place the lowBuf
 *              endif
 *            endfor
 *
 * Uses:      JINSRC_read - read a frame of jin data
 *            jin_getcomponent - get a component of a jin frame
 *
 **************************************************************************/

  static V_Err
findHighLow(VISE vise, V_SN startSN, V_SN endSN, V_Int * highBuf, V_Int numHigh, V_Int * lowBuf, V_Int numLow)
  /* V_SN  startSN;   - start of region of high jzero values */
  /* V_SN  endSN;     - end of region of high jzero values   */
  /* V_Int highBuf[]; - array of largest jzeros              */
  /* V_Int numHigh;   - number of entries in highBuf         */
  /* V_Int lowBuf[];  - array of smallest jzeros             */
  /* V_Int numLow;    - number of entries in lowBuf          */
{
  V_Int   jzero;      /* jin[0] value                   */
  V_Int * bufPtr;     /* pointer into highBuf or lowBuf */
  V_Int   i;
  V_SN    sn;
  jinr    frame;
  V_Err   status;

  for (i = 0; i < numHigh; i++)
    highBuf[i] = LOWAMP;

  for (i = 0; i < numLow; i++)
    lowBuf[i] = HIGHAMP;

  /* Read all the jin[0] between the start and end sequence numbers */
  sn = startSN;
  do {
    if (!JINSRC_read(vise->jinSource, &sn, &frame, &status)) return(status);
    jzero = jin_getcomponent(&frame, 0);

    /* Check if jzero should be added to highBuf */
    if (highBuf[0] < jzero) {
      /* Put jzero into appropriate spot in highBuf, moving preceding entries up one place */
      for (bufPtr = highBuf, i = numHigh - 1; i--; bufPtr++) {
        if (bufPtr[1] < jzero) {
          /* Move preceding entry up */
          bufPtr[0] = bufPtr[1];
          /* If at end of highBuf, put jzero in bottom spot */
          if (i == 0)
            bufPtr[1] = jzero;
        } else {
          /* Put jzero in place in highBuf */
          bufPtr[0] = jzero;
          break;
        }
      }
    }

    /* Check if jzero should be added to lowBuf */
    if (jzero < lowBuf[0]) {
      /* Put jzero into appropriate spot in lowBuf, moving preceding entries up one place */

      for(bufPtr = lowBuf, i = numLow - 1; i--; bufPtr++) {
        if (jzero < bufPtr[1]) {
          /* Move preceding entry up */
          bufPtr[0] = bufPtr[1];

          /* If at end of lowBuf, put jzero in bottom spot */
          if( i == 0 )
            bufPtr[1] = jzero;
        } else {
          /* Put jzero in place in lowBuf */
          bufPtr[0] = jzero;
          break;
        }
      }
    }
  } while ((sn < endSN) && (sn = -1));

  return(VISESUCCESS);
}
