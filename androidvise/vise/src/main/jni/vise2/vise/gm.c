#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_am.h"
#include "v_gm.h"
#include "v_mem.h"
#include "v_nm.h"
#include "v_par.h"

/**************************************************************************
 *
 * Name:     gm -- Grammar Model Cluster
 *
 * Description: 
 *
 *           The grammar model cluster implements the abstract grammar
 *           model objects,  each of which describes the sequence of words
 *           which are legal in the utterances for some particular
 *           recognition.  A grammar model consists of arc models and node
 *           models.   Grammar models are "write-once" in the sense that
 *           they are initialized when they are created, and are not
 *           changed over the lifetime of the object.
 *
 * Operations:
 *
 *   1. gm_init(vise); -- Initialize the grammar model cluster.
 *
 *   2. gm_putinfo(grammar, id); -- Initializes the grammar model.
 *
 *   3. gm_addarc(vise, grammar, sourceid, destinationid) -- Add an
 *      arc to the grammar, with the specified source and destination
 *      node ids.
 *
 *   4. gm_firstarc(grammar); -- Return the first arc in the grammar.
 *
 *   5. gm_nextarc(grammar); -- Return the next arc in the grammar.
 *
 *   6. gm_nicount(grammar) --  Return the  nodecount   of  the
 *      grammar.
 *
 *   7. gm_id(grammar); -- Return the identification number of  the
 *      grammar.
 *
 *   8. gm_deallocate(vise, grammar); -- Remove and deallocate all arcs
 *      from the grammar.
 *
 *
 *         VISE   vise;         -- The VISE descriptor
 *         gm     grammar;      -- A grammar model object
 *         V_GId  id;           -- Grammar identification number
 *         VISE   vise;         -- A VISE descriptor
 *         V_NId  sourceid      -- Id of source node of arc
 *         V_NId  destinationid -- Id of destination node of arc
 *
 *   NOTE:  It's important to note that the arcs stored in the
 *      grammar model are FIFO.   The first arc returned by
 *      gm_firstarc() was the first added by gm_addarc().  The next
 *      arc returned by gm_nextarc() also are in the same order as
 *      added.  This must be consistent with the functions of the
 *      GI cluster:  gi_addarc(); gi_farc(), gi_narc().  The order
 *      of the arc instances in the grammar instance should  be the
 *      same order as the arc models in the grammar model.
 *
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\GM.C_V  $
 * 
 *    Rev 3.5   20 Mar 1992 12:35:30   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.4   29 Jan 1992 14:48:38   DCV
 * Changed basic types; replaced nm_write() with nm_setnid()
 * 
 *    Rev 3.3   14 Aug 1991 16:01:26   DCV
 * Now uses alloclist() to allocate arc model entry objects
 * Added ARCMODELALLOC to VISE parameters block
 * 
 *    Rev 3.2   18 Jul 1991 16:10:56   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.1   07 Nov 1990 16:09:46   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:45:22   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:07:36   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:01:08   DCV
 * Initial revision.
 *
 **************************************************************************/

/* An Arc Entry Descriptor */
typedef struct arcent_s {
  ARCENT  nextarc;   /* A pointer to the next arc */
  am_t    thisarc;   /* The arc model */
} arcent_t;

/***************************************************************************
 *
 * Name:     gm_init - Initialize grammar model cluster.
 *
 * Call:     gm_init(VISE vise)
 *
 * Returns:  V_Void
 *
 * Requires:
 *
 * Modifies: The grammar model cluster.
 *
 * Effect:   Initializes the grammar model cluster.
 *
 * Implementation:
 *
 *           NULL the list of free arc model entries.
 *           Initialize the arc model cluster.
 *
 * Uses:     am_init -- initialize the arc model cluster
 *
 **************************************************************************/

  V_Void
gm_init(VISE vise)
{
   /* Terminate the freelist */
  vise->amPool.freearc = NULL;

   /* Initialize the arc model cluster */
  am_init(vise);
}


/***************************************************************************
 *
 * Name:     gm_putinfo - Grammar model initialization
 *
 * Call:     gm_putinfo(grammar, id)
 *
 *             gm     grammar; -- Grammar to be initialized
 *             V_GId  id;      -- Identification number of grammar
 *
 * Returns:
 *
 *
 * Requires: gm_putinfo should never be invoked with a particular grammar
 *           model more than once without first invoking gm_deallocate.
 *
 * Modifies: The grammar model.
 *
 * Effect:   Initializes the grammar model.
 *
 * Implementation:
 *
 *           Put the id into the grammar model object.
 *           Clear first, last, and current arc pointers.
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
  V_Void
gm_putinfo(gm grammar, V_GId id)
{
  grammar->gmid = id;
  grammar->gmfirstarc = grammar->gmcurrentarc = grammar->gmlastarc = NULL;
}
#endif

/**************************************************************************
 *
 * Name:     gm_addarc - Add an arc to a grammar
 *
 * Call:     arc = gm_addarc(vise, grammar, sourceid, destinationid)
 *
 *             am     arc;           -- returned new arc
 *             VISE   vise;          -- a VISE descriptor
 *             gm     grammar;       -- grammar model
 *             V_NId  sourceid;      -- node id
 *             V_NId  destinationid; -- node id
 *
 * Returns:  The arc model of the added arc or NULL if no more arc
 *           models can be allocated.
 *
 * Requires: Previous initialization of the gm cluster by gm_init()
 *           Initialization of grammar model object with gm_putinfo
 *
 * Modifies: The grammar model
 *
 * Effect:   Adds an arc model to a grammar model object
 *
 * Implementation:
 *
 *           If no free arc model exists,
 *             Make some if possible or return NULL
 *             Link them into a NULL-terminated free list.
 *
 *           Get an arc model off of the free list.
 *           Initialize the arc model.
 *           Append the arc model to the grammar model's arc list.
 *           Return the arc model.
 *
 * Uses:
 *
 *           am_arcinit -- initialize an arc model object
 *           am_sourcenode -- get the source node of an arc
 *           am_destinationnode -- get the destination node of an arc
 *           param_get -- get a VISE parameter
 *           nm_setnid -- put a node id into a node model object
 *           MEM_alloclist -- allocate a linked list of objects
 *
 ************************************************************************/

  am
gm_addarc(VISE vise, gm grammar, V_NId sourceid, V_NId destinationid)
{
  am      arcModel;  /* an arc model */
  ARCENT  nextFreeAM;

   /* If no free arc model entries exist, make some */
  if (vise->amPool.freearc == NULL &&
      (vise->amPool.freearc =
        (ARCENT) MEM_alloclist(vise->memCfg, sizeof(arcent_t), (V_ULong) param_get(vise, ARCMODELALLOC), SLOW_MEM, "gm_addarc am")) == NULL)
    return(NULL);

   /* At least one free arc model entry now exists; get it */
  nextFreeAM = vise->amPool.freearc;
  vise->amPool.freearc = vise->amPool.freearc->nextarc;

   /* Initialize the arc model in the arc model entry */
  arcModel = &(nextFreeAM->thisarc);
  am_arcinit(arcModel);
  nm_setnid(am_sourcenode(arcModel), sourceid);
  nm_setnid(am_destinationnode(arcModel), destinationid);

   /* Append it to the grammar model's list of arcs */
  if (grammar->gmfirstarc != NULL)
    (grammar->gmlastarc)->nextarc = nextFreeAM;
  else
    grammar->gmcurrentarc = grammar->gmfirstarc = nextFreeAM;
  grammar->gmlastarc = nextFreeAM;
  nextFreeAM->nextarc = NULL;

   /* Return the new arc model */
  return(&(nextFreeAM->thisarc));
}


/**************************************************************************
 *
 * Name:     gm_removearc - Remove an arc from a grammar
 *
 * Call:     gm_removearc(vise, grammar, arc)
 *
 *             am     arc;           -- returned new arc
 *             VISE   vise;          -- a VISE descriptor
 *             am     arc;           -- returned new arc
 *
 * Returns:
 *
 * Requires: Previous initialization of the gm cluster by gm_init()
 *           Initialization of grammar model object with gm_putinfo
 *
 * Modifies: The grammar model
 *
 * Effect:   Removes an arc model from the grammar model object
 *
 * Implementation:
 *
 * Uses:     am_deallocate -- deallocate an arc model object
 *
 ************************************************************************/

  V_Void
gm_removearc(VISE vise, gm grammar, am arc)
{
  ARCENT thisArcEnt, prevArcEnt;

  prevArcEnt = NULL;
  if ((thisArcEnt = grammar->gmfirstarc) != NULL ) {
    if (&thisArcEnt->thisarc == arc) {
      grammar->gmfirstarc = thisArcEnt->nextarc;
    } else {
      prevArcEnt = thisArcEnt;
      while ((thisArcEnt = thisArcEnt->nextarc) != NULL) {
        if (&thisArcEnt->thisarc == arc) {
          prevArcEnt->nextarc = thisArcEnt->nextarc;
          break;
        } else {
          prevArcEnt = thisArcEnt;
        }
      }
    }

    /* Deallocate the arc and place its entry back on the free list */
    if (thisArcEnt) {
      if (grammar->gmlastarc == thisArcEnt)
        grammar->gmlastarc = prevArcEnt;
      if (grammar->gmcurrentarc == thisArcEnt)
        grammar->gmcurrentarc = thisArcEnt->nextarc;
      am_deallocate(vise, arc);
      thisArcEnt->nextarc = vise->amPool.freearc;
      vise->amPool.freearc = thisArcEnt;
    }
  }
}


/***************************************************************************
 *
 * Name:     gm_firstarc - Get first arc in grammar
 *
 * Call:     arc = gm_firstarc(grammar);
 *
 *             am arc;     -- First arc model in the grammar model.
 *             gm grammar; -- A grammar model object.
 *
 * Returns:  The first arc model in the grammar model, or NULL if
 *           there are no arc models in the grammar model.
 *
 * Requires: Previous initialization of the gm cluster by gm_init().
 *           Initalization of grammar model object with gm_putinfo.
 *
 * Modifies: The grammar model.
 *
 * Effect:   Returns the first arc model in the grammar model object
 *           and resets the grammar model arc iterator such that the
 *           next invocation of gm_nextarc will return the  second  arc
 *           model in the grammar.
 *
 * Implementation:
 *
 *           If the grammar model has an arc,
 *             Set current arc to point to the second arc in the grammar.
 *             Return the first arc model.
 *           Else
 *             Return NULL.
 *
 * Uses:
 *
 **************************************************************************/

  am
gm_firstarc(gm grammar)
{
  ARCENT firstarc;

  if ((firstarc = grammar->gmfirstarc)) {
    grammar->gmcurrentarc = firstarc->nextarc;
    return(&firstarc->thisarc);
  } else {
    return(NULL);
  }
}


/**************************************************************************
 *
 * Name:     gm_nextarc - Get next arc in grammar
 *
 * Call:     arc = gm_nextarc(grammar);
 *
 *             am arc;     -- Next arc model in the grammar model
 *             gm grammar; -- A grammar model object
 *
 * Returns:  The next arc model in the grammar model, or NULL if there
 *           are no additional arc models in the grammar model.
 *
 * Requires: Previous initialization of the gm cluster by gm_init().
 *           Initalization of  grammar  model object with gm_putinfo.
 *           Previous call to gm_firstarc.
 *
 * Modifies: The grammar model
 *
 * Effect:   Returns the next arc model in the grammar model object.
 *           Resets  the  grammar  model  arc iterator such that the
 *           next invocation of gm_nextarc will return the next arc
 *           model in the grammar model's arc list.
 *
 * Implementation:
 *
 *           If the grammar model has an additional arc,
 *             Set current arc to point to the next arc in the grammar.
 *             Return the arc model.
 *           Else
 *             Return NULL.
 *
 * Uses:
 *
 **************************************************************************/

  am
gm_nextarc(gm grammar)
{
  ARCENT nextarc;

  if ((nextarc = grammar->gmcurrentarc)) {
    grammar->gmcurrentarc = nextarc->nextarc;
    return(&nextarc->thisarc);
  } else {
    return(NULL);
  }
}


/***************************************************************************
 *
 * Name:     gm_nicount - Get node count of grammar
 *
 * Call:     nodecount = gm_nicount(grammar);
 *
 *             gm     grammar;   -- A grammar model object
 *             V_Int  nodecount; -- Number of nodes in the grammar
 *
 * Returns:  Number of nodes in grammar model
 *
 * Requires: Initalization of grammar model object with gm_putinfo
 *
 * Modifies:
 *
 * Effect:   Returns the number of nodes in a grammar
 *
 * Implementation:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
  V_ULong
gm_nicount(gm grammar)
{
  return(grammar->gmnicount);
}
#endif

/***************************************************************************
 *
 * Name:     gm_id - Get identification number of grammar
 *
 * Call:     id = gm_id(grammar);
 *
 *             gm    grammar; -- A grammar model object
 *             V_GId id;      -- Identification number of grammar
 *
 * Returns:  The identification number for the given grammar model
 *
 * Requires: Initalization of grammar model object with gm_putinfo
 *
 * Modifies:
 *
 * Effect:   Returns the grammar model identification number
 *
 * Implementation:
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
  V_GId
gm_id(gm grammar)
{
  return(grammar->gmid);
}
#endif

/**************************************************************************
 *
 * Name:     gm_deallocate - Remove all arcs from grammar
 *
 * Call:     gm_deallocate(vise, grammar);
 *
 *             VISE  vise;     -- The VISE descriptor
 *             gm    grammar;  -- A grammar model object
 *
 * Returns:
 *
 * Requires: gm_init should be invoked prior to any invocations of
 *           gm_deallocate.  gm_putinfo should be run on a particular
 *           grammar before any invocations of gm_deallocate on
 *           that grammar.
 *
 * Modifies: The grammar
 *
 * Effect:   Deallocates the grammar.
 *
 * Implementation:
 *
 *           For all arcs in grammar,
 *              Deallocate the arc.
 *              Place arc entry on unallocated list.
 *           Reset the grammar model.
 *
 * Uses:     am_deallocate -- deallocate an arc model object
 *           gm_putinfo
 *           gm_setnicount
 *
 **************************************************************************/

  V_Void
gm_deallocate(VISE vise, gm grammar)
{
  ARCENT scan;  /* arc entry iterator */

  if ((scan = grammar->gmfirstarc) != NULL ) {
    while ( scan->nextarc != NULL ) {
      am_deallocate(vise, &scan->thisarc);
      scan = scan->nextarc;
    }
    am_deallocate(vise, &scan->thisarc);

    /* Place grammar's arcs back on the free list */
    scan->nextarc = vise->amPool.freearc;
    vise->amPool.freearc = grammar->gmfirstarc;
  }

  /* Reinitialize the grammar */
  gm_putinfo(grammar, (V_GId) -1);
  gm_setnicount(grammar, 0);
}
