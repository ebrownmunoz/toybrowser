#include "pthr.h"
#include "vise.h"
#include "v_fyi.h"
#include "v_err.h"
#include "v_alg.h"
#include "v_jin.h"
#include "v_syn.h"
#include "v_tm.h"
#include "v_vise.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/**************************************************************************
 *
 * Name:      recloop -- Recognition Inner Loop
 *
 * Returns:   VISESUCCESS -- successful recognition
 *            RECOGTIMEOUT -- recognition timeout
 *            ABORTED -- recognition aborted
 *
 * Requires:  The function WARMSTART should have been called already.
 *            The recognition grammar has been activated.
 *
 * Modifies:  A lot.
 *
 * Effect:    Implements the inner loop of the recognition function.
 *
 *            During each iteration of the loop, a new frame is read
 *            from the jin buffer (as soon as it is available) and given
 *            to the process function to perform dynamic programming.
 *            Grammar node processing is done to update the history
 *            network, check for end-of-utterance, and update the 
 *            grammar nodes for the next frame of dynamic programming.
 *
 * Implementation:
 *
 *            Initialize scores.
 *            Initialize for grammar node processing.
 *            For each frame in the recognition time period,
 *              If the abort flag is set, return ABORTED.
 *              Wait for the jin frame.
 *              Get the jin frame.
 *              Perform dynamic programming.
 *              Process grammar nodes.
 *              Update scores.
 *            Return the proper code.
 *
 * Uses:
 *
 *            JINSRC_read -- Get the next jin frame from the jin buffer
 *            FLAG_lower -- Test and lower the abort flag
 *            process -- Perform the dynamic programming
 *            gramnodes -- Process grammar nodes.
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\RECLOOP.C_V  $
 * 
 *    Rev 3.8   20 Mar 1992 12:49:06   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.7   12 Nov 1991 15:23:02   DCV
 * Now accepts maxpaths as its last argument.
 * Calls nullarcs() separately from process().
 * Expects standard VISE status codes from nullarcs(), process()
 *   and gramnodes().
 * 
 *    Rev 3.6   08 Oct 1991 10:36:40   DCV
 * Added code to eliminate the possibility of hanging in a loop waiting for jin data.
 * 
 *    Rev 3.5   14 Aug 1991 16:22:48   DCV
 * Changed jin_canread() and jin_read() calls to report jin buffer overflow
 * 
 *    Rev 3.4   02 Aug 1991 15:44:10   DCV
 * Eliminated possibility of undetected failure of first call to gramnodes()
 * 
 *    Rev 3.3   12 Jun 1991 14:26:42   DCV
 * Modified for N-best path reporting
 * 
 *    Rev 3.2   22 Jan 1991 11:01:06   DCV
 * Added FYI invocations
 * Improved documentation
 * 
 *    Rev 3.1   13 Sep 1990 13:54:48   DCV
 * First version with individual header files
 * 
 *    Rev 3.0   05 Sep 1990  9:08:50   DCV
 * First SPOX revision
 * 
 *    Rev 1.1   24 Jul 1990 14:14:18   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990  9:32:44   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Err
recloop(VISE vise, V_SN startSN, V_SN endSN, tbn * termnode, V_Uns maxpaths, V_Bool abortable)
{
  V_Int   bestscore;      /* Global best score for current frame  */
  V_Int   oldbestscore;   /* Global best score for previous frame */
  V_Long  sumbestscores;  /* Sum of all previous best scores      */
  V_Long  sumglobalmins;  /* Sum of all previous global minima    */
  V_SN    sn;             /* Sequence number of the current frame */
  V_Time  frameId;        /* Arbitrary sequential frame index     */
  jinr    jinframe;       /* current acoustic frame data          */
  V_Err   exception;
  V_Err   status;

  FYI_DEFHANDLE(minBestScore); /* DIAGNOSTIC */
  FYI_DEFHANDLE(maxBestScore); /* DIAGNOSTIC */
  FYI_DEFHANDLE(minGlobalMin); /* DIAGNOSTIC */
  FYI_DEFHANDLE(maxGlobalMin); /* DIAGNOSTIC */

  /* Set up and initialize diagnostic parameters */
  FYI_ASSIGNP(FYI_GETHANDLE(vise, minBestScore, FYI_MINBESTSCORE),=,V_INFINITY);
  FYI_ASSIGNP(FYI_GETHANDLE(vise, maxBestScore, FYI_MAXBESTSCORE),=,0);
  FYI_ASSIGNP(FYI_GETHANDLE(vise, minGlobalMin, FYI_MINGLOBALMIN),=,V_INFINITY);
  FYI_ASSIGNP(FYI_GETHANDLE(vise, maxGlobalMin, FYI_MAXGLOBALMIN),=,0);

  /* VBX_DEBUG(VBX_print("  VISE::RECLOOP(startSN = %ld, endSN = %ld, nbest = %u, abortable = %d)\n",
                                          startSN,       endSN,       maxpaths,   abortable)); */

  /* Initialize scores */
  bestscore = 0;
  sumbestscores = sumglobalmins = 0L;

  /* Extend paths over null arcs */
  if ((status = nullarcs(vise, syn_getactive(vise), maxpaths)) != VISESUCCESS)
    return(status);

  /* Create a starting frame index not recently used */
  frameId = (V_Time) (startSN & (SN) 8191);

  VBX_DEBUG(VBX_print("  VISE::recloop: Starting SN = %ld; starting frameId = %d; max frames = %u\n", startSN, frameId, vise->maxFrames));

  /* Iterate over each frame in the specified range of sequence numbers */
  sn = startSN;
  while (TRUE) {

    /* Process grammar nodes:
       Perform the end of utterance test
       Add a new frame to the traceback tree
       Update grammar nodes for next frame
       Prune the traceback tree */
    if ((status = gramnodes(vise, bestscore, sumglobalmins, sumbestscores, frameId, sn, termnode)) != EOUNOTYETFOUND)
      return(status);

    oldbestscore = bestscore; 
    sumbestscores += (V_Long) bestscore;

    /* If the specified period has been exhausted, give up */
    if ((vise->maxFrames && !--vise->maxFrames) || (endSN >= (V_SN) 0 && sn > endSN))
      return(RECOGTIMEOUT);

    /* Fetch the new jin frame */
    VBX_DEBUG(VBX_print("  VISE::recloop: requesting SN = %ld\n", sn));
    if (!JINSRC_read(vise->jinSource, &sn, &jinframe, &exception))
      return(exception);
    VBX_DEBUG(VBX_print("  VISE::recloop: got SN = %ld\n", sn));

    /* Perform the dynamic programming */
    if ((status = process(vise, &jinframe, frameId, oldbestscore, &bestscore, maxpaths)) != VISESUCCESS)
      return(status);

    /* Extend paths over null arcs */
    if ((status = nullarcs(vise, syn_getactive(vise), maxpaths)) != VISESUCCESS)
      return(status);

    sumglobalmins += (V_Long) tm_globalmin(vise);

    FYI_SETMINP(minBestScore, bestscore);
    FYI_SETMAXP(maxBestScore, bestscore);
    FYI_SETMINP(minGlobalMin, tm_globalmin(vise));
    FYI_SETMAXP(maxGlobalMin, tm_globalmin(vise));

    frameId++;
    if (sn == SN_MAX) sn = (V_SN) 0;
    else              sn++;
  }
}

