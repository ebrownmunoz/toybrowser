#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_fyi.h"
#include "v_ai.h"
#include "v_alg.h"
#include "v_gi.h"
#include "v_jin.h"
#include "v_ki.h"
#include "v_ni.h"
#include "v_par.h"
#include "v_syn.h"
#include "v_tm.h"
#include "v_wi.h"

/***************************************************************************
 *
 * Name:      process -- Dynamic Programming for one frame
 *
 * Returns:   VISESUCCESS or CANTALLOCATETBP
 *
 * Requires:  Prior initialization of grammar state.  Proper
 *            initialization for signal processing.
 *
 * Modifies:  the active grammar, *bestscore.
 *
 * Effect:    This function controls the dynamic programming portion of
 *            recognition.  On each invocation, process performs the 
 *            dynamic programming calculation for a single frame of
 *            acoustic parameter data over the entire active grammar.
 *
 * Implementation:
 *
 *            Iterate over all the word instances on all the non-null
 *            arcs of the active grammar.
 *
 *            Perform word level dynamic programming on every word
 *            which is not completely inactive.
 *
 *            The word inherits a new score and traceback node
 *            from the source node of its arc instance.
 *
 *            Inactive words have their first kernel turned on if the
 *            seed score in the source node is below the activation
 *            threshold.
 *
 *            A minimum score for the word and the last kernel is
 *            kept during the dynamic programming process.
 *            If the minimum score for the whole word is greater than
 *            some predetermined threshold, then the word is considered
 *            to be on a fruitless path, and it is deactivated, which
 *            means that all kernels but the first become inactive,
 *            thus reducing the number of kernels that will have to
 *            be examined when processing the next frame of jin data.
 *
 *            If the minimum score for the final kernel is greater
 *            than threshold, then it alone is deactivated.
 *
 *            If all of the kernels in a word are active, then it is
 *            possible for the word to inherit its scores to the 
 *            destination node of the arc it is on.  This is attempted
 *            provided that the word's score (the score of the self-
 *            loop node of the final kernel in the word) is better
 *            (less) than the worst inherited score of the grammar node.
 *
 *            If not all of the kernels in the word are active, then
 *            if the score of the final active kernel in the word is 
 *            less than a threshold, another kernel (the next in time)
 *            is made active. Thus, when the word gets on a good path,
 *            its kernels will wake up one by one until the
 *            word is at last fully active, and able to inherit to
 *            its destination grammar node.
 *
 *            Process maintains a return value which is the global
 *            minimum score that it encountered for this frame.
 *
 *            For all non-null arcs in the grammar
 *              For all words on the arc
 *
 *                If word completely inactive AND
 *                  seedscore exceeds activation threshold
 *                  Activate first kernel of word
 *
 *                If word not completely inactive
 *
 *                  Process word
 *
 *                  If minimum score for word > DEACTIVATIONTHRESHOLD
 *                    deactivate word
 *
 *                  Else
 *                  If the minimum score in the last kernel > DEACTIVATIONTHRESHOLD
 *                    put it to sleep
 *
 *                If all kernels in word are active
 *                  If word score < destination node's worst
 *                    saved path score
 *                    Attempt to inherit from word to
 *                    destination grammar node
 *                Else
 *                  If word's final score < ACTIVATIONTHRESHOLD
 *                    wake next inactive kernel in word
 *
 *            Set call-by-result parameter to minimum score
 *            encountered for the current frame
 *
 *            Return TRUE
 *
 * Uses:      ai_destinationnode -- Get destination node for arc
 *            ai_firstword -- Get first word on arc
 *            ai_sourcenode -- Get source node for arc
 *            ni_worstscore -- Get worst path score from node
 *            ni_gseed -- Get seed score from node
 *            insertpaths -- Put path information into node
 *            nullarcs -- Process null arcs
 *            processword -- Do dynamic programming on a word
 *            wi_active -- Test if word completely active
 *            wi_inactive -- Test if word completely inactive
 *            ai_deactivate -- Deactivate word on arc
 *            wi_wakekernel -- Make next inactive kernel active
 *            wi_napkernel -- Make last active kernel inactive
 *            wi_id -- Get word identification number
 *            wi_status -- Get status of word
 *            syn_getactive -- Get active grammar instance
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\PROCESS.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:48:04   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   29 Jan 1992 15:15:24   DCV
 * Changed basic types
 * 
 *    Rev 3.5   12 Nov 1991 15:15:00   DCV
 * Now calls insertpaths() in place of ni_insertpaths().
 * No longer calls nullarcs().
 * Now accepts numpaths as its last argument.
 * 
 *    Rev 3.4   20 Sep 1991 09:35:34   DCV
 * Replaced ni_insertpath() with ni_insertpaths()
 * 
 *    Rev 3.3   12 Jun 1991 14:19:28   DCV
 * Modified to support N-best path reporting.
 * Re-enabled wildcard pruning (MUTHAH thresholding still prevents pruning
 *   the joker path) to avoid excessive use of tbns during enrollment.
 * 
 *    Rev 3.2   22 Jan 1991 10:58:12   DCV
 * Modified FYI invocations
 * Disabled deactivation of wildcards
 * Improved documentation
 * 
 *    Rev 3.1   20 Dec 1990 14:29:32   DCV
 * Replaced ai_active() with ai_activeCount()
 * Added FYI calls to set FYI_ACTIVEWORDS
 * 
 *    Rev 3.0   13 Sep 1990 13:54:02   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:13:42   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990  9:29:22   DCV
 * Initial revision.
 *
 **************************************************************************/

  V_Err
process(VISE vise, jin jinframe, V_Time time, V_Int oldbestscore, V_Int *bestscore, V_Uns maxpaths)
  /* jin    jinframe     - Time slice characterization           */
  /* V_Time time         - Time of jinframe                      */
  /* V_Int  oldbestscore - Best score for previous frame         */
  /* V_Int *bestscore    - Returned best score in current frame  */
  /* V_Uns  maxpaths;    - Max # of alternative paths to retain  */
{
  V_Int  newbestscore;   /* Minimum score so far this frame      */
  V_Int  wordbestscore;  /* Minimum score of current word        */
  V_Int  bestlastscore;  /* Minimum score of last kernel         */
  gi     grammar;        /* Active grammar                       */
  ai     arc;            /* Arc being processed                  */
  wi     word;           /* Word being processed                 */
  ni     srcnode;        /* Source grammar node for current arc  */
  ni     dstnode;        /* Target grammar node for current arc  */
  ai     lastarc;        /* Iteration sentinel for arcs          */
  wi     lastword;       /* Iteration sentinel for words         */
  KND    lastknode;      /* Last knode in kernel                 */
  V_Int  wordstatus;     /* Status of current word               */

  FYI_DEFINE(V_Int,wrdCnt=0);  /* DIAGNOSTIC: Num active words   */
  FYI_DEFHANDLE(activeWords);  /* DIAGNOSTIC                     */

  FYI_GETHANDLE(vise, activeWords, FYI_ACTIVEWORDS);
  FYI_ASSIGNP(activeWords,=,0);

  /* Assume the worst score */
  newbestscore = V_INFINITY;

  /* Initialize template matcher */
  tm_init(vise, jinframe, time, oldbestscore);

  /* Get the active grammar */
  grammar = syn_getactive(vise);

  lastarc = gi_lastarc(grammar);

  /* Iterate over all non-null arcs in the active grammar */
  for (arc = gi_firstnonnullarc(grammar); arc < lastarc; arc++) {
    /* Get word-independent arc characterization information */
    srcnode = ai_sourcenode(arc);

    /* If this arc is inheriting good scores from the source node, */
    if (ni_seedscore(srcnode) < param_get(vise, ACTIVATIONTHRESHOLD))
      /* Activate all words on this arc */
      ai_activate(arc);

    /* If there are any active words on this arc, process the arc */
    if ((V_Bool)ai_activeCount(arc)) {
      FYI_ASSIGN(wrdCnt,+=,ai_activeCount(arc));

      dstnode = ai_destinationnode(arc);

      word = ai_firstword(arc);

       /* Set template matcher for word type.
        (All of the words on an arc must have the same type) */
      tm_setstatus(vise, (wordstatus = wi_status(word)));

       /* Iterate over all words on the arc */
      for (lastword = ai_lastword(arc); word <= lastword; word++) {
        /* If the word is active */
        if (!wi_inactive(word)) {
          /* Perform word level dynamic programming */
          processword(vise, word, ni_seedscore(srcnode), ni_seedpath(srcnode), &wordbestscore, &bestlastscore);

          /* Update minimum score for this frame */
          if (wordbestscore < newbestscore)
            newbestscore = wordbestscore;

          /* If the word's minimum score is greater than the deactivation threshold */
          if (param_get(vise, DEACTIVATIONTHRESHOLD) < wordbestscore) {
            /* Deactivate all kernels in the word */
            wi_deactivate(word);
            ai_deactivate(arc);
          } else {
            /* If the minimum score of the last active kernel is greater than the deactivation threshold,
               deactivate the last kernel (We know there is more than one kernel active) */
            if (param_get(vise, DEACTIVATIONTHRESHOLD) < bestlastscore)
              wi_napkernel(word);

            lastknode = wi_lastkernel(word)->kilastknode;

            /* If all kernels in word are active */
            if (wi_active(word)) {
              /* If the score of the last kernel of the word is smaller (better) than the score of the worst path
                 to the destination node of the current arc (smaller, not smaller or equal, lest we needlessly
                 inherit infinite scores!), the destination node just may inherit from the current word */
              if (lastknode->kiscore < ni_worstscore(dstnode))
                if (!insertpaths(vise, dstnode, lastknode->kiscore, lastknode->kitbpath, wi_model(word), &(dstnode->niworstscore), maxpaths))
              return(CANTALLOCATETBP);

            /* Else, not all kernels in word active yet */
            } else {
             /* If the score of the last active kernel is less than the activation threshold, then the next
                inactive kernel is made active for the next frame of dynamic programming, perform a simplified
                wi_wakekernel(word), given: word->wiactivecount > 0 and word->wiactiveregion < wi_lastkernel(word) */      
              if (lastknode->kiscore < param_get(vise, ACTIVATIONTHRESHOLD)) {
                ki_knodeinit(++word->wiactiveregion);
                word->wiactivecount++;
              }
            }
          }
        }
      }
    }
  }

   /* Pass back the minimum score for this frame */
  *bestscore = newbestscore;

  FYI_SETMAXP(activeWords, wrdCnt);

  return(VISESUCCESS);
}
