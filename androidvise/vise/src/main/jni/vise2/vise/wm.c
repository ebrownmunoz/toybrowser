#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_km.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_tem.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:        wm -- Word Model Cluster
 *
 * Description: The word model cluster implements the abstract word
 *              model objects.  These objects contain the relatively
 *              static description of words, and do not change
 *              frequently.  A word model contains of an ordered set of
 *              kernel model objects.
 *
 * Operations:
 *
 *    1. wm_id(word); -- Get word identification number.
 *
 *    2. wm_firstkernel(word); -- Returns the first kernel in the word
 *
 *    3. wm_numkernels(word); -- Return the number of kernels in the
 *    word.
 *
 *    4. wm_allocate(vise, word, id, class, numkernels);  --  Allocate
 *    and insert into the word model the specified number of kernels.
 *
 *    5. wm_deallocate(vise, word) -- Removes and deallocates all kernels in
 *    the given word model.
 *
 *    6. wm_status(word) -- Get the status of a word model
 *
 *    7. wm_setstatus(word) -- Set the status of a word model
 *
 *    8. wm_wildcard(word, &relative, &absolute) -- Get the wildcard
 *    thresholds for a word
 *
 *    9. wm_setwildcard(word, relative, absolute) -- Set the wildcard
 *    thresholds for a word
 *
 *   10. wm_init(vise) -- Initialize word model cluster.
 *
 *   11. wm_allocatekt(vise, word) -- Allocate kernel training objects for the word
 *
 *      wm    word;     -- a word model object
 *      VISE  vise;     -- a VISE descriptor
 *      km    kernel;   -- a kernel model object
 *      V_Int status;   -- word status, WILDCARD or TEMPLATES
 *      V_Int relative; -- WILDCARD word relative cost threshold
 *      V_Int absolute; -- WILDCARD word absolute cost threshold
 *
 * Computer:  TMS320C30
 *
 * Unit Test:  wmt.ct
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\WM.C_V  $
 * 
 *    Rev 3.8   20 Mar 1992 13:15:12   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.7   08 Oct 1991 10:22:38   DCV
 * Changed wm_allocate() to deal gracefully with words having no kernels.
 * 
 *    Rev 3.6   20 Sep 1991 09:49:16   DCV
 * Added a word class field to word models.
 * 
 *    Rev 3.5   14 Aug 1991 16:48:12   DCV
 * Made KERNELMODELALLOC a VISE parameter.
 * Now calls MEM_alloclist() to allocate kernel model objects.
 * 
 *    Rev 3.4   22 Jul 1991 16:26:56   DCV
 * Corrected error in wm_allocate()
 * 
 *    Rev 3.3   18 Jul 1991 16:29:46   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   20 Dec 1990 14:26:32   DCV
 * Eliminated useless search for km in wm_deallocate()
 * 
 *    Rev 3.1   07 Nov 1990 16:42:06   DCV
 * Modified MEM_xxx calls for mem.c
 * 
 *    Rev 3.0   13 Sep 1990 14:05:58   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:21:00   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:40:40   DCV
 * Initial revision.
 *
 ************************************************************************/

/* Representation */

/* A word model consists of an ordered set (list) of kernel model  */
/* objects.  Each kernel model representation (km_t) contains a pointer */
/* to the next kernel model in the list, as well as a template model */
/* object for the template characterizing the kernel.     */

/* A pointer to a kernel model representation (km_t) is a kernel model */
/* (km), just as a pointer to a tmr is a tm.  This is the only extra */
/* knowledge which the word model cluster has about foreign objects  */
/* besides that obtainable through the appropriate cluster operations. */

/* A word model object contains a pointer to the first kernel model in */
/* the linked list of kernel models describing the kernels in the word, */
/* as well as a count of the number of kernels in the word.    */

/**************************************************************************
 *
 * Name:      wm_id - Get word model identification number
 *
 * Call:      ident = wm_id(word);
 *
 *              V_WId ident; -- word model identification number
 *              wm     word; -- a word model
 *
 * Returns:   The identification number for the given word model object
 *
 * Requires:  wm_allocate should be invoked on a word model before
 *            any invocation of wm_id.
 *
 * Modifies:
 *
 * Effect:    Returns the ID number for the given word model
 *
 * Implementation:
 *
 *            gets identification number from representation.
 *
 * Uses:
 *
 ************************************************************************/

#ifdef NOMACROS
  V_WId
wm_id(wm word)
{
  return(word->wmid);
}
#endif

/**************************************************************************
 *
 * Name:      wm_firstkernel -- Get the first kernel in a word
 *
 * Call:      kernel = wm_firstkernel(word);
 *
 *              km kernel; -- a kernel model
 *              wm word;   -- a word model
 *
 * Returns:   The first kernel model in the word.
 *
 * Requires:  wm_init should be invoked before any invocations of
 *            wm_firstkernel.  wm_allocate should be invoked with a
 *            particular word model before any invocation of
 *            wm_firstkernel is made with that kernel.
 *
 * Modifies:  
 *
 * Effect:    Returns the first kernel model in a given word model.
 *
 * Implementation:
 *
 *            Return the first kernel in the word
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
  km
wm_firstkernel(wm word)
{
  return(word->wmfirstkernel);
}
#endif

/*************************************************************************
 *
 * Name:      wm_numkernels -- Number of kernels in word
 *
 * Call:      kernelcount = wm_numkernels(word);
 *
 *              V_Int kernelcount; -- Number of kernels in word.
 *              wm           word; -- A word model object.
 *
 * Returns:   The number of kernel models in the given word model
 *            object.
 *
 * Requires:  wm_allocate must be invoked on a word model before
 *            wm_numkernels ever is.
 *
 * Modifies:
 *
 * Effect:    Returns the number of kernels in the word as specified
 *            in the invocation of wm_allocate.
 *
 * Implementation:
 *
 *            Return number of kernels field of word model
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
  V_Int
wm_numkernels(wm word)
{
  return(word->wmkernelcount);
}
#endif

/************************************************************************
 *
 * Name:      wm_allocate -- Allocate kernels for a word model
 *
 * Call:      wm_allocate(vise, word, id, class, numkernels);
 *
 *              VISE        vise; -- A VISE descriptor
 *              wm          word; -- The word model to be initialized.
 *              V_WId         id; -- The word identification number.
 *              V_Uns      class; -- The word class number.
 *              V_Int numkernels; -- Number of kernels for word.
 *
 * Returns:   TRUE if successful, FALSE if there are not enough
 *            kernels to allocate for the word as requested.
 *
 * Requires:  This operation may only be performed on an unallocated
 *            word model.  wm_init should be invoked before any calls
 *            to wm_allocate.  The numkernels argument must be
 *            greater than or equal to 1.
 *
 * Modifies:  word
 *
 * Effect:    The given word model is initialized and given a list
 *            containing the specified number of newly allocated
 *            kernel models.
 *
 * Implementation:
 *
 *            Fill in ID, class, status and kernel count.
 *
 *            For numkernels iterations
 *              If there are no more free kernels,
 *                Give the word no kernel models
 *                Return FALSE
 *              Else
 *                Get a kernel off the free kernel list and
 *                  append it to word model's kernel list
 *
 *            NULL-terminate the word model's kernel list
 *
 *            Return TRUE
 *
 * Dependency:
 *
 * Uses:      param_get -- get a VISE parameter
 *            km_putnextkernel
 *            km_nextkernel
 *            MEM_alloclist
 *
 *************************************************************************/

  V_Bool
wm_allocate(VISE vise, wm word, V_WId id, V_Uns class, V_Int numkernels)
{
  km previousKM;

  word->wmid = id;
  word->wmclass = class;
  word->wmstatus = UNINITIALIZED;

   /* If the word has no kernels, NULL its kernel list pointer */
  if ((word->wmkernelcount = numkernels) == 0) {
    word->wmfirstkernel = NULL;

   /* Otherwise, link together numkernels kernel models for the word */
  } else {
    word->wmfirstkernel = previousKM = vise->kmPool.freekernel;
    while (numkernels--) {
       /* If there are no free kernel models, try to make some */
      if (vise->kmPool.freekernel == NULL) {
         /* If none can be made */
        if ((vise->kmPool.freekernel =
                           (km) MEM_alloclist(vise->memCfg, KM_SIZE, (V_ULong) param_get(vise, KERNELMODELALLOC), SLOW_MEM, "wm_allocate km")) == NULL) {
           /* Restore the free kernel pointer */
          vise->kmPool.freekernel = word->wmfirstkernel;
           /* Give the word no kernels */
          word->wmkernelcount = 0;
          word->wmfirstkernel = NULL;
          return(FALSE);
        }

         /* Append the new freelist to the end of the old one */
        if (previousKM != NULL)
          km_putnextkernel(previousKM, vise->kmPool.freekernel);

         /* Make sure wmfirstkernel doesn't end up NULL */
        if (word->wmfirstkernel == NULL)
          word->wmfirstkernel = vise->kmPool.freekernel;
      }

       /* Move to the next free KM, but remember the previous one */
      vise->kmPool.freekernel = km_nextkernel(previousKM = vise->kmPool.freekernel);
    }

     /* NULL-terminate the list */
    km_putnextkernel(previousKM, NULL);
  }

   /* Allocate kernel training objects if batch training */
  if (vise->batchTrain)
    wm_allocatekt(vise, word);

  return(TRUE);
}

/************************************************************************
 *
 * Name:      wm_allocatekt -- Allocate kernel training objects for a word model
 *
 * Call:      wm_allocatekt(vise, word);
 *
 *              VISE        vise; -- A VISE descriptor
 *              wm          word; -- The word model to be initialized.
 *
 * Returns:   TRUE if successful, FALSE if there are not enough kernel
 *            training objects to allocate for the word as requested.
 *
 * Requires:  wm_allocate should be invoked before any calls to wm_allocatekt
 *
 * Modifies:  word
 *
 * Effect:    The given word model is given a list of kernel training objects
 *
 * Implementation:
 *
 *            For numkernels iterations
 *              If there are no more free kernel training objects,
 *                Give the word no kernel training objects
 *                Return FALSE
 *              Else
 *                Get a kernel training object off the free list and
 *                  append it to word model's kt list
 *
 *            NULL-terminate the word model's kt list
 *
 *            Return TRUE
 *
 * Dependency:
 *
 * Uses:      param_get -- get a VISE parameter
 *            kt_putnextkt
 *            kt_nextkt
 *            MEM_alloclist
 *
 *************************************************************************/

  V_Bool
wm_allocatekt(VISE vise, wm word)
{
  kt    previousKT;
  V_Int numkernels;

   /* If the word has no kernels, NULL its kt list pointer */
  if ((numkernels = word->wmkernelcount) == 0) {
    word->wmfirstkt = NULL;
    VBX_print("  VISE:wm_allocatekt setting word->wmfirstkt to NULL for word @%p (wid = %d)\n", word , word->wmid); /* DEBUG */

   /* Otherwise, link together numkernels kt objects for the word */
  } else if (word->wmfirstkt == NULL) {
    word->wmfirstkt = previousKT = vise->kmPool.freekt;
    while (numkernels--) {
       /* If there are no free kt objects, try to make some */
      if (vise->kmPool.freekt == NULL) {
         /* If none can be made */
        if ((vise->kmPool.freekt =
               (kt) MEM_alloclist(vise->memCfg, KT_SIZE, (V_ULong) param_get(vise, KERNELMODELALLOC), SLOW_MEM, "wm_allocate kt")) == NULL) {
           /* Restore the free kt pointer */
          vise->kmPool.freekt = word->wmfirstkt;
           /* Give the word no kernel training objects */
          word->wmfirstkt = NULL;
          return(FALSE);
        }

         /* Append the new freelist to the end of the old one */
        if (previousKT != NULL)
          kt_putnextkt(previousKT, vise->kmPool.freekt);

         /* Make sure wmfirstkt doesn't end up NULL */
        if (word->wmfirstkt == NULL)
          word->wmfirstkt = vise->kmPool.freekt;
      }

       /* Move to the next free kt, but remember the previous one */
      vise->kmPool.freekt = kt_nextkt(previousKT = vise->kmPool.freekt);

       /* Initialize the kt */
      kt_settrainingcount(previousKT, 0);
      kt_clearaccumulator(previousKT);
      kt_putobsdwells(previousKT, MAXDWELL, 1);
    }

     /* NULL-terminate the list */
    kt_putnextkt(previousKT, NULL);
  }

  return(TRUE);
}

/**************************************************************************
 *
 * Name:      wm_deallocate -- Remove kernels from word model
 *
 * Call:      wm_deallocate(vise, word);
 *
 *              VISE  vise; -- A VISE descriptor
 *              wm    word; -- the word model which is to be deallocated
 *
 * Returns:   wm_init must be invoked before any invocation of
 *            wm_deallocate.   wm_allocate must be called on a word
 *            model before wm_deallocate is invoked on that word
 *            model.
 *
 * Requires:  word must have at least one kernel model.
 *
 * Modifies:  word
 *
 * Effect:    Removes all kernels from word.  Kernels are disposed of
 *            in a manner such that they can be recycled. This
 *            operation is therefore an important part of the word
 *            deletion process.
 *
 * Implementation:
 *
 *            For all kernels in word
 *              Place kernel on deallocated kernel list
 *
 *            Reset word model to indicate that it is empty
 *
 * Uses:      km_nextkernel
 *            km_putnextkernel
 *
 **************************************************************************/

  V_Void
wm_deallocate(VISE vise, wm word)
{
  km  nextKM = word->wmfirstkernel;
  km  lastKM;
  kt  nextKT = word->wmfirstkt;
  kt  lastKT;

   /* Deallocate the kernel model objects */
  if (nextKM != NULL) {
    do nextKM = km_nextkernel(lastKM = nextKM);
    while (nextKM != NULL);

    km_putnextkernel(lastKM, vise->kmPool.freekernel);
    vise->kmPool.freekernel = word->wmfirstkernel;
    word->wmfirstkernel = NULL;
  }

   /* Deallocate the kernel training objects */
  if (nextKT != NULL) {
    do nextKT = kt_nextkt(lastKT = nextKT);
    while (nextKT != NULL);

    kt_putnextkt(lastKT, vise->kmPool.freekt);
    vise->kmPool.freekt = word->wmfirstkt;
    word->wmfirstkt = NULL;
  }

  word->wmkernelcount = 0;
}

/**************************************************************************
 *
 * Name:      wm_init -- Initialize word model cluster
 *
 * Call:      wm_init(vise);
 *
 *              VISE vise; -- A VISE descriptor
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies:  the word model cluster.
 *
 * Effect:    Initializes the word model cluster.  Invocation of this
 *            function must preceed invocation of any other word
 *            model cluster functions.
 *
 * Implementation:
 *
 *            Empty the list of free kernel models and the list of free
 *            kernel training objects.
 *
 * Dependency:
 *
 * Uses:    
 *
 *************************************************************************/

  V_Void
wm_init(VISE vise)
{
  vise->kmPool.freekernel = NULL;
  vise->kmPool.freekt = NULL;
}


/*************************************************************************
 *
 * Name:      wm_status - Get the status of word model
 *
 * Call:      status = wm_status(word);
 *
 *            V_Int  status; -- The status of the word
 *            wm       word; -- A word model object
 *
 * Returns:   The status of the word. This should be one of
 *            { UNINITIALIZED, TEMPLATES, WILDCARD }.
 *
 *
 * Requires:  The word model must already be allocated.
 *
 * Modifies:
 *
 * Effect:    This function returns the word model status,  which
 *            refers to the status of the word's sound representation
 *            or cost generation components.
 *
 * Implementation:
 *
 *            Retrieve the status field of the word model representation
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS
  V_Int
wm_status(wm word)
{
  return(word->wmstatus);
}
#endif

/**************************************************************************
 *
 * Name:      wm_setstatus - Set status of word model
 *
 * Call:      wm_setstatus(word, status);
 *
 *              wm      word; -- A word model object
 *              V_Int status; -- The status of the word
 *
 * Returns:
 *
 * Requires:  The word model must already be allocated.
 *
 * Modifies:  word
 *
 * Effect:    This function sets the word model status, which refers
 *            to the status of the word's sound representation or
 *            cost generation components.   It should be one of
 *            { UNINITIALIZED, TEMPLATES, WILDCARD }.
 *
 * Implementation:
 *
 *            Store the given status in the status field
 *              of the word model representation
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
  V_Void
wm_setstatus(wm word, V_Int status)
{
  word->wmstatus = status;
}
#endif

/*************************************************************************
 *
 * Name:      wm_setwildcard - Set wildcard thresholds for word model
 *
 * Call:      wm_setwildcard(word, relative, absolute);
 *
 *            wm        word; -- A word model object
 *            V_Int relative; -- The relative wildcard threshold
 *            V_Int absolute; -- The absolute wildcard theshold
 *
 * Returns:
 *
 * Requires:  word must have status WILDCARD
 *
 * Modifies:  word
 *
 * Effect:    This function sets the word model wildcard thresholds.
 *
 * Implementation:
 *
 *            relative and abosolute wildcard thresholds are stored
 *            in the first two words of the template of the word's
 *            first kernel.
 *
 *            Store the given relative and absolute
 *              wildcard thresholds in the first two
 *              words of a template image array.
 *            Get word's kernel
 *            Get kernlel's template
 *            Store array in template
 *
 * Uses:
 *
 *      wm_firstkernel -- Get first kernel of word
 *      km_template -- Get kernel's template
 *      tem_setwildcard -- Write data to template
 *
 **************************************************************************/

  V_Void
wm_setwildcard(wm word, V_Int relative, V_Int absolute)
{
  tem_setwildcard(km_template(wm_firstkernel(word)), relative, absolute);
}

/*************************************************************************
 *
 * Name:      wm_wildcard - Get wildcard thresholds for word model
 *
 * Call:      wm_wildcard(word, relative, absolute);
 *
 *            wm          word; -- A word model object
 *            V_Int * relative; -- The relative wildcard threshold
 *            V_Int * absolute; -- The absolute wildcard theshold
 *
 * Returns:
 *
 * Requires:  word must have status  WILDCARD.  Wildcard thresholds
 *            must be already set with wm_setwildcard.
 *
 * Modifies:  word
 *
 * Effect:    This function gets the word model wildcard thresholds.
 *
 * Implementation:
 *
 *            wildcard thresholds are stored in the first two words
 *            of the word's template
 *            Get first kernel in word
 *            Get template for kernel
 *            Read template into image array
 *            Retrieve the relative and absolute
 *              wildcard thresholds from the array
 *
 * Uses:
 *
 *            wm_firstkernel -- get first kernel in word
 *            km_template -- get template for kernel
 *            tem_getwildcard -- get info from template
 *
 ***************************************************************************/

#ifdef UNUSED
  V_Void
wm_wildcard(wm word, V_Int * relative, V_Int * absolute)
{
  tem_getwildcard(km_template(wm_firstkernel(word)), relative, absolute);
}
#endif

