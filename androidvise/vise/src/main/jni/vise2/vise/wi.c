#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_ki.h"
#include "v_km.h"
#include "v_mem.h"
#include "v_wi.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:    wi -- Word Instance Cluster
 *
 * Description: The word instance cluster implements the abstract word
 *      instance objects.  One word instance exists for each
 *      occurrence of a word in the active grammar; which is
 *      unlike the word models, where only one word model
 *      exists for each word in the vocabulary.  A word
 *      instance object consists of an ordered set of kernel
 *      instances.  A word instance has an active region, which
 *      is a contiguous set of kernels starting at the first
 *      kernel instance in the word.  A word is active when all
 *      of its kernels are part of the active region.  A word
 *      is inactive when none of its kernels are in the active
 *      region.
 *
 * Operations:
 *
 *    1. result = wi_init(vise, kicount); - Initialize word instance cluster.
 *
 *    2. kicount = wi_kernelcount(vise); - Count of kernel instances allocated.
 *
 *    3. outcome = wi_replicate(vise, wordinst, model); - Create an instance of a word model.
 *
 *    4. wi_wakekernel(wordinst); - Extend active region by one kernel.
 *
 *    5. wi_napkernel(wordinst); - Reduce active region by one kernel.
 *
 *    6. wi_deactivate(wordinst); - Reduce active region to first kernel.
 *
 *    7. wi_lastkernel(wordinst); - Get last kernel in active portion of word instance.
 *
 *    8. wi_previouskernel(wordinst); - Get previous kernel in word instance.
 *
 *    9. wi_numactive(wordinst); - Number of active kernels in word.
 *
 *   10. wi_active(wordinst); - Tells if whole word is active.
 *
 *   11. wi_inactive(wordinst); - Tells if whole word is inactive
 *
 *   12. wi_model(wordinst); - Get the word model.
 *
 *   13. wi_status(wordinst); - Get word's status
 *
 *     V_Bool   result; -- Was memory allocated for the ki's?
 *     VISE       vise; -- A VISE descriptor
 *     V_Int   outcome; -- VISE status code
 *     VISE       vise; -- A VISE descriptor
 *     V_ULong kicount; -- The number of kernel instances
 *     wi     wordinst; -- A word instance object.
 *     wm        model; -- A word model object.
 *
 * Computer: TMS320C30
 *
 * Unit test: wit.ct
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\WI.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 13:14:00   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   29 Jan 1992 15:32:34   DCV
 * Changed basic types
 * 
 *    Rev 3.5   12 Nov 1991 16:19:12   DCV
 * wi_replicate() now returns standard VISE status codes.
 * 
 *    Rev 3.4   08 Oct 1991 10:05:16   DCV
 * Added wi_kernelcount() to report the number of kernel instances allocated since
 *   last wi_init().  Changed wi_replicate() to allocate kernel instances from free
 *   memory if none are left in the kernel instance pool.
 * 
 *    Rev 3.3   20 Sep 1991 10:13:14   DCV
 * Eliminated wi_id() and added wi_model()
 * 
 *    Rev 3.2   18 Jul 1991 16:29:06   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.1   07 Nov 1990 16:41:28   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 14:05:42   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:20:46   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:39:52   DCV
 * Initial revision.
 *
 **************************************************************************/

/* first <= active <= last              */
/* current < active               */
/* it is possible for (current = first - 1),         */
/* which implies that the "previous kernel" operation has iterated */
/* over all of the  kernel instances          */

/* when (active == last), the whole word is active        */
/* when (active == NULL), the word is inactive        */

/**************************************************************************
 *
 * Name:    wi_init - Initialize word instance cluster
 *
 * Calling Sequence:
 *
 *      result = wi_init(vise, kicount);
 *
 *      V_Bool result;  -- Was memory allocated for the ki's?
 *                      VISE    vise;    -- A VISE descriptor
 *      V_ULong kicount; -- The number of kernel instances
 *
 * Returns:  TRUE or FALSE
 *
 * Requires:  Memory for kernel instances must have been deallocated
 *
 * Modifies:  The word instance cluster
 *
 * Effect:   Initializes the word instance cluster.
 *
 * Implementation:
 *
 *      Allocate fast memory for kernel instances, or return FALSE.
 *      Set free-kernel pointer to origin of the new memory area.
 *      Set freecount to total kernel instances available
 *
 * Dependency:
 *
 * Uses:    MEM_alloc - Allocate memory
 *
 *************************************************************************/

 V_Bool
wi_init(VISE vise, V_ULong kicount)
 {
 vise->kiPool.freekernel = (ki) MEM_alloc(vise->memCfg, KI_SIZE, kicount, FAST_MEM, "wi_init ki");
 if (vise->kiPool.freekernel == NULL && kicount)
   return(FALSE);
 vise->kiPool.freecount = kicount;
 vise->kiPool.kernelcount = 0;
 return(TRUE);
 }

/**************************************************************************
 *
 * Name:    wi_kernelcount - Count of kernel instances allocated
 *
 * Calling Sequence:
 *
 *      kicount = wi_kernelcount(vise);
 *
 *      V_ULong kicount; -- The number of kernel instances
 *                      VISE    vise;    -- A VISE descriptor
 *
 * Returns:  Count of kernel instances allocated
 *
 * Requires:  The grammar must have been activated since being
 *      initialized
 *
 * Modifies:  Nothing
 *
 * Effect:   Returns a count of the kernel instances allocated
 *
 * Implementation:
 *
 * Uses:
 *
 *************************************************************************/

 V_ULong
wi_kernelcount(VISE vise)
 {
 return(vise->kiPool.kernelcount);
 }

/**************************************************************************
 *
 * Name:    wi_replicate - Make a word instance from a model
 *
 * Calling Sequence:
 *
 *      outcome = wi_replicate(vise, wordinst, model);
 *
 *      V_Int   outcome; -- Returned status
 *                      VISE    vise;       -- A VISE descriptor
 *      wi  wordinst; -- A word instance
 *      wm  model;  -- A word model
 *
 * Returns:   VISESUCCESS if successful, another VISE status code
 *      otherwise.
 *
 * Requires:  wi cluster initialization.
 *
 * Modifies:  wordinst
 *
 * Effect:   The given word instance is built up with kernel instances.
 *      From the word model, each kernel model builds a kernel
 *      instance.
 *
 * Implementation:
 *
 *      Set count to the number of kernel models in the word
 *      model
 *
 *      If there are enough available kernel instances
 *
 *      Decrement the count of free kernels
 *
 *      Set the active kernel count to 0
 *      Set first-kernel pointer to first free kernel instance
 *      Set last-kernel pointer to first free kernel instance
 *      Set active-region pointer to NULL
 *
 *      Make first kernel instance like first kernel model in
 *      word model
 *      Initialize the first kernel instance's knode scores
 *
 *      For all remaining kernel instances in the word model
 *       Get a kernel instance
 *       Increment the last kernel instance pointer
 *       Put the kernel model in the kernel instance
 *
 *      Set last kernel instance pointer to last-used kernel
 *      instance
 *
 *      Else
 *       Set count to 0
 *
 *      Return count
 *
 * Dependency:
 *
 * Uses:    MEM_alloc - Allocate memory
 *      wm_numkernels - Get the number of kernels in a word model
 *      wm_firstkernel - Get first kernel model in word model
 *      km_nextkernel - Get the next kernel in a kernel model list
 *      ki_putmodel - Associate kernel model with kernel instance
 *      ki_knodeinit - Initialize a kernel instance knodes's scores
 *
 *************************************************************************/

 V_Int
wi_replicate(VISE vise, wi wordinst, wm wordmodel)
 {
 V_Int numKernels;
 km   kernelModel;
 ki   kernelInstance;

  /* If there's room left in the normally allocated ki pool */
 if ((V_ULong) (numKernels = wm_numkernels(wordmodel)) <= vise->kiPool.freecount) {
   /* Allocate kernel instance objects from the pool */
  kernelInstance = vise->kiPool.freekernel;
  vise->kiPool.freekernel += numKernels;
  vise->kiPool.freecount -= numKernels;
 } else {
   /* Else try to allocate them from free memory */
  kernelInstance = (ki) MEM_alloc(vise->memCfg, KI_SIZE, numKernels, FAST_MEM, "wi_replicate ki");
  if (kernelInstance == NULL) return(CANTALLOCATEKI);
 }
    vise->kiPool.kernelcount += numKernels;

  /* Proceed with replication */
 wordinst->wimodel = wordmodel;
 wordinst->wiactivecount = 0;
 wordinst->wifirstkernel = kernelInstance;
 wordinst->wilastkernel = kernelInstance + numKernels - 1;
 wordinst->wiactiveregion  = NULL;

  /* Loop through the kernel models in the word model */
 for ( kernelModel = wm_firstkernel(wordmodel); kernelModel != NULL; kernelModel = km_nextkernel(kernelModel)) {

   /* Associate a kernel instance with each kernel model */
  if (!ki_putmodel(vise, kernelInstance, kernelModel))
   return(CANTALLOCATEKND);

   /* Initialize the ki nodes and get the next kernel instance */
  ki_knodeinit(kernelInstance++);
 }

  /* Report success */
 return(VISESUCCESS);
 }


/**************************************************************************
 *
 * Name:    wi_wakekernel - Extend active region of word
 *
 * Calling Sequence:
 *
 *      kerninst = wi_wakekernel(wordinst);
 *
 *      wi wordinst; -- A word instance
 *      ki kerninst; -- a kernel instance
 *
 * Returns:   The newly activated kernel instance
 *
 * Requires:  wordinst should first be initialized with wi_replicate
 *
 * Modifies:  wordinst
 *
 * Effect:   Extends the active region of the  word  by  one  kernel
 *      instance. If the word is completely active, then does
 *      nothing.
 *
 *      The kernel nodes of the activated kernel instance  are
 *      initialized.
 *
 * Implementation:
 *
 *      If word is inactive
 *       set active region to first kernel in word
 *      else If active-region pointer < last-kernel pointer
 *       increment active-region pointer to next kernel
 *      instance
 *      else
 *       return NULL
 *
 *      initialize activated kernel's nodes's scores
 *      increment the count of the word's active kernels
 *      return activated kernel
 *
 * Dependency:
 *
 * Uses:    ki_knodesinit - initialize knodes in kernel instance
 *
 **************************************************************************/

 ki
wi_wakekernel(wi wordinst)
 {
 ki nextkernel;

 if (wordinst->wiactivecount == 0)
  nextkernel = wordinst->wiactiveregion = wordinst->wifirstkernel;
 else if ( wordinst->wiactiveregion < wordinst->wilastkernel )
  nextkernel =  ++wordinst->wiactiveregion;
 else
  return(NULL);

  ki_knodeinit(nextkernel);
  wordinst->wiactivecount++;
  return(nextkernel);
 }


/*************************************************************************
 *
 * Name:     wi_napkernel - Reduce active region by one kernel
 *
 * Calling Sequence:
 *
 *      wi_napkernel(wordinst);
 *
 *      wi wordinst; -- A word instance object
 *
 * Returns:
 *
 * Requires:  wordinst should first be initializd with wi_replicate.
 *
 * Modifies:  wordinst
 *
 * Effect:   Reduces the active region of the word by  one kernel.
 *      The  last  kernel  in  the  active region  is rendered
 *      inactive.  If no kernels are active, this function  has
 *      no effect.
 *
 * Implementation:
 *
 *      If active kernel count > 0
 *       decrement active region pointer
 *       decrement active kernel count
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/
#ifdef NOMACROS

 V_Void
wi_napkernel(wi wordinst)
 {
 if (wordinst->wiactivecount > 0)
  {
  (wordinst->wiactiveregion)--;
  (wordinst->wiactivecount)--;
  }
 }


#endif
/************************************************************************
 *
 * Name:    wi_deactivate - Deactivate word instance
 *
 * Calling Sequence:
 *
 *      wi_deactivate(wordinst);
 *
 *      wi wordinst; -- A word instance object.
 *
 * Returns:
 *
 * Requires:  the  wordinst should first   be  initialized with
 *      wi_replicate
 *
 * Modifies:  wordinst
 *
 * Effect:   wi_deactivate turns off all the kernels in the word.
 *      after wi_deactivate is invoked, wi_inactive returns TRUE
 *
 * Implementation:
 *
 *      Set active region pointer to NULL
 *      Set count of active kernels in the word to 0
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/
#ifdef NOMACROS

 V_Void
wi_deactivate(wi wordinst)
 {
 wordinst->wiactiveregion = NULL;
 wordinst->wiactivecount  = 0;
 }


#endif
/*************************************************************************
 *
 * Name:    wi_lastkernel - Get last active kernel in word
 *
 * Calling Sequence:
 *
 *      krninst = wi_lastkernel(wordinst);
 *
 *      ki kerninst;  -- The last active kernel instance
 *      wi wordinst;  -- A word instance
 *
 * Returns:   The last active  kernel  instance  in the  given word
 *      instance
 *
 * Requires:  The word instance must first have been initialized with
 *      wi_replicate.
 *
 * Modifies:  wordinst
 *
 * Effect:   Returns the last active kernel  instance in  the word
 *      instance   object. If  there  are  no  active  kernel
 *      instances in the word instance, returns  NULL.  Resets
 *      the  word instance current kernel pointer such that the
 *      next call to wi_previouskernel returns the next to last
 *      kernel instance in the word.
 *
 * Implementation:
 *
 *      set currentkernel pointer to activeregion-1
 *      return activeregion
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
 ki
wi_lastkernel(wi wordinst)
 {
 if(wordinst->wiactivecount != 0)
  {
  return(wordinst->wiactiveregion);
  }
 else
  return(NULL);
 }
#endif

/*************************************************************************
 *
 * Name:    wi_previouskernel - Get previous kernel in word
 *
 * Calling Sequence:
 *
 *      kerninst = wi_previouskernel(wordinst);
 *
 *      ki kerninst; -- The next kernel instance (moving from
 *      last active to first)
 *      wi wordinst; -- A word instance
 *
 * Returns:   The kernel instance in the word instance  previous  to
 *      the  kernel instance which was last accessed via either
 *      wi_lastkernel or wi_previouskernel operations. If  all
 *      kernel instances  in the  word  instance have been
 *      accessed NULL is returned.
 *
 * Requires:  word instance   must first   be  initialized with
 *      wi_replicate operation.
 *
 * Modifies:  wordinst
 *
 * Effect:   Returns the current kernel instance and sets the kernel
 *      instance  iterator so that the next (going from last to
 *      first) kernel instance will be returned on a subsequent
 *      call to ki_previouskernel.
 *
 * Implementation:
 *
 *      Save pointer to current-kernel
 *      Set current kernel to previous kernel instance in word
 *      Return saved pointer
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NEVEREVERUSED
 ki
wi_previouskernel(wi wordinst)
 {
 ki save;

 if( wordinst->wifirstkernel <= wordinst->wicurrentkernel )
  {
  save = wordinst->wicurrentkernel;
  wordinst->wicurrentkernel--;
  return(save);
  }
 else
  return(NULL);
 }
#endif

/*************************************************************************
 *
 * Name:    wi_numactive - Number of active kernels in word
 *
 * Calling Sequence:
 *
 *      actcount = wi_numactive(wordinst);
 *
 *      V_Int actcount;   -- Active kernel count.
 *      wi  wordinst;   -- A word instance object.
 *
 * Returns:   The number of kernels in the word which  are  currently
 *      active.
 *
 * Requires:  The word should first be initialized with wi_replicate()
 *
 * Modifies:
 *
 * Effect:   Returns the number of kernels in  the word which  are
 *      currently active.
 *
 * Implementation:
 *
 *      Return count of active kernels in the word
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/
#ifdef NOMACROS

 V_Int
wi_numactive(wi wordinst)
 {
 return(wordinst->wiactivecount);
 }

#endif
/**************************************************************************
 *
 * Name:    wi_active - Tell if whole word is active
 *
 * Calling Sequence:
 *
 *      isactive = wi_active(wordinst);
 *
 *      V_Bool isactive; -- Boolean result indicating if
 *              wordinst is active.
 *      wi     wordinst; -- A word instance object.
 *
 * Returns:   TRUE if the whole word is active, else FALSE.
 *
 * Requires:  The word should first be initialized with wi_replicate()
 *
 * Modifies:
 *
 * Effect:   Returns TRUE iff all kernels in the word are contained
 *      in the active region.  Otherwise, returns FALSE.
 *
 * Implementation:
 *
 *      Return(lastkernel == activeregion)
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/
#ifdef NOMACROS


 V_Bool
wi_active(wi wordinst)
 {
 return(wordinst->wilastkernel == wordinst->wiactiveregion);
 }


#endif
/**************************************************************************
 *
 * Name:    wi_model - Get the word model for this word instance
 *
 * Calling Sequence:
 *
 *      model = wi_model(wordinst);
 *
 *      wm ident;   -- A word model
 *      wi wordinst;  -- A word instance object
 *
 * Returns:   The  word model for the given word instance object.
 *
 * Requires:  wi_model should not be invoked before wi_replicate has
 *      been invoked.
 *
 * Modifies:
 *
 * Effect:   Returns the word model associated with the word instance.
 *
 * Implementation:
 *
 *      Return the word model field of given word instance
 *
 * Dependency:
 *
 * Uses:
 *
 **************************************************************************/
#ifdef NOMACROS

 wm
wi_model(wi wordinst)
 {
 return(wordinst->wimodel);
 }

#endif
/**************************************************************************
 *
 * Name:    wi_inactive - Tell if whole word is inactive
 *
 * Calling Sequence:
 *
 *      isinactive = wi_inactive(wordinst)
 *
 *      V_Bool isinactive; -- Boolean result indicating all
 *               kernels in the word are inactive.
 *      wi     wordinst;   -- A word instance object
 *
 * Returns:   A boolean result indicating whether the whole word is
 *      inactive.
 *
 * Requires:  The word should first be initialized with the
 *      wi_replicate function.
 *
 * Modifies:
 *
 * Effect:   Returns TRUE iff all kernels in the word are  contained
 *      in the active region.  Otherwise, returns FALSE.  
 *      NOTE: this is NOT equivalent to (!wi_active(wordinst)).
 *
 * Implementation:
 *
 *      if active kernel count is 0
 *       return TRUE
 *      else
 *       return FALSE
 *
 * Dependency:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS
 V_Bool
wi_inactive(wi wordinst)
 {
 return(wordinst->wiactivecount == 0);
 }
#endif

/*************************************************************************
 *
 * Name:    wi_status - Get status of word instance
 *
 * Calling Sequence:
 *
 *      status = wi_status(word);
 *
 *      V_Int status;  -- status of word
 *      wi  word;  -- word instance object
 *
 * Returns:   The status of the given word instance object.  This can
 *      be either WILDCARD or TEMPLATES.
 *
 *
 * Requires:  word must be properly  replicated from a valid word
 *      model with status.
 *
 * Modifies:
 *
 * Effect:   This function retrieves a word instance's status.
 *
 * Implementation:
 *
 *      Return status field of the word instance
 *
 * Dependency:
 *
 * Uses:
 *
 ************************************************************************/
#ifdef NOMACROS

 V_Int
wi_status(wi word)
 {
 return(wm_status(word->wimodel));
 }
#endif
