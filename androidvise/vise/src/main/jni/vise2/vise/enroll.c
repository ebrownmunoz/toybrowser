#include "pthr.h"
#include "vbx.h"
#include "vise.h"
#include "v_vise.h"
#include <stdlib.h> /* Needed to get div() and the div_t typedef */
#include "v_div.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_voc.h"
#include "v_wm.h"
#include "v_mem.h"

/* #define LOC_DEBUG 1 */

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_VERBOSE            TRUE
#else
#define VBX_DEBUG(P)
#define VBX_VERBOSE            FALSE
#endif

#define ENROLL_MAXUTTERANCES     100       /* Maximum allowable number of sample utterances */
#define ENROLL_MAXKERNELSINWORD  42        /* Maximum allowable number of kernels in a word (MUST equal UPAT_MAX_KERNELS in vfile.h) */
#define ENROLL_MINKERNELSINWORD  2         /* Distinguishes real words from special words like wc and sil */
#define ENROLL_MAXDWELL          20        /* Maximum allowable number of frames to be absorbed by kernel - 256ms */
#define ENROLL_INFINITY          VLONGMAX

#define ENROLL_MIN(x,y) (((x) < (y)) ? (x) : (y))
#define ENROLL_MAX(x,y) (((x) > (y)) ? (x) : (y))

 /* An Utterance Descriptor */
typedef struct utt_s {
  V_Int     duration;                                     /* The number of frames in the utterance */
  V_Int     wordDuration;                                 /* The number of frames in the word without silences */
  V_Int     numKernels;                                   /* Number of kernels in model based on the utt */
  V_Int     minDwells[ENROLL_MAXKERNELSINWORD];           /* Final minDwells for word model built on base of this utt */
  V_Int     maxDwells[ENROLL_MAXKERNELSINWORD];           /* Final maxDwells for word model built on base of this utt */
  V_SN      startSN;
  V_SN      wordStartSN;
  V_SN      wordEndSN;
  V_SN      endSN;
  V_Byte  * jin;                                          /* The jin data for the utterance */
  V_Uns     temSize;
  V_Byte  * template;                                     /* Template based on the utterance */
  V_Uns     dwellSize;
  V_Int   * dwells;                                       /* Dwells of all utterances segmented with template based on the utterance */
} utt_t, * UTT;

 /* Local Prototypes */
static V_Long  ENROLL_cost(V_Byte * x, V_Byte * y);
static V_Long  ENROLL_segmentUtterance(UTT utterance, V_Int numKernels, V_Byte * template, V_Int * dwells, V_Int * trace);
static V_Void  ENROLL_buildTemplate(V_Byte * templates, V_Int * dwells, V_Int numKernels, UTT utterances, V_Int * listUttIdx, V_Uns numUtts);
static V_Void  ENROLL_firstSet(V_Int * set, V_Uns kClusters);
static V_Bool  ENROLL_nextSet(V_Int * set, V_Uns kClusters, V_Uns nObjects);
static V_Void  ENROLL_doDwells(UTT butt, V_Uns numUtts);

/**************************************************************************
 *
 * Name:      ENROLLWORD - Enroll multiple models for one word
 *
 * Call:      ENROLLWORD(vise);
 *
 *            VISE vise; -- a VISE descriptor
 *
 * Returns:   Nothing
 *
 * Requires:  The function WARMSTART should have been called before this one.
 *            Word utterances must be in jin buffer.
 *            The format of the message should be as follows:
 *
 *              Contents                                     Type
 *
 *              framesPerKernel                              V_Uns
 *              maxKernelsInWord                             V_Uns
 *              minKernelsInWord                             V_Uns
 *              wordClass                                    V_Uns
 *              numWordModels                                V_Uns
 *                numWordModels of wordIds                   V_Uns
 *              Number of sample utterances in frame queue   V_Uns
 *              For each sample utterance:
 *                Starting sequence number of utterance      V_SN
 *                Starting sequence number of word           V_SN
 *                Ending sequence number of word             V_SN
 *                Ending sequence number of utterance        V_SN
 *
 * Modifies:  The specified word's templates
 *
 * Effect:    Enrolls the specified word using the regions of the frame
 *            queue delimited by the specified starting and ending SNs.
 *
 *************************************************************************/

  V_Err
ENROLLWORD(VISE vise)
{
  V_Uns    framesPerKernel;
  V_Uns    maxKernels;
  V_Uns    minKernels;
  V_Uns    wordClass;
  V_Uns    nuMods;
  V_Uns  * wids;                        /* word ids array */
  V_Uns    wid;                         /* word id */
  V_Uns    numUtts;                     /* number of utterances to process */
  wm       word;                        /* word model */
  km       kernel;                      /* current kernel to process */
  kt       ktrainer;                    /* Kernel training object */
  V_Int    maxNumKernels;
  V_Int    minWordDuration;
  V_Int    maxDuration;                 /* Max length jin data */
  V_Int    frameCnt;                    /* Frame counter */
  V_Int    denominator;                 /* denominator in division */
  V_Int    duration;                    /* Duration of word without silence */
  V_Int    remainder;                   /* remainder in division */
  div_t    dv, div();
  UTT      utterances, u, butt;
  V_Int    i, j, k, m, uttIdx;
  V_SN     sn;
  V_Byte * jinPtr;
  V_Long   accumulator[NUMCOMPONENTS];
  V_Long * distut;                      /* Matrix distut[i*numUtts + k] - distance from temlate based on utt[i] to utt[k] */
  V_Long   newTotalScore, oldTotalScore, globalScore, minScore, score;
  V_Byte   silence[NUMCOMPONENTS];
  V_Byte * temPtr;                      /* Template pointer */
  V_Int  * sdwells;                     /* Dwell pointer */
  V_Int  * traceBuf;                    /* Trace back dwells buffer */
  V_Int  * optSet, * curSet;            /* Sets of base utt indexes - optimal and current */
  V_Int    clust[ENROLL_MAXUTTERANCES]; /* List of utt indexes gravitating to some base utterance */
  V_Int    clustNum;                    /* Number of utts in cluster */
  V_Uns    senderId = MSG_correspondentId(vise->incoming);
  V_Err    vise_status, status;

  /* Get and check the data */
  if (
      !MSG_readUns(vise->incoming, &framesPerKernel) ||
      !MSG_readUns(vise->incoming, &maxKernels) ||
      !MSG_readUns(vise->incoming, &minKernels) ||
      !MSG_readUns(vise->incoming, &wordClass) ||
      !MSG_readUns(vise->incoming, &nuMods)
     )
    VISE_EXIT(MESSAGETOOSHORT);

  VBX_DEBUG(VBX_print("  VISE::ENROLLWORD: FPK = %d; maxK = %d; minK = %d; wClass = %d; nMods = %d; ", framesPerKernel, maxKernels, minKernels, wordClass, nuMods));

  if (maxKernels > ENROLL_MAXKERNELSINWORD)
    VISE_EXIT(TOOMANYKERNELS);
  if (minKernels < ENROLL_MINKERNELSINWORD)
    VISE_EXIT(TOOFEWKERNELS);
  if (minKernels > maxKernels)
    VISE_EXIT(FATALINCONSISTENCY);

  if (nuMods == 0 || nuMods > ENROLL_MAXUTTERANCES)
    VISE_EXIT(BADWORDCOUNT);

  if ((wids = (V_Uns *) MEM_alloc(vise->memCfg, sizeof(V_Uns), nuMods, FAST_MEM, "ENROLLWORD WIDS")) == NULL)
    VISE_EXIT(OUTOFMEMORY);

  for (i = 0; i < nuMods; i++)
    if (!MSG_readUns(vise->incoming, &wids[i]))
      VISE_EXIT(MESSAGETOOSHORT);

  if (!MSG_readUns(vise->incoming, &numUtts))
    VISE_EXIT(MESSAGETOOSHORT);

  VBX_DEBUG(VBX_print("nUtts = %d\n", numUtts));

  if (numUtts < nuMods || numUtts > ENROLL_MAXUTTERANCES)
    VISE_EXIT(BADWORDCOUNT);

   /* Allocate utterances */
  if ((utterances = (UTT) MEM_alloc(vise->memCfg, sizeof(utt_t), numUtts, FAST_MEM, "ENROLLWORD UTTS")) == NULL)
    VISE_EXIT(OUTOFMEMORY);

   /* Construct an utterance descriptor (UTT) for each utterance */
  duration = 0;
  maxDuration = 0;
  minWordDuration = MAXDWELL;

  for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++) {

    if (!MSG_readSN(vise->incoming, &u->startSN) ||
        !MSG_readSN(vise->incoming, &u->wordStartSN) ||
        !MSG_readSN(vise->incoming, &u->wordEndSN) ||
        !MSG_readSN(vise->incoming, &u->endSN))
      VISE_EXIT(MESSAGETOOSHORT);
    VBX_DEBUG(VBX_print("  VISE::ENROLLWORD: uttIdx %d startSN " QuadFmt(-) " wordStartSN " QuadFmt(-) " wordEndSN " QuadFmt(-) " endSN " QuadFmt(-) "\n",
                        uttIdx, u->startSN, u->wordStartSN, u->wordEndSN, u->endSN));

    if (u->wordStartSN <= u->startSN || u->wordEndSN >= u->endSN || u->wordStartSN >= u->wordEndSN)
      VISE_EXIT(CANTFINDWORD);

    if (!JINSRC_canread(vise->jinSource, u->startSN, u->endSN))
      VISE_EXIT(CANTREADFRAME);

    u->duration = (V_Int) JINSRC_numframes(vise->jinSource, &u->startSN, &u->endSN);
    u->wordDuration = (V_Int) JINSRC_numframes(vise->jinSource, &u->wordStartSN, &u->wordEndSN);
    VBX_DEBUG(VBX_print("  VISE::ENROLLWORD: duration %d, word duration %d\n", u->duration, u->wordDuration));

    if (u->wordDuration > MAXDWELL)
      VISE_EXIT(WORDDURATIONTOOLONG);

    if (u->wordDuration < minKernels)
      VISE_EXIT(WORDDURATIONTOOSHORT);

     /* Collect statistics */
    duration += u->wordDuration;
    if (u->duration > maxDuration)
      maxDuration = u->duration;
    if (u->wordDuration < minWordDuration)
      minWordDuration = u->wordDuration;

     /* Allocate jin array */
    if ((u->jin = (V_Byte *)MEM_alloc(vise->memCfg, NUMCOMPONENTS, u->duration, FAST_MEM, "ENROLLWORD JIN")) == NULL)
      VISE_EXIT(OUTOFMEMORY);

     /* Unpack jin data into the UTT's jin array */
    for (sn = u->startSN, jinPtr = u->jin; sn <= u->endSN; sn++, jinPtr += NUMCOMPONENTS)
      if (!JINSRC_read(vise->jinSource, &sn, (jin) jinPtr, &status))
        VISE_EXIT(status);
  }

   /* Find numKernels for each utterance, and maxNumKernels */
  if (nuMods == 1) {
     /* numKernels = HardLimit(averageDuration / framesPerKernel) for all utts and it is maxNumKernels */
    duration /= numUtts;
    maxNumKernels = duration / framesPerKernel;
    maxNumKernels = (maxNumKernels > maxKernels) ? maxKernels : maxNumKernels;
    maxNumKernels = (maxNumKernels < minKernels) ? minKernels : maxNumKernels;
    maxNumKernels = (maxNumKernels > minWordDuration) ? minWordDuration : maxNumKernels;
    for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++)
      u->numKernels = maxNumKernels;
  } else {
     /* numKernels = HardLimit(wordDuration / framesPerKernel) for each utt */
    maxNumKernels = minKernels;
    for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++) {
      u->numKernels = u->wordDuration / framesPerKernel;
      u->numKernels = (u->numKernels > maxKernels) ? maxKernels : u->numKernels;
      u->numKernels = (u->numKernels < minKernels) ? minKernels : u->numKernels;
      u->numKernels = (u->numKernels > minWordDuration) ? minWordDuration : u->numKernels;
      maxNumKernels = (maxNumKernels < u->numKernels) ? u->numKernels : maxNumKernels;
    }
  }

   /* Allocate template and dwells objects (one for each base utterance) */
  for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++) {
     /* A template object contains a jin vector for each kernel (including starting and ending silences */
    u->temSize = (u->numKernels + 2) * NUMCOMPONENTS;
    u->template = (V_Byte *) MEM_alloc(vise->memCfg, u->temSize, 1, FAST_MEM, "ENROLLWORD TEMS");
     /* A dwell object contains a dwell of each kernel for each utterance */
    u->dwellSize = (u->numKernels + 2) * numUtts;
    u->dwells = (V_Int *) MEM_alloc(vise->memCfg, u->dwellSize * sizeof(V_Int), 1, FAST_MEM, "ENROLLWORD DWELLS");
    if (!u->template || !u->dwells)
      VISE_EXIT(OUTOFMEMORY);
  }

   /* Allocate a traceback buffer for ENROLL_segmentUtterance() */
  if ((traceBuf = (V_Int *) MEM_alloc(vise->memCfg, (maxNumKernels + 2) * sizeof(V_Int), maxDuration, FAST_MEM, "ENROLLWORD TRACE")) == NULL)
    VISE_EXIT(OUTOFMEMORY);

   /* Allocate sets */
  if ((optSet = (V_Int *) MEM_alloc(vise->memCfg, nuMods * sizeof(V_Int), 2, FAST_MEM, "ENROLLWORD SETS")) == NULL)
    VISE_EXIT(OUTOFMEMORY);
  curSet = optSet + nuMods;

   /* List of utt indexes for ENROLL_buildTemplate consists of single utt referenced in the call */
  clust[0] = 0;

   /* First build initial base templates, segmenting the base utterance linearly, and then optimizing it */
  for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++) {

     /* Get the silence dwells */
    u->dwells[0] = (V_Int) JINSRC_numframes(vise->jinSource, &u->startSN, &u->wordStartSN) - 1;
    u->dwells[u->numKernels + 1] = (V_Int) JINSRC_numframes(vise->jinSource, &u->wordEndSN, &u->endSN) - 1;

     /* Other dwells */
    remainder = u->numKernels;
    denominator = u->numKernels << 1;

     /* Iterate over all nonsilence kernels in the word model */
    for (k = 1; k <= u->numKernels; k++) {
      dv = div((u->wordDuration << 1) + remainder, denominator);
      u->dwells[k] = (V_Int) dv.quot;
      remainder = dv.rem;
    }

#ifdef LOC_DEBUG
    VBX_print("  VISE::ENROLLWORD: Utt %d; NumKernels %d; Linear Dwells:", uttIdx, u->numKernels);
    for (k = 0; k < u->numKernels + 2; k++)
      VBX_print("%3d", u->dwells[k]);
    VBX_print("\n  Utterance:");
    for (i = 0, jinPtr = u->jin; i < u->duration; i++) {
      VBX_print("\n   %3d__", i);
      for (j = 0; j < NUMCOMPONENTS; j++)
        VBX_print("%3d ", *jinPtr++);
    }
    VBX_print("\n\n");
#endif

     /* E/M procedure on single utterance */
    oldTotalScore = ENROLL_INFINITY;
    while (TRUE) {

       /* Build a new template from the segmented utterance */
      ENROLL_buildTemplate(u->template, u->dwells, u->numKernels + 2, u, clust, 1);

       /* Segment the utterance against the template */
      newTotalScore = ENROLL_segmentUtterance(u, u->numKernels + 2, u->template, u->dwells, traceBuf);

       /* If this doesn't improve things, quit */
      if (newTotalScore >= oldTotalScore)
        break;

      oldTotalScore = newTotalScore;
    }

     /* Using a Euclidean cost function, the convergence must be exact */
    if (newTotalScore != oldTotalScore)
       /* Something has gone awry */
      SEVERE_BRA_ "  VISE::ENROLLWORD: convergence not exact on utt %d; (newTotalScore = %d, oldTotalScore = %d)", uttIdx, newTotalScore, oldTotalScore _KET;

#ifdef LOC_DEBUG
    VBX_print("  VISE::ENROLLWORD: Initial Dwells:");
    for (k = 0; k < u->numKernels + 2; k++)
      VBX_print("%3d", u->dwells[k]);
    VBX_print("\n  Initial Template:");
    for (i = 0, jinPtr = u->template; i < u->numKernels + 2; i++) {
      VBX_print("\n        ");
      for (j = 0; j < NUMCOMPONENTS; j++)
        VBX_print("%3d ", *jinPtr++);
    }
    VBX_print("\n\n");
#endif
  }

  if (nuMods == 1) {
     /* "Improve" each base template using E/M procedure on all utterances */
    globalScore = ENROLL_INFINITY;

     /* List of utt indexes for ENROLL_buildTemplate includes all of them */
    for (i = 0; i < numUtts; i++)
      clust[i] = i;

     /* For each base utterance ... */
    for (uttIdx = 0, butt = utterances; uttIdx < numUtts; uttIdx++, butt++) {

      oldTotalScore = ENROLL_INFINITY;
      while (TRUE) {

        newTotalScore = 0;

         /* Segment each sample utterance against the base template */
        for (j = 0, u = utterances, sdwells = butt->dwells; j < numUtts; j++, u++, sdwells += butt->numKernels + 2)
          newTotalScore += ENROLL_segmentUtterance(u, butt->numKernels + 2, butt->template, sdwells, traceBuf);

         /* If this doesn't improve things, quit */
        if (newTotalScore >= oldTotalScore)
          break;

        oldTotalScore = newTotalScore;

         /* Build a new base template from the segmented utterances */
        ENROLL_buildTemplate(butt->template, butt->dwells, butt->numKernels + 2, utterances, clust, numUtts);
      }

       /* Using a Euclidean cost function, the convergence must be exact */
      if (newTotalScore != oldTotalScore)
         /* Something has gone awry */
        SEVERE_BRA_ "  VISE::ENROLLWORD: convergence not exact for base utt %d; (newTotScore = %d, oldTotScore = %d)",uttIdx,newTotalScore,oldTotalScore _KET;

       /* Remember the score and index of the best base utterance */
      if (newTotalScore < globalScore) {
        globalScore = newTotalScore;
        optSet[0] = uttIdx;
      }
    }
#ifdef LOC_DEBUG
    VBX_print("  VISE::ENROLLWORD: And the Winner is Utterance %d!\n", optSet[0]);
#endif

     /* Compute min/maxDwells for best model */
    ENROLL_doDwells(utterances + optSet[0], numUtts);
  } else { /* Clustering */
     /* First make all initial templates have common flanking silences */
    frameCnt = 0;
    for (j = 0; j < NUMCOMPONENTS; j++)
      accumulator[j] = 0;

    for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++) {
      i = u->dwells[0];                 /* Number of silence frames at the beginning of utterance u */
      k = u->dwells[u->numKernels + 1]; /* Number of silence frames at the end of utterance u */
      jinPtr = u->template + u->temSize - NUMCOMPONENTS; /* Ending silence kernel vector */
      for (j = 0; j < NUMCOMPONENTS; j++)
        accumulator[j] += ((V_Long)u->template[j] * i + (V_Long)jinPtr[j] * k);
      frameCnt += (i + k);
    }

     /* Compute silence template */
    for (j = 0; j < NUMCOMPONENTS; j++)
      silence[j] = (V_Byte)rndiv32(accumulator[j], frameCnt);

     /* Put this vector in all base templates silence kernels */
    for (uttIdx = 0, u = utterances; uttIdx < numUtts; uttIdx++, u++) {
      memcpy(u->template, silence, (size_t)NUMCOMPONENTS);
      memcpy(u->template + u->temSize - NUMCOMPONENTS, silence, (size_t)NUMCOMPONENTS);
    }

     /* Allocate and compute matrix of distances */
    if ((distut = (V_Long *)MEM_alloc(vise->memCfg, numUtts * sizeof(V_Long), numUtts, FAST_MEM, "ENROLLWORD MATRIX")) == NULL)
      VISE_EXIT(OUTOFMEMORY);

    j = 0; /* j = i*numUtts + k */
    for (i = 0, butt = utterances; i < numUtts; i++, butt++)
      for (k = 0, u = utterances, sdwells = butt->dwells; k < numUtts; k++, u++, sdwells += butt->numKernels + 2)
        distut[j++] = ENROLL_segmentUtterance(u, butt->numKernels + 2, butt->template, sdwells, traceBuf);

     /* With exhaustive search get the best set of base models */
    globalScore = ENROLL_INFINITY;
    ENROLL_firstSet(curSet, nuMods);
    do {
       /* Using distance matrix evaluate how this set of models approximates all utts */
      newTotalScore = 0;
      for (uttIdx = 0; uttIdx < numUtts; uttIdx++) {
         /* Classify utt as belonging to one of the clusters with center in base utt from curSet */
        for (k = 0, minScore = ENROLL_INFINITY; k < nuMods; k++)
          if ((score = distut[numUtts * curSet[k] + uttIdx]) < minScore)
            minScore = score;
         /* Accumulate */
        newTotalScore += minScore;
      }
       /* If this set of models "improves", save it */
      if (newTotalScore < globalScore) {
        globalScore = newTotalScore;
        memcpy(optSet, curSet, nuMods * sizeof(V_Int));
      }
    } while (ENROLL_nextSet(curSet, nuMods, numUtts));

     /* "Improve" each cluster template using E/M procedure on utterances belonging to cluster */
    for (m = 0; m < nuMods; m++) {
       /* Cluster center attributes */
      uttIdx = optSet[m];
      butt = utterances + uttIdx;
       /* Cluster includes utts for which template based on butt renders the best score */
      clustNum = 0;
      for (i = 0; i < numUtts; i++) {
        score = distut[numUtts * uttIdx + i]; /* Distance from cluster center to utt number i */
         /* Check if other cluster renders better score */
        for (k = 0; k < nuMods; k++)
          if (distut[numUtts * optSet[k] + i] < score)
            break;

        if (k == nuMods) /* Then utt[i] belongs to butt cluster */
          clust[clustNum++] = i;
      }
#ifdef LOC_DEBUG
      VBX_print("Cluster: %d; Center Utterance Index: %d; Number of Utterances in Cluster: %d; Member Indexes:\n        ", m, uttIdx, clustNum);
      for (i = 0; i < clustNum; i++)
        VBX_print("%3d%s", clust[i], ((i+1)%16 == 0) ? "\n        " : " ");
      VBX_print("\n");
#endif
      if (clustNum > 0) {

        oldTotalScore = ENROLL_INFINITY;
        while (TRUE) {

          newTotalScore = 0;
           /* Segment each utterance in cluster against the base template */
          for (i = 0,  sdwells = butt->dwells; i < clustNum; i++, sdwells += butt->numKernels + 2)
            newTotalScore += ENROLL_segmentUtterance(utterances + clust[i], butt->numKernels + 2, butt->template, sdwells, traceBuf);

           /* If this doesn't improve things, quit */
          if (newTotalScore >= oldTotalScore)
            break;

          oldTotalScore = newTotalScore;

           /* Build a new base template from the segmented utterances in cluster */
          ENROLL_buildTemplate(butt->template, butt->dwells, butt->numKernels + 2, utterances, clust, clustNum);
        }
         /* Using a Euclidean cost function, the convergence must be exact */
        if (newTotalScore != oldTotalScore)
           /* Something has gone awry */
          SEVERE_BRA_ "  VISE::ENROLLWORD: convergence not exact for butt %d; (newTotScore = %d, oldTotScore = %d)",uttIdx,newTotalScore,oldTotalScore _KET;
      }

       /* Put dwells in model. If cluster is empty, they will be wide open */
      ENROLL_doDwells(butt, clustNum);
    }
  }

   /* Perform (ReplaceWord/DownloadWord + DloadTemplate) actions for each word model built */
  for (m = 0; m < nuMods; m++) {

    wid = wids[m];
    butt = utterances + optSet[m];
   
    VBX_DEBUG(VBX_print("  VISE::ENROLLWORD: wId = %d, numKernels = %d\n", wid, butt->numKernels));
#ifdef LOC_DEBUG
    VBX_print("  Dwell Limits: ");
    for (k = 0; k < butt->numKernels; k++)
      VBX_print("%d/%d ", butt->minDwells[k], butt->maxDwells[k]);
    VBX_print("\n  Final Template:");
    for (i = 0, jinPtr = butt->template + NUMCOMPONENTS; i < butt->numKernels; i++) {
      VBX_print("\n        ");
      for (j = 0; j < NUMCOMPONENTS; j++)
        VBX_print("%3d ", *jinPtr++);
    }
    VBX_print("\n\n");
#endif

     /* If there is a word model for the wid ... */
    if ((word = voc_getword(vise, (V_WId) wid))) {
       /* Deallocate the kernel models for the old word */
      wm_deallocate(vise, word);
       /* Allocate kernel models for the new word */
      if (!(wm_allocate(vise, word, (V_WId) wid, wordClass, butt->numKernels)))
        VISE_EXIT(CANTALLOCATEVOCWORD);
    } else {
       /* Allocate a word model for the new word */
      if (!(word = voc_createword(vise, (V_WId) wid, wordClass, butt->numKernels)))
        VISE_EXIT(CANTALLOCATEVOCWORD);
    }

     /* Pointer to template, minus flanking silences */
    temPtr = butt->template + NUMCOMPONENTS;
    for (k = 0, kernel = wm_firstkernel(word), ktrainer = wm_firstkt(word);
         kernel != NULL;
         kernel = km_nextkernel(kernel), temPtr += NUMCOMPONENTS, k++) {
       /* Put kernel dwells */
      km_putdwells(kernel, butt->minDwells[k], butt->maxDwells[k] - butt->minDwells[k]);
      if (ktrainer) {
        kt_putobsdwells(ktrainer, butt->maxDwells[k], butt->minDwells[k]);
        ktrainer = kt_nextkt(ktrainer);
      }
       /* Put kernel template */
      tem_write(km_template(kernel), (jin) temPtr);
    }
    /* Set status */
    wm_setstatus(word, TEMPLATES);
  }
  
   /* Happy to reach this point */
  vise_status = VISESUCCESS;

  /* Exception handling */
  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ENROLLWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}


/**************************************************************************
 *
 * Name:      ENROLL_cost -- Euclidian cost function
 *
 * Returns:   The Euclidian distance between two jin vectors
 *
 * Effect:    Returns the Euclidian distance between two jin vectors
 *
 *************************************************************************/

  static V_Long
ENROLL_cost(V_Byte * x, V_Byte * y)
{
  V_Int   i;
  V_Long  sum, dif;

  for (i = 0, sum = 0; i < NUMCOMPONENTS; i++) {
    dif = (V_Long) x[i] - (V_Long) y[i];
    sum += dif * dif;
  }

  return(sum);
}


/**************************************************************************
 *
 * Name:      ENROLL_segmentUtterance
 *
 * Returns:   The distance between the template and the segmented utterance
 *
 * Effect:    Given an utterance descriptor "utterance" and a template with
 *            "numKernels" constituent kernel templates in "template", this
 *            function reports the optimal segmentation in "dwells" and
 *            returns the distance between the template and the segmented
 *            utterance.  The workspace "trace" is allocated externally and
 *            passed to the function to avoid having to allocate and free
 *            it in every call.
 *
 *************************************************************************/

  static V_Long
ENROLL_segmentUtterance(UTT utterance, V_Int numKernels, V_Byte * template, V_Int * dwells, V_Int * trace)
{
  V_Byte * jinPtr = &utterance->jin[0];
  V_Int    duration = utterance->duration;
  V_Long   fn[ENROLL_MAXKERNELSINWORD + 6];
  V_Byte * kernelTem;
  V_Int    i;
  V_Int    k, kmin, kmax;  /* kernel indices */

   /* Initialize process */
  for (k = 0; k <= numKernels; k++)
    fn[k] = ENROLL_INFINITY;

  fn[1] = ENROLL_cost(jinPtr, template); /* fn[0] is always infinite */
  *trace = 1;
  jinPtr += NUMCOMPONENTS;

  for (i = 2; i <= duration; i++, jinPtr += NUMCOMPONENTS) {
    trace += numKernels;
    kmin = ENROLL_MAX(1, i + numKernels - duration);
    kmax = ENROLL_MIN(numKernels, i);
    kernelTem = template + (kmax - 1) * NUMCOMPONENTS;
     /* From the last active kernel to the first one */
    for (k = kmax; k >= kmin; --k, kernelTem -= NUMCOMPONENTS)
      if (fn[k] < fn[k - 1]) {
        fn[k] += ENROLL_cost(jinPtr, kernelTem);
        trace[k - 1] = trace[k - 1 - numKernels] + 1;
      } else {
        fn[k] = fn[k - 1] + ENROLL_cost(jinPtr, kernelTem);
        trace[k - 1] = 1;
      }
  }

   /* Trace back */
  for (k = numKernels; k >= 1; --k) {
    dwells[k - 1] = trace[k - 1];
    trace -= dwells[k - 1] * numKernels;
  }

  return(fn[numKernels]);
}


/**************************************************************************
 *
 * Name:      ENROLL_buildTemplate
 *
 * Returns:   Nothing
 *
 * Effect:    Given a set of "numUtts" utterances in "utterances" and their
 *            segmentations into "numKernels" kernels with dwells given in
 *            "dwells", this function builds a template and returns it in
 *            "template"
 *
 *************************************************************************/

  static V_Void
ENROLL_buildTemplate(V_Byte * template, V_Int * dwells, V_Int numKernels, UTT utterances, V_Int * listUttIdx, V_Uns numUtts)
{
  V_Byte * starts[ENROLL_MAXUTTERANCES];
  V_Int  * dwell;
  V_Long   accumulator[NUMCOMPONENTS];
  V_Int    totalFrameCnt;
  V_Int    frameCnt;
  V_Int    componentIdx;
  V_Int    kernelIdx;
  V_Int    uttIdx;

   /* Find the starts of all the utterances included in the list */
  for (uttIdx = 0; uttIdx < numUtts; uttIdx++)
    starts[uttIdx] = &(utterances[listUttIdx[uttIdx]].jin[0]);

   /* For each kernel ... */
  for (kernelIdx = 0; kernelIdx < numKernels; kernelIdx++) {

     /* Zero the accumulator */
    for (componentIdx = 0; componentIdx < NUMCOMPONENTS; componentIdx++)
      accumulator[componentIdx] = 0;

     /* For each utterance ... */
    totalFrameCnt = 0;
    for (uttIdx = 0, dwell = dwells; uttIdx < numUtts; uttIdx++, dwell += numKernels) {
       /* Accumulate each frame from the kernel segment */
      for (frameCnt = 0; frameCnt < dwell[kernelIdx]; frameCnt++)
        for (componentIdx = 0; componentIdx < NUMCOMPONENTS; componentIdx++)
          accumulator[componentIdx] += *starts[uttIdx]++;
       /* Accumulate the number of frames absorbed */
      totalFrameCnt += dwell[kernelIdx];
    }

     /* Calculate the average of all the frames accumulated */
    for (componentIdx = 0; componentIdx < NUMCOMPONENTS; componentIdx++)
      *template++ = (V_Byte) rndiv32(accumulator[componentIdx], totalFrameCnt);
  }
}

/**************************************************************************
 *
 * Name:      ENROLL_firstSet
 *
 * Returns:   Nothing
 *
 * Effect:    Puts in set, which must be at least kClusters long, the first
 *            combination of indexes, i. e. 0, 1, 2, ...,  kClusters - 1.
 *
 *************************************************************************/

static V_Void  ENROLL_firstSet(V_Int * set, V_Uns kClusters)
{
  V_Int i;

  for (i = 0; i < kClusters; i++)
    set[i] = i;
}

/**************************************************************************
 *
 * Name:      ENROLL_nextSet
 *
 * Returns:   TRUE if generated next valid combination of indexes,
 *            FALSE otherwise.
 *
 * Effect:    Given a valid combination of 0-based Object indexes in array,
 *            puts there next valid combination if any.
 *            
 * Requires:  kClusters <= nObjects; if i < j, set[i] < set[j];
 *            set[i] < nObjects for i = 0, 1, ..., kClusters - 1.
 *
 *************************************************************************/

static V_Bool  ENROLL_nextSet(V_Int * set, V_Uns kClusters, V_Uns nObjects)
{
  V_Int maxContent = nObjects - 1;
  V_Int maxPos = kClusters -1;
  V_Int i, j;

    /* Find min position in which index can be incremented */
  for (i = 0; i < maxPos; i++)
    if (set[i] + 1 < set[i + 1])
      break;

  if (i == maxPos && set[i] == maxContent)
    return FALSE; /* No more valid combinations */

  set[i]++;

  /* Reset all lower position indexes to lowest allowable content */
  for (j = 0; j < i; j++)
    set[j] = j;

  return TRUE;
}

/**************************************************************************
 *
 * Name:      ENROLL_doDwells
 *
 * Returns:   Nothing
 *
 * Effect:    For specified butt computes min/maxDwells,
 *            using first nutts (number of utts in cluster) segmentation
 *            results in butt->dwells object. if (nutts == 0) put wide open 
 *            dwells
 *************************************************************************/

static V_Void  ENROLL_doDwells(UTT butt, V_Uns nutts)
{
  V_Int    maxDwell = ENROLL_MIN(ENROLL_MAXDWELL, MAXDWELL / butt->numKernels);
  V_Int  * dw;
  V_Int  * minD = butt->minDwells;
  V_Int  * maxD = butt->maxDwells;
  V_Int    i, k;

  if (0 == nutts)
    for (k = 0; k < butt->numKernels; k++) {
      minD[k] = 1;
      maxD[k] = maxDwell;
    }
  else {
     /* Initialize */
    for (k = 0; k < butt->numKernels; k++) {
      minD[k] = MAXDWELL;
      maxD[k] = 0;
    }

    for (i = 0, dw = butt->dwells; i < nutts; i++, dw += butt->numKernels + 2)
      for (k = 0; k < butt->numKernels; k++) {
        if (dw[k + 1] < minD[k])
          minD[k] = dw[k + 1];
        if (dw[k + 1] > maxD[k])
          maxD[k] = dw[k + 1];
      }

    for (k = 0; k < butt->numKernels; k++) {
      minD[k] = ENROLL_MAX(1, (minD[k]) / 2);
      maxD[k] = ENROLL_MIN(maxDwell, (2 * maxD[k]));
      if (minD[k] > maxD[k])
        minD[k] = maxD[k];
    }
  }
}
