#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_tbf.h"
#include "v_tbn.h"

/**************************************************************************
 *
 *	Name: 			tbn -- The Traceback Node Cluster
 *
 *	Description:	The traceback node cluster implements the abstract
 *						traceback node objects which populate the traceback
 *						frame.  A traceback node in a traceback frame represents
 *						a grammar node which is reached via a finite cost path
 *						at the creation time of the traceback frame containing
 *						it.  It contains a traceback path element for each
 *						finite path terminating on that grammar node at that
 *						time, up to a specified maximum number of alternative
 *						paths.
 *
 *						Each traceback node contains a list of the traceback
 *						path elements leading to it, a field specifying how
 *						many paths are on this list, and a link to the traceback
 *						frame containing it.  In addition, each traceback node
 *						is linked to the following traceback node in the frame,
 *						in the order in which gramnodes() inserts them into the
 *						frame's node list.
 *
 *	Operations:
 *
 *						tbn_init(vise); -- Initialize  the  traceback	node cluster.
 *
 *						tbn_alloc(vise); -- Allocate a traceback node.
 *
 *						tbn_free(vise, node); -- Deallocate a traceback node.
 *
 *					    tbn_used(vise); -- Return the number of tbns currently
 *							in use.
 *
 *						tbn_setinfo(node, bestpath, frame, pathcount); -
 *							Initialize a traceback node with a traceback frame,
 *							cost, word identification number, and parent node.
 *
 *						tbn_info(node, &bestpath, &frame, &pathcount);
 *							Get trace back node characterization information
 *
 *					    tbn_setsuccessor(node, successor); --	Make successor
 *							the successor to node.
 *
 *					    tbn_successor(node); -- Get the successor of a
 *							traceback node.
 *
 *						tbn_setbestpath(node); -- Set the traceback path
 *							element terminating the best path to a traceback node.
 *
 *						tbn_bestpath(node); -- Get the traceback path element
 *							terminating the best path to a traceback node.
 *
 *					    tbn_setframe(node, frame) -- Set the traceback frame
 *							of a traceback path element.
 *
 *						tbn_frame(node); -- Get frame containing a given
 *							traceback node.
 *
 *						tbn_setpathcount(node); -- Set the number of paths to
 *							a traceback node.
 *
 *						tbn_pathcount(node); -- Get number of paths to
 *							a traceback node.
 *
 *					    tbn_incpathcount(node); -- Increment path count
 *							of a traceback node.
 *
 *					    tbn_decpathcount(node); -- Decrement path count
 *							of a traceback node.
 *
 *						tbn_globalmin(node); -- Get globalmin of the traceback
 *							frame containing this traceback node.
 *
 *						tbn_time(node); -- Get creation time of a traceback	node.
 *
 *						tbn_sn(node); -- Get sequence number of a traceback	node.
 *
 *                      VISE       vise; -- a VISE descriptor
 *						tbn	 	   node; -- a traceback node
 *						tbn	  successor; -- a traceback node
 *						tbf		  frame; -- a traceback frame
 *						tbp	   bestpath; -- a traceback path element
 *						V_Int pathcount; -- count of paths leading to a node
 *
 * Computer:		TMS320C30
 *
 * Unit Test:		tbnt.ct
 *
 *	$Log:   C:\USR\SRC\VISE\C\VCS\TBN.C_V  $
 * 
 *    Rev 3.8   20 Mar 1992 13:00:54   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.7   29 Jan 1992 12:58:46   DCV
 * Changed basic types
 * 
 *    Rev 3.6   23 Aug 1991 09:48:10   DCV
 * tbn_alloc() now increments tbnused.
 * 
 *    Rev 3.5   14 Aug 1991 16:39:58   DCV
 * Made TRACEBACKNODEALLOC a VISE parameter.
 * Nopw calls MEM_alloclist() to allocate tbn objects.
 * 
 *    Rev 3.4   18 Jul 1991 16:24:08   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.3   12 Jun 1991 14:40:42   DCV
 * Rewritten for new traceback tree structure to support N-best path and
 *   globalmin reporting.
 * 
 *    Rev 3.2   09 Jan 1991 15:13:20   DCV
 * Added tbn_used()
 * 
 *    Rev 3.1   25 Sep 1990 10:44:10   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 3.0   13 Sep 1990 13:59:18   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:16:58   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990 10:22:46   DCV
 * Initial revision.
 *
 ***************************************************************************/

/***************************************************************************
 *
 *	Name: 			tbn_init -- Initialize Traceback Node Cluster
 *
 *	Calling Sequence:
 *
 *						tbn_init(vise);
 *
 *                      VISE  vise; -- the VISE descriptor
 *
 *	Returns:
 *
 *	Requires:		The memory for traceback nodes must have been deallocated
 *
 *	Modifies:		The traceback node cluster
 *
 *	Effect:			Initializes the traceback node cluster.
 *
 *	Implementation:
 *
 *						Set the vise->tbnPool.freeentries pointer to NULL.
 *						Set vise->tbnPool.used to zero.
 *
 *	Dependency: 	
 *
 *	Uses:
 *
 **************************************************************************/

	V_Void
tbn_init(VISE vise)
	{
	vise->tbnPool.freeentries = NULL;
	vise->tbnPool.used = 0;
	}

/* end tbn_init */

/**************************************************************************
 *
 *	Name: 			tbn_alloc -- Traceback Node Allocation Function
 *
 *	Calling Sequence:
 *
 *						node = tbn_alloc(vise);
 *
 *						tbn  node;	-- newly allocated traceback node object
 *                      VISE vise;  -- a VISE descriptor
 *
 *	Returns: 		A newly allocated and uninitialized traceback node,
 *						or NULL if all	available traceback nodes are already
 *						allocated.
 *
 *
 *	Requires:		tbn_init must be called before invoking tbn_alloc.
 *
 *	Modifies:		The traceback node cluster
 *
 *	Effect:			Returns a new traceback node object, or NULL if none
 *						are available.  Traceback nodes can be returned to free
 *						storage with the tbn_free function.
 *
 *
 *	Implementation:
 *
 *						If no free traceback node exists,
 *							Make some if possible or return NULL
 *							Link them into a NULL-terminated free list.
 *
 *  					Get an traceback node off of the free list.
 *  					Increment the used node count.
 *  					Return the traceback node.
 *
 *	Dependency:
 *
 *	Uses:				param_get()
 *						MEM_alloclist()
 *
 **************************************************************************/

	tbn
tbn_alloc(VISE vise)
	{
	tbn	nextFreeTBN;

		/* If no free traceback nodes exist, make some */
	if (vise->tbnPool.freeentries == NULL &&
		(vise->tbnPool.freeentries =
          (tbn) MEM_alloclist(vise->memCfg, (V_ULong) sizeof(tbn_t), (V_ULong) param_get(vise, TRACEBACKNODEALLOC), FAST_MEM, "tbn_alloc tbn")) == NULL)
        return(NULL);

		/* At least one free traceback node now exists; get it */
	nextFreeTBN = vise->tbnPool.freeentries;
	vise->tbnPool.freeentries = vise->tbnPool.freeentries->tbnsuccessor;
	vise->tbnPool.used++;

	return(nextFreeTBN);
	}

/* end tbn_alloc */

/***************************************************************************
 *
 *	Name: 			tbn_free -- Deallocate a traceback node
 *
 *	Calling Sequence:
 *
 *						tbn_free(vise, node)
 *
 *                      VISE  vise; -- the VISE descriptor
 *						tbn   node; -- The node which is to be deallocated
 *
 *	Returns:
 *
 *
 *	Requires:		tbn_init must be called before invoking tbn_free.
 *						The node must not be NULL.
 *
 *
 *	Modifies:		The traceback node cluster
 *
 *
 *	Effect:			Returns the traceback node to free storage
 *
 *
 *	Implementation:
 *
 *						Point the node's successor pointer at the free list.
 *						Point the free list at the given node.
 *						Decrement the used node count.
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef UNUSED

	V_Void
tbn_free(VISE vise, tbn node)
	{
	node->tbnsuccessor = vise->tbnPool.freeentries;
	vise->tbnPool.freeentries = node;
	vise->tbnPool.used--;
	}

/* end of tbn_free */

#endif

/***************************************************************************
 *
 *	Name: 			tbn_used -- Return the number of tbns in use
 *
 *	Calling Sequence:
 *
 *						numused = tbn_used(vise);
 *
 *						V_Int	numused;
 *                      VISE       vise; -- the VISE descriptor
 *
 *	Returns:			The number of tbns currently in use
 *
 *	Requires:		tbn_init() must have been called previously
 *
 *	Modifies:		Nothing
 *
 *	Effect:
 *
 *	Implementation:
 *
 *	Dependency: 	
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Int
tbn_used(VISE vise)
	{
	return(vise->tbnPool.used);
	}

/* end tbn_used */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_setinfo -- Initialize a traceback node
 *
 *	Calling Sequence:
 *
 *						tbn_setinfo(node, bestpath, frame, pathcount);
 *
 *						tbn		  node;	-- A traceback node object.
 *						tbp	 bestpath;	--	The path element terminating
 *													the best path to this node.
 *						tbf		 frame;	--	The traceback frame containing
 *													this tbn
 *						V_Int	pathcount;	--	The number of paths leading
 *													to this node in this frame
 *
 *	Returns:
 *
 *	Requires:
 *
 *	Modifies:		node
 *
 *	Effect:			Places  the  given  path, frame and pathcount
 *						information into the given traceback node.
 *
 *	Implementation:
 *
 *						bestpath 	==> bestpath field of node
 *						frame			==> frame field of node
 *						pathcount	==> pathcount field of node
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Void
tbn_setinfo(tbn node, tbp bestpath, tbf frame, V_Int pathcount)
	/* tbn		  node	-	Node to receive new data				*/
	/* tbp	 bestpath	-	The best path to this node				*/
	/* tbf		 frame	-	The frame this tbn belongs to			*/
	/* V_Int	pathcount	-	The number of paths to this node		*/
	{
	node->tbnbestpath = bestpath;
	node->tbnframe= frame;
	node->tbnpathcount = pathcount;
	}

/* end tbn_setinfo */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_info -- Get traceback node information
 *
 *	Calling Sequence:
 *
 *						tbn_info(node, bestpath, frame, pathcount);
 *
 *						tbn		   node;	-- A traceback node object.
 *						tbp	 *bestpath;	--	The path element terminating
 *													the best path to this node.
 *						tbf		 *frame;	--	The traceback frame containing
 *													this tbn
 *						V_Int *pathcount;	--	The number of paths leading
 *													to this node in this frame
 *
 *	Returns:
 *
 *	Requires:		tbn_setinfo must have been called for the traceback
 *						node prior to invoking tbn_info on that node.
 *
 *	Modifies:		bestpath, frame, pathcount
 *
 *	Effect:			Returns the bestpath, frame and pathcount	for a
 *						given traceback node.
 *
 *	Implementation:
 *
 *						node's bestpath			==> bestpath
 *						node's frame				==> frame
 *						node's pathcount			==> pathcount
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

	V_Void
tbn_info(tbn node, tbp *bestpath, tbf *frame, V_Int *pathcount)
	/* tbn			node	-	Node to get information from				*/
	/* tbp	 *bestpath	-	Best path to this node						*/
	/* tbf		 *frame	-	Traceback frame containing this node	*/
	/* V_Int	*pathcount	-	Number of paths to this node				*/
	{
	*bestpath	= node->tbnbestpath;
	*frame		= node->tbnframe;
	*pathcount	= node->tbnpathcount;
	}

/* end tbn_info */

#endif

/***************************************************************************
 *
 *	Name: 			tbn_setsuccessor -- Set successor for tbn node
 *
 *	Calling Sequence:
 *
 *						tbn_setsuccessor(predecessor, successor);
 *
 *						tbn predecessor, successor; -- traceback nodes to be
 *						linked
 *
 *	Returns:
 *
 *	Requires:
 *
 *	Modifies:		predecessor
 *
 *	Effect:			Makes successor the successor of predecessor
 *
 *	Implementation:
 *
 *						copy successor to predecessor's successor field
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Void
tbn_setsuccessor(tbn predecessor, tbn successor)
	{
	predecessor->tbnsuccessor = successor;
	}

/* end of tbn_setsuccessor */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_successor -- Get the successor of a tbn node
 *
 *	Calling Sequence:
 *
 *						successor = tbn_successor(predecessor);
 *
 *						tbn successor, predecessor;
 *
 *	Returns: 		The successor to the given predecessor node
 *
 *	Requires:		A successor should have already been  established	with
 *						the tbn_setsuccessor function.
 *
 *	Modifies:
 *
 *	Effect:			Returns the successor of the given predecessor node.
 *
 *	Implementation:
 *
 *						Return successor field of predecessor node
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

	tbn
tbn_successor(tbn predecessor)
	{
	return(predecessor->tbnsuccessor);
	}

/* end of tbn_successor */

#endif

/***************************************************************************
 *
 *	Name: 			tbn_setbestpath -- Set the bestpath to a traceback node
 *
 *	Calling Sequence:
 *
 *						tbn_setbestpath(node, bestpath);
 *
 *						tbn	node;		 -	A traceback node
 *						tbp	bestpath; -	A traceback path element
 *
 *	Returns:			Nothing
 *
 *	Requires:		tbn_init() must have been called previously
 *
 *	Modifies:		The traceback node
 *
 *	Effect:			Sets the bestpath pointer in the traceback node.
 *
 *	Implementation:
 *
 *						Set tbnbestpath to bestpath in the traceback node.
 *
 *	Dependency: 	
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Void
tbn_setbestpath(tbn node, tbp bestpath)
	{
	node->tbnbestpath	= bestpath;
	}

/* end tbn_setbestpath */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_bestpath -- Get the bestpath containing a traceback node
 *
 *	Calling Sequence:
 *
 *						bestpath = tbn_bestpath(node);
 *
 *						tbp bestpath;	-- The last traceback path element on the
 *												best path to the given traceback node
 *						tbn node;		-- The traceback node
 *
 *	Returns: 		The last traceback path element on the best path to the
 *						given traceback node.
 *
 *	Requires:		A bestpath must have been asserted for	the given node
 *						with tbn_setinfo or tbn_setbestpath before invoking
 *						tbn_bestpath.
 *
 *	Modifies:
 *
 *	Effect:			Returns the most recently asserted bestpath for the
 *						given traceback node object.
 *
 *	Implementation:
 *
 *						Return bestpath field of the given traceback node
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	tbp
tbn_bestpath(tbn node)
	/* tbn node	-	Node whose bestpath we're seeking  */
	{
	return(node->tbnbestpath);
	}

/* end tbn_bestpath */

#endif

/***************************************************************************
 *
 *	Name: 			tbn_setframe -- Set the frame of a traceback node
 *
 *	Calling Sequence:
 *
 *						tbn_setframe(node, frame);
 *
 *						tbn	node;
 *						tbf	frame;
 *
 *	Returns:			Nothing
 *
 *	Requires:		tbn_init() must have been called previously
 *
 *	Modifies:		The traceback node
 *
 *	Effect:			Sets the frame pointer in the traceback node.
 *
 *	Implementation:
 *
 *						Set tbnframe to frame in the traceback node.
 *
 *	Dependency: 	
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Void
tbn_setframe(tbn node, tbf frame)
	{
	node->tbnframe	= frame;
	}

/* end tbn_setframe */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_frame -- Get the frame containing a traceback node
 *
 *	Calling Sequence:
 *
 *						frame = tbn_frame(node);
 *
 *						tbn frame;	-- The frame containing the given
 *											traceback node object.
 *						tbn node;	-- The given traceback node object.
 *
 *	Returns: 		The frame containing a given traceback node object.
 *
 *	Requires:		A frame must have been asserted for	the given node with
 *						tbn_setinfo or tbn_setframe before invoking tbn_frame.
 *
 *	Modifies:
 *
 *	Effect:			Returns the most recently asserted frame for the
 *						given traceback node object.
 *
 *	Implementation:
 *
 *						Return frame field of the given traceback node
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	tbf
tbn_frame(tbn node)
	/* tbn node	-	Node whose frame we're seeking  */
	{
	return(node->tbnframe);
	}

/* end tbn_frame */

#endif

/***************************************************************************
 *
 *	Name: 			tbn_setpathcount -- Set the path count of a traceback node
 *
 *	Calling Sequence:
 *
 *						tbn_setpathcount(node, pathcount);
 *
 *						tbn	node;			--	A traceback node
 *						V_Int	pathcount;	-- The number of paths to this node
 *
 *	Returns:			Nothing
 *
 *	Requires:		tbn_init() must have been called previously
 *
 *	Modifies:		The traceback node
 *
 *	Effect:			Sets the pathcount in the traceback node.
 *
 *	Implementation:
 *
 *						Set tbnpathcount to pathcount in the traceback node.
 *
 *	Dependency: 	
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Void
tbn_setpathcount(tbn node, V_Int pathcount)
	{
	node->tbnpathcount	= pathcount;
	}

/* end tbn_setpathcount */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_pathcount -- Path count of traceback node
 *
 *	Calling Sequence:
 *
 *						numpaths = tbn_pathcount(node);
 *
 *						V_Int	numpaths;	-- Number of paths to node
 *						tbn	node;			-- A traceback node object
 *
 *	Returns: 		The number of paths to the	given traceback node object.
 *
 *	Requires:		tbn_setinfo must be invoked on a traceback
 *						node before invoking tbn_pathcount on it.
 *
 *	Modifies:
 *
 *	Effect:			Returns the number of paths to the given traceback
 *						node object.
 *
 *	Implementation:
 *
 *						Return pathcount field of traceback node object
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS

	V_Int
tbn_pathcount (tbn node)
	/* tbn	node	-	Node we're getting pathcount from */
	{
	return(node->tbnpathcount);
	}

/* end tbn_pathcount */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_incpathcount -- Increment path count of a node
 *
 *	Calling Sequence:
 *
 *						num = tbn_incpathcount(node);
 *
 *						V_Int num;	-- New pathcount
 *						tbn node;	-- A traceback node object.
 *
 *	Returns: 		The new pathcount
 *
 *
 *	Requires:		The pathcount of the node must have been set using
 *						tbn_setinfo or tbn_setpathcount before invoking
 *						tbn_incpathcount on that node.
 *
 *	Modifies:		node
 *
 *	Effect:			Increments the path count of a traceback node object.
 *
 *	Implementation:
 *
 *						pathcount = pathcount + 1
 *						return pathcount
 *
 *	Dependency:
 *
 *	Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

	V_Int
tbn_incpathcount(tbn node)
	/* tbn  node	-	Node whose pathcount we're incrementing */
	{
	return(++node->tbnpathcount);
	}

/* end tbn_incpathcount */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_decpathcount -- Decrement the path count of a node
 *
 *	Calling Sequence:
 *
 *						num = tbn_decpathcount(node);
 *
 *						V_Int num;	-- New pathcount.
 *						tbn node;	-- A traceback node object.
 *
 *	Returns: 		The decremented pathcount.
 *
 *
 *	Requires:		The pathcount of the node must have been set using
 *						tbn_setinfo or tbn_setpathcount before invoking
 *						tbn_decpathcount on that node.
 *
 *	Modifies:		node
 *
 *	Effect:			Unconditionally decrements the path count of a traceback
 *						node object.
 *
 *	Implementation:
 *
 *						If pathcount field of node > 0
 *						  Decrement pathcount field
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

	V_Int
tbn_decpathcount(tbn node)
	/* tbn  node	-	Node whose pathcount we're decrementing	*/
	{
	return(--node->tbnpathcount);
	}

/* end tbn_decpathcount */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_globalmin -- Get the globalmin of a traceback node
 *
 *	Calling Sequence:
 *
 *						globalmin = tbn_globalmin(node);
 *
 *						V_Long globalmin; -- The globalmin of the traceback node
 *						tbn			node; -- A traceback node
 *
 *	Returns: 		The globalmin of the traceback node
 *
 *	Requires:		A traceback frame containing this node must exist.
 *
 *	Modifies:		Nothing
 *
 *	Effect:			Returns the globalmin of the traceback frame containing
 *						the given traceback node.
 *
 *	Implementation:
 *
 *						Return the globalmin field of the traceback frame
 *						containing the traceback node.
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS

	V_Long
tbn_globalmin(tbn node)
	/* tbn node	- the traceback node whose frame globalmin is desired */
	{
	return(tbf_globalmin(node->tbnframe));
	}

/* end tbn_globalmin */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_time -- Get the frame time of a traceback node
 *
 *	Calling Sequence:
 *
 *						time = tbn_time(node);
 *
 *						V_Time time; -- The creation time of the traceback node
 *						tbn	 node; -- A traceback node
 *
 *	Returns: 		The creation time of the traceback node object
 *
 *	Requires:		A traceback frame containing this node must exist.
 *
 *	Modifies:		Nothing
 *
 *	Effect:			Returns the creation time of the traceback node.
 *
 *	Implementation:
 *
 *						Return the time field of the traceback frame containing
 *						the traceback node.
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS

	V_Time
tbn_time(tbn node)
	/* tbn node	- the traceback node whose frame time is desired */
	{
	return(tbf_time(node->tbnframe));
	}

/* end tbn_time */

#endif

/**************************************************************************
 *
 *	Name: 			tbn_sn -- Get the sequence number of a traceback node
 *
 *	Calling Sequence:
 *
 *						sn = tbn_sn(node);
 *
 *						V_SN   sn; -- The sequence number of the traceback node
 *						tbn	 node; -- A traceback node
 *
 *	Returns: 		The sequence number of the traceback node object
 *
 *	Requires:		A traceback frame containing this node must exist.
 *
 *	Modifies:		Nothing
 *
 *	Effect:			Returns the sequence number of the traceback node.
 *
 *	Implementation:
 *
 *						Return the sn field of the traceback frame containing
 *						the traceback node.
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

#ifdef	NOMACROS

	V_SN
tbn_sn(tbn node)
	/* tbn node	- the traceback node whose sequence number is desired */
	{
	return(tbf_sn(node->tbnframe));
	}

/* end tbn_sn */

#endif

/* END TBN CLUSTER */
