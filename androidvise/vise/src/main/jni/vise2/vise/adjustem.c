#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_alg.h"
#include "v_div.h"
#include "v_jin.h"
#include "v_km.h"
#include "v_par.h"
#include "v_tem.h"

/**************************************************************************
 *
 * Name:      adjustTemplate -- Adjust template means with new jin data
 *
 * Returns:   A standard VISE status code
 *
 * Requires:  Jin data must be available for dwell frames, starting at *snPtr
 *
 * Modifies:  The template, and sets *snPtr to the last SN found
 *            The training data accumulator in the kernel model
 *
 * Effect:    Mixes each acoustic parameter mean in the old template
 *            with the mean of that particular parameter over the
 *            specified range of frames of jin data, according to the
 *            mixing formula:
 *
 *              (temWeight * tem-mean) + (jinWeight * jin_mean)
 *
 *            and replaces the old template with the new one thus built.
 *
 *            If the weighting factor is in the range from 0 to 254,
 *
 *              temWeight = oldWeight / (oldWeight + numFrames),
 *              jinWeight = numFrames / (oldWeight + numFrames)
 *                 = 1.0 - temWeight.
 *
 *            The effect is to weight the new jin_mean more heavily in
 *            proportion to the number of jin frames it contains.
 *
 *            If the weighting factor is equal to 255,
 *
 *              temWeight = 1,
 *              jinWeight = 0.
 *
 *            The effect is to leave the template unchanged.
 *
 *            If the weighting factor is greater than 256,
 *
 *              temWeight = oldWeight / WEIGHTNORM,
 *              jinWeight = 1.0 - temWeight.
 *
 *            The effect is to weight the new jin_mean independently
 *            of the number of jin frames contributing to it.
 *
 *            Setting oldWeight to WEIGHTNORM has the same effect
 *            as setting it to 255:  the template remains unchanged.
 *
 *            WEIGHTNORM is a VISE parameter.
 *
 * Implementation:
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\ADJUSTEM.C_V  $
 * 
 *    Rev 1.4   20 Mar 1992 12:24:18   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.3   29 Jan 1992 14:24:54   DCV
 * Eliminated superfluous comparison of unsigned variable with zero.
 * 
 *    Rev 1.2   22 Nov 1991 11:58:16   DCV
 * Changed to hybrid weighting scheme
 * 
 *    Rev 1.1   27 Feb 1991 10:54:36   DCV
 * Changed to floating-point weighting scheme
 * 
 *    Rev 1.0   10 Jan 1991 11:11:26   DCV
 * Initial revision.
 * 
 **************************************************************************/

/* Maximum possible numerator value must be less than 2^30 */
#define MAXNUMERATOR 1073741824

  V_Err
adjustTemplate(VISE vise, km kernel, kt ktrainer, V_SN * sn, V_Uns dwell, V_Uns oldWeight)
{
  V_Uns   accum[NUMCOMPONENTS]; /* Acoustic Parameter Accumulator */
  V_Int   i;                    /* Acoustic parameter iterator */
  V_SN    snReq;
  V_Err   exception;
  jinr    jinData;              /* Buffer for unpacked jin data */
  jinr    temData;              /* Buffer for unpacked template means */
  V_Long  jinWeight;            /* Mixing weight for sum of new jin values */
  V_Long  temWeight;
  V_Long  divisor;
  V_Long  weightNorm;
  tem     template;

  if (!ktrainer || vise->firstPass) {
     /* This is incremental || 1st pass of batch training. Initialize */
    weightNorm = (V_Long) param_get(vise, WEIGHTNORM);

    /* Compute the mixing weights */
    if (oldWeight < 255) {
      jinWeight = 1;
      temWeight = (V_Long) oldWeight;
      divisor = (V_Long) oldWeight + dwell;
    } else if (oldWeight == 255) {
      jinWeight = 0;
      temWeight = 1;
      divisor = 1;
    } else if (oldWeight <= weightNorm) {
      jinWeight = weightNorm - oldWeight;
      temWeight = (V_Long) oldWeight * dwell;
      divisor = weightNorm * dwell;
      if (divisor * JINMAXVALUE >= MAXNUMERATOR)
        return(CANTADJUSTTEMPLATE);
    } else {
      return(BADWEIGHTFACTOR);
    }

    /* Zero the acoustic parameter accumulator */
    for (i = 0; i < NUMCOMPONENTS; i++)
      accum[i] = 0;
  }
  /* Accumulate the new acoustic parameter values */
  snReq = *sn;
  if (ktrainer) kt_uptrainingcount(ktrainer, (V_Int) dwell);
  while (dwell--) {
    /* Get the next jin frame from the frame buffer */
    if (!JINSRC_read(vise->jinSource, &snReq, &jinData, &exception))
      return(exception);

    /* Accumulate the acoustic parameters */
    for (i = 0; i < NUMCOMPONENTS; i++) {
      if (ktrainer)
        kt_accumulate(ktrainer, i, jin_getcomponent(&jinData, i));
      if (!ktrainer || vise->firstPass)
        accum[i] += jin_getcomponent(&jinData, i);
    }

    /* Remember the SN found */
    *sn = snReq;
    /* Get the next frame, regardless of SN */
    snReq = -1;
  }

  if (!ktrainer || vise->firstPass) {
     /* This is incremental || 1st pass of batch training. Update template */
    template = km_template(kernel);

    /* Unpack template jin data into temData array */
    tem_read(template, &temData);

    /* Mix the old template means with the accumulated new jin */
    for (i = 0; i < NUMCOMPONENTS; i++)
      accum[i] = ldiv32((V_Long) accum[i] * jinWeight + (V_Long) jin_getcomponent(&temData, i) * temWeight, divisor);

    /* Re-make the template */
    for (i = 0; i < NUMCOMPONENTS; i++)
      jin_setcomponent(&temData, i, (V_Byte) accum[i]);

    tem_write(template, &temData);
  }
  return(VISESUCCESS);
}
