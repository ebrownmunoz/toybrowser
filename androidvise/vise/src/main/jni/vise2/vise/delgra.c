#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "visemsg.h"
#include "v_syn.h"

/**************************************************************************
 *
 * Name:      DELGRAMMAR -- Delete grammar from syntax
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                       Type
 *
 *              Grammar ID                     V_Uns
 *
 * Modifies:  the syntax object
 *
 * Effect:    Attempts to delete the specified grammar from the syntax.
 *            Sends a VISESUCCESS reply if grammar deleted.
 *            Sends a GRAMNOTINSYNTAX reply if grammar not in the syntax.
 *
 * Implementation:
 *
 *            Get the grammar id from the command message
 *            Delete the grammar from the syn object
 *
 * Uses:      syn_deletegrammar -- delete grammar from syntax (syn.c)
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\DELGRA.C_V  $
 * 
 *    Rev 3.3   20 Mar 1992 12:26:50   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.2   12 Nov 1991 15:52:04   DCV
 * Changed return status code GRAMNOTINSYNTAX to GRAMMARNOTINSYNTAX.
 * 
 *    Rev 3.1   02 Aug 1991 09:57:28   DCV
 * Installed new message system
 * 
 *    Rev 3.0   13 Sep 1990 13:36:50   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:03:04   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:34:34   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
DELGRAMMAR(VISE vise)
{
  V_Uns  grammar;
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  if (!MSG_readUns(vise->incoming, &grammar))
    VISE_EXIT(MESSAGETOOSHORT);

  status = syn_deletegrammar(vise, (V_GId) grammar) ? VISESUCCESS : GRAMMARNOTINSYNTAX;

  VISE_EXIT(status);

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DELGRAMMAR);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}

