#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_km.h"
#include "visemsg.h"
#include "v_voc.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:      DLOADWORD -- Download one word model
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                         Type
 *
 *              Word identification number       V_Uns
 *              Word class identification prime  V_Uns
 *              Number of acoustic kernels       V_Uns
 *              For each acoustic kernel:
 *                Minimum duration               V_Uns
 *                Maximum duration               V_Uns
 *
 * Modifies:  The voc object, the wm object and the km object
 *
 * Effect:    Downloads the word model information for one word.
 *            This includes the minimum and maximum dwells of all
 *            the kernels which comprise the word model, but not
 *            the word's templates.
 *
 * Implementation:
 *
 *            Get the word id
 *            Get the word class id prime
 *            Get the number of kernels.
 *
 *            Create a new vocabulary word
 *            For each kernel
 *             put in minimum and optional dwell
 *                 (optional dwell = max dwell - min dwell)
 *            Send a VISESUCCESS reply.
 *
 *            Exception handling:
 *            if the word already is in the vocabulary,
 *              send a WORDINVOCAB reply.
 *            if there is no room for the new word,
 *             send a CANTALLOCATEVOCWORD reply.
 *            if the word has too many kernels,
 *              send a TOOMANYKERNELS reply.
 *            if the duration of the word > MAXDWELL,
 *              send a WORDDURATIONTOOLONG reply.
 *            if the minimum dwell = 0  for any kernel,
 *              send a BADDWELLS reply.
 *            if the minimum dwell > maximum dwell for any kernel,
 *              send a BADDWELLS reply.
 *
 * Uses:      voc_createword -- add a word entry in vocabulary
 *            wm_firstkernel -- return first kernel of word model
 *            km_putdwells -- add dwells to kernel model
 *            MSG_xxx -- message routines
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\DLOADW.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:32:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   12 Nov 1991 15:57:52   DCV
 * Changed return status code VOCABFULL to CANTALLOCATEVOCWROD.
 * 
 *    Rev 3.5   08 Oct 1991 09:54:18   DCV
 * Now accepts word models containing no kernels.
 * 
 *    Rev 3.4   24 Sep 1991 16:02:28   DCV
 * Now expects a class id prime as the second message element.
 * 
 *    Rev 3.3   20 Sep 1991 17:27:10   DCV
 * Assigns a unique prime (word class) to each word based on its id, except
 * that all words with id >= 1000 are assigned to class 1.
 * 
 *    Rev 3.2   02 Aug 1991 10:13:50   DCV
 * Installed new message system
 * 
 *    Rev 3.1   18 Jul 1991 16:05:40   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.0   13 Sep 1990 13:41:26   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:05:10   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:48:14   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
DLOADWORD(VISE vise)
{
  V_Uns  nKernels;            /* Number of kernels in word      */
  V_Uns  minDwell, maxDwell;  /* Dwell constraints for a kernel */
  V_Uns  wid;                 /* Word identification number     */
  V_Uns  class;               /* Word class number              */
  V_Int  duration;            /* word duration                  */
  wm     word;                /* New word model                 */
  km     kernel;              /* New kernel model               */
  kt     ktrainer;            /* Kernel training object         */
  V_Uns  senderId = MSG_correspondentId(vise->incoming);
  V_Err  vise_status, status;

  /* Get the wid, the class prime and the number of kernels */
  if (!MSG_readUns(vise->incoming, &wid) ||
      !MSG_readUns(vise->incoming, &class) ||
      !MSG_readUns(vise->incoming, &nKernels))
    VISE_EXIT(MESSAGETOOSHORT);

  /* Make sure the number of kernels is not too large */
  if (NUMKERNEL < nKernels)
    VISE_EXIT(TOOMANYKERNELS);

  /* Make sure word is not already in the vocabulary */
  if ((word = voc_getword(vise, (V_WId) wid)))
    VISE_EXIT(WORDINVOCAB);

  /* Allocate a vocabulary entry for the word */
  if (!(word = voc_createword(vise, (V_WId) wid, class, (V_Int) nKernels)))
    VISE_EXIT(CANTALLOCATEVOCWORD);

  /* The normal case */
  duration = 0;
  for (kernel = wm_firstkernel(word), ktrainer = wm_firstkt(word); kernel != NULL; kernel = km_nextkernel(kernel)) {

    if (!MSG_readUns(vise->incoming, &minDwell) ||
        !MSG_readUns(vise->incoming, &maxDwell)) {
      voc_deleteword(vise, (V_WId) wid);
      VISE_EXIT(MESSAGETOOSHORT);
    }

    duration += maxDwell;

    if ((maxDwell < minDwell) || (minDwell == 0)) {
      voc_deleteword(vise, (V_WId) wid);
      VISE_EXIT(BADDWELLS);
    }

    if (MAXDWELL < duration) {
      voc_deleteword(vise, (V_WId) wid);
      VISE_EXIT(WORDDURATIONTOOLONG);
    }

    km_putdwells(kernel, (V_Int) minDwell, (V_Int) (maxDwell - minDwell));
    if (ktrainer) {
      kt_putobsdwells(ktrainer, (V_Int) maxDwell, (V_Int) minDwell);
      ktrainer = kt_nextkt(ktrainer);
    }
  }

  VISE_EXIT(VISESUCCESS);

  VISE_RETURN:
  MSG_closeRead(vise->incoming, &status);
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _DLOADWORD);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
