#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_fyi.h"
#include "v_alg.h"
#include "v_gi.h"
#include "v_ni.h"
#include "v_par.h"
#include "v_syn.h"
#include "v_tb.h"
#include "v_tbn.h"
#include "v_tbp.h"

#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/***************************************************************************
 *
 * Name:      gramnodes -- Process grammar nodes
 *
 * Returns:   EOUNOTYETFOUND  - processed grammar nodes successfully
 *            VISESUCCESS     - detected end-of-utterance
 *            CANTALLOCATETBF - ran out of memory allocating tbf
 *            CANTALLOCATETBN - ran out of memory allocating tbn
 *
 * Requires:  The active grammar must have at least three nodes, which must
 *            be properly ordered.  In particular, the last node must be the
 *            starting node, the penultimate node must be the "joker" node,
 *            and the first node must be the terminating node.
 *
 * Modifies:  grammar nodes, traceback tree
 *
 * Effect:    Processes the grammar nodes after a frame of dynamic
 *            programming, producing a list of new traceback nodes for
 *            the frame, each containing a list of the last elements
 *            on the n-best paths reaching the node at this time.
 *
 *            Checks for end-of-utterance, puts the time, the accumulated
 *            sum of global minimum frame costs, the list of traceback nodes
 *            and the number of nodes that list contains into the next frame
 *            of the traceback tree for this time, allocates a new next
 *            frame, and prunes the oldest unpruned frame in the tree.
 *
 * Implementation:
 *
 *            Get the next frame, which was allocated but received no
 *              frame information in the previous call to gramnodes().
 *
 *            Prune the starting, wildcard and termination nodes of all
 *              except the best paths leading to them in this frame.
 *
 *            If score of the best path to the termination node < V_INFINITY,
 *              Add a traceback node for the termination node to the
 *              list of grammar nodes reached in this frame.
 *              If score = best score,
 *                Fill in the frame information and return ENDOFUTTERANCE.
 *              Copy the best inherited score to seed score.
 *              Set the seed tbn for the node to be the newly added tbn.
 *              Reset the scores of all paths to this node to V_INFINITY.
 *            Otherwise,
 *              Set seed score to V_INFINITY and seed node to NULL.
 *
 *            For all non-terminal nodes,
 *              If the score of the best path to this node < V_INFINITY,
 *                Add a traceback node for this node to the list
 *                 of grammar nodes reached in this frame.
 *                Copy the best inherited score to seed score.
 *                Set the seed tbn for the node to be the newly added tbn.
 *              Otherwise,
 *                Set seed score to V_INFINITY and seed node to NULL.
 *              Reset scores of all paths to this node to V_INFINITY.
 *
 *            If list of grammar nodes reached in this frame is not empty,
 *              Terminate it,
 *              Put it and the rest of frame information into next frame,
 *              Add a new frame (new next frame) to the traceback tree.
 *
 *            If the score of a path to the start node < V_INFINITY,
 *              Make its tb node an orphan.
 *
 *            Prune the last row in the traceback tree.
 *
 *            Do BOU processing.
 *
 * Uses:      syn_getactive - Get the active grammar
 *            tb_addframe - Add a layer to the traceback tree
 *            tbf_setinfo - Put the frame information into a frame
 *            gi_firstnode - Get the terminal node of a grammar
 *            gi_lastnode - Get the last (start) node of a grammar
 *            ni_nextnode - Get the next grammar node instance
 *            ni_pathlist - Get the list of paths reaching a node
 *            ni_prevnode - Get the previous grammar node instance
 *            ni_putpathlist - Give a node an inherited path list
 *            ni_putseed - Set seed score and seed tb node of a grammar node
 *            ni_setworstscore - Set the grammar node's worst path score
 *            tbp_cost - Get a path's cost
 *            tbp_decchildcount - Decrement child count of a traceback node
 *            tbp_orphan - Make a traceback node an orphan.
 *            tb_prune - Prune the oldest row of the traceback tree
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\GRMNODES.C_V  $
 * 
 *    Rev 3.9   20 Mar 1992 12:36:18   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.8   12 Nov 1991 15:35:40   DCV
 * Modified to use inherited pathlist instead of inherited path array,
 *   and seed tbp in place of seed tbn, eliminating pathlist().
 * Now returns standard VISE status codes, including the new code
 *   EOUNOTYETFOUND.
 * 
 *    Rev 3.7   20 Sep 1991 09:39:42   DCV
 * Rewrote pathlist() as a simple copy of iprs into tbprs.
 * Eliminated pruning of ipr arrays in terminal and wildcard nodes.
 * 
 *    Rev 3.6   30 Aug 1991 17:00:08   DCV
 * Eliminated redundant code dealing with allocating a new tbn for
 *   the terminal grammar node.
 * 
 *    Rev 3.5   23 Aug 1991 09:46:26   DCV
 * Fixed to record tbn and tbp usage correctly.
 * 
 *    Rev 3.4   02 Aug 1991 15:46:26   DCV
 * Eliminated possibility of returning a spurious terminal tbn when
 * traceback structures cannot be allocated
 * 
 *    Rev 3.3   18 Jul 1991 16:11:48   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   12 Jun 1991 13:41:26   DCV
 * Rewrote to support N-best path reporting
 * 
 *    Rev 3.1   22 Jan 1991 10:53:50   DCV
 * Added FYI invocations and improved documentation
 * 
 *    Rev 3.0   13 Sep 1990 13:45:56   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:07:54   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:02:48   DCV
 * Initial revision.
 *
 ***************************************************************************/

  V_Err
gramnodes(VISE vise, V_Int bestscore, V_Long sigmaglobalmin, V_Long sigmabestscore, V_Time frameid, V_SN sn, tbn * termnode)
{
  gi     grammar;      /* Active grammar                    */
  ni     gramnode;     /* Grammar node instance             */
  V_Int  nodescore;    /* Score of best path to a node      */
  V_Int  jokescore;    /* Score of wildcard grammar node    */
  tbn    newtbn;       /* New traceback node                */
  tbn    nextfreetbn;  /* The next free tb node in freelist */
  tbn    nodelist;     /* List of new traceback nodes       */
  V_Int  nodecount;    /* Number of nodes in nodelist       */
  tbp    path;         /* A traceback path element          */
  tbp    parent;       /* Previous element on a path        */
  tbp    nextpath;     /* The next best path in a pathlist  */
  ni     lastnode;     /* Iteration terminator              */
  tbf    nextframe;    /* The next frame in the tbtree      */

  FYI_DEFHANDLE(numTbfsInUse); /* DIAGNOSTIC */
  FYI_DEFHANDLE(numTbnsInUse); /* DIAGNOSTIC */
  FYI_DEFHANDLE(numTbpsInUse); /* DIAGNOSTIC */

  FYI_GETHANDLE(vise, numTbfsInUse, FYI_NUMTBFSINUSE);
  FYI_GETHANDLE(vise, numTbnsInUse, FYI_NUMTBNSINUSE);
  FYI_GETHANDLE(vise, numTbpsInUse, FYI_NUMTBPSINUSE);

  /* Get the active grammar */
  grammar = syn_getactive(vise);

  /* Get the traceback frame to be filled */
  nextframe = tb_nextframe(vise);

  /* Prune the joker (next-to-last) node of all except the best path, and limit its score to MUTHAH */
  gramnode = ni_prevnode(ni_prevnode(gi_lastnode(grammar)));
  if ((path = ni_pathlist(gramnode)) != NULL) {
    if ((jokescore = tbp_cost(path)) > param_get(vise, MUHTHAH))
      tbp_setcost(path, (jokescore = param_get(vise, MUHTHAH)));
    for (path = tbp_successor(path); path; path = nextpath) {
      nextpath = tbp_successor(path);
      tbp_free(vise, path);
    }
    ni_setworstscore(gramnode, V_INFINITY);
  } else {
    jokescore = param_get(vise, MUHTHAH);
  }

  /* Get the list of free traceback nodes */
  nodelist = nextfreetbn = vise->tbnPool.freeentries;
  newtbn = NULL;

  /* Zero the count of new traceback nodes for this frame */
  nodecount = 0;

  /* Make sure the terminal node has no spurious tbn, in case an allocation failure prevents it from being assigned a valid one */
  *termnode = NULL;

  /* Get the termination (first) grammar node */
  gramnode = gi_firstnode(grammar);
  /* If there are (finite cost) paths to the termination node, insert a traceback node for it into the node list */
  if ((path = ni_pathlist(gramnode)) != NULL) {
    /* Seed the grammar node with the (finite) score of the best path and the best path */
    ni_putseed(gramnode, (nodescore = tbp_cost(path)), path);

    /* Get a new traceback node, if possible */
    if (nextfreetbn == NULL) {
      /* vise->tbnPool.freeentries, nodelist and newtbn are also NULL */
      if ((nextfreetbn = tbn_alloc(vise)) == NULL) return(CANTALLOCATETBN);
      nodelist = nextfreetbn;
    } else {
      vise->tbnPool.used++;
    }
    newtbn = nextfreetbn;
    nextfreetbn = tbn_successor(nextfreetbn);

    /* Put the inherited path list into the new traceback node and make that node aware of the frame it will belong to */
    tbn_setinfo(newtbn, path, nextframe, 0);
    while (path) {
      tbp_setnode(path, newtbn);
      tbn_incpathcount(newtbn);
      if ((parent = tbp_parent(path)))
        tbp_incchildcount(parent);
      path = tbp_successor(path);
    }

    /* Count the new traceback node */
    nodecount++;

    /* Null the inherited path list */
    ni_putpathlist(gramnode, NULL);
    ni_setworstscore(gramnode, V_INFINITY);

    /* The tbn for the termination node is the new tbn */
    *termnode = newtbn;

    /* DEBUG */
    VBX_DEBUG(VBX_print("  VISE::gramnodes: sn = " QuadFmt(-) "; bestscore = %d; terminalscore = %d\n", sn, bestscore, nodescore));

    /* If the path having the best finite score at this time ends at this (the termination) node, return VISESUCCESS. */
    if (nodescore == bestscore) {
      /* Terminate the nodelist, which has one node in it */
      tbn_setsuccessor(newtbn, NULL);
      /* Put the frame information into the next frame.  There is no need to call tb_addframe() to allocate another frame */
      tbf_setinfo(nextframe, nodelist, nodecount, frameid, sn, sigmabestscore, sigmaglobalmin);
      /* Report the number of tbfs, tbns and tbps in use */
      FYI_SETMAXP(numTbfsInUse, tbf_used(vise));
      FYI_SETMAXP(numTbnsInUse, tbn_used(vise));
      FYI_SETMAXP(numTbpsInUse, tbp_used(vise));
      /* Return for end-of-utterance processing */
      return(VISESUCCESS);
    }

  /* Otherwise, the cost of the best path to the termination node is infinite, so no traceback
     is possible (the tbn is left NULL), and no traceback node is needed to seed the next frame */
  } else {
    ni_putseed(gramnode, V_INFINITY, NULL);
  }

  /* Process the non-terminal grammar nodes */
  lastnode  = gi_lastnode(grammar);
  while ((gramnode = ni_nextnode(gramnode)) < lastnode) {

    /* If there are (finite cost) paths to this node, insert a traceback node for it into the node list */
    if ((path = ni_pathlist(gramnode)) != NULL) {
      /* Seed the grammar node with the (finite) score of the best path and the best path */
      ni_putseed(gramnode, tbp_cost(path), path);

      /* Get a new traceback node, if possible */
      if (nextfreetbn == NULL) {
        vise->tbnPool.freeentries = NULL;
        if ((nextfreetbn = tbn_alloc(vise)) == NULL) return(CANTALLOCATETBN);
        if (newtbn != NULL) tbn_setsuccessor(newtbn, nextfreetbn);
        if (nodelist == NULL) nodelist = nextfreetbn;
      } else {
        vise->tbnPool.used++;
      }
      newtbn = nextfreetbn;
      nextfreetbn = tbn_successor(nextfreetbn);

      /* Put the inherited path list into the new traceback node and make that node aware of the frame it will belong to */
      tbn_setinfo(newtbn, path, nextframe, 0);
      while (path != NULL) {
        tbp_setnode(path, newtbn);
        tbn_incpathcount(newtbn);
        if ((parent = tbp_parent(path)) != NULL)
          tbp_incchildcount(parent);
        path = tbp_successor(path);
      }

      /* Count the new traceback node */
      nodecount++;

      /* Null the inherited path list */
      ni_putpathlist(gramnode, NULL);
      ni_setworstscore(gramnode, V_INFINITY);


    /* Else, we don't need to assign a traceback node (tbn) as niseednode for the grammar node, whose score is V_INFINITY, since
       processkernel() assures that a path's score remains V_INFINITY once it becomes V_INFINITY, and process() assures that a
       destination node will never inherit from a path whose score is V_INFINITY.  Furthermore, if the score for the terminal
       node is V_INFINITY, gramnodes() will not return VISESUCCESS.  Hence, a path whose score reaches V_INFINITY at any time
       cannot lead to end of utterance.  Such a path needs no tbn's, since traceback will never be performed on it. */
    } else {
      ni_putseed(gramnode, V_INFINITY, NULL);
    }
  }

  /* Restore unused tbnodes to the freelist */
  vise->tbnPool.freeentries = nextfreetbn;

  /* If the nodelist now has nodes in it, terminate it, put it and the rest of the frame information into the next frame,
     and add a new frame (which becomes the new next frame) to the traceback tree. */
  if (nodecount) {
    tbn_setsuccessor(newtbn, NULL);
    if (tb_addframe(vise, nodelist, nodecount, frameid, sn, sigmabestscore, sigmaglobalmin) == NULL)
      return(CANTALLOCATETBF);
  }

  /* If the starting node's score < V_INFINITY, then newtbn belongs to the starting node, and the best path to the starting node
     at this time ends in the best traceback path element in newtbn, which we must orphan along with its siblings such that,
     if any one of them occurs in the eventual traceback, the traceback will stop here (and not find all the silences and
     wildcards that may precede the first node); note that this can render the parent node childless, but since it is always less 
     than MAXDWELL frames old, it will eventually get pruned;  otherwise, there are no paths to the starting node at this time. */
  nodescore = ni_seedscore(ni_prevnode(gi_lastnode(grammar)));
  if (nodescore < V_INFINITY) {
    path = tbn_bestpath(newtbn);
    do {
      if ((parent = tbp_parent(path)) != NULL)
        tbp_decchildcount(parent);
      tbp_orphan(path);
    } while ((path = tbp_successor(path)) != NULL);
  }

  /* Report the number of tbfs, tbns and tbps in use */
  FYI_SETMAXP(numTbfsInUse, tbf_used(vise));
  FYI_SETMAXP(numTbnsInUse, tbn_used(vise));
  FYI_SETMAXP(numTbpsInUse, tbp_used(vise));

  /* Prune the traceback tree */
  tb_prune(vise, frameid);

  /* Indicate the Beginning of Utterance status.  The score of Node 0 (the starting node) is nodescore; that of Node 1 is jokescore */
  if ((nodescore == bestscore) || ((jokescore == bestscore) && (++vise->jokeTimer >= param_get(vise, GRACEPERIOD)))) {
    param_set(vise, ACTIVATIONTHRESHOLD, param_get(vise, TIGHTDPTHRESH));
    param_set(vise, DEACTIVATIONTHRESHOLD, param_get(vise, TIGHTDPTHRESH));
    vise->jokeTimer = 0;
  } else if (jokescore != bestscore) {
    param_set(vise, ACTIVATIONTHRESHOLD, param_get(vise, LOOSEDPTHRESH));
    param_set(vise, DEACTIVATIONTHRESHOLD, param_get(vise, LOOSEDPTHRESH));
    vise->jokeTimer = 0;
  }

  return(EOUNOTYETFOUND);
}

