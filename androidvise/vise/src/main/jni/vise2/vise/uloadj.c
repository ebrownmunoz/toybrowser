#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_cmd.h"
#include "visecomp.h"
#include "v_jin.h"
#include "visemsg.h"

/**************************************************************************
 *
 * Name:      ULOADJIN - Upload jin data
 *
 * Returns:   Status of reply message
 *
 * Requires:  WARMSTART must already have been called
 *
 *            The format of the command message must be as follows:
 *
 *              Contents                         Type
 *
 *              Starting sequence number         V_SN
 *              Ending sequence number           V_SN
 *
 * Modifies:  The message
 *
 * Effect:    Uploads all the jin frames having sequence numbers in
 *            the specified range, reporting the actual starting and
 *            ending sequence numbers and the number of frames.  The
 *            contents of the reply message are:
 *
 *              Contents                         Type
 *
 *              Starting sequence number         V_SN
 *              Ending sequence number           V_SN
 *              Number of frames                 V_Uns
 *              For each frame:
 *                Sequence number                V_SN
 *                Jin data (as a jin2r)          V_Uns * SIZE_OF_JIN2_ARRAY
 *
 * Implementation:
 *
 * Uses:      JINSRC_numframes
 *            JINSRC_extract
 *            MSG_xxx -- Message functions
 *
 **************************************************************************/

  V_Err
ULOADJIN(VISE vise)
{
  V_SN    startSN, endSN, sn;
  V_Uns   numFrames;
  V_ULong length;
  jin2r   features;
  V_Uns   senderId = MSG_correspondentId(vise->incoming);
  V_Err   vise_status, status;

  /* Get the requested limiting sequence numbers */
  MSG_readSN(vise->incoming, &startSN);
  MSG_readSN(vise->incoming, &endSN);

  if (!MSG_closeRead(vise->incoming, &status))
    VISE_EXIT(status);

  /* Get the actual limiting sequence numbers and the number of frames within the limits */
  numFrames = (V_Uns) JINSRC_numframes(vise->jinSource, &startSN, &endSN);

  /* Construct the reply */
  length = 3 * VINTSIZEINMELS + 2 * VSNSIZEINMELS + numFrames * (VSNSIZEINMELS + SIZE_OF_JIN2_ARRAY * VINTSIZEINMELS);
  if (MSG_openWrite(vise->outgoing, senderId, length, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ULOADJIN);
    MSG_writeInt(vise->outgoing, VISESUCCESS);

    MSG_writeSN(vise->outgoing, startSN);
    MSG_writeSN(vise->outgoing, endSN);
    MSG_writeUns(vise->outgoing, numFrames);

    sn = startSN;
    while (numFrames-- &&
           JINSRC_extract(vise->jinSource, &sn, &features, &status) &&
           MSG_writeSN(vise->outgoing, sn) &&
           MSG_writeMels(vise->outgoing, (V_Mel *) &features, (V_ULong) SIZE_OF_JIN2_ARRAY))
      sn = -1;

    /* Close the reply message */
    MSG_closeWrite(vise->outgoing, &status);
  }

  /* Normal return */
  return(status);

  /* Exception handling */
  VISE_RETURN:
  if (MSG_openWrite(vise->outgoing, senderId, (V_ULong) (2 * VINTSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
    MSG_writeInt(vise->outgoing, _ULOADJIN);
    MSG_writeInt(vise->outgoing, (V_Int) vise_status);
    MSG_closeWrite(vise->outgoing, &status);
  }
  return(status);
}
