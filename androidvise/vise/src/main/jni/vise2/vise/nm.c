#include "pthr.h"
#include "vise.h"
#include "v_nm.h"

/**************************************************************************
 *
 *	NAME:				NM V3.0 -- Node Model Cluster
 *
 *	DESCRIPTION:	The node model cluster implements the abstract node
 *						model objects which are the nodes of the grammar model
 *						objects.  Arc model objects span between these nodes in
 *						the grammar. A node model object contains only one field,
 *						its identification number.
 *
 *	OPERATIONS:
 *
 *		1.	nodeID = nm_nid(nodeModel) -- Read ID from node model.
 *
 *		2.	nm_setnid(nodeModel, nodeID) -- Write ID to node model.
 *
 *			nm	 nodeModel		--	A node model object.
 *			V_NId nodeID		--	An identification number.
 *
 *	UNIT TEST:	nmt.ct
 *
 *	COMPUTER:	TMS320C30
 *
 *	$Log:   C:\USR\SRC\VISE\C\VCS\NM.C_V  $
 * 
 *    Rev 3.2   20 Mar 1992 12:45:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.1   29 Jan 1992 15:10:52   DCV
 * Changed basic types
 * Replaced nm_read() with nm_nid() and nm_write() with nm_setnid()
 * 
 *    Rev 3.0   13 Sep 1990 13:52:14   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:11:58   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 18:26:40   DCV
 * Initial revision.
 *
 *************************************************************************/

/**************************************************************************
 * 
 *	nm_nid(nodeModel);
 *	nm nodeModel;
 *
 * RETURNS: the identification number of the argument.
 *
 * MODIFIES: nothing.
 *
 * SIDE EFFECTS: none.
 *
 * CALLS: nothing.
 *
 * NOTE: nm_nid is now a macro in funcdefs.h
 *
 *************************************************************************/
#ifdef NOMACROS
	V_NId
nm_nid(nm nodeModel)
	{
	return (nodeModel->nmid);
	}
#endif

/**************************************************************************
 * 
 *	nm_setnid(nodeModel, nodeID);
 *	nm nodeModel;
 *	V_NId	nodeID;
 *
 * RETURNS: V_Void
 *
 * MODIFIES: the passed node model.
 *
 * SIDE EFFECTS: none.
 *
 * CALLS: nothing.
 *
 * NOTE: nm_setnid is now a macro in funcdefs.h
 *
 *************************************************************************/
#ifdef NOMACROS
	V_Void
nm_setnid(nm nodeModel, V_NId nodeID)
	{
	nodeModel->nmid = nodeID;
	}
#endif

