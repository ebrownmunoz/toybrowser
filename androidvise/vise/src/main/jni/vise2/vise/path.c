#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_pat.h"

/**************************************************************************
 *
 *	Name: 			PATH.C -- Path Cluster
 *
 *	Description:	The path cluster records the	utterance: It is best
 *						thought of as a stack filled by the forced traceback
 *						and read by the recognition module.  Each entry in the
 *						path object is a quadruplet consisting of a word
 *						identification number, a starting time, ending time,
 *						and score for that word.
 *
 *	Operations:
 *
 *		  1. path_init  - Initializes the path.
 *		  2. path_flush - Empty the path.
 *		  3. path_push  - Adds a path entry to the path.
 *		  4. path_pop   - Gets the next path entry from the path.
 *
 *	Computer:		TMS320C30
 *
 *	Unit Test:		patht.ct
 *
 *	$Log:   C:\USR\SRC\VISE\C\VCS\PATH.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:47:24   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.6   29 Jan 1992 13:08:50   DCV
 * Changed basic types
 * 
 *    Rev 3.5   14 Aug 1991 16:19:40   DCV
 * Made PATHSTACKSIZE a VISE parameter
 * 
 *    Rev 3.4   01 Aug 1991 17:24:40   DCV
 * Added path_flush() function
 * Moved path buffer to slow memory
 * 
 *    Rev 3.3   18 Jul 1991 16:18:52   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 3.2   12 Jun 1991 14:11:02   DCV
 * Added globalmin reporting
 * 
 *    Rev 3.1   25 Sep 1990 10:42:18   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 3.0   13 Sep 1990 13:53:36   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:13:24   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990  9:27:00   DCV
 * Initial revision.
 *
 **************************************************************************/

/*
 *	The basic element of a path is the following structure, which contains
 *	information about a recognized word.
 */
typedef struct wrd_s
	{
	V_WId	wid;		/* id of a word on the path							*/
    V_Uns   numframes;  /* the number of frames in the word                 */
	V_SN	startsn;	/* starting sequence number for the word			*/
	V_SN	endsn;		/* ending sequence number for the word				*/
	V_Int	score;	    /* average cost per frame in the word				*/
	V_Int	gblmin;	    /* average global min cost per frame in the word	*/
	} wrd_t;

#define PATH_SIZE	


/***************************************************************************
 *
 *	Name: 			path_init -- Initialize path object
 *
 *	Calling Sequence:
 *
 *						V_Bool success = path_init(VISE vise);
 *
 *	Returns:        TRUE if successful, FALSE otherwise
 *
 *	Requires:		The slow memory must have been deallocated.
 *
 *	Modifies:		The path object
 *
 *	Effect:			Initializes the path.
 *
 *	Implementation:
 *
 *						Allocate slow memory for the stack.
 *						Set stack pointer to empty stack position.
 *						Set end pointer to first position past end of stack.
 *
 *	Dependency: 	
 *
 *	Uses:
 *
 **************************************************************************/

	V_Bool
path_init(VISE vise)
	{
	vise->path.nextWord =
      vise->path.firstWord = (WRD) MEM_alloc(vise->memCfg, (V_ULong) sizeof(wrd_t), (V_ULong) param_get(vise, PATHSTACKSIZE), SLOW_MEM, "path_init wrd");
	vise->path.lastWord = vise->path.firstWord + param_get(vise, PATHSTACKSIZE);
	return(vise->path.firstWord != NULL);
	}

/***************************************************************************
 *
 *	Name: 			path_flush -- Empty the path object
 *
 *	Calling Sequence:
 *
 *						path_flush(VISE vise);
 *
 *	Returns:
 *
 *	Requires:		path_init() must have been called 
 *
 *	Modifies:		The path object
 *
 *	Effect:			Empties the path.
 *
 *	Implementation:
 *
 *						Set stack pointer to empty stack position.
 *
 *	Uses:
 *
 **************************************************************************/

	V_Void
path_flush(VISE vise)
	{
	vise->path.nextWord = vise->path.firstWord;
	}

/**************************************************************************
 *
 *	Name: 			path_push -- Push an entry on the path
 *
 *	Calling Sequence:
 *
 *						path_push(vise, wordid, startsn, endsn, score, gblmin);
 *
 *                      VISE       vise; -- a VISE descriptor
 *						V_WId    wordid; -- word identification number
 *                      V_Uns numframes; -- the number of frames in the word
 *						V_SN    startsn; -- starting sequence number of word
 *						V_SN      endsn; -- ending sequence number of word
 *						V_Int     score; -- average frame cost in word
 *						V_Int    gblmin; -- average global min frame cost
 *
 *	Returns: 		TRUE if successful, FALSE if path is full.
 *
 *	Requires:		word score should be positive.
 *
 *	Modifies:		The path object.
 *
 *	Effect:			Adds one path entry to the path with the specified word id number,
 *						number of frames, starting sequence number, ending sequence number,
 *                      score and global min.
 *
 *	Implementation:
 *
 *						If the path is full, return FALSE.
 *						Otherwise,
 *							Set the call parameters to the item pointed to
 *								by the stack pointer,
 *							Increment the stack pointer,
 *							and return TRUE.
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

	V_Bool
path_push(VISE vise, V_WId wordid, V_Uns numframes, V_SN startsn, V_SN endsn, V_Int score, V_Int gblmin)
	{
		/* If the path is full, abort */
	if (vise->path.nextWord >= vise->path.lastWord)
		return (FALSE);

		/*	Otherwise, set the path item */
	vise->path.nextWord->wid       = wordid;
    vise->path.nextWord->numframes = numframes;
	vise->path.nextWord->startsn   = startsn;
	vise->path.nextWord->endsn     = endsn;
	vise->path.nextWord->score     = score;
	vise->path.nextWord->gblmin    = gblmin;

		/* Increment the stack pointer */
	vise->path.nextWord++;

	return (TRUE);
	}

/***************************************************************************
 *
 *	Name: 			path_pop -- Pull the next path entry off the path
 *
 *	Calling Sequence:
 *
 *						path_pop(vise, wordid, numframes, startsn, endsn, score, gblmin);
 *
 *                      VISE        vise; -- a VISE descriptor
 *						V_WId    *wordid; -- word identification number
 *                      V_Uns *numframes; -- number of frames in the word
 *						V_SN    *startsn; -- starting sequence number of word
 *						V_SN      *endsn; -- ending sequence number of word
 *						V_Int     *score; -- average frame cost in the word
 *						V_Int    *gblmin; -- average global min frame cost
 *
 *	Returns: 		TRUE if successful, FALSE if the path is empty.
 *
 *	Requires:
 *
 *	Modifies:		The path object.
 *
 *	Effect:			Gets the next item from the path.
 *
 *	Implementation:
 *
 *						If the path is empty, return FALSE.
 *						Otherwise,
 *							Decrement the stack pointer,
 *							Set the call parameters to the item pointed to
 *								by the stack pointer,
 *							and return TRUE.
 *
 *	Dependency:
 *
 *	Uses:
 *
 **************************************************************************/

	V_Bool
path_pop(VISE vise, V_WId * wordid, V_Uns * numframes, V_SN * startsn, V_SN * endsn, V_Int * score, V_Int * gblmin)
	{
		/* If the path is empty, abort */
	if (vise->path.nextWord <= vise->path.firstWord)
		return (FALSE);

		/* Otherwise, decrement the stack pointer */
	vise->path.nextWord--;

		/* Return the contents of the path item */
	*wordid    = vise->path.nextWord->wid;
    *numframes = vise->path.nextWord->numframes;
	*startsn   = vise->path.nextWord->startsn;
	*endsn     = vise->path.nextWord->endsn;
	*score     = vise->path.nextWord->score;
	*gblmin    = vise->path.nextWord->gblmin;

	return (TRUE);
	}

