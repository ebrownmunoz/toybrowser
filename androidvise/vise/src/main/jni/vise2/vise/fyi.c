#include "vise.h"
#include "v_vise.h"
#include "v_fyi.h"
#include "visemsg.h"
#include "v_mem.h"

/**************************************************************************
 *
 * Name:  FYI.c -- Information Collecting and Reporting Cluster
 *
 * Description:
 *
 *  This cluster implements a consistent scheme for collecting
 *  and reporting useful information about the behavior of VISE.
 *
 * Operations:
 *
 *  1. FYI_init(vise) -- Initialize the FYI cluster.
 *
 *  2. parmHandle = FYI_allocate(vise, parameterId) -- Allocate
 *     a parameter and return a pointer to it.
 *
 *  3. size = FYI_msgSize(vise);
 *
 *  4. success = FYI_writeMsg(vise) -- Send the current parameter
 *     values via the outgoing vise message.  Return TRUE if
 *     successful, FALSE otherwise.
 *    
 * $Log:   C:\USR\SRC\VISE\C\VCS\FYI.C_V  $
 * 
 *    Rev 1.5   20 Mar 1992 12:34:42   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.4   29 Jan 1992 14:43:20   DCV
 * Changed basic types
 * 
 *    Rev 1.3   22 Aug 1991 11:19:48   DCV
 * Added dynamic memory allocation.
 * Modified for new message system.
 * 
 *    Rev 1.2   24 Jul 1991 14:13:08   DCV
 * Raised maximum number of parameters to 24
 * 
 *    Rev 1.1   07 Jun 1991 15:58:50   DCV
 * Increased number of parameters to 10
 * 
 *    Rev 1.0   22 Jan 1991 11:05:20   DCV
 * Initial revision.
 * 
 *************************************************************************/

 /* Define the number of FYI parameters per allocation */
#ifdef _FYI_
#define FYI_ALLOC  5
#else
#define FYI_ALLOC  1
#endif /*_FYI_*/

/*************************************************************************
 *
 * Name:     FYI_init() - Initialize the cluster.
 *
 * Requires: MEM_init() called to initialize fast memory
 *
 * Modifies: The static FYI variables
 *
 * Effect:   Initializes the FYI cluster.  This operation must
 *           be performed before any calls to other cluster routines.
 *
 * Implementation:
 *
 *           Point the head of the used parameter list at nothing.
 *           Point the head of the free parameter list at nothing.
 *
 * Dependency:
 *
 * Uses:     Nothing
 *
 *************************************************************************/

  V_Void
FYI_init(VISE vise)
{
  /* Initially, the used parameter list has nothing in it */
  vise->fyi.firstParm = vise->fyi.freeParms = (FYIENT) NULL;
}

/*************************************************************************
 *
 * Name:     FYI_allocate() - Allocate a parameter
 *
 * Returns:  The parameter entry
 *
 * Requires: FYI_init()
 *
 * Modifies: The new parameter and the static FYI variables
 *
 * Effect:  
 *
 * Implementation:
 *
 *           If there is already a parameter allocated with this ID,
 *             return a pointer to it.
 *
 *           If there are no free parameters, try to make some.
 *           If unsuccessful, return NULL.
 *
 *           Get a free parameter
 *             set its ID to the specified ID,
 *             set its value to zero,
 *             set its routine address to NULL.
 *
 *           Prepend it to the allocated parameters list.
 *
 *           Return a pointer to it.
 *
 * Dependency:
 *
 * Uses:     MEM_alloc()
 *
 *************************************************************************/

  FYIENT
FYI_allocate(VISE vise, fyiid_t parameterId)
{
  FYIENT parm;

  /* If this parameter is allocated, return it */
  for (parm = vise->fyi.firstParm; parm; parm = parm->nextParm)
   if (parm->id == parameterId) return(parm);

  /* If no free parameters exist, try to make some */
  if (vise->fyi.freeParms == NULL &&
      (vise->fyi.freeParms = (FYIENT) MEM_alloclist(vise->memCfg, sizeof(fyient_t), FYI_ALLOC, FAST_MEM, "FYI_allocate")) == NULL)
    return(NULL);

  /* At least one free parameter now exists; get it */
  parm = vise->fyi.freeParms;
  vise->fyi.freeParms = vise->fyi.freeParms->nextParm;

  /* Initialize it */
  parm->id = parameterId;
  parm->value = 0;
  parm->routine = (V_Fcn)NULL;

  /* Put it at the beginning of the allocated parameters list */
  parm->nextParm = vise->fyi.firstParm;
  vise->fyi.firstParm = parm;

  /* Return the new parameter */
  return(parm);
}

/*************************************************************************
 *
 * Name:     FYI_writeMsg() - Send the parameters in a message
 *
 * Returns:  TRUE if successful, FALSE otherwise
 *
 * Requires: FYI_init() must have been called previously.  The message
 *           must be open for writing, and must have room remaining
 *           for the parameters.
 *
 * Modifies: The message
 *
 * Effect:   Sends the parameters in the specified message, as follows:
 *
 *           Size of parameter area  1 word
 *           For each parameter:
 *             Parameter ID          1 word
 *             Parameter value       1 word
 *
 *           The size of the parameter area, sent as the first word of
 *           this part of the message, does not include the size word
 *           itself.
 *
 * Implementation:
 *
 *           Send a word containing the size of the parameter area.
 *
 *           For each parameter in the used parameter list,
 *             Send its ID as the next word in the message,
 *             Send its value next in the message.
 *
 *           Return TRUE or FALSE.
 *
 * Dependency:
 *
 * Uses:     MSG_writeUns()
 *           MSG_writeInt()
 *
 *************************************************************************/

  V_Bool
FYI_writeMsg(VISE vise)
{
  FYIENT  parm;
  V_Bool  written;

  /* Send the size of the parameter part of the message */
  /* The superfluous and misleading cast of the difference of V_Uns types as V_Uns circumvents a bug in the M$ compiler */
  written = MSG_writeUns(vise->outgoing, (V_Uns) (FYI_msgSize(vise) - (V_Uns) VINTSIZEINMELS));

  /* Send the parameters */
  for (parm = vise->fyi.firstParm; written && parm; parm = parm->nextParm)
    written = MSG_writeInt(vise->outgoing, (V_Int) parm->id) && MSG_writeInt(vise->outgoing, (V_Int) parm->value);

  return(written);
}

/*************************************************************************
 *
 * Name:     FYI_msgSize() - Return the length of the FYI message
 *
 * Returns:  The length of the FYI portion of a message (what
 *           FYI_writeMsg would send)
 *
 * Requires: FYI_init()
 *
 * Modifies: 
 *
 * Effect:   Returns the size (in V_Int types) of the message segment
 *           which FYI_writeMsg() would send if called at this time.
 *
 * Implementation:
 *
 *           Count the parameters in the used parameter list.
 *           Multiply the result by the number of words FYI_writeMsg()
 *             sends for each parameter.
 *           Add one for the size word.
 *           Return the result.
 *
 * Dependency:
 *
 * Uses:     Nothing
 *
 *************************************************************************/

  V_Uns
FYI_msgSize(VISE vise)
{
  V_Uns   length = VINTSIZEINMELS;  /* Reserve one word for the size */
  FYIENT  parm;

  /* Count the parameters */
  for (parm = vise->fyi.firstParm; parm; parm = parm->nextParm)
    length += 2 * VINTSIZEINMELS;

  return(length);
}

