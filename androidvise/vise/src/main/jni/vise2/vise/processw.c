#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_alg.h"
#include "v_ki.h"
#include "v_wi.h"

/**************************************************************************
 *
 * Name:      processword -- Dynamic programming for one word
 *
 * Returns:   Nothing
 *
 * Requires:  The current time and jinframe must already be loaded
 *            into the tm cluster.
 *
 * Modifies:  word, *wordbestscore, *bestlastscore
 *
 * Effect:    Performs the word level dynamic programming calculation
 *            on a single word instance object for a single frame of
 *            acoustic parameter data.
 *
 * Implementation:
 *
 *            The word level dynamic programming function iterates
 *            over all the active kernels of the word.  The active
 *            kernels of a particular word are processed last to
 *            first in order to eliminate the need to save multiple
 *            copies of the kernels.  Each kernel inherits from its
 *            predecessor, except for the first kernel, which inherits
 *            from the seed of the source grammar node of the arc on
 *            which its word is located.  The seed information for the
 *            word is passed in the calling parameters.
 *
 *            Initialize word best score to V_INFINITY
 *
 *            If there is a last kernel (2 or more kernels)
 *              Process last kernel
 *              Save minimum best score for word at this point
 *                as minimum best score for last kernel
 *            else
 *              Process first (and last) kernel
 *              Save minimum best score for word at this point
 *                as minimum best score for last kernel
 *              Return
 *
 *            For the second-to-last to second active kernels in word
 *              Process kernel
 *
 *            Process first kernel
 *            Determine global minimum score for word
 *
 * Uses:      processkernel -- Do kernel dynamic programming
 *            ki_getscore -- Get last score in kernel
 *            wi_lastkernel -- Get last active kernel in word
 *            wi_previouskernel -- Get previous kernel in word
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\PROCESSW.C_V  $
 * 
 *    Rev 3.4   20 Mar 1992 12:48:20   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 3.3   29 Jan 1992 15:16:20   DCV
 * Changed basic types
 * 
 *    Rev 3.2   12 Nov 1991 15:11:22   DCV
 * Now accepts a tbp rather than a tbn as its third argument, and passes
 *   it to processkernel().
 * 
 *    Rev 3.1   12 Jun 1991 14:24:46   DCV
 * Eliminated inclusion of v_tbn.h
 * 
 *    Rev 3.0   13 Sep 1990 13:54:22   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   24 Jul 1990 14:14:02   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   30 May 1990  9:30:54   DCV
 * Initial revision.
 *
 *************************************************************************/

  V_Void
processword(VISE vise, wi word, V_Int inscore, tbp intbpath, V_Int * wordbestscore, V_Int * bestlastscore)
  /* wi      word          - The word instance to be processed     */
  /* V_Int   inscore       - The score that the word inherits      */
  /* tbp     intbpath      - Traceback path for inscore            */
  /* V_Int * wordbestscore - The minimum score over the whole word */
  /* V_Int * bestlastscore - The minimum score for the last kernel */
{
  ki     kernel;             /* The current kernel in the word   */
  ki     predecessorkernel;  /* The kernel before it, in time    */
  V_Int  kernelscore;        /* Score of Self-Loop of Kernel TBP */
  ki     firstkernel;        /* First kernel in word             */
  KND    loopknode;          /* Pointer to last knode in kernel  */

  *wordbestscore = V_INFINITY;  /* It can only get better */

  firstkernel = word->wifirstkernel; /* Note: assumes at least 1 kernel */
  kernel = word->wiactiveregion;
  loopknode = kernel->kilastknode;
  kernelscore = loopknode->kiscore;
  predecessorkernel = kernel - 1; 

  if (predecessorkernel < firstkernel) {
    /* Only one kernel in the word */
    processkernel(vise, kernel, kernelscore, inscore, intbpath, wordbestscore);
    *bestlastscore = *wordbestscore;
  } else {
    /* More than one kernel in the word */
    loopknode = predecessorkernel->kilastknode;
    processkernel(vise, kernel, kernelscore, loopknode->kiscore, loopknode->kitbpath, wordbestscore);

    *bestlastscore = *wordbestscore;
    kernel = predecessorkernel;
    kernelscore = loopknode->kiscore;

    /* Loop over all kernels but last and first, causing each to inherit from its predecessor in time */
    while (firstkernel <= --predecessorkernel) {
      loopknode = predecessorkernel->kilastknode;
      processkernel(vise, kernel, kernelscore, loopknode->kiscore, loopknode->kitbpath, wordbestscore);
      kernel = predecessorkernel;
      kernelscore = loopknode->kiscore;
    }

    /* "kernel" is now the first kernel in the word.  Process it, inheriting
       the score and traceback node provided in the calling parameters */
    processkernel(vise, kernel, kernelscore, inscore, intbpath, wordbestscore);
  }
}
