#include "pthr.h"
#include "vise.h"
#include "v_vise.h"
#include "v_err.h"
#include "v_ai.h"
#include "v_mem.h"
#include "v_ni.h"
#include "v_wi.h"
#include "v_wm.h"

/**************************************************************************
 *
 * Name:   ai -- Arc Instance Cluster
 *
 * Description: 
 *     The arc instance cluster implements the  abstract arc
 *     instance objects which, along with the node instance
 *     objects, populate the active grammar.  An arc instance
 *     represents a directed transition between node instances
 *     in the active grammar.  This transition is triggered by
 *     any word in the set that is associated with the arc.
 *
 *     An arc instance, therefore, contains a source node, a
 *     destination node, and a set of word instances.
 *
 * Operations:
 *
 * 1.  result = ai_init(vise, wicount) -- Initialize the arc instance cluster.
 *
 * 2.  ai_arcinit(arc, source, destination)  -- Initialize an  arc
 *   instance. Associate  an arc  with  a  source  node  and a
 *   destination node, and remove all words from the arc's list.
 *
 * 3.  node = ai_sourcenode(arc) -- Return the node instance of  the
 *   source node for the given arc.
 *
 * 4.  node = ai_destinationnode(arc) -- Return the node instance of
 *   the destination node for the given arc.
 *
 * 5.  outcome = ai_addword(vise, arc, wordmodel) -- Add a  word  instance
 *   (replicated from the model) to the arc's word list.
 *
 * 6.  word =  ai_firstword(arc)  -- Return  the  first  word  instance
 *   associated with the given arc.
 *
 * 7.  word = ai_lastword(arc) -- Return the last word instance
 *   associated with the given arc.
 *
 * 8.  ai_activate(arc) -- Activate an arc instance and all its word
 *   instances
 *
 * 9.  ai_deactivate(arc,  word) --  Deactivate  the  given word
 *   instance on the given arc.
 *
 * 10.     activecount = ai_activeCount(arc, word) -- How many words on
 *   the arc are active?
 *
 *     V_Bool result  -- Was memory allocated for the wi's?
 *     V_ULong wicount  -- The number of word instances
 *     ai   arc   -- An arc instance object.
 *     wi   word   -- A word instance object.
 *     ni   node   -- A node instance object.
 *     ni   source  -- Source node instance.
 *     ni   destination -- Destination node instance.
 *     wm   wordmodel -- A word model object.
 *     V_Int  outcome  -- Was a word instance added to an arc?
 *     V_Bool active  -- Are any words on the arc active?
 *
 * Computer: TMS320C30 and iAPX86
 *
 * Unit Test: ait.ct
 *
 * $Log:   C:\USR\SRC\VISE\C\VCS\AI.C_V  $
 * 
 *    Rev 3.7   20 Mar 1992 12:06:52   DCV
 * Changed to V_xxx types to avoid conflicts
 * 
 *    Rev 3.6   29 Jan 1992 13:35:00   DCV
 * Changed basic types
 * 
 *    Rev 3.5   12 Nov 1991 18:12:44   DCV
 * Changed ai_addword() to return standard VISE status codes.
 * 
 *    Rev 3.4   08 Oct 1991 09:19:54   DCV
 * Changed ai_activate() to deal gracefully with arcs having no active words.
 * 
 *    Rev 3.3   18 Jul 1991 15:42:42   DCV
 * Implemented dynamic memory allocation
 * 
 *    Rev 3.2   20 Dec 1990 14:01:00   DCV
 * Replaced ai_active() with ai_activeCount()
 * 
 *    Rev 3.2   20 Dec 1990 13:53:14   DCV
 * Eliminated useless search for km in wm_deallocate()
 * 
 *    Rev 3.1   07 Nov 1990 16:01:52   DCV
 * Modified MEM_xxx calls for new mem.c
 * 
 *    Rev 3.0   13 Sep 1990 13:34:10   DCV
 * First version with individual header files
 * 
 *    Rev 1.1   11 Jul 1990 11:23:44   DCV
 * Function declarations changed to conform to ANSI standard
 * 
 *    Rev 1.0   29 May 1990 17:25:28   DCV
 * Initial revision.
 * 
 *************************************************************************/


/*************************************************************************
 *
 * Name:   ai_init() - Initialize the arc instance cluster.
 *
 * Calling Sequence:
 *
 *     result = ai_init(vise, wicount);
 *
 *     V_Bool result  -- Was memory allocated for the wi's?
 *                  VISE    vise        --  VISE descriptor
 *     V_ULong wicount  -- The number of word instances
 *
 * Returns:  V_Void
 *
 * Requires: Memory for arc instance objects must have been deallocated
 *
 * Modifies: The arc instance cluster.
 *
 * Effect:  Initializes the arc instance cluster.  This  operation
 *     must be performed before each activation of a grammar.
 *     It reclaims all word instance objects so that the can
 *     be reused for the next set of arc instances.
 *
 * Implementation:
 *
 *     Allocate fast memory for word instances, or return FALSE.
 *     Set first word instance pointer to origin of new memory area.
 *     Set last word instance pointer to end of new memory area.
 *
 * Dependency:
 *
 * Uses:   MEM_alloc()
 *
 *************************************************************************/

 V_Bool
ai_init(VISE vise, V_ULong wicount)
 {
 vise->wiPool.firstwi = (wi) MEM_alloc(vise->memCfg, WI_SIZE, wicount, FAST_MEM, "ai_init wi");
 if (vise->wiPool.firstwi == NULL && wicount)
   return(FALSE);
 vise->wiPool.lastwi = vise->wiPool.firstwi + wicount;
 return(TRUE);
 }

/*************************************************************************
 *
 * Name:    ai_arcinit - Associate arc and node instances
 *
 * Calling Sequence:
 *
 *      ai_arcinit(arc, srcnode, destnode)
 *
 *      ai arc;   -- An arc instance object.
 *      ni srcnode;  -- The source node instance.
 *      ni destnode; -- The destination node instance.
 *
 * Returns:
 *
 * Requires:
 *
 * Modifies: The arc instance.
 *
 * Effect:
 *
 *     Associates the given arc instance object with the given
 *     source  and  destination node instance objects.  Remove
 *     all word instances from the arc's list.
 *
 * Implementation:
 *
 *     src node ==> source node field of arc
 *     dst node ==> destination node field of arc
 *     clear arc instance word pointers
 *
 * Uses:
 *
 ************************************************************************/

 V_Void
ai_arcinit(ai arc, ni srcnode, ni destnode)
 {
  /* Associate the nodes with the arc */
 arc->aisource = srcnode;
 arc->aidestination = destnode;

  /* Initialize the words on the arc */
 arc->aifirstword = arc->ailastword = NULL;

  /* Set word to be inactive with 0 word instances active */
 arc->aifullyactive = FALSE;
 arc->aiactivecount = 0;
 }

/****************************************************************************
 *
 * Name:   ai_sourcenode - Get source node for arc instance
 *
 * Calling Sequence:
 *
 *     node = ai_sourcenode(arc)
 *
 *     ai arc -- arc instance object
 *     ni node -- source node instance of the arc
 *
 * Returns:  A node instance object corresponding to the source node
 *     of the given arc instance object.
 *
 * Requires: The arc instance has been initialized.
 *
 * Modifies:
 *
 * Effect:  Returns the node instance corresponding to  the  source
 *     node of the given arc instance.
 *
 * Implementation:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 ni
ai_sourcenode(ai arc)
 {
 return (arc->aisource);
 }

#endif

/***************************************************************************
 *
 * Name:   ai_destinationnode - Get destination node for  arc
 *     instance
 *
 * Calling Sequence:
 *
 *     node = ai_destinationnode(arc)
 *
 *     ai arc -- arc instance object
 *     ni node -- destination node instance of the arc
 *
 * Returns:
 *
 *     A node instance object corresponding to the destination
 *     node of the given arc instance object.
 *
 * Requires:
 *
 * The arc instance has been initialized.
 *
 * Modifies:
 *
 *
 * Effect:
 *
 *     Returns the  node  instance  corresponding   to  the
 *     destination node of the given arc instance.
 *
 * Implementation:
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 ni
ai_destinationnode(ai arc)
 {
 return(arc->aidestination);
 }

#endif

/*************************************************************************
 *
 * Name:    ai_addword - Add word to arc instance
 *
 * Calling Sequence:
 *
 *     ai_addword(vise, arc, wordmodel)
 *
 *     ai arc     -- The arc instance object which
 *            is to have a word added to it
 *                  VISE vise   -- The VISE descriptor
 *     wm  word -- The word model upon which the new
 *         word instance is to be patterned
 *
 * Returns:  TRUE if word was successfully added, or FALSE if  there
 *     is no room for the additional word or if the word model
 *     could not be replicated.
 *
 * Requires: The arc instance and the arc instance cluster have been
 *     initialized. All invocations of ai_addword for a
 *     particular arc must be done before any invocations of
 *     ai_addword are performed for a different arc.
 *
 * Modifies: arc instance
 *
 * Effect:  Adds the given word instance  to  the  list  of  words
 *     belonging to the given arc instance.
 *
 * Implementation:
 *
 *     If an unallocated word instance exists,
 *      Get next word instance from the word instance array.
 *      Place word instance on the arc instance's list of words.
 *      Replicate the word model.
 *      Return TRUE.
 *     Else
 *      Return FALSE.
 *
 * Uses:
 *     wi_replicate -- make word instance from model (wi.c)
 *
 ************************************************************************/

 V_Int
ai_addword(VISE vise, ai arc, wm wordmodel)
 {
 wi   wordinst;    /* word instance */

 if (vise->wiPool.firstwi < vise->wiPool.lastwi) {
   /* A free word instance exists */
  wordinst = vise->wiPool.firstwi++;

   /* Place the word instance into the arc instance object */
  if (arc->aifirstword == NULL)
   arc->aifirstword = wordinst;

  arc->ailastword = wordinst;

   /* Replicate the word model and return status */
  return(wi_replicate(vise, wordinst, wordmodel));
  }
 else
   /* No more free word instances to allocate */
  return(BADWORDCOUNT);
 }

/***************************************************************************
 *
 * Name:    ai_firstword - Get first word instance on an arc
 *
 * Calling Sequence:
 *
 *     word = ai_firstword(arc)
 *
 *     ai arc -- arc instance object
 *     wi word -- first word instance object on arc
 *
 * Returns:
 *
 *     The first word instance on the given arc instance,  or
 *     NULL if there are no word instances on the arc.
 *
 * Requires:
 *
 *     The arc instance and the arc instance cluster have been
 *     initialized.
 *
 * Modifies:
 *
 * Effect:
 *
 *     Returns the first word instance associated with the arc
 *     instance,  if there are any.  Updates the arc instance
 *     word iterator such that the next call to ai_nword will
 *     return  the  second word instance in the arc instance
 *     object.
 *
 * Implementation:
 *
 *     set currentword = firstword+1
 *     return firstword
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 wi
ai_firstword(ai arc)
 {
 return(arc->aifirstword);
 }

 wi
ai_lastword(ai arc)
 {
 return( arc->ailastword );
 }

#endif
 
/**************************************************************************
 *
 * Name:    ai_activate - Activate all the words on an arc
 *
 * Calling Sequence:
 *
 *     ai_activate(arc);
 *
 *     ai arc; -- A arc instance object
 *
 * Returns:
 *
 * Requires: The arc instance and the arc instance cluster have been
 *     initialized.
 *
 * Modifies: The  arc  instance  object  and its  associated word
 *     instances.
 *
 * Effect:  This function activates all of the inactive  words  on
 *     the given arc.  Words which are already active are left
 *     alone.
 *
 * Implementation:
 *
 *     If there are any inactive words on the arc
 *      For all words on the arc
 *       If the word is inactive
 *        Wake its first kernel
 *      Set activecount of arc to number of words on arc
 *
 * Uses:
 *
 **************************************************************************/

 V_Void
ai_activate(ai arc)
 {
 wi  currentword, lastword;
 V_Int wordcount;

  /* Don't do anything unless it's necessary */
 if (!arc->aifullyactive)
  {
  wordcount = 0;
  if ((lastword = arc->ailastword) != NULL)
   for ( currentword = arc->aifirstword;
     currentword <= lastword;
     wordcount++, currentword++ )
    if (wi_inactive(currentword))
     wi_wakekernel(currentword);

  arc->aiactivecount = wordcount;
  arc->aifullyactive = TRUE;
  }
 }

/************************************************************************
 *
 * Name:     ai_deactivate - Deactivate a word on an arc.
 *
 * Calling Sequence:
 *
 *     ai_deactivate(arc);
 *
 *     ai arc; -- An arc instance object
 *
 * Returns:
 *
 * Requires: The specified word instance must really be a member  of
 *     the  given arc instance's word list. The arc and word
 *     must be appropriately initialized.
 *
 * Modifies: arc, word
 *
 * Effect:  This function deactivates the  specified word on  the
 *     given arc.
 *
 * Implementation:
 *
 *     deactivate word
 *     decrement active word count
 *     set fully active flag to FALSE
 *
 * Uses:
 *
 *************************************************************************/

#ifdef NOMACROS

 V_Void
ai_deactivate(ai arc)
 {
 arc->aifullyactive = FALSE;
 --arc->aiactivecount;
 }

#endif

/**************************************************************************
 *
 * Name:   ai_activeCount - How many active words are on this arc?
 *
 * Calling Sequence:
 *
 *     activeCount = ai_activeCount(arc);
 *
 *     V_Int activeCount; -- How many active words are on this arc?
 *     ai  arc;    -- An arc instance object.
 *
 * Returns:  The number of active words on the arc.
 *
 * Requires: The arc must be properly initialized.
 *
 * Modifies:
 *
 * Effect:  
 *
 * Implementation:
 *
 *     Return the number of active words on the arc.
 *
 * Uses:
 *
 **************************************************************************/

#ifdef NOMACROS

 V_Int
ai_activeCount(ai arc)
 {
 return(arc->aiactivecount);
 }

#endif

