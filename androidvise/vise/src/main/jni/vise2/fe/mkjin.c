/* mkjin.c - a creator of jin frames
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Produces frames of derived jin features from cepstral input frames.  These
 * frames always contain a single vector consisting of the cepstrum and
 * a single (60 ms) cepstral difference.  The output is the traditional 16 byte
 * JIN vector. 
 *
 * ASSUMPTIONS
 * VISE has a 50 Hertz analysis frame rate
 * 
 * HISTORY
 *  9-May-97  Craig A. Vanderborgh (craigv@verbex.com)
 *      Derived from feat.c
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: mkjin.c[1.0] Tue Oct  7 23:34:51 1997 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "floorfix.h"
#include "c.h"
#include "cepattr.h"
#include "mkjin.h"
#include "jinattr.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "utils.h"
#include "vbx.h"
#include <assert.h>

#ifdef DEBUG
#define VBX_DEBUG(P) (P)
/*#define VBX_DEBUG(P) */
#else
#define VBX_DEBUG(P)
#endif

/* The Class of MKJINATTR (MKJIN Attributes) Interface Objects */
static iclass_t IMKJINATTR_Class = {IMKJINATTRID};

/* The Class of IMKJIN Interface Objects */
static iclass_t IMKJIN_Class = {IMKJINID};

/* The MKJIN Interface */
typedef struct imkjin_s {
  iface_t  iface;
} imkjin_t;

/* Default lag for cepstral difference (60 ms) */
#define DFLT_CLASS_SIZE  50
#define DFLT_FRAME_BYTES 204
#define DFLT_LD_WIDTH    3
#define DFLT_TOLERANCE   512

/* MKJIN Attributes */
typedef struct mkjinattr_s {
  UInt     classSize;    /* The size of the CLASS of JIN frame objects */
  Int      frameBytes;   /* Number of bytes in a sample frame object */
  Int      ldW;          /* Lag for (60ms) cepstral difference */
  SN       fwdTol;       /* The tolerance for SNs out of range forward */
  SN       bwdTol;       /* The tolerance for SNs out of range backward */
} mkjinattr_t, * MKJINATTR;

/*---------------------------------------------------------------------------*
 * The Intermediate Derived Feature Frame Buffer
 * The derived feature frame queue buffers cepstral and delta-cepstral data
 * locally so that the derived features can be produced efficiently.
 *---------------------------------------------------------------------------*/

#define IDX(I,MASK)          ((I) & (MASK))
#define MKJIN_resetQueue(Q)  ((Q)->count = 0)
#define MKJIN_sizeQueue(Q)   ((Q)->mask + 1)

#ifdef FIXED_POINT
# define JNORM(C)            (((Int)(C)) + 128)     /* jin converter */
# define THREE_FOURTHS(A)    ((3 * (A)) >> 2)       /* d-cepstr normalizer - 3/4 value */
#else  /* Floating Point */
# define JNORM(C)            ((Int)((C) + (Float)0.5) + 128)
# define THREE_FOURTHS(A)    ((Float)0.75 * (A))
#endif  /* FIXED_POINT */


/* Feature Frame */
typedef struct ffr_s {
  SN        sn;
  OBJT      cep;
} ffr_t,  * FFR;

/* Feature Frame Buffer */
typedef struct ffq_s {
  IMemory * imem;
  FFR       frame;
  Int       mask;
  Int       count;
} ffq_t, * FFQ;


/* An MKJIN descriptor */
typedef struct mkjin_s {
  IMemory * imem;       /* The memory allocator */
  ISRC      isrc;       /* MKJIN's source interface (output) */
  ISNK      isnk;       /* MKJIN's sink interface (input) */
  IMKJIN    imkjin;     /* MKJIN's direct interface */
  IGEN      igenattr;   /* Interface to the feature attributes block */
  Int       classSize;  /* The size of the CLASS of feature frame OBJTs */
  Int       frameBytes; /* Number of bytes in a frame */
  CLASS     framePool;  /* The CLASS of feature frame OBJTs */
  FFQ       ffq;        /* The local intermediate feature frame queue */
  SN        seqNum;     /* The previous frame read by the feature sink */
  SN        prevSeqNum; /* The previous sequence number produced */
  SN        snOffset;   /* The audio byte SN start of the current sequence */
  dir_t     readDir;    /* The direction of the previous read by the sink */
  Int       ldW;        /* Lag for (60ms) cepstral difference */
  SN        fwdTol;     /* The tolerance for SNs out of range forward */
  SN        bwdTol;     /* The tolerance for SNs out of range backward */
  SN        lastSN;     /* The last byte SN returned */
  Int       maxW;       /* Maximum lead/lag */
  Int       minWindow;  /* Number of frames necessary to compute features */
  Int       cepVecLen;  /* The number of coefficients in a cepstral vector (in) */
  Int       numFeats;   /* The number of feature vectors in a frame (out) */
  Int     * featLen;    /* The lengths of the feature vectors (out) */
  Val     * featFrame;  /* To compute JIN frame during dequeue operation */
} mkjin_t, * MKJIN;


/* Private functions */

static IMKJIN  IMKJIN_create(IGEN igen);
static Bool    MKJIN_annihilate(IGEN igen);
static Bool    MKJIN_initialize(IGEN igen, IATTR iattr);
static Bool    MKJIN_init(MKJIN mkjin, IFACE iface);
static Bool    MKJIN_setAttributes(ISNK isnk, IATTR iattr);
static Bool    MKJIN_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool    MKJIN_frameAvailable(ISRC isrc, dir_t dir, SN * snP);
static FFQ     MKJIN_createQueue(IMemory * imem, Int size, Int frSize);
static void    MKJIN_annihilateQueue(FFQ ffq);
static void    MKJIN_enqueue(MKJIN mkjin, dir_t dir, SN sn, OBJT cep);
static SN      MKJIN_dequeue(MKJIN mkjin, SN sn, OBJT obj);

static IMKJINATTR  IMKJINATTR_create(IGEN igen);


/*** MKJIN Methods ***/

IGEN
MKJIN_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a derived-feature extractor
 *---------------------------------------------------------------------------*/
{
  MKJIN      mkjin = (MKJIN) imem->Calloc(imem, 1, sizeof(mkjin_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) mkjin);
  ISNK       isnk = ISNK_create(igen);
  ISRC       isrc = ISRC_create(igen);
  IGEN       igenattr = JINATTR_create(imem);
  IMKJIN     imkjin = NULL;
  IATTR      iattr;

  imkjin = IMKJIN_create(igen);

  /* Initialize the object */
  mkjin->imem = imem;
  mkjin->isnk = isnk;
  mkjin->isrc = isrc;
  mkjin->imkjin = imkjin;
  mkjin->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, MKJIN_annihilate);
  IGEN_setInitializer(igen, MKJIN_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the sink interface */
  ISNK_setSetAttributes(isnk, MKJIN_setAttributes);

  /* Set up the source interface */
  ISRC_setGetFrame(isrc, MKJIN_getFrame);
  ISRC_setFrameAvailable(isrc, MKJIN_frameAvailable);
  iattr = (IATTR) IGEN_interface(igenattr, IJINATTRID);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, IJINATTRID));

  return(igen);
}


/*** GENERIC METHODS ***/

static Bool
MKJIN_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a mkjin object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  MKJIN  mkjin = (MKJIN) IFACE_object((IFACE) igen);

  if (mkjin->featLen)
    mkjin->imem->Free(mkjin->imem, mkjin->featLen);
  if (mkjin->featFrame)
    mkjin->imem->Free(mkjin->imem, mkjin->featFrame);

  /* Free the CLASS of feature frame OBJTs */
  if (!CLASS_destroy(mkjin->framePool, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Annihilate the internal feature frame queue */
  if (mkjin->ffq) MKJIN_annihilateQueue(mkjin->ffq);

  /* Annihilate the feature attributes object */
  if (!IGEN_annihilate(mkjin->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) mkjin->igenattr));
    return(FALSE);
  }
}


static Bool
MKJIN_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a feature maker according to a mkjin attributes specification
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  MKJIN      mkjin = (MKJIN) IFACE_object((IFACE) igen);
  ICEPATTR   icepattr;

  VBX_DEBUG(VBX_print("MKJIN: REINITIALIZING\n"));

  /* Copy in the mkjin attributes */
  IMKJINATTR_getClassSize((IMKJINATTR) iattr, &mkjin->classSize);
  IMKJINATTR_getFrameBytes((IMKJINATTR) iattr, &mkjin->frameBytes);
  IMKJINATTR_getLDW((IMKJINATTR) iattr, &mkjin->ldW);
  IMKJINATTR_getFwdTol((IMKJINATTR) iattr, &mkjin->fwdTol);
  IMKJINATTR_getBwdTol((IMKJINATTR) iattr, &mkjin->bwdTol);

  /* Compute the derived attributes */
  mkjin->maxW = mkjin->ldW;
  mkjin->minWindow = mkjin->maxW + 1;

  /* Get the cepVecLen from the sink (input) interface */
  icepattr = (ICEPATTR) ISNK_attributes(mkjin->isnk);
  ICEPATTR_getVecLen(icepattr, &mkjin->cepVecLen);

  /* Do the actual initialization */
  return(MKJIN_init(mkjin, (IFACE) igen));
}

static Bool
MKJIN_init(MKJIN mkjin, IFACE iface)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a feature maker
 * Invoking this before MKJIN gets valid attributes does nothing.
 *---------------------------------------------------------------------------*/
{
  IJINATTR   ijinattr;
  UInt       totalLen;
  ISNK       sink;

  assert(mkjin && iface);

  /* If MKJIN has no valid attributes, do nothing */
  if (!mkjin->minWindow) return(TRUE);

  /* Make seqNum so negative that +/- mkjin->maxW leaves it negative */
  mkjin->seqNum = - mkjin->minWindow;
  mkjin->readDir = UNTOWARD;

  /* Create a local intermediate feature frame queue */
  if (mkjin->ffq) MKJIN_annihilateQueue(mkjin->ffq);
  mkjin->ffq = MKJIN_createQueue(mkjin->imem, mkjin->minWindow, mkjin->cepVecLen);

  /* Remove any old feature attributes */
  if (mkjin->featLen)
    mkjin->imem->Free(mkjin->imem, mkjin->featLen);
  if (mkjin->featFrame)
    mkjin->imem->Free(mkjin->imem, mkjin->featFrame);

  /* Derive the new feature attributes */
  mkjin->numFeats = NUM_FEATURES;
  mkjin->featLen = (Int *) mkjin->imem->Calloc(mkjin->imem, mkjin->numFeats, sizeof(Int), OOPS_MEMORY);
  totalLen = mkjin->featLen[CEP_FEAT] = mkjin->cepVecLen;
  totalLen += mkjin->featLen[DCEP_FEAT] = mkjin->cepVecLen;
  mkjin->featFrame = (Val *) mkjin->imem->Calloc(mkjin->imem, totalLen, sizeof(Val), OOPS_MEMORY);

  /* Create a new CLASS of feature frame OBJTs if none exists or the current one is obsolete */
  if (mkjin->framePool && (totalLen != CLASS_objectSize(mkjin->framePool) || mkjin->classSize != CLASS_size(mkjin->framePool))) {
    CLASS_destroy(mkjin->framePool, NULL, NULL);
    mkjin->framePool = NULL;
  }
  if (!mkjin->framePool)
    mkjin->framePool = CLASS_create(mkjin->imem, mkjin->classSize, totalLen, NULL, NULL);
  if (!mkjin->framePool) {
    IFACE_setError(iface, EXC_CANTCREATECLASS);
    return(FALSE);
  }

  /* Get the feature attributes interface (from ISRC) */
  if (!ISRC_getAttributes(mkjin->isrc, (IATTR *) &ijinattr)) {
    IFACE_setError(iface, IFACE_error((IFACE) mkjin->isrc));
    return(FALSE);
  }

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(mkjin->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError(iface, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the feature attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, (IATTR) ijinattr)) {
    /* If successfully passed, return TRUE */
    return(TRUE);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError(iface, IFACE_error((IFACE) sink));
    return(FALSE);
  }
}


/*** SOURCE AND SINK METHODS ***/

static Bool
MKJIN_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Import cepstral attributes via the sink (input) interface.  This function
 * supplements the default ISNK_setAttributes() function, which calls this
 * first and then sets the attributes of isnk if the call is successful.
 *---------------------------------------------------------------------------*/
{
  MKJIN  mkjin;
  Int    cepVecLen;

  assert(isnk && iattr);

  mkjin = (MKJIN) IFACE_object((IFACE) isnk);

  /* Get the length of the cepstral vector from the cepstral attributes */
  if (!ICEPATTR_getVecLen((ICEPATTR) iattr, &cepVecLen)) {
    IFACE_setError((IFACE) isnk, IFACE_error((IFACE) iattr));
    return(FALSE);
  }

  /* Re-initialize if cepVecLen has changed */
  if (cepVecLen != mkjin->cepVecLen) {
    mkjin->cepVecLen = cepVecLen;
    return(MKJIN_init(mkjin, (IFACE) isnk));
  } else {
    return(TRUE);
  }
}


static Bool
MKJIN_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * objP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a feature frame via the source (output) interface.
 * Extracts feature vectors from the feature frame queue.  Since
 * feature extraction has a lookahead of mkfeat->maxW frames, this function
 * actually extracts results for *snP - mkfeat->maxW.  Since feature
 * extraction also has a lookback of mkfeat->maxW frames, it cannot yield
 * valid results until it puts at least mkfeat->minWindow frames in the queue.
 *---------------------------------------------------------------------------*/
{
  MKJIN    mkjin;
  ISRC     src;       /* The cepstral source */
  SN       snReq;     /* The SN requested from the cepstral source */
  SN       snRead;    /* The SN read from the cepstral source */
  OBJT     cepIn;     /* The cepstral frame object gotten from the source */
  Int      exc, i;
  FFQ      ffq;
  Int      mask;

  assert(isrc && snP);

  mkjin = ((MKJIN) IFACE_object((IFACE) isrc));
  ffq = mkjin->ffq;
  mask = ffq->mask;

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(mkjin->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Check to see if a frame is available from the source */
  snRead = -1;
  if (!ISRC_frameAvailable(src, FORWARD, &snRead)) {
    exc = IFACE_error((IFACE) src);
    IFACE_setError((IFACE) mkjin->isrc, exc);
    if (exc == EXC_EOU) {
      /*!! Not yet sure how to handle EOU, set SN_MAX?? !!*/
        IFACE_setError((IFACE) isrc, EXC_EOU);
       *snP = SN_MAX;
        return(FALSE);
      } else {
        SEVERE_BRA_ "MKJIN_getFrame: unexpected exception %d getting audio frame\n", exc _KET;
      }
      return(FALSE);
    }

/*---------------------------------------------------------------------------*
 * If there is a frame available, but not requested, get out
 * NOTE: If we are unprimed, we only report on the availability of a single
 *       frame from the source, assuming that if the source can produce one 
 *       frame, the additional 3 frames required to produce a JIN frame could
 *       also be obtained.  This avoids producing an unwanted frame (which 
 *       would be discarded) just to see if a frame is available!
 *---------------------------------------------------------------------------*/
  if (objP == NULL) {
    if (mkjin->seqNum < 0) {
      *snP = snRead;
       return TRUE;
    } else {
      *snP = ffq->frame[IDX(mkjin->seqNum - mkjin->ldW, mask)].sn;
       return TRUE;
    }
  }

  /* Get the cepstral frame(s) needed to derive the feature frame */

  /* If this request is for next SN, get next frame in specified direction */
  VBX_DEBUG(VBX_print("MKJIN_getFrame: dir %d *snP "QuadFmt(-19)" seqNum "QuadFmt(-19)"\n", dir, *snP, mkjin->seqNum));
  if ((*snP < 0 || *snP == ffq->frame[IDX(mkjin->seqNum - mkjin->ldW, mask)].sn) &&
      mkjin->ffq->count >= mkjin->minWindow) {

    /* Set the SN of the "left" frame, to be read from the queue */
    if (*snP < 0)
     *snP =  ffq->frame[IDX(mkjin->seqNum - mkjin->ldW, mask)].sn;

    /* Always request the next frame forward from the source */
    snRead = -1;
    if (!ISRC_getFrame(src, dir, &snRead, &cepIn)) {
      IFACE_setError((IFACE) isrc, IFACE_error((IFACE) src));
      return(FALSE);
    }
    mkjin->seqNum++;

    /* Looks like we should explicitly handle EOU */
    if (snRead == SN_MAX) {
      IFACE_setError((IFACE) isrc, EXC_EOU);
     *snP = SN_MAX;
      return(FALSE);
    }

    /* Put it in the queue */
    MKJIN_enqueue(mkjin, dir, snRead, cepIn);

  /* Otherwise, prime the queue from scratch */
  } else {
    VBX_DEBUG(VBX_print("MKJIN: REPRIMING THE QUEUE (count %d), first frame read will be "QuadFmt(-19)"\n",
              mkjin->ffq->count, snRead));
    MKJIN_resetQueue(mkjin->ffq);

    mkjin->readDir = dir;
    /* Read it */
    VBX_DEBUG(VBX_print("MKJIN: Calling ISRC_getFrame for SN "QuadFmt(-19)"\n", snRead));
    if (!ISRC_getFrame(src, dir, &snRead, &cepIn)) {
      IFACE_setError((IFACE) isrc, IFACE_error((IFACE) src));
      VBX_print("MKJIN: ERROR - ISRC_getFrame FAILS for SN "QuadFmt(-19)"\n", snRead);
      return(FALSE);
    }
    /* Put it in the queue */
    MKJIN_enqueue(mkjin, dir, snRead, cepIn);

    /* Set the SN of the "center" frame, to be read from the queue */
    mkjin->seqNum = 0;
    mkjin->snOffset = *snP = snRead;
    VBX_DEBUG(VBX_print("MKJIN: snRead: "QuadFmt(-19)" snOffset: "QuadFmt(-19)"\n", snRead, mkjin->snOffset));

    /* Read the rest of the frames needed to derive the center frame's features */
    for (i = 1; i < mkjin->minWindow; i++) {
      snReq = snRead + dir * mkjin->frameBytes;
      snRead = -1;
      VBX_DEBUG(VBX_print("MKJIN: PRIME1: snRead "QuadFmt(-19)"\n", snRead));
      if (!ISRC_getFrame(src, dir, &snRead, &cepIn)) {
        IFACE_setError((IFACE) isrc, IFACE_error((IFACE) src));
        VBX_print("MKJIN: ERROR - ISRC_getFrame FAILS for SN "QuadFmt(-19)"\n", snRead);
        return(FALSE);
      }
      if (snRead != snReq)
        VBX_print("MKJIN: WARNING - SEQ NUM GAP "QuadFmt(-19)" != "QuadFmt(-19)" during prime\nn",
                  snRead, snReq);

      VBX_DEBUG(VBX_print("MKJIN: PRIME2: snRead "QuadFmt(-19)"\n", snRead));
      MKJIN_enqueue(mkjin, dir, snRead, cepIn);
    }
  }

  /* Construct the feature frame if requested */
  if (objP) {
    *objP = OBJT_new(mkjin->framePool);
    if (!*objP) {
      IFACE_setError((IFACE) isrc, EXC_FEATCLASSEMPTY);
      return(FALSE);
    }
    VBX_DEBUG(VBX_print("MKJIN: dequeue: "QuadFmt(-19)"\n", mkjin->seqNum + dir * mkjin->maxW));
   *snP = MKJIN_dequeue(mkjin, mkjin->seqNum + dir * mkjin->maxW, *objP);
  }

  VBX_DEBUG(VBX_print("MKJIN_getFrame returns TRUE for "QuadFmt(-19)"\n", *snP));
  return(TRUE);
}

static Bool
MKJIN_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an OBJT via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  MKJIN    mkjin;
  ISRC     src;       /* The cepstral source */

  assert(isrc && snP);

  mkjin = ((MKJIN) IFACE_object((IFACE) isrc));
  src = ISNK_source(mkjin->isnk);
  return(ISRC_getFrame(src, dir, snP, NULL));
}


static FFQ
MKJIN_createQueue(IMemory * imem, Int size, Int frSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a local feature frame queue
 *---------------------------------------------------------------------------*/
{
  Int      actualSize;
  FFQ      ffq = (FFQ) imem->Calloc(imem, 1, sizeof(ffq_t), OOPS_MEMORY);

  assert(size > 0);

  actualSize = nextPowerOfTwo(size);
  ffq->imem = imem;
  ffq->mask = actualSize - 1;
  ffq->frame = (FFR) imem->Calloc(imem, actualSize, sizeof(ffr_t), OOPS_MEMORY);

  return(ffq);
}


static void
MKJIN_annihilateQueue(FFQ ffq)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate the local feature frame queue
 *---------------------------------------------------------------------------*/
{
  if (ffq) {
    if (ffq->frame) {
      do if (ffq->frame[ffq->mask].cep) OBJT_free(ffq->frame[ffq->mask].cep);
      while (ffq->mask--);
      ffq->imem->Free(ffq->imem, ffq->frame);
    }
    ffq->imem->Free(ffq->imem, ffq);
  }
}


static void
MKJIN_enqueue(MKJIN mkjin, dir_t dir, SN sn, OBJT cep)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert derived features in the local intermediate feature frame queue
 *---------------------------------------------------------------------------*/
{
  FFQ      ffq = (FFQ) mkjin->ffq;
  FFR      ffr = &ffq->frame[IDX(mkjin->ffq->count, ffq->mask)];

  /* If there is an old cepstral frame OBJT at sn, free it */
  if (ffr->cep) OBJT_free(ffr->cep);

  /* Buffer the new cepstral frame OBJT at count */
  ffr->sn = sn;
  ffr->cep = cep;
  mkjin->ffq->count++;
}


static SN
MKJIN_dequeue(MKJIN mkjin, SN sn, OBJT obj)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Extract derived features from the local feature frame queue and fill
 * a feature frame object.  Return the SN of the resulting jin frame
 *---------------------------------------------------------------------------*/
{
  FFQ      ffq = mkjin->ffq;
  Int      i, j, mask = ffq->mask;
  Val    * dfwd, * dbwd;
  Val    * cep, * dcep;
  UChar  * ucp;

  cep = mkjin->featFrame;
  dcep = mkjin->featFrame + mkjin->featLen[CEP_FEAT];

  /* Extract the cepstrum */
  memcpy(cep, &OBJT_contents(ffq->frame[IDX(sn - mkjin->ldW, mask)].cep), sizeof(Val) * mkjin->cepVecLen);
  
  /* Append the long-duration cepstral difference */
  dfwd = (Val *) &OBJT_contents(ffq->frame[IDX(sn, mask)].cep);
  dbwd = cep;
  i = mkjin->cepVecLen;
  while (i--)
    *dcep++ = THREE_FOURTHS(*dfwd++ - *dbwd++);

  /* Floor/Ceil the feature vector and convert to traditional unsigned char format */
  ucp = (UChar *) &OBJT_contents(obj);
  for (i = 0; i < mkjin->cepVecLen + mkjin->featLen[DCEP_FEAT]; i++) {
    j = JNORM(mkjin->featFrame[i]);
    j = (j < 1) ? 1 : j;
    j = (j > 255) ? 255 : j;
    ucp[i] = (UChar)j;    
  }

  /* Return the frame's sequence number */
  return(ffq->frame[IDX(sn - mkjin->ldW, mask)].sn);
}


/*** IMKJIN Methods ***/

static IMKJIN
IMKJIN_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IMKJIN interface to the feature extractor
 *---------------------------------------------------------------------------*/
{
  IMKJIN imkjin;

  if ((imkjin = (IMKJIN) IFACE_calloc((IFACE) igen, 1, sizeof(imkjin_t)))) {
    IFACE_setClass((IFACE) imkjin, &IMKJIN_Class);
    IFACE_setObject((IFACE) imkjin, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) imkjin, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) imkjin);
  }

  return(imkjin);
}


Bool
IMKJIN_reset(IMKJIN imkjin)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the JIN maker
 *---------------------------------------------------------------------------*/
{
  MKJIN mkjin = ((MKJIN) IFACE_object((IFACE) imkjin));

  assert(mkjin != NULL);
  mkjin->seqNum = - mkjin->minWindow;
  MKJIN_resetQueue(mkjin->ffq);
  return(TRUE);
}

/*** MKJINATTR Methods ***/

IGEN
MKJINATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a MKJIN attributes object
 *---------------------------------------------------------------------------*/
{
  MKJINATTR   mkjinattr = (MKJINATTR) imem->Calloc(imem, 1, sizeof(mkjinattr_t), OOPS_MEMORY);
  IGEN        igen = IGEN_create(imem, (OBJ) mkjinattr);
  IMKJINATTR  imkjinattr = IMKJINATTR_create(igen);

  /* Generic IGEN_annihilate() suffices */

  /* Set the defaults for MKJINATTR */
  mkjinattr->classSize = DFLT_CLASS_SIZE;
  mkjinattr->frameBytes = DFLT_FRAME_BYTES;
  mkjinattr->ldW = DFLT_LD_WIDTH;
  mkjinattr->fwdTol = mkjinattr->bwdTol = DFLT_TOLERANCE;
  return(igen);
}


/*** IMKJINATTR METHODS ***/

static IMKJINATTR
IMKJINATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IMKJINATTR interface
 *---------------------------------------------------------------------------*/
{
  IMKJINATTR  imkjinattr;

  if ((imkjinattr = (IMKJINATTR) IFACE_calloc((IFACE) igen, 1, sizeof(imkjinattr_t)))) {
    IFACE_setClass((IFACE) imkjinattr, &IMKJINATTR_Class);
    IFACE_setObject((IFACE) imkjinattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) imkjinattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) imkjinattr);
  }

  return(imkjinattr);
}


Bool
IMKJINATTR_getClassSize(IMKJINATTR imkjinattr, Int * classSizeP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size of the CLASS of FEAT frames
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  *classSizeP = mkjinattr->classSize;
  return(TRUE);
}


Bool
IMKJINATTR_setClassSize(IMKJINATTR imkjinattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the CLASS of FEAT frames
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  mkjinattr->classSize = classSize;
  return(TRUE);
}


Bool
IMKJINATTR_getLDW(IMKJINATTR imkjinattr, Int * ldWP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the half-width of the long-duration window
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  *ldWP = mkjinattr->ldW;
  return(TRUE);
}


Bool
IMKJINATTR_setLDW(IMKJINATTR imkjinattr, Int ldW)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the half-width of the long-duration window
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  mkjinattr->ldW = ldW;
  return(TRUE);
}

Bool
IMKJINATTR_getFwdTol(IMKJINATTR imkjinattr, SN * fwdTolP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the forward tolerance for out of range SNs
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  *fwdTolP = mkjinattr->fwdTol;
  return(TRUE);
}


Bool
IMKJINATTR_setFwdTol(IMKJINATTR imkjinattr, SN fwdTol)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the forward tolerance for out of range SNs
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  mkjinattr->fwdTol = fwdTol;
  return(TRUE);
}

Bool
IMKJINATTR_getBwdTol(IMKJINATTR imkjinattr, SN * bwdTolP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the backward tolerance for out of range SNs
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  *bwdTolP = mkjinattr->bwdTol;
  return(TRUE);
}


Bool
IMKJINATTR_setBwdTol(IMKJINATTR imkjinattr, SN bwdTol)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the backward tolerance for out of range SNs
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  mkjinattr->bwdTol = bwdTol;
  return(TRUE);
}

Bool
IMKJINATTR_getFrameBytes(IMKJINATTR imkjinattr, Int * frameBytesP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size in bytes of a sample frame
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  *frameBytesP = mkjinattr->frameBytes;
  return(TRUE);
}


Bool
IMKJINATTR_setFrameBytes(IMKJINATTR imkjinattr, Int frameBytes)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size in bytes of a sample frame
 *---------------------------------------------------------------------------*/
{
  MKJINATTR  mkjinattr;

  assert(imkjinattr);
  mkjinattr = (MKJINATTR) IFACE_object((IFACE) imkjinattr);

  mkjinattr->frameBytes = frameBytes;
  return(TRUE);
}
