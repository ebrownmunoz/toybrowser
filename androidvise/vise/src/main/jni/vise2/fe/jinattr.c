/* jinattr.c - jin frame attributes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * HISTORY
 * 26-Oct-96  Craig Vanderborgh (craigv@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "jinattr.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "vbx.h"
#include <assert.h>

#define JIN_PRINT_PRIO VBX_MID_PRIO
#define DFLT_JINVECLEN 16

/* The Class of JINATTR (JIN Frame Attributes) Interface Objects */
static iclass_t IJINATTR_Class = {IJINATTRID};

/* JIN Attributes */
typedef struct jinattr_s {
  Int         vecLen;     /* Number of elements in a jin frame (bytes) */
} jinattr_t, *JINATTR;

/* Private functions */

static IJINATTR  IJINATTR_create(IGEN igen);
static Bool      IJINATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields);

/*** JINATTR Methods ***/

IGEN
JINATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a jin frame attributes object
 *---------------------------------------------------------------------------*/
{
  JINATTR   jinattr = (JINATTR) imem->Calloc(imem, 1, sizeof(jinattr_t), OOPS_MEMORY);
  IGEN      igen = IGEN_create(imem, (OBJ) jinattr);
  IJINATTR  ijinattr = IJINATTR_create(igen);

  jinattr->vecLen = DFLT_JINVECLEN;

  /* Generic IGEN_annihilate() suffices */

  /* Set up the default behavior of IJINATTR */
  IOBJATTR_setPrinter((IOBJATTR) ijinattr, IJINATTR_dfltPrint);
  IOBJATTR_setLabel((IOBJATTR) ijinattr, "JIN ");

  return(igen);
}


Bool
IJINATTR_getVecLen(IJINATTR ijinattr, Int * vecLenP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the length of a jin feature vector (in bytes)
 *---------------------------------------------------------------------------*/
{
  JINATTR  jinattr;

  assert(ijinattr);
  jinattr = (JINATTR) IFACE_object((IFACE) ijinattr);

  *vecLenP = jinattr->vecLen;
  return(TRUE);
}


Bool
IJINATTR_setVecLen(IJINATTR ijinattr, Int vecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of a jin feature vector (in bytes)
 *---------------------------------------------------------------------------*/
{
  JINATTR  jinattr;

  assert(ijinattr);
  jinattr = (JINATTR) IFACE_object((IFACE) ijinattr);

  jinattr->vecLen = vecLen;
  return(TRUE);
}


/*** IJINATTR METHODS ***/

static IJINATTR
IJINATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IJINATTR interface
 *---------------------------------------------------------------------------*/
{
  IJINATTR  ijinattr = (IJINATTR) IOBJATTR_create(igen);

  /* Turn it from an IOBJATTR into an IJINATTR */
  IFACE_setClass((IFACE) ijinattr, &IJINATTR_Class);

  return(ijinattr);
}


static Bool
IJINATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Print out a jin frame object
 *---------------------------------------------------------------------------*/
{
  UChar  * jin  = (UChar *) &OBJT_contents(objt);
  Int      i;

  for (i = 0; i < 16; i++) {
    VBX_print("%3d ", (Int) jin[i]);
  }
  VBX_print("\n");
  
  return(TRUE);
}
