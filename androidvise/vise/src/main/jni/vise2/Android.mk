LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := vvisese
# google recommends convenience variables to be prefixed with "MY_"
MY_SYSTEM := fyi.c jinmsg.c memory.c se.c vise.c
MY_ALGORITHM := adjustem.c ai.c am.c divide.c findwo.c gi.c gm.c gramnodes.c \
            insrtpth.c jincnvrt.c jinsource.c ki.c km.c ni.c nm.c nullarcs.c \
            path.c process.c processk.c processw.c recloop.c syn.c tb.c \
            tbf.c tbn.c tbp.c tem.c tm.c trace.c voc.c wi.c wm.c
MY_COMMAND := delgra.c delwor.c dloadg.c dloadj.c dloadt.c dloadw.c dloadwc.c \
            enroll.c listen.c param.c recog.c replacew.c segword.c srcinit.c \
            timeout.c trainword.c uloadj.c uloadt.c uloadw.c warmstart.c

LOCAL_SRC_FILES := $(MY_SYSTEM:%.c=vise/%.c) $(MY_ALGORITHM:%.c=vise/%.c) $(MY_COMMAND:%.c=vise/%.c)
LOCAL_STATIC_LIBRARIES := fe
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/include/priv $(LOCAL_PATH)/../vbx/include $(LOCAL_PATH)/../fe/include
LOCAL_STATIC_LIBRARIES := cep oops vvisefe

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := vvisefe
LOCAL_SRC_FILES := fe/mkjin.c fe/jinattr.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/include/priv $(LOCAL_PATH)/../vbx/include $(LOCAL_PATH)/../fe/include
LOCAL_STATIC_LIBRARIES := vbxutil
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := vulcan
LOCAL_SRC_FILES := vulcan/buds.c vulcan/ekb.c vulcan/hyp.c vulcan/kb.c vulcan/models.c vulcan/parse.c vulcan/recfile.c vulcan/script.c vulcan/spwrds.c vulcan/syntax.c vulcan/template.c vulcan/trans.c vulcan/tree.c vulcan/vfile.c vulcan/vocab.c vulcan/vvise.c vulcan/wrdmap.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/include/priv $(LOCAL_PATH)/../vbx/include $(LOCAL_PATH)/../fe/include

include $(BUILD_STATIC_LIBRARY)
