#include "pthr.h"
#include <assert.h>
#include "types.h"
#include "vbx.h"

#include "parse.h"
#include "tree.h"
#include "buds.h"

#define TREE_MAX_NODES 3250

/* Function Error Codes */
#define TREES_INIT_FREE         1
#define TREES_GET_TREE          2
#define TREES_PUT_TREE          3
#define TREES_SET_TREE          4
#define TREES_REMOVE_DAUGHTER   5
#define TREES_PRUNE             6
#define TREES_INVERT_STALK      7
#define TREES_CLOSE             8

typedef struct tree_s {
  TREENODE nodeList;
  TREENODE nextNode;
  UShort   nodeListSize;
  UShort   usedNodes;
  UShort   maxUsedNodes;
} tree_t;

TREE
TREE_create()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a TREE object containing an empty free list of nodes
 * Return: Pointer to the new tree
 *---------------------------------------------------------------------------*/
{
  TREE tree = (TREE) VBX_calloc(1, sizeof(tree_t));
  
  tree->nodeListSize = TREE_MAX_NODES;
  tree->nodeList = (TREENODE) VBX_calloc(tree->nodeListSize, sizeof(treeNode_t));

  return(tree);
}

void
TREE_destroy(TREE tree)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free the given tree object
 *---------------------------------------------------------------------------*/
{
  if (tree != NULL) {
    VBX_free(tree->nodeList);
    VBX_free(tree);
  }
}

Bool
TREE_initialize(TREE tree)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a TREE structure by putting all available nodes into a branchless
 * tree (stalk) and setting nextNode to point to the bud.  The stalk serves as
 * the free node list for the TREE module
 * Return: Pointer to the new tree
 *---------------------------------------------------------------------------*/
{
  UShort nodeIdx;
  Bool   rval = FALSE;

  if (tree != NULL) {
    if (tree->nodeList == NULL) {
  /*    error_report(ERROR_TREES, TREES_INIT_FREE, 1, 1, nodeListSize); */
      return(FALSE);
    }

    tree->maxUsedNodes = tree->usedNodes = 0;
    tree->nodeList[0].motherPtr = (TREENODE) NULL;

    for (nodeIdx = 1; nodeIdx < tree->nodeListSize; nodeIdx++)
      tree->nodeList[nodeIdx].motherPtr = (TREENODE) &tree->nodeList[nodeIdx - 1];
    tree->nextNode = (TREENODE) &tree->nodeList[tree->nodeListSize - 1];
    rval = TRUE;
  }

  return(rval);
}

TREENODE
TREE_nodeAlloc(TREE tree)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Attempt to allocate a tree node from the given TREE's free node stalk.  If 
 * the stalk is empty, a NULL pointer is returned.
 * Return: Pointer to the new tree
 *---------------------------------------------------------------------------*/
{
  TREENODE node = (TREENODE) NULL;

  if (tree != NULL && tree->nextNode != NULL) {
    if (++tree->usedNodes > tree->maxUsedNodes)
     	tree->maxUsedNodes++;
    node = tree->nextNode;
    tree->nextNode = tree->nextNode->motherPtr;
    return(node);
  } else {
/*    error_report(ERROR_TREES, TREES_GET_TREE, 1, 0); */
    return(node);
  }
}

void 
TREE_nodeFree(TREE tree, TREENODE node)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the given node to the free node stalk
 *---------------------------------------------------------------------------*/
{
  if (tree != NULL) {
    tree->usedNodes--;
    node->motherPtr = tree->nextNode;
    tree->nextNode = node;
  }
}

void 
TREE_nodeSet(TREENODE node, UShort stateId, UShort wordId, 
             TREENODE mother, UShort daughters)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set all the fields in the given tree node to the values specified
 *---------------------------------------------------------------------------*/
{
  if (node != NULL) {
    node->nodeStateId = stateId;
    node->branchWordId = wordId;
    node->motherPtr = mother;
    node->daughterCount = daughters;
  }
}

TREENODE
TREE_nodeRemoveDaughter(TREE tree, TREENODE daughterPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Remove the specified daughter node from the given tree, and put it on
 * the free node list.  A pointer to the resulting mother node is returned
 *---------------------------------------------------------------------------*/
{
  TREENODE motherPtr = (TREENODE) NULL;

  if (tree != NULL) {
    motherPtr = daughterPtr->motherPtr;
    TREE_nodeFree(tree, daughterPtr);

    if (motherPtr != NULL)
      motherPtr->daughterCount--;
    return(motherPtr);
  }
}

void
TREE_prune(TREE tree, TREENODE node)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Prune upward through the tree recursively, starting with the given node
 *---------------------------------------------------------------------------*/
{
  TREENODE motherPtr = (TREENODE) NULL;

  if (tree != NULL && node != (TREENODE) NULL) {
    if (!node->daughterCount) {
     	motherPtr = TREE_nodeRemoveDaughter(tree, node);
     	TREE_prune(tree, motherPtr);
    }
  }
}

TREENODE
TREE_invertStalk(TREENODE node)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Invert the stalk ending in "node", such that the motherPtr in each node 
 * points to the daughter (rather than the mother) and the branchWordId now 
 * labels the branch to the daughter (rather than the branch to the mother). 
 * Set all daughter counts to 1 except the last one, which becomes zero.
 * Return the resulting matriarch node
 * NOTE: To minimize stack use, this method is iterative rather than recursive
 *---------------------------------------------------------------------------*/
{
  TREENODE  mother, grandmother, daughter, retNode = (TREENODE) NULL;
  UShort    branchWordId, grandbranch;
  Int       count = 0;

  if (node != NULL) {
    daughter = node;
    mother = daughter->motherPtr;
    branchWordId = daughter->branchWordId;
    daughter->motherPtr = (TREENODE) NULL;
    daughter->branchWordId = PARSE_NULL_WORD;
    daughter->daughterCount = 0;
    while (mother) {
   	  grandmother = mother->motherPtr;
      grandbranch = mother->branchWordId;
      mother->motherPtr = daughter;
      mother->branchWordId = branchWordId;
      mother->daughterCount = 1;
      daughter = mother;
      mother = grandmother;
      branchWordId = grandbranch;
      count++;
   	}
    retNode = daughter;
  }
  return(retNode);
}

UShort 
TREE_maxUsedNodes(TREE tree)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the high water mark for used nodes
 *---------------------------------------------------------------------------*/
{
  UShort retval = 0;

  if (tree != NULL) {
    retval = tree->maxUsedNodes;
  }

  return(retval);
}
