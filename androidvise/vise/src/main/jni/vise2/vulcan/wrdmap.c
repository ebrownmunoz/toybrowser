/* wrdmap.c - VISE Wordmap Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Methods for dealing with wordname maps for the VISE recognizer.
 * 
 * HISTORY
 * 12-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include "types.h"
#include "wrdmap.h"
#include "vbx.h"


/* Structure for mapping model names into word names */
typedef struct wrdmap_s {
  Char * modelName;
  Char * wordName;
} wrdmap_t;


WRDMAP
WRDMAP_create(void)
/***************************************************************************
 *
 * Create a word map
 *
 ***************************************************************************/
{
  return((WRDMAP) NULL);
}


void
WRDMAP_annihilate(WRDMAP map)
/***************************************************************************
 *
 * Annihilate a word map
 *
 ***************************************************************************/
{
  VBX_free(map);
}


Char *
WRDMAP_name(WRDMAP map, Char * name)
/***************************************************************************
 *
 * Return the mapped name
 *
 ***************************************************************************/
{
  return(name);
}
