/* syntax.c - VISE Grammar Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Methods for dealing with grammars for the VISE recognizer.  Methods are
 * provided for loading grammars and grammar graphs from "rec" files.  This
 * is one of two interfaces the VISE recognizer has to rec files, the other
 * being the vocabulary module.
 * 
 * HISTORY
 * 12-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: syntax.c[1.0] Mon Apr  7 23:26:52 1997 dvetter@titan saved $";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "vbx.h"
#include "utils.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "spwrds.h"
#include "syntax.h"
#include "viseerr.h"
#include "vocab.h"
#include "parse.h"

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#define VBX_FATALSTATUS(R,S)   { Int v = (R); if (R) FATAL_BRA_ "condition = %d; status = %s", v, VISEERR_string(S) _KET; }
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#define VBX_FATALSTATUS(R,S)
#endif

/* Grammar */
typedef struct syntax_s {
  UInt        id;              /* The ID for this grammar */
  UInt        vocabId;         /* The vocabulary this grammar uses */
  Char      * name;            /* The name of the grammar */
  UInt        wordsOnArc;      /* Total number of word instances */
  UShort      dfltCRTmplIdx;   /* Default control response template */
  UShort      dfltHRTmplIdx;   /* Default host response template */
  UShort      dfltVRTmplIdx;   /* Default voice response template */
  UShort      dfltDRTmplIdx;   /* Default display response template */
  TRANS       trans;           /* Optional response facility data */
  FGRAPH    * graph;           /* The grammar graph header */
  FGRAPHARC * arc;             /* The arcs of the grammar graph */
  V_Err       status;
} syntax_t;

#ifdef DEBUG_PARSER
static void  SYNTAX_printNodesReached(Char * label, Bool * node, UInt nodeCnt);
static void  SYNTAX_printGraph(SYNTAX syntax);
#endif

static void  SYNTAX_traverse(UInt wid, FGRAPHARC * arc, UInt arcCnt, Bool * srcNode, Bool * dstNode);


SYNTAX
SYNTAX_create(UInt grammarId)
/***************************************************************************
 *
 * Create and return a grammar object
 *
 ***************************************************************************/
{
  SYNTAX syntax = (SYNTAX) calloc(1, sizeof(syntax_t));

  if (syntax) {
    syntax->id = grammarId;
    syntax->status = VISESUCCESS;
  }

  return(syntax);
}


void
SYNTAX_annihilate(SYNTAX grammar)
/***************************************************************************
 *
 * Annihilate a grammar object
 *
 ***************************************************************************/
{
  if (grammar) {
    VBX_free(grammar->name);
    VBX_free(grammar->graph);
    VBX_free(grammar->arc);
    TRANS_annihilate(grammar->trans);
    free(grammar);
  }
}


V_Err
SYNTAX_status(SYNTAX grammar)
/***************************************************************************
 *
 * Return the status of a grammar object
 *
 ***************************************************************************/
{
  return(grammar ? grammar->status : NOSUCHOBJECT);
}


Bool
SYNTAX_loadHeader(SYNTAX syntax, VFILE recFile)
/***************************************************************************
 *
 * Load the grammar block header from a recognition file into a syntax
 * object.  Returns TRUE if successful, FALSE otherwise.
 *
 ***************************************************************************/
{
  FGRAMMAR header;
  Bool     success;

  if (!(success = syntax != NULL && recFile != NULL)) {
    syntax->status = INVALIDARGUMENT;

   /* Read in the grammar block header */
  } else if ((success = VFILE_SUCCESS == VFILE_readStructure(recFile, &header, 1, RS_FGRAMMAR))) {
    syntax->vocabId = header.wVocabulary;
     /* Allocate enough space to hold a string of at least 7 characters */
    if (!(syntax->name = (Char *) calloc((size_t) (header.bloGrammarName.bLength + 8), sizeof(Char)))) {
      syntax->status = OUTOFMEMORY;
    } else {
      if (!header.bloGrammarName.bLength || VFILE_SUCCESS != VFILE_getText(recFile, &header.bloGrammarName, (UChar *) syntax->name))
        sprintf(syntax->name, "<_%d_>", syntax->id);
      syntax->dfltCRTmplIdx = header.wDefaultCRTemplate;
      syntax->dfltHRTmplIdx = header.wDefaultHRTemplate;
      syntax->dfltVRTmplIdx = header.wDefaultVRTemplate;
      syntax->dfltDRTmplIdx = header.wDefaultDRTemplate;
      syntax->status = VISESUCCESS;
    }
  } else {
    syntax->status = CANTREADVFILESTRUCT;
  }

  return(success);
}


Bool
SYNTAX_loadGraph(SYNTAX syntax, VFILE recFile)
/***************************************************************************
 *
 * Load a grammar graph object from a recognition file into a syntax
 * object.  Returns TRUE if successful, FALSE otherwise.
 *
 ***************************************************************************/
{
  FGRAPH     * graph;          /* The grammar graph */
  FGRAPHARC  * arc;            /* Grammar arc iterator */
  UInt         arcCnt;         /* Count of the arcs in the graph */
  UShort     * word;           /* Word Id iterator */
  UInt         wrdCnt;         /* Count of the words on an arc */
  Bool         success;

  if (!(success = syntax != NULL && recFile != NULL)) {
    syntax->status = INVALIDARGUMENT;

  } else if (!(success = (graph =
                          (FGRAPH *) calloc(1, sizeof(FGRAPH))) != NULL)) {
    syntax->status = OUTOFMEMORY;

   /* Load the graph parameters structure */
  } else if ((success = VFILE_SUCCESS ==
                       VFILE_readStructure(recFile, graph, 1, RS_GRAPH))) {

    syntax->graph = graph;
    syntax->wordsOnArc = 0;

     /* Allocate space for the arcs and word Ids */
    wrdCnt = graph->wGraphSize - 3 * graph->wNumberOfArcs;
    if ((success = (syntax->arc = arc =
         (FGRAPHARC *) calloc(graph->wNumberOfArcs * sizeof(FGRAPHARC) +
                              wrdCnt * sizeof(UShort), 1)) != NULL)) {
      syntax->status = VISESUCCESS;
       /* Loop over the arc structures */
      arcCnt = graph->wNumberOfArcs;
      while (success && arcCnt--) {
        /* Read the arc structure */
        if (!(success = VFILE_SUCCESS ==
                      VFILE_readStructure(recFile, arc, 1, RS_GRAPHARC))) {
          syntax->status = CANTREADVFILESTRUCT;
        } else {
          word = (UShort *) (arc + 1);
           /* Read the id's of the words on that arc */
          wrdCnt = arc->uNumberOfWords;
          while (success && wrdCnt--)
            if (!(success = VFILE_SUCCESS ==
                    VFILE_readStructure(recFile, word++, 1, RS_GRAPHARCE)))
              syntax->status = CANTREADVFILESTRUCT;
          syntax->wordsOnArc += arc->uNumberOfWords;
          arc = (FGRAPHARC *) word;
        }
      }
    } else {
      syntax->status = OUTOFMEMORY;
    }

  } else {
    syntax->status = CANTREADVFILESTRUCT;
  }

  return(success);
}


Bool
SYNTAX_loadTrans(SYNTAX syntax, VFILE recFile)
/***************************************************************************
 *
 * Load the results translation blocks from the voice stream
 * Returns TRUE if successful, FALSE otherwise.
 *
 ***************************************************************************/
{
  FGRAMMARTRANS  gramTrans;
  FGRAMMARTRANSE gramTransEntry;
  TRANSENT       transent = NULL;
  TRANS          trans = NULL;
  FLOFF          hostTrans;
  FLOFF          voiceTrans;
  FLOFF          displayTrans;
  UShort         blockId, classId;
  UInt           wid, numWords;
  ULong          size;

  trans = syntax->trans = TRANS_create();

  if (VFILE_rewind(recFile) != VFILE_SUCCESS)
    FATAL_BRA_ "Can't rewind the rec file" _KET;

  /* Peruse all recfile blocks for grammar response data having our classId */
  while (VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &classId, (V_ULong *)&size )) {
    VBX_DEBUG(VBX_print("BLOCKID = %d  CLASSID = %d\n", blockId, classId));

    if (syntax->id == classId) {

      switch (blockId) {

      case TR_GRAMMAR_TRANS:
        VBX_DEBUG(VBX_print("Loading GRAMMARTRANS for syntax %d\n", syntax->id));
        if (VFILE_readStructure(recFile, &gramTrans, 1, RS_FGRAMMARTRANS) == VFILE_SUCCESS) {
          numWords = (UInt) gramTrans.wNumberWords;
          if (numWords > 0) {

            /* Hash table for translation entries, hashed by wordId */
            trans->widHT = HASH_create(numWords, HASH_integer, HASH_integerEquals, (void (*)(Ptr)) TRANSENT_annihilate);

            /* Read L/O for grammar translation entries */
            for (wid = 1; wid <= numWords; wid++) {
              VFILE_readStructure(recFile, &gramTransEntry, 1, RS_FGRAMMARTRANSE);
              /* VBX_DEBUG(VBX_print("(wid %d)\n", gramTransEntry.wWordID)); */
              /* VBX_DEBUG(PRINT_FLOFF(&gramTransEntry.loHostTranslation)); */
              /* VBX_DEBUG(PRINT_FLOFF(&gramTransEntry.loVoiceTranslation)); */
              /* VBX_DEBUG(PRINT_FLOFF(&gramTransEntry.loDisplayTranslation)); */
              hostTrans = gramTransEntry.loHostTranslation;
              voiceTrans = gramTransEntry.loVoiceTranslation;
              displayTrans = gramTransEntry.loDisplayTranslation;

              transent = TRANSENT_create((UInt) gramTransEntry.wWordID, hostTrans, voiceTrans, displayTrans);
              transent->wid = gramTransEntry.wWordID;
              HASH_add(trans->widHT, (Ptr) gramTransEntry.wWordID, (Ptr) transent);
            }
          }

          /* Read Host Response Delineators */
          trans->HRDelin = DELIN_create();
          trans->HRDelin->initiator = gramTrans.loHRFInitiator;
          trans->HRDelin->separator = gramTrans.loHRFSeparator;
          trans->HRDelin->terminator = gramTrans.loHRFTerminator;

          /* Read Voice Response Delineators */
          trans->VRDelin = DELIN_create();
          trans->VRDelin->initiator = gramTrans.loVRFInitiator;
          trans->VRDelin->separator = gramTrans.loVRFSeparator;
          trans->VRDelin->terminator = gramTrans.loVRFTerminator;
 
          /* Read Display Response Delineators */
          trans->DRDelin = DELIN_create();
          trans->DRDelin->initiator = gramTrans.loDRFInitiator;
          trans->DRDelin->separator = gramTrans.loDRFSeparator;
          trans->DRDelin->terminator = gramTrans.loDRFTerminator;
        }
        break;

      case TR_CONTROL_PARSE:
        trans->controlParseTbl = PARSE_loadTable(recFile);
        if (trans->controlParseTbl == NULL)
          FATAL_BRA_ "CONTROL_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("CONTROL_PARSE load for syntax %d returns %p\n", syntax->id, trans->controlParseTbl));
        break;

      case TR_VOICE_PARSE:
        trans->voiceParseTbl = PARSE_loadTable(recFile);
        if (trans->voiceParseTbl == NULL)
          FATAL_BRA_ "VOICE_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("VOICE_PARSE load for syntax %d returns %p\n", syntax->id, trans->voiceParseTbl));
        break;

      case TR_HOST_PARSE:
        trans->hostParseTbl = PARSE_loadTable(recFile);
        if (trans->hostParseTbl == NULL)
          FATAL_BRA_ "HOST_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("HOST_PARSE load for syntax %d returns %p\n", syntax->id, trans->hostParseTbl));
        break;

      case TR_DISPLAY_PARSE:
        trans->displayParseTbl = PARSE_loadTable(recFile);
        if (trans->displayParseTbl == NULL)
          FATAL_BRA_ "DISPLAY_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("DISPLAY_PARSE load for syntax %d returns %p\n", syntax->id, trans->displayParseTbl));
        break;

      default:
        break;
	  }
    }
  }

  return(TRUE);
}

TRANS
SYNTAX_trans(SYNTAX grammar)
/***************************************************************************
 *
 * Return the translation structure for the specified grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->trans);
}

UShort
SYNTAX_dfltHRTmplIdx(SYNTAX grammar)
/***************************************************************************
 *
 * Return the default host response template index for the given grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->dfltHRTmplIdx);
}

UShort
SYNTAX_dfltVRTmplIdx(SYNTAX grammar)
/***************************************************************************
 *
 * Return the default voice response template index for the given grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->dfltVRTmplIdx);
}


UShort
SYNTAX_dfltDRTmplIdx(SYNTAX grammar)
/***************************************************************************
 *
 * Return the default display response template index for the given grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->dfltDRTmplIdx);
}


UInt
SYNTAX_vocabularyId(SYNTAX grammar)
/***************************************************************************
 *
 * Return the Id of the vocabulary this grammar uses
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->vocabId);
}


UInt
SYNTAX_numWordsOnArc(SYNTAX grammar)
/***************************************************************************
 *
 * Return the number of words on arc in this grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->wordsOnArc);
}


UInt
SYNTAX_id(SYNTAX grammar)
/***************************************************************************
 *
 * Return the Id of this grammar, or zero if none.
 *
 ***************************************************************************/
{
  if (grammar && grammar->graph)
    return(grammar->graph->wGrammarID);
  else
    return(0);
}


Char *
SYNTAX_name(SYNTAX grammar)
/***************************************************************************
 *
 * Return the name of this grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->name);
}


FGRAPH *
SYNTAX_graph(SYNTAX grammar)
/***************************************************************************
 *
 * Return the graph header for this grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->graph);
}


FGRAPHARC *
SYNTAX_arcs(SYNTAX grammar)
/***************************************************************************
 *
 * Return the graph arcs for this grammar
 *
 ***************************************************************************/
{
  assert(grammar);

  return(grammar->arc);
}


SYNTAX
SYNTAX_parse(SYNTAX grammar, VOCENT * tokens, UInt numTokens)
/***************************************************************************
 *
 * Return the grammar if it parses the token array, NULL otherwise.
 *
 ***************************************************************************/
{
  FGRAPH    * graph;
  FGRAPHARC * arc;                    /* Arc iterator */
  UInt        arcCnt;                 /* Count of the arcs in the graph */
  UInt        endNode;                /* The Id of the terminal node */
  Bool      * srcNode, * dstNode, *tmpNode;
  size_t      length;
  UInt        wordIndex;
  UInt        wid;

  assert(grammar);

# ifdef DEBUG_PARSER
  SYNTAX_printGraph(grammar);
# endif

  graph = grammar->graph;
  arc = grammar->arc;
  arcCnt = (UInt) graph->wNumberOfArcs;
  endNode = (UInt) graph->wNumberOfNodes - 1;
  length = (size_t) graph->wNumberOfNodes * sizeof(Bool);

  srcNode = (Bool *) VBX_calloc((size_t) graph->wNumberOfNodes, sizeof(Bool));
  dstNode = (Bool *) VBX_calloc((size_t) graph->wNumberOfNodes, sizeof(Bool));

  srcNode[0] = TRUE;
  for (wordIndex = 0; wordIndex < numTokens; wordIndex++) {
    wid = VOCENT_firstWordId(tokens[wordIndex]);
    while (wid) {
      SYNTAX_traverse(wid, arc, arcCnt, srcNode, dstNode);
      wid = (UShort) VOCENT_nextWordId(tokens[wordIndex]);
    }
#   ifdef DEBUG_PARSER
    SYNTAX_printNodesReached(VOCENT_wordName(tokens[wordIndex]), dstNode, (UInt) graph->wNumberOfNodes);
#   endif
    /* Swap src and dst node arrays */
    tmpNode = srcNode;
    srcNode = dstNode;
    dstNode = tmpNode;
    /* Set dst node array to all FALSE */
    memset(dstNode, 0, length);
  }
  SYNTAX_traverse(LONG_SILENCE, arc, arcCnt, srcNode, dstNode);

# ifdef DEBUG_PARSER
  SYNTAX_printNodesReached("*long", dstNode, (UInt) graph->wNumberOfNodes);
  VBX_print("  Terminal node = %u\n", endNode);
# endif

  grammar = dstNode[endNode] ? grammar : NULL;
  
  free(srcNode);
  free(dstNode);

  return(grammar);
}


static void
SYNTAX_traverse(UInt wid, FGRAPHARC * arc, UInt arcCnt, Bool * srcNode, Bool * dstNode)
/***************************************************************************
 *
 * First traverse all of the null arcs, propagating from TRUE (reached)
 * SOURCE nodes to SOURCE nodes;  then traverse all of the arcs containing
 * the given word Id, propagating from TRUE (reached) SOURCE nodes to
 * DESTINATION nodes.
 *
 ***************************************************************************/
{
  UShort    wrdCnt;
  UShort  * word;
  UInt      skip;

  while (arcCnt--) {
    wrdCnt = arc->uNumberOfWords;
    if (!wrdCnt) {
      /* Propagate from source to source across a null arc */
      srcNode[arc->uDestNode] |= srcNode[arc->uSourceNode];
#     ifdef DEBUG_PARSER
      if (srcNode[arc->uSourceNode])
        VBX_print("  srcNode = %3u;                                       dstNode = %3u\n", arc->uSourceNode, arc->uDestNode);
#     endif
      arc++;
    } else {
      /* Propagate from source to dest across an arc containing the word */
      word = (UShort *) (arc + 1);
      skip = wrdCnt;
      while (wrdCnt--) {
        if (*word++ == (UShort) wid) {
          dstNode[arc->uDestNode] |= srcNode[arc->uSourceNode];
#         ifdef DEBUG_PARSER
          VBX_print("  srcNode = %3u (%01.1u); grmWord = %5u; tokWord = %5u; dstNode = %3u\n",
                    arc->uSourceNode, srcNode[arc->uSourceNode], *(word - 1), wid, arc->uDestNode);
#         endif
          break;
        }
      }
      arc = (FGRAPHARC *) ((UShort *) (arc + 1) + skip);
    }
  }
}


#ifdef DEBUG_PARSER

static void
SYNTAX_printNodesReached(Char * label, Bool * node, UInt nodeCnt)
{
  UInt i;
  VBX_print("  %12.12s ", label);
  for (i = 0; i < nodeCnt; i++)
    if (node[i])
      VBX_print("%3u", i);
  VBX_print("\n");
}


static void
SYNTAX_printGraph(SYNTAX syntax)
/***************************************************************************
 *
 * Print out a grammar graph object
 *
 ***************************************************************************/
{
  FGRAPHARC  * arc;            /* Grammar arc iterator */
  UInt         arcCnt;         /* Count of the arcs in the graph */
  UShort     * word;           /* Word Id iterator */
  UInt         wrdCnt;         /* Count of the words on an arc */

  assert(syntax);

  /* Print out the grammar Id */
  VBX_print(" GRAPH OF GRAMMAR %u - \"%s\"\n\n", syntax->graph->wGrammarID, syntax->name);

  /* Print out the graph header */
  PRINT_FGRAPH(syntax->graph);

  /* Loop over the arc structures */
  VBX_print("\n");
  arc = syntax->arc;
  for (arcCnt = syntax->graph->wNumberOfArcs; arcCnt--; ) {
    /* Print out the arc structure */
    PRINT_FGRAPHARC(arc);
    word = (UShort *) (arc + 1);
    /* Print out the id's of the words on that arc */
    VBX_print("   ");
    for (wrdCnt = arc->uNumberOfWords; wrdCnt--; )
      VBX_print(" %3u", *word++);
    VBX_print("\n");
    arc = (FGRAPHARC *) word;
  }
  VBX_print("\nEND OF GRAPH OF GRAMMAR %u\n\n", syntax->graph->wGrammarID);
}

#endif
