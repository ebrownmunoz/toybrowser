/* models.c - VISE Word Model Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Methods for dealing with word models and sets of word models for the VISE
 * recognizer.  Methods are provided for loading and saving sets of models
 * from/to "voice" streams.  This is the VISE recognizer's only interface to
 * voice streams.  The models are subsequently added to a vocabulary by
 * methods in the vocabulary module (vocab.c).
 * 
 * HISTORY
 * 12-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: models.c[1.1] Wed Oct  8 13:51:29 1997 dvetter@titan saved $";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "ntmath.h"
#include "vbx.h"
#include "ifaces.h"
#include "imemory.h"
#include "critsect.h"
#include "kb.h"
#include "models.h"
#include "viseparm.h"
#include "vfile.h"
#include "utils.h"
#include "vise.h"

#ifdef DEBUG
#define VBX_DEBUG(P)        (P)
#define VBX_IF_NOT_DO(C,P)  if (!(C)) (P)
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)  C
#endif

#define MODHANDARRAY_GROWTHQUANTUM   200
#define NO_JINVERSION               -1

/* Model handles */
typedef struct modhand_s * MODHAND;

/* Set of sets of word models for all vocabularies */
typedef struct modset_s {
  HASH         vocabHT;       /* Vocabulary hash table (hashed by vocab Id */
  Char       * speaker;       /* The name of the speaker */
  VFILE        voiFile;       /* The underlying voice file */
  IMemory      allocator;
  CRITSECT_DECLARE(critsect)
  Int          jinVersion;
  V_Err        status;
} modset_t;

/* Set of word models for a vocabulary */
typedef struct models_s {
  MODSET       modset;        /* The set of models blocks this vocab belongs to */
  FUPATHEAD  * header;        /* The header from the models block */
  Bool         headerTouched; /* Flag signaling that header was updated and needs to be saved */
  FUPATPARM  * parameter;     /* The parameters from the models block */
  MODHAND      models;        /* The array of model handles for this vocabulary */
  UInt         numModels;     /* The number of active elements in the MODHAND array */
  UInt         modelsSize;    /* The number of elements in the MODHAND array */
  UInt         newModels;     /* The number of models not in the voice stream */
  Char       * vocabName;
  V_Err        status;
} models_t;

/* A models iterator */
typedef struct moditer_s {
  MODELS       models;        /* The models */
  UInt         modelId;       /* The iterator over the models */
} moditer_t;

/* Model handles */
typedef struct modhand_s {
  VFILE_MARK   fmark;         /* The position of the acoustic model in the voice stream */
  FUPATE *     model;         /* The unstreamed model (NULL iff fmark is non-zero) */
} modhand_t;


/* Private prototypes */

static void  MODSET_unload(MODSET modset);
static Bool  MODSET_addModels(MODSET modset, MODELS models, UInt vocabId);

static Bool  MODELS_load(MODELS models, VFILE voiFile);
static Bool  MODELS_save(MODELS models, VFILE voiFile);
static Int   MODELS_parameter(MODELS models, UInt parmId);

static Bool  MODEL_read(FUPATE * model, VFILE voiFile);


MODSET
MODSET_create(void)
/**************************************************************************
 *
 * Create a repository for all the word models in a voice file
 *
 **************************************************************************/
{
  MODSET    modset = (MODSET) calloc(1, sizeof(modset_t));

  if (modset) {
    modset->allocator.Calloc = IMEM_calloc;
    modset->allocator.Free = IMEM_free;
    CRITSECT_CREATE(modset->critsect, &modset->allocator, NULL);
    if (modset->critsect != NULL && (modset->vocabHT = HASH_create(1, HASH_integer, HASH_integerEquals, (void (*)(Ptr)) MODELS_destroy)) != NULL) {
      modset->jinVersion = NO_JINVERSION;
      modset->status = VISESUCCESS;
    } else {
      MODSET_destroy(modset);
      modset = NULL;
    }
  }

  return(modset);
}


void
MODSET_destroy(MODSET modset)
/**************************************************************************
 *
 * Annihilate a models repository
 *
 **************************************************************************/
{
  if (modset) {
    HASH_annihilate(modset->vocabHT);
    CRITSECT_DESTROY(modset->critsect);
    VBX_free(modset->speaker);
    free(modset);
  }
}


V_Err
MODSET_status(MODSET modset)
/***************************************************************************
 *
 * Return the current status of a models repository
 *
 ***************************************************************************/
{
  return(modset ? modset->status : NOSUCHOBJECT);
}


static Bool
MODSET_addModels(MODSET modset, MODELS models, UInt vocabId)
/**************************************************************************
 *
 * Try to add the models for a vocabulary to the set of models blocks,
 * using the given vocabId.  Return TRUE if successful, FALSE if there
 * are already models for this vocabulary in the table.
 *
 **************************************************************************/
{
  assert(modset);

  /* If there is no entry for vocabId, add this one */
  if (!HASH_lookup(modset->vocabHT, (Ptr) vocabId, NULL)) {
    if (!HASH_add(modset->vocabHT, (Ptr) vocabId, (Ptr) models)) {
      VBX_DEBUG(VBX_print("  MODSET_addModels: can't add entry not in vocabHT\n"));
      modset->status = CANTADDMODELS;
    } else {
      VBX_DEBUG(VBX_print("  MODSET_addModels: adding vocab %d to hash table\n", vocabId));
      models->modset = modset;
      modset->status = VISESUCCESS;
      return(TRUE);
    }
  }

  /* Otherwise, return failure */
  return(FALSE);
}


Bool
MODSET_load(MODSET modset, VFILE voiFile, Bool verbose)
/**************************************************************************
 *
 * Load all the sets of models (one set for each vocabulary) from the given
 * voice file into the MODSET
 *
 **************************************************************************/
{
  UShort  blockId, vocabId;
  ULong   size;
  MODELS  models;
  Bool    success;

  assert(modset && voiFile);
  
  if ((success = VFILE_SUCCESS == VFILE_rewind(voiFile))) {

    CRITSECT_ENTER(modset->critsect);

    modset->voiFile = voiFile;

    while (VFILE_SUCCESS == VFILE_getNextBlock(voiFile, &blockId, &vocabId, (V_ULong *)&size))
      if (blockId == TR_UPAT && !MODSET_models(modset, (UInt) vocabId)) {
        if (!(models = MODELS_create())) {
          VBX_print("  MODSET_load: Can't create models for block %u from the voice file @%p\n", vocabId, voiFile);
          modset->status = OUTOFMEMORY;
          success = FALSE;
          break;
        } else if (!MODELS_load(models, voiFile)) {
          VBX_print("  MODSET_load: Can't load models block %u from the voice file @%p\n", vocabId, voiFile);
          modset->status = MODELS_status(models);
          MODELS_destroy(models);
          success = FALSE;
          break;
        } else if (verbose) {
          VBX_print("  MODSET_load: Models block %u loaded from the voice file @%p\n", vocabId, voiFile);
        }
        if (!MODSET_addModels(modset, models, (UInt) vocabId)) {
          MODELS_destroy(models);
          VBX_print("  MODSET_load: Skipping superfluous models block %u in the voice file @%p\n", vocabId, voiFile);
        } else if (verbose) {
          VBX_print("  MODSET_load: Models block %u added to the models repository @%p\n", vocabId, modset);
        }
      }
#ifdef DEBUG
      else {
        VBX_print("  MODSET_load: blockId = %u; classId = %u; size = %u\n", blockId, vocabId, size);
      }
#endif

    if (success) {
      if (MODSET_jinVersion(modset) < 0) {
        VBX_print("MODSET_load: No consistent jin versions in voice file @%p\n", voiFile);
        success = FALSE;
      } else if (!HASH_used(modset->vocabHT)) {
        VBX_print("MODSET_load: There are no blocks of word models in voice file @%p\n", voiFile);
        modset->status = NOMODELSINVOIFILE;
        success = FALSE;
      } else if (!MODSET_models(modset, (UInt) UPHEADSILCLASSID)) {
        VBX_print("MODSET_load: There is no block of silence models in voice file @%p\n", voiFile);
        modset->status = NOSILENCEINVOIFILE;
        success = FALSE;
      } else {
        modset->status = VISESUCCESS;
      }
    }

    if (!success)
      MODSET_unload(modset);

    CRITSECT_LEAVE(modset->critsect);

  } else {
    VBX_print("  MODSET_load: Can't rewind the voice file @%p\n", voiFile);
    modset->status = CANTREWINDVOIFILE;
  }

  return(success);
}


static void
MODSET_unload(MODSET modset)
/**************************************************************************
 *
 * Empty the modset
 *
 **************************************************************************/
{
  if (modset) {
    HASH_annihilate(modset->vocabHT);
    modset->vocabHT = HASH_create(1, HASH_integer, HASH_integerEquals, (void (*)(Ptr)) MODELS_destroy);
    modset->voiFile = NULL;
    VBX_free(modset->speaker);
    modset->jinVersion = NO_JINVERSION;
  }
}


Bool
MODSET_save(MODSET modset, VFILE voiFile, Bool verbose)
/**************************************************************************
 *
 * Copy the current voice stream to voiFile, updating all the model blocks
 *
 **************************************************************************/
{
  UShort  blockId, vocabId;
  ULong   size;
  MODELS  models;
  Bool    mustSave = FALSE;
  UChar   byte;
  UChar * buffer = NULL;
  size_t  bufferSize = 0;
  Bool    success;

  if ((success = modset && voiFile)) {

    CRITSECT_ENTER(modset->critsect);

    if (VFILE_SUCCESS != VFILE_rewind(modset->voiFile) || VFILE_SUCCESS != VFILE_rewind(voiFile)) {
      modset->status = CANTREWINDVOIFILE;
      success = FALSE;

    } else {

      while (VFILE_SUCCESS == VFILE_getNextBlock(modset->voiFile, &blockId, &vocabId, (V_ULong *)&size)) {

        VFILE_openBlock(voiFile, blockId, vocabId);

         /* If this is a models block for the first vocabulary that has new models or for any subsequent vocabulary, update it */
        if (blockId == TR_UPAT && (models = MODSET_models(modset, vocabId)) && (MODELS_mustSave(models) || mustSave)) {
          MODELS_save(models, voiFile);
          if (verbose) VBX_print("  MODSET_save: Models for vocabulary %u saved to the voice file @%p\n", vocabId, voiFile);
           /* Force the saving of all subsequent models blocks */
          mustSave = TRUE;

         /* Otherwise, just copy it */
        } else {
           /* Try to allocate a buffer for the whole block */
          if (size > bufferSize) {
            VBX_free(buffer);
            buffer = (UChar *) malloc(bufferSize = (size_t) size);
          }
           /* If a buffer has been allocated, use it to copy the whole block at once */
          if (buffer) {
            VFILE_readByte(modset->voiFile, size, buffer);
            VFILE_writeByte(voiFile, size, buffer);
           /* Otherwise, copy the block byte by byte */
          } else {
            while (size--) {
              VFILE_readByte(modset->voiFile, 1, &byte);
              VFILE_writeByte(voiFile, 1, &byte);
            }
          }
        }

        VFILE_closeBlock(voiFile);
      }

      VBX_free(buffer);
      modset->status = VISESUCCESS;
      success = TRUE;
    }

    if (success) modset->voiFile = voiFile;

    CRITSECT_LEAVE(modset->critsect);
  } else if (modset) {
    modset->status = INVALIDARGUMENT;
  }

  return(success);
}



Int
MODSET_jinVersion(MODSET modset)
/**************************************************************************
 *
 * Return the jin version of the models in the repository, or
 * NO_JINVERSION if they are lacking or inconsistent.
 *
 **************************************************************************/
{
  MODELS  models;
  Int     jinVersion = NO_JINVERSION;

  if (modset) {
    models = (MODELS) HASH_firstValue(modset->vocabHT, NULL);
    if (models)
      jinVersion = MODELS_parameter(models, FEVERSION);

    while ((models = (MODELS) HASH_nextValue(modset->vocabHT, NULL)) && models)
      if (jinVersion != MODELS_parameter(models, FEVERSION)) {
        jinVersion = NO_JINVERSION;
        break;
      }

    if (jinVersion == NO_JINVERSION)
      modset->status = INVALIDJINVERSION;
    else
      modset->status = VISESUCCESS;
  }

  return(jinVersion);
}


MODELS
MODSET_models(MODSET modset, UInt vocabId)
/**************************************************************************
 *
 * Return the model set for the vocabulary with the given vocabId;
 * If there is no such vocabulary, return NULL.
 *
 **************************************************************************/
{
  Ptr     hashValue;

  if (modset && HASH_lookup(modset->vocabHT, (Ptr) vocabId, &hashValue)) {
    return((MODELS) hashValue);
  } else {
    modset->status = NOMODELSFORVOCAB;
    return(NULL);
  }
}


void
MODSET_setVoiFile(MODSET modset, VFILE voiFile)
/**************************************************************************
 *
 * Set the voice stream that underlies the model set
 * 
 **************************************************************************/
{
  if (modset) modset->voiFile = voiFile;
}


VFILE
MODSET_voiFile(MODSET modset)
/**************************************************************************
 *
 * Return the voice stream that underlies the model set
 * 
 **************************************************************************/
{
  if (modset)
    return(modset->voiFile);
  else
    return(NULL);
}


void
MODSET_lock(MODSET modset)
/**************************************************************************
 *
 * Enter the model set's critical section
 * 
 **************************************************************************/
{
  if (modset) CRITSECT_ENTER(modset->critsect);
}


void
MODSET_unlock(MODSET modset)
/**************************************************************************
 *
 * Leave the model set's critical section
 * 
 **************************************************************************/
{
  if (modset) CRITSECT_LEAVE(modset->critsect);
}


MODELS
MODELS_create(void)
/**************************************************************************
 *
 * Create a repository for word models for a vocabulary
 *
 **************************************************************************/
{
  MODELS  models = (MODELS) calloc(1, sizeof(models_t));

  if (models) {
    if ((models->header = (FUPATHEAD *) calloc(1, sizeof(FUPATHEAD))) != NULL) {
      models->status = VISESUCCESS;
    } else {
      MODELS_destroy(models);
      models = NULL;
    }
  }

  return(models);
}


void
MODELS_destroy(MODELS models)
/**************************************************************************
 *
 * Annihilate a repository of word models
 * 
 **************************************************************************/
{
  FUPATE * model;

  if (models) {
    VBX_free(models->header);
    VBX_free(models->parameter);
    while (models->newModels && models->numModels)
      if ((model = models->models[models->numModels--].model)) {
        free(model);
        models->newModels--;
      }
    VBX_free(models->models);
    VBX_free(models->vocabName);
    free(models);
  }
}


V_Err
MODELS_status(MODELS models)
/***************************************************************************
 *
 * Return the current status of the models repository for a vocabulary
 *
 ***************************************************************************/
{
  return(models ? models->status : NOSUCHOBJECT);
}


static Bool
MODELS_load(MODELS models, VFILE voiFile)
/**************************************************************************
 *
 * Load a model repository from a models block in the voice stream.
 * If successful, return TRUE; otherwise, return FALSE.  THE VOICE STREAM
 * IS ASSUMED TO BE POSITIONED AT THE BEGINNING OF THE BLOCK.
 * 
 **************************************************************************/
{
  FUPATHEAD * header = models->header;
  FUPATE      model;
  UInt        modelId, numParms, parmId;
  VFILE_MARK  fmark;
  Bool        success;


  if ((success = models && voiFile)) {

     /* Read in the header for this vocabulary */
    if ((success = (VFILE_SUCCESS == VFILE_readStructure(voiFile, header, 1, RS_FUPATHEAD)))) {
      models->headerTouched = FALSE;
       /* Check that the voice stream has the correct version */
      if (header->wAlwaysFFFF != 0xFFFF || header->wAlwaysZero != 0x0000) {
        VBX_DEBUG(VBX_print("  MODELS_load: voice file @%p has obsolete format\n", voiFile));
        models->status = ILLFORMEDVOIFILE;
        success = FALSE;

      } else {

         /* Allocate an array for the parameters */
        numParms = header->bNumberParms;
        if (numParms && !(models->parameter = (FUPATPARM *) calloc((size_t) numParms, sizeof(FUPATPARM)))) {
          models->status = OUTOFMEMORY;
          return(FALSE);
        }

         /* Read in the control parameters */
        for (parmId = 0; parmId < numParms; parmId++)
          VFILE_readStructure(voiFile, models->parameter + parmId, 1, RS_FUPATPARM);

        VBX_DEBUG(VFILE_placeMark(voiFile, &fmark));
        VBX_DEBUG(VBX_print("  MODELS_load: loading %d models from offset %ld in voice stream @%p\n", header->wNumberPatterns, fmark, voiFile));

         /* Allocate an array (1-origin) of model handles */
        models->modelsSize = models->numModels = header->wNumberPatterns;
        if (models->numModels  && !(models->models = (MODHAND) calloc((size_t) models->numModels + 1, sizeof(modhand_t)))) {
          models->status = OUTOFMEMORY;
          return(FALSE);
        }

        /* Fill the array */
        modelId = 0;
        while (modelId++ < models->numModels && VFILE_SUCCESS == VFILE_placeMark(voiFile, &fmark) && MODEL_read(&model, voiFile)) {
          models->models[modelId].fmark = fmark;
          models->models[modelId].model = NULL;
          VBX_DEBUG(VBX_print("  MODELS_load: loaded model for %s from offset %ld in voice stream @%p (mid = %d)\n", (Char *) model.bName, fmark, voiFile, modelId));
        }

        /* Check consistency */
        if (modelId - 1 != models->numModels) {
          VBX_DEBUG(VBX_print("  MODELS_load: ill-formed models block in voice file @%p (read %d of %d models)\n", voiFile, modelId - 1, models->numModels));
          models->numModels = modelId;
          models->status = ILLFORMEDVOIFILE;
          success = FALSE;
        } else {
          models->status = VISESUCCESS;
        }
      }
    } else {
      models->status = CANTREADVOIFILE;
    }
  } else if (models) {
    models->status = INVALIDARGUMENT;
  }

  return(success);  
}


static Bool
MODELS_save(MODELS models, VFILE voiFile)
/**************************************************************************
 *
 * Saves a model repository into a voice stream.  THE BLOCK TO RECEIVE THE
 * MODELS IS ASSUMED TO BE OPEN FOR WRITING WHEN THIS FUNCTION IS CALLED.
 * Returns TRUE if successful and leaves the stream pointer positioned for
 * writing the next block.
 * 
 **************************************************************************/
{
  FUPATE      model;
  MODHAND     modhand;
  VFILE       srcFile;
  VFILE_MARK  fmark;
  UInt        modelId;
  Bool        success;

  if ((success = models && voiFile && models->modset && (srcFile = models->modset->voiFile))) {

     /* Update the header */
    models->header->wNumberPatterns = models->numModels;
    
     /* Write the header and the parameters into the new block */
    if (!(success = VFILE_writeStructure(voiFile, models->header, 1, RS_FUPATHEAD) == VFILE_SUCCESS &&
                    VFILE_writeStructure(voiFile, models->parameter, models->header->bNumberParms, RS_FUPATPARM) == VFILE_SUCCESS))
      models->status = CANTSAVEMODELS;

     /* Record the position of the models in the new voice file */
    VBX_DEBUG(VFILE_placeMark(voiFile, &fmark));
    VBX_DEBUG(VBX_print("  MODELS_save: saving models at offset %ld in voice file @%p\n", fmark, voiFile));

     /* Scan through the array of model handles, saving all the models into the new stream */
    modelId = 0;
    while (success && modelId++ < models->numModels) {
      VBX_DEBUG(VBX_print("  MODELS_save: modelId = %d\n", modelId));
      modhand = models->models + modelId;

       /* Remember the position of the model in the new stream */
      VFILE_placeMark(voiFile, &fmark);

       /* If the model is in the old stream, copy it */
      if (modhand->fmark) {
        if (!(success = VFILE_gotoMark(srcFile, &modhand->fmark) == VFILE_SUCCESS &&
                        VFILE_readStructure(srcFile, &model, 1, RS_FUPATE) == VFILE_SUCCESS &&
                        VFILE_writeStructure(voiFile, &model, 1, RS_FUPATE) == VFILE_SUCCESS))
          models->status = CANTSAVEMODELS;
        VBX_DEBUG(model.bName[model.bNameCount] = 0);
        VBX_DEBUG(VBX_print("  MODELS_save: saved OLD model for \"%s\" (mid = %d) from models @%p to voice file at offset %ld\n",
                            (Char *) model.bName, modelId, models, fmark));

       /* Otherwise, if it is a new, unstreamed model, write it out from memory */
      } else if (modhand->model) {
        if (!(success = VFILE_writeStructure(voiFile, modhand->model, 1, RS_FUPATE) == VFILE_SUCCESS))
          models->status = CANTSAVEMODELS;
        VBX_DEBUG(modhand->model->bName[modhand->model->bNameCount] = 0);
        VBX_DEBUG(VBX_print("  MODELS_save: saved NEW model for \"%s\" (mid = %d) from models @%p to voice file at offset %ld\n",
                            (Char *) modhand->model->bName, modelId, models, fmark));
        free(modhand->model);
        modhand->model = NULL;
        models->newModels--;

       /* Otherwise, something is rotten in Denmark */
      } else {
        models->status = FATALINCONSISTENCY;
        success = FALSE;
        assert(FALSE);
      }

       /* Record the position of the model in the new stream */
      modhand->fmark = fmark;
    }
    if (success) {
      if (models->newModels != 0) {
        models->status = FATALINCONSISTENCY;
        success = FALSE;
        assert(FALSE);
      } else {
        models->headerTouched = FALSE;
        models->status = VISESUCCESS;
      }
    }
  } else if (models) {
    models->status = INVALIDARGUMENT;
  }

  return(success);
}


UInt
MODELS_addModel(MODELS models, FUPATE * model)
/***************************************************************************
 *
 * Adds a new model to the model set and returns its model ID.  Returns zero
 * if unsuccessful.
 *
 * THE CALLER IS RESPONSIBLE FOR ALLOCATING AND FREEING THE MODEL STRUCTURE.
 *
 ***************************************************************************/
{
  UInt      modelId = 0;
  MODHAND   modhand;
  FUPATE  * modelCopy;
  Bool      status = TRUE;

  if (models && model && models->models && models->modset && CRITSECT_EXISTS(models->modset->critsect)) {
    CRITSECT_ENTER(models->modset->critsect);

     /* Enlarge the array of model handles if necessary */
    if (++models->numModels >= models->modelsSize) {
      models->modelsSize += MODHANDARRAY_GROWTHQUANTUM;
      modhand = (MODHAND) realloc(models->models, (size_t) ((models->modelsSize + 1) * sizeof(modhand_t)));
      if (modhand) {
        models->models = modhand;
        status = TRUE;
      } else {
        VBX_DEBUG(VBX_print("MODELS_addModel: ERROR - realloc FAILED\n"));
        models->modelsSize -= MODHANDARRAY_GROWTHQUANTUM;
        status = FALSE;
      }
    } else {
      modhand = models->models;
    }
    if (status && modhand && (modelCopy = (FUPATE *) malloc(sizeof(FUPATE)))) {
      memcpy(modelCopy, model, sizeof(FUPATE));
      modelId = models->numModels;
      modhand[modelId].model = modelCopy;
      modhand[modelId].fmark = 0;
      models->newModels++;
      models->status = VISESUCCESS;
    } else {
      models->status = OUTOFMEMORY;
    }

    CRITSECT_LEAVE(models->modset->critsect);
  } else if (models) {
    models->status = INVALIDARGUMENT;
  }

  return(modelId);
}


Bool
MODELS_getModel(MODELS models, UInt modelId, FUPATE * model)
/***************************************************************************
 *
 * Gets a model from a models block
 * If the model parameter is NULL, returns TRUE iff the model exists;
 * otherwise, returns TRUE iff the model is successfully extracted.
 *
 * THE CALLER IS RESPONSIBLE FOR ALLOCATING AND FREEING THE MODEL STRUCTURE.
 *
 ***************************************************************************/
{
  MODHAND     modhand;
  VFILE       voiFile;
  VFILE_MARK  fmark;
  FUPATE      discard;
  Bool        success;

  if ((success = (models && models->modset && CRITSECT_EXISTS(models->modset->critsect)))) {
    CRITSECT_ENTER(models->modset->critsect);

    if ((success = (models->models && 1 <= modelId && modelId <= models->numModels))) {

      modhand = models->models + modelId;
      models->status = VISESUCCESS;

       /* If the model is in the voice stream, read it from there */
      if (modhand->fmark) {
         /* Success presupposes finding the underlying voice file and successfully seeking to the desired position in it */
        if ((success = ((voiFile = models->modset->voiFile) && VFILE_placeMark(voiFile, &fmark) == VFILE_SUCCESS &&
                        VFILE_gotoMark(voiFile, &modhand->fmark) == VFILE_SUCCESS))) {
           /* If the model parameter is NULL, just make sure the model is readable */
          if (!model) model = &discard;
           /* Success is being able to read the model and seek back to the original position */
          if (!(success = MODEL_read(model, voiFile) && VFILE_gotoMark(voiFile, &fmark) == VFILE_SUCCESS))
            models->status = CANTREADVFILESTRUCT;
        } else {
          models->status = CANTSEEKVFILEMARK;
        }

       /* Otherwise, if it is in memory, copy it from there */
      } else if (modhand->model) {
        if (model)
          memcpy(model, (void *) modhand->model, sizeof(FUPATE));

       /* Otherwise, no such model exists (FATAL inconsistency) */
      } else {
        models->status = FATALINCONSISTENCY;
        success = FALSE;
        VBX_fatalIf(FALSE);
      }
    }

    CRITSECT_LEAVE(models->modset->critsect);
  } else if (models) {
    models->status = INVALIDARGUMENT;
  }

  return(success);
}


Bool
MODELS_putModel(MODELS models, UInt modelId, FUPATE * model)
/***************************************************************************
 *
 * Replace a model in the models block with a new one
 * If the model parameter is NULL, returns TRUE iff replacement is possible;
 * otherwise, returns TRUE iff the replacement is successful.  Does not add
 * new models to the block; call MODELS_addModel for that.
 *
 * THE CALLER IS RESPONSIBLE FOR ALLOCATING AND FREEING THE MODEL STRUCTURE.
 *
 ***************************************************************************/
{
  MODHAND     modhand;
  VFILE       voiFile;
  VFILE_MARK  fmark;
  FUPATE      oldModel;
  Bool        success;

  if ((success = (models && models->modset && CRITSECT_EXISTS(models->modset->critsect)))) {
    CRITSECT_ENTER(models->modset->critsect);

    if ((success = (models->models && 1 <= modelId && modelId <= models->numModels && models->modset))) {

      modhand = models->models + modelId;
      models->status = VISESUCCESS;

       /* If the old model is in the voice file ... */
      if (modhand->fmark) {
         /* Success presupposes finding the underlying voice file and successfully seeking to the desired position in it */
        if ((success = ((voiFile = models->modset->voiFile) && VFILE_placeMark(voiFile, &fmark) == VFILE_SUCCESS &&
                        VFILE_gotoMark(voiFile, &modhand->fmark) == VFILE_SUCCESS))) {
           /* Success entails reading the old model */
          if ((success = (VFILE_readStructure(voiFile, &oldModel, 1, RS_FUPATE) == VFILE_SUCCESS))) {
            if (model) {
               /* If the new and old models are the same size, write the new one over the old one in the stream */
              if (oldModel.bNKernWordMod == model->bNKernWordMod) {
                success = (VFILE_gotoMark(voiFile, &modhand->fmark) == VFILE_SUCCESS &&
                           VFILE_insertStructure(voiFile, model, 1, RS_FUPATE) == VFILE_SUCCESS);
               /* Otherwise, allocate memory for the new model and copy the new model into it */
              } else if ((success = (modhand->model = (FUPATE *) malloc(sizeof(FUPATE))) != NULL)) {
                memcpy(modhand->model, model, sizeof(FUPATE));
                modhand->fmark = 0;
                models->newModels++;
              } else {
                models->status = OUTOFMEMORY;
              }
            }
          } else {
            models->status = CANTREADVFILESTRUCT;
          }
           /* Success entails seeking back to the original position in the voice file */
          if (!(success = (VFILE_gotoMark(voiFile, &fmark) == VFILE_SUCCESS) && success) && models->status == VISESUCCESS)
            models->status = CANTSEEKVFILEMARK;
          
        } else {
          models->status = CANTREADVOIFILE;
        }

       /* Otherwise, if it is in memory, replace it with the new one */
      } else if (modhand->model) {
        if (model)
          memcpy(modhand->model, model, sizeof(FUPATE));

       /* Otherwise, no such model exists (FATAL inconsistency) */
      } else {
        models->status = FATALINCONSISTENCY;
        success = FALSE;
        assert(FALSE);
      }
    } else {
      models->status = MISSINGWORDMODEL;
    }
    CRITSECT_LEAVE(models->modset->critsect);
  } else if (models) {
    models->status = INVALIDARGUMENT;
  }

  return(success);
}


UInt
MODELS_numNew(MODELS models)
/***************************************************************************
 *
 * Returns the number of models not currently in the voice stream
 *
 ***************************************************************************/
{
  if (models)
    return(models->newModels);
  else
    return(0);
}


static Int
MODELS_parameter(MODELS models, UInt parmId)
/**************************************************************************
 *
 * Returns the value of the requested parameter, or a negative number if
 * no such parameter is available
 * 
 **************************************************************************/
{
  UInt        numParms;
  FUPATPARM * parameter;
  Int         value = -1;

  assert(models);

  numParms = models->header->bNumberParms;
  parameter = models->parameter;
  
  if (parameter && numParms)
    while (numParms--)
      if (parmId == (UInt) parameter[numParms].bNumber) {
        value = (Int) parameter[numParms].wValue;
        break;
      }

  if (value == -1)
    models->status = INVALIDARGUMENT;
  else
    models->status = VISESUCCESS;

  return(value);
}


Char *
MODELS_vocabName(MODELS models)
/***************************************************************************
 *
 * Gets the name of the vocabulary for this set of models
 *
 ***************************************************************************/
{
  FUPATHEAD * header;
  Char      * name;

  assert(models);

  if ((name = models->vocabName) == NULL) {
    header = models->header;
    if (header && header->bVocabNameLength) {
      if ((name = (Char *) VBX_malloc((size_t) header->bVocabNameLength + 1)) != NULL) {
        strncpy(name, header->bVocabName, (size_t) header->bVocabNameLength);
        name[header->bVocabNameLength] = '\0';
        models->vocabName = name;
        models->status = VISESUCCESS;
      } else {
        models->status = OUTOFMEMORY;
      }
    }
  }

  return(name);
}


Bool
MODELS_getTraining(MODELS models, UInt * sum, UInt * sumSq, UInt * count, Bool reset)
/***************************************************************************
 *
 * If reset is TRUE, REPLACE the quantities that the arguments point to
 * with the values of the corresponding wildcard parameters in the model
 * set.  Otherwise, ADD the parameters to the corresponding quantities
 * pointed to.  Returns FALSE if there is no header, TRUE otherwise.
 * NOTE THAT THIS FUNCTION DOES NOT MODIFY THE VALUES OF THE WILDCARD
 * PARAMETERS IN THE MODEL SET.  Use MODELS_setTraining() for that.  Nor
 * does it compute the training statistics over the active models, instead
 * just reading them from the header.  To compute them, call
 * VOCAB_getTraining().
 *
 ***************************************************************************/
{
  FUPATHEAD * header;

  assert(models);

  header = models->header;
  if (header) {
    if (reset) {
      *sum   = header->lWildcardSum;
      *sumSq = header->lWildcardSumSquares;
      *count = (UInt) header->wWildcardCount;
    } else {
      *sum   += header->lWildcardSum;
      *sumSq += header->lWildcardSumSquares;
      *count += (UInt) header->wWildcardCount;
    }
    return(TRUE);
  } else {
    return(FALSE);
  }
}


Bool
MODELS_setTraining(MODELS models, UInt sum, UInt sumSq, UInt count, Bool reset)
/***************************************************************************
 *
 * If reset is TRUE, REPLACE the values of the wildcard parameters in the
 * model set with the corresponding arguments.  Otherwise, ADD the arguments
 * to the parameters.  In both cases, multiply sum and sumSq by count.
 * Returns FALSE and does nothing if there is no header or if the new
 * count would exceed USHRT_MAX, TRUE otherwise.
 *
 ***************************************************************************/
{
  FUPATHEAD * header;

  assert(models);

  header = models->header;
  if (header) {
    models->headerTouched = TRUE;
    if (reset) {
      if (count <= USHRT_MAX) {
        header->lWildcardSum = sum * count;
        header->lWildcardSumSquares = sumSq * count;
        header->wWildcardCount = (UShort) count;
        return(TRUE);
      }
    } else {
      if (count + (UInt) header->wWildcardCount <= USHRT_MAX) {
        header->lWildcardSum += sum * count;
        header->lWildcardSumSquares += sumSq * count;
        header->wWildcardCount += (UShort) count;
        return(TRUE);
      }
    }
  }

  return(FALSE);
}


Int
MODELS_trainScheme(MODELS models)
/***************************************************************************
 *
 ***************************************************************************/
{
  if (models && models->header)
    return (Int)models->header->bTrainScheme;

  return -1;
}


void
MODELS_setSchemeBatch(MODELS models)
/***************************************************************************
 *
 ***************************************************************************/
{
  if (models && models->header) {
    models->header->bTrainScheme = TRAIN_SCHEME_BATCH;
    models->headerTouched = TRUE;
  }
}

Bool
MODELS_mustSave(MODELS models)
/***************************************************************************
 *
 ***************************************************************************/
{
  if (models && (MODELS_numNew(models) > 0 || models->headerTouched))
    return TRUE;
  return FALSE;
}

MODITER
MODITER_create(MODELS models)
/***************************************************************************
 *
 * Returns an iterator to use to read successive models from a models block,
 * or NULL if the block is empty.
 *
 ***************************************************************************/
{
  MODITER  moditer = NULL;

  if (models && models->modset && CRITSECT_EXISTS(models->modset->critsect)) {
    CRITSECT_ENTER(models->modset->critsect);
    if (models->models && models->numModels && (moditer = (MODITER) malloc(sizeof(moditer_t)))) {
      moditer->models = models;
      moditer->modelId = 0;
    }
    CRITSECT_LEAVE(models->modset->critsect);
  }

  return(moditer);
}


void
MODITER_destroy(MODITER moditer)
/***************************************************************************
 *
 * Destroy a model iterator
 *
 ***************************************************************************/
{
  VBX_free(moditer);
}


UInt
MODITER_nextModelId(MODITER moditer)
/***************************************************************************
 *
 * Returns the Id of the next model in the associated models block.
 * Returns zero when the models are exhausted.
 *
 ***************************************************************************/
{
  if (moditer && ++moditer->modelId <= moditer->models->numModels) {
    return(moditer->modelId);
  } else {
    return(0);
  }
}


Bool
MODEL_init(FUPATE * model, Char * name, UInt numKernels)
/**************************************************************************
 *
 * Initialize and name a model
 * 
 **************************************************************************/
{
  if (model && name && MODEL_rename(model, name)) {
    model->lSum = 0L;               /* weighted sum of scores for word */
    model->lSquares = 0L;           /* weighted sum of squares of scores */
    model->wCount = 0;              /* weighted training count */
    model->wTrainingCount = 0;      /* # of times word has been trained */
    model->wTrainingSavedCount = 0;
    model->bNKernWordMod = (V_Byte) numKernels;  /* word model kernel count */
    model->bTemplateCount = 0;                   /* template kernel count */
    return(TRUE);
  } else {
    return(FALSE);
  }
}


static Bool
MODEL_read(FUPATE * model, VFILE voiFile)
/**************************************************************************
 *
 * Read a model from the voice stream into a pre-allocated model structure.
 * Returns TRUE if successful, FALSE otherwise.
 * THE STREAM IS ASSUMED TO BE POSITIONED ON THE MODEL STRUCTURE.
 *
 **************************************************************************/
{
  if (VFILE_SUCCESS == VFILE_readStructure(voiFile, model, 1, RS_FUPATE)) {
     /* Zero terminate the word name */
    model->bName[model->bNameCount] = 0;
    return(TRUE);
  } else {
    return(FALSE);
  }
}


Bool
MODEL_copy(FUPATE * dst, FUPATE * src, Bool copyDwells, Bool copyName)
/**************************************************************************
 *
 * Copy the source models's data into the destination model.  Optionally
 * copies the kernel dwells and the name.  If the kernel dwells are not
 * copied, the models must have the same number of kernels.
 * 
 **************************************************************************/
{
  if (copyDwells && copyName) {
    memcpy(dst, src, sizeof(FUPATE));
  } else if (copyDwells && !copyName) {
    memcpy(dst, src, ((size_t)&(((FUPATE *)NULL)->bNameCount)));
  } else if (!copyDwells) {
    if (dst->bTemplateCount == src->bTemplateCount &&
        dst->bNKernWordMod == src->bNKernWordMod &&
        src->bTemplateCount == src->bNKernWordMod) {
      /* Copy the training counts, etc. */
      dst->lSum = src->lSum;
      dst->lSquares = src->lSquares;
      dst->wCount = src->wCount;
      dst->wTrainingCount = src->wTrainingCount;
      dst->wTrainingSavedCount = src->wTrainingSavedCount;
      /* Copy the kernel templates */
      memcpy((void *) dst->bTemplate, (void *) src->bTemplate, (size_t) (UPAT_NUM_PARMS * src->bTemplateCount));
      /* Optionally, copy the name, including its length and terminator */
      if (copyName)
        memcpy((void *) &dst->bNameCount, (const void *) &src->bNameCount, src->bNameCount + 2);
    } else {
      return(FALSE);
    }
  }
  return(TRUE);
}


Bool
MODEL_rename(FUPATE * model, Char * name)
/**************************************************************************
 *
 * Change the name of the model
 * 
 **************************************************************************/
{
  size_t nameLen = strlen(name);

  if (nameLen > UPAT_MAX_NAME)
    return(FALSE);

  strcpy((Char *) model->bName, name);
  model->bNameCount = (UChar) nameLen;

  return(TRUE);
}

jin
MODEL_template(FUPATE * model, UChar kid)
/***************************************************************************
 *
 * Return the template (as jin data) for the kid-th (0-origin) kernel
 * in the specified model.
 *
 ***************************************************************************/
{
  assert(model && kid < model->bTemplateCount);

  return((jin) (model->bTemplate + UPAT_NUM_PARMS * kid));
}


void
MODEL_swapDwells(FUPATE * model, UChar kid, UChar * minDwell, UChar * maxDwell)
/***************************************************************************
 *
 * Report the current values of the dwells for the kid-th (0-origin) kernel
 * in the specified model and change them to the values passed.
 *
 ***************************************************************************/
{
  UChar    dwell, * dwellPtr;

  if (model && kid < model->bNKernWordMod) {

    assert(minDwell && maxDwell);

    /* Swap minDwell */
    dwellPtr = model->bKernelDwells + 2 * kid;
    dwell = *dwellPtr;
    *dwellPtr = *minDwell;
    *minDwell = dwell;

    /* Swap maxDwell */
    dwellPtr++;
    dwell = *dwellPtr;
    *dwellPtr = *maxDwell;
    *maxDwell = dwell;
  }
}


void
MODEL_initializeDwells(FUPATE * pattern)
/**************************************************************************
 *
 * Make the mindwells for pattern's kernels 1 and the maxdwells as big as
 * possible, consistent with the limit (MAXDWELL) on the length of words.
 * N.B. MAXDWELL is (version.h)-dependent.  This function is primarily
 * useful for initializing models before training their dwells.
 * 
 **************************************************************************/
{
  Int  k, sumOfMaxDwells = 0;

  for (k = 0; k < pattern -> bNKernWordMod; k++) {
    if ((pattern -> bKernelDwells)[2 * k + 1] > MAXKERNELDWELL)
      (pattern -> bKernelDwells)[2 * k + 1] = MAXKERNELDWELL;
    sumOfMaxDwells += (pattern -> bKernelDwells)[2 * k + 1];
  }
  for (k = 0; k < pattern -> bNKernWordMod; k++) {
    (pattern -> bKernelDwells)[2 * k] = 1;
    (pattern -> bKernelDwells)[2 * k + 1] =
       ((pattern -> bKernelDwells)[2 * k + 1] * MAXDWELL) / sumOfMaxDwells;
    if ((pattern -> bKernelDwells)[2 * k + 1] > MAXKERNELDWELL)
      (pattern -> bKernelDwells)[2 * k + 1] = MAXKERNELDWELL;
  }
}
