/* template.c - Response template facility
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * Methods for dealing with the response templates used by the response
 * facility.  This module is an adaptation of some truly ancient code
 * from the 1980's.  The highly convoluted so-called "Global Response Facility"
 * has been expunged, however, since it appeared to be useless in the new
 * context in which this module will be used.
 * 
 * HISTORY
 * 11-Nov-97  Craig A. Vanderborgh (craigv@verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"
#include <assert.h>
#include "types.h"
#include "vbx.h"

#include "recfile.h"
#include "hyp.h"
#include "parse.h"
#include "tree.h"
#include "buds.h"
#include "template.h"

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#endif

#define DFLTFLOFFALLOC         128
#define MAXWID                 32000
#define SMALLSTR               32
#define SMAX                   128
#define STATE_FLAG             0x80000000L
#define START_STATE_ID(TE)     (((UShort) (TE)) & 0x7FFF)
#define END_STATE_ID(TE)       (((UShort) (TE >> 16)) & 0x7FFF)
#define TEXT_LENGTH(TE)        ((UChar) TE)
#define TEXT_OFFSET(TE)        (TE >> 8)

/*---------------------------------------------------------------------------*
 * TEMPLATETABLE contains an array of word offsets to templates for creating 
 * responses to recognition events.  The offsets are relative to the beginning
 * of the template storage area, which is an array of integers
 *---------------------------------------------------------------------------*/

typedef struct templateTable_s {
  UShort templateCount;
  UInt  *templateOffsets;
  UChar *templateData;
} templateTable_t, *TEMPLATETABLE;

Ptr
TEMPLATE_load(VFILE recFile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Load a template block from the specified recognition file
 * Return: Pointer to the loaded template
 *---------------------------------------------------------------------------*/
{
  FTEMPLATEHDR  header;
  FTEMPLATEHDRE hent;
  FTEMPLATEE    entry;
  UShort       *sPtr;
  ULong         blockSize;
  TEMPLATETABLE tmpl = (TEMPLATETABLE) NULL;
  Int           i, tAlloc;

  if (VFILE_SUCCESS == VFILE_readStructure(recFile, &header, 1, RS_FTEMPLATEHDR)) {
    tmpl = (TEMPLATETABLE) VBX_calloc(1, sizeof(templateTable_t));
    tmpl->templateCount = header.wTemplateCount;

    if (tmpl->templateCount == 0)
      FATAL_BRA_ "Zero template count encountered in tmpl block - goodbye!\n" _KET;

    /* Allocate and read the offsets for all the templates */
    tmpl->templateOffsets = (UInt *) VBX_calloc(tmpl->templateCount, sizeof(UInt));
    VFILE_readStructure(recFile, tmpl->templateOffsets, tmpl->templateCount, RS_FTEMPLATEHDRE);

    /* Allocate and read the template data */
    blockSize = VFILE_currentBlockSize(recFile);
    assert(blockSize > 0);
    tAlloc = blockSize - header.lTemplateOffset;
    assert(tAlloc > 0 && (tAlloc % 2) == 0);
    tmpl->templateData = VBX_calloc(tAlloc, sizeof(UChar));

    sPtr = (UShort *) tmpl->templateData;
    for (i = 0; i < tAlloc / 2; i++) {
      if (VFILE_SUCCESS != VFILE_readStructure(recFile, &entry, 1, RS_FTEMPLATEE))
        FATAL_BRA_ "TEMPLATE_load: Can't read template data entry\n" _KET;
     *sPtr = entry.wTemplateData;
      sPtr++;
    }
  }

  return((Ptr) tmpl);
}

void
TEMPLATE_annihilate(Ptr ptr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free a template block and its associated resources
 *---------------------------------------------------------------------------*/
{
  TEMPLATETABLE tmpl = (TEMPLATETABLE) ptr;

  if (tmpl) {
    VBX_free(tmpl->templateOffsets);
    VBX_free(tmpl->templateData);
    VBX_free(tmpl);
  }
}

TemplateEl *
TEMPLATE_lookup(Ptr templateTablePtr, UShort templateIdx)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns a pointer to the template at the specified index.  If the template
 * table is NULL or the index is invalid, the function returns NULL.
 * Return:  TemplateEl pointer
 *---------------------------------------------------------------------------*/
{
  TEMPLATETABLE templateTable = (TEMPLATETABLE) templateTablePtr;

  if (templateTable == NULL || templateIdx >= templateTable->templateCount)
    return(NULL);
  else
    return((TemplateEl *) ((UChar *) templateTable->templateData + templateTable->templateOffsets[templateIdx]));
}

Bool 
TEMPLATE_makeResponse(KB kb, UInt type, TemplateEl *template, VFILE recFile,
                      TREENODE parse, UInt vocabId, UInt grammarId,
                      Char *buffer, UInt bufSize, UInt *sizeNeeded)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a response string of the specified type given an input template,
 * parse, vocab, and grammar ID.  The "type" parameter determines whether
 * host, voice, or display response text is generated.
 * Return:  Status of request
 *---------------------------------------------------------------------------*/
{
  Char    initiator[SMALLSTR];
  Char    separator[SMALLSTR];
  Char    terminator[SMALLSTR];
  Char    trans[SMAX];
  Int     i, len;
  Bool    rval;
  DELIN   delin;
  FLOFF  *floffPtr;
  FLOFF  *floffArray;
  Int     floffAlloc;
  Int     floffEntries;
  Int     textLen;
  UShort  wordId;
  TemplateEl element;
  TREENODE startNode, endNode;
    
  /* reject a null template pointer */
  if (template == NULL) {
    VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: NULL template, return FALSE\n"));
    return(FALSE);
  }
    
  delin = KB_getDelin(kb, type, vocabId, grammarId);
  assert(delin != NULL);

  rval = VFILE_getText(recFile, &delin->initiator, (UChar *)initiator);
  if (rval != VFILE_SUCCESS)
    WARN_BRA_ "Can't find initiator text in rec file" _KET;
  rval = VFILE_getText(recFile, &delin->separator, (UChar *)separator);
  if (rval != VFILE_SUCCESS)
    WARN_BRA_ "Can't find separator text in rec file" _KET;
  rval = VFILE_getText(recFile, &delin->terminator, (UChar *)terminator);
  if (rval != VFILE_SUCCESS)
    WARN_BRA_ "Can't find terminator text in rec file" _KET;
  
  element = *template;   
    
  if (strlen(initiator) > bufSize) {
    VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: buffer overflow\n"));
    return(FALSE);
  }

  buffer[0] = '\0';

  floffAlloc = DFLTFLOFFALLOC;
  floffArray = (FLOFF *) VBX_calloc(floffAlloc, sizeof(FLOFF));
  floffEntries = 0;

  while ((element = *template) != 0) {
    VBX_DEBUG(VBX_print("TEMPLATE_makeResponse(element->%p)\n", element));
    if (element & STATE_FLAG) {
    
      /* element is state id, find start state in the parse */
      if (parse == NULL) {
        VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: NULL parse, return FALSE\n"));
        return(FALSE);
      }
      if ((startNode = TEMPLATE_findState(parse, (UShort) START_STATE_ID(element))) == NULL) {
        VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: NULL parse, return FALSE\n"));
        return(FALSE);
      }

      /* find end state in the parse */
      if ((endNode = TEMPLATE_findState(startNode, (UShort) END_STATE_ID(element))) == NULL) {
        VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: can't find END_STATE_ID\n"));
        return(FALSE);
      }
      template++;

      /* obtain all word ids along parse between start node and  next node */
      while (startNode != endNode) {
        if ((wordId = (startNode)->branchWordId) != 0) {

         /* wordId is not NULL_WORD, it's a word id so look up 
          * its translation and add it to the response string */
          floffPtr = KB_translateWordId(kb, type, wordId, vocabId, grammarId);
          if (floffPtr == NULL)
            continue;

          floffArray[floffEntries] = *floffPtr;
          floffEntries++;

          if (floffEntries >= floffAlloc) {
            floffAlloc *= 2;
            floffArray = (FLOFF *) VBX_realloc(floffArray, floffAlloc * sizeof(FLOFF));
          }
        }
        startNode = startNode->motherPtr;
      }
    } else {
      /* This element is text */
      floffArray[floffEntries].bLength = TEXT_LENGTH(element);
      floffArray[floffEntries].lOffset = TEXT_OFFSET(element);
      floffEntries++;
      template++;
    }
  }

  /* if this is a null response then just return here */
  if (floffEntries == 0) {
    VBX_free(floffArray);
    return(TRUE);
  }

  /* Calculate the length translation result to see if it fits in buffer */
  for (i = textLen = 0; i < floffEntries; i++)
    textLen += floffArray[i].bLength;

  textLen += (floffEntries - 1) * strlen(separator);
  textLen += strlen(initiator) + strlen(terminator);

 *sizeNeeded = textLen;
  if (textLen > bufSize) {
    WARN_BRA_"TEMPLATE_makeResponse: translation buffer too small!!\n" _KET;
    VBX_free(floffArray);
    return(FALSE);
  }

  /* Assemble the translation result */
  strcpy(buffer, initiator);
  for (i = 0; i < floffEntries; i++) {
    rval = VFILE_getText(recFile, &floffArray[i], (UChar *)trans);
    if (rval != VFILE_SUCCESS) {
      WARN_BRA_ "Can't find translation text in rec file" _KET;
      continue;
    }
    strcat(buffer, trans);
    if (i < floffEntries - 1)
      strcat(buffer, separator);
  }
  strcat(buffer, terminator);

  /* Clean up dynamic memory allocations */
  VBX_free(floffArray);
    
  VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: \"%s\"\n", buffer));
  return(TRUE);
}

Bool 
TEMPLATE_makeResponseHyp(KB kb, UInt type, VFILE recFile, HYP hyp, UInt vocabId, UInt grammarId, Char *buffer, UInt bufSize, UInt *sizeNeeded)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a response string of the specified type given an input hypothesis,
 * vocab ID, and grammar ID.  The word sequence as given in the hypothesis
 * is the basis for the response.  The "type" parameter determines whether
 * host, voice, or display response text is generated.
 * Return:  Status of request
 *---------------------------------------------------------------------------*/
{
  Char   initiator[SMALLSTR];
  Char   separator[SMALLSTR];
  Char   terminator[SMALLSTR];
  Char   trans[SMAX];
  Int    i, len;
  FLOFF  *floffPtr;
  FLOFF  *floffArray;
  Int     floffAlloc;
  Int     floffEntries;
  Int     textLen;
  Bool    rval;
  DELIN   delin;
  UShort  wordId;
  UInt    wordCount;
  WRDHYP  words;
    
  delin = KB_getDelin(kb, type, vocabId, grammarId);
  assert(delin != NULL);

  rval = VFILE_getText(recFile, &delin->initiator, (UChar *)initiator);
  if (rval != VFILE_SUCCESS)
    WARN_BRA_ "Can't find initiator text in rec file" _KET;
  rval = VFILE_getText(recFile, &delin->separator, (UChar *)separator);
  if (rval != VFILE_SUCCESS)
    WARN_BRA_ "Can't find separator text in rec file" _KET;
  rval = VFILE_getText(recFile, &delin->terminator, (UChar *)terminator);
  if (rval != VFILE_SUCCESS)
    WARN_BRA_ "Can't find terminator text in rec file" _KET;
  
  if (strlen(initiator) > bufSize) {
    VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: buffer overflow\n"));
    return(FALSE);
  }

  buffer[0] = '\0';
  floffAlloc = DFLTFLOFFALLOC;
  floffArray = (FLOFF *) VBX_calloc(floffAlloc, sizeof(FLOFF));
  floffEntries = 0;

  /* provide translations for the wordId sequence given by HYP */
  words = HYP_words(hyp, &wordCount);
  for (i = 0; i < wordCount; i++) {
    wordId = WRDHYP_wid(words + i);
    if (wordId > MAXWID)
      continue;

    floffPtr = KB_translateWordId(kb, type, wordId, vocabId, grammarId);
    if (floffPtr == NULL)
      continue;

    floffArray[floffEntries] = *floffPtr;
    floffEntries++;

    if (floffEntries >= floffAlloc) {
      floffAlloc *= 2;
      floffArray = (FLOFF *) VBX_realloc(floffArray, floffAlloc * sizeof(FLOFF));
    }
  }

  /* if this is a null response then just return here */
  if (floffEntries == 0) {
    VBX_free(floffArray);
    return(TRUE);
  }

  /* Calculate the length translation result to see if it fits in buffer */
  for (i = textLen = 0; i < floffEntries; i++)
    textLen += floffArray[i].bLength;

  textLen += (floffEntries - 1) * strlen(separator);
  textLen += strlen(initiator) + strlen(terminator);

 *sizeNeeded = textLen;
  if (textLen > bufSize) {
    WARN_BRA_"TEMPLATE_makeResponse: translation buffer too small!!\n" _KET;
    VBX_free(floffArray);
    return(FALSE);
  }

  /* Assemble the translation result */
  strcpy(buffer, initiator);
  for (i = 0; i < floffEntries; i++) {
    rval = VFILE_getText(recFile, &floffArray[i], (UChar *)trans);
    if (rval != VFILE_SUCCESS) {
      WARN_BRA_ "Can't find translation text in rec file" _KET;
      continue;
    }
    strcat(buffer, trans);
    if (i < floffEntries - 1)
      strcat(buffer, separator);
  }
  strcat(buffer, terminator);

  /* Clean up dynamic memory allocations */
  VBX_free(floffArray);
    
  VBX_DEBUG(VBX_print("TEMPLATE_makeResponse: \"%s\"\n", buffer));
  return(TRUE);
}

TREENODE
TEMPLATE_findState(TREENODE trace, UShort state)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Locate the node that has the state id "state".  If successful, a pointer
 * to the node is returned in "node"
 * Return:  Status of request
 *---------------------------------------------------------------------------*/
{
  TREENODE node = (TREENODE) NULL;

  if (trace) {
    while (trace) {
      if (state == trace->nodeStateId) {
        node = trace;
        break;
      } else
        trace = trace->motherPtr;
    }
  }

  /* Get here only if it never finds the state it's looking for. */
  return(node);
}

