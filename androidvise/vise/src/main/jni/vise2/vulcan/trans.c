/* trans.c - VISE Translations Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * Reads and maintains the translations for grammars and vocabularies.
 * The translations are used via KB by the response facililty.  This
 * implementation reflects the "100% FLOFF" design choice made earlier
 * this fall.  Accordingly, access to the actual text is via recfile
 * text lookup ONLY.
 * 
 * HISTORY
 * 15-Oct-97  Craig Vanderborgh (craigv@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "ntmath.h"
#include "vbx.h"
#include "llist.h"
#include "utils.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "hash.h"
#include "parse.h"
#include "trans.h"

TRANS
TRANS_create()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new translation object
 * Return: Pointer to the new object
 *---------------------------------------------------------------------------*/
{
  TRANS trans = (TRANS) NULL;

  trans = (TRANS) VBX_calloc(1, sizeof(trans_t)); 
  return(trans);
}

void
TRANS_annihilate(TRANS trans)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free a translation object, and any PARSEs or DELINeators it contains
 *---------------------------------------------------------------------------*/
{
  if (trans) {
    /* Free the parse tables */
    PARSE_destroyTable(trans->controlParseTbl);
    PARSE_destroyTable(trans->voiceParseTbl);
    PARSE_destroyTable(trans->hostParseTbl);
    PARSE_destroyTable(trans->displayParseTbl);

    /* Free the delineators */
    DELIN_annihilate(trans->HRDelin);
    DELIN_annihilate(trans->VRDelin);
    DELIN_annihilate(trans->DRDelin);

    /* Destroy the translation hash */
    HASH_annihilate(trans->widHT);

    VBX_free(trans);
  }
}

TRANSENT
TRANSENT_create(UInt wid, FLOFF hostTrans, FLOFF voiceTrans, FLOFF displayTrans)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new translation entry, given length/offset specifiers for the
 * host, voice, and display translation text
 * Return: Pointer to the new translation entry
 *---------------------------------------------------------------------------*/
{
  TRANSENT transent;

  transent = (TRANSENT) VBX_calloc(1, sizeof(transent_t));
  transent->wid = wid;
  transent->hostTrans = hostTrans;
  transent->voiceTrans = voiceTrans;
  transent->displayTrans = displayTrans;
 
  return(transent);
}

void
TRANSENT_annihilate(TRANSENT transent)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free a translation entry
 *---------------------------------------------------------------------------*/
{
  if (transent) {
    VBX_free(transent);
  }
}

DELIN
DELIN_create()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new DELINeator object
 *---------------------------------------------------------------------------*/
{
  DELIN delin;

  delin = (DELIN) VBX_calloc(1, sizeof(delin_t));

  return(delin);
}

void
DELIN_annihilate(DELIN delin)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free a DELINeator object
 *---------------------------------------------------------------------------*/
{
  if (delin) {
    VBX_free(delin);
  }
}
