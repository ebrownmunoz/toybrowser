/* spwrds.c- VISE Special Words Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Currently just defines the silence vocabulary
 * 
 * HISTORY
 * 13-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: spwrds.c[1.0] Mon Apr  7 23:26:51 1997 dvetter@titan saved $";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "vocab.h"
#include "spwrds.h"

/* N.B. NUM_MULTISILENCES (see spwrds.h) must correspond to the number here */

/* Vocabulary map for the silences */
vocmap_t silenceMap[] = {
  { "*multi1", FIRST_MULTISILENCE,     SILENCE_CLASS },
  { "*multi2", FIRST_MULTISILENCE + 1, SILENCE_CLASS },
  { "*multi3", FIRST_MULTISILENCE + 2, SILENCE_CLASS },
  { "*multi4", FIRST_MULTISILENCE + 3, SILENCE_CLASS },
  { "*multi5", FIRST_MULTISILENCE + 4, SILENCE_CLASS },
  { "*multi6", FIRST_MULTISILENCE + 5, SILENCE_CLASS },
  { "*multi7", FIRST_MULTISILENCE + 6, SILENCE_CLASS },
  { "*multi8", FIRST_MULTISILENCE + 7, SILENCE_CLASS },
  { "*multi9", FIRST_MULTISILENCE + 8, SILENCE_CLASS },
  { "*short" , SHORT_SILENCE,          SILENCE_CLASS },
  { "*long"  , LONG_SILENCE,           SILENCE_CLASS },
  { "*short" , SHORT_ADAPTIVE_SIL,     SILENCE_CLASS },
  { "*long"  , LONG_ADAPTIVE_SIL,      SILENCE_CLASS },
  /* wordId == 0 signifies end of map */
  {  NULL, 0, 0 }
};
