/* hyp.c - VISE Hypothesis Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Methods for recognition hypotheses
 * 
 * HISTORY
 * 20-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "vbx.h"
#include "hyp.h"
#include "sn.h"
#include "vocab.h"

 /* Initial number of WRDHYPs allocated per hypothesis */
#define HYP_INITIALMAXWORDS  20
#define HYP_WORDCOUNTEXCESS  10

#define TEXT_GROWTH_QUANTUM  100


HYP
HYP_create(UInt nBest)
/**************************************************************************
 *
 * Create an array for up to nBest hypotheses.  An hypothesis with no
 * allocation for words delimits the array.
 * 
 **************************************************************************/
{
  HYP  hyp = (HYP) calloc((size_t) (nBest + 1), sizeof(hyp_t));
  UInt i;

  if (hyp) {
    for (i = 0; i < nBest; i++) {
      if ((hyp[i].words = (WRDHYP) calloc((size_t) HYP_INITIALMAXWORDS, sizeof(wrdhyp_t))))
        hyp[i].maxWordCount = HYP_INITIALMAXWORDS;
      else
        break;
    }
    if (i != nBest)
      VBX_free(hyp);
  }

  return(hyp);
}


void
HYP_destroy(HYP hyp)
/**************************************************************************
 *
 * Annihilate an array of hypotheses
 * 
 **************************************************************************/
{
  HYP  thisHyp;

  if (hyp) {

    /* Free the arrays of word hypotheses, if any */
    for (thisHyp = hyp; thisHyp->words; thisHyp++)
      VBX_free(thisHyp->words);

    free(hyp);
  }
}


HYP
HYP_clone(HYP hyp)
/**************************************************************************
 *
 * Create an array of hypotheses identical to the specified HYP.  THIS FCN
 * ALLOCATES A HYP, WHICH IT IS THE CALLER'S RESPONSIBILITY TO FREE.
 * 
 **************************************************************************/
{
  HYP     clone = NULL;
  HYP     alternate;
  UInt    nBest;

  if (hyp) {

     /* Determine the number of hypotheses in hyp, empty or not */
    for (alternate = hyp, nBest = 0; alternate->words; alternate++, nBest++);

     /* Create an array of the same number of hypotheses */
    if ((clone = HYP_create(nBest))) {
       /* Copy the hypotheses */
      alternate = clone;
      while (hyp->words) {
         /* Copy the hypothesis */
        alternate->totalScore = hyp->totalScore;
        alternate->grammarId = hyp->grammarId;
        alternate->startNode = hyp->startNode;
        alternate->endNode = hyp->endNode;
         /* Copy the arrays of word hypotheses */
        HYP_setWordCount(alternate, hyp->wordCount);
        if (hyp->wordCount) memcpy(alternate->words, hyp->words, hyp->wordCount * sizeof(wrdhyp_t));
        alternate++;
        hyp++;
      }    
    }
  }

  return(clone);
}


Bool
HYP_setWordCount(HYP hyp, UInt wordCount)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the word count for a hypothesis, re-allocating space for the WRDHYPs
 * if necessary
 *---------------------------------------------------------------------------*/
{
  WRDHYP  newWords;

  if (wordCount > hyp->maxWordCount) {
    newWords = (WRDHYP) VBX_recalloc(hyp->words, wordCount + HYP_WORDCOUNTEXCESS, sizeof(wrdhyp_t));
    if (newWords) {
      hyp->maxWordCount = wordCount + HYP_WORDCOUNTEXCESS;
      hyp->words = newWords;
    } else {
      return(FALSE);
    }
  }
  hyp->wordCount = wordCount;

  return(TRUE);
}


WRDHYP
HYP_words(HYP hyp, UInt * wordCountPtr)
/**************************************************************************
 *
 * Return the hypothesis' array of word hypotheses and report its size
 * 
 **************************************************************************/
{
  if (hyp) {
    if (wordCountPtr) *wordCountPtr = hyp->wordCount;
    return(hyp->words);
  } else {
    if (wordCountPtr) *wordCountPtr = 0;
    return(NULL);
  }
}


WRDHYP
HYP_findWord(HYP hyp, UInt wid)
/**************************************************************************
 *
 * Return the first WRDHYP in HYP that has the specified word ID
 * 
 **************************************************************************/
{
  WRDHYP  word;
  UInt    wordCount;

  if ((word = HYP_words(hyp, &wordCount)) && wordCount)
    while (wordCount)
      if (word->wid == wid) {
        break;
      } else {
        word++;
        wordCount--;
      }

  if (wordCount == 0)
    word = NULL;

  return(word);
}


UInt
HYP_numAlternatives(HYP hyp)
/**************************************************************************
 *
 * Return the number of alternative hypotheses in an array
 * 
 **************************************************************************/
{
  UInt  numHyp = 0;
  HYP   thisHyp;

  if (hyp)
    for (thisHyp = hyp; thisHyp->words != NULL && thisHyp->wordCount != 0; thisHyp++)
      numHyp++;

  return numHyp;
}


void
HYP_setSyntaxData(HYP hyp, UInt grammarId, UInt startNode, UInt endNode)
/**************************************************************************
 *
 * Set the syntactic data for a hypothesis
 * 
 **************************************************************************/
{
  if (hyp) {
    hyp->grammarId = grammarId;
    hyp->startNode = startNode;
    hyp->endNode = endNode;
  }
}


void
HYP_getSyntaxData(HYP hyp, UInt * grammarIdPtr, UInt * startNodePtr, UInt * endNodePtr)
/**************************************************************************
 *
 * Get the syntactic data for a hypothesis; pointers may be NULL if the
 * data they report is not needed.
 * 
 **************************************************************************/
{
  if (hyp) {
    if (grammarIdPtr) *grammarIdPtr = hyp->grammarId;
    if (startNodePtr) *startNodePtr = hyp->startNode;
    if (endNodePtr)   *endNodePtr   = hyp->endNode;
  }
}


void
HYP_boundaries(HYP hyp, SN * startSN, SN * endSN)
/**************************************************************************
 *
 * Find the starting and ending frame numbers of a hypothesis
 * 
 **************************************************************************/
{
  UInt    wordCount;
  WRDHYP  word;

  if (hyp && (word = HYP_words(hyp, &wordCount)) != NULL && wordCount) {
    *startSN = word[0].startSN;
    *endSN   = word[wordCount - 1].endSN;
  } else {
    *startSN = (SN) 0;
    *endSN   = (SN) 0;
  }
}


Char *
HYP_text(HYP hyp, VOCAB vocab)
/**************************************************************************
 *
 * Assemble and return the text string for the hypothesis.  THIS FUNCTION
 * ALLOCATES SPACE FOR THE STRING, WHICH THE CALLER MUST DEALLOCATE WHEN
 * THE STRING IS NO LONGER NEEDED.
 * 
 **************************************************************************/
{
  UInt    wordCount;
  WRDHYP  word;
  size_t  length, textAlloc = TEXT_GROWTH_QUANTUM;
  Char  * recText = VBX_calloc(TEXT_GROWTH_QUANTUM, sizeof(Char));
  Char  * wrdText;

  recText[0] = '\0';
  for (word = HYP_words(hyp, &wordCount); wordCount--; word++)
    /* If the word is in the vocabulary, append it to the text */
    if (wrdText = VOCAB_wordNameFromId(vocab, (UInt) word->wid)) {
      /* Expand the text allocation if necessary */
      if (strlen(recText) + strlen(wrdText) + 2 > textAlloc)
        recText = (Char *) VBX_recalloc(recText, (textAlloc += TEXT_GROWTH_QUANTUM), sizeof(Char));
	  strcat(recText, wrdText);
      strcat(recText, " ");
    }
  /* Strip the final blank off of the recText string */
  if (length = strlen(recText))
    recText[length - 1] = '\0';

  return(recText);    
}


void
HYP_print(HYP hyp, VOCAB vocab, VOCAB silences)
/**************************************************************************
 *
 * Print out the hypothesis
 * 
 **************************************************************************/
{
  UInt    wordCount;
  WRDHYP  word;
  Char  * wordName;

  VBX_print("   Word Name           Id   Size                Start                 End     Score    GMin\n");
  for (word = HYP_words(hyp, &wordCount); wordCount--; word++) {
    if (vocab && (wordName = VOCAB_wordNameFromId(vocab, word->wid)));
    else if (silences && (wordName = VOCAB_wordNameFromId(silences, word->wid)));
    else wordName = "";
    VBX_print("   %-16.16s %5d %6u  "QuadFmt(19)" "QuadFmt(19)"    %6d  %6d\n",
              wordName, word->wid, word->numFrames, word->startSN, word->endSN, word->score, word->globalMin);
  }
}
