#include "pthr.h"
#include <assert.h>
#include "types.h"
#include "vbx.h"

#include "parse.h"
#include "tree.h"
#include "buds.h"

#define BUDS_MAX_ELEMENTS   2730

/* Function Error Codes */
#define BUDS_INIT_FREE        1
#define BUDS_GET_BUD          2
#define BUDS_INSERT_BUD       3
#define BUDS_REMOVE_POINTER   4
#define BUDS_SET_BUD          5
#define BUDS_ADD_DAUGHTER     6
#define BUDS_CLOSE            7

typedef struct buds_s {
  BUDSEL elList;                        /* Free list of buds */
  BUDSEL nextEl;                        /* Next element available for use */
  UShort elListSize;                    /* Allocation size of element list */
  UShort usedEl;                        /* Number of elements in use */
  UShort maxUsedEl;                     /* High water mark for elements used */
} buds_t;
  
BUDS
BUDS_create()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a BUDS object containing an empty free list of bud elements
 * Return: Pointer to the new object
 *---------------------------------------------------------------------------*/
{
  BUDS buds = (BUDS) VBX_calloc(1, sizeof(buds_t));
  
  buds->elListSize = BUDS_MAX_ELEMENTS;
  buds->elList = (BUDSEL) VBX_calloc(buds->elListSize, sizeof(budsEl_t));

  return(buds);
}

void
BUDS_destroy(BUDS buds)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free the given BUDS object
 *---------------------------------------------------------------------------*/
{
  if (buds != NULL) {
    VBX_free(buds->elList);
    VBX_free(buds);
  }
}

Bool 
BUDS_initialize(BUDS buds)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize the given BUDS object by setting up the BUDSEL free list.  The
 * nextEl pointer is set to the last bud element in the free list
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  UShort  budIdx;
  BUDSEL  next;

  if (buds != NULL) {
    buds->usedEl = buds->maxUsedEl = 0;
    buds->elList[0].prev = (BUDSEL) NULL;

    for (budIdx = 1; budIdx < buds->elListSize; budIdx++)
      buds->elList[budIdx].prev = &buds->elList[budIdx - 1];
    next = &buds->elList[BUDS_MAX_ELEMENTS - 1];
    buds->nextEl = next;
  }

  return(TRUE);
}

BUDSEL 
BUDS_elAlloc(BUDS buds)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Allocat a BUDSEL from the BUDS free list of elements.  A NULL pointer is
 * returned if the free list is exhausted
 * Return: Pointer to BUDSEL, or NULL
 *---------------------------------------------------------------------------*/
{
  BUDSEL bud = (BUDSEL) NULL;

  if (buds != NULL) {
    if (buds->nextEl != NULL) {
      if (++buds->usedEl > buds->maxUsedEl)
        buds->maxUsedEl++;

      bud = buds->nextEl;
      buds->nextEl = buds->nextEl->prev;
    }
  }

  return(bud);
}

Bool 
BUDS_elInsert(BUDS buds, BUDSEL posBud, TREENODE node)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * If there is a free bud available, turn it into an active bud pointing
 * to the given TREENODE (nodePtr) and insert it into the bud list 
 * immediately following posBud.
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  BUDSEL oldNextBud;
  BUDSEL bud;

  if ((bud = BUDS_elAlloc(buds)) != NULL) {
    oldNextBud = posBud->next;
    oldNextBud->prev = bud;
    posBud->next = bud;
    BUDS_elSet(bud, posBud, oldNextBud, node);
    return(TRUE);
  } else
    return(FALSE);
}

void 
BUDS_elFree(BUDS buds, BUDSEL bud)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Remove the specified bud element from the list of active buds and put it
 * back on the free list
 *---------------------------------------------------------------------------*/
{
  if (buds != NULL) {
    buds->usedEl--;

   (bud->prev)->next = bud->next;
   (bud->next)->prev = bud->prev;
    bud->prev = buds->nextEl;
    buds->nextEl = bud;
  }
}

void 
BUDS_elSet(BUDSEL bud, BUDSEL prev, BUDSEL next, TREENODE node)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the fields of the given BUDSEL to the specified values
 *---------------------------------------------------------------------------*/
{
  if (bud != NULL) {
    bud->prev = prev;
    bud->next = next;
    bud->bud = node;
  }
}

Bool 
BUDS_elAddDaughter(BUDS buds, TREE tree, ParsePos_t pos, BUDSEL motherBud,
                   UShort stateId, UShort wordId)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * If both a tree node and bud element can be allocated, add the node to
 * the tree as a daughter to the node that motherBud points to.  A pointer
 * to the daughter is added immediately before or after motherBud in the
 * bud element list (according to the position specifier).
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  TREENODE motherNode;
  TREENODE daughterNode;
  BUDSEL   prevBud;

  assert(buds != NULL && tree != NULL);

  if ((daughterNode = TREE_nodeAlloc(tree)) != NULL) {
    if (pos == before)
      prevBud = motherBud->prev;
    else
      prevBud = motherBud;
    if (BUDS_elInsert(buds, prevBud, daughterNode)) {
      motherNode = motherBud->bud;
      TREE_nodeSet(daughterNode, stateId, wordId, motherNode, 0);
      motherNode->daughterCount++;
      return(TRUE);
    }
  }

  return(FALSE);
}

UShort 
BUDS_maxUsedEl(BUDS buds)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the high water mark for used buds elements
 *---------------------------------------------------------------------------*/
{
  UShort retval = 0;

  if (buds != NULL)
    retval = buds->maxUsedEl;

  return(retval);
}
