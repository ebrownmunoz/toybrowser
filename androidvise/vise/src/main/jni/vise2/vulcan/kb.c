/* kb.c - VISE Knowledge Base Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Methods for dealing with the application knowledge base for the VISE
 * recognizer.  Methods are provided for adding vocabularies, sets of models,
 * and grammars to the knowledge base, and for assigning models to their
 * corresponding word IDs ("reconciliation") and grammars to the vocabularies
 * they use.  The knowledge base contains all of the information about the
 * application, syntactic as well as lexical.
 *
 * HISTORY
 * 12-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* Generic inclusions */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define VBX_DEBUG(P) (P)
#else
#define VBX_DEBUG(P)
#endif


/* Generic Verbex inclusions */
#include "types.h"
#include "vbx.h"

/* Vulcan inclusions */
#include "vlbldefs.h"
#include "vfile.h"
#include "recfile.h"
#include "ekb.h"
#include "hash.h"
#include "kb.h"
#include "models.h"
#include "syntax.h"
#include "utils.h"
#include "vocab.h"
#include "template.h"
#include "wrdmap.h"
#include "parse.h"
#include "script.h"
#include "spwrds.h"

/* Entry in vocabulary hash table in knowledge base */
typedef struct kbent_s {
  VOCAB   vocabulary;      /* A vocabulary */
  MODELS  models;          /* Its models */
  WRDMAP  wordMap;         /* The model->vocabulary word name map */
  SCRIPT  script;          /* Training script */
} kbent_t, * KBENT;

/* Knowledge Base */
typedef struct kb_s {
  HASH    vocabHT;         /* Vocabulary hash table (hashed by vocab Id */
  HASH    vocabNameHT;     /* Vocabulary hash table (hashed by vocab name */
  HASH    grammarHT;       /* Grammar hash table (hashed by grammar Id */
  HASH    gramNameHT;      /* Grammar hash table (hashed by grammar name */
  EKB     ekb;             /* Engine KB */
  Char  * name;            /* The name of the application */
  MODSET  modset;          /* The set of sets of word models */
  Ptr     controlTtbl;     /* Control template table */
  Ptr     responseTtbl;    /* Response template table */
  Bool    responseData;    /* Has the response data been loaded? */
  UInt    firstGrammarId;  /* The first grammar to launch */
  PARSE   parse;           /* Parser object for response facililty */
  V_Err   status;
} kb_t;

/* Private prototypes */

static KBENT  KBENT_create(void);
static void   KBENT_destroy(KBENT kbent);

static Bool   KB_loadAppInfo(KB kb, VFILE recFile);
static Bool   KB_loadVocabularies(KB kb, VFILE recFile, Bool verbose);
static Bool   KB_addVocabulary(KB kb, VOCAB vocab, UInt vocabId);
static Bool   KB_loadGrammars(KB kb, VFILE recFile, Bool verbose);
static Bool   KB_addGrammar(KB kb, SYNTAX grammar, UInt grammarId);
static Bool   KB_assignGrammars(KB kb);
static Bool   KB_addModels(KB kb, MODSET models);
static Bool   KB_assignModels(KB kb);
static void   KB_removeModels(KB kb);

static Bool   KB_hashGrammarByName(KB kb, SYNTAX grammar);

static KBENT
KBENT_create(void)
/**************************************************************************
 *
 * Create the knowledge base entry for a vocabulary
 *
 **************************************************************************/
{
  return((KBENT) VBX_calloc(1, sizeof(kbent_t)));
}


static void
KBENT_destroy(KBENT kbent)
/**************************************************************************
 *
 * Annihilate a knowledge base entry
 *
 **************************************************************************/
{
  if (kbent) {
    VOCAB_annihilate(kbent->vocabulary);
    WRDMAP_annihilate(kbent->wordMap);
    SCRIPT_annihilate(kbent->script);
    free(kbent);
  }
}


KB
KB_create(EKB ekb)
/**************************************************************************
 *
 * Create an application knowledge base
 *
 **************************************************************************/
{
  KB  kb = (KB) calloc(1, sizeof(kb_t));

  if (kb &&
      (kb->vocabHT = HASH_create(1, HASH_integer, HASH_integerEquals, (void (*)(Ptr)) KBENT_destroy)) &&
      (kb->vocabNameHT = HASH_create(1, HASH_string, HASH_stringEquals, NULL)) &&
      (kb->grammarHT = HASH_create(1, HASH_integer, HASH_integerEquals, (void (*)(Ptr)) SYNTAX_annihilate)) &&
      (kb->gramNameHT = HASH_create(1, HASH_string, HASH_stringEquals, NULL))) {
    kb->ekb = ekb;
    kb->status = VISESUCCESS;
  } else {
    KB_destroy(kb);
    kb->status = OUTOFMEMORY;
  }

  return(kb);
}


void
KB_destroy(KB kb)
/**************************************************************************
 *
 * Annihilate an application knowledge base
 *
 **************************************************************************/
{
  if (kb) {
    HASH_annihilate(kb->vocabHT);
    HASH_annihilate(kb->vocabNameHT);
    HASH_annihilate(kb->grammarHT);
    HASH_annihilate(kb->gramNameHT);
    VBX_free(kb->name);
    if (kb->responseData) {
      TEMPLATE_annihilate(kb->responseTtbl);
      TEMPLATE_annihilate(kb->controlTtbl);
      PARSE_destroy(kb->parse);
    }
    free(kb);
  }
}


V_Err
KB_status(KB kb)
/**************************************************************************
 *
 * Return the status of the kb object
 *
 **************************************************************************/
{
  return(kb ? kb->status : NOSUCHOBJECT);
}


Bool
KB_load(KB kb, VFILE recFile, Bool verbose)
/**************************************************************************
 *
 * Load the SI/AD (Speaker-Independent/Application-Dependent) data from
 * the recFile into the KB.  If called with recFile NULL, this function
 * just constructs the silence vocabulary
 *
 **************************************************************************/
{
  VOCAB silences;

  if (recFile) {

     /* Load the speaker-independent data from the Recog Stream */
    VBX_DEBUG(VBX_print("  VISERecStreamObj: loading application from rec stream @%p\n", recFile));

     /* Load the application information */
    if (!KB_loadAppInfo(kb, recFile))
      FATAL_BRA_ "Can't load the application info from the rec stream" _KET;
    VBX_DEBUG(VBX_print("  VISERecStreamObj::Init: The application's name is \"%s\"\n", KB_name(kb)));
    VBX_DEBUG(VBX_print("  VISERecStreamObj::Init: The first grammar is #%u\n", KB_firstGrammarId(kb)));

     /* Read in the vocabularies */
    if (!KB_loadVocabularies(kb, recFile, verbose))
      FATAL_BRA_ "Can't load all the vocabularies from the rec stream" _KET;
  }

   /* If there is no silence vocabulary in the rec stream, construct one */
  if (!(silences = KB_vocabulary(kb, (UInt) UPHEADSILCLASSID))) {
    if (!(silences = VOCAB_create((UInt) UPHEADSILCLASSID)) || !VOCAB_loadFromMap(silences, silenceMap))
      FATAL_BRA_ "Can't make silence vocabulary from vocabulary map" _KET;
    if (!KB_addVocabulary(kb, silences, (UInt) UPHEADSILCLASSID))
      FATAL_BRA_ "Can't add silence vocabulary to knowledge base" _KET;
  }

  if (recFile) {

     /* Read in the grammars */
    if (!KB_loadGrammars(kb, recFile, verbose))
      FATAL_BRA_ "Can't load all the grammars from the rec stream" _KET;

     /* Associate each grammar with its vocabulary */
    if (!KB_assignGrammars(kb))
      FATAL_BRA_ "Cannot reconcile grammars and vocabularies" _KET;
  }

  return TRUE;
}


Bool
KB_reconcile(KB kb, MODSET models)
/**************************************************************************
 *
 * Meld the Speaker-Dependent (MODSET) data into the KB
 *
 **************************************************************************/
{
   /* Remove the models (if any) from the KB */
  KB_removeModels(kb);

   /* Load the models into the the KB */
  KB_addModels(kb, models);

   /* Associate a model with each word Id */
  if (!KB_assignModels(kb)) {
    VBX_DEBUG(VBX_print("  KB_reconcile cannot assign models to all word Ids\n"));
  }

  return TRUE;
}


static Bool
KB_loadAppInfo(KB kb, VFILE recFile)
/**************************************************************************
 *
 * Read the application identification information from the rec file
 * put it into the knowledge base
 *
 **************************************************************************/
{
	UShort blockId, discard;
	ULong size;
	FIDENT info;
	FLOFF name;
	Bool success;

	if (!(success = kb != NULL && recFile != NULL)) {
		VBX_print("KB_loadAppInfo: ERROR, kb = %p, recFile = %p\n", kb, recFile);
		kb->status = INVALIDARGUMENT;

	} else if (!(success = VFILE_SUCCESS == VFILE_rewind(recFile))) {
		VBX_print("KB_loadAppInfo: Can't rewind the rec file\n");
		kb->status = CANTREWINDRECFILE;

	} else {
		kb->status = VISESUCCESS;
		while ((success = success && VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &discard, (V_ULong *) &size))) {
			if (blockId == TR_IDENT) {
				if (!(success = VFILE_SUCCESS == VFILE_readStructure(recFile, &info, 1, RS_IDENT))) {
					VBX_print("KB_loadAppInfo: Can't load application ident block from the rec file\n");
					kb->status = CANTREADVFILESTRUCT;
				} else {
					/* Get the Id of the first grammar */
					kb->firstGrammarId = (UInt) info.wFirstGrammar;
					/* Get the name of the application */
					name = info.bloApplicationName;
					if (!(success = (kb->name = (Char *) malloc((size_t) (name.bLength + 1))) != NULL)) {
						VBX_print("KB_loadAppInfo: Failed to malloc %d bytes\n", name.bLength + 1);
						kb->status = OUTOFMEMORY;
                      break;
					}
					if (VFILE_SUCCESS != VFILE_getText(recFile, &name, (UChar *) kb->name)) {
						VBX_print("KB_loadAppInfo: Can't find application name in rec file\n");
					}
					break;
				}
			}
		}
	}

	return (success);
}


UInt
KB_firstGrammarId(KB kb)
/**************************************************************************
 *
 * Return the Id of the first grammar to launch
 *
 **************************************************************************/
{
  assert(kb);

  return(kb->firstGrammarId);
}


Char *
KB_name(KB kb)
/**************************************************************************
 *
 * Return the name of the application
 *
 **************************************************************************/
{
  assert(kb);

  return(kb->name);
}


static Bool
KB_loadVocabularies(KB kb, VFILE recFile, Bool verbose)
/**************************************************************************
 *
 * Read all the vocabularies from the given rec file and load them
 * into the knowledge base.
 *
 **************************************************************************/
{
  UShort  blockId, vocabId;
  ULong   size;
  VOCAB   vocabulary;
  Bool    success;

  if (!(success = kb != NULL && recFile != NULL)) {
    kb->status = INVALIDARGUMENT;

  } else if (!(success = VFILE_SUCCESS == VFILE_rewind(recFile))) {
    VBX_print("KB_loadVocabularies: Can't rewind the rec file\n");
    kb->status = CANTREWINDRECFILE;

  } else {
    kb->status = VISESUCCESS;
    while ((success && VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &vocabId, (V_ULong *)&size)))
      if (blockId == TR_VOCABULARY_TRANS && !KB_vocabulary(kb, (UInt) vocabId)) {
        if (!(success = (vocabulary = VOCAB_create(vocabId)) != NULL)) {
          kb->status = OUTOFMEMORY;
        } else if (!(success = VOCAB_load(vocabulary, recFile))) {
          VBX_print("KB_loadVocabularies: Can't load vocabulary block %u from the rec file\n", vocabId);
          kb->status = VOCAB_status(vocabulary);
        } else if (verbose) {
          VBX_print("  KB_loadVocabularies: Vocabulary block %u loaded from the rec file\n", vocabId);
        }
        if (!KB_addVocabulary(kb, vocabulary, (UInt) vocabId)) {
          if (VOCABEXISTS == kb->status) {
            VBX_print("KB_loadVocabularies: Skipping superfluous vocabulary block %u in the rec file\n", vocabId);
            kb->status = VISESUCCESS;
          } else {
            VBX_print("KB_loadVocabularies: Cannot add vocabulary block %u to the knowledge base\n", vocabId);
            success = FALSE;
          }
        } else if (verbose) {
          VBX_print("  KB_loadVocabularies: Vocabulary block %u added to the knowledge base\n", vocabId);
        }
      }
  }

  return(success);
}


static Bool
KB_addVocabulary(KB kb, VOCAB vocab, UInt vocabId)
/**************************************************************************
 *
 * Try to add a vocabulary to the knowledge base, using the given vocabId.
 * Return TRUE if successful, FALSE if there is already a vocabulary with
 * this vocabId in the table.
 *
 **************************************************************************/
{
  Ptr     hashValue;
  KBENT   entry;

  assert(kb);

  /* If there is no entry for vocabId, make one */
  if (!HASH_lookup(kb->vocabHT, (Ptr) vocabId, &hashValue)) {
    entry = KBENT_create();
    if (!HASH_add(kb->vocabHT, (Ptr) vocabId, (Ptr) entry)) {
      VBX_print("KB_addVocabulary: can't add entry not in vocabHT\n");
      kb->status = FATALINCONSISTENCY;
      return(FALSE);
    }
    VBX_DEBUG(VBX_print("  KB_addVocabulary: adding vocab %d to hash table\n", vocabId));

  /* Otherwise, use the one that is in the table */
  } else {
    entry = (KBENT) hashValue;
  }

  if (VOCAB_name(vocab))
    HASH_add(kb->vocabNameHT, (Ptr) VOCAB_name(vocab), (Ptr) entry);

  if (entry->vocabulary) {
    kb->status = VOCABEXISTS;
    return(FALSE);
  } else {
    kb->status = VISESUCCESS;
    entry->vocabulary = vocab;
    return(TRUE);
  }
}


VOCAB
KB_vocabulary(KB kb, UInt vocabId)
/**************************************************************************
 *
 * Return the vocabulary with the given vocabId from the knowledge base,
 * or NULL if no such vocabulary is present.
 *
 **************************************************************************/
{
  Ptr     hashValue;

  assert(kb);

  VBX_DEBUG(VBX_print("KB_vocabulary: looking up vocab %d in ht %x\n",
            vocabId, kb->vocabHT));
  if (HASH_lookup(kb->vocabHT, (Ptr) vocabId, &hashValue)) {
    kb->status = VISESUCCESS;
    return(((KBENT) hashValue)->vocabulary);
  } else {
    kb->status = VOCABDOESNOTEXIST;
    return((VOCAB) NULL);
  }
}


VOCAB
KB_vocabularyByName(KB kb, Char * name)
/**************************************************************************
 *
 * Return the vocabulary with the given name from the knowledge base,
 * or NULL if no such vocabulary is present.
 *
 **************************************************************************/
{
  Ptr     hashValue;

  assert(kb);

  if (name != NULL && HASH_lookup(kb->vocabNameHT, (Ptr) name, &hashValue)) {
    kb->status = VISESUCCESS;
    return(((KBENT) hashValue)->vocabulary);
  } else {
    kb->status = VOCABDOESNOTEXIST;
    return((VOCAB) NULL);
  }
}


VOCAB
KB_firstVocab(KB kb, UInt * vocabIdPtr)
/**************************************************************************
 *
 * Return the first (physically, not chronologically) vocabulary in the kb,
 * or NULL if no vocabulary is present.
 *
 **************************************************************************/
{
  Ptr    symbol;
  VOCAB  vocab;

  assert(kb);

  vocab = (VOCAB) HASH_firstValue(kb->vocabHT, &symbol);
  *vocabIdPtr = (UInt) symbol;

  kb->status = vocab ? VISESUCCESS : VOCABDOESNOTEXIST;

  return(vocab);
}


VOCAB
KB_nextVocab(KB kb, UInt * vocabIdPtr)
/**************************************************************************
 *
 * Return the next (physically, not chronologically) vocabulary in the kb,
 * or NULL if no more vocabularies are present.
 *
 **************************************************************************/
{
  Ptr    symbol;
  VOCAB  vocab;

  assert(kb);

  vocab = (VOCAB) HASH_nextValue(kb->vocabHT, &symbol);
  *vocabIdPtr = (UInt) symbol;

  kb->status = vocab ? VISESUCCESS : VOCABDOESNOTEXIST;

  return(vocab);
}


static Bool
KB_addModels(KB kb, MODSET modset)
/**************************************************************************
 *
 * Add models for all vocabularies to the knowledge base.
 * Return TRUE if successful, FALSE otherwise.
 *
 **************************************************************************/
{
  KBENT   vocab;
  Ptr     vocabId;
  Bool    success;

  if ((success = kb != NULL)) {
    kb->status = VISESUCCESS;
    for (vocab = (KBENT) HASH_firstValue(kb->vocabHT, &vocabId); vocab; vocab = (KBENT) HASH_nextValue(kb->vocabHT, &vocabId))
      if (!(vocab->models = MODSET_models(modset, (UInt) vocabId))) {
        VBX_print("KB_addModels: found no set of models for vocab %u\n", (UInt) vocabId);
        kb->status = MODSET_status(modset);
        success = FALSE;
      }
    kb->modset = modset;
  }

  return(success);
}


static Bool
KB_loadGrammars(KB kb, VFILE recFile, Bool verbose)
/**************************************************************************
 *
 * Read all the grammars from the given rec file and load them into the
 * knowledge base.
 *
 **************************************************************************/
{
  UShort  blockId, grammarId;
  ULong   size;
  SYNTAX  grammar;
  Bool    success;

  if (!(success = kb != NULL && recFile != NULL)) {
    kb->status = INVALIDARGUMENT;

  } else if (!(success = VFILE_SUCCESS == VFILE_rewind(recFile))) {
    VBX_print("KB_loadGrammars: Can't rewind the rec file\n");
    kb->status = CANTREWINDRECFILE;

  } else {
    kb->status = VISESUCCESS;
    while ((success && VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &grammarId, (V_ULong *)&size)))
      if (blockId == TR_GRAMMAR || blockId == TR_GRAPH) {
        /* If there is no grammar with this Id, try to add one */
        if (!(success = (grammar = KB_grammar(kb, (UInt) grammarId)) != NULL || KB_addGrammar(kb, grammar = SYNTAX_create(grammarId), (UInt) grammarId))) {
          VBX_print("KB_loadGrammars: Can't add grammar %u, though none exists\n", (UInt) grammarId);
          kb->status = CANTADDGRAMMAR;
        /* Load the grammar header or graph from the rec file */
        } else if (blockId == TR_GRAMMAR) {
          if (!SYNTAX_name(grammar)) {
            if (!(success = SYNTAX_loadHeader(grammar, recFile))) {
              VBX_print("KB_loadGrammars: Can't load the header for grammar %u\n", (UInt) grammarId);
              kb->status = SYNTAX_status(grammar);
            } else if (!(success = KB_hashGrammarByName(kb, grammar))) {
              VBX_print("KB_loadGrammars: Grammar %u already exists\n", (UInt) grammarId);
              kb->status = GRAMMAREXISTS;
            } else if (verbose) {
              VBX_print("  KB_loadGrammars: Loaded header for grammar %u\n", (UInt) grammarId);
            }
          }
        } else /* blockId == TR_GRAPH */ {
          if (!SYNTAX_graph(grammar)) {
            if (!(success = SYNTAX_loadGraph(grammar, recFile))) {
              VBX_print("KB_loadGrammars: Can't read the graph for grammar %u\n", (UInt) grammarId);
              kb->status = SYNTAX_status(grammar);
            } else if (verbose) {
              VBX_print("  KB_loadGrammars: Loaded graph for grammar %u\n", (UInt) grammarId);
            }
          }
        }
      }
  }

  return(success);
}


static Bool
KB_addGrammar(KB kb, SYNTAX grammar, UInt grammarId)
/**************************************************************************
 *
 * Try to add the grammar to the knowledge base, using the specified
 * grammar Id.  Return TRUE if successful, FALSE if there is already
 * a grammar with this Id in the table.
 *
 **************************************************************************/
{
  assert(kb);

  return(HASH_add(kb->grammarHT, (Ptr) grammarId, (Ptr) grammar));
}


static Bool
KB_hashGrammarByName(KB kb, SYNTAX grammar)
/**************************************************************************
 *
 * Try to hash the grammar by name.  Return TRUE if successful, FALSE if
 * there is already a grammar with this name in the table.  NOTE THAT THE
 * NAME IS NOT AVAILABLE UNTIL THE GRAMMAR HEADER HAS BEEN LOADED.
 *
 **************************************************************************/
{
  assert(kb);

  return(HASH_add(kb->gramNameHT, (Ptr) SYNTAX_name(grammar), (Ptr) grammar));
}


SYNTAX
KB_grammar(KB kb, UInt grammarId)
/**************************************************************************
 *
 * Return the grammar with the given grammarId from the knowledge base,
 * or NULL if no such grammar is present.
 *
 **************************************************************************/
{
  Ptr     hashValue;

  assert(kb);

  if (HASH_lookup(kb->grammarHT, (Ptr) grammarId, &hashValue)) {
    kb->status = GRAMMAREXISTS;
    return((SYNTAX) hashValue);
  } else {
    kb->status = GRAMMARDOESNOTEXIST;
    return((SYNTAX) NULL);
  }
}


SYNTAX
KB_grammarByName(KB kb, Char * name)
/**************************************************************************
 *
 * Return the grammar with the given name from the knowledge base,
 * or NULL if no such grammar is present.
 *
 **************************************************************************/
{
  Ptr     hashValue;

  VBX_DEBUG(VBX_print("  KB: %p\n", kb));
  assert(kb);

  if (HASH_lookup(kb->gramNameHT, (Ptr) name, &hashValue)) {
    kb->status = GRAMMAREXISTS;
    return((SYNTAX) hashValue);
  } else {
    kb->status = GRAMMARDOESNOTEXIST;
    return((SYNTAX) NULL);
  }
}


static Bool
KB_assignModels(KB kb)
/**************************************************************************
 *
 * Assigns each available model to its wid in each vocabulary in the kb.
 * Even when no models are available, this function creates and initializes
 * the MODENT array to support lookup by wid, though this normally has
 * already been done by KB_loadVocabularies().
 * Returns TRUE if models are successfully assigned to all wids,
 * FALSE otherwise.
 *
 **************************************************************************/
{
  KBENT   vocab;
  Bool    success = TRUE;

  if (kb) {
    kb->status = VISESUCCESS;
    for (vocab = (KBENT) HASH_firstValue(kb->vocabHT, NULL); vocab; vocab = (KBENT) HASH_nextValue(kb->vocabHT, NULL)) {
      if (vocab->vocabulary) {
        if (vocab->models) {
          /* Assign the word models to the vocabulary */
          if (!(VOCAB_assignModels(vocab->vocabulary, vocab->models, vocab->wordMap))) {
            kb->status = VOCAB_status(vocab->vocabulary);
            success = FALSE;
          }
        } else {
          kb->status = NOMODELSFORVOCAB;
          success = FALSE;
        }
        /* Fill in for models not present */
        if (!VOCAB_cloneModels(vocab->vocabulary)) {
          kb->status = VOCAB_status(vocab->vocabulary);
          success = FALSE;
        }
      }
    }
  }

  return(success);
}


static void
KB_removeModels(KB kb)
/**************************************************************************
 *
 * Removes the models from all vocabularies
 *
 **************************************************************************/
{
  KBENT   vocab;

  if (kb) {
    for (vocab = (KBENT) HASH_firstValue(kb->vocabHT, NULL); vocab; vocab = (KBENT) HASH_nextValue(kb->vocabHT, NULL)) {
      if (vocab->vocabulary) {
        if (vocab->models)
          vocab->models = NULL;
        VOCAB_removeModels(vocab->vocabulary);
      }
    }
    kb->status = VISESUCCESS;
  }
}


static Bool
KB_assignGrammars(KB kb)
/**************************************************************************
 *
 * Assign each grammar to the vocabulary which it uses, returning TRUE
 * if successful, FALSE otherwise.
 *
 **************************************************************************/
{
  SYNTAX  grammar;
  Bool    success = TRUE;
  Ptr     value;

  if (kb) {
    kb->status = VISESUCCESS;
    for (grammar = (SYNTAX) HASH_firstValue(kb->grammarHT, NULL); grammar; grammar = (SYNTAX) HASH_nextValue(kb->grammarHT, NULL)) {
      /* If the vocabulary exists, add the grammar to it */
      if (!(HASH_lookup(kb->vocabHT, (Ptr) SYNTAX_vocabularyId(grammar), &value) && ((KBENT) value)->vocabulary)) {
        VBX_print("There is no vocabulary for grammar \"%s\"\n", SYNTAX_name(grammar));
        kb->status = NOVOCABFORGRAMMAR;
        success = FALSE;
      } else if (!VOCAB_addGrammar(((KBENT) value)->vocabulary, grammar)) {
        VBX_print("Can't add grammar \"%s\" to vocabulary %u\n", SYNTAX_name(grammar), SYNTAX_vocabularyId(grammar));
        kb->status = VOCAB_status(((KBENT) value)->vocabulary);
        success = FALSE;
      }
    }
  }

  return(success);
}


UInt
KB_numVocabularies(KB kb)
/**************************************************************************
 *
 * Return the number of vocabularies (KBENTs) in the knowledge base
 *
 **************************************************************************/
{
  assert(kb);

  return(HASH_used(kb->vocabHT));
}


UInt
KB_numGrammars(KB kb)
/**************************************************************************
 *
 * Return the number of grammars in the knowledge base
 *
 **************************************************************************/
{
  assert(kb);

  return(HASH_used(kb->grammarHT));
}


UInt
KB_unusedGrammarId(KB kb)
/**************************************************************************
 *
 * Return the smallest positive integer not in use as a grammar Id
 *
 **************************************************************************/
{
  UInt   result;
  Ptr    value;

  assert(kb);

  for (result = 1; HASH_lookup(kb->grammarHT, (Ptr) result, &value); result++);

  return(result);
}

Bool
KB_loadResponseData(KB kb, VFILE recFile, Bool verbose)
/**************************************************************************
 *
 * Load the translations, templates, and parses for each of the vocabularies
 * and grammars in the knowledge base.
 *
 **************************************************************************/
{
  KBENT   vocab;
  SYNTAX  grammar;
  Bool    success = TRUE;
  UShort  blockId, classId;
  ULong   size;
  Ptr     ptr;

  if (kb->responseData) {
    VBX_DEBUG(VBX_print("  KB_loadResponseData: Response data already loaded\n"));
    kb->status = VISESUCCESS;
    return(TRUE);
  }

  if (!(success = kb != NULL && recFile != NULL)) {
    kb->status = INVALIDARGUMENT;

  } else if (!(success = VFILE_SUCCESS == VFILE_rewind(recFile))) {
    VBX_print("KB_loadResponseData: Can't rewind the rec file\n");
    kb->status = CANTREWINDRECFILE;

  } else {
    kb->status = VISESUCCESS;
     /* Peruse all recfile blocks for vocab response data having our classId */
    while (VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &classId, (V_ULong *)&size)) {
      VBX_DEBUG(VBX_print("  KB_loadResponseData: BLOCKID = %d  CLASSID = %d\n", blockId, classId));
      if (classId == 0) {
        switch (blockId) {
        case TR_CONTROL_TEMPLATE:
          VBX_DEBUG(VBX_print("  KB_loadResponseData: Loading CONTROL template\n"));
          kb->controlTtbl = TEMPLATE_load(recFile);
          VBX_DEBUG(VBX_print("  KB_loadResponseData: Loaded CONTROL template at %p\n", kb->controlTtbl));
          break;
        case TR_RESPONSE_TEMPLATE:
          VBX_DEBUG(VBX_print("  KB_loadResponseData: Loading RESPONSE template\n"));
          kb->responseTtbl = TEMPLATE_load(recFile);
           /* The Intermec 700C fails unless the following printout occurs, exactly as though an unobserved responseTtbl were NULL. */
          VBX_DEBUG(VBX_print("  KB_loadResponseData: Loaded RESPONSE template at %p\n", kb->responseTtbl));
        default:
          break;
        }
      }
    }

    /* Find and load response data for all vocabularies */
    if (verbose)
      VBX_print("  KB_loadResponseData: Loading response data for %d vocabularies\n", HASH_used(kb->vocabHT));
    for (vocab = (KBENT) HASH_firstValue(kb->vocabHT, &ptr); vocab; vocab = (KBENT) HASH_nextValue(kb->vocabHT, &ptr)) {
      if (vocab->vocabulary)
        VOCAB_loadTrans(vocab->vocabulary, recFile);
    }

    /* Find and load response data for all grammars */
    if (verbose)
      VBX_print("  KB_loadResponseData: Loading response data for %d grammars\n", HASH_used(kb->grammarHT));
    for (grammar = (SYNTAX) HASH_firstValue(kb->grammarHT, &ptr); grammar; grammar = (SYNTAX) HASH_nextValue(kb->grammarHT, &ptr)) {
      SYNTAX_loadTrans(grammar, recFile);
    }
  }

  if (success) {
    kb->responseData = TRUE;
    if (kb->parse == NULL)
      kb->parse = PARSE_create();
  }

  return(success);
}

Bool
KB_response(KB kb, UInt type, HYP hyp, VFILE recFile, UInt vocabId, UInt grammarId, Char *buf, UInt bufLen, UInt *sizeNeeded)
/**************************************************************************
 *
 * Assemble the response of type "type" given a HYPothesis, vocabId,
 * grammarId, a recFile (for text lookup) and a place to put the result
 *
 **************************************************************************/
{
  TemplateEl *tPtr = NULL;
  PARSETABLE  parseTable = NULL;
  Ptr         parseResult = NULL;
  SYNTAX      syntax = NULL;
  VOCAB       vocab = NULL;
  TRANS       trans = NULL;
  UShort      tmplIdx;
  Bool        rval;

  buf[0] = '\0';

  /* Bail immediately if the hypothesis or the output buffer is NULL */
  if (hyp == NULL || buf == NULL)
    return(FALSE);

  /* If the response data has not been loaded, load it now */
  if (!KB_loadResponseData(kb, recFile, FALSE))
    FATAL_BRA_ "KB_hostResponse: can't load response data" _KET;

  /* Try to find a grammar-level parse table */
  syntax = KB_grammar(kb, grammarId);

  if (syntax == NULL) {
    WARN_BRA_ "KB_hostResponse: Grammar ID %d is not valid", grammarId _KET;
    return(FALSE);
  }

  /* Set the default template index */
  switch (type) {
  case KB_HOST_RESPONSE:
    tmplIdx = SYNTAX_dfltHRTmplIdx(syntax);
    break;
  case KB_VOICE_RESPONSE:
    tmplIdx = SYNTAX_dfltVRTmplIdx(syntax);
    break;
  case KB_DISPLAY_RESPONSE:
    tmplIdx = SYNTAX_dfltHRTmplIdx(syntax);
    break;
  SEVERE_BRA_ "KB_hostResponse: unsupported response type %d", type _KET;
    return(FALSE);
  }

  if ((trans = SYNTAX_trans(syntax)) != NULL) {
    switch (type) {
    case KB_HOST_RESPONSE:
      parseTable = (PARSETABLE) trans->hostParseTbl;
      break;
    case KB_VOICE_RESPONSE:
      parseTable = (PARSETABLE) trans->voiceParseTbl;
      break;
    case KB_DISPLAY_RESPONSE:
      parseTable = (PARSETABLE) trans->displayParseTbl;
      break;
    default:
      SEVERE_BRA_ "KB_hostResponse: unsupported response type %d", type _KET;
      return(FALSE);
    }
  }

  /* If a parse does not exist, try vocab level */
  if (parseTable == NULL) {
    vocab = KB_vocabulary(kb, vocabId);

    if (vocab != NULL && ((trans = VOCAB_trans(vocab)) != NULL)) {
      switch (type) {
      case KB_HOST_RESPONSE:
        parseTable = (PARSETABLE) trans->hostParseTbl;
        break;
      case KB_VOICE_RESPONSE:
        parseTable = (PARSETABLE) trans->voiceParseTbl;
        break;
      case KB_DISPLAY_RESPONSE:
        parseTable = (PARSETABLE) trans->displayParseTbl;
        break;
      default:
        SEVERE_BRA_ "KB_hostResponse: unsupported response type %d", type _KET;
        return(FALSE);
      }
    }
  }

  if (parseTable == NULL)
    VBX_DEBUG(VBX_print("  KB_response: parse table not found for %d %d\n", vocabId, grammarId));

  /* Do the parse now if we found a parse table */
  if (parseTable != NULL) {
    if (!(rval = PARSE_parse(kb->parse, hyp, VOCAB_lastWordId(KB_vocabulary(kb, vocabId)), parseTable, &parseResult, &tmplIdx))) {
      /* WARN_BRA_ "KB_response: PARSE_parse fails for %d %d" vocabId, grammarId _KET; */
      return(FALSE);
    }
  } else {
    VBX_DEBUG(VBX_print("  KB_response: parse table is NULL\n"));
  }

  VBX_DEBUG(VBX_print("  KB_response: looking up template %d in response table at %p\n", tmplIdx, kb->responseTtbl));
  tPtr = TEMPLATE_lookup(kb->responseTtbl, tmplIdx);

  /* If there is no template or parse, default to translating the wid seq */
  if (tPtr == NULL || parseTable == NULL || !rval || parseResult == NULL) {
    rval = TEMPLATE_makeResponseHyp(kb, type, recFile, hyp, vocabId, grammarId, buf, bufLen, sizeNeeded);
  } else {
    rval = TEMPLATE_makeResponse(kb, type, tPtr, recFile, (TREENODE) parseResult, vocabId, grammarId, buf, bufLen, sizeNeeded);
  }
  VBX_DEBUG(VBX_print("  KB_response: TEMPLATE_makeResponse returns %d\n", rval));
  return(rval);
}

FLOFF *
KB_translateWordId(KB kb, UInt type, UInt wordId, UInt vocabId, UInt grammarId)
/**************************************************************************
 *
 * Obtain the FLOFF (length/offset descriptor a la VISE) for the given
 * wordId, returning NULL if one cannot be found.
 *
 **************************************************************************/
{
  VOCAB    vocab = KB_vocabulary(kb, vocabId);
  SYNTAX   grammar = NULL;
  TRANS    trans = NULL;
  TRANSENT entry = NULL;

  /* Return NULL if the word is non-lexical */
  if (vocab == NULL || wordId > VOCAB_lastWordId(vocab))
    return(NULL);

  /* First try to find a grammar-level translation */
  if ((grammar = KB_grammar(kb, grammarId)) != NULL) {
    if ((trans = SYNTAX_trans(grammar)) != NULL) {
      if (trans->widHT != NULL && HASH_lookup(trans->widHT, (Ptr) wordId, (Ptr *) &entry)) {
        assert(entry != NULL);
      } else {
        entry = NULL;
      }
    }
  }

  /* Back off to a vocab-level translation */
  if (entry == NULL) {
    if ((trans = VOCAB_trans(vocab)) != NULL) {
      if (trans->widHT != NULL && HASH_lookup(trans->widHT, (Ptr) wordId, (Ptr *) &entry)) {
        assert(entry != NULL);
      } else {
        entry = NULL;
      }
    }
  }

  if (entry != NULL) {
    switch (type) {
    case KB_HOST_RESPONSE:
      return(&entry->hostTrans);
    case KB_VOICE_RESPONSE:
      return(&entry->voiceTrans);
    case KB_DISPLAY_RESPONSE:
      return(&entry->displayTrans);
    default:
      SEVERE_BRA_ "KB_translateWordId: unsupported response type %d", type _KET;
 }
  }

  /* No translation found: complain */
  WARN_BRA_ "KB_translateWordId: no translation found for wordId %d vocab %d grammar %d", wordId, vocabId, grammarId _KET;
  return(NULL);
}

DELIN
KB_getDelin(KB kb, UInt type, UInt vocabId, UInt grammarId)
/**************************************************************************
 *
 * Obtain the DELINeators for the specified vocab/grammar.
 * Return NULL if one cannot be found
 *
 **************************************************************************/
{
  DELIN    delin = NULL;
  TRANS    trans = NULL;
  SYNTAX   grammar = NULL;
  VOCAB    vocab = NULL;
  TRANSENT entry = NULL;

  /* First try to find grammar-level delineators */
  if ((grammar = KB_grammar(kb, grammarId)) != NULL) {
    if ((trans = SYNTAX_trans(grammar)) != NULL) {
      switch (type) {
      case KB_HOST_RESPONSE:
        delin = trans->HRDelin;
        break;
      case KB_VOICE_RESPONSE:
        delin = trans->VRDelin;
        break;
      case KB_DISPLAY_RESPONSE:
        delin = trans->DRDelin;
        break;
      default:
        SEVERE_BRA_ "KB_getDelin: unsupported response type %d", type _KET;
        return(NULL);
      }
      if (delin != NULL) {
        return(delin);
      }
    }
  }

  /* Back off to vocab-level delineators */
  if ((vocab = KB_vocabulary(kb, vocabId)) != NULL) {
    if ((trans = VOCAB_trans(vocab)) != NULL) {
      switch (type) {
      case KB_HOST_RESPONSE:
        delin = trans->HRDelin;
        break;
      case KB_VOICE_RESPONSE:
        delin = trans->VRDelin;
        break;
      case KB_DISPLAY_RESPONSE:
        delin = trans->DRDelin;
        break;
      default:
        SEVERE_BRA_ "KB_getDelin: unsupported response type %d", type _KET;
        return(NULL);
      }
      if (delin != NULL) {
        return(delin);
      }
    }
  }

  /* No delineators found: complain */
  WARN_BRA_ "KB_getDelin: no delineators found for vocab %d grammar %d", vocabId, grammarId _KET;
  return(NULL);
}
