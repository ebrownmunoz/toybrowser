static char     AtFSid[] = "$__Header$";

#include "pthr.h"
#include <assert.h>
#include "types.h"
#include "vbx.h"

#include "vfile.h"
#include "vlbldefs.h"
#include "script.h"

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#endif

/* My internal function prototypes */
static Bool  SCRIPT_readEntry(VFILE recFile, HYP entryPtr, Bool loadWords);

typedef struct script_s {
  UShort      entryCnt;
  UShort      discMinCnt;
  UShort      contMinCnt;
  VFILE_MARK *entryMarks;
} script_t;

SCRIPT
SCRIPT_load(VFILE recFile, UInt vocabId)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Load a training script from the specified recognition file
 * Return: SCRIPT object pointer, or NULL if block does not exist
 *---------------------------------------------------------------------------*/
{
  UShort      blockId, classId;
  ULong       size;
  UInt        entryCnt;
  FSCRIPT     header;
  SCRIPT      script = NULL;
  VFILE_MARK *entryMarks;
  HYP         entry;
  Int         i;

  if (VFILE_rewind(recFile) != VFILE_SUCCESS)
    FATAL_BRA_ "Can't rewind the rec file" _KET;

  /* Peruse all recfile blocks for the training script */
  while (VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &classId, (V_ULong *)&size )) {
    VBX_DEBUG(VBX_print("BLOCKID = %d  CLASSID = %d\n", blockId, classId));
    if (classId == vocabId) {
      switch (blockId) {
      case TR_SCRIPT:
        if (VFILE_SUCCESS == VFILE_readStructure(recFile, &header, 1, RS_FSCRIPT)) {
          script = (SCRIPT) VBX_calloc(1, sizeof(script_t));
          entryCnt = script->entryCnt = header.wEntryCnt;
          script->discMinCnt = header.bDiscMinCnt;
          script->contMinCnt = header.bContMinCnt;
        } else {
          FATAL_BRA_ "SCRIPT_load: unable to read FSCRIPT (script header)" _KET;
        }

        VBX_DEBUG(VBX_print("Loading training SCRIPT with %d entries\n", entryCnt));

        entryMarks = script->entryMarks = (VFILE_MARK *) VBX_calloc(entryCnt, sizeof(VFILE_MARK));
        if ((entry = HYP_create(1))) {
          for (i = 0; i < entryCnt; i++) {
            VFILE_placeMark(recFile, &entryMarks[i]);
            if (!SCRIPT_readEntry(recFile, entry, FALSE))
              FATAL_BRA_ "SCRIPT_load: Can't read script entry %d", i _KET;
          }
          HYP_destroy(entry);
        } else {
          FATAL_BRA_ "SCRIPT_load: Can't create HYP for script entry" _KET;
        }
        break;
      default:
        break;
      }
    }
  }

  return(script);
}

void
SCRIPT_annihilate(SCRIPT script)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a training SCRIPT object
 * Return:  Only good will
 *---------------------------------------------------------------------------*/
{
  if (script) {
    if (script->entryMarks)
      VBX_free(script->entryMarks);
    VBX_free(script);
  }
}

HYP
SCRIPT_entry(SCRIPT script, VFILE recFile, UInt idx)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a pointer to the HYP (entry) at index idx, or NULL if there is no
 * entry at idx.
 *---------------------------------------------------------------------------*/
{
  HYP          entry = NULL;
  VFILE_MARK * entryMarks;

  if (script && idx < script->entryCnt) {
    entryMarks = script->entryMarks;
    VFILE_gotoMark(recFile, &entryMarks[idx]);
    entry = HYP_create(1);
    assert(entry != NULL);
    if (!SCRIPT_readEntry(recFile, entry, TRUE)) {
      HYP_destroy(entry);
      entry = NULL;
    }
  }

  return(entry);
}


UInt
SCRIPT_entryCnt(SCRIPT script)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the number of entries in a SCRIPT structure
 *---------------------------------------------------------------------------*/
{
  UInt entryCnt = 0;

  if (script)
    entryCnt = script->entryCnt;

  return(entryCnt);
}


static Bool
SCRIPT_readEntry(VFILE recFile, HYP entryPtr, Bool loadWords)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read a script block entry.  If loadWords is TRUE, allocate an array
 * of WRDHYP structures for the words and store the word sequence
 * Return: TRUE if successful, FALSE otherwise
 *---------------------------------------------------------------------------*/
{
  FSCRIPTE  header;
  UInt      wordCount;
  UShort    word;
  Int       i;
  Bool      rval = FALSE;

  if (VFILE_SUCCESS == VFILE_readStructure(recFile, &header, 1, RS_FSCRIPTE)) {

    HYP_setSyntaxData(entryPtr, (UInt) header.wGrammarID, (UInt) header.wStartNode, (UInt) header.wEndNode);
    wordCount = (UInt) header.bWordCnt;

    /* Allocate & assign entries if requested; otherwise, skip over them */
    if (loadWords && (!wordCount || !HYP_setWordCount(entryPtr, wordCount)))
      return(FALSE);

    for (i = 0; i < wordCount; i++) {
      if (VFILE_SUCCESS != VFILE_readStructure(recFile, &word, 1, RS_FV_Uns))
        FATAL_BRA_ "SCRIPT_readEntry: Can't read training script words" _KET;
      if (loadWords)
        HYP_setWId(entryPtr, i, (UInt) word);
    }
    rval = TRUE;
  }

  return(rval);
}

