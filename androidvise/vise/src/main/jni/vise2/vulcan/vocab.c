/* vocab.c - VISE Vocabulary Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Implements vocabularies for the VISE recognizer.  Vocabularies contain
 * all of the knowledge about the words (their word IDs, names and models)
 * which a set of grammars may share, but nothing about the grammars
 * themselves.
 * 
 * HISTORY
 * 12-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"
#if defined(CELIB)
#include "celib.h"
#endif

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "ntmath.h"
#include "vbx.h"
#include "llist.h"
#include "utils.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "hash.h"
#include "trans.h"
#include "models.h"
#include "syntax.h"
#include "vocab.h"
#include "parse.h"

#define VOCAB_DFLT_TRAIN_CNT   1
#define VOCAB_DFLT_TRAIN_SUM   200

#define VOCENT_GROW_MAX_MODELS 2
#define HYP_GROWTH_QUANTUM     25

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#define VBX_FATALSTATUS(R,S)   { Int v = (R); if (R) FATAL_BRA_ "condition = %d; status = %s", v, VISEERR_string(S) _KET; }
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#define VBX_FATALSTATUS(R,S)
#endif

/* Vocabulary entry (one per word) */
typedef struct vocabent_s {
  VOCAB       vocabulary;      /* The vocabulary this entry belongs to */
  Char      * name;            /* The name of the word */
  UInt        baseWId;         /* The wid of the base model */
  UInt        widIter;         /* The model iterator */
  UInt        class;           /* The class of equivalent models */
} vocent_t;

/* Model entry (one per wid) */
typedef struct modent_s {
  VOCENT      vocent;          /* The corresponding vocabulary entry */
  UInt        altWId;          /* Wid of the next alternative model */
  UInt        modelId;
} modent_t, * MODENT;

#define VOCAB_GROW_MAX_GRAMMARS 16

/* Vocabulary */
typedef struct vocab_s {
  Char      * name;            /* The name of the vocabulary */
  HASH        nameHT;          /* Word name hash table */
  MODENT      model;           /* The word model array */
  MODELS      models;          /* The repertoire of acoustic models */
  SYNTAX    * grammar;         /* The grammars that use this vocabulary */
  VOCMAP      map;             /* The word map used to make the vocabulary */
  TRANS       trans;           /* Optional response facility data */
  UInt        id;              /* The ID for this vocabulary */
  UInt        numModels;       /* The number of models in the array */
  size_t      maxGrammars;     /* The current size of the grammar array */
  UInt        numGrammars;     /* The number of grammars in the array */
  UInt        minWId;          /* The minimum word ID */
  UInt        maxWId;          /* The maximum word ID */
  UInt        currentWId;      /* The word ID iterator */
  UInt        dfltTrainCount;
  UInt        dfltTrainScore;
  V_Err       status;
} vocab_t;

/* A vocabulary iterator */
typedef struct vociter_s {
  VOCAB        vocab;          /* The vocabulary */
  UInt         wordId;         /* The iterator over the word models */
} vociter_t;

/* Private prototypes */

static UInt     VOCMAP_numWords(VOCMAP map, UInt * minWId, UInt * maxWId);

static VOCENT   VOCENT_create(VOCAB vocab, Char * name, UInt baseWId, UInt classId);
static void     VOCENT_annihilate(VOCENT entry);
static Bool     VOCENT_addWordId(VOCENT entry, UInt wid);
static Bool     VOCENT_cloneModels(VOCENT entry);

static MODENT   VOCAB_createModents(VOCAB vocab, UInt minWId, UInt maxWId);
static UInt     VOCAB_assignModel(VOCAB vocab, FUPATE * model, UInt modelId, WRDMAP map);
static UInt     VOCAB_assignModelToEntry(VOCAB vocab, VOCENT entry, FUPATE * model, UInt modelId);
static VOCENT * VOCAB_tokenize(VOCAB vocab, Char * word, Char * rest, UInt * index);
static Char   * GetNextWord(Char ** rest);

static UInt
VOCMAP_numWords(VOCMAP map, UInt * minWId, UInt * maxWId)
/***************************************************************************
 *
 * Return the number of entries in a vocabulary map and report the
 * minimum and maximum word Ids
 *
 ***************************************************************************/
{
  UInt  numWords = 0;
  UInt  wid;

  *minWId = UINT_MAX;
  *maxWId = 0;

  while ((wid = map[numWords].wordId)) {
    numWords++;
    if (wid < *minWId) *minWId = wid;
    if (wid > *maxWId) *maxWId = wid;
  }

  return(numWords);
}


static VOCENT
VOCENT_create(VOCAB vocab, Char * name, UInt baseWId, UInt classId)
/***************************************************************************
 *
 * Create a vocabulary entry. Note that this function allocates a string
 * for the name, so that VOCENT_annihilate() can always safely free it.
 * Thus the name parameter need not point to a persistent string.
 *
 ***************************************************************************/
{
  VOCENT  entry = (VOCENT) calloc(1, sizeof(vocent_t));
  MODENT  modent;

  if (entry) {
    if (name) entry->name = strdup(name);
    if ((name == NULL || entry->name != NULL) && ((modent = vocab->model) != NULL)) {
      entry->vocabulary = vocab;
      entry->baseWId = baseWId;
      entry->class = classId;
      modent[baseWId - vocab->minWId].vocent = entry;
      modent[baseWId - vocab->minWId].altWId = 0;
      modent[baseWId - vocab->minWId].modelId = 0;
    } else {
      VOCENT_annihilate(entry);
      entry = NULL;
    }
  }

  return(entry);
}


static void
VOCENT_annihilate(VOCENT entry)
/***************************************************************************
 *
 * Annihilate a vocabulary entry, freeing its name string and wid array.
 *
 ***************************************************************************/
{
  if (entry) {
    VBX_free(entry->name);
    free(entry);
  }
}


UInt
VOCENT_numModels(VOCENT entry)
/***************************************************************************
 *
 * Return the number of models for the specified vocabulary entry
 *
 ***************************************************************************/
{
  VOCAB   vocab;
  MODENT  modent;
  UInt    numModels;
  UInt    wid;

  assert(entry);

  numModels = 0;
  if ((vocab = entry->vocabulary) && (modent = vocab->model))
    for (wid = entry->baseWId; wid; wid = modent[wid - vocab->minWId].altWId)
      numModels++;

  return(numModels);
}


static Bool
VOCENT_addWordId(VOCENT entry, UInt wid)
/***************************************************************************
 *
 * Add a word Id to an existing vocabulary entry
 *
 ***************************************************************************/
{
  VOCAB   vocab;
  MODENT  modent;
  UInt    thisWId;
  UInt    altWId;

  assert(entry);

  if ((vocab = entry->vocabulary) && (thisWId = entry->baseWId) && (modent = vocab->model)) {

    /* Find the last alternative modent for this entry */
    while ((altWId = modent[thisWId - vocab->minWId].altWId))
      thisWId = altWId;

    /* Append the modent for the new word Id to the list of alternative modents */
    modent[thisWId - vocab->minWId].altWId = wid;
    modent[wid - vocab->minWId].vocent = entry;
    modent[wid - vocab->minWId].altWId = 0;
    modent[wid - vocab->minWId].modelId = 0;

    return(TRUE);

  } else {
    return(FALSE);
  }
}


UInt
VOCENT_firstWordId(VOCENT entry)
/***************************************************************************
 *
 * Return the word ID of the first model for the specified vocabulary entry,
 * or zero if there are no models.
 *
 ***************************************************************************/
{
  assert(entry);

  entry->widIter = entry->vocabulary->model[entry->baseWId - entry->vocabulary->minWId].altWId;

  return(entry->baseWId);
}


UInt
VOCENT_nextWordId(VOCENT entry)
/***************************************************************************
 *
 * Return the word ID for the next model for the specified vocabulary entry,
 * or zero if there are no more models.
 *
 ***************************************************************************/
{
  UInt  wid;

  assert(entry);

  if ((wid = entry->widIter))
    entry->widIter = entry->vocabulary->model[wid - entry->vocabulary->minWId].altWId;

  return(wid);
}


VOCAB
VOCENT_vocabulary(VOCENT entry)
/***************************************************************************
 *
 * Return the vocabulary to which a vocabulary entry belongs
 *
 ***************************************************************************/
{
  assert(entry);
  return(entry->vocabulary);
}


UInt
VOCENT_class(VOCENT entry)
/***************************************************************************
 *
 * Return the class ID of a vocabulary entry
 *
 ***************************************************************************/
{
  assert(entry);
  return(entry->class);
}


Char *
VOCENT_wordName(VOCENT entry)
/***************************************************************************
 *
 * Return the word name of a vocabulary entry
 *
 ***************************************************************************/
{
  assert(entry);
  return(entry->name);
}


void
VOCENT_useBestModel(VOCENT entry)
/***************************************************************************
 *
 * Replace all the models for this entry with the most highly trained model
 *
 ***************************************************************************/
{
  VOCAB    vocab;
  UInt     wid, bestWId;
  FUPATE   modelA, modelB;
  FUPATE * model, * bestModel, * newBestModel;

  assert(entry);

  vocab = VOCENT_vocabulary(entry);

  /* Find the most highly trained model */
  bestWId = VOCENT_firstWordId(entry);
  bestModel = &modelA;
  VOCAB_getModel(vocab, bestWId, bestModel);
  model = &modelB;
  while (wid = VOCENT_nextWordId(entry)) {
    VOCAB_getModel(vocab, wid, model);
    if (model->wCount > bestModel->wCount) {
      bestWId = wid;
      newBestModel = model;
      model = bestModel;
      bestModel = newBestModel;
    }
  }

  /* Copy it into the others */
  for (wid = VOCENT_firstWordId(entry); wid; wid = VOCENT_nextWordId(entry))
    if (wid != bestWId)
      VOCAB_putModel(vocab, wid, bestModel);
}


static Bool
VOCENT_cloneModels(VOCENT entry)
/***************************************************************************
 *
 * If some of the wids have models, copy them into the MODENT's whose wid's
 * lack models.
 * Return FALSE if there are no existing models, TRUE otherwise.
 *
 ***************************************************************************/
{
  VOCAB    vocab;
  FUPATE   model;
  FUPATE * modelPtr;
  Bool     success;
  UInt     wid;

  assert(entry);

  vocab = VOCENT_vocabulary(entry);

  /* If the first wid has no model, then none do */
  success = VOCAB_getModel(vocab, entry->baseWId, &model);
  modelPtr = success ? &model : NULL;

  wid = entry->baseWId;
  while (VOCAB_assignModelToEntry(vocab, entry, modelPtr, 0)) {
    wid = vocab->model[wid - vocab->minWId].altWId;
    VOCAB_getModel(vocab, wid, modelPtr);
  }

  return(success);
}


VOCAB
VOCAB_create(UInt vocabId)
/***************************************************************************
 *
 * Create a new vocabulary with the specified Id
 *
 ***************************************************************************/
{
  VOCAB  vocab = (VOCAB) calloc(1, sizeof(vocab_t));

  if (vocab) {
    vocab->minWId = UINT_MAX;
    vocab->id = vocabId;
    vocab->dfltTrainCount = VOCAB_DFLT_TRAIN_CNT;
    vocab->dfltTrainScore = VOCAB_DFLT_TRAIN_SUM;
  }

  return(vocab);
}


void
VOCAB_annihilate(VOCAB vocab)
/***************************************************************************
 *
 * Annihilate a vocabulary, including all of its entries
 *
 ***************************************************************************/
{
  if (vocab) {

    /* Free the name string */
    VBX_free(vocab->name);

    /* Free the hash table and the vocabulary entries its values point to */
    HASH_annihilate(vocab->nameHT);

    /* Free the model array, if any */
    VBX_free(vocab->model);

    /* Free the grammar array, if any */
    VBX_free(vocab->grammar);

    /* Free the translations entry, if any */
    if (vocab->trans)
      TRANS_annihilate(vocab->trans);

    free(vocab);
  }
}


V_Err
VOCAB_status(VOCAB vocab)
/***************************************************************************
 *
 * Return the status of a vocabulary
 *
 ***************************************************************************/
{
  return(vocab ? vocab->status : NOSUCHOBJECT);
}


UInt
VOCAB_id(VOCAB vocab)
/***************************************************************************
 *
 * Return the id of a vocabulary
 *
 ***************************************************************************/
{
  return(vocab->id);
}


Char *
VOCAB_name(VOCAB vocab)
/***************************************************************************
 *
 * Return the name of a vocabulary
 *
 ***************************************************************************/
{
  return(vocab->name);
}


Bool
VOCAB_load(VOCAB vocab, VFILE recFile)
/***************************************************************************
 *
 * Load a vocabulary from a rec file
 * Returns TRUE if successful, FALSE otherwise.
 *
 ***************************************************************************/
{
  FVOCABTRANS   vocTrans;
  FVOCABTRANSE  wordTrans;
  VFILE_MARK    fmark;
  Char        * wordName;
  UShort      * wMultID;          /* Array of words' multiple template IDs */
  FLOFF       * loWN;             /* Word name lengths and offsets */
  UInt          wid, numWords;
  Bool          success;


  /* Read the vocabulary translation block */
  if ((success = vocab != NULL && recFile != NULL && VFILE_SUCCESS == VFILE_readStructure(recFile, &vocTrans, 1, RS_FVOCABTRANS))) {

    numWords = (UInt) vocTrans.wNumberWords;

    /* Get the name of the vocabulary */
    if (vocTrans.loVocabularyName.bLength > 0 &&
        (vocab->name = (Char *) malloc((size_t) (vocTrans.loVocabularyName.bLength + 1))) &&
        VFILE_SUCCESS == VFILE_placeMark(recFile, &fmark) &&
        VFILE_SUCCESS == VFILE_getText(recFile, &vocTrans.loVocabularyName, (UChar *) (vocab->name)) &&
        VFILE_SUCCESS == VFILE_gotoMark(recFile, &fmark)) {
      VBX_DEBUG(VBX_print("  VOCAB_load loading vocabulary \"%s\"\n", vocab->name));
    } else {
      VBX_free(vocab->name);
      VBX_DEBUG(VBX_print("  VOCAB_load can't get name of vocabulary from recFile\n"));
    }

    /* Create the name hash table for the vocabulary */
    vocab->nameHT = HASH_create((Int) numWords, NULL, NULL, (void (*)(Ptr)) VOCENT_annihilate);

    /* Create temporary arrays of wMultID and len/offset */
    wMultID = (UShort *) calloc((size_t) (numWords + 1), sizeof(UShort));
    loWN = (FLOFF *) calloc((size_t) (numWords + 1), sizeof(FLOFF));

    if ((success = vocab->nameHT != NULL && wMultID != NULL && loWN != NULL && (VOCAB_createModents(vocab, 1, numWords)))) {

      /* Read translation entries */
      for (wid = 1; wid <= numWords; wid++) {
        if (!(success = VFILE_SUCCESS == VFILE_readStructure(recFile, &wordTrans, 1, RS_FVOCABTRANSE))) {
          vocab->status = CANTREADVFILESTRUCT;
          break;
        }
        wMultID[wid] = wordTrans.wMultipleTemplateID;
        loWN[wid] = wordTrans.loWordName;
      }

      /* Get the word names and add entries to the vocabulary */
      for (wid = 1; success && wid <= numWords; wid++) {
        if (!(success = (wordName = (Char *) malloc((size_t) (loWN[wid].bLength + 1))) != NULL)) {
          vocab->status = OUTOFMEMORY;
        } else if (!(success = VFILE_SUCCESS == VFILE_getText(recFile, &(loWN[wid]), (UChar *) wordName))) {
          VBX_DEBUG(VBX_print("  VOCAB_load can't find word name in recFile\n"));
          vocab->status = CANTREADVFILETEXT;
        } else {
          success = VOCAB_addWord(vocab, wordName, (UInt) wid, (UInt) wMultID[wid]);
        }
        VBX_free(wordName);
      }

    } else {
      vocab->status = OUTOFMEMORY;
    }

    /* Deallocate the temporary arrays */
    VBX_free(wMultID);
    VBX_free(loWN);

  } else if (vocab) {
    vocab->status = CANTREADVFILESTRUCT;
  }

  return(success);
}

Bool
VOCAB_loadTrans(VOCAB vocab, VFILE recFile)
/***************************************************************************
 *
 * Load the translation entries for the specified vocab from the recfile
 * of its origin.  Returns TRUE if successful, FALSE otherwise
 *
 ***************************************************************************/
{
  FVOCABTRANS    vocTrans;
  FVOCABTRANSE   vocTransEntry;
  TRANSENT       transent = NULL;
  TRANS          trans = NULL;
  FLOFF          hostTrans;
  FLOFF          voiceTrans;
  FLOFF          displayTrans;
  UShort         blockId, classId;
  UInt           wid, numWords;
  ULong          size;

  if (vocab->trans != NULL) {
    WARN_BRA_ "Translations already loaded for vocab %d\n", vocab->id _KET;
    return(TRUE);
  }

  trans = vocab->trans = TRANS_create();

  if (VFILE_rewind(recFile) != VFILE_SUCCESS)
    FATAL_BRA_ "Can't rewind the rec file" _KET;

  /* Peruse all recfile blocks for vocab response data having our classId */
  while (VFILE_SUCCESS == VFILE_getNextBlock(recFile, &blockId, &classId, (V_ULong *)&size )) {
    VBX_DEBUG(VBX_print("BLOCKID = %d  CLASSID = %d\n", blockId, classId));
    if (vocab->id == classId) {
      switch (blockId) {
      case TR_VOCABULARY_TRANS:
        VBX_DEBUG(VBX_print("Loading VOCABTRANS for vocab %d\n", vocab->id));
        if (VFILE_readStructure(recFile, &vocTrans, 1, RS_FVOCABTRANS) == VFILE_SUCCESS) {
          numWords = (UInt) vocTrans.wNumberWords;
          if (numWords > 0) {

            /* Hash table for translation entries, hashed by wordId */
            trans->widHT = HASH_create(numWords, HASH_integer, HASH_integerEquals, (void (*)(Ptr)) TRANSENT_annihilate);

            /* Read L/O for vocab translation entries */
            for (wid = 1; wid <= numWords; wid++) {
              VFILE_readStructure(recFile, &vocTransEntry, 1, RS_FVOCABTRANSE);
              /* VBX_DEBUG(VBX_print("(wid %d)\n", wid)); */
              /* VBX_DEBUG(PRINT_FLOFF(&vocTransEntry.loHostTranslation)); */
              /* VBX_DEBUG(PRINT_FLOFF(&vocTransEntry.loVoiceTranslation)); */
              /* VBX_DEBUG(PRINT_FLOFF(&vocTransEntry.loDisplayTranslation)); */
              hostTrans = vocTransEntry.loHostTranslation;
              voiceTrans = vocTransEntry.loVoiceTranslation;
              displayTrans = vocTransEntry.loDisplayTranslation;

              transent = TRANSENT_create(wid, hostTrans, voiceTrans, displayTrans);
              transent->wid = wid;
              HASH_add(trans->widHT, (Ptr) wid, (Ptr) transent);
            }
          }

          /* Read Host Response Delineators */
          trans->HRDelin = DELIN_create();
          trans->HRDelin->initiator = vocTrans.loHRFInitiator;
          trans->HRDelin->separator = vocTrans.loHRFSeparator;
          trans->HRDelin->terminator = vocTrans.loHRFTerminator;

          /* Read Voice Response Delineators */
          trans->VRDelin = DELIN_create();
          trans->VRDelin->initiator = vocTrans.loVRFInitiator;
          trans->VRDelin->separator = vocTrans.loVRFSeparator;
          trans->VRDelin->terminator = vocTrans.loVRFTerminator;
 
          /* Read Display Response Delineators */
          trans->DRDelin = DELIN_create();
          trans->DRDelin->initiator = vocTrans.loDRFInitiator;
          trans->DRDelin->separator = vocTrans.loDRFSeparator;
          trans->DRDelin->terminator = vocTrans.loDRFTerminator;
        }
        break;
      case TR_CONTROL_PARSE_VOCAB:
        trans->controlParseTbl = PARSE_loadTable(recFile);
        if (trans->controlParseTbl == NULL)
          FATAL_BRA_ "CONTROL_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("CONTROL_PARSE load for vocab %d returns %p\n", vocab->id, trans->controlParseTbl));
        break;
      case TR_VOICE_PARSE_VOCAB:
        trans->voiceParseTbl = PARSE_loadTable(recFile);
        if (trans->voiceParseTbl == NULL)
          FATAL_BRA_ "VOICE_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("VOICE_PARSE load for vocab %d returns %p\n", vocab->id, trans->voiceParseTbl));
        break;
      case TR_HOST_PARSE_VOCAB:
        trans->hostParseTbl = PARSE_loadTable(recFile);
        if (trans->hostParseTbl == NULL)
          FATAL_BRA_ "HOST_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("HOST_PARSE load for vocab %d returns %p\n", vocab->id, trans->hostParseTbl));
        break;
      case TR_DISPLAY_PARSE_VOCAB:
        trans->displayParseTbl = PARSE_loadTable(recFile);
        if (trans->displayParseTbl == NULL)
          FATAL_BRA_ "DISPLAY_PARSE load FAILED, PARSE_loadTable returns NULL\n" _KET;
        VBX_DEBUG(VBX_print("DISPLAY_PARSE load for vocab %d returns %p\n", vocab->id, trans->displayParseTbl));
        break;
      default:
        break;
      }
    }
  }

  return(TRUE);
}


Bool
VOCAB_loadFromMap(VOCAB vocab, VOCMAP map)
/***************************************************************************
 *
 * Load a vocabulary from a vocabulary map
 * Returns TRUE if successful, FALSE otherwise.
 *
 ***************************************************************************/
{
  UInt  numWords;
  UInt  minWId;
  UInt  maxWId;
  Bool  success = TRUE;

  /* Get the number of words in the map */
  numWords = VOCMAP_numWords(map, &minWId, &maxWId);

  if (numWords) {

    /* If we can create a name hash table and models array for the vocabulary, load them from the map */
    if ((success = (vocab->nameHT = HASH_create((Int) numWords, NULL, NULL, (void (*)(Ptr)) VOCENT_annihilate)) && 
                   VOCAB_createModents(vocab, minWId, maxWId))) {
      vocab->map = map;
      do success = success && VOCAB_addWord(vocab, map->wordName, map->wordId, map->classId);
      while (map++, --numWords);

    /* Otherwise, we've run out of memory */
    } else {
      vocab->status = OUTOFMEMORY;
    }
  } else {
    vocab->status = VISESUCCESS;
  }

  return(success);
}


static MODENT
VOCAB_createModents(VOCAB vocab, UInt minWId, UInt maxWId)
/***************************************************************************
 *
 * Create a MODENT array for a vocabulary
 *
 ***************************************************************************/
{
  VOCENT  vocent;
  UInt    wid;

  assert(vocab);

  vocab->minWId = minWId;
  vocab->maxWId = maxWId;

  if (vocab->model != NULL || (vocab->model = (MODENT) calloc((size_t) (maxWId - minWId + 1), sizeof(modent_t))) != NULL) {
     if (vocab->name) {
      VBX_DEBUG(VBX_print("VOCAB_createModents for vocabulary \"%s\" succeeds\n", vocab->name));
     } else {
      VBX_DEBUG(VBX_print("VOCAB_createModents succeeds\n"));
     }
    vocab->status = VISESUCCESS;
  } else {
    vocab->status = OUTOFMEMORY;
  }

  return(vocab->model);
}


Bool
VOCAB_addWord(VOCAB vocab, Char * name, UInt wid, UInt classId)
/***************************************************************************
 *
 * Add a word Id for a model of the named word to a vocabulary.  Creates
 * a vocabulary entry for the word if none exists.
 *
 ***************************************************************************/
{
  Ptr     hashValue;
  VOCENT  entry;

  /* If the word is not yet in vocab, put it there */
  if (!HASH_lookup(vocab->nameHT, name, &hashValue)) {
    entry = VOCENT_create(vocab, name, wid, classId);
    HASH_add(vocab->nameHT, VOCENT_wordName(entry), (Ptr) entry);

  /* Otherwise, it must have the same properties as the one already there, or else */
  } else {
    entry = (VOCENT) hashValue;
    if (strcmp(name, entry->name) || classId != entry->class) {
      vocab->status = WORDINVOCAB;
      return(FALSE);
    } else {
      VOCENT_addWordId(entry, wid);
    }
  }

  vocab->status = VISESUCCESS;
  return(TRUE);
}


VOCENT
VOCAB_entryFromName(VOCAB vocab, Char * wordName)
/***************************************************************************
 *
 * Return the vocabulary entry for the named word, or NULL if the word is
 * not in the vocabulary.
 *
 ***************************************************************************/
{
  Ptr     hashValue;
  VOCENT  entry;

  entry = (VOCENT) (HASH_lookup(vocab->nameHT, wordName, &hashValue) ? hashValue : NULL);

  vocab->status = entry == NULL ? WORDNOTINVOCAB : VISESUCCESS;

  return(entry);
}


UInt
VOCAB_wordIdFromName(VOCAB vocab, Char * wordName)
/***************************************************************************
 *
 * Return the next word Id for the named word, or zero if the word is not
 * in the vocabulary or has no more word Ids.
 *
 ***************************************************************************/
{
  VOCENT  entry = VOCAB_entryFromName(vocab, wordName);

  return(entry == NULL ? 0 : VOCENT_nextWordId(entry));
}


VOCENT
VOCAB_entryFromId(VOCAB vocab, UInt wid)
/***************************************************************************
 *
 * Return the vocabulary entry of the word with the given wid.  Returns NULL
 * if the word has no vocabulary entry.
 *
 ***************************************************************************/
{
  VOCENT  vocent = NULL;

  assert(vocab);

  if (vocab->model && vocab->minWId <= wid && wid <= vocab->maxWId)
    vocent = vocab->model[wid - vocab->minWId].vocent;

  vocab->status = vocent == NULL ? WORDNOTINVOCAB : VISESUCCESS;

  return(vocent);
}


Char *
VOCAB_wordNameFromId(VOCAB vocab, UInt wid)
/***************************************************************************
 *
 * Return the name of the word with the given wid.  Returns NULL if the
 * word is not in the vocabulary or has no models.
 *
 ***************************************************************************/
{
  VOCENT  vocent = VOCAB_entryFromId(vocab, wid);

  return(vocent == NULL ? NULL : vocent->name);
}


Bool
VOCAB_assignModels(VOCAB vocab, MODELS models, WRDMAP map)
/***************************************************************************
 *
 * Assign models to a vocabulary.  This must be called ONCE, AFTER all calls
 * to VOCAB_load() and VOCAB_loadFromMap()).  If the models argument is
 * NULL, this function just allocates the MODENT array.
 *
 ***************************************************************************/
{
  MODITER  moditer;
  FUPATE   model;
  UInt     modelId;
  Bool     success;

  if ((success = vocab != NULL)) {

     /* Put the model repertoire into the vocabulary */
    vocab->models = models;

     /* Allocate the MODENT array if it doesn't already exist */
    if ((success = vocab->model != NULL || (vocab->model = (MODENT) calloc((size_t) (vocab->maxWId - vocab->minWId + 1), sizeof(modent_t))) != NULL)) {

      if (models != NULL) {
         /* Add individual models to the vocabulary as needed */
        if ((success = (moditer = MODITER_create(models)) != NULL)) {
          while (success && (modelId = MODITER_nextModelId(moditer))) {
            if (!(success = MODELS_getModel(models, modelId, &model)))
              vocab->status = MODELS_status(models);
            else if (!VOCAB_assignModel(vocab, &model, modelId, map))
              VBX_DEBUG(VBX_print("  VOCAB_assignModels: can't assign model for %s\n", (Char *) model.bName));
          }
          MODITER_destroy(moditer);
        } else {
          free(vocab->model);
          vocab->status = OUTOFMEMORY;
        }
      } else {
        vocab->status = VISESUCCESS;
      }

    } else {
      vocab->status = OUTOFMEMORY;
    }

  }

  return(success);
}


void
VOCAB_removeModels(VOCAB vocab)
/***************************************************************************
 *
 * Remove models from a vocabulary
 *
 ***************************************************************************/
{
  UInt     index;

  if (vocab) {
     /* Remove the model repertoire from the vocabulary */
    vocab->models = NULL;
     /* Remove the model Ids from the MODENTs */
    if (vocab->model != NULL) {
      for (index = 0; index < vocab->maxWId - vocab->minWId + 1; index++) {
         /* Remove the model Id */
        vocab->model[index].modelId = 0;
         /* Reset the word Id iterator in the VOCENT */
        if (vocab->model[index].vocent) vocab->model[index].vocent->widIter = vocab->model[index].vocent->baseWId;
      }
    }
    vocab->numModels = 0;
  }
}


Bool
VOCAB_cloneModels(VOCAB vocab)
/***************************************************************************
 *
 * For each entry in the vocabulary, give existing models to wid's which
 * have no models, optionally replicating the actual model structures.
 * Return TRUE if this leaves no wid's without models, FALSE otherwise.
 * N.B. CALL THIS FUNCTION IMMEDIATELY AFTER CALLING VOCAB_assignModels(),
 * with no intervening calls which might disturb the state of the VOCENTs.
 *
 ***************************************************************************/
{
  VOCENT  entry;
  Bool    allModelled = TRUE;

  assert(vocab);

  if (vocab->model != NULL || (vocab->model = (MODENT) calloc((size_t) (vocab->maxWId - vocab->minWId + 1), sizeof(modent_t))) != NULL) {
    for (entry = (VOCENT) HASH_firstValue(vocab->nameHT, NULL); entry; entry = (VOCENT) HASH_nextValue(vocab->nameHT, NULL))
      allModelled = VOCENT_cloneModels(entry) && allModelled;
    VBX_DEBUG(VBX_print("VOCAB_cloneModels: models %sassigned to all wid's\n", allModelled ? "" : "NOT "));
    vocab->status = allModelled ? VISESUCCESS : CANTCLONEMODELS;
  } else {
    vocab->status = OUTOFMEMORY;
    allModelled = FALSE;
  }

  return(allModelled);
}


UInt
VOCAB_assignModel(VOCAB vocab, FUPATE * model, UInt modelId, WRDMAP map)
/***************************************************************************
 *
 * Assign the model for the given word to the vocabulary, and return the
 * model's word Id.  This function should not be called until all calls
 * to VOCAB_addWord() have been made.
 *
 ***************************************************************************/
{
  VOCENT  entry;
  Char  * name;

  if (vocab && model) {

    name = (Char *) model->bName;
    if (map) name = WRDMAP_name(map, name);

    /* If the entry is in the vocabulary add the model */
    if ((entry = VOCAB_entryFromName(vocab, name)))
      return(VOCAB_assignModelToEntry(vocab, entry, model, modelId));

  } else if (vocab) {
    vocab->status = INVALIDARGUMENT;
  }

  return(0);
}


UInt
VOCAB_assignModelToEntry(VOCAB vocab, VOCENT entry, FUPATE * model, UInt modelId)
/***************************************************************************
 *
 * Tries to add the model (or, if modelId is zero, a copy of the model) to
 * the specified vocabulary entry.  Returns the word Id if successful, zero
 * otherwise.  Failure means that there is no wid corresponding to this
 * vocabulary entry.
 *
 ***************************************************************************/
{
  UInt     wid = 0;
  MODENT   modent;

  if (vocab) {
    if (entry) {
      if ((wid = VOCENT_nextWordId(entry))) {

        assert(vocab->minWId <= wid && wid <= vocab->maxWId);

        vocab->status = VISESUCCESS;

        modent = &vocab->model[wid - vocab->minWId];

         /* Assign the model only if one has been supplied */
        if (model) {
           /* Assign the given model only if this wid doesn't already have one */
          if (!modent->modelId) {
             /* If no model ID is supplied, add the model to the models repository and get its new model ID */
            if (!modelId) {
              if ((modent->modelId = MODELS_addModel(vocab->models, model))) {
                VBX_DEBUG(VBX_print("  VOCAB_assignModelToEntry: assigned NEW model (%d) for \"%s\" (wid = %d) to vocab %d\n",
                                    vocab->numModels + 1, (Char *) model->bName, wid, vocab->id));
                vocab->numModels++;
              }
              vocab->status = MODELS_status(vocab->models);
             /* Otherwise, use the model ID supplied */
            } else {
              modent->modelId = modelId;
              VBX_DEBUG(VBX_print("  VOCAB_assignModelToEntry: assigned OLD model (%d) for \"%s\" (wid = %d) to vocab %d\n",
                                  vocab->numModels + 1, (Char *) model->bName, wid, vocab->id));
              vocab->numModels++;
            }
          }
        }
      }
    } else {
      vocab->status = INVALIDARGUMENT;
    }
  }

  return(wid);
}


MODELS
VOCAB_models(VOCAB vocab)
/***************************************************************************
 *
 * Return the set of models for the vocabulary
 *
 ***************************************************************************/
{
  assert(vocab);

  return(vocab->models);
}


TRANS
VOCAB_trans(VOCAB vocab)
/***************************************************************************
 *
 * Return the translations for this vocabulary
 *
 ***************************************************************************/
{
  assert(vocab);

  return(vocab->trans);
}


Bool
VOCAB_makeModel(VOCAB vocab, UInt wid, UInt numKernels, FUPATE * model)
/***************************************************************************
 *
 * Make a model for the word whose id is wid
 *
 ***************************************************************************/
{
  return(MODEL_init(model, VOCAB_wordNameFromId(vocab, wid), numKernels));
}


Bool
VOCAB_copyModel(VOCAB vocab, UInt srcWId, UInt destWId, Bool copyDwells)
/***************************************************************************
 *
 * Copy the model for srcWId into the model for destWId, optionally
 * copying the dwells.  If the kernel dwells are not copied, the models
 * must have the same number of kernels.
 *
 ***************************************************************************/
{
  FUPATE  srcModel, destModel;
  Bool    success;

  if ((success = VOCAB_getModel(vocab, srcWId, &srcModel) &&
                 (VOCAB_getModel(vocab, destWId, &destModel) || VOCAB_makeModel(vocab, destWId, 0, &destModel)))) {
    if ((success = MODEL_copy(&destModel, &srcModel, copyDwells, FALSE)))
      success = VOCAB_putModel(vocab, destWId, &destModel);
    else
      vocab->status = CANTCOPYMODEL;
  }

  return(success);
}


UInt
VOCAB_modelId(VOCAB vocab, UInt wid)
/***************************************************************************
 *
 * Returns the modelId if a model exists for the wid, zero otherwise.
 *
 ***************************************************************************/
{
  if (vocab && vocab->model && vocab->minWId <= wid && wid <= vocab->maxWId)
    return(vocab->model[wid - vocab->minWId].modelId);
  else
    return(0);
}


Bool
VOCAB_getModel(VOCAB vocab, UInt wid, FUPATE * model)
/***************************************************************************
 *
 * Get a model from the vocabulary
 * If the model parameter is NULL, returns TRUE iff the model exists;
 * otherwise, returns TRUE iff the model is successfully extracted.
 * THE CALLER IS RESPONSIBLE FOR ALLOCATING AND FREEING THE MODEL STRUCTURE.
 *
 ***************************************************************************/
{
  Bool  success;

  if ((success = vocab && vocab->model && vocab->minWId <= wid && wid <= vocab->maxWId)) {
    success = MODELS_getModel(vocab->models, vocab->model[wid - vocab->minWId].modelId, model);
    vocab->status = MODELS_status(vocab->models);
  } else if (vocab) {
    vocab->status = WORDNOTINVOCAB;
  }

  return(success);
}


Bool
VOCAB_putModel(VOCAB vocab, UInt wid, FUPATE * model)
/***************************************************************************
 *
 * Put a model into the vocabulary
 * If the model parameter is NULL, returns TRUE iff there is an existing
 * model for this wid;  otherwise, returns TRUE iff the model is inserted
 * successfully.
 * THE CALLER MAY FREE THE MODEL AFTER THE CALL.
 *
 ***************************************************************************/
{
  Bool  success;

  if ((success = vocab && vocab->model && vocab->minWId <= wid && wid <= vocab->maxWId)) {
    if (!(success = MODELS_putModel(vocab->models, vocab->model[wid - vocab->minWId].modelId, model)))
      if (model && (success = MODELS_status(vocab->models) == MISSINGWORDMODEL))
        if ((success = (vocab->model[wid - vocab->minWId].modelId = MODELS_addModel(vocab->models, model)) != 0))
          vocab->numModels++;
    vocab->status = MODELS_status(vocab->models);
  } else if (vocab) {
    vocab->status = WORDNOTINVOCAB;
  }

  return(success);
}


Bool
VOCAB_setModelDwells(VOCAB vocab, UInt wid, UChar kid, UChar minDwell, UChar maxDwell)
/***************************************************************************
 *
 * Set the dwells for the kid-th (0-origin) kernel in the specified model
 *
 ***************************************************************************/
{
  FUPATE  model;

  if (VOCAB_getModel(vocab, wid, &model)) {
    MODEL_swapDwells(&model, kid, &minDwell, &maxDwell);
    return(VOCAB_putModel(vocab, wid, &model));
  } else {
    return(FALSE);
  }
}


UInt
VOCAB_numModels(VOCAB vocab)
/***************************************************************************
 *
 * Return the number of models in the vocabulary
 *
 ***************************************************************************/
{
  assert(vocab);

  return(vocab->numModels);
}


Bool
VOCAB_addGrammar(VOCAB vocab, SYNTAX grammar)
/***************************************************************************
 *
 * Add the grammar to the vocabulary
 *
 ***************************************************************************/
{
  SYNTAX * syntax;

  assert(vocab);

  /* Enlarge the grammar array if it is full */
  if (vocab->numGrammars >= vocab->maxGrammars) {
    vocab->maxGrammars += VOCAB_GROW_MAX_GRAMMARS;
    if ((syntax = (SYNTAX *) realloc((Ptr) vocab->grammar, (size_t) (vocab->maxGrammars * sizeof(SYNTAX))))) {
      vocab->grammar = syntax;
    } else {
      vocab->maxGrammars -= VOCAB_GROW_MAX_GRAMMARS;
      vocab->status = OUTOFMEMORY;
      return(FALSE);
    }
  }

  /* Insert the new grammar */
  vocab->grammar[vocab->numGrammars++] = grammar;
  vocab->status = VISESUCCESS;
  return(TRUE);
}


SYNTAX *
VOCAB_grammars(VOCAB vocab, UInt * numGrammars)
/***************************************************************************
 *
 * Return the array of grammars using the vocabulary, and report its size.
 *
 ***************************************************************************/
{
  assert(vocab);

  *numGrammars = vocab->numGrammars;
  vocab->status = VISESUCCESS;

  return(vocab->grammar);
}


SYNTAX *
VOCAB_parse(VOCAB vocab, Char * transcription, UInt * numParsers)
/***************************************************************************
 *
 * Return an array of the grammars in the vocabulary which parse the given
 * transcription, and report its size.  THIS FUNCTION ALLOCATES AN ARRAY OF
 * GRAMMARS, WHICH THE CALLER MUST DEALLOCATE WHEN IT IS NO LONGER NEEDED.
 * Returns NULL if no grammars in the vocabulary parse the transcription.
 *
 ***************************************************************************/
{
  VOCENT  * tokens;
  UInt      numTokens;
  UInt      numGrammars;
  SYNTAX  * grammar;
  SYNTAX  * parser = NULL;

  assert(vocab);

  *numParsers = 0;
  /* If all the words in the transcription are in the vocabulary, and ... */
  if ((tokens = VOCAB_tokens(vocab, transcription, &numTokens)) &&
      /* there are grammars in the vocabulary, then ... */
      (grammar = VOCAB_grammars(vocab, &numGrammars))) {
    /* Allocate an array of grammars for the results */
    if ((parser = (SYNTAX *) calloc((size_t) numGrammars, sizeof(SYNTAX)))) {
      /* Find the grammars that parse the transcription */
      while (numGrammars--)
        if (parser[*numParsers] = SYNTAX_parse(*grammar++, tokens, numTokens))
          (*numParsers)++;
      if (!(*numParsers)) VBX_free(parser);
    } else {
      vocab->status = OUTOFMEMORY;
    }
  }
  VBX_free(tokens);

  return(parser);
}


UInt
VOCAB_getTraining(VOCAB vocab, UInt * sum, UInt * sumSqr, UInt * count, Bool reset)
/***************************************************************************
 *
 * Get the training statistics for the active models in a vocabulary, and
 * return the number of active models.  If reset is TRUE, the values that
 * the arguments point to are set to the computed values for this vocab;
 * otherwise, they are added to them.  THIS FUNCTION DIFFERS FROM
 * MODELS_getTraining() IN THAT IT COMPUTES THE TRAINING STATISTICS OVER
 * THE ACTIVE MODELS, WHEREAS THE LATTER SIMPLY EXTRACTS THEM FROM THE
 * HEADER IN THE MODEL SET, WHICH MAY NOT BE UP TO DATE.
 *
 ***************************************************************************/
{
  UInt     wid;
  FUPATE   model;
  UInt     numModels = 0;

  assert(vocab && sum && sumSqr && count);

  if (reset) {
    *sum = 0;
    *sumSqr = 0;
    *count = 0;
  }

  for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab)) {
    if (VOCAB_getModel(vocab, wid, &model)) {
      *sum += (model.wTrainingCount ? model.lSum : vocab->dfltTrainScore * vocab->dfltTrainCount);
      *sumSqr += (model.wTrainingCount ? model.lSquares : vocab->dfltTrainScore * vocab->dfltTrainScore * vocab->dfltTrainCount);
      *count += (model.wTrainingCount ? model.wTrainingCount : vocab->dfltTrainCount);
      numModels++;
    }
  }

  vocab->status = VISESUCCESS;

  return(numModels);
}


void
VOCAB_setTrainingDefault(VOCAB vocab, UInt score, UInt count)
/***************************************************************************
 *
 * Set the default values for the training statistics for models that
 * have no training.
 *
 ***************************************************************************/
{
  assert(vocab);

  vocab->dfltTrainCount = count;
  vocab->dfltTrainScore = score;
}


UInt
VOCAB_firstWordId(VOCAB vocab)
/***************************************************************************
 *
 * Return the word ID of the first model in the vocabulary,
 * or zero if there are no models.
 *
 ***************************************************************************/
{
  assert(vocab);

  vocab->currentWId = vocab->minWId;
  return(VOCAB_nextWordId(vocab));
}


UInt
VOCAB_nextWordId(VOCAB vocab)
/***************************************************************************
 *
 * Return the word ID for the next model in the vocabulary,
 * or zero if there are no more models.
 *
 ***************************************************************************/
{
  UInt  wid;

  assert(vocab);

  while (vocab->currentWId <= vocab->maxWId && !VOCAB_getModel(vocab, vocab->currentWId, NULL))
    vocab->currentWId++;

  if (vocab->currentWId > vocab->maxWId)
    wid = (UInt) 0;
  else
    wid = vocab->currentWId++;

  return(wid);
}


UInt
VOCAB_lastWordId(VOCAB vocab)
/***************************************************************************
 *
 * Return the word ID of the last model in the vocabulary,
 * or zero if there are no models.
 *
 ***************************************************************************/
{
  assert(vocab);

  return(vocab->maxWId);
}


UInt
VOCAB_numWordIds(VOCAB vocab)
/***************************************************************************
 *
 * Return the range of word IDs in the vocabulary
 *
 ***************************************************************************/
{
  return(vocab->maxWId - vocab->minWId + 1);
}


static Char    *
GetNextWord(Char ** rest)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns the next word in the string *rest, or null string if none.
 * If end of rest, sets *rest to NULL; otherwise, sets *rest to rest of *rest
 *---------------------------------------------------------------------------*/
{
  Char * word;
  Char * character = *rest;

  /* Skip over white space to the next word */
  while (*character == ' ' || *character == '\t')
    character++;

  /* Extract the word */
  word = character;
  while (*character != ' '  && *character != '\t' &&
         *character != '\n' && *character != '\0')
    character++;

  /* If end of sentence, null rest */
  if (*character == '\0') {
    /* No further words remain, so set *rest to NULL */
    *rest = NULL;
  } else {
    *character = '\0';
    character++;
    /* Skip over white space to try to find the next word */
    while (*character == ' ' || *character == '\t')
      character++;
    /* If none, set *rest to NULL; else set it to point to the next word */
    if (*character == '\0')
      *rest = NULL;
    else
      *rest = character;
  }

  return(word);
}


VOCENT *
VOCAB_tokens(VOCAB vocab, Char * transcription, UInt * numTokens)
/**************************************************************************
 *
 * Return the array of vocabulary entries for a transcription, and report
 * the size of the array.  Return NULL if the transcription cannot be
 * tokenized.  THIS FUNCTION ALLOCATES AN ARRAY OF TOKENS, WHICH THE
 * CALLER MUST DEALLOCATE WHEN IT IS NO LONGER NEEDED.
 * 
 **************************************************************************/
{
  Char   * firstWord, * rest, * text;
  VOCENT * tokens = NULL;

  *numTokens = 0;

  /* Allocate a string for the transcription */
  if ((rest = text = strdup(transcription))) {

    firstWord = GetNextWord(&rest);
    tokens = VOCAB_tokenize(vocab, firstWord, rest, numTokens);
    if (tokens)
      vocab->status = VISESUCCESS;

    free(text);

  } else {
    vocab->status = OUTOFMEMORY;
  }

  return(tokens);
}


static VOCENT *
VOCAB_tokenize(VOCAB vocab, Char * word, Char * rest, UInt * index)
/***************************************************************************
 *
 * If word is in the vocabulary and rest is tokenizable, this function
 * returns the tokenization of the concatenation of word and rest as an
 * array of VOCENTs, starting at index, and reporting in *index the total
 * number of tokens found; otherwise, it returns NULL, reports zero in
 * *index, and leaves rest in the same state it found it in on entry.
 * THIS FUNCTION ALLOCATES AN ARRAY OF TOKENS, WHICH THE CALLER MUST
 * DEALLOCATE WHEN IT IS NO LONGER NEEDED.
 *
 ***************************************************************************/
{
  VOCENT   entry = VOCAB_entryFromName(vocab, word);
  Char   * nextWord = rest ? GetNextWord(&rest) : NULL;
  VOCENT * tokens = NULL;
  UInt     thisIndex = (*index)++;
  Char   * terminator;

   /* If word is a token and rest is a tokenizable string, insert word's VOCENT into the array of tokens at this index */
  if (entry != NULL && (nextWord == NULL || (tokens = VOCAB_tokenize(vocab, nextWord, rest, index)))) {
     /* Allocate the array of tokens if necessary; this happens only when nextWord is NULL */
    if (!tokens && (!(tokens = (VOCENT *) calloc((size_t) *index, sizeof(VOCENT)))))
      vocab->status = OUTOFMEMORY;
    else
      tokens[thisIndex] = entry;

   /* Otherwise, if there is a next word, concatenate it with word and try to tokenize the result */
  } else if (nextWord != NULL) {
     /* Remove the terminator between word and nextWord */
    terminator = word + strlen(word);
    *terminator = ' ';
    *index = thisIndex;
    tokens = VOCAB_tokenize(vocab, word, rest, index);
     /* If unsuccessful, restore the terminator between word and nextWord */
    if (!tokens)
      *terminator = '\0';
  }

   /* If unsuccessful, remove the terminator separating nextWord from rest and report that no tokens were found */
  if (!tokens) {
    if (nextWord && rest) nextWord[strlen(nextWord)] = ' ';
    *index = 0;
  }

  return(tokens);
}


VOCITER
VOCITER_create(VOCAB vocab)
/***************************************************************************
 *
 * Returns an iterator to use to read successive wordIds from a vocabulary,
 * or NULL if the vocabulary is empty.
 *
 ***************************************************************************/
{
  VOCITER  vociter = NULL;

  if (vocab && vocab->model) {
    if (vocab->model && vocab->minWId && vocab->maxWId && (vociter = (VOCITER) malloc(sizeof(vociter_t)))) {
      vociter->vocab = vocab;
      vociter->wordId = vocab->minWId - 1;
    }
  }

  return(vociter);
}


void
VOCITER_destroy(VOCITER vociter)
/***************************************************************************
 *
 * Destroys a vocabulary iterator
 *
 ***************************************************************************/
{
  VBX_free(vociter);
}


UInt
VOCITER_nextWordId(VOCITER vociter)
/***************************************************************************
 *
 * Returns the Id of the next word in the vocabulary.
 * Returns zero when the words are exhausted.
 *
 ***************************************************************************/
{
  if (vociter) {
    while (++vociter->wordId <= vociter->vocab->maxWId)
       /* If this wid has no corresponding vocabulary entry, skip it */
      if (vociter->vocab->model[vociter->wordId - vociter->vocab->minWId].vocent)
        return(vociter->wordId);
  }

  return(0);
}
