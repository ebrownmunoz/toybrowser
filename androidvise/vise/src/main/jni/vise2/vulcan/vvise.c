/* vvise.c - VISE Interface Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Interface to the VISE Search Engine
 * 
 * HISTORY
 * 12-Aug-96  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* #define NDEBUG */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "types.h"
#include "vbx.h"
#include "primes.h"
#include "utils.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "hyp.h"
#include "models.h"
#include "spwrds.h"
#include "sn.h"
#include "vocab.h"
#include "vvise.h"

#include "ifaces.h"
#include "imail.h"
#include "imemory.h"
#include "se.h"
#include "vise.h"
#include "visecmds.h"
#include "v_vise.h"
#include "v_fyidef.h"
#include "visecomp.h"
#include "viseerr.h"
#include "viseparm.h"
#include "visemsg.h"

#ifdef DEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

#define TRAIN_DELTA  10

/* Define array FYI_parmTxt of ptrs to FYI parameter text */
FYI_DEFPARMTXT;

/* VVISE Descriptor */
typedef struct vvise_s {
  VISE        vise;
  IMemory   * memory;
  IMail     * mail;
  IMBox     * mailbox;
  V_Uns       myMBoxId;
  V_Uns       seMBoxId;
  VBXMSG      message;
  pthread_t   thread;
  V_Err       status;
  Bool        textMode;
} vvise_t;

/* Private VVISE methods */
static Bool   VVISE_Run(VVISE vvise, void (* function)(SE), V_Err * status);
static Bool   VVISE_DownloadModel(VVISE vvise, UInt oldWid, UInt wid, UInt class, FUPATE * pattern, Bool loadTemplates);
static Bool   VVISE_UploadModel(VVISE vvise, UInt wid, Bool uploadDwells, FUPATE * pattern);
static Bool   VVISE_AdjustModel(VVISE vvise, UInt wid, UInt weight, Bool adjustDwells);
static Bool   VVISE_GetRecognitionStats(VBXMSG message, Bool verbose);
static HYP    VVISE_GetRecognitionResults(VBXMSG message, UInt nBest, UInt * hypCnt);
static V_Err  VVISE_verifyAck(VBXMSG message, V_Int command, V_Int cmdExpected);
static void   VVISE_fillTextModel(UInt class, FUPATE * textModel, FUPATE * voiceModel);

VVISE
VVISE_create(IMemory * memory, IMail * mail)
/***************************************************************************
 *
 * Instantiate Virtual VISE
 *
 ***************************************************************************/
{
  VVISE  vvise;
  V_Err  status;

  if (  /* Instantiate VVISE */
      (vvise = (VVISE) memory->Calloc(memory, 1, sizeof(vvise_t), FAST_MEMORY)) &&
        /* Get IMemory */
      (vvise->memory = memory) &&
        /* Get IMail */
      (vvise->mail = mail) &&
        /* Create a mailbox for VVISE */
      (vvise->mailbox = mail->CreateMBox(mail, VISECOMP_RC, NUMPRIORITIES, &status)) &&
        /* Get VVISE's mailbox identifier */
      (vvise->myMBoxId = mail->MailBoxId(mail, VISECOMP_RC, &status)) &&
        /* Create a message port for VVISE */
      (vvise->message = MSG_create(memory, vvise->mailbox)) &&
        /* Instantiate the VISE Search Engine */
      (vvise->vise = SE_create(mail, memory, &status)) &&
        /* Run the search engine */
      (VVISE_Run(vvise, SE_run, &status))
     ) {
    VBX_DEBUG(VBX_print("  VVISE_create: succeeded\n"));
  } else {
    VBX_DEBUG(VBX_print("  VVISE_create: failed (status = %s)\n", VISEERR_string(status)));
    VVISE_destroy(vvise);
    vvise = NULL;
  }

  return vvise;
}


static Bool
VVISE_Run(VVISE vvise, void (* function)(SE), V_Err * status)
/***************************************************************************
 *
 * Create a thread and run an instance of the Search Engine in it
 *
 ***************************************************************************/
{
  Int   threadStatus = pthread_create(&vvise->thread, NULL, (pthread_startroutine_t) function, (void *) vvise->vise);

  vvise->status = VISESUCCESS;
  if (!threadStatus) {
    VBX_DEBUG(VBX_print("  VVISE_Run: created thread for %s\n", VISECOMP_SE, threadStatus, errno));
    if (!(vvise->seMBoxId) && !(vvise->seMBoxId = vvise->mail->MailBoxId(vvise->mail, VISECOMP_SE, &vvise->status))) {
      VBX_DEBUG(VBX_print("  VVISE_Run: cannot get ID of SE mailbox (*status = %s)\n", VISEERR_string(vvise->status)));
    } else if (!VVISE_init(vvise)) {
      VBX_DEBUG(VBX_print("  VVISE_Run: cannot initialize the Search Engine (*status = %s)\n", VISEERR_string(vvise->status)));
    }
  } else {
    switch (threadStatus) {
      case EAGAIN: vvise->status = SYSLIMITREACHED; break;
      case EINVAL: vvise->status = INVALIDARGUMENT; break;
      case ENOMEM: vvise->status = OUTOFMEMORY; break;
      case EPERM:  vvise->status = NOPERMISSION; break;
      default:     vvise->status = UNKNOWNERROR; break;
    }
  }

  *status = vvise->status;
  return(vvise->status == VISESUCCESS);
}


void
VVISE_destroy(VVISE vvise)
/***************************************************************************
 *
 * Destroy an instance of virtual VISE
 *
 ***************************************************************************/
{
  void  *status;

  if (vvise) {
    SE_destroy(vvise->vise);
    /* Wait for the SE thread to exit */
    if (vvise->thread) pthread_join(vvise->thread, &status);
    if (vvise->message) MSG_destroy(vvise->message);
    if (vvise->mailbox) IMBOX_Destroy(vvise->mailbox);
    vvise->memory->Free(vvise->memory, vvise);
  }
}


V_Err
VVISE_status(VVISE vvise)
/***************************************************************************
 *
 * Return the current status of an instance of virtual VISE
 *
 ***************************************************************************/
{
  return(vvise ? vvise->status : NOSUCHOBJECT);
}


Bool
VVISE_abort(VVISE vvise, VBXMSG message)
/***************************************************************************
 *
 * Abort the current VISE operation.  If the message argument is NULL,
 * the message is sent and acknowledged through VVISE's message port;
 * otherwise, it is sent and acknowledged through the specified port.
 *
 ***************************************************************************/
{
  V_Uns    sender;
  V_ULong  length;
  V_Int    command;
  V_Uns    cstatus;
  V_Err    status;

  if (vvise) {
    if (!message) message = vvise->message;
    if (MSG_openWrite(message, vvise->seMBoxId, 0, _ABORT, ABORT_PRIORITY, &status) &&
        MSG_closeWrite(message, &status) &&
        MSG_openRead(message, &sender, &length, &command, TRUE, &status)) {
      MSG_readInt(message, &command);
      MSG_readUns(message, &cstatus);
      MSG_closeRead(message, &status);
    }
    VBX_DEBUG(VBX_print("  VISE_abort: status = %s\n", VISEERR_string(status)));
  }

  return(vvise && command == _ABORT && (status == VISESUCCESS || status == UNTIMELYCOMMAND) && cstatus == VISESUCCESS);
}


Bool
VVISE_listen(VVISE vvise, ENRUTT utt, UInt maxFrames, Bool live, Bool findWord, Int * volumePtr)
/***************************************************************************
 *
 * Acquire data from the startSN to the endSN, reporting the starting and
 * ending sequence numbers and the volume of any utterance in that range.
 *
 ***************************************************************************/
{
  V_Int   volume;
  V_SN    wordStartSN;
  V_SN    wordEndSN;
  V_ULong numFrames;
  V_Uns   senderId;
  V_ULong length;
  V_Int   command;
  V_Err   vstatus, cstatus;

  length = (V_ULong) (2 * VINTSIZEINMELS + 2 * VSNSIZEINMELS + VLONGSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _LISTEN, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, (V_Uns) live);
    MSG_writeUns(vvise->message, (V_Uns) findWord);       /* Don't try to find a word */
    MSG_writeSN(vvise->message, (V_SN) (utt->startSN));   /* Starting sequence number */
    MSG_writeSN(vvise->message, (V_SN) (utt->endSN));     /* Ending sequence number   */
    MSG_writeULong(vvise->message, (V_ULong) maxFrames);  /* Maximum number of frames */
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _LISTEN);
      if ((vstatus == VISESUCCESS) &&
          MSG_readInt(vvise->message, &volume) &&
          MSG_readSN(vvise->message, &wordStartSN) &&
          MSG_readSN(vvise->message, &wordEndSN) &&
          MSG_readULong(vvise->message, &numFrames)) {
        utt->wordStartSN = (SN) wordStartSN;
        utt->wordEndSN = (SN) wordEndSN;
        utt->wordFrameCnt = (UInt) numFrames;
        if (volumePtr) *volumePtr = (Int) volume;
      } else {
        utt->wordStartSN  = (SN) 0;
        utt->wordEndSN = (SN) 0;
        utt->wordFrameCnt = (UInt) 0;
        if (volumePtr) *volumePtr = (Int) 0;
      }
      MSG_closeRead(vvise->message, &cstatus);
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


HYP
VVISE_recognize(VVISE vvise, UInt grammarId, SN startSN, SN endSN, UInt maxFrames, Bool live, UInt nBest, Bool verbose)
/***************************************************************************
 *
 * Recognize, returning the recognition hypotheses.  THIS FUNCTION ALLOCATES
 * AN ARRAY OF HYPOTHESES, WHICH THE CALLER MUST DEALLOCATE WHEN IT IS
 * NO LONGER NEEDED.  Reports a status of VISESUCCESS only if the
 * recognition event is fully realized.
 *
 ***************************************************************************/
{
  HYP     hyp = NULL;
  UInt    hypCnt = 0;
  V_Uns   senderId;
  V_ULong length;
  V_Int   command;
  V_Err   vstatus, cstatus;

  length = (V_ULong) (3 * VINTSIZEINMELS + 2 * VSNSIZEINMELS + VLONGSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _RECOGNIZE, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, (V_Uns) grammarId);      /* The id of the grammar to use */
    MSG_writeUns(vvise->message, (V_Uns) live);           /* Source is live? */
    MSG_writeSN(vvise->message, (V_SN) startSN);          /* Starting sequence number */
    MSG_writeSN(vvise->message, (V_SN) endSN);            /* Ending sequence number */
    MSG_writeULong(vvise->message, (V_ULong) maxFrames);  /* Maximum number of frames */
    MSG_writeUns(vvise->message, (V_Uns) nBest);          /* Number of hypotheses */
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _RECOGNIZE);
      if (verbose) VBX_print("  VVISE_recognize: status = %s\n", VISEERR_string(vstatus));
      if (vstatus != UNTIMELYCOMMAND && vstatus != MESSAGETOOSHORT &&
          VVISE_GetRecognitionStats(vvise->message, verbose) &&
          (hyp = VVISE_GetRecognitionResults(vvise->message, nBest, &hypCnt)))
        if (verbose) VBX_print("  VVISE_recognize: %d results found\n", hypCnt);
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_recognize: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
      if (!vstatus) vstatus = cstatus;
    }
  }

  vvise->status = (Int) vstatus;
  return(hyp);
}


static HYP
VVISE_GetRecognitionResults(VBXMSG message, UInt nBest, UInt * hypCount)
/**************************************************************************
 *
 * Get the recognition hypotheses from VISE, returning the number
 * of hypotheses actually acquired.  THIS FUNCTION ALLOCATES AN ARRAY OF
 * HYPOTHESES, WHICH THE CALLER MUST EVENTUALLY DEALLOCATE.
 * 
 **************************************************************************/
{
  HYP      hyp;
  HYP      thisHyp;
  V_Uns    nWords;
  WRDHYP   word;
  V_Uns    wid;
  V_Uns    numFrames;
  V_SN     startSN;
  V_SN     endSN;
  Long     totalScore;
  V_Int    score;
  V_Int    globalMin;
  V_Int    incremCost;

  /* Allocate hypotheses for the nBest results */
  if ((thisHyp = hyp = HYP_create(nBest))) {

    /* Try to get the nBest hypotheses */
    for (*hypCount = 0; *hypCount < nBest; (*hypCount)++) {

      /* Try to get the number of words in the hypothesis */
      if (MSG_readUns(message, &nWords) && nWords) {

        totalScore = (Long) UNITSCORE;

        /* Allocate an array of recognition word hypotheses */
        if (HYP_setWordCount(thisHyp + *hypCount, (UInt) nWords)) {

          word = HYP_words(thisHyp + *hypCount, NULL);

          /* Put all the words into the hypothesis */
          while (nWords--) {
            if (MSG_readUns(message, &wid) &&
                MSG_readUns(message, &numFrames) &&
                MSG_readSN(message, &startSN) &&
                MSG_readSN(message, &endSN) &&
                MSG_readInt(message, &score) &&
                MSG_readInt(message, &globalMin)) {
              WRDHYP_setWId(word, (UInt) wid);
              WRDHYP_setNumFrames(word, (UInt) numFrames);
              WRDHYP_setStartSN(word, (SN) startSN);
              WRDHYP_setEndSN(word, (SN) endSN);
              WRDHYP_setScore(word, (Int) score);
              WRDHYP_setGlobalMin(word, (Int) globalMin);
              totalScore += (Long) numFrames * (Long) score;
            } else {
              HYP_setWordCount(thisHyp + *hypCount, 0);
              break;
            }
            word++;
          }
        }

        HYP_setTotalScore(thisHyp + *hypCount, totalScore);
        MSG_readInt(message, &incremCost);
      } else {
        break;
      }
    }
    /* Read the terminating zero if all nBest hypotheses were read */
    if (*hypCount == nBest)
      MSG_readUns(message, &nWords);
  }

  return(hyp);
}


Bool
VVISE_trainVocabulary(VVISE vvise, HYP hyp, VOCAB vocab, UInt weight)
/***************************************************************************
 *
 * Train the words in the hypothesis which belong to the given vocabulary
 *
 ***************************************************************************/
{
  WRDHYP     word;
  UInt       wordCount;
  Bool       result = TRUE;

  vvise->status = (Int) VISESUCCESS;
  for (word = HYP_words(hyp, &wordCount); wordCount--; word++) {
    result = result && VVISE_trainWord(vvise, WRDHYP_wid(word), vocab, WRDHYP_startSN(word), WRDHYP_endSN(word), weight, WRDHYP_score(word), TRUE);
    if (vvise->status == OUTOFMEMORY) break;
  }

  return(result);
}


Bool
VVISE_trainWord(VVISE vvise, UInt wid, VOCAB vocab, SN startSN, SN endSN, UInt weight, UInt score, Bool verbose)
/***************************************************************************
 *
 * Train the given word
 *
 ***************************************************************************/
{
  FUPATE    model;
  V_Err     vstatus = VISESUCCESS;
  V_Err     cstatus;
  V_Int     command;
  V_Uns     senderId;
  V_ULong   length;
  V_Int     cmdEcho;

  /* If the word is in vocab, train it */
  if (VOCAB_getModel(vocab, wid, &model)) {

    VBX_DEBUG(VBX_print("  VVISE_trainWord: Training \"%s\" (wid = %u)\n", VOCAB_wordNameFromId(vocab, wid), wid));

    /* If the word has more than one kernel, use TRAINWORD; otherwise, use SEGMENTWORD */
    command = model.bNKernWordMod > 1 ? _TRAINWORD : _SEGMENTWORD;
    length = (V_ULong) (command == _TRAINWORD ? 3 * VINTSIZEINMELS + 2 * VSNSIZEINMELS : 2 * VINTSIZEINMELS + 2 * VSNSIZEINMELS);
    if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, command, COMMAND_PRIORITY, &vstatus)) {
      MSG_writeUns(vvise->message, (V_Uns) wid);     /* Id of word to train */
      MSG_writeSN(vvise->message, (V_SN) startSN);   /* Starting sequence number */
      MSG_writeSN(vvise->message, (V_SN) endSN);     /* Ending sequence number */
      MSG_writeUns(vvise->message, (V_Uns) weight);  /* Weight of old versus new data */
      if (command == _TRAINWORD)
        MSG_writeUns(vvise->message, (V_Uns) (score + TRAIN_DELTA)); /* maxScore */
      if (MSG_closeWrite(vvise->message, &vstatus) &&
          MSG_openRead(vvise->message, &senderId, &length, &cmdEcho, TRUE, &vstatus)) {
        vstatus = VVISE_verifyAck(vvise->message, cmdEcho, command);
        if (vstatus == VISESUCCESS) {
          if (score) {
            /* Update training count and sums in individual models */
            model.lSum += score; 
            model.wCount++;
            model.wTrainingCount = model.wCount;
            VOCAB_putModel(vocab, wid, &model);
            /* Update wildcard training counts and sums */
            (void) MODELS_setTraining(VOCAB_models(vocab), score, score * score, 1, FALSE);
          }
        } else if (verbose) {
          if (vstatus == BADSCORE)
            VBX_print("  VVISE_trainWord: BADSCORE on \"%s\" (score = %d)\n", VOCAB_wordNameFromId(vocab, wid), score);
          else
            VBX_print("  VVISE_trainWord: ABNORMAL training on \"%s\" (status = %s)\n", VOCAB_wordNameFromId(vocab, wid), VISEERR_string(vstatus));
        }
        /* Ignore any closing error */
        MSG_closeRead(vvise->message, &cstatus);
        VBX_DEBUG(if (cstatus) VBX_print("  VVISE_trainWord: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
      }
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_enrollWord(VVISE vvise, VOCAB vocab, UInt frPerKernel, UInt maxKernels, UInt minKernels, UInt nModels, UInt * wids, UInt nUtts, ENRUTT utts)
/***************************************************************************
 *
 * Enroll the given word
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  UInt     uttCount;
  UInt     wrdCount;
  UInt     class = PRIMES_nth(VOCENT_class(VOCAB_entryFromId(vocab, wids[0])));
  V_Err    vstatus, cstatus;


  length = (V_ULong) ((6 + nModels) * VINTSIZEINMELS + nUtts * 4 * VSNSIZEINMELS);
  VBX_DEBUG(VBX_print("VVISE_enrollWord: fpk %d maxk %d mink %d cls %d nmod %d\n",
            frPerKernel, maxKernels, minKernels, class, nModels));
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _ENROLLWORD, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, (V_Uns) frPerKernel);
    MSG_writeUns(vvise->message, (V_Uns) maxKernels);
    MSG_writeUns(vvise->message, (V_Uns) minKernels);
    MSG_writeUns(vvise->message, (V_Uns) class);
    MSG_writeUns(vvise->message, (V_Uns) nModels);
    wrdCount = 0;
    while (wrdCount < nModels && MSG_writeUns(vvise->message, (V_Uns) wids[wrdCount]))
      wrdCount++;
    VBX_DEBUG(if (wrdCount != nModels) VBX_print("  VVISE_enrollWord: wrote only %u of %u wordIds requested\n", wrdCount, nModels));
    MSG_writeUns(vvise->message, (V_Uns) nUtts);
    uttCount = 0;
    while (uttCount < nUtts &&
           MSG_writeSN(vvise->message, utts[uttCount].startSN) &&
           MSG_writeSN(vvise->message, utts[uttCount].wordStartSN) &&
           MSG_writeSN(vvise->message, utts[uttCount].wordEndSN) &&
           MSG_writeSN(vvise->message, utts[uttCount].endSN))
      uttCount++;
    VBX_DEBUG(if (uttCount != nUtts) VBX_print("  VVISE_enrollWord: wrote only %u of %u utterances requested\n", uttCount, nUtts));
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _ENROLLWORD);
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_enrollWord: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_downloadForcingGrammar(VVISE vvise, HYP hyp, UInt forcingGrammarId, VOCAB vocab)
/**************************************************************************
 *
 * Create and download a forcing grammar for the hyp
 * 
 **************************************************************************/
{
  WRDHYP   words;
  VOCENT   vocent;
  UInt     numHypWords, numWords, numModels, wordIndex, numArcs, numNodes, numWInst;
  UShort   nodeID, wid;
  UShort   nWrdsNInit;
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  words = HYP_words(hyp, &numHypWords);
  if (!words)
    return(FALSE);

  /* Count the modelled words and the word models */
  numWords = 0;
  numModels = 0;
  for (wordIndex = 0; wordIndex < numHypWords; wordIndex++)
    if ((vocent = VOCAB_entryFromId(vocab, WRDHYP_wid(words + wordIndex)))) {
      numWords++;
      numModels += VOCENT_numModels(vocent);
    }

  numArcs = 2 * numWords + 3;
  numNodes = numWords + 4;
  numWInst = numWords + numModels + 2;

  length = (V_ULong) ((3 + 3 * numArcs + numWInst) * VINTSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _DLOADGRAMMAR, COMMAND_PRIORITY, &vstatus)) {
    /* Put the grammarwide parameters into the message */
    MSG_writeUns(vvise->message, (V_Uns) forcingGrammarId);
    MSG_writeUns(vvise->message, (V_Uns) numArcs);
    MSG_writeUns(vvise->message, (V_Uns) numNodes);

    /* Insert a null arc to bypass the joker node (node 1) */
    MSG_writeUns(vvise->message, (V_Uns) 0);
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) 0);

    /* Insert a silence loop on the start node to improve framing */
    MSG_writeUns(vvise->message, 0);
    MSG_writeUns(vvise->message, 0);
    MSG_writeUns(vvise->message, (V_Uns) 1);
    MSG_writeUns(vvise->message, (V_Uns) FIRST_MULTISILENCE);

    /* Insert the modelled words and silence loops into the message */
    nodeID = 2;
    for (wordIndex = 0; wordIndex < numHypWords; wordIndex++) {
      if ((vocent = VOCAB_entryFromId(vocab, WRDHYP_wid(words + wordIndex)))) {
        /* Insert the short silence loop parameters */
        MSG_writeUns(vvise->message, nodeID);
        MSG_writeUns(vvise->message, nodeID);
        MSG_writeUns(vvise->message, (V_Uns) 1);
        MSG_writeUns(vvise->message, (V_Uns) SHORT_SILENCE);
        /* Insert the IDs for all models of the next word */
        MSG_writeUns(vvise->message, nodeID++);
        MSG_writeUns(vvise->message, nodeID);
        MSG_writeUns(vvise->message, (V_Uns) VOCENT_numModels(vocent));
        wid = (UShort) VOCENT_firstWordId(vocent);
        assert(VOCENT_numModels(vocent) && wid);
        while (wid) {
          MSG_writeUns(vvise->message, wid);
          wid = (UShort) VOCENT_nextWordId(vocent);
        }
      }
    }

    /* Insert the final long silence into the message */
    MSG_writeUns(vvise->message, nodeID);
    MSG_writeUns(vvise->message, ++nodeID);
    MSG_writeUns(vvise->message, (V_Uns) 1);
    MSG_writeUns(vvise->message, (V_Uns) LONG_SILENCE);

    assert((UInt) nodeID == numNodes - 1);

    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _DLOADGRAMMAR);
      if (MSG_readUns(vvise->message, &nWrdsNInit) && nWrdsNInit && vstatus == VISESUCCESS) {
        VBX_DEBUG(VBX_print("%d uninitialized words in the forcing grammar (#%d)\n", nWrdsNInit, (Int) forcingGrammarId));
        vstatus = UNINITIALIZEDWORDS;
      }
      /* Ignore any closing error */
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_downloadForcingGrammar: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_downloadMultiSilenceTrainingGrammar(VVISE vvise, UInt grammarId)
/**************************************************************************
 *
 * Create and download a "multi-silences" grammar
 * 
 **************************************************************************/
{
  UInt     wordIndex, numArcs, numNodes, numWInst;
  UShort   nWrdsNInit;
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  numArcs = 3;
  numNodes = 4;
  numWInst = NUM_MULTISILENCES + 1;

  length = (V_ULong) ((3 + 3 * numArcs + numWInst) * VINTSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _DLOADGRAMMAR, COMMAND_PRIORITY, &vstatus)) {
    /* Put the grammarwide parameters into the message */
    MSG_writeUns(vvise->message, (V_Uns) grammarId);
    MSG_writeUns(vvise->message, (V_Uns) numArcs);
    MSG_writeUns(vvise->message, (V_Uns) numNodes);

    /* Insert a null arc to bypass the joker node (node 1) */
    MSG_writeUns(vvise->message, (V_Uns) 0);
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) 0);

    /* Insert all the multi-silences on a loop on node 2 */
    MSG_writeUns(vvise->message, 2);
    MSG_writeUns(vvise->message, 2);
    MSG_writeUns(vvise->message, (V_Uns) NUM_MULTISILENCES);
    for (wordIndex = 0; wordIndex < NUM_MULTISILENCES; wordIndex++)
      MSG_writeUns(vvise->message, (V_Uns) (FIRST_MULTISILENCE + wordIndex));

    /* Insert the final wildcard into the message */
    MSG_writeUns(vvise->message, 2);
    MSG_writeUns(vvise->message, 3);
    MSG_writeUns(vvise->message, (V_Uns) 1);
    MSG_writeUns(vvise->message, (V_Uns) TRAINMULTIWC_WID);

    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _DLOADGRAMMAR);
      if (MSG_readUns(vvise->message, &nWrdsNInit) && nWrdsNInit && vstatus == VISESUCCESS) {
        VBX_DEBUG(VBX_print("%d uninitialized words in the forcing grammar (#%d)\n", nWrdsNInit, (Int) grammarId));
        vstatus = UNINITIALIZEDWORDS;
      }
      /* Ignore any closing error */
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_downloadMultiSilenceTrainingGrammar: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_downloadEnrollmentGrammar(VVISE vvise, UInt grammarId, UInt initialWCCount)
/**************************************************************************
 *
 * Create and download an enrollment grammar
 * 
 **************************************************************************/
{
  UInt     nodeIndex, numArcs, numNodes, numWInst;
  UShort   nWrdsNInit;
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  numArcs = initialWCCount + 4;
  numNodes = numArcs + 1;
  numWInst = numArcs + 1;

  length = (V_ULong) ((3 + 3 * numArcs + numWInst) * VINTSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _DLOADGRAMMAR, COMMAND_PRIORITY, &vstatus)) {
    /* Put the grammarwide parameters into the message */
    MSG_writeUns(vvise->message, (V_Uns) grammarId);
    MSG_writeUns(vvise->message, (V_Uns) numArcs);
    MSG_writeUns(vvise->message, (V_Uns) numNodes);

    /* Insert a null arc to bypass the joker node (node 1) */
    MSG_writeUns(vvise->message, (V_Uns) 0);
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) 0);

    /* Insert the short silences on a loop on node 2 */
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) SHORT_SILENCE);
    MSG_writeUns(vvise->message, (V_Uns) SHORT_ADAPTIVE_SIL);

    /* Insert occurences of the initial enrollment wildcard in line */
    nodeIndex = 2;
    while (nodeIndex < initialWCCount + 2) {
      MSG_writeUns(vvise->message, (V_Uns) nodeIndex++);
      MSG_writeUns(vvise->message, (V_Uns) nodeIndex);
      MSG_writeUns(vvise->message, (V_Uns) 1);
      MSG_writeUns(vvise->message, (V_Uns) ENROLLINITWC_WID);
    }

    /* Insert the main enrollment wildcard in line */
    MSG_writeUns(vvise->message, (V_Uns) nodeIndex++);
    MSG_writeUns(vvise->message, (V_Uns) nodeIndex);
    MSG_writeUns(vvise->message, (V_Uns) 1);
    MSG_writeUns(vvise->message, (V_Uns) ENROLLBODYWC_WID);

    /* Insert the final long silences in line */
    MSG_writeUns(vvise->message, (V_Uns) nodeIndex++);
    MSG_writeUns(vvise->message, (V_Uns) nodeIndex);
    MSG_writeUns(vvise->message, (V_Uns) 2);
    MSG_writeUns(vvise->message, (V_Uns) LONG_SILENCE);
    MSG_writeUns(vvise->message, (V_Uns) LONG_ADAPTIVE_SIL);

    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _DLOADGRAMMAR);
      if (MSG_readUns(vvise->message, &nWrdsNInit) && nWrdsNInit && vstatus == VISESUCCESS) {
        VBX_DEBUG(VBX_print("%d uninitialized words in the enrollment grammar (#%d)\n", nWrdsNInit, (Int) grammarId));
        vstatus = UNINITIALIZEDWORDS;
      }
      /* Ignore any closing error */
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_downloadEnrollmentGrammar: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_deleteGrammar(VVISE vvise, UInt grammarId)
/**************************************************************************
 *
 * Delete the specified grammar
 * 
 **************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) VINTSIZEINMELS, _DELGRAMMAR, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, (V_Uns) grammarId);
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _DELGRAMMAR);
      /* Ignore any closing error */
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_deleteGrammar: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  vvise->status = (Int) vstatus;
  if (vvise->status != VISESUCCESS)
    VBX_DEBUG(VBX_print("  VVISE_deleteGrammar: WARNING: can't delete grammarId %d status = %s\n", grammarId, VISEERR_string(vvise->status)));
  else
    VBX_DEBUG(VBX_print("  VVISE_deleteGrammar: SUCCEEDED for grammarId %d\n", grammarId));
  return(vstatus == VISESUCCESS);
}


UInt
VVISE_reinitializeModels(VVISE vvise, VOCAB vocab, Bool firstPass)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reinitializes VISE's models for all words in the vocabulary and zeroes the
 * training counts and sums and wildcard parameters in the model set.  Returns
 * the number of models initialized.  Call before each training pass.
 *---------------------------------------------------------------------------*/
{
  UInt     wid, numInitialized = 0;
  FUPATE   model;
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus = VISESUCCESS;
  V_Err    cstatus;

  MODELS_setTraining(VOCAB_models(vocab), 0, 0, 0, TRUE);
  for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab)) {
    if (VOCAB_getModel(vocab, wid, &model)) {
      if (firstPass)
        model.lSquares = model.lSum;     /* save the old sum of scores for word */
      model.lSum = 0;                    /* ini the new sum of scores for word */
      model.wCount = 0;                  /* weighted training count */
      if (firstPass)
        model.wTrainingSavedCount = model.wTrainingCount; /* save the old training count for word */
      model.wTrainingCount = 0;          /* # of times word has been trained */
      VOCAB_putModel(vocab, wid, &model);
      if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) VINTSIZEINMELS, _STARTTRAININGWORD, COMMAND_PRIORITY, &vstatus)) {
        MSG_writeUns(vvise->message, (V_Uns) wid);
        if (MSG_closeWrite(vvise->message, &vstatus) &&
            MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
          vstatus = VVISE_verifyAck(vvise->message, command, _STARTTRAININGWORD);
          if (vstatus == VISESUCCESS) numInitialized++;
          /* Ignore any closing error */
          MSG_closeRead(vvise->message, &cstatus);
          VBX_DEBUG(if (cstatus) VBX_print("  VVISE_reinitializeModels: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
        } else break;
      } else break;
    }
  }

  vvise->status = (Int) vstatus;
  return(numInitialized);
}


UInt
VVISE_downloadVocab(VVISE vvise, VOCAB vocab, Bool initializeDwells, Bool verbose)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Download the models and dwells for every model in the vocabulary.
 * Returns the number of models downloaded.
 *---------------------------------------------------------------------------*/
{
  UInt   * primes = NULL;
  UInt     numPrimes;
  UInt     wid;
  UInt     class;
  FUPATE   model;
  UInt     numDownloaded = 0;

  vvise->status = VISESUCCESS;
  if (verbose) VBX_print("  Downloading models\n   WordName       WId  Class\n");
  if ((primes = PRIMES_firstN(VOCAB_numWordIds(vocab), &numPrimes)) && numPrimes >= VOCAB_numWordIds(vocab)) {
    for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab)) {
      if (VOCAB_getModel(vocab, wid, &model)) {
        class = primes[VOCENT_class(VOCAB_entryFromId(vocab, wid))];
        if (initializeDwells)
          MODEL_initializeDwells(&model);
        if (VVISE_DownloadModel(vvise, wid, wid, class, &model, TRUE)) {
          if (verbose) VBX_print("   %-12.12s %5u %6u\n", VOCAB_wordNameFromId(vocab, wid), wid, class);
          numDownloaded++;
        } else {
          if (vvise->status == OUTOFMEMORY) break;
        }
      }
    }
  } else {
    vvise->status = OUTOFMEMORY;
  }
  if (primes) free(primes);
  if (verbose) VBX_print("  %u models downloaded\n", numDownloaded);

  return(numDownloaded);
}


UInt
VVISE_downloadTranscribedModels(VVISE vvise, VOCAB vocab, Char * transcription, Bool initializeDwells, Bool verbose)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Download the models and perhaps dwells for every word in the transcription
 * Returns the number of models downloaded.
 *---------------------------------------------------------------------------*/
{
  VOCENT * tokens;
  UInt     numWords, wordIndex;
  UInt   * primes = NULL;
  UInt     numPrimes;
  UInt     wid;
  UInt     class;
  FUPATE   model;
  UInt     numDownloaded = 0;

  vvise->status = VISESUCCESS;
  if ((tokens = VOCAB_tokens(vocab, transcription, &numWords))) {
    if (verbose) VBX_print("  Downloading models\n   WordName       WId  Class\n");
    if ((primes = PRIMES_firstN(VOCAB_numWordIds(vocab), &numPrimes)) &&
         numPrimes >= VOCAB_numWordIds(vocab)) {
      for (wordIndex = 0; wordIndex < numWords; wordIndex++) {
        for (wid = VOCENT_firstWordId(tokens[wordIndex]); wid; wid = VOCENT_nextWordId(tokens[wordIndex])) {
          if (VOCAB_getModel(vocab, wid, &model)) {
            class = primes[VOCENT_class(VOCAB_entryFromId(vocab, wid))];
            if (initializeDwells)
              MODEL_initializeDwells(&model);
            if (VVISE_DownloadModel(vvise, wid, wid, class, &model, TRUE)) {
              if (verbose) VBX_print("   %-12.12s %5u %6u\n", VOCAB_wordNameFromId(vocab, wid), wid, class);
              numDownloaded++;
            } else {
              if (vvise->status == OUTOFMEMORY) break;
            }
          }
        }
        if (vvise->status == OUTOFMEMORY) break;
      }
      free(primes);
    } else {
      vvise->status = OUTOFMEMORY;
    }
    free(tokens);
  } else {
    vvise->status = OUTOFMEMORY;
  }

  if (verbose) VBX_print("  %u models downloaded\n", numDownloaded);

  return(numDownloaded);
}


Bool
VVISE_downloadWord(VVISE vvise, VOCAB vocab, UInt oldWid, UInt newWid)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Download a word model WITHOUT TEMPLATES, optionally replacing a previously
 * downloaded word model if oldWid is non-zero.  Returns TRUE iff successful.
 *---------------------------------------------------------------------------*/
{
  FUPATE  model;
  UInt    class = PRIMES_nth(VOCENT_class(VOCAB_entryFromId(vocab, newWid)));
  
  vvise->status = MISSINGWORDMODEL;
  return(VOCAB_getModel(vocab, newWid, &model) && VVISE_DownloadModel(vvise, oldWid, newWid, class, &model, FALSE));
}


Bool
VVISE_downloadModel(VVISE vvise, VOCAB vocab, UInt oldWid, UInt newWid)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Download a word model, optionally replacing a previously downloaded word
 * model if oldWid is non-zero.  Returns TRUE if successful; FALSE otherwise.
 *---------------------------------------------------------------------------*/
{
  FUPATE  model;
  UInt    class = PRIMES_nth(VOCENT_class(VOCAB_entryFromId(vocab, newWid)));
  
  vvise->status = MISSINGWORDMODEL;
  return(VOCAB_getModel(vocab, newWid, &model) && VVISE_DownloadModel(vvise, oldWid, newWid, class, &model, TRUE));
}


static Bool
VVISE_DownloadModel(VVISE vvise, UInt oldWid, UInt wid, UInt class, FUPATE * pattern, Bool loadTemplates)
/**************************************************************************
 *
 * Download the model for a word, optionally replacing a previously
 * downloaded word model (if oldWid is non-zero).  Optionally download
 * acoustic templates for the kernels.
 * 
 **************************************************************************/
{
  UChar  * wmData;
  V_Uns    temData[UPAT_NUM_PARMS/2];
  V_Uns    nKernels;                    /* Number of kernels in word */
  V_Uns    minDwell;
  V_Uns    maxDwell;
  Int      i;
  V_Int    command, cmdEcho;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (vvise->textMode) {
    FUPATE   textModel;
    VVISE_fillTextModel(class, &textModel, pattern);
    pattern = &textModel;
  }

    /* Download the word model */

  wmData = pattern->bKernelDwells;
  nKernels = (V_Uns) pattern->bNKernWordMod;

  /* If there is an old wid, send a REPLACEWORD command; otherwise, send a DOWNLOADWORD command */
  command = oldWid ? _REPLACEWORD : _DLOADWORD;
  length = (V_ULong) (((oldWid ? 4 : 3) + nKernels * 2) * VINTSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, command, COMMAND_PRIORITY, &vstatus)) {
    if (oldWid) MSG_writeUns(vvise->message, (V_Uns) oldWid);
    MSG_writeUns(vvise->message, (V_Uns) wid);
    MSG_writeUns(vvise->message, (V_Uns) class);
    MSG_writeUns(vvise->message, nKernels);
    while (nKernels--) {
      /* Send new minDwell */
      MSG_writeUns(vvise->message, minDwell = (V_Uns) *wmData++);
      /* Send new maxDwell */
      MSG_writeUns(vvise->message, maxDwell = (V_Uns) *wmData++);
    }
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &cmdEcho, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, cmdEcho, command);
      MSG_closeRead(vvise->message, &cstatus);

        /* Optionally download the template */

      if (loadTemplates) {

        wmData = pattern->bTemplate;
        nKernels = (V_Uns) pattern->bTemplateCount;

        /* Assemble the DLOADTEMPLATE command */
        length = (V_ULong) ((nKernels * UPAT_NUM_PARMS / 2 + 2) * VINTSIZEINMELS);
        if (vstatus == VISESUCCESS &&
            MSG_openWrite(vvise->message, vvise->seMBoxId, length, _DLOADTEMPLATE, COMMAND_PRIORITY, &vstatus)) {
          MSG_writeUns(vvise->message, (V_Uns) wid);
          MSG_writeUns(vvise->message, nKernels);
          while (nKernels--) {
            memcpy((void *) temData, (void *) wmData, (size_t) UPAT_NUM_PARMS);
            wmData += UPAT_NUM_PARMS;
            for (i = 0; i < UPAT_NUM_PARMS/2; i++)
              MSG_writeUns(vvise->message, temData[i]);
          }
          if (MSG_closeWrite(vvise->message, &vstatus) &&
              MSG_openRead(vvise->message, &senderId, &length, &cmdEcho, TRUE, &vstatus)) {
            vstatus = VVISE_verifyAck(vvise->message, cmdEcho, _DLOADTEMPLATE);
            MSG_closeRead(vvise->message, &cstatus);
          }
        }
      }
    }
  }

  vvise->status = (Int) vstatus;
  if (vstatus != VISESUCCESS)
    VBX_print("  VVISE_DownloadModel FAILED with status = %s\n", VISEERR_string(vstatus));
  return(vstatus == VISESUCCESS);
}


UInt
VVISE_uploadSelectedModels(VVISE vvise, VOCAB vocab, Char ** words, Bool uploadDwells, Int minTrainingCount)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload VISE's models and dwells for every model in the vocabulary whose
 * name occurs in the given (NULL-terminated) array of word names and whose
 * training count equals or exceeds the specified minimum.
 * Returns  the number of models uploaded.
 * Call at the end of each training pass, before saving to the voice file.
 *---------------------------------------------------------------------------*/
{
  Int      windex;
  Char   * wordName;
  UInt     wid;
  UInt     numUploaded = 0;

  if (words) {
    for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab)) {
      wordName = VOCAB_wordNameFromId(vocab, wid);
      if (wordName) {
        for (windex = 0; words[windex]; windex++)
          if (0 == strcmp(wordName, words[windex]))
            break;
        if (words[windex] && VVISE_uploadTrainedModel(vvise, vocab, wid, uploadDwells, minTrainingCount)) numUploaded++;
        else if (!words[windex] && VVISE_uploadTrainedModel(vvise, vocab, wid, uploadDwells, INT_MAX)) numUploaded++;
      }
    }
  } else {
    numUploaded = VVISE_uploadTrainedModels(vvise, vocab, uploadDwells, minTrainingCount);
  }
  vvise->status = VISESUCCESS;

  return(numUploaded);
}

UInt
VVISE_uploadVocab(VVISE vvise, VOCAB vocab, Bool uploadDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload VISE's models and dwells for every model in the vocabulary. Returns
 * the number of models actually uploaded.
 * Call at the end of each training pass, before saving to the voice file.
 *---------------------------------------------------------------------------*/
{
  UInt     wid;
  UInt     numUploaded = 0;

  for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab))
    if (VVISE_uploadModel(vvise, vocab, wid, uploadDwells))
      numUploaded++;
  vvise->status = VISESUCCESS;

  return(numUploaded);
}


UInt
VVISE_uploadTrainedModels(VVISE vvise, VOCAB vocab, Bool uploadDwells, Int minTrainingCount)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload VISE's models and dwells for every model in the vocabulary whose
 * training count equals or exceeds the specified minimum. Returns the number
 * of models actually uploaded.
 * Call at the end of each training pass, before saving to the voice file.
 *---------------------------------------------------------------------------*/
{
  UInt     wid;
  UInt     numUploaded = 0;

  for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab))
    if (VVISE_uploadTrainedModel(vvise, vocab, wid, uploadDwells, minTrainingCount))
      numUploaded++;
  vvise->status = VISESUCCESS;

  return(numUploaded);
}


UInt
VVISE_uploadHypModels(VVISE vvise, VOCAB vocab, HYP hyp, Bool uploadDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload VISE's models and dwells for every modelled word in the hypothesis
 *---------------------------------------------------------------------------*/
{
  UInt    wid;
  WRDHYP  words;
  VOCENT  vocent;
  UInt    numWords, wordIndex;
  UInt    numUploaded = 0;

  if ((words = HYP_words(hyp, &numWords)))
    for (wordIndex = 0; wordIndex < numWords; wordIndex++)
      if ((vocent = VOCAB_entryFromId(vocab, WRDHYP_wid(words + wordIndex))))
        for (wid = VOCENT_firstWordId(vocent); wid; wid = VOCENT_nextWordId(vocent))
          if (VVISE_uploadModel(vvise, vocab, wid, uploadDwells))
            numUploaded++;
  vvise->status = VISESUCCESS;

  return(numUploaded);
}


UInt
VVISE_uploadTranscribedModels(VVISE vvise, VOCAB vocab, Char * transcription, Bool uploadDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload VISE's models and dwells for every word in the transcription
 *---------------------------------------------------------------------------*/
{
  UInt     wid;
  VOCENT * tokens;
  UInt     numWords, wordIndex;
  UInt     numUploaded = 0;

  if ((tokens = VOCAB_tokens(vocab, transcription, &numWords))) {
    for (wordIndex = 0; wordIndex < numWords; wordIndex++)
      for (wid = VOCENT_firstWordId(tokens[wordIndex]); wid; wid = VOCENT_nextWordId(tokens[wordIndex]))
        if (VVISE_uploadModel(vvise, vocab, wid, uploadDwells))
          numUploaded++;
    free(tokens);
  }
  vvise->status = VISESUCCESS;

  return(numUploaded);
}


Bool
VVISE_uploadModel(VVISE vvise, VOCAB vocab, UInt wid, Bool uploadDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload a word model, optionally including the dwells
 *---------------------------------------------------------------------------*/
{
  FUPATE  model;

  vvise->status = MISSINGWORDMODEL;
  return((VOCAB_getModel(vocab, wid, &model) || VOCAB_makeModel(vocab, wid, 0, &model)) &&
         VVISE_UploadModel(vvise, wid, uploadDwells, &model) &&
         VOCAB_putModel(vocab, wid, &model));
}


Bool
VVISE_uploadTrainedModel(VVISE vvise, VOCAB vocab, UInt wid, Bool uploadDwells, Int minTrainingCount)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload a word model if its training count equals or exceeds the specified
 * training count, optionally including the dwells
 *---------------------------------------------------------------------------*/
{
  FUPATE  model;

  vvise->status = MISSINGWORDMODEL;
  if (!(VOCAB_getModel(vocab, wid, &model) || VOCAB_makeModel(vocab, wid, 0, &model)))
    return FALSE;

  if (model.wTrainingCount >= minTrainingCount) {
    if (!VVISE_UploadModel(vvise, wid, uploadDwells, &model))
      return FALSE;
  } else {
     /* Restore old statistics for undertrained model */
      model.lSum = model.lSquares;
      model.wCount = model.wTrainingSavedCount;
      model.wTrainingCount = model.wTrainingSavedCount;
  }
  return VOCAB_putModel(vocab, wid, &model);
}


static Bool
VVISE_UploadModel(VVISE vvise, UInt wid, Bool uploadDwells, FUPATE * model)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Upload VISE's model for a word
 *---------------------------------------------------------------------------*/
{
  V_Uns    class;
  V_Uns    retWid;
  V_Uns    numKernels, i, j;
  V_Uns    temData[UPAT_NUM_PARMS/2];
  UChar  * wmData;
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) VINTSIZEINMELS, _ULOADTEMPLATE, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, (V_Uns) wid);
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _ULOADTEMPLATE);
      if (vstatus == VISESUCCESS &&
          MSG_readUns(vvise->message, &retWid) && wid == retWid &&
          MSG_readUns(vvise->message, &numKernels)) {
        model->bTemplateCount = (V_Byte) numKernels;
        wmData = model->bTemplate;
        for (i = 0; i < numKernels; i++, wmData += UPAT_NUM_PARMS) {
          for (j = 0; j < UPAT_NUM_PARMS/2; j++)
            MSG_readUns(vvise->message, &temData[j]);
          memcpy((void *) wmData, (void *) temData, (size_t) UPAT_NUM_PARMS);
        }
      }
      /* Ignore any closing error */
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_uploadModel::ULOADTEMPLATE: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));

      if (uploadDwells) {
        /* Get word models for all words from VISE */
        if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) VINTSIZEINMELS, _ULOADWORD, COMMAND_PRIORITY, &vstatus)) {
          MSG_writeUns(vvise->message, (V_Uns) wid);
          if (MSG_closeWrite(vvise->message, &vstatus) &&
              MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
            vstatus = VVISE_verifyAck(vvise->message, command, _ULOADWORD);
            if (vstatus == VISESUCCESS &&
                MSG_readUns(vvise->message, &retWid) && wid == retWid &&
                MSG_readUns(vvise->message, &class) &&
                MSG_readUns(vvise->message, &numKernels)) {
              model->bNKernWordMod = (V_Byte) numKernels;
              wmData = model->bKernelDwells;
              for (i = 0; i < numKernels; i++) {
                /* Get the mindwell */
                MSG_readUns(vvise->message, &temData[0]);
                *wmData++ = (UChar) temData[0];
                /* Get the maxdwell */
                MSG_readUns(vvise->message, &temData[1]);
                *wmData++ = (UChar) temData[1];
              }
            }
            /* Ignore any closing error */
            MSG_closeRead(vvise->message, &cstatus);
            VBX_DEBUG(if (cstatus) VBX_print("  VVISE_uploadModel::ULOADWORD: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
          }
        }
      }
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


UInt
VVISE_adjustModels(VVISE vvise, VOCAB vocab, UInt weight, Bool adjustDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Adjust VISE's models and dwells for every model in the vocabulary. Returns
 * the number of models actually adjusted.
 * Call after each training pass.
 *---------------------------------------------------------------------------*/
{
  UInt     wid;
  UInt     numAdjusted = 0;
  
  for (wid = VOCAB_firstWordId(vocab); wid; wid = VOCAB_nextWordId(vocab))
    if (VVISE_AdjustModel(vvise, wid, weight, adjustDwells)) numAdjusted++;

  vvise->status = VISESUCCESS;
  return(numAdjusted);
}

UInt
VVISE_adjustSelectedModels(VVISE vvise, VOCAB vocab, Char ** words, UInt weight, Bool adjustDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Adjust VISE's models and dwells for every model in the vocabulary whose
 * name is in the given (NULL-terminated) array of word names.
 * Returns the number of models adjusted.
 * Call after each training pass.
 *---------------------------------------------------------------------------*/
{
  Int      windex;
  VOCENT   word;
  UInt     wid;
  UInt     numAdjusted = 0;

  if (words) {
    for (windex = 0; words[windex]; windex++) {
      word = VOCAB_entryFromName(vocab, words[windex]);
      if (word) {
        for (wid = VOCENT_firstWordId(word); wid; wid = VOCENT_nextWordId(word))
          if (VVISE_AdjustModel(vvise, wid, weight, adjustDwells)) numAdjusted++;
      }
    }
  } else {
    numAdjusted = VVISE_adjustModels(vvise, vocab, weight, adjustDwells);
  }
  vvise->status = VISESUCCESS;
  return numAdjusted;
}


static Bool
VVISE_AdjustModel(VVISE vvise, UInt wid, UInt weight, Bool adjustDwells)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Adjust VISE's model and dwells for the specified wid.
 * Returns TRUE iff successful
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;
  Bool     success = FALSE;

  /* Adjust kernel patterns */
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) (3 * VINTSIZEINMELS), _FINISHTRAININGWORD, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, (V_Uns) wid);
    MSG_writeUns(vvise->message, (V_Uns) weight);        /* Weight of old template */
    MSG_writeUns(vvise->message, (V_Uns) adjustDwells);
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _FINISHTRAININGWORD);
      if (vstatus == VISESUCCESS)
        success = TRUE;
      else
        VBX_DEBUG(VBX_print("  VVISE_adjustModels: cannot adjust model for word %u\n", wid));
      MSG_closeRead(vvise->message, &cstatus);
    }
  }
  return(success);
}


Bool
VVISE_warmstart(VVISE vvise)
/***************************************************************************
 *
 * Warmstart the recognition engine
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) 0, _WARMSTART, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vvise->message, &vstatus) &&
      MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VVISE_verifyAck(vvise->message, command, _WARMSTART);
    MSG_closeRead(vvise->message, &cstatus);
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_sourceInit(VVISE vvise)
/***************************************************************************
 *
 * Initialize the recognition engine's source
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) 0, _SOURCEINIT, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vvise->message, &vstatus) &&
      MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VVISE_verifyAck(vvise->message, command, _SOURCEINIT);
    MSG_closeRead(vvise->message, &cstatus);
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_init(VVISE vvise)
/***************************************************************************
 *
 * Initialize the recognition engine
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) 0, _VISEINIT, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vvise->message, &vstatus) &&
      MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VVISE_verifyAck(vvise->message, command, _VISEINIT);
    MSG_closeRead(vvise->message, &cstatus);
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


UInt
VVISE_deleteGrammars(VVISE vvise, VOCAB vocab)
/***************************************************************************
 *
 * Delete all of the grammars that use the given vocabulary, and
 * return the number deleted.
 *
 ***************************************************************************/
{
  SYNTAX  * grammar;
  FGRAPH  * graph;
  UInt      numGrammars;
  UInt      numDone = 0;    /* Count of successful deletes */

  assert(vvise && vocab);

  grammar = VOCAB_grammars(vocab, &numGrammars);

  while (numGrammars--) {

    /* Get the graph */
    graph = SYNTAX_graph(*grammar);

    /* Delete the grammar */
    if (VVISE_deleteGrammar(vvise, (UInt) graph->wGrammarID))
      numDone++;

    /* Get the next grammar */
    grammar++;
  }

  return(numDone);
}


UInt
VVISE_downloadGrammars(VVISE vvise, VOCAB vocab, Bool verbose)
/***************************************************************************
 *
 * Download all of the grammars that use the given vocabulary, and
 * return the number downloaded.
 *
 ***************************************************************************/
{
  UInt        numGrammars;
  UInt        wrdsOnArc;
  UInt        numDone = 0;    /* Count of successful downloads */
  SYNTAX    * grammar;
  FGRAPH    * graph;
  FGRAPHARC * arc;            /* Arc iterator */
  UInt        arcCnt;         /* Count of the arcs in the graph */
  V_Uns     * word;           /* Word Id iterator */
  UInt        wrdCnt;         /* Count of the words on an arc */
  V_Uns       nWrdsNInit;
  V_Int       command;
  V_Uns       senderId;
  V_ULong     length;
  V_Err       vstatus, cstatus;

  assert(vocab);

  grammar = VOCAB_grammars(vocab, &numGrammars);

  while (numGrammars--) {

    /* Get the graph parameters */
    graph = SYNTAX_graph(*grammar);
    arc = SYNTAX_arcs(*grammar);
    wrdsOnArc = SYNTAX_numWordsOnArc(*grammar);
    arcCnt = graph->wNumberOfArcs;

    length = (V_ULong) ((3 + 3 * arcCnt + wrdsOnArc) * VINTSIZEINMELS);
    if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _DLOADGRAMMAR, COMMAND_PRIORITY, &vstatus)) {
      /* Put the grammarwide parameters into the message */
      MSG_writeUns(vvise->message, graph->wGrammarID);
      MSG_writeUns(vvise->message, graph->wNumberOfArcs);
      MSG_writeUns(vvise->message, graph->wNumberOfNodes);
      /* Put the arcs into the message */
      while (arcCnt--) {
        /* Insert the arc parameters */
        MSG_writeUns(vvise->message, arc->uSourceNode);
        MSG_writeUns(vvise->message, arc->uDestNode);
        MSG_writeUns(vvise->message, (V_Uns) (wrdCnt = arc->uNumberOfWords));
        word = (V_Uns *) (arc + 1);
        /* Insert the id's of the words on the arc */
        while (wrdCnt--)
          MSG_writeUns(vvise->message, *word++);
        arc = (FGRAPHARC *) word;
      }
      if (MSG_closeWrite(vvise->message, &vstatus) &&
          MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
        vstatus = VVISE_verifyAck(vvise->message, command, _DLOADGRAMMAR);
        MSG_readUns(vvise->message, &nWrdsNInit);
        MSG_closeRead(vvise->message, &cstatus);
        if (vstatus == VISESUCCESS) {
          /* Count the download successful even if there are words missing */
          if (nWrdsNInit && verbose)
            VBX_print("  VVISE_downloadGrammars: %d uninitialized words in the grammar %d\n", nWrdsNInit, (Int) graph->wGrammarID);
          numDone++;
        } else break;
      } else break;
    } else break;

    /* Get the next grammar */
    grammar++;
  }

  vvise->status = (Int) vstatus;
  return(numDone);
}


Bool
VVISE_downloadJin(VVISE vvise, JIN2FR frames, SN * startSN, SN * endSN, UInt * numFrames)
/***************************************************************************
 *
 * Download the jin2 frames whose sequence numbers are within the specified
 * limits, reporting the actual starting and ending sequence numbers and
 * the number of frames downloaded.  Because the function uses the value of
 * the numFrames argument to avoid reading beyond the end of the JIN2FR
 * array, the numFrames argument must not exceed the size of the array.
 * THE FUNCTION COPIES THE JIN2FR ARRAY, WHICH THE CALLER MAY THUS
 * FREE IMMEDIATELY AFTER THE CALL, IF DESIRED.
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_Uns    frameCnt;
  V_ULong  length;
  V_SN     sn;
  UInt     index, startIdx, endIdx;
  V_Err    vstatus, cstatus;

  /* Delimit and count the frames to be sent */
  frameCnt = 0;
  startIdx = endIdx = *numFrames;
  for (index = 0, sn = *startSN; index < *numFrames && sn <= *endSN; index++) {
    sn = frames[index].sn;
    if (sn >= *startSN) {
      if (startIdx > index) startIdx = index;
      if (sn <= *endSN) {
        frameCnt++;
        endIdx = index;
      }
      if (sn >= *endSN) break;
    }
  }

  /* If the write fails, the number of frames sent will be zero */
  *numFrames = 0;

  length = (V_ULong) (2 * VSNSIZEINMELS + VINTSIZEINMELS + frameCnt * (VSNSIZEINMELS + SIZE_OF_JIN2_ARRAY));
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _DLOADJIN, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeSN(vvise->message, (V_SN) *startSN);
    MSG_writeSN(vvise->message, (V_SN) *endSN);
    MSG_writeUns(vvise->message, (V_Uns) frameCnt);
    index = startIdx;
    while (index <= endIdx &&
           MSG_writeSN(vvise->message, frames[index].sn) &&
           MSG_writeMels(vvise->message, (V_Mel *) &frames[index].jin, (V_ULong) SIZE_OF_JIN2_ARRAY))
      index++;
    VBX_DEBUG(if (index <= endIdx) VBX_print("  VVISE_downloadJin: wrote only %u of %u frames requested\n", (UInt) (index - startIdx), frameCnt));
    /* Tolerate length errors */
    if ((MSG_closeWrite(vvise->message, &vstatus) || vstatus == MESSAGEINCOMPLETE || vstatus == MESSAGEOVERFLOW) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _DLOADJIN);
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_downloadJin: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  /* Report the actual limits and number of frames */
  *numFrames = (UInt) (index - startIdx);
  *startSN = frames[startIdx].sn;
  *endSN = frames[index > 0 ? index - 1 : index].sn;

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


JIN2FR
VVISE_uploadJin(VVISE vvise, SN * startSN, SN * endSN, UInt * numFrames)
/***************************************************************************
 *
 * Upload the jin2 frames whose sequence numbers are within the specified
 * limits, reporting the actual starting and ending sequence numbers and
 * the number of frames uploaded.  Specifying a startSN of zero and an
 * endSN of SN_MAX causes all of the stored frames to be uploaded.  THIS
 * FUNCTION ALLOCATES A JIN2FR ARRAY, WHICH THE CALLER IS RESPONSIBLE FOR
 * FREEING.
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  JIN2FR   frames = NULL;
  V_Uns    frameCnt = 0;
  V_Uns    index = 0;
  V_Err    vstatus, cstatus;

  if (MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) (2 * VSNSIZEINMELS), _ULOADJIN, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeSN(vvise->message, (V_SN) *startSN);
    MSG_writeSN(vvise->message, (V_SN) *endSN);
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      if ((vstatus = VVISE_verifyAck(vvise->message, command, _ULOADJIN)) == VISESUCCESS &&
          MSG_readSN(vvise->message, startSN) &&
          MSG_readSN(vvise->message, endSN) &&
          MSG_readUns(vvise->message, &frameCnt) &&
          (*numFrames = (UInt) frameCnt) &&
          (frames = (JIN2FR) vvise->memory->Calloc(vvise->memory, (V_ULong) frameCnt, sizeof(jin2fr_t), SLOW_MEMORY))) {
        index = 0;
        while (index < frameCnt &&
               MSG_readSN(vvise->message, &frames[index].sn) &&
               MSG_readMels(vvise->message, (V_Mel *) &frames[index].jin, (V_ULong) SIZE_OF_JIN2_ARRAY))
          index++;
      }
      if (index != frameCnt) {
        VBX_DEBUG(VBX_print("  VVISE_uploadJin: read only %u of %u frames available\n", index, frameCnt));
        vstatus = MESSAGETOOSHORT;
        *numFrames = (UInt) index;
      }
      /* Ignore any closing error */
      MSG_closeRead(vvise->message, &cstatus);
      VBX_DEBUG(if (cstatus) VBX_print("  VVISE_uploadJin: MSG_closeRead status = %s\n", VISEERR_string(cstatus)));
    }
  }

  vvise->status = (Int) vstatus;
  return(frames);
}


Bool
VVISE_downloadWildcard(VVISE vvise, UInt oldWid, UInt wid, UInt minDwell, UInt maxDwell, UInt relThresh, UInt absThresh)
/***************************************************************************
 *
 * Download a wildcard model, optionally replacing a previously downloaded
 * wildcard model if oldWid is non-zero.
 *
 ***************************************************************************/
{
  V_Int    command, cmdEcho;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  /* If there is an old wid, assemble a REPLACEWORD command; otherwise, assemble a DLOADWORD command */
  command = oldWid ? _REPLACEWORD : _DLOADWORD;
  length = (V_ULong) (oldWid ? 6 * VINTSIZEINMELS : 5 * VINTSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, command, COMMAND_PRIORITY, &vstatus)) {
    if (oldWid) MSG_writeUns(vvise->message, (V_Uns) oldWid);       /* The old word Id */
    MSG_writeUns(vvise->message, (V_Uns) wid);                      /* The new word Id */
    MSG_writeUns(vvise->message, (V_Uns) PRIMES_nth(JOKER_CLASS));  /* The word class id */
    MSG_writeUns(vvise->message, (V_Uns) 1);                        /* The number of kernels (always 1) */
    MSG_writeUns(vvise->message, (V_Uns) minDwell);                 /* The minDwell */
    MSG_writeUns(vvise->message, (V_Uns) maxDwell);                 /* The maxDwell */
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &cmdEcho, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, cmdEcho, command);
      MSG_closeRead(vvise->message, &cstatus);
      if (vstatus == VISESUCCESS &&
          MSG_openWrite(vvise->message, vvise->seMBoxId, (V_ULong) (3 * VINTSIZEINMELS), _DLOADWILDCARD, COMMAND_PRIORITY, &vstatus)) {
        MSG_writeUns(vvise->message, (V_Uns) wid);                  /* The word id */
        MSG_writeUns(vvise->message, (V_Uns) relThresh);            /* The relative WC threshold */
        MSG_writeUns(vvise->message, (V_Uns) absThresh);            /* The absolute WC threshold */
        if (MSG_closeWrite(vvise->message, &vstatus) &&
            MSG_openRead(vvise->message, &senderId, &length, &cmdEcho, TRUE, &vstatus)) {
          vstatus = VVISE_verifyAck(vvise->message, cmdEcho, _DLOADWILDCARD);
          MSG_closeRead(vvise->message, &cstatus);
        }
      }
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_setTimeout(VVISE vvise, VBXMSG message, UInt maxFrames)
/***************************************************************************
 *
 * Set the recognition frame count.  If the message argument is NULL,
 * the message is sent and acknowledged through VVISE's message port;
 * otherwise, it is sent and acknowledged through the specified port.
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  length = (V_ULong) (VLONGSIZEINMELS);
  if (!message) message = vvise->message;
  if (MSG_openWrite(message, vvise->seMBoxId, length, _SETTIMEOUT, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeULong(message, (V_ULong) maxFrames);
    if (MSG_closeWrite(message, &vstatus) &&
        MSG_openRead(message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(message, command, _SETTIMEOUT);
      MSG_closeRead(message, &cstatus);
    }
  }

  if (vstatus != VISESUCCESS && vstatus != UNTIMELYCOMMAND)
    VBX_print("VVISE_setTimeout(%d) FAILED: status = %s\n", maxFrames, VISEERR_string(vstatus));

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_setParameter(VVISE vvise, VBXMSG message, UInt index, Int value)
/***************************************************************************
 *
 * Set the value of a VISE parameter.  If the message argument is NULL,
 * the message is sent and acknowledged through VVISE's message port;
 * otherwise, it is sent and acknowledged through the specified port.
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  length = (V_ULong) (2 * VINTSIZEINMELS);
  if (!message) message = vvise->message;
  if (MSG_openWrite(message, vvise->seMBoxId, length, _SETPARAMETERS, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(message, (V_Uns) index);
    MSG_writeInt(message, (V_Int) value);
    if (MSG_closeWrite(message, &vstatus) &&
        MSG_openRead(message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(message, command, _SETPARAMETERS);
      MSG_closeRead(message, &cstatus);
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_getParameter(VVISE vvise, VBXMSG message, UInt index, Int * value)
/***************************************************************************
 *
 * Get the value of a VISE parameter.  If the message argument is NULL,
 * the message is sent and acknowledged through VVISE's message port;
 * otherwise, it is sent and acknowledged through the specified port.
 *
 ***************************************************************************/
{
  V_Int    command, ivalue;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  if (!message) message = vvise->message;
  if (MSG_openWrite(message, vvise->seMBoxId, (V_ULong) VINTSIZEINMELS, _GETPARAMETERS, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(message, (V_Uns) index);
    if (MSG_closeWrite(message, &vstatus) &&
        MSG_openRead(message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(message, command, _GETPARAMETERS);
      MSG_readInt(message, &ivalue);
      MSG_closeRead(message, &cstatus);
      *value = (Int) ivalue;
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


Bool
VVISE_setThresholds(VVISE vvise, UInt act, UInt deact, UInt loose, UInt tight, UInt muthah)
/***************************************************************************
 *
 * Set the VISE thresholds
 *
 ***************************************************************************/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  length = (V_ULong) (10 * VINTSIZEINMELS);
  if (MSG_openWrite(vvise->message, vvise->seMBoxId, length, _SETPARAMETERS, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vvise->message, ACTIVATIONTHRESHOLD);
    MSG_writeUns(vvise->message, (V_Uns) act);
    MSG_writeUns(vvise->message, DEACTIVATIONTHRESHOLD);
    MSG_writeUns(vvise->message, (V_Uns) deact);
    MSG_writeUns(vvise->message, LOOSEDPTHRESH);
    MSG_writeUns(vvise->message, (V_Uns) loose);
    MSG_writeUns(vvise->message, TIGHTDPTHRESH);
    MSG_writeUns(vvise->message, (V_Uns) tight);
    MSG_writeUns(vvise->message, MUHTHAH);
    MSG_writeUns(vvise->message, (V_Uns) muthah);
    if (MSG_closeWrite(vvise->message, &vstatus) &&
        MSG_openRead(vvise->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VVISE_verifyAck(vvise->message, command, _SETPARAMETERS);
      MSG_closeRead(vvise->message, &cstatus);
    }
  }

  vvise->status = (Int) vstatus;
  return(vstatus == VISESUCCESS);
}


void
VVISE_setTextMode(VVISE vvise, Bool flag)
/***************************************************************************
 *
 * Set the VISE operation mode
 *
 ***************************************************************************/
{
  vvise->textMode = flag;
}


static Bool
VVISE_GetRecognitionStats(VBXMSG message, Bool verbose)
/**************************************************************************
 *
 * Get the recognition statistics from the reply
 * 
 **************************************************************************/
{
  V_Uns    numParms;
  V_Int    parmId;
  V_Int    parmValue;

  if (!MSG_readUns(message, &numParms))
    return(FALSE);
 
  if (verbose) {
    if (numParms)
      VBX_print("  Recognizer Statistics:\n");
    else
      VBX_print("  No recognizer statistics\n");
  }
 
  while (numParms && MSG_readInt(message, &parmId) && MSG_readInt(message, &parmValue)) {
    /* If parameter ID is valid, print text and value */
    if (verbose && IS_VALID_PID((fyiid_t) parmId))
      VBX_print("   %s = %d\n", FYI_parmTxt[(fyiid_t) parmId], (Int) parmValue);
    numParms -= 2;
  }

  return(numParms == 0);
}


static V_Err
VVISE_verifyAck(VBXMSG message, V_Int command, V_Int cmdExpected)
/**************************************************************************
 *
 * Verify the reply
 * 
 **************************************************************************/
{
  V_Int   vstatus;

  /* Verify that the message is an acknowledgment */
  if (command != _ACKNOWLEDGMENT) {
    VBX_DEBUG(VBX_print("  VVISE_verifyAck: message is not an ACKNOWLEDGMENT (command = %d)\n", command));
    vstatus = (V_Int) UNTIMELYCOMMAND;

  /* Get the command being acknowledged */
  } else if (!MSG_readInt(message, &command)) {
    VBX_DEBUG(VBX_print("  VVISE_verifyAck: cannot read command acknowledged\n"));
    vstatus = (V_Int) MESSAGETOOSHORT;

  /* Verify that the command is the one expected */
  } else if (command != cmdExpected) {
    VBX_DEBUG(VBX_print("  VVISE_verifyAck: command acknowledged (%d) is not that expected (%d)\n", command, cmdExpected));
    vstatus = (V_Int) UNTIMELYCOMMAND;

  /* Get the message status */
  } else if (!MSG_readInt(message, &vstatus)) {
    VBX_DEBUG(VBX_print("  VVISE_verifyAck: cannot read status\n"));
    vstatus = (V_Int) MESSAGETOOSHORT;
  }

  return((V_Err) vstatus);
}

#define ACTUAL_SILENCE_CLASS 1
static void
VVISE_fillTextModel(UInt class, FUPATE * textModel, FUPATE * voiceModel)
/**************************************************************************
 *
 * Fills textModel on the basis of voiceModel; class defines special case of
 * silence model.
 **************************************************************************/
{
  Int      i, n;
  V_Byte * bt;

  /* Replicate */
  memcpy(textModel, voiceModel, sizeof(FUPATE));
  /* Edit */
  if (ACTUAL_SILENCE_CLASS == class) {
    /* It is silence model. The only thing to replace is the first 16 bytes of template */
    assert(1 == textModel->bNKernWordMod);
    assert(1 == textModel->bTemplateCount);
    memset(textModel->bTemplate, 0, UPAT_NUM_PARMS);
    textModel->bTemplate[0] = ' ';
  } else {
    /* Regular word. use its name to fill in model */
    n = textModel->bNKernWordMod = textModel->bTemplateCount = textModel->bNameCount;
    /* Make mindwell == maxdwell == 1 */
    memset(textModel->bKernelDwells, 1, n * 2);
    bt = textModel->bTemplate;
    memset(bt, 0, n * UPAT_NUM_PARMS);
    for (i = 0; i < n; i++, bt += UPAT_NUM_PARMS)
      *bt = textModel->bName[i];
  }
}
