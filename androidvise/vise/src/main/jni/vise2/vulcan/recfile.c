#include "pthr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ifaces.h"
#include "recfile.h"
#include "vfile.h"

#define   MAX(A,B)     ((A)>(B)?(A):(B))
#define   MIN(A,B)     ((A)<(B)?(A):(B))


/*---------------------------------------------------------------------------*
 * STREAM IMPLEMENTATION
 *---------------------------------------------------------------------------*/

typedef struct streamHndl_s {
  UChar      *dataPtr;
  UInt        dataSize;
} streamHndl_t, *STREAMHNDL;

typedef struct recfile_s {
   IFile      myIFile;
   IMemory   *myIMem;
   UInt       ioType;
} recfile_t;

#undef   INTERFACE
#define  INTERFACE IFile

/*---------------------------------------------------------------------------*
 * FILESYSTEM IMPLEMENTATION
 *---------------------------------------------------------------------------*/

VBXMETHODIMP_(void *)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Treat the fileHandle as a zero terminated string and open it in the
 * specified mode.
 * Return: Pointer to the system-returned FILE descriptor
 *---------------------------------------------------------------------------*/
StdioOpen(VBXTHIS_ void *fileHandle, V_Uns mode)
{
  FILE *fPtr;

  switch (mode) {
  case RECFILE_READWRITE:
    fPtr = fopen(fileHandle,"r+b");
    break;

  case RECFILE_READ:
    fPtr = fopen(fileHandle,"rb");
    break;

  case RECFILE_WRITE:
    fPtr = fopen(fileHandle,"wb");
    break;

  default:
    return(NULL);
  }
   
  return(fPtr);
}


VBXMETHODIMP_(V_Err)
StdioClose(VBXTHIS_ void *fileID)
{
  return((V_Err) fclose((FILE *) fileID));
}


VBXMETHODIMP_(V_ULong)
StdioRead(VBXTHIS_ void *fileID, void *data, V_ULong size)
{
  return((V_ULong) fread(data, 1, size, (FILE *) fileID));
}


VBXMETHODIMP_(V_ULong)
StdioWrite(VBXTHIS_ void *fileID, void *data, V_ULong size)
{
  return((V_ULong) fwrite(data, 1, size, (FILE *) fileID));
}

VBXMETHODIMP_(V_Err)
StdioSeek(VBXTHIS_ void *fileID, V_Long offset, V_Uns mode)
{
  return((V_Err) fseek((FILE *) fileID, offset, mode));
}

VBXMETHODIMP_(V_ULong)
StdioTell(VBXTHIS_ void *fileID)
{
  return((V_ULong) ftell((FILE *) fileID));
}

#define   VSTELL(vs)          (vs->currPosPtr - vs->streamPtr)

VBXMETHODIMP_(void *)
MemStreamOpen(VBXTHIS_ void *fileHandle, V_Uns mode)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Treat the fileHandle as a STREAMHNDL.
 * The structure pointed at by the STREAMHNDL contains a pointer to 
 * the actual data along with its size.
 * Return: Pointer to the heap-allocated VSTREAM descriptor
 *---------------------------------------------------------------------------*/
{
  VSTREAM	vs;
  RECFILE recfile = (RECFILE) This;

  vs = recfile->myIMem->Calloc(recfile->myIMem, 1, sizeof(vstream_t), SLOW_MEMORY);
  if (vs == NULL) {
    return(NULL);
  }  

  vs->streamPtr = (Ptr)((STREAMHNDL) fileHandle)->dataPtr;
  vs->lFileSize = ((STREAMHNDL) fileHandle)->dataSize;
  vs->uMode     = mode;
  vs->iMem      = recfile->myIMem;
  return(vs);
}


VBXMETHODIMP_(V_Err)
MemStreamClose(VBXTHIS_ void *streamId)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Data has been written out directly to stream all along.  Nothing to do but
 * release all memory associated with the VSTREAM descriptor.
 *---------------------------------------------------------------------------*/
{
  VSTREAM vs = (VSTREAM) streamId;
  RECFILE recfile = (RECFILE) This;

  vs->iMem->Free(vs->iMem, vs);

  return RECFILE_SUCCESS;
}


VBXMETHODIMP_(void *)
FileStreamOpen(VBXTHIS_ void *fileHandle, V_Uns mode)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Treat the fileHandle as a zero-terminated file name.  Open it, read the
 * data into a single chunk-o-mem.
 * Return: Pointer to the heap-allocated VSTREAM descriptor
 *---------------------------------------------------------------------------*/
{
  FILE    *fp;
  UInt     dataSize;
  void    *dataPtr;
  VSTREAM  vs;
  RECFILE  recfile = (RECFILE) This;

  if (NULL == (fp = fopen(fileHandle, "rb"))) {
    return(NULL);
  }

  fseek(fp, 0, SEEK_END);
  dataSize = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  
  if (NULL == (dataPtr = recfile->myIMem->Calloc(recfile->myIMem, 1, dataSize, SLOW_MEMORY))) {
    fclose(fp);
    return(NULL);
  }  

  if (fread(dataPtr, 1, dataSize, fp) != dataSize) {
    fclose(fp);
    recfile->myIMem->Free(recfile->myIMem, dataPtr);
    return(NULL);
  }  

  fclose(fp);

  if (NULL == (vs = recfile->myIMem->Calloc(recfile->myIMem, 1, sizeof(vstream_t), SLOW_MEMORY))) {
    recfile->myIMem->Free(recfile->myIMem, dataPtr);
    return(NULL);
  }  

  vs->streamPtr = dataPtr;
  vs->lFileSize = dataSize;
  vs->uMode     = mode;
  vs->iMem     = recfile->myIMem;

  vs->fileHandle = recfile->myIMem->Calloc(recfile->myIMem, 1, strlen(fileHandle) + 1, SLOW_MEMORY);
  if (vs->fileHandle == NULL) {
    recfile->myIMem->Free(recfile->myIMem, dataPtr);
    recfile->myIMem->Free(recfile->myIMem, vs);
    return NULL;
  }  
  strcpy(vs->fileHandle, fileHandle);

  return(vs);
}


VBXMETHODIMP_(V_Err)
FileStreamClose(VBXTHIS_ void *streamId)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Write the data into the file we read it from, close that file and release
 * all memory associated with the VSTREAM descriptor.
 *---------------------------------------------------------------------------*/
{
  VSTREAM vs = (VSTREAM) streamId;

#if 0
  FILE	 * fp;

  THIS SEEMS EXTREMELY INAPPROPRIATE
  if ((fp = fopen(vs->fileHandle, "wb")) == NULL) {
    return(RECFILE_OPEN_FAILS);
  }

  if (fwrite(vs->streamPtr, 1, vs->lFileSize, fp) != 1) {
    fclose(fp);
    return(RECFILE_FILE_WRITE_ERROR);
  }

  fclose(fp);
#endif

  vs->iMem->Free(vs->iMem, vs->streamPtr);
  vs->iMem->Free(vs->iMem, vs->fileHandle);
  vs->iMem->Free(vs->iMem, vs);

  return(RECFILE_SUCCESS);
}

VBXMETHODIMP_(V_ULong)
StreamRead(VBXTHIS_ void *streamId, void *data, V_ULong size)
{
  VSTREAM vs = (VSTREAM) streamId;
  V_ULong nReadable = vs->lFileSize - VSTELL(vs);
  V_ULong nRead = 0;

  if (nReadable > 0) {
    nRead = MIN(nReadable, size);
    memcpy(data, vs->currPosPtr, nRead);
  }
  vs->currPosPtr += nRead;
  return(nRead);
}


VBXMETHODIMP_(V_ULong)
StreamWrite(VBXTHIS_ void *streamId, void *data, V_ULong size)
{
  VSTREAM vs = (VSTREAM) streamId;
  V_ULong nWritable = vs->lFileSize - VSTELL(vs);
  V_ULong nWritten = 0;

  if (nWritable > 0) {
    nWritten = MIN(nWritable, size);
    memcpy(vs->currPosPtr, data, nWritten);
  }
  vs->currPosPtr += nWritten;
  return(nWritten);
}

VBXMETHODIMP_(V_Err)
StreamSeek(VBXTHIS_ void *streamId, V_Long offset, V_Uns mode)
{
  VSTREAM vs = (VSTREAM) streamId;
  V_ULong nReachable;

  switch (mode) {
  case RECFILE_SEEK_SET:
    nReachable = MIN(vs->lFileSize,offset);
    vs->currPosPtr = vs->streamPtr + nReachable;
    break;
  case RECFILE_SEEK_CUR:
    nReachable = MIN(vs->lFileSize-VSTELL(vs),offset);
    vs->currPosPtr += nReachable;
    break;
  case RECFILE_SEEK_END:
    nReachable = MIN(vs->lFileSize,-offset);
    vs->currPosPtr = vs->streamPtr + vs->lFileSize - nReachable;
    break;
  default:
   return(RECFILE_SEEK_ERROR);
  }
   
  return(RECFILE_SUCCESS);
}

VBXMETHODIMP_(V_ULong)
StreamTell(VBXTHIS_ void *streamId)
{
  VSTREAM vs = (VSTREAM) streamId;
  return((V_ULong) VSTELL(vs));
}


IFile *
RECFILE_create(IMemory *iMem, UInt ioType)
{
  RECFILE recfile;

  recfile = iMem->Calloc(iMem, 1, sizeof(recfile_t), SLOW_MEMORY);
  if (recfile == NULL) {
    return(NULL);
  }

  recfile->myIMem = iMem;                 /* For use again later */
  recfile->ioType = ioType;

  switch (ioType) {
  case RECFILE_STDIO:
    recfile->myIFile.Open  = StdioOpen;
    recfile->myIFile.Close = StdioClose;
    recfile->myIFile.Read  = StdioRead;
    recfile->myIFile.Write = StdioWrite;
    recfile->myIFile.Seek  = StdioSeek;
    recfile->myIFile.Tell  = StdioTell;
    break;
  case RECFILE_MEMSTREAM:
    recfile->myIFile.Open  = MemStreamOpen;
    recfile->myIFile.Close = MemStreamClose;
    recfile->myIFile.Read  = StreamRead;
    recfile->myIFile.Write = StreamWrite;
    recfile->myIFile.Seek  = StreamSeek;
    recfile->myIFile.Tell  = StreamTell;
    break;
  case RECFILE_FILESTREAM:
    recfile->myIFile.Open  = FileStreamOpen;
    recfile->myIFile.Close = FileStreamClose;
    recfile->myIFile.Read  = StreamRead;
    recfile->myIFile.Write = StreamWrite;
    recfile->myIFile.Seek  = StreamSeek;
    recfile->myIFile.Tell  = StreamTell;
    break;
  default:
    iMem->Free(iMem, recfile);
    recfile = NULL;
  }

  return((IFile *) recfile);
}

void
RECFILE_destroy(IFile *iFile)
{
  RECFILE recfile = (RECFILE) iFile;

  recfile->myIMem->Free(recfile->myIMem, recfile);
}

Int 
RECFILE_ioType(IFile *iFile)
{
  RECFILE recfile = (RECFILE) iFile;

  return(recfile->ioType);
}


