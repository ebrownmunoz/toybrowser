/* parse.c - Response facility parser
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * The parser uses the sequence of wordId's returned in a  recognition 
 * hypothesis (HYP) object to guide it through a simulation of the
 * non-deterministic finite-state automaton (NFA) described by a given
 * parse table.
 * Parse table blocks originate from CONVERT-compiled grammars, thus are found
 * in .REC files.  The BlockId for parse tables will generally be one of the 
 * following:
 *   TR_HOST_PARSE, TR_VOICE_PARSE, TR_GRAMMAR_PARSE, TR_CONTROL_PARSE
 *
 * The structure of the parse blocks is:
 *   UShort block id
 *   UShort grammar class
 *   UInt length of table (less the block id and class)
 *   UShort checksum
 *   ------------------------ (everything from here up is unused in parse)
 *   UShort state count
 *   UInt offset
 *   (UInt * state count) state index entries (each entry is the index of a state)
 *   {UShort state ID }
 *   {UShort arc type } * (state count + 1) state descriptors
 * 
 * HISTORY
 * 11-Nov-97 Craig Vanderborgh (craigv@verbex.com)
 *      Modernized for use in the SAPI project
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"
#include <assert.h>
#include "types.h"
#include "vbx.h"

#include "vfile.h"
#include "tree.h"
#include "buds.h"
#include "parse.h"

#ifdef DEBUG
#define VBX_DEBUG(P)           P
#define VBX_IF_NOT_DO(C,P)     if (!(C)) (P)
#define VBX_VERBOSE            TRUE
#else
#define VBX_DEBUG(P)
#define VBX_IF_NOT_DO(C,P)     C
#define VBX_VERBOSE            FALSE
#endif

#define DEFAULT_ARC             0x7FFF
#define DEFAULT_WORD            0x7FFF

#define NO_TERMINALS            0
#define PARSE_START_STATE       0

#define LAST_ARC_MASK           0x8000
#define LAST_ARC                LAST_ARC_MASK
#define PARSE_WORD_ID_MASK      0x7FFF
#define WORDONARC(arc)         (PARSE_WORD_ID_MASK &(arc))
#define LASTARC(arc)           (LAST_ARC_MASK &(arc))

#define LAST_STATE_MASK         0x8000
#define LAST_STATE              LAST_STATE_MASK
#define PARSE_STATE_ID_MASK     0x7FFF
#define PARSE_TERMINAL_STATE    0xFFFF
#define STATEID(succ)           (PARSE_STATE_ID_MASK &(succ))
#define ISLASTSUCCESSOR(state)  (LAST_STATE_MASK &(state))
#define ISTERMINAL(state)       ((state) == PARSE_TERMINAL_STATE)

/*---------------------------------------------------------------------------*
 * parseTable_t is a structure which characterizes the NFA which PARSE 
 * simulates to parse the recognition hypothesis object.  A parse table is 
 * an array of pointers to ParseStates which reside in the state data storage 
 * area immediately following the array.  The array is ordered by state number,
 * such that the indices into the array are the PARSE_STATE_ID's of the states
 * they index.
 *---------------------------------------------------------------------------*/

typedef struct parseTable_s {
  UShort stateCount;
  UInt  *stateOffsets;
  UChar *stateData;
} parseTable_t;

/* My internal function prototypes */
static PARSESTATE PARSE_state(PARSETABLE parseTable, UShort stateIdx);
static Bool       PARSE_nullCloseBudList(PARSE parse, PARSETABLE parseTable);
static Bool       PARSE_addDaughters(PARSE parse, ParsePos_t pos, BUDSEL motherBud,
                                     UShort wordId, PARSETABLE parseTable);
static Bool       PARSE_growPruneTree(PARSE parse, UShort wordId, PARSETABLE parseTable);
static UShort     PARSE_principalBud(PARSE parse, TREENODE *principalBud, PARSETABLE parseTable);
static void       PARSE_printBuds(PARSE parse);

typedef struct parse_s {
  treeNode_t  result;                   /* Tree of state transitions */
  budsEl_t    budList;                  /* List of pointers to tips of branches*/
  TREE        tree;                     /* TREE and BUDS free list objects */
  BUDS        buds;
} parse_t;

PARSE
PARSE_create()
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a PARSE object containing initialized TREE and BUDS objects
 * Return: Pointer to the new object
 *---------------------------------------------------------------------------*/
{
  PARSE parse;

  parse = (PARSE) VBX_calloc(1, sizeof(parse_t));

  return(parse);
}

void
PARSE_destroy(PARSE parse)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy the given PARSE object
 *---------------------------------------------------------------------------*/
{
  if (parse != NULL) {
    if (parse->buds != NULL)
      BUDS_destroy(parse->buds);
    if (parse->tree != NULL)
      TREE_destroy(parse->tree);
    VBX_free(parse);
  }
}

Bool 
PARSE_parse(PARSE parse, HYP hyp, UInt maxWId, PARSETABLE parseTable, Ptr * parsePtr, UShort * index)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return: parsePtr pointing to leaf node (bud) of a non-branching tree 
 *        (stalk) which traces the parse.  If the parse fails, parsePtr
 *        is set to NULL.  If the parser exhausts tree nodes or bud elements,
 *        return FALSE, otherwise return TRUE
 *---------------------------------------------------------------------------*/
{
  TREENODE  * parseResult = (TREENODE *) parsePtr;
  UShort      wordIdx;            /* Index of word in recog sequence */
  UInt        wordCount;
  WRDHYP      words;

  VBX_DEBUG(VBX_print("Beginning parse()\n"));
  VBX_DEBUG(VBX_print("Address of recognition hypothesis: %p\n", hyp));
  VBX_DEBUG(VBX_print("Address of parse table is          %p\n", parseTable));
  assert(parse != NULL);

  /* Set *parseResult to its unsuccessful value */
 *parseResult = (TREENODE) NULL;

  if (parse->tree == NULL) {
    parse->tree = TREE_create();
    if (parse->tree == NULL)
      return(FALSE);           /* can't set up the tree memory */
    VBX_DEBUG(VBX_print("TREE object initialized\n"));
  }
  if (!TREE_initialize(parse->tree))
    return(FALSE);

  if (parse->buds == NULL) {
    parse->buds = BUDS_create();
    if (parse->buds == NULL)
      return(FALSE);

    VBX_DEBUG(VBX_print("BUDS object initialized\n"));
  }
  if (!BUDS_initialize(parse->buds))
    return(FALSE);

  /* Set up the root node */
  TREE_nodeSet(&parse->result, PARSE_START_STATE, PARSE_NULL_WORD, (void *) NULL, 0);
  VBX_DEBUG(VBX_print("Root node initialized\n"));

  /* Set up the initial list of bud elements */
  BUDS_elSet(&parse->budList, &parse->budList, &parse->budList, NULL);
  BUDS_elInsert(parse->buds, (BUDSEL) &parse->budList, (TREENODE) &parse->result);

  VBX_DEBUG(VBX_print("Bud pointer list initialized\n"));
  VBX_DEBUG(PARSE_printBuds(parse));

  /* As long as the tree has not been pruned to nothing and the recognition
     sequence has not been exhausted, continue building the tree.  */
  wordIdx = 0;
  words = HYP_words(hyp, &wordCount);
  while ((parse->budList.next != (BUDSEL) & parse->budList) && (wordIdx < wordCount)) {
    if (WRDHYP_wid(words + wordIdx) > maxWId) { /* Ignore non-lexical words */
      wordIdx++;
      continue;
    }

    if (!PARSE_growPruneTree(parse, WRDHYP_wid(words + wordIdx), parseTable))
      return(FALSE);       /* failure */

    VBX_DEBUG(VBX_print("-------------------------------------------------------------\n"));
    VBX_DEBUG(VBX_print("\n"));
    VBX_DEBUG(PARSE_printBuds(parse));
    VBX_DEBUG(VBX_print("\n"));

    #ifdef STATISTICS
    VBX_DEBUG(VBX_print("The maximum number of tree nodes used was %1d\n", TREE_maxUsedNodes(parse->tree)));
    VBX_DEBUG(VBX_print("The maximum number of bud elements used was %d\n", BUDS_maxUsedEl(parse->buds)));
    #endif

    wordIdx++;
  }	

  /* Presumably we have a good tree */
  if ((parse->budList.next != (BUDSEL) &parse->budList) && (wordIdx == wordCount)) {
    if (!PARSE_nullCloseBudList(parse, parseTable))
      return(FALSE);       /* Nope, it fails this test */

    VBX_DEBUG(VBX_print("=============================================================\n"));
    VBX_DEBUG(VBX_print("\n"));
    VBX_DEBUG(PARSE_printBuds(parse));
    VBX_DEBUG(VBX_print("\n"));

    #ifdef STATISTICS
    VBX_print("The maximum number of tree nodes used was %1d\n", TREE_maxUsedNodes(parse->tree));
    VBX_print("The maximum number of bud elements used was %d\n", BUDS_maxUsedEl(parse->buds));
    #endif

    /* looks good; set up the index */
   *index = (UShort) PARSE_principalBud(parse, parseResult, parseTable);

    VBX_DEBUG(VBX_print("-------------------------------------------------------------\n"));
    VBX_DEBUG(VBX_print("Current index value = %u.\n", *index));

    /* invert the stalk */
   *parseResult = TREE_invertStalk(*parseResult);
  } else {
   *parseResult = (TREENODE) NULL;
  }

  return(TRUE);
}

Ptr
PARSE_loadTable(VFILE recFile)
{
  FPARSEHDR   header;
  FPARSEHDRE  hent;
  FPARSEE     entry;
  UShort     *sPtr;
  ULong       blockSize;
  PARSETABLE  parseTbl = (PARSETABLE) NULL;
  Int         i, stateAlloc;

  if (VFILE_SUCCESS == VFILE_readStructure(recFile, &header, 1, RS_FPARSEHDR)) {
    parseTbl = (PARSETABLE) VBX_calloc(1, sizeof(parseTable_t));
    parseTbl->stateCount = header.wStateCount;

    if (parseTbl->stateCount == 0)
      FATAL_BRA_ "Zero state count encountered in parse block - goodbye!\n" _KET;

    /* Allocate and read the offsets for all the states */
    parseTbl->stateOffsets = (UInt *) VBX_calloc(parseTbl->stateCount, sizeof(UInt));
    VFILE_readStructure(recFile, parseTbl->stateOffsets, parseTbl->stateCount, RS_FPARSEHDRE);

    /* Allocate and read the state data */
    blockSize = VFILE_currentBlockSize(recFile);
    assert(blockSize > 0);
    stateAlloc = blockSize - header.lStateOffset;
    assert(stateAlloc > 0 && (stateAlloc % 2) == 0);
    parseTbl->stateData = VBX_calloc(stateAlloc, sizeof(UChar));

    sPtr = (UShort *) parseTbl->stateData;
    for (i = 0; i < stateAlloc / 2; i++) {
      if (VFILE_SUCCESS != VFILE_readStructure(recFile, &entry, 1, RS_FPARSEE))
        FATAL_BRA_ "PARSE_loadTable: Can't read state data entry\n" _KET;
     *sPtr = entry.wStateData;
      sPtr++;
    }
  }

  return((Ptr) parseTbl);
}

void
PARSE_destroyTable(Ptr ptr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Free the memory associated with a PARSETABLE object
 *---------------------------------------------------------------------------*/
{
  PARSETABLE parse = (PARSETABLE) ptr;
  
  if (parse) {
    VBX_free(parse->stateOffsets);
    VBX_free(parse->stateData);
    VBX_free(parse);
  }
}

static PARSESTATE
PARSE_state(PARSETABLE parseTable, UShort stateIdx)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a pointer to the parse state corresponding to the specified index
 * Return: Pointer to the requested state, or NULL if stateIdx is invalid
 *---------------------------------------------------------------------------*/
{
  if (stateIdx >= parseTable->stateCount) {
    SEVERE_BRA_ "PARSE_state: stateIdx out of bounds (%d > %d)\n",
               parseTable->stateCount _KET;
    return((PARSESTATE) NULL);
  } else {
    return((PARSESTATE) ((UChar *) parseTable->stateData +
                         parseTable->stateOffsets[stateIdx]));
  }
}

Bool 
PARSE_nullCloseBudList(PARSE parse, PARSETABLE parseTable)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add to the tree all nodes reachable from buds by NULL transitions,
 * remove from the bud element list pointers to such erstwhile buds, and add
 * pointers to their new daughters
 * Return: If tree nodes/bud elements are exhausted, return FALSE with the
 *         tree and/or bud element list only partially updated.  Otherwise 
 *         return TRUE
 *---------------------------------------------------------------------------*/
{
  BUDSEL budPtr;
  BUDSEL nextBudPtr;

  VBX_DEBUG(VBX_print("NULL CLOSING the bud list\n "));
  assert(parse != NULL);
  VBX_DEBUG(PARSE_printBuds(parse));

  budPtr = parse->budList.next;

  while (budPtr != (BUDSEL) &parse->budList) {
    VBX_DEBUG(VBX_print(" ADDING any daughters to [%u/%u] via NULL\n Before:: ",
                        (budPtr->bud)->nodeStateId, budPtr));
    VBX_DEBUG(PARSE_printBuds(parse));

    /* This will fail only if tree nodes or bud elements are exhausted */
    if (!PARSE_addDaughters(parse, after, budPtr, PARSE_NULL_ARC, parseTable)) {
      SEVERE_BRA_ "PARSE_nullCloseBudList: addDaughters FAILED, giving up!\n" _KET;
      return(FALSE);
    }

    VBX_DEBUG(VBX_print(" After:: "));
    VBX_DEBUG(PARSE_printBuds(parse));
    VBX_DEBUG(VBX_print(" Removing [%u,%u] if it now has any daughters.\n",
                        (budPtr->bud)->nodeStateId, budPtr));

    nextBudPtr = budPtr->next;

    /* Remove budPtr from the bud list if it now has daughters */
    if ((budPtr->bud)->daughterCount)
      BUDS_elFree(parse->buds, budPtr);

    VBX_DEBUG(VBX_print(" After:: "));
    VBX_DEBUG(PARSE_printBuds(parse));

    /* Go on to the next */
    budPtr = nextBudPtr;
  }

  return(TRUE);
}

Bool
PARSE_addDaughters(PARSE parse, ParsePos_t pos, BUDSEL motherBud,
                    UShort wordId, PARSETABLE parseTable)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add all the states reachable from motherBud's state via wordId as daughters 
 * to *motherBud's node in the tree, then add pointers to them to the bud 
 * element list immediately before or after the motherBud, as specified by pos  
 * Return: TRUE, or FALSE if tree nodes and/or bud elements are exhausted
 *---------------------------------------------------------------------------*/
{
  PARSESTATE statePtr;
  UShort     stateId;

  SUCCESSOR    successor;
  UShort *arc;  /* wordId */
  Short lastSuccessor;

  assert(parse != NULL);

  /* Get the state index of the mother node: */
  if ((stateId = (motherBud->bud)->nodeStateId) >=
      parseTable->stateCount) {
    SEVERE_BRA_ "PARSE_addDaughters: stateId out of bounds (%d > %d)\n", 
               stateId, parseTable->stateCount _KET;
    return(FALSE);
  }

  /* Initialize the successor pointer to first successor to this state */
  statePtr = PARSE_state(parseTable, stateId);
  successor = statePtr->successor;

  VBX_DEBUG(VBX_print("  SEARCHING successors to %u for an ARC labelled WORD %u\n",
                      stateId, wordId));

  /* If this is a terminal state, quit immediately */
  if ((lastSuccessor = ISTERMINAL(successor->state)) != 0)
    return(TRUE);

  /* Otherwise, iterate over all successors for this state */
  while (!lastSuccessor) {

    /* Exit after this successor if it is the last one for this state */
    lastSuccessor = ISLASTSUCCESSOR(successor->state);

    /* Initialize arc pointer to first arc to this successor */
    arc = successor->arc;

    /* Search arcs to this successor for one labelled "word" */
    while ((!LASTARC(*arc)) && (WORDONARC(*arc) != wordId) &&
           (WORDONARC(*arc) != DEFAULT_WORD))
      arc++;             /* Move to next arc */

    /* Found a matching arc */
    if ((WORDONARC(*arc) == wordId) || ((WORDONARC(*arc) == DEFAULT_WORD) &&
        (wordId != PARSE_NULL_WORD))) {
      VBX_DEBUG(VBX_print("   Can reach successor state %u from %u via ARC %u\n",
                          STATEID(successor->state),
                         (motherBud->bud)->nodeStateId, WORDONARC(*arc)));

      /* Try to add a daughter. */
      if (!BUDS_elAddDaughter(parse->buds, parse->tree, pos, motherBud,
          (UShort) STATEID(successor->state), wordId))
        return(FALSE);   /* Ran out of available free nodes or bud elements */
    } else {
      VBX_DEBUG(VBX_print("   Cannot reach successor state %u form %u via any ARC\n",
                          STATEID(successor->state),
                         (motherBud->bud)->nodeStateId));
    }

    /* Then find the last arc to this successor */
    while (!LASTARC(*arc++));

    /* The next successor begins after the last arc in the previous one */
    successor = (SUCCESSOR) (arc);
  }

  return(TRUE);
}

Bool 
PARSE_growPruneTree(PARSE parse, UShort wordId, PARSETABLE parseTable)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Adds to the tree all nodes reachable from buds by transitions on wordId, 
 * removes from the bud pointer list pointers to such erstwhile buds, and adds 
 * pointers to the new daughters to the list of bud pointers. If a bud has no 
 * such transitions, it is recursively pruned
 * Return: TRUE if successful, FALSE if tree nodes or bud elements are 
 *         during the attempted operation
 *---------------------------------------------------------------------------*/
{
  BUDSEL budPtr;
  BUDSEL nextBudPtr;

  assert(parse != NULL);
  budPtr = parse->budList.next;

  while (budPtr != (BUDSEL) &parse->budList) {
    if (!PARSE_addDaughters(parse, after, budPtr, PARSE_NULL_ARC, parseTable)) {
      SEVERE_BRA_ "PARSE_growPruneTree: exhausted tree nodes or bud elements\n" _KET;
      return(FALSE);
    }

    if (!PARSE_addDaughters(parse, before, budPtr, wordId, parseTable)) {
      SEVERE_BRA_ "PARSE_growPruneTree: exhausted tree nodes or bud elements\n" _KET;
      return(FALSE);
    }

    nextBudPtr = budPtr->next;

    /* Recursively prune ancestors whose daughters are gone */
    TREE_prune(parse->tree, budPtr->bud);

    /* The bud has daughters or is pruned, so it's no longer a bud */
    BUDS_elFree(parse->buds, budPtr);

    /* Go on to the next */
    budPtr = nextBudPtr;
  }

  return(TRUE);
}

UShort 
PARSE_principalBud(PARSE parse, TREENODE * principalBud, PARSETABLE parseTable)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns with *principalBud pointing to the terminal bud (one representing 
 * a state having no successor states) with the lowest-valued nodeStateId 
 * (state ID), and evaluates to an arbitrary integral index, which it extracts 
 * from the arc[0] field of the lone successor to that state in the PARSETABLE
 * If there are no terminal buds, *principalBud is set to NULL, and
 * NO_TERMINALS is returned
 *---------------------------------------------------------------------------*/
{
  UShort stateId, minState;
  BUDSEL budPtr;

  SUCCESSOR sPtr;

 *principalBud = (TREENODE) NULL;
  minState = parseTable->stateCount;

  for (budPtr = parse->budList.next;
       budPtr != (BUDSEL) & parse->budList;
       budPtr = budPtr->next) {
    stateId = budPtr->bud->nodeStateId;

    sPtr = (SUCCESSOR) &PARSE_state(parseTable, stateId)->successor[0];

    if ((ISTERMINAL(sPtr->state)) && (stateId < minState)) {
      minState = stateId;
     *principalBud = (TREENODE) budPtr->bud;
    }
  }

  if (*principalBud != NULL) {
    sPtr = (SUCCESSOR) &PARSE_state(parseTable, minState)->successor[0];
    return((UShort) *sPtr->arc);
  } else {
    return((UShort) STATEID(NO_TERMINALS));
  }
}

void 
PARSE_printBuds(PARSE parse)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *  Displays a list of buds in readable format
 *---------------------------------------------------------------------------*/
{
  BUDSEL budPtr;

  return;
  if (parse->budList.bud != NULL)
    VBX_print("Buds: <%u>", (parse->budList.bud)->nodeStateId);
  else
    VBX_print("Buds: <NULL>");

  budPtr = parse->budList.next;

  while (budPtr != (BUDSEL) &parse->budList) {
    VBX_print("<%u>", (budPtr->bud)->nodeStateId);
    budPtr = budPtr->next;
  }

  VBX_print("\n");
}

