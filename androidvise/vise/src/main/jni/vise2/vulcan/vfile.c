/*  vfile.c - rec/voi file reading and writing
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: recfile.c[1.0] Mon Apr  7 23:26:48 1997 dvetter@titan saved $
 *
 * Routines for reading and writing Verbex "voice" and "rec" files, which have
 * a block-oriented structure.
 *
 * HISTORY
 * 07-Jul-96 (cav) Rewrote RRLee's old version for use in the SAPI project
 * 17-Jan-97 (dcv) Added the VSWrite functions
 * 04-Nov-97 (ret) Consolidated file and stream methods.
 *---------------------------------------------------------------------------*/

#include "pthr.h"
#include <stdlib.h>
#include <stddef.h>

#include "types.h"
#include "vlbldefs.h"
#include "swap.h"
#include "vfile.h"
#include "recfile.h"

#define   VFD           vfile->handle
#define   VIF           vfile->myIFile
#define   VOPEN(b,m)    VIF->Open(VIF,b,m)
#define   VCLOSE()      VIF->Close(VIF,VFD)
#define   VSEEK(o,m)    VIF->Seek(VIF,VFD,o,m)
#define   VTELL()       VIF->Tell(VIF,VFD)
#define   VREAD(b,s)    VIF->Read(VIF,VFD,b,s)
#define   VWRITE(b,s)   VIF->Write(VIF,VFD,b,s)

#define   MAX(A,B)      ((A)>(B)?(A):(B))
#define   MIN(A,B)      ((A)<(B)?(A):(B))

/* Bytes to Word Macro */
#define MKWORD(l,h) ((V_Uns)(((V_Byte)(l))|(((V_Uns)((V_Byte)(h)))<<8)))

/* Structure Assembly Macro */
#define TABLEENTRY(T) { sizeof(T), countof(sd##T), sd##T }

/* VFILE Structure definition */

typedef struct vfile_s {
  void         *handle;                /* IFile descriptor pointer */
  IFile        *myIFile;               /* IFile interface pointer  */
  IMemory      *myIMem;                /* IMemory interface pointer  */
  FBLOCKHEAD   bhHead;                 /* current block */
  V_ULong      lCurBlockOrigin;        /* file offset to block data */
  V_ULong      lCurBlockOffset;        /* position within block */
  V_ULong      lTextBlockOrigin;       /* remembered position and size */
  V_ULong      lTextBlockSize;         /* of text block */
  V_Bool       bOpen;                  /* open block flag */
  V_Bool       bDirty;                 /* invalid checksum flag */
} vfile_t;

/* General Structure Reader Atom and Header */
typedef struct tSTRUCTDESCATOM {
  V_Byte     ucAtomType;
  V_Byte     ucAtomCount;
  V_Uns      wOffset;
} STRUCTDESCATOM;

typedef struct vfile_sdesc_s {
  V_Uns      wStructureSize;
  V_Uns      wAtomCount;
  STRUCTDESCATOM *sdAtom;
} STRUCTDESC;

/* Atom Types */
#define VFSA_V_Byte         0
#define VFSA_V_Uns          1
#define VFSA_V_ULong        2
#define VFSA_String         3

/* VF Structure Repertoire */
static  STRUCTDESCATOM sdFBLOCKHEAD[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FBLOCKHEAD), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FBLOCKHEAD,wBlockID) } ,
  { VFSA_V_Uns,    1, offsetof(FBLOCKHEAD,wClassID) } ,
  { VFSA_V_ULong,  1, offsetof(FBLOCKHEAD,vck.lBlockSize) } ,
  { VFSA_V_Uns,    1, offsetof(FBLOCKHEAD,vck.wBlockChecksum)},
#endif
};


static  STRUCTDESCATOM sdFKEY[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FKEY), 0 } ,
#else
  { VFSA_V_ULong,  1, offsetof(FKEY, lMask) } ,
#endif
};


static  STRUCTDESCATOM sdFSTRING[] = {
  { VFSA_V_Byte,   1, offsetof(FSTRING,bStringLength) } ,
  { VFSA_String,   1, offsetof(FSTRING,bStringData) } ,
  { VFSA_V_Byte,   0, sizeof(FSTRING) } ,
};


static  STRUCTDESCATOM sdFSWITCH[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FSWITCH), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FSWITCH, wNumberEntries) } ,
#endif
};


static  STRUCTDESCATOM sdFSWITCHE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FSWITCHE), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FSWITCHE, wSwitchNumber) } ,
  { VFSA_V_Uns,    1, offsetof(FSWITCHE, wSwitchValue ) } ,
#endif
};


static  STRUCTDESCATOM sdFUPATHEAD[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FUPATHEAD), 0 } ,
#else
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bVocabNameLength) },
  { VFSA_V_Byte,   EMBEDDED_NAME_SIZE, offsetof(FUPATHEAD,bVocabName) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bGender) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bDiscreteMinCount) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bContinuousMinCount) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wAlwaysFFFF) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wAlwaysZero) },
  { VFSA_V_ULong,  1, offsetof(FUPATHEAD,lWildcardSum) },
  { VFSA_V_ULong,  1, offsetof(FUPATHEAD,lWildcardSumSquares) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wWildcardCount) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wScriptOrigin) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wScriptOffset) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bDateLength) },
  { VFSA_V_Byte,   EMBEDDED_NAME_SIZE, offsetof(FUPATHEAD,bDate) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bTrainScheme) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bNameLength) },
  { VFSA_V_Byte,   EMBEDDED_NAME_SIZE,offsetof(FUPATHEAD,bName) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bReserved) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wNonAPSPassCount) },
  { VFSA_V_Uns,    1, offsetof(FUPATHEAD,wNumberPatterns) },
  { VFSA_V_Byte,   1, offsetof(FUPATHEAD,bNumberParms) },
#endif
};


static  STRUCTDESCATOM sdFUPATPARM[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FUPATPARM), 0 } ,
#else
  { VFSA_V_Byte,   1, offsetof(FUPATPARM,bNumber) },
  { VFSA_V_Uns,    1, offsetof(FUPATPARM,wValue) },
  { VFSA_V_Byte,   1, offsetof(FUPATPARM,bControl) },
#endif
};


static  STRUCTDESCATOM sdFVOCABTRANS[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FVOCABTRANS), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FVOCABTRANS, wNumberWords) },
  { VFSA_V_Uns,    1, offsetof(FVOCABTRANS, wNumberDiscreteWords) },
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loVocabularyName.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loVocabularyName.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loHRFInitiator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loHRFInitiator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loHRFSeparator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loHRFSeparator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loHRFTerminator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loHRFTerminator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loVRFInitiator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loVRFInitiator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loVRFSeparator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loVRFSeparator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loVRFTerminator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loVRFTerminator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loDRFInitiator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loDRFInitiator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loDRFSeparator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loDRFSeparator.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANS, loDRFTerminator.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANS, loDRFTerminator.lOffset) } ,
  { VFSA_V_Uns,    1, offsetof(FVOCABTRANS, wYesChoiceID) } ,
  { VFSA_V_Uns,    1, offsetof(FVOCABTRANS, wNoChoiceID) } ,
  { VFSA_V_Uns,    1, offsetof(FVOCABTRANS, wExitChoiceID) } ,
#endif
};


static  STRUCTDESCATOM sdFVOCABTRANSE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FVOCABTRANSE), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FVOCABTRANSE, wMultipleTemplateID)},
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANSE, bDiscreteWord)},
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANSE, loWordName.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANSE, loWordName.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANSE, loHostTranslation.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANSE, loHostTranslation.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANSE, loVoiceTranslation.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANSE, loVoiceTranslation.lOffset) } ,
  { VFSA_V_Byte,   1, offsetof(FVOCABTRANSE, loDisplayTranslation.bLength) } ,
  { VFSA_V_ULong,  1, offsetof(FVOCABTRANSE, loDisplayTranslation.lOffset) } ,
#endif
};


static  STRUCTDESCATOM sdFSCRIPT[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FSCRIPT), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FSCRIPT, wEntryCnt) },
  { VFSA_V_Byte,   1, offsetof(FSCRIPT, bDiscMinCnt) },
  { VFSA_V_Byte,   1, offsetof(FSCRIPT, bContMinCnt) },
#endif
};


static  STRUCTDESCATOM sdFSCRIPTE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FSCRIPTE), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FSCRIPTE,wGrammarID) },
  { VFSA_V_Uns,    1, offsetof(FSCRIPTE,wStartNode) },
  { VFSA_V_Uns,    1, offsetof(FSCRIPTE,wEndNode) },
  { VFSA_V_Byte,   1, offsetof(FSCRIPTE,bWordCnt) },
#endif
};


static  STRUCTDESCATOM sdFGRAMMAR[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAMMAR), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FGRAMMAR,wVocabulary) },
  { VFSA_V_Uns,    1, offsetof(FGRAMMAR,wDefaultCRTemplate) },
  { VFSA_V_Uns,    1, offsetof(FGRAMMAR,wDefaultHRTemplate) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMAR,bloGrammarName.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMAR,bloGrammarName.lOffset) },
  { VFSA_V_Uns,    1, offsetof(FGRAMMAR,wDefaultVRTemplate) },
  { VFSA_V_Uns,    1, offsetof(FGRAMMAR,wDefaultDRTemplate) },
#endif
};


static  STRUCTDESCATOM sdFGRAPH[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAPH), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FGRAPH,wGraphSize) },
  { VFSA_V_Uns,    1, offsetof(FGRAPH,wGrammarID) },
  { VFSA_V_Uns,    1, offsetof(FGRAPH,wNumberOfArcs) },
  { VFSA_V_Uns,    1, offsetof(FGRAPH,wNumberOfNodes) },
#endif
};

static  STRUCTDESCATOM sdFUPATE[] = {
  { VFSA_V_ULong,  2, offsetof(FUPATE,lSum) },
  { VFSA_V_Uns,    3, offsetof(FUPATE,wCount) },
  { VFSA_V_Byte,   1, offsetof(FUPATE,bNKernWordMod) },
  { VFSA_String,   2, offsetof(FUPATE,bKernelDwells) } ,
  { VFSA_V_Byte,   1, offsetof(FUPATE,bTemplateCount) },
  { VFSA_String,   16,offsetof(FUPATE,bTemplate) },
  { VFSA_V_Byte,   1, offsetof(FUPATE,bNameCount) },
  { VFSA_String,   1, offsetof(FUPATE,bName) },
  { VFSA_V_Byte,   0, sizeof(FUPATE) },
};

static  STRUCTDESCATOM sdFGENINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGENINFO), 0 },
#else
  { VFSA_V_ULong,  3, offsetof(FGENINFO,uGinclude) },
  { VFSA_V_Uns,    1, offsetof(FGENINFO,uNumIncludes) },
  { VFSA_V_ULong,  1, offsetof(FGENINFO,uIncludes) },
  { VFSA_V_Uns,    5, offsetof(FGENINFO,uNumGrams) },
#endif
};

static  STRUCTDESCATOM sdFGRAMINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAMINFO), 0 } ,
#else
  { VFSA_V_ULong,  3, offsetof(FGRAMINFO,uGramName) },
  { VFSA_V_Uns,    4, offsetof(FGRAMINFO,uInactive) },
#endif
};

static STRUCTDESCATOM sdFPHRGENINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FPHRGENINFO), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FPHRGENINFO, uNumPhrases) },
#endif
};


static STRUCTDESCATOM sdFPHRASEINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FPHRASEINFO), 0 },
#else
  { VFSA_V_ULong,  1, offsetof(FPHRASEINFO, uPhraseHndl) },
  { VFSA_V_Uns,    1, offsetof(FPHRASEINFO, uFileIndex) },
  { VFSA_V_Uns,    1, offsetof(FPHRASEINFO, uFlags) },
  { VFSA_V_Uns,    1, offsetof(FPHRASEINFO, uStage) },
#endif
};


static STRUCTDESCATOM sdFLISTGENINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FLISTGENINFO), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FLISTGENINFO, uNumLists) },
#endif
};


static STRUCTDESCATOM sdFLISTINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FLISTINFO), 0 } ,
#else
  { VFSA_V_ULong,  1, offsetof(FLISTINFO, uListHndl) },
  { VFSA_V_Uns,    1, offsetof(FLISTINFO, uFlags) },
  { VFSA_V_Uns,    1, offsetof(FLISTINFO, uStage) },
  { VFSA_V_Uns,    1, offsetof(FLISTINFO, uMinCount) },
  { VFSA_V_Uns,    1, offsetof(FLISTINFO, uMaxCount) },
  { VFSA_V_ULong,  1, offsetof(FLISTINFO, uListPhrase) },
  { VFSA_V_Uns,    1, offsetof(FLISTINFO, uListFileIndex) },
  { VFSA_V_Uns,    1, offsetof(FLISTINFO, uNumElements) },
  { VFSA_V_ULong,  1, offsetof(FLISTINFO, uFirstElement) },
#endif
};

static STRUCTDESCATOM sdFGRAPHARC[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAPHARC), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FGRAPHARC, uSourceNode) },
  { VFSA_V_Uns,    1, offsetof(FGRAPHARC, uDestNode)   },
  { VFSA_V_Uns,    1, offsetof(FGRAPHARC, uNumberOfWords)  },
#endif
};

static STRUCTDESCATOM sdFIDENT[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FIDENT), 0 } ,
#else
  { VFSA_V_Byte,   1, offsetof(FIDENT,bloApplicationName.bLength) },
  { VFSA_V_ULong,  1, offsetof(FIDENT,bloApplicationName.lOffset) },
  { VFSA_V_Uns,    1, offsetof(FIDENT,wHostStartMsg) },
  { VFSA_V_Uns,    1, offsetof(FIDENT,wVoiceStartMsg) },
  { VFSA_V_Byte,   1, offsetof(FIDENT,bVCC) },
  { VFSA_V_Byte,   1, offsetof(FIDENT,bloVersionName.bLength) },
  { VFSA_V_ULong,  1, offsetof(FIDENT,bloVersionName.lOffset) },
  { VFSA_V_ULong,  1, offsetof(FIDENT,ulVersionKey) },
  { VFSA_V_Uns,    1, offsetof(FIDENT,wDisplayStartMsg) },
  { VFSA_V_Uns,    1, offsetof(FIDENT,wFirstGrammar) },
#endif
};


static  STRUCTDESCATOM sdFGRAMMARTRANS[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAMMARTRANS), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FGRAMMARTRANS, wNumberWords) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loHRFInitiator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loHRFInitiator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loHRFSeparator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loHRFSeparator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loHRFTerminator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loHRFTerminator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loVRFInitiator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loVRFInitiator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loVRFSeparator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loVRFSeparator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loVRFTerminator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loVRFTerminator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loDRFInitiator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loDRFInitiator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loDRFSeparator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loDRFSeparator.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANS, loDRFTerminator.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANS, loDRFTerminator.lOffset) },
#endif
};

static  STRUCTDESCATOM sdFGRAMMARTRANSE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAMMARTRANSE), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FGRAMMARTRANSE, wWordID) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANSE, loHostTranslation.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANSE, loHostTranslation.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANSE, loVoiceTranslation.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANSE, loVoiceTranslation.lOffset) },
  { VFSA_V_Byte,   1, offsetof(FGRAMMARTRANSE, loDisplayTranslation.bLength) },
  { VFSA_V_ULong,  1, offsetof(FGRAMMARTRANSE, loDisplayTranslation.lOffset) } ,
#endif
};

static  STRUCTDESCATOM sdFGRAPHARCE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FGRAPHARCE), 0 } ,
#else
  { VFSA_V_Uns,    1, offsetof(FGRAPHARCE, wWordID) },
#endif
};

static  STRUCTDESCATOM sdFV_Uns[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FV_Uns), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FV_Uns, wWord) },
#endif
};

static  STRUCTDESCATOM sdFV_ULong[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FV_ULong), 0 },
#else
  { VFSA_V_ULong,  1, offsetof(FV_ULong, lLong) },
#endif
};

static STRUCTDESCATOM sdFHINTGENINFO[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FHINTGENINFO), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FHINTGENINFO, uNumHints) },
#endif
} ;

static STRUCTDESCATOM sdFHINTINFOSTRUCT[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FHINTGENINFO), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FHINTINFOSTRUCT, uType) },
  { VFSA_V_ULong,  1, offsetof(FHINTINFOSTRUCT, uHandleOffset) },
  { VFSA_V_ULong,  1, offsetof(FHINTINFOSTRUCT, uHintOffset) },
  { VFSA_V_ULong,  1, offsetof(FHINTINFOSTRUCT, uDescripOffset) },
#endif
} ;

static STRUCTDESCATOM sdFPARSEHDR[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FPARSEHDR), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FPARSEHDR, wStateCount) },
  { VFSA_V_ULong,  1, offsetof(FPARSEHDR, lStateOffset) },
#endif
};

static STRUCTDESCATOM sdFPARSEHDRE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FPARSEHDRE), 0 },
#else
  { VFSA_V_ULong,  1, offsetof(FPARSEHDRE, lStateOffset) },
#endif
};

static STRUCTDESCATOM sdFPARSEE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FPARSEE), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FPARSEE, wStateData) },
#endif
};

static STRUCTDESCATOM sdFTEMPLATEHDR[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FTEMPLATEHDR), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FTEMPLATEHDR, wTemplateCount) },
  { VFSA_V_ULong,  1, offsetof(FTEMPLATEHDR, lTemplateOffset) },
#endif
};

static STRUCTDESCATOM sdFTEMPLATEHDRE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FTEMPLATEHDRE), 0 },
#else
  { VFSA_V_ULong,  1, offsetof(FTEMPLATEHDRE, lTemplateOffset) },
#endif
};

static STRUCTDESCATOM sdFTEMPLATEE[] = {
#ifdef PACKED
  { VFSA_V_Byte,   sizeof(FTEMPLATEE), 0 },
#else
  { VFSA_V_Uns,    1, offsetof(FTEMPLATEE, wTemplateData) },
#endif
};

static  STRUCTDESC sdTable[] =
{
  /* 0*/    TABLEENTRY(FBLOCKHEAD),
  /* 1*/    TABLEENTRY(FKEY),
  /* 2*/    TABLEENTRY(FSTRING),
  /* 3*/    TABLEENTRY(FSWITCH),
  /* 4*/    TABLEENTRY(FSWITCHE),
  /* 5*/    TABLEENTRY(FUPATHEAD),
  /* 6*/    TABLEENTRY(FUPATPARM),
  /* 7*/    TABLEENTRY(FVOCABTRANS),
  /* 8*/    TABLEENTRY(FVOCABTRANSE),
  /* 9*/    TABLEENTRY(FSCRIPT),
  /*10*/    TABLEENTRY(FSCRIPTE),
  /*11*/    TABLEENTRY(FGRAMMAR),
  /*12*/    TABLEENTRY(FGRAPH),
  /*13*/    TABLEENTRY(FUPATE),
  /*14*/    TABLEENTRY(FGENINFO),
  /*15*/    TABLEENTRY(FGRAMINFO),
  /*16*/    TABLEENTRY(FPHRGENINFO),
  /*17*/    TABLEENTRY(FPHRASEINFO),
  /*18*/    TABLEENTRY(FLISTGENINFO),
  /*19*/    TABLEENTRY(FLISTINFO),
  /*20*/    TABLEENTRY(FGRAPHARC),
  /*21*/    TABLEENTRY(FIDENT),
  /*22*/    TABLEENTRY(FGRAMMARTRANS),
  /*23*/    TABLEENTRY(FGRAMMARTRANSE),
  /*24*/    TABLEENTRY(FGRAPHARCE),
  /*25*/    TABLEENTRY(FV_Uns),
  /*26*/    TABLEENTRY(FV_ULong),
  /*27*/    TABLEENTRY(FHINTGENINFO),
  /*28*/    TABLEENTRY(FHINTINFOSTRUCT),
  /*29*/    TABLEENTRY(FPARSEHDR),
  /*30*/    TABLEENTRY(FPARSEHDRE),
  /*31*/    TABLEENTRY(FPARSEE),
  /*32*/    TABLEENTRY(FTEMPLATEHDR),
  /*33*/    TABLEENTRY(FTEMPLATEHDRE),
  /*34*/    TABLEENTRY(FTEMPLATEE),
};

#define STRUCTURETYPE_MAX   countof(sdTable)

static  V_Uns wAtomSize[] = {
  sizeof(V_Byte),
  sizeof(V_Uns),
  sizeof(V_ULong),
  sizeof(V_Byte),
};

#define VFSA_ATOM_MAX countof(wAtomSize)
#define FILE_ADM_OVERHEAD   VCHECKSUM_SIZE   /* size of size + checksum */


/* My internal function prototypes */
static V_Err  ChecksumWrite(VFILE vfile);

/*----------------------------------------------------------------------------*
 * Initialize checksum structure. If pBuffer argument is NULL, *pChecksum is 
 * initialized to 0, else member values are unpacked from first VCHECKSUM_SIZE 
 * bytes of bufPtr
 *----------------------------------------------------------------------------*/
static V_Err  ChecksumOpen(VCHECKSUM * checksum, V_Byte * buffer);
static V_Err  ChecksumContinue(VCHECKSUM * checksum, V_ULong bufferSize, V_Byte * buffer);
/*----------------------------------------------------------------------------*
 * Adjust checksum for output.  Given a non-NULL, bufPtr, an ordered, packed 
 * copy of the checksum is written into VCHECKSUM_SIZE bytes of bufPtr
 *----------------------------------------------------------------------------*/
static V_Err  ChecksumClose(VCHECKSUM * checksum, V_Byte * buffer);


VFILE 
VFILE_open(IMemory *thisIMem, IFile *thisIFile, void * fileHandle, V_Uns mode)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Allocate memory for instance descriptor, open file in mode specified,
 * get file size.
 * Return: Pointer to the heap-allocated descriptor
 *---------------------------------------------------------------------------*/
{
  VFILE vfile = thisIMem->Calloc(thisIMem, 1, sizeof(vfile_t), SLOW_MEMORY);

  vfile->myIFile = thisIFile;
  VFD = VOPEN(fileHandle, mode);
  if (NULL == VFD) {
    thisIMem->Free(thisIMem, vfile);
    return NULL;
  }

  vfile->myIMem = thisIMem;
  return vfile;
}


V_Err
VFILE_close(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Close file and free file descriptor
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Err status;

  if (VFILE_SUCCESS != (status = VCLOSE()))
    return status;

  vfile->myIMem->Free(vfile->myIMem, vfile);

  return VFILE_SUCCESS;
}


IFile *
VFILE_interface(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the IFile interface that this VFILE uses
 * Return: IFile
 *---------------------------------------------------------------------------*/
{
  if (vfile)
    return(vfile->myIFile);
  else
    return(NULL);
}


Ptr
VFILE_streamData(VFILE vfile, UInt *dataSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the data from a stream and report its size
 * Return: Pointer to the stream data
 *---------------------------------------------------------------------------*/
{
  VSTREAM vs;

  if (vfile != NULL && ((vs = (VSTREAM) vfile->handle) != NULL)) {
    *dataSize = vs->lFileSize;
    return(vs->streamPtr);
  } else {
    *dataSize = 0;
  }

  return NULL;
}


V_Err    
VFILE_rewind(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the file pointer to the beginning of the file
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Err status;

  if (VFILE_SUCCESS != (status = VSEEK(0, RECFILE_SEEK_SET)))
    return status;

  vfile->bhHead.wBlockID = 0;
  vfile->bhHead.wClassID = 0;
  vfile->bhHead.vck.lBlockSize = 0;
  vfile->bhHead.vck.wBlockChecksum = 0;
  vfile->lCurBlockOrigin = 0;
  vfile->lCurBlockOffset = 0;

  return VFILE_SUCCESS;
}


V_Err
VFILE_placeMark(VFILE vfile, VFILE_MARK *pMark)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Record the current position in the file for a possible return to the same
 * position later
 * Return: SUCCESS unconditionally.
 *---------------------------------------------------------------------------*/
{
  *pMark = VTELL();
  return VFILE_SUCCESS;
}


V_Err
VFILE_gotoMark(VFILE vfile, VFILE_MARK *pMark)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Jump to a previously noted location in file
 * Return:  Status of operation
 *---------------------------------------------------------------------------*/
{
  VSEEK(*pMark, RECFILE_SEEK_SET);
  if (*pMark == (V_ULong) VTELL())
    return VFILE_SUCCESS;

  return VFILE_INVALID_MARK;
}


V_ULong  
VFILE_markDifference(VFILE_MARK *pMark1, VFILE_MARK *pMark2)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Compute difference in marks in BYTES
 * Return:  Mark difference
 *---------------------------------------------------------------------------*/
{
  return((V_ULong) * pMark2) - ((V_ULong) * pMark1);
}


FBLOCKHEAD *
VFILE_currentBlockHdr(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return a pointer to the header for the current block
 *---------------------------------------------------------------------------*/
{
  return(&vfile->bhHead);
}


V_ULong
VFILE_currentBlockSize(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the size in bytes of the current block
 *---------------------------------------------------------------------------*/
{
  V_ULong size = 0;

  if (vfile && &vfile->bhHead)
    size = vfile->bhHead.vck.lBlockSize;

  return size;
}


V_Err    
VFILE_getNextBlock(VFILE vfile, V_Uns * uBlockID, V_Uns * uClassID, V_ULong * ulSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Step to the next block in the file
 * Return:  Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns  uFault;
  //VBX_print("vfile->lCurBlockOrigin: %d, vfile->bhHead.vck.lBlockSize: %d\n", vfile->lCurBlockOrigin, vfile->bhHead.vck.lBlockSize);
  if (VSEEK(vfile->lCurBlockOrigin + vfile->bhHead.vck.lBlockSize, RECFILE_SEEK_SET))
    return VFILE_SEEK_ERROR;

  if (VFILE_SUCCESS == (uFault = VFILE_readStructure(vfile, &vfile->bhHead, 1, RS_FBLOCKHEAD))) {
    vfile->bhHead.vck.lBlockSize -= FILE_ADM_OVERHEAD;

   *uBlockID = vfile->bhHead.wBlockID;
   *uClassID = vfile->bhHead.wClassID;
   *ulSize = vfile->bhHead.vck.lBlockSize;

    vfile->lCurBlockOrigin = VTELL();
    vfile->lCurBlockOffset = 0;

    if (TR_TEXT == vfile->bhHead.wBlockID) {
      vfile->lTextBlockSize = vfile->bhHead.vck.lBlockSize;
      vfile->lTextBlockOrigin = vfile->lCurBlockOrigin;
    }
  } else {
   *uBlockID = 0;
   *uClassID = 0;
   *ulSize = 0;
  }
  return uFault;
}


V_Err    
VFILE_readStructure(VFILE vfile, void *pStructure, V_Uns wCount, V_Uns sType)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read one of the defined structure types
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  if (sType >= STRUCTURETYPE_MAX)
    return VFILE_INVALID_STRUCTURE_TYPE;

  return VFILE_readGeneralStructure(vfile, pStructure, wCount, sdTable + sType);
}


V_Err    
VFILE_readGeneralStructure(VFILE vfile, void *pStructure, V_Uns wCount, VFILE_SDESC pSDesc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read a structure from the current location in the file given the structure
 * description provided
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Int           i, j;
  V_Char          *pData;
  V_Uns           nBytes, nBytesRead;
  STRUCTDESCATOM  *sdAtom;

  while (wCount--) {
    sdAtom = pSDesc->sdAtom;

    for (i = 0; i < pSDesc->wAtomCount; i++, sdAtom++) {
      if (sdAtom->ucAtomType >= VFSA_ATOM_MAX)
        return VFILE_INVALID_STRUCTURE_ATOM;

      nBytes = wAtomSize[sdAtom->ucAtomType] * sdAtom->ucAtomCount;
      pData = ((V_Char *) pStructure) + sdAtom->wOffset;

      if (sdAtom->ucAtomType == VFSA_String) {
        V_Uns    wMaxBytes;
        V_Byte * puItemCount;

        puItemCount = ((V_Byte *) pStructure) + ((sdAtom - 1)->wOffset);

        nBytes *= (V_Uns) *puItemCount;
        wMaxBytes = (sdAtom + 1)->wOffset - sdAtom->wOffset;

        /* Changed from >= as per Alex's notes RRLee 4/11/95 */
        if (nBytes > wMaxBytes)
          return VFILE_INVALID_VARIABLE_SIZE;
      }

      nBytesRead = (nBytes > 0) ? VREAD(pData, nBytes) : 0;

      if (nBytes != nBytesRead) {
        return VFILE_END_OF_FILE;
      }
      vfile->lCurBlockOffset += nBytes;

      switch (sdAtom->ucAtomType) {
      case VFSA_V_Uns:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPW(&((V_Uns *)pData)[j]);
        break;
      case VFSA_V_ULong:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPL(&((V_ULong *)pData)[j]);
        break;
      }
    }
    pStructure = ((V_Char *) pStructure) + pSDesc->wStructureSize;
  }
  return VFILE_SUCCESS;
}


V_Err
VFILE_getText(VFILE vfile, FLOFF * pText, V_Byte * szBuffer)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Translate length/offset to text
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns     uFault;
  V_Uns     uBlockID;
  V_Uns     uClassID;
  V_ULong   ulSize;

 *szBuffer = 0;
  if (pText->bLength == 0)
    return VFILE_SUCCESS;

  if (vfile->lTextBlockOrigin == 0) {
    while (vfile->lTextBlockOrigin == 0) {
      if (VFILE_SUCCESS !=
          (uFault = VFILE_getNextBlock(vfile, &uBlockID, &uClassID, &ulSize)))
        return uFault;
      }
    } else {
      vfile->bhHead.wBlockID = TR_TEXT;
      vfile->bhHead.wClassID = 0;
      vfile->bhHead.vck.lBlockSize = vfile->lTextBlockSize;
      vfile->lCurBlockOrigin = vfile->lTextBlockOrigin;
    }

  if (vfile->lTextBlockSize < pText->lOffset + pText->bLength)
    return VFILE_INVALID_TEXT;

  if (VSEEK(vfile->lTextBlockOrigin + pText->lOffset, RECFILE_SEEK_SET))
    return VFILE_SEEK_ERROR;

  if (pText->bLength != (V_Byte) VREAD(szBuffer, pText->bLength))
    return VFILE_FILE_READ_ERROR;

  szBuffer[pText->bLength] = 0;
  vfile->lCurBlockOffset = VTELL() - vfile->lTextBlockOrigin;

  return VFILE_SUCCESS;
}


V_Err
VFILE_readByte(VFILE vfile, V_ULong byteCount, V_Byte * pData)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Read a sequence of bytes from file
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_ULong nBytesRead;

  nBytesRead = VREAD(pData, byteCount);
  if (byteCount != nBytesRead) {
      return VFILE_END_OF_FILE;
  }

  vfile->lCurBlockOffset = VTELL() - vfile->lCurBlockOrigin;
  return VFILE_SUCCESS;
}

#define WMOD_BYTES_TO_WM        15      /* offset of word model byte */
#define WMOD_BYTES_WM           UPAT_WM_SIZE
#define WMOD_BYTES_TEM          UPAT_NUM_PARMS


V_Err
VFILE_extractWordStats(VFILE vfile, WORDSTATS *pwWordStats, V_Byte *buf)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Pick up word model and measure
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  static V_Uns wByteMultiplier[] = {1, WMOD_BYTES_WM, WMOD_BYTES_TEM, 1};
  static V_Uns wByteAdder[] =      {WMOD_BYTES_TO_WM, 1, 1, 0};
  V_Uns i;
  V_Byte  bCount;
  V_Uns wReadBytes;
  V_Uns wFault;

  pwWordStats->wDataSize = 0;

  bCount = 0;
  for (i = 0; i < 4; i++) {
    wReadBytes = (wByteMultiplier[i] * (V_Uns) bCount) + wByteAdder[i];

    if (VFILE_SUCCESS != (wFault = VFILE_readByte(vfile, wReadBytes, buf)))
      return wFault;

    pwWordStats->wNameOffset = pwWordStats->wDataSize;
    pwWordStats->bNameSize = bCount;
    pwWordStats->wDataSize += wReadBytes;
    bCount = buf[wReadBytes - 1];
    buf += wReadBytes;
  }
  return VFILE_SUCCESS;
}


V_Err
VFILE_openBlock(VFILE vfile, V_Uns uBlockID, V_Uns uClassID)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Perform start of block processing, initialize checksum, etc.
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns   uFault;
  V_Uns   wData[2];

  if (vfile->bOpen && (VFILE_SUCCESS != (uFault = VFILE_closeBlock(vfile))))
    return uFault;

  wData[0] = uBlockID;
  wData[1] = uClassID;
  BSWAPW(&wData[0]);
  BSWAPW(&wData[1]);

  if (sizeof(wData) != VWRITE(wData, sizeof(wData)))
    return VFILE_FILE_WRITE_ERROR;

  vfile->bhHead.wBlockID = uBlockID;
  vfile->bhHead.wClassID = uClassID;
  ChecksumOpen(&vfile->bhHead.vck, NULL);

  if (VFILE_SUCCESS != (uFault = ChecksumWrite(vfile)))
    return uFault;

  vfile->bOpen = 1;
  vfile->lCurBlockOrigin = VTELL();
  vfile->lCurBlockOffset = 0;
  return VFILE_SUCCESS;
}


V_Err    
VFILE_openCurrentBlock(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Open the current block for over-writing.
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns   uFault;

  if (vfile->bOpen && VFILE_SUCCESS != (uFault = VFILE_closeBlock(vfile)))
    return uFault;

  VSEEK(vfile->lCurBlockOrigin - VCHECKSUM_SIZE, RECFILE_SEEK_SET);

  ChecksumOpen(&vfile->bhHead.vck, NULL);

  if (VFILE_SUCCESS != (uFault = ChecksumWrite(vfile)))
    return uFault;

  vfile->bOpen = 1;
  vfile->lCurBlockOrigin = VTELL();
  vfile->lCurBlockOffset = 0;
  return VFILE_SUCCESS;
}


V_Err
VFILE_closeBlock(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Perform end of block processing, including closure of checksum and update
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns     uFault;
  V_Byte    bBuffer[256];
  V_Uns     wBytesRead;
  V_Uns     wBytesToRead;
  V_ULong   lBlockSize;

  if (vfile->bOpen == 0)
    return VFILE_BLOCK_CLOSED;

  if (vfile->bDirty) {
    if (VSEEK(vfile->lCurBlockOrigin, RECFILE_SEEK_SET))
      return VFILE_SEEK_ERROR;

    lBlockSize = vfile->bhHead.vck.lBlockSize;
    ChecksumOpen(&vfile->bhHead.vck, NULL);

    while (lBlockSize) {
      wBytesToRead = (V_Uns) MIN((V_ULong) sizeof (bBuffer), lBlockSize);
      wBytesRead = VREAD(bBuffer, wBytesToRead);
      if (wBytesRead != wBytesToRead)
        return VFILE_FILE_READ_ERROR;

      ChecksumContinue(&vfile->bhHead.vck, wBytesRead, bBuffer);
      lBlockSize -= wBytesRead;
    }
    vfile->bDirty = 0;
  }

  if (VSEEK(vfile->lCurBlockOrigin - VCHECKSUM_SIZE, RECFILE_SEEK_SET))
    return VFILE_SEEK_ERROR;

  lBlockSize = vfile->bhHead.vck.lBlockSize;
  ChecksumClose(&vfile->bhHead.vck, NULL);

  if (VFILE_SUCCESS != (uFault = ChecksumWrite(vfile)))
    return uFault;

  if (VSEEK(vfile->lCurBlockOrigin + lBlockSize, RECFILE_SEEK_SET))
    return VFILE_SEEK_ERROR;

  vfile->bOpen = 0;
  return VFILE_SUCCESS;
}


V_Err
VFILE_writeByte(VFILE vfile, V_ULong byteCount, V_Byte *pData)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Output a sequence of bytes to file
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_ULong lCurrentSize;

  if (vfile->bOpen == 0)
    return VFILE_BLOCK_CLOSED;

  if (byteCount != VWRITE(pData, byteCount))
    return VFILE_FILE_WRITE_ERROR;

  vfile->lCurBlockOffset = VTELL() - vfile->lCurBlockOrigin;

  /* save current size in case of dirty */
  lCurrentSize = vfile->bhHead.vck.lBlockSize;

  ChecksumContinue(&vfile->bhHead.vck, byteCount, pData);

  if (vfile->bhHead.vck.lBlockSize != vfile->lCurBlockOffset) {
    vfile->bhHead.vck.lBlockSize = MAX(lCurrentSize, vfile->lCurBlockOffset);
    vfile->bDirty = 1;
  }

  return VFILE_SUCCESS;
}


V_Err
VFILE_writeStructure(VFILE vfile, void *pStructure, V_Uns wCnt, V_Uns wType)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Output one of the defined structures
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  if (wType >= STRUCTURETYPE_MAX)
    return VFILE_INVALID_STRUCTURE_TYPE;

  return VFILE_writeGeneralStructure(vfile, pStructure, wCnt, sdTable + wType);
}


V_Err    
VFILE_writeGeneralStructure(VFILE vfile, void *pStructure, V_Uns wCnt, VFILE_SDESC pSDesc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Output all members of structure described by the STRUCTDESC
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns           i, j;
  V_ULong         lCurrentSize;
  V_Char          * pData;
  V_Uns           nBytes, nBytesWritten;
  STRUCTDESCATOM  * sdAtom;

  if (vfile->bOpen == 0)
    return VFILE_BLOCK_CLOSED;

  lCurrentSize = vfile->bhHead.vck.lBlockSize;

  while (wCnt--) {
    sdAtom = pSDesc->sdAtom;

    for (i = 0; i < pSDesc->wAtomCount; i++, sdAtom++) {
      if (sdAtom->ucAtomType >= VFSA_ATOM_MAX)
        return VFILE_INVALID_STRUCTURE_ATOM;

      nBytes = wAtomSize[sdAtom->ucAtomType] * sdAtom->ucAtomCount;
      pData = ((V_Char *) pStructure) + sdAtom->wOffset;

      switch (sdAtom->ucAtomType) {
      case VFSA_V_Uns:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPW(&((V_Uns *)pData)[j]);
        break;

      case VFSA_V_ULong:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPL(&((V_ULong *)pData)[j]);
        break;
      }

      if (sdAtom->ucAtomType == VFSA_String) {
        V_Byte *puItemCount;

        puItemCount = ((V_Byte *) pStructure) + ((sdAtom - 1)->wOffset);
        nBytes *= (V_Uns) *puItemCount;
      }

      if (nBytes > 0) {
        nBytesWritten = VWRITE(pData, nBytes);
        ChecksumContinue (&vfile->bhHead.vck, nBytesWritten, (V_Byte *) pData);
      } else
        nBytesWritten = 0;

      switch (sdAtom->ucAtomType) {
      case VFSA_V_Uns:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPW(&((V_Uns *)pData)[j]);
        break;

      case VFSA_V_ULong:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPL(&((V_ULong *)pData)[j]);
        break;
      }

      if (nBytes != nBytesWritten)
        return VFILE_FILE_WRITE_ERROR;
    }
    pStructure = ((V_Char *) pStructure) + pSDesc->wStructureSize;
  }

  vfile->lCurBlockOffset = VTELL() - vfile->lCurBlockOrigin;
  if (vfile->bhHead.vck.lBlockSize != vfile->lCurBlockOffset) {
    vfile->bhHead.vck.lBlockSize = MAX(lCurrentSize, vfile->lCurBlockOffset);
    vfile->bDirty = 1;
  }

  return VFILE_SUCCESS;
}


V_Err
VFILE_insertStructure(VFILE vfile, void *pStructure, V_Uns wCnt, V_Uns wType)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Rudely insert one of the defined structures at the current position,
 * regardless of what block we are in and whether it is open.
 * THIS OPERATION TRASHES THE CHECKSUM!
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  if (wType >= STRUCTURETYPE_MAX)
    return VFILE_INVALID_STRUCTURE_TYPE;

  return VFILE_insertGeneralStructure(vfile, pStructure, wCnt, sdTable + wType);
}


V_Err    
VFILE_insertGeneralStructure(VFILE vfile, void *pStructure, V_Uns wCnt, VFILE_SDESC pSDesc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert all members of structure described by the STRUCTDESC.  
 * THIS OPERATION TRASHES THE CHECKSUM!
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  V_Uns           i, j;
  V_Char          * pData;
  V_Uns           nBytes, nBytesWritten;
  STRUCTDESCATOM  * sdAtom;

  while (wCnt--) {
    sdAtom = pSDesc->sdAtom;

    for (i = 0; i < pSDesc->wAtomCount; i++, sdAtom++) {
      if (sdAtom->ucAtomType >= VFSA_ATOM_MAX)
        return VFILE_INVALID_STRUCTURE_ATOM;

      nBytes = wAtomSize[sdAtom->ucAtomType] * sdAtom->ucAtomCount;
      pData = ((V_Char *) pStructure) + sdAtom->wOffset;

      switch (sdAtom->ucAtomType) {
      case VFSA_V_Uns:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPW(&((V_Uns *)pData)[j]);
        break;

      case VFSA_V_ULong:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPL(&((V_ULong *)pData)[j]);
        break;
      }

      if (sdAtom->ucAtomType == VFSA_String) {
        V_Byte *puItemCount;

        puItemCount = ((V_Byte *) pStructure) + ((sdAtom - 1)->wOffset);
        nBytes *= (V_Uns) *puItemCount;
      }

      if (nBytes > 0) {
        nBytesWritten = VWRITE(pData, nBytes);
      } else
        nBytesWritten = 0;

      switch (sdAtom->ucAtomType) {
      case VFSA_V_Uns:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPW(&((V_Uns *)pData)[j]);
        break;

      case VFSA_V_ULong:
        for (j = 0; j < sdAtom->ucAtomCount; j++)
          BSWAPL(&((V_ULong *)pData)[j]);
        break;
      }

      if (nBytes != nBytesWritten)
        return VFILE_FILE_WRITE_ERROR;
    }
    pStructure = ((V_Char *) pStructure) + pSDesc->wStructureSize;
  }

  return VFILE_SUCCESS;
}


static V_Err
ChecksumWrite(VFILE vfile)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Output checksum
 * Return: Status of operation
 *---------------------------------------------------------------------------*/
{
  BSWAPL(&vfile->bhHead.vck.lBlockSize);
  if (sizeof (V_ULong) != VWRITE(&vfile->bhHead.vck.lBlockSize, sizeof(V_ULong)))
    return VFILE_FILE_WRITE_ERROR;

  BSWAPW(&vfile->bhHead.vck.wBlockChecksum);
  if (sizeof (V_Uns) != VWRITE(&vfile->bhHead.vck.wBlockChecksum, sizeof(V_Uns)))
    return VFILE_FILE_WRITE_ERROR;

  return VFILE_SUCCESS;
}

     /* V_Uns rotate right macro */
#if defined(DOS) | defined(WINDOWS) 
#  include <stdlib.h>
#  pragma intrinsic (_rotr)
#  define VROTR(W)       _rotr((W),1)
#else
#  define VROTR(W)       (((W)&1)?0x8000|(((UShort)(W))>>1):((UShort)(W))>>1)
#endif

     /* WORD from BYTE and BYTE extraction macros */
# define VLMAKEWORD(L,H) ((V_Uns)(((V_Byte)(L))|((V_Uns)((V_Byte)(H)))<<8))
# define VLLOBYTE(W)     ((V_Byte) ((W) & 0xFF))
# define VLHIBYTE(W)     ((V_Byte)((((V_Uns)(W)) >> 8 ) & 0xFF))

     /* LONG from WORD and WORD extraction macros */
# define VLMAKELONG(L,H) ((V_ULong)(((V_Uns)(L))|((V_ULong)((V_Uns)(H)))<<16))
# define VLLOWORD(L)     (((V_Uns) (L)) & 0xFFFF)
# define VLHIWORD(L)     ((V_Uns)((((V_ULong)(L)) >> 16)) & 0xFFFF)


static V_Err
ChecksumOpen(VCHECKSUM * checksum, V_Byte * buffer)
{
  if (!checksum)
    return(VFILE_INVALID_HANDLE);

  if (!buffer) {
    checksum->lBlockSize = 0 ;
    checksum->wBlockChecksum = 0 ;
  } else {
#if defined(DOS) | defined(WINDOWS) 
    *checksum = *(VCHECKSUM *)buffer;
#else
    checksum->lBlockSize =  VLMAKEWORD(buffer[4],buffer[5]);
    checksum->wBlockChecksum = VLMAKELONG(VLMAKEWORD(buffer[0],buffer[1]), VLMAKEWORD(buffer[2],buffer[3]));
#endif
  }
  return VFILE_SUCCESS;
}


static V_Err
ChecksumContinue(VCHECKSUM * checksum,  /* pointer to VCHECKSUM */
                 V_ULong bufferSize,    /* number of bytes to fold in */
                 V_Byte * buffer)       /* pointer to buffer */
{
  V_Uns  wTmp;
  V_Bool bIsOdd ;

  if (!checksum || !buffer)
    return(VFILE_INVALID_HANDLE);

  if (bufferSize) {
    wTmp = checksum->wBlockChecksum;
    bIsOdd = 1 & (int)checksum->lBlockSize;
    checksum->lBlockSize += bufferSize ;

    /* If previous call left an odd byte, fold in first byte and rotate */
    if (bIsOdd) {
      wTmp ^= VLMAKEWORD(0,*buffer++) ;      /* to 'High' byte */
      wTmp = VROTR(wTmp) ;
      bufferSize-- ;
    }

    /* Remember if this operation leaves an odd byte */
    bIsOdd =  bufferSize & 1;
        
    /* Loop through as WORDS */
    for (bufferSize >>= 1 ; bufferSize ; buffer += 2, bufferSize--) {
      wTmp ^= VLMAKEWORD(buffer[0],buffer[1]) ;
      wTmp = VROTR(wTmp) ;
    }

    /* Fold in last odd byte, but don't rotate */
    if (bIsOdd)
      wTmp ^= VLMAKEWORD(buffer[0],0) ;

    checksum->wBlockChecksum = wTmp;
  }
  return VFILE_SUCCESS;
}


static V_Err
ChecksumClose(VCHECKSUM * checksum, V_Byte * buffer)
{
  V_Uns wSizeLow;
  V_Uns wSizeHigh;
  V_Uns wFixup;
    
  if (!checksum)
    return(VFILE_INVALID_HANDLE);

  /* Add in overhead */
  checksum->lBlockSize += VCHECKSUM_SIZE ;
    
  wSizeLow  = VLLOWORD(checksum->lBlockSize);
  wSizeHigh = VLHIWORD(checksum->lBlockSize);

  /* Rotate low once to get word count aligned */
  wSizeLow = VROTR(wSizeLow);

  /* Arcane, ain't it? */
  wFixup = (0x0F ^ (wSizeLow-3) ) & 0x0F;

  /* Squish around */
  while (wFixup--)
    checksum->wBlockChecksum = VROTR(checksum->wBlockChecksum) ;
        
  /* Fold in size */
  checksum->wBlockChecksum ^= VROTR(wSizeLow) ;
  checksum->wBlockChecksum ^= VROTR(wSizeHigh) ;

  /* Pack for output */
  if (buffer) {
#if defined(DOS) | defined(WINDOWS) 
    *(VCHECKSUM *)buffer = *checksum;
#else
    wSizeLow  = VLLOWORD(checksum->lBlockSize);
    buffer[0] = VLLOBYTE(wSizeLow);
    buffer[1] = VLHIBYTE(wSizeLow);
    buffer[2] = VLLOBYTE(wSizeHigh);
    buffer[3] = VLHIBYTE(wSizeHigh);
    buffer[4] = VLLOBYTE(checksum->wBlockChecksum);
    buffer[5] = VLHIBYTE(checksum->wBlockChecksum);
#endif
  }
  return VFILE_SUCCESS;
}
