/* ekb.c - VISE Engine Knowledge Base Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Methods for maintaining the engine knowledge base (the speaker-independent,
 * application-independent data characterizing the engine).  In VISE, the EKB
 * is empty.
 * 
 * HISTORY
 * 12-Jun-98  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"

/* Generic inclusions */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define VBX_DEBUG(P) (P)
#else
#define VBX_DEBUG(P)
#endif


/* Generic Verbex inclusions */
#include "types.h"

/* Vulcan inclusions */
#include "ekb.h"

/* Engine Knowledge Base */
typedef struct ekb_s {
  V_Err   status;
} ekb_t;


EKB
EKB_create(void)
/**************************************************************************
 *
 * Create an engine knowledge base
 *
 **************************************************************************/
{
  EKB  ekb = (EKB) calloc(1, sizeof(ekb_t));

  return(ekb);
}


void
EKB_destroy(EKB ekb)
/**************************************************************************
 *
 * Destroy an engine knowledge base
 *
 **************************************************************************/
{
  if (ekb) free(ekb);
}


V_Err
EKB_status(EKB ekb)
/**************************************************************************
 *
 * Return the status of the ekb object
 *
 **************************************************************************/
{
  return(ekb ? ekb->status : NOSUCHOBJECT);
}


Bool
EKB_load(EKB ekb, Ptr engineFile, Bool verbose)
/**************************************************************************
 *
 * Load the SI/AD (Speaker-Independent/Application-Independent) data from
 * the engineFile into the EKB.  In VISE, this does nothing.
 *
 **************************************************************************/
{
  if (ekb) ekb->status = VISESUCCESS;

  return TRUE;
}
