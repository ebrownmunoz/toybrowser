    /* Word Add exports header */
#ifndef _VBXWAD_H
#define _VBXWAD_H

#ifdef __cplusplus
    extern "C" {
#endif

#include "types.h"
#include "recfile.h"

#define  VBXWAD_SUCCESS                   0
#define  VBXWAD_OUT_OF_MEMORY           -31
#define  VBXWAD_CANT_OPEN_QAD           -32
#define  VBXWAD_ILLEGAL_FETYPE          -33
#define  VBXWAD_ILLEGAL_GENDER          -34
#define  VBXWAD_CANT_LOAD_COMP_FILE     -35
#define  VBXWAD_CANT_LOAD_DICT_FILE     -36
#define  VBXWAD_NO_RESULT               -37
#define  VBXWAD_ALREADY_OPEN            -38
#define  VBXWAD_CLOSED                  -39
#define  VBXWAD_IN_DICT                 -40
#define  VBXWAD_NOT_IN_DICT             -41
#define  VBXWAD_NO_CHANGE               -42
#define  VBXWAD_AMBIGUOUS               -43
#define  VBXWAD_LENGTH_WARNING          -44
#define  VBXWAD_LANGUAGE_NOTSUPPORTED   -45
#define  VBXWAD_SHORT_BUFFER            -46
#define  VBXWAD_MISS_KANA_TOKEN         -47
#define  VBXWAD_DICT_ALREADY_OPEN       -48
#define  VBXWAD_DICT_EMPTY              -49
#define  VBXWAD_DICT_CLOSED             -50
#define  VBXWAD_BAD_PARAMETER           -51
#define  VBXWAD_CANT_FIND_DIR           -52

   /* Exported functions */

extern  short VBXWAD_Open(UShort feVersion, UShort gender, UShort *versionNum, 
                          UShort *buildNum);

extern  short VBXWAD_OpenLang(UShort feVersion, UShort gender, char *language,
                              UShort *versionNum, UShort *buildNum);

extern  short VBXWAD_CDOpen( void );

extern  short VBXWAD_CDSpelling(char *wordName, char *spelling, 
                                UShort spellingSize);

extern  short VBXWAD_CDInsertEntry(char *wordName, char *spelling);

extern  short VBXWAD_CDDeleteEntry(char *wordName);

extern  short VBXWAD_CDCountEntries(UShort *numEntries);

extern  short VBXWAD_CDGetEntry(UShort entry, char *wordName,
                                UShort wordNameLen, char *spelling,
                                UShort spellingLen);

extern  short VBXWAD_CDClose(void);

extern  short VBXWAD_Create(char *wordName, char *ortho, FUPATE *upat);

extern  short VBXWAD_CreateInfo(char   *ortho,
                                UShort orthoSize,
                                UShort *orthoFoundInCustDict,
                                char   *phoneTrans,
                                UShort phoneTransSize,
                                UShort *phoneTransFoundInCustDict,
                                UShort *phoneTransFoundInDict,
                                char   *compTrans,
                                UShort compTransSize,
                                UShort *compTransFoundInCustDict);

extern  short VBXWAD_Close(void);

extern  short VBXWAD_FindInDictionaryLin(char   *dicFN,
                                         char   *wordName,
                                         char   *kanaBuf,
                                         size_t bufMaxSize);

extern  short VBXWAD_DictOpen(char *fullPath);

extern  short VBXWAD_FindInDictionaryBin(char *wordName,
                                       char   *kanaBuf,
                                       size_t bufMaxSize);

extern  short VBXWAD_DictClose(void);

extern  FKEY      *VBXWAD_getKey(void);
extern  FSTRING   *VBXWAD_getVer(void);
extern  FUPATHEAD *VBXWAD_getSilenceHeader(void);
extern  FUPATPARM *VBXWAD_getParameters(void);
extern  FUPATE    *VBXWAD_getSilenceModels(void);

#ifdef __cplusplus
    }
#endif   /* __cplusplus */

#endif /* _VBXWAD_H */


