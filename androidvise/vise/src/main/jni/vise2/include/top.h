    /* Text tO Phone conversion header */
#ifndef _TOP_H
#define _TOP_H

#ifdef __cplusplus
    extern "C" {
#endif

#include <stdio.h>

   /* TO Phone Object */
typedef struct top_s * TOP;

   /* TOP object error codes */
#define TOP_SUCCESS                          0
#define TOP_FAILURE_DIRECTORY_SEARCH      -100
#define TOP_FAILURE_PARAMETERS            -101
#define TOP_FAILURE_CREATE_CFG            -102
#define TOP_FAILURE_OPEN_NETFILE          -103
#define TOP_FAILURE_PARSE_CFG             -104
#define TOP_FAILURE_LOAD_NET              -105
#define TOP_FAILURE_MALLOC                -106
#define TOP_FAILURE_NO_LANGUAGE_IN_CFG    -107
#define TOP_FAILURE_MALLOC_NET            -108
#define TOP_FAILURE_MALLOC_IO_VEC         -109
#define TOP_FAILURE_ILLEGAL_INPUT_CHAR    -110
#define TOP_FAILURE_NO_DIC_FILE           -111
#define TOP_FAILURE_CORRUPT_NET_FILE      -112
#define TOP_FAILURE_VERSION_INCOMPATIBLE  -113
#define TOP_FAILURE_BUFFER_TOO_SMALL      -114
#define TOP_FAILURE_NO_NET_FILE           -115
#define TOP_FAILURE_MALLOC_NODES          -116
#define TOP_FAILURE_MALLOC_LINKS          -117
#define TOP_FAILURE_UNSUPPORTED_LANGUAGE  -118
#define TOP_FAILURE_LOAD_RULES_MALLOC     -119
#define TOP_FAILURE_TEXT_TO_PHONES_RULES  -120
#define TOP_FAILURE_NON_KANA_CHARACTER    -121
#define TOP_FAILURE_CORRUPT_DIC_FILE      -122

extern  TOP   TOP_create(FILE *cfgFP, int *rc);
extern  void  TOP_destroy(TOP top);

extern  int   TOP_phonesFromSpelling(TOP top, const char *spelling,
                                    char *phones, int maxSize, int *inDict);

#ifdef __cplusplus
    }
#endif   /* __cplusplus */

#endif /* _TOP_H */
