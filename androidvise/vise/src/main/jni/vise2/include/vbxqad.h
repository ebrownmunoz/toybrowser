    /* Base phone and diphone units header */
#ifndef _QAD_H
#define _QAD_H

#ifdef __cplusplus
    extern "C" {
#endif

#include "types.h"
#include "recfile.h"

   /* Quick Add Object */
typedef struct duset_s * QAD;
   /* To Phone Object */
typedef struct top_s * TOP;

   /* QAD object error codes */
#define QAD_SUCCESS                          0
#define QAD_FAILURE_OUTOFMEMORY             -1
#define QAD_FAILURE_PARAMETERS              -2
#define QAD_FAILURE_OPENFILE                -3
#define QAD_FAILURE_READFILE                -4
#define QAD_FAILURE_INCOMPATIBLE            -5
#define QAD_FAILURE_BADPHONE                -6
#define QAD_FAILURE_SHORTBUFFER             -7
#define QAD_FAILURE_LONGWORD                -8
#define QAD_FAILURE_BADCOMPONENT            -9
#define QAD_LANGUAGE_NOTSUPPORTED          -10

   /* TOP object error codes */
#define TOP_SUCCESS                          0
#define TOP_FAILURE_DIRECTORY_SEARCH      -100
#define TOP_FAILURE_PARAMETERS            -101
#define TOP_FAILURE_OPEN_DICFILE          -102
#define TOP_FAILURE_OPEN_NETFILE          -103
#define TOP_FAILURE_LOAD_DIC              -104
#define TOP_FAILURE_LOAD_NET              -105
#define TOP_FAILURE_MALLOC                -106
#define TOP_FAILURE_MALLOC_DIC            -107
#define TOP_FAILURE_MALLOC_NET            -108
#define TOP_FAILURE_MALLOC_IO_VEC         -109
#define TOP_FAILURE_ILLEGAL_INPUT_CHAR    -110
#define TOP_FAILURE_CORRUPT_DIC_FILE      -111
#define TOP_FAILURE_CORRUPT_NET_FILE      -112
#define TOP_FAILURE_VERSION_INCOMPATIBLE  -113
#define TOP_FAILURE_BUFFER_TOO_SMALL      -114
#define TOP_FAILURE_DIC_DUPLICATE_ENTRY   -115
#define TOP_FAILURE_MALLOC_NODES          -116
#define TOP_FAILURE_MALLOC_LINKS          -117
#define TOP_FAILURE_UNSUPPORTED_LANGUAGE  -118
#define TOP_FAILURE_LOAD_RULES_MALLOC     -119
#define TOP_FAILURE_TEXT_TO_PHONES_RULES  -120
#define TOP_FAILURE_NON_KANA_CHARACTER    -121

   /* Dictionary object error codes */
#define TOP_ALREADY_OPEN                  -131
#define TOP_FILE_OPEN_FAILS               -132
#define TOP_SIZE_FAILS                    -133
#define TOP_EMPTY_DICTIONARY              -134
#define TOP_OUT_OF_MEMORY                 -135
#define TOP_FILL_FAILS                    -136

   /* Exported functions */

extern  QAD   QAD_create(const char *language, int *rc);
extern  void  QAD_destroy(QAD pObj);
extern  TOP   TOP_create(const char *language, int *rc);
extern  void  TOP_destroy(TOP pObj);

extern  int   TOP_phonesFromSpelling(TOP pObj, const char *spelling,
                                    char *phones, int maxSize, int *inDict);

extern  int   QAD_load(QAD pObj, char *fName);

extern  TOP   QAD_getTOP(QAD pObj);
extern  int   QAD_getGender(QAD pObj);
extern  int   QAD_getJinVersion(QAD pObj);
extern  int   QAD_getSampleRate(QAD pObj);

extern  FKEY      * QAD_getKey(void);
extern  FSTRING   * QAD_getVer(void);
extern  FUPATHEAD * QAD_getSilenceHeader(QAD pObj);
extern  FUPATPARM * QAD_getParameters(QAD pObj);
extern  FUPATE    * QAD_getSilenceModels(QAD pObj);


extern  int   QAD_wordFromSpelling(QAD pObj, FUPATE *wordModel,
                     const char *wordName, const char *spelling, int *inDict);
extern  int   QAD_wordFromPhones(QAD pObj, FUPATE *word,
                     const char *wordName, const char *transcription);
extern  int   QAD_wordFromComponents(QAD pObj, FUPATE *word,
                     const char *wordName, const char *components);
extern  int   QAD_componentsFromPhones(QAD pObj, const char *phones,
                     char *components, int maxSize);
extern  void  QAD_Version(UShort *version, UShort *buildNum);

#ifdef __cplusplus
    }
#endif   /* __cplusplus */

#endif /* _QAD_H */
