    /* Voice File Utilities header */
#ifndef _VFU_H
#define _VFU_H

#ifdef __cplusplus
    extern "C" {
#endif

    /* VFU Result Codes */
#define VFU_SUCCESS                  0
#define VFU_BAD_PARAMETERS         -16
#define VFU_OUT_OF_MEMORY          -17
#define VFU_NOT_FULLY_TRAINED      -18
#define VFU_NOTHING_DONE           -19
#define VFU_RENAME_ERROR           -20
#define VFU_BAD_FEVERSION          -21
#define VFU_BAD_GENDER             -22
#define VFU_NO_SUCH_WORD           -23
#define VFU_FILES_INCOMPATIBLE     -24
#define VFU_CANT_OPEN_FILE         -25

    /* Exported functions */

/* Caller is responsible for freeing all char pointers from array wordList 
   and array wordList itself */
extern int VFU_wordsToEnroll(char *recFN, char *voiFN,
                             char ***wordList, int *wordCnt);

extern int VFU_addWords(char *recFN, char *voiFN, char *language, char *cfgFN);
extern int VFU_combine(char *voi1FN, char *voi2FN, char * dstFN, int force);
extern int VFU_deleteWord(char *voiFN, char *wordName);
extern int VFU_generate(char *voiFN, char *listFN, char *language, char *cfgFN,
                        unsigned short feVersion, unsigned short gender);
extern int VFU_info(char *voiFN, unsigned short *feVersion,
                    unsigned short *gender);
extern int VFU_select(char *listFN, char *srcFN, char *dstFN);
extern int VFU_clean(char *srcFN, char *dstFN);
extern int VFU_try(char *language, char *cfgFN, char **wordList, int wordCnt,
                   char *logFN);
extern int VFU_vdump(char *voiFN, char *wordName);


    /* Result Codes coming from the layers below */
/*
VFILE_SUCCESS                        0
VFILE_ILLEGAL_MODE                   1
VFILE_OPEN_FAILS                     2
VFILE_ENOMEM                         3
VFILE_NO_FREE_HANDLES                4
VFILE_INVALID_HANDLE                 5
VFILE_INVALID_MARK                   6
VFILE_INVALID_STRUCTURE_TYPE         8
VFILE_INVALID_STRUCTURE_ATOM         9
VFILE_FILE_READ_ERROR               10
VFILE_END_OF_FILE                   11
VFILE_INVALID_TEXT                  12
VFILE_FILE_WRITE_ERROR              13
VFILE_BLOCK_CLOSED                  14
VFILE_INVALID_VARIABLE_SIZE         15
VFILE_SEEK_ERROR                    16

QAD_SUCCESS                          0
QAD_FAILURE_OUTOFMEMORY             -1
QAD_FAILURE_PARAMETERS              -2
QAD_FAILURE_OPENFILE                -3
QAD_FAILURE_READFILE                -4
QAD_FAILURE_INCOMPATIBLE            -5
QAD_FAILURE_BADPHONE                -6
QAD_FAILURE_SHORTBUFFER             -7
QAD_FAILURE_LONGWORD                -8
QAD_FAILURE_BADCOMPONENT            -9
QAD_LANGUAGE_NOTSUPPORTED          -10

TOP_SUCCESS                          0
TOP_FAILURE_DIRECTORY_SEARCH      -100
TOP_FAILURE_PARAMETERS            -101
TOP_FAILURE_CREATE_CFG            -102
TOP_FAILURE_OPEN_NETFILE          -103
TOP_FAILURE_PARSE_CFG             -104
TOP_FAILURE_LOAD_NET              -105
TOP_FAILURE_MALLOC                -106
TOP_FAILURE_NO_LANGUAGE_IN_CFG    -107
TOP_FAILURE_MALLOC_NET            -108
TOP_FAILURE_MALLOC_IO_VEC         -109
TOP_FAILURE_ILLEGAL_INPUT_CHAR    -110
TOP_FAILURE_NO_DIC_FILE           -111
TOP_FAILURE_CORRUPT_NET_FILE      -112
TOP_FAILURE_VERSION_INCOMPATIBLE  -113
TOP_FAILURE_BUFFER_TOO_SMALL      -114
TOP_FAILURE_NO_NET_FILE           -115
TOP_FAILURE_MALLOC_NODES          -116
TOP_FAILURE_MALLOC_LINKS          -117
TOP_FAILURE_UNSUPPORTED_LANGUAGE  -118
TOP_FAILURE_LOAD_RULES_MALLOC     -119
TOP_FAILURE_TEXT_TO_PHONES_RULES  -120
TOP_FAILURE_NON_KANA_CHARACTER    -121
TOP_FAILURE_CORRUPT_DIC_FILE      -122

DICS_SUCCESS                         0
DICS_BAD_PARAMETERS               -131
DICS_FILE_OPEN_FAILS              -132
DICS_OUT_OF_MEMORY                -133
DICS_EMPTY                        -134

WAD_SUCCESS                          0
WAD_OUT_OF_MEMORY                  -31
WAD_CANT_OPEN_CFG                  -32
WAD_ILLEGAL_FETYPE                 -33
WAD_ILLEGAL_GENDER                 -34
WAD_CANT_FIND_DIR                  -35
WAD_BAD_PARAMETERS                 -36
WAD_NO_RESULT                      -37
WAD_CANT_CREATE_CFG                -38
WAD_CANT_PARSE_CFG                 -39
WAD_NO_LANGUAGE_IN_CFG             -40
WAD_BAD_LANGUAGE_IN_CFG            -41
WAD_LANGUAGE_NOTSUPPORTED          -42
WAD_SHORT_BUFFER                   -43

*/

#ifdef __cplusplus
    }
#endif   /* __cplusplus */

#endif /* _VFU_H */
