/* jinrw.h - jin data file i/o routines
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (jrt)
 * $Header: jinrw.h[1.0] Thu Apr 10 20:54:00 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _JINRW_H_
#define _JINRW_H_

#include <stdio.h>
#include "types.h"
#include "sphdr.h"
#include "hndl.h"

#if defined(__cplusplus)
extern "C" {
#endif

extern Bool JIN_read(Char * fname, HDR hdr, jin * data, size_t * numFrames, Int * jinVersion);

#if defined(__cplusplus)
}
#endif

#endif              /* ndef _JINRW_H_ */
