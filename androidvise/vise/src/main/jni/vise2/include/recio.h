/* recio.h
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 *
 * DESCRIPTION
 * Recognizer/voice file parsing module
 *
 * HISTORY
 * 07-Jul-96 (cav) Updated RRLee's old version for use in the SAPI project
 * 17-Jan-97 (dcv) Added VSStreamData()
 * $Header: recio.h[1.0] Thu Apr 10 20:54:06 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _RECIO_H_
#define _RECIO_H_

#include "vlbldefs.h"

/* Descriptor for open file, stream */
typedef struct VFile_s * VFILE;
typedef struct VStream_s * VSTREAM;
typedef UShort (*TERMPROCPNTR)(VFILE, UShort);

#ifdef __cplusplus
extern "C" { 
#endif

/* File-Based I/O Routines */
VFILE   VFOpenFile(const Char * szFileName, Int iMode, TERMPROCPNTR tpTermProc);
UShort  VFCloseFile(VFILE vfdesc);
UShort  VFRewindFile(VFILE vfdesc);

/* Routines to load/save files to/from buffers in memory */
Ptr     VSLoadFile(const Char * fileName, UInt * fileSize);
Bool    VSSaveFile(const Char * fileName, Ptr buffer, UInt bufferSize);

/* Stream-Based I/O Routines */
VSTREAM VSOpenStream(Ptr dataPtr, Int mode, UInt dataSize);
UShort  VSCloseStream(VSTREAM vstream);
UShort  VSRewindStream(VSTREAM vstream);
Ptr     VSStreamData(VSTREAM vstream, UInt * dataSize);

#ifdef __cplusplus
}
#endif
#endif	/* _RECIO_H_ */
