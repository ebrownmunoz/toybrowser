/****************** Include File *****************************************

 $Workfile:   vlerrno.h  $
 $Revision:   1.1  $
 $Date:   26 Aug 1993 17:44:34  $
 $Author:   RRLee  $

 Release Version 4.2.0

       Copyright (c) 1991 Verbex Voice Systems, Inc.
       All Rights Reserved

 Description:
   Error codes returned by VERBEX library functions. 

 Restrictions:    none

**************************************************************************/
#ifndef VLERRNO_H
# define VLERRNO_H

#ifdef __cplusplus
    extern "C" {        /* Assume C declarations for C++ */
#endif	/* __cplusplus */

     /* General function error return codes    */
#define   VLX_SOFTFAIL        0x1234    /* Very bad problems */
#define   VLX_SUCCESS         0         /* Function completes O.K. */
#define   VLX_NULL_PNTR       1         /* Null pointer is passed */
#define   VLX_ENOMEM          2         /* Cannot allocate memory */
#define   VLX_ENDFILE         3         /* End of file hit on read */
#define   VLX_FILE_NOT_FOUND  4         /* Requested file not found */
#define   VLX_FILE_EXISTS     5         /* Trapped overwrite of exist. file */
#define   VLX_ILL_RANGE       6         /* Argument out of allowed range */
#define   VLX_FILE_CREATE     7         /* cant create file */
#define   VLX_FILE_WRITE      8         /* Error writing file */

     /* VASSL public error codes */
#define   VLX_DENIED          100       /* Fails - caller does not own hrec */
#define   VLX_TIMEOUT         101       /* Fails due to device timeout */
#define   VLX_IO_ERROR        102       /* Fails due to I/O fault */
#define   VLX_CMD_TOO_LARGE   103       /* Generated escape sequence too big */
#define   VLX_PK_CHECKSUM     104       /* packet checksum fails */
#define   VLX_PK_TOO_LARGE    105       /* packet too large */
#define   VLX_PK_ILL_DATA     106       /* packet has bad data */
#define   VLX_BUFFERSIZE      107       /* read buffer too small */

     /* VASSL transfer errors */
#define   VLX_IMAGE_PRESENT   110       /* write to complete /no overw */
#define   VLX_NO_SUCH_IMAGE   111       /* Access to illegal image     */
#define   VLX_ACCESS_FAILS    112       /* Cant delete image           */
#define   VLX_USER_BREAK      113       /* Terminated by user request */
#define   VLX_IMAGE_VERIFY    114       /* Verify data fails          */
#define   VLX_IMAGE_INCOMPLETE 115      /* Accessed image is incomplete */
#define   VLX_IMAGE_BLANK     116       /* Accessed image is blank    */
#define   VLX_MISSING_BLOCK   117       /* A mandatory block is missing */
#define   VLX_NO_FIRST_BLOCK  118       /* Internal error code */

     /* VASSL internal error codes */
#define   VLX_ILLEGAL_CMD     195       /* Illegal recognizer command */
#define   VLX_NO_BOX          196       /* Internal message queue fault */
#define   VLX_NO_SPACE        197       /* Internal message queue fault */
#define   VLX_NOTDEVSPEC      198       /* Invalid device description */
#define   VLX_NO_DATA         199       /* Node data unavailable */

     /* File block operations */
#define   VLX_ILLEGAL_KEY     200       /* File key incompatable with device */
#define   VLX_FB_WRONG_ID     201       /* File block ID does not match */
#define   VLX_FB_TOO_LARGE    202       /* File block size absurd */
#define   VLX_FB_CHECKSUM     203       /* File block checksum fails */
#define   VLX_FB_BLOCKSIZE    204       /* File block size fails */


#ifdef __cplusplus
    }               /* End of extern "C" { */
#endif	/* __cplusplus */
#endif /* VLERRNO_H */

