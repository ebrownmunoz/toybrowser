/* speaker.h - the VISE speaker-dependent knowledge base
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _SPKR_H_
#define _SPKR_H_

#include "types.h"
#include "models.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* A Speaker Knowledge Base */
typedef struct modset_s spkr_t, * SPKR;

#define SPKR_create(engkb)              MODSET_create()
#define SPKR_destroy(spkr)              MODSET_destroy(spkr)
#define SPKR_load(spkr,stream,verbose)  MODSET_load(spkr, stream, verbose)
#define SPKR_feVersion(spkr)            MODSET_jinVersion(spkr)

#ifdef __cplusplus
}
#endif
#endif /* _SPKR_H_ */
