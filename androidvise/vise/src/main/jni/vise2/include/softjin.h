#ifndef _SOFTJIN_H_
#define _SOFTJIN_H_

#include "types.h"
#ifdef __cplusplus
extern "C" {
#endif

#define FRAMELENGTH     256
#define FRAMEPERIOD     12.8

Int  SOFTJIN_appendSil(UInt numFrames);
Bool SOFTJIN_enqueue(Short *frame, Int shift);
Int  SOFTJIN_getFrameShift(UInt jinVersion);
Int  SOFTJIN_getJinCount();
Int  SOFTJIN_getSampleRate(UInt jinVersion);
void SOFTJIN_init(Int jinVersion);
void SOFTJIN_reset(Bool resetJin);
void SOFTJIN_setSilence(jinr *silenceJin);

#ifdef __cplusplus
}
#endif
#endif /* _SOFTJIN_H_ */
