/* vvise.h - VISE Search Engine Interface Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _VVISE_H_
#define _VVISE_H_

#include "types.h"
#include "vise.h"
#include "vocab.h"
#include "hyp.h"
#include "ifaces.h"
#include "jincnvrt.h"
#include "vbxtypes.h"
#include "viseerr.h"
#include "visemsg.h"
#include "viseparm.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* VVISE Descriptor */
typedef struct vvise_s * VVISE;

/* Utterance structure for VVISE_enroll() */
typedef struct enrutt_s {
  SN   startSN;
  SN   wordStartSN;
  SN   wordEndSN;
  SN   endSN;
  UInt wordFrameCnt;
  UInt frameCnt;
} enrutt_t, * ENRUTT;

/* VVISE Methods */
extern VVISE  VVISE_create(IMemory * memory, IMail * mail);
extern void   VVISE_destroy(VVISE vvise);
extern V_Err  VVISE_status(VVISE vvise);
extern Bool   VVISE_abort(VVISE vvise, VBXMSG message);
extern Bool   VVISE_init(VVISE vvise);
extern Bool   VVISE_sourceInit(VVISE vvise);
extern Bool   VVISE_warmstart(VVISE vvise);
extern Bool   VVISE_downloadWildcard(VVISE vvise, UInt oldWid, UInt wid, UInt minDwell, UInt maxDwell, UInt relThresh, UInt absThresh);
extern Bool   VVISE_setParameter(VVISE vvise, VBXMSG message, UInt index, Int value);
extern Bool   VVISE_getParameter(VVISE vvise, VBXMSG message, UInt index, Int * value);
extern Bool   VVISE_setThresholds(VVISE vvise, UInt act, UInt deact, UInt loose, UInt tight, UInt muthah);
extern Bool   VVISE_setTimeout(VVISE vvise, VBXMSG message, UInt maxFrames);
extern Bool   VVISE_deleteGrammar(VVISE vvise, UInt grammarId);
extern Bool   VVISE_downloadForcingGrammar(VVISE vvise, HYP hyp, UInt forcingGrammarId, VOCAB vocab);
extern Bool   VVISE_downloadJin(VVISE vvise, JIN2FR features, SN * startSN, SN * endSN, UInt * numFrames);
extern JIN2FR VVISE_uploadJin(VVISE vvise, SN * startSN, SN * endSN, UInt * numFrames);
extern Bool   VVISE_downloadMultiSilenceTrainingGrammar(VVISE vvise, UInt grammarId);
extern Bool   VVISE_downloadEnrollmentGrammar(VVISE vvise, UInt grammarId, UInt initialWCCount);
extern UInt   VVISE_deleteGrammars(VVISE vvise, VOCAB vocab);
extern UInt   VVISE_downloadGrammars(VVISE vvise, VOCAB vocab, Bool verbose);
extern Bool   VVISE_listen(VVISE vvise, ENRUTT utt, UInt maxFrames, Bool live, Bool findWord, Int * volumePtr);
extern HYP    VVISE_recognize(VVISE vvise, UInt grammarId, SN startSN, SN endSN, UInt maxFrames, Bool live, UInt nBest, Bool verbose);
extern Bool   VVISE_trainVocabulary(VVISE vvise, HYP hyp, VOCAB vocab, UInt weight);
extern Bool   VVISE_trainWord(VVISE vvise, UInt wid, VOCAB vocab, SN startSN, SN endSN, UInt weight, UInt score, Bool verbose);
extern Bool   VVISE_enrollWord(VVISE vvise, VOCAB vocab, UInt frPerKernel, UInt maxKernels, UInt minKernels, UInt nModels, UInt * wids, UInt nUtts, ENRUTT utts);
extern UInt   VVISE_reinitializeModels(VVISE vvise, VOCAB vocab, Bool firstPass);
extern UInt   VVISE_downloadVocab(VVISE vvise, VOCAB vocab, Bool initializeDwells, Bool verbose);
extern UInt   VVISE_downloadTranscribedModels(VVISE vvise, VOCAB vocab, Char * transcription, Bool initializeDwells, Bool verbose);
extern Bool   VVISE_downloadModel(VVISE vvise, VOCAB vocab, UInt oldWid, UInt wid);
extern Bool   VVISE_downloadWord(VVISE vvise, VOCAB vocab, UInt oldWid, UInt wid);
extern UInt   VVISE_uploadVocab(VVISE vvise, VOCAB vocab, Bool uploadDwells);
extern UInt   VVISE_uploadTrainedModels(VVISE vvise, VOCAB vocab, Bool uploadDwells, Int minTrainingCount);
extern UInt   VVISE_uploadSelectedModels(VVISE vvise, VOCAB vocab, Char ** words, Bool uploadDwells, Int minTrainingCount);
extern UInt   VVISE_uploadHypModels(VVISE vvise, VOCAB vocab, HYP hyp, Bool uploadDwells);
extern UInt   VVISE_uploadTranscribedModels(VVISE vvise, VOCAB vocab, Char * transcription, Bool uploadDwells);
extern Bool   VVISE_uploadModel(VVISE vvise, VOCAB vocab, UInt wid, Bool uploadDwells);
extern Bool   VVISE_uploadTrainedModel(VVISE vvise, VOCAB vocab, UInt wid, Bool uploadDwells, Int minTrainingCount);
extern UInt   VVISE_adjustModels(VVISE vvise, VOCAB vocab, UInt weight, Bool adjustDwells);
extern UInt   VVISE_adjustSelectedModels(VVISE vvise, VOCAB vocab, Char ** words, UInt weight, Bool adjustDwells);
extern void   VVISE_setTextMode(VVISE vvise, Bool flag);

#ifdef __cplusplus
}
#endif
#endif /* _VVISE_H_ */
