/* vocab.h - VISE Vocabulary Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: vocab.h[1.0] Thu Apr 10 20:54:14 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _VOCAB_H_
#define _VOCAB_H_

#include "types.h"
#include "llist.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "viseerr.h"

/* Vocabulary entry (one per word) */
typedef struct vocabent_s  * VOCENT;

/* Vocabulary */
typedef struct vocab_s  * VOCAB;

/* Iterator over a vocabulary */
typedef struct vociter_s * VOCITER;

/* Structure for specifying vocabulary entries not in the .rec file */
typedef struct vocmap_s {
  Char * wordName;
  UInt   wordId;
  UInt   classId;
} vocmap_t, * VOCMAP;

#include "models.h"
#include "wrdmap.h"
#include "syntax.h"
#include "trans.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* Methods for vocabulary entries */
extern UInt     VOCENT_numModels(VOCENT entry);
extern UInt     VOCENT_firstWordId(VOCENT entry);
extern UInt     VOCENT_nextWordId(VOCENT entry);
extern VOCAB    VOCENT_vocabulary(VOCENT entry);
extern UInt     VOCENT_class(VOCENT entry);
extern Char   * VOCENT_wordName(VOCENT entry);
extern void     VOCENT_useBestModel(VOCENT entry);

/* Methods for vocabularies */
extern VOCAB    VOCAB_create(UInt vocabId);
extern void     VOCAB_annihilate(VOCAB vocab);
extern V_Err    VOCAB_status(VOCAB vocab);
extern UInt     VOCAB_id(VOCAB vocab);
extern Char   * VOCAB_name(VOCAB vocab);
extern Bool     VOCAB_load(VOCAB vocab, VFILE recFile);
extern Bool     VOCAB_loadTrans(VOCAB vocab, VFILE recFile);
extern Bool     VOCAB_loadFromMap(VOCAB vocab, VOCMAP map);
extern Bool     VOCAB_addWord(VOCAB vocab, Char * name, UInt wid, UInt classId);
extern Bool     VOCAB_assignModels(VOCAB vocab, MODELS models, WRDMAP map);
extern void     VOCAB_removeModels(VOCAB vocab);
extern Bool     VOCAB_cloneModels(VOCAB vocab);
extern UInt     VOCAB_numModels(VOCAB vocab);
extern Bool     VOCAB_addGrammar(VOCAB vocab, SYNTAX grammar);
extern SYNTAX * VOCAB_grammars(VOCAB vocab, UInt * numGrammars);
extern UInt     VOCAB_getTraining(VOCAB vocab, UInt * sum, UInt * sumSqr, UInt * count, Bool reset);
extern void     VOCAB_setTrainingDefault(VOCAB vocab, UInt score, UInt count);
extern UInt     VOCAB_wordIdFromName(VOCAB vocab, Char * wordName);
extern VOCENT   VOCAB_entryFromName(VOCAB vocab, Char * wordName);
extern MODELS   VOCAB_models(VOCAB vocab);
extern Bool     VOCAB_makeModel(VOCAB vocab, UInt wid, UInt numKernels, FUPATE * model);
extern Bool     VOCAB_copyModel(VOCAB vocab, UInt srcWId, UInt destWId, Bool copyDwells);
extern UInt     VOCAB_modelId(VOCAB vocab, UInt wid);
extern Bool     VOCAB_getModel(VOCAB vocab, UInt wid, FUPATE * model);
extern Bool     VOCAB_putModel(VOCAB vocab, UInt wid, FUPATE * model);
extern Bool     VOCAB_setModelDwells(VOCAB vocab, UInt wid, UChar kid, UChar minDwell, UChar maxDwell);
extern Char   * VOCAB_wordNameFromId(VOCAB vocab, UInt wid);
extern VOCENT   VOCAB_entryFromId(VOCAB vocab, UInt wid);
extern UInt     VOCAB_firstWordId(VOCAB vocab);
extern UInt     VOCAB_nextWordId(VOCAB vocab);
extern UInt     VOCAB_lastWordId(VOCAB vocab);
extern UInt     VOCAB_numWordIds(VOCAB vocab);
extern VOCENT * VOCAB_tokens(VOCAB vocab, Char * transcription, UInt * numTokens);
extern SYNTAX * VOCAB_parse(VOCAB vocab, Char * transcription, UInt * numParsers);
extern TRANS    VOCAB_trans(VOCAB vocab);

/* Methods for iterating over a vocabulary */
extern VOCITER  VOCITER_create(VOCAB vocab);
extern void     VOCITER_destroy(VOCITER vociter);
extern UInt     VOCITER_nextWordId(VOCITER moditer);

#ifdef __cplusplus
}
#endif
#endif /* _VOCAB_H_ */
