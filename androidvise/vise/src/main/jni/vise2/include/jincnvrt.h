#ifndef V_JINCNVRT_

/******************************************************************************
 * 
 * File:         V_JIN.H
 *
 * Description:  Header file for the JIN conversion module
 *
 *****************************************************************************/

#define V_JINCNVRT_

#include <string.h>
#include "sn.h"
#include "vbxtypes.h"
#include "types.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* The number of acoustic parameters (components) in a JIN frame */
#define NUMCOMPONENTS 16  /* Can be gotten from IJINATTR */

/* The largest value a jin parameter can assume */
#define JINMAXVALUE  255

/* Most of the world sees jin data as an array of NUMCOMPONENTS bytes.
   (N.B. A byte is 32 bits in the 'C3x, 8 bits in 'x86.) */
#define SIZE_OF_JIN_ARRAY NUMCOMPONENTS
typedef V_Byte jinel;
typedef struct jin_s {
  jinel  jincomponents[SIZE_OF_JIN_ARRAY];
} jinr;

/* A jin frame */
typedef struct jinfr_s {
  jinr   jin;      /* A JINFR inherits from a jinr, SO THIS MUST BE FIRST! */
  V_SN   sn;
} jinfr_t, * JINFR;

/* The message system always views jin data viewed as an array of
   NUMCOMPONENTS/2 unsigned shortwords, each containing two jin components.
   The lower-order jin component is in the lower-order 8-bit chunk.
   (N.B. A shortword is 32 bits in the 'C3x, 16 bits in the 'x86.) */
#define SIZE_OF_JIN2_ARRAY NUMCOMPONENTS/2
typedef V_Uns jin2el;
typedef struct jin2_s {
  jin2el jin2components[SIZE_OF_JIN2_ARRAY];
} jin2r;

/* A jin2 frame */
typedef struct jin2fr_s {
  jin2r  jin;      /* A JIN2FR inherits from a jin2r, SO THIS MUST BE FIRST! */
  V_SN   sn;
} jin2fr_t, * JIN2FR;

/* Jin data viewed as an array of NUMCOMPONENTS/4 unsigned longwords,
   each containing four components.
   The lower-order components are in the lower-order 8-bit chunks.
   (N.B. A longword is 32 bits in both the 'C3x and the 'x86.) */
#define SIZE_OF_JIN4_ARRAY NUMCOMPONENTS/4
typedef V_ULong jin4el;
typedef struct jin4_s {
 jin4el jin4components[SIZE_OF_JIN4_ARRAY];
} jin4r, *jin4;

/* A jin4 frame */
typedef struct jin4fr_s {
  jin4r  jin;      /* A JIN4FR inherits from a jin4r, SO THIS MUST BE FIRST! */
  V_SN   sn;
} jin4fr_t, * JIN4FR;

/* The jinsource, tem and tm view the jin data as an array of NUMCOMPONENTS/VBYTESIZE
   bytes, where a byte is the smallest addressable unit, NOT NECESSARILY 8 BITS!
   (see vbxtypes.h)  The lower-order components are always in the lower-order 8-bit
   chunks. (N.B. VBYTESIZE is 4 in the 'C3x and 1 in the 'x86.) */
#define SIZE_OF_JINPACK_ARRAY NUMCOMPONENTS/VBYTESIZE
typedef V_Byte jinpackel;

extern V_Uns   jin_getcomponent(jin, V_Int);
extern V_Void  jin_setcomponent(jin, V_Int, V_Uns);
extern V_Void  jin_from_jin2(jin dest, jin2 src);
extern V_Void  jin2_from_jin(jin2 dest, jin src);
extern V_Void  jin_from_jin4(jin dest, jin4 src);
extern V_Void  jin4_from_jin(jin4 dest, jin src);
extern V_Void  jin2_from_jin4(jin2 dest, jin4 src);
extern V_Void  jin4_from_jin2(jin4 dest, jin2 src);

#if   (VBYTESIZE == 1)
/* If a byte is 8 bits, packed jin is jin */
typedef jinr     jinpackr, * jinpack;
typedef jinfr_t  jinpackfr_t, * JINPACKFR;
#define jin_from_jinpack(dest, src)  memmove((void *) dest, (const void *) src, SIZE_OF_JINPACK_ARRAY)
#define jinpack_from_jin(dest, src)  memmove((void *) dest, (const void *) src, SIZE_OF_JINPACK_ARRAY)
#define jin2_from_jinpack(dest, src) jin2_from_jin(dest, src)
#define jinpack_from_jin2(dest, src) jin_from_jin2(dest, src)
#elif (VBYTESIZE == 2)
/* Else, if a byte is 16 bits, packed jin is jin2 */
typedef jin2r    jinpackr, * jinpack;
typedef jin2fr_t jinpackfr_t, * JINPACKFR;
#define jin_from_jinpack(dest, src)  jin_from_jin2(dest, src)
#define jinpack_from_jin(dest, src)  jin2_from_jin(dest, src)
#define jin2_from_jinpack(dest, src) memmove((void *) dest, (const void *) src, NUMCOMPONENTS)
#define jinpack_from_jin2(dest, src) memmove((void *) dest, (const void *) src, NUMCOMPONENTS)
#elif (VBYTESIZE == 4)
/* Else, if a byte is 32 bits, packed jin is jin4 */
typedef jin4r    jinpackr, * jinpack;
typedef jin4fr_t jinpackfr_t, * JINPACKFR;
#define jin_from_jinpack(dest, src)  jin_from_jin4(dest, src)
#define jinpack_from_jin(dest, src)  jin4_from_jin(dest, src)
#define jin2_from_jinpack(dest, src) jin2_from_jin4(dest, src)
#define jinpack_from_jin2(dest, src) jin4_from_jin2(dest, src)
#endif

#ifndef NOMACROS

#define jin_getcomponent(J,I)    (J)->jincomponents[I]
#define jin_setcomponent(J,I,X)  (J)->jincomponents[I] = (X)

#endif

#define PRINTJIN16(N,S,J)  VBX_print("%s " QuadFmt(1) ": %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u %3.3u\n",\
                                      N, S, J[0], J[1], J[2], J[3], J[4], J[5], J[6], J[7], J[8], J[9], J[10], J[11], J[12], J[13], J[14], J[15])

#ifdef __cplusplus
}
#endif

#endif /* V_JINCNVRT_ */
