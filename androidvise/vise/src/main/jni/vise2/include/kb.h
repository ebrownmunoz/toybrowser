/* kb.h - VISE Knowledge Base Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: kb.h[1.0] Thu Apr 10 20:54:01 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _KB_H_
#define _KB_H_

#include "types.h"
#include "vfile.h"

/* Knowledge Base */
typedef struct kb_s * KB;

#include "ekb.h"
#include "hyp.h"
#include "vocab.h"
#include "models.h"
#include "syntax.h"
#include "trans.h"
#include "script.h"
#include "viseerr.h"

#ifdef __cplusplus
extern "C" { 
#endif

typedef enum { KB_HOST_RESPONSE, KB_VOICE_RESPONSE, KB_DISPLAY_RESPONSE } KBResponse_t;
typedef enum { KB_HOST_PARSE, KB_VOICE_PARSE, KB_DISPLAY_PARSE } KBParse_t;

/* Methods for manipulating the knowledge base */

extern KB      KB_create(EKB ekb);
extern void    KB_destroy(KB kb);
extern V_Err   KB_status(KB kb);

extern Bool    KB_load(KB kb, VFILE recFile, Bool verbose);
extern Bool    KB_reconcile(KB kb, MODSET models);

extern Char  * KB_name(KB kb);

extern UInt    KB_numVocabularies(KB kb);
extern VOCAB   KB_vocabulary(KB kb, UInt vocabId);
extern VOCAB   KB_vocabularyByName(KB kb, Char * name);
extern VOCAB   KB_firstVocab(KB kb, UInt * vocabIdPtr);
extern VOCAB   KB_nextVocab(KB kb, UInt * vocabIdPtr);

extern UInt    KB_numGrammars(KB kb);
extern SYNTAX  KB_grammar(KB kb, UInt grammarId);
extern SYNTAX  KB_grammarByName(KB kb, Char * name);
extern UInt    KB_firstGrammarId(KB kb);
extern UInt    KB_unusedGrammarId(KB kb);

extern Bool    KB_loadResponseData(KB kb, VFILE recFile, Bool verbose);
extern FLOFF * KB_translateWordId(KB kb, UInt type, UInt wordId, UInt vocabId, UInt grammarId);
extern Bool    KB_response(KB kb, UInt type, HYP hyp, VFILE recFile, UInt vocabId, UInt grammarId, Char *buf, UInt bufLen, UInt * sizeNeeded);
extern DELIN   KB_getDelin(KB kb, UInt type, UInt vocabId, UInt grammarId);

#ifdef __cplusplus
}
#endif
#endif /* _KB_H_ */
