/******************************************************************************
 * 
 * File:         SE.H
 *
 * Description:  Header file for the VISE Search Engine
 *
 *****************************************************************************/

#ifndef SE_
#define SE_

#include "vbxtypes.h"  /* The basic VISE types */
#include "viseerr.h"
#include "ifacedef.h"
#include "ifaces.h"

  /* An instance of the VISE Search Engine */
typedef struct vise_s * SE;

#ifdef __cplusplus
extern "C" { 
#endif

  /* A Search Engine's public functions */
extern SE    SE_create(IMail * mail, IMemory * memory, V_Err * status);
extern void  SE_destroy(SE se);
extern void  SE_run(SE se);

#ifdef __cplusplus
}
#endif

#endif /* defined SE_ */
