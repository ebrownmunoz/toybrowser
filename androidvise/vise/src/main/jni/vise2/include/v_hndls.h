#ifndef V_HNDLS_

/******************************************************************************
 * 
 * File:        v_hndls.h
 *
 * Description: Header file containing VISE handles
 *
 *****************************************************************************/

#define V_HNDLS_

typedef struct ai_s    *ai;   /* Arc Instance           */
typedef struct am_s    *am;   /* Arc Model              */
typedef struct gi_s    *gi;   /* Grammar Instance       */
typedef struct gm_s    *gm;   /* Grammar Model          */
typedef struct jin_s   *jin;  /* Jin for the world      */
typedef struct jin2_s  *jin2; /* Jin for messages       */
typedef struct ki_s    *ki;   /* Kernel Instance        */
typedef struct km_s    *km;   /* Kernel Model           */
typedef struct kt_s    *kt;   /* Kernel Training Object */
typedef struct msg_s   *msg;  /* Message Descriptor     */
typedef struct ni_s    *ni;   /* Node Instance          */
typedef struct nm_s    *nm;   /* Node Model             */
typedef struct tbf_s   *tbf;  /* Traceback Frame        */
typedef struct tbn_s   *tbn;  /* Traceback Node         */
typedef struct tbp_s   *tbp;  /* Traceback Path Element */
typedef struct tem_s   *tem;  /* Template               */
typedef struct wi_s    *wi;   /* Word Instance          */
typedef struct wm_s    *wm;   /* Word Model             */

#endif /* V_HNDLS_ */
