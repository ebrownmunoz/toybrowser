/*
 *  tutil.h: Utilities for Units training support.
 */
#ifndef _TUTIL_H
#define _TUTIL_H

#include "types.h"
#include "units.h"
#include "dics.h"

#define INFIN 2147483647L    /* Infinite value for segment function */

typedef struct node_s  * NOD;

#ifdef __cplusplus
    extern "C" {
#endif

extern Bool  iniToVersion(Int feVersion);
extern Int   getPhInfo(Char ** phString, Short ** phEnds, Char * fName,
                      Int jinCount, Int sampleRate);
extern Int   getDuInfo(Char ** duString, Short ** duEnds, Char * fName,
                      PHS phset, Int jinCount, Int sampleRate);
extern Int   getTrnFromPhn(TRN *trnArr, DUS duset, Char * fName, PHS phset,
                          Int jinCount, Bool doEnds, Int sampleRate);
extern Int   getTrnFromDun(TRN *trnArr, DUS duset, Char *fName, Int jinCount);
extern Bool  putTrn(Char *fName, TRN trnArr, Int duCount);
extern ULong segment(TRN trnArr, Int duCount, jin jinbuf, Int jinCount);
extern Int   phonesFromWords(Char ** phString, Char * wordString, GDIC gdic);
extern Int   nodesFromPhn(NOD * nodes, DUS duset, Char *phString, Int phCount);
extern ULong genseg(NOD nodes, Int nodCount, jin jinbuf, Int jinCount,
                    TRN * trnArr, Int * duCount);
#ifdef __cplusplus
    }
#endif /* __cplusplus */
#endif /* _TUTIL_H */
