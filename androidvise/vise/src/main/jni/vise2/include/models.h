/* models.h - VISE Word Model Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: models.h[1.1] Wed Oct  8 13:53:07 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _MODELS_H_
#define _MODELS_H_

#include "types.h"
#include "vise.h"
#include "vfile.h"
#include "viseerr.h"


/* Set of vocabulary models */
typedef struct modset_s  * MODSET;

/* Word Models for a vocabulary */
typedef struct models_s  * MODELS;

/* Iterator over a vocabulary */
typedef struct moditer_s * MODITER;

#ifdef __cplusplus
extern "C" { 
#endif

/* Methods for manipulating all the word models for a speaker */
extern MODSET   MODSET_create(void);
extern void     MODSET_destroy(MODSET modset);
extern V_Err    MODSET_status(MODSET modset);
extern Bool     MODSET_load(MODSET modset, VFILE voiFile, Bool verbose);
extern Bool     MODSET_save(MODSET modset, VFILE voiFile, Bool verbose);
extern MODELS   MODSET_models(MODSET modset, UInt vocabId);
extern Int      MODSET_jinVersion(MODSET modset);
extern void     MODSET_setVoiFile(MODSET modset, VFILE voiFile);
extern VFILE    MODSET_voiFile(MODSET modset);
extern void     MODSET_lock(MODSET modset);
extern void     MODSET_unlock(MODSET modset);

/* Methods for manipulating a block (vocabulary) of word models */
extern MODELS   MODELS_create(void);
extern void     MODELS_destroy(MODELS models);
extern V_Err    MODELS_status(MODELS models);
extern UInt     MODELS_addModel(MODELS models, FUPATE * model);
extern Bool     MODELS_getModel(MODELS models, UInt modelId, FUPATE * model);
extern Bool     MODELS_putModel(MODELS models, UInt modelId, FUPATE * model);
extern UInt     MODELS_firstModel(MODELS models, FUPATE * model);
extern UInt     MODELS_nextModel(MODELS models, FUPATE * model);
extern UInt     MODELS_numNew(MODELS models);
extern Bool     MODELS_mustSave(MODELS models);
extern Char   * MODELS_vocabName(MODELS models);
extern Bool     MODELS_getTraining(MODELS models, UInt * sum, UInt * sumSq, UInt * count, Bool reset);
extern Bool     MODELS_setTraining(MODELS models, UInt sum, UInt sumSq, UInt count, Bool reset);
extern Int      MODELS_trainScheme(MODELS models);
extern void     MODELS_setSchemeBatch(MODELS models);

/* Methods for iterating over a models block */
extern MODITER  MODITER_create(MODELS models);
extern void     MODITER_destroy(MODITER moditer);
extern UInt     MODITER_nextModelId(MODITER moditer);

/* Methods for manipulating a single word model */
extern Bool     MODEL_init(FUPATE * model, Char * name, UInt numKernels);
extern Bool     MODEL_copy(FUPATE * dst, FUPATE * src, Bool copyDwells, Bool copyName);
extern Bool     MODEL_rename(FUPATE * model, Char * name);
extern jin      MODEL_template(FUPATE * model, UChar kid);
extern void     MODEL_swapDwells(FUPATE * model, UChar kid, UChar * minDwell, UChar * maxDwell);
extern void     MODEL_initializeDwells(FUPATE * pattern);

#ifdef __cplusplus
}
#endif
#endif /* _MODELS_H_ */
