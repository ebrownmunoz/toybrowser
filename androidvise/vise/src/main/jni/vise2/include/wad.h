    /* Word Add exports header */
#ifndef _WAD_H
#define _WAD_H

#ifdef __cplusplus
    extern "C" {
#endif

#include "types.h"
#include "recfile.h"

   /* Word Add Object */
typedef struct wad_s * WAD;

#define  WAD_SUCCESS                   0
#define  WAD_OUT_OF_MEMORY           -31
#define  WAD_CANT_OPEN_CFG           -32
#define  WAD_ILLEGAL_FETYPE          -33
#define  WAD_ILLEGAL_GENDER          -34
#define  WAD_CANT_FIND_DIR           -35
#define  WAD_BAD_PARAMETERS          -36
#define  WAD_NO_RESULT               -37
#define  WAD_CANT_CREATE_CFG         -38
#define  WAD_CANT_PARSE_CFG          -39
#define  WAD_NO_LANGUAGE_IN_CFG      -40
#define  WAD_BAD_LANGUAGE_IN_CFG     -41
#define  WAD_LANGUAGE_NOTSUPPORTED   -42
#define  WAD_SHORT_BUFFER            -43

 /* Exported functions */
extern  WAD       WAD_create(UShort feVersion, UShort gender, char *language,
                             char *cfgFN, int *rc);
extern  void      WAD_destroy(WAD wad);
extern  int       WAD_makeModel(WAD wad, char *wordName, char *ortho, FUPATE *model);
extern  int       WAD_modelInfo(WAD wad, char *ortho, int orthoLen,
                                char *phone, int phoneLen, char *units, int unitsLen);
extern  FUPATHEAD *WAD_getSilenceHeader(WAD wad);
extern  FUPATPARM *WAD_getParameters(WAD wad);
extern  FUPATE    *WAD_getSilenceModels(WAD wad);
 /* May be called without object instantiation */
extern  FKEY      *WAD_getKey(void);
extern  FSTRING   *WAD_getVer(void);
extern  void      WAD_version(UShort *version, UShort *build);

#ifdef __cplusplus
    }
#endif   /* __cplusplus */

#endif /* _WAD_H */
