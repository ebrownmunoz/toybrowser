/******************************************************************************
 * 
 * File:         VISE.H
 *
 * Description:  Common Type and Constant Definitions for VISE
 *
 *****************************************************************************/

#ifndef VISE_

#define VISE_

#include <stddef.h>
#include <limits.h>
#include "v_hndls.h"         /* Handles for all the VISE objects */
#include "vbxtypes.h"        /* The basic types */

  /* Frame sequence number */
#include "sn.h"

  /* Recognition time, in frames */
typedef V_ULong    V_Time;
#define VTIMESIZE  VULONGSIZE

  /* Infinity for VISE recognition scores */
#define V_INFINITY 32767

  /* The identifier types need only permit equality comparisons (== or !=) */
typedef V_Uns      V_WId;    /* 16 bits - a word ID */
typedef V_Uns      V_GId;    /* 16 bits - a grammar ID */
typedef V_Uns      V_NId;    /* 16 bits - a node ID */

  /* Parameters which determine the size of the traceback buffer */
#define MAXDWELL   255       /* Maximum number of frames in a word */
#define NUMKERNEL  42        /* Maximum number of kernels in a word model */

#define MAXKERNELDWELL 24    /* Maximum number of frames a kernel can absorb */

#endif
