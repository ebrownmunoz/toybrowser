/* hyp.h - VISE Hypothesis Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _HYP_H_
#define _HYP_H_

#include "types.h"
#include "sn.h"

#define UNITSCORE 0

 /* Word Hypothesis Structure */
typedef struct wrdhyp_s {
  UInt    wid;
  UInt    numFrames;
  SN      startSN;
  SN      endSN;
  Int     score;
  Int     globalMin;
} wrdhyp_t, * WRDHYP;

 /* Hypothesis Structure */
typedef struct hyp_s {
  WRDHYP  words;
  Long    totalScore;
  UInt    maxWordCount;
  UInt    wordCount;
  UInt    grammarId;
  UInt    startNode;
  UInt    endNode;
} hyp_t, * HYP;

#include "vocab.h"

#ifdef __cplusplus
extern "C" { 
#endif

 /* Hypothesis Methods */
extern HYP    HYP_create(UInt nBest);
extern void   HYP_destroy(HYP hyp);
extern HYP    HYP_clone(HYP hyp);
extern Bool   HYP_setWordCount(HYP hyp, UInt wordCount);
extern WRDHYP HYP_words(HYP hyp, UInt * wordCountPtr);
extern WRDHYP HYP_findWord(HYP hyp, UInt wid);
extern UInt   HYP_numAlternatives(HYP hyp);
extern void   HYP_setSyntaxData(HYP hyp, UInt grammarId, UInt startNode, UInt endNode);
extern void   HYP_getSyntaxData(HYP hyp, UInt * grammarIdPtr, UInt * startNodePtr, UInt * endNodePtr);
extern void   HYP_boundaries(HYP hyp, SN * startSN, SN * endSN);

extern Char * HYP_text(HYP hyp, VOCAB vocab);
extern void   HYP_print(HYP hyp, VOCAB vocab, VOCAB silences);

#define HYP_wordCount(H)          ((H)->wordCount)

#define HYP_totalScore(H)         ((H)->totalScore)
#define HYP_setTotalScore(H,S)    (H)->totalScore = (S)

#define HYP_wid(H,W)              ((H)->words[W].wid)
#define HYP_setWId(H,W,I)         (H)->words[W].wid = (I)
#define HYP_startSN(H,W)          ((H)->words[W].startSN)
#define HYP_setStartSN(H,W,S)     (H)->words[W].startSN = (S)
#define HYP_endSN(H,W)            ((H)->words[W].endSN)
#define HYP_setEndSN(H,W,S)       (H)->words[W].endSN = (S)
#define HYP_score(H,W)            ((H)->words[W].score)
#define HYP_setScore(H,W,S)       (H)->words[W].score = (S)

 /* Word Hypothesis Methods */
#define WRDHYP_wid(W)             ((W)->wid)
#define WRDHYP_setWId(W,I)        (W)->wid = (I)
#define WRDHYP_startSN(W)         ((W)->startSN)
#define WRDHYP_setStartSN(W,S)    (W)->startSN = (S)
#define WRDHYP_endSN(W)           ((W)->endSN)
#define WRDHYP_setEndSN(W,S)      (W)->endSN = (S)
#define WRDHYP_score(W)           ((W)->score)
#define WRDHYP_setScore(W,S)      (W)->score = (S)
#define WRDHYP_globalMin(W)       ((W)->globalMin)
#define WRDHYP_setGlobalMin(W,G)  (W)->globalMin = (G)
#define WRDHYP_numFrames(W)       ((W)->numFrames)
#define WRDHYP_setNumFrames(W,N)  (W)->numFrames = (N)

#ifdef __cplusplus
}
#endif
#endif /* _HYP_H_ */
