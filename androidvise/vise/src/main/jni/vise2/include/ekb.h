/* ekb.h - VISE Engine Knowledge Base
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: ekb.h[1.0] Thu Apr 10 20:54:01 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _EKB_H_
#define _EKB_H_

#include "types.h"

/* Knowledge Base */
typedef struct ekb_s * EKB;

#include "viseerr.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* Methods for manipulating the engine knowledge base */

extern EKB     EKB_create();
extern void    EKB_destroy(EKB kb);
extern V_Err   EKB_status(EKB kb);

extern Bool    EKB_load(EKB kb, Ptr engineFile, Bool verbose);

#ifdef __cplusplus
}
#endif
#endif /* _EKB_H_ */
