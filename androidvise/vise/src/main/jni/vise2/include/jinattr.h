/* jinattr.h - the jin extractor attributes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: jinattr.h[1.0] Tue Jun  9 11:50:21 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _IJINATTR_H_
#define _IJINATTR_H_

#include "types.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/* The Feature Types */
typedef enum {CEP_FEAT, DCEP_FEAT, NUM_FEATURES} jinfeat_t;

/* Jin Frame Attributes */

/* Create a JIN attributes object */
extern IGEN  JINATTR_create(IMemory * imem);

/* A JIN Attributes Interface */
typedef IOBJATTR IJINATTR;
typedef iobjattr_t ijinattr_t;

/* JIN Attributes Methods */
extern Bool  IJINATTR_getVecLen(IJINATTR ijinattr, Int *vecLen);
extern Bool  IJINATTR_setVecLen(IJINATTR ijinattr, int vecLen);

#ifdef __cplusplus
}
#endif
#endif /* _IJINATTR_H_ */
