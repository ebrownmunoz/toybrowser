#ifndef V_TB_

/******************************************************************************
 * 
 * File:        V_TB.H
 *
 * Description: Header file for Traceback module
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_TB.H_V  $
 * 
 *    Rev 1.6   20 Mar 1992 13:45:16   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.5   29 Jan 1992 17:24:24   DCV
 * Changed basic types
 * 
 *    Rev 1.4   20 Sep 1991 09:58:36   DCV
 * Added sumbestscores as argument to tb_addframe()
 * 
 *    Rev 1.3   12 Jun 1991 15:18:46   DCV
 * Rewritten for new traceback tree structure to support N-best path reporting,
 *   including moving old partial_traceback() function here as trace(time).
 * 
 *    Rev 1.2   25 Sep 1990 10:37:06   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 1.1   19 Sep 1990  9:22:28   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:48:36   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_TB_ 0

/*
 * The Traceback Tree contains a repository of traceback path elements,
 * which are linked into lists headed by traceback nodes, which in turn
 * are linked into traceback frames, one for each frame of speech data.
 */

/* The Traceback Tree Descriptor (instantiated once in the VISE descriptor) */
typedef struct tb_s {
  tbf tbnextframe;      /* The head of the next tbframe */
  tbf tbmatureframe;    /* The oldest unpruned frame  */
  tbf tblastimmortal;   /* The youngest immortal frame */
} tb_t, * tb;

#include "v_vise.h"

extern tbf     tb_addframe(VISE vise, tbn nodelist, V_Int nodecount, V_Time time, V_SN sn, V_Long sumbestscores, V_Long sigmaglobalmin);
extern tbf     tb_init(VISE vise);
extern tbf     tb_nextframe(VISE vise);
extern V_Void  tb_prune(VISE vise, V_Time time);
extern V_Void  tb_removeframe(VISE vise, tbf frame);

#endif /* V_TB_ */
