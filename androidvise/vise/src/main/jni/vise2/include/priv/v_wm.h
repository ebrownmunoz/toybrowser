#ifndef V_WM_

/******************************************************************************
 * 
 * File:    V_WM.H
 *
 * Description: Header file for Word Model module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_WM.H_V  $
 * 
 *    Rev 1.6   20 Mar 1992 13:49:12   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.5   29 Jan 1992 17:32:18   DCV
 * Changed basic types
 * 
 *    Rev 1.4   20 Sep 1991 09:50:20   DCV
 * Added a word class field to word models
 * 
 *    Rev 1.3   14 Aug 1991 17:18:44   DCV
 * Replaced KM_ALLOC with a VISE parameter.
 * 
 *    Rev 1.2   18 Jul 1991 16:55:00   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.1   19 Sep 1990  9:24:20   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:52:28   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_WM_ 0

#include "v_vise.h"

/* Possible values for word model status (wm_ptr->wmstatus) */
#define NONEXISTENT    1   /* Word does not exist (NOT USED) */
#define UNINITIALIZED  2   /* Word has no acoustic representation */
#define TEMPLATES      3   /* Word has templates */
#define WILDCARD       4   /* Word has wildcards */

/*
 * The Word Model Descriptor is exported via a wm_t type declaration
 * in the Vocabulary Entry Descriptor, but in any case has to be defined
 * here in the header for the macros to work.
 */
typedef struct wm_s {
  V_WId  wmid;             /* The word identification number */
  V_Uns  wmclass;          /* The word class number */
  V_Int  wmstatus;         /* The status of the word */
  V_Int  wmkernelcount;    /* Number of kernels in word */
  km     wmfirstkernel;    /* The first kernel in the word */
  kt     wmfirstkt;        /* The first kernel training element in the word */
} wm_t;

/* The Kernel Model Pool Descriptor (instantiated once in the VISE descriptor) */
typedef struct kmpool_s {
  km     freekernel;       /* List of available kernel models */
  kt     freekt;
} kmpool_t, * KMPOOL;


extern V_Bool  wm_allocate(VISE vise, wm word, V_WId id, V_Uns class, V_Int numkernels);
extern V_Bool  wm_allocatekt(VISE vise, wm word);
extern V_Uns   wm_class(wm word);
extern V_Void  wm_deallocate(VISE vise, wm word);
extern km      wm_firstkernel(wm word);
extern V_WId   wm_id(wm word);
extern V_Void  wm_init(VISE vise);
extern V_Int   wm_numkernels(wm word);
extern V_Void  wm_setclass(wm word, V_Uns class);
extern V_Void  wm_setstatus(wm word, V_Int status);
extern V_Void  wm_setwildcard(wm word,V_Int relative, V_Int absolute);
extern V_Int   wm_status(wm word);
extern V_Void  wm_wildcard(wm word, V_Int *relative, V_Int *absolute);

#ifndef NOMACROS
#define wm_id(W)           ((W)->wmid)
#define wm_class(W)        ((W)->wmclass)
#define wm_firstkernel(W)  ((W)->wmfirstkernel)
#define wm_numkernels(W)   ((W)->wmkernelcount)
#define wm_status(W)       ((W)->wmstatus)
#define wm_setclass(W,C)   ((W)->wmclass = (C))
#define wm_setstatus(W,S)  ((W)->wmstatus = (S))
#endif

#define wm_firstkt(W)      ((W)->wmfirstkt)

#endif /* V_WM_ */
