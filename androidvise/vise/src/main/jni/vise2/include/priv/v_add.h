#ifndef V_ADD_

/******************************************************************************
 * 
 * File:    V_ADD.H
 *
 * Description: Header file for VISE addition functions
 *
 * Computer:  
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_ADD.H_V  $
 * 
 *    Rev 1.1   31 Aug 1992 12:32:52   DCV
 * Fixed the bug: now score remains V_INFINITY once it becomes V_INFINITY.
 * 
 *    Rev 1.0   20 Mar 1992 13:26:48   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_ADD_ 0

extern V_Int add_inf(V_Int score, V_Int cost);

/* Assuming that S is not negative, but placing no restrictions on C,
 infadd(S,C) sums S and C and hard limits the result to V_INFINITY,
 while assuring that no intermediate result exceeds V_INFINITY.  For
 example, with V_INFINITY set to 32767, it can be used to add path
 scores of type V_Int, which are never negative, to frame costs of
 the same type, which may be, without fear of overflowing V_Int. */

#define add_inf(S,C) (((S) == V_INFINITY || (C) > (V_INFINITY - (S))) ? V_INFINITY : ((S) +(C)))

#endif /* V_ADD_ */
