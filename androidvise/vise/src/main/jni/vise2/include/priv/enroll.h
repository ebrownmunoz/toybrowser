#ifndef ENROLL_DEFS

#define ENROLL_DEFS

#include "vultdef.h"

						/*----------------------------------*/
						/*----- ENROLL Cluster Defines -----*/
						/*----------------------------------*/

/* Enrollment Constants. Assume no compression here */

#define ENROLL_MAXSAMPLES              120	/* Data base dependent */
#define ENROLL_MAXWORDDURATION         200
/* #define ENROLL_MINWORDDURATION         10 */
#define ENROLL_MAXNUMTEMPLATES         4
#define ENROLL_MINSAMPLES              2

#define ENROLL_MAXKERNELSINWORD        14
#define ENROLL_MINKERNELSINWORD        4	
/* #define ENROLL_FRAMESPERKERNEL         60	    10 * ACTUAL_VALUE */
/* #define ENROLL_MINDWELL                2 */
#define ENROLL_MAXDWELL                11
#define ENROLL_MAXDIFFBOUNDARIES       4
#define ENROLL_MAXRANGE                60
#define ENROLL_EXTENSION	       10

#define ENROLL_MAXWORDNUM	       10000
#define VISENP_NUM_PARAMETERS	       16
#define ENROLL_MAX_FILENAME_LEN	       256 /* Watch! The same as in VBX.C */
#define ENROLL_MAX_WORD_LENGTH	       256

				/*-------------------------------------------*/
                                /*----- ENROLL Globals for Export ----- -----*/
				/*-------------------------------------------*/

extern Uns jinVersion;
extern Uns ENROLL_MINWORDDURATION;
extern Uns ENROLL_FRAMESPERKERNEL;
extern Uns ENROLL_MINDWELL;
extern Uns SS_MAX_DWELL;
extern Uns LS_MIN_DWELL;

				/*-------------------------------------------*/
				/*----- ENROLL Function Protos --------------*/
				/*-------------------------------------------*/

Int enroll_word(UWORD16	wordMultID, Int numTemplates);
Int edit_word(Int numKernels);

#endif


