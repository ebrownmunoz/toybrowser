#ifndef V_TM_

/******************************************************************************
 * 
 * File:				V_TM.H
 *
 * Description:	Header file for Template Matcher module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_TM.H_V  $
 * 
 *    Rev 1.4   20 Mar 1992 13:46:28   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.3   29 Jan 1992 17:29:16   DCV
 * Changed basic types
 * 
 *    Rev 1.2   12 Jun 1991 15:26:56   DCV
 * Added declaration of function returning globalmin.
 * 
 *    Rev 1.1   19 Sep 1990  9:23:30   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:50:16   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_TM_ 0

#include "v_vise.h"
#include "v_jin.h"				/* jinr is referred to in tm_t below */

/* The Template Matcher Descriptor (instantiated once in the VISE descriptor) */
typedef struct tm_s	{
	V_Time	time;
	V_Int	oldbestscore;
	V_Int	status;
	V_Int	globalmin;
	jinr	jinframe;
} tm_t, * TM;


extern V_Void	tm_init(VISE vise, jin jinframe, V_Time time, V_Int oldbestscore);
extern V_Int	tm_globalmin(VISE vise);
extern V_Int	tm_match(VISE vise, tem template);
extern V_Void	tm_setstatus(VISE vise, V_Int status);

#ifndef	NOMACROS
#define tm_setstatus(vise, S)	  vise->temMatcher.status = (S)
#endif

#endif /* V_TM_ */
