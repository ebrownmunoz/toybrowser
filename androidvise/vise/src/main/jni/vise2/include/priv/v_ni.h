#ifndef V_NI_

/******************************************************************************
 * 
 * File:    V_NI.H
 *
 * Description: Header file for Node Instance module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_NI.H_V  $
 * 
 *    Rev 1.10   20 Mar 1992 13:41:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.9   29 Jan 1992 17:12:36   DCV
 * Changed basic types
 * 
 *    Rev 1.8   12 Nov 1991 17:45:40   DCV
 * Major revision which eliminates the inherited path array in favor of
 *   a list of inherited paths (tbps).  The new fields nipathlist and
 *   niworstscore replace the old path array, and the new field
 *   niseedpath, a tbp, replaces the old niseednode, which was a tbn.
 *   The old numpaths global disappears.
 * 
 *    Rev 1.7   20 Sep 1991 17:25:36   DCV
 * New version using DGNs to distinguish between paths.  Doesn't yet
 * distinguish between paths with same contents in different order.
 * Replaced ni_insertpath() (which becomes static) with ni_insertpaths().
 * 
 *    Rev 1.6   12 Sep 1991 14:34:10   DCV
 * Corrected comment
 * 
 *    Rev 1.5   02 Aug 1991 09:46:14   DCV
 * Added code to deal with equivalence classes of words
 * 
 *    Rev 1.4   18 Jul 1991 16:49:26   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.3   12 Jun 1991 15:12:38   DCV
 * Rewritten to support N-best path reporting.
 * 
 *    Rev 1.2   07 Nov 1990 17:10:14   DCV
 * Defined NI_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:21:32   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:44:52   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_NI_ 0

/* The Node Instance Representation is exposed in nullarcs(), process() and gramnodes() */
typedef struct ni_s {
  tbp    niseedpath;    /* Traceback path that goes with score */
  tbp    nipathlist;    /* List of inherited path information */
  V_Int  niseedscore;   /* Seed score for inheritance out */
  V_NId  niid;          /* Id of this node */
  V_Int  niworstscore;  /* The worst inherited path score  */
} ni_t;

#define NI_SIZE sizeof(ni_t)

extern V_NId   ni_id(ni node);
extern V_Void  ni_getseed(ni node, V_Int *pscore, tbn *ptbpath);
extern V_Void  ni_nodeinit(ni node, V_NId nodeid);
extern tbp     ni_pathlist(ni node);
extern V_Void  ni_putpathlist(ni node, tbp tbpath);
extern V_Void  ni_putseed(ni node, V_Int score, tbn tbpath);
extern tbp     ni_seedpath(ni node);
extern V_Int   ni_seedscore(ni node);
extern V_Void  ni_setseedscore(ni node, V_Int score);
extern V_Void  ni_setworstscore(ni node, V_Int score);
extern V_Int   ni_worstscore(ni node);

#ifndef NOMACROS
#define ni_id(N)              ((N)->niid)
#define ni_getseed(N,S,T)     (*(S)=(N)->niseedscore),(*(T)=(N)->niseedpath)
#define ni_nextnode(N)        ((N)+1)
#define ni_nodeinit(N,I)      ((N)->niid = I),\
                              ((N)->nipathlist = NULL), \
                              ((N)->niseedscore = V_INFINITY), \
                              ((N)->niseedpath = NULL), \
                              ((N)->niworstscore = V_INFINITY)
#define ni_pathlist(N)        ((N)->nipathlist)
#define ni_prevnode(N)        ((N)-1)
#define ni_putpathlist(N,P)   ((N)->nipathlist = (P))
#define ni_putseed(N,S,T)     ((N)->niseedscore=S),((N)->niseedpath=T)
#define ni_seedpath(N)        ((N)->niseedpath)
#define ni_seedscore(N)       ((N)->niseedscore)
#define ni_setseedscore(N,S)  ((N)->niseedscore=S)
#define ni_setworstscore(N,S) ((N)->niworstscore=S)
#define ni_worstscore(N)      ((N)->niworstscore)
#endif

#endif /* V_NI_ */
