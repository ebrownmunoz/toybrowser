#ifndef V_JIN_

/******************************************************************************
 * 
 * File:         V_JIN.H
 *
 * Description:  Header file for the JIN module
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_JIN.H_V  $
 * 
 *    Rev 1.8   20 Mar 1992 13:39:12   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.7   22 Nov 1991 12:05:40   DCV
 * Added definition of JINMAXVALUE
 * 
 *    Rev 1.6   08 Oct 1991 09:33:42   DCV
 * Added jin_settime() and jin_writetime() functions.
 * 
 *    Rev 1.5   14 Aug 1991 17:09:46   DCV
 * Replaced JIN_ALLOC with a VISE parameter.
 * Changed prototype for jin_canread().
 * 
 *    Rev 1.4   18 Jul 1991 16:46:54   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.3   20 Dec 1990 14:18:02   DCV
 * Added jin_unread() and jin_maxunread()
 * 
 *    Rev 1.2   07 Nov 1990 17:00:16   DCV
 * Defined JIN_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:20:18   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 13:08:12   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_JIN_ 0

/* A Jin Source Descriptor */
typedef struct jinsrc_s * JINSRC;

#include "jincnvrt.h"
#include "sn.h"
#include "v_err.h"
#include "v_vise.h"
#include "vbxtypes.h"

extern JINSRC  JINSRC_create(VISE);
extern V_Bool  JINSRC_init(JINSRC);
extern V_Void  JINSRC_destroy(JINSRC);
extern V_Bool  JINSRC_canread(JINSRC, V_SN, V_SN);
extern V_Bool  JINSRC_extract(JINSRC, V_SN *, jin2, V_Err *);
extern V_Bool  JINSRC_insert(JINSRC, V_SN *, jin2, V_Err *);
extern V_SN    JINSRC_numframes(JINSRC, V_SN *, V_SN *);
extern V_Err   JINSRC_pad(JINSRC vise);
extern V_Bool  JINSRC_read(JINSRC, V_SN *, jin, V_Err *);
extern V_Bool  JINSRC_resetQueue(JINSRC);
extern V_Bool  JINSRC_writeEXC(JINSRC, V_Err);
extern V_Bool  JINSRC_writeFrame(JINSRC, VBXMSG);

#endif /* V_JIN_ */
