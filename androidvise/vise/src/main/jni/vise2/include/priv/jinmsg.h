/* jinmsg.h - a jin frame source which reads from VISE's incoming VBXMSG
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A jin frame source which reads frames from VISE's incoming VBXMSG
 *
 * Verbex Voice Systems (cav)
 * $Header: jinmsg.h[1.0] Tue Jun  9 16:39:07 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _JINMSG_H_
#define _JINMSG_H_

#include "types.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "jinattr.h"
#include "v_vise.h"
#include "visemsg.h"

#ifdef __cplusplus
extern "C" {
#endif


/*** JINMSG ***/

/* Create a JINMSG */
extern IGEN  JINMSG_create(VISE vise);

/* JINMSG Interface */
typedef struct iqueue_s * IJINMSG;

/* JINMSG Methods */
extern Bool  IJINMSG_writeFrame(IJINMSG ijinmsg, VBXMSG message);
extern Bool  IJINMSG_writeEXC(IJINMSG ijinmsg, EXC exception);
extern OBJT  IJINMSG_newJinFrame(IJINMSG ijinmsg);


/*** JINMSG Attributes ***/

/* Create a JINMSG attributes object */
extern IGEN  JINMSGATTR_create(IMemory * memory);

/* A JINMSG attributes interface */
typedef IATTR IJINMSGATTR;
typedef iattr_t ijinmsgattr_t;

/* JINMSG Attributes Methods */
extern Bool  IJINMSGATTR_setClassSize(IJINMSGATTR ijinmsgattr, Uns classSize);
extern Bool  IJINMSGATTR_setFrameAttr(IJINMSGATTR ijinmsgattr, IJINATTR ijinattr);
extern Bool  IJINMSGATTR_setMaxGapLen(IJINMSGATTR ijinmsgattr, SN maxGapLen);
extern Bool  IJINMSGATTR_setMaxReject(IJINMSGATTR ijinmsgattr, SN maxReject);
extern Bool  IJINMSGATTR_setGrowQuant(IJINMSGATTR ijinmsgattr, Uns growQuant);
extern Bool  IJINMSGATTR_setMaxLatent(IJINMSGATTR ijinmsgattr, Uns maxLatent);

#ifdef __cplusplus
}
#endif
#endif /* _JINMSG_H_ */
