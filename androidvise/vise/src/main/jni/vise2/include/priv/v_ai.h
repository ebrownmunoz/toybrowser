#ifndef V_AI_

/******************************************************************************
 * 
 * File:    V_AI.H
 *
 * Description: Header file for Arc Instance module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_AI.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:27:44   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Jan 1992 16:42:32   DCV
 * Changed basic types
 * 
 *    Rev 1.5   12 Nov 1991 18:16:26   DCV
 * Changed prototype for ai_addword(), which now returns int_t.
 * 
 *    Rev 1.4   18 Jul 1991 16:36:28   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.3   20 Dec 1990 14:04:10   DCV
 * Replaced ai_active() with ai_activeCount()
 * 
 *    Rev 1.2   07 Nov 1990 16:47:06   DCV
 * Defined AI_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:16:16   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:31:22   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_AI_ 0

#include "v_vise.h"

/*
 * The Arc Instance Descriptor is not for export, but unfortunately
 * must be defined here for the macros and memory allocation to work.
 */
typedef struct ai_s {
 ni     aisource;       /* Source Node         */
 ni     aidestination;  /* Destination Node    */
 wi     aifirstword;    /* First word in arc   */
 wi     ailastword;     /* Last word in arc    */
 V_Int  aiactivecount;  /* Number words active */
 V_Bool aifullyactive;  /* All words active?   */
} ai_t;

#define AI_SIZE  sizeof(ai_t)

/* A Word Instance Pool Descriptor (the sole instance is in the VISE descriptor) */
typedef struct wipool_s {
  wi    firstwi;
  wi    lastwi;
} wipool_t, * WIPOOL;

extern V_Void ai_activate(ai);
extern V_Int  ai_activeCount(ai);
extern V_Int  ai_addword(VISE, ai, wm);
extern V_Void ai_arcinit(ai, ni, ni);
extern V_Void ai_deactivate(ai);
extern ni     ai_destinationnode(ai);
extern V_Bool ai_init(VISE vise, V_ULong wicount);
extern wi     ai_firstword(ai);
extern wi     ai_lastword(ai);
extern ni     ai_sourcenode(ai);

#ifndef NOMACROS
#define ai_sourcenode(A)      (A)->aisource
#define ai_destinationnode(A) (A)->aidestination
#define ai_firstword(A)       (A)->aifirstword
#define ai_lastword(A)        (A)->ailastword
#define ai_deactivate(A)      (A)->aifullyactive=FALSE,--((A)->aiactivecount)
#define ai_activeCount(A)     ((A)->aiactivecount)
#endif

#endif /* V_AI_ */
