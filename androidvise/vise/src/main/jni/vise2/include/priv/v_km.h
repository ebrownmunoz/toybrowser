#ifndef V_KM_

/******************************************************************************
 * 
 * File:    V_KM.H
 *
 * Description: Header file for Kernel Model module
 *
 *****************************************************************************/

#define V_KM_ 0

#include "v_tem.h"        /* tem_t is referred to in km_t below */

/*
 * The Kernel Model Descriptor is not for export, but unfortunately
 * must be defined here in the header for the macros to work.
 */

typedef struct km_s {
  km      kmnextkernel;   /* The next kernel model in word */
  V_Int   kmmindwell;     /* Minimum dwell value */
  V_Int   kmoptdwell;     /* Optional dwell value */
  tem_t   kmtemplate;     /* Template object represents sound */
} km_t;

#define KM_SIZE  sizeof(km_t)

/* The Kernel Training structure (used only in batch training) */
typedef struct kt_s {
  kt      ktnextkt;       /* The next kernel training element in word */
  V_Long  kttraincount;   /* Number of frames absorbed during training */
  V_Long  ktaccumulator[NUMCOMPONENTS]; /* Sum of all absorbed frames */
  V_Int   ktobsmindw;     /* Mindwell observed during training */
  V_Int   ktobsmaxdw;     /* Maxdwell observed during training */
} kt_t;

#define KT_SIZE  sizeof(kt_t)

extern V_Void  km_dwells(km kernel, V_Int *mindwell, V_Int *optdwell);
extern V_Int   km_mindwell(km kernel);
extern km      km_nextkernel(km kernel);
extern V_Void  km_putdwells(km kernel, V_Int mindwell, V_Int optdwell);
extern V_Void  km_putnextkernel(km kernel, km nextkernel);
extern V_Void  km_puttemplate(km kernel, tem template);
extern tem     km_template(km kernel);

#define km_mindwell(K)           (K)->kmmindwell

#ifndef NOMACROS
#define km_dwells(K,M,O)         *(M) = (K)->kmmindwell, *(O) = (K)->kmoptdwell
#define km_putdwells(K,M,O)      (K)->kmmindwell = (M), (K)->kmoptdwell = (O)
#define km_nextkernel(K)         (K)->kmnextkernel
#define km_putnextkernel(K,N)    (K)->kmnextkernel = (N)
#define km_template(K)           (&((K)->kmtemplate))
#define km_puttemplate(K,T)      (K)->kmtemplate = *(T)
#endif

/* Macros for batch training */
#define kt_nextkt(K)             (K)->ktnextkt
#define kt_putnextkt(K,N)        (K)->ktnextkt = (N)
#define kt_trainingcount(K)      (K)->kttraincount
#define kt_settrainingcount(K,C) (K)->kttraincount = (C)
#define kt_uptrainingcount(K,I)  (K)->kttraincount += (I)
#define kt_accumulator(K,I)      (K)->ktaccumulator[I]
#define kt_setaccumulator(K,I,A) (K)->ktaccumulator[I] = (A)
#define kt_clearaccumulator(K)   { V_Int i; for (i = 0; i < NUMCOMPONENTS; i++) (K)->ktaccumulator[i] = 0; }
#define kt_accumulate(K,I,A)     (K)->ktaccumulator[I] += (A)
#define kt_getobsdwells(K,S,L)   *(S) = (K)->ktobsmindw, *(L) = (K)->ktobsmaxdw
#define kt_putobsdwells(K,S,L)   (K)->ktobsmindw = (S), (K)->ktobsmaxdw = (L)
#define kt_adjustobsdwells(K,D)  { if ((D) > (K)->ktobsmaxdw) (K)->ktobsmaxdw = (D); if ((D) < (K)->ktobsmindw) (K)->ktobsmindw = (D); }

#endif /* V_KM_ */

