#ifndef V_KI_

/******************************************************************************
 * 
 * File:				V_KI.H
 *
 * Description:	Header file for Kernel Instance module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_KI.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:39:32   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   12 Nov 1991 18:28:46   DCV
 * Replaced kitbnode with kitbpath in the knd structure.
 * Eliminated prototype for ki_getscore().
 * 
 *    Rev 1.5   08 Oct 1991 10:11:46   DCV
 * Added ki_knodecount() function.
 * 
 *    Rev 1.4   18 Jul 1991 16:47:56   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.3   12 Jun 1991 15:06:58   DCV
 * Cosmetic changes
 * 
 *    Rev 1.2   07 Nov 1990 17:00:54   DCV
 * Defined KND_SIZE and KI_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:20:44   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:41:38   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_KI_ 0

/*
 * The Kernel Node Representation is exposed in trainword.c, process.c
 * processw.c, processk.c and trainword.c.
 */
typedef struct knd_s * KND;
typedef struct knd_s {
	V_Int	kiscore;			/*	Score					*/
	tbp	    kitbpath;			/*	Traceback path element	*/
} knd_t;

/*
 * The Kernel Instance Representation is not for export, but nevertheless
 * must be defined here in the header for the macros and memory allocation
 * to work.
 */
typedef struct ki_s	{
	tem	kitemplate;				/*	Template object from model			*/
	V_Int	kimindwell;			/*	Minimum _& optional dwell values	*/
	V_Int	kioptdwell;			/*	These values derived from model	    */
	V_Int	kidwellcount;		/*	The self loop dwell count			*/
	KND	    kifirstknode;		/*	The first knode in the kernel		*/
	KND	    kilastknode;		/*	The last knode in the kernel		*/
} ki_t;
#define	KI_SIZE	 sizeof(ki_t)

/* The Kernel Node Pool descriptor (instantiated once in the VISE descriptor) */
typedef struct knpool_s {
    KND 	freeknode;
    V_ULong	freecount;
    V_ULong	knodecount;
} knpool_t, * KNPOOL;

#include "v_vise.h"

extern V_Bool	ki_allocate(VISE vise, ki kerninstance, V_Int mindwell);
extern V_Void	ki_getdwells(ki kerninstance, V_Int *pmindwell, V_Int *poptdwell, V_Int *pdwellcount);
extern KND		ki_lastknode(ki kerninstance);
extern KND		ki_firstknode(ki kerninstance);
extern V_Bool	ki_init(VISE vise, V_ULong kndcount);
extern V_ULong	ki_knodecount(VISE vise);
extern V_Void	ki_knodeinit(ki kerninstance);
extern V_Void	ki_putdwellcount(ki kerninstance, V_Int dwellcount);
extern tem		ki_template(ki kerninstance);
extern V_Bool	ki_putmodel(VISE vise, ki kerninstance, km model);

#ifndef	NOMACROS
#define ki_putdwellcount(K,D)	((K)->kidwellcount = D)
#define ki_getdwells(K,M,O,D)	(*(M) = K->kimindwell),	(*(O) = K->kioptdwell),	(*(D) = K->kidwellcount)
#define ki_lastknode(K)			((K)->kilastknode)
#define ki_firstknode(K)		((K)->kifirstknode)
#define ki_template(K)			((K)->kitemplate)
#endif

#endif /* V_KI_ */
