#ifndef V_VISE_

/******************************************************************************
 * 
 * File:        V_VISE.H
 *
 * Description: Header file for Main Vise module
 *
 *****************************************************************************/

#define V_VISE_

/* This typedef MUST precede the inclusions, since most of them need it */
typedef struct vise_s * VISE;

#include "ifaces.h"
#include "visemsg.h"
#include "v_ai.h"
#include "v_am.h"
#include "v_err.h"
#include "v_fyi.h"
#include "v_gi.h"
#include "v_gm.h"
#include "v_jin.h"
#include "v_ki.h"
#include "v_mem.h"
#include "v_par.h"
#include "v_pat.h"
#include "v_syn.h"
#include "v_tb.h"
#include "v_tbf.h"
#include "v_tbn.h"
#include "v_tbp.h"
#include "v_tm.h"
#include "v_voc.h"
#include "v_wi.h"
#include "v_wm.h"

 /* VISE Descriptor */
typedef struct vise_s {
   /* Memory System */
  IMemory    * memory;          /* SE_create() */
  MEMCFG       memCfg;          /* SE_create() */
   /* Mail System */
  IMail      * mail;            /* SE_create() */
  IMBox      * mailbox;         /* SE_create() */
  V_Uns        myMBoxId;        /* SE_create() */
  VBXMSG       incoming;        /* SE_create() */
  VBXMSG       outgoing;        /* SE_create() */
  V_Uns        rcMBoxId;        /* SE_run() */
  V_Uns        feMBoxId;        /* SE_run() */
   /* System */
  PARAM        parameters;      /* param_init() */
  fyi_t        fyi;             /* fyi_init() */
   /* Source */
  JINSRC       jinSource;       /* SE_create() */
   /* Search */
  path_t       path;            /* path_init() */
  voc_t        vocab;           /* voc_init() */
  syn_t        syntax;          /* syn_init() */
  ampool_t     amPool;          /* gm_init() */
  wapool_t     waPool;          /* am_init() */
  kmpool_t     kmPool;          /* wm_init() */
  wipool_t     wiPool;          /* ai_init() */
  kipool_t     kiPool;          /* wi_init() */
  knpool_t     knPool;          /* ki_init() */
  tb_t         tbtree;          /* tb_init() */
  tbfpool_t    tbfPool;         /* tbf_init() */
  tbnpool_t    tbnPool;         /* tbn_init() */
  tbppool_t    tbpPool;         /* tbp_init() */
  tm_t         temMatcher;      /* tm_init() */
  V_Time       jokeTimer;       /* gramnodes() */
  V_Bool       batchTrain;      /* STARTTRAININGWORD() */
  V_Bool       firstPass;       /* STARTTRAININGWORD() */
  V_ULong      maxFrames;       /* recloop() */
} vise_t;

extern V_Err  VISE_dispatch(VISE vise);
extern V_Err  VISE_busyDispatch(VISE vise, V_Int * command, V_Bool loopAndBlock);

#endif /* V_VISE_ */
