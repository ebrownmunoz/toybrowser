#ifndef V_VOC_

/******************************************************************************
 * 
 * File:				V_VOC.H
 *
 * Description:	Header file for Vocabulary module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_VOC.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:48:26   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Jan 1992 17:30:12   DCV
 * Changed basic types
 * 
 *    Rev 1.5   20 Sep 1991 09:51:36   DCV
 * Added a word class field to word models
 * 
 *    Rev 1.4   14 Aug 1991 17:17:56   DCV
 * Replaced WM_ALLOC with a VISE parameter.
 * 
 *    Rev 1.3   18 Jul 1991 16:53:46   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.2   07 Nov 1990 17:14:38   DCV
 * Defined WM_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:23:50   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:50:46   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_VOC_ 0

#include "v_vise.h"

/* The Vocabulary Entry Descriptor */
typedef struct vocwrd_s * VOCWRD;

/* The Vocabulary Descriptor (instantiated once in the VISE descriptor) */
typedef struct voc_s {
	VOCWRD	firstword;		/* First word in vocabulary	*/
	VOCWRD	freeword;		/* First unallocated word	*/
} voc_t, * VOC;

extern wm		voc_createword(VISE vise, V_WId wid, V_Uns class, V_Int numkernels);
extern V_Bool	voc_deleteword(VISE vise, V_WId wordid);
extern wm		voc_getword(VISE vise, V_WId wordid);
extern V_Void	voc_init(VISE vise);

#endif /* V_VOC_ */
