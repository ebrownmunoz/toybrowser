#ifndef V_CMD_

/******************************************************************************
 * 
 * File:				V_CMD.H
 *
 * Description:	Header file for VISE Commands
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_CMD.H_V  $
 * 
 *    Rev 1.8   14 Oct 1994 09:22:38   DCV
 * Added GETFACILITIES and SETAUDIO comand names.
 * 
 *    Rev 1.7   12 Mar 1993 16:07:32   DCV
 * Added commands supporting CELP.
 * 
 * 
 *    Rev 1.6   20 Mar 1992 13:30:24   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.5   29 Jan 1992 16:48:28   DCV
 * Changed basic types
 * 
 *    Rev 1.4   08 Oct 1991 09:31:32   DCV
 * Added STARTLISTENING, STOPLISTENING, SETTIME, GETTIME and REPLACEWORD commands.
 * 
 *    Rev 1.3   14 Aug 1991 16:53:28   DCV
 * Eliminated MEMORYLAYOUT command.
 * 
 *    Rev 1.2   01 Aug 1991 18:14:24   DCV
 * Changed all prototypes to use new message system
 * 
 *    Rev 1.1   25 Sep 1990 10:34:32   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 1.0   13 Sep 1990 12:36:34   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_CMD_ 0

#include "v_vise.h"
#include "v_err.h"
#include "visecmds.h"

 /* General error exit macro */
#define VISE_EXIT(S) { vise_status = (S); goto VISE_RETURN; }

/* VISE Search Engine Command Prototypes */

extern V_Err DELGRAMMAR(VISE vise);         /* delgra.c */
extern V_Err DELWORD(VISE vise);            /* delwor.c */
extern V_Err DLOADAMPLITUDE(VISE vise);     /* dloada.c */
extern V_Err DLOADGRAMMAR(VISE vise);       /* dloadg.c */
extern V_Err DLOADJIN(VISE vise);           /* dloadj.c */
extern V_Err DLOADTEMPLATE(VISE vise);      /* dloadt.c */
extern V_Err DLOADWILDCARD(VISE vise);      /* dloadwc.c */
extern V_Err DLOADWORD(VISE vise);          /* dloadw.c */
extern V_Err ECHO(VISE vise);               /* echo.c */
extern V_Err ENROLLWORD(VISE vise);         /* enroll.c */
extern V_Err LISTEN(VISE vise);             /* listen.c */
extern V_Err STARTLISTENING(VISE vise);     /* listenin.c */
extern V_Err STOPLISTENING(VISE vise);
extern V_Err SETPARAMETERS(VISE vise);      /* param.c */
extern V_Err GETPARAMETERS(VISE vise);
extern V_Err RECOGNIZE(VISE vise);          /* recog.c */
extern V_Err REPLACEWORD(VISE vise);        /* replacew.c */
extern V_Err SEGMENTWORD(VISE vise);        /* segword.c */
extern V_Err SETTIME(VISE vise);            /* time.c */
extern V_Err GETTIME(VISE vise);
extern V_Err SETTIMEOUT(VISE vise);         /* timeout.c */
extern V_Err SOURCEINIT(VISE vise);         /* srcinit.c */
extern V_Err TRAINWORD(VISE vise);          /* trainword.c */
extern V_Err STARTTRAININGWORD(VISE vise);
extern V_Err FINISHTRAININGWORD(VISE vise);
extern V_Err ULOADAMPLITUDE(VISE vise);     /* uloada.c */
extern V_Err ULOADJIN(VISE vise);           /* uloadj.c */
extern V_Err ULOADTEMPLATE(VISE vise);      /* uloadt.c */
extern V_Err ULOADWORD(VISE vise);          /* uloadw.c */
extern V_Err WARMSTART(VISE vise);          /* warmstart.c */

#endif /* V_CMD_ */
