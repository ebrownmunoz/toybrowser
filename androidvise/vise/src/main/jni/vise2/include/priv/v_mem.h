#ifndef V_MEM_

/******************************************************************************
 * 
 * File:    V_MEM.H
 *
 * Description: Header file for Memory Configuration module
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_MEM.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:40:04   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Aug 1991 14:47:00   DCV
 * Eliminated MEM_GM_errfxn and moved local definitions to mem.c
 * 
 *    Rev 1.5   14 Aug 1991 17:11:16   DCV
 * Added MEM_alloclist()
 * 
 *    Rev 1.4   18 Jul 1991 16:49:00   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.3   12 Jun 1991 15:11:14   DCV
 * Eliminated SCOREKEEPING ifdef
 * 
 *    Rev 1.2   13 Dec 1990 18:35:02   DCV
 * Corrected definition of DRAM_7000_BASE and added PROM_BASE
 * 
 *    Rev 1.1   07 Nov 1990 17:06:02   DCV
 * Completely revised for new mem.c
 * 
 *    Rev 1.0   13 Sep 1990 12:42:58   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_MEM_ 0

#include "vbxtypes.h"
#include "ifaces.h"

#define MEMTYPESTR(T)  ((T == SLOW_MEM) ? "SLOW_MEM" : ((T == FAST_MEM) ? "FAST_MEM" : ((T == STATIC_MEM) ? "STATIC_MEM" : "UNKNOWN_MEM_TYPE")))

/* A memory allocator */
typedef struct memcfg_s * MEMCFG;

/* Allocation Classes -- classes may be added simply by adding them before NUM_MEM_TYPES here */
typedef enum mem_e { SLOW_MEM, FAST_MEM, STATIC_MEM, NUM_MEM_TYPES, ALL_MEM = NUM_MEM_TYPES } mem_t;

extern MEMCFG   MEM_create(IMemory * imem);
extern V_Void   MEM_destroy(MEMCFG cfg);
extern V_Void   MEM_free(MEMCFG memCfg, mem_t memType);
extern V_ULong  MEM_size(MEMCFG cfg, mem_t memType);
extern V_Ptr    MEM_alloc(MEMCFG cfg, V_ULong blkSize, V_ULong blkCount, mem_t memType, V_Char * caller);
extern V_Ptr    MEM_alloclist(MEMCFG cfg, V_ULong objSize, V_ULong objCount, mem_t memType, V_Char * caller);
#ifdef VBXDEBUG
extern V_Void   MEM_printCfg(MEMCFG cfg, mem_t memType, V_Char * label);
#endif

#endif /* V_MEM_ */
