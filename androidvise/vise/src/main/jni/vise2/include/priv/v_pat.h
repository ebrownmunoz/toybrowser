#ifndef V_PAT_

/******************************************************************************
 * 
 * File:				V_PATH.H
 *
 * Description:	Header file for Path Maintenance module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_PAT.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:43:08   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Jan 1992 17:19:04   DCV
 * Changed basic types
 * 
 *    Rev 1.5   14 Aug 1991 17:13:42   DCV
 * Replaced PATH_ALLOC with VISE parameter PATHSTACKSIZE
 * 
 *    Rev 1.4   01 Aug 1991 17:21:14   DCV
 * Added prototype for new path_flush() function
 * Added path_size() macro
 * 
 *    Rev 1.3   18 Jul 1991 16:50:04   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.2   12 Jun 1991 15:14:38   DCV
 * Added globalmin reporting
 * 
 *    Rev 1.1   25 Sep 1990 10:36:40   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 1.0   13 Sep 1990 12:46:32   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_PAT_ 0

#include "v_vise.h"
#include "v_par.h"

/* A Path Element Descriptor */
typedef struct wrd_s * WRD;

/* A Path Descriptor (instantiated once in the VISE descriptor) */
typedef struct path_s {
  WRD  nextWord;		/* Pointer to the current stack position */
  WRD  firstWord;	 	/* Pointer to the beginning of the stack */
  WRD  lastWord;		/* Pointer to the end of the stack */
} path_t, * PATH;

extern V_Bool   path_init(VISE vise);
extern V_Void	path_flush(VISE vise);
extern V_Bool	path_pop(VISE vise, V_WId * wordid, V_Uns * numframes, V_SN * startsn, V_SN * endsn, V_Int * score, V_Int * gblmin);
extern V_Bool	path_push(VISE vise, V_WId wordid, V_Uns numframes, V_SN startsn, V_SN endsn, V_Int score, V_Int gblmin);

#define path_size(vise)	((V_Uns) param_get(vise, PATHSTACKSIZE))

#endif /* V_PAT_ */
