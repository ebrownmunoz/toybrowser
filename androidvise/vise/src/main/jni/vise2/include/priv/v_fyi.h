#ifndef V_FYI_

/******************************************************************************
 * 
 * File:         V_FYI.H
 *
 * Description:  Header file for Information Reporting module
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_FYI.H_V  $
 * 
 *    Rev 1.3   20 Mar 1992 13:36:46   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.2   29 Jan 1992 16:54:40   DCV
 * Changed basic types
 * 
 *    Rev 1.1   22 Aug 1991 11:21:14   DCV
 * Added dynamic memory allocation.
 * Modified for new message system.
 * 
 *    Rev 1.0   22 Jan 1991 11:12:04   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_FYI_
#define _FYI_ 1

#include "v_fyidef.h"
#include "visemsg.h"

/*
 * The FYIENT Descriptor is not for export, but unfortunately must be
 * defined here in the header for the macros to work.
 */
typedef struct fyient_s * FYIENT;
typedef struct fyient_s {
  FYIENT  nextParm;
  fyiid_t id;
  V_Int   value;
  V_Fcn   routine;
} fyient_t;

/* The FYI Descriptor (the sole instance is found in the VISE descriptor) */
typedef struct fyi_s {
  FYIENT  freeParms;  /* Ptr to head of free parameter list */
  FYIENT  firstParm;  /* Ptr to head of used parameter list */
} fyi_t;

#include "v_vise.h"

extern V_Void  FYI_init(VISE vise);
extern FYIENT  FYI_allocate(VISE vise, fyiid_t parmId);
extern V_Uns   FYI_msgSize(VISE vise);
extern V_Bool  FYI_writeMsg(VISE vise);

#ifdef _FYI_
#define FYI_INIT(vise)             FYI_init(vise)
#define FYI_HANDLE(vise,ID)        FYI_allocate(vise, ID)
#define FYI_MSGSIZE(vise)          FYI_msgSize(vise)
#define FYI_WRITEMSG(vise)         FYI_writeMsg(vise)

#define FYI_ASSIGNP(PTR,OP,VAL)    if (PTR) (PTR)->value OP (VAL)
#define FYI_SETMAXP(PTR,VAL)       if (PTR) ((PTR)->value < ((V_Int)VAL)) ? ((PTR)->value = (VAL)) : (PTR)->value
#define FYI_SETMINP(PTR,VAL)       if (PTR) ((PTR)->value > (VAL)) ? ((PTR)->value = (VAL)) : (PTR)->value

#define FYI_DEFINE(T,VAR)          T VAR
#define FYI_DEFHANDLE(PTR)         FYIENT PTR
#define FYI_GETHANDLE(vise,PTR,ID) PTR = FYI_allocate(vise, ID)
#define FYI_ASSIGN(VAR,OP,VAL)     (VAR) OP (VAL)
#define FYI_SETMAX(VAR,VAL)        if ((VAR) < (VAL)) (VAR) = (VAL)
#define FYI_SETMIN(VAR,VAL)        if ((VAR) > (VAL)) (VAR) = (VAL)
#else /* not _FYI_ */
#define FYI_INIT(vise)             (FYI_init(vise), FYI_allocate(vise, FYI_NOTINSTALLED))
#define FYI_HANDLE(vise,ID)
#define FYI_MSGSIZE(vise)          FYI_msgSize(vise)
#define FYI_WRITEMSG(vise)         FYI_writeMsg(vise)

#define FYI_ASSIGNP(PTR,OP,VAL)
#define FYI_SETMAXP(PTR,VAL)
#define FYI_SETMINP(PTR,VAL)

#define FYI_DEFINE(T,VAR)
#define FYI_DEFHANDLE(PTR)
#define FYI_GETHANDLE(vise,PTR,ID)
#define FYI_ASSIGN(VAR,OP,VAL)
#define FYI_SETMAX(VAR,VAL)
#define FYI_SETMIN(VAR,VAL)
#endif /*_FYI_*/

#endif /* V_FYI_ */

