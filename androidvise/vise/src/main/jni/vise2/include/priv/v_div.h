#ifndef V_DIV_

/******************************************************************************
 * 
 * File:				V_DIV.H
 *
 * Description:	Header file for division functions
 *
 *****************************************************************************/

#define V_DIV_ 0

extern V_Int	rndiv32(V_Long n, V_Int d);
extern V_Int	ldiv32(V_Long n, V_Long d);

#endif /* V_DIV_ */
