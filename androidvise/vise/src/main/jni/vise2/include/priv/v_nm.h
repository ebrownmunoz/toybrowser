#ifndef V_NM_

/******************************************************************************
 * 
 * File:				V_NM.H
 *
 * Description:	Header file for Node Model module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_NM.H_V  $
 * 
 *    Rev 1.3   20 Mar 1992 13:42:12   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.2   29 Jan 1992 17:15:16   DCV
 * Changed basic types
 * Replaced nm_read() with nm_nid() and nm_write() with nm_setnid()
 * 
 *    Rev 1.1   19 Sep 1990  9:21:50   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:45:22   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_NM_ 0

/*
 * The Node Model Representation is exported via nm_t type declarations in
 * the Arc Model representation.  In any case, it must be defined here
 * in the header for the macros and memory allocation to work.
 */
typedef struct nm_s	{
	V_NId		nmid;					/* Node id */
} nm_t;


extern V_NId	nm_nid(nm nodemodel);
extern V_Void	nm_setnid(nm nodemodel, V_NId id);

#ifndef	NOMACROS
#define nm_nid(N)		((N)->nmid)
#define nm_setnid(N,I)	((N)->nmid = (I))
#endif

#endif /* V_NM_ */
