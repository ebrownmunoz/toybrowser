#ifndef V_AM_

/******************************************************************************
 * 
 * File:    V_AM.H
 *
 * Description: Header file for Arc Model module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_AM.H_V  $
 * 
 *    Rev 1.6   20 Mar 1992 13:28:24   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.5   29 Jan 1992 16:44:56   DCV
 * Changed basic types
 * Changed am_addword() to return wdentp instead of bool_t
 * 
 *    Rev 1.4   14 Aug 1991 16:50:42   DCV
 * Replaced WDENT_ALLOC with a VISE parameter.
 * 
 *    Rev 1.3   18 Jul 1991 16:38:56   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.2   07 Nov 1990 16:48:32   DCV
 * Defined WDENT_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:17:30   DCV
 * Extracted handle into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:33:22   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_AM_ 0

#include "v_vise.h"
#include "v_nm.h"    /* nm_t is referred to in am_t below */

typedef struct wdent_s * WDENT;     /* Word Entry List Element */

/*
 * The Arc Model Representation is not for export, but unfortunately
 * must be defined here in the header for the macros and memory allocation
 * to work.
 */
typedef struct am_s {
  nm_t  amsource;       /* Source node */
  nm_t  amdestination;  /* Destination node */
  WDENT amfirstword;    /* First word on word list */
  WDENT amcurrentword;  /* Word iterator */
  WDENT amlastword;     /* Last word on list */
} am_t;

typedef struct wapool_s {
  WDENT freewdent;
} wapool_t, * WAPOOL;


extern WDENT  am_addword(VISE, am, wm);
extern V_Void am_arcinit(am);
extern V_Void am_deallocate(VISE, am);
extern nm     am_destinationnode(am);
extern wm     am_firstword(am);
extern V_Void am_init(VISE);
extern wm     am_nextword(am);
extern nm     am_sourcenode(am);

#ifndef NOMACROS
#define am_sourcenode(A)      &((A)->amsource)
#define am_destinationnode(A) &((A)->amdestination)
#define am_arcinit(A)         (A)->amfirstword = (A)->amcurrentword = (A)->amlastword = NULL
#endif

#endif /* V_AM_ */
