#ifndef V_WI_

/******************************************************************************
 * 
 * File:    V_WI.H
 *
 * Description: Header file for Word Instance module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_WI.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:48:56   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Jan 1992 17:31:38   DCV
 * Changed basic types
 * 
 *    Rev 1.5   08 Oct 1991 10:12:32   DCV
 * Added wi_kernelcount() function.
 * 
 *    Rev 1.4   20 Sep 1991 10:11:08   DCV
 * Eliminated wi_id() and added wi_model()
 * 
 *    Rev 1.3   18 Jul 1991 16:54:36   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.2   07 Nov 1990 17:15:24   DCV
 * Defined WI_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:24:04   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:51:58   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_WI_ 0

#include "v_vise.h"
#include "v_wm.h" /* wm_id() & wm_status() used in macro definitions */ 

/*
 * The Word Instance Descriptor is not for export, but unfortunately must be
 * defined here in the header for the macros and memory allocation to work.
 * A wi_t is declared in trainword(), but the structure is not exposed there.
 */
typedef struct wi_s {
  ki       wiactiveregion; /* The last active kernel in word */
  ki       wifirstkernel;  /* The first kernel in the word */
  ki       wilastkernel;   /* The last kernel in the word */
  wm       wimodel;        /* The word model we're made from */
  V_Int    wiactivecount;  /* The number of active kernels */
} wi_t;

/* Used in ai.c */
#define WI_SIZE  sizeof(wi_t)

/* The Kernel Instance Pool (instantiated once in the VISE descriptor */
typedef struct kipool_s {
  ki       freekernel;     /* The first free kernels marker */
  V_ULong  freecount;      /* The number of free kernels */
  V_ULong  kernelcount;    /* The number of kernels allocated */
} kipool_t, * KIPOOL;


extern V_Bool  wi_active(wi wordinst);
extern V_Void  wi_deactivate(wi wordinst);
extern wm      wi_model(wi wordinst);
extern V_Bool  wi_inactive(wi wordinst);
extern V_Bool  wi_init(VISE vise, V_ULong kicount);
extern V_ULong wi_kernelcount(VISE vise);
extern ki      wi_lastkernel(wi wordinst);
extern V_Void  wi_napkernel(wi wordinst);
extern V_Int   wi_numactive(wi wordinst);
extern ki      wi_previouskernel(wi wordinst);
extern V_Int   wi_replicate(VISE vise, wi wordinst, wm wordmodel);
extern V_Int   wi_status(wi word);
extern ki      wi_wakekernel(wi wordinst);

#ifndef NOMACROS
#define wi_inactive(W)    ((W)->wiactivecount == 0)
#define wi_napkernel(W)   ((W)->wiactiveregion--, (W)->wiactivecount--)
#define wi_deactivate(W)  (W)->wiactivecount = 0
#define wi_model(W)       ((W)->wimodel)
#define wi_lastkernel(W)  (W)->wiactiveregion
#define wi_numactive(W)   (W)->wiactivecount
#define wi_active(W)      ((W)->wilastkernel == (W)->wiactiveregion)
#define wi_status(W)      wm_status((W)->wimodel)
#endif

#endif /* V_WI_ */
