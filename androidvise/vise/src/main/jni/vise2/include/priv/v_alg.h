#ifndef V_ALG_

/******************************************************************************
 * 
 * File:         V_ALG.H
 *
 * Description:  Header file for VISE Algorithmic functions
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_ALG.H_V  $
 * 
 *    Rev 1.13   20 Mar 1992 13:28:06   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.12   29 Jan 1992 16:43:42   DCV
 * Changed basic types
 * 
 *    Rev 1.11   22 Nov 1991 12:01:58   DCV
 * Eliminated accumparams(), weightmeans() and maketemplate().
 * Modified prototype for adjustTemplate().
 * 
 *    Rev 1.10   12 Nov 1991 17:54:48   DCV
 * Eliminated special return codes for findword() and gramnodes().
 * Added insertpaths().
 * Changed prototypes for nullarcs(), process() and recloop() to
 *   take numpaths as their final argument and return int_t status.
 * Changed prototypes for processword() and processkernel() to
 *   take tbp instead of tbn argument.
 * Changed the training weight normalization from 32767 to 65535.
 * Moved MAXPATHS parameter here.
 * 
 *    Rev 1.9   01 Aug 1991 17:19:18   DCV
 * Added prototype for new tracelength() function
 * 
 *    Rev 1.8   18 Jul 1991 16:37:48   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.7   12 Jun 1991 14:54:46   DCV
 * Modified for new traceback tree organization
 * 
 *    Rev 1.6   28 Feb 1991 10:21:46   DCV
 * Changed to floating-point weighting scheme
 * 
 *    Rev 1.5   10 Jan 1991 14:35:38   DCV
 * Moved declarations of maketemplate() and weightmeans() from v_dsp.h to here.
 * Added declaration of adjustTemplate().
 * 
 *    Rev 1.4   08 Nov 1990 17:47:40   DCV
 * Moved declaration of accumparams() from v_dsp.h to here
 * 
 *    Rev 1.3   07 Nov 1990 16:47:46   DCV
 * Defined SHARE_SIZE
 * 
 *    Rev 1.2   25 Sep 1990 10:33:20   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 1.1   19 Sep 1990  9:17:02   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:32:44   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_ALG_ 0

#include "v_vise.h"
#include "v_err.h"

#define MAXPATHS         32         /* Maximum number of alternate paths (maximum "n-best") */
#define NULLWORDID       0          /* Unused word id */
#define MAXLISTENFRAMES  16777216L  /* Maximum number of frames in a LISTEN or RECOGNITION */

 /* adjustem.c */
extern V_Err   adjustTemplate(VISE, km, kt, V_SN *, V_Uns, V_Uns);
 /* findwo.c */
extern V_Err   findword(VISE, V_SN, V_SN, V_SN *, V_SN *, V_Int *);
 /* insrtpth.c */
extern V_Bool  insertpaths(VISE vise, ni, V_Int, tbp, wm, V_Int *, V_Uns);
 /* grmnodes.c */
extern V_Err   gramnodes(VISE, V_Int, V_Long, V_Long, V_Time, V_SN, tbn *);
 /* nullarcs.c */
extern V_Err   nullarcs(VISE, gi, V_Uns);
 /* process.c */
extern V_Err   process(VISE, jin, V_Time, V_Int, V_Int *, V_Uns);
 /* processk.asm */
extern V_Void  processkernel(VISE, ki, V_Int, V_Int, tbp, V_Int *);
 /* processw.c */
extern V_Void  processword(VISE, wi, V_Int, tbp, V_Int *, V_Int *);
 /* recloop.c */
extern V_Err   recloop(VISE, V_SN, V_SN, tbn *, V_Uns, V_Bool);
 /* trace.c */
extern V_Bool  trace(VISE, tbp);
extern V_Int   tracelength(tbp);

#endif /* V_ALG_ */

