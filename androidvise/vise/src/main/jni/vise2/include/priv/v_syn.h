#ifndef V_SYN_

/******************************************************************************
 * 
 * File:				V_SYN.H
 *
 * Description:	Header file for Syntax module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_SYN.H_V  $
 * 
 *    Rev 1.6   20 Mar 1992 13:45:04   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.5   29 Jan 1992 17:23:26   DCV
 * Changed basic types
 * 
 *    Rev 1.4   12 Nov 1991 17:52:10   DCV
 * Eliminated the numpaths argument from syn_activategrammar(),
 *   which now also returns a standard VISE status code.
 * 
 *    Rev 1.3   14 Aug 1991 17:14:40   DCV
 * Replaced GM_ALLOC with VISE parameter
 * 
 *    Rev 1.2   18 Jul 1991 16:50:32   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.1   19 Sep 1990  9:22:10   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:48:08   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_SYN_ 0

#include "v_vise.h"
#include "v_gi.h"		/* gi_t is referred to in struct syn_t below */

/*
 * The grammars in the syntax are linked together in two singly linked
 * lists.  One list contains all the currently allocated grammars and one
 * contains all of the currently unallocated grammars.  This facilitates
 * storage management, as the available grammar model structures are
 * always kept easily accessible.
 */

/* A Syntax Entry Descriptor */
typedef struct synent_s * SYNENT;

/* A Syntax Descriptor (the sole instance is in the VISE descriptor) */
typedef struct syn_s {
	gi_t 	activegrammar;			/* The sole active grammar 	  */
	SYNENT	firstgrammar;			/* First grammar in syntax	  */
	SYNENT	currentgrammar;		    /* Next grammar for iteration */
	SYNENT	freegrammars;			/* First unallocated grammar  */
} syn_t;

extern V_Int	syn_activategrammar(VISE vise, V_GId id);
extern gm		syn_creategrammar(VISE vise, V_GId id);
extern V_Bool	syn_deletegrammar(VISE vise, V_GId id);
extern gm		syn_firstgrammar(VISE vise);
extern gi		syn_getactive(VISE vise);
extern gm		syn_getgrammar(VISE vise, V_GId id);
extern V_Void	syn_init(VISE vise);
extern gm		syn_nextgrammar(VISE vise);

#ifndef	NOMACROS
#define syn_getactive(vise)	 &(vise)->syntax.activegrammar
#endif

#endif /* V_SYN_ */
