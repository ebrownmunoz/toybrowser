/******************************************************************************
 * 
 * File:				VERSION.H
 *
 * Description:	The VISE Version Number
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\VERSION.H_V  $
 * 
 *    Rev 3.1   18 Oct 1994 14:21:44   DCV
 * Version supporting CELP/Raw data play back.
 * 
 * 
 *    Rev 3.0   18 Oct 1994 14:13:56   DCV
 * Added VISE_VERSION_ defenition to prevent multiple inclusions.
 * Added FRONTEND definition for version control purposes.
 * Fixed bug in tb_addframe making tb_prune fail,
 * and Vise asleep.
 * 
 *    Rev 2.0.6.0   30 Jun 1992 08:57:56   DCV
 * Add sig_reset() function to reset
 * delta cepstrum buffer after sam_start()
 * invocation.
 * 
 *    Rev 2.0.5.0   18 Jun 1992 13:32:52   DCV
 * First version VISE with MelFourierCepstrum
 * coefficients representing new front end.
 * Modules sigcal.c and v_dsp.h were modified.
 * 
 *    Rev 2.0.4.0   17 Jun 1992 16:27:08   DCV
 * Analog Devices ADC is now supported.
 * 
 * 
 *    Rev 2.0.3.0   06 May 1992 18:00:38   DCV
 * Added multiple results, dynamic memory allocation, uninterrupted
 * listening, etc.
 * 
 *    Rev 2.0.1.3   22 Apr 1991 09:41:06   DCV
 * Changed sam.c to avoid singularity in quiet.
 * 
 *    Rev 2.0.1.2   28 Feb 1991 15:54:40   DCV
 * Changed mem.c to avoid bus capacitance problem in 6000 without DRAM
 * 
 *    Rev 2.0.1.1   17 Dec 1990 14:08:30   DCV
 * Fixed 7000 Memory Configuration and added FYI cluster
 * 
 *    Rev 2.0.1.0   13 Nov 1990 11:57:44   DCV
 * Floating point version (F2.2) with individual header files.
 * Corresponds to integer version I1.1.
 * 
 *    Rev 2.0   01 Jun 1990 12:08:46   DCV
 * Original floating-point version
 * 
 *    Rev 1.0   01 Jun 1990 12:05:28   DCV
 * Initial revision.
 *
 *****************************************************************************/

/*
 * VISEVERSION = ( major<<8 ) + minor = ( major*256 ) + minor
 *
 * where major and minor are the major and minor parts of the revision
 * number of this file.
 *
 * VISEVERSION was previously computed automatically from the revision
 * number by vvgen.exe, which needs to be modified to deal with changes
 * in this file.
 */

#ifndef VISE_VERSION_

/* Who cares ? */
#define VISEVERSION 1024

/* 8KHz uncompressed with a frame shift of 102 samples */
#define FRONTEND 813

#endif /* VISE_VERSION_ */

