#ifndef V_TBP_

/******************************************************************************
 * 
 * File:    V_TBP.H
 *
 * Description: Header file for Traceback Path Element module
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_TBP.H_V  $
 * 
 *    Rev 1.7   20 Mar 1992 13:46:02   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.6   29 Jan 1992 17:27:18   DCV
 * Changed basic types
 * 
 *    Rev 1.5   13 Nov 1991 10:43:28   DCV
 * Added some macros
 * 
 *    Rev 1.4   20 Sep 1991 10:08:26   DCV
 * Added DGN and pathlength fields to tbps, and changed pathcost into a
 *  renormalized cost (sumbestscores is now available in the tbf).
 * 
 *    Rev 1.3   30 Aug 1991 17:09:54   DCV
 * tbp_copy() macro no longer copies tbpnode and tbpchildcount.
 *   It now sets tbpchildcount to zero.
 * 
 *    Rev 1.2   14 Aug 1991 17:17:00   DCV
 * Replaced TBP_ALLOC with a VISE parameter.
 * 
 *    Rev 1.1   18 Jul 1991 16:53:18   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.0   12 Jun 1991 15:25:36   DCV
 * Initial Version
 * 
 *****************************************************************************/

#define V_TBP_ 0

/*
 * The traceback path element object is not for export, but unfortunately
 * must be defined here in the header for the macros to work.
 */
typedef struct tbp_s {
  tbp     tbpsuccessor;  /* The next-best traceback path element */
  tbn     tbpnode;       /* The traceback node containing this element */
  tbp     tbpparent;     /* The previous element on this traceback path */
  V_ULong tbpDGN;        /* The degenerate Goedel number of this path */
  V_Int   tbpcost;       /* The renormalized path cost */
  V_WId   tbpwid;        /* The id of the previous word on this path */
  V_Int   tbpchildcount; /* The number of paths rooted in this element */
  V_Uns   tbplength;     /* The number of significant words on this path */
} tbp_t;

/* The Traceback Path Pool descriptor (instantiated once in the VISE descriptor) */
/* The member variables are EXPOSED in tb_prune()! */
typedef struct tbppool_s {
  tbp     freeentries;   /* The next free traceback path element */
  V_ULong used;          /* The number of tbp's in use */
} tbppool_t, * TBPPOOL;

#include "v_vise.h"

extern tbp      tbp_alloc(VISE vise);
extern V_Int    tbp_childcount(tbp path);
extern V_Void   tbp_copy(tbp srctbp, tbp dsttbp);
extern V_Int    tbp_cost(tbp path);
extern V_Int    tbp_decchildcount(tbp path);
extern V_ULong  tbp_DGN(tbp path);
extern V_Void   tbp_free(VISE vise, tbp path);
extern V_Long   tbp_globalmin(tbp path);
extern V_Int    tbp_incchildcount(tbp path);
extern V_Void   tbp_info(tbp path, tbn * node, V_Int * cost, V_WId * wid, tbp * parent, V_ULong * dgn, V_Uns * length);
extern V_Void   tbp_init(VISE vise);
extern tbn      tbp_node(tbp path);
extern V_Void   tbp_orphan(tbp path);
extern tbp      tbp_parent(tbp path);
extern V_Void   tbp_setinfo(tbp path, tbn node, V_Int cost, V_WId wid, tbp parent, V_ULong dgn, V_Uns length);
extern V_Void   tbp_setnode(tbp path, tbn node);
extern V_Void   tbp_setsuccessor(tbp predecessor,tbp successor);
extern tbp      tbp_successor(tbp predecessor);
extern V_Time   tbp_time(tbp path);
extern V_SN     tbp_sn(tbp path);
extern V_Long   tbp_sumbestscores(tbp path);
extern V_Int    tbp_used(VISE vise);
extern V_WId    tbp_wid(tbp path);

#ifndef NOMACROS
#define tbp_childcount(E)           ((E)->tbpchildcount)
#define tbp_copy(S,D)               ((D)->tbpwid=(S)->tbpwid),\
                                    ((D)->tbpchildcount=0),\
                                    ((D)->tbpcost=(S)->tbpcost),\
                                    ((D)->tbpparent=(S)->tbpparent),\
                                    ((D)->tbpDGN=(S)->tbpDGN),\
                                    ((D)->tbplength=(S)->tbplength)
#define tbp_cost(E)                 ((E)->tbpcost)
#define tbp_decchildcount(E)        (--(E)->tbpchildcount)
#define tbp_DGN(E)                  ((E)->tbpDGN)
#define tbp_free(vise,E)            ((E)->tbpsuccessor = vise->tbpPool.freeentries),\
                                    (vise->tbpPool.freeentries = (E)),\
                                    (vise->tbpPool.used--)
#define tbp_globalmin(E)            ((E)->tbpnode->tbnframe->tbfglobalmin)
#define tbp_incchildcount(E)        (++(E)->tbpchildcount)
#define tbp_info(E,N,C,W,P,D,L)     (*(N)=(E)->tbpnode),\
                                    (*(W)=(E)->tbpwid),\
                                    (*(C)=(E)->tbpcost),\
                                    (*(P)=(E)->tbpparent),\
                                    (*(D)=(E)->tbpDGN),\
                                    (*(L)=(E)->tbplength)
#define tbp_length(E)               ((E)->tbplength)
#define tbp_node(E)                 ((E)->tbpnode)
#define tbp_orphan(E)               ((E)->tbpparent = NULL)
#define tbp_parent(E)               ((E)->tbpparent)
#define tbp_setcost(E,C)            ((E)->tbpcost = (C))
#define tbp_setinfo(E,N,C,W,P,D,L)  ((E)->tbpnode=(N)),\
                                    ((E)->tbpwid =(W)),\
                                    ((E)->tbpchildcount=0),\
                                    ((E)->tbpcost=(C)),\
                                    ((E)->tbpparent=(P)),\
                                    ((E)->tbpDGN=(D)),\
                                    ((E)->tbplength=(L))
#define tbp_setnode(E,N)            ((E)->tbpnode=(N))
#define tbp_setparent(E,P)          ((E)->tbpparent=(P))
#define tbp_setsuccessor(E,S)       ((E)->tbpsuccessor = (S))
#define tbp_setwid(E,W)             ((E)->tbpwid =(W))
#define tbp_successor(E)            ((E)->tbpsuccessor)
#define tbp_time(E)                 ((E)->tbpnode->tbnframe->tbftime)
#define tbp_sn(E)                   ((E)->tbpnode->tbnframe->tbfsn)
#define tbp_sumbestscores(E)        ((E)->tbpnode->tbnframe->tbfsumbestscores)
#define tbp_used(vise)              (vise->tbpPool.used)
#define tbp_wid(E)                  ((E)->tbpwid)
#endif

#endif /* V_TBP_ */
