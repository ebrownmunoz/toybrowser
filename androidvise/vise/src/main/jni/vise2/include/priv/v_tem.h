#ifndef V_TEM_

/******************************************************************************
 * 
 * File:    V_TEM.H
 *
 * Description: Header file for Template Maintenance module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_TEM.H_V  $
 * 
 *    Rev 1.3   20 Mar 1992 13:46:14   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.2   29 Jan 1992 17:28:24   DCV
 * Changed basic types
 * 
 *    Rev 1.1   19 Sep 1990  9:23:10   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:49:42   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_TEM_ 0

#include "v_jin.h"    /* jinpackr is referred to in tem_t */

/*
 * The Template Descriptor is exported via a tem_t type declaration
 * in the Kernel Model Descriptor, but in any case it must be defined
 * here in the header for the macros to work.
 */
typedef struct tem_s {
  V_Time    temtime;     /* Time of most recent template match */
  V_Int     temcost;     /* Cost of most recent template match */
  union {                /* Template */
    jinpackr  jin;
    struct {
      V_Uns     relative;
      V_Uns     absolute;
    }         wildcard;
  }         temparam;
} tem_t;

extern V_Void tem_copy(tem source,tem dest);
extern V_Void tem_gcost(tem template, V_Int *cost,V_Time *time);
extern V_Void tem_pcost(tem template, V_Int cost,V_Time time);
extern V_Void tem_read(tem template,jin array);
extern V_Void tem_write(tem template,jin array);
extern V_Void tem_getwildcard(tem, V_Uns *, V_Uns *);
extern V_Void tem_setwildcard(tem, V_Uns, V_Uns);

#ifndef NOMACROS
#define tem_read(T,A)     jin_from_jinpack(A, &(T)->temparam.jin)
#define tem_write(T,A)    jinpack_from_jin(&(T)->temparam.jin, A)
#define tem_copy(S,D)     memcpy(D, S, sizeof(*D))
#define tem_pcost(T,C,F)  ((T)->temtime = (F), (T)->temcost = (C))
#endif

#endif /* V_TEM_ */
