#ifndef V_GM_

/******************************************************************************
 * 
 * File:    V_GM.H
 *
 * Description: Header file for Grammar Model module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_GM.H_V  $
 * 
 *    Rev 1.6   20 Mar 1992 13:38:02   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.5   29 Jan 1992 16:58:08   DCV
 * Changed basic types
 * 
 *    Rev 1.4   14 Aug 1991 17:08:24   DCV
 * Replaced AM_ALLOC with a VISE parameter.
 * 
 *    Rev 1.3   18 Jul 1991 16:46:24   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.2   07 Nov 1990 16:59:36   DCV
 * Defined AM_SIZE and GM_SIZE
 * 
 *    Rev 1.1   19 Sep 1990  9:19:54   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:40:40   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_GM_ 0

#include "v_vise.h"
#include "v_am.h"   /* am_t is referred to in arcent below */

/* An Arc Entry Descriptor */
typedef struct arcent_s * ARCENT;

/* The Arc Model Pool Descriptor (instantiated once in the VISE descriptor) */
typedef struct ampool_s {
  ARCENT   freearc;
} ampool_t, * AMPOOL;

/*
 * The Grammar Model Descriptor is instantiated in the 
 * in the Syntax Entry Descriptor, but in any case must be defined
 * here for the macros and memory allocation to work.
 */
typedef struct gm_s {
  V_GId    gmid;          /* Identification number */
  V_ULong  gmnicount;     /* Number of nodes in grammar */
  V_ULong  gmaicount;     /* Number of arcs in grammar */
  V_ULong  gmwicount;     /* Number of word instances  */
  V_ULong  gmkicount;     /* Number of kernel instances */
  V_ULong  gmkndcount;    /* Number of kernel traceback nodes */
  ARCENT   gmcurrentarc;  /* Current arc in grammar */
  ARCENT   gmfirstarc;    /* First arc in grammar */
  ARCENT   gmlastarc;     /* Last arc in grammar */
} gm_t;

extern am      gm_addarc(VISE vise, gm grammar, V_NId sourceid, V_NId destinationid);
extern V_Void  gm_removearc(VISE vise, gm grammar, am arc);
extern V_Void  gm_deallocate(VISE vise, gm grammar);
extern am      gm_firstarc(gm grammar);
extern V_GId   gm_id(gm grammar);
extern V_ULong gm_nicount(gm grammar);
extern V_Void  gm_init(VISE vise);
extern am      gm_nextarc(gm grammar);
extern V_Void  gm_putinfo(gm grammar, V_NId id);

#define gm_setid(G,I)       (G)->gmid = (I)
#define gm_setnicount(G,N)  (G)->gmnicount = (N)
#define gm_aicount(G)       (G)->gmaicount
#define gm_setaicount(G,A)  (G)->gmaicount = (A)
#define gm_wicount(G)       (G)->gmwicount
#define gm_setwicount(G,W)  (G)->gmwicount = (W)
#define gm_kicount(G)       (G)->gmkicount
#define gm_setkicount(G,K)  (G)->gmkicount = (K)
#define gm_kndcount(G)      (G)->gmkndcount
#define gm_setkndcount(G,N) (G)->gmkndcount = (N)

#ifndef NOMACROS
#define gm_putinfo(G,I)     ((G)->gmid=I),((G)->gmfirstarc=(G)->gmcurrentarc=(G)->gmlastarc=NULL)
#define gm_nicount(G)       (G)->gmnicount
#define gm_id(G)            (G)->gmid
#endif

#endif /* V_GM_ */
