#ifndef V_TBN_

/******************************************************************************
 * 
 * File:    V_TBN.H
 *
 * Description: Header file for Traceback Node module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_TBN.H_V  $
 * 
 *    Rev 1.8   20 Mar 1992 13:45:46   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.7   29 Jan 1992 17:26:26   DCV
 * Changed basic types
 * 
 *    Rev 1.6   14 Aug 1991 17:16:20   DCV
 * Replaced TBN_ALLOC with a VISE parameter.
 * 
 *    Rev 1.5   18 Jul 1991 16:52:48   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.4   12 Jun 1991 15:24:10   DCV
 * Rewritten for new traceback tree structure to support N-best path and
 *   globalmin reporting.
 * 
 *    Rev 1.3   09 Jan 1991 15:15:14   DCV
 * Added tbn_used()
 * 
 *    Rev 1.2   25 Sep 1990 10:37:30   DCV
 * Removed dispatcher's dependence on path and traceback objects.
 * 
 *    Rev 1.1   19 Sep 1990  9:22:52   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:49:10   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_TBN_ 0

/*
 * The traceback node object is not for export, but unfortunately
 * must be defined here in the header for the macros to work.
 */
typedef struct tbn_s {
  tbn     tbnsuccessor; /* The next traceback node in this frame  */
  tbp     tbnbestpath;  /* The best path to this node in this frame  */
  tbf     tbnframe;     /* The traceback frame containing this node  */
  V_Int   tbnpathcount; /* The number of alternative paths to this node */
} tbn_t;

/* The Traceback Node Pool descriptor (instantiated once in the VISE descriptor) */
/* The member variables are EXPOSED in gramnodes() and tb_prune()! */
typedef struct tbnpool_s {
  tbn     freeentries;  /* The next free traceback node */
  V_ULong used;         /* The number of traceback nodes in use */
} tbnpool_t, * TBNPOOL;

#include "v_vise.h"

extern tbn     tbn_alloc(VISE vise);
extern tbp     tbn_bestpath(tbn node);
extern V_Int   tbn_decpathcount(tbn node);
extern tbf     tbn_frame(tbn node);
extern V_Void  tbn_free(VISE vise, tbn node);
extern V_Long  tbn_globalmin(tbn node);
extern V_Int   tbn_incpathcount(tbn node);
extern V_Void  tbn_info(tbn node, tbp *path, tbf *frame, V_Int *pathcount);
extern V_Void  tbn_init(VISE vise);
extern V_Int   tbn_pathcount(tbn node);
extern V_Void  tbn_setbestpath(tbn node, tbp path);
extern V_Void  tbn_setframe(tbn node, tbf frame);
extern V_Void  tbn_setinfo(tbn node, tbp path, tbf frame, V_Int pathcount);
extern V_Void  tbn_setpathcount(tbn node, V_Int npaths);
extern V_Void  tbn_setsuccessor(tbn node, tbn successor);
extern tbn     tbn_successor(tbn node);
extern V_Time  tbn_time(tbn node);
extern V_SN    tbn_sn(tbn node);
extern V_Int   tbn_used(VISE vise);

#ifndef NOMACROS
#define tbn_bestpath(N)       ((N)->tbnbestpath)
#define tbn_decpathcount(N)   (--(N)->tbnpathcount)
#define tbn_frame(N)          ((N)->tbnframe)
#define tbn_free(vise,N)      ((N)->tbnsuccessor = vise->tbnPool.freeentries), (vise->tbnPool.freeentries = (N)), (vise->tbnPool.used--)
#define tbn_globalmin(N)      ((N)->tbnframe->tbfglobalmin)
#define tbn_incpathcount(N)   (++(N)->tbnpathcount)
#define tbn_info(N,P,F,C)     (*(P) = (N)->tbnbestpath), (*(F) = (N)->tbnframe), (*(C) = (N)->tbnpathcount)
#define tbn_pathcount(N)      ((N)->tbnpathcount)
#define tbn_setframe(N,F)     ((N)->tbnframe = F)
#define tbn_setbestpath(N,P)  ((N)->tbnbestpath = P)
#define tbn_setinfo(N,P,F,C)  ((N)->tbnbestpath  = P), ((N)->tbnframe   = F), ((N)->tbnpathcount = C)
#define tbn_setpathcount(N,C) ((N)->tbnpathcount = C)
#define tbn_setsuccessor(N,S) ((N)->tbnsuccessor = S)
#define tbn_successor(N)      ((N)->tbnsuccessor)
#define tbn_time(N)           ((N)->tbnframe->tbftime)
#define tbn_sn(N)             ((N)->tbnframe->tbfsn)
#define tbn_used(vise)        (vise->tbnPool.used)
#endif

#endif /* V_TBN_ */
