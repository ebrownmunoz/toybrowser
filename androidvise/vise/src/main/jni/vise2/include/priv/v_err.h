#ifndef V_ERR_

/******************************************************************************
 * 
 * File:        v_err.h
 *
 * Description: VISE Symbolic Error Codes
 *
 *****************************************************************************/

#define V_ERR_ 0

#include "viseerr.h"

#endif /* V_ERR_ */
