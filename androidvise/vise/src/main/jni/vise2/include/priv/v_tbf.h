#ifndef V_TBF_

/******************************************************************************
 * 
 * File:        V_TBF.H
 *
 * Description: Header file for Traceback Frame module
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_TBF.H_V  $
 * 
 *    Rev 1.5   20 Mar 1992 13:45:30   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.4   29 Jan 1992 17:25:30   DCV
 * Changed basic types
 * 
 *    Rev 1.3   20 Sep 1991 10:00:30   DCV
 * Added a sumbestscores field to tbfs
 * 
 *    Rev 1.2   14 Aug 1991 17:15:26   DCV
 * Replaced TBF_ALLOC with VISE parameter.
 * 
 *    Rev 1.1   18 Jul 1991 16:51:28   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.0   12 Jun 1991 15:22:36   DCV
 * Initial Version
 * 
 *****************************************************************************/

#define V_TBF_ 0

/*
 * The traceback frame object is not for export, but unfortunately
 * must be defined here in the header for the macros to work.
 */
typedef struct tbf_s {
  tbf     tbfsuccessor;     /* The next traceback frame */
  tbn     tbffirstnode;     /* The list of tb nodes for this frame */
  tbf     tbfpredecessor;   /* The preceding traceback frame */
  V_Time  tbftime;          /* The index of this frame */
  V_Long  tbfsumbestscores; /* The sum of all best frame costs so far */
  V_Int   tbfglobalmin;     /* The sum of global min to this frame */
  V_Int   tbfnodecount;     /* The number of tb nodes in this frame */
  V_SN    tbfsn;            /* The sequence number of this frame */
} tbf_t;

/* The Traceback Frame Pool descriptor (instantiated once in the VISE descriptor) */
typedef struct tbfpool_s {
  tbf     freeentries;      /* The next free tbf */
  V_ULong used;             /* The number of tbf's used so far */
} tbfpool_t, * TBFPOOL;

#include "v_vise.h"

extern tbf     tbf_alloc(VISE vise);
extern V_Int   tbf_decnodecount(tbf frame);
extern tbn     tbf_firstnode(tbf frame);
extern V_Void  tbf_free(VISE vise, tbf frame);
extern V_Long  tbf_globalmin(tbf frame);
extern V_Int   tbf_incnodecount(tbf frame);
extern V_Void  tbf_info(tbf frame, tbn * node, V_Int * ncount, V_Time * time, V_SN * sn, V_Long * sumbest, V_Long * gmin);
extern V_Void  tbf_init(VISE vise);
extern V_Int   tbf_nodecount(tbf frame);
extern tbf     tbf_predecessor(tbf successor);
extern V_Void  tbf_setglobalmin(tbf frame, V_Long gmin);
extern V_Void  tbf_setinfo(tbf frame, tbn node, V_Int ncount, V_Time time, V_SN sn, V_Long sumbest, V_Long gmin);
extern V_Void  tbf_setfirstnode(tbf frame, tbn node);
extern V_Void  tbf_setnodecount(tbf frame, V_Int nodecount);
extern V_Void  tbf_setpredecessor(tbf tbframe, tbf predecessor);
extern V_Void  tbf_setsumbestscores(tbf frame, V_Long sum);
extern V_Void  tbf_setsuccessor(tbf tbframe, tbf successor);
extern V_Void  tbf_settime(tbf frame, V_Time time);
extern V_Void  tbf_setsn(tbf frame, V_SN sn);
extern tbf     tbf_successor(tbf tbframe);
extern V_Long  tbf_sumbestscores(tbf tbframe);
extern V_Time  tbf_time(tbf frame);
extern V_SN    tbf_sn(tbf frame);
extern V_Int   tbf_used(VISE vise);

#ifndef NOMACROS
#define tbf_decnodecount(F)         (--(F)->tbfnodecount)
#define tbf_firstnode(F)            ((F)->tbffirstnode)
#define tbf_free(vise,F)            ((F)->tbfsuccessor = vise->tbfPool.freeentries),\
                                    (vise->tbfPool.freeentries = (F)),\
                                    (vise->tbfPool.used--)
#define tbf_globalmin(F)            ((F)->tbfglobalmin)
#define tbf_incnodecount(F)         (++(F)->tbfnodecount)
#define tbf_info(F,N,C,T,Q,S,G)     (*(N) = (F)->tbffirstnode),\
                                    (*(C) = (F)->tbfnodecount),\
                                    (*(T) = (F)->tbftime),\
                                    (*(Q) = (F)->tbfsn),\
                                    (*(S) = (F)->tbfsumbestscores),\
                                    (*(G) = (F)->tbfglobalmin)
#define tbf_nodecount(F)            ((F)->tbfnodecount)
#define tbf_predecessor(F)          ((F)->tbfpredecessor)
#define tbf_setglobalmin(F,G)       ((F)->tbfglobalmin = G)
#define tbf_setinfo(F,N,C,T,Q,S,G)  ((F)->tbffirstnode = N),\
                                    ((F)->tbfnodecount = C),\
                                    ((F)->tbftime      = T),\
                                    ((F)->tbfsn        = Q),\
                                    ((F)->tbfsumbestscores = S),\
                                    ((F)->tbfglobalmin = G)
#define tbf_setfirstnode(F,N)       ((F)->tbffirstnode = N)
#define tbf_setnodecount(F,C)       ((F)->tbfnodecount = C)
#define tbf_setpredecessor(F,P)     ((F)->tbfpredecessor = P)
#define tbf_setsumbestscores(F,S)   ((F)->tbfsumbestscores = S)
#define tbf_setsuccessor(F,S)       ((F)->tbfsuccessor = S)
#define tbf_settime(F,T)            ((F)->tbftime = T)
#define tbf_setsn(F,Q)              ((F)->tbfsn = Q)
#define tbf_successor(F)            ((F)->tbfsuccessor)
#define tbf_sumbestscores(F)        ((F)->tbfsumbestscores)
#define tbf_time(F)                 ((F)->tbftime)
#define tbf_sn(F)                   ((F)->tbfsn)
#define tbf_used(vise)              (vise->tbfPool.used)

/* For DEBUG */
#define tbf_print(F)                VBX_print("TBF [%p]: nodecount = %d, time = %d, sn = %ld, globalmin = %d, sumbestscores = %d\n", \
                                              (F), (F)->tbfnodecount, (F)->tbftime, (F)->tbfsn, (F)->tbfglobalmin, (F)->tbfsumbestscores)
#endif

#endif /* V_TBF_ */

