#ifndef V_GI_

/******************************************************************************
 * 
 * File:				V_GI.H
 *
 * Description:	Header file for the Grammar Instance module
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_GI.H_V  $
 * 
 *    Rev 1.5   20 Mar 1992 13:37:38   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * 
 *    Rev 1.4   29 Jan 1992 16:57:12   DCV
 * Changed basic types
 * 
 *    Rev 1.3   08 Nov 1991 12:59:30   DCV
 * Eliminated inherited path structures in node instances.
 * Inherited paths now handled with lists of path elements.
 * Added gifirstnonnullarc to prevent arcs with no initialized words
 *   from aliasing as null arcs.
 * 
 *    Rev 1.2   18 Jul 1991 16:45:26   DCV
 * Installed dynamic memory allocation
 * 
 *    Rev 1.1   19 Sep 1990  9:19:26   DCV
 * Extracted handles into HNDL.H
 * 
 *    Rev 1.0   13 Sep 1990 12:40:08   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_GI_ 0

#include "v_vise.h"

/*
 * Unlike the grammar model, the grammar instance descriptor
 * need not be concerned about memory allocation/deallocation, because
 * only a single grammar instance ever exists at any time.  Thus, a
 * simpler array representation can be used here, since the old grammar
 * can just be replaced with the new.
 *
 * The sole Grammar Instance Descriptor resides in the Syntax Descriptor.
 * It must be defined here in the header for the macros to work.
 */
typedef struct gi_s	{
	ai		giarcs;				/* The array of arc instances				*/
	ai		gifreeai;			/* The first free arc in the giarcs array	*/
	ai		gimaxai;			/* The last arc in the giarcs array			*/
	ai		gifirstnonnullai;	/* The first non-null arc instance			*/

	ni		ginodes;			/* The array of node instances				*/
	ni		gifreeni;			/* The first free node in the ginodes array	*/
	ni		gimaxni;			/* The last node in the ginodes array		*/
} gi_t;


extern ai  gi_addarc(gi grammar, ni source, ni destination);
extern ni  gi_addnode(gi grammar, V_NId nodeID);
extern ai  gi_firstarc(gi grammar);
extern ni  gi_firstnode(gi grammar);
extern ni  gi_lastnode(gi grammar);
extern ni  gi_getnode(gi grammar, V_NId nodeid);
extern ai  gi_lastarc(gi grammar);
extern gi  gi_init(VISE vise, gm grmmodel);

#ifndef	NOMACROS
#define gi_addnode(G,N)		        ((G)->gifreeni < (G)->gimaxni ? (ni_nodeinit((G)->gifreeni,N),(G)->gifreeni++) : NULL)
#define gi_addarc(G,S,D)	        ((G)->gifreeai < (G)->gimaxai ? (ai_arcinit((G)->gifreeai,S,D),(G)->gifreeai++) : NULL)
#define gi_firstarc(G)				((G)->giarcs)
#define gi_firstnonnullarc(G)		((G)->gifirstnonnullai)
#define gi_setfirstnonnullarc(G,A)	((G)->gifirstnonnullai = (A))
#define gi_lastarc(G)				((G)->gifreeai)
#define gi_firstnode(G)				((G)->ginodes)
#define gi_lastnode(G)				((G)->gifreeni)
#endif

#endif /* V_GI_ */
