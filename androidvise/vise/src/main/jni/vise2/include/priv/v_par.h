#ifndef V_PAR_

/******************************************************************************
 * 
 * File:    V_PAR.H
 *
 * Description: Header file for Parameters module
 *
 * Computer:  TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_PAR.H_V  $
 * 
 *    Rev 1.7   13 Oct 1994 14:48:40   DCV
 * Added 8 parameters for purpose of version control.
 * NUMCOMPONENTS, FRAMELENGTH, SAMPLERATE, SIGBITS,
 * ADCTYPE, SRAMFREE, DRAMFREE, JINVERSION.
 * 
 * 
 *    Rev 1.6   20 Mar 1992 13:42:30   DCV
 * Changed to V_xxx types to avoid conflicts with standard types
 * Added FRAMESHIFT parameter, defaulting to 128.
 * 
 *    Rev 1.5   29 Jan 1992 17:17:16   DCV
 * Changed basic types
 * Replaced ADC with BOOTVERSION
 * 
 *    Rev 1.4   22 Nov 1991 12:06:48   DCV
 * Added WEIGHTNORM parameter
 * 
 *    Rev 1.3   14 Nov 1991 12:16:12   DCV
 * Added JINSCALE and NOISEBITMASK parameters.
 * 
 *    Rev 1.2   14 Aug 1991 17:12:26   DCV
 * Added memory allocation parameters.
 * 
 *    Rev 1.1   20 Dec 1990 14:20:16   DCV
 * Added FASTMEM and SLOWMEM parameters
 * 
 *    Rev 1.0   13 Sep 1990 12:45:58   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_PAR_ 0

#ifdef __cplusplus
extern "C" {
#endif

typedef V_Int param_t, * PARAM;

#include "viseparm.h"
#include "v_vise.h"

extern V_Bool   param_init(VISE vise);
extern V_Bool   param_set(VISE vise, parid_t index, param_t value);
extern param_t  param_get(VISE vise, parid_t index);

#ifndef NOMACROS
#define param_get(vise, index)  (vise->parameters[(V_Int) index])
#endif

#ifdef __cplusplus
}
#endif
#endif /* V_PAR_ */
