/* Voice File Skeleton */

#ifndef _INIVOI_H
#define _INIVOI_H

#include "vfile.h"
#include "viseparm.h"

/* TR_KEY block */
#define KEYCLASSID 0

static FKEY keyStruct = { 0x1F };

/* TR_VERSION block */
#define VERCLASSID 0

static FSTRING verStruct = { 29, "Batch enrollment. Version 1.0"};

/* TR_UPAT silence pattern block */
#define SILCLASSID     0
#define NUM_SILENCES_IN_FILE 11
#define MAX_NUM_PARMS 16
#define LSUM                 200
#define LSQUARES             40000

static FUPATHEAD silHeader = {
  15,
  {"Global Silences"},
  0,                               /* Gender : must be filled */
  1,
  1,
  0xFFFF,
  0,
  LSUM * NUM_SILENCES_IN_FILE,
  LSQUARES * NUM_SILENCES_IN_FILE,
  NUM_SILENCES_IN_FILE,
  0,
  0,
  0,
  {""},
  0,                                /* TrainScheme : defaults to incremental */
  0,
  {""},
  0,
  NUM_SILENCES_IN_FILE,
  MAX_NUM_PARMS
};

/*  Array of FUPATPARM structures */


static FUPATPARM parmArray[MAX_NUM_PARMS] = {
  {VERSION, 4, 1},          /* Update according to FEVERSION -- for 850 - 3 */
  {JINSCALE, 0, 1},
  {NOISEBITMASK, 0, 1},
  {FRAMESHIFT, 141, 1},     /* Update according to FEVERSION */
  {JINNUMCOMPONENTS, 16, 1},
  {FRAMEWIDTH, 256, 1},
  {SAMPLERATE, 11025, 1},   /* Update according to FEVERSION */
  {SIGBITS, 12, 1},
  {AUDIOLEVEL, 2, 0},
  {FEVERSION, 900, 1},      /* Update with actual FEVERSION */
  {0, 0, 0},
  {0, 0, 0},
  {0, 0, 0},
  {0, 0, 0},
  {0, 0, 0},
  {0, 0, 0}
};

/* Silence patterns */

static FUPATE silPats[NUM_SILENCES_IN_FILE] = {
  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi1"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi2"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi3"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi4"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi5"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi6"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi7"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi8"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    7,
    {"*multi9"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {1, 50},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    6,
    {"*short"}
  },

  {
    LSUM,
    LSQUARES,
    1,
    1,
    1,
    1,
    {35, 55},
    1,
    {150, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128},
    5,
    {"*long"}
  }
};

/* TR_UPAT vocabulary pattern block */
#define VOCCLASSID 1

static FUPATHEAD vocHeader = {
  1,
  {"1"},
  0,                 /* Gender : must be filled */
  1,
  1,
  0xFFFF,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  {""},
  0,                                /* TrainScheme : defaults to incremental */
  0,
  {""},
  0,
  0,                /* wNumberPatterns -- Fill it with actual number of models */
  MAX_NUM_PARMS
};

typedef struct gender_s {
  char * gName;
  int    gVal;
} gender_t;

#define MAX_GENDER 6
static gender_t genders[MAX_GENDER] = {
  {"F", 1},
  {"M", 2},
  {"U", 3},
  {"G", 4},
  {"B", 5},
  {"C", 6}
};

#endif /* _INIVOI_H */
