/* mkjin.h - a creator of frames of derived features
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates frames of derived features from cepstral frames
 *
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _MKJIN_H_
#define _MKJIN_H_

#include "types.h"
#include "jinattr.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** MKJIN ***/

/* Create an MKJIN */
extern IGEN  MKJIN_create(IMemory * imem, ISRC src);

/* MKJIN Interface */
typedef struct imkjin_s * IMKJIN;

/* MKJIN Methods */
extern Bool IMKJIN_reset(IMKJIN imkjin);

/*** MKJIN Attributes ***/

/* Create a MKJIN attributes object */
extern IGEN  MKJINATTR_create(IMemory * imem);

/* A MKJIN Attributes interface */
typedef IATTR IMKJINATTR;
typedef iattr_t imkjinattr_t;

/* MKJIN Attributes Methods */
extern Bool  IMKJINATTR_getClassSize(IMKJINATTR imkjinattr, Int * classSizeP);
extern Bool  IMKJINATTR_setClassSize(IMKJINATTR imkjinattr, Int classSize);
extern Bool  IMKJINATTR_getFrameBytes(IMKJINATTR imkjinattr, Int * frameBytesP);
extern Bool  IMKJINATTR_setFrameBytes(IMKJINATTR imkjinattr, Int frameBytes);
extern Bool  IMKJINATTR_getLDHW(IMKJINATTR imkjinattr, Int * ldHWP);
extern Bool  IMKJINATTR_setLDHW(IMKJINATTR imkjinattr, Int ldHW);
extern Bool  IMKJINATTR_getFwdTol(IMKJINATTR imkjinattr, SN * fwdTolP);
extern Bool  IMKJINATTR_setFwdTol(IMKJINATTR imkjinattr, SN fwdTol);
extern Bool  IMKJINATTR_getBwdTol(IMKJINATTR imkjinattr, SN * bwdTolP);
extern Bool  IMKJINATTR_setBwdTol(IMKJINATTR imkjinattr, SN bwdTol);

#ifdef __cplusplus
}
#endif

#endif /* _MKJIN_H_ */
