/* syntax.h - VISE Grammar Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: syntax.h[1.0] Thu Apr 10 20:54:09 1997 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _SYNTAX_H_
#define _SYNTAX_H_

#include "types.h"
#include "vlbldefs.h"
#include "vfile.h"
#include "viseerr.h"

/* #define DEBUG_PARSER */

/* Grammar */
typedef struct syntax_s * SYNTAX;

#include "vocab.h"
#include "trans.h"
#include "vfile.h"

#ifdef __cplusplus
extern "C" { 
#endif

/* Methods for grammars */

extern SYNTAX      SYNTAX_create(UInt grammarId);
extern void        SYNTAX_annihilate(SYNTAX grammar);
extern V_Err       SYNTAX_status(SYNTAX grammar);
extern Bool        SYNTAX_loadHeader(SYNTAX syntax, VFILE recFile);
extern Bool        SYNTAX_loadGraph(SYNTAX syntax, VFILE recFile);
extern Bool        SYNTAX_loadTrans(SYNTAX syntax, VFILE recFile);
extern UInt        SYNTAX_vocabularyId(SYNTAX grammar);
extern UInt        SYNTAX_numWordsOnArc(SYNTAX grammar);
extern UInt        SYNTAX_id(SYNTAX grammar);
extern Char      * SYNTAX_name(SYNTAX grammar);
extern FGRAPH    * SYNTAX_graph(SYNTAX grammar);
extern FGRAPHARC * SYNTAX_arcs(SYNTAX grammar);
extern SYNTAX      SYNTAX_parse(SYNTAX grammar, VOCENT * tokens, UInt numTokens);
extern TRANS       SYNTAX_trans(SYNTAX grammar);
extern UShort      SYNTAX_dfltHRTmplIdx(SYNTAX grammar);
extern UShort      SYNTAX_dfltVRTmplIdx(SYNTAX grammar);
extern UShort      SYNTAX_dfltDRTmplIdx(SYNTAX grammar);

#ifdef __cplusplus
}
#endif
#endif /* _SYNTAX_H_ */
