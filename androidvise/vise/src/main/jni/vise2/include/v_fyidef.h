#ifndef V_FYIDEF_

/******************************************************************************
 * 
 * File:				V_FYIDEF.H
 *
 * Description:	Parameter type defs for Information Reporting module (FYI)
 *
 * Computer:		TMS320C30
 *
 * $Log:   C:\USR\SRC\VISE\INC\VCS\V_FYIDEF.H_V  $
 * 
 *    Rev 1.2   18 Jul 1991 16:43:28   DCV
 * Added parameters to report available memory, among other things
 * 
 *    Rev 1.1   12 Jun 1991 15:01:08   DCV
 * Added codes and messages to report number of tbfs and tbps in use.
 * 
 *    Rev 1.0   23 Jan 1991 09:32:40   DCV
 * Initial revision.
 * 
 *****************************************************************************/

#define V_FYIDEF_ 0

typedef enum fyiid_e {
	FYI_NOSUCHPARM,		/* Must always be the first parameter in enum */
	FYI_JINUNREAD,
	FYI_MAXJINUNREAD,
	FYI_ACTIVEWORDS,
	FYI_MINBESTSCORE,
	FYI_MAXBESTSCORE,
	FYI_MINGLOBALMIN,
	FYI_MAXGLOBALMIN,
	FYI_NUMTBFSINUSE,
	FYI_NUMTBNSINUSE,
	FYI_NUMTBPSINUSE,
	FYI_AVERAGEAMP,
	FYI_MAXAMP,
	FYI_NICOUNT,
	FYI_AICOUNT,
	FYI_WICOUNT,
	FYI_KICOUNT,
	FYI_KNDCOUNT,
	FYI_GENERIC0,
	FYI_GENERIC1,
	FYI_GENERIC2,
	FYI_GENERIC3,
	FYI_GENERIC4,
	FYI_FASTFREEMEM,
	FYI_SLOWFREEMEM,
    FYI_FASTMEM,
    FYI_SLOWMEM,
	FYI_NOTINSTALLED		/* Must always be the last parameter in enum */
	} fyiid_t;

	/* Text definitions for host reporting facility */
#define FYI_DEFPARMTXT \
	static char *FYI_parmTxt[] = { \
		"Unknown parameter type",\
		"JIN frames unread at end ",\
		"Max JIN frames unread    ",\
		"Max active word instances",\
		"Min best path score      ",\
		"Max best path score      ",\
		"Min global min frame cost",\
		"Max global min frame cost",\
		"Max tbfs used            ",\
		"Max tbns used            ",\
		"Max tbps used            ",\
		"Average amplitude        ",\
		"Maximum amplitude        ",\
		"Node instances in grammar",\
		"Arc instances in grammar ",\
		"Word instances in grammar",\
		"Kernel instances         ",\
		"Kernel traceback nodes   ",\
		"First generic parameter  ",\
		"Second generic parameter ",\
		"Third generic parameter  ",\
		"Fourth generic parameter ",\
		"Fifth generic parameter  ",\
		"Min free pages fast mem  ",\
		"Min free pages slow mem  ",\
		"Pages fast mem allocated ",\
		"Pages slow mem allocated ",\
		"FYI facility not installed"\
	}

#define IS_VALID_PID(PID) ((FYI_NOSUCHPARM < (PID)) && ((PID) < FYI_NOTINSTALLED))

#endif /* V_FYIDEF_ */

