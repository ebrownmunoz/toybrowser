/*----------------------------------------------------------------------------*
 *
 * DICS Module.
 *
 * Provides functions to create, search for entries in, and destroy big sorted
 * General Phonetic Dictionary as well as small unsorted Exception Dictionary.
 *
 * General Phonetic Dictionary public methods:
 *
 *    GDIC GDIC_create(const char *gdicFN, const char *orthoDelimiter, int *rc)
 *
 *    void GDIC_destroy(GDIC gdic);
 *
 *    char * GDIC_getPhone(GDIC gdic, const char *orthography);
 *
 * General Dictionary file is assumed to have the following format:
 *
 *    1)  Any line beginning with a '!' is a comment and is ignored.
 *    2)  All other lines should contain a orthography string, followed
 *        by orthoDelimiter symbol, followed by optional SPACE characters,
 *        followed by phonetic translation of the word, followed by \n.
 *        Total number of chars in line is expected to be < SMAX = 256.
 *    3)  If on GDIC_create the orthoDelimiter == NULL, the SPACE character
 *        is used as default delimiter.
 *    4)  orthoDelimiter symbol is assumed NOT to be a part of any multibyte
 *        character for language in use, so regular string search routins work.
 *
 *    For example:
 *
 *        ABALOS:               AA B AA L OW Z
 *        ABANDA:               AX B AE N DX AX
 *
 * Exception Dictionary public methods:
 *
 *    XDIC XDIC_create(const char *xdicFN, const char *wordDel,
 *                     const char *orthoDel, const char *phoneDel, int *rc);
 *
 *    void XDIC_destroy(XDIC xdic);
 *
 *    int    XDIC_wordIndex(XDIC xdic, const char *word);
 *    int    XDIC_orthoIndex(XDIC xdic, const char *ortho);
 *    int    XDIC_phoneIndex(XDIC xdic, const char *phone);
 *    char * XDIC_getOrtho(XDIC xdic, int index);
 *    char * XDIC_getPhone(XDIC xdic, int index);
 *    char * XDIC_getUnits(XDIC xdic, int index);
 *
 *  Exception Dictionary file is assumed to have the following format:
 *
 *    1)  Any line beginning with a '!' is a comment and is ignored.
 *    2)  All other lines contain: wordName, followed by
 *        wordDelimiter, followed by optional SPACE characters,
 *        followed by optional (orthography, followed by orthoDelimiter),
 *        followed by optional SPACE characters, followed by optional
 *        (phone sequence, followed by phoneDelimiter), followed by optional
 *        SPACE characters, followed by optional unit sequence, delimeted by \n
 *        Total number of chars in line is expected to be < DMAX = 512.
 *    3)  On XDIC_create ALL delimiters != NULL. All delimiter symbols must be
 *        distinctive, and wordDelimiter and orthoDelimiter assumed NOT to be
 *        a part of any multibyte character for language in use, so regular
 *        string search routins work.
 *
 *    For example:
 *
 *        AT&T:  A.T.&T.;  EY T IY AE N DD T IY,  *>ey Ey eY ey<t ^t<t t<iy t>iy iy iy<ae iy>ae ae ae<n n n<d ^d<d ^t<t t<iy t>iy iy iy<*
 *
 *---------------------------------------------------------------------------*/

#ifndef _DICS_H
#define _DICS_H

#ifdef __cplusplus
    extern "C" {
#endif

 /* General Dictionary object */
typedef struct gdic_s * GDIC;
 /* Exception Dictionary object */
typedef struct xdic_s * XDIC;

#define DICS_SUCCESS               0
#define DICS_BAD_PARAMETERS     -131
#define DICS_FILE_OPEN_FAILS    -132
#define DICS_OUT_OF_MEMORY      -133
#define DICS_EMPTY              -134


 /* General Dictionary Functions */
extern GDIC   GDIC_create(const char *gdicFN, const char *orthoDel, int *rc);
extern void   GDIC_destroy(GDIC gdic);
extern char * GDIC_getPhone(GDIC gdic, const char *ortho);

 /* Exception Dictionary Functions */
extern XDIC   XDIC_create(const char *xdicFN, const char *wordDel,
                        const char *orthoDel, const char *phoneDel, int *rc);
extern void   XDIC_destroy(XDIC xdic);
extern int    XDIC_wordIndex(XDIC xdic, const char *word);
extern int    XDIC_orthoIndex(XDIC xdic, const char *ortho);
extern int    XDIC_phoneIndex(XDIC xdic, const char *phone);
extern char * XDIC_getOrtho(XDIC xdic, int index);
extern char * XDIC_getPhone(XDIC xdic, int index);
extern char * XDIC_getUnits(XDIC xdic, int index);

#ifdef __cplusplus
    }
#endif /* __cplusplus */

#endif /* _DICS_H */
