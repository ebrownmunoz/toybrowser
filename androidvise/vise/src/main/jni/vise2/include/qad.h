    /* Base phone and diphone units header */
#ifndef _QAD_H
#define _QAD_H

#ifdef __cplusplus
    extern "C" {
#endif

#include "types.h"
#include "recfile.h"

   /* Quick Add Object */
typedef struct duset_s * QAD;

   /* QAD object error codes */
#define QAD_SUCCESS                          0
#define QAD_FAILURE_OUTOFMEMORY             -1
#define QAD_FAILURE_PARAMETERS              -2
#define QAD_FAILURE_OPENFILE                -3
#define QAD_FAILURE_READFILE                -4
#define QAD_FAILURE_INCOMPATIBLE            -5
#define QAD_FAILURE_BADPHONE                -6
#define QAD_FAILURE_SHORTBUFFER             -7
#define QAD_FAILURE_LONGWORD                -8
#define QAD_FAILURE_BADCOMPONENT            -9
#define QAD_LANGUAGE_NOTSUPPORTED          -10

   /* Exported functions */

 /* Constructor, destructor, initializer */
extern  QAD   QAD_create(const char *language, int *rc);
extern  void  QAD_destroy(QAD qad);
extern  int   QAD_load(QAD qad, char *fName);

 /* Transform functions */
extern  int   QAD_wordFromPhones(QAD qad, FUPATE *word,
                     const char *wordName, const char *transcription);
extern  int   QAD_wordFromComponents(QAD qad, FUPATE *word,
                     const char *wordName, const char *components);
extern  int   QAD_componentsFromPhones(QAD qad, const char *phones,
                     char *components, int maxSize);

 /* Acceess functions */
extern  void  QAD_version(UShort *version, UShort *buildNum);
extern  int   QAD_getGender(QAD qad);
extern  int   QAD_getJinVersion(QAD qad);
extern  int   QAD_getSampleRate(QAD qad);

extern  FKEY      * QAD_getKey(void);
extern  FSTRING   * QAD_getVer(void);
extern  FUPATHEAD * QAD_getSilenceHeader(QAD qad);
extern  FUPATPARM * QAD_getParameters(QAD qad);
extern  FUPATE    * QAD_getSilenceModels(QAD qad);

#ifdef __cplusplus
    }
#endif   /* __cplusplus */

#endif /* _QAD_H */
