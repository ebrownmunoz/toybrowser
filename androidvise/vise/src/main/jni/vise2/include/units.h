/*
 *  Public interface to subphone units operations
 */
#ifndef _UNITS_H
#define _UNITS_H

#include "types.h"

typedef struct jin_s   * jin;
typedef struct ph_s    * PH;
typedef struct phset_s * PHS;
typedef struct du_s    * DU;
typedef struct duset_s * DUS;
typedef struct trn_s   * TRN;

#ifdef __cplusplus
    extern "C" {
#endif

extern PH      getGeneralSet(const char * language);
extern PH      getLabeledSet(const char * language);

extern PHS     PHS_create(PH phones);
extern void    PHS_destroy(PHS phset);

extern char  * DU_name(DU u);
extern UChar * DU_template(DU u);

extern DUS     DUS_create(PHS phset);
extern void    DUS_destroy(DUS duset);
extern Int     DUS_load(DUS duset, char * fName);
extern Int     DUS_gender(DUS duset);
extern Int     DUS_version(DUS duset);
extern void    DUS_reset(DUS duset);
extern void    DUS_update(TRN trnArr, Int duCount, jin jinbuf, Bool final);
extern void    DUS_train(DUS duset);
extern void    DUS_save(DUS duset, Int jinVersion, Int genderVal, char *fName);
extern void    DUS_propagate(DUS duset, Bool all);
extern void    DUS_dump(DUS duset);
extern Int     getGenderVal(char *gender);

extern DU      TRN_unit(TRN t, Int i);
extern Int     TRN_end(TRN t, Int i);

#ifdef __cplusplus
    }
#endif /* __cplusplus */
#endif /* _UNITS_H */
