#ifndef _MSTHREAD_H_
#define _MSTHREAD_H_

typedef struct CRITICAL_SECTION {
  unsigned int LockCount;     /* Nesting count on critical section */
  HANDLE OwnerThread;         /* Handle of owner thread */
  HANDLE hCrit;               /* Handle to this critical section */
  unsigned int needtrap;      /* Trap in when freeing critical section */
  unsigned int dwReserved;    /* currently unused */
} CRITICAL_SECTION, *LPCRITICAL_SECTION;

#ifdef __cplusplus
extern "C" {
#endif

/* Micro$oft Critical Section Routines */
VOID __stdcall EnterCriticalSection(LPCRITICAL_SECTION);
VOID __stdcall LeaveCriticalSection(LPCRITICAL_SECTION);
VOID __stdcall InitializeCriticalSection(LPCRITICAL_SECTION);
BOOL __stdcall TryEnterCriticalSection(LPCRITICAL_SECTION);
VOID __stdcall DeleteCriticalSection(LPCRITICAL_SECTION);

#ifdef __cplusplus
}
#endif
#endif  /* _MSTHREAD_H_ */

