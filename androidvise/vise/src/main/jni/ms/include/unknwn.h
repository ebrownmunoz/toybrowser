#ifndef _UNKNWN_H_
#define _UNKNWN_H_

#ifdef __cplusplus
extern "C"{
#endif 

typedef interface IUnknown IUnknown;
typedef IUnknown *LPUNKNOWN;
typedef interface IClassFactory IClassFactory;

EXTERN_C const IID IID_IUnknown;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
interface IUnknown 
{
  public:
    virtual HRESULT QueryInterface(REFIID riid, void **ppvObject) = 0;
    virtual ULONG   AddRef(void) = 0;
    virtual ULONG   Release(void) = 0;
};
    
#else 	/* C style interface */

typedef struct IUnknownVtbl {
  HRESULT (*QueryInterface )(IUnknown *This, REFIID riid, void **ppvObject);
  ULONG   (*AddRef )(IUnknown *This);
  ULONG   (*Release )(IUnknown *This);
} IUnknownVtbl;

interface IUnknown {
  CONST_VTBL struct IUnknownVtbl *lpVtbl;
};

#ifdef COBJMACROS
#define IUnknown_QueryInterface(This, riid, ppvObject)	\
  (This)->lpVtbl->QueryInterface(This, riid, ppvObject)

#define IUnknown_AddRef(This)	(This)->lpVtbl->AddRef(This)
#define IUnknown_Release(This)	(This)->lpVtbl->Release(This)
#endif /* COBJMACROS */

#endif 	/* C style interface */

EXTERN_C const IID IID_IClassFactory;

#if defined(__cplusplus) && !defined(CINTERFACE)

interface IClassFactory : public IUnknown
{
  public:
    virtual HRESULT CreateInstance(IUnknown *pUnkOuter,
                                   REFIID riid, void **ppvObject) = 0;
    virtual HRESULT LockServer(BOOL fLock) = 0;
};
    
#else 	/* C style interface */

typedef struct IClassFactoryVtbl {
  HRESULT (*QueryInterface )(IClassFactory *This, REFIID riid, 
                             void **ppvObject);
  ULONG   (*AddRef )(IClassFactory *This);
  ULONG   (*Release )(IClassFactory *This);
  HRESULT (*CreateInstance )(IClassFactory *This, IUnknown *pUnkOuter,
                             REFIID riid, void **ppvObject);
  HRESULT (*LockServer )(IClassFactory *This, BOOL fLock);
} IClassFactoryVtbl;

interface IClassFactory { CONST_VTBL struct IClassFactoryVtbl *lpVtbl };

#ifdef COBJMACROS
#define IClassFactory_QueryInterface(This, riid, ppvObject)	\
        (This)->lpVtbl->QueryInterface(This, riid, ppvObject)

#define IClassFactory_AddRef(This)	 (This)->lpVtbl->AddRef(This)
#define IClassFactory_Release(This)	 (This)->lpVtbl->Release(This)
#define IClassFactory_CreateInstance(This, pUnkOuter, riid, ppvObject) \
           (This)->lpVtbl->CreateInstance(This, pUnkOuter, riid, ppvObject)
#define IClassFactory_LockServer(This, fLock) 
           (This)->lpVtbl->LockServer(This, fLock)
#endif /* COBJMACROS */

#endif 	/* C style interface */
#ifdef __cplusplus
}
#endif

#endif /* _UNKNWN_H_ */
