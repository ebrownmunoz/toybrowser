#ifndef _VBXOLE2_H_
#define _VBXOLE2_H_

#define _OLE32_               // Turn this on

#if defined(__cplusplus)
extern "C" {
#define PURE  =0
#else
#define PURE
#endif
#ifdef NEWLIB
#include <sys/wcefile.h>
#else
#include "mstypes.h"

typedef struct _FILETIME {
  DWORD dwLowDateTime;
  DWORD dwHighDateTime;
} FILETIME, *LPFILETIME;
#endif

typedef struct  _GUID {
  DWORD Data1;
  WORD Data2;
  WORD Data3;
  BYTE Data4[ 8 ];
} GUID;

typedef GUID  IID;
typedef IID  *LPIID;
#define IID_NULL            GUID_NULL
#define IsEqualIID(riid1, riid2) IsEqualGUID(riid1, riid2)

typedef GUID   CLSID;
typedef CLSID *LPCLSID;
#define CLSID_NULL GUID_NULL;
#define IsEqualCLSID(rclsid1, rclsid2) IsEqualGUID(rclsid1, rclsid2)

#if defined(__cplusplus)
#define REFGUID    const GUID &
#define REFIID     const IID &
#define REFCLSID   const CLSID &
#else
#define REFGUID    const GUID * const
#define REFIID     const IID * const
#define REFCLSID   const CLSID * const
#endif

#ifdef __cplusplus
}
#endif

#endif  /* _VBXOLE2_H_ */
