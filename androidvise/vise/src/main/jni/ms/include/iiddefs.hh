#ifndef _IIDDEFS_HH_
#define _IIDDEFS_HH_

#ifdef INITGUID
extern "C" const GUID IID_IUnknown =  {35, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46};
extern "C" const GUID IID_IDispatch = {36, 1, 2, 0xC0, 0, 0, 0, 0, 0, 0, 0x46};
#else
extern "C" const GUID IID_IUnknown;
extern "C" const GUID IID_IDispatch;
#endif

#endif /* __IIDDEFS_HH_ */
