#ifndef _VBXCOM_H_
#define _VBXCOM_H_

#if defined(NEWLIB)
#include "vbxole2.h"
#include "winerror.h"
#include "objbase.h"
#include "iiddefs.hh"
#include "unknwn.h"
#include "speech.h"
#else
#if !defined(WIN32)

/* Verbex SAPI subset */
#include "mstypes.h"
#include "vbxole2.h"
#include "winerror.h"
#include "objbase.h"
#include "iiddefs.hh"
#include "unknwn.h"
#include "speech.h"

#else

#include <ole2.h>
#include <winerror.h>
#include <objbase.h>
#include <unknwn.h>
#include <speech.h>

#endif
#endif
#endif /* _VBXCOM_H_ */
