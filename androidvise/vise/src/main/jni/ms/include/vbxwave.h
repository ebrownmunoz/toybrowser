/* vbxwave.h - Microsoft MMAudio Wave format definitions
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _VBXWAVE_H_
#define _VBXWAVE_H_

#ifndef WAVE_FORMAT_VBXCEP13
#define WAVE_FORMAT_VBXCEP13    601
#endif

#if !defined(CELIB)

#include "types.h"
#ifndef NEWLIB
#include "mstypes.h"
#endif

#ifdef __cplusplus
extern "C" { 
#endif

/* OLD general waveform format structure common to all formats */
typedef struct waveformat_tag {
    WORD        wFormatTag;      /* format type */
    WORD        nChannels;       /* number of channels (i.e. mono, stereo, etc.) */
    DWORD       nSamplesPerSec;  /* sample rate */
    DWORD       nAvgBytesPerSec; /* for buffer estimation */
    WORD        nBlockAlign;     /* block size of data */
} WAVEFORMAT, * PWAVEFORMAT, FAR * LPWAVEFORMAT;

#define  WAVE_FORMAT_QUERY     0x00000001
#define  WAVE_ALLOWSYNC        0x00000002
#define  WAVE_MAPPED           0x00000004
#define  WAVE_FORMAT_DIRECT    0x00000008
#define  WAVE_FORMAT_DIRECT_QUERY  (WAVE_FORMAT_QUERY | WAVE_FORMAT_DIRECT)

/* Flags for wFormatTag field of WAVEFORMAT */
#ifndef WAVE_FORMAT_PCM
#define WAVE_FORMAT_PCM     1
#endif

/* Specific waveform format structure for PCM data */
typedef struct pcmwaveformat_tag {
    WAVEFORMAT  wf;
    WORD        wBitsPerSample;
} PCMWAVEFORMAT, * PPCMWAVEFORMAT, FAR * LPPCMWAVEFORMAT;

/* Extended waveform format structure common to all non-PCM formats */
#ifndef _WAVEFORMATEX_
#define _WAVEFORMATEX_
typedef struct tWAVEFORMATEX {
    WORD        wFormatTag;      /* format type */
    WORD        nChannels;       /* number of channels (i.e. mono, stereo...) */
    DWORD       nSamplesPerSec;  /* sample rate */
    DWORD       nAvgBytesPerSec; /* for buffer estimation */
    WORD        nBlockAlign;     /* block size of data */
    WORD        wBitsPerSample;  /* number of bits per sample of mono data */
    WORD        cbSize;          /* the count in bytes of the size of */
			                     /* extra information (after cbSize) */
} WAVEFORMATEX, * PWAVEFORMATEX, FAR * LPWAVEFORMATEX;
typedef const WAVEFORMATEX FAR * LPCWAVEFORMATEX;
#endif /* _WAVEFORMATEX_ */

#ifdef __cplusplus
}
#endif

#endif /* _MMSYSTEM_H_ */ 

#endif /* _VBXWAVE_H_ */
