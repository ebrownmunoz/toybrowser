#ifndef _MSTYPES_H_
#define _MSTYPES_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"

#ifndef DECMME

typedef Long           LONG;
#ifndef VXWORKS
typedef ULong          ULONG;
#endif
typedef ULONG         *PULONG;
typedef UShort         USHORT;
typedef USHORT        *PUSHORT;
typedef UChar          UCHAR;
typedef UCHAR         *PUCHAR;
typedef Char          *PSZ;
typedef Void          *PVOID;

#define IN
#define FAR
#define OUT
#define OPTIONAL

#define __export
#if !defined(CYGWIN)
#define __cdecl
#define __stdcall
#endif


#undef  far
#undef  near
#undef  pascal

#define far
#define near
#define CONST          const

typedef ULong          DWORD;
#ifndef XWINDOWS
typedef Bool           BOOL;
#else
#ifndef XMD_H
typedef int            BOOL;
#endif
#endif
typedef UChar          BYTE;
typedef UShort         WORD;
typedef Char           CHAR;
typedef Char          *PCHAR;
typedef Float          FLOAT;
typedef Float         *PFLOAT;
typedef Bool          *PBOOL;
typedef Bool          *LPBOOL;
typedef Char          *PBYTE;
typedef Char          *LPBYTE;
typedef Int           *PINT;
typedef Int           *LPINT;
typedef UShort        *PWORD;
typedef UShort        *LPWORD;
typedef Long          *LPLONG;
typedef Long          *PLONG;
typedef ULong         *PDWORD;
typedef ULong         *LPDWORD;
typedef Void           VOID;
typedef Void          *LPVOID;
typedef const Void    *LPCVOID;
typedef Int            INT;
typedef UInt           UINT;
typedef UInt          *PUINT;

typedef Quad           QWORD;
typedef Quad          *PQWORD;

typedef UShort         WCHAR;
typedef UShort        *PWCHAR;
typedef Char          *PSTR;
typedef UShort        *PWSTR;
typedef UShort        *LPWSTR;
typedef const UShort  *PCWSTR;
typedef const UShort  *LPCWSTR;
typedef const Char    *LPCSTR, *PCSTR;
typedef const Char   **LPPCSTR, *PPCSTR;
typedef Ptr            LPSTORAGE;

/* Function pointer types used in various interfaces */
typedef void (*LPFNDESTROYED) (void);

/* Types for passing & returning polymorphic values */
typedef UInt   WPARAM;
typedef Long   HRESULT;
typedef Long   SCODE;
typedef Long  *PSCODE;
typedef Long   LPARAM;
typedef Long   LRESULT;
typedef UShort LANGID;

/* The HWND typedef */
typedef Void  *HWND;

typedef Void        *RECT;
typedef const Void  *LPCRECT;

#if defined(LINUX)
  typedef Char          *LPSTR;
  typedef wchar_t       *LPTSTR;
  typedef wchar_t        TCHAR;

  typedef Void          *HANDLE;
  typedef Void          *HINSTANCE;
  typedef Void          *HMODULE;
#endif

#if defined(CYGWIN)
typedef Char          *LPSTR;
typedef wchar_t       *LPTSTR;
typedef wchar_t        TCHAR;

typedef Void          *HANDLE;
typedef Void          *HINSTANCE;
typedef Void          *HMODULE;

#if 0
#define DECLSPEC_IMPORT __declspec(dllimport)
#define DECLSPEC_EXPORT __declspec(dllexport)
#endif
#include "msthread.h"
#endif

#else
#include <mme/mmsystem.h>
#include <mme/mme_api.h>
#define IN
#define FAR
#define OUT
#define OPTIONAL

#define __cdecl
#define __export
#define __stdcall


#undef  far
#undef  near
#undef  pascal

#define far
#define near
#define CONST          const
typedef Long           HRESULT;
typedef const Char    *PCSTR;
typedef Ptr            LPSTORAGE;
typedef Char          *PCHAR;
typedef Long          *PLONG;
typedef ULong          ULONG;
typedef Void          *PVOID;
typedef UShort         LANGID;
typedef Long           SCODE;

typedef Quad           QWORD;
typedef Quad          *PQWORD;

/* Function pointer types used in various interfaces */
typedef void (*LPFNDESTROYED) (void);
typedef void (*DESTROYFCN) (void);
#endif /* ndef DECMME */
#ifdef __cplusplus
}
#endif
#endif /* _MSTYPES_H_ */
