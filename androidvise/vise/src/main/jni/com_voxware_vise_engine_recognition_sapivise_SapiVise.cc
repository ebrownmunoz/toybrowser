// Generic inclusions
#include "android/log.h"
#include "pthr.h"
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include "vbx.h"
#include "urlcodec.h"

// Verbex C infrastructure
#include "types.h"
#include "imemory.h"

#include "cnisapivise.hh"
#include "voxware_engine_recognition_sapivise_SapiVise.h"
#include "cnisapivisegram.hh"
#include "vise_jni.h"

#define SMAX      256

#define VBX_DEBUG(P)
#define VBX_print(...) __android_log_print(ANDROID_LOG_INFO, "com_voxware_vise_engine_recognition_sapivise_SapiVise.cpp", __VA_ARGS__)

#define VISE_COMPLETETO  1000
#define VISE_INCOMPLTTO  1001

using namespace java::lang;
using voxware::engine::recognition::sapivise::SapiVise;
using java::util::ArrayList;
using java::util::List;

static UInt minLSDuration = 448;
static UInt maxSilDuration = 640;


//#define DECLARE_RECOGNIZER_ALIAS() \
//jobject recognizer = env->GetObjectField(thiz, SapiVise.recognizer); \
//if (!recognizer) { \
//	env->ThrowNew(NullPointerException.clazz, "recognizer is null"); \
//	return; \
//}

#define DECLARE_RECOGNIZER_ALIAS(aliasName, ret) \
jobject recognizer = env->GetObjectField(thiz, SapiVise.recognizer); \
if (!recognizer) { \
	env->ThrowNew(NullPointerException.clazz, "recognizer is null"); \
	return ret; \
}\
CNISapiVise *aliasName = (CNISapiVise *) env->GetDirectBufferAddress(recognizer);

CNISapiVise::CNISapiVise() :
		AudioUnknwnPtr(NULL),
		VbxEnumPtr(NULL),
		IAudioPtr(NULL),
		IVbxAudioFilePtr(NULL),
		ISRAttributesPtr(NULL),
		ISRCentralPtr(NULL),
		ISRFindPtr(NULL),
		ISRSpeakerPtr(NULL),
		IVbxEnginePtr(NULL),
		MboxAttr(NULL),
		NotifyMbox(NULL),
		NotifyClass(NULL),
		RefCnt(0),
		VU(0),
		success(FALSE),
		NotifySinkKey(0) {
	allocator.Calloc = IMEM_calloc;
	allocator.Free = IMEM_free;
}

CNISapiVise::~CNISapiVise() {
	if (NotifyMbox) {
		MBOX_destroy(NotifyMbox);
		NotifyMbox = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: NotifyMbox destroyed\n"));
	}
	if (MboxAttr) {
		MBOX_destroyAttr(MboxAttr);
		MboxAttr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: MboxAttr destroyed\n"));
	}
	if (NotifyClass) {
		CLASS_destroy(NotifyClass, NULL, NULL);
		NotifyClass = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: NotifyClass destroyed\n"));
	}
	if (IVbxEnginePtr) {
		IVbxEnginePtr->Release();
		IVbxEnginePtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: IVbxEnginePtr destroyed\n"));
	}
	if (ISRSpeakerPtr) {
		ISRSpeakerPtr->Release();
		ISRSpeakerPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: ISRSpeakerPtr destroyed\n"));
	}
	if (ISRFindPtr) {
		ISRFindPtr->Release();
		ISRFindPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: ISRFindPtr destroyed\n"));
	}
	if (ISRCentralPtr) {
		ISRCentralPtr->Release();
		ISRCentralPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: ISRCentralPtr destroyed\n"));
	}
	if (ISRAttributesPtr) {
		ISRAttributesPtr->Release();
		ISRAttributesPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: ISRAttributesPtr destroyed\n"));
	}
	if (VbxEnumPtr) {
		VbxEnumPtr->Release();
		VbxEnumPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: VbxEnumPtr destroyed\n"));
	}
	if (IVbxAudioFilePtr) {
		IVbxAudioFilePtr->Release();
		IVbxAudioFilePtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: IVbxAudioFilePtr destroyed\n"));
	}
	if (IAudioPtr) {
		IAudioPtr->Release();
		IAudioPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: IAudioPtr destroyed\n"));
	}
	if (AudioUnknwnPtr) {
		AudioUnknwnPtr->Release();
		AudioUnknwnPtr = NULL;
		VBX_DEBUG(VBX_print("CNSapiVise::~CNISapiVise: AudioUnknwnPtr destroyed\n"));
	}
}

STDMETHODIMP CNISapiVise::QueryInterface(REFIID riid, LPVOID *ppv)
{
	*ppv = NULL;

	// Always return our IUnkown for IID_IUnknown, IID_ITTSNotifySink
	if (IsEqualIID (riid, IID_IUnknown)) {
		VBX_DEBUG(VBX_print("CNISapiVise::QueryInterface: IUnknown interface queried\n"));
		*ppv = (LPVOID) this;
		return NOERROR;
	}
	if (IsEqualIID(riid, IID_ISRNotifySink)) {
		VBX_DEBUG(VBX_print("CNSapiVise::QueryInterface: ISRNotifySink interface queried\n"));
		*ppv = (LPVOID)(ISRNotifySink *) this;
		return NOERROR;
	}

	// otherwise, can't find
	return E_NOINTERFACE;
}

STDMETHODIMP_ (ULONG) CNISapiVise::AddRef(void) {
	return ++RefCnt;
}

STDMETHODIMP_(ULONG) CNISapiVise::Release(void) {
	if (--RefCnt != 0L) {
		return RefCnt;
	} else {
		delete this;
		return 0L;
	}
}

// ISRNotifySink notification methods

STDMETHODIMP CNISapiVise::AttribChanged(DWORD dwAttribute) {
	VBX_DEBUG(VBX_print("CNISapiVise::AttribChanged\n"));
	return NOERROR; // TODO: send AttribChanged event back to app
}

STDMETHODIMP CNISapiVise::Interference(QWORD timeStampBegin, QWORD timeStampEnd, DWORD type) {
	//reportStatus("Interference");
	return NOERROR;
}

STDMETHODIMP CNISapiVise::Sound(QWORD timeStampBegin, QWORD timeStampEnd) {
	VBX_DEBUG(VBX_print("CNISapiVise::Sound\n"));
	return NOERROR;
}

STDMETHODIMP CNISapiVise::UtteranceBegin(QWORD timeStamp) {
	return NOERROR;
}

STDMETHODIMP CNISapiVise::UtteranceEnd(QWORD timeStampBegin, QWORD timeStampEnd) {
	return NOERROR;
}

STDMETHODIMP CNISapiVise::VUMeter(QWORD timeStamp, WORD Level) {
	//reportStatus("VUMeter");
	VBX_DEBUG(VBX_print("CNISapiVise::VUMeter: %d", Level));
	VU = Level;
	return NOERROR;
}

jobject JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_newRecognizer(JNIEnv *env, jobject thiz) {
	CNISapiVise *rec;
	rec = new CNISapiVise();
	rec->AddRef();
	VBX_DEBUG(VBX_print("SapVise::newRecognizer() returns %p\n", rec));
	return env->NewDirectByteBuffer(rec, sizeof(CNISapiVise));
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1deallocate(JNIEnv *env, jobject thiz) {
	VBX_DEBUG(VBX_print("SapiVise::n_deallocate\n"));
	jobject recognizer = env->GetObjectField(thiz, SapiVise.recognizer);
	if (recognizer) {
	    CNISapiVise* vise = (CNISapiVise *) env->GetDirectBufferAddress(recognizer);
	    vise->Release();
		env->SetObjectField(thiz, SapiVise.recognizer, NULL);
		VBX_DEBUG(VBX_print("SapiVise::n_deallocate: recognizer deallocated\n"));
	}
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1notify(JNIEnv *env, jobject thiz, jobject rawResult) {
	VBX_DEBUG(VBX_print("SapiVise::n_notify\n"));
	jobject recognizer = env->GetObjectField(thiz, SapiVise.recognizer);
	if (!recognizer) {
		env->ThrowNew(NullPointerException.clazz, "recognizer is null");
		return;
	}
	CNISapiVise *rec = (CNISapiVise *) env->GetDirectBufferAddress(recognizer);

	OBJT objt = NULL;
	// TODO: Add support here to check for Thread.currentThread().isInterrupted()
	objt = MBOX_remove(rec->NotifyMbox);

	if (!objt) {
		VBX_print("SapiVise::n_notify: FATAL ERROR objt == NULL\n");
		return;
	}

	NotifyMsg *nmPtr = (NotifyMsg *) &OBJT_contents(objt);

	if (nmPtr->gramPtr == NULL) {
		VBX_print("SapiVise::n_notify: FATAL ERROR gramPtr == NULL\n");
		OBJT_free(objt);
		return;
	}

	nmPtr->gramPtr->ProcessNotify(env, objt, rawResult);
	memset(nmPtr, 0, sizeof(NotifyMsg));
	OBJT_free(objt);
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1pause(JNIEnv *env, jobject thiz) {
	VBX_DEBUG(VBX_print("SapiVise::n_pause\n"));

	jobject recognizer = env->GetObjectField(thiz, SapiVise.recognizer);
	if (!recognizer) {
		env->ThrowNew(NullPointerException.clazz, "recognizer is null");
		return;
	}
	CNISapiVise *rec = (CNISapiVise *) env->GetDirectBufferAddress(recognizer);

	if (rec != NULL && rec->ISRCentralPtr != NULL)
		rec->ISRCentralPtr->Pause();
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1resume(JNIEnv *env, jobject thiz) {
	jobject recognizer = env->GetObjectField(thiz, SapiVise.recognizer);
	if (!recognizer) {
		env->ThrowNew(NullPointerException.clazz, "recognizer is null");
		return;
	}
	CNISapiVise *rec = (CNISapiVise *) env->GetDirectBufferAddress(recognizer);

	Bool resumed;

	if (rec != NULL && rec->ISRCentralPtr != NULL) {
		resumed = rec->ISRCentralPtr->Resume();
		VBX_DEBUG(VBX_print("SapiVise::n_resume: ISRCentral::Resume returns %d\n", resumed));
	} else {
		VBX_print("SapiVise::n_resume: ERROR - ISRCentralPtr is NULL!\n");
	}
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1initialize(JNIEnv *env, jobject thiz, jstring mode) {
	jboolean success = JNI_FALSE;
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	// Get the mode ("voice" or "text")
	char modestr[SMAX];
	if (mode != NULL) {
		JavaToUTF8String(env, mode, modestr, SMAX);
	}

	VBX_print("SapiVise::n_initialize: rec = %p, mode is \"%s\"\n", rec, modestr);
	success = rec->Initialize(mode && strcmp(modestr, "text"));
	VBX_DEBUG(VBX_print("SapiVise::n_initialize: done\n"));
	return (success);
}

Bool CNISapiVise::Initialize(Bool voice) {
	HRESULT hres;

	AudioUnknwnPtr = NULL;
	ISRCentralPtr = NULL;

	VBX_print("CNISapiVise::Initialize: starting initialization\n");

	if (voice) {
#if defined(DECMME)
		// Create a DEC MME MMAudio Object
		MMAudioSrc *MMAudioSrcPtr = new MMAudioSrc(NULL, NULL);
		if (MMAudioSrcPtr == NULL) FATAL_BRA_ "could not instantiate DEC MMAudioSrc\n" _KET;
		MMAudioSrcPtr->AddRef();
		AudioUnknwnPtr = (LPUNKNOWN) MMAudioSrcPtr;
		VBX_fatalIf(!MMAudioSrcPtr->Initialize());
		VBX_print("CNISapiVise::Initialize: DEC MMAudioSrc instantiated\n");
#elif defined(OSSAUDIO)
		// Create an OSS Audio Source Object
		OssAudioSrc *OssAudioSrcPtr = new OssAudioSrc(NULL, NULL);
		if (OssAudioSrcPtr == NULL) FATAL_BRA_ "could not instantiate OSSAudioSrc\n" _KET;
		OssAudioSrcPtr->AddRef();
		AudioUnknwnPtr = (LPUNKNOWN) OssAudioSrcPtr;
		VBX_fatalIf(!OssAudioSrcPtr->Initialize("/dev/dsp"));
		VBX_print("CNISapiVise::Initialize: OSSAudioSrc instantiated\n");
#elif defined(WINVC)
		// Create a Verbex WINVC audio Object
		WINVCAudio *WINVCAudioPtr = new WINVCAudio(NULL, NULL);
		if (WINVCAudioPtr == NULL) FATAL_BRA_ "could not instantiate WINVCAudio\n" _KET;
		WINVCAudioPtr->AddRef();
		AudioUnknwnPtr = (LPUNKNOWN) WINVCAudioPtr;
		VBX_fatalIf(!WINVCAudioPtr->Initialize("/dev/winvc0", "/a/lib/new_winvc_fe.vbf"));
		VBX_print("CNISapiVise::Initialize: WINVCAudio instantiated\n");
#elif defined(INROADAUDIO)
		// Create a InroadAudioSrc Object
		InroadAudioSrc *InroadAudioSrcPtr = new InroadAudioSrc(NULL, NULL);
		if (InroadAudioSrcPtr == NULL) FATAL_BRA_ "could not instantiate InroadAudioSrc\n" _KET;
		InroadAudioSrcPtr->AddRef();
		AudioUnknwnPtr = (LPUNKNOWN) InroadAudioSrcPtr;
		VBX_fatalIf(!InroadAudioSrcPtr->Initialize());
		VBX_print("CNISapiVise::Initialize: InroadAudioSrc instantiated InroadAudioSrc\n");
#elif defined(WINCEAUDIO)
		// Create an MMAudioSrc Object
		MMAudioSrc *MMAudioSrcPtr = new MMAudioSrc(NULL, NULL);
		if (MMAudioSrcPtr == NULL) FATAL_BRA_ "could not instantiate MMAudioSrc\n" _KET;
		AudioUnknwnPtr = (LPUNKNOWN) MMAudioSrcPtr;
		AudioUnknwnPtr->AddRef();
		VBX_fatalIf(!MMAudioSrcPtr->Initialize(2048, 40));
		VBX_print("CNISapiVise::Initialize: MMAudioSrc instantiated\n");
#elif defined(__ANDROID__)
		AndroidAudioSrc *AndroidAudioSrcPtr = new AndroidAudioSrc(NULL, NULL);
		if (AndroidAudioSrcPtr == NULL)
			FATAL_BRA_ "could not instantiate AndroidAudioSrc\n" _KET;
		AudioUnknwnPtr = (LPUNKNOWN) AndroidAudioSrcPtr;
		AudioUnknwnPtr->AddRef();
		VBX_fatalIf(!AndroidAudioSrcPtr->Initialize(2048, 40));
		VBX_print("CNISapiVise::Initialize: AndroidAudioSrc instantiated\n");
#else
		// Create a NISTAudio object
		NISTIn *nistIn = new NISTIn(NULL, NULL);
		if (nistIn == NULL) FATAL_BRA_ "could not instantiate NISTIn\n" _KET;
		AudioUnknwnPtr = (LPUNKNOWN) nistIn;
		AudioUnknwnPtr->AddRef();
		VBX_fatalIf(AudioUnknwnPtr->QueryInterface(IID_IAudioFile, (PVOID *) &IVbxAudioFilePtr));
		// Set the default directory for loading NIST files into the NISTAudio object
		IVbxAudioFilePtr->SetDefaultDir(".");
		VBX_print("CNISapiVise::Initialize: NISTIn instantiated\n");
#endif
		// Get the IAudio interface for future use
		VBX_fatalIf(AudioUnknwnPtr->QueryInterface(IID_IAudio, (PVOID * ) &IAudioPtr));
	}

	// Create a Verbex Engine Enumerator object
	VbxEnumPtr = new VbxEnum((LPUNKNOWN) NULL);
	VbxEnumPtr->AddRef();
	if (VbxEnumPtr == NULL)
		FATAL_BRA_ "could not instantiate VbxEnum\n" _KET;
	VBX_DEBUG(VBX_print("CNISapiVise::Initialize: VbxEnumPtr = %p\n", VbxEnumPtr));

	// Query the Enumerator for the Find interface
	*ISRFindPtr;
	VBX_fatalIf(VbxEnumPtr->QueryInterface(IID_ISRFind, (PVOID *) &ISRFindPtr)); VBX_DEBUG(VBX_print("CNISapiVise::Initialize: ISRFindPtr = %p\n", ISRFindPtr));

	// Ask the Enumerator to find the "most appropriate recognition mode"
	SRMODEINFOW modeInfo;
	ISRFindPtr->Find(NULL, NULL, (SRMODEINFO *) &modeInfo);
	VBX_DEBUG(VBX_print("CNISapiVise::Initialize: Find returned\n"));

	// Utilize the Find interface to instantiate an engine for this mode
	ISRFindPtr->Select(modeInfo.gModeID, &ISRCentralPtr, AudioUnknwnPtr);
	VBX_print("CNISapiVise::Initialize: SAPIVISE instantiated (ISRCentralPtr = %p)\n", ISRCentralPtr);

	hres = ISRCentralPtr->QueryInterface(IID_ISRAttributes, (void **) &ISRAttributesPtr);
	if (FAILED(hres))
		VBX_print("CNISapiVise::Initialize: ERROR - can't get ISRAttributes\n");

	// Get the ISRSpeaker interface to the engine object
	hres = ISRCentralPtr->QueryInterface(IID_ISRSpeaker, (PVOID *) &ISRSpeakerPtr);
	// Get the IVbxEngine (proprietary extension) interface to the engine object
	hres = ISRCentralPtr->QueryInterface(IID_IVbxEngine, (PVOID *) &IVbxEnginePtr);

	// Set the VISE object parameters from command-line options
	ISRAttributesPtr->TimeOutSet(maxSilDuration, minLSDuration);

	// Register our notification sink
	hres = ISRCentralPtr->Register((ISRNotifySink *) this, IID_ISRNotifySink, &NotifySinkKey);
	if (hres) {
		VBX_DEBUG(VBX_print("CNISapiVise::Initialize: ERROR - couldn't register notify sink\n"));
		return JNI_FALSE;
	}

	// Create the PhraseFinish notification mailbox
	MboxAttr = MBOX_createAttr(&allocator, 10, 10, 1);
	if (!MboxAttr) {
		VBX_print("CNISapiVise::Initialize: FATAL ERROR could not create PhraseFinish notification MBOX_Att\n");
		return JNI_FALSE;
	}
	NotifyMbox = MBOX_create(&allocator, MboxAttr);
	if (!NotifyMbox) {
		VBX_print("CNISapiVise::Initialize: SEVERE ERROR could not create PhraseFinish notification Mbox\n");
		return JNI_FALSE;
	}

	// Set up the CLASS of notification messages
	NotifyClass = CLASS_create(&allocator, 10, sizeof(NotifyMsg_t), NULL, NULL);

	VBX_print("CNISapiVise::Initialize: finished\n");
	return TRUE;
}

jint JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1levelget(JNIEnv *env, jobject thiz) {
	DECLARE_RECOGNIZER_ALIAS(rec, -1)

	VBX_DEBUG(VBX_print("SapiVise::n_levelget: rec = %p\n", rec));

	DWORD dwLevel = 0;
#if !defined(CYGWIN)
	if (!rec->IAudioPtr) {
		VBX_print("SapiVise::n_levelget: ERROR - IAudioPtr == NULL\n");
		return -1;
	}

	HRESULT hres;
	hres = rec->IAudioPtr->LevelGet(&dwLevel);
	if (hres)
		return -1;
#endif

	return (jint) dwLevel;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1levelset(JNIEnv *env, jobject thiz, jint level) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

#if !defined(CYGWIN)
	if (!rec->IAudioPtr) {
		VBX_print("SapiVise::n_levelset: ERROR - IAudioPtr == NULL\n");
		return JNI_FALSE;
	}

	HRESULT hres;
	hres = rec->IAudioPtr->LevelSet((jint) level);
	if (hres)
		return JNI_FALSE;
#endif

	return TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1calibrate(JNIEnv *env, jobject thiz) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	VBX_DEBUG(VBX_print("SapiVise::n_calibrate: rec = %p\n", rec));

	// Calibrate Silence
	HRESULT hres;
	ISRDialogs *ISRDialogsPtr;
	hres = rec->ISRCentralPtr->QueryInterface(IID_ISRDialogs, (PVOID *) &ISRDialogsPtr);
	VBX_DEBUG(VBX_print("SapiVise::n_calibrate: Query IID_ISRDialogs: hres %x iptr %p\n", hres, ISRDialogsPtr));
	if (!ISRDialogsPtr) {
		VBX_print("SapiVise::n_calibrate: ERROR - QI for ISRDialogs FAILS\n");
		return JNI_FALSE;
	} VBX_DEBUG(VBX_print("CALIBRATING SILENCE, PLEASE BE QUIET!\n"));
	ISRDialogsPtr->TrainMicDlg(NULL, NULL);
	ISRDialogsPtr->Release();

	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1deletespeaker(JNIEnv *env, jobject thiz, jstring spkrName) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	jboolean retval = JNI_TRUE;
	HRESULT hres = SRERR_NONE;
	VBX_DEBUG(VBX_print("SapiVise::n_deletespeaker: rec = %p\n", rec));

	if (spkrName == NULL) {
		VBX_print("SapiVise::n_deletespeaker: ERROR - spkrName == NULL\n");
		return (JNI_FALSE);
	}

	char strbuf[SMAX];
	char encbuf[SMAX];
	env->GetStringUTFRegion(spkrName, 0, env->GetStringLength(spkrName), strbuf);
	strbuf[(int) env->GetStringUTFLength(spkrName)] = '\0';
	if (strlen(strbuf) == 0) {
		VBX_print("SapiVise::n_deletespeaker: ERROR - GetStringUTFChars(spkrName) returns empty string\n");
		retval = JNI_FALSE;
	} else if (!URL_encode(strbuf, encbuf, SMAX)) {
		VBX_print("SapiVise::n_deletespeaker: ERROR - cannot URL-encode the speaker name\n");
		retval = JNI_FALSE;
	} else {
		VBX_DEBUG(VBX_print("SapiVise::n_deletespeaker: deleting speaker %s\n", encbuf));
		hres = rec->ISRSpeakerPtr->Delete(encbuf);
	}

	if (hres) {
		VBX_print("SapiVise::n_deletespeaker: ERROR - ISRSpeaker::Delete returns hres %x\n", hres);
		retval = JNI_FALSE;
	}

	VBX_DEBUG(VBX_print("SapiVise::n_deletespeaker: speaker %s deleted\n", encbuf));
	return retval;
}

jobject JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1listspeakers(JNIEnv *env, jobject thiz) {
	int i, namesCnt;

	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	VBX_DEBUG(VBX_print("SapiVise::n_listspeakers: rec = %p\n", rec));

	if (rec == NULL) {
		VBX_print("SapiVise::n_listspeakers: ERROR - rec == NULL\n");
		return NULL;
	}

	HRESULT hres;
	Char *namesBuf, *entryPtr;
	DWORD namesBufLen;
	hres = rec->ISRSpeakerPtr->Enum(&namesBuf, &namesBufLen);

	if (hres || !namesBuf || !namesBufLen) {
		VBX_print("SapiVise::n_listspeakers:  ERROR - ISRSpeaker::Enum failed\n");
		return NULL;
	}

	// Count the number of speaker entries first
	entryPtr = namesBuf;

	namesCnt = 0;
	while (*entryPtr != 0) {
		entryPtr += strlen(entryPtr);
		entryPtr++;
		namesCnt++;
	}

	jobject namesList = env->NewObject(ArrayList.clazz, ArrayList.ctor);
	//jobjectarray namesArray = env->NewObjectArray(namesCnt, jni.java.lang.String.clazz, NULL);
	//jstringArray namesArray = (jstringArray) JvNewObjectArray(namesCnt, (new java::lang::String())->getClass(), NULL);

	if (namesList == NULL) {
		VBX_print("SapiVise::n_listspeakers:  ERROR - NULL result for array allocation\n");
		return NULL;
	}

	// Now populate the Java string array with the decoded speaker names
	entryPtr = namesBuf;
	Char decBuf[SMAX];
	//jstring * namesPtr = env->elements(namesArray);
	for (i = 0; i < namesCnt; i++) {
		if (!URL_decode(decBuf, entryPtr, SMAX)) {
			VBX_print("SapiVise::n_listspeakers: could not URL-decode speaker[%d]\n", i);
			decBuf[0] = '0';
		} else {
			VBX_DEBUG(VBX_print("SapiVise::n_listspeakers: speaker[%d] is \"%s\"\n", i, decBuf));
		}

		//jstring name = JvNewStringUTF(decBuf);
		jstring name = env->NewStringUTF(decBuf);
		//namesPtr[i] = name;
		env->CallBooleanMethod(namesList, List.add__Object, name);
		entryPtr += strlen(entryPtr);
		entryPtr++;
	}

	VBX_free(namesBuf);
	//return namesArray;
	return namesList;
}

jstring JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1queryspeaker(JNIEnv *env, jobject thiz) {
	DECLARE_RECOGNIZER_ALIAS(rec, NULL)

	VBX_DEBUG(VBX_print("SapiVise::n_queryspeaker: rec = %p\n", rec));

	// Query SAPIVISE for the current speaker's name
	Char nameBuf[SMAX];
	DWORD nameBufMinLen = 0;
	Char decBuf[SMAX];
	HRESULT hres;
	nameBuf[0] = '\0';
	hres = rec->ISRSpeakerPtr->Query(nameBuf, SMAX, &nameBufMinLen);

	if (hres) {
		VBX_DEBUG(VBX_print("SapiVise::n_queryspeaker: ERROR - ISRSpeakerQuery FAILS hres = %x\n", hres));
	}

	if (nameBufMinLen >= SMAX) {
		VBX_print("SapiVise::n_queryspeaker: ERROR - nameBufLen exceeds string allocation\n");
		nameBuf[0] = '\0';
	}

	decBuf[0] = '\0';
	if (strlen(nameBuf) == 0) {
		VBX_DEBUG(VBX_print("SapiVise::n_queryspeaker: INFO - current speaker name has 0 length\n"));
	} else if (!URL_decode(decBuf, nameBuf, SMAX)) {
		VBX_print("SapiVise::n_queryspeaker: ERROR - cannot URL-decode the speaker name\n");
	}

	VBX_DEBUG(VBX_print("SapiVise::n_queryspeaker: INFO: current speaker is \"%s\"\n", decBuf));
	return env->NewStringUTF(decBuf);
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1readspeaker(JNIEnv *env, jobject thiz, jstring spkrName, jbyteArray array) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	jboolean retval = JNI_FALSE;
	VBX_DEBUG(VBX_print("SapiVise::n_readspeaker: rec = %p\n", rec));

	if (rec == NULL) {
		VBX_print("SapiVise::n_readspeaker: ERROR - rec == NULL\n");
	} else if (rec->ISRSpeakerPtr == NULL) {
		VBX_print("SapiVise::n_readspeaker: ERROR - ISRSpeakerPtr == NULL\n");
	} else {
		char strbuf[SMAX];
		char encbuf[SMAX];
		JavaToUTF8String(env, spkrName, strbuf, SMAX);
		if (strlen(strbuf) == 0) {
			VBX_print("SapiVise::n_readspeaker: ERROR - GetStringUTFChars(spkrName) returns empty string\n");
		} else if (!URL_encode(strbuf, encbuf, SMAX)) {
			VBX_print("SapiVise::n_readspeaker: ERROR - cannot URL-encode the speaker name\n");
		} else {
			DWORD voiLen = env->GetArrayLength(array);
			VBX_DEBUG(VBX_print("SapiVise::n_readspeaker: got %d bytes for speaker \"%s\"\n", voiLen, encbuf));
			jbyte *voiData = (jbyte *) calloc(1, voiLen);
			if (voiData == NULL) {
				VBX_print("SapiVise::n_readspeaker: ERROR - voiData block is NULL\n");
			} else {
				jbyte* arrayElements = env->GetByteArrayElements(array, NULL);
				memcpy(voiData, (char *) arrayElements, voiLen);
				env->ReleaseByteArrayElements(array, arrayElements, JNI_ABORT);

				HRESULT hres = rec->ISRSpeakerPtr->Write(encbuf, voiData, voiLen);
				if (hres == SRERR_SPEAKEREXISTS) {
					hres = rec->ISRSpeakerPtr->Merge(encbuf, voiData, voiLen);
					if (hres) {
						VBX_print("SapiVise::n_readspeaker: ERROR - ISRSpeaker::Merge returns hres %x\n", hres);
					} else {
						VBX_DEBUG(VBX_print("SapiVise::n_readspeaker: ISRSpeaker::Merge wrote %d bytes for speaker \"%s\"\n", voiLen, encbuf));
						retval = JNI_TRUE;
					}
				} else if (hres) {
					VBX_DEBUG(VBX_print("SapiVise::n_readspeaker: ERROR - ISRSpeaker::Write returns hres %x\n", hres));
				} else {
					VBX_DEBUG(VBX_print("SapiVise::n_readspeaker: ISRSpeaker::Write wrote %d bytes for speaker \"%s\"\n", voiLen, encbuf));
					retval = JNI_TRUE;
				}
				free(voiData);
			}
		}
	}

	return retval;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1selectspeaker (JNIEnv *env, jobject thiz, jstring spkrName) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	jboolean retval = JNI_TRUE;
	HRESULT hres = SRERR_NONE;
	VBX_DEBUG(VBX_print("SapiVise::n_selectspeaker: rec = %p\n", rec));

	if (spkrName == NULL) {
		VBX_DEBUG(VBX_print("SapiVise::n_selectspeaker: WARNING - NULL speaker name\n"));
		return JNI_FALSE;
	}

	if (rec->ISRSpeakerPtr == NULL) {
		VBX_print("SapiVise::n_selectspeaker: ERROR - ISRSpeakerPtr == NULL\n");
		return JNI_FALSE;
	}

	char strbuf[SMAX];
	char encbuf[SMAX];
	JavaToUTF8String(env, spkrName, strbuf, SMAX);
	if (strlen(strbuf) == 0) {
		VBX_print("SapiVise::n_selectspeaker: ERROR - GetStringUTFChars(spkrName) returns empty string\n");
		retval = JNI_FALSE;
	} else if (!URL_encode(strbuf, encbuf, SMAX)) {
		VBX_print("SapiVise::n_selectspeaker: ERROR - cannot URL-encode the speaker name\n");
		retval = JNI_FALSE;
	} else {
		VBX_DEBUG(VBX_print("SapiVise::n_selectspeaker: speaker is \"%s\"\n", encbuf));
		hres = rec->ISRSpeakerPtr->Select(encbuf, TRUE);
		if (hres) {
			VBX_print("SapiVise::n_selectspeaker: ERROR - ISRSpeaker::Select returns hres %x\n", hres);
			retval = JNI_FALSE;
		}
	}

	VBX_DEBUG(VBX_print("SapiVise::n_selectspeaker: DONE\n"));
	return retval;
}

jbyteArray JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1writespeaker(JNIEnv *env, jobject thiz, jstring spkrName) {
	DECLARE_RECOGNIZER_ALIAS(rec, NULL)

	jbyteArray array = NULL;
	VBX_DEBUG(VBX_print("SapiVise::n_writespeaker: rec = %p\n", rec));

	if (spkrName == NULL) {
		VBX_DEBUG(VBX_print("SapiVise::n_writespeaker: WARNING - NULL speaker name\n"));
		return array;
	}

	if (rec->ISRSpeakerPtr == NULL) {
		VBX_print("SapiVise::n_writespeaker: ERROR - ISRSpeakerPtr == NULL\n");
		return array;
	}

	HRESULT hres = SRERR_NONE;
	PVOID voiData = NULL;
	DWORD voiLen = 0;
	char strbuf[SMAX];
	char encbuf[SMAX];
	JavaToUTF8String(env, spkrName, strbuf, SMAX);
	if (strlen(strbuf) == 0) {
		VBX_print("SapiVise::n_writespeaker: ERROR - GetStringUTFChars(spkrName) returns empty string\n");
		return array;
	} else if (!URL_encode(strbuf, encbuf, SMAX)) {
		VBX_print("SapiVise::n_writespeaker: ERROR - cannot URL-encode the speaker name\n");
		return array;
	} else {
		hres = rec->ISRSpeakerPtr->Read(encbuf, &voiData, &voiLen);
	}

	if (hres) {
		VBX_print("SapiVise::n_writespeaker: ERROR - ISRSpeaker::Read returns hres %x\n", hres);
		return array;
	} else {
		VBX_DEBUG(VBX_print("SapiVise::n_writespeaker: ISRSpeaker::Read read %d bytes for speaker \"%s\"\n", voiLen, encbuf));
	}

	if (voiData == NULL || voiLen == 0) {
		VBX_print("SapiVise::n_writespeaker: ERROR - ISRSpeaker::Read returns NO DATA for speaker \"%s\"\n", encbuf);
		return array;
	}

	// Finally, transfer the voice file data read by SAPIVISE into a
	// Java byte array we can return to JSAPI
	array = env->NewByteArray(voiLen);
	env->SetByteArrayRegion(array, 0, voiLen, (jbyte*)voiData);

	// Free the allocation from SAPIVISE (M$ Windoze: convert to CoTaskMemFree)
	VBX_free(voiData);
	return array;
}


jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1getparameter__IF(JNIEnv *env, jobject thiz, jint index, jfloat value) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	jboolean retval = JNI_FALSE;
	Float ccval;

	if (rec == NULL) {
		VBX_print("SapiVise::n_getparameter: ERROR - rec is NULL\n");
		return retval;
	}

	HRESULT hres = rec->IVbxEnginePtr->GetParameter(index, &ccval);
	value = (jfloat) ccval;

	env->SetFloatField(thiz, SapiVise.floatVal, value);
	retval = (hres == SRERR_NONE);
	return retval;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1getparameter__II(JNIEnv *env, jobject thiz, jint index, jint value) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	jboolean retval = JNI_FALSE;
	Int ccval;

	if (rec == NULL) {
		VBX_print("SapiVise::n_getparameter: ERROR - rec is NULL\n");
		return retval;
	}

	HRESULT hres;
	DWORD complete, incomplete;

	switch (index) {
	case VISE_COMPLETETO:
		hres = rec->ISRAttributesPtr->TimeOutGet(&incomplete, (DWORD *) &ccval);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_getparameter(long, int, int): WARNING - ISRAttributes::TimeOutGet returns %x\n", hres);
			return JNI_FALSE;
		}
		break;
	case VISE_INCOMPLTTO:
		hres = rec->ISRAttributesPtr->TimeOutGet((DWORD *) &ccval, &complete);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_getparameter(long, int, int): WARNING - ISRAttributes::TimeOutGet returns %x\n", hres);
			return JNI_FALSE;
		}
		break;
	default:
		hres = rec->IVbxEnginePtr->GetParameter(index, &ccval);
	}

	value = (jint) ccval;
	env->SetIntField(thiz, SapiVise.intVal, value);
	retval = (hres == SRERR_NONE);
	return retval;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1setparameter__I(JNIEnv *env, jobject thiz, jint index){
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	jboolean retval = JNI_FALSE;

	if (rec == NULL) {
		VBX_print("SapiVise::n_setparameter: ERROR - rec is NULL\n");
		return retval;
	}

	HRESULT hres;
	VBX_DEBUG(VBX_print("SapiVise::n_setparameter: Calling EngineObj::SetParameter(%d)\n", index));
	hres = rec->IVbxEnginePtr->SetParameter(index);

	if (hres != SRERR_NONE) {
		VBX_print("SapiVise::n_setparameter(long, int): WARNING - EngineObj::SetParameter returns %x\n", hres);
		return JNI_FALSE;
	} else {
		return JNI_TRUE;
	}
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1setparameter__IF(JNIEnv *env, jobject thiz, jint index, jfloat value) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	jboolean retval = JNI_FALSE;

	if (rec == NULL) {
		VBX_print("SapiVise::n_setparameter: ERROR - rec is NULL\n");
		return retval;
	}

	HRESULT hres;
	Float ccval = (Float) value;
	VBX_DEBUG(VBX_print("SapiVise::n_setparameter: Calling EngineObj::SetParameter(%d, %f)\n", index, ccval));
	hres = rec->IVbxEnginePtr->SetParameter(index, ccval);

	if (hres != SRERR_NONE) {
		VBX_print("SapiVise::n_setparameter(long, int, float): WARNING - EngineObj::SetParameter returns %x\n", hres);
		return JNI_FALSE;
	} else {
		return JNI_TRUE;
	}
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1setparameter__II(JNIEnv *env, jobject thiz, jint index, jint value) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	jboolean retval = JNI_FALSE;

	if (rec == NULL) {
		VBX_print("SapiVise::n_setparameter: ERROR - rec is NULL\n");
		return retval;
	}

	HRESULT hres;
	Int ccval = (Int) value;
	DWORD complete, incomplete;

	switch (index) {
	case VISE_COMPLETETO:
		hres = rec->ISRAttributesPtr->TimeOutGet(&incomplete, &complete);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_setparameter(long, int, int): WARNING - ISRAttributes::TimeOutGet returns %x\n", hres);
			return JNI_FALSE;
		}

		hres = rec->ISRAttributesPtr->TimeOutSet(incomplete, (DWORD) ccval);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_setparameter(long, int, int): WARNING - ISRAttributes::TimeOutSet returns %x\n", hres);
			return JNI_FALSE;
		}
		break;
	case VISE_INCOMPLTTO:
		hres = rec->ISRAttributesPtr->TimeOutGet(&incomplete, &complete);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_setparameter(long, int, int): WARNING - ISRAttributes::TimeOutGet returns %x\n", hres);
			return JNI_FALSE;
		}

		hres = rec->ISRAttributesPtr->TimeOutSet((DWORD) ccval, complete);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_setparameter(long, int, int): WARNING - ISRAttributes::TimeOutSet returns %x\n", hres);
			return JNI_FALSE;
		}
		break;
	default:
		VBX_DEBUG(VBX_print("SapiVise::n_setparameter: Calling SetParameter(%d, %d)\n", index, ccval));
		hres = rec->IVbxEnginePtr->SetParameter(index, ccval);
		if (hres != SRERR_NONE) {
			VBX_print("SapiVise::n_setparameter(long, int, int): WARNING - EngineObj::SetParameter returns %x\n", hres);
			return JNI_FALSE;
		}
	}

	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1setparameter__ILjava_lang_String_2(JNIEnv *env, jobject thiz, jint index, jstring value) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	HRESULT hres;

	if (rec == NULL) {
		VBX_print("SapiVise::n_setparameter: ERROR - rec is NULL\n");
		return JNI_FALSE;
	}

	if (value != NULL) {
		char strbuf[SMAX];
		JavaToUTF8String(env, value, strbuf, SMAX);

		VBX_DEBUG(VBX_print("SapiVise::n_setparameter: Calling EngineObj::SetParameter(%d, \"%s\")\n", index, strbuf));
		hres = rec->IVbxEnginePtr->SetParameter(index, strbuf);
		if (hres == SRERR_NONE) {
			return JNI_TRUE;
		} else {
			VBX_print("SapiVise::n_setparameter: WARNING - EngineObj::SetParameter returns %x\n", hres);
			return JNI_FALSE;
		}
	}

	VBX_print("SapiVise::n_setparameter: WARNING - cannot set parameter value to NULL\n");
	return JNI_FALSE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1settimeout(JNIEnv *env, jobject thiz, jint timeout) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)

	if (rec == NULL) {
		VBX_print("SapiVise::n_settimeout: ERROR - rec is NULL\n");
		return JNI_FALSE;
	}

	VBX_DEBUG(VBX_print("SapiVise::n_settimeout: Calling SetTimeout with value %d\n", timeout));
	HRESULT hres = rec->IVbxEnginePtr->SetTimeout((UInt) timeout);
	if (hres != SRERR_NONE) {
		VBX_DEBUG(VBX_print("SapiVise::n_settimeout: WARNING - EngineObj::SetTimeout returns %x\n", hres));
		return JNI_FALSE;
	}

	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1setutterance(JNIEnv *env, jobject thiz, jstring fullPath) {
	DECLARE_RECOGNIZER_ALIAS(rec, JNI_FALSE)
	HRESULT hres;
	char s[SMAX];

	if (rec == NULL) {
		env->ThrowNew(NullPointerException.clazz, "recognizer is null");
		return JNI_FALSE;
	}

	if (rec->IVbxAudioFilePtr == NULL) {
		env->ThrowNew(UnsupportedOperationException.clazz, "audio source does not support audio file input");
		return JNI_FALSE;
	}

	if (fullPath == NULL) {
		env->ThrowNew(NullPointerException.clazz, "fullPath is null");
		return JNI_FALSE;
	}

	char strbuf[SMAX];
	if (!JavaToUTF8String(env, fullPath, s, SMAX)) {
		env->ThrowNew(IllegalArgumentException.clazz, "fullPath is too long");
		return JNI_FALSE;
	}

	if (hres = rec->IVbxAudioFilePtr->Read(s)) {
		return JNI_FALSE;
	}

	return JNI_TRUE;
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1flush(JNIEnv *env, jobject thiz) {
	DECLARE_RECOGNIZER_ALIAS(rec,)

	if (rec == NULL) {
		VBX_print("SapiVise::n_flush: ERROR - rec is NULL\n");
		return;
	}

	if (rec->IAudioPtr == NULL) {
		VBX_print("SapiVise::n_flush: ERROR - rec->IAudioPtr is NULL\n");
		return;
	}

	rec->IAudioPtr->Flush();
	return;
}

jint JNICALL Java_voxware_engine_recognition_sapivise_SapiVise_n_1getVU(JNIEnv *env, jobject thiz) {
	DECLARE_RECOGNIZER_ALIAS(rec, 0)

	jint ret = (jint) rec->VU;
	return ret;
}
