 // Generic inclusions
#include "android/log.h"
#include "pthr.h"
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include "vbx.h"


#include "voxware_engine_recognition_sapivise_ViseTrainer.h"
#include "vise_jni.h"
#include "cnisapivise.hh"
#include "cnivisetrainer.hh"

#define VBX_DEBUG(P)
#define VBX_print(...) __android_log_print(ANDROID_LOG_ERROR, "com_voxware_vise_engine_recognition_sapivise_ViseTrainer.cpp", __VA_ARGS__)

#define SMAX   256

// These values must match the "static final ints" in ViseTrainer.java
#define DATAFILEERROR     100
#define NOTENOUGHDATA     101
#define VALUEOUTOFRANGE   102

using namespace java::lang;
using voxware::engine::recognition::sapivise::ViseTrainer;
using voxware::engine::recognition::sapivise::ViseEnrollStatus;

typedef struct _ViseTrainer_NP {
	IVbxSpkrTrain *IVbxSpkrTrainPtr;
	IVbxTrain     *IVbxTrainPtr;
	LPUNKNOWN      TrainerPtr;
} ViseTrainer_NP;

#define DECLARE_NATIVE_ALIAS(aliasName, ret) \
jobject result = env->GetObjectField(thiz, ViseTrainer.nativePeer); \
if (!result) { \
	env->ThrowNew(NullPointerException.clazz, "recognizer is null"); \
	return ret; \
}\
ViseTrainer_NP *aliasName = (ViseTrainer_NP *)env->GetDirectBufferAddress(result);

jobject JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1enrollword(JNIEnv *env, jobject thiz) {
	DECLARE_NATIVE_ALIAS(np, NULL)
	// ViseEnrollStatus *
	jobject status = NULL;

	VBX_DEBUG(VBX_print("ViseTrainer::n_enrollword: IVbxTrainPtr @%p", np->IVbxTrainPtr));

	HRESULT hres;
	DWORD count = 0, needed = 0;
	jint rval = SRERR_NONE;
	hres = np->IVbxTrainPtr->EnrollWord(NULL, NULL, &count, &needed);
	VBX_DEBUG(VBX_print("ViseTrainer::n_enrollword: EnrollWord: hres = %x count %d needed %d", hres, count, needed));

	switch (hres) {
        case SRERR_DATAFILEERROR:
            rval = DATAFILEERROR;
            VBX_print("ViseTrainer::n_enrollword: ERROR - unable to write output file");
            break;
        case SRERR_NOTENOUGHDATA:
            rval = NOTENOUGHDATA;
            break;
        case SRERR_VALUEOUTOFRANGE:
            rval = VALUEOUTOFRANGE;
            VBX_print("ViseTrainer::n_enrollword: ERROR - IVbxTrain::EnrollWord fails (VALUEOUTOFRANGE)");
            break;
        default:
            break;
	}

	status = env->NewObject(ViseEnrollStatus.clazz, ViseEnrollStatus.ctor_III, (jint)rval, (jint) count, (jint) needed);

 // if status is NULL, JVM will have OutOfMemoryError setup
//	if (status == NULL) {
//		VBX_print("ViseTrainer::n_enrollword: ERROR - status == NULL");
//		return NULL;
//	}
	return status;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1initialize(JNIEnv *env, jobject thiz, jobject sapiViseObj) {
	ViseTrainer_NP *np = new ViseTrainer_NP();
	CNISapiVise *CNISapiVisePtr = (CNISapiVise *) env->GetDirectBufferAddress(sapiViseObj);

	if (CNISapiVisePtr == NULL || CNISapiVisePtr->ISRCentralPtr == NULL) {
		VBX_print("ViseTrainer::n_initialize: ERROR - CNISapiVise/ISRCentral == NULL");
		return JNI_FALSE;
	}

	// Get the IVbxSpkrTrain interface to get training objects
	HRESULT hres;

	hres = CNISapiVisePtr->ISRCentralPtr->QueryInterface(IID_IVbxSpkrTrain, (PVOID *) &np->IVbxSpkrTrainPtr);
	VBX_DEBUG(VBX_print("ViseTrainer::n_initialize: Query IID_IVbxSpkrTrain: hres %p iptr %p", hres, IVbxSpkrTrainPtr));

	if (FAILED(hres)) {
		VBX_print("ViseTrainer::n_initialize: ERROR - QI for IID_IVbxSpkrTrain FAILS hres = %x", hres);
		env->ThrowNew(ClassCastException.clazz, "CNISapiVise.ISRCentral.QueryInterface for IID_IVbxSpkrTrain failed");
		delete np;
		return JNI_FALSE;
	}

	// Create a Trainer object (this is the namesake of this class)
	hres = ((IVbxSpkrTrain *) np->IVbxSpkrTrainPtr)->GetTrainer((LPUNKNOWN *) &np->TrainerPtr);
	VBX_DEBUG(VBX_print("ViseTrainer::n_initialize: IVbxSpkrTrain::GetTrainer: hres %p ptr %p", hres, TrainerPtr));

	if (hres || np->TrainerPtr == NULL) {
		VBX_print("ViseTrainer::n_initialze: ERROR - could not get Trainer object");
		env->ThrowNew(ClassCastException.clazz, "ViseTrainingObj does not implement IUnknown");
		delete np;
		return JNI_FALSE;
	}

	// Get the IVbxTrain interface from the trainer, which has the enrollment method
	hres = ((LPUNKNOWN) np->TrainerPtr)->QueryInterface(IID_IVbxTrain, (PVOID *) &np->IVbxTrainPtr);
	VBX_DEBUG(VBX_print("ViseTrainer::n_initialize: Query IID_IVbxTrain: hres %p iptr %p", hres, IVbxTrainPtr));

	if (FAILED(hres)) {
		VBX_print("ViseTrainer::n_initialize: ERROR - QI for IID_IVbxTrain FAILS hres = %x", hres);
		env->ThrowNew(ClassCastException.clazz, "ViseTrainingObj does not implement IVbxTrain");
		delete np;
		return JNI_FALSE;
	}

	jobject v = env->NewDirectByteBuffer(np, sizeof(ViseTrainer_NP));
	if (!v) {

		delete np;
		return JNI_FALSE;
	}
	env->SetObjectField(thiz, ViseTrainer.nativePeer, v);
	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1release(JNIEnv *env, jobject thiz) {
	DECLARE_NATIVE_ALIAS(np, FALSE)
	// Release IVbxTrain interface
	if (np->IVbxTrainPtr != NULL) {
		np->IVbxTrainPtr->Release();
		np->IVbxTrainPtr = NULL;
	}

	// Release the Trainer object
	if (np->TrainerPtr != NULL) {
		np-> TrainerPtr->Release();
		np->TrainerPtr = NULL;
	}

	// Release the IVbxSpkrTrain interface
	if (np->IVbxSpkrTrainPtr != NULL) {
		np->IVbxSpkrTrainPtr->Release();
		np->IVbxSpkrTrainPtr = NULL;
	}
	env->SetObjectField(thiz, ViseTrainer.nativePeer, NULL);
	delete np;
	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseTrainer_n_1insertcorrection(JNIEnv *env, jobject thiz, jstring transcription, jobject viseresult){
  VBX_DEBUG(VBX_print("n_insertcorrection: started.."));
  DECLARE_NATIVE_ALIAS(np, JNI_FALSE)
  // First, dig up the LPUNKNOWN for the results object in the given ViseResult
  if (viseresult == NULL) {
    VBX_print("ViseTrainer::n_insertcorrection: ERROR - ViseResult is NULL");
    return FALSE;
  }

  jobject res = env->GetObjectField(viseresult, voxware::engine::recognition::sapivise::ViseResult.result);
  if (res == NULL) {
    VBX_print("ViseTrainer::n_insertcorrection: ERROR - NULL ResultsObject in ViseResult");
    return FALSE;
  }

  if (np->IVbxTrainPtr == NULL) {
    VBX_print("ViseTrainer::n_insertcorrection: ERROR - IVbxTrainPtr == 0");
    return FALSE;
  }

  LPUNKNOWN  resultsPtr = (LPUNKNOWN) env->GetDirectBufferAddress(res);
  HRESULT    hres;

  char utftrans[2*SMAX];
  int  slen;

  if (transcription != NULL && (slen = env->GetStringUTFLength(transcription)) > 0) {
	  if (!JavaToUTF8String(env, transcription, utftrans, 2*SMAX)) {
		  env->ThrowNew(IllegalArgumentException.clazz, "Transcription too long");
		  return JNI_FALSE;
	  }
  } else {
	  return JNI_FALSE;
  }

  VBX_DEBUG(VBX_print("ViseTrainer::n_insertcorrection: transcription is \"%s\"", utftrans));

  SAPIPhrase *sapiPhrasePtr = new SAPIPhrase();
  sapiPhrasePtr->AppendWordString((Char *)utftrans, -1);

  Char *text = sapiPhrasePtr->Text();
  VBX_print("ViseTrainer::n_insertcorrection: sapi phrase contains \"%s\"", text);
  VBX_free(text);

  hres = (np->IVbxTrainPtr)->InsertCorrection(resultsPtr, sapiPhrasePtr->SRPhrase(), 0);
  VBX_DEBUG(VBX_print("ViseTrainer::n_insertcorrection: InsertCorrection CALLED, hres = %x", hres));
  delete sapiPhrasePtr;

  if (hres) {
    VBX_print("ViseTrainer::n_insertcorrection: IVbxTrain::InsertCorrection FAILS hres = %x", hres);
  }

  return hres == SRERR_NONE;
}

