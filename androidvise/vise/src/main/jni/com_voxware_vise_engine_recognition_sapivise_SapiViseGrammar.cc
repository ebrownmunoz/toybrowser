// Generic inclusions
#include "android/log.h"
#include "pthr.h"
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include "vbx.h"
#include <sys/mman.h>

#include "vise_jni.h"
// This is a gcjh hack that should not be needed, but is..
//#define hostTrans__ hostTrans
#include "voxware_engine_recognition_sapivise_SapiViseGrammar.h"

#include "cnisapivise.hh"
#include "cnisapivisegram.hh"

#define  RECOMMENDED     1
#define  REQUIRED        2
#define  SMAX            256
#define  NULL_RULE_NAME  "(NULL)"
#ifdef CNIDEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

using namespace java::lang;
using voxware::engine::recognition::sapivise::SapiViseGrammar;
using voxware::engine::recognition::sapivise::RawResult;

#define DECLARE_GRAMMAR_ALIAS(aliasName, ret) \
jobject grammar = env->GetObjectField(thiz, SapiViseGrammar.grammar); \
if (!grammar) { \
	env->ThrowNew(NullPointerException.clazz, "recognizer is null"); \
	return ret; \
}\
CNISapiViseGram *aliasName = (CNISapiViseGram *) env->GetDirectBufferAddress(grammar);

Char ** jstringArrayToStrArray(JNIEnv *, jobjectArray);
void freeStrArray(Char **);

// Utility function that translates a (Unicode) Java String array into a NULL-terminated array of (UTF-8) C strings
// THE CALLER MUST FREE THE ARRAY THAT IS RETURNED
Char **jstringArrayToStrArray(JNIEnv *env, jobjectArray words) {
	jsize numWords = env->GetArrayLength(words);
	Char ** wordArray = (Char **) VBX_calloc(numWords + 1, sizeof(Char *));
	//Char ** wordArray = (Char **)mmap(NULL, (numWords + 1) * sizeof(Char *), PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
	//if (wordArray != MAP_FAILED) {
	//    VBX_print("mmap %d elements (%d bytes) at %p\n", numWords + 1, (numWords + 1) * sizeof(Char *), wordArray);
	//} else {
	//    VBX_print("Failed to allocated %d elements: %d\n", numWords + 1, errno);
	//    return NULL;
	//}
	Int index;
	for (index = 0; index < numWords; index++) {
		jstring wordName = (jstring)env->GetObjectArrayElement(words, index);
		Int mbsLength = env->GetStringUTFLength(wordName);
		wordArray[index] = (Char *) VBX_calloc(mbsLength + 1, sizeof(Char));
		//VBX_print("Allocated %d elements at %p\n", mbsLength + 1, wordArray[index]);
		env->GetStringUTFRegion(wordName, 0, env->GetStringLength(wordName), wordArray[index]);
		wordArray[index][mbsLength] = '\0';
		env->DeleteLocalRef(wordName);
	}
	// NULL-terminate the array
	wordArray[numWords] = NULL;
	//int ret = mprotect(wordArray, (numWords + 1) * sizeof(Char *), PROT_READ);
	//if (ret != 0) {
	//    VBX_print("Failed to set protection on %p: %d\n", wordArray, ret);
	//} else {
	//    VBX_print("Set protection on %p to PROT_READ\n", wordArray);
	//}
	return wordArray;
}

// Frees the array produced by jstringArrayToStrArray
void freeStrArray(Char **array) {

    if (NULL != array) {
        int index;
        //for (index = 0; NULL != array[index]; index++);
        //VBX_print("Deallocating array at %p\n", array);
        //int ret = mprotect(array, (index + 1) * sizeof(Char *), PROT_READ|PROT_WRITE);
        //if (ret != 0) {
        //    VBX_print("Failed to set protection on %p: %d\n", array, ret);
        //} else {
        //    VBX_print("Set protection on %p to PROT_READ|PROT_WRITE\n", array);
        //}

        for (index = 0; NULL != array[index]; index++) {
       		//VBX_print("Deallocating %p\n", array[index]);
            VBX_free(array[index]);
        }
   		//VBX_print("munmap %p\n", array[index]);

        //munmap(array, (index + 1) * sizeof(Char *));
        VBX_free(array);
	}
}

CNISapiViseGram::CNISapiViseGram(jobject javaGram, class CNISapiVise *rec) :
				Owner(rec),
				JavaGram(javaGram),
				ISRGramCommonPtr(NULL),
				GramUnknwnPtr(NULL),
				IVbxGrammarPtr(NULL),
				ActiveRule { '\0' },
				RefCnt(0) {
//  Owner = rec;
//  JavaGram = javaGram;
//  ISRGramCommonPtr = NULL;
//  GramUnknwnPtr = NULL;
//  IVbxGrammarPtr = NULL;
//  ActiveRule[0] = '\0';        // Indicates that no rule is active
//  RefCnt = 0;
}

CNISapiViseGram::~CNISapiViseGram() {
	VBX_DEBUG(VBX_print("CNISapiViseGram::~CNISapiViseGram CALLED\n"));
}

STDMETHODIMP CNISapiViseGram::QueryInterface(REFIID riid, LPVOID *ppv) {
	*ppv = NULL;

	// Always return our IUnkown for IID_IUnknown, IID_ITTSNotifySink
	if (IsEqualIID (riid, IID_IUnknown)) {
		VBX_DEBUG(VBX_print("CNISapiViseGram::QueryInterface: IUnknown interface queried\n"));
		*ppv = (LPVOID) this;
		return NOERROR;
	}

	if (IsEqualIID (riid, IID_ISRGramNotifySink)) {
		VBX_DEBUG(VBX_print("CNISapiViseGram::QueryInterface: ISRGramNotifySink interface queried\n"));
		*ppv = (LPVOID) this;
		return NOERROR;
	}

	// Otherwise, can't find
	return E_NOINTERFACE;
}

STDMETHODIMP_ (ULONG) CNISapiViseGram::AddRef(void) {
	++RefCnt;
	VBX_DEBUG(VBX_print("  CNISapiViseGram::AddRef(): RefCnt for @%p = %d\n", this, RefCnt));
	return RefCnt;
}

STDMETHODIMP_(ULONG) CNISapiViseGram::Release(void) {
	if (--RefCnt != 0) {
		VBX_DEBUG(VBX_print("  CNISapiViseGram::Release(): RefCnt for @%p = %d\n", this, RefCnt));
		return RefCnt;
	} else {
		VBX_DEBUG(VBX_print("  CNISapiViseGram::Release(): RefCnt for @%p = %d, deleting\n", this, RefCnt));
		if (this->IVbxGrammarPtr != NULL) {
			this->IVbxGrammarPtr->Release();
			this->IVbxGrammarPtr = NULL;
		}
		if (this->ISRGramCommonPtr != NULL) {
			this->ISRGramCommonPtr->Release();
			this->ISRGramCommonPtr = NULL;
		}
		if (this->GramUnknwnPtr != NULL) {
			this->GramUnknwnPtr->Release();
			this->GramUnknwnPtr = NULL;
		}

		delete this;
		VBX_DEBUG(VBX_print("  CNISapiViseGram::Release(): successfully deleted\n"));
		return 0;
	}
}

// ISRGramNotifySink notification methods

STDMETHODIMP CNISapiViseGram::BookMark(DWORD dwBookMarkID) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::Bookmark\n"));
	return NOERROR;
}

STDMETHODIMP CNISapiViseGram::Paused(void) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::Paused\n"));
	return NOERROR;
}

#define VALID_RECOGNITION (ISRNOTEFIN_RECOGNIZED | ISRNOTEFIN_THISGRAMMAR)

STDMETHODIMP CNISapiViseGram::PhraseFinish(DWORD flags, QWORD qTimeStampBegin, QWORD qTimeStampEnd, PSRPHRASE pSRPhrase, LPUNKNOWN results) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::PhraseFinish\n"));
	results->AddRef();

	OBJT obj = OBJT_new(Owner->NotifyClass);
	if (!obj) {
		VBX_print("CNISapiViseGram::PhraseFinish: FATAL ERROR obj == NULL\n");
		return NOERROR;
	}

	NotifyMsg *nmPtr = (NotifyMsg *) &OBJT_contents(obj);
	nmPtr->flags = flags;
	nmPtr->gramPtr = this;
	nmPtr->results = results;
	MBOX_insert(Owner->NotifyMbox, obj);
}

void CNISapiViseGram::ProcessNotify(JNIEnv *env, OBJT obj, jobject rawresult) {
	NotifyMsg * nmPtr = (NotifyMsg *) &OBJT_contents(obj);
	DWORD flags = nmPtr->flags;
	Bool accepted = ((flags & VALID_RECOGNITION) == VALID_RECOGNITION);
	LPUNKNOWN results = nmPtr->results;
	PSAPIPhrase sapiPhrasePtr = NULL;
	Int firstResultLen = 0;

	VBX_DEBUG(VBX_print("CNISapiViseGram::ProcessNotify 0\n"));

	// N-Best implementation
	ISRResBasic * ISRResBasicPtr = NULL;
	PSRPHRASE srPhrasePtr = NULL;
	ULong needed;
	HRESULT hres;

	if (hres = results->QueryInterface(IID_ISRResBasic, (PVOID *) &ISRResBasicPtr)) {
		VBX_print("CNISapiViseGram::ProcessNotify ERROR: QI for ISRResBasic FAILS\n");
		results->Release();
		return;
	}

	srPhrasePtr = (PSRPHRASE) VBX_calloc(SMAX, sizeof(Char));

	// First, count how many NBest were returned
	UInt nbestCnt = 0, rank = 0;
	while (SRERR_NONE == ISRResBasicPtr->PhraseGet(nbestCnt, srPhrasePtr, (UInt) SMAX, &needed) || needed)
		nbestCnt++;
	VBX_DEBUG(VBX_print("CNISapiViseGram::ProcessNotify has %d NBest to return\n", nbestCnt));

	// If we have made it this far, we really have to return some result (even if blank)
	// or the higher layers will throw exceptions.  Limit nbestCnt to a minimum of 1
	if (nbestCnt == 0) {
		nbestCnt = 1;
		VBX_DEBUG(VBX_print("CNISapiViseGram::ProcessNotify will return ONE blank result\n"));
	}

	// Now create a jstring array and populate it with NBest results
	//jstringArray nbestArray = (jstringArray) JvNewObjectArray(nbestCnt, (new java::lang::String())->getClass(), NULL);
	jobjectArray nbestArray = env->NewObjectArray(nbestCnt, String.clazz, NULL);

	if (nbestArray == NULL) {
		VBX_print("CNISapiViseGram::ProcessNotify:  ERROR - NULL result for array allocation\n");
		results->Release();
		return;
	}

	Char *firstBestText = NULL;
	//jstring *nbestArrayPtr = env->Getelements(nbestArray);
	for (rank = 0; rank < nbestCnt; rank++) {
		Char * sapiText = NULL;
		jstring resultStr = NULL;
		hres = ISRResBasicPtr->PhraseGet(rank, srPhrasePtr, (UInt) SMAX, &needed);

		// Accept only (1) no error OR (2) insufficient SRPhrase allocation
		if (hres == SRERR_NONE || (hres == SRERR_INVALIDPARAM && needed)) {
			if (needed > srPhrasePtr->dwSize) {
				VBX_free(srPhrasePtr);
				srPhrasePtr = (PSRPHRASE) VBX_calloc(needed, sizeof(Char));
				hres = ISRResBasicPtr->PhraseGet(rank, srPhrasePtr, (UInt) needed, &needed);
				assert(!FAILED(hres));
			}
			sapiPhrasePtr = new SAPIPhrase(srPhrasePtr);
			sapiText = sapiPhrasePtr->Text();
			resultStr = env->NewStringUTF(sapiText);
			env->SetObjectArrayElement(nbestArray, rank, resultStr);
			delete sapiPhrasePtr;
		} else {
			// Put SOMETHING in so that higher layers don't throw exceptions
			sapiText = (Char *) VBX_calloc(10, sizeof(Char));
			sprintf(sapiText, " ");
			resultStr = env->NewStringUTF(" ");
			env->SetObjectArrayElement(nbestArray, rank, resultStr);
		}
		if (rank == 0) {
			firstBestText = sapiText;
		} else {
			VBX_free(sapiText);
		}
	}
	VBX_free(srPhrasePtr);

	jstring hostTrans = NULL;
	if (accepted) {
		ISRResTranslate * ISRResTranslatePtr = NULL;
		if (hres = results->QueryInterface(IID_ISRResTranslate, (PVOID *) &ISRResTranslatePtr)) {
			VBX_print("CNISapiViseGram::ProcessNotify ERROR: QueryInterface for ISRResTranslate FAILS; no host translation provided\n");
		} else {
			Char buf[SMAX], *bufPtr = NULL;
			DWORD sizeNeeded = 0;
			Bool bufNeedsFree = FALSE;

			// If the stack allocation is big enough, use it.  Otherwise, allocate something from the heap according to the sizeNeeded
			bufPtr = buf;
			*bufPtr = '\0';
			if (strlen(firstBestText) != 0 && !((strlen(firstBestText) == 1) && firstBestText[0] == ' ')) {
				if ((hres = ISRResTranslatePtr->Translate(SRRESTRKIND_HOST, 0, bufPtr, SMAX, &sizeNeeded)) != SRERR_NONE) {
					if (sizeNeeded > SMAX) {
						bufPtr = (Char *) VBX_calloc(1, sizeNeeded + 16);
						hres = ISRResTranslatePtr->Translate(SRRESTRKIND_HOST, 0, bufPtr, sizeNeeded + 16, &sizeNeeded);
						if (FAILED(hres)) {
							VBX_print("CNISapiViseGram::ProcessNotify ERROR: could not get SRRESTRKIND_HOST response, hres = %x\n", hres);
							VBX_free(bufPtr);
							bufPtr = NULL;
						} else {
							bufNeedsFree = TRUE;
						}
					}
				}

				// If bufPtr is non-NULL, there should be a host response we can use...
				if (bufPtr != NULL) {
					hostTrans = env->NewStringUTF(bufPtr);
					if (bufNeedsFree)
						VBX_free(bufPtr);
				}
			} else {
				hostTrans = env->NewStringUTF(" ");
			}
			ISRResTranslatePtr->Release();
			ISRResTranslatePtr = NULL;
		}
	}
	VBX_free(firstBestText);

	// Find out if training is enabled
	jboolean training = env->CallBooleanMethod(JavaGram, SapiViseGrammar.isTrainingProvided);
	env->SetBooleanField(rawresult, RawResult.training, training);
	VBX_DEBUG(VBX_print("CNISapiViseGram::ProcessNotify training is %s\n", training ? "TRUE" : "FALSE"));

	// Find out if resultAudio is enabled
	jboolean audio = env->CallBooleanMethod(JavaGram, SapiViseGrammar.isResultAudioProvided);
	env->SetBooleanField(rawresult, RawResult.audio, audio);

	// Reduce the reference count on the results object to one
	ISRResBasicPtr->Release();
	VBX_DEBUG(VBX_print("CNISapiViseGram::ProcessNotify, setting RawResult fields..\n"));
	jobject res;
	if (training || audio) {
		// If both training and resultAudio are enabled, add back a second reference
		if (training && audio) {
			results->AddRef();
		}
		res = env->NewDirectByteBuffer(results, sizeof(LPUNKNOWN));
	} else {
		// Neither training nor resultAudio are enabled, so release the final reference
		res = NULL;
		results->Release();
	}

	env->SetBooleanField(rawresult, RawResult.accepted, accepted);
	env->SetObjectField(rawresult, RawResult.result, res);

	jstring grammarName;
	Char nameBuf[SMAX];
	DWORD bytesNeeded;

	nameBuf[0] = '\0';
	hres = IVbxGrammarPtr->QueryGrammarName(nameBuf, (DWORD) SMAX, &bytesNeeded);

	if (hres || !strlen(nameBuf))
		strcpy(nameBuf, "NONAME");
	grammarName = env->NewStringUTF(nameBuf);

	env->SetObjectField(rawresult, RawResult.grammarName, grammarName);
	env->SetObjectField(rawresult, RawResult.nbestArray, nbestArray);
	env->SetObjectField(rawresult, RawResult.hostTrans, hostTrans);
	//rawresult->grammarName = grammarName;
	//rawresult->nbestArray = nbestArray;
	//rawresult->hostTrans = hostTrans;

	VBX_print("CNISapiViseGram::ProcessNotify: RESULT %s\n", (accepted ? "ACCEPTED" : "REJECTED"));
	return;
}

STDMETHODIMP CNISapiViseGram::PhraseHypothesis(DWORD dwFlags, QWORD qTimeStampBegin, QWORD qTimeStampEnd,
PSRPHRASE pSRPhrase, LPUNKNOWN pIUnknownResult) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::PhraseHypothesis\n"));
	return NOERROR;
}

STDMETHODIMP CNISapiViseGram::PhraseStart(QWORD qTimeStampBegin) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::PhraseStart\n"));
	return NOERROR;
}

STDMETHODIMP CNISapiViseGram::ReEvaluate(LPUNKNOWN pIUnknown) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::ReEvaluate\n"));
	return NOERROR;
}

STDMETHODIMP CNISapiViseGram::Training(DWORD dwTrain) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::Training\n"));
	return NOERROR;
}

STDMETHODIMP CNISapiViseGram::UnArchive(LPUNKNOWN pIUnknown) {
	VBX_DEBUG(VBX_print("CNISapiViseGram::UnArchive\n"));
	return NOERROR;
}

jobject JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_newGrammar(JNIEnv *env, jobject thiz, jobject sapivise) {
	CNISapiViseGram *gram;
	if (sapivise == NULL) {
		env->ThrowNew(NullPointerException.clazz, "recognizer is null");
		return NULL;
	}
	jobject globalThiz = env->NewGlobalRef(thiz);
	if (!globalThiz) {
		// we ran out of memory?
		if (!env->ExceptionOccurred()) {
			env->ThrowNew(OutOfMemoryError.clazz, "Failed to create global reference to SapiViseGrammar");
		}
		return NULL;
	}

	gram = new CNISapiViseGram(globalThiz, (CNISapiVise *) env->GetDirectBufferAddress(sapivise));
	gram->AddRef();
	return env->NewDirectByteBuffer(gram, sizeof(CNISapiViseGram));
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1load(JNIEnv *env, jobject thiz, jbyteArray recImage) {
	VBX_print("SapiViseGrammar::n_load: invoked\n");
	DECLARE_GRAMMAR_ALIAS(gram,)
	VBX_print("SapiViseGrammar::n_load: after DECLARE_GRAMMAR_ALIAS\n");
	HRESULT hres;
	SDATA natgram;

	// Initial sanity checks
	if (gram == NULL) {
		VBX_print("SapiViseGrammar::n_load: ERROR - gram == NULL\n");
		return;
	}

	if (gram->Owner == NULL || gram->Owner->ISRCentralPtr == NULL) {
		VBX_print("SapiViseGrammar::n_load: ERROR - Owner or ISRCentralPtr == NULL\n");
		return;
	}

	natgram.dwSize = env->GetArrayLength(recImage);
	natgram.pData = (PVOID) VBX_calloc(1, natgram.dwSize);

	if (natgram.dwSize == 0 || natgram.pData == NULL) {
		VBX_print("SapiViseGrammar::n_load: ERROR - natgram SDATA is bad\n");
		return;
	}

	VBX_print("SapiViseGrammar::n_load: about to copy %d bytes to %p\n", natgram.dwSize, natgram.pData);
	env->GetByteArrayRegion(recImage, 0, (jsize) natgram.dwSize, (signed char *) natgram.pData);
	VBX_print("SapiViseGrammar::n_load: copied %d bytes to %p\n", natgram.dwSize, natgram.pData);

	// Release any extant natgram interface pointers from any prior load
	if (gram->IVbxGrammarPtr != NULL) {
		gram->IVbxGrammarPtr->Release();
		gram->IVbxGrammarPtr = NULL;
	}
	if (gram->ISRGramCommonPtr != NULL) {
		gram->ISRGramCommonPtr->Release();
		gram->ISRGramCommonPtr = NULL;
	}
	if (gram->GramUnknwnPtr != NULL) {
		gram->GramUnknwnPtr->Release();
		gram->GramUnknwnPtr = NULL;
	}

	VBX_print("SapiViseGrammar::n_load: loading %d bytes from %p\n", natgram.dwSize, natgram.pData);

	hres = gram->Owner->ISRCentralPtr->GrammarLoad(SRGRMFMT_CFGNATIVE, natgram, gram, IID_ISRGramNotifySink, &(gram->GramUnknwnPtr));
	VBX_print("SapiViseGrammar::n_load: Query IID_ISRGramCommon: hres %x iptr %p\n", hres, gram->GramUnknwnPtr);

	// Get the ISRGramCommon interface to the grammar object
	hres = gram->GramUnknwnPtr->QueryInterface(IID_ISRGramCommon, (PVOID *) &(gram->ISRGramCommonPtr));
	VBX_DEBUG(VBX_print("SapiViseGrammar::n_load: Query IID_ISRGramCommon: hres %x iptr %p\n", hres, gram->ISRGramCommonPtr));

	// Get the IVbxGrammar interface to the grammar object
	hres = gram->GramUnknwnPtr->QueryInterface(IID_IVbxGrammar, (PVOID *) &(gram->IVbxGrammarPtr));
	VBX_DEBUG(VBX_print("SapiViseGrammar::n_load: Query IID_IVbxGrammar: hres %x iptr %p\n", hres, gram->IVbxGrammarPtr));VBX_DEBUG(VBX_print("SapiViseGrammar::n_load: load complete, hres = %x\n", hres));
	VBX_free(natgram.pData);
}

jint JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1release(JNIEnv *env, jobject thiz) {
	DECLARE_GRAMMAR_ALIAS(gram, 0)

	if (gram == NULL) {
		VBX_DEBUG(VBX_print("SapiViseGrammar::n_release: ERROR gram == NULL\n"));
		return 0;
	}

	gram->IVbxGrammarPtr->RemoveNotifySink();  // Force the VISEGrammar object to relinquish its reference

	env->DeleteGlobalRef(gram->JavaGram);
	gram->JavaGram = NULL;
	ULong refCount = gram->Release();          // Relinquish the self reference

	VBX_DEBUG(VBX_print("SapiViseGrammar::n_release: refCount for CNISapiViseGram @%p reduced to %ul\n", gram, refCount));

	return (jint) refCount;
}

void JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1setactive(JNIEnv *env, jobject thiz, jstring name, jboolean act) {
	DECLARE_GRAMMAR_ALIAS(gram,)
	if (gram == NULL) {
		VBX_DEBUG(VBX_print("SapiViseGrammar::n_setactive: ERROR gram == NULL\n"));
		return;
	}

	if (gram->Owner->ISRCentralPtr != NULL) {
		// Get rule name from "name" argument
		int slen;
		char utfname[SMAX];
		if (name != NULL && (slen = env->GetStringUTFLength(name)) > 0) {
			//JvGetStringUTFRegion(name, 0, name->length(), utfname);
			JavaToUTF8String(env, name, utfname, SMAX);
			if (strlen(utfname) > 0) {
				VBX_DEBUG(VBX_print("SapiViseGrammar::n_setactive %s %d\n", utfname, act));

				const char * ruleName = strcmp(utfname, NULL_RULE_NAME) ? utfname : NULL;

				if (act) {
					// If there is currently an active rule, deactivate it
					VBX_print("Deactivating active rule\n");
					if (gram->ActiveRule[0]) {
						if (FAILED(gram->ISRGramCommonPtr->Deactivate(strcmp(gram->ActiveRule, NULL_RULE_NAME) ? gram->ActiveRule : NULL))) {
							VBX_DEBUG(VBX_print("SapiViseGrammar::n_setactive: ERROR - Deactivate FAILED for rule \"%s\"\n", gram->ActiveRule));
						} else {
							gram->ActiveRule[0] = '\0';
						}
					}
					VBX_print("Clearing autopause\n");
					// Clear autopause
					gram->Owner->ISRCentralPtr->Resume();
					VBX_print("Activating new rule with autopause enabled");
					// Activate the new rule with autopause enabled
					HRESULT hres = gram->ISRGramCommonPtr->Activate(NULL, TRUE, ruleName);
					if (FAILED(hres) && hres != SRERR_RULEALREADYACTIVE) {
						VBX_DEBUG(VBX_print("SapiViseGrammar::n_setactive: ERROR - Activate FAILED for rule \"%s\"\n", utfname));
					} else {
						VBX_DEBUG(VBX_print("SapiViseGrammar::n_setactive: ISRGramCommon::Activate CALLED for rule %s\n", utfname));
#ifdef NISTAUDIO
						gram->Owner->IAudioPtr->Flush();
						gram->Owner->IVbxAudioFilePtr->Read("/vlsroot/vxml/digits31.wav");
#endif
						strcpy(gram->ActiveRule, utfname);
					}
				} else {
					HRESULT hres = gram->ISRGramCommonPtr->Deactivate(ruleName);
					if (FAILED(hres) && hres != SRERR_RULENOTACTIVE) {
						VBX_DEBUG(VBX_print("SapiViseGrammar::n_setactive: Deactivate failed for rule \"%s\"\n", utfname));
					} else {
						gram->ActiveRule[0] = '\0';
					}
				}
			} else {
				VBX_print("SapiViseGrammar::n_setactive: ERROR - can't get string for name argument\n");
			}
		} else {
			VBX_print("SapiViseGrammar::n_setactive: NULL or empty name, giving up\n");
		}
	}
}

jstring JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1grammarname(JNIEnv *env, jobject thiz) {
	DECLARE_GRAMMAR_ALIAS(gram, NULL);
	jstring grammarName = NULL;
	if (gram == NULL || gram->IVbxGrammarPtr == NULL) {
		VBX_print("SapiViseGrammar::n_grammarname: ERROR - gram/IVbxGrammarPtr == NULL\n");
	} else {
		Char nameBuf[SMAX];
		nameBuf[0] = '\0';
		DWORD bytesNeeded;
		HRESULT hres = gram->IVbxGrammarPtr->QueryGrammarName(nameBuf, (DWORD) SMAX, &bytesNeeded);
		if (hres || strlen(nameBuf) == 0)
			strcpy(nameBuf, "NONAME");
		grammarName = env->NewStringUTF(nameBuf);
	}

	return grammarName;
}

jstring JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1defaultrulename(JNIEnv *env, jobject thiz) {
	DECLARE_GRAMMAR_ALIAS(gram, NULL)
	jstring ruleName = NULL;

	if (gram == NULL || gram->IVbxGrammarPtr == NULL) {
		VBX_print("SapiViseGrammar::n_defaultrulename: ERROR - gram/IVbxGrammarPtr == NULL\n");
	} else {
		Char nameBuf[SMAX];
		nameBuf[0] = '\0';
		DWORD bytesNeeded;
		HRESULT hres = gram->IVbxGrammarPtr->QueryDefaultRuleName(nameBuf, (DWORD) SMAX, &bytesNeeded);
		if (hres || strlen(nameBuf) == 0)
			VBX_print("SapiViseGrammar::n_defaultrulename: ERROR - cannot acquire default rule name\n");
		else
			ruleName = env->NewStringUTF(nameBuf);
	}

	return ruleName;
}

jobjectArray JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1parse(JNIEnv *env, jobject thiz, jstring phrase) {
	jobjectArray ruleNamesArray;
	int i, ruleCnt;

	DECLARE_GRAMMAR_ALIAS(gram, NULL)
	Char * ruleNamesBuf, *entryPtr;

	// Get the phrase to parse from the CNI argument "phrase" and parse it
	Char utfphrase[2 * SMAX];
	int slen;
	if (phrase != NULL && (slen = env->GetStringUTFLength(phrase)) > 0) {
		env->GetStringUTFRegion(phrase, 0, env->GetStringUTFLength(phrase), utfphrase);
		utfphrase[slen] = '\0';
		if (strlen(utfphrase) > 0) {
			DWORD ruleBytes;
			HRESULT hres = gram->IVbxGrammarPtr->Parse(NULL, utfphrase, &ruleNamesBuf, &ruleBytes);
			if (hres || !ruleNamesBuf || !ruleBytes) {
				VBX_print("SapiViseGrammar::n_parse:  ERROR - IVbxGrammar::Parse failed, hres = %x\n", hres);
				return NULL;
			}
		} else {
			VBX_print("SapiViseGrammar::n_parse: ERROR - can't get string for phrase argument\n");
			return NULL;
		}
	} else {
		VBX_print("SapiViseGrammar::n_parse: NULL or empty phrase, giving up\n");
		return NULL;
	}

	// Count the number of rule name entries first, and get the length of the longest rule name
	entryPtr = ruleNamesBuf;
	ruleCnt = 0;
	while (*entryPtr != '\0') {
		entryPtr += strlen(entryPtr) + 1;
		ruleCnt++;
	}

	ruleNamesArray = env->NewObjectArray(ruleCnt, java::lang::String.clazz, NULL);
	if (ruleNamesArray == NULL) {
		VBX_print("SapiViseGrammar::n_parse: ERROR - can't allocate jstringArray for rule names\n");
		return NULL;
	}

	// Now populate the Java string array with the rule names
	entryPtr = ruleNamesBuf;
	for (i = 0; i < ruleCnt; i++) {
		VBX_DEBUG(VBX_print("SapiViseGrammar::n_parse: rule %d: \"%s\"\n", i, entryPtr));
		jstring ruleName = env->NewStringUTF(entryPtr);
		env->SetObjectArrayElement(ruleNamesArray, i, ruleName);
		entryPtr += strlen(entryPtr) + 1;
	}

	VBX_free(ruleNamesBuf);
	return ruleNamesArray;
}

jobjectArray JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1rulenames(JNIEnv *env, jobject thiz) {
	jobjectArray ruleNamesArray;
	int i, ruleCnt;

	DECLARE_GRAMMAR_ALIAS(gram, NULL)

	HRESULT hres;
	Char *ruleNamesBuf, *entryPtr;
	DWORD ruleBytes;

	hres = gram->IVbxGrammarPtr->QueryRuleNames(&ruleNamesBuf, &ruleBytes);

	if (hres || !ruleNamesBuf || !ruleBytes) {
		VBX_print("SapiViseGrammar.n_rulenames:  ERROR - IVbxGrammar::IVbxQueryRuleNames failed\n");
		return NULL;
	}

	// Count the number of rule name entries first, and get the length of the longest rule name
	entryPtr = ruleNamesBuf;
	ruleCnt = 0;
	while (*entryPtr != '\0') {
		entryPtr += strlen(entryPtr) + 1;
		ruleCnt++;
	}

	// Allocate a Java array for the rule names
	ruleNamesArray = env->NewObjectArray(ruleCnt, java::lang::String.clazz, NULL);
	if (ruleNamesArray == NULL) {
		VBX_print("SapiViseGrammar::n_rulenames:  ERROR - NULL result for array allocation\n");
		return NULL;
	}

	// Now populate the Java string array with the rule names
	entryPtr = ruleNamesBuf;
	//jstring *ruleNamesArrayPtr = elements(ruleNamesArray);
	for (i = 0; i < ruleCnt; i++) {
		VBX_DEBUG(VBX_print("SapiViseGrammar::n_rulenames: rule %d: \"%s\"\n", i, entryPtr));
		jstring ruleName = env->NewStringUTF(entryPtr);
		env->SetObjectArrayElement(ruleNamesArray, i, ruleName);
		entryPtr += strlen(entryPtr) + 1;
	}

	VBX_free(ruleNamesBuf);
	return ruleNamesArray;
}

jobjectArray JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1trainphrase(JNIEnv *env, jobject thiz, jint extent) {
	jobjectArray promptsArray;
	int i;

	DECLARE_GRAMMAR_ALIAS(gram, NULL)

	if (gram == NULL) {
		VBX_print("SapiViseGrammar::n_trainphrase: ERROR - gram == NULL\n");
		return NULL;
	}

	// Obtain the list of words that need training/enrollment.  This is a list of SRPHRASEs, each containing a single word
	HRESULT hres;
	SDATA prompts;
	Int promptsRemaining = TRUE;

	switch (extent) {
	case RECOMMENDED:
		hres = gram->ISRGramCommonPtr->TrainPhrase(SRTQEX_RECOMMENDED, &prompts);
		break;
	case REQUIRED:
		hres = gram->ISRGramCommonPtr->TrainPhrase(SRTQEX_REQUIRED, &prompts);
		break;
	default:
		VBX_print("SapiViseGrammar::n_trainphrase ERROR: unknown/unsupported extent %d\n", extent);
		return NULL;
	}

	if (hres) {
		VBX_print("SapiViseGrammar::n_trainphrase: ERROR ISRGramCommon::TrainPhrase returns %x\n", hres);
		return NULL;
	}

	DWORD bytesRemaining = prompts.dwSize;
	UChar * phrasePtr = (UChar *) prompts.pData;
	PSAPIPhrase promptPhrasePtr;
	Char * promptText = NULL;

	if (bytesRemaining == 0 || prompts.pData == NULL) {
		VBX_print("SapiViseGrammar::n_trainphrase: INFO - There are no words that require %s\n", (extent == RECOMMENDED) ? "training" : "enrollment");
		return NULL;
	}

	// Count how many enrollment/training phrases there are, and find the maximum phrase length
	Int promptCnt = 0;

	while (bytesRemaining) {
		promptPhrasePtr = new SAPIPhrase((PSRPHRASE) phrasePtr);
		Int phraseLength = promptPhrasePtr->SRPhraseSize();
		phrasePtr += phraseLength;
		bytesRemaining -= phraseLength;
		delete promptPhrasePtr;
		promptCnt++;
	}VBX_DEBUG(VBX_print("SapiViseGrammar::n_trainphrase: %d training phrases\n", promptCnt));

	// Allocate a Java array for the prompt Strings
	promptsArray = env->NewObjectArray(promptCnt, java::lang::String.clazz, NULL);
	if (promptsArray == NULL) {
		VBX_print("SapiViseGrammar::n_trainphrase: ERROR - NULL result for array allocation\n");
		return NULL;
	}

	// Now populate the Java string array with the prompts
	i = 0;
	bytesRemaining = prompts.dwSize;
	phrasePtr = (UChar *) prompts.pData;
	while (bytesRemaining) {
		promptPhrasePtr = new SAPIPhrase((PSRPHRASE) phrasePtr);
		Int phraseLength = promptPhrasePtr->SRPhraseSize();
		promptText = promptPhrasePtr->Text();
		VBX_DEBUG(VBX_print("SapiViseGrammar::n_trainphrase: prompt %d: \"%s\"\n", i, promptText ? promptText : ""));
		if (promptText == NULL)
			VBX_print("SapiViseGrammar::n_trainphrase: WARNING - NULL promptText\n");
		jstring prompt = env->NewStringUTF(promptText ? promptText : "");
		env->SetObjectArrayElement(promptsArray, i, prompt);
		phrasePtr += phraseLength;
		bytesRemaining -= phraseLength;
		delete promptPhrasePtr;
		env->DeleteLocalRef(prompt);
		VBX_free(promptText);
		i++;
	}

	VBX_free(prompts.pData);
	return promptsArray;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1startpass(JNIEnv *env, jobject thiz) {
	HRESULT hres;
	DECLARE_GRAMMAR_ALIAS(gram, JNI_FALSE)

	if (gram == NULL || gram->IVbxGrammarPtr == NULL) {
		VBX_print("SapiViseGrammar::n_startpass: ERROR - gram/IVbxGrammarPtr == NULL\n");
		return JNI_FALSE;
	}

	if (hres = gram->IVbxGrammarPtr->StartTrainingPass(NULL)) {
		VBX_print("SapiViseGrammar::n_startpass: ERROR: IVbxGramma::StartTrainingPass returned %d\n", hres);
		return JNI_FALSE;
	}

	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1endpass(JNIEnv *env, jobject thiz, jobjectArray words, jboolean final) {
	HRESULT hres;
	DECLARE_GRAMMAR_ALIAS(gram, JNI_FALSE)
	Bool finalPass = (final) ? TRUE : FALSE;

	if (gram == NULL || gram->IVbxGrammarPtr == NULL) {
		VBX_print("SapiViseGrammar::n_endpass: ERROR - gram/IVbxGrammarPtr == NULL\n");
		return JNI_FALSE;
	}

	Char ** wordArray = NULL;
	if (words != NULL)
		wordArray = jstringArrayToStrArray(env, words);

	if (hres = gram->IVbxGrammarPtr->EndTrainingPass(NULL, wordArray, finalPass)) {
		VBX_print("SapiViseGrammar::n_endpass: ERROR: IVbxGramma::EndTrainingPass returned %d\n", hres);
		if (wordArray) {
			freeStrArray(wordArray);
		}
		return JNI_FALSE;
	}

	VBX_print("SapiViseGrammar::n_endpass: IVbxGramma::EndTrainingPass returned %d\n", hres);

	if (wordArray) {
	    VBX_print("SapiViseGrammar::n_endpass: Deallocating wordArray\n");
		freeStrArray(wordArray);
	}
	return JNI_TRUE;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_SapiViseGrammar_n_1savemodels(JNIEnv *env, jobject thiz, jobjectArray words, jint minTrainingCount) {
	HRESULT hres;
	DECLARE_GRAMMAR_ALIAS(gram, JNI_FALSE);

	if (gram == NULL || gram->IVbxGrammarPtr == NULL) {
		VBX_print("SapiViseGrammar::n_savemodels: ERROR - gram/IVbxGrammarPtr == NULL\n");
		return JNI_FALSE;
	}

	Char ** wordArray = NULL;
	if (words != NULL)
		wordArray = jstringArrayToStrArray(env, words);

	if (hres = gram->IVbxGrammarPtr->SaveModels(NULL, wordArray, (Int) minTrainingCount)) {
		VBX_print("SapiViseGrammer::n_savemodels: ERROR: IVbxGrammar::SaveModels returned %d\n", hres);
		if (wordArray) {
			freeStrArray(wordArray);
		}
		return JNI_FALSE;
	}

	if (wordArray) {
		freeStrArray(wordArray);
	}
	return JNI_TRUE;
}
