#include <stdlib.h>
#include <jni.h>
#include <android/log.h>
#include "vbx.h"
#include <vise_jni.h>
#include "voxware_engine_recognition_sapivise_SapiVise.h"
#include "voxware_engine_recognition_sapivise_SapiViseGrammar.h"
#include "voxware_engine_recognition_sapivise_ViseResult.h"
#include "voxware_engine_recognition_sapivise_ViseTrainer.h"
#include "com_voxware_vise_NativeLogSource.h"

namespace java {
	namespace lang {
		struct _ClassCastException ClassCastException;
		struct _IllegalArgumentException IllegalArgumentException;
		struct _InterruptedException InterruptedException;
		struct _NullPointerException NullPointerException;
		struct _OutOfMemoryError OutOfMemoryError;
		struct _RuntimeException RuntimeException;
		struct _String String;
		struct _Thread Thread;
		struct _UnsupportedOperationException UnsupportedOperationException;
	}
	namespace nio {
        struct _ByteBuffer ByteBuffer;
	}
    namespace util {
        struct _List List;
        struct _ArrayList ArrayList;
    }
}

namespace com {
	namespace voxware {
		namespace vise {
			struct _AndroidAudioSource AndroidAudioSource;
		}
	}
}

namespace voxware {
	namespace engine {
		namespace recognition {
			namespace sapivise {
				struct _RawResult RawResult;
				struct _SapiVise SapiVise;
				struct _SapiViseGrammar SapiViseGrammar;
				struct _ViseEnrollStatus ViseEnrollStatus;
				struct _ViseResult ViseResult;
				struct _ViseTrainer ViseTrainer;
			}
		}
	}
}

bool findClass(JNIEnv *env, jclass* clazz, const char *className) {
    // this should be true
    jclass lclazz = env->FindClass(className);
    if (!lclazz) {
    	__android_log_print(ANDROID_LOG_FATAL, "VISE.cpp", "Failed to find class named %s", className);
        return false;
    }
    jclass ret = reinterpret_cast<jclass>(env->NewGlobalRef(lclazz));
    if (ret) {
        *clazz = ret;
        return true;
    } else {
    	__android_log_print(ANDROID_LOG_FATAL, "VISE.cpp", "Failed to make global ref to class named %s", className);
        return false;
    }
}

bool findMethod(JNIEnv *env, jmethodID *method, jclass clazz, const char *name, const char *signature) {
    jmethodID _method = env->GetMethodID(clazz, name, signature);
    if (_method) {
        *method = _method;
        return true;
    } else {
    	__android_log_print(ANDROID_LOG_FATAL, "VISE.cpp", "Failed to find method named %s with signature %s", name, signature);
        return false;
    }
}

bool findStaticMethod(JNIEnv *env, jmethodID *method, jclass clazz, const char *name, const char *signature) {
    jmethodID _method = env->GetStaticMethodID(clazz, name, signature);
    if (_method) {
        *method = _method;
        return true;
    } else {
    	__android_log_print(ANDROID_LOG_FATAL, "VISE.cpp", "Failed to find static method named %s with signature %s", name, signature);
        return false;
    }
}

bool findField(JNIEnv *env, jfieldID *field, jclass clazz, const char *name, const char *signature) {
	jfieldID _field = env->GetFieldID(clazz, name, signature);
	if (_field) {
		*field = _field;
		return true;
	} else {
		__android_log_print(ANDROID_LOG_FATAL, "VISE.cpp", "Failed to find field named %s with signature %s", name, signature);
		return false;
	}
}

#define failOnFalse(e) do {if (!e) { __android_log_print(ANDROID_LOG_FATAL, "VISE.cpp", "Failure on line %d", __LINE__); return JNI_ERR;}} while(0)

jboolean JavaToUTF8String(JNIEnv *env, jstring src, char *dest, const size_t destLen) {
	memset(dest, 0, destLen);
	int byteLen = env->GetStringUTFLength(src);
	// need room for the terminal '\0'
	if (byteLen >= (destLen - 1)) {
		return JNI_FALSE;
	}
	env->GetStringUTFRegion(src, 0, env->GetStringLength(src), dest);
	dest[(int) byteLen] = '\0';
	return JNI_TRUE;
}

void vbx_android_log_bridge(const char * string) {
	__android_log_print(ANDROID_LOG_INFO, "voxware", "%s", string);
}

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv* env;
    __android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "JNI_OnLoad called");
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

	VBX_setPrintFcn(vbx_com_voxware_vise_NativeLogSource_bridge);

    {
    	using namespace java::lang;
        failOnFalse(findClass(env, &ClassCastException.clazz, "java/lang/ClassCastException"));
        failOnFalse(findClass(env, &IllegalArgumentException.clazz, "java/lang/IllegalArgumentException"));
        failOnFalse(findClass(env, &InterruptedException.clazz, "java/lang/InterruptedException"));
        failOnFalse(findClass(env, &NullPointerException.clazz, "java/lang/NullPointerException"));
        failOnFalse(findClass(env, &RuntimeException.clazz, "java/lang/RuntimeException"));
        failOnFalse(findClass(env, &UnsupportedOperationException.clazz, "java/lang/UnsupportedOperationException"));
        failOnFalse(findClass(env, &String.clazz, "java/lang/String"));
        failOnFalse(findClass(env, &Thread.clazz, "java/lang/Thread"));
        failOnFalse(findStaticMethod(env, &Thread.currentThread, Thread.clazz, "currentThread", "()Ljava/lang/Thread;"));
        failOnFalse(findMethod(env, &Thread.isInterrupted, Thread.clazz, "isInterrupted", "()Z"));
        __android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached java::lang classes");
    }

    failOnFalse(findClass(env, &com::voxware::vise::AndroidAudioSource.clazz, "com/voxware/vise/AndroidAudioSource"));

    {
    	using java::nio::ByteBuffer;
    	failOnFalse(findClass(env, &ByteBuffer.clazz, "java/nio/ByteBuffer"));
    	failOnFalse(findMethod(env, &ByteBuffer.asReadOnlyBuffer, ByteBuffer.clazz, "asReadOnlyBuffer", "()Ljava/nio/ByteBuffer;"));
    	__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached java::nio::ByteBuffer elements");
    }

    {
		using java::util::List;
		failOnFalse(findClass(env, &List.clazz, "java/util/List"));
		failOnFalse(findMethod(env, &List.add__Object, List.clazz, "add", "(Ljava/lang/Object;)Z"));
		__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached java::util::List elements");
    }

    {
    	using java::util::ArrayList;
    	failOnFalse(findClass(env, &ArrayList.clazz, "java/util/ArrayList"));
    	failOnFalse(findMethod(env, &ArrayList.ctor, ArrayList.clazz, "<init>", "()V"));
    	__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached java::util::ArrayList elements");
    }

    {
    	using voxware::engine::recognition::sapivise::SapiViseGrammar;
    	failOnFalse(findClass(env, &SapiViseGrammar.clazz, "voxware/engine/recognition/sapivise/SapiViseGrammar"));
    	failOnFalse(findField(env, &SapiViseGrammar.grammar, SapiViseGrammar.clazz, "grammar", "Ljava/nio/ByteBuffer;"));
    	failOnFalse(findField(env, &SapiViseGrammar.sapivise, SapiViseGrammar.clazz, "sapivise", "Ljava/nio/ByteBuffer;"));
    	failOnFalse(findField(env, &SapiViseGrammar.myName, SapiViseGrammar.clazz, "myName", "Ljava/lang/String;"));
    	failOnFalse(findField(env, &SapiViseGrammar.defaultRuleName, SapiViseGrammar.clazz, "defaultRuleName", "Ljava/lang/String;"));
    	failOnFalse(findMethod(env, &SapiViseGrammar.isResultAudioProvided, SapiViseGrammar.clazz, "isResultAudioProvided", "()Z"));
    	failOnFalse(findMethod(env, &SapiViseGrammar.isTrainingProvided, SapiViseGrammar.clazz, "isTrainingProvided", "()Z"));
    	__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached voxware::engine::recognition::sapivise::SapiViseGrammar elements");
    }

    {
    	using voxware::engine::recognition::sapivise::RawResult;
    	failOnFalse(findClass(env, &RawResult.clazz, "voxware/engine/recognition/sapivise/RawResult"));
        failOnFalse(findField(env, &RawResult.accepted, RawResult.clazz, "accepted", "Z"));
        failOnFalse(findField(env, &RawResult.audio, RawResult.clazz, "audio", "Z"));
        failOnFalse(findField(env, &RawResult.grammarName, RawResult.clazz, "grammarName", "Ljava/lang/String;"));
        failOnFalse(findField(env, &RawResult.hostTrans, RawResult.clazz, "hostTrans", "Ljava/lang/String;"));
        failOnFalse(findField(env, &RawResult.nbestArray, RawResult.clazz, "nbestArray", "[Ljava/lang/String;"));
        failOnFalse(findField(env, &RawResult.result, RawResult.clazz, "result", "Ljava/nio/ByteBuffer;"));
        failOnFalse(findField(env, &RawResult.training, RawResult.clazz, "training", "Z"));
        __android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached voxware::engine::recognition::sapivise::RawResult elements");
    }

    {
    	using voxware::engine::recognition::sapivise::SapiVise;
    	failOnFalse(findClass(env, &SapiVise.clazz, "voxware/engine/recognition/sapivise/SapiVise"));
        failOnFalse(findField(env, &SapiVise.recognizer, SapiVise.clazz, "recognizer", "Ljava/nio/ByteBuffer;"));
        failOnFalse(findField(env, &SapiVise.floatVal, SapiVise.clazz, "floatVal", "F"));
        failOnFalse(findField(env, &SapiVise.intVal, SapiVise.clazz, "intVal", "I"));
        JNINativeMethod sapiViseMethods[] = {
        		{"newRecognizer", "()Ljava/nio/ByteBuffer;", (void*) &Java_voxware_engine_recognition_sapivise_SapiVise_newRecognizer}
        };
        jint ret = env->RegisterNatives(SapiVise.clazz, sapiViseMethods, 1);
        if (ret != 0) return JNI_ERR;
        __android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached voxware::engine::recognition::sapivise::SapiVise elements");
    }

    {
    	using voxware::engine::recognition::sapivise::ViseEnrollStatus;
    	failOnFalse(findClass(env, &ViseEnrollStatus.clazz, "voxware/engine/recognition/sapivise/ViseEnrollStatus"));
    	failOnFalse(findMethod(env, &ViseEnrollStatus.ctor_III, ViseEnrollStatus.clazz, "<init>", "(III)V"));
    	__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached voxware::engine::recognition::sapivise::ViseEnrollStatus elements");
    }

    {
    	using voxware::engine::recognition::sapivise::ViseResult;
    	failOnFalse(findClass(env, &ViseResult.clazz, "voxware/engine/recognition/sapivise/ViseResult"));
    	failOnFalse(findField(env, &ViseResult.result, ViseResult.clazz, "result", "Ljava/nio/ByteBuffer;"));
    	__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached voxware::engine::recognition::sapivise::ViseResult elements");
    }

    {
    	using voxware::engine::recognition::sapivise::ViseTrainer;
    	failOnFalse(findClass(env, &ViseTrainer.clazz, "voxware/engine/recognition/sapivise/ViseTrainer"));
    	failOnFalse(findField(env, &ViseTrainer.nativePeer, ViseTrainer.clazz, "nativePeer", "Ljava/nio/ByteBuffer;"));
    	__android_log_print(ANDROID_LOG_INFO, "VISE.cpp", "Cached voxware::engine::recognition::sapivise::ViseTrainer elements");
    }

    return JNI_VERSION_1_6;
}
