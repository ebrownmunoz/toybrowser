LOCAL_PATH := $(call my-dir)
MY_LOCAL_PATH := $(LOCAL_PATH)
include $(CLEAR_VARS)

LOCAL_MODULE    := vise
LOCAL_SRC_FILES := com_voxware_vise_AndroidAudioSource.cpp com_voxware_vise_NativeLogSource.cpp vise.cpp com_voxware_vise_engine_recognition_sapivise_SapiVise.cc com_voxware_vise_engine_recognition_sapivise_SapiViseGrammar.cc com_voxware_vise_engine_recognition_sapivise_ViseResult.cc com_voxware_vise_engine_recognition_sapivise_ViseTrainer.cc
LOCAL_C_INCLUDES += $(MY_LOCAL_PATH)/vise2/include $(MY_LOCAL_PATH)/vbx/include $(MY_LOCAL_PATH)/sapi/include $(MY_LOCAL_PATH)/ms/include $(MY_LOCAL_PATH)/include
LOCAL_STATIC_LIBRARIES := vvisese vbxsys vbxutil fe oops vvisefe vbxaudio sapivise cep vulcan
LOCAL_LDLIBS := -llog -lOpenSLES -latomic

include $(BUILD_SHARED_LIBRARY)

include $(MY_LOCAL_PATH)/vise2/Android.mk
include $(MY_LOCAL_PATH)/vbx/sys/Android.mk
include $(MY_LOCAL_PATH)/vbx/util/Android.mk
include $(MY_LOCAL_PATH)/fe/Android.mk
include $(MY_LOCAL_PATH)/sapi/Android.mk
