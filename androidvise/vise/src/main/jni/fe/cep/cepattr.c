/* cepattr.c - cepstral frame attributes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 29-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: cepattr.c[1.0] Tue Jun  9 17:37:34 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "floorfix.h"
#include "c.h"
#include "cepattr.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "vbx.h"
#include <assert.h>

#define CEP_PRINT_PRIO VBX_MID_PRIO

/* The Class of CEPATTR (CEP Frame Attributes) Interface Objects */
static iclass_t ICEPATTR_Class = {ICEPATTRID};

/* CEP Attributes */
typedef struct cepattr_s {
  Int         vecLen;     /* The number coefficients in a cepstral vector */
} cepattr_t, *CEPATTR;

/* Private functions */

static ICEPATTR  ICEPATTR_create(IGEN igen);
static Bool      ICEPATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields);

/*** CEPATTR Methods ***/

IGEN
CEPATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a cepstral frame attributes object
 *---------------------------------------------------------------------------*/
{
  CEPATTR   cepattr = (CEPATTR) imem->Calloc(imem, 1, sizeof(cepattr_t), OOPS_MEMORY);
  IGEN      igen = IGEN_create(imem, (OBJ) cepattr);
  ICEPATTR  icepattr = ICEPATTR_create(igen);

  /* Generic IGEN_annihilate() suffices */

  /* Set up the default behavior of ICEPATTR */
  IOBJATTR_setPrinter((IOBJATTR) icepattr, ICEPATTR_dfltPrint);
  IOBJATTR_setLabel((IOBJATTR) icepattr, "CEP ");

  return(igen);
}


Bool
ICEPATTR_getVecLen(ICEPATTR icepattr, Int * vecLenP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the length of a cepstral vector
 *---------------------------------------------------------------------------*/
{
  CEPATTR  cepattr;

  assert(icepattr);
  cepattr = (CEPATTR) IFACE_object((IFACE) icepattr);

  *vecLenP = cepattr->vecLen;
  return(TRUE);
}


Bool
ICEPATTR_setVecLen(ICEPATTR icepattr, Int vecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of a cepstral vector
 *---------------------------------------------------------------------------*/
{
  CEPATTR  cepattr;

  assert(icepattr);
  cepattr = (CEPATTR) IFACE_object((IFACE) icepattr);

  cepattr->vecLen = vecLen;
  return(TRUE);
}


/*** ICEPATTR METHODS ***/

static ICEPATTR
ICEPATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICEPATTR interface
 *---------------------------------------------------------------------------*/
{
  ICEPATTR  icepattr = (ICEPATTR) IOBJATTR_create(igen);

  /* Turn it from an IOBJATTR into an ICEPATTR */
  IFACE_setClass((IFACE) icepattr, &ICEPATTR_Class);

  return(icepattr);
}


static Bool
ICEPATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Print out a cepstral frame object
 *---------------------------------------------------------------------------*/
{
  Val    * cep  = (Val *) &OBJT_contents(objt);
  Int      cepVecLen, numFields;
  Char     prefix[8];  /* 9999 in sprintf below assures that prefix will fit */

  ICEPATTR_getVecLen((ICEPATTR) iobjattr, &cepVecLen);
  /* Build a prefix string to align the first field of any continuation line */
  sprintf(prefix, "\n%%%ds", MIN(prefixLen, 9999));
  for (numFields = 0; numFields < cepVecLen; numFields++) {
    /* If the next field would make the line too long, go to the next line */
    if (!(numFields % maxFields) && numFields) VBX_print(prefix, "");
#ifdef FIXED_POINT
    VBX_print(" %10d", cep[numFields]);
#else  /* Floating Point */
    VBX_print(" %10.5f", cep[numFields]);
#endif  /* FIXED_POINT */
  }
  VBX_print("\n");

  return(TRUE);
}
