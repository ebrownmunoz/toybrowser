/* mkcep.cc - a source of cepstral frames
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Produces frames of cepstral data from audio stream input.  
 *
 * HISTORY
 *  18-May-97  Craig Vanderborgh (craigv@rad.verbex.com)
 *      Derived from mkcep.cc
 *---------------------------------------------------------------------------*/

#include "pthr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "types.h"
#include "class.h"
#include "iface.h"
#include "ifaces.h"
#include "mkcep.h"
#include "feversion.h"
#include "mel.h"
#include "cepattr.h"
#include "sn.h"
#include "vbx.h"

#ifdef DEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif


/* The Class of MKCEPATTR (MKCEP Attributes) Interface Objects */
static iclass_t IMKCEPATTR_Class = {IMKCEPATTRID};

/* The Class of IMKCEP Interface Objects */
static iclass_t IMKCEP_Class = {IMKCEPID};

/* The MKCEP Interface */
typedef struct imkcep_s {
  iface_t  iface;
} imkcep_t;

/* A MKCEP descriptor */
typedef struct mkcep_s {
  IMemory      * imem;          /* The memory allocator */
  ISRC           isrc;          /* MKCEP's source interface (output) */
  ISNK           isnk;          /* MKCEP's sink interface (input) */
  IMKCEP         imkcep;        /* MKCEP's direct interface */
  IGEN           igenattr;      /* Interface to the cep attributes block */
  Int            classSize;     /* Size of the CLASS of cep frames we produce */
  Int            cepVersion;    /* Version: specifies a configuration */
  Int            cepVecLen;     /* Number of actually computed cepstrum components */
  Int            snGap;         /* Gap in bytes from analFrame to last piece of data in it */
  CLASS          cepClass;      /* The CLASS of cep frames */
  MELSRC         melsrc;        /* Descriptor for the Mel Filter Cepstrum maker */
  SN             prevSN;
  OBJT           prevFrm;
} mkcep_t, * MKCEP;

/* Private functions */
static Bool MKCEP_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool MKCEP_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

/*---------------------------------------------------------------------------*
 * NOTE: The following default Cep attributes values correspond to the old
 *       "Jin Version" 800.
 *---------------------------------------------------------------------------*/

/* Default Cep Maker attributes for product */
#define DFLT_CLASSSIZE    8

/* MKCEP Attributes */
typedef struct mkcepattr_s {
  Int        cepVersion;         /* Actually FEVersion */
  Int        classSize;          /* The size of the CLASS of Cep frames produced */
} mkcepattr_t, * MKCEPATTR;

/* Private functions */

static IMKCEP  IMKCEP_create(IMemory * imem, IGEN igen);
static Bool    MKCEP_annihilate(IGEN igen);
static Bool    MKCEP_initialize(IGEN igen, IATTR iattr);
static OBJT    MKCEP_compute(MKCEP mkcep, Short *audioData, Val *power);
static Bool    MKCEP_putFrame(MKCEP mkcep, dir_t dir, SN * snP);
static Bool    MKCEP_reset(MKCEP mkcep);
static Bool    MKCEP_setAttributes(ISNK isnk, IATTR iattr);

static IMKCEPATTR  IMKCEPATTR_create(IMemory * imem, IGEN igen);

/*** MKCEP Methods ***/

IGEN
MKCEP_create(IMemory *imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a cep feature extractor
 *---------------------------------------------------------------------------*/
{
  MKCEP      mkcep = (MKCEP) imem->Calloc(imem, 1, sizeof(mkcep_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) mkcep);
  ISNK       isnk = ISNK_create(igen);            /* the EXPORTED sink */
  ISRC       isrc = ISRC_create(igen);            /* the EXPORTED source */
  IGEN       igenattr = CEPATTR_create(imem);
  ICEPATTR   icepattr = NULL;
  IMKCEP     imkcep = NULL;
  IATTR      iattr;
  Int        i;

  imkcep = IMKCEP_create(imem, igen);

  /* Initialize the object */
  mkcep->imem = imem;
  mkcep->isnk = isnk;
  mkcep->isrc = isrc;
  mkcep->imkcep = imkcep;
  mkcep->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, MKCEP_annihilate);
  IGEN_setInitializer(igen, MKCEP_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setAttributes(isrc, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the source interface */
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, ICEPATTRID));
  ISRC_setGetFrame(isrc, MKCEP_getFrame);
  ISRC_setFrameAvailable(isrc, MKCEP_frameAvailable);
  
  return(igen);
}

/*** GENERIC METHODS ***/

static Bool
MKCEP_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a mkcep object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  MKCEP    mkcep = (MKCEP) IFACE_object((IFACE) igen);
  IMemory *imem;
  
  /* Reset MKCEP.  This ensures all OBJTs are freed */
  MKCEP_reset(mkcep);
  
  /* Annihilate the CLASS of cepstral frame OBJTs */
  if (mkcep->cepClass && !CLASS_destroy(mkcep->cepClass, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Annihilate the Cep attributes object */
  if (!IGEN_annihilate(mkcep->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) mkcep->igenattr));
    return(FALSE);
  }

  imem = mkcep->imem;
  MEL_destroy(mkcep->melsrc);

  return(TRUE);
}

static Bool
MKCEP_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a Cep feature extractor according to a MKCEP attributes 
 * specification.  Like all IGEN_initialize() functions, this may be called 
 * repeatedly with different IATTRs.
 *---------------------------------------------------------------------------*/
{
  MKCEP      mkcep = (MKCEP) IFACE_object((IFACE) igen);
  MKCEPATTR  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) iattr);
  IMemory   *imem = mkcep->imem;
  ICEPATTR   icepattr;
  ISNK       sink;
  Int        i, j;
  Int        feVersion, classSize, cepVecLen;

   /* Get the mkcep attributes */
  IMKCEPATTR_getCepVersion((IMKCEPATTR) iattr, &feVersion);
  IMKCEPATTR_getClassSize((IMKCEPATTR) iattr, &classSize);

   /* Get cepVecLen */
  if (!FEVER_getCepVecLen(feVersion, &cepVecLen)) {
    VBX_print("MKCEP_initialize: Bad FEVersion %d from mkcepattr\n", feVersion);
    return FALSE;
  }

  /* If the the classSize has changed, re-create the CLASS */
  if (mkcep->cepVecLen != cepVecLen || mkcep->classSize != classSize) {
    if (mkcep->cepClass)
      CLASS_destroy(mkcep->cepClass, NULL, NULL);
    mkcep->cepClass = CLASS_create(imem, classSize, cepVecLen * sizeof(Val), NULL, NULL);
    mkcep->cepVecLen = cepVecLen;
    mkcep->classSize = classSize;
  }

  if (mkcep->cepVersion == feVersion)
    return TRUE;

   /* Otherwise do the actual initialization */
  VBX_DEBUG(VBX_print("MKCEP_initialize: initializing from FEVersion %d to FEVersion %d\n",
                      mkcep->cepVersion, feVersion));

  mkcep->cepVersion = feVersion;

  /* Set the size of the Cepstral vector on the sink.  Configure accordingly */
  ISRC_getAttributes(mkcep->isrc, (IATTR *) &icepattr);
  assert(icepattr != NULL);
  ICEPATTR_setVecLen(icepattr, mkcep->cepVecLen);
  ICEPATTR_getVecLen(icepattr, &mkcep->cepVecLen);

  /* CEP calculator destruction is a no-op unless already initialized */
  if (mkcep->melsrc != NULL) {
    MEL_destroy(mkcep->melsrc);
    mkcep->melsrc = NULL;
  }

  mkcep->melsrc = MEL_create(imem, mkcep->cepVersion);
  VBX_fatalIf(mkcep->melsrc == NULL);
  VBX_DEBUG(VBX_print("MKCEP_initialize: complete (1)\n"));

  mkcep->snGap = MEL_snGap(mkcep->melsrc);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(mkcep->isrc);

  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) igen, EXC_NOSNK);
    VBX_DEBUG(VBX_print("MKCEP_initialize FAILS, no sink found\n"));
    return(FALSE);
  }

  /* Pass the Cep attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, (IATTR) icepattr)) {
    /* If successfully passed, return TRUE */
    VBX_DEBUG(VBX_print("MKCEP_initialize: successful return (2)\n"));
    return(TRUE);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
    VBX_DEBUG(VBX_print("MKCEP_initialize FAILS at the last possible spot\n"));
    return(FALSE);
  }
  return(TRUE);
}

static OBJT
MKCEP_compute(MKCEP mkcep, Short * audioData, Val * power)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Use the given audio data to construct an analysis frame, and submit
 * the result to the Cep calculator.  Construct an OBJT containing the 
 * resultant cepstral frame and return a pointer to it
 * NOTE: No cepstra are returned until the analysis frame is filled and
 *       (if configured) the RASTA cold-start is complete
 *---------------------------------------------------------------------------*/
{
  OBJT    obj;
  Val   * vptr;
  Int     activity;
  Bool    featReady;

  obj = OBJT_new(mkcep->cepClass);
  assert(obj);
  vptr = (Val *) &OBJT_contents(obj);

  featReady = MEL_cepstrum(mkcep->melsrc, vptr, power, audioData, &activity);

  if (featReady) {
    return(obj);
  } else {
    OBJT_free(obj);
    return(NULL);
  }
}

static Bool
MKCEP_reset(MKCEP mkcep)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the front end, e.g. after a hiatus in the audio signal
 *---------------------------------------------------------------------------*/
{
  if (mkcep->prevFrm) {
    OBJT_free(mkcep->prevFrm);
    mkcep->prevFrm = NULL;
  }
  MEL_reset(mkcep->melsrc);

  return(TRUE);
}  

static Bool
MKCEP_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Export a cepstral frame object via the isrc (output) interface
 * acquiring the required audio frames from the source attached to the isnk
 * (input) interface
 * Return: Success/Failure of attempted operation
 * DON'T NEED SN ARG.  EXC * FROM SRC WOULD BE MORE USEFUL.  FIX !!
 *---------------------------------------------------------------------------*/
{
  ISRC  src;        /* The source of audio frame input */
  ISNK  snk;        /* The sink of Cep frame output */
  SN    snRead;     /* The SN read from the audio source */
  OBJT  audioIn;    /* The audio frame OBJT obtained from the source */
  OBJT  audioInClone;
  Val   power;
  MKCEP mkcep;
  Int   exc;
  SN    snOut;
  OBJT  frame;
  Bool  avail;
    
  mkcep  = (MKCEP) IFACE_object((IFACE) isrc);
  assert(mkcep && snP);
  if (outP != NULL)
   *outP = NULL;

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(mkcep->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) mkcep->isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Get the sink associated with the source (output) interface */
  snk = ISRC_sink(mkcep->isrc);
  /* If there is none, register an exception */
  if (!snk) {
    IFACE_setError((IFACE) mkcep->isrc, EXC_NOSNK);
    return(FALSE);
  }

  /* VBX_DEBUG(VBX_print(" <1> ")); */
  frame = NULL;
  do {
    /* Check to see if a frame is available from the source */
    snRead = -1;

    if (!ISRC_frameAvailable(src, FORWARD, &snRead)) {
      exc = IFACE_error((IFACE) src); 
      IFACE_setError((IFACE) mkcep->isrc, exc);
      if (exc == EXC_TOOFEWSAMPLES) {
        ;
      } else if (exc == EXC_EOU) {
        if (outP != NULL)
          *outP = OBJT_new(mkcep->cepClass);
        snOut = SN_MAX;
      } else {
        SEVERE_BRA_ "MKCEP_getFrame: unexpected exception %d getting audio frame\n", exc _KET;
      }
      return(FALSE);
    }

    /* If we are primed and there is a frame available, but not requested, get out */
    if (MEL_frameAvailable(mkcep->melsrc) && outP == NULL) {
      *snP = snRead - mkcep->snGap;
      return TRUE;
    }

    /* Special case: Doing a MKCEP_frameAvailable() produced a frame
     * and this is the requested frame.  Clone and return it without 
     * doing anything else */
    if (mkcep->prevFrm != NULL && (mkcep->prevSN == *snP || *snP == -1)) {
      *outP = OBJT_clone(mkcep->prevFrm);
      *snP = mkcep->prevSN;
       OBJT_free(mkcep->prevFrm);
       mkcep->prevFrm = NULL;
       mkcep->prevSN = 0;
       return TRUE;
    }
      
    /* VBX_DEBUG(VBX_print(" <2> ")); */

    /* Get the audio frame required to derive the Cep frame */
    if (!ISRC_getFrame(src, FORWARD, &snRead, &audioIn))
      FATAL_BRA_ "MKCEP unable to get an available frame from source: exc = %d\n", 
                  IFACE_error((IFACE) src) _KET;

    /* VBX_DEBUG(VBX_print(" <3> ")); */
    /* Make a Cep frame */
    frame = MKCEP_compute(mkcep, (Short *) &OBJT_contents(audioIn), &power);

    /* Free the audio frame object immediately */
    OBJT_free(audioIn);
  } while (!frame);

  *snP = snRead - mkcep->snGap;
  if (outP == NULL && frame != NULL) {
    mkcep->prevSN = *snP;
    if (mkcep->prevFrm != NULL)
      OBJT_free(mkcep->prevFrm);
    mkcep->prevFrm = frame;
  } else {
   *outP = frame;
  }

  return(frame != NULL);
}

static Bool
MKCEP_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an OBJT via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  MKCEP mkcep;

  mkcep = (MKCEP) IFACE_object((IFACE) isrc);
  if (mkcep->prevFrm != NULL) {
    return(TRUE);
  } else {
    return(MKCEP_getFrame(isrc, dir, snP, NULL));
  }
}

/*** IMKCEP Methods ***/

static IMKCEP
IMKCEP_create(IMemory *imem, IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IMKCEP interface to the feature extractor
 *---------------------------------------------------------------------------*/
{
  IMKCEP imkcep;

  if (imkcep = (IMKCEP) IFACE_calloc((IFACE) igen, 1, sizeof(imkcep_t))) {
    IFACE_setClass((IFACE) imkcep, &IMKCEP_Class);
    IFACE_setObject((IFACE) imkcep, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) imkcep, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) imkcep);
  }

  return(imkcep);
}

Bool
IMKCEP_reset(IMKCEP imkcep)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the front end
 *---------------------------------------------------------------------------*/
{
  MKCEP mkcep = ((MKCEP) IFACE_object((IFACE) imkcep));

  return(MKCEP_reset(mkcep));
}

Bool
IMKCEP_setSigBits(IMKCEP imkcep, Int sigBits)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the number of significant bits in the incoming samples
 *---------------------------------------------------------------------------*/
{
  MKCEP mkcep = ((MKCEP) IFACE_object((IFACE) imkcep));

  return(MEL_setSigBits(mkcep->melsrc, sigBits));
}  

void
IMKCEP_startCalibration(IMKCEP imkcep, Int maxFrames)
{
  MKCEP mkcep = ((MKCEP) IFACE_object((IFACE) imkcep));
  MEL_startCalibration(mkcep->melsrc, maxFrames);
}

void
IMKCEP_stopCalibration(IMKCEP imkcep)
{
  MKCEP mkcep = ((MKCEP) IFACE_object((IFACE) imkcep));
  MEL_stopCalibration(mkcep->melsrc);
}

Int
IMKCEP_getNoiseTracking(IMKCEP imkcep)
{
  MKCEP mkcep = ((MKCEP) IFACE_object((IFACE) imkcep));
  return MEL_getNoiseTracking(mkcep->melsrc);
}

void
IMKCEP_setNoiseTracking(IMKCEP imkcep, Int track)
{
  MKCEP mkcep = ((MKCEP) IFACE_object((IFACE) imkcep));
  MEL_setNoiseTracking(mkcep->melsrc, track);
}

/*** SOURCE AND SINK METHODS ***/

static Bool
MKCEP_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Disallow alteration of Cep attributes
 *---------------------------------------------------------------------------*/
{
  FATAL_BRA_ "MKCEP src/snk attributes are presently not alterable\n" _KET;

  return(TRUE);
}


/*** MKCEPATTR Methods ***/

IGEN
MKCEPATTR_create(IMemory *imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a MKCEP attributes object
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr = (MKCEPATTR) imem->Calloc(imem, 1, sizeof(mkcepattr_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) mkcepattr);
  IMKCEPATTR imkcepattr = IMKCEPATTR_create(imem, igen);

  /* Generic IGEN_annihilate() suffices */

  /* Set the defaults for MKCEPATTR */
  mkcepattr->classSize = DFLT_CLASSSIZE;

  return(igen);
}


/*** IMKCEPATTR METHODS ***/

static IMKCEPATTR
IMKCEPATTR_create(IMemory *imem, IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IMKCEPATTR interface
 *---------------------------------------------------------------------------*/
{
  IMKCEPATTR  imkcepattr;

  if (imkcepattr = (IMKCEPATTR) IFACE_calloc((IFACE) igen, 1, sizeof(imkcepattr_t))) {
    IFACE_setClass((IFACE) imkcepattr, &IMKCEPATTR_Class);
    IFACE_setObject((IFACE) imkcepattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) imkcepattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) imkcepattr);
  }

  return(imkcepattr);
}

Bool
IMKCEPATTR_getCepVersion(IMKCEPATTR imkcepattr, Int *cepVersion)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the front end
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  *cepVersion = mkcepattr->cepVersion;
  return(TRUE);
}

Bool
IMKCEPATTR_setCepVersion(IMKCEPATTR imkcepattr, Int cepVersion)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the front end
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  mkcepattr->cepVersion = cepVersion;
  return(TRUE);
}

Bool
IMKCEPATTR_getClassSize(IMKCEPATTR imkcepattr, Int * classSizeP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size of the CLASS of Cep frames
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  *classSizeP = mkcepattr->classSize;
  return(TRUE);
}

Bool
IMKCEPATTR_setClassSize(IMKCEPATTR imkcepattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the CLASS of Cep frames
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  mkcepattr->classSize = classSize;
  return(TRUE);
}

Bool
IMKCEPATTR_getSampleRate(IMKCEPATTR imkcepattr, Int *sampleRate)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the sampling rate
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  return FEVER_getSampleRate(mkcepattr->cepVersion, sampleRate);
}

Bool
IMKCEPATTR_getFrameLen(IMKCEPATTR imkcepattr, Int *frameLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the analysis frame length
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  return FEVER_getFrameLen(mkcepattr->cepVersion, frameLen);
}

Bool
IMKCEPATTR_getFrameShift(IMKCEPATTR imkcepattr, Int *frameShift)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the analysis frame shift
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  return FEVER_getFrameShift(mkcepattr->cepVersion, frameShift);
}

Bool
IMKCEPATTR_getCepVecLen(IMKCEPATTR imkcepattr, Int *cepVecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the cepstral vector length
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  return FEVER_getCepVecLen(mkcepattr->cepVersion, cepVecLen);
}

Bool
IMKCEPATTR_getOtherProc(IMKCEPATTR imkcepattr, Int *otherProc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the value of the "other processing" attribute which specifies
 * optional signal processing (e.g. RASTA)
 *---------------------------------------------------------------------------*/
{
  MKCEPATTR  mkcepattr;

  assert(imkcepattr);
  mkcepattr = (MKCEPATTR) IFACE_object((IFACE) imkcepattr);

  return FEVER_getOtherProc(mkcepattr->cepVersion, otherProc);
}
