#include "pthr.h"
#include <math.h>
#include "imemory.h"
#include "floorfix.h"
#include "ham.h"

#define KCYUFAC ((Float)0.97)

typedef struct ham_s {
  IMemory * imem;
  Val     * HAMMING_window;
  Int       windowSize;
  Val       kcyuFac;            /* Regular case pre-emphasis factor */
  Val       compFac;            /* Pre-emphasis factor at the start */
  Bool      start;
  Short     last;
} ham_t;


HAM
HAM_create(IMemory *imem, Int windowSize)
{
  HAM    ham;
  Double rps;  /* (double) radians per sample */
  Int    i;
  
  if (imem == NULL || NULL == (ham = (HAM) imem->Calloc(imem, 1, sizeof(ham_t), FAST_MEMORY)))
    return NULL;

  ham->imem = imem;  
  if (!(ham->windowSize = windowSize) ||
      !(ham->HAMMING_window = (Val *) imem->Calloc(imem, 1, (Int)windowSize * sizeof(Val), FAST_MEMORY)))
    return NULL;
  
  ham->kcyuFac = FLO2VAL(KCYUFAC);
  /* Start factor */
  ham->compFac = FLO2VAL((Float)1.0 - KCYUFAC);
  ham->start = TRUE;

  rps = 2. * PI / (Double) windowSize;
  for (i = 0; i < windowSize; i++) {
    ham->HAMMING_window[i] = FLO2VAL((Float)(0.54 - 0.46 * cos((Double) i * rps)));
  }

  return ham;
} 


void
HAM_destroy(HAM ham)
{
  IMemory * imem;

  if (ham) {
    if ((imem = ham->imem) != NULL) {   
      imem->Free(imem, ham->HAMMING_window);
      imem->Free(imem, ham);
    }
  }
}


void
HAM_reset(HAM ham)
{
  if (ham)
    ham->start = TRUE;
}


void
HAM_preEmph(HAM ham, Short * input, Val * output, Int size)
{ /* Retains original scale (scales back immediatly) */
  Int i;
  
  if (ham->start) {
    output[0] = NORMBACK(ham->compFac * (Val)input[0]);
    ham->start = FALSE;
  } else {
    output[0] = (Val)input[0] - NORMBACK(ham->kcyuFac * (Val)ham->last);
  }
  for (i = 1; i < size; i++)
    output[i] = (Val)input[i] - NORMBACK(ham->kcyuFac * (Val)input[i - 1]);

  ham->last = input[size - 1];
}


void
HAM_apply(HAM ham, Val * input, Val * output)
{
 /* When FIXED_POINT, output will have maximum 21 significant bits -
    15 from Short sample, 1 from HAM_preEmph, SHIFTVAL - SHIFTWIN = 5
    from this routine. We have 9 bits to go for 512-FFT before scaling */
  Int i;
  
  for (i = 0; i < ham->windowSize; i++)
    output[i] = NORMWIND(input[i] * ham->HAMMING_window[i]);
}
