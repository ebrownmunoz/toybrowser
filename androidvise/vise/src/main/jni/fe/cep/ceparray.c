/* ceparray.c - a cepstral frame source which reads from an array
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 26-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "floorfix.h"
#include "c.h"
#include "ceparray.h"
#include "class.h"
#include "iattr.h"
#include "cepattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include <assert.h>


/* The Class of CEPARATTR (CEPARRAY Attributes) Interface Objects */
static iclass_t ICEPARATTR_Class = {ICEPARATTRID};

/* CEPARRAY Attributes */
typedef struct ceparattr_s {
  Val     * array;          /* The array of cepstral coefficients */
  Int       arraySize;      /* The number of coefficients in the array */
  Int       classSize;      /* The size of the CLASS of cepstral frame objects */
  Int       cepVecLen;      /* The number of coefficients in a cepstral frame */
  Int       minSN;          /* The minimum sequence number */
} ceparattr_t, * CEPARATTR;


/* A CEPARRAY descriptor */
typedef struct ceparray_s {
  IMemory * imem;          /* The memory allocator */
  ISRC      isrc;          /* CEPARRAY's (cepstral) source interface (output) */
  IGEN      igenattr;      /* Interface to the cepstral attributes block */
  Int       classSize;     /* The size of the CLASS of cepstral frame OBJTs */
  CLASS     cepstra;       /* The CLASS of cepstral frame OBJTs */
  Val     * array;         /* The array of cepstral coefficients */
  Int       coefCount;     /* The number of cepstral coeffs in the array */
  Int       vecCount;      /* The number of cepstral vectors in the array */
  Int       cepVecLen;     /* The number of coefficients in a cepstral vector */
  SN        seqNum;        /* The last sequence number requested */
  SN        minSN;         /* The lower limit on sequence numbers */
  SN        maxSN;         /* The upper limit on sequence numbers */
} ceparray_t, * CEPARRAY;


/* Private functions */

static Bool  CEPARRAY_annihilate(IGEN igen);
static Bool  CEPARRAY_initialize(IGEN igen, IATTR iattr);
static Bool  CEPARRAY_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  CEPARRAY_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static ICEPARATTR  ICEPARATTR_create(IGEN igen);


/*** CEPARRAY Methods ***/

IGEN
CEPARRAY_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPARRAY object
 *---------------------------------------------------------------------------*/
{
  CEPARRAY   ceparray = (CEPARRAY) imem->Calloc(imem, 1, sizeof(ceparray_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) ceparray);
  ISRC       isrc = ISRC_create(igen);
  IGEN       igenattr = CEPATTR_create(imem);

  /* Initialize the object */
  ceparray->imem = imem;
  ceparray->isrc = isrc;
  ceparray->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, CEPARRAY_annihilate);
  IGEN_setInitializer(igen, CEPARRAY_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, CEPARRAY_getFrame);
  ISRC_setFrameAvailable(isrc, CEPARRAY_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, ICEPATTRID));

  return(igen);
}


/*** GENERIC METHODS ***/

static Bool
CEPARRAY_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a ceparray object
 * This supplements the generic IGEN_annihilate() by annihilating the CLASS
 * of cepstral frame OBJTs and the cepstral attributes block
 *---------------------------------------------------------------------------*/
{
  CEPARRAY ceparray = (CEPARRAY) IFACE_object((IFACE) igen);

  /* Annihilate the CLASS of cepstral frame OBJTs */
  if (ceparray->cepstra && !CLASS_destroy(ceparray->cepstra, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Annihilate the cepstral attributes object */
  if (!IGEN_annihilate(ceparray->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) ceparray->igenattr));
    return(FALSE);
  }

  return(TRUE);

}


static Bool
CEPARRAY_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a ceparray according to a ceparray attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  CEPARATTR   ceparattr = (CEPARATTR) IFACE_object((IFACE) iattr);
  CEPARRAY    ceparray = (CEPARRAY) IFACE_object((IFACE) igen);
  ICEPATTR    icepattr;
  ISNK        sink;
  Int         oldCepVecLen;

  /* Get the array and array size */
  ceparray->array = ceparattr->array;
  ceparray->coefCount = ceparattr->arraySize;

  /* Get the length of the cepstral vectors */
  ceparray->cepVecLen = ceparattr->cepVecLen;

  /* Get the vecLen in the cepstral attributes block (in ISRC) */
  if (!ISRC_getAttributes(ceparray->isrc, (IATTR *) &icepattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) ceparray->isrc));
    return(FALSE);
  }
  ICEPATTR_getVecLen(icepattr, &oldCepVecLen);

  /* If the cepVecLen or the classSize has changed, re-create the CLASS */
  if (ceparray->cepVecLen != oldCepVecLen || ceparattr->classSize != ceparray->classSize) {
    if (ceparray->cepstra)
      CLASS_destroy(ceparray->cepstra, NULL, NULL);
    ceparray->cepstra = CLASS_create(ceparray->imem, ceparattr->classSize, ceparray->cepVecLen * sizeof(Val), NULL, NULL);
    ceparray->classSize = ceparattr->classSize;
  }

  /* Compute the number of vectors in the array */
  ceparray->vecCount = ceparray->coefCount / ceparray->cepVecLen;

  /* Get the limiting sequence numbers */
  ceparray->minSN = ceparattr->minSN;
  /* If new minSN is negative, continue old sequence */
  if (ceparray->minSN < 0)
    ceparray->minSN = ceparray->maxSN + 1;
  ceparray->maxSN = ceparray->minSN + ceparray->vecCount - 1;

  /* Initialize the current sequence number */
  ceparray->seqNum = ceparray->minSN;

  /* If cepVecLen has changed, promulgate the change to the sink */
  if (oldCepVecLen != ceparray->cepVecLen) {
    ICEPATTR_setVecLen(icepattr, ceparray->cepVecLen);

    /* Get the sink associated with the source (output) interface */
    sink = ISRC_sink(ceparray->isrc);
    /* If there is none, register an exception */
    if (!sink) {
      IFACE_setError((IFACE) igen, EXC_NOSNK);
      return(FALSE);
    }

    /* Pass the cepstral attributes on to the sink, if possible */
    if (ISNK_setAttributes(sink, (IATTR) icepattr)) {
      /* If successfully passed, return TRUE */
      return(TRUE);
    } else {
      /* Otherwise, register the exception and return FALSE */
      IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
      return(FALSE);
    }
  }
}


/*** SOURCE METHODS ***/

static Bool
CEPARRAY_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a cepstral output frame via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  CEPARRAY  ceparray;
  SN        sn = *snP;

  assert(isrc && snP);

  ceparray = ((CEPARRAY) IFACE_object((IFACE) isrc));

  /* A negative SN is a request for the next SN in the specified direction */
  sn = (*snP < 0) ? ceparray->seqNum + dir : *snP;

  /* Jumps in the specified direction are permitted */
  if (dir == FORWARD)
    /* If looking FORWARD, move forward to the minimum SN */
    sn = MAX(sn, ceparray->minSN);
  else
    /* If looking BACKWARD, move backward to the maximum SN */
    sn = MIN(sn, ceparray->maxSN);

  /* Honor only requests for frames within the bounding sequence numbers */
  if (ceparray->minSN <= sn && sn <= ceparray->maxSN) {
    /* If the request can be met, report and record the new SN */
    *snP = ceparray->seqNum = sn;
    /* If requested, create a frame object with the desired elements */
    if (outP) {
      *outP = OBJT_new(ceparray->cepstra);
      /* If an OBJT is available, fill it with the cepstral data */
      if (*outP) {
        memcpy(&OBJT_contents(*outP),
               ceparray->array + ceparray->cepVecLen * (sn - ceparray->minSN),
               ceparray->cepVecLen * sizeof(Val));
      /* Otherwise, report an exception */
      } else {
        IFACE_setError((IFACE) isrc, EXC_CEPCLASSEMPTY);
        return(FALSE);
      }
    }
    return(TRUE);
  } else {
    /* Otherwise, register the error and return FALSE */
    IFACE_setError((IFACE) isrc, (*snP < 0) ? (dir == FORWARD ? EXC_EOU : EXC_BOU) : EXC_INVALIDSN);
    return(FALSE);
  }
}


static Bool
CEPARRAY_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of a cepstral frame via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(CEPARRAY_getFrame(isrc, dir, snP, NULL));
}


/*** CEPARATTR Methods ***/

IGEN
CEPARATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPARRAY attributes object
 *---------------------------------------------------------------------------*/
{
  CEPARATTR   ceparattr = (CEPARATTR) imem->Calloc(imem, 1, sizeof(ceparattr_t), OOPS_MEMORY);
  IGEN        igen = IGEN_create(imem, (OBJ) ceparattr);
  ICEPARATTR  iceparattr = ICEPARATTR_create(igen);

  return(igen);
}


/*** ICEPARATTR METHODS ***/

static ICEPARATTR
ICEPARATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICEPARATTR interface
 *---------------------------------------------------------------------------*/
{
  ICEPARATTR  iceparattr;

  if ((iceparattr = (ICEPARATTR) IFACE_calloc((IFACE) igen, 1, sizeof(iceparattr_t)))) {
    IFACE_setClass((IFACE) iceparattr, &ICEPARATTR_Class);
    IFACE_setObject((IFACE) iceparattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iceparattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iceparattr);
  }

  return(iceparattr);
}


Bool
ICEPARATTR_setArray(ICEPARATTR iceparattr, Val * array, Int size)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the array of cepstral coefficients
 *---------------------------------------------------------------------------*/
{
  CEPARATTR  ceparattr;

  assert(iceparattr);
  ceparattr = (CEPARATTR) IFACE_object((IFACE) iceparattr);

  ceparattr->array = array;
  ceparattr->arraySize = size;

  return(TRUE);
}


Bool
ICEPARATTR_setClassSize(ICEPARATTR iceparattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  CEPARATTR  ceparattr;

  assert(iceparattr);
  ceparattr = (CEPARATTR) IFACE_object((IFACE) iceparattr);

  ceparattr->classSize = classSize;

  return(TRUE);
}


Bool
ICEPARATTR_setCepVecLen(ICEPARATTR iceparattr, Int cepVecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of cepstral vectors
 *---------------------------------------------------------------------------*/
{
  CEPARATTR  ceparattr;

  assert(iceparattr);
  ceparattr = (CEPARATTR) IFACE_object((IFACE) iceparattr);

  ceparattr->cepVecLen = cepVecLen;

  return(TRUE);
}


Bool
ICEPARATTR_setMinSN(ICEPARATTR iceparattr, SN minSN)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the minimum sequence number
 *---------------------------------------------------------------------------*/
{
  CEPARATTR  ceparattr;

  assert(iceparattr);
  ceparattr = (CEPARATTR) IFACE_object((IFACE) iceparattr);

  ceparattr->minSN = minSN;

  return(TRUE);
}
