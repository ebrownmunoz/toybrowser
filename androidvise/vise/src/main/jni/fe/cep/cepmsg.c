/* cepmsg.c - a cepstral frame source which reads from FlexVISE's incoming VBXMSG
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 20-Oct-97  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdlib.h>
#include <string.h>
#include "cep.h"
#include "class.h"
#include "iattr.h"
#include "cepattr.h"
#include "cepmsg.h"
#include "visecomp.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include "types.h"
#include "vbxtypes.h"
#include "visecmds.h"
#include "viseerr.h"
#include "visemsg.h"
#include <assert.h>

#ifdef DEBUG
#define CEPMSG_DEBUG
#endif

#ifdef CEPMSG_DEBUG
#define VBX_DEBUG(P)    P
#define CEPMSG_FRAME(F) F
#include "vbx.h"            /* VBX_print */
#else
#define VBX_DEBUG(P)
#define CEPMSG_FRAME(F)
#endif

#define DFLT_MAXFRAMES  (SN_MAX / 2);
#define DFLT_MAXGAPLEN  (SN_MAX / 2);

 /* The Class of CEPMSG Interface Objects */
static iclass_t ICEPMSG_Class = {ICEPMSGID};

 /* The CEPMSG Interface */
typedef struct icepmsg_s {
  iface_t  iface;
} icepmsg_t;

 /* The Class of CEPMSGATTR (CEPMSG Attributes) Interface Objects */
static iclass_t ICEPMSGATTR_Class = {ICEPMSGATTRID};

 /* CEPMSG attributes */
typedef struct cepmsgattr_s {
  Uns        classSize;     /* The size of the CLASS of cep frame objects */
  ICEPATTR   frameAttr;     /* The attributes of a cepstral frame */
  SN         maxReject;     /* The maximum number of bytes getFrame will consume to reach the requested frame */
  SN         maxGapLen;     /* The maximum SN gap getFrame will allow */
  IMBox    * mailbox;       /* CEPMSG's mailbox */
} cepmsgattr_t, * CEPMSGATTR;

 /* A CEPMSG descriptor */
typedef struct cepmsg_s {
  IMemory  * memory;        /* The memory allocator */
  IMBox    * mailbox;       /* CEPMSG's mailbox */
  ISRC       isrc;          /* CEPMSG's (cep) source interface (output) */
  IGEN       igenattr;      /* Interface to the cep attributes block */
  ICEPMSG    icepmsg;       /* CEPMSG's external interface */
  VBXMSG     incoming;      /* Message port for incoming messages from FE */
  VBXMSG     outgoing;      /* Message port for outgoing messages to FE */
  Uns        classSize;     /* The size of the CLASS of cep frame OBJTs */
  CLASS      frames;        /* The CLASS of cep frame OBJTs */
  Int        cepVecLen;     /* The number of coefficients in a cepstral vector */
  SN         fwdTol;
  SN         fwdTolOff;
  SN         bwdTol;
  SN         bwdTolOff;
} cepmsg_t, * CEPMSG;

 /* SN Macros */
#define INFWDTOL(D)  ((D) < 0 ? (D) < cepmsg->fwdTolOff : (D) <= cepmsg->fwdTol)
#define INBWDTOL(D)  ((D) > 0 ? (D) > cepmsg->bwdTolOff : (D) >= cepmsg->bwdTol)

 /* Private functions */

static ICEPMSG  ICEPMSG_create(IGEN igen);

static Bool  CEPMSG_annihilate(IGEN igen);
static Bool  CEPMSG_initialize(IGEN igen, IATTR iattr);
static Bool  CEPMSG_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  CEPMSG_frameAvailable(ISRC isrc, dir_t dir, SN * snP);
static V_Err CEPMSG_readMessage(CEPMSG cepmsg, V_Int * command);
static V_Err CEPMSG_acknowledge(CEPMSG cepmsg, V_Int command, V_Err status);

static ICEPMSGATTR  ICEPMSGATTR_create(IGEN igen);


 /*** CEPMSG Methods ***/

IGEN
CEPMSG_create(IMemory * memory)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPMSG object
 *---------------------------------------------------------------------------*/
{
  CEPMSG  cepmsg = (CEPMSG) memory->Calloc(memory, 1, sizeof(cepmsg_t), OOPS_MEMORY);
  IGEN    igen = IGEN_create(memory, (OBJ) cepmsg);
  ISRC    isrc = ISRC_create(igen);
  IGEN    igenattr = CEPATTR_create(memory);
  ICEPMSG icepmsg = ICEPMSG_create(igen);

   /* Initialize the object */
  cepmsg->memory = memory;
  cepmsg->isrc = isrc;
  cepmsg->igenattr = igenattr;
  cepmsg->icepmsg = icepmsg;

   /* Set up the generic interface */
  IGEN_setAnnihilator(igen, CEPMSG_annihilate);
  IGEN_setInitializer(igen, CEPMSG_initialize);

   /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, CEPMSG_getFrame);
  ISRC_setFrameAvailable(isrc, CEPMSG_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, ICEPATTRID));

  return(igen);
}


OBJT
ICEPMSG_newFrame(ICEPMSG icepmsg)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a new cepstral frame object from the CLASS of them that CEPMSG
 * maintains.  Returns NULL if none are available
 *---------------------------------------------------------------------------*/
{
  CEPMSG    cepmsg;

  cepmsg = ((CEPMSG) IFACE_object((IFACE) icepmsg));

  return(OBJT_new(cepmsg->frames));
}


 /*** ICEPMSG METHODS ***/

static ICEPMSG
ICEPMSG_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICEPMSG interface
 *---------------------------------------------------------------------------*/
{
  ICEPMSG  icepmsg;

  if ((icepmsg = (ICEPMSG) IFACE_calloc((IFACE) igen, 1, sizeof(icepmsg_t)))) {
    IFACE_setClass((IFACE) icepmsg, &ICEPMSG_Class);
    IFACE_setObject((IFACE) icepmsg, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) icepmsg, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) icepmsg);
  }

  return(icepmsg);
}


 /*** GENERIC METHODS ***/

static Bool
CEPMSG_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a cepmsg object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  CEPMSG cepmsg = (CEPMSG) IFACE_object((IFACE) igen);

   /* Annihilate the CLASS of cepstral frame OBJTs */
  if (cepmsg->frames && !CLASS_destroy(cepmsg->frames, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }
  VBX_DEBUG(VBX_print("CEPMSG_annihilate: destroyed CLASS of %u cepframes @%p\n", cepmsg->classSize, cepmsg->frames));

   /* Annihilate the cepstral attributes object */
  if (!IGEN_annihilate(cepmsg->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) cepmsg->igenattr));
    return(FALSE);
  }

   /* Flush out any unread messages */
  if (cepmsg->incoming) {
    V_Uns    senderId;
    V_ULong  length;
    V_Int    cmd;
    V_Err    status;
    while (MSG_openRead(cepmsg->incoming, &senderId, &length, &cmd, FALSE, &status))
      MSG_closeRead(cepmsg->incoming, &status);
  }

  MSG_destroy(cepmsg->incoming);
  MSG_destroy(cepmsg->outgoing);

  return(TRUE);
}


static Bool
CEPMSG_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a cepmsg according to a cepmsg attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) iattr);
  CEPMSG      cepmsg = (CEPMSG) IFACE_object((IFACE) igen);
  ICEPATTR    icepattr;
  ISNK        sink;
  OBJT        object;
  Int         oldCepVecLen;

   /* Get the attributes */
  cepmsg->mailbox   =   (cepmsgattr->mailbox);
  cepmsg->fwdTol    =   (cepmsgattr->maxGapLen);
  VBX_DEBUG(VBX_print("CEPMSG_initialize: cepmsg->fwdTol    = %ld\n", cepmsg->fwdTol));
  cepmsg->fwdTolOff = SN_MIN + cepmsg->fwdTol;
  VBX_DEBUG(VBX_print("CEPMSG_initialize: cepmsg->fwdTolOff = %ld\n", cepmsg->fwdTolOff));
  cepmsg->bwdTol    = - (cepmsgattr->maxReject);
  VBX_DEBUG(VBX_print("CEPMSG_initialize: cepmsg->bwdTol    = %ld\n", cepmsg->bwdTol));
  cepmsg->bwdTolOff = SN_MAX + cepmsg->bwdTol;
  VBX_DEBUG(VBX_print("CEPMSG_initialize: cepmsg->bwdTolOff = %ld\n", cepmsg->bwdTolOff));
  ICEPATTR_getVecLen(cepmsgattr->frameAttr, &cepmsg->cepVecLen);
  VBX_DEBUG(VBX_print("CEPMSG_initialize: maxReject = %ld, maxGapLen = %ld, cepVecLen = %d\n",
                      cepmsgattr->maxReject, cepmsgattr->maxGapLen, cepmsg->cepVecLen));

   /* Make sure a mailbox has been specified */
  if (!cepmsg->mailbox) {
    IFACE_setError((IFACE) igen, EXC_NOMAILBOX);
    return(FALSE);
  }
   /* Create a read message port for the mailbox */
  MSG_destroy(cepmsg->incoming);
  cepmsg->incoming = MSG_create(cepmsg->memory, cepmsg->mailbox);
   /* Create a write message port for the mailbox */
  MSG_destroy(cepmsg->outgoing);
  cepmsg->outgoing = MSG_create(cepmsg->memory, cepmsg->mailbox);
  if (!cepmsg->incoming || !cepmsg->outgoing) {
    IFACE_setError((IFACE) igen, EXC_CANTCREATEVBXMSG);
    return(FALSE);
  }

   /* Get the vecLen in the cepstral attributes block (in ISRC) */
  if (!ISRC_getAttributes(cepmsg->isrc, (IATTR *) &icepattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) cepmsg->isrc));
    return(FALSE);
  }
  ICEPATTR_getVecLen(icepattr, &oldCepVecLen);

   /* If there is no CLASS of cepstral frames, or if the cepVecLen or the classSize has changed, (re)create the CLASS */
  if (!cepmsg->frames || cepmsg->cepVecLen != oldCepVecLen || cepmsgattr->classSize != cepmsg->classSize) {
    VBX_DEBUG(VBX_print("CEPMSG_initialize: (re)creating the CLASS of cepframes (old Class size = %u)\n", cepmsg->classSize));
    if (cepmsg->frames && !CLASS_destroy(cepmsg->frames, NULL, NULL)) {
      IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
      return(FALSE);
    }
    cepmsg->classSize = cepmsgattr->classSize;
    
    cepmsg->frames = CLASS_create(cepmsg->memory, (UInt) cepmsg->classSize, cepmsg->cepVecLen * sizeof(CEPEL), NULL, NULL);
    VBX_DEBUG(VBX_print("CEPMSG_initialize: new CLASS of %u cepstral frames @%p\n", cepmsg->classSize, cepmsg->frames));
    if (!cepmsg->frames) {
      IFACE_setError((IFACE) igen, EXC_CANTCREATECLASS);
      return(FALSE);
    }
  }

   /* If the cepVecLen has changed, promulgate the change to the sink */
  if (oldCepVecLen != cepmsg->cepVecLen) {
    ICEPATTR_setVecLen(icepattr, cepmsg->cepVecLen);

     /* Get the sink associated with the source (output) interface */
    sink = ISRC_sink(cepmsg->isrc);
     /* If there is none, register an exception */
    if (!sink) {
      IFACE_setError((IFACE) igen, EXC_NOSNK);
      return(FALSE);
    }

     /* Pass the cep attributes on to the sink, if possible */
    if (ISNK_setAttributes(sink, (IATTR) icepattr)) {
       /* If successfully passed, return TRUE */
      return(TRUE);
    } else {
       /* Otherwise, register the exception and return FALSE */
      IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
      return(FALSE);
    }
  } else {
    return(TRUE);
  }
}


 /*** SOURCE METHODS ***/

static Bool
CEPMSG_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a cep frame via the source (output) interface.  If this is a
 * request for a particular SN, end-of-utterance is ignored if encountered
 * before that SN is reached; otherwise, it is reported as an exception.
 *---------------------------------------------------------------------------*/
{
  CEPMSG    cepmsg;
  VBXMSG    message;
  V_Bool    found;
  OBJT      object;
  V_Int     command;
  V_SN      sn, targetSN, snDiff;
  V_Mel   * frame;
  V_Err     status;
  V_Int     viseerror;
  EXC       exception;
  V_Int     i;

  assert(isrc && snP);

  cepmsg = (CEPMSG) IFACE_object((IFACE) isrc);
  message = cepmsg->incoming;

  if (outP) *outP = NULL;

   /* Can't go BACKWARD */
  if (dir == BACKWARD) {
    IFACE_setError((IFACE) isrc, EXC_NOBACKWARD);
    return(FALSE);
  }

   /* Set the target to the desired SN, which is negative if the next available SN will do */
  targetSN = *snP;

   /* Block on the message port until we get a message we want */
  object = NULL;
  while ((status = CEPMSG_readMessage(cepmsg, &command)) == VISESUCCESS) {
    if (command == _ACOUSTICFEATURES) {
      if (MSG_readSN(message, &sn)) {
         /* If any available SN will do, then this one will */
        if (targetSN < 0) targetSN = sn;
        snDiff = sn - targetSN;
        CEPMSG_FRAME(VBX_print("  CEPMSG_getFrame: readSN = " QuadFmt(-) ", targetSN = " QuadFmt(-) "\n", sn, targetSN));
         /* If the current SN is not at the target or less than maxGapLen beyond it ... */
        if (!INFWDTOL(snDiff)) {
           /* If the current SN is less than maxReject before it, reject it and keep on looking */
          if (INBWDTOL(snDiff)) {
            MSG_closeRead(message, &status);
            continue;
           /* Otherwise, the current SN is outside of the acceptable range */
          } else {
            MSG_closeRead(message, &status);
            IFACE_setError((IFACE) isrc, EXC_SEQNUMGAP);
            VBX_DEBUG(VBX_print("  CEPMSG_getFrame SN gap (sn = %d, target = %d, btol = %d, ftol = %d) from FE\n",
                                (Int) sn, (Int) targetSN, (Int) cepmsg->fwdTol, (Int) -cepmsg->bwdTol));
            return(FALSE);
          }
        }
         /* If an unused cepstral frame OBJT is available, fill it with the cepstral data */
        if ((object = OBJT_new(cepmsg->frames))) {
          frame = (V_Mel *) &OBJT_contents(object);
          if (!MSG_readMels(message, frame, 2 * cepmsg->cepVecLen)) {
            OBJT_free(object);
          } else if (outP) {
             /* Report this frame to the caller */
            *snP = sn;
            *outP = object;
          }
         /* Otherwise, report that the class of cepstral frame objects is empty */
        } else {
          MSG_closeRead(message, &status);
          IFACE_setError((IFACE) isrc, EXC_CEPCLASSEMPTY);
          return(FALSE);
        }
      }
      if (MSG_closeRead(message, &status)) {
        IFACE_setError((IFACE) isrc, EXC_NONE);
        return(TRUE);
      } else {
        IFACE_setError((IFACE) isrc, EXC_CANTGETFEATURE);
        return(FALSE);
      }
    } else if (command == _EXCEPTION) {
      MSG_readInt(message, &viseerror);
      MSG_closeRead(message, &status);
       /* Ignore EODATA if seeking a specific SN */
      if (viseerror == ENDOFDATA && targetSN >= 0) {
        continue;
       /* Otherwise, report the exception */
      } else {
        exception = viseerror == ENDOFDATA ? EXC_EOU : viseerror == SNWRAPAROUND ? EXC_SNWRAPAROUND : EXC_UNEXPECTED;
        VBX_DEBUG(VBX_print("  CEPMSG_getFrame: got EXC %d from FE\n", (Int) exception));
        IFACE_setError((IFACE) isrc, exception);
        return(FALSE);
      }
    } else {
       /* The message is already closed and acknowledged */
      VBX_DEBUG(VBX_print("  CEPMSG_getFrame: received %s command\n", VISECMD_string((V_Cmd) command)));
      IFACE_setError((IFACE) isrc, command == _ABORT ? EXC_ABORT : EXC_UNEXPECTED);
      return(FALSE);
    }
  }

  if (status != VISESUCCESS && status != NOMESSAGES) {
    VBX_DEBUG(VBX_print("  CEPMSG_getFrame: could not acknowledge inappropriate %s command\n", VISECMD_string((V_Cmd) command)));
     /* Any other exception means acknowledgement of an inappropriate command was impossible */
    if (outP) OBJT_free(*outP);
    IFACE_setError((IFACE) isrc, EXC_CANTACKNOWLEDGE);
    return(FALSE);
  }

  IFACE_setError((IFACE) isrc, EXC_NONE);
  return(TRUE);
}


static Bool
CEPMSG_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of a cep frame via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(CEPMSG_getFrame(isrc, dir, snP, NULL));
}


static V_Err
CEPMSG_readMessage(CEPMSG cepmsg, V_Int * command)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * This function blocks until it gets a message on cepmsg->incoming, and loops
 * back for the next message until it receives an _ACOUSTICFEATURES,
 * _EXCEPTION or _ABORT command.  When it encounters an _ACOUSTICFEATURES or
 * _EXCEPTION command, it reports receiving the command and returns
 * VISESUCCESS, but does not close the message or acknowledge it.  IN THESE
 * TWO CASES THE CALLER MUST CLOSE THE INCOMING MESSAGE after reading its
 * contents.  When it encounters an _ABORT command, it acknowledges it and
 * returns VISESUCCESS.  Otherwise, it ignores command messages, just
 * sending an acknowledgment of having received them back to their
 * sender with an UNTIMELYCOMMAND or ILLEGALCOMMAND status and
 * looping back for the next command.  If it fails trying to send
 * an acknowledgment, it returns the status of the failure.
 *---------------------------------------------------------------------------*/
{
  V_Uns    senderId;
  V_ULong  length;
  V_Bool   success;
  V_Err    status = VISESUCCESS;

   /* Open the next command */
  while ((success = MSG_openRead(cepmsg->incoming, &senderId, &length, command, TRUE, &status))) {

    /* VBX_DEBUG(VBX_print("  CEPMSG_readMessage: received %s command from sender %d\n", VISECMD_string((V_Cmd) *command), senderId)); */

    if (*command == _ACOUSTICFEATURES || *command == _EXCEPTION) {
      break;
    } else if (*command == _ABORT) {
      status = CEPMSG_acknowledge(cepmsg, *command, VISESUCCESS);
      break;
    } else if (0 <= *command && *command < NUMVISECOMMANDS) {
      status = CEPMSG_acknowledge(cepmsg, *command, UNTIMELYCOMMAND);
    } else {
      status = CEPMSG_acknowledge(cepmsg, *command, ILLEGALCOMMAND);
    }
    if (status) break;

  }

  if (status) VBX_DEBUG(VBX_print("  CEPMSG_readMessage: reply status = %s\n", VISEERR_string(status)));

  return(status);
}


static V_Err
CEPMSG_acknowledge(CEPMSG cepmsg, V_Int command, V_Err status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Send an acknowledgement to the sender
 *---------------------------------------------------------------------------*/
{
  V_Uns  source = MSG_correspondentId(cepmsg->incoming);
  V_Err  vstatus;

  MSG_closeRead(cepmsg->incoming, &vstatus);
  if (MSG_openWrite(cepmsg->outgoing, source, (V_ULong) 2, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeInt(cepmsg->outgoing, command);
    MSG_writeInt(cepmsg->outgoing, (V_Int) status);
    MSG_closeWrite(cepmsg->outgoing, &vstatus);
  }

  return(vstatus);
}


 /*** CEPMSGATTR Methods ***/

IGEN
CEPMSGATTR_create(IMemory * memory)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPMSG attributes object
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR   cepmsgattr = (CEPMSGATTR) memory->Calloc(memory, 1, sizeof(cepmsgattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(memory, (OBJ) cepmsgattr);
  ICEPMSGATTR  icepmsgattr = ICEPMSGATTR_create(igen);

   /* Set defaults */
  cepmsgattr->maxReject = DFLT_MAXFRAMES;
  cepmsgattr->maxGapLen = DFLT_MAXGAPLEN;

  return(igen);
}


 /*** ICEPMSGATTR METHODS ***/

static ICEPMSGATTR
ICEPMSGATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICEPMSGATTR interface
 *---------------------------------------------------------------------------*/
{
  ICEPMSGATTR  icepmsgattr;

  if ((icepmsgattr = (ICEPMSGATTR) IFACE_calloc((IFACE) igen, 1, sizeof(icepmsgattr_t)))) {
    IFACE_setClass((IFACE) icepmsgattr, &ICEPMSGATTR_Class);
    IFACE_setObject((IFACE) icepmsgattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) icepmsgattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) icepmsgattr);
  }

  return(icepmsgattr);
}


Bool
ICEPMSGATTR_setClassSize(ICEPMSGATTR icepmsgattr, Uns classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr;

  assert(icepmsgattr);
  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) icepmsgattr);

  cepmsgattr->classSize = classSize;

  return(TRUE);
}


Bool
ICEPMSGATTR_getClassSize(ICEPMSGATTR icepmsgattr, Uns * classSizePtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr;

  assert(icepmsgattr);
  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) icepmsgattr);

  *classSizePtr = cepmsgattr->classSize;

  return(TRUE);
}


Bool
ICEPMSGATTR_setFrameAttr(ICEPMSGATTR icepmsgattr, ICEPATTR icepattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the attributes of a cepstral frame
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr;

  assert(icepmsgattr);
  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) icepmsgattr);

  cepmsgattr->frameAttr = icepattr;

  return(TRUE);
}


Bool
ICEPMSGATTR_setMaxGapLen(ICEPMSGATTR icepmsgattr, SN maxGapLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the maximum allowable sequence number gap
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr;

  assert(icepmsgattr);
  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) icepmsgattr);

  cepmsgattr->maxGapLen = maxGapLen;

  return(TRUE);
}


Bool
ICEPMSGATTR_setMaxReject(ICEPMSGATTR icepmsgattr, SN maxReject)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the maximum number of bytes to discard to reach the desired frame
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr;

  assert(icepmsgattr);
  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) icepmsgattr);

  cepmsgattr->maxReject = maxReject;

  return(TRUE);
}


Bool
ICEPMSGATTR_setMailbox(ICEPMSGATTR icepmsgattr, IMBox * mailbox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the mailbox for communicating with the feature extractor
 *---------------------------------------------------------------------------*/
{
  CEPMSGATTR  cepmsgattr;

  assert(icepmsgattr);
  cepmsgattr = (CEPMSGATTR) IFACE_object((IFACE) icepmsgattr);

  cepmsgattr->mailbox = mailbox;

  return(TRUE);
}
