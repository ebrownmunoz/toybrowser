/* cepmbox.c - a cepstral frame source which reads from a mailbox
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 26-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "cep.h"
#include "cepmbox.h"
#include "class.h"
#include "cmd.h"
#include "iattr.h"
#include "cepattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "mbox.h"
#include "pack.h"
#include "packets.h"
#include "sn.h"
#include "sock.h"
#include "sockets.h"
#include <assert.h>


#ifdef DEBUG
#define DBX(P)  P
#else
#define DBX(P)
#endif

#define DFLT_MAXFRAMES 360000

/* The Class of CEPMBATTR (CEPMBOX Attributes) Interface Objects */
static iclass_t ICEPMBATTR_Class = {ICEPMBATTRID};

/* CEPMBOX attributes */
typedef struct cepmbattr_s {
  MBOX      mbox;          /* The mbox for incoming cepstral coefficients */
  Int       classSize;     /* The size of the CLASS of cepstral frame objects */
  Int       cepVecLen;     /* The number of coefficients in a cepstral frame */
  Int       tolerance;     /* The maximum number of frames getFrame will read */
} cepmbattr_t, * CEPMBATTR;

/* A CEPMBOX descriptor */
typedef struct cepmbox_s {
  IMemory * imem;          /* The memory allocator */
  ISRC      isrc;          /* CEPMBOX's (cepstral) source interface (output) */
  IGEN      igenattr;      /* Interface to the cepstral attributes block */
  MBOX      mbox;          /* The mbox of cepstral coefficients */
  Int       classSize;     /* The size of the CLASS of cepstral frame OBJTs */
  CLASS     cepstra;       /* The CLASS of cepstral frame OBJTs */
  Int       cepVecLen;     /* The number of coefficients in a cepstral vector */
  Int       tolerance;     /* The maximum number of frames getFrame will read */
} cepmbox_t, * CEPMBOX;

/* Private functions */

static Bool  CEPMBOX_annihilate(IGEN igen);
static Bool  CEPMBOX_initialize(IGEN igen, IATTR iattr);
static Bool  CEPMBOX_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  CEPMBOX_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static ICEPMBATTR  ICEPMBATTR_create(IGEN igen);


/*** CEPMBOX Methods ***/

IGEN
CEPMBOX_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPMBOX object
 *---------------------------------------------------------------------------*/
{
  CEPMBOX   cepmbox = (CEPMBOX) imem->Calloc(imem, 1, sizeof(cepmbox_t), OOPS_MEMORY);
  IGEN      igen = IGEN_create(imem, (OBJ) cepmbox);
  ISRC      isrc = ISRC_create(igen);
  IGEN      igenattr = CEPATTR_create(imem);

  /* Initialize the object */
  cepmbox->imem = imem;
  cepmbox->isrc = isrc;
  cepmbox->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, CEPMBOX_annihilate);
  IGEN_setInitializer(igen, CEPMBOX_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, CEPMBOX_getFrame);
  ISRC_setFrameAvailable(isrc, CEPMBOX_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, ICEPATTRID));

  return(igen);
}


/*** GENERIC METHODS ***/

static Bool
CEPMBOX_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a cepmbox object
 * This supplements the generic IGEN_annihilate() by annihilating the
 * CLASS of cepstral frame OBJTs and the cepstral attributes block
 *---------------------------------------------------------------------------*/
{
  CEPMBOX cepmbox = (CEPMBOX) IFACE_object((IFACE) igen);

  /* Annihilate the CLASS of cepstral frame OBJTs */
  if (cepmbox->cepstra && !CLASS_destroy(cepmbox->cepstra, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Annihilate the cepstral attributes object */
  if (!IGEN_annihilate(cepmbox->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) cepmbox->igenattr));
    return(FALSE);
  }

  return(TRUE);
}


static Bool
CEPMBOX_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a cepmbox according to a cepmbox attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR   cepmbattr = (CEPMBATTR) IFACE_object((IFACE) iattr);
  CEPMBOX     cepmbox = (CEPMBOX) IFACE_object((IFACE) igen);
  ICEPATTR    icepattr;
  ISNK        sink;
  Int         oldCepVecLen;

  /* Copy attributes */
  cepmbox->mbox = cepmbattr->mbox;
  cepmbox->tolerance = cepmbattr->tolerance;
  cepmbox->cepVecLen = cepmbattr->cepVecLen;

  /* Get the vecLen in the cepstral attributes block (in ISRC) */
  if (!ISRC_getAttributes(cepmbox->isrc, (IATTR *) &icepattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) cepmbox->isrc));
    return(FALSE);
  }
  ICEPATTR_getVecLen(icepattr, &oldCepVecLen);

  /* If the cepVecLen or the classSize has changed, re-create the CLASS */
  if (cepmbox->cepVecLen != oldCepVecLen || cepmbattr->classSize != cepmbox->classSize) {
    if (cepmbox->cepstra)
      CLASS_destroy(cepmbox->cepstra, NULL, NULL);
    cepmbox->cepstra = CLASS_create(cepmbox->imem, cepmbattr->classSize, cepmbox->cepVecLen * sizeof(CEPEL), NULL, NULL);
    cepmbox->classSize = cepmbattr->classSize;
  }

  /* If the cepVecLen has changed, promulgate the change to the sink */
  if (oldCepVecLen != cepmbox->cepVecLen) {
    ICEPATTR_setVecLen(icepattr, cepmbox->cepVecLen);

    /* Get the sink associated with the source (output) interface */
    sink = ISRC_sink(cepmbox->isrc);
    /* If there is none, register an exception */
    if (!sink) {
      IFACE_setError((IFACE) igen, EXC_NOSNK);
      return(FALSE);
    }

    /* Pass the cepstral attributes on to the sink, if possible */
    if (ISNK_setAttributes(sink, (IATTR) icepattr)) {
      /* If successfully passed, return TRUE */
      return(TRUE);
    } else {
      /* Otherwise, register the exception and return FALSE */
      IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
      return(FALSE);
    }
  }
}


/*** SOURCE METHODS ***/

static Bool
CEPMBOX_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a cepstral output frame via the source (output) interface
 * If this is a request for a particular SN, end-of-utterance is ignored if
 * encountered before that SN is reached; otherwise, it is reported as an
 * exception.
 *---------------------------------------------------------------------------*/
{
  CEPMBOX   cepmbox;
  PACK      packet;
  cep_t     frame;
  CEP       framePtr;
  SN        sn, minSN;

  assert(isrc && snP);

  cepmbox = (CEPMBOX) IFACE_object((IFACE) isrc);

  /* Set the target to the desired SN, or -1 if the next SN will do */
  if ((sn = *snP) < 0) sn = -1;
  /* Make sure *snP is less than the target SN until a frame is read */
  *snP = sn - 1;
  /* Set a lower limit on the range of SN's to be searched */
  minSN = sn - cepmbox->tolerance;

  /* Can't go BACKWARD */
  if (dir == BACKWARD) {
    IFACE_setError((IFACE) isrc, EXC_NOBACKWARD);
    return(FALSE);
  }

  /* Read frames from mailbox until the desired or a greater SN is reached */
  do {
    packet = (PACK) MBOX_remove(cepmbox->mbox);
    switch (packet->id) {
      case CEP_FRAME:               /* Cepstral frame */
        framePtr = (CEP) & frame;
        PACK_destroy(packet, (caddr_t *) & framePtr);
        DBX(printf("CEPMBOX_getFrame: CEP sn: %d; c[0]: %.5f\n",
                frame.seqNum, frame.data[0]));
        *snP = frame.seqNum;
        break;
      case STAT_MSG:                /* End of utterance */
        /* Ignore EOU if seeking a specific SN */
        DBX(printf("CEPMBOX_getFrame: EOU\n"));
        PACK_free(packet);
        if (sn >= 0) continue;
        IFACE_setError((IFACE) isrc, EXC_EOU);
        return FALSE;
      default:                      /* Unexpected packet id */
        DBX(printf("CEPMBOX_getFrame: UNX [%d]\n", packet->id));
        IFACE_setError((IFACE) isrc, EXC_BADPACKETID);
        PACK_free(packet);
        return FALSE;
    }
  } while (sn >= 0 && *snP < sn && minSN <= *snP);

  /* If reaching the desired SN would mean reading too many frames, quit */
  if (*snP < minSN) {
    IFACE_setError((IFACE) isrc, EXC_SEQNUMGAP);
    return(FALSE);
  }

  /* If requested, create a frame object with the desired cepstrum */
  if (outP) {
    *outP = OBJT_new(cepmbox->cepstra);
    /* If an OBJT is available, fill it with the cepstral data */
    if (*outP) {
      memcpy(&OBJT_contents(*outP), frame.data,
             cepmbox->cepVecLen * sizeof(CEPEL));
    /* Otherwise, report an exception */
    } else {
      IFACE_setError((IFACE) isrc, EXC_CEPCLASSEMPTY);
      return(FALSE);
    }
  }

  return(TRUE);
}


static Bool
CEPMBOX_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of a cepstral frame via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(CEPMBOX_getFrame(isrc, dir, snP, NULL));
}


/*** CEPMBATTR Methods ***/

IGEN
CEPMBATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPMBOX attributes object
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR   cepmbattr = (CEPMBATTR) imem->Calloc(imem, 1, sizeof(cepmbattr_t), OOPS_MEMORY);
  IGEN        igen = IGEN_create(imem, (OBJ) cepmbattr);
  ICEPMBATTR  icepmbattr = ICEPMBATTR_create(igen);

  /* Set defaults */
  cepmbattr->tolerance = DFLT_MAXFRAMES;

  return(igen);
}


/*** ICEPMBATTR METHODS ***/

static ICEPMBATTR
ICEPMBATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICEPMBATTR interface
 *---------------------------------------------------------------------------*/
{
  ICEPMBATTR  icepmbattr;

  if ((icepmbattr = (ICEPMBATTR) IFACE_calloc((IFACE) igen, 1, sizeof(icepmbattr_t)))) {
    IFACE_setClass((IFACE) icepmbattr, &ICEPMBATTR_Class);
    IFACE_setObject((IFACE) icepmbattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) icepmbattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) icepmbattr);
  }

  return(icepmbattr);
}


Bool
ICEPMBATTR_setMailbox(ICEPMBATTR icepmbattr, MBOX mbox)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the mailbox for incoming cepstral frames
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR  cepmbattr;

  assert(icepmbattr);
  cepmbattr = (CEPMBATTR) IFACE_object((IFACE) icepmbattr);

  cepmbattr->mbox = mbox;

  return(TRUE);
}


Bool
ICEPMBATTR_setClassSize(ICEPMBATTR icepmbattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR  cepmbattr;

  assert(icepmbattr);
  cepmbattr = (CEPMBATTR) IFACE_object((IFACE) icepmbattr);

  cepmbattr->classSize = classSize;

  return(TRUE);
}


Bool
ICEPMBATTR_getClassSize(ICEPMBATTR icepmbattr, Int * classSizePtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR  cepmbattr;

  assert(icepmbattr);
  cepmbattr = (CEPMBATTR) IFACE_object((IFACE) icepmbattr);

  *classSizePtr = cepmbattr->classSize;

  return(TRUE);
}


Bool
ICEPMBATTR_setCepVecLen(ICEPMBATTR icepmbattr, Int cepVecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of cepstral vectors
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR  cepmbattr;

  assert(icepmbattr);
  cepmbattr = (CEPMBATTR) IFACE_object((IFACE) icepmbattr);

  cepmbattr->cepVecLen = cepVecLen;

  return(TRUE);
}


Bool
ICEPMBATTR_setTolerance(ICEPMBATTR icepmbattr, Int tolerance)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the maximum allowable number of frames to read to reach the desired one
 *---------------------------------------------------------------------------*/
{
  CEPMBATTR  cepmbattr;

  assert(icepmbattr);
  cepmbattr = (CEPMBATTR) IFACE_object((IFACE) icepmbattr);

  cepmbattr->tolerance = tolerance;

  return(TRUE);
}
