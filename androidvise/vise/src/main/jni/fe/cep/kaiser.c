#include "pthr.h"
#include "floorfix.h"
#include "imemory.h"
#include "kaiser.h"

typedef struct kaiser_s {
  IMemory * imem;
  Bool      start;
  Short     last;
} kaiser_t;

#define MAXFRAMELEN  256

static Val Kaiser256[MAXFRAMELEN/2] =
{
#ifndef FIXED_POINT
 0.079999998212,  0.080139629543,  0.080558441579,  0.081256181002, 
 0.082232415676,  0.083486564457,  0.085017859936,  0.086825370789, 
 0.088908009231,  0.091264493763,  0.093893408775,  0.096793152392, 
 0.099961966276,  0.103397928178,  0.107098944485,  0.111062772572, 
 0.115287013352,  0.119769088924,  0.124506287277,  0.129495739937, 
 0.134734392166,  0.140219092369,  0.145946487784,  0.151913121343, 
 0.158115342259,  0.164549425244,  0.171211436391,  0.178097337484, 
 0.185202941298,  0.192523941398,  0.200055897236,  0.207794234157, 
 0.215734258294,  0.223871126771,  0.232199922204,  0.240715593100, 
 0.249412938952,  0.258286714554,  0.267331510782,  0.276541829109, 
 0.285912126303,  0.295436650515,  0.305109649897,  0.314925253391, 
 0.324877500534,  0.334960371256,  0.345167696476,  0.355493307114, 
 0.365930914879,  0.376474231482,  0.387116789818,  0.397852182388, 
 0.408673882484,  0.419575303793,  0.430549830198,  0.441590785980, 
 0.452691495419,  0.463845223188,  0.475045174360,  0.486284554005, 
 0.497556567192,  0.508854329586,  0.520170986652,  0.531499743462, 
 0.542833566666,  0.554165720940,  0.565489292145,  0.576797366142, 
 0.588083088398,  0.599339663982,  0.610560178757,  0.621737837791, 
 0.632865905762,  0.643937587738,  0.654946208000,  0.665884971619, 
 0.676747381687,  0.687526702881,  0.698216497898,  0.708810269833, 
 0.719301521778,  0.729683935642,  0.739951193333,  0.750097036362, 
 0.760115325451,  0.770000040531,  0.779745042324,  0.789344549179, 
 0.798792660236,  0.808083713055,  0.817211925983,  0.826171934605, 
 0.834958136082,  0.843565285206,  0.851988136768,  0.860221624374, 
 0.868260681629,  0.876100480556,  0.883736193180,  0.891163229942, 
 0.898377060890,  0.905373334885,  0.912147819996,  0.918696343899, 
 0.925014972687,  0.931099891663,  0.936947345734,  0.942553818226, 
 0.947915911674,  0.953030347824,  0.957894027233,  0.962504029274, 
 0.966857492924,  0.970951855183,  0.974784553051,  0.978353321552, 
 0.981655955315,  0.984690487385,  0.987455010414,  0.989947915077, 
 0.992167651653,  0.994112849236,  0.995782375336,  0.997175216675, 
 0.998290479183,  0.999127507210,  0.999685823917,  0.999965071678 
#else  /* Integer, scaled by factor 256 */
    20,     20,     20,     20,     21,     21,     21,     22,
    22,     23,     24,     24,     25,     26,     27,     28,
    29,     30,     31,     33,     34,     35,     37,     38,
    40,     42,     43,     45,     47,     49,     51,     53,
    55,     57,     59,     61,     63,     66,     68,     70,
    73,     75,     78,     80,     83,     85,     88,     91,
    93,     96,     99,    101,    104,    107,    110,    113,
   115,    118,    121,    124,    127,    130,    133,    136,
   138,    141,    144,    147,    150,    153,    156,    159,
   162,    164,    167,    170,    173,    176,    178,    181,
   184,    186,    189,    192,    194,    197,    199,    202,
   204,    206,    209,    211,    213,    215,    218,    220,
   222,    224,    226,    228,    229,    231,    233,    235,
   236,    238,    239,    241,    242,    243,    245,    246,
   247,    248,    249,    250,    251,    252,    252,    253,
   253,    254,    254,    255,    255,    255,    255,    255
#endif  /* FIXED__POINT */
};


KAISER
KAISER_create(IMemory *imem)
{
  KAISER kaiser;

  kaiser = (KAISER) imem->Calloc(imem, 1, sizeof(kaiser_t), FAST_MEMORY);
  kaiser->start = TRUE;
  kaiser->imem = imem;
  return(kaiser);
}

void
KAISER_destroy(KAISER kaiser)
{
  kaiser->imem->Free(kaiser->imem, kaiser);
}  

void
KAISER_reset(KAISER kaiser)
{
  kaiser->start = TRUE;
}  

void
KAISER_firstDif(KAISER kaiser, Short * input, Val * output, Int size)
{
  Int   i;

  if (kaiser->start) {
    output[0] = (Val)0;
    kaiser->start = FALSE;
  } else {
    output[0] = (Val)((Int)input[0] - (Int)kaiser->last);
  }
  for (i = 1; i < size; i++)
    output[i] = (Val)((Int)input[i] - (Int)input[i - 1]);

  kaiser->last = input[size - 1];
}

void
KAISER_preEmph(KAISER kaiser, Short * input, Val * output, Int shift, Int size)
{
  Int   i;

  if (kaiser->start) {
    output[0] = (Val)0;
    kaiser->start = FALSE;
  } else {
    output[0] = (Val)(((Int)input[0] - (Int)kaiser->last) >> shift);
  }
  for (i = 1; i < size; i++)
    output[i] = (Val)(((Int)input[i] - (Int)input[i - 1]) >> shift);

  kaiser->last = input[size - 1];
}

Val
KAISER_apply256(Val * input, Val * output)
{
  Int   i, j, k;
  Val   val;
#ifdef FIXED_POINT
   /* To prevent overflow */
  Quad  e = 0;
#else  /* Floating Point */
  Float e = 0.;
#endif  /* FIXED__POINT */

  for (i = 0, j = MAXFRAMELEN/2; j; i++) {
    j--;
    output[i] = NORMBACK(Kaiser256[i] * input[i]);
    val = input[i];
    e += val * val;
    k = j+(MAXFRAMELEN/2);
    output[k] = NORMBACK(Kaiser256[i] * input[k]);
    val = input[k];
    e += val * val;
  }
#ifdef FIXED_POINT
   /* Hardlimit energy so log2b(1+e) will make sense */
  val = (e >= VLONGMAX) ? VLONGMAX - 1 : (Int)e;
#else  /* Floating Point */
  val = e;
#endif  /* FIXED__POINT */

  return val;
}
  
 /* The same as above except that normalizes energy per sample. */
Val
KAISER_window256(Val * input, Val * output)
{
  Int   i, j, k;
#ifdef FIXED_POINT
   /* To prevent overflow */
  Quad  e = 0, t;
#else  /* Floating Point */
  Float e = 0., t;
#endif  /* FIXED__POINT */

  for (i = 0, j = MAXFRAMELEN/2; j; i++) {
    j--;
    output[i] = NORMBACK(Kaiser256[i] * input[i]);
    t = input[i];
    e += t * t;
    k = j+(MAXFRAMELEN/2);
    output[k] = NORMBACK(Kaiser256[i] * input[k]);
    t = input[k];
    e += t * t;
  }
   /* Normalize per sample here */
  e /= (Val)MAXFRAMELEN;

  return (Val)e;
}
