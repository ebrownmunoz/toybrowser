 /*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Enforces Front End parameter consistency across FE(Cep) versions.  
 *
 *---------------------------------------------------------------------------*/

#include "pthr.h"
#include "types.h"
#include "feversion.h"
#include "mel.h"

typedef struct cepCfg_s {
  Int     feVersion;
  Int     sampleRate;
  Int     frameLen;
  Int     frameShift;
  Int     cepVecLen;
  Int     otherProc;
  meltype melType;
} cepCfg_t, *CEPCFG;

  /* FEVersion                SRate  FLen FShf Vec OtherP meltype */
static cepCfg_t CepCfgTbl[] = {
  { FE_VISE11KHz,             11025, 256, 141,  8,     0, VISE11   },
  { FE_VISE10KHz,             10000, 256, 128,  8,     0, VISE10   },
  { FE_VISE8KHz,               8000, 256, 102,  8,     0, VISE8    },
  { FE_VISE11KHzNSS,          11025, 256, 141,  8, NMASS, VISE11   },
  { FE_VISE8KHzNSS,            8000, 256, 102,  8, NMASS, VISE8    },
  { FE_FVISE16KHzOldNorm,     16000, 410, 160, 13,     0, FLEX16OLD},
  { FE_FVISE8KHzRastaOldNorm,  8000, 205,  80, 13, RASTA, FLEX8OLD },
  { FE_FVISE16KHzExternalDSP, 16000, 410, 160, 13,     0, FLEX16OLD},
  { FE_FVISE8KHzExternalDSP,   8000, 205,  80, 13, RASTA, FLEX8OLD },
  { FE_BADVERSION,                0,   0,   0,  0,     0, BADMEL   }
};

static Int
indexFromVersion(Int version)
{
  Int i;

  for (i = 0; CepCfgTbl[i].feVersion != FE_BADVERSION; i++)
    if (version == CepCfgTbl[i].feVersion)
      break;

  return i;
}

 /* Public Methods */
Bool
FEVER_getSampleRate(Int feVersion, Int * sampleRate)
{
  Int i;

  i = indexFromVersion(feVersion);
  *sampleRate = CepCfgTbl[i].sampleRate;

  return (FE_BADVERSION != CepCfgTbl[i].feVersion);
}

Bool
FEVER_getFrameLen(Int feVersion, Int * frameLen)
{
  Int i;

  i = indexFromVersion(feVersion);
  *frameLen = CepCfgTbl[i].frameLen;

  return (FE_BADVERSION != CepCfgTbl[i].feVersion);
}

Bool
FEVER_getFrameShift(Int feVersion, Int * frameShift)
{
  Int i;

  i = indexFromVersion(feVersion);
  *frameShift = CepCfgTbl[i].frameShift;

  return (FE_BADVERSION != CepCfgTbl[i].feVersion);
}

Bool
FEVER_getCepVecLen(Int feVersion, Int * cepVecLen)
{
  Int i;

  i = indexFromVersion(feVersion);
  *cepVecLen = CepCfgTbl[i].cepVecLen;

  return (FE_BADVERSION != CepCfgTbl[i].feVersion);
}

Bool
FEVER_getOtherProc(Int feVersion, Int * otherProc)
{
  Int i;

  i = indexFromVersion(feVersion);
  *otherProc = CepCfgTbl[i].otherProc;

  return (FE_BADVERSION != CepCfgTbl[i].feVersion);
}

meltype
FEVER_getMelType(Int feVersion)
{
  return (CepCfgTbl[indexFromVersion(feVersion)].melType);
}
