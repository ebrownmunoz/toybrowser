/* cepsn.c - a simple cepstral frame source
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 24-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: cepsn.c[1.0] Tue Jun  9 17:40:42 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "cep.h"
#include "cepattr.h"
#include "cepsn.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include <assert.h>


/* The Class of CEPSNATTR (CEPSN Attributes) Interface Objects */
static iclass_t ICEPSNATTR_Class = {ICEPSNATTRID};


/* A CEPSN descriptor */
typedef struct cepsn_s {
  ISRC   isrc;          /* CEPSN's cepstral source interface (output) */
  IGEN   igenattr;      /* Interface to the cepstral attributes block */
  CLASS  cepstra;       /* The CLASS of cepstral frame OBJTs */
  Int    cepVecLen;     /* The number of coefficients in a cepstral vector */
  SN     seqNum;        /* The last sequence number requested */
} cepsn_t, * CEPSN;


/* Private functions */

static Bool  CEPSN_annihilate(IGEN igen);
static Bool  CEPSN_initialize(IGEN igen, IATTR iattr);
static Bool  CEPSN_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  CEPSN_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static ICEPSNATTR  ICEPSNATTR_create(IGEN igen);


/*** CEPSN Methods ***/

IGEN
CEPSN_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPSN object
 *---------------------------------------------------------------------------*/
{
  CEPSN    cepsn = (CEPSN) imem->Calloc(imem, 1, sizeof(cepsn_t), OOPS_MEMORY);
  IGEN     igen = IGEN_create(imem, (OBJ) cepsn);
  ISRC     isrc = ISRC_create(igen);
  IGEN     igenattr = CEPATTR_create(imem);

  /* Initialize the object */
  cepsn->isrc = isrc;
  cepsn->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, CEPSN_annihilate);
  IGEN_setInitializer(igen, CEPSN_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, CEPSN_getFrame);
  ISRC_setFrameAvailable(isrc, CEPSN_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, ICEPATTRID));

  return(igen);
}


/*** GENERIC METHODS ***/

static Bool
CEPSN_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a cepsn object
 * This supplements the generic IGEN_annihilate() by annihilating the
 * cepstral attributes block
 *---------------------------------------------------------------------------*/
{
  CEPSN cepsn = (CEPSN) IFACE_object((IFACE) igen);

  /* Annihilate the cepstral attributes object */
  if (!IGEN_annihilate(cepsn->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) cepsn->igenattr));
    return(FALSE);
  } else {
    return(TRUE);
  }
}


static Bool
CEPSN_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a cepsn according to a cepsn attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  ICEPSNATTR  icepsnattr = (ICEPSNATTR) iattr;
  CEPSN       cepsn = (CEPSN) IFACE_object((IFACE) igen);
  ICEPATTR    icepattr;
  ISNK        sink;

  /* Get the class of cepstral frame objects */
  ICEPSNATTR_getClass(icepsnattr, &cepsn->cepstra);

  /* Compute the length of the cepstral vectors */
  cepsn->cepVecLen = CLASS_objectSize(cepsn->cepstra) / sizeof(CEPEL);
  if (!cepsn->cepVecLen) {
    IFACE_setError((IFACE) igen, EXC_OBJTSIZEZERO);
    return(FALSE);
  }

  /* Initialize the current sequence number */
  cepsn->seqNum = 0;

  /* Set the vecLen in the cepstral attributes block (in ISRC) */
  if (!ISRC_getAttributes(cepsn->isrc, (IATTR *) &icepattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) cepsn->isrc));
    return(FALSE);
  }
  ICEPATTR_setVecLen(icepattr, cepsn->cepVecLen);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(cepsn->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) igen, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the cepstral attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, (IATTR) icepattr)) {
    /* If successfully passed, return TRUE */
    return(TRUE);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
    return(FALSE);
  }
}


/*** CEPSTRAL SOURCE METHODS ***/

static Bool
CEPSN_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a cepstral output frame via the cepstral source interface
 *---------------------------------------------------------------------------*/
{
  CEPSN    cepsn;
  SN       sn = *snP;
  CEPEL  * cep;
  Int      i;

  assert(isrc && snP);

  cepsn = ((CEPSN) IFACE_object((IFACE) isrc));

  /* A negative SN is a request for the next SN in the specified direction */
  sn = (*snP < 0) ? cepsn->seqNum + dir : *snP;

  /* Honor only requests for frames with positive sequence numbers */
  if (sn >= 0) {
    /* If the request can be met, report and record the new SN */
    *snP = cepsn->seqNum = sn;
    /* If requested, create a frame object with the desired elements */
    if (outP) {
      *outP = OBJT_new(cepsn->cepstra);
      cep = (CEPEL *) &OBJT_contents(*outP);
      cep[0] = MEAN2VAL(((Float) (sn % 100000000)) / ((Float) 100000.0));
      for (i = 1; i < cepsn->cepVecLen; i++)
        cep[i] = cep[0];
    }
    return(TRUE);
  } else {
    /* Otherwise, register the error and return FALSE */
    IFACE_setError((IFACE) isrc, dir == FORWARD ? EXC_NOFORWARD : EXC_NOBACKWARD);
    return(FALSE);
  }
}


static Bool
CEPSN_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of a cepstral frame via the cepstral source interface
 *---------------------------------------------------------------------------*/
{
  return(CEPSN_getFrame(isrc, dir, snP, NULL));
}


/*** CEPSNATTR Methods ***/

IGEN
CEPSNATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CEPSN attributes object
 *---------------------------------------------------------------------------*/
{
  IGEN    igen = IGEN_create(imem, NULL);
  ICEPSNATTR  icepsnattr = ICEPSNATTR_create(igen);

  return(igen);
}


/*** ICEPSNATTR METHODS ***/

static ICEPSNATTR
ICEPSNATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICEPSNATTR interface
 *---------------------------------------------------------------------------*/
{
  ICEPSNATTR  icepsnattr;

  if ((icepsnattr = (ICEPSNATTR) IFACE_calloc((IFACE) igen, 1, sizeof(icepsnattr_t)))) {
    IFACE_setClass((IFACE) icepsnattr, &ICEPSNATTR_Class);
    IFACE_setObject((IFACE) icepsnattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) icepsnattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) icepsnattr);
  }

  return(icepsnattr);
}
