/* agcsub.c - a constant-subtracting automatic gain control
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 21-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created from pieces of agc.c
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "floorfix.h"
#include "agcsub.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include <assert.h>

#define  AGCSUB_EQUILIBRATIONUTTS  4
#define  AGCSUB_WARMUPSUBTRAHEND   6.5

/* The Class of AGCSUB Interface Objects */
static iclass_t IAGCSUB_Class = {IAGCSUBID};

/* The AGCSUB Interface */
typedef iface_t iagcsub_t;

/* The Class of AGCSUBATTR (AGCSUB Attributes) Interface Objects */
static iclass_t IAGCSUBATTR_Class = {IAGCSUBATTRID};

/* AGCSUB attributes */
typedef struct agcsubattr_s {
  Float    subtrahend;     /* The initial subtrahend */
  Float    newWeight;      /* The weight of maxEnergy in new subtrahend */
  Float    oldWeight;      /* The weight of old subtrahend in new */
  Float    minEnergy;      /* Reset value of maxEnergy */
} agcsubattr_t, * AGCSUBATTR;

/* An AGCSUB descriptor */
typedef struct agcsub_s {
  ISRC     isrc;           /* AGCSUB's source interface (output) */
  ISNK     isnk;           /* AGCSUB's sink interface (input) */
  Val      subtrahend;     /* What to subtract from the log-energy */
  Val      maxEnergy;      /* The maximum c0 (> minEnergy) since reset */
  Val      newWeight;      /* The weight of maxEnergy in new subtrahend */
  Val      oldWeight;      /* The weight of old subtrahend in new */
  Val      minEnergy;      /* Reset value of maxEnergy */
  Val      subtraWarmup;   /* Subtrahend while warmup */
  Val      subtraSaved;    /* Saved subtrahend for some state */
  Val      maxEnSaved;     /* Saved maxEnergy for some state */
  Int      resetCntSaved;  /* Saved resetCnt for some state */
  Int      resetCnt;       /* Counter: number of resets since initialization */
  Int      printSubFrms;   /* Print subtrahend after this many frames elapse */
  SN       sn;             /* Internal sequence number for the current frame */
} agcsub_t, * AGCSUB;


/* Private functions */

static IAGCSUB  IAGCSUB_create(IGEN igen);

static Bool  AGCSUB_initialize(IGEN igen, IATTR iattr);
static Bool  AGCSUB_setAttributes(ISNK isnk, IATTR iattr);
static Bool  AGCSUB_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  AGCSUB_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static IAGCSUBATTR  IAGCSUBATTR_create(IGEN igen);


/*** AGCSUB Methods ***/

IGEN
AGCSUB_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a constant-subtracting AGC
 *---------------------------------------------------------------------------*/
{
  AGCSUB     agcsub = (AGCSUB) imem->Calloc(imem, 1, sizeof(agcsub_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) agcsub);
  ISNK       isnk = ISNK_create(igen);
  ISRC       isrc = ISRC_create(igen);
  IAGCSUB    iagcsub = IAGCSUB_create(igen);
  IATTR      iattr;

  /* Initialize the object */
  agcsub->isnk = isnk;
  agcsub->isrc = isrc;

  /* Set up the generic interface */
  IGEN_setInitializer(igen, AGCSUB_initialize);

  /* DEBUG SETTING (CAV) */
  agcsub->printSubFrms = 100;
  
  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setAttributes(isrc, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the sink interface */
  ISNK_setSetAttributes(isnk, AGCSUB_setAttributes);

  /* Set up the source interface */
  ISRC_setGetFrame(isrc, AGCSUB_getFrame);
  ISRC_setFrameAvailable(isrc, AGCSUB_frameAvailable);

  return(igen);
}


Bool
IAGCSUB_reset(IAGCSUB iagcsub, Float offset)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the AGC with a new subtrahend
 *---------------------------------------------------------------------------*/
{
  AGCSUB  agcsub;
  Val     voffset;

  if (offset == (Float) 0.0)
    voffset = (Val)0;
  else
    voffset = MEAN2VAL(offset);

  assert(iagcsub);
  agcsub = (AGCSUB) IFACE_object((IFACE) iagcsub);
  agcsub->sn = 0;
  agcsub->resetCnt++;

  /* Compute the new subtrahend */
  agcsub->subtrahend = NORMBACK(agcsub->oldWeight * agcsub->subtrahend +
                       agcsub->newWeight * agcsub->maxEnergy) + voffset;

  /* Reset the maximum energy to the minimum allowed */
  agcsub->maxEnergy = agcsub->minEnergy;

  return(TRUE);
}


Bool
IAGCSUB_save(IAGCSUB iagcsub)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Save the state of the AGC (One level depth preservation)
 *---------------------------------------------------------------------------*/
{
  AGCSUB  agcsub;

  assert(iagcsub);
  agcsub = (AGCSUB) IFACE_object((IFACE) iagcsub);

  agcsub->subtraSaved = agcsub->subtrahend;
  agcsub->maxEnSaved = agcsub->maxEnergy;
  agcsub->resetCntSaved = agcsub->resetCnt;

  return(TRUE);
}

Bool
IAGCSUB_getSub(IAGCSUB iagcsub, Float * agcSub)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the actual value of what to subtract from the log-energy
 *---------------------------------------------------------------------------*/
{
  AGCSUB  agcsub;

  assert(iagcsub);
  agcsub = (AGCSUB) IFACE_object((IFACE) iagcsub);

  *agcSub = RESTOREMEAN((agcsub->resetCnt > AGCSUB_EQUILIBRATIONUTTS) ? agcsub->subtrahend : agcsub->subtraWarmup);

  return(TRUE);
}

Bool
IAGCSUB_setSub(IAGCSUB iagcsub, Float agcSub)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the actual value of what to subtract from the log-energy
 * This call replaces IAGCSUB_reset call
 *---------------------------------------------------------------------------*/
{
  AGCSUB  agcsub;

  assert(iagcsub);
  agcsub = (AGCSUB) IFACE_object((IFACE) iagcsub);
  agcsub->sn = 0;

  /* Make sure the supplied value will be used */
  agcsub->resetCnt = AGCSUB_EQUILIBRATIONUTTS + 1;

  agcsub->subtrahend = MEAN2VAL(agcSub);

  /* Reset the maximum energy to the minimum allowed */
  agcsub->maxEnergy = agcsub->minEnergy;

  return(TRUE);
}

Bool
IAGCSUB_restore(IAGCSUB iagcsub)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Restore the state of the AGC
 *---------------------------------------------------------------------------*/
{
  AGCSUB  agcsub;

  assert(iagcsub);
  agcsub = (AGCSUB) IFACE_object((IFACE) iagcsub);

  /* Check saving first */
  if (agcsub->resetCntSaved < 0)
    return FALSE;

  agcsub->subtrahend = agcsub->subtraSaved;
  agcsub->maxEnergy = agcsub->maxEnSaved;
  agcsub->resetCnt = agcsub->resetCntSaved;

  return(TRUE);
}

/*** IAGCSUB METHODS ***/

static IAGCSUB
IAGCSUB_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IAGCSUB interface
 *---------------------------------------------------------------------------*/
{
  IAGCSUB  iagcsub;

  if ((iagcsub = (IAGCSUB) IFACE_calloc((IFACE) igen, 1, sizeof(iagcsub_t)))) {
    IFACE_setClass((IFACE) iagcsub, &IAGCSUB_Class);
    IFACE_setObject((IFACE) iagcsub, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iagcsub, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iagcsub);
  }

  return(iagcsub);
}


/*** GENERIC METHODS ***/

static Bool
AGCSUB_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize an agcsub according to an agcsub attributes specification
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  AGCSUB      agcsub = (AGCSUB) IFACE_object((IFACE) igen);
  AGCSUBATTR  agcsubattr = (AGCSUBATTR) IFACE_object((IFACE) iattr);

  /* Initialize the reset counter */
  agcsub->resetCnt = 0;

  /* Convert the attributes */
  agcsub->subtrahend = MEAN2VAL(agcsubattr->subtrahend);
  agcsub->newWeight = FLO2VAL(agcsubattr->newWeight);
  agcsub->oldWeight = FLO2VAL(agcsubattr->oldWeight);
  agcsub->minEnergy = MEAN2VAL(agcsubattr->minEnergy);

  agcsub->subtraWarmup = MEAN2VAL(AGCSUB_WARMUPSUBTRAHEND);

  /* Initialize the maximum energy to the minimum allowed */
  agcsub->maxEnergy = agcsubattr->minEnergy;

  /* Set AGC to unrestorable state */
  agcsub->resetCntSaved = -1;

  return(TRUE);
}


/*** CEPSTRAL SOURCE AND SINK METHODS ***/

static Bool
AGCSUB_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Import object attributes via the sink (input) interface and pass them
 * to the sink associated with the source (output) interface.  This function
 * supplements the default ISNK_setAttributes() function, which calls this
 * first and then sets the attributes of isnk if the call is successful.
 *---------------------------------------------------------------------------*/
{
  AGCSUB    agcsub;
  ISNK      sink;

  assert(isnk);

  agcsub = (AGCSUB) IFACE_object((IFACE) isnk);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(agcsub->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) isnk, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, iattr)) {
    /* If successful, set the attributes locally and return TRUE */
    ISRC_setAttributes(agcsub->isrc, iattr);
    return(TRUE);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
    return(FALSE);
  }
}


static Bool
AGCSUB_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a frame via the source (output) interface, attenuating c[0] by
 * the amount specified in agcsub->subtrahend.  This function clones OBJTs
 * so as not to modify the ones it gets, which may reside in earlier queues.
 *---------------------------------------------------------------------------*/
{
  AGCSUB     agcsub;
  ISRC       src;
  OBJT       agcObj;
  Val      * cep;

  assert(isrc && snP);

  agcsub = ((AGCSUB) IFACE_object((IFACE) isrc));

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(agcsub->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Obtain the requested frame from the source */
  if (ISRC_getFrame(src, dir, snP, outP)) {
    /* If the source yields a valid frame OBJT, clone it and attenuate c[0] */
    if (outP) {
      agcObj = OBJT_clone(*outP);
      OBJT_free(*outP);
      *outP = agcObj;
      if (agcObj) {
        cep = (Val *) &OBJT_contents(agcObj);
        if (cep[0] > agcsub->maxEnergy) 
          agcsub->maxEnergy = cep[0];
        if (agcsub->resetCnt > AGCSUB_EQUILIBRATIONUTTS)
          cep[0] -= agcsub->subtrahend;
        else
          cep[0] -= agcsub->subtraWarmup;
      } else {
        IFACE_setError((IFACE) isrc, EXC_CEPCLASSEMPTY);
        return(FALSE);
      }
    }
#if 0
    agcsub->sn++;
    if (!(agcsub->sn % agcsub->printSubFrms))
      VBX_print("AGCSUB: SN  "QuadFmt(-19)" subtrahend = %f\n",
                 agcsub->sn, agcsub->subtrahend);
#endif
    return(TRUE);
  } else {
    /* Otherwise, just pass along whatever exception the source yields */
    IFACE_setError((IFACE) isrc, IFACE_error((IFACE) src));
    return(FALSE);
  }
}


static Bool
AGCSUB_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an OBJT via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(AGCSUB_getFrame(isrc, dir, snP, NULL));
}


/*** AGCSUBATTR Methods ***/

IGEN
AGCSUBATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a AGCSUB attributes object
 *---------------------------------------------------------------------------*/
{
  AGCSUBATTR   agcsubattr = (AGCSUBATTR) imem->Calloc(imem, 1, sizeof(agcsubattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(imem, (OBJ) agcsubattr);
  IAGCSUBATTR  iagcsubattr = IAGCSUBATTR_create(igen);

  return(igen);
}


/*** IAGCSUBATTR METHODS ***/

static IAGCSUBATTR
IAGCSUBATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IAGCSUBATTR interface
 *---------------------------------------------------------------------------*/
{
  IAGCSUBATTR  iagcsubattr;

  if ((iagcsubattr = (IAGCSUBATTR) IFACE_calloc((IFACE) igen, 1, sizeof(iagcsubattr_t)))) {
    IFACE_setClass((IFACE) iagcsubattr, &IAGCSUBATTR_Class);
    IFACE_setObject((IFACE) iagcsubattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iagcsubattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iagcsubattr);
  }

  return(iagcsubattr);
}


Bool
IAGCSUBATTR_setSubtrahend(IAGCSUBATTR iagcsubattr, Float subtrahend)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the subtrahend
 *---------------------------------------------------------------------------*/
{
  AGCSUBATTR  agcsubattr;

  assert(iagcsubattr);
  agcsubattr = (AGCSUBATTR) IFACE_object((IFACE) iagcsubattr);

  agcsubattr->subtrahend = subtrahend;

  return(TRUE);
}


Bool
IAGCSUBATTR_setNewWeight(IAGCSUBATTR iagcsubattr, Float newWeight)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the weight of the previous maximum energy in the new subtrahend
 *---------------------------------------------------------------------------*/
{
  AGCSUBATTR  agcsubattr;

  assert(iagcsubattr);
  agcsubattr = (AGCSUBATTR) IFACE_object((IFACE) iagcsubattr);

  agcsubattr->newWeight = newWeight;

  return(TRUE);
}


Bool
IAGCSUBATTR_setOldWeight(IAGCSUBATTR iagcsubattr, Float oldWeight)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the weight of the old subtrahend in the new
 *---------------------------------------------------------------------------*/
{
  AGCSUBATTR  agcsubattr;

  assert(iagcsubattr);
  agcsubattr = (AGCSUBATTR) IFACE_object((IFACE) iagcsubattr);

  agcsubattr->oldWeight = oldWeight;

  return(TRUE);
}


Bool
IAGCSUBATTR_setMinEnergy(IAGCSUBATTR iagcsubattr, Float minEnergy)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the reset value of maxEnergy
 *---------------------------------------------------------------------------*/
{
  AGCSUBATTR  agcsubattr;

  assert(iagcsubattr);
  agcsubattr = (AGCSUBATTR) IFACE_object((IFACE) iagcsubattr);

  agcsubattr->minEnergy = minEnergy;

  return(TRUE);
}
