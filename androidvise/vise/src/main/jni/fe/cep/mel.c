/*  mel.c:  Per frame mel-fft cepstrum package
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav/jrt)
 * $__Header$
 *
 * Description:
 *  Mel cepstrum front-end facility.
 *
 * References:
 *   S.B. Davis & P. Mermelstein, "Comparison of Parametric Representations
 *   for Monosyllabic Word Recognition in Continuously Spoken Sentences", 
 *   IEEE Trans. Acoustics, Speech, Signal Proc., ASSP-28(4): 357-366, 
 *   August, 1980.
 *
 * Functionality:
 *   MELSRC MEL_create(IMemory *imem, Int feVersion) - Initializes the MEL package.
 *   Bool MEL_cepstrum(MELSRC melsrc, Val *mfc, , Val *power, Short *samples, Int *activity);
 *     Computes the mel-fft cepstrum for a frame of data:
 *     Val *mfc: vector of cepstral coefficients (output)
 *     Short *samples: window of sampled speech data (input)
 *     Fills the power and the Hirsch activity level.
 *
 *---------------------------------------------------------------------------*/

#include "pthr.h"
#include <math.h>
#include <assert.h>
#include "imemory.h"
#include "floorfix.h"
#include "sn.h"
#include "ham.h"
#include "kaiser.h"
#include "ness.h"
#include "rasta.h"
#include "mel.h"
#include "feversion.h"
#include "filtdefs.h"
#include "vbx.h"

#ifdef DEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

#define MAXSIGBITS  9    /* Largest number of significant bits in a sample */

 /* Cepstrum Computation Modes */
typedef enum {VISEMode, FLEXMode} cepmode;

typedef struct melCfg_s {
  meltype melType;
  Int     cepMode;
  Int     filterCnt;
  MEL     filterTable;
  Bool    oldFiltNorm;
} melCfg_t, *MELCFG;

static melCfg_t MelCfgTbl[] = {
  {VISE11,    VISEMode, 24, ViseMelTbl,    FALSE},
  {VISE10,    VISEMode, 23, ViseMelTbl,    FALSE},
  {VISE8,     VISEMode, 20, ViseMelTbl,    FALSE},
  {FLEX16OLD, FLEXMode, 24, OldFlexMelTbl, TRUE },
  {FLEX8OLD,  FLEXMode, 21, ViseMelTbl,    TRUE },
  {BADMEL,    0,         0,       NULL,    FALSE}
};

typedef struct filter_s {
  Int   leftIndx;
  Int   count;
  Val * values;
} filter_t, *FILTER;

typedef struct melsrc_s {
  IMemory * imem;
  Int      sampleRate;
  Int      frameLen;
  Int      frameShift;
  Int      cepVecLen;
  Int      otherProc;
  cepmode  cepMode;
  Int      filterCnt;
  Bool     nmassOn;
  Int      fftSize;
  Int      dftSize;
  Int      fftOrder;
  Int      dftOrder;
  Val    * fftFrame;
  FILTER   filterBank;
  HAM      ham;
  KAISER   kaiser;
  NESS     ness;
  RAST     rasta;
  Val    * melBuf;
  Val   ** rotMatrix;
  Val    * sineTable;
  Val      scaleFactor;
  SN       frameSN;       /* Internal feature frame sequence number */
  Int      frameSegments; /* Derived: # of frameShift segments per frame */
  Val    * analFrame;     /* Analysis frame, used for history maintenance */
  Int      analFrameLen;  /* The length (in shorts) of the analysis frame */
  Int      gainShift;
} melsrc_t;

#define R_LOG2      3.3219281             /* Reciprocal of log10(2.0) */
#define SCALEFACTOR 0.230258519743758522
/* Default right-shift of incoming audio data */
#define DFLT_GAINSHIFT    3

 /* Internal function protos */
static Bool  MEL_enqueue(MELSRC melsrc, Short * samples);
static void  MEL_filtCreate(MELSRC melsrc, MEL filterData, Bool oldFiltNorm);
static Float MEL_slope(Float freq, MEL filter);
static void  MEL_matrixInit(MELSRC melsrc);
static Int   MEL_spectrum(MELSRC melsrc);
static void  MEL_filtApplyMag(MELSRC melsrc);
static void  MEL_powerSpectrum(MELSRC melsrc);
static void  MEL_filtApply(MELSRC melsrc);
static void  MEL_vecRotate(MELSRC melsrc, Val * mfc, Int offset);
static Int   MEL_scale(MELSRC melsrc, Int msb);
static Int   log2b(Int v);
static Bool  MEL_cepstrum2(MELSRC melsrc, Val * mfc, Val * power);
static Bool  MEL_cepstrNMASS(MELSRC melsrc, Val * mfc, Val * power, Int * activity);

MELSRC
MEL_create(IMemory *imem, Int feVersion)
{
  MELSRC  melsrc;
  meltype melType;
  MELCFG  melCfg;
  Int     i;

  if (BADMEL == (melType = FEVER_getMelType(feVersion)) || !imem ||
      !(melsrc = (MELSRC) imem->Calloc(imem, 1, sizeof(melsrc_t), FAST_MEMORY)))
    return NULL;

  melsrc->imem = imem;
  FEVER_getSampleRate(feVersion, &melsrc->sampleRate);
  FEVER_getFrameLen(feVersion, &melsrc->frameLen);
  FEVER_getFrameShift(feVersion, &melsrc->frameShift);
  FEVER_getCepVecLen(feVersion, &melsrc->cepVecLen);
  FEVER_getOtherProc(feVersion, &melsrc->otherProc);

   /* Find melCfg */
  for (melCfg = MelCfgTbl; melCfg->melType != BADMEL; melCfg++)
    if (melType == melCfg->melType)
      break;
   /* If not found ... */
  if (BADMEL == melCfg->melType)
    return NULL;
  melsrc->cepMode = melCfg->cepMode;
  melsrc->filterCnt = melCfg->filterCnt;

  melsrc->fftSize = 1;
  melsrc->fftOrder = 0;
  while (melsrc->fftSize < melsrc->frameLen) {
    melsrc->fftSize *= 2;
    melsrc->fftOrder++;
  }
  melsrc->dftSize = melsrc->fftSize / 2;
  melsrc->dftOrder = melsrc->fftOrder - 1;

  /* Determine length of analysis frame */
  melsrc->frameSegments = 1;
  while (melsrc->frameSegments * melsrc->frameShift < melsrc->frameLen)
    melsrc->frameSegments++;
  assert(melsrc->frameSegments > 0 && melsrc->frameSegments <= 4);
  VBX_DEBUG(VBX_print("MEL_create: cepVecLen %d frameLen %d frameShift %d frameSegments %d\n", 
            melsrc->cepVecLen, melsrc->frameLen, melsrc->frameShift, melsrc->frameSegments));

  melsrc->analFrameLen = melsrc->frameSegments * melsrc->frameShift;
   /* Allocate */
  if (
      !(melsrc->analFrame = (Val *) imem->Calloc(imem, melsrc->analFrameLen, sizeof(Val),
                                                 FAST_MEMORY)) ||
      !(melsrc->fftFrame = (Val *) imem->Calloc(imem, 1, melsrc->fftSize * sizeof(Val),
                                                FAST_MEMORY)) ||
      !(melsrc->sineTable = (Val *) imem->Calloc(imem, 1, melsrc->dftSize * sizeof(Val),
                                                 FAST_MEMORY)) ||
      !(melsrc->filterBank = (FILTER) imem->Calloc(imem, 1, melsrc->filterCnt * sizeof(filter_t),
                                                   FAST_MEMORY)) ||
      !(melsrc->melBuf = (Val *) imem->Calloc(imem, 1, melsrc->dftSize * sizeof(Val),
                                              FAST_MEMORY)) ||
      !(melsrc->rotMatrix = (Val **) imem->Calloc(imem, 1, melsrc->cepVecLen * sizeof(Val *),
                                                  FAST_MEMORY)) ||
      !(melsrc->ham = HAM_create(imem, melsrc->frameLen)) ||
      !(melsrc->kaiser = KAISER_create(imem)) ||
      !(melsrc->ness = NESS_create(imem, melsrc->filterCnt))
     )
    return NULL;

  for (i = 0; i < melsrc->cepVecLen; i++)
    if (!(melsrc->rotMatrix[i] = (Val *) imem->Calloc(imem, 1, melsrc->filterCnt * sizeof(Val), 
          FAST_MEMORY)))
      return NULL;

  MEL_filtCreate(melsrc, melCfg->filterTable, melCfg->oldFiltNorm);
  
  MEL_matrixInit(melsrc);

  melsrc->scaleFactor = MEAN2VAL(SCALEFACTOR / (Float)melsrc->filterCnt);
  melsrc->gainShift = DFLT_GAINSHIFT;
  NESS_setMask(melsrc->ness, DFLT_GAINSHIFT);

  for (i = 0; i < melsrc->dftSize; i++)
    melsrc->sineTable[i] = FLO2VAL(sin(2.0 * PI * i / melsrc->fftSize));

  if (melsrc->otherProc & RASTA) {
    if (!(melsrc->rasta = RASTA_create(imem, melsrc->filterCnt)))
      return NULL;
  }
  else
    melsrc->rasta = NULL;

  if (melsrc->otherProc & NMASS)
    melsrc->nmassOn = TRUE;
  else
    melsrc->nmassOn = FALSE;

  return(melsrc);
}

void
MEL_destroy(MELSRC melsrc)
{
  IMemory * imem;
  Int       i;

  if (melsrc) {
    imem = melsrc->imem;
    KAISER_destroy(melsrc->kaiser);
    HAM_destroy(melsrc->ham);
    NESS_destroy(melsrc->ness);
    if (melsrc->rasta)
      RASTA_destroy(melsrc->rasta);

    for (i = 0; i < melsrc->cepVecLen; i++) {
      imem->Free(imem, melsrc->rotMatrix[i]);
    }

    imem->Free(imem, melsrc->rotMatrix);
    imem->Free(imem, melsrc->melBuf);

    for (i = 0; i < melsrc->filterCnt; i++) {
      imem->Free(imem, melsrc->filterBank[i].values);
    }

    imem->Free(imem, melsrc->filterBank);
    imem->Free(imem, melsrc->fftFrame);
    imem->Free(imem, melsrc->sineTable);
    imem->Free(imem, melsrc->analFrame);
    imem->Free(imem, melsrc);
  }
}

static void
MEL_filtCreate(MELSRC melsrc, MEL filterData, Bool oldFiltNorm)
/*---------------------------------------------------------------------------*
 * DESCRIPTION - oldFiltNorm = TRUE means old incorrect filter normalization
 * Difference in factor dftDelta (31.25 for 8KHz and 16KHz, 43 for 11025Hz 
 * and fftSize = 256). Thus we need additional scaling by factor 64. 
 * Total: 256 * 64 = 16384. Power bins will have at most 19 significant
 * bits (after squaring and summing Re and Im fft values having at most 9 
 * significant bits). Estimated filter factor = height * count / 2 is between
 * 490 and 520, which adds no more than 10 bits to total 29 bits of 
 * Mel-Filter output.
 *---------------------------------------------------------------------------*/
{
  Float dftDelta;    /* spacing between DFT points (freq) */
  Float freq, filtVal;
  Float slope1, slope2;
  Int i, j, k;

  dftDelta = (Float) melsrc->sampleRate / (Float)(2 * melsrc->dftSize);
  for (i = 0; i < melsrc->filterCnt; i++) {
    slope1 = MEL_slope(filterData[i].left, &filterData[i]);
    slope2 = MEL_slope(filterData[i].right, &filterData[i]);
    for (j = 0; j < melsrc->dftSize; j++) 
      melsrc->melBuf[j] = (Val)0;
    j = filterData[i].left / dftDelta;
    if ((dftDelta * j) < filterData[i].left) j++;
    melsrc->filterBank[i].leftIndx = j;
    k = 0;
    freq = j * dftDelta;
    while (freq <= filterData[i].right) {
      if (j >= melsrc->dftSize)
        break;
      if (freq >= filterData[i].left && freq <= filterData[i].center) {
        filtVal = slope1 * (freq - filterData[i].left);
        if (oldFiltNorm)
          melsrc->melBuf[k++] = FLX2VAL(filtVal);
        else
          melsrc->melBuf[k++] = FLO2VAL(filtVal * dftDelta);
      } else if (freq > filterData[i].center && freq <= filterData[i].right) {
        filtVal = slope2 * (freq - filterData[i].right);
        if (oldFiltNorm)
          melsrc->melBuf[k++] = FLX2VAL(filtVal);
        else
          melsrc->melBuf[k++] = FLO2VAL(filtVal * dftDelta);
      }
      j++;
      freq = j * dftDelta;
    }
    melsrc->filterBank[i].count = k;
    melsrc->filterBank[i].values = (Val *) melsrc->imem->Calloc(melsrc->imem,
                                                 k, sizeof(Val), FAST_MEMORY);
    memcpy(melsrc->filterBank[i].values, melsrc->melBuf, k * sizeof(Val));
  }
}

static Float
MEL_slope(Float freq, MEL filter)
{
  Float height;
  
  height = 2.0 / (filter->right - filter->left); /* for normalized area */
  return(height / (filter->center - freq));      /* slope is rise / run */
}

static void
MEL_matrixInit(MELSRC melsrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Generate the Mermelstein transformation matrix and store the result in 
 * RotMatrx.  The transformation matrix is computed as
 *       c[k] = sum x[n] cos (pi/N n-1/2 k)
 * and the result is stored by row.
 *---------------------------------------------------------------------------*/
{
  Int i, j;
  
  for (i = 0; i < melsrc->cepVecLen; i++)
    for (j = 0; j < melsrc->filterCnt; j++)
      melsrc->rotMatrix[i][j] = FLOS2VAL(cos(PI * i * (j + 0.5) / melsrc->filterCnt));
}

static Bool
MEL_enqueue(MELSRC melsrc, Short * samples)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Enqueue a single frame of raw audio data from the source.  The length of
 * the frame MUST be equal to "frameShift" samples.
 *---------------------------------------------------------------------------*/
{
  Int blockLength = (melsrc->frameSegments - 1) * melsrc->frameShift;

  memmove(melsrc->analFrame, &melsrc->analFrame[melsrc->frameShift], blockLength * sizeof(Val));

  if (melsrc->cepMode == VISEMode) {
    if (melsrc->nmassOn)
      KAISER_firstDif(melsrc->kaiser, samples, &melsrc->analFrame[blockLength], melsrc->frameShift);
    else
      KAISER_preEmph(melsrc->kaiser, samples, &melsrc->analFrame[blockLength], melsrc->gainShift, melsrc->frameShift);
  } else { /* FLEXMode */
    HAM_preEmph(melsrc->ham, samples, &melsrc->analFrame[blockLength], melsrc->frameShift);
  }

  melsrc->frameSN++;
  if (melsrc->frameSN < melsrc->frameSegments)
    return FALSE;

  return TRUE;
}

Bool
MEL_frameAvailable(MELSRC melsrc)
{
  if (!melsrc)
    return(FALSE);

  return(melsrc->frameSN >= melsrc->frameSegments);
}

Int
MEL_snGap(MELSRC melsrc)
{
  if (!melsrc)
    return(0);

  return melsrc->frameShift * sizeof(Short) * (melsrc->frameSegments - 1);
}

void
MEL_reset(MELSRC melsrc)
{
  memset(melsrc->analFrame, 0, melsrc->frameLen * sizeof(Val));
  melsrc->frameSN = 0;
  HAM_reset(melsrc->ham);
  KAISER_reset(melsrc->kaiser);
  NESS_reset(melsrc->ness);
  if (melsrc->rasta)
    RASTA_reset(melsrc->rasta);
}

void
MEL_startCalibration(MELSRC melsrc, Int maxFrames)
{
  if (melsrc && melsrc->nmassOn)
    NESS_startCalibration(melsrc->ness, maxFrames);
}

void
MEL_stopCalibration(MELSRC melsrc)
{
  if (melsrc && melsrc->nmassOn )
    NESS_stopCalibration(melsrc->ness);
}

Int
MEL_getNoiseTracking(MELSRC melsrc)
{
  if (melsrc)
    return NESS_getNoiseTracking(melsrc->ness);

  return 0;
}

void
MEL_setNoiseTracking(MELSRC melsrc, Int track)
{
  if (melsrc)
    NESS_setNoiseTracking(melsrc->ness, track);
}

Bool
MEL_setSigBits(MELSRC melsrc, Int sigBits)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Sets right shift value
 *---------------------------------------------------------------------------*/
{
  if (melsrc && MAXSIGBITS <= sigBits && sigBits <= 16) {
    melsrc->gainShift = sigBits - MAXSIGBITS;
    NESS_setMask(melsrc->ness, melsrc->gainShift);
    return TRUE;
  } else {
    return FALSE;
  }
}

Bool
MEL_cepstrum(MELSRC melsrc, Val * mfc, Val * power, Short * samples, Int * activity)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * The cepstral calculation sequence specific to the FlexVISE recognizer
 *---------------------------------------------------------------------------*/
{
  Int i, j;
  
#if 0
  printf("INPUT FRAME:\n");
  for (i = 0; i < 16; i++) {
    for (j = 0; j < 16; j++) {
      printf("%3d ", samples[i * 16 + j]);
    }
    printf("\n");
  }
#endif

   /* Initialize return value to nonsense */
  *activity = -1;

  if (!MEL_enqueue(melsrc, samples))
    return FALSE;

  if (melsrc->cepMode == VISEMode) {
    if (melsrc->nmassOn)
      return MEL_cepstrNMASS(melsrc, mfc, power, activity);
    else
      return MEL_cepstrum2(melsrc, mfc, power);
  }

   /* Otherwise it is FLEXVise */
  *activity = MEL_spectrum(melsrc);

  if (melsrc->rasta)
    if (!RASTA_computeFrame(melsrc->rasta, melsrc->fftFrame))
      return FALSE; /* RASTA cold start */

  MEL_vecRotate(melsrc, mfc, 0);

  /* Compensate factor from rotMatrix (256. or none), and RASTA if any */
  for (i = 0; i < melsrc->cepVecLen; i++) {
    mfc[i] = NORMBACK(mfc[i] * melsrc->scaleFactor);
    if (melsrc->rasta)
      mfc[i] = NORMBACK(mfc[i]);
  }

 *power = mfc[0];
  return TRUE;
}

static Bool
MEL_cepstrNMASS(MELSRC melsrc, Val * mfc, Val * power, Int * activity)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * The cepstral calculation sequence specific to the VVISE recognizer with
 * Noise Masking And Spectral Subtraction
 *---------------------------------------------------------------------------*/
{
  Int   i, j, shVal = 0;
  Val * mfs = melsrc->fftFrame;
  Val   t;

   /* msb <= 15, counting from 0, after KAISER_firstDif. No right shift here,
    * but power is normalized by number of samples analyzed  */
  t = KAISER_window256(melsrc->analFrame, melsrc->fftFrame);

#ifdef FIXED_POINT
  *power = (3 * log2b(1 + t)) >> LOG2_PLACES;
#else  /* Floating Point */
  *power = (Val)(10. * log10(1.0 + t));
#endif  /* FIXED_POINT */

   /* still msb <= 15, since KAISER_window256 preserves scale */
  fft(melsrc->fftFrame, melsrc->fftSize, melsrc->fftOrder, melsrc->sineTable);

#ifdef FIXED_POINT
   /* msb <= 23 after 8 butterflies. Scale, making msb = 8. Scaler returns
    * negative shift applied. Double it for MEL_powerSpectrum squaring */
  shVal = MEL_scale(melsrc, 8) << 1;

#endif  /* FIXED_POINT */
  /* Power spectrum bins */
  MEL_powerSpectrum(melsrc);

  /* Clean spectrum */
  *activity = NESS_activity(melsrc->ness, mfs, *power, shVal);

  mfc[0] = *power;

  if (*activity <= 0) { /* Frame skipping, or in fact silence frame */
     /* Fill in the rest of cepstra with 0, and get out */
    for (i = 1; i < melsrc->cepVecLen; i++)
      mfc[i] = (Val)0;
    return TRUE;
  }

  /* Apply triangular filters to cleaned spectrum */
  MEL_filtApply(melsrc);

  /* Mask power spectrum in bands */
  NESS_apply(melsrc->ness, mfs, *power, shVal, *activity);

   /* Compute 0.5*DBPower, rotate, weigh */
#ifdef FIXED_POINT

  for (i = 0; i < melsrc->filterCnt; i++)
    mfs[i] = 3 * log2b(mfs[i]);

  MEL_vecRotate(melsrc, &mfc[1], 1);

   /* Compensate rotation and log2b scaling; +3 for 0.5*0.25 */
  for (i = 1; i < melsrc->cepVecLen; i++)
    mfc[i] = (mfc[i] * (i + 3)) >> (SHIFTVAL + LOG2_PLACES + 3);
  
#else  /* Floating Point */

  for (i = 0; i < melsrc->filterCnt; i++)
    mfs[i] = 5.0 * log10(mfs[i]);

  MEL_vecRotate(melsrc, &mfc[1], 1);

  for (i = 1, t = 1.0; i < melsrc->cepVecLen; i++, t += 0.25)
    mfc[i] *= t;

#endif  /* FIXED_POINT */

  return TRUE;
}

static Bool
MEL_cepstrum2(MELSRC melsrc, Val * mfc, Val * power)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * The cepstral calculation sequence specific to the VISE recognizer
 *---------------------------------------------------------------------------*/
{
  Val  tmp;
  Int  i, j, shVal;

  /* msb <= 15, counting from 0, after preEmph even with shift==0.
   * KAISER_apply256 retains the scale now.*/
  *power = KAISER_apply256(melsrc->analFrame, melsrc->fftFrame);

#if 0
  printf("WINDOW RESULT:\n");
  for (i = 0; i < 16; i++) {
    for (j = 0; j < 16; j++) {
      printf("%7.4f ", melsrc->fftFrame[i * 16 + j]);
    }
    printf("\n");
  }
#endif

  /* Since msb <= 15, and on the last (8th) butterfly we add no more then 16 bits
   * (8 from sineTable and 8 bits from all 8 butterflies) we are overflow-safe.
   * After NORMBACK on the last butterfly end up with msb <= 22 */
  fft(melsrc->fftFrame, melsrc->fftSize, melsrc->fftOrder, melsrc->sineTable);

#if 0
  printf("FFT RESULT:\n");
  for (i = 0; i < 16; i++) {
    for (j = 0; j < 16; j++) {
      printf("%7.4f ", melsrc->fftFrame[i * 16 + j]);
    }
    printf("\n");
  }
#endif
	   
#ifdef FIXED_POINT
  /* Scale, making msb = 8. Scaler returns negative shift applied.
     Double it for MEL_filtApplyMag squaring */
  shVal = MEL_scale(melsrc, 8) << 1;
#endif  /* FIXED_POINT */

  MEL_filtApplyMag(melsrc);

  /* Take 10*log10 == 3*log2b*/
#ifdef FIXED_POINT

  tmp = (shVal >= 0) ? (8 << shVal) : (8 >> -shVal);
  if (0 == tmp)
    tmp = 1;

  for (i = 0; i < melsrc->filterCnt; i++)
    melsrc->fftFrame[i] = 3 * log2b(melsrc->fftFrame[i] + tmp);

  MEL_vecRotate(melsrc, &mfc[1], 1);

   /* Compensate rotation and log2b scaling; +3 for 0.5*0.25 */
  for (i = 1; i < melsrc->cepVecLen; i++)
    mfc[i] = (mfc[i] * (i + 3)) >> (SHIFTVAL + LOG2_PLACES + 3);

  mfc[0] = (3 * log2b(1 + *power)) >> LOG2_PLACES;

#else  /* Floating Point */

  for (i = 0; i < melsrc->filterCnt; i++) {
    melsrc->fftFrame[i] = 0.5 * R_LOG2 * log10(melsrc->fftFrame[i] + 8.0);
  }

  MEL_vecRotate(melsrc, mfc, 0);

  /* Weight, normalize ... . tmp = ALPHA*weight in VISE FE */
  for (i = 1, tmp = 3.0; i < melsrc->cepVecLen; i++, tmp += 0.75)
    mfc[i] *= tmp;

  mfc[0] = (Val)(10. * log10(1.0 + *power));

#if 0
  printf("MELCEP: ");
  for (i = 0; i < melsrc->cepVecLen; i++)
    printf("%7.4f ", mfc[i]);
  printf("\n");
#endif

#endif  /* FIXED_POINT */

  return(TRUE);
}


static Int
MEL_spectrum(MELSRC melsrc)
{
  Int   activity = 0;
  Int   i, shVal = 0, tmp;
  Val * mfs = melsrc->fftFrame;
  Val   power = (Val)24; /* Dummy power for now */
  
  for (i = melsrc->frameLen; i < melsrc->fftSize; i++)
    melsrc->fftFrame[i] = (Val)0;

  /* msb <= 15 */
  HAM_apply(melsrc->ham, melsrc->analFrame, melsrc->fftFrame);
  /* msb <= 20. Windowing added SHIFTVAL - SHIFTWIN = 5 bits
     REVISIT THIS !!! Overflow still possible in fft 2 last butterflies */
  fft(melsrc->fftFrame, melsrc->fftSize, melsrc->fftOrder, melsrc->sineTable);

#ifdef FIXED_POINT
  /* msb <= 29. It's time to scale, making msb = 8. Scaler returns negative shift applied.
     Compensate accumulated shift with HAM_apply and future MEL_filtApplyMag scaling */
  shVal = ((MEL_scale(melsrc, 8) + SHIFTVAL - SHIFTWIN) << 1) + SHIFTFLX - SHIFTVAL;
#endif  /* FIXED_POINT */

  MEL_filtApplyMag(melsrc);
  /* Magnitude has msb <= 18. Filtering adds no more than 10 bits.
     NORMBACK brings filer output 8 bit back to msb = 20. Safe for NESS */

/*  activity = NESS_activity(melsrc->ness, mfs, power, shVal); */

  /* Take log */
#ifdef FIXED_POINT

  /* 10.*log10(v+8.) = 3.*log2(v+8.) = 3.*(log2(v*scale+8.*scale)-log2(scale)) =
     = (3 * (log2b(mfs + tmp) - (shVal << LOG2_PLACES))) >> LOG2_PLACES */
  tmp = (shVal >= 0) ? (8 << shVal) : (8 >> -shVal);
  if (0 == tmp)
    tmp = 1;

  for (i = 0; i < melsrc->filterCnt; i++)
    mfs[i] = (3 * (log2b(mfs[i] + tmp) - (shVal << LOG2_PLACES))) >> LOG2_PLACES;

#else  /* Floating Point */

  for (i = 0; i < melsrc->filterCnt; i++)
    mfs[i] = 10. * log10(mfs[i] + 8.0);

#endif  /* FIXED_POINT */
  return(activity);
}


static void
MEL_filtApplyMag(MELSRC melsrc)
{ /* When FIXED_POINT, doubles accumulated shift due to squaring.
     For FlexVise adds SHIFTFLX - SHIFTVAL due to Mel-Filter coefficient additional scaling */
  Val *fftFrame = melsrc->fftFrame;
  Val *imPtr;
  Int  i, j, indx;
  Val  sum;

  /* First compute the power spectrum from the FFT butterflies */
  imPtr = fftFrame + melsrc->fftSize - 1;
 *fftFrame = *fftFrame * *fftFrame;
  fftFrame++;
  for (i = 0; i < (melsrc->dftSize) - 1; i++, imPtr--)
    fftFrame[i] = fftFrame[i] * fftFrame[i] + *imPtr * *imPtr;

  /* Restore the frame pointer and apply the Mel filters */
  fftFrame = melsrc->fftFrame;
  for (i = 0; i < melsrc->filterCnt; i++) {
    sum = (Val)0;
    for (j = 0, indx = melsrc->filterBank[i].leftIndx; j < melsrc->filterBank[i].count; j++)
      sum  += melsrc->filterBank[i].values[j] * fftFrame[indx + j];
    fftFrame[i] = NORMBACK(sum);
  }
}

/* Break routine above apart - power spectrum and filtering */
static void
MEL_powerSpectrum(MELSRC melsrc)
{ /* When FIXED_POINT, doubles accumulated shift due to squaring. */
  Val *fftFrame = melsrc->fftFrame;
  Val *imPtr = fftFrame + melsrc->fftSize - 1;
  Int  i;

 *fftFrame = *fftFrame * *fftFrame;
  fftFrame++;
  for (i = 0; i < (melsrc->dftSize) - 1; i++, imPtr--)
    fftFrame[i] = fftFrame[i] * fftFrame[i] + *imPtr * *imPtr;
}

static void
MEL_filtApply(MELSRC melsrc)
{
  /* For FlexVise adds SHIFTFLX - SHIFTVAL due to Mel-Filter coefficient additional scaling */
  Val *fftFrame = melsrc->fftFrame;
  Int  i, j, indx;
  Val  sum;

  for (i = 0; i < melsrc->filterCnt; i++) {
    sum = (Val)0;
    for (j = 0, indx = melsrc->filterBank[i].leftIndx; j < melsrc->filterBank[i].count; j++)
      sum  += melsrc->filterBank[i].values[j] * fftFrame[indx + j];
    fftFrame[i] = NORMBACK(sum);
  }
}


static void
MEL_vecRotate(MELSRC melsrc, Val * mfc, Int offset)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Use RotMatrix (dimension CepOrder x FilterCnt) to apply a rotation to
 * input vector mfs, which must have dimension FilterCnt.  This operation
 * essentially amounts to taking the inverse Fourier transform to get back
 * to the time domain.
 *---------------------------------------------------------------------------*/
{
  Int   i, j;
  Val   sum;
  Val * mfs = melsrc->fftFrame;

  for (i = offset; i < melsrc->cepVecLen; i++) {
    sum = (Val)0;
    for (j = 0; j < melsrc->filterCnt; j++)
      sum += mfs[j] * melsrc->rotMatrix[i][j];
    mfc[i - offset] = sum;
  }
}

#ifdef FIXED_POINT

 static Int
MEL_scale(MELSRC melsrc, Int msb)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Scales melsrc->fftFrame shifting data right so the most significant bit,
 * counting from 0, won't exceed msb. Returns negative of shift applied.
 *---------------------------------------------------------------------------*/
{
  Val * data = melsrc->fftFrame;
  UInt  mag = 0;
  Int   shift = msb - 30;
  Int   i, size = melsrc->fftSize;

  for (i = 0; i < size; i++)
    mag |= ((data[i] < 0) ? -data[i] : data[i]);

  /* If signal is zero there is no shift */
  if (mag == 0) return 0;

  for (; !(mag & 0x40000000L); mag <<= 1, shift++);

  /* Don't shift left */
  if (shift >= 0) return 0;

  for (i = 0; i < size; i++)
    data[i] >>= -shift;

  return shift;
}


static Int
log2b(Int v)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns scaled up log2 of v with LOG2_PLACES additional accuracy bits.
 *---------------------------------------------------------------------------*/
{
  Int c = v, msb = 0;

  /* Hack acceptable for LOG2_PLACES = 2.
     Assures nonnegative shift right operand in the last statement */
  if (v <= LOG2_PLACEMASK)
    return ((v-1) << 1);

  /* Find the most significant bit msb >= LOG2_PLACES */
  while ( c >>= 1 )
    msb++;
                 
  return ( (msb << LOG2_PLACES) | ((v >> (msb - LOG2_PLACES)) & LOG2_PLACEMASK) );
}

#endif  /* FIXED_POINT */
