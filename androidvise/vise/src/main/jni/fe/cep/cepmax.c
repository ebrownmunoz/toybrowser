/* cepmax.c - find the max c0 in a cepstral array
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 *---------------------------------------------------------------------------*/

#include "pthr.h"

#include <stdlib.h>
#include <ctype.h>
#include "types.h"
#include "floorfix.h"
#include "cep.h"

void
CEP_agcMax(Val * cep, Int fcnt, Int cepVecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the maximum log-energy in an array of cepstral frames.
 *
 * Arguments:
 *    cep:        The cepstral array
 *    fcnt:       The number of cepstral frames in the array
 *    cepVecLen:  The number of cepstral coefficients per frame
 *---------------------------------------------------------------------------*/
{
  Val    *p;                         /* Data pointer */
  Val    maxEnergy;                  /* Maximum log-energy */
  Int    frame;                      /* Frame index */

  /* Determine the maximum log-energy in the array */
  maxEnergy = *cep;
  for (frame = 0, p = cep; frame < fcnt; frame++, p += cepVecLen)
    if (*p > maxEnergy)
      maxEnergy = *p;

  for (frame = 0, p = cep; frame < fcnt; frame++, p += cepVecLen)
    *p -= maxEnergy;
}

Val
CEP_max(Val * cep, Int fcnt, Int cepVecLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the maximum log-energy in an array of cepstral frames.
 *
 * Arguments:
 *    cep:        The cepstral array
 *    fcnt:       The number of cepstral frames in the array
 *    cepVecLen:  The number of cepstral coefficients per frame
 *---------------------------------------------------------------------------*/
{
  Val    *p;                         /* Data pointer */
  Val    maxEnergy;                  /* Maximum log-energy */
  Int    frame;                      /* Frame index */

  /* Determine the maximum log-energy in the array */
  maxEnergy = *cep;
  for (frame = 0, p = cep; frame < fcnt; frame++, p += cepVecLen)
    if (*p > maxEnergy) maxEnergy = *p;

  return(maxEnergy);
}
