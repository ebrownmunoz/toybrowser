/* txtframe.c - Character Source to an OOPS chain head
 *---------------------------------------------------------------------------*
 *
 * DESCRIPTION
 * Produces frames on basis of CHARSRC, placing single char in the first byte
 * of the output frame, and zeroing the remaining bytes.
 *---------------------------------------------------------------------------*/

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "txtframe.h"
#include "types.h"
#include "feversion.h"
#include "floorfix.h"
#include "cepattr.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include "vbx.h"

#define VBXDEBUG 1
#ifdef VBXDEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

#define DFLT_CLASS_SIZE  50
#define DFLT_FRAME_SIZE  16

/* The Class of TXTFRAMEATTR (TXTFRAME Attributes) Interface Objects */
static iclass_t ITXTFRAMEATTR_Class = {ITXTFRAMEATTRID};

/* The Class of ITXTFRAME Interface Objects */
static iclass_t ITXTFRAME_Class = {ITXTFRAMEID};

/* The TXTFRAME Interface */
typedef struct itxtframe_s {
  iface_t  iface;
} itxtframe_t;

/* TXTFRAME Attributes */
typedef struct txtframeattr_s {
  Int     classSize;    /* The size of the CLASS of JIN frame objects */
  Int     frameSize;    /* Number of bytes in a sample frame object */
} txtframeattr_t, * TXTFRAMEATTR;

/* A TXTFRAME descriptor */
typedef struct txtframe_s {
  IMemory     * imem;         /* The memory allocator */
  ISRC          isrc;         /* TXTFRAME's source interface (output) */
  ISNK          isnk;         /* TXTFRAME's sink interface (input) */
  IGEN          igenattr;     /* Interface to the cep attributes block */
  Int           classSize;    /* The size of the CLASS of frame OBJTs */
  CLASS         frameClass;   /* The CLASS of output frame OBJTs */
  Int           frameSize;    /* The size of frame OBJTs */
  Char        * frame;
} txtframe_t, * TXTFRAME;

/* Private functions */

static ITXTFRAME ITXTFRAME_create(IGEN igen);

static Bool  TXTFRAME_annihilate(IGEN igen);
static Bool  TXTFRAME_initialize(IGEN igen, IATTR iattr);
static Bool  TXTFRAME_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  TXTFRAME_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static ITXTFRAMEATTR  ITXTFRAMEATTR_create(IGEN igen);

/*** TXTFRAME Methods ***/

IGEN
TXTFRAME_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an TXTFRAME object
 *---------------------------------------------------------------------------*/
{
  TXTFRAME   txtframe = (TXTFRAME) imem->Calloc(imem, 1, sizeof(txtframe_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) txtframe);
  ISNK       isnk = ISNK_create(igen);
  ISRC       isrc = ISRC_create(igen);
  IGEN       igenattr = CEPATTR_create(imem);

  ITXTFRAME_create(igen);

  /* Initialize the object */
  txtframe->imem = imem;
  txtframe->isnk = isnk;
  txtframe->isrc = isrc;
  txtframe->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, TXTFRAME_annihilate);
  IGEN_setInitializer(igen, TXTFRAME_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the source (output) interface */
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, ICEPATTRID));
  ISRC_setGetFrame(isrc, TXTFRAME_getFrame);
  ISRC_setFrameAvailable(isrc, TXTFRAME_frameAvailable);

  return(igen);
}

/*** GENERIC METHODS ***/

static Bool
TXTFRAME_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a frame object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  TXTFRAME txtframe = (TXTFRAME) IFACE_object((IFACE) igen);

  /* Annihilate the CLASS of frame OBJTs */
  if (txtframe->frameClass && !CLASS_destroy(txtframe->frameClass, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Annihilate the Cep attributes object */
  if (!IGEN_annihilate(txtframe->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) txtframe->igenattr));
    return(FALSE);
  }

  return(TRUE);
}

static Bool
TXTFRAME_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a frame maker according to a TXTFRAME attributes 
 * specification.  Like all IGEN_initialize() functions, this may be called 
 * repeatedly with different IATTRs.
 *---------------------------------------------------------------------------*/
{
  TXTFRAME txtframe = (TXTFRAME) IFACE_object((IFACE) igen);

  /* Get the frame attributes */
  ITXTFRAMEATTR_getClassSize((ITXTFRAMEATTR)iattr, &txtframe->classSize);
  ITXTFRAMEATTR_getFrameSize((ITXTFRAMEATTR)iattr, &txtframe->frameSize);
  
  /* Create a new CLASS of frame OBJTs if none exists or the current one is obsolete */
  if (txtframe->frameClass && (txtframe->frameSize != CLASS_objectSize(txtframe->frameClass) ||
                               txtframe->classSize != CLASS_size(txtframe->frameClass))) {
    CLASS_destroy(txtframe->frameClass, NULL, NULL);
    txtframe->frameClass = NULL;
  }
  if (!txtframe->frameClass)
    txtframe->frameClass = CLASS_create(txtframe->imem, txtframe->classSize, txtframe->frameSize, NULL, NULL);
  assert(txtframe->frameClass);

  return TRUE;
}

static Bool
TXTFRAME_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Export a frame object via the isrc (output) interface
 * acquiring the required character frames from the source attached to the isnk
 * (input) interface
 * Return: Success/Failure of attempted operation
 *---------------------------------------------------------------------------*/
{
  TXTFRAME txtframe;
  ISRC     src;              /* The char source */
  SN       snRead;           /* The SN read from the char source */
  OBJT     charIn = NULL;    /* The char object gotten from the source */
  Int      exc;
  Char   * readP;
  Char   * writeP;

  assert(isrc && snP);
  txtframe = (TXTFRAME) IFACE_object((IFACE) isrc);
  assert(txtframe);
  if (outP)
   *outP = NULL;

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(txtframe->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) txtframe->isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Check to see if a frame is available from the source */
  snRead = -1;
  if (!ISRC_frameAvailable(src, FORWARD, &snRead)) {
    exc = IFACE_error((IFACE) src);
    IFACE_setError((IFACE) txtframe->isrc, exc);
    if (exc == EXC_EOU) {
      IFACE_setError((IFACE) isrc, EXC_EOU);
      *snP = SN_MAX;
    } else {
      SEVERE_BRA_ "TXTFRAME_getFrame: unexpected exception %d getting char frame\n", exc _KET;
    }
    return(FALSE);
  }

  *snP = snRead;

  /* If there is a frame available, but not requested, get out */
  if (outP == NULL)
    return TRUE;

  /* Get the char frame needed to derive the feature frame */
  if (!ISRC_getFrame(src, FORWARD, &snRead, &charIn)) {
    exc = IFACE_error((IFACE) src);
    IFACE_setError((IFACE) txtframe->isrc, exc);
    SEVERE_BRA_ "TXTFRAME_getFrame: unexpected exception %d getting char frame\n", exc _KET;
    return(FALSE);
  }
  assert(charIn);
  readP = (Char *)&OBJT_contents(charIn);
  /* Make a output frame */
  *outP = OBJT_new(txtframe->frameClass);
  assert(*outP);
  writeP = (Char *)&OBJT_contents(*outP);
  memset(writeP, 0, OBJT_size(*outP));
  *writeP = *readP;
  OBJT_free(charIn);
  return TRUE;
}

static Bool
TXTFRAME_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an OBJT via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return TXTFRAME_getFrame(isrc, dir, snP, NULL);
}

/*** ITXTFRAME Methods ***/

static ITXTFRAME
ITXTFRAME_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ITXTFRAME interface to the frame maker
 *---------------------------------------------------------------------------*/
{
  ITXTFRAME  itxtframe;

  if ((itxtframe = (ITXTFRAME) IFACE_calloc((IFACE) igen, 1, sizeof(itxtframe_t)))) {
    IFACE_setClass((IFACE) itxtframe, &ITXTFRAME_Class);
    IFACE_setObject((IFACE) itxtframe, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) itxtframe, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) itxtframe);
  }

  return(itxtframe);
}

/*** TXTFRAMEATTR Methods ***/

IGEN
TXTFRAMEATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a TXTFRAME attributes object
 *---------------------------------------------------------------------------*/
{
  TXTFRAMEATTR   txtframeattr = (TXTFRAMEATTR) imem->Calloc(imem, 1, sizeof(txtframeattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(imem, (OBJ) txtframeattr);
  ITXTFRAMEATTR  itxtframeattr = ITXTFRAMEATTR_create(igen);

  /* Generic IGEN_annihilate() suffices */

  /* Set the default attributes */
  txtframeattr->classSize = DFLT_CLASS_SIZE;

  return(igen);
}

/*** ITXTFRAMEATTR METHODS ***/

static ITXTFRAMEATTR
ITXTFRAMEATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ITXTFRAMEATTR interface
 *---------------------------------------------------------------------------*/
{
  ITXTFRAMEATTR  itxtframeattr;

  if ((itxtframeattr = (ITXTFRAMEATTR) IFACE_calloc((IFACE) igen, 1, sizeof(itxtframeattr_t)))) {
    IFACE_setClass((IFACE) itxtframeattr, &ITXTFRAMEATTR_Class);
    IFACE_setObject((IFACE) itxtframeattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) itxtframeattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) itxtframeattr);
  }

  return(itxtframeattr);
}

Bool
ITXTFRAMEATTR_getClassSize(ITXTFRAMEATTR itxtframeattr, Int * classSizeP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  TXTFRAMEATTR  txtframeattr;

  assert(itxtframeattr != NULL);
  txtframeattr = (TXTFRAMEATTR) IFACE_object((IFACE) itxtframeattr);

  *classSizeP = txtframeattr->classSize;

  return(TRUE);
}

Bool
ITXTFRAMEATTR_setClassSize(ITXTFRAMEATTR itxtframeattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  TXTFRAMEATTR  txtframeattr;

  assert(itxtframeattr != NULL);
  txtframeattr = (TXTFRAMEATTR) IFACE_object((IFACE) itxtframeattr);

  txtframeattr->classSize = classSize;

  return(TRUE);
}

Bool
ITXTFRAMEATTR_getFrameSize(ITXTFRAMEATTR iTxtFrameAttr, Int * frameSizeP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size of a frame
 *---------------------------------------------------------------------------*/
{
  TXTFRAMEATTR  txtframeattr;

  assert(iTxtFrameAttr != NULL);
  txtframeattr = (TXTFRAMEATTR) IFACE_object((IFACE) iTxtFrameAttr);

  *frameSizeP = txtframeattr->frameSize;

  return(TRUE);
}

Bool
ITXTFRAMEATTR_setFrameSize(ITXTFRAMEATTR iTxtFrameAttr, Int feVersion)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Compute and set the size of an output frame on basis of feVersion
 *---------------------------------------------------------------------------*/
{
  TXTFRAMEATTR  txtframeattr;
  Int           cepVecLen;

  assert(iTxtFrameAttr != NULL);
  txtframeattr = (TXTFRAMEATTR) IFACE_object((IFACE) iTxtFrameAttr);

  FEVER_getCepVecLen(feVersion, &cepVecLen);

  if (VALID_FE_VISE(feVersion))
    txtframeattr->frameSize = 2 * cepVecLen;
  else if (VALID_FE_FVISE(feVersion))
    txtframeattr->frameSize = cepVecLen * sizeof(Val);
  else
    FATAL_BRA_ "ITXTFRAMEATTR_setFrameSize: BAD feVersion used for setting frame size\n" _KET;

  return(TRUE);
}
