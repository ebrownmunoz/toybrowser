/* cep.c - local cepstral buffer
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: cep.c[2.3] Tue May  3 15:09:39 1994 dvetter@noname saved $";


#include "pthr.h"

#include <stdlib.h>
#include <ctype.h>
#include "types.h"
#include "floorfix.h"
#include "cep.h"
#include "vbx.h"

#define CEP_FRAME_REPEAT  10

/* The cepstral buffer struct */
typedef struct cepbuf_s {
  Val           * start;        /* Ptr to 1st coefficient in 1st frame */
  Int             size;         /* Total number of frames */
  Val           * current;      /* Ptr to 1st coefficient in current frame */
  Int             left;         /* Number of frames left */
  Int             frameSize;    /* Number of coefficients (Vals) per frame */
  Int             seqNum;       /* The sequence number of the frame */
} cepbuf_t, *CEPBUF;

Ptr
CEP_newBuffer(void)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a new cepstral buffer
 *---------------------------------------------------------------------------*/
{
  CEPBUF cepbuf;

  cepbuf = (CEPBUF) VBX_calloc(1, sizeof(cepbuf_t));
  cepbuf->seqNum = 1;
  return((Ptr) cepbuf);
}

void
CEP_setBuffer(Ptr buf, Val * cep, size_t numFrames, Int frameSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize the specified cepstral buffer
 *---------------------------------------------------------------------------*/
{
  CEPBUF cepbuf = (CEPBUF) buf;

  cepbuf->start = cepbuf->current = cep;
  cepbuf->size = cepbuf->left = (Int) numFrames;
  cepbuf->frameSize = frameSize;
  cepbuf->seqNum = 1;
}

void
CEP_resetBuffer(Ptr buf)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Re-initialize the specified cepstral buffer
 *---------------------------------------------------------------------------*/
{
  CEPBUF cepbuf = (CEPBUF) buf;

  cepbuf->current = cepbuf->start;
  cepbuf->left = cepbuf->size;
  cepbuf->seqNum = 1;
}


Val *
CEP_nextFrame(Ptr buf, Long * seqNumPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns a ptr to the next frame in the cepstral buffer, or NULL if none.
 *---------------------------------------------------------------------------*/
{
  CEPBUF  cepbuf = (CEPBUF) buf;
  Val   * thisFrame;
  
  if (cepbuf->left) {
    *seqNumPtr = (Long) cepbuf->seqNum++;
    thisFrame = cepbuf->current;
    cepbuf->current += cepbuf->frameSize;
    cepbuf->left--;
  } else {
    *seqNumPtr = (Long) 0;
    thisFrame = NULL;
  }
  
  return (thisFrame);
}


Val *
CEP_frame(Ptr buf, Int i, Long * seqNumPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns a pointer to the ith frame in the cepstral buffer, or NULL if none.
 *---------------------------------------------------------------------------*/
{
  CEPBUF  cepbuf = (CEPBUF) buf;
  Val   * ithFrame;

  *seqNumPtr = (Long) (i + 1);
  if (0 <= i && i < cepbuf->size)
    ithFrame = cepbuf->start + i * cepbuf->frameSize;
  else
    ithFrame = NULL;

  return (ithFrame);
}

Bool
CEP_frameAvail(Ptr buf)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns TRUE if there is currently a frame available, FALSE otherwise.
 *---------------------------------------------------------------------------*/
{
  CEPBUF  cepbuf = (CEPBUF) buf;

  return((cepbuf->left == 0) ? FALSE : TRUE);
}
