#include "pthr.h"
#include <math.h>
#include "floorfix.h"

 Void
fft(Val *x, UInt n, UInt m, Val * table)
{
  Int             i, j, k, n1, n2, n4, i1, i2, i3, i4, ir0, ir1;
  Val             t1, t2, xt, *ptsin;

  /* Switch from C to Fortran array indexing */
  x--;
  
  /* Bit reversal */
  j = 1;
  for (i = 1; i < n; i++) {
    if (j > i) {
      xt = x[j];
      x[j] = x[i];
      x[i] = xt;
    }
    k = n >> 1;
    while (j > k) {
      j -= k;
      k >>= 1;
    }
    j += k;
  }

  /* length two butterflies */
  for (i = 1; i <= n; i += 2) {
    xt = x[i];
    x[i] = xt + x[i + 1];
    x[i + 1] = xt - x[i + 1];
  }

  /* other butterflies */
  /* first pass of the do-20 loop with n4=1 (stage k=2 in do-10 loop) */
  for (i = 1; i <= n; i += 4) {
    xt = x[i];
    x[i] = xt + x[i + 2];
    x[i + 2] = xt - x[i + 2];
    x[i + 3] = -x[i + 3];
  }

  n4 = 1;
  n2 = 2;
  n1 = 4;
  ir0 = n >> 2;                         /* index into sin table */
  ir1 = ir0;                            /* const displacement frm sin to cos */
  for (k = 3; k <= m; k++) {
    n4 <<= 1;
    n2 <<= 1;
    n1 <<= 1;
    ir0 >>= 1;

    for (i = 1; i <= n; i += n1) {
      xt = x[i];
      x[i] = xt + x[i + n2];
      x[i + n2] = xt - x[i + n2];
      x[i + n4 + n2] = -x[i + n4 + n2];
      ptsin = table + ir0;

      for (j = 1; j < n4; j++, ptsin += ir0) {
        i1 = i + j;
        i2 = i - j + n2;
        i3 = i + j + n2;
        i4 = i - j + n1;
        t1 = NORMBACK(ptsin[ir1] * x[i3] + *ptsin * x[i4]);
        t2 = NORMBACK(*ptsin * x[i3] - ptsin[ir1] * x[i4]);
        x[i4] = x[i2] - t2;
        x[i3] = -x[i2] - t2;
        x[i2] = x[i1] - t1;
        x[i1] = x[i1] + t1;
      }
    }
  }
}
