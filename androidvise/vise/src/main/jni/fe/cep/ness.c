#include "pthr.h"
#include <limits.h>
#include "floorfix.h"
#include "imemory.h"
#include "vbx.h"
#include "ness.h"

#ifdef FIXED_POINT
/* Division with rounding to the nearest integer. Works for POSITIVE numbers only !!! */
#define RNDIV(n,d) ((((n)<<1)/(d)+1)>>1)
/* Right shift with rounding to the nearest integer. Works for POSITIVE numbers only. s >= 0 !!! */
#define RNSHIFT(n,s) (((((n)<<1)>>(s))+1)>>1)
#else  /* Floating Point */
#define RNDIV(n,d) ((n)/(d))
#endif  /* FIXED_POINT */

 /* Number of power spectrum bins.
    Should be parameter of NESS_create along with filterCnt */
#define DFTSIZE     128

typedef struct vin_s {
  Val   value;
  Int   shift;
} vin_t, *VIN;

typedef struct specs_s {
  Val   spectr[DFTSIZE];
  Val   power;
  Int   shift;
} specs_t, *SPECS;

typedef struct ness_s {
  IMemory * imem;
  Val     * NHat;            /* Current noise estimate */
  Val       powerHat;        /* Current noise  powerDB estimate */
  Int       shiftHat;        /* Accumulated shift for NHat when FIXED_POINT */
  Val     * NCal;            /* Noise mesured during calibration if any */
  Val       powerCal;        /* Calibration noise powerDB estimate */
  Int       shiftCal;        /* Accumulated shift for NCal when FIXED_POINT */
  Int       numCal;          /* Number of frames used for calibration so far */
  Int       maxCal;          /* Maximum number of frames to use for calibration */
  SPECS     CBuf;            /* Buffer for Noise Spectrum, logPower, shifts during calibration */
  Val     * SHat;            /* Clean signal estimate */
  Int       shiftSig;        /* Accumulated shift for SHat when FIXED_POINT */
  Int       FilterCnt;       /* Number of Mel filters */
  Val       One;             /* 1 scaled by 16 in FIXED_POINT variant */
  Val       oldSNRweight;    /* OLD SNR weight in recursive update */
  Val       newSNRweight;    /* NEW SNR weight in recursive update */
  Int       reNoiT;          /* Noise reestimation threshold, scaled by 16 */
  Int       maSilT;          /* Silence masking threshold, scaled by 16 */
  Val       Mask;            /* White Noise Mask value before skaling */
  Val       maxMask;         /* Upper limit on Mask value before skaling */
  Bool      First;           /* TRUE if it is first frame of the utterance */
  SPECS     NBuf;            /* Last BUFSIZE samples of Noise Power Spectrum, logPower, shift */
  Int       shiftBuf;        /* Average of entries' shifts for NBuf when FIXED_POINT */
  Val       SumPower;        /* Sum of NBUF power components */
  Int       SumShift;        /* Sum of NBUF shift components when FIXED_POINT */
  Val     * SumBuf;          /* Sum of NBUF bins */
  Int       bufNum;          /* Actual number of noise framess in NBuf so far */
  Int       bufIndex;        /* Next position in the NBuf to be filled */
  Bool      calibrationDone; /* TRUE if noise calibration took place */
  Bool      calInProcess;    /* TRUE with ongoing background noise calibration */
  Int       noiseTracking;   /* 0 if backup noise estimation based on spectrum minimum tracking is not allowed */
  Val     * PHat;            /* Smoothed periodogram for within 1s window minimum tracking providing noise estimate */
  Int       shiftPer;        /* Accumulated shift for PHat when FIXED_POINT */
  VIN       minP;            /* Current minimum of Smoothed periodogram */
  Int       resetMinFrame;   /* When this frame was processed minP became NCal and new min tracking started */
} ness_t;
/* Recursive weights */
#define OLDSNRWEIGHT (0.75F)
/* Reestimation and Masking Thresholds */
#define RENOIT       (1.6875F)
#define MASILT       (1.1875F)
/* Skip at least 10 when release to public since CE audio produces that much garbage !!! */
#define NUMTOSKIP    (6)
/* After skipping use this number of frames to estimate backup noise */
#define NUMTOCAL     (4)
/* Search for the minimum of smoothed periodogram within this window of frames */
#define MINWINDOW    (80)
/* Limits on the number of frames for calibration */
#define MINFRAMES    (100)
#define MAXFRAMES    (256)
/* Mask limits */
#define MIN_MASK     (4096)
#define DFLT_MASK    (16384)
#define MAX_MASK     (1048576)
 /* Noise Buffer Size */
#define BUFSIZE      32

static Int  NESS_noiseProximity(NESS ness, Val * mfs, Int accShift, Int * curActivity);
static void NESS_noiseInsert(NESS ness, Val * mfs, Val logPower, Int accShift, Bool sweep);
static void NESS_noiseExtract(NESS ness, Val * noise, Val * logPower, Int * shiftHat);

NESS
NESS_create(IMemory * imem, Int filterCnt)
{
  NESS ness;
  Int    i;
  
  if (imem == NULL || !(ness = (NESS)imem->Calloc(imem, 1, sizeof(ness_t), FAST_MEMORY)))
    return NULL;

  ness->imem = imem;
  if (!(ness->FilterCnt = filterCnt) ||
      !(ness->NCal = (Val *)imem->Calloc(imem, 1, DFTSIZE * sizeof(Val), FAST_MEMORY)) ||
      !(ness->NHat = (Val *)imem->Calloc(imem, 1, DFTSIZE * sizeof(Val), FAST_MEMORY)) ||
      !(ness->SHat = (Val *)imem->Calloc(imem, 1, DFTSIZE * sizeof(Val), FAST_MEMORY)) ||
      !(ness->SumBuf = (Val *)imem->Calloc(imem, 1, DFTSIZE * sizeof(Val), FAST_MEMORY)) ||
      !(ness->PHat = (Val *)imem->Calloc(imem, 1, DFTSIZE * sizeof(Val), FAST_MEMORY)) ||
      !(ness->minP = (VIN)imem->Calloc(imem, 1, DFTSIZE * sizeof(vin_t), FAST_MEMORY)) ||
      !(ness->NBuf = (SPECS)imem->Calloc(imem, BUFSIZE, sizeof(specs_t), FAST_MEMORY))
     )
    return NULL;

  ness->CBuf = NULL; /* Will be allocated when needed */
  ness->powerHat = ness->powerCal = (Val)0;
  ness->shiftHat = ness->shiftCal = ness->shiftSig = ness->shiftBuf = 0;
  ness->bufNum = ness->bufIndex = 0;
  ness->numCal = 0;
  ness->maxCal = MAXFRAMES;
  ness->One = FLO2LOW(1.0F);
  ness->oldSNRweight = FLO2LOW(OLDSNRWEIGHT);
  ness->newSNRweight = FLO2LOW(1.0F - OLDSNRWEIGHT);
  /* These are scaled by 16 for both FIXED_POINT and floating */
  ness->reNoiT = (Int)(RENOIT * 16);
  ness->maSilT = (Int)(MASILT * 16);
  ness->Mask = (Val)DFLT_MASK;
  ness->maxMask = (Val)MAX_MASK;
  ness->First = TRUE;
  ness->calibrationDone = FALSE;
  ness->calInProcess = FALSE;
   /* Background noise minimum tracking is OFF by default */
  ness->noiseTracking = 0;
  ness->resetMinFrame = 0;
  return ness;
}

void
NESS_destroy(NESS ness)
{
  IMemory * imem;
  int       i;

  if (ness) {
    if ((imem = ness->imem) != NULL) {
      imem->Free(imem, ness->NHat);
      imem->Free(imem, ness->NCal);
      imem->Free(imem, ness->SHat);
      imem->Free(imem, ness->SumBuf);
      imem->Free(imem, ness->NBuf);
      imem->Free(imem, ness->PHat);
      imem->Free(imem, ness->minP);
      imem->Free(imem, ness);
    }
  }
}

void
NESS_reset(NESS ness)
{
  ness->First = TRUE;
  ness->numCal = 0;
}

void
NESS_startCalibration(NESS ness, Int maxFrames)
{
  IMemory * imem = ness->imem;

  if (!ness->calInProcess) {
    maxFrames -= NUMTOSKIP;
    if (maxFrames < MINFRAMES) maxFrames = MINFRAMES;
    if (maxFrames > MAXFRAMES) maxFrames = MAXFRAMES;
    ness->maxCal = maxFrames;
    if (!(ness->CBuf = (SPECS)imem->Calloc(imem, maxFrames, sizeof(specs_t), FAST_MEMORY))) {
      VBX_print("NESS_startCalibration: ERROR !! FAILED to allocate memory for calibration\n");
      return;
    }
    ness->calInProcess = TRUE;
    ness->First = TRUE;
    ness->numCal = 0;
  }
}

void
NESS_stopCalibration(NESS ness)
{
  IMemory * imem = ness->imem;
  Int       i, j, n;
  Val     * sum = ness->SumBuf, power;
  Val     * bins;
  SPECS     entry;
  static Bool report = TRUE;

  if (ness->calInProcess) {
    ness->calInProcess = FALSE;
    if (ness->numCal) {
      ness->calibrationDone = TRUE;
       /* Ini sums with 0 */
      for (i = 0; i < DFTSIZE; i++)
        sum[i] = (Val)0;
      power = (Val)0;
#ifdef FIXED_POINT
     /* Compute new shift. Shifts are negative and even. Make new one 
        closest even, less negative to mean. This will result in lossless left
        shift of frames most of the time. !!! Make it shift(Cal/Hat) */
      for (j = 0, n = 0, entry =  ness->CBuf; j < ness->numCal; j++, entry++)
        n += entry->shift;
      j = RNDIV(-n, ness->numCal);
      ness->shiftCal = (j & 1) - j;
      ness->shiftHat = ness->shiftCal;
#endif  /* FIXED_POINT */
       /* Find the sum of power and bins */
      for (j = 0, entry =  ness->CBuf; j < ness->numCal; j++, entry++) {
        power += entry->power;
        bins = entry->spectr;
#ifdef FIXED_POINT
         /* Align entry to shiftCal scale. The more negative shift
           accompanies value, the farther right value is shifted */
        n = entry->shift - ness->shiftCal;
        if (n == 0) {
          for (i = 0; i < DFTSIZE; i++)
            sum[i] += bins[i];
        } else if (n > 0) {
          for (i = 0; i < DFTSIZE; i++)
            sum[i] += RNSHIFT(bins[i], n);
        } else {
          for (i = 0; i < DFTSIZE; i++)
            sum[i] += bins[i] << -n;
        }
#else  /* Floating Point */
        for (i = 0; i < DFTSIZE; i++)
          sum[i] += bins[i];
#endif  /* FIXED_POINT */
      }
      ness->powerCal = RNDIV(power, ness->numCal);
      ness->powerHat = ness->powerCal;
      for (i = 0; i < DFTSIZE; i++) {
        ness->NCal[i] = RNDIV(sum[i], ness->numCal);
        if (ness->NCal[i] < 0 && report) {
          VBX_print("NESS_stopCalibration: ERROR !!! OVERFLOW !!! \n");
          report = FALSE;
        }
         /* Prevent division by 0 */
        if (ness->NCal[i] == (Val)0) ness->NCal[i] = (Val)1;
        ness->NHat[i] = ness->NCal[i];
      }
       /* Initialize all entries of the NBuf for further noise adaptation */
      NESS_noiseInsert(ness, ness->NHat, ness->powerHat, ness->shiftHat, TRUE);
      imem->Free(imem, ness->CBuf);
    }
  }
}

Int
NESS_getNoiseTracking(NESS ness)
{
  return ness->noiseTracking;
}

void
NESS_setNoiseTracking(NESS ness, Int track)
{
  ness->noiseTracking = track;
}

void
NESS_setMask(NESS ness, Int gainShift)
{
   /* from MIN_MASK == 4096 to 128*MIN_MASK == 524288 */
  Int m = MIN_MASK;

  if (gainShift > 0)
    m <<= gainShift;
/* ness->Mask = (Val)m; */
  ness->maxMask = (Val)m;
}

Int
NESS_activity(NESS ness, Val * mfs, Val powerDB, Int accShift)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Doesn't affect mfs. When FIXED_POINT, alignes mfs or N(Hat|Cal) to one with
 * more negative shiftVal before adding. Mind shift negativness. Returns -1,
 * when hasn't had noise estimate yet (either calibrates or skips bad frames).
 *---------------------------------------------------------------------------*/
{
  Int       activity, curActivity;
  Int       i, m, n, v;
  Val       val;
  Val     * bins;
  SPECS     entry;

   /* Skip several possibly corrupted frames at the beginning of the utterance */
  if (ness->First && ness->numCal < NUMTOSKIP) {
    ness->numCal++;
    return -1;
  }

   /* If we are doing calibration collect ness->maxCal frames in CBuf */
  if (ness->calInProcess) {
    if (ness->First) {
      ness->First = FALSE;
      ness->numCal = 0;
    }
    if (ness->numCal < ness->maxCal) {
      entry = ness->CBuf + ness->numCal++;
      bins = entry->spectr;
      for (i = 0; i < DFTSIZE; i++)
        bins[i] = mfs[i];
      entry->power = powerDB;
      entry->shift = accShift;
    }
    return -1;
  }

   /* Not a calibration. Handle beginning of the utterance */
  if (ness->First) {
     /* Initialize backup noise with presumed silence frame. This will allow to adapt
        to abrupt change of noise at least for new utterance */
    ness->powerCal = powerDB;
    ness->shiftCal = accShift;
    for (i = 0; i < DFTSIZE; i++) {
      ness->NCal[i] = mfs[i];
      if (ness->NCal[i] == (Val)0) ness->NCal[i] = (Val)1;
    }
    ness->numCal = 1;
    if (!ness->calibrationDone && ness->powerHat == (Val)0) {
       /* Bootstrap noise estimation with the first frame presumed to be silence */
      ness->powerHat = powerDB;
      ness->shiftHat = accShift;
      for (i = 0; i < DFTSIZE; i++) {
        ness->NHat[i] = mfs[i];
        if (ness->NHat[i] == (Val)0) ness->NHat[i] = (Val)1;
         /* Initialze clean speech assuming its absence */
        ness->SHat[i] = (Val)0;
      }
      /* Put noise into empty NBuf */
      NESS_noiseInsert(ness, ness->NHat, powerDB, accShift, FALSE);
      ness->First = FALSE;
      return 0;
    }
  }

  /* Update backup noise */
  if (ness->numCal < NUMTOCAL && ness->numCal > 1) {
    m = ness->numCal + 1;
    ness->powerCal = RNDIV(ness->numCal * ness->powerCal + powerDB, m);
#ifdef FIXED_POINT
     /* Align noise to mfs scale */
    n = accShift - ness->shiftCal;
    if (n == 0) {
      for (i = 0; i < DFTSIZE; i++) {
        ness->NCal[i] = RNDIV(ness->numCal * ness->NCal[i] + mfs[i], m);
        if (ness->NCal[i] == 0) ness->NCal[i] = 1;
      }
    } else if (n > 0) {
      for (i = 0; i < DFTSIZE; i++) {
        ness->NCal[i] = RNDIV(ness->numCal * (ness->NCal[i] << n) + mfs[i], m);
        if (ness->NCal[i] == 0) ness->NCal[i] = 1;
      }
    } else {
      for (i = 0; i < DFTSIZE; i++) {
        ness->NCal[i] = RNDIV(ness->numCal * (ness->NCal[i] >> -n) + mfs[i], m);
        if (ness->NCal[i] == 0) ness->NCal[i] = 1;
      }
    }
    ness->shiftCal = accShift;
#else  /* Floating Point */
    for (i = 0; i < DFTSIZE; i++) {
      ness->NCal[i] = (ness->numCal * ness->NCal[i] + mfs[i]) / m;
    }
#endif  /* FIXED_POINT */
    if (ness->noiseTracking && m == NUMTOCAL) {
       /* Initialize smoothed periodogram minimum tracking */
      ness->resetMinFrame = ness->numCal;
      ness->shiftPer = ness->shiftCal;
      for (i = 0; i < DFTSIZE; i++) {
        ness->PHat[i] = ness->NCal[i];
        ness->minP[i].value = ness->NCal[i];
        ness->minP[i].shift = ness->shiftCal;
      }
    }
  } else if (ness->noiseTracking && ness->numCal >= NUMTOCAL) {
#ifdef FIXED_POINT
     /* Update smoothed periodogram using simple recursive smoothing with forgetting factor of 7/8 */
    Quad temp;
    if (accShift == ness->shiftPer) {
      for (i = 0; i < DFTSIZE; i++) {
        temp = (7 * (Quad)ness->PHat[i] + (Quad)mfs[i]) >> 3;
        ness->PHat[i] = (temp > INT_MAX) ? INT_MAX : (Int)temp;
      }
    } else if (accShift > ness->shiftPer) {
       /* Slowly move more negative PHat shift, scaling PHat up, in direction of the less negative mfs shift */
      ness->shiftPer++;
      for (i = 0; i < DFTSIZE; i++) {
        temp = (7 * ((Quad)ness->PHat[i] << 1) + ((Quad)mfs[i] >> (accShift - ness->shiftPer))) >> 3;
        ness->PHat[i] = (temp > INT_MAX) ? INT_MAX : (Int)temp;
      }
    } else {
       /* Here we move much faster to avoid overflow. Target n for ness->shiftPer first gets within distance of 3
          to mfs shift, then continue with single steps if needed. Mind: shifts are negative */
      if (ness->shiftPer - accShift > 3) n = accShift + 3;
      else n = ness->shiftPer - 1;
      for (i = 0; i < DFTSIZE; i++) {
        temp = (7 * ((Quad)ness->PHat[i] >> (ness->shiftPer - n)) + ((Quad)mfs[i] << (n - accShift))) >> 3;
        ness->PHat[i] = (temp > INT_MAX) ? INT_MAX : (Int)temp;
      }
      ness->shiftPer = n;
    }
    if (ness->numCal - ness->resetMinFrame < MINWINDOW) {
       /* Update minimum statistics */
      for (i = 0; i < DFTSIZE; i++) {
        n = ness->shiftPer - ness->minP[i].shift;
        if (n == 0) {
          if (ness->PHat[i] < ness->minP[i].value)
            ness->minP[i].value = ness->PHat[i];
        } else if (n > 0) {
          if ((ness->PHat[i] >> n) < ness->minP[i].value) {
            ness->minP[i].value = ness->PHat[i];
            ness->minP[i].shift = ness->shiftPer;
          }
        } else {
          if (ness->PHat[i] < (ness->minP[i].value >> -n)) {
            ness->minP[i].value = ness->PHat[i];
            ness->minP[i].shift = ness->shiftPer;
          }
        }
      }
    } else {
       /* Find the most negative shift. Make it the closest less negative and even */
      for (i = 0, m = 0; i < DFTSIZE; i++)
        if (ness->minP[i].shift < m)
          m = ness->minP[i].shift;
      m += m & 1;
       /* Redefine backup noise with 50% bias. Reinitialize minimum tracking */
      ness->resetMinFrame = ness->numCal;
      ness->shiftCal = m;
      for (i = 0; i < DFTSIZE; i++) {
        n = ness->minP[i].shift - m;
        if (n == 0)     ness->NCal[i] = (3 * ness->minP[i].value) >> 1;
        else if (n > 0) ness->NCal[i] = (3 * (ness->minP[i].value >> n)) >> 1;
        else            ness->NCal[i] = (3 * (ness->minP[i].value << -n)) >> 1;
        if (ness->NCal[i] == 0) ness->NCal[i] = 1;
        ness->minP[i].value = ness->PHat[i];
        ness->minP[i].shift = ness->shiftPer;
      }
    }
#else  /* Floating Point */
    for (i = 0; i < DFTSIZE; i++) {
       /* Update smoothed periodogram using simple recursive smoothing with forgetting factor of 7/8 */
      ness->PHat[i] = (7.0F * ness->PHat[i] + mfs[i]) / 8.0F;
      if (ness->numCal - ness->resetMinFrame < MINWINDOW) {
         /* Update minimum statistics */
        if (ness->PHat[i] < ness->minP[i].value)
          ness->minP[i].value = ness->PHat[i];
      } else {
        /* Redefine backup noise with 50% bias. Reinitialize minimum tracking */
        ness->NCal[i] = 1.5F * ness->minP[i].value;
        ness->minP[i].value = ness->PHat[i];
      }
    }
    if (ness->numCal - ness->resetMinFrame >= MINWINDOW)
      ness->resetMinFrame = ness->numCal;
#endif  /* FIXED_POINT */
  }
  ness->numCal++;

   /* Compute previous frame noise and clean signal dependent part of a priori SNR,
      and put it in SHat temporarily. It is scaled by 16 in FIXED_POINT case */
#ifdef FIXED_POINT
  n = ness->shiftSig - ness->shiftHat;
  if (n == 0) {
    for (i = 0; i < DFTSIZE; i++)
      ness->SHat[i] = (ness->First) ? ness->oldSNRweight : (ness->oldSNRweight * ness->SHat[i]) / ness->NHat[i];
  } else if (n > 0) {
    for (i = 0; i < DFTSIZE; i++)
      ness->SHat[i] = (ness->First) ? ness->oldSNRweight : (ness->oldSNRweight * ness->SHat[i]) / (ness->NHat[i] << n);
  } else {
    for (i = 0; i < DFTSIZE; i++) {
      v = ness->NHat[i] >> -n;
      if (0 == v) v = 1;
      ness->SHat[i] = (ness->First) ? ness->oldSNRweight : (ness->oldSNRweight * ness->SHat[i]) / v;
    }
  }
#else  /* Floating Point */
  for (i = 0; i < DFTSIZE; i++)
    ness->SHat[i] = (ness->First) ? ness->oldSNRweight : (ness->oldSNRweight * ness->SHat[i]) / ness->NHat[i];
#endif  /* FIXED_POINT */

   /* If the frame is suitable for noise reestimation, do it */
  activity = NESS_noiseProximity(ness, mfs, accShift, &curActivity);
  if (activity < ness->reNoiT) {
    NESS_noiseInsert(ness, mfs, powerDB, accShift, FALSE);
    NESS_noiseExtract(ness, ness->NHat, &ness->powerHat, &ness->shiftHat);
  }

   /* Now estimate clean speech */
  for (i = 0; i < DFTSIZE; i++) {
    Val ksi, ksiplusone;
     /* Update a ppriori SNR with current frame contribution */
    ksi = ness->SHat[i];
    val = ness->NHat[i];
#ifdef FIXED_POINT
     /* Align noise to mfs scale */
    n = accShift - ness->shiftHat;
    if (n > 0)
      val <<= n;
    else if (n < 0) {
      val >>= -n;
      if (0 == val) val = 1;
    }
    ksi += (ness->newSNRweight * mfs[i]) / val - ness->newSNRweight;
    if (ksi > 0) {
      Quad tmp = (Quad)ksi * (Quad)mfs[i];
      ksiplusone = ksi + ness->One;
      tmp /= ksiplusone;
      ness->SHat[i] = (Val)((tmp * (Quad)ksi) / ksiplusone);
    } else {
      ness->SHat[i] = 0;
    }
#else  /* Floating Point */
    ksi += (ness->newSNRweight * mfs[i]) / val - ness->newSNRweight;
    if (ksi > (Val)0) {
      Val tmp = ksi / (ksi + ness->One);
      ness->SHat[i] = tmp * tmp *  mfs[i];
    } else {
      ness->SHat[i] = (Val)0;
    }
#endif  /* FIXED_POINT */
    mfs[i] = ness->SHat[i];
    ness->shiftSig = accShift;
  }
  
  ness->First = FALSE;
  /* If frame is almost certainly silence make activity==0 with following complete frame masking */
  if (curActivity < ness->maSilT) activity = 0;
  return activity;
}

void
NESS_apply(NESS ness, Val * mfs, Val powerDB, Int accShift, Int activity)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Masks Mel-filter outputs. When FIXED_POINT, alignes  Mask
 * to mfs scale before masking. Mind shift negativness.
 *---------------------------------------------------------------------------*/
{
  Int  i, m, n, v;
  Val  mask;

  /* Compute Mask for this frame on basis of current powerHat estimate */
  /* It defaults to DFLT_MASK upto 22 dB then grows at half rate of noise
     power, so it doubles (adds 3 dB) for each 6 dB increase of powerHat */
  m = DFLT_MASK;
  v = (Int)(ness->powerHat - (Val)22);
  if (v > 0) {
    n = v / 6;
    if (n > 0)
      m <<= n;
    /* Now fractional part: for each 1 dB multiply by 10**0.05 = 9/8 */
    n = v - 6 * n;
    for (i = 0; i < n; i++)
      m = (m * 9) >> 3;
  }
  ness->Mask = (Val)m;
  if (ness->Mask > (Val)MAX_MASK) ness->Mask = (Val)MAX_MASK;

  mask = ness->Mask;
#ifdef FIXED_POINT

   /* Align mask to mfs scale */
  if (accShift < 0)
    mask >>= -accShift;
  if (0 == mask)
    mask = 1;

#endif  /* FIXED_POINT */

  for (i = 0; i < ness->FilterCnt; i++)
    if (mfs[i] < mask)
      mfs[i] = mask;
}

static Int
NESS_noiseProximity(NESS ness, Val * mfs, Int accShift, Int * curActivity)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Computes proximity to both current noise estimate and calibration noise,
 * if any, as averaged over bins minimum SNR scaled by 16.
 *---------------------------------------------------------------------------*/
{
  Val  avgSNRcur = (Val)0, avgSNRcal = (Val)0;
  Int  i, n, ret, v;

#ifdef FIXED_POINT
  /* Align noise to mfs scale. The more negative shift
     accompanies value, the farther right value is shifted */
  n = accShift - ness->shiftHat;
  if (n == 0) {
    for (i = 0; i < DFTSIZE; i++)
      avgSNRcur += (mfs[i] << SHIFTLOW) / ness->NHat[i];
  } else if (n > 0) {
    for (i = 0; i < DFTSIZE; i++)
      avgSNRcur += (mfs[i] << SHIFTLOW) / (ness->NHat[i] << n);
  } else {
    for (i = 0; i < DFTSIZE; i++) {
      v = ness->NHat[i] >> -n;
      if (0 == v) v = 1;
      avgSNRcur += (mfs[i] << SHIFTLOW) / v;
    }
  }
  avgSNRcur = RNDIV(avgSNRcur, DFTSIZE);
  *curActivity = avgSNRcur;
  n = accShift - ness->shiftCal;
  if (n == 0) {
    for (i = 0; i < DFTSIZE; i++)
      avgSNRcal += (mfs[i] << SHIFTLOW) / ness->NCal[i];
  } else if (n > 0) {
    for (i = 0; i < DFTSIZE; i++)
      avgSNRcal += (mfs[i] << SHIFTLOW) / (ness->NCal[i] << n);
  } else {
    for (i = 0; i < DFTSIZE; i++) {
      v = ness->NCal[i] >> -n;
      if (0 == v) v = 1;
      avgSNRcal += (mfs[i] << SHIFTLOW) / v;
    }
  }
  avgSNRcal = RNDIV(avgSNRcal, DFTSIZE);
  if (avgSNRcal < avgSNRcur)
    avgSNRcur = avgSNRcal;
  ret = avgSNRcur;
#else  /* Floating Point */
  for (i = 0; i < DFTSIZE; i++)
    avgSNRcur += mfs[i] / ness->NHat[i];
  avgSNRcur /= DFTSIZE;
  /* Scale by factor 16 */
  ret = *curActivity = (Int)(avgSNRcur * 16.0F + 0.5F);
  for (i = 0; i < DFTSIZE; i++)
    avgSNRcal += mfs[i] / ness->NCal[i];
  avgSNRcal /= DFTSIZE;
  if (avgSNRcal < avgSNRcur)
    ret = (Int)(avgSNRcal * 16.0F + 0.5F);
#endif  /* FIXED_POINT */

  return ret;
}

static void
NESS_noiseInsert(NESS ness, Val * mfs, Val logPower, Int accShift, Bool sweep)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Puts a noise frame mfs and logPower in the circular buffer NBuf,
 * modifies current SumBuf adding new, subtracting replaced data. if (sweep)
 * treats input data as result of calibration, replicating it in all entries.
 *---------------------------------------------------------------------------*/
{
  Int     i, j, n, newShift;
  Val   * sum = ness->SumBuf;
  Val   * bins;
  SPECS   entry;

  if (sweep) {
     /* Populate buffer with calibration results */
    for (j = 0, entry = ness->NBuf; j < BUFSIZE; j++, entry++) {
      for (i = 0, bins = entry->spectr; i < DFTSIZE; i++)
        bins[i] = mfs[i];
      entry->power = logPower;
      entry->shift = accShift;
    }
    ness->shiftBuf = accShift;
    ness->SumPower = BUFSIZE * logPower;
    ness->SumShift = BUFSIZE * accShift;
    for (i = 0; i < DFTSIZE; i++)
      sum[i] = BUFSIZE * mfs[i];
    ness->bufNum = BUFSIZE;
    ness->bufIndex = 0;
  } else if (ness->bufNum == 0) {
     /* No calibration. First frame. Buffer is empty. Initialize */
    entry = ness->NBuf;
    entry->power = ness->SumPower = logPower;
    entry->shift = ness->SumShift = ness->shiftBuf = accShift;
    for (i = 0, bins = entry->spectr; i < DFTSIZE; i++) {
      bins[i] = mfs[i];
      sum[i] = mfs[i];
    }
    ness->bufNum = ness->bufIndex = 1;
  } else {
     /* Get position. Update circular buffer index */
    entry = ness->NBuf + ness->bufIndex++;
    if (ness->bufIndex >= BUFSIZE) ness->bufIndex = 0;
    bins = entry->spectr;
    if (ness->bufNum == BUFSIZE) {
       /* Regular case with new data coming in full buffer.
          Subtract the oldest components from corresponding sums */
      ness->SumPower -= entry->power;
#ifdef FIXED_POINT
      ness->SumShift -= entry->shift;
      n = entry->shift - ness->shiftBuf;
      if (n == 0) {
        for (i = 0; i < DFTSIZE; i++)
          sum[i] -= bins[i];
      } else if (n > 0) {
        for (i = 0; i < DFTSIZE; i++)
          sum[i] -= RNSHIFT(bins[i], n);
      } else {
        for (i = 0; i < DFTSIZE; i++)
          sum[i] -= bins[i] << -n;
      }
#else  /* Floating Point */
      for (i = 0; i < DFTSIZE; i++)
        sum[i] -= bins[i];
#endif  /* FIXED_POINT */
    }
     /* Now put in new data. Works for both full and incomplete buffer */
    for (i = 0; i < DFTSIZE; i++)
      bins[i] = mfs[i];
    entry->power = logPower;
    entry->shift = accShift;
     /* Update entry counter for incomplete buffer */
    if (ness->bufNum < BUFSIZE) ness->bufNum++;
     /* Add to sums */
    ness->SumPower += logPower;
#ifdef FIXED_POINT
    ness->SumShift += accShift;
     /* Compute new shift. Shifts are negative and even. Make the new one 
        closest even, less negative to mean. This will result in lossless left
        shift of incoming frame most of the time, and less frequent sum shift !!! */
    newShift = RNDIV(-ness->SumShift, ness->bufNum);
    newShift = (newShift & 1) - newShift;
     /* If it is less negative than shiftBuf, need to shift SumBuf left */
    n = newShift - ness->shiftBuf;
    if (n > 0) {
      for (i = 0; i < DFTSIZE; i++)
        sum[i] <<= n;
    } else if (n < 0) {
      for (i = 0; i < DFTSIZE; i++)
        sum[i] = RNSHIFT(sum[i], -n);
    }
    ness->shiftBuf = newShift;
     /* Now add properly scaled new data */
    n = entry->shift - ness->shiftBuf;
    if (n == 0) {
      for (i = 0; i < DFTSIZE; i++)
        sum[i] += bins[i];
    } else if (n > 0) {
      /* With right shift must put shifted new data into bins so later it will be subtracted
         as it was put in. Otherwise underflow may happen rendering negative spectrum */
      for (i = 0; i < DFTSIZE; i++) {
        bins[i] = RNSHIFT(bins[i], n);
        sum[i] += bins[i];
      }
      entry->shift = ness->shiftBuf;
      ness->SumShift -= n;
    } else {
      for (i = 0; i < DFTSIZE; i++)
        sum[i] += bins[i] << -n;
    }
#else  /* Floating Point */
    for (i = 0; i < DFTSIZE; i++)
      sum[i] += bins[i];
#endif  /* FIXED_POINT */
  }
}

static void
NESS_noiseExtract(NESS ness, Val * noise, Val * logPower, Int * shiftHat)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Returns average values of noise components and logPower. Puts shiftBuf
 * into *shiftHat.
 *---------------------------------------------------------------------------*/
{
  Int     i;
  Val   * sum = ness->SumBuf;
  static Bool report = TRUE;

  for (i = 0; i < DFTSIZE; i++) {
    noise[i] = RNDIV(sum[i], ness->bufNum);
    if (noise[i] < 0 && report) {
      VBX_print("NESS_noiseExtract: ERROR !!! OVERFLOW !!! \n");
      report = FALSE;
    }
    if (noise[i] == (Val)0) noise[i] = (Val)1;
  }
  *logPower = RNDIV(ness->SumPower, ness->bufNum);
  *shiftHat = ness->shiftBuf;
}
