#include "pthr.h"
#include "imemory.h"
#include "floorfix.h"
#include "rasta.h"

#define RASTA_PRIME_FRAMES   4         /* RASTA cold-start w/this number of frames */
#define RF_COEFF  ((Float)0.94)

typedef struct queueEl_s {
  Val              * mfs;
  struct queueEl_s * next;
} queueEl_t, *QEL;

typedef struct rastaCfg_s {
  Val   coeff;
  Val * mfs;
} rastaCfg_t, *RASTACFG;

typedef struct rast_s {
  IMemory  * imem;
  Val      * PrevFrm;
  queueEl_t  top[QUEUELEN];  /* Queue */
  QEL        qelp;           /* Queue iterator */
  rastaCfg_t RC[QUEUELEN];
  UInt       FilterCount;
  Val        rf_coeff;
  Int        PrimeCnt;       /* RASTA cold-start priming count */
} rast_t;


RAST
RASTA_create(IMemory * imem, UInt filterCount)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a "Relative Adaptive" (RASTA) spectral processor
 *---------------------------------------------------------------------------*/
{
  RAST rast;
  Int  i, j;

  if (imem == NULL || NULL == (rast = (RAST) imem->Calloc(imem, 1, sizeof(rast_t), FAST_MEMORY)))
    return NULL;

  rast->imem = imem;
  if (!(rast->FilterCount = filterCount) ||
      !(rast->PrevFrm = (Val *) imem->Calloc(imem, 1, (Int)filterCount * sizeof(Val), FAST_MEMORY)))
    return NULL;

  rast->qelp = rast->top;
  for (i = 0; i < QUEUELEN; i++) {
    if (!(rast->qelp->mfs = (Val *) imem->Calloc(imem, 1, (Int)filterCount * sizeof(Val), FAST_MEMORY)))
      return NULL;

    for (j = 0; j < filterCount; j++)
      rast->qelp->mfs[j] = (Val)0;

    if (i < QUEUELEN - 1)
      rast->qelp->next = rast->qelp + 1;
    else
      rast->qelp->next = rast->top;

    rast->qelp = rast->qelp->next;
  }

  for (i = 0; i < filterCount; i++)
    rast->PrevFrm[i] = (Val)0;

  rast->RC[0].coeff = FLO2VAL((Float)(-0.2));
  rast->RC[1].coeff = FLO2VAL((Float)(-0.1));
/*rast->RC[2].coeff = FLO2VAL((Float)(0.0)); */
  rast->RC[3].coeff = - rast->RC[1].coeff;
  rast->RC[4].coeff = - rast->RC[0].coeff;

  rast->rf_coeff = FLO2VAL(RF_COEFF);
  rast->PrimeCnt = 0;

  return rast;
}


void
RASTA_destroy(RAST rast)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy a RASTA processor
 *---------------------------------------------------------------------------*/
{
  IMemory * imem;
  Int       i;

  if (rast) {
    if ((imem = rast->imem) != NULL) {
      for (i = 0; i < QUEUELEN; i++) {
        imem->Free(imem, rast->top[i].mfs);
      }
      imem->Free(imem, rast->PrevFrm);
      imem->Free(imem, rast);
    }
  }
}


Bool
RASTA_computeFrame(RAST rast, Val * mfs)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Compute a single frame of RASTA output, given an input frame and history
 *---------------------------------------------------------------------------*/
{
  QEL qelp;
  Int i;
  
  /* First enqueue */
  memcpy(rast->qelp->mfs, mfs, rast->FilterCount * sizeof(Val));
  qelp = rast->qelp = rast->qelp->next;

  /* If warm up in progress quit */
  if (rast->PrimeCnt < RASTA_PRIME_FRAMES) {
    rast->PrimeCnt++;
    return FALSE;
  }

  for (i = 0; i < QUEUELEN; i++) {
    rast->RC[i].mfs = qelp->mfs;
    qelp = qelp->next;
  }

  /* Scale back only rast->PrevFrm[i]. So output adds 8 bits accuracy */
  for (i = 0; i < rast->FilterCount; i++) {
    rast->PrevFrm[i] = mfs[i] = NORMBACK(rast->rf_coeff * rast->PrevFrm[i]) +
                                    rast->RC[0].coeff * rast->RC[0].mfs[i] +
                                    rast->RC[1].coeff * rast->RC[1].mfs[i] +
                                    rast->RC[3].coeff * rast->RC[3].mfs[i] +
                                    rast->RC[4].coeff * rast->RC[4].mfs[i];
  }

  return TRUE;
}


void
RASTA_reset(RAST rast)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the given RASTA processor (erases history)
 *---------------------------------------------------------------------------*/
{
  Int i;

  rast->qelp = rast->top;
  rast->PrimeCnt = 0;
  for (i = 0; i < rast->FilterCount; i++)
    rast->PrevFrm[i] = (Val)0;
}
