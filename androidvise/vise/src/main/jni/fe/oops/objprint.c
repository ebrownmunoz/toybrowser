/* objprint.c - a frame object printer
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 24-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: objprint.c[1.1] Tue Jun  9 17:47:37 1998 dvetter@charon saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "objprint.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include "vbx.h"
#include <assert.h>

#define OBJ_PRINT_PRIO VBX_MID_PRIO

#define DFLT_MAXLINE   13


/* The Class of PRINTATTR (OBJPRINT Attributes) Interface Objects */
static iclass_t IPRINTATTR_Class = {IPRINTATTRID};


/* An OBJPRINT descriptor */
typedef struct objprint_s {
  ISRC    isrc;           /* OBJPRINT's source interface (output) */
  ISNK    isnk;           /* OBJPRINT's sink interface (input) */
  Char  * label;          /* The label to print on each line */
  Int     maxLine;        /* The maximum number of values to put on a line */
} objprint_t, * OBJPRINT;


/* Private functions */

static Bool  OBJPRINT_initialize(IGEN igen, IATTR iattr);
static Bool  OBJPRINT_setAttributes(ISNK isnk, IATTR iattr);
static Bool  OBJPRINT_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  OBJPRINT_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static IPRINTATTR  IPRINTATTR_create(IGEN igen);


/*** OBJPRINT Methods ***/

IGEN
OBJPRINT_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a frame object printer
 *---------------------------------------------------------------------------*/
{
  OBJPRINT   objprint = (OBJPRINT) imem->Calloc(imem, 1, sizeof(objprint_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) objprint);
  ISNK       isnk = ISNK_create(igen);
  ISRC       isrc = ISRC_create(igen);
  IATTR      iattr;

  /* Initialize the object */
  objprint->isnk = isnk;
  objprint->isrc = isrc;
  objprint->label = "";
  objprint->maxLine = DFLT_MAXLINE;

  /* Set up the generic interface */
  IGEN_setInitializer(igen, OBJPRINT_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setAttributes(isrc, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the sink interface */
  ISNK_setSetAttributes(isnk, OBJPRINT_setAttributes);

  /* Set up the source interface */
  ISRC_setGetFrame(isrc, OBJPRINT_getFrame);
  ISRC_setFrameAvailable(isrc, OBJPRINT_frameAvailable);

  return(igen);
}


static Bool
OBJPRINT_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a objprint according to a objprint attributes specification
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  OBJPRINT  objprint = (OBJPRINT) IFACE_object((IFACE) igen);

  IPRINTATTR_getLabel((IPRINTATTR) iattr, &objprint->label);
  IPRINTATTR_getMaxLine((IPRINTATTR) iattr, &objprint->maxLine);

  return(TRUE);
}


/*** CEPSTRAL SOURCE AND SINK METHODS ***/

static Bool
OBJPRINT_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Import object attributes via the sink (input) interface and pass them
 * to the sink associated with the source (output) interface.  This function
 * supplements the default ISNK_setAttributes() function, which calls this
 * first and then sets the attributes of isnk if the call is successful.
 *---------------------------------------------------------------------------*/
{
  OBJPRINT  objprint;
  ISNK      sink;

  assert(isnk);

  objprint = (OBJPRINT) IFACE_object((IFACE) isnk);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(objprint->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) isnk, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, iattr)) {
    /* If successful, set the attributes locally and return TRUE */
    ISRC_setAttributes(objprint->isrc, iattr);
    return(TRUE);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
    return(FALSE);
  }
}


static Bool
OBJPRINT_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a frame via the source (output) interface, calling
 * IOBJATTR_printObject() to print it out.
 *---------------------------------------------------------------------------*/
{
  OBJPRINT   objprint;
  ISRC       src;
  IOBJATTR   iobjattr;
  Char     * label;
  EXC        exc;
  Int        prefixLen;

  assert(isrc && snP);

  objprint = ((OBJPRINT) IFACE_object((IFACE) isrc));

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(objprint->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Get the frame attributes from the sink (input) interface */
  iobjattr = (IOBJATTR) ISNK_attributes(objprint->isnk);

  /* If OBJPRINT has been given a special label to use, use it */
  if (objprint->label) label = objprint->label;
  /* Otherwise, use the label from the object attributes block */
  else IOBJATTR_getLabel(iobjattr, &label);

  /* Obtain the requested frame from the source */
  if (ISRC_getFrame(src, dir, snP, outP)) {
    /* If the source yields a valid frame, print it out and pass it on */
    if (outP) {
      VBX_print("%s "QuadFmt(-19)" %n", label, *snP, &prefixLen);
      if (!IOBJATTR_printObject(iobjattr, *outP, prefixLen, objprint->maxLine)) {
        IFACE_setError((IFACE) isrc, IFACE_error((IFACE) iobjattr));
        return(FALSE);
      }
    } else {
      VBX_print("%s "QuadFmt(-19)" available\n", label, *snP);
    }
    return(TRUE);
  } else {
    /* Otherwise, just pass along whatever exception the source yields */
    exc = IFACE_error((IFACE) src);
    VBX_print("%s %-10ld EXCEPTION %d\n", label, *snP, exc);
    IFACE_setError((IFACE) isrc, exc);
    return(FALSE);
  }
}


static Bool
OBJPRINT_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an OBJT via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(OBJPRINT_getFrame(isrc, dir, snP, NULL));
}


/*** PRINTATTR Methods ***/

IGEN
PRINTATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a OBJPRINT attributes object
 *---------------------------------------------------------------------------*/
{
  IGEN        igen = IGEN_create(imem, NULL);
  IPRINTATTR  iprintattr = IPRINTATTR_create(igen);

  return(igen);
}


/*** IPRINTATTR METHODS ***/

static IPRINTATTR
IPRINTATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IPRINTATTR interface
 *---------------------------------------------------------------------------*/
{
  IPRINTATTR  iprintattr;

  if ((iprintattr = (IPRINTATTR) IFACE_calloc((IFACE) igen, 1, sizeof(iprintattr_t)))) {

    IFACE_setClass((IFACE) iprintattr, &IPRINTATTR_Class);
    IFACE_setObject((IFACE) iprintattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iprintattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iprintattr);

    /* Set the defaults */
    iprintattr->maxLine = DFLT_MAXLINE;
  }

  return(iprintattr);
}
