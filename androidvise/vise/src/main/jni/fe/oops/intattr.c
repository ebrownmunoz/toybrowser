/* intattr.c - integer frame attributes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 25-Oct-96  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: intattr.c[1.1] Tue Jun  9 17:43:15 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "intattr.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "vbx.h"


/* The Class of INTATTR (INT Frame Attributes) Interface Objects */
static iclass_t IINTATTR_Class = {IINTATTRID};

/* INT Attributes */
typedef struct intattr_s {
  Quad nothing;             /* The compiler wants something */
} intattr_t, *INTATTR;

/* Private functions */

static IINTATTR  IINTATTR_create(IGEN igen);
static Bool      IINTATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields);

/*** INTATTR Methods ***/

IGEN
INTATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an integer frame attributes object
 *---------------------------------------------------------------------------*/
{
  INTATTR   intattr = (INTATTR) imem->Calloc(imem, 1, sizeof(intattr_t), OOPS_MEMORY);
  IGEN      igen = IGEN_create(imem, (OBJ) intattr);
  IINTATTR  iintattr = IINTATTR_create(igen);

  /* Generic IGEN_annihilate() suffices */

  /* Set up the default behavior of IINTATTR */
  IOBJATTR_setPrinter((IOBJATTR) iintattr, IINTATTR_dfltPrint);
  IOBJATTR_setLabel((IOBJATTR) iintattr, "INT ");

  return(igen);
}


/*** IINTATTR METHODS ***/

static IINTATTR
IINTATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IINTATTR interface
 *---------------------------------------------------------------------------*/
{
  IINTATTR  iintattr = (IINTATTR) IOBJATTR_create(igen);

  /* Turn it from an IOBJATTR into an IINTATTR */
  IFACE_setClass((IFACE) iintattr, &IINTATTR_Class);

  return(iintattr);
}


static Bool
IINTATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Print out a integer frame object
 *---------------------------------------------------------------------------*/
{
  VBX_print(" %d\n", (Int) objt->contents[0]);

  return(TRUE);
}
