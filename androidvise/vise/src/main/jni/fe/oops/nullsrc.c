/* nullsrc.c - a null passive source
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 8-Nov-96  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: nullsrc.c[1.1] Tue Jun  9 17:45:49 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "nullsrc.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include <assert.h>


/* The Class of NULLSRCATTR (NULLSRC Attributes) Interface Objects */
static iclass_t INULLSRCATTR_Class = {INULLSRCATTRID};

/* NULLSRC attributes */
typedef struct nullsrcattr_s {
  EXC      bwdExc;
  EXC      fwdExc;
} nullsrcattr_t, * NULLSRCATTR;

/* A NULLSRC descriptor */
typedef struct nullsrc_s {
  ISRC     isrc;          /* NULLSRC's source interface (output) */
  EXC      bwdExc;        /* The exception to report on BACKWARD requests */
  EXC      fwdExc;        /* The exception to report on FORWARD requests */
} nullsrc_t, * NULLSRC;

/* Private functions */

static Bool  NULLSRC_initialize(IGEN igen, IATTR iattr);
static Bool  NULLSRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  NULLSRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static INULLSRCATTR  INULLSRCATTR_create(IGEN igen);


/*** NULLSRC Methods ***/

IGEN
NULLSRC_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a NULLSRC object
 *---------------------------------------------------------------------------*/
{
  NULLSRC   nullsrc = (NULLSRC) imem->Calloc(imem, 1, sizeof(nullsrc_t), OOPS_MEMORY);
  IGEN      igen = IGEN_create(imem, (OBJ) nullsrc);
  ISRC      isrc = ISRC_create(igen);

  /* Initialize the object */
  nullsrc->isrc = isrc;
  nullsrc->fwdExc = EXC_EOU;
  nullsrc->bwdExc = EXC_BOU;

  /* Set up the generic interface */
  IGEN_setInitializer(igen, NULLSRC_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, NULLSRC_getFrame);
  ISRC_setFrameAvailable(isrc, NULLSRC_frameAvailable);

  return(igen);
}


/*** GENERIC METHODS ***/

static Bool
NULLSRC_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a nullsrc according to a nullsrc attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  NULLSRCATTR nullsrcattr = (NULLSRCATTR) IFACE_object((IFACE) iattr);
  NULLSRC     nullsrc = (NULLSRC) IFACE_object((IFACE) igen);

  /* Copy attributes */
  nullsrc->fwdExc = nullsrcattr->fwdExc;
  nullsrc->bwdExc = nullsrcattr->bwdExc;

  return TRUE;
}


/*** SOURCE METHODS ***/

static Bool
NULLSRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reports an exception via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  NULLSRC   nullsrc;

  assert(isrc && snP);

  nullsrc = (NULLSRC) IFACE_object((IFACE) isrc);

  if (dir == FORWARD) {
    IFACE_setError((IFACE) isrc, nullsrc->fwdExc);
    *snP = EXC_EOU;
  } else if (dir == BACKWARD) {
    IFACE_setError((IFACE) isrc, nullsrc->bwdExc);
    *snP = EXC_BOU;
  } else {
    IFACE_setError((IFACE) isrc, EXC_INVALIDREQUEST);
    *snP = EXC_INVALIDREQUEST;
  }

  return(FALSE);
}


static Bool
NULLSRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of a cepstral frame via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(NULLSRC_getFrame(isrc, dir, snP, NULL));
}


/*** NULLSRCATTR Methods ***/

IGEN
NULLSRCATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a NULLSRC attributes object
 *---------------------------------------------------------------------------*/
{
  NULLSRCATTR   nullsrcattr = (NULLSRCATTR) imem->Calloc(imem, 1, sizeof(nullsrcattr_t), OOPS_MEMORY);
  IGEN          igen = IGEN_create(imem, (OBJ) nullsrcattr);
  INULLSRCATTR  inullsrcattr = INULLSRCATTR_create(igen);

  return(igen);
}


/*** INULLSRCATTR METHODS ***/

static INULLSRCATTR
INULLSRCATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an INULLSRCATTR interface
 *---------------------------------------------------------------------------*/
{
  INULLSRCATTR  inullsrcattr;

  if ((inullsrcattr = (INULLSRCATTR) IFACE_calloc((IFACE) igen, 1, sizeof(inullsrcattr_t)))) {
    IFACE_setClass((IFACE) inullsrcattr, &INULLSRCATTR_Class);
    IFACE_setObject((IFACE) inullsrcattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) inullsrcattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) inullsrcattr);
  }

  return(inullsrcattr);
}


Bool
INULLSRCATTR_setFwdException(INULLSRCATTR inullsrcattr, EXC fwdExc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the exception to report on a forward request
 *---------------------------------------------------------------------------*/
{
  NULLSRCATTR  nullsrcattr;

  assert(inullsrcattr);
  nullsrcattr = (NULLSRCATTR) IFACE_object((IFACE) inullsrcattr);

  nullsrcattr->fwdExc = fwdExc;

  return(TRUE);
}


Bool
INULLSRCATTR_setBwdException(INULLSRCATTR inullsrcattr, EXC bwdExc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the exception to report on a backward request
 *---------------------------------------------------------------------------*/
{
  NULLSRCATTR  nullsrcattr;

  assert(inullsrcattr);
  nullsrcattr = (NULLSRCATTR) IFACE_object((IFACE) inullsrcattr);

  nullsrcattr->bwdExc = bwdExc;

  return(TRUE);
}
