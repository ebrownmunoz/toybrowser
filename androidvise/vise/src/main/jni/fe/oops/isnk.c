/*  isnk.c - the sink interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * HISTORY:
 * 20-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: isnk.c[1.1] Tue Jun  9 17:43:43 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdlib.h>
#include "types.h"
#include <assert.h>

#include "class.h"
#include "iface.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"


/*** SINK ***/

/* The Class of Sink Interface Objects */
static iclass_t ISNK_Class = {ISNKID};

/* Default sink methods */
static Bool  ISNK_dfltPutFrame(ISNK isnk, dir_t dir, SN * snP, OBJT fr);
static Bool  ISNK_dfltSpaceAvailable(ISNK isnk, dir_t dir, SN * snP);


/*** SINK METHODS ***/

Bool
ISNK_setSource(ISNK isnk, ISRC isrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the source for a sink
 *---------------------------------------------------------------------------*/
{
  isnk->source = isrc;
  if (isnk->setSource) isnk->setSource(isnk, isrc);
  return(TRUE);
}


Bool
ISNK_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the attributes for a sink
 *---------------------------------------------------------------------------*/
{
  /* If there is a special function, report any exception it generates */
  if (isnk->setAttributes && !isnk->setAttributes(isnk, iattr))
    return(FALSE);

  /* Otherwise, set the sink's attributes and report success */
  isnk->iattr = iattr;
  return(TRUE);
}


Bool
ISNK_putFrame(ISNK isnk, dir_t dir, SN * snP, OBJT fr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Pass the frame and its SN to the sink in the specified direction.  A
 * negative sn instructs the sink to assign the next available SN in the
 * specified direction, and return the SN assigned.  Called with fr NULL,
 * this method is the same as ISNK_spaceAvailable().
 *---------------------------------------------------------------------------*/
{
  return(isnk->putFrame(isnk, dir, snP, fr));
}


Bool
ISNK_spaceAvailable(ISNK isnk, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Same as ISNK_putFrame, but does not put the frame
 *---------------------------------------------------------------------------*/
{
  return(isnk->spaceAvailable(isnk, dir, snP));
}


static Bool
ISNK_dfltPutFrame(ISNK isnk, dir_t dir, SN * snP, OBJT fr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  *snP = EXC_DFLTSNK;
  IFACE_setError((IFACE) isnk, EXC_DFLTSNK);
  return(FALSE);
}


static Bool
ISNK_dfltSpaceAvailable(ISNK isnk, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Same as ISNK_putFrame, but does not put the frame
 *---------------------------------------------------------------------------*/
{
  *snP = EXC_DFLTSNK;
  IFACE_setError((IFACE) isnk, EXC_DFLTSNK);
  return(FALSE);
}


/*** ISNK METHODS ***/

ISNK
ISNK_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a sink interface for an object
 *---------------------------------------------------------------------------*/
{
  ISNK  isnk;

  if ((isnk = (ISNK) IFACE_calloc((IFACE) igen, 1, sizeof(isnk_t)))) {
    IFACE_setClass((IFACE) isnk, &ISNK_Class);
    IFACE_setObject((IFACE) isnk, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) isnk, IFACE_allocator((IFACE) igen));
    ISNK_default(isnk);
    IFACE_addSibling((IFACE) igen, (IFACE) isnk);
  }

  return(isnk);
}


void
ISNK_default(ISNK isnk)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set a sink interface to its default (active) configuration
 *---------------------------------------------------------------------------*/
{
  isnk->putFrame = ISNK_dfltPutFrame;
  isnk->spaceAvailable = ISNK_dfltSpaceAvailable;
}


ISRC
ISNK_source(ISNK isnk)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the sink's source
 *---------------------------------------------------------------------------*/
{
  return(isnk->source);
}


IATTR
ISNK_attributes(ISNK isnk)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * OBSOLETE method providing the same service as ISNK_getAttributes()
 *---------------------------------------------------------------------------*/
{
  return(isnk->iattr);
}


void
ISNK_getAttributes(ISNK isnk, IATTR * iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  *iattr = isnk->iattr;
}


void
ISNK_setSetSource(ISNK isnk, Bool (* fcn)(ISNK, ISRC))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isnk->setSource = fcn;
}


void
ISNK_setSetAttributes(ISNK isnk, Bool (* fcn)(ISNK, IATTR))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isnk->setAttributes = fcn;
}


void
ISNK_setPutFrame(ISNK isnk, Bool (*fcn)(ISNK, dir_t, SN *, OBJT))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isnk->putFrame = fcn;
}


void
ISNK_setSpaceAvailable(ISNK isnk, Bool (*fcn)(ISNK, dir_t, SN *))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isnk->spaceAvailable = fcn;
}
