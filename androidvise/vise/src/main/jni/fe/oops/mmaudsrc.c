/* mmaudsrc.c - MMAUDIN interface to an OOPS chain head
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * HISTORY
 * 04-Aug-98  Craig Vanderborgh (craigv@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "mmaudsrc.h"
#include "types.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include "vbx.h"

#ifdef DEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

#define DFLT_CLASSSIZE  2
#define DFLT_FRAMELEN   128
#define DFLT_SAMPBYTES  2

/* MMAUDSRCATTR (Multimedia Audio Source Attributes) Interface Objects */
static iclass_t IMMAUDSRCATTR_Class = {IMMAUDSRCATTRID};

/* The Class of IMMAUDSRC Interface Objects */
static iclass_t IMMAUDSRC_Class = {IMMAUDSRCID};

/* The MMAUDSRC Interface */
typedef struct immaudsrc_s {
  iface_t  iface;
} immaudsrc_t;

/* MMAUDSRC Attributes */
typedef struct mmaudsrcattr_s {
  Int           classSize;    /* Size of the CLASS of audio frame objects */
  Int           frameLen;     /* Number of samples in audio frame (Shorts) */
  Int           sampBytes;    /* Number of bytes in a sample (for future) */
  IAudioIn     *iAudioPtr;    /* Verbex MMAUDIN audio source pointer */
} mmaudsrcattr_t, *MMAUDSRCATTR;

/* A MMAUDSRC descriptor */
typedef struct mmaudsrc_s {
  IMemory     * imem;         /* The memory allocator */
  ISRC          isrc;         /* MMAUDSRC's (audio) source interface (output) */
  IGEN          igenattr;     /* Interface to the audio attributes block */
  Int           classSize;    /* The size of the CLASS of audio frame OBJTs */
  CLASS         audioClass;   /* The CLASS of audio frame OBJTs */
  Int           frameLen;     /* Number of samples in an audio frame */
  Int           sampBytes;    /* Number of bytes in a sample (for future) */
  IAudioIn     *iAudioPtr;    /* Verbex MMAUDIN audio source pointer */
} mmaudsrc_t;

/* Private functions */

static IMMAUDSRC IMMAUDSRC_create(IGEN igen);

static Bool  MMAUDSRC_annihilate(IGEN igen);
static Bool  MMAUDSRC_initialize(IGEN igen, IATTR iattr);
static Bool  MMAUDSRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  MMAUDSRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static IMMAUDSRCATTR  IMMAUDSRCATTR_create(IGEN igen);

/*** MMAUDSRC Methods ***/

IGEN
MMAUDSRC_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an MMAUDSRC object
 *---------------------------------------------------------------------------*/
{
  MMAUDSRC     mmaudsrc = (MMAUDSRC) imem->Calloc(imem, 1, sizeof(mmaudsrc_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) mmaudsrc);
  ISRC       isrc = ISRC_create(igen);
  IGEN       igenattr = AUDIOATTR_create(imem);

  /* Initialize the object */
  mmaudsrc->imem = imem;
  mmaudsrc->isrc = isrc;
  mmaudsrc->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, MMAUDSRC_annihilate);
  IGEN_setInitializer(igen, MMAUDSRC_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, MMAUDSRC_getFrame);
  ISRC_setFrameAvailable(isrc, MMAUDSRC_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, IAUDIOATTRID));

  return(igen);
}

/*** MMAUDSRC METHODS ***/

static Bool
MMAUDSRC_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a mmaudsrc object
 * This supplements the generic IGEN_annihilate() by annihilating the CLASS
 * of cepstral frame OBJTs and the cepstral attributes block
 *---------------------------------------------------------------------------*/
{
  MMAUDSRC mmaudsrc = (MMAUDSRC) IFACE_object((IFACE) igen);

  /* Annihilate the CLASS of cepstral frame OBJTs */
  if (mmaudsrc->audioClass && !CLASS_destroy(mmaudsrc->audioClass, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Annihilate the cepstral attributes object */
  if (!IGEN_annihilate(mmaudsrc->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) mmaudsrc->igenattr));
    return(FALSE);
  }

  return(TRUE);
}

static Bool
MMAUDSRC_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize an MMAUDSRC according to an MMAUDSRCATTR attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 * NOTE: The initial implementation supports 16-bit linear encoding ONLY!
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) iattr);
  MMAUDSRC      mmaudsrc = (MMAUDSRC) IFACE_object((IFACE) igen);
  IAUDIOATTR    iaudioattr;
  ISNK          sink;
  Int           oldFrameLen;

  /* Get the audio unknown and the interfaces we need from it */
  mmaudsrc->iAudioPtr = mmaudsrcattr->iAudioPtr;
  assert(mmaudsrc->iAudioPtr != NULL);

  /* Get the frame length and sample size for the audio frames */
  mmaudsrc->frameLen = mmaudsrcattr->frameLen;
  mmaudsrc->sampBytes = mmaudsrcattr->sampBytes;

  /* Get the frameLen in the audio attributes block (in ISRC) */
  if (!ISRC_getAttributes(mmaudsrc->isrc, (IATTR *) &iaudioattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) mmaudsrc->isrc));
    return(FALSE);
  }
  IAUDIOATTR_getFrameLen(iaudioattr, &oldFrameLen);

  /* If the frameLen or the classSize has changed, re-create the CLASS */
  if (mmaudsrc->frameLen != oldFrameLen || mmaudsrcattr->classSize != mmaudsrc->classSize) {
    if (mmaudsrc->audioClass)
      CLASS_destroy(mmaudsrc->audioClass, NULL, NULL);
    mmaudsrc->audioClass =
      CLASS_create(mmaudsrc->imem, mmaudsrcattr->classSize, mmaudsrc->frameLen * mmaudsrc->sampBytes, NULL, NULL);
    mmaudsrc->classSize = mmaudsrcattr->classSize;
  }

  /* If frameLen has changed, promulgate the change to the sink */
  if (oldFrameLen != mmaudsrc->frameLen) {
    IAUDIOATTR_setFrameLen(iaudioattr, mmaudsrc->frameLen);

    /* Get the sink associated with the source (output) interface */
    sink = ISRC_sink(mmaudsrc->isrc);
    /* If there is none, register an exception */
    if (!sink) {
      IFACE_setError((IFACE) igen, EXC_NOSNK);
      return(FALSE);
    }
    /* Pass the audio attributes on to the sink, if possible */
    if (ISNK_setAttributes(sink, (IATTR) iaudioattr)) {
      /* If successfully passed, return TRUE */
      return(TRUE);
    } else {
      /* Otherwise, register the exception and return FALSE */
      IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
      return(FALSE);
    }
  }
  return(TRUE);
}

OBJT
MMAUDSRC_newFrame(ISRC isrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Allocate a sample frame object from MMAUDSRC's internal CLASS of such
 * objects.  
 * NOTE: IT IS EXTREMELY IMPORTANT THAT THE CALLER FREE THE ALLOCATION
 *       USING OBJT_free().
 *---------------------------------------------------------------------------*/
{
  MMAUDSRC  mmaudsrc;
  OBJT      obj;

  assert(isrc != NULL);
  mmaudsrc = ((MMAUDSRC) IFACE_object((IFACE) isrc));
  assert(mmaudsrc != NULL);
  assert(mmaudsrc->audioClass != NULL);

  obj = OBJT_new(mmaudsrc->audioClass);
  return(obj);
}
  

/*** SOURCE METHODS ***/

static Bool
MMAUDSRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports an audio frame from an MMAudio source via the source (output)
 * interface
 * NOTE:     The intention is that no frame queueing is to occur here
 * BEHAVIOR: Depends on the implementation of the audio source itself, but
 *           it is generally expected that this routine will BLOCK if 
 *           there is insufficient data available to produce a frame.
 *---------------------------------------------------------------------------*/
{
  MMAUDSRC  mmaudsrc;
  SN        nextSN;
  V_Long    bytesAvail;
  Short     eof;
  UShort    i, *usptr;

  assert(isrc != NULL && snP != NULL);
  mmaudsrc = ((MMAUDSRC) IFACE_object((IFACE) isrc));
  assert(mmaudsrc != NULL);

  /* Frame retrieval is only permitted in the FORWARD direction */
  if (dir != FORWARD) {
    IFACE_setError((IFACE) isrc, EXC_NOBACKWARD);
    return(FALSE);
  }

  /* Only the next SN is available from the audio source */
  if (!mmaudsrc->iAudioPtr->TotalGet(mmaudsrc->iAudioPtr, &nextSN))
    SEVERE_BRA_ "  MMAUDSRC::TotalGet failed" _KET;
  if (TRUE /**snP < 0*/) {      /* Return next frame, regardless of request */
    *snP = nextSN;
  } else if (nextSN != *snP) {
    IFACE_setError((IFACE) isrc, EXC_CANTGETSN);
    return(FALSE);
  }

  /* First see if a frame could be gotten */
  if (!mmaudsrc->iAudioPtr->DataAvailable(mmaudsrc->iAudioPtr, &bytesAvail, &eof))
    SEVERE_BRA_ "  MMAUDSRC::DataAvailable failed" _KET;

  if (bytesAvail < (ULong) mmaudsrc->frameLen * mmaudsrc->sampBytes) {
	  VBX_DEBUG(VBX_print("  MMAUDSRC: bytesAvail(%d) < %d\n", bytesAvail, (ULong) mmaudsrc->frameLen * mmaudsrc->sampBytes));
    IFACE_setError((IFACE) isrc, eof ? EXC_EOU : EXC_TOOFEWSAMPLES);
    VBX_DEBUG(VBX_print("  MMAUDSRC: setting %s\n", eof ? "EXC_EOU" : "EXC_TOOFEWSAMPLES"));
    return(FALSE);
  }

  /* Retrieve the frame if requested */
  if (outP) {
   *outP = OBJT_new(mmaudsrc->audioClass);

    /* If an OBJT is available, fill it with the sample data */
    if (*outP) {
      VBX_DEBUG(VBX_print("MMAUDSRC_getFrame: return snP %lld\n", *snP));
      if (!mmaudsrc->iAudioPtr->Read(mmaudsrc->iAudioPtr, (V_Int *) &OBJT_contents(*outP), mmaudsrc->frameLen)) {
        SEVERE_BRA_ "  MMAUDSRC::Read failed" _KET;
          
        /* CHANGE THIS TO CACHE AND CONCATENATE, BUT STILL RETURN THIS EXC */
        IFACE_setError((IFACE) isrc, EXC_TOOFEWSAMPLES);
        return(FALSE);
      }
    } else {                            /* Otherwise, report an exception */
      IFACE_setError((IFACE) isrc, EXC_AUDIOCLASSEMPTY);
      return(FALSE);
    }
  } else {
    VBX_DEBUG(VBX_print("MMAUDSRC_frameAvailable: return snP %lld\n", *snP));
  }

  IFACE_setError((IFACE) isrc, EXC_NONE);
  return(TRUE);
}

static Bool
MMAUDSRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an audio frame via the source (output) interface
 * Requested sequence number is ignored.
 *---------------------------------------------------------------------------*/
{
  return(MMAUDSRC_getFrame(isrc, dir, snP, NULL));
}

/*** MMAUDSRCATTR Methods ***/

IGEN
MMAUDSRCATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a MMAUDSRC attributes object
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR   mmaudsrcattr = (MMAUDSRCATTR) imem->Calloc(imem, 1, sizeof(mmaudsrcattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(imem, (OBJ) mmaudsrcattr);
  IMMAUDSRCATTR  immaudsrcattr = IMMAUDSRCATTR_create(igen);

  /* Set the default attributes */
  mmaudsrcattr->classSize = DFLT_CLASSSIZE;
  mmaudsrcattr->frameLen = DFLT_FRAMELEN;
  mmaudsrcattr->sampBytes = DFLT_SAMPBYTES;

  return(igen);
}


/*** IMMAUDSRC METHODS ***/

static IMMAUDSRC
IMMAUDSRC_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IOBJSOURCE interface on the emitter
 *---------------------------------------------------------------------------*/
{
  IMMAUDSRC  immaudsrc;

  if ((immaudsrc = (IMMAUDSRC) IFACE_calloc((IFACE) igen, 1, sizeof(immaudsrc_t)))) {
    IFACE_setClass((IFACE) immaudsrc, &IMMAUDSRC_Class);
    IFACE_setObject((IFACE) immaudsrc, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) immaudsrc, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) immaudsrc);
  }

  return(immaudsrc);
}

/*** IMMAUDSRCATTR METHODS ***/

static IMMAUDSRCATTR
IMMAUDSRCATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IMMAUDSRCATTR interface
 *---------------------------------------------------------------------------*/
{
  IMMAUDSRCATTR  immaudsrcattr;

  if ((immaudsrcattr = (IMMAUDSRCATTR) IFACE_calloc((IFACE) igen, 1, sizeof(immaudsrcattr_t)))) {
    IFACE_setClass((IFACE) immaudsrcattr, &IMMAUDSRCATTR_Class);
    IFACE_setObject((IFACE) immaudsrcattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) immaudsrcattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) immaudsrcattr);
  }

  return(immaudsrcattr);
}

Bool
IMMAUDSRCATTR_setIAudioIn(IMMAUDSRCATTR immaudsrcattr, Ptr iAudioPtr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr;

  assert(immaudsrcattr != NULL && iAudioPtr != NULL);
  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) immaudsrcattr);

  mmaudsrcattr->iAudioPtr = (IAudioIn *) iAudioPtr;
  return(TRUE);
}

Bool
IMMAUDSRCATTR_setClassSize(IMMAUDSRCATTR immaudsrcattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr;

  assert(immaudsrcattr != NULL);
  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) immaudsrcattr);

  mmaudsrcattr->classSize = classSize;

  return(TRUE);
}

Bool
IMMAUDSRCATTR_setFrameLen(IMMAUDSRCATTR immaudsrcattr, Int frameLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr;

  assert(immaudsrcattr != NULL);
  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) immaudsrcattr);

  mmaudsrcattr->frameLen = frameLen;
  return(TRUE);
}

Bool
IMMAUDSRCATTR_getFrameLen(IMMAUDSRCATTR immaudsrcattr, Int *frameLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr;

  assert(immaudsrcattr != NULL);
  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) immaudsrcattr);

 *frameLen = mmaudsrcattr->frameLen;
  return(TRUE);
}

Bool
IMMAUDSRCATTR_setSampleSize(IMMAUDSRCATTR immaudsrcattr, Int sampleSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of a sample in bytes
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr;

  assert(immaudsrcattr != NULL);
  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) immaudsrcattr);

  mmaudsrcattr->sampBytes = sampleSize;
  return(TRUE);
}

Bool
IMMAUDSRCATTR_getSampleSize(IMMAUDSRCATTR immaudsrcattr, Int *sampleSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of a sample in bytes
 *---------------------------------------------------------------------------*/
{
  MMAUDSRCATTR  mmaudsrcattr;

  assert(immaudsrcattr != NULL);
  mmaudsrcattr = (MMAUDSRCATTR) IFACE_object((IFACE) immaudsrcattr);

 *sampleSize = mmaudsrcattr->sampBytes;
  return(TRUE);
}
