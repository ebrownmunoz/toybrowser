/* audioattr.c - audio frame attributes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * DESCRIPTION:
 * Attributes for frames of audio sample data within the OOPS realm.  This
 * initial implementation supports only 16-bit linear sample encoding and
 * therefore makes several assumptions about sample length, etc.
 * HISTORY
 * 03-Nov-96  Craig Vanderborgh (craigv@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "audioattr.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "vbx.h"
#include "c.h"
#include <assert.h>

#define DFLT_AUDIOFRAMELEN 102

/* The Class of AUDIOATTR (AUDIO Frame Attributes) Interface Objects */
static iclass_t IAUDIOATTR_Class = {IAUDIOATTRID};

/* AUDIO Attributes */
typedef struct audioattr_s {
  Int         frameLen;     /* Number of samples in an audio frame (Shorts) */
  Int         sampBytes;    /* Number of bytes in a sample (for the future) */
} audioattr_t, *AUDIOATTR;

/* Private functions */

static IAUDIOATTR  IAUDIOATTR_create(IGEN igen);
static Bool        IAUDIOATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields);

/*** AUDIOATTR Methods ***/

IGEN
AUDIOATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a audio frame attributes object
 *---------------------------------------------------------------------------*/
{
  AUDIOATTR  audioattr = (AUDIOATTR) imem->Calloc(imem, 1, sizeof(audioattr_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) audioattr);
  IAUDIOATTR iaudioattr = IAUDIOATTR_create(igen);

  audioattr->frameLen = DFLT_AUDIOFRAMELEN;
  /* Generic IGEN_annihilate() suffices */

  /* Set up the default behavior of IAUDIOATTR */
  IOBJATTR_setPrinter((IOBJATTR) iaudioattr, IAUDIOATTR_dfltPrint);
  IOBJATTR_setLabel((IOBJATTR) iaudioattr, "AUDIO ");

  return(igen);
}


Bool
IAUDIOATTR_getFrameLen(IAUDIOATTR iaudioattr, Int * frameLenP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the length of an audio frame (in samples)
 *---------------------------------------------------------------------------*/
{
  AUDIOATTR  audioattr;

  assert(iaudioattr);
  audioattr = (AUDIOATTR) IFACE_object((IFACE) iaudioattr);

  *frameLenP = audioattr->frameLen;
  return(TRUE);
}


Bool
IAUDIOATTR_setFrameLen(IAUDIOATTR iaudioattr, Int frameLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame (in samples)
 *---------------------------------------------------------------------------*/
{
  AUDIOATTR  audioattr;

  assert(iaudioattr);
  audioattr = (AUDIOATTR) IFACE_object((IFACE) iaudioattr);

  audioattr->frameLen = frameLen;
  return(TRUE);
}


/*** IAUDIOATTR METHODS ***/

static IAUDIOATTR
IAUDIOATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IAUDIOATTR interface
 *---------------------------------------------------------------------------*/
{
  IAUDIOATTR  iaudioattr = (IAUDIOATTR) IOBJATTR_create(igen);
  AUDIOATTR   audioattr;

  assert(iaudioattr);

  /* Turn it from an IOBJATTR into an IAUDIOATTR */
  IFACE_setClass((IFACE) iaudioattr, &IAUDIOATTR_Class);
  audioattr = (AUDIOATTR) IFACE_object((IFACE) iaudioattr);
  audioattr->sampBytes = 2;

  return(iaudioattr);
}


static Bool
IAUDIOATTR_dfltPrint(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Print out an audio frame object
 *---------------------------------------------------------------------------*/
{
  Short  * audio  = (Short *) &OBJT_contents(objt);
  Int      audioFrameLen, numFields;
  Char     prefix[8];  /* 9999 in sprintf below assures that prefix will fit */

  IAUDIOATTR_getFrameLen((IAUDIOATTR) iobjattr, &audioFrameLen);
  /* Build a prefix string to align the first field of any continuation line */
  sprintf(prefix, "\n%%%ds", MIN(prefixLen, 9999));
  for (numFields = 0; numFields < audioFrameLen; numFields++) {
    /* If the next field would make the line too long, go to the next line */
    if (!(numFields % maxFields) && numFields) VBX_print(prefix, "");
    VBX_print(" %5.5d", audio[numFields]);
  }
  VBX_print("\n");

  return(TRUE);
}
