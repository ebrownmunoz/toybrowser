/* objpad.c - a frame object stream padding module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 17-Nov-96  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "objpad.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include <assert.h>

#undef VBXDEBUG
#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

#define DFLT_PADLENGTH   0
#define MAXUTTERANCELEN  (SN_MAX / 4)

/* The Class of PADATTR (OBJPAD Attributes) Interface Objects */
static iclass_t IPADATTR_Class = {IPADATTRID};


/* An OBJPAD descriptor */
typedef struct objpad_s {
  ISRC    isrc;           /* OBJPAD's source interface (output) */
  ISNK    isnk;           /* OBJPAD's sink interface (input) */
  OBJT    padObject;      /* The OBJT to use as padding */
  SN      bouPadLen;      /* The number of frames of BOU padding */
  SN      eouPadLen;      /* The number of frames of EOU padding */
  SN      snBOU;          /* The SN of the BOU */
  SN      snEOU;          /* The SN of the EOU */
  SN      snBOP;          /* The SN of the beginning of the BOU pad */
  SN      snEOP;          /* The SN of the end of the EOU pad */
  SN      lastSN;         /* The last real SN found */
  SN      lastSNFwd;      /* The last real SN found FORWARD */
  SN      lastSNBwd;      /* The last real SN found BACKWARD */
  Bool    externPad;      /* Padding OBJT supplied externally */
} objpad_t, * OBJPAD;

/* SN Tolerance Macro */
#define SNDIFFPOSITIVE(D)  ((D) > 0 ? (D) <= SN_MAX/4 : (D) < (SN_MIN/4) * 3)

/* Private functions */

static Bool  OBJPAD_annihilate(IGEN igen);
static Bool  OBJPAD_initialize(IGEN igen, IATTR iattr);
static Bool  OBJPAD_setAttributes(ISNK isnk, IATTR iattr);
static Bool  OBJPAD_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  OBJPAD_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static Bool      PADATTR_annihilate(IGEN igen);
static IPADATTR  IPADATTR_create(IGEN igen);


/*** OBJPAD Methods ***/

IGEN
OBJPAD_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a frame object padder
 *---------------------------------------------------------------------------*/
{
  OBJPAD     objpad = (OBJPAD) imem->Calloc(imem, 1, sizeof(objpad_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) objpad);
  ISNK       isnk = ISNK_create(igen);
  ISRC       isrc = ISRC_create(igen);
  IATTR      iattr;

  /* Initialize the object */
  objpad->isnk = isnk;
  objpad->isrc = isrc;
  objpad->padObject = (OBJT) NULL;
  objpad->bouPadLen = DFLT_PADLENGTH;
  objpad->eouPadLen = DFLT_PADLENGTH;
  objpad->lastSN = SN_MAX / 2;     /* Initial lastSN + BACKWARD must be >= 0 */
  objpad->snBOU = objpad->snBOP = 0;
  objpad->snEOU = objpad->snEOP = SN_MAX;
  objpad->lastSNBwd = SN_MAX;
  objpad->lastSNFwd = 0;
  objpad->externPad = FALSE;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, OBJPAD_annihilate);
  IGEN_setInitializer(igen, OBJPAD_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setAttributes(isrc, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the sink interface */
  ISNK_setSetAttributes(isnk, OBJPAD_setAttributes);

  /* Set up the source interface */
  ISRC_setGetFrame(isrc, OBJPAD_getFrame);
  ISRC_setFrameAvailable(isrc, OBJPAD_frameAvailable);

  return(igen);
}


/*** GENERIC METHODS ***/

static Bool
OBJPAD_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate an objpad object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  OBJPAD  objpad = (OBJPAD) IFACE_object((IFACE) igen);

  /* Free the padding OBJT if it is not externally supplied */
  if (!objpad->externPad && objpad->padObject != NULL)
    OBJT_free(objpad->padObject);

  return(TRUE);
}


static Bool
OBJPAD_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a objpad according to an objpad attributes specification
 * Like all IGEN_initialize() functions, this one may be called repeatedly
 * with different IATTRs.
 *---------------------------------------------------------------------------*/
{
  OBJPAD  objpad = (OBJPAD) IFACE_object((IFACE) igen);

  /* Free an existing padding OBJT if it is not externally supplied */
  if (!objpad->externPad && objpad->padObject != NULL)
    OBJT_free(objpad->padObject);

  IPADATTR_getPadObject((IPADATTR) iattr, &objpad->padObject);
  IPADATTR_getBOUPadLen((IPADATTR) iattr, &objpad->bouPadLen);
  IPADATTR_getEOUPadLen((IPADATTR) iattr, &objpad->eouPadLen);
  objpad->lastSN = SN_MAX / 2;
  objpad->snBOU = objpad->snBOP = 0;
  objpad->snEOU = objpad->snEOP = SN_MAX;
  objpad->lastSNBwd = SN_MAX;
  objpad->lastSNFwd = 0;
  objpad->externPad = objpad->padObject != NULL;

  return(TRUE);
}


/*** CEPSTRAL SOURCE AND SINK METHODS ***/

static Bool
OBJPAD_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Import object attributes via the sink (input) interface and pass them
 * to the sink associated with the source (output) interface.  This function
 * supplements the default ISNK_setAttributes() function, which calls this
 * first and then sets the attributes of isnk if the call is successful.
 *---------------------------------------------------------------------------*/
{
  OBJPAD  objpad;
  ISNK    sink;

  assert(isnk);

  objpad = (OBJPAD) IFACE_object((IFACE) isnk);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(objpad->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) isnk, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, iattr)) {
    /* If successful, set the attributes locally and return TRUE */
    ISRC_setAttributes(objpad->isrc, iattr);
    return(TRUE);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
    return(FALSE);
  }
}


static Bool
OBJPAD_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports a frame via the source (output) interface.  Assumes that the
 * ISRC will send an EXC_SNWRAPAROUND when the SN wraps around.
 *---------------------------------------------------------------------------*/
{
  OBJPAD     objpad;
  ISRC       src;
  SN         snSought, snDiff;
  EXC        exc;

  assert(isrc && snP);

  /* VBX_DEBUG(VBX_print("  OBJPAD_getFrame: seeking frame %#lx %s\n", *snP, DIR_SYM(dir))); */

  objpad = ((OBJPAD) IFACE_object((IFACE) isrc));

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(objpad->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Assume we won't encounter an exception */
  IFACE_setError((IFACE) isrc, EXC_NONE);

  if (*snP < 0) {
    snSought = objpad->lastSN + dir;
    /* Deal with SN wraparound */
    if (snSought < 0) {
      snSought = dir == FORWARD ? 0 : SN_MAX;
      VBX_DEBUG(VBX_print("  OBJPAD_getFrame: sn wraparound %s (snSought = %#lx)\n", DIR_SYM(dir), snSought));
    }
  } else {
    snSought = *snP;
  }

  /* VBX_DEBUG(VBX_print("  OBJPAD_getFrame: snSought = %#lx %s\n", snSought)); */

  /* If the request seems to be in the range of the real data ... */
  if (objpad->snBOU <= snSought && snSought <= objpad->snEOU) {

    /* Try to obtain the requested frame from the source */
    if (ISRC_getFrame(src, dir, snP, outP)) {
      objpad->lastSN = *snP;
      /* VBX_DEBUG(VBX_print("  OBJPAD_getFrame: got frame %#lx from source\n", *snP)); */
      if (dir == FORWARD) {
        if (*snP > objpad->lastSNFwd) objpad->lastSNFwd = *snP;
      } else {
        if (*snP < objpad->lastSNBwd) objpad->lastSNBwd = *snP;
      }
      /* If the pad is not supplied externally, save the current object as pad */
      if (!objpad->externPad) {
        if (objpad->padObject) OBJT_free(objpad->padObject);
        objpad->padObject = OBJT_copy(*outP);
      }
      return(TRUE);
    } else {
      /* If not padding return any exception; otherwise, return any except EOU FORWARD or BOU BACKWARD */
      exc = IFACE_error((IFACE) src);
      VBX_DEBUG(VBX_print("  OBJPAD_getFrame: got EXC %d from source\n", exc));
      if (!(dir == FORWARD ? objpad->eouPadLen != (SN) 0 && exc == EXC_EOU : objpad->bouPadLen != (SN) 0 && exc == EXC_BOU)) {
        IFACE_setError((IFACE) isrc, exc);
        return(FALSE);
      }
    }

    /* We've reached the EOU FORWARD or BOU BACKWARD */
    if (dir == FORWARD) {
      objpad->snEOU = objpad->lastSN = objpad->lastSNFwd;
      objpad->snEOP = SN_MAX - objpad->snEOU > objpad->eouPadLen ? objpad->snEOU + objpad->eouPadLen : SN_MAX;
      if (objpad->snEOU - objpad->snBOU > MAXUTTERANCELEN) {
        objpad->snBOU = objpad->snEOU - MAXUTTERANCELEN;
        objpad->snBOP = objpad->snBOU > objpad->bouPadLen ? objpad->snBOU - objpad->bouPadLen : 0;
      }
      VBX_DEBUG(VBX_print("  OBJPAD_getFrame: reached EOU (snBOP = %#lx, snBOU = %#lx, snEOU = %#lx, snEOP = %#lx)\n",
                          objpad->snBOP, objpad->snBOU, objpad->snEOU, objpad->snEOP));
    } else {
      objpad->snBOU = objpad->lastSN = objpad->lastSNBwd;
      objpad->snBOP = objpad->snBOU > objpad->bouPadLen ? objpad->snBOU - objpad->bouPadLen : 0;
      if (objpad->snEOU - objpad->snBOU  > MAXUTTERANCELEN) {
        objpad->snEOU = objpad->snBOU + MAXUTTERANCELEN;
        objpad->snEOP = SN_MAX - objpad->snEOU > objpad->eouPadLen ? objpad->snEOU + objpad->eouPadLen : SN_MAX;
      }
      VBX_DEBUG(VBX_print("  OBJPAD_getFrame: reached BOU (snBOP = %#lx, snBOU = %#lx, snEOU = %#lx, snEOP = %#lx)\n",
                          objpad->snBOP, objpad->snBOU, objpad->snEOU, objpad->snEOP));
    }
  }

  /* The request is for a frame outside the range available from the source */

  /* If the request is for a frame beyond the EOU padding ... */
  if ((snDiff = snSought - objpad->snEOP) && SNDIFFPOSITIVE(snDiff)) {

    VBX_DEBUG(VBX_print("  OBJPAD_getFrame: requesting frame beyond EOU padding (snSought = %ld, snEOP = %ld, snDiff = %ld)\n",
                        snSought, objpad->snEOP, snDiff, 3 * (SN_MIN/4), SN_MAX/4));

    /* If getting FORWARD, send an EOU */
    if (dir == FORWARD) {
      IFACE_setError((IFACE) isrc, EXC_EOU);
      return(FALSE);
    /* Otherwise, get the last padding frame available */
    } else {
      snSought = objpad->snEOP;
    }

  /* Otherwise, if it is for one before the BOU padding ... */
  } else if ((snDiff = objpad->snBOP - snSought) && SNDIFFPOSITIVE(snDiff)) {

    VBX_DEBUG(VBX_print("  OBJPAD_getFrame: requesting frame before BOU padding (snSought = %ld, snBOP = %ld, snDiff = %ld)\n",
                        snSought, objpad->snBOP, snDiff, 3 * (SN_MIN/4), SN_MAX/4));

    /* If getting BACKWARD, send a BOU */
    if (dir == BACKWARD) {
      IFACE_setError((IFACE) isrc, EXC_BOU);
      return(FALSE);
    /* Otherwise, get the first padding frame available */
    } else {
      snSought = objpad->snBOP;
    }

  }

  /* The request is for a padding frame */
  *snP = objpad->lastSN = snSought;
  VBX_DEBUG(VBX_print("  OBJPAD_getFrame: requesting pad frame (snSought = %ld)\n", snSought));
  if (outP)
    *outP = OBJT_copy(objpad->padObject);
  return(TRUE);
}


static Bool
OBJPAD_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an OBJT via the source (output) interface
 *---------------------------------------------------------------------------*/
{
  return(OBJPAD_getFrame(isrc, dir, snP, NULL));
}


/*** PADATTR Methods ***/

IGEN
PADATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a OBJPAD attributes object
 *---------------------------------------------------------------------------*/
{
  IGEN      igen = IGEN_create(imem, NULL);
  IPADATTR  ipadattr = IPADATTR_create(igen);

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, PADATTR_annihilate);

  return(igen);
}


static Bool
PADATTR_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate an padattr object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  IPADATTR  ipadattr = (IPADATTR) IFACE_object((IFACE) igen);

  /* Free the padding object */
  /* OBJT_free(ipadattr->padObject); */

  return(TRUE);
}


/*** IPADATTR METHODS ***/

static IPADATTR
IPADATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IPADATTR interface
 *---------------------------------------------------------------------------*/
{
  IPADATTR  ipadattr;

  if ((ipadattr = (IPADATTR) IFACE_calloc((IFACE) igen, 1, sizeof(ipadattr_t)))) {

    IFACE_setClass((IFACE) ipadattr, &IPADATTR_Class);
    IFACE_setObject((IFACE) ipadattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) ipadattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) ipadattr);

    /* Set the defaults */
    ipadattr->bouPadLen = DFLT_PADLENGTH;
    ipadattr->eouPadLen = DFLT_PADLENGTH;

  }

  return(ipadattr);
}
