/*  iface.c - an interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * HISTORY:
 * 19-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: iface.c[1.1] Tue Jun  9 17:42:12 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdlib.h>
#include "types.h"
#include "ifaces.h"
#include <assert.h>

#include "iface.h"


/*** IFACE METHODS ***/

/*---------------------------------------------------------------------------*
 * IFACE needs no creation function, since the creation function for every
 * specific type of interface creates an iface_t as part of the interface.
 *---------------------------------------------------------------------------*/


ICLASS
IFACE_class(IFACE iface)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return class of an interface
 *---------------------------------------------------------------------------*/
{
  return(iface->clas);
}


void
IFACE_setClass(IFACE iface, ICLASS clas)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the class of an interface
 *---------------------------------------------------------------------------*/
{
  iface->clas = clas;
}


OBJ
IFACE_object(IFACE iface)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the object that an interface gives access to
 *---------------------------------------------------------------------------*/
{
  return(iface->object);
}


void
IFACE_setObject(IFACE iface, OBJ object)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the object that an interface gives access to
 *---------------------------------------------------------------------------*/
{
  iface->object = object;
}


IMemory *
IFACE_allocator(IFACE iface)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the allocator for the interface
 *---------------------------------------------------------------------------*/
{
  return(iface->imem);
}


void
IFACE_setAllocator(IFACE iface, IMemory * allocator)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the allocator for the interface
 *---------------------------------------------------------------------------*/
{
  iface->imem = allocator;
}


EXC
IFACE_error(IFACE iface)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the current error status of the interface
 *---------------------------------------------------------------------------*/
{
  return(iface->error);
}


void
IFACE_setError(IFACE iface, EXC error)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the error status of an interface
 *---------------------------------------------------------------------------*/
{
  iface->error = error;
}


void *
IFACE_calloc(IFACE iface, size_t num, size_t size)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Use the interface's allocator to allocate memory
 *---------------------------------------------------------------------------*/
{
  return(iface->imem->Calloc(iface->imem, num, size, OOPS_MEMORY));
}

void
IFACE_free(IFACE iface, void * allocation)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Use the interface's allocator to free the allocation
 *---------------------------------------------------------------------------*/
{
  iface->imem->Free(iface->imem, allocation);
}


IFACE
IFACE_interface(IFACE iface, IFACEID iid)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Find an interface with the given interface id in iface's sibling list
 *---------------------------------------------------------------------------*/
{
  while (iface && iface->clas->id != iid)
    iface = iface->sibling;

  if (iface)
    return(iface);
  else
    return(NULL);
}


void
IFACE_addSibling(IFACE iface, IFACE sibling)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Add a sibling to an interface
 *---------------------------------------------------------------------------*/
{
  sibling->sibling = iface->sibling;
  iface->sibling = sibling;
}


Bool
IFACE_removeSibling(IFACE iface, IFACE sibling)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Remove a sibling from an interface if it is there
 *---------------------------------------------------------------------------*/
{
  /* Find the sibling */
  while (iface && iface->sibling != sibling)
    iface = iface->sibling;

  /* If found, remove it, free it and return TRUE; otherwise, return FALSE */
  if (iface) {
    iface->sibling = sibling->sibling;
    sibling->imem->Free(sibling->imem, sibling);
    return(TRUE);
  } else {
    return(FALSE);
  }
}


void
IFACE_annihilate(IFACE iface)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate an interface and all of its siblings
 *---------------------------------------------------------------------------*/
{
  IFACE  sibling;

  while (iface) {
    sibling = iface->sibling;
    iface->imem->Free(iface->imem, iface);
    iface = sibling;
  }
}

