/* charsrc.c - Character Source to an OOPS chain head
 *---------------------------------------------------------------------------*
 *
 * DESCRIPTION
 * Reads line from stdin then sends objects containing 1 letter each on request
 *
 *---------------------------------------------------------------------------*/

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "charsrc.h"
#include "types.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include "vbx.h"

#define VBXDEBUG 1
#ifdef VBXDEBUG
#define VBX_DEBUG(P) P
#else
#define VBX_DEBUG(P)
#endif

#define DFLT_CLASSSIZE  8
#define DFLT_BUFSIZE    1024
#define BOU_SILENCE_LEN 15
#define EOU_SILENCE_LEN 50
#define MAX_INPUT_LEN   (DFLT_BUFSIZE - (BOU_SILENCE_LEN + EOU_SILENCE_LEN))

/* CHARSRCATTR (Character Source Attributes) Interface Objects */
static iclass_t ICHARSRCATTR_Class = {ICHARSRCATTRID};

/* The Class of ICHARSRC Interface Objects */
static iclass_t ICHARSRC_Class = {ICHARSRCID};

/* The CHARSRC Interface */
typedef struct icharsrc_s {
  iface_t  iface;
} icharsrc_t;

/* CHARSRC Attributes */
typedef struct charsrcattr_s {
  Int           classSize;    /* Size of the CLASS of audio frame objects */
} charsrcattr_t, *CHARSRCATTR;

/* A CHARSRC descriptor */
typedef struct charsrc_s {
  IMemory     * imem;         /* The memory allocator */
  ISRC          isrc;         /* CHARSRC's source interface (output) */
  ICHARSRC      icharsrc;     /* CHARSRC's direct interface */
  Int           classSize;    /* The size of the CLASS of char frame OBJTs */
  CLASS         charClass;    /* The CLASS of char frame OBJTs */
  Bool          startFlag;    /* True when readLine is to be called on the next char request */
  Char        * startPtr;
  Char        * readPtr;
  Char          line[DFLT_BUFSIZE];
  SN            seqNum;
} charsrc_t, * CHARSRC;

/* Private functions */

static ICHARSRC ICHARSRC_create(IGEN igen);

static Bool  CHARSRC_annihilate(IGEN igen);
static Bool  CHARSRC_initialize(IGEN igen, IATTR iattr);
static Bool  CHARSRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  CHARSRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static ICHARSRCATTR  ICHARSRCATTR_create(IGEN igen);

/*** CHARSRC Methods ***/

IGEN
CHARSRC_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an CHARSRC object
 *---------------------------------------------------------------------------*/
{
  CHARSRC    charsrc = (CHARSRC) imem->Calloc(imem, 1, sizeof(charsrc_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) charsrc);
  ISRC       isrc = ISRC_create(igen);
  ICHARSRC   icharsrc = NULL;

  icharsrc = ICHARSRC_create(igen);

  /* Initialize the object */
  charsrc->imem = imem;
  charsrc->isrc = isrc;
  charsrc->icharsrc = icharsrc;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, CHARSRC_annihilate);
  IGEN_setInitializer(igen, CHARSRC_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, CHARSRC_getFrame);
  ISRC_setFrameAvailable(isrc, CHARSRC_frameAvailable);

  return(igen);
}

/*** GENERIC METHODS ***/

static Bool
CHARSRC_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a charsrc object
 * This supplements the generic IGEN_annihilate()
 *---------------------------------------------------------------------------*/
{
  CHARSRC     charsrc = (CHARSRC) IFACE_object((IFACE) igen);

  /* Annihilate the CLASS of char OBJTs */
  if (charsrc->charClass && !CLASS_destroy(charsrc->charClass, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }
  return(TRUE);
}

static Bool
CHARSRC_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a CHARSRC according to a CHARSRC attributes 
 * specification.  Like all IGEN_initialize() functions, this may be called 
 * repeatedly with different IATTRs.
 *---------------------------------------------------------------------------*/
{
  CHARSRC     charsrc = (CHARSRC) IFACE_object((IFACE) igen);
  CHARSRCATTR charsrcattr = (CHARSRCATTR) IFACE_object((IFACE) iattr);
  Int         classSize;

  /* Get the charsrc attributes */
  ICHARSRCATTR_getClassSize((ICHARSRCATTR) iattr, &classSize);
  /* If the the classSize has changed, re-create the CLASS */
  if (classSize != charsrc->classSize) {
    if (charsrc->charClass)
      CLASS_destroy(charsrc->charClass, NULL, NULL);
    charsrc->charClass = CLASS_create(charsrc->imem, classSize, 2, NULL, NULL);
    charsrc->classSize = classSize;
  }
  charsrc->seqNum = 0;
  /* Fill in initial silence */
  memset(charsrc->line, ' ', BOU_SILENCE_LEN);
  charsrc->startPtr = charsrc->line + BOU_SILENCE_LEN;
  charsrc->readPtr = charsrc->startPtr;
  *(charsrc->readPtr) = '\0';
  return(TRUE);
}

static Bool
CHARSRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports an char frame from via the source (output) interface
 * BEHAVIOR: This routine will BLOCK on reading line from stdin if there is no
 *           char available in charsrc->line buffer and charsrc->startFlag==TRUE.
 *---------------------------------------------------------------------------*/
{
  CHARSRC     charsrc;
  Int         n;
  Char      * p;
  Char      * s;

  assert(isrc && snP);
  charsrc = (CHARSRC) IFACE_object((IFACE) isrc);
  assert(charsrc);

  /* Frame retrieval is only permitted in the FORWARD direction */
  if (dir != FORWARD) {
    IFACE_setError((IFACE) isrc, EXC_NOBACKWARD);
    return(FALSE);
  }
  /* Only the next SN is available from the char source */
  *snP = charsrc->seqNum;
  /* First see if a frame could be gotten */
  if ('\0' == *(charsrc->readPtr) && !charsrc->startFlag) {
    /* Nothing to send, set End-of-Utterance exception */
    IFACE_setError((IFACE) isrc, EXC_EOU);
/*    VBX_DEBUG(VBX_print("  CHARSRC: setting %s\n", "EXC_EOU")); */
    return(FALSE);
  } else if ('\0' == *(charsrc->readPtr)) {
    charsrc->startFlag = FALSE;
    /* Read new line */
/*    VBX_DEBUG(VBX_print("PLEASE ENTER A PHRASE: ")); */
    if (!fgets(charsrc->startPtr, MAX_INPUT_LEN, stdin)) {
      /* Read error or eof on stdin reached. Set End-of-Utterance exception again */
      IFACE_setError((IFACE) isrc, EXC_EOU);
      VBX_DEBUG(VBX_print("  CHARSRC: setting %s reading the line\n", "EXC_EOU"));
      return(FALSE);
    }
    n = strlen(charsrc->line);
    if ('\n' == charsrc->line[n-1])
      n--;
    if ('\r' == charsrc->line[n-1])
      n--;
    /* Handle backspaces among real char */
    while (p = strchr(charsrc->startPtr, '\b')) {
      /* Kill bs itself and char before it unless it is the first */
      s = p + 1;
      if (p > charsrc->startPtr) {
        --p;
        n -= 2;
      } else {
        n -= 1;
      }
      while (*p++ = *s++);
    }
    /* Fill in ending silence and terminate */
    memset(charsrc->line + n, ' ', EOU_SILENCE_LEN);
    charsrc->line[n + EOU_SILENCE_LEN] = '\0';
    charsrc->readPtr = charsrc->line;
  }
  /* Retrieve the frame if requested */
  if (outP) {
    *outP = OBJT_new(charsrc->charClass);
    /* If an OBJT is available, fill it with the char data */
    if (*outP) {
      p = (Char *)&OBJT_contents(*outP);
      *p = *(charsrc->readPtr)++;
/*      VBX_DEBUG(VBX_print("CHARSRC_getFrame: return char <%c>; snP " QuadFmt(-) "\n", *p, *snP)); */
      (charsrc->seqNum)++;
    } else { /* Otherwise, report an exception */
      IFACE_setError((IFACE) isrc, EXC_CHARCLASSEMPTY);
      return(FALSE);
    }
  } else {
/*    VBX_DEBUG(VBX_print("CHARSRC_frameAvailable: return snP " QuadFmt(-) "\n", *snP)) */;
  }

  IFACE_setError((IFACE) isrc, EXC_NONE);
  return(TRUE);
}

static Bool
CHARSRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an char frame via the source (output) interface
 * Requested sequence number is ignored.
 *---------------------------------------------------------------------------*/
{
  return(CHARSRC_getFrame(isrc, dir, snP, NULL));
}

/*** ICHARSRC Methods ***/

static ICHARSRC
ICHARSRC_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICHARSRC interface to the character source
 *---------------------------------------------------------------------------*/
{
  ICHARSRC  icharsrc;

  if ((icharsrc = (ICHARSRC) IFACE_calloc((IFACE) igen, 1, sizeof(icharsrc_t)))) {
    IFACE_setClass((IFACE) icharsrc, &ICHARSRC_Class);
    IFACE_setObject((IFACE) icharsrc, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) icharsrc, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) icharsrc);
  }

  return(icharsrc);
}

void
ICHARSRC_Start(ICHARSRC iCharSrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Starts the character source
 *---------------------------------------------------------------------------*/
{
  CHARSRC charsrc = (CHARSRC) IFACE_object((IFACE) iCharSrc);

  charsrc->startFlag = TRUE;
}

void
ICHARSRC_Stop(ICHARSRC iCharSrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Stops and flushes the character source
 *---------------------------------------------------------------------------*/
{
  CHARSRC charsrc = (CHARSRC) IFACE_object((IFACE) iCharSrc);

  /* Make sure the readPtr points within buffer at 0 byte,
     so next read if any will produce EXC_EOU  */
  *(charsrc->startPtr) = '\0';
  charsrc->readPtr = charsrc->startPtr;
}

/*** CHARSRCATTR Methods ***/

IGEN
CHARSRCATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a CHARSRC attributes object
 *---------------------------------------------------------------------------*/
{
  CHARSRCATTR   charsrcattr = (CHARSRCATTR) imem->Calloc(imem, 1, sizeof(charsrcattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(imem, (OBJ) charsrcattr);
  ICHARSRCATTR  icharsrcattr = ICHARSRCATTR_create(igen);

  /* Generic IGEN_annihilate() suffices */

  /* Set the default attributes */
  charsrcattr->classSize = DFLT_CLASSSIZE;

  return(igen);
}

/*** ICHARSRCATTR METHODS ***/

static ICHARSRCATTR
ICHARSRCATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ICHARSRCATTR interface
 *---------------------------------------------------------------------------*/
{
  ICHARSRCATTR  icharsrcattr;

  if ((icharsrcattr = (ICHARSRCATTR) IFACE_calloc((IFACE) igen, 1, sizeof(icharsrcattr_t)))) {
    IFACE_setClass((IFACE) icharsrcattr, &ICHARSRCATTR_Class);
    IFACE_setObject((IFACE) icharsrcattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) icharsrcattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) icharsrcattr);
  }

  return(icharsrcattr);
}

Bool
ICHARSRCATTR_getClassSize(ICHARSRCATTR icharsrcattr, Int * classSizeP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the size of the class of char frames
 *---------------------------------------------------------------------------*/
{
  CHARSRCATTR  charsrcattr;

  assert(icharsrcattr != NULL);
  charsrcattr = (CHARSRCATTR) IFACE_object((IFACE) icharsrcattr);

  *classSizeP = charsrcattr->classSize;

  return(TRUE);
}

Bool
ICHARSRCATTR_setClassSize(ICHARSRCATTR icharsrcattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of char frames
 *---------------------------------------------------------------------------*/
{
  CHARSRCATTR  charsrcattr;

  assert(icharsrcattr != NULL);
  charsrcattr = (CHARSRCATTR) IFACE_object((IFACE) icharsrcattr);

  charsrcattr->classSize = classSize;

  return(TRUE);
}
