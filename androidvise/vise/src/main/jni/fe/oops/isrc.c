/*  isrc.c - the source interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * HISTORY:
 * 20-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: isrc.c[1.1] Tue Jun  9 17:44:11 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdlib.h>
#include "types.h"
#include <assert.h>

#include "class.h"
#include "iface.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"


/*** SOURCE ***/

/* The Class of Source Interface Objects */
static iclass_t ISRC_Class = {ISRCID};

/* Default source methods */
static Bool  ISRC_dfltGetFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * frP);
static Bool  ISRC_dfltFrameAvailable(ISRC isrc, dir_t dir, SN * snP);


/*** SOURCE METHODS ***/

Bool
ISRC_setSink(ISRC isrc, ISNK isnk)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the sink for a source
 *---------------------------------------------------------------------------*/
{
  isrc->sink = isnk;
  if (isrc->setSink) isrc->setSink(isrc, isnk);
  return(TRUE);
}


Bool
ISRC_getAttributes(ISRC isrc, IATTR * iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the attributes of a source
 *---------------------------------------------------------------------------*/
{
  /* If there is a special function, report any exception it generates */
  if (isrc->getAttributes && !isrc->getAttributes(isrc, &isrc->iattr))
    return(FALSE);

  /* Otherwise, just proceed to get the source's attributes */
  *iattr = isrc->iattr;
  if (!*iattr) {
    IFACE_setError((IFACE) isrc, EXC_NOATTRIBUTES);
    return(FALSE);
  } else {
    return(TRUE);
  }
}


Bool
ISRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * frP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the requested frame or the available frame nearest to the requested
 * frame in the specified direction, returning it and its SN.  Called with a
 * negative SN, it returns the next available frame relative to the last frame
 * requested.  Called with frP NULL, it is the same as ISRC_frameAvailable().
 *---------------------------------------------------------------------------*/
{
  return(isrc->getFrame(isrc, dir, snP, frP));
}


Bool
ISRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Same as ISRC_getFrame, but does not get the frame
 *---------------------------------------------------------------------------*/
{
  return(isrc->frameAvailable(isrc, dir, snP));
}


static Bool
ISRC_dfltGetFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * frP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  *snP = EXC_DFLTSRC;
  IFACE_setError((IFACE) isrc, EXC_DFLTSRC);
  *frP = NULL;
  return(FALSE);
}


static Bool
ISRC_dfltFrameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  *snP = EXC_DFLTSRC;
  IFACE_setError((IFACE) isrc, EXC_DFLTSRC);
  return(FALSE);
}


/*** ISRC METHODS ***/

ISRC
ISRC_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a source interface for an object
 *---------------------------------------------------------------------------*/
{
  ISRC  isrc;

  if ((isrc = (ISRC) IFACE_calloc((IFACE) igen, 1, sizeof(isrc_t)))) {
    IFACE_setClass((IFACE) isrc, &ISRC_Class);
    IFACE_setObject((IFACE) isrc, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) isrc, IFACE_allocator((IFACE) igen));
    ISRC_default(isrc);
    IFACE_addSibling((IFACE) igen, (IFACE) isrc);
  }

  return(isrc);
}


void
ISRC_default(ISRC isrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set a source interface to its default configuration
 *---------------------------------------------------------------------------*/
{
  isrc->getFrame = ISRC_dfltGetFrame;
  isrc->frameAvailable = ISRC_dfltFrameAvailable;
}


ISNK
ISRC_sink(ISRC isrc)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Return the source's sink
 *---------------------------------------------------------------------------*/
{
  return(isrc->sink);
}


void
ISRC_setAttributes(ISRC isrc, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the source's attributes
 *---------------------------------------------------------------------------*/
{
  isrc->iattr = iattr;
}


void
ISRC_setSetSink(ISRC isrc, Bool (* fcn)(ISRC, ISNK))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isrc->setSink = fcn;
}


void
ISRC_setGetAttributes(ISRC isrc, Bool (* fcn)(ISRC, IATTR *))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isrc->getAttributes = fcn;
}


void
ISRC_setGetFrame(ISRC isrc, Bool (* fcn)(ISRC, dir_t, SN *, OBJT *))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isrc->getFrame = fcn;
}


void
ISRC_setFrameAvailable(ISRC isrc, Bool (* fcn)(ISRC, dir_t, SN *))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  isrc->frameAvailable = fcn;
}
