/*  igen.c - the generic interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * HISTORY:
 * 19-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$Header: igen.c[1.1] Tue Jun  9 17:42:39 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdlib.h>
#include "types.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"

/* The Class of Generic Interface Objects */
static iclass_t IGEN_Class = {IGENID};

/* The Generic Interface to an Object */
typedef struct igen_s {
  iface_t  iface;
  Bool     (* annihilate) (IGEN igen);
  Bool     (* initialize) (IGEN igen, IATTR attributes);
} igen_t;

/* Default Generic Methods */
static Bool  IGEN_dfltInit(IGEN igen, IATTR attr);


/*** GENERIC METHODS ***/

Bool
IGEN_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate an object -- calls the object-specific annihilation function,
 * if any, then annihilates the object itself and all its interfaces.
 *---------------------------------------------------------------------------*/
{
  if (igen) {

    /* Call the object-specific annihilation function, if any */
    if (igen->annihilate && !igen->annihilate(igen)) return(FALSE);

    /* Free the object */
    if (IFACE_object((IFACE) igen))
      IFACE_free((IFACE) igen, IFACE_object((IFACE) igen));

    /* Free the interfaces */
    IFACE_annihilate((IFACE) igen);

  }

  return(TRUE);
}


Bool
IGEN_initialize(IGEN igen, IATTR attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize the object
 *---------------------------------------------------------------------------*/
{
  return(igen->initialize(igen, attr));
}


static Bool
IGEN_dfltInit(IGEN igen, IATTR attr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Default object initializer
 *---------------------------------------------------------------------------*/
{
  return(TRUE);
}


/*** IGEN METHODS ***/

IGEN
IGEN_create(IMemory * allocator, OBJ object)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a generic interface for an object
 *---------------------------------------------------------------------------*/
{
  IGEN igen;

  if ((igen = (IGEN) allocator->Calloc(allocator, 1, sizeof(igen_t), OOPS_MEMORY))) {
    IFACE_setClass((IFACE) igen, &IGEN_Class);
    IFACE_setObject((IFACE) igen, object);
    IFACE_setAllocator((IFACE) igen, allocator);
    igen->annihilate = NULL;
    igen->initialize = IGEN_dfltInit;
  }

  return(igen);
}


void
IGEN_setAnnihilator(IGEN igen, Bool (* fcn)(IGEN))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Provide an object-specific annihilation function.  This SUPPLEMENTS, NOT
 * REPLACES, the default annihilation function, which handles everything
 * except memory which the object itself has allocated.
 *---------------------------------------------------------------------------*/
{
  igen->annihilate = fcn;
}


void
IGEN_setInitializer(IGEN igen, Bool (* fcn)(IGEN, IATTR))
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Provide an object-specific initialization function
 *---------------------------------------------------------------------------*/
{
  igen->initialize = fcn;
}
