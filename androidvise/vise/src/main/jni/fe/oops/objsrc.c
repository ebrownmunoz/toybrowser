/* objsrc.c - an active frame emitter
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 25-Oct-96  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: objsrc.c[1.0] Tue Jun  9 17:49:23 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "objsrc.h"
#include "class.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include <assert.h>


/* The Class of OBJSOURCE Interface Objects */
static iclass_t IOBJSOURCE_Class = {IOBJSOURCEID};

/* The OBJSOURCE Interface */
typedef struct iobjsource_s {
  iface_t  iface;
} iobjsource_t;


/* An OBJSOURCE descriptor */
typedef struct objsource_s {
  ISRC  isrc;               /* OBJSOURCE's source interface (output) */
} objsource_t, * OBJSOURCE;


/* Private functions */

static IOBJSOURCE  IOBJSOURCE_create(IGEN igen);


/*** OBJSOURCE Methods ***/

IGEN
OBJSOURCE_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a frame emitter
 *---------------------------------------------------------------------------*/
{
  OBJSOURCE  objsource = (OBJSOURCE) imem->Calloc(imem, 1, sizeof(objsource_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) objsource);
  ISRC       isrc = ISRC_create(igen);
  IOBJSOURCE iobjsource = IOBJSOURCE_create(igen);

  /* Initialize the object */
  objsource->isrc = isrc;

  return(igen);
}


/*** IOBJSOURCE METHODS ***/

static IOBJSOURCE
IOBJSOURCE_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IOBJSOURCE interface on the emitter
 *---------------------------------------------------------------------------*/
{
  IOBJSOURCE  iobjsource;

  if ((iobjsource = (IOBJSOURCE) IFACE_calloc((IFACE) igen, 1, sizeof(iobjsource_t)))) {
    IFACE_setClass((IFACE) iobjsource, &IOBJSOURCE_Class);
    IFACE_setObject((IFACE) iobjsource, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iobjsource, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iobjsource);
  }

  return(iobjsource);
}


Bool
IOBJSOURCE_setAttributes(IOBJSOURCE iobjsource, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the attributes of the source (output) interface
 *---------------------------------------------------------------------------*/
{
  OBJSOURCE  objsource;
  ISNK       snk;

  assert(iobjsource && iattr);

  objsource = (OBJSOURCE) IFACE_object((IFACE) iobjsource);

  /* Set the attributes of the source (output) interface */
  ISRC_setAttributes(objsource->isrc, iattr);

  /* If there is a sink attached, pass the attributes to it */
  if (snk = ISRC_sink(objsource->isrc))
    ISNK_setAttributes(snk, iattr);

  return(TRUE);
}


Bool
IOBJSOURCE_putFrame(IOBJSOURCE iobjsource, dir_t dir, SN * snP, OBJT frame)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Insert a frame OBJT into the sink
 *---------------------------------------------------------------------------*/
{
  OBJSOURCE   objsource;
  ISNK        snk;
  
  assert(iobjsource && snP);

  objsource = ((OBJSOURCE) IFACE_object((IFACE) iobjsource));

  /* Get the sink associated with the source (output) interface */
  snk = ISRC_sink(objsource->isrc);
  /* If there is none, register an exception */
  if (!snk) {
    IFACE_setError((IFACE) iobjsource, EXC_NOSNK);
    return(FALSE);
  }

  /* Try to insert the frame into the sink */
  if (ISNK_putFrame(snk, dir, snP, frame)) {
    /* If successful, just return TRUE */
    return(TRUE);
  } else {
    /* Otherwise, register whatever exception the sink yields */
    IFACE_setError((IFACE) iobjsource, IFACE_error((IFACE) snk));
    return(FALSE);
  }
}


Bool
IOBJSOURCE_spaceAvailable(IOBJSOURCE iobjsource, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Report the availability of space to insert a frame object
 *---------------------------------------------------------------------------*/
{
  return(IOBJSOURCE_putFrame(iobjsource, dir, snP, NULL));
}
