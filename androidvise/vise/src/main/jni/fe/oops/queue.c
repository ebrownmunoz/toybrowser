/* queue.c - a bidirectional queue of arbitrary objects
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 29-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$__Header$";

#include "pthr.h"
#include <stdio.h>
#include <stdlib.h>
#include "types.h"
#include "class.h"
#include "critsect.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "queue.h"
#include "sn.h"
#include <assert.h>

#undef VBXDEBUG
#ifdef VBXDEBUG
#include "vbx.h"
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif


/* The Class of QUEUE Interface Objects */
static iclass_t IQUEUE_Class = {IQUEUEID};

/* A Queue Element Index */
typedef Int  qidx_t;

/* The QUEUE Interface */
typedef struct iqueue_s {
  isrc_t   isrc;
  qidx_t   read;           /* The index of the element for IQUEUE to get */
  SN       prevSN;         /* The SN most recently gotten via IQUEUE */
  dir_t    getDir;         /* The direction of the last getFrame via IQUEUE */
} iqueue_t;

/* The Class of QSINK Interface Objects */
static iclass_t IQSINK_Class = {IQSINKID};

/* The QSINK Interface */
typedef struct iqsink_s {
  isrc_t   isrc;
} iqsink_t;

/* The Class of QATTR (QUEUE Attributes) Interface Objects */
static iclass_t IQATTR_Class = {IQATTRID};

/* A Queue Element */
typedef struct qel_s {
  SN    sn;
  OBJT  objt;
} qel_t, * QEL;

/* A QUEUE descriptor */
typedef struct queue_s {
  IMemory * imem;             /* The memory allocator */
  ISRC      isrc;             /* QUEUE's source interface (output) */
  ISNK      isnk;             /* QUEUE's sink interface (input) */
  IQUEUE    iqueue;           /* QUEUE's external interface (output) */
  IQSINK    iqsink;           /* QUEUE's external passive sink interface (input) */
  CRITSECT_DECLARE(critsect)  /* The queue's synchronization object */
  qidx_t    first;            /* The index of the first valid element - 1 */
  qidx_t    last;             /* The index of the last valid element */
  qidx_t    read;             /* The index of the element for ISRC to read */
  dir_t     getDir;           /* The direction of the last get from the input */
  SN        snSought;         /* The last SN sought and found by isrc */
  Bool      snFound;          /* Flag indicating that snSought has been found */
  UInt      overwritten;      /* The number of frames overwritten by wraparound */
  qidx_t    size;             /* The number of elements in the queue */
  SN        fwdTol;           /* The tolerance for SNs out of range forward */
  SN        bwdTol;           /* The tolerance for SNs out of range backward */
  Int       snkMode;          /* The sink's mode: SNK_PASSIVE or SNK_ACTIVE */
  Int       timeout;          /* SNK_PASSIVE getFrame timeout (in millisec, 0 => none) */
  TIMESPEC  wait;             /* SNK_PASSIVE getFrame timeout (as a timespec) */
  Bool      faq;              /* If TRUE, frameAvailable gets/queues a frame */
  QEL       body;             /* The body of the queue */
} queue_t, * QUEUE;


/* QUEUE Macros */

#define QUEUE_empty(Q)       ((Q)->first == (Q)->last)
#define QUEUE_full(Q)        ((Q)->first == (Q)->last)
#define QUEUE_incrIdx(Q, I)  { (I)++; if ((I) >= (Q)->size) (I) -= (Q)->size; }
#define QUEUE_decrIdx(Q, I)  { (I)--; if ((I) < 0) I += (Q)->size; }

#define QUEUE_setNextSN(Q, DIR, SNP)  {                      \
  if ((Q)->getDir == (DIR)) { *(SNP) = -1; }                 \
  else { *(SNP) += (DIR); (Q)->getDir = (DIR); }             \
}

#define QUEUE_minmaxSN(Q, BODY, MINSNP, MAXSNP)  {           \
  qidx_t  _first_ = (Q)->first;                              \
  /* Pre-increment post-decremented backward write index */  \
  QUEUE_incrIdx((Q), _first_);                               \
  *(MINSNP) = (BODY)[_first_].sn;                            \
  *(MAXSNP) = (BODY)[(Q)->last].sn;                          \
}

#define QUEUE_nextSNToWrite(Q, BODY, DIR, SNP) {             \
  if ((DIR) == FORWARD) {                                    \
    *(SNP) = (BODY)[(Q)->last].sn + (DIR);                   \
  } else {                                                   \
    qidx_t  _first_ = (Q)->first;                            \
    /* Pre-increment post-decremented BWD write index */     \
    QUEUE_incrIdx((Q), _first_);                             \
    *(SNP) = (BODY)[_first_].sn + (DIR);                     \
  }                                                          \
}

#define QUEUE_writeFwd(Q, BODY, SN, OBJ)  {                  \
  /* Pre-increment the write index when writing forward */   \
  QUEUE_incrIdx((Q), (Q)->last);                             \
  (BODY)[(Q)->last].sn = (SN);                               \
  (BODY)[(Q)->last].objt = (OBJ);                            \
  /* If this fills the queue, remove the first object */     \
  if (QUEUE_full(Q)) {                                       \
    QUEUE_incrIdx((Q), (Q)->first);                          \
    OBJT_free((BODY)[(Q)->first].objt);                      \
    /* If the read index was at the start, keep it there */  \
    if ((Q)->read == (Q)->last)                              \
      { (Q)->read = (Q)->first; (Q)->overwritten++; }        \
  }                                                          \
}

#define QUEUE_writeBwd(Q, BODY, SN, OBJ) {                   \
  /* Post-decrement the write index when writing backward */ \
  (BODY)[(Q)->first].sn = (SN);                              \
  (BODY)[(Q)->first].objt = (OBJ);                           \
  QUEUE_decrIdx((Q), (Q)->first);                            \
  /* If this fills the queue, remove the last object */      \
  if (QUEUE_full(Q)) {                                       \
    OBJT_free((BODY)[(Q)->last].objt);                       \
    QUEUE_decrIdx((Q), (Q)->last);                           \
    /* If the read index was at the end, keep it there */    \
    if ((Q)->read == (Q)->first)                             \
      { (Q)->read = (Q)->last; (Q)->overwritten++; }         \
  }                                                          \
}

/* Only the following five macros call OBJT_copy in this module */

#define QUEUE_readFwd(Q, BODY, RDIDX, SNP, OBJP) {           \
  /* Pre-increment the read index when reading forward */    \
  QUEUE_incrIdx((Q), (RDIDX));                               \
  *(SNP) = (BODY)[(RDIDX)].sn;                               \
  if (OBJP) *(OBJP) = OBJT_copy((BODY)[(RDIDX)].objt);       \
}

#define QUEUE_reReadFwd(Q, BODY, RDIDX, SNP, OBJP) {         \
  *(SNP) = (BODY)[(RDIDX)].sn;                               \
  if (OBJP) *(OBJP) = OBJT_copy((BODY)[(RDIDX)].objt);       \
}

#define QUEUE_readBwd(Q, BODY, RDIDX, SNP, OBJP) {           \
  *(SNP) = (BODY)[(RDIDX)].sn;                               \
  if (OBJP) *(OBJP) = OBJT_copy((BODY)[(RDIDX)].objt);       \
  /* Post-decrement the read index when reading backward */  \
  QUEUE_decrIdx((Q), (RDIDX));                               \
}

#define QUEUE_reReadBwd(Q, BODY, RDIDX, SNP, OBJP) {         \
  /* Pre-increment the read index when re-reading bwd */     \
  QUEUE_incrIdx((Q), (RDIDX));                               \
  *(SNP) = (BODY)[(RDIDX)].sn;                               \
  if (OBJP) *(OBJP) = OBJT_copy((BODY)[(RDIDX)].objt);       \
  /* Post-decrement the read index when re-reading bwd */    \
  QUEUE_decrIdx((Q), (RDIDX));                               \
}

#define QUEUE_readAtIndex(Q, BODY, RDIDX, DIR, SNP, OBJP) {  \
  *(SNP) = (BODY)[RDIDX].sn;                                 \
  if (OBJP) *(OBJP) = OBJT_copy((BODY)[RDIDX].objt);         \
  /* Post-decrement the read index if reading backward */    \
  if (DIR == BACKWARD) QUEUE_decrIdx((Q), (RDIDX));          \
}


/* Private functions */

static IQUEUE  IQUEUE_create(IGEN igen);
static IQSINK  IQSINK_create(IGEN igen);

static Bool    IQUEUE_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool    IQUEUE_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static void    QUEUE_reset(QUEUE q);
static Bool    QUEUE_annihilate(IGEN igen);
static Bool    QUEUE_initialize(IGEN igen, IATTR iattr);
static Bool    QUEUE_setAttributes(ISNK isnk, IATTR iattr);
static Bool    QUEUE_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool    QUEUE_getFromSource(QUEUE q, ISRC source, dir_t dir, SN * snP, OBJT * objP);
static qidx_t  QUEUE_findSN(Int size, QEL body, dir_t dir, qidx_t first, qidx_t last, SN targetSN);
static Bool    QUEUE_frameAvailable(ISRC isrc, dir_t dir, SN * snP);
static Bool    QUEUE_putFrame(ISNK isnk, dir_t dir, SN * snP, OBJT obj);
static Bool    QUEUE_spaceAvailable(ISNK isnk, dir_t dir, SN * snP);
static Bool    QUEUE_insertObject(QUEUE q, dir_t dir, SN sn, OBJT obj);

static IQATTR  IQATTR_create(IGEN igen);


/*** QUEUE Methods ***/

IGEN
QUEUE_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a queue
 *---------------------------------------------------------------------------*/
{
  QUEUE   queue = (QUEUE) imem->Calloc(imem, 1, sizeof(queue_t), OOPS_MEMORY);
  IGEN    igen = IGEN_create(imem, (OBJ) queue);
  ISNK    isnk = ISNK_create(igen);
  ISRC    isrc = ISRC_create(igen);
  IQUEUE  iqueue = IQUEUE_create(igen);
  IQSINK  iqsink = IQSINK_create(igen);
  IATTR   iattr;

  /* Initialize the object */
  queue->imem = imem;
  queue->isnk = isnk;
  queue->isrc = isrc;
  queue->iqueue = iqueue;
  queue->iqsink = iqsink;
  CRITSECT_CREATE(queue->critsect, imem, NULL);
  queue->snFound = FALSE;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, QUEUE_annihilate);
  IGEN_setInitializer(igen, QUEUE_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setAttributes(isrc, iattr);
    ISRC_setAttributes((ISRC) iqueue, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the sink interface (active by default) */
  ISNK_setSetAttributes(isnk, QUEUE_setAttributes);

  /* Set up the source interface (passive) */
  ISRC_setGetFrame(isrc, QUEUE_getFrame);
  ISRC_setFrameAvailable(isrc, QUEUE_frameAvailable);

  /* Set up the queue interface (passive) */
  ISRC_setGetFrame((ISRC) iqueue, IQUEUE_getFrame);
  ISRC_setFrameAvailable((ISRC) iqueue, IQUEUE_frameAvailable);

  /* Set up the qsink interface (passive) */
  ISNK_setSetAttributes((ISNK) iqsink, QUEUE_setAttributes);
  ISNK_setPutFrame((ISNK) iqsink, QUEUE_putFrame);
  ISNK_setSpaceAvailable((ISNK) iqsink, QUEUE_spaceAvailable);

  VBX_DEBUG(VBX_print("  QUEUE: queue created @%p\n", queue));

  return(igen);
}


static void
QUEUE_reset(QUEUE q)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the QUEUE, freeing any OBJTs it contains
 *---------------------------------------------------------------------------*/
{
  QEL     body;
  qidx_t  idx;
  IQUEUE  iq = q->iqueue;

  /* Get the body of the queue */
  body = q->body;

  /* If the queue has a body, free all the OBJTs in it */
  if (body) {
    for (idx = q->first; idx != q->last; ) {
      QUEUE_incrIdx(q, idx);
      OBJT_free(body[idx].objt);
    }
  }

  /* Reset the queue indices */
  q->first = q->last = q->read = 0;

  q->overwritten = 0;

  q->snSought = -1;
  q->getDir = UNTOWARD;
  q->snFound = FALSE;

  /* Initialize IQUEUE */
  iq->read = 0;
  iq->prevSN = -1;
  iq->getDir = UNTOWARD;
}


/*** IQUEUE METHODS ***/

static IQUEUE
IQUEUE_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IQUEUE interface
 *---------------------------------------------------------------------------*/
{
  IQUEUE  iqueue;

  if ((iqueue = (IQUEUE) IFACE_calloc((IFACE) igen, 1, sizeof(iqueue_t)))) {
    IFACE_setClass((IFACE) iqueue, &IQUEUE_Class);
    IFACE_setObject((IFACE) iqueue, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iqueue, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iqueue);
  }

  return(iqueue);
}


Bool
IQUEUE_reset(IQUEUE iq)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the QUEUE, freeing any OBJTs it contains
 *---------------------------------------------------------------------------*/
{
  QUEUE   q;
  assert(iq);

  q = ((QUEUE) IFACE_object((IFACE) iq));

  CRITSECT_ENTER(q->critsect);
  QUEUE_reset(q);
  VBX_DEBUG(VBX_print("  QUEUE: queue @%p reset by IQUEUE_reset\n", q));
  CRITSECT_LEAVE(q->critsect);

  return(TRUE);
}


Bool
IQUEUE_range(IQUEUE iq, SN * minSNP, SN * maxSNP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Report the minimum and maximum sequence numbers in the queue
 *---------------------------------------------------------------------------*/
{
  QUEUE    q;
  QEL      body;
  Bool     status = FALSE;

  assert(iq);

  q = ((QUEUE) IFACE_object((IFACE) iq));
  body = q->body;

  /* Report an exception if the queue has no body */
  if (!body) {
    IFACE_setError((IFACE) iq, EXC_QUNINIT);
  } else {
    CRITSECT_ENTER(q->critsect);
    /* Report an exception if the queue is empty */
    if (QUEUE_empty(q)) {
      IFACE_setError((IFACE) iq, EXC_QEMPTY);
    } else {
      /* Get the limiting sequence numbers */
      QUEUE_minmaxSN(q, body, minSNP, maxSNP);
      status = TRUE;
    }
    CRITSECT_LEAVE(q->critsect);
  }

  return(status);

}


Bool
IQUEUE_frameCount(IQUEUE iq, SN * minSNP, SN * maxSNP, SN * countP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Report the number of frames that have sequence numbers in the given range,
 * and the nearest extant starting and ending sequence numbers in the range.
 * If there are no frames in the range, the sequence numbers remain unchanged.
 *---------------------------------------------------------------------------*/
{
  QUEUE    q;
  QEL      body;
  SN       minQSN, maxQSN;
  qidx_t   minQIdx, maxQIdx;
  SN       minSN,  maxSN;
  qidx_t   minIdx, maxIdx;

  assert(iq);

  q = ((QUEUE) IFACE_object((IFACE) iq));
  body = q->body;

  *countP = 0;
  minSN = *minSNP;
  maxSN = *maxSNP;

  /* Report an exception if the queue has no body */
  if (!body) {
    IFACE_setError((IFACE) iq, EXC_QUNINIT);
    return(FALSE);
  } else {
    IFACE_setError((IFACE) iq, EXC_NONE);
    CRITSECT_ENTER(q->critsect);

    if (!QUEUE_empty(q) && minSN <= maxSN) {

      /* Get the limiting indices and sequence numbers in the queue */
      minQIdx = q->first;
      QUEUE_incrIdx(q, minQIdx);
      minQSN = body[minQIdx].sn;
      maxQIdx = q->last;
      maxQSN = body[maxQIdx].sn;

      /* Make sure the desired region overlaps what's in the queue */
      if (minQSN <= maxSN && minSN <= maxQSN) {

        /* Limit the desired region to what's in the queue */
        if (minSN < minQSN) minSN = minQSN;
        if (maxQSN < maxSN) maxSN = maxQSN;

        /* Get the index of the frame with the starting or nearest greater SN */
        if (minSN == body[minQIdx].sn)
          minIdx = minQIdx;
        else if (minSN == body[maxQIdx].sn)
          minIdx = maxQIdx;
        else
          minIdx = QUEUE_findSN(q->size, body, FORWARD, minQIdx, maxQIdx, minSN);
        /* Get the actual starting SN */
        minSN = body[minIdx].sn;

        /* Make sure the actual starting SN doesn't exceed the desired ending SN */
        if (minSN <= maxSN) {

          /* Get the index of the frame with the ending or nearest less SN */
          if (maxSN == body[minIdx].sn)
            maxIdx = minIdx;
          else if (maxSN == body[maxQIdx].sn)
            maxIdx = maxQIdx;
          else
            maxIdx = QUEUE_findSN(q->size, body, BACKWARD, minIdx, maxQIdx, maxSN);
          /* Get the actual ending SN */
          maxSN = body[maxIdx].sn;

          *countP = maxIdx - minIdx + 1;
          /* If wrapped around, add queue size */
          if (*countP <= 0) *countP += q->size;

          *minSNP = minSN;
          *maxSNP = maxSN;
        }
      }
    }

    CRITSECT_LEAVE(q->critsect);
    return(TRUE);
  }
}


static Bool
IQUEUE_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * objP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a frame OBJT from the queue
 * This implements a second ISRC interface for QUEUE, one which permits
 * reading elements from the QUEUE but does not provoke getFrame calls to
 * the QUEUE's source if the requested frame isn't in the QUEUE.  Care is
 * taken to ensure that IQUEUE's read index is still valid, regardless of
 * what might be happening at the main ISRC.
 *---------------------------------------------------------------------------*/
{
  QUEUE     q;
  QEL       body;
  IQUEUE    iq = (IQUEUE) isrc;
  qidx_t    first, last, target;
  SN        prevSN, firstSN, lastSN, targetSN = *snP;

  assert(isrc && snP);

  q = ((QUEUE) IFACE_object((IFACE) isrc));
  body = q->body;

  /* Report an exception if the queue has no body */
  if (!body) {
    IFACE_setError((IFACE) isrc, EXC_QUNINIT);
    return(FALSE);
  }

  CRITSECT_ENTER(q->critsect);

  /* Report an exception if the queue is empty */
  if (QUEUE_empty(q)) {
    CRITSECT_LEAVE(q->critsect);
    IFACE_setError((IFACE) isrc, EXC_QEMPTY);
    return(FALSE);
  }

  /* Get the beginning and ending indices of the valid part of the queue */
  first = q->first;
  last = q->last;

  /* If this is a request for the next SN ... */
  if (*snP < 0) {
    /* A read relative to no previous SN is senseless */
    if (iq->prevSN < 0) {
      CRITSECT_LEAVE(q->critsect);
      IFACE_setError((IFACE) isrc, EXC_NOPREVIOUSSN);
      return(FALSE);
    }
    /* If the read index is in the valid region, check what's there */
    if (first < last && first < iq->read && iq->read < last ||
        first > last && (iq->read < last || first < iq->read)) {
      /* Get the SN of the element last read */
      if (iq->getDir == FORWARD) {
        QUEUE_reReadFwd(q, body, iq->read, &prevSN, (OBJT *) NULL);
      } else {
        QUEUE_reReadBwd(q, body, iq->read, &prevSN, (OBJT *) NULL);
      }
      /* If it hasn't changed, get the next element in the specified direction */
      if (prevSN == iq->prevSN) {
        if (dir == FORWARD) {
          QUEUE_readFwd(q, body, iq->read, snP, objP);
        } else {
          QUEUE_readBwd(q, body, iq->read, snP, objP);
        }
        CRITSECT_LEAVE(q->critsect);
        iq->getDir = dir;
        iq->prevSN = *snP;
        return(TRUE);
      }
    }
    /* Set the SN to the likely desired value and fall through */
    targetSN = iq->prevSN + dir;
    if (targetSN < 0) targetSN = dir == FORWARD ? 0 : SN_MAX;
  }

  /* Treat this as a request for a specific target SN */

  /* Get the limiting SNs */
  QUEUE_incrIdx(q, first);
  firstSN = body[first].sn;
  lastSN = body[last].sn;

  /* If the target SN is in the body of the queue */
  if (firstSN <= targetSN && targetSN <= lastSN) {
    if (targetSN == firstSN) target = first;
    else if (targetSN == lastSN) target = last;
    else target = QUEUE_findSN(q->size, body, dir, first, last, targetSN);

    /* Read from the queue at the target index, in the specified direction */
    iq->read = target;
    QUEUE_readAtIndex(q, body, iq->read, dir, snP, objP);
    iq->prevSN = *snP;
    CRITSECT_LEAVE(q->critsect);
    return(TRUE);

  /* Otherwise, report an exception */
  } else {
    CRITSECT_LEAVE(q->critsect);
    IFACE_setError((IFACE) isrc, EXC_NOTINQUEUE);
    VBX_DEBUG(VBX_print("QUEUE: couldn't find %ld, (%ld - %ld)\n", targetSN, firstSN, lastSN));
    return(FALSE);
  }
}


static Bool
IQUEUE_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports the availability of a frame via the source interface
 *---------------------------------------------------------------------------*/
{
  return(IQUEUE_getFrame(isrc, dir, snP, NULL));
}


/*** IQSINK METHODS ***/

static IQSINK
IQSINK_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IQSINK interface
 *---------------------------------------------------------------------------*/
{
  IQSINK  iqsink;

  if ((iqsink = (IQSINK) IFACE_calloc((IFACE) igen, 1, sizeof(iqsink_t)))) {
    IFACE_setClass((IFACE) iqsink, &IQSINK_Class);
    IFACE_setObject((IFACE) iqsink, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iqsink, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iqsink);
  }

  return(iqsink);
}


Bool
IQSINK_reset(IQSINK iq)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset the QUEUE, freeing any OBJTs it contains
 *---------------------------------------------------------------------------*/
{
  return(IQUEUE_reset((IQUEUE) IFACE_interface((IFACE) iq, IQUEUEID)));
}


Bool
IQSINK_range(IQSINK iq, SN * minSNP, SN * maxSNP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Report the minimum and maximum sequence numbers in the queue
 *---------------------------------------------------------------------------*/
{
  return(IQUEUE_range((IQUEUE) IFACE_interface((IFACE) iq, IQUEUEID), minSNP, maxSNP));
}


Bool
IQSINK_frameCount(IQSINK iq, SN * minSNP, SN * maxSNP, SN * countP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Report the number of frames that have sequence numbers in the given range,
 * and the nearest extant starting and ending sequence numbers in the range.
 * If there are no frames in the range, the sequence numbers remain unchanged.
 *---------------------------------------------------------------------------*/
{
  return(IQUEUE_frameCount((IQUEUE) IFACE_interface((IFACE) iq, IQUEUEID), minSNP, maxSNP, countP));
}


/*** GENERIC METHODS ***/

static Bool
QUEUE_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a queue
 * This supplements the generic IGEN_annihilate() by freeing up any OBJTs left
 * in the queue and de-allocating the body of the queue.
 *---------------------------------------------------------------------------*/
{
  QUEUE q = (QUEUE) IFACE_object((IFACE) igen);

  /* Empty the queue */
  QUEUE_reset(q);
  VBX_DEBUG(VBX_print("  QUEUE: queue @%p reset by QUEUE_annihilate\n", q));

  /* Deallocate the queue */
  q->imem->Free(q->imem, q->body);

  /* Destroy the synchronization object */
  CRITSECT_DESTROY(q->critsect);

  VBX_DEBUG(VBX_print("  QUEUE: queue @%p annihilated\n", q));

  return(TRUE);
}


static Bool
QUEUE_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a queue according to a queue attributes specification
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  IQATTR  iqattr = (IQATTR) iattr;
  Int     size;
  QUEUE   q = (QUEUE) IFACE_object((IFACE) igen);

  CRITSECT_ENTER(q->critsect);

  /* Empty the queue */
  QUEUE_reset(q);
  VBX_DEBUG(VBX_print("  QUEUE: queue @%p reset by QUEUE_initialize\n", q));

  /* Get the number of elements the queue will hold (zero is not allowed) */
  IQATTR_getSize(iqattr, &size);
  if (!size) {
    CRITSECT_LEAVE(q->critsect);
    IFACE_setError((IFACE) igen, EXC_QSIZEZERO);
    return(FALSE);
  }

  /* Provide the separation element to distinguish full and empty conditions */
  size++;

  /* If the new size is different from the old, reallocate */
  if (size != q->size) {
    QEL  newBody = (QEL) q->imem->Calloc(q->imem, size, sizeof(qel_t), OOPS_MEMORY);
    if (!newBody) {
      CRITSECT_LEAVE(q->critsect);
      IFACE_setError((IFACE) igen, EXC_OUTOFMEMORY);
      return(FALSE);
    }
    if (q->size) q->imem->Free(q->imem, q->body);
    q->body = newBody;
    q->size = size;
  }

  /* Set the tolerances */
  IQATTR_getFwdTol(iqattr, &q->fwdTol);
  IQATTR_getBwdTol(iqattr, &q->bwdTol);

  /* Make sure that tolerances are valid */
  if (q->fwdTol < 0) q->fwdTol = 0;
  if (q->bwdTol < 0) q->bwdTol = 0;

  /* Set the sink mode */
  IQATTR_getSnkMode(iqattr, &q->snkMode);

  /* Set the SNK_PASSIVE mode getFrame timeout */
  IQATTR_getTimeout(iqattr, &q->timeout);
  CRITSECT_SET_TIMESPEC(q->wait, q->timeout * 1000);

  /* Set the frameAvailable behavior */
  IQATTR_getFAQ(iqattr, &q->faq);

  /* Set up the sink interface */
  if (q->snkMode == SNK_PASSIVE) {
    ISNK_setPutFrame(q->isnk, QUEUE_putFrame);
    ISNK_setSpaceAvailable(q->isnk, QUEUE_spaceAvailable);
  } else {
    /* All sink interfaces are active by default */
    ISNK_default(q->isnk);
  }

  CRITSECT_LEAVE(q->critsect);
  return(TRUE);
}


/*** SOURCE AND SINK METHODS ***/

static Bool
QUEUE_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Import object attributes via the sink (input) interface and pass them
 * to the sink associated with the source (output) interface.  This function
 * supplements the default ISNK_setAttributes() function, which calls this
 * first and then sets the attributes of isnk if the call is successful.
 *---------------------------------------------------------------------------*/
{
  QUEUE  queue;
  ISNK   sink;

  assert(isnk);

  queue = (QUEUE) IFACE_object((IFACE) isnk);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(queue->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) isnk, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, iattr)) {
    /* If successful, set the attributes locally in both ISRC and IQUEUE */
    ISRC_setAttributes(queue->isrc, iattr);
    ISRC_setAttributes((ISRC) queue->iqueue, iattr);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
    return(FALSE);
  }

  /* Get the sink (if any) associated with the IQUEUE (output) interface */
  sink = ISRC_sink((ISRC) queue->iqueue);
  /* If there is one, pass the attributes to it */
  if (sink && !ISNK_setAttributes(sink, iattr)) {
      IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
      return(FALSE);
  }

  return(TRUE);
}


static Bool
QUEUE_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * objP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get an OBJT from the queue
 * If objP is NULL, this returns no OBJT, but it primes the queue so that a
 * subsequent request for the same frame would succeed without causing the
 * QUEUE to resort to calling getFrame() on its source.
 *---------------------------------------------------------------------------*/
{
  QUEUE     q;
  QEL       body;
  qidx_t    first, last;
  SN        delta, extremeSN, targetSN = *snP;
  ISRC      source;
  Bool      status;
  Int       exc, retval;

  assert(isrc && snP);

  q = ((QUEUE) IFACE_object((IFACE) isrc));
  body = q->body;

  /* Correct "frameAvailable" behavior is now available from QUEUE
   * if the "frameAvailableQueues" attribute is set to FALSE.
   * In this case, report on the availability of a frame from the 
   * source and get out! */
  if (!(q->faq) && objP == NULL) {
    source = ISNK_source(q->isnk);
    retval = ISRC_frameAvailable(source, FORWARD, snP);
    IFACE_setError((IFACE) q->isrc, IFACE_error((IFACE) source));
    return retval;
  }

  VBX_DEBUG(VBX_print("  QUEUE_getFrame: requesting @%ld %s of queue @%p\n", *snP, DIR_SYM(dir), q));

  CRITSECT_ENTER(q->critsect);

  /* If the queue is empty, request the object from the source */
  if (QUEUE_empty(q)) {
    status = QUEUE_getFromSource(q, ISNK_source(q->isnk), dir, snP, objP);
    CRITSECT_LEAVE(q->critsect);
    return(status);
  }

  /* If this is a request for the next SN in the specified direction ... */
  if (*snP < 0) {

    /* If the next OBJT is past the end of the queue, get it from the source */
    if (q->read == (dir == FORWARD ? q->last : q->first)) {

      /* Get the OBJT from the source */
      status = QUEUE_getFromSource(q, ISNK_source(q->isnk), dir, snP, objP);
      CRITSECT_LEAVE(q->critsect);
      return(status);

    /* Otherwise, get it from the queue */
    } else {

      /* Read out the next OBJT from the queue */
      if (dir == FORWARD) {
        QUEUE_readFwd(q, body, q->read, snP, objP);
      } else {
        QUEUE_readBwd(q, body, q->read, snP, objP);
      }
      q->snSought = *snP;
      /* Report if frames this getFrame would have gotten were overwritten */
      if (q->overwritten) {
        IFACE_setError((IFACE) q->isrc, EXC_fromErrorCount(EXC_NUMOVERWRITTEN, EXC_MANYOVERWRITTEN, q->overwritten));
        q->overwritten = 0;
        status = FALSE;
      } else {
        status = TRUE;
      }
      CRITSECT_LEAVE(q->critsect);
      return(status);
    }

  /* Otherwise, this is a request for a specific sequence number */
  } else {

    /* First look at the end of the queue */
    last = q->last;
    extremeSN = body[last].sn;
    delta = targetSN - extremeSN;

    /* If the target SN is at or past the end of the queue ... */
    if (delta >= 0) {

      /* If the target SN is the last one in the queue ... */
      if (delta == 0) {
        q->read = last;

      /* Else, if the target SN is past but near the end of the queue ... */
      } else if (delta <= q->fwdTol) {

        /* If isnk is ACTIVE ... */
        if (q->snkMode == SNK_ACTIVE) {

          /* Read FWD from the source until the targetSN is reached */
          q->read = q->last;
          QUEUE_setNextSN(q, FORWARD, &extremeSN);
          source = ISNK_source(q->isnk);
          do
            if (!QUEUE_getFromSource(q, source, FORWARD, &extremeSN, NULL) &&
                !EXC_toErrorCount(EXC_NUMOVERWRITTEN, EXC_MANYOVERWRITTEN, IFACE_error((IFACE) source))) {
              IFACE_setError((IFACE) q->isrc, IFACE_error((IFACE) source));
              CRITSECT_LEAVE(q->critsect);
              return(FALSE);
            }
          while (extremeSN < targetSN && (extremeSN = -1));
          /* Read the targetSN in the specified direction from the queue */
          if (dir == FORWARD) {
            QUEUE_reReadFwd(q, body, q->read, snP, objP);
          } else {
            if (extremeSN > targetSN)
              QUEUE_readBwd(q, body, q->read, snP, (OBJT *) NULL);
            QUEUE_readBwd(q, body, q->read, snP, objP);
          }
          q->snSought = *snP;
          CRITSECT_LEAVE(q->critsect);
          return(TRUE);

        /* Otherwise, isnk is PASSIVE.  If FORWARD, wait for data from source */
        } else if (dir == FORWARD) {
          status = QUEUE_getFromSource(q, ISNK_source(q->isnk), dir, snP, objP);
          CRITSECT_LEAVE(q->critsect);
          return(status);

        /* Otherwise, read is BACKWARD, so read the last element in the queue */
        } else {
          q->read = last;
        }

      /* Otherwise, the target SN is far past the end of the queue */
      } else {
        /* Reset the queue if request is for a BWD read */
        if (dir == BACKWARD) {
          QUEUE_reset(q);
          VBX_DEBUG(VBX_print("  QUEUE_getFrame: queue @%p reset by QUEUE_getFrame @%ld BWD (beyond end)\n", q, *snP));
        }
        /* Read SN from the source in the specified direction */
        status = QUEUE_getFromSource(q, ISNK_source(q->isnk), dir, snP, objP);
        CRITSECT_LEAVE(q->critsect);
        return(status);
      }

    /* Else, the target SN is not at the end of the queue */
    } else {

      /* Look at the beginning of the queue */
      first = q->first;
      QUEUE_incrIdx(q, first);
      extremeSN = body[first].sn;
      delta = extremeSN - targetSN;

      /* If the target SN is at or before the beginning of the queue ... */
      if (delta >= 0) {

        /* If the target SN is the first one in the queue ... */
        if (delta == 0) {
          q->read = first;

        /* Else, if it is before but near the beginning of the queue ... */
        } else if (delta <= q->bwdTol) {

          /* If isnk is ACTIVE ... */
          if (q->snkMode == SNK_ACTIVE) {

            /* Read BWD from source until targetSN is reached */
            q->read = q->first;
            QUEUE_setNextSN(q, BACKWARD, &extremeSN);
            source = ISNK_source(q->isnk);
            do
              if (!QUEUE_getFromSource(q, source, BACKWARD, &extremeSN, NULL) &&
                  !EXC_toErrorCount(EXC_NUMOVERWRITTEN, EXC_MANYOVERWRITTEN, IFACE_error((IFACE) source))) {
                IFACE_setError((IFACE) q->isrc, IFACE_error((IFACE) source));
                CRITSECT_LEAVE(q->critsect);
                return(FALSE);
              }
            while (extremeSN > targetSN && (extremeSN = -1));
            /* Read the targetSN in the specified direction from the queue */
            if (dir == BACKWARD) {
              QUEUE_reReadBwd(q, body, q->read, snP, objP);
            } else {
              if (extremeSN < targetSN)
                QUEUE_readFwd(q, body, q->read, snP, (OBJT *) NULL);
              QUEUE_readFwd(q, body, q->read, snP, objP);
            }
            q->snSought = *snP;
            CRITSECT_LEAVE(q->critsect);
            return(TRUE);

          /* Otherwise, isnk is PASSIVE.  If BACKWARD, wait for data from source */
          } else if (dir == BACKWARD) {
            status = QUEUE_getFromSource(q, ISNK_source(q->isnk), dir, snP, objP);
            CRITSECT_LEAVE(q->critsect);
            return(status);

          /* Otherwise, read is FORWARD, so read the first element in the queue */
          } else {
            q->read = first;
          }

        /* Otherwise, the target SN is far before the beginning of the queue */
        } else {
          /* Reset the queue if request is for a FWD read */
          if (dir == FORWARD) {
            QUEUE_reset(q);
            VBX_DEBUG(VBX_print("  QUEUE_getFrame: queue @%p reset by QUEUE_getFrame @%ld FWD (before start)\n", q, *snP));
          }
          /* Read SN from the source in the specified direction and return */
          status = QUEUE_getFromSource(q, ISNK_source(q->isnk), dir, snP, objP);
          CRITSECT_LEAVE(q->critsect);
          return(status);
        }

      /* Otherwise, the target SN is in the body of the queue */
      } else {
        q->read = QUEUE_findSN(q->size, body, dir, first, last, targetSN);
      }
    }

    /* Read from the queue at the target index in the specified direction */
    QUEUE_readAtIndex(q, body, q->read, dir, snP, objP);
    q->overwritten = 0;
    q->snSought = *snP;
    CRITSECT_LEAVE(q->critsect);
    return(TRUE);
  }
}


static Bool
QUEUE_getFromSource(QUEUE q, ISRC source, dir_t dir, SN * snP, OBJT * objP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * If isnk is ACTIVE, get an OBJT from the source, enqueue it, and read it
 * back out of the queue.  Otherwise, wait for the PASSIVE source's putFrame()
 * method to insert a suitable SN into the queue, then read it out.
 * THIS FUNCTION MUST ALWAYS BE CALLED FROM WITHIN THE CRITSECT.
 *---------------------------------------------------------------------------*/
{
  QEL     body = q->body;
  OBJT    srcObj;

  /* Cannot have overwritten anything not yet gotten from the source */
  q->overwritten = 0;

  /* If isnk is ACTIVE ... */
  if (q->snkMode == SNK_ACTIVE) {

    VBX_DEBUG(VBX_print("  QUEUE_getFromSource: queue @%p getting frame %ld %s via active sink\n", q, *snP, DIR_SYM(dir)));

    /* If this access is relative to an access in another direction, request OBJT by SN */
    if (*snP < 0 && q->getDir != dir) {
      QUEUE_nextSNToWrite(q, body, dir, snP);
      if (*snP < 0) *snP = dir == FORWARD ? 0 : SN_MAX;
    }

    q->getDir = dir;

    /* If the OBJT can be gotten from the source, enqueue it and read it out */
    if (ISRC_getFrame(source, dir, snP, &srcObj)) {
      VBX_DEBUG(VBX_print("  QUEUE_getFromSource: queue @%p got frame %ld %s via active sink\n", q, *snP, DIR_SYM(dir)));
      if (dir == FORWARD) {
        /* Make sure that the read index is at the end of the queue */
        q->read = q->last;
        QUEUE_writeFwd(q, body, *snP, srcObj);
        QUEUE_readFwd(q, body, q->read, snP, objP);
      } else {
        /* Make sure that the read index is at the beginning of the queue */
        q->read = q->first;
        QUEUE_writeBwd(q, body, *snP, srcObj);
        QUEUE_readBwd(q, body, q->read, snP, objP);
      }
      VBX_DEBUG(VBX_print("  QUEUE_getFromSource: queue @%p read %s @%ld \n", q, DIR_SYM(dir), *snP));
      q->snSought = *snP;
      return(TRUE);

    /* Otherwise, register the exception and pass it back */
    } else {
      IFACE_setError((IFACE) q->isrc, IFACE_error((IFACE) source));
      return(FALSE);
    }

  /* Otherwise, isnk is PASSIVE */
  } else {

    q->getDir = dir;

    /* If an OBJT is requested, wait for it */
    if (objP) {
      Int     timedout = 0;
      SN      lastSNSought = q->snSought;
      qidx_t  index;

      if (*snP < 0) {
        /* A read relative to no previous SN is senseless */
        if (q->snSought < 0) {
          IFACE_setError((IFACE) q->isrc, EXC_NOPREVIOUSSN);
          return(FALSE);
        }
        q->snSought += dir;
        if (q->snSought < 0) {
          VBX_DEBUG(VBX_print("QUEUE: snSought %ld lastSNSought %ld\n", q->snSought, lastSNSought));
          IFACE_setError((IFACE) q->isrc, EXC_SNWRAPAROUND);
          return(FALSE);
        }
        *snP = q->snSought;
      } else {
        q->snSought = *snP;
      }
      q->snFound = FALSE;

      CRITSECT_LEAVE(q->critsect);
      if (q->timeout) {
        CRITSECT_TIMEDWAIT_ENTER(q->critsect, &q->wait, timedout, !q->snFound);
      } else {
        CRITSECT_WAIT_ENTER(q->critsect, !q->snFound);
      }

      /* If a suitable object can be found (before timing out), get it */
      if (q->snFound) {
        if (dir == FORWARD) {
          /* Search backward for the SN sought */
          index = q->last;
          /* Find index of last SN less than or equal to SN sought */
          while (index != q->first && q->snSought < body[index].sn)
            QUEUE_decrIdx(q, index);
          /* If less than, or if none, get following one */
          if (index == q->first || body[index].sn < q->snSought)
            QUEUE_incrIdx(q, index);
        } else {
          /* Search forward for the SN sought */
          index = q->first;
          /* Find index of the first SN greater than or equal to SN sought */
          do { QUEUE_incrIdx(q, index) }
          while (body[index].sn < q->snSought && index != q->last);
          /* If greater than, get the preceding one */
          if (q->snSought < body[index].sn)
            QUEUE_decrIdx(q, index);
        }
        q->read = index;
        QUEUE_readAtIndex(q, body, q->read, dir, snP, objP);
        q->overwritten = 0;
        q->snSought = *snP;
        q->snFound = FALSE;
        return(TRUE);
      /* Otherwise, restore the last SN sought, and report the reason for failure */
      } else {
        q->snSought = lastSNSought;
        if (timedout == CRITSECT_TIMEDOUT)
          IFACE_setError((IFACE) q->isrc, EXC_TIMEDOUT);
        else if (timedout == CRITSECT_NOMEMORY)
          IFACE_setError((IFACE) q->isrc, EXC_OUTOFMEMORY);
        else
          IFACE_setError((IFACE) q->isrc, EXC_BADTIMEOUTSPEC);
        return(FALSE);
      }
    /* Otherwise, report that it is not available */
    } else {
      IFACE_setError((IFACE) q->isrc, EXC_NOTINQUEUE);
      return(FALSE);
    }
  }
}


static qidx_t
QUEUE_findSN(Int size, QEL body, dir_t dir,
             qidx_t first, qidx_t last, SN targetSN)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Given the indices of the first and last valid elements in the queue, this
 * function performs a straightforward binary search and returns the index
 * of the element whose SN is nearest to the requested target SN in the
 * specified direction.
 *---------------------------------------------------------------------------*/
{
  Int     range;
  qidx_t  middle;
  SN      middleSN;

  /* Need these if not tested before calling */
  /* if (targetSN == body[first].sn) return(first); */
  /* if (targetSN == body[last].sn) return(last); */

  while (TRUE) {
    range = last - first;
    if (range < 0) range += size;
    if (range >>= 1) {
      middle = last - range;
      if (middle < 0) middle += size;
      middleSN = body[middle].sn;
      if (targetSN > middleSN) first = middle;
      else if ( targetSN < middleSN) last = middle;
      else return(middle);
    } else {
      if (dir == FORWARD) return(last);
      else return(first);
    }
  }
}


static Bool
QUEUE_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports the availability of a frame via the source interface
 *---------------------------------------------------------------------------*/
{
  return(QUEUE_getFrame(isrc, dir, snP, NULL));
}


static Bool
QUEUE_putFrame(ISNK isnk, dir_t dir, SN * snP, OBJT obj)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Put an OBJT into the queue, reporting the SN used, which will always be
 * the same as the given *snP if *snP >= 0.  If *snP < 0, the next SN in the
 * specified direction will be used and reported.  The method behaves the
 * same if obj is NULL, except that nothing is queued.  THIS METHOD DOES
 * NOT BLOCK; WHEN THE QUEUE IS FULL, IT OVERWRITES.  However, it will not
 * write into the currently valid range of sequence numbers, nor will it
 * write forward (backward) from the beginning (end) of the queue.
 *---------------------------------------------------------------------------*/
{
  QUEUE     q;
  QEL       body;
  SN        delta, nextSN;

  assert(isnk && snP);

  q = ((QUEUE) IFACE_object((IFACE) isnk));
  body = q->body;

  CRITSECT_ENTER(q->critsect);

  VBX_DEBUG(VBX_print("  QUEUE_putFrame(dir = %d, sn = %ld, obj = %p)\n", dir, *snP, obj));

  /* If the queue is empty ... */
  if (QUEUE_empty(q)) {

    /* A write relative to a previous SN is senseless */
    if (*snP < 0) {
      IFACE_setError((IFACE) isnk, EXC_NOPREVIOUSSN);
      CRITSECT_LEAVE(q->critsect);
      return(FALSE);

    /* Otherwise, insert the object in the specified direction */
    } else {
      return(QUEUE_insertObject(q, dir, *snP, obj));
    }
  }

  /* Get the next available SN in the specified direction */
  QUEUE_nextSNToWrite(q, body, dir, &nextSN);
  if (nextSN < 0) nextSN = dir == FORWARD ? 0 : SN_MAX;

  /* If this is a request to use the next SN in the specified direction ... */
  if (*snP < 0) {

    *snP = nextSN;
    return(QUEUE_insertObject(q, dir, *snP, obj));

  /* Otherwise, this is a request to insert at a specific sequence number */
  } else {

    /* Compute the distance to the specified SN */
    delta = dir * (*snP - nextSN);

    /* If the target SN is past the appropriate end of the queue ... */
    if (delta >= 0) {

      /* Insert the object in the specified direction */
      return(QUEUE_insertObject(q, dir, *snP, obj));

    /* Otherwise, the target SN is in the wrong direction*/
    } else {
      IFACE_setError((IFACE) q->isnk, EXC_CANTPUTSN);
      VBX_DEBUG(VBX_print("QUEUE: queue @%p got EXC_CANTPUTSN in QUEUE_putFrame (reqSN = %ld; nextSN = %ld; delta = %ld)\n", q, *snP, nextSN, delta));
      CRITSECT_LEAVE(q->critsect);
      return(FALSE);
    }
  }
}


static Bool
QUEUE_insertObject(QUEUE q, dir_t dir, SN sn, OBJT obj)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Enqueue an OBJT in the specified direction with the specified SN
 * CALL THIS FUNCTION ONLY FROM WITHIN THE CRITSECT
 *---------------------------------------------------------------------------*/
{
  QEL   body = q->body;

  if (obj) {
    if (dir == FORWARD) {
      QUEUE_writeFwd(q, body, sn, obj);
    } else {
      QUEUE_writeBwd(q, body, sn, obj);
    }
    /* Signal if this satisfies a pending read requirement */
    if (q->snFound = ((q->getDir) * (sn - q->snSought) >= 0))
      CRITSECT_SIGNAL(q->critsect);
  }
  CRITSECT_LEAVE(q->critsect);

  return(TRUE);
}


static Bool
QUEUE_spaceAvailable(ISNK isnk, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Queries the availability of space to insert a frame via the sink interface
 *---------------------------------------------------------------------------*/
{
  return(QUEUE_putFrame(isnk, dir, snP, NULL));
}


/*** QATTR Methods ***/

IGEN
QATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a queue attributes object
 *---------------------------------------------------------------------------*/
{
  IGEN    igen = IGEN_create(imem, NULL);
  IQATTR  iqattr = IQATTR_create(igen);

  /* The queue's sink (input) is ACTIVE by default */
  IQATTR_setSnkMode(iqattr, SNK_ACTIVE);

  /* By default, there is no SNK_PASSIVE getFrame timeout */
  IQATTR_setTimeout(iqattr, 0);

  /* Default behavior: frameAvailable gets/queues a frame */
  IQATTR_setFAQ(iqattr, TRUE);

  return(igen);
}


/*** IQATTR METHODS ***/

static IQATTR
IQATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IQATTR interface
 *---------------------------------------------------------------------------*/
{
  IQATTR  iqattr;

  if ((iqattr = (IQATTR) IFACE_calloc((IFACE) igen, 1, sizeof(iqattr_t)))) {
    IFACE_setClass((IFACE) iqattr, &IQATTR_Class);
    IFACE_setObject((IFACE) iqattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iqattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iqattr);
  }

  return(iqattr);
}
