/* objattr.c - generic object attributes interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 29-Oct-96  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: objattr.c[1.1] Tue Jun  9 17:46:16 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"


/* The Class of OBJATTR (Frame Attributes) Interface Objects */
static iclass_t IOBJATTR_Class = {IOBJATTRID};

/* Private functions */

static Bool  IOBJATTR_defaultDeconstructor(IOBJATTR iobjattr, OBJT objt, Char * buff, UInt buffLen, UInt * buffReq);
static Bool  IOBJATTR_defaultReconstructor(IOBJATTR iobjattr, OBJT objt, Char * buff, UInt buffLen, UInt * buffReq);

/*** IOBJATTR Methods ***/

IOBJATTR
IOBJATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IOBJATTR interface
 *---------------------------------------------------------------------------*/
{
  IOBJATTR  iobjattr;

  if ((iobjattr = (IOBJATTR) IFACE_calloc((IFACE) igen, 1, sizeof(iobjattr_t)))) {

    IFACE_setClass((IFACE) iobjattr, &IOBJATTR_Class);
    IFACE_setObject((IFACE) iobjattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iobjattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iobjattr);

    IOBJATTR_setDeconstructor(iobjattr, IOBJATTR_defaultDeconstructor);
    IOBJATTR_setReconstructor(iobjattr, IOBJATTR_defaultReconstructor);

  }

  return(iobjattr);
}


static Bool
IOBJATTR_defaultDeconstructor(IOBJATTR iobjattr, OBJT objt, Char * buff, UInt buffLen, UInt * buffReq)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * The default deconstructor for an object -- copies the contents of the
 * object into an array of bytes.
 *---------------------------------------------------------------------------*/
{
  UInt objtSize = OBJT_size(objt);

  /* Report the number of bytes required to hold the resulting array */
  *buffReq = objtSize;

  /* If there is room, copy the contents of objt into buff and return TRUE */
  if (objtSize <= buffLen) {
    memcpy(buff, &OBJT_contents(objt), objtSize);
    return(TRUE);

  /* Otherwise, return FALSE */
  } else {
    return(FALSE);
  }
}


static Bool
IOBJATTR_defaultReconstructor(IOBJATTR iobjattr, OBJT objt, Char * buff, UInt buffLen, UInt * buffReq)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * The default reconstructor for an object -- copies an array of bytes into
 * the contents of the object.
 *---------------------------------------------------------------------------*/
{
  UInt objtSize = OBJT_size(objt);

  /* Report the number of bytes required to reconstruct the obect */
  *buffReq = objtSize;

  /* If there are enough, copy the contents of buff into objt and return TRUE */
  if (objtSize <= buffLen) {
    memcpy(&OBJT_contents(objt), buff, objtSize);
    return(TRUE);

  /* Otherwise, return FALSE */
  } else {
    return(FALSE);
  }
}
