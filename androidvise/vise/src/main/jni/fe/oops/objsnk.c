/* objsnk.c - a frame absorber
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 25-Apr-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: objsnk.c[1.0] Tue Jun  9 17:48:20 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "objsnk.h"
#include "class.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include <assert.h>

#define AGC_PRINT_PRIO VBX_MID_PRIO

/* The Class of OBJSINK Interface Objects */
static iclass_t IOBJSINK_Class = {IOBJSINKID};

/* The OBJSINK Interface */
typedef struct iobjsink_s {
  iface_t  iface;
} iobjsink_t;


/* An OBJSINK descriptor */
typedef struct objsink_s {
  ISNK  isnk;               /* OBJSINK's sink interface (input) */
} objsink_t, * OBJSINK;


/* Private functions */

static IOBJSINK  IOBJSINK_create(IGEN igen);


/*** OBJSINK Methods ***/

IGEN
OBJSINK_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a frame absorber
 *---------------------------------------------------------------------------*/
{
  OBJSINK  objsink = (OBJSINK) imem->Calloc(imem, 1, sizeof(objsink_t), OOPS_MEMORY);
  IGEN     igen = IGEN_create(imem, (OBJ) objsink);
  ISNK     isnk = ISNK_create(igen);
  IOBJSINK iobjsink = IOBJSINK_create(igen);
  IATTR    iattr;

  /* Initialize the object */
  objsink->isnk = isnk;

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  return(igen);
}


/*** IOBJSINK METHODS ***/

static IOBJSINK
IOBJSINK_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IOBJSINK interface
 *---------------------------------------------------------------------------*/
{
  IOBJSINK  iobjsink;

  if ((iobjsink = (IOBJSINK) IFACE_calloc((IFACE) igen, 1, sizeof(iobjsink_t)))) {
    IFACE_setClass((IFACE) iobjsink, &IOBJSINK_Class);
    IFACE_setObject((IFACE) iobjsink, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) iobjsink, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) iobjsink);
  }

  return(iobjsink);
}


Bool
IOBJSINK_getAttributes(IOBJSINK iobjsink, IATTR * iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 *---------------------------------------------------------------------------*/
{
  OBJSINK  objsink;

  assert(iobjsink && iattr);

  objsink = (OBJSINK) IFACE_object((IFACE) iobjsink);

  /* Get the attributes directly from the sink (input) interface */
  *iattr = ISNK_attributes(objsink->isnk);

  return(TRUE);
}


Bool
IOBJSINK_getFrame(IOBJSINK iobjsink, dir_t dir, SN * snP, OBJT * fP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Gets a frame OBJT
 *---------------------------------------------------------------------------*/
{
  OBJSINK   objsink;
  ISRC      src;
  
  assert(iobjsink && snP);

  objsink = ((OBJSINK) IFACE_object((IFACE) iobjsink));

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(objsink->isnk);
  /* If there is none, register an exception */
  if (!src) {
	  VBX_print("IOBJSINK_getFrame: No src!!!");
    IFACE_setError((IFACE) iobjsink, EXC_NOSRC);
    return(FALSE);
  }

  /* Obtain the requested frame from the source */
  if (ISRC_getFrame(src, dir, snP, fP)) {
    /* If the frame is available, pass it on out */
    return(TRUE);
  } else {
	  VBX_print("IOBJSINK_getFrame: Failed to get frame, error = %d", IFACE_error((IFACE)src));
    /* Otherwise, register whatever exception the source yields */
    IFACE_setError((IFACE) iobjsink, IFACE_error((IFACE) src));
    return(FALSE);
  }
}


Bool
IOBJSINK_frameAvailable(IOBJSINK iobjsink, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reports the availability of a frame object
 *---------------------------------------------------------------------------*/
{
  return(IOBJSINK_getFrame(iobjsink, dir, snP, NULL));
}
