/* split.c - a stream splitter
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 *  5-Jun-95  Dave Vetter (dvetter@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: split.c[1.1] Tue Jun  9 17:50:39 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "split.h"
#include "sn.h"
#include <assert.h>


/* The Class of SPLIT Interface Objects */
static iclass_t ISPLIT_Class = {ISPLITID};

/* The SPLIT Interface */
typedef struct isplit_s {
  isrc_t   isrc;
} isplit_t;

/* A SPLIT descriptor */
typedef struct split_s {
  ISRC     isrc;           /* SPLIT's source interface (output) */
  ISNK     isnk;           /* SPLIT's sink interface (input) */
  ISPLIT   isplit;         /* SPLIT's external interface (output) */
  EXC      exc;            /* The exception (if any) reported via isrc */
  SN       seqNum;         /* The SN of the OBJT last exported via isrc */
  OBJT     objt;           /* The last OBJT exported via isrc */
} split_t, * SPLIT;


/* Private functions */

static ISPLIT  ISPLIT_create(IGEN igen);

static Bool    ISPLIT_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool    ISPLIT_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static Bool    SPLIT_annihilate(IGEN igen);
static Bool    SPLIT_initialize(IGEN igen, IATTR iattr);
static Bool    SPLIT_setAttributes(ISNK isnk, IATTR iattr);
static Bool    SPLIT_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool    SPLIT_frameAvailable(ISRC isrc, dir_t dir, SN * snP);


/*** SPLIT Methods ***/

IGEN
SPLIT_create(IMemory * imem, ISRC src)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a split
 *---------------------------------------------------------------------------*/
{
  SPLIT   split = (SPLIT) imem->Calloc(imem, 1, sizeof(split_t), OOPS_MEMORY);
  IGEN    igen = IGEN_create(imem, (OBJ) split);
  ISNK    isnk = ISNK_create(igen);
  ISRC    isrc = ISRC_create(igen);
  ISPLIT  isplit = ISPLIT_create(igen);
  IATTR   iattr;

  /* Initialize the object */
  split->isnk = isnk;
  split->isrc = isrc;
  split->isplit = isplit;
  split->exc = EXC_NOPREVIOUSSN;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, SPLIT_annihilate);
  IGEN_setInitializer(igen, SPLIT_initialize);

  /* Use the specified source, if any */
  if (src) {
    ISRC_getAttributes(src, &iattr);
    ISNK_setAttributes(isnk, iattr);
    ISRC_setAttributes(isrc, iattr);
    ISRC_setAttributes((ISRC) isplit, iattr);
    ISRC_setSink(src, isnk);
    ISNK_setSource(isnk, src);
  }

  /* Set up the sink interface */
  ISNK_setSetAttributes(isnk, SPLIT_setAttributes);

  /* Set up the source interface */
  ISRC_setGetFrame(isrc, SPLIT_getFrame);
  ISRC_setFrameAvailable(isrc, SPLIT_frameAvailable);

  /* Set up the split interface */
  ISRC_setGetFrame((ISRC) isplit, ISPLIT_getFrame);
  ISRC_setFrameAvailable((ISRC) isplit, ISPLIT_frameAvailable);

  return(igen);
}


static Bool
ISPLIT_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * objP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get a frame OBJT from the split
 * This implements a second ISRC interface for SPLIT, one which permits
 * reading the last OBJT to pass through SPLIT, but which does not provoke
 * getFrame calls to SPLIT's source.
 *---------------------------------------------------------------------------*/
{
  SPLIT     split;
  ISPLIT    isplit;

  assert(isrc && snP);
  isplit = (ISPLIT) isrc;

  split = ((SPLIT) IFACE_object((IFACE) isrc));

  /* Report any existing exception */
  if (split->exc != EXC_NONE) {
    IFACE_setError((IFACE) isrc, split->exc);
    return(FALSE);
  }

  /* If any SN will do, get the one that's there */
  if (*snP < 0) {
    *snP = split->seqNum;
  /* Else, report an exception if the one that's there isn't the one requested */
  } else if (*snP != split->seqNum) {
    /* The desired SN is not available */
    IFACE_setError((IFACE) isrc, EXC_CANTGETSN);
    return(FALSE);
  }

  /* Get the frame OBJT if requested */
  if (objP) {
    /* If an OBJT is available, get it */
    if (split->objt) {
      /* The OBJT is available, so copy it */
      *objP = OBJT_copy(split->objt);
    /* Otherwise, report an error */
    } else {
      *objP = NULL;
      IFACE_setError((IFACE) isrc, EXC_NOOBJT);
      return(FALSE);
    }
  }

  return(TRUE);
}


static Bool
ISPLIT_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports the availability of a frame via the source interface
 *---------------------------------------------------------------------------*/
{
  return(ISPLIT_getFrame(isrc, dir, snP, NULL));
}


/*** ISPLIT METHODS ***/

static ISPLIT
ISPLIT_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an ISPLIT interface
 *---------------------------------------------------------------------------*/
{
  ISPLIT  isplit;

  if ((isplit = (ISPLIT) IFACE_calloc((IFACE) igen, 1, sizeof(isplit_t)))) {
    IFACE_setClass((IFACE) isplit, &ISPLIT_Class);
    IFACE_setObject((IFACE) isplit, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) isplit, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) isplit);
  }

  return(isplit);
}


/*** GENERIC METHODS ***/

static Bool
SPLIT_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a SPLIT
 * This supplements the generic IGEN_annihilate() by freeing up the OBJT
 * (if any) left in the SPLIT
 *---------------------------------------------------------------------------*/
{
  SPLIT  split = (SPLIT) IFACE_object((IFACE) igen);

  if (split->objt) OBJT_free(split->objt);

  return(TRUE);
}


static Bool
SPLIT_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize a SPLIT (iattr is ignored)
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 *---------------------------------------------------------------------------*/
{
  SPLIT  split = (SPLIT) IFACE_object((IFACE) igen);

  split->exc = EXC_NOPREVIOUSSN;
  split->objt = NULL;

  return(TRUE);
}


/*** SOURCE AND SINK METHODS ***/

static Bool
SPLIT_setAttributes(ISNK isnk, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Import object attributes via the sink (input) interface and pass them
 * to the sink associated with the source (output) interface.  This function
 * supplements the default ISNK_setAttributes() function, which calls this
 * first and then sets the attributes of isnk if the call is successful.
 *---------------------------------------------------------------------------*/
{
  SPLIT  split;
  ISNK   sink;

  assert(isnk);

  split = (SPLIT) IFACE_object((IFACE) isnk);

  /* Get the sink associated with the source (output) interface */
  sink = ISRC_sink(split->isrc);
  /* If there is none, register an exception */
  if (!sink) {
    IFACE_setError((IFACE) isnk, EXC_NOSNK);
    return(FALSE);
  }

  /* Pass the attributes on to the sink, if possible */
  if (ISNK_setAttributes(sink, iattr)) {
    /* If successful, set the attributes locally in both ISRC and ISPLIT */
    ISRC_setAttributes(split->isrc, iattr);
    ISRC_setAttributes((ISRC) split->isplit, iattr);
  } else {
    /* Otherwise, register the exception and return FALSE */
    IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
    return(FALSE);
  }

  /* Get the sink (if any) associated with the ISPLIT (output) interface */
  sink = ISRC_sink((ISRC) split->isplit);
  /* If there is one, pass the attributes to it */
  if (sink && !ISNK_setAttributes(sink, iattr)) {
      IFACE_setError((IFACE) isnk, IFACE_error((IFACE) sink));
      return(FALSE);
  }

  return(TRUE);
}


static Bool
SPLIT_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * objP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get an OBJT from the split
 *---------------------------------------------------------------------------*/
{
  SPLIT     split;
  ISRC      src;

  assert(isrc && snP);
  split = ((SPLIT) IFACE_object((IFACE) isrc));

  /* Get the source associated with the sink (input) interface */
  src = ISNK_source(split->isnk);
  /* If there is none, register an exception */
  if (!src) {
    IFACE_setError((IFACE) isrc, EXC_NOSRC);
    return(FALSE);
  }

  /* Free any OBJT in the SPLIT */
  if (split->objt) {
    OBJT_free(split->objt);
    split->objt = NULL;
  }

  /* Try to obtain the requested frame from the source */
  if (ISRC_getFrame(src, dir, snP, objP)) {
    /* If the source yields a valid frame OBJT, copy it into the SPLIT */
    if (objP) split->objt = OBJT_copy(*objP);
    split->seqNum = *snP;
    split->exc = EXC_NONE;
  } else {
    /* Otherwise, just pass along whatever exception the source yields */
    split->exc = IFACE_error((IFACE) src);
    IFACE_setError((IFACE) isrc, split->exc);
    return(FALSE);
  }

  return(TRUE);
}


static Bool
SPLIT_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports the availability of a frame via the source interface
 *---------------------------------------------------------------------------*/
{
  return(SPLIT_getFrame(isrc, dir, snP, NULL));
}
