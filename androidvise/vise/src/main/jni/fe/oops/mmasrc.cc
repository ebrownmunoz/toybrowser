/* mmasrc.cc - MMAudio interface to an OOPS chain head
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * HISTORY
 * 02-Nov-96  Craig Vanderborgh (craigv@rad.verbex.com)
 *      Created
 *---------------------------------------------------------------------------*/
static char AtFSid[]= "$Header: mmasrc.cc[1.1] Tue Jun  9 17:45:13 1998 dvetter@titan saved $";

#include "pthr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vbxcom.hh"
#include "mmasrc.hh"
#include "types.h"
#include "c.h"
#include "class.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"
#include "isnk.h"
#include "isrc.h"
#include "sn.h"
#include "vbx.h"
#include <assert.h>

#define VBX_DEBUG(P)

#define DFLT_CLASSSIZE  2
#define DFLT_FRAMELEN   128
#define DFLT_SAMPBYTES  2

/* MMASRCATTR (Multimedia Audio Source Attributes) Interface Objects */
static iclass_t IMMASRCATTR_Class = {IMMASRCATTRID};

/* The Class of IMMASRC Interface Objects */
static iclass_t IMMASRC_Class = {IMMASRCID};

/* The MMASRC Interface */
typedef struct immasrc_s {
  iface_t  iface;
} immasrc_t;

/* MMASRC Attributes */
typedef struct mmasrcattr_s {
  Int           classSize;    /* Size of the CLASS of audio frame objects */
  Int           frameLen;     /* Number of samples in audio frame (Shorts) */
  Int           sampBytes;    /* Number of bytes in a sample (for future) */
  LPUNKNOWN     audioUnknown; /* Micro$oft MMAudio unknown */
} mmasrcattr_t, *MMASRCATTR;

/* A MMASRC descriptor */
typedef struct mmasrc_s {
  IMemory     * imem;         /* The memory allocator */
  ISRC          isrc;         /* MMASRC's (audio) source interface (output) */
  IGEN          igenattr;     /* Interface to the audio attributes block */
  Int           classSize;    /* The size of the CLASS of audio frame OBJTs */
  CLASS         audioClass;   /* The CLASS of audio frame OBJTs */
  Int           frameLen;     /* Number of samples in an audio frame */
  Int           sampBytes;    /* Number of bytes in a sample (for future) */
  LPUNKNOWN     audioUnknown; /* Micro$oft MMAudio unknown */
  IAudio       *iAudioPtr;    /* Micro$oft Audio Interfaces (e.g. DataGet())*/
  IAudioSource *iAudioSourcePtr;
} mmasrc_t, * MMASRC;

/* Private functions */

static IMMASRC IMMASRC_create(IGEN igen);

static Bool  MMASRC_annihilate(IGEN igen);
static Bool  MMASRC_initialize(IGEN igen, IATTR iattr);
static Bool  MMASRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP);
static Bool  MMASRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

static IMMASRCATTR  IMMASRCATTR_create(IGEN igen);

/*** MMASRC Methods ***/

IGEN
MMASRC_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an MMASRC object
 *---------------------------------------------------------------------------*/
{
  MMASRC     mmasrc = (MMASRC) imem->Calloc(imem, 1, sizeof(mmasrc_t), OOPS_MEMORY);
  IGEN       igen = IGEN_create(imem, (OBJ) mmasrc);
  ISRC       isrc = ISRC_create(igen);
  IGEN       igenattr = AUDIOATTR_create(imem);

  /* Initialize the object */
  mmasrc->imem = imem;
  mmasrc->isrc = isrc;
  mmasrc->igenattr = igenattr;

  /* Set up the generic interface */
  IGEN_setAnnihilator(igen, MMASRC_annihilate);
  IGEN_setInitializer(igen, MMASRC_initialize);

  /* Set up the source (output) interface */
  ISRC_setGetFrame(isrc, MMASRC_getFrame);
  ISRC_setFrameAvailable(isrc, MMASRC_frameAvailable);
  ISRC_setAttributes(isrc, (IATTR) IGEN_interface(igenattr, IAUDIOATTRID));

  return(igen);
}

/*** MMASRC METHODS ***/

static Bool
MMASRC_annihilate(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Annihilate a mmasrc object
 * This supplements the generic IGEN_annihilate() by annihilating the CLASS
 * of cepstral frame OBJTs and the cepstral attributes block
 *---------------------------------------------------------------------------*/
{
  MMASRC mmasrc = (MMASRC) IFACE_object((IFACE) igen);

  /* Annihilate the CLASS of cepstral frame OBJTs */
  if (mmasrc->audioClass && !CLASS_destroy(mmasrc->audioClass, NULL, NULL)) {
    IFACE_setError((IFACE) igen, EXC_CANTFREECLASS);
    return(FALSE);
  }

  /* Release the audio source interfaces */
  mmasrc->iAudioPtr->Release();
  mmasrc->iAudioSourcePtr->Release();

  /* Annihilate the cepstral attributes object */
  if (!IGEN_annihilate(mmasrc->igenattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) mmasrc->igenattr));
    return(FALSE);
  }

  return(TRUE);
}

static Bool
MMASRC_initialize(IGEN igen, IATTR iattr)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize an MMASRC according to an MMASRCATTR attributes specification.
 * Like all IGEN_initialize() functions, this may be called repeatedly with
 * different IATTRs.
 * NOTE: The initial implementation supports 16-bit linear encoding ONLY!
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) iattr);
  MMASRC      mmasrc = (MMASRC) IFACE_object((IFACE) igen);
  IAUDIOATTR  iaudioattr;
  Int         oldFrameLen;

  /* Get the audio unknown and the interfaces we need from it */
  mmasrc->audioUnknown = mmasrcattr->audioUnknown;
  assert(mmasrc->audioUnknown != NULL);
  VBX_fatalIf(mmasrc->audioUnknown->QueryInterface(IID_IAudio, (PVOID *) &mmasrc->iAudioPtr));
  VBX_fatalIf(mmasrc->audioUnknown->QueryInterface(IID_IAudioSource, (PVOID *) &mmasrc->iAudioSourcePtr));

  /* Get the frame length and sample size for the audio frames */
  mmasrc->frameLen = mmasrcattr->frameLen;
  mmasrc->sampBytes = mmasrcattr->sampBytes;

  /* Get the frameLen in the audio attributes block (in ISRC) */
  if (!ISRC_getAttributes(mmasrc->isrc, (IATTR *) &iaudioattr)) {
    IFACE_setError((IFACE) igen, IFACE_error((IFACE) mmasrc->isrc));
    return(FALSE);
  }
  IAUDIOATTR_getFrameLen(iaudioattr, &oldFrameLen);

  /* If the frameLen or the classSize has changed, re-create the CLASS */
  if (mmasrc->frameLen != oldFrameLen || mmasrcattr->classSize != mmasrc->classSize) {
    if (mmasrc->audioClass)
      CLASS_destroy(mmasrc->audioClass, NULL, NULL);
    mmasrc->audioClass =
      CLASS_create(mmasrc->imem, mmasrcattr->classSize, mmasrc->frameLen * mmasrc->sampBytes, NULL, NULL);
    mmasrc->classSize = mmasrcattr->classSize;
  }

  /* If frameLen has changed, promulgate the change to the sink */
  if (oldFrameLen != mmasrc->frameLen) {
    IAUDIOATTR_setFrameLen(iaudioattr, mmasrc->frameLen);

#if 0
    /* Get the sink associated with the source (output) interface */
    sink = ISRC_sink(mmasrc->isrc);
    /* If there is none, register an exception */
    if (!sink) {
      IFACE_setError((IFACE) igen, EXC_NOSNK);
      return(FALSE);
    }
    /* Pass the audio attributes on to the sink, if possible */
    if (ISNK_setAttributes(sink, (IATTR) iaudioattr)) {
      /* If successfully passed, return TRUE */
      return(TRUE);
    } else {
      /* Otherwise, register the exception and return FALSE */
      IFACE_setError((IFACE) igen, IFACE_error((IFACE) sink));
      return(FALSE);
    }
#endif
  }
  return(TRUE);
}

/*** SOURCE METHODS ***/

static Bool
MMASRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * outP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports an audio frame from an MMAudio source via the source (output)
 * interface
 * NOTE:     The intention is that no frame queueing is to occur here
 * BEHAVIOR: Depends on the implementation of the audio source itself, but
 *           it is generally expected that this routine will BLOCK if 
 *           there is insufficient data available to produce a frame.
 *---------------------------------------------------------------------------*/
{
  MMASRC    mmasrc;
  SN        nextSN;
  ULong     bytesCopied;
  ULong     bytesAvail;
  Bool      eof;
  HRESULT   hresult;

  assert(isrc != NULL && snP != NULL);
  mmasrc = ((MMASRC) IFACE_object((IFACE) isrc));
  assert(mmasrc != NULL);

  /* Frame retrieval is only permitted in the FORWARD direction */
  if (dir != FORWARD) {
    IFACE_setError((IFACE) isrc, EXC_NOBACKWARD);
    return(FALSE);
  }

  /* Only the next SN is available from the audio source */
  if (hresult = mmasrc->iAudioPtr->TotalGet((PQWORD) &nextSN))
    SEVERE_BRA_ "  MMASRC::TotalGet failed (hresult = %8.8x)", hresult _KET;
  if (*snP < 0) {
    *snP = nextSN;
    VBX_DEBUG(VBX_print("  MMASRC: sn = %ld\n", *snP)); // DEBUG
  } else if (nextSN != *snP) {
    IFACE_setError((IFACE) isrc, EXC_CANTGETSN);
    return(FALSE);
  }

  /* Try to get a frame if requested */
  if (outP) {
   *outP = OBJT_new(mmasrc->audioClass);

    /* If an OBJT is available, fill it with the sample data */
    if (*outP) {
      if (hresult = mmasrc->iAudioSourcePtr->DataGet((PVOID) &OBJT_contents(*outP), mmasrc->frameLen * mmasrc->sampBytes, &bytesCopied))
        SEVERE_BRA_ "  MMASRC::DataGet failed (hresult = %8.8x)", hresult _KET;
      VBX_DEBUG(VBX_print("  MMASRC: got %d of %d bytes requested at sn = %ld\n", bytesCopied, mmasrc->frameLen * mmasrc->sampBytes, *snP));
      if ((mmasrc->frameLen * mmasrc->sampBytes) != (Int) bytesCopied) {
        /* CHANGE THIS TO CACHE AND CONCATENATE, BUT STILL RETURN THIS EXC */
        IFACE_setError((IFACE) isrc, EXC_TOOFEWSAMPLES);
        return(FALSE);
      }
    } else {                            /* Otherwise, report an exception */
      IFACE_setError((IFACE) isrc, EXC_AUDIOCLASSEMPTY);
      return(FALSE);
    }

  /* Otherwise, just see if one could be gotten */
  } else {
    if (hresult = mmasrc->iAudioSourcePtr->DataAvailable(&bytesAvail, &eof))
      SEVERE_BRA_ "  MMASRC::DataAvailable failed (hresult = %8.8x)", hresult _KET;
    VBX_DEBUG(VBX_print("  MMASRC: %d bytes available; %d bytes needed; eof = %d\n", bytesAvail, mmasrc->frameLen * mmasrc->sampBytes, eof));
    if (bytesAvail < (ULong) mmasrc->frameLen * mmasrc->sampBytes) {
      IFACE_setError((IFACE) isrc, eof ? EXC_EOU : EXC_TOOFEWSAMPLES);
      VBX_DEBUG(VBX_print("  MMASRC: setting %s\n", eof ? "EXC_EOU" : "EXC_TOOFEWSAMPLES"));
      return(FALSE);
    }
  }

  IFACE_setError((IFACE) isrc, EXC_NONE);
  return(TRUE);
}

static Bool
MMASRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Exports availability of an audio frame via the source (output) interface
 * Requested sequence number is ignored.
 *---------------------------------------------------------------------------*/
{
  return(MMASRC_getFrame(isrc, dir, snP, NULL));
}

/*** MMASRCATTR Methods ***/

IGEN
MMASRCATTR_create(IMemory * imem)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a MMASRC attributes object
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR   mmasrcattr = (MMASRCATTR) imem->Calloc(imem, 1, sizeof(mmasrcattr_t), OOPS_MEMORY);
  IGEN         igen = IGEN_create(imem, (OBJ) mmasrcattr);
  IMMASRCATTR  immasrcattr = IMMASRCATTR_create(igen);

  /* Set the default attributes */
  mmasrcattr->classSize = DFLT_CLASSSIZE;
  mmasrcattr->frameLen = DFLT_FRAMELEN;
  mmasrcattr->sampBytes = DFLT_SAMPBYTES;

  return(igen);
}


/*** IMMASRC METHODS ***/

static IMMASRC
IMMASRC_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IOBJSOURCE interface on the emitter
 *---------------------------------------------------------------------------*/
{
  IMMASRC  immasrc;

  if ((immasrc = (IMMASRC) IFACE_calloc((IFACE) igen, 1, sizeof(immasrc_t)))) {
    IFACE_setClass((IFACE) immasrc, &IMMASRC_Class);
    IFACE_setObject((IFACE) immasrc, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) immasrc, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) immasrc);
  }

  return(immasrc);
}

/*** IMMASRCATTR METHODS ***/

static IMMASRCATTR
IMMASRCATTR_create(IGEN igen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create an IMMASRCATTR interface
 *---------------------------------------------------------------------------*/
{
  IMMASRCATTR  immasrcattr;

  if ((immasrcattr = (IMMASRCATTR) IFACE_calloc((IFACE) igen, 1, sizeof(immasrcattr_t)))) {
    IFACE_setClass((IFACE) immasrcattr, &IMMASRCATTR_Class);
    IFACE_setObject((IFACE) immasrcattr, IFACE_object((IFACE) igen));
    IFACE_setAllocator((IFACE) immasrcattr, IFACE_allocator((IFACE) igen));
    IFACE_addSibling((IFACE) igen, (IFACE) immasrcattr);
  }

  return(immasrcattr);
}

Bool
IMMASRCATTR_setAudioUnknown(IMMASRCATTR immasrcattr, Ptr audioUnknown)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr;

  assert(immasrcattr != NULL && audioUnknown != NULL);
  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) immasrcattr);

  mmasrcattr->audioUnknown = (LPUNKNOWN) audioUnknown;
  return(TRUE);
}

Bool
IMMASRCATTR_setClassSize(IMMASRCATTR immasrcattr, Int classSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of the class of cepstral frames
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr;

  assert(immasrcattr != NULL);
  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) immasrcattr);

  mmasrcattr->classSize = classSize;

  return(TRUE);
}

Bool
IMMASRCATTR_setFrameLen(IMMASRCATTR immasrcattr, Int frameLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr;

  assert(immasrcattr != NULL);
  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) immasrcattr);

  mmasrcattr->frameLen = frameLen;
  return(TRUE);
}

Bool
IMMASRCATTR_getFrameLen(IMMASRCATTR immasrcattr, Int *frameLen)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the length of an audio frame
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr;

  assert(immasrcattr != NULL);
  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) immasrcattr);

 *frameLen = mmasrcattr->frameLen;
  return(TRUE);
}

Bool
IMMASRCATTR_setSampleSize(IMMASRCATTR immasrcattr, Int sampleSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of a sample in bytes
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr;

  assert(immasrcattr != NULL);
  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) immasrcattr);

  mmasrcattr->sampBytes = sampleSize;
  return(TRUE);
}

Bool
IMMASRCATTR_getSampleSize(IMMASRCATTR immasrcattr, Int *sampleSize)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the size of a sample in bytes
 *---------------------------------------------------------------------------*/
{
  MMASRCATTR  mmasrcattr;

  assert(immasrcattr != NULL);
  mmasrcattr = (MMASRCATTR) IFACE_object((IFACE) immasrcattr);

 *sampleSize = mmasrcattr->sampBytes;
  return(TRUE);
}
