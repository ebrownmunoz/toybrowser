/* mmsrc.hh - MMAudio interface for an OOPS chain
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A simple interface to allow an OOPS chain to obtain data from an OLE
 * MMAudio source.  This interface is intended for use at the head of a
 * processing chain only.
 *
 * NOTE: Unlike other components in the OOPS world, this module is
 *       (necessarily) written for C++.
 * Verbex Voice Systems (cav)
 * $Header: mmasrc.hh[1.0] Tue Jun  9 18:37:05 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _MMSRC_HH_
#define _MMSRC_HH_

#include "types.h"
#include "class.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "audioattr.h"
#include "sn.h"

/*** MMASRC ***/

/* Create a MMASRC */
extern IGEN  MMASRC_create(IMemory * imem);


/*** MMASRC Attributes ***/

/* Create a MMASRCATTR attributes object */
extern IGEN  MMASRCATTR_create(IMemory * imem);

/* A MMASRC Interface */
typedef struct immasrc_s * IMMASRC;

/* A MMASRC attributes interface */
typedef IATTR IMMASRCATTR;
typedef iattr_t immasrcattr_t;

/* MMASRC Attributes Methods */
extern Bool  IMMASRCATTR_setAudioUnknown(IMMASRCATTR immmasrcattr, Ptr unknown);
extern Bool  IMMASRCATTR_setFrameLen(IMMASRCATTR immasrcattr, Int frameLen);
extern Bool  IMMASRCATTR_getFrameLen(IMMASRCATTR immasrcattr, Int *frameLen);
extern Bool  IMMASRCATTR_setSampleSize(IMMASRCATTR immasrcattr, Int sampleSize);
extern Bool  IMMASRCATTR_getSampleSize(IMMASRCATTR immasrcattr, Int *sampleSize);
extern Bool  IMMASRCATTR_setClassSize(IMMASRCATTR immasrcattr, Int classSize);

#endif /* _MMASRC_HH_ */
