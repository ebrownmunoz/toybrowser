/* winvcioctl.h - ioctl definitions for host/WINVC driver interaction
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (jrt)
 * $__Header$
 * DESCRIPTION
 * ioctl commands for winvc device driver
 *---------------------------------------------------------------------------*/
#ifndef _WINVCIOCTL_H_
#define _WINVCIOCTL_H_
#if defined(WIN32)

/* Micro$oft NT ioctl definitions */
#ifndef _NTDDK_
#include <winioctl.h>
#endif

#define FILE_DEVICE_WINVC 32768  /* Micro$oft reserves all below this number */
#define WINVC_IOCTL_INDEX 2048   /* Micro$oft reserves all below this number */

/* reset winvc keeping device open */
#define WINVC_RESET  (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 0, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* flush input buffer */
#define WINVC_FLUSH  (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 1, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* size of the next message in buffer */
#define WINVC_DATA   (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 2, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* test SPMSG bit */
#define WINVC_SPMSG  (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 3, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* set/clear VMSG bit */
#define WINVC_VMSG   (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 4, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* select mode 1 or 2 protocol */
#define WINVC_MODE   (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 5, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* specify DMA/PIO word cutoff */
#define WINVC_DMA    (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 6, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)
/* print driver status to kernel debug output */
#define WINVC_STATUS (ULONG)CTL_CODE(FILE_DEVICE_WINVC,     \
                                     WINVC_IOCTL_INDEX + 7, \
                                     METHOD_BUFFERED,       \
                                     FILE_ANY_ACCESS)

#define MODE_ONE    1  /* select message protocol mode 1 (default) */
#define MODE_TWO    2  /* select message protocol mode 2 */
#define AFTER_WRITE 4  /* change mode AFTER next write to the device */
#define AFTER_READ  8  /* change mode AFTER next read from the device */

#else
/* Digtal Unix ioctl definitions */

#include <sys/ioctl.h>

#define WINVC_RESET _IO('v', 0)       /* reset winvc keeping device open */
#define WINVC_FLUSH _IO('v', 1)       /* flush input buffer */
#define WINVC_DATA  _IOR('v', 2, int) /* size of the next message in buffer */
#define WINVC_SPMSG _IOR('v', 3, int) /* test SPMSG bit */
#define WINVC_VMSG  _IOW('v', 4, int) /* set/clear VMSG bit */
#define WINVC_MODE  _IOW('v', 5, int) /* select mode 1 or 2 protocol */
#define WINVC_DMA   _IOW('v', 6, int) /* specify DMA/PIO word cutoff */
#define WINVC_STATUS _IO('v', 7)      /* print driver status to screen */

#define MODE_ONE    0  /* select message protocol mode 1 (default) */
#define MODE_TWO    1  /* select message protocol mode 2 */
#define AFTER_WRITE 2  /* change mode AFTER next write to the device */
#define AFTER_READ  4  /* change mode AFTER next read from the device */

#endif
#endif 
