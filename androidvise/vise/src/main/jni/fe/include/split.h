/* split.h - a stream splitter
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * Verbex Voice Systems (cav)
 * $Header: split.h[1.0] Tue Jun  9 18:48:11 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _SPLIT_H_
#define _SPLIT_H_

#include "types.h"
#include "ifaces.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** SPLIT ***/

/* Create a SPLIT */
extern IGEN  SPLIT_create(IMemory * imem, ISRC source);

/* SPLIT Interface */
typedef struct isplit_s * ISPLIT;

#ifdef __cplusplus
}
#endif
#endif /* _SPLIT_H_ */
