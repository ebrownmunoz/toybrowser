/* mkcep.h - a creator of cepstral frames
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates frames of cepstral features from audio input
 *
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _MKCEP_H_
#define _MKCEP_H_

#include "types.h"
#include "sn.h"
#include "cepattr.h"
#include "iattr.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Create a MKCEP */
extern IGEN MKCEP_create(IMemory *imem, ISRC isrc);


/* MKCEP Interface */
typedef struct imkcep_s * IMKCEP;

/* MKCEP Methods */
extern Bool IMKCEP_reset(IMKCEP imkcep);
extern Bool IMKCEP_setSigBits(IMKCEP imkcep, Int sigBits);
extern void IMKCEP_startCalibration(IMKCEP imkcep, Int maxFrames);
extern void IMKCEP_stopCalibration(IMKCEP imkcep);
extern Int  IMKCEP_getNoiseTracking(IMKCEP imkcep);
extern void IMKCEP_setNoiseTracking(IMKCEP imkcep, Int track);
/*** MKCEP Attributes ***/
extern IGEN MKCEPATTR_create(IMemory *imem);

/* Create a MKCEP Attributes interface */
typedef IATTR IMKCEPATTR;
typedef iattr_t imkcepattr_t;

/* MKCEP Attributes Methods */
extern Bool IMKCEPATTR_getCepVersion(IMKCEPATTR imkcepattr, Int *cepVersion);
extern Bool IMKCEPATTR_setCepVersion(IMKCEPATTR imkcepattr, Int cepVersion);
extern Bool IMKCEPATTR_getClassSize(IMKCEPATTR imkcepattr, Int *classSize);
extern Bool IMKCEPATTR_setClassSize(IMKCEPATTR imkcepattr, Int classSize);
extern Bool IMKCEPATTR_getSampleRate(IMKCEPATTR imkcepattr, Int *sampleRate);
extern Bool IMKCEPATTR_getFrameLen(IMKCEPATTR imkcepattr, Int *frameLen);
extern Bool IMKCEPATTR_getFrameShift(IMKCEPATTR imkcepattr, Int *frameShift);
extern Bool IMKCEPATTR_getCepVecLen(IMKCEPATTR imkcepattr, Int *cepVecLen);
extern Bool IMKCEPATTR_getOtherProc(IMKCEPATTR imkcepattr, Int *otherProc);

#ifdef __cplusplus
}
#endif
#endif /* _MKCEP_H_ */
