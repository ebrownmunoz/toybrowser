#ifndef _SOB_H_
#define _SOB_H_
#include "types.h"

#define SOBUFSIZ  75

typedef struct sob_ {
  Int   readEl;
  Int   writeEl;
  Int   sizeEl;
} sob_t, *SOB;

typedef struct sobfrm_ {
  Int   seqNum;
  union {
    Float  f;
    Int    i;
  } cep[13];
  Int   wav[80];
} sobfrm_t, *SOBFRM;

#ifdef sun
void SOB_new(Char *sobname, Char *bufname, Int sizeEl);
#else
void SOB_new(SOB, SOBFRM bufptr, Int);
#endif
Bool SOB_overrun(SOB);
Bool SOB_readnext(SOB, Int *hbuf,  Int);
void SOB_reset(SOB);
Bool SOB_write(SOB, Int *hbuf, Int);
Bool SOB_writenext(SOB, Int *hbuf, Int);
#endif /* _SOB_H_ */
