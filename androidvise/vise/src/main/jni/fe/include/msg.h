/*
 * msg.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _MSG_H_
#define _MSG_H_

#include "types.h"

Bool MSG_open(Char *device);
Bool MSG_close(void);
Bool MSG_sendExec(Char *fname);
Bool MSG_send(packet_t);
Bool MSG_avail(void);
packet_t MSG_recv(void);

#endif /* _MSG_H_ */
