/* igen.h - the generic interface to an object
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: igen.h[1.0] Tue Jun  9 18:26:49 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _IGEN_H_
#define _IGEN_H_

#include "types.h"
#include "iface.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** GENERIC INTERFACE ***/

typedef struct igen_s * IGEN;

#include "iattr.h"

/* Generic Methods */
#define IGEN_interface(igen, iid)  IFACE_interface((IFACE) igen, iid)
extern Bool   IGEN_annihilate(IGEN igen);
extern Bool   IGEN_initialize(IGEN igen, IATTR attr);

/* IGEN Methods */
extern IGEN   IGEN_create(IMemory * allocator, OBJ object);
extern void   IGEN_setAnnihilator(IGEN igen, Bool (* fcn)(IGEN));
extern void   IGEN_setInitializer(IGEN igen, Bool (* fcn)(IGEN, IATTR));

#ifdef __cplusplus
}
#endif
#endif /* _IGEN_H_ */
