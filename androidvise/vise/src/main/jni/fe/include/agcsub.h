/* agcsub.h - a constant-subtracting automatic gain control
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Subtracts a constant value from the log-energy (c[0])
 *
 * Verbex Voice Systems (cav)
 * $Header: agcsub.h[1.0] Tue Jun  9 18:08:29 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _AGCSUB_H_
#define _AGCSUB_H_

#include "types.h"
#include "cepattr.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** AGCSUB ***/

/* Create an AGCSUB */
extern IGEN  AGCSUB_create(IMemory * imem, ISRC src);

/* AGCSUB Interface */
typedef struct iagcsub_s * IAGCSUB;

/* AGCSUB Methods */
extern Bool  IAGCSUB_reset(IAGCSUB iAgc, Float offset);
extern Bool  IAGCSUB_save(IAGCSUB iAgc);
extern Bool  IAGCSUB_getSub(IAGCSUB iAgc, Float * agcSub);
extern Bool  IAGCSUB_setSub(IAGCSUB iAgc, Float agcSub);
extern Bool  IAGCSUB_restore(IAGCSUB iAgc);

/*** AGCSUB Attributes ***/

/* Create a AGCSUB attributes object */
extern IGEN  AGCSUBATTR_create(IMemory * imem);

/* An AGCSUB attributes interface */
typedef IATTR IAGCSUBATTR;
typedef iattr_t iagcsubattr_t;

/* AGCSUB Attributes Methods */
extern Bool  IAGCSUBATTR_setSubtrahend(IAGCSUBATTR iagcsubattr, Float subtrahend);
extern Bool  IAGCSUBATTR_setNewWeight(IAGCSUBATTR iagcsubattr, Float newWeight);
extern Bool  IAGCSUBATTR_setOldWeight(IAGCSUBATTR iagcsubattr, Float oldWeight);
extern Bool  IAGCSUBATTR_setMinEnergy(IAGCSUBATTR iagcsubattr, Float minEnergy);

#ifdef __cplusplus
}
#endif
#endif /* _AGCSUB_H_ */
