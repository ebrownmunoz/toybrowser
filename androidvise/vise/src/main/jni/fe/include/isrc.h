/* isrc.h - the source interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: isrc.h[1.0] Tue Jun  9 18:30:28 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _ISRC_H_
#define _ISRC_H_

#include "types.h"
#include "class.h"
#include "igen.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ISRC is needed in isnk.h */
typedef struct isrc_s * ISRC;

#include "isnk.h"

/*** SOURCE ***/

/* The Source Interface */
typedef struct isrc_s {
  iface_t  iface;
  Bool     (* getFrame)(ISRC isrc, dir_t dir, SN * snP, OBJT * frP);
  Bool     (* frameAvailable)(ISRC isrc, dir_t dir, SN * snP);
  Bool     (* setSink)(ISRC isrc, ISNK isnk);
  Bool     (* getAttributes)(ISRC isrc, IATTR * iattr);
  IATTR    iattr;
  ISNK     sink;
} isrc_t;

/* Source Methods */
extern Bool  ISRC_setSink(ISRC isrc, ISNK isnk);
extern Bool  ISRC_getAttributes(ISRC isrc, IATTR * iattr);
extern Bool  ISRC_getFrame(ISRC isrc, dir_t dir, SN * snP, OBJT * frP);
extern Bool  ISRC_frameAvailable(ISRC isrc, dir_t dir, SN * snP);

/* ISRC Methods */
extern ISRC  ISRC_create(IGEN igen);
extern void  ISRC_default(ISRC isrc);
extern ISNK  ISRC_sink(ISRC isrc);
extern void  ISRC_setAttributes(ISRC isrc, IATTR iattr);
extern void  ISRC_setSetSink(ISRC isrc, Bool (* fcn)(ISRC, ISNK));
extern void  ISRC_setGetAttributes(ISRC isrc, Bool (* fcn)(ISRC, IATTR *));
extern void  ISRC_setGetFrame(ISRC isrc, Bool (* fcn)(ISRC, dir_t, SN *, OBJT *));
extern void  ISRC_setFrameAvailable(ISRC isrc, Bool (* fcn)(ISRC, dir_t, SN *));

#ifdef __cplusplus
}
#endif
#endif /* _ISRC_H_ */
