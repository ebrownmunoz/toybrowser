/* cepattr.h - cepstral frame attributes
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * Verbex Voice Systems (cav)
 * $Header: cepattr.h[1.0] Tue Jun  9 18:11:17 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _CEPATTR_H_
#define _CEPATTR_H_

#include "types.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** Cepstral Frame Attributes ***/

/* Create a CEP attributes object */
extern IGEN  CEPATTR_create(IMemory * imem);

/* A CEP Attributes Interface */
typedef IOBJATTR ICEPATTR;
typedef iobjattr_t icepattr_t;

/* CEP Attributes Methods */
extern Bool  ICEPATTR_getVecLen(ICEPATTR icepattr, Int * vecLenP);
extern Bool  ICEPATTR_setVecLen(ICEPATTR icepattr, Int vecLen);

#ifdef __cplusplus
}
#endif
#endif /* _CEPATTR_H_ */
