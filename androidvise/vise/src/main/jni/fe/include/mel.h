#ifndef _MEL_H_
#define _MEL_H_

#include "floorfix.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {BADMEL, VISE11, VISE10, VISE8, FLEX16OLD, FLEX8OLD} meltype;

typedef struct melsrc_s *MELSRC;

extern MELSRC MEL_create(IMemory *imem, Int feVersion);
extern void   MEL_destroy(MELSRC melsrc);
extern Bool   MEL_frameAvailable(MELSRC melsrc);
extern Int    MEL_snGap(MELSRC melsrc);
extern void   MEL_reset(MELSRC melsrc);
extern void   MEL_startCalibration(MELSRC melsrc, Int maxFrames);
extern void   MEL_stopCalibration(MELSRC melsrc);
extern Int    MEL_getNoiseTracking(MELSRC melsrc);
extern void   MEL_setNoiseTracking(MELSRC melsrc, Int track);
extern Bool   MEL_setSigBits(MELSRC melsrc, Int sigBits);
extern Bool   MEL_cepstrum(MELSRC melsrc, Val * mfc, Val * power,
                    Short * samples, Int * activity);

#ifdef __cplusplus
}
#endif
#endif  /* _MEL_H_ */
