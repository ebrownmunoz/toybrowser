#ifndef _KAISER_H_
#define _KAISER_H_

#include "imemory.h"

typedef struct kaiser_s *KAISER;

extern KAISER KAISER_create(IMemory *imem);
extern void   KAISER_destroy(KAISER kaiser);
extern void   KAISER_reset(KAISER kaiser);
extern void   KAISER_firstDif(KAISER kaiser, Short * input, Val * output, Int size);
extern void   KAISER_preEmph(KAISER kaiser, Short * input, Val * output, Int shift, Int size);
extern Val    KAISER_apply256(Val * input, Val * output);
extern Val    KAISER_window256(Val * input, Val * output);

#endif  /* _KAISER_H_ */
