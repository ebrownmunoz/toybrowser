/* cepmbox.h - a cepstral frame source which reads from a mailbox
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A simple cepstral frame source which reads cepstral frames from a mailbox
 *
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _CEPMBOX_H_
#define _CEPMBOX_H_

#include "types.h"
#include "class.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "mbox.h"
#include "cepattr.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** CEPMBOX ***/

/* Create a CEPMBOX */
extern IGEN  CEPMBOX_create(IMemory * imem);


/*** CEPMBOX Attributes ***/

/* Create a CEPMBOX attributes object */
extern IGEN  CEPMBATTR_create(IMemory * imem);

/* A CEPMBOX attributes interface */
typedef IATTR ICEPMBATTR;
typedef iattr_t icepmbattr_t;

/* CEPMBOX Attributes Methods */
extern Bool  ICEPMBATTR_setMailbox(ICEPMBATTR icepmbattr, MBOX mbox);
extern Bool  ICEPMBATTR_setClassSize(ICEPMBATTR icepmbattr, Int classSize);
extern Bool  ICEPMBATTR_getClassSize(ICEPMBATTR icepmbattr, Int * classSizePtr);
extern Bool  ICEPMBATTR_setCepVecLen(ICEPMBATTR icepmbattr, Int cepVecLen);
extern Bool  ICEPMBATTR_setTolerance(ICEPMBATTR icepmbattr, Int tolerance);

#ifdef __cplusplus
}
#endif
#endif /* _CEPMBOX_H_ */
