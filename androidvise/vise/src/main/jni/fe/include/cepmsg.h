/* cepmsg.h
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A cepstral frame source which reads frames from FlexVISE's incoming VBXMSG
 *
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _CEPMSG_H_
#define _CEPMSG_H_

#include "types.h"
#include "class.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "cepattr.h"
#include "visemsg.h"

#ifdef __cplusplus
extern "C" {
#endif


 /*** CEPMSG ***/

 /* Create a CEPMSG */
extern IGEN  CEPMSG_create(IMemory * memory);

 /* CEPMSG Interface */
typedef struct iqueue_s * ICEPMSG;

 /* CEPMSG Methods */
extern OBJT  ICEPMSG_newFrame(ICEPMSG icepmsg);


 /*** CEPMSG Attributes ***/

 /* Create a CEPMSG attributes object */
extern IGEN  CEPMSGATTR_create(IMemory * memory);

 /* A CEPMSG attributes interface */
typedef IATTR ICEPMSGATTR;
typedef iattr_t icepmsgattr_t;

 /* CEPMSG Attributes Methods */
extern Bool  ICEPMSGATTR_setClassSize(ICEPMSGATTR icepmsgattr, Uns classSize);
extern Bool  ICEPMSGATTR_getClassSize(ICEPMSGATTR icepmsgattr, Uns * classSizePtr);
extern Bool  ICEPMSGATTR_setFrameAttr(ICEPMSGATTR icepmsgattr, ICEPATTR icepattr);
extern Bool  ICEPMSGATTR_setMaxGapLen(ICEPMSGATTR icepmsgattr, SN maxGapLen);
extern Bool  ICEPMSGATTR_setMaxReject(ICEPMSGATTR icepmsgattr, SN maxReject);
extern Bool  ICEPMSGATTR_setMailbox(ICEPMSGATTR icepmsgattr, IMBox * mailbox);

#ifdef __cplusplus
}
#endif
#endif /* _CEPMSG_H_ */
