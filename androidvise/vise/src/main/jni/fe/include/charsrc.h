/* charsrc.h - Character Source to an OOPS chain head
 *---------------------------------------------------------------------------*
 *
 * DESCRIPTION
 *
 *---------------------------------------------------------------------------*/

#ifndef _CHARSRC_H_
#define _CHARSRC_H_

#include "types.h"
#include "iface.h"
#include "iattr.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Create a CHARSRC */
extern IGEN CHARSRC_create(IMemory *imem);

/* CHARSRC Interface */
typedef struct icharsrc_s * ICHARSRC;

/* CHARSRC Methods */
void  ICHARSRC_Start(ICHARSRC iCharSrc);
void  ICHARSRC_Stop(ICHARSRC iCharSrc);

/*** CHARSRC Attributes ***/
extern IGEN CHARSRCATTR_create(IMemory *imem);

typedef IATTR ICHARSRCATTR;
typedef iattr_t icharsrcattr_t;

/* CHARSRC Attributes Methods */
extern Bool ICHARSRCATTR_getClassSize(ICHARSRCATTR iCharSrcAttr, Int * classSizeP);
extern Bool ICHARSRCATTR_setClassSize(ICHARSRCATTR iCharSrcAttr, Int classSize);

#ifdef __cplusplus
}
#endif
#endif /* _CHARSRC_H_ */
