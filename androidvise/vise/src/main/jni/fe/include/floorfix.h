
#ifndef _FLOORFIX_H_
#define _FLOORFIX_H_

#include "types.h"

  /* These macros absorb the differences between floating and integer code */

#ifdef FIXED_POINT

typedef  Int                Val;                         /* 32-bit int */
#define SHIFTVAL           (8)                          /* Scaling for integer arithmetics */
#define SHIFTLOW           (4)                          /* Scaling for low accuracy and overflow prevention */
#define SHIFTWIN           (3)                          /* Scaling for windower */
#define SHIFTFLX           (14)                         /* Scaling of FlexVise Mel-Filter coefficents */
#define SHIFTMEAN          (10)                         /* Scaling of codebook feature mean values */
#define FLO2VAL(V)         ((Val)(256.0 * (V) + 0.5))   /* Majority table values converter */
#define FLOS2VAL(V)        ((Val)(256.0 * (V)))         /* Signed table values converter retains symmetry */
#define FLO2LOW(V)         ((Val)(16.0 * (V) + 0.5))    /* Low accuracy values converter */
#define FLX2VAL(V)         ((Val)(16384.0 * (V) + 0.5)) /* FlexVise Mel-Filter coefficients converter */
#define MEAN2VAL(V)        ((Val)(1024.0 * (V) + 0.5))  /* cep and codebook feature means and det converter */
#define NORMBACK(V)        ((V) >> SHIFTVAL)            /* Immediate back scaler */
#define NORMLOW(V)         ((V) >> SHIFTLOW)            /* Low accuracy values scaler */
#define NORMWIND(V)        ((V) >> SHIFTWIN)            /* Window scaler */
#define NORMMEAN(V)        ((V) >> SHIFTMEAN)           /* Restore mean scale in FIXED_POINT domain */
#define RESTOREMEAN(V)     (((Float)V) * (Float)0.0009765625) /* Multiply by 1./1024. and back to Float */
/* Divisor bringing Quad sum(var*diff*diff) in VQ to determinant (mean) scale */
#define DIVMEANVAR         ((Int)(1 << (SHIFTMEAN + SHIFTVAL)))
/* Divisor bringing Quad (det-sum(var*diff*diff))*LOG_RLnBaseInt in VQ to SCORE scale */
#define DIVDET             ((Int)(1 << SHIFTMEAN))
                                                        
/* log2b() constants */
#define LOG2_PLACES        2
#define LOG2_PLACEMASK     3              /* (( 1 << LOG2_PLACES ) - 1 ) */

#else  /* Floating Point */

typedef  Float              Val;
#define FLO2VAL(V)         (V)
#define FLOS2VAL(V)        (V)
#define FLO2LOW(V)         (V)
#define FLX2VAL(V)         (V)
#define MEAN2VAL(V)        (V)
#define DET2VAL(V)         (V)
#define VAR2VAL(V)         (V)
#define NORMBACK(V)        (V)
#define NORMLOW(V)         (V)
#define NORMWIND(V)        (V)
#define NORMMEAN(V)        (V)
#define RESTOREMEAN(V)     (V)
#define NORMDET(V)         (V)

#endif  /* FIXED_POINT */
#endif  /* _FLOORFIX_H_ */
