#ifndef _NESS_H_
#define _NESS_H_

#include "floorfix.h"
#include "imemory.h"

typedef struct ness_s *NESS;

extern NESS NESS_create(IMemory * imem, Int filterCnt);
extern void NESS_destroy(NESS ness);
extern void NESS_reset(NESS ness);
extern void NESS_startCalibration(NESS ness, Int maxFrames);
extern void NESS_stopCalibration(NESS ness);
extern Int  NESS_getNoiseTracking(NESS ness);
extern void NESS_setNoiseTracking(NESS ness, Int track);
extern void NESS_setMask(NESS ness, Int gainShift);
extern Int  NESS_activity(NESS ness, Val * mfs, Val powerDB, Int accShift);
extern void NESS_apply(NESS ness, Val * mfs, Val powerDB, Int accShift, Int activity);

#endif  /* _NESS_H_ */
