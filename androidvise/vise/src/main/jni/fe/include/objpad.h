/* objpad.h - a frame object stream padding module
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Generates a specified number of clones of a given frame object after
 * receiving an EOU (or SN == 0) FORWARD or BOU (or SN == SN_MAX) BACKWARD.
 *
 * Verbex Voice Systems (cav)
 * $Header: objpad.h[1.0] Tue Jun  9 18:41:03 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _OBJPAD_H_
#define _OBJPAD_H_

#include "types.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** OBJPAD ***/

/* Create a OBJPAD */
extern IGEN  OBJPAD_create(IMemory * imem, ISRC src);


/*** OBJPAD Attributes ***/

/* Create an OBJPAD attributes object */
extern IGEN  PADATTR_create(IMemory * imem);

/* A OBJPAD attributes interface */
typedef struct ipadattr_s {
  iattr_t  iattr;
  OBJT     padObject;
  SN       bouPadLen;
  SN       eouPadLen;
} ipadattr_t, * IPADATTR;

/* OBJPAD Attributes Methods */
#define IPADATTR_getPadObject(ipadattr, objP)  ((* objP) = (ipadattr)->padObject, TRUE)
#define IPADATTR_setPadObject(ipadattr, obj)   ((ipadattr)->padObject = (obj), TRUE)
#define IPADATTR_getBOUPadLen(ipadattr, plP)   ((* plP) = (ipadattr)->bouPadLen, TRUE)
#define IPADATTR_setBOUPadLen(ipadattr, pl)    ((ipadattr)->bouPadLen = (pl), TRUE)
#define IPADATTR_getEOUPadLen(ipadattr, plP)   ((* plP) = (ipadattr)->eouPadLen, TRUE)
#define IPADATTR_setEOUPadLen(ipadattr, pl)    ((ipadattr)->eouPadLen = (pl), TRUE)

#ifdef __cplusplus
}
#endif
#endif /* _OBJPAD_H_ */
