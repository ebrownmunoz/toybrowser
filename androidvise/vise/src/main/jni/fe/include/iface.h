/* iface.h - an interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $__Header$
 *---------------------------------------------------------------------------*/
#ifndef _IFACE_H_
#define _IFACE_H_

#include "types.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Use FAST_MEMORY for all objects and interfaces */
#define OOPS_MEMORY  FAST_MEMORY

/* All Interface Class IDs must be registered here */
typedef enum {
  IGENID,         ISRCID,         ISNKID,
  IATTRID,
  INULLSRCATTRID,
  IOBJATTRID,     IOBJSOURCEID,   IOBJSINKID,    
  IPRINTATTRID,   IQUEUEID,       IQSINKID,      IQATTRID,
  ISPLITID,
  IPADATTRID,
  IINTATTRID,
  ICEPATTRID,     ICEPSNATTRID,   ICEPARATTRID,  ICEPMBATTRID,
  IAGCSUBATTRID,  IAGCSUBID,
  IMMAUDSRCID,    IMMAUDSRCATTRID,
  IMMASRCID,      IMMASRCATTRID,  IAUDIOATTRID,
  IMKJINID,       IJINATTRID,     IMKJINATTRID,
  IMKFEATID,      IFEATATTRID,    IMKFEATATTRID,
  IVQATTRID,      IMKVQATTRID,
  IEPATTRID,      IMKEPATTRID,    IMKEPID,
  IMKCEPID,       IMKCEPATTRID,
  IJINMSGID,      IJINMSGATTRID,
  ICEPMSGID,      ICEPMSGATTRID,
  IFRAMEMSGID,    IFRAMEMSGATTRID,
  ICHARSRCID,     ICHARSRCATTRID,
  ITXTFRAMEID,    ITXTFRAMEATTRID
} IFACEID;

/* A Class of Interfaces */
typedef struct iclass_s {
  IFACEID   id;
} iclass_t, * ICLASS;

typedef Ptr  OBJ;           /* Any Object */
typedef Int  EXC;           /* Any Exception */

/* An Interface */
typedef struct iface_s * IFACE;
typedef struct iface_s {
  ICLASS    clas;
  OBJ       object;
  IFACE     sibling;
  IMemory * imem;
  EXC       error;
} iface_t;

/* All Exception conditions must be registered here */
#define EXC_NONE               0       /* No exception */
#define EXC_EOU               -1       /* End of utterance */
#define EXC_BOU               -2       /* Beginning of utterance */
#define EXC_NOFORWARD         -3       /* Cannot fulfill request for later frames */
#define EXC_NOBACKWARD        -4       /* Cannot fulfill request for earlier frames */
#define EXC_CANNOTDO          -5       /* Cannot perform requested operation */
#define EXC_DFLTSRC           -6       /* Default source cannot fulfill request */
#define EXC_DFLTSNK           -7       /* Default sink cannot fulfill request */
#define EXC_NOSRC             -8 
#define EXC_NOSNK             -9 
#define EXC_SEQNUMGAP         -10      /* Intolerable gap in the sequence numbering */
#define EXC_INVALIDSN         -11
#define EXC_SNWRAPAROUND      -12
#define EXC_NOPREVIOUSSN      -13      /* There's been no previous request for an SN */
#define EXC_CANTGETSN         -14
#define EXC_CANTPUTSN         -15
#define EXC_INVALIDREQUEST    -16
#define EXC_BADPACKETID       -17
#define EXC_CANTGETATTR       -18
#define EXC_CANTSETATTR       -19
#define EXC_QSIZEZERO         -20
#define EXC_QUNINIT           -21      /* The queue has no memory allocation (body) */
#define EXC_QEMPTY            -22
#define EXC_NOTINQUEUE        -23
#define EXC_NOOBJT            -24
#define EXC_OBJTSIZEZERO      -25
#define EXC_NOATTRIBUTES      -26
#define EXC_TIMEDOUT          -27      /* Timed out waiting for data */
#define EXC_BADTIMEOUTSPEC    -28      /* Invalid timeout specification */
#define EXC_CEPCLASSEMPTY     -29      /* The CLASS of cepstral frames is exhausted */
#define EXC_CEPCLASSTOOSMALL  -30
#define EXC_NOSUCHFEATURE     -31      /* The specified feature does not exist */
#define EXC_NOFEATURES        -32      /* Features or feature lengths lacking */
#define EXC_FEATCLASSEMPTY    -33      /* The CLASS of feature frames is exhausted */
#define EXC_FEATCLASSTOOSMALL -34
#define EXC_BADFEATATTR       -35
#define EXC_NUMFEATMISMATCH   -36
#define EXC_VECLENMISMATCH    -37
#define EXC_CANTGETNUMFEATS   -38
#define EXC_CANTGETFEATURE    -39
#define EXC_CANTCREATECLASS   -40
#define EXC_CANTFREECLASS     -41
#define EXC_CANTCLONEOBJT     -42
#define EXC_VQCLASSEMPTY      -45
#define EXC_BADVQATTR         -46
#define EXC_QUEUEFULL         -47
#define EXC_ALREADYSET        -50      /* Tried to change a permanent attribute */
#define EXC_NOCODEBOOKDATA    -51
#define EXC_CANTREADMEANS     -52      /* Can't read a file of codebook means */
#define EXC_CANTWRITEMEANS    -53      /* Can't write a file of codebook means */
#define EXC_WRONGSIZEMEANS    -54      /* Means array is the wrong size */
#define EXC_CANTREADVARS      -55      /* Can't read a file of codebook variances */
#define EXC_CANTWRITEVARS     -56      /* Can't write a file of codebook variances */
#define EXC_WRONGSIZEVARS     -57      /* Variances array is the wrong size */
#define EXC_NODISTRIBUTIONS   -60
#define EXC_EPCLASSEMPTY      -61      /* The CLASS of EP frames is exhausted */
#define EXC_AUDIOCLASSEMPTY   -62      /* The CLASS of audio frames is empty */
#define EXC_TOOFEWSAMPLES     -63      /* Not enough samples to make up a frame */
#define EXC_CHARCLASSEMPTY    -64      /* The CLASS of char frames is empty */
#define EXC_OUTOFMEMORY       -70      /* Can't allocate memory for requested operation */
#define EXC_CANTACKNOWLEDGE   -71      /* Can't acknowledge a command */
#define EXC_ABORT             -72
#define EXC_UNEXPECTED        -73      /* Something wicked this way comes */
#define EXC_NOMAILBOX         -80      /* No mailbox specified */
#define EXC_CANTSETMAILBOX    -81      /* Can't specify a mailbox */
#define EXC_CANTCREATEVBXMSG  -82      /* Can't create a VBXMSG port */
#define EXC_NUMOVERWRITTEN    -16384   /* Range reserved for reporting ... */
#define EXC_MANYOVERWRITTEN   -32767   /* the number of frames overwritten in a queue */

/* Make an EXC from an error count */
#define EXC_fromErrorCount(ONE, MANY, COUNT)   ((MANY) <= (ONE) - (COUNT) + 1 ? (ONE) - (COUNT) + 1 : (MANY))

/* Return an error count if the exception is in the range reserved for that error;  otherwise, return zero */
#define EXC_toErrorCount(ONE, MANY, EXC)       ((MANY) <= (EXC) && (EXC) <= (ONE) ? (ONE) - (EXC) + 1 : 0)


/* IFACE Methods */
extern ICLASS    IFACE_class(IFACE iface);
extern void      IFACE_setClass(IFACE iface, ICLASS clas);
extern OBJ       IFACE_object(IFACE iface);
extern void      IFACE_setObject(IFACE iface, OBJ object);
extern IMemory * IFACE_allocator(IFACE iface);
extern void      IFACE_setAllocator(IFACE iface, IMemory * allocator);
extern EXC       IFACE_error(IFACE iface);
extern void      IFACE_setError(IFACE iface, EXC error);
extern void *    IFACE_calloc(IFACE iface, size_t num, size_t size);
extern void      IFACE_free(IFACE iface, void * allocation);
extern IFACE     IFACE_interface(IFACE iface, IFACEID iid);
extern void      IFACE_addSibling(IFACE iface, IFACE sibling);
extern Bool      IFACE_removeSibling(IFACE iface, IFACE sibling);
extern void      IFACE_annihilate(IFACE iface);

#ifdef __cplusplus
}
#endif
#endif /* _IFACE_H_ */
