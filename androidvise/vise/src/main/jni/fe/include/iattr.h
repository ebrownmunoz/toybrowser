/* iattr.h - an interface to an attributes block
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: iattr.h[1.0] Tue Jun  9 18:17:20 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _IATTR_H_
#define _IATTR_H_

#include "types.h"
#include "class.h"
#include "iface.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

/* An Interface to an Attributes Block */
typedef iface_t iattr_t, * IATTR;

#include "igen.h"

/* An Interface to an OBJT Attributes Block */
typedef struct iobjattr_s * IOBJATTR;
typedef struct iobjattr_s {
  iattr_t  iattr;
  Bool     (* print)(IOBJATTR iobjattr, OBJT objt, Int prefixLen, Int maxFields);
  Bool     (* deconstruct)(IOBJATTR iobjattr, OBJT objt, Char * buff, UInt buffLen, UInt * buffReq);
  Bool     (* reconstruct)(IOBJATTR iobjattr, OBJT objt, Char * buff, UInt buffLen, UInt * buffReq);
  Char   * label;
} iobjattr_t;

/* Create an IOBJATTR Interface */
extern IOBJATTR  IOBJATTR_create(IGEN igen);

/* IOBJATTR Methods */
#define IOBJATTR_printObject(iobjattr, objt, prefixLen, maxFields)          ((iobjattr)->print(iobjattr, objt, prefixLen, maxFields))
#define IOBJATTR_deconstructObject(iobjattr, objt, buff, buffLen, buffReq)  ((iobjattr)->deconstruct(iobjattr, objt, buff, buffLen, buffReq))
#define IOBJATTR_reconstructObject(iobjattr, objt, buff, buffLen, buffReq)  ((iobjattr)->reconstruct(iobjattr, objt, buff, buffLen, buffReq))
#define IOBJATTR_setPrinter(iobjattr, fcn)                                  ((iobjattr)->print = (fcn))
#define IOBJATTR_setDeconstructor(iobjattr, fcn)                            ((iobjattr)->deconstruct = (fcn))
#define IOBJATTR_setReconstructor(iobjattr, fcn)                            ((iobjattr)->reconstruct = (fcn))
#define IOBJATTR_setLabel(iobjattr, str)                                    ((iobjattr)->label = (str))
#define IOBJATTR_getLabel(iobjattr, strP)                                   (*(strP) = (iobjattr)->label, TRUE)

#ifdef __cplusplus
}
#endif
#endif /* _IATTR_H_ */
