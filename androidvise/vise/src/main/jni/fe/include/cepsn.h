/* cepsn.h - a cepstral frame source
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A simple cepstral frame source for testing
 * Sources a cepstral frame filled with any positive SN requested.  Requesting
 * a negative SN produces the next positive SN in the specified direction.
 *
 * Verbex Voice Systems (cav)
 * $Header: cepsn.h[1.0] Tue Jun  9 18:13:22 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _CEPSN_H_
#define _CEPSN_H_

#include "types.h"
#include "class.h"
#include "ifaces.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** CEPSN ***/

/* Create an CEPSN */
extern IGEN  CEPSN_create(IMemory * imem);


/*** CEPSN Attributes ***/

/* Create a CEPSN attributes object */
extern IGEN  CEPSNATTR_create(IMemory * imem);

/* A CEPSN attributes interface */
typedef struct icepsnattr_s {
  iattr_t  iattr;
  CLASS    cepstra;        /* The class of cepstral vectors */
} icepsnattr_t, * ICEPSNATTR;

/* CEPSN Attributes Methods */
#define ICEPSNATTR_getClass(icepsnattr, classP)  ((* classP) = (icepsnattr)->cepstra, TRUE)
#define ICEPSNATTR_setClass(icepsnattr, class)   ((icepsnattr)->cepstra = (class), TRUE)

#ifdef __cplusplus
}
#endif
#endif /* _CEPSN_H_ */
