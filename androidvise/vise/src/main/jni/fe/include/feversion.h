#ifndef _FEVERSION_H_
#define _FEVERSION_H_

#include "mel.h"

#ifdef __cplusplus
extern "C" {
#endif

 /* Front End Versions */
#define FE_BADVERSION             0
#define FE_FVISE16KHzOldNorm      1
#define FE_FVISE8KHzRastaOldNorm  2
#define FE_VISE8KHz               813
#define FE_VISE10KHz              850
#define FE_VISE11KHz              900
#define FE_FVISE16KHzExternalDSP  10001
#define FE_FVISE8KHzExternalDSP   10002
#define FE_VISE8KHzNSS            8
#define FE_VISE11KHzNSS           11

/* Valid VVISE Front End version */
#define VALID_FE_VISE(V) ((V) == FE_VISE11KHz || (V) == FE_VISE11KHzNSS ||\
                          (V) == FE_VISE8KHz  || (V) == FE_VISE8KHzNSS  ||\
                          (V) == FE_VISE10KHz)

/* Valid FVISE Front End version */
#define VALID_FE_FVISE(V) ((V) == FE_FVISE16KHzOldNorm || (V) == FE_FVISE8KHzRastaOldNorm)

/* Cepstrum Processing Options Bits */
#define RASTA  1
#define NMASS  2

 /* Public Methods */
extern Bool    FEVER_getSampleRate(Int feVersion, Int * sampleRate);
extern Bool    FEVER_getFrameLen(Int feVersion, Int * frameLen);
extern Bool    FEVER_getFrameShift(Int feVersion, Int * frameShift);
extern Bool    FEVER_getCepVecLen(Int feVersion, Int * cepVecLen);
extern Bool    FEVER_getOtherProc(Int feVersion, Int * otherProc);
extern meltype FEVER_getMelType(Int feVersion);

#ifdef __cplusplus
}
#endif
#endif /* _FEVERSION_H_ */
