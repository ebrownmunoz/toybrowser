#ifndef _HAM_H_
#define _HAM_H_

#include "floorfix.h"
#include "imemory.h"

#define PI 3.141592654

typedef struct ham_s *HAM;
 /* For FlexVise */
HAM   HAM_create(IMemory *imem, Int windowSize);
void  HAM_destroy(HAM ham);
void  HAM_reset(HAM ham);
void  HAM_preEmph(HAM ham, Short * input, Val * output, Int size);
void  HAM_apply(HAM ham, Val * input, Val * output);

#endif  /* _HAM_H_ */
