/* ceparray.h - a cepstral frame source which reads from an array
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A simple cepstral frame source which reads from an unstructured array of
 * cepstral coefficients.
 *
 * Verbex Voice Systems (cav)
 * $Header: ceparray.h[1.0] Tue Jun  9 18:10:03 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _CEPARRAY_H_
#define _CEPARRAY_H_

#include "types.h"
#include "floorfix.h"
#include "class.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "cepattr.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** CEPARRAY ***/

/* Create a CEPARRAY */
extern IGEN  CEPARRAY_create(IMemory * imem);


/*** CEPARRAY Attributes ***/

/* Create a CEPARRAY attributes object */
extern IGEN  CEPARATTR_create(IMemory * imem);

/* A CEPARRAY attributes interface */
typedef IATTR ICEPARATTR;
typedef iattr_t iceparattr_t;

/* CEPARRAY Attributes Methods */
extern Bool  ICEPARATTR_setArray(ICEPARATTR iceparattr, Val * array, Int size);
extern Bool  ICEPARATTR_setClassSize(ICEPARATTR iceparattr, Int classSize);
extern Bool  ICEPARATTR_setCepVecLen(ICEPARATTR iceparattr, Int cepVecLen);
extern Bool  ICEPARATTR_setMinSN(ICEPARATTR iceparattr, SN minSN);

#ifdef __cplusplus
}
#endif
#endif /* _CEPARRAY_H_ */
