#ifndef _RASTA_H_
#define _RASTA_H_

#include "floorfix.h"
#include "imemory.h"

#define QUEUELEN 5

typedef struct rast_s *RAST;

RAST RASTA_create(IMemory * imem, UInt filterCount);
Void RASTA_destroy(RAST rasta);
Bool RASTA_computeFrame(RAST rasta, Val * mfs);
Void RASTA_reset(RAST rasta);

#endif
