/* intattr.h - integer frame attributes
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * Verbex Voice Systems (cav)
 * $Header: intattr.h[1.0] Tue Jun  9 18:28:42 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _INTATTR_H_
#define _INTATTR_H_

#include "types.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** Integer Frame Attributes ***/

/* Create an INT attributes object */
extern IGEN  INTATTR_create(IMemory * imem);

/* An INT Attributes Interface */
typedef IOBJATTR IINTATTR;
typedef iobjattr_t iintattr_t;

#ifdef __cplusplus
}
#endif
#endif /* _INTATTR_H_ */
