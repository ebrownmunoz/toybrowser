/*
 * hostmsg.h
 *
 * Verbex Voice Systems (jrt)
 */

#ifndef _HOSTMSG_H_
#define _HOSTMSG_H_

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

Bool MSG_open(Char *device);
Bool MSG_close(void);
Bool MSG_sendExec(Char *fname);
Bool MSG_send(PACK);
Bool MSG_avail(void);
PACK MSG_recv(void);

#ifdef __cplusplus
}
#endif
#endif /* _HOSTMSG_H_ */
