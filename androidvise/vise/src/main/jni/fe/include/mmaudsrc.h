/* mmaudsrc.h - MMAUDIN interface for an OOPS chain
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A simple interface to allow an OOPS chain to obtain data from a "Port Kit"
 * MMAUDIN audio source.  This interface is intended for use at the head of a
 * processing chain only.
 *
 * Verbex Voice Systems (cav)
 * $__Header$
 *---------------------------------------------------------------------------*/

#ifndef _MMAUDSRC_H_
#define _MMAUDSRC_H_

#include "types.h"
#include "class.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "isrc.h"
#include "audioattr.h"
#include "sn.h"

/*** MMAUDSRC ***/
typedef struct mmaudsrc_s *MMAUDSRC;

/* Create an MMAUDSRC */
extern IGEN  MMAUDSRC_create(IMemory * imem);

/*** MMAUDSRC Attributes ***/

/* Create a MMAUDSRCATTR attributes object */
extern IGEN  MMAUDSRCATTR_create(IMemory * imem);

/* Accessor for getting wave frames */
extern OBJT  MMAUDSRC_newFrame(ISRC isrc);

/* A MMAUDSRC Interface */
typedef struct immasrc_s * IMMAUDSRC;

/* A MMAUDSRC attributes interface */
typedef IATTR IMMAUDSRCATTR;
typedef iattr_t immaudsrcattr_t;

/* MMAUDSRC Attributes Methods */
extern Bool  IMMAUDSRCATTR_setIAudioIn(IMMAUDSRCATTR immaudsrcattr, Ptr iAudioPtr);
extern Bool  IMMAUDSRCATTR_setFrameLen(IMMAUDSRCATTR immaudsrcattr, Int frameLen);
extern Bool  IMMAUDSRCATTR_getFrameLen(IMMAUDSRCATTR immaudsrcattr, Int *frameLen);
extern Bool  IMMAUDSRCATTR_setSampleSize(IMMAUDSRCATTR immaudsrcattr, Int sampleSize);
extern Bool  IMMAUDSRCATTR_getSampleSize(IMMAUDSRCATTR immaudsrcattr, Int *sampleSize);
extern Bool  IMMAUDSRCATTR_setClassSize(IMMAUDSRCATTR immaudsrcattr, Int classSize);

#endif /* _MMAUDSRC_H_ */
