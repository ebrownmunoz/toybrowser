/* txtframe.h - a creator of text frames 
 *---------------------------------------------------------------------------*
 *
 * DESCRIPTION
 * Creates FE frames with char as first byte from character stream input
 *
 *---------------------------------------------------------------------------*/

#ifndef _TXTFRAME_H_
#define _TXTFRAME_H_

#include "types.h"
#include "iface.h"
#include "iattr.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Create a TXTFRAME */
extern IGEN TXTFRAME_create(IMemory *imem, ISRC isrc);

/* TXTFRAME Interface */
typedef struct itxtframe_s * ITXTFRAME;

/* TXTFRAME Methods */

/*** TXTFRAME Attributes ***/
extern IGEN TXTFRAMEATTR_create(IMemory *imem);

typedef IATTR ITXTFRAMEATTR;
typedef iattr_t itxtframeattr_t;

/* TXTFRAME Attributes Methods */
extern Bool ITXTFRAMEATTR_getClassSize(ITXTFRAMEATTR iTxtFrameAttr, Int * classSizeP);
extern Bool ITXTFRAMEATTR_setClassSize(ITXTFRAMEATTR iTxtFrameAttr, Int classSize);
extern Bool ITXTFRAMEATTR_getFrameSize(ITXTFRAMEATTR iTxtFrameAttr, Int * frameSizeP);
extern Bool ITXTFRAMEATTR_setFrameSize(ITXTFRAMEATTR iTxtFrameAttr, Int feVersion);

#ifdef __cplusplus
}
#endif
#endif /* _TXTFRAME_H_ */
