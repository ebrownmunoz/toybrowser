/* cep.h - external interface to cepstral library
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 * $Header: cep.h[2.2] Thu Feb 24 00:27:18 1994 dvetter@noname saved $
 * HISTORY
 * 15-Oct-93 Created by Dave Vetter (dvetter@rad.verbex.com)
 *---------------------------------------------------------------------------*/
#ifndef _CEP_H_
#define _CEP_H_

#include <stdlib.h>
#include "types.h"
#include "floorfix.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef Val CEPEL;

#define CEP_VECLEN   13
#define CEP_VECSIZE  (CEP_VECLEN * sizeof(CEPEL))

typedef struct cepvec_s {
  CEPEL  cep[CEP_VECLEN];
} cepvec_t, * CEPVEC;

typedef struct cepframe_s {
  cepvec_t  cepstrum;      /* A CEPFRAME inherits from a CEPVEC, SO THIS MUST BE FIRST! */
  SN        sn;
} cepframe_t, * CEPFRAME;

void    CEP_agcMax(Val *cep, Int fcnt, Int cepVecLen);
Val     CEP_max(Val *cep, Int fcnt, Int cepVecLen);
Ptr     CEP_newBuffer(void);
void    CEP_setBuffer(Ptr buf, Val *cep, size_t numFrames, Int frameSize);
void    CEP_resetBuffer(Ptr buf);
Val   * CEP_nextFrame(Ptr buf, Long * seqNumPtr);
Val   * CEP_frame(Ptr buf, Int i, Long * seqNumPtr);
Bool    CEP_frameAvail(Ptr buf);

#define PRINTCEP13(N,S,C)  VBX_print("%s %#016.16lx: %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f\n",\
                                     (N), (V_SN) (S), (C)->cep[0], (C)->cep[1], (C)->cep[2], (C)->cep[3], (C)->cep[4], (C)->cep[5], \
                                                      (C)->cep[6], (C)->cep[7], (C)->cep[8], (C)->cep[9], (C)->cep[10], (C)->cep[11], (C)->cep[12])

#ifdef __cplusplus
}
#endif
#endif  /* _CEP_H_ */
