/* nullsrc.h - a null passive source
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * A null passive source
 *
 * Verbex Voice Systems (cav)
 * $Header: nullsrc.h[1.0] Tue Jun  9 18:31:48 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _NULLSRC_H_
#define _NULLSRC_H_

#include "types.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** NULLSRC ***/

/* Create a NULLSRC */
extern IGEN  NULLSRC_create(IMemory * imem);

/*** NULLSRC Attributes ***/

/* Create a NULLSRC attributes object */
extern IGEN  NULLSRCATTR_create(IMemory * imem);

/* A NULLSRC attributes interface */
typedef IATTR INULLSRCATTR;
typedef iattr_t inullsrcattr_t;

/* NULLSRC Attributes Methods */
extern Bool INULLSRCATTR_setFwdException(INULLSRCATTR inullsrcattr, EXC fwdExc);
extern Bool INULLSRCATTR_setBwdException(INULLSRCATTR inullsrcattr, EXC bwdExc);

#ifdef __cplusplus
}
#endif

#endif /* _NULLSRC_H_ */
