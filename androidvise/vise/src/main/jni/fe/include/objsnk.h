/* objsnk.h - a sink for frames of arbitrary type
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Gets frames from a frame source of arbitrary type
 *
 * Verbex Voice Systems (cav)
 * $Header: objsnk.h[1.0] Tue Jun  9 18:43:47 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _OBJSINK_H_
#define _OBJSINK_H_

#include "types.h"
#include "iattr.h"
#include "ifaces.h"
#include "isrc.h"
#include "igen.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Create a OBJSINK */
extern IGEN  OBJSINK_create(IMemory * imem, ISRC src);

/* OBJSINK Interface */
typedef struct iobjsink_s * IOBJSINK;

/* OBJSINK Methods */
extern Bool IOBJSINK_getAttributes(IOBJSINK iobjsink, IATTR * iattr);
extern Bool IOBJSINK_getFrame(IOBJSINK iobjsink, dir_t dir, SN * snP, OBJT * fP);
extern Bool IOBJSINK_frameAvailable(IOBJSINK iobjsink, dir_t dir, SN * snP);

#ifdef __cplusplus
}
#endif
#endif /* _OBJSINK_H_ */
