/* objprint.h - a frame object printer
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Prints out a frame object while passing it through unmodified
 *
 * Verbex Voice Systems (cav)
 * $Header: objprint.h[1.0] Tue Jun  9 18:42:32 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _OBJPRINT_H_
#define _OBJPRINT_H_

#include "types.h"
#include "iattr.h"
#include "ifaces.h"
#include "igen.h"
#include "isrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** OBJPRINT ***/

/* Create a OBJPRINT */
extern IGEN  OBJPRINT_create(IMemory * imem, ISRC src);


/*** OBJPRINT Attributes ***/

/* Create a OBJPRINT attributes object */
extern IGEN  PRINTATTR_create(IMemory * imem);

/* A OBJPRINT attributes interface */
typedef struct iprintattr_s {
  iattr_t  iattr;
  Char   * label;
  Int      maxLine;
} iprintattr_t, * IPRINTATTR;

/* OBJPRINT Attributes Methods */
#define IPRINTATTR_getLabel(iprintattr, strP) \
  ((* strP) = (iprintattr)->label, TRUE)
#define IPRINTATTR_setLabel(iprintattr, str)  \
  ((iprintattr)->label = (str), TRUE)
#define IPRINTATTR_getMaxLine(iprintattr, mlP) \
  ((* mlP) = (iprintattr)->maxLine, TRUE)
#define IPRINTATTR_setMaxLine(iprintattr, ml)  \
  ((iprintattr)->maxLine = (ml), TRUE)

#ifdef __cplusplus
}
#endif
#endif /* _OBJPRINT_H_ */
