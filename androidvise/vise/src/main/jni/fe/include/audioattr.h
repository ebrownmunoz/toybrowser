/* audiattr.h - the audio extractor attributes
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: audioattr.h[1.0] Tue Jun  9 18:06:55 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _IAUDIOATTR_H_
#define _IAUDIOATTR_H_

#include "types.h"
#include "iattr.h"
#include "iface.h"
#include "ifaces.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** Audio Frame Attributes */

/* Create an AUDIO attributes object */
extern IGEN  AUDIOATTR_create(IMemory * imem);

/* An AUDIO Attributes Interface */
typedef IOBJATTR IAUDIOATTR;
typedef iobjattr_t iaudioattr_t;

/* AUDIO Attributes Methods */
extern Bool  IAUDIOATTR_getFrameLen(IAUDIOATTR iaudioattr, Int *vecLen);
extern Bool  IAUDIOATTR_setFrameLen(IAUDIOATTR iaudioattr, int vecLen);

#ifdef __cplusplus
}
#endif
#endif /* _IAUDIOATTR_H_ */
