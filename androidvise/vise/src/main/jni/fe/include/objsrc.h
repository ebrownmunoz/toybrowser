/* objsrc.h - an active source for frames of arbitrary type
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Puts frames of arbitrary type into a passive frame sink
 *
 * Verbex Voice Systems (dcv)
 * $Header: objsrc.h[1.0] Tue Jun  9 18:45:37 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _OBJSOURCE_H_
#define _OBJSOURCE_H_

#include "types.h"
#include "iattr.h"
#include "isrc.h"
#include "ifaces.h"
#include "igen.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Create a OBJSOURCE */
extern IGEN  OBJSOURCE_create(IMemory * imem);

/* OBJSOURCE Interface */
typedef struct iobjsource_s * IOBJSOURCE;

/* OBJSOURCE Methods */
extern Bool IOBJSOURCE_setAttributes(IOBJSOURCE iobjsource, IATTR iattr);
extern Bool IOBJSOURCE_putFrame(IOBJSOURCE iobjsource, dir_t dir, SN * snP, OBJT frame);
extern Bool IOBJSOURCE_spaceAvailable(IOBJSOURCE iobjsource, dir_t dir, SN * snP);

#ifdef __cplusplus
}
#endif
#endif /* _OBJSOURCE_H_ */
