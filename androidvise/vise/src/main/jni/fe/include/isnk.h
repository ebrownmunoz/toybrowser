/* isnk.h - the sink interface
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (cav)
 * $Header: isnk.h[1.0] Tue Jun  9 18:29:37 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/
#ifndef _ISNK_H_
#define _ISNK_H_

#include "types.h"
#include "class.h"
#include "iattr.h"
#include "igen.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ISNK is needed in isrc.h */
typedef struct isnk_s * ISNK;

#include "isrc.h"

/*** SINK ***/

/* The Sink Interface */
typedef struct isnk_s {
  iface_t  iface;
  Bool     (* putFrame)(ISNK isnk, dir_t dir, SN * snP, OBJT fr);
  Bool     (* spaceAvailable)(ISNK isnk, dir_t dir, SN * snP);
  Bool     (* setSource)(ISNK isnk, ISRC isrc);
  Bool     (* setAttributes)(ISNK isnk, IATTR iattr);
  IATTR    iattr;
  ISRC     source;
} isnk_t;

/* Sink Methods */
extern Bool  ISNK_setSource(ISNK isnk, ISRC isrc);
extern Bool  ISNK_setAttributes(ISNK isnk, IATTR iattr);
extern Bool  ISNK_putFrame(ISNK isnk, dir_t dir, SN * snP, OBJT fr);
extern Bool  ISNK_spaceAvailable(ISNK isnk, dir_t dir, SN * snP);

/* ISNK Methods */
extern ISNK  ISNK_create(IGEN igen);
extern void  ISNK_default(ISNK isnk);
extern ISRC  ISNK_source(ISNK isnk);
extern void  ISNK_getAttributes(ISNK isnk, IATTR * iattr);
extern void  ISNK_setSetSource(ISNK isnk, Bool (* fcn)(ISNK, ISRC));
extern void  ISNK_setSetAttributes(ISNK isnk, Bool (* fcn)(ISNK, IATTR));
extern void  ISNK_setPutFrame(ISNK isnk, Bool (* fcn)(ISNK, dir_t, SN *, OBJT));
extern void  ISNK_setSpaceAvailable(ISNK isnk, Bool (* fcn)(ISNK, dir_t, SN *));

/* OBSOLETE method providing the same service as ISNK_getAttributes() */
extern IATTR ISNK_attributes(ISNK isnk);  /* OBSOLETE */

#ifdef __cplusplus
}
#endif
#endif /* _ISNK_H_ */
