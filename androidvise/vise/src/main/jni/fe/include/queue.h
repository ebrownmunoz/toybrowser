/* queue.h - a bidirectional queue
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 *
 * Verbex Voice Systems (cav)
 * $Header: queue.h[1.0] Tue Jun  9 18:47:10 1998 dvetter@titan saved $
 *---------------------------------------------------------------------------*/

#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "types.h"
#include "iface.h"
#include "iattr.h"
#include "igen.h"
#include "isrc.h"
#include "sn.h"

#ifdef __cplusplus
extern "C" {
#endif

/*** QUEUE ***/

/* Create a QUEUE */
extern IGEN  QUEUE_create(IMemory * imem, ISRC source);

/* QUEUE Interface */
typedef struct iqueue_s * IQUEUE;

/* QUEUE Methods */
extern Bool  IQUEUE_reset(IQUEUE iq);
extern Bool  IQUEUE_range(IQUEUE iq, SN * minSNP, SN * maxSNP);
extern Bool  IQUEUE_frameCount(IQUEUE iq, SN * minSNP, SN * maxSNP, SN * countP);

/* QSINK Interface */
typedef struct iqsink_s * IQSINK;

/* QSINK Methods */
extern Bool  IQSINK_reset(IQSINK iq);
extern Bool  IQSINK_range(IQSINK iq, SN * minSNP, SN * maxSNP);
extern Bool  IQSINK_frameCount(IQSINK iq, SN * minSNP, SN * maxSNP, SN * countP);

/*** QUEUE Attributes ***/

/* Create a QUEUE attributes object */
extern IGEN  QATTR_create(IMemory * imem);

enum QUEUE_snkModes { SNK_ACTIVE, SNK_PASSIVE };

/* A QUEUE attributes interface */
typedef struct iqattr_s {
  iattr_t  iattr;
  Int      size;       /* The number of objects the queue can hold */
  Int      snkMode;    /* The sink (input) mode: SNK_PASSIVE or SNK_ACTIVE */
  SN       fwdTol;     /* The tolerance for SNs out of range forward */
  SN       bwdTol;     /* The tolerance for SNs out of range backward */
  Int      timeout;    /* SNK_PASSIVE getFrame timeout (in millisec, 0 => none) */
  Bool     faq;        /* If TRUE, frameAvailable gets and queues a frame */
} iqattr_t, * IQATTR;

/* QUEUE Attributes Methods */
#define IQATTR_getSize(iqattr, numP)     ((* numP) = (iqattr)->size, TRUE)
#define IQATTR_setSize(iqattr, num)      ((iqattr)->size = (num), TRUE)
#define IQATTR_getFwdTol(iqattr, fwdTP)  ((* fwdTP) = (iqattr)->fwdTol, TRUE)
#define IQATTR_setFwdTol(iqattr, fwdT)   ((iqattr)->fwdTol = (fwdT), TRUE)
#define IQATTR_getBwdTol(iqattr, bwdTP)  ((* bwdTP) = (iqattr)->bwdTol, TRUE)
#define IQATTR_setBwdTol(iqattr, bwdT)   ((iqattr)->bwdTol = (bwdT), TRUE)
#define IQATTR_getSnkMode(iqattr, sMP)   ((* sMP) = (iqattr)->snkMode, TRUE)
#define IQATTR_setSnkMode(iqattr, sM)    ((iqattr)->snkMode = (sM), TRUE)
#define IQATTR_getTimeout(iqattr, waitP) ((* waitP) = (iqattr)->timeout, TRUE)
#define IQATTR_setTimeout(iqattr, wait)  ((iqattr)->timeout = (wait), TRUE)
#define IQATTR_setFAQ(iqattr, val)       ((iqattr)->faq = (val), TRUE)
#define IQATTR_getFAQ(iqattr, valP)      ((* valP) = (iqattr)->faq, TRUE)

#ifdef __cplusplus
}
#endif
#endif /* _QUEUE_H_ */
