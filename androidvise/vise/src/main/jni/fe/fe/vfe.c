/* vfe.c - VISE Feature Extractor Interface Module
 *---------------------------------------------------------------------------*
 * Verbex Voice Systems (dcv)
 *
 * DESCRIPTION
 * Interface to the VISE Feature Extractor
 * 
 * HISTORY
 * 23-Oct-97  Dave Vetter (dvetter@verbex.com)
 *      Created
 *
 *---------------------------------------------------------------------------*/
static char     AtFSid[] = "$__Header$";

#include "pthr.h"
#include <errno.h>
#include <assert.h>

/* #define NDEBUG */

#include "types.h"
#include "ifaces.h"
#include "fe.h"
#include "sn.h"
#include "vbxtypes.h"
#include "vfe.h"
#include "visecmds.h"
#include "visecomp.h"
#include "viseerr.h"
#include "visemsg.h"
#include "vbx.h"

#ifdef DEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

#define VFE_NAME  "VFE"

/* VFE Descriptor */
typedef struct vfe_s {
  FE          fe;
  IMemory   * imem;
  IMail     * mail;
  IMBox     * mailbox;
  V_Uns       myMBoxId;
  V_Uns       feMBoxId;
  VBXMSG      message;
  pthread_t   thread;
} vfe_t;

/* Private VFE methods */
static Bool   VFE_Run(VFE vfe, void (* function)(FE), V_Err * status);
static V_Err  VFE_verifyAck(VBXMSG message, V_Int command, V_Int cmdExpected);

VFE
VFE_create(IMemory * imem, IMail * mail, IAudioIn * audio)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Instantiate Virtual FE
 *---------------------------------------------------------------------------*/
{
  VFE    vfe;
  V_Err  status;

  if (  /* Instantiate VFE */
      (vfe = (VFE) imem->Calloc(imem, 1, sizeof(vfe_t), FAST_MEMORY)) &&
        /* Get IMemory */
      (vfe->imem = imem) &&
        /* Get IMail */
      (vfe->mail = mail) &&
        /* Create a mailbox for VFE */
      (vfe->mailbox = mail->CreateMBox(mail, VFE_NAME, NUMPRIORITIES, &status)) &&
        /* Get VFE's mailbox identifier */
      (vfe->myMBoxId = mail->MailBoxId(mail, VFE_NAME, &status)) &&
        /* Create a message port for VFE */
      (vfe->message = MSG_create(imem, vfe->mailbox)) &&
        /* Instantiate the VISE Feature Extractor */
      (vfe->fe = FE_create(audio, mail, imem, &status)) &&
        /* Run the Feature Extractor */
      (VFE_Run(vfe, FE_run, &status))
     ) {
    VBX_DEBUG(VBX_print("VFE_create: succeeded\n"));
  } else {
    VBX_DEBUG(VBX_print("VFE_create: failed (status = %s)\n", VISEERR_string(status)));
    VFE_destroy(vfe);
    vfe = NULL;
  }

  return vfe;
}


static Bool
VFE_Run(VFE vfe, void (* function)(FE), V_Err * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create a thread and run an instance of the Feature Extractor in it
 *---------------------------------------------------------------------------*/
{
  Int  threadStatus = pthread_create(&vfe->thread, NULL, (pthread_startroutine_t) function, (void *) vfe->fe);

  if (!threadStatus) {
    VBX_DEBUG(VBX_print("VFE_Run: created thread for %s\n", VISECOMP_FE, threadStatus, errno));
    if (!(vfe->feMBoxId) && !(vfe->feMBoxId = vfe->mail->MailBoxId(vfe->mail, VISECOMP_FE, status)))
      VBX_DEBUG(VBX_print("VFE_Run: cannot get ID of FE mailbox (*status = %s)\n", VISEERR_string(*status)));
  } else {
    switch (threadStatus) {
      #ifndef WIN32
      case EAGAIN: *status = SYSLIMITREACHED; break;
      case EINVAL: *status = INVALIDARGUMENT; break;
      case ENOMEM: *status = OUTOFMEMORY; break;
      case EPERM:  *status = NOPERMISSION; break;
      #endif
      default:     *status = UNKNOWNERROR; break;
    }
  }

  return(*status == VISESUCCESS);
}


void
VFE_destroy(VFE vfe)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Destroy an instance of virtual FE
 *---------------------------------------------------------------------------*/
{
  void    *status;

  if (vfe) {
    FE_destroy(vfe->fe);
    /* Wait for the FE thread to exit */
    if (vfe->thread) pthread_join(vfe->thread, &status);
    if (vfe->message) MSG_destroy(vfe->message);
    if (vfe->mailbox) vfe->mailbox->Destroy(vfe->mailbox);
    vfe->imem->Free(vfe->imem, vfe);
  }
}


Bool
VFE_startListening(VFE vfe, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Start the Feature Extractor if it's not already running
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_startListening: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) 0, _STARTLISTENING, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vfe->message, &vstatus) &&
      MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VFE_verifyAck(vfe->message, command, _STARTLISTENING);
    MSG_closeRead(vfe->message, &cstatus);
  }

  /* Report success if successful or if it was already running */
  *status = (Int) (vstatus == UNTIMELYCOMMAND ? VISESUCCESS : vstatus);
  return(!(Bool) vstatus);
}


Bool
VFE_stopListening(VFE vfe, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Stop the Feature Extractor
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_stopListening: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) 0, _STOPLISTENING, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vfe->message, &vstatus) &&
      MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VFE_verifyAck(vfe->message, command, _STOPLISTENING);
    MSG_closeRead(vfe->message, &cstatus);
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_startCalibration(VFE vfe, Int maxFrames, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the Feature Extractor in noise calibration mode.
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_startCalibration: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) VINTSIZEINMELS, _STARTCALIBRATION, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeInt(vfe->message, (V_Int) maxFrames);
    if (MSG_closeWrite(vfe->message, &vstatus) &&
      MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VFE_verifyAck(vfe->message, command, _STARTCALIBRATION);
      MSG_closeRead(vfe->message, &cstatus);
    }
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_stopCalibration(VFE vfe, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Take the Feature Extractor out of noise calibration mode.
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_stopCalibration: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) 0, _STOPCALIBRATION, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vfe->message, &vstatus) &&
      MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VFE_verifyAck(vfe->message, command, _STOPCALIBRATION);
    MSG_closeRead(vfe->message, &cstatus);
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_getParameter(VFE vfe, UInt index, Int * value, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the value of an FE parameter
 *---------------------------------------------------------------------------*/
{
  V_Int    command, ivalue;
  V_Uns    senderId, uindex;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_getParameter: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) VINTSIZEINMELS, _GETPARAMETERS, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vfe->message, (V_Uns) index);
    if (MSG_closeWrite(vfe->message, &vstatus) &&
        MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VFE_verifyAck(vfe->message, command, _GETPARAMETERS);
      MSG_readUns(vfe->message, &uindex);
      MSG_readInt(vfe->message, &ivalue);
      MSG_closeRead(vfe->message, &cstatus);
      *value = (Int) ivalue;
    }
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_setParameter(VFE vfe, UInt index, Int value, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the value of an FE parameter
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_setParameter: index=%d value=%d\n", index, value));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) (2 * VINTSIZEINMELS), _SETPARAMETERS, COMMAND_PRIORITY, &vstatus)) {
    VBX_DEBUG(VBX_print("VFE_setParameter: MSG_openWrite SUCCEEDS\n"));
    MSG_writeUns(vfe->message, (V_Uns) index);
    MSG_writeInt(vfe->message, (V_Int) value);
    if (MSG_closeWrite(vfe->message, &vstatus) &&
        MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
      VBX_DEBUG(VBX_print("VFE_setParameter: doing VFE_verifyAck()\n"));
      vstatus = VFE_verifyAck(vfe->message, command, _SETPARAMETERS);
      MSG_closeRead(vfe->message, &cstatus);
    } else {
      VBX_DEBUG(VBX_print("VFE_setParameter: MSG_closeWrite FAILS\n"));
    }
  } else {
    VBX_DEBUG(VBX_print("VFE_setParameter: MSG_openWrite FAILS, vstatus=%d\n", vstatus));
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_getSN(VFE vfe, SN * sn, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get the current sequence number
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_getSN: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) 0, _GETSEQUENCENUMBER, COMMAND_PRIORITY, &vstatus)) {
    if (MSG_closeWrite(vfe->message, &vstatus) &&
        MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VFE_verifyAck(vfe->message, command, _GETSEQUENCENUMBER);
      MSG_readSN(vfe->message, sn);
      MSG_closeRead(vfe->message, &cstatus);
    }
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_getAudioData(VFE vfe, SN * startSN, SN *endSN, UShort ** data, Int * sampCnt, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Get audio data in the specified range
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;
  V_Int    frameLen;
  SN       frameCnt = 0;
  Int      i;
  UShort  *blk;
  V_ULong  longword;

  VBX_DEBUG(VBX_print("VFE_getAudioData: CALLED\n"));

  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) 2 * VSNSIZE / VAMESIZE, 
                    _GETAUDIOFRAMES, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeSN(vfe->message, *startSN);
    MSG_writeSN(vfe->message, *endSN);
    if (MSG_closeWrite(vfe->message, &vstatus) &&
        MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VFE_verifyAck(vfe->message, command, _GETAUDIOFRAMES);
      MSG_readSN(vfe->message, startSN);
      MSG_readSN(vfe->message, endSN);
      MSG_readSN(vfe->message, &frameCnt);
      MSG_readInt(vfe->message, &frameLen);
      MSG_readULong(vfe->message, (V_ULong *) &longword);
      MSG_closeRead(vfe->message, &cstatus);
    }
  }

  VBX_DEBUG(VBX_print("VFE_getAudioData: exchange done\n"));
		
 *data = (UShort *) longword;
 *sampCnt = frameCnt * frameLen;
  
  if (*sampCnt == 0) {
    VBX_print("VFE_getAudioData: WARNING - sampCnt is zero!\n");
    return(FALSE);
  }

  VBX_DEBUG(VBX_print("VFE_getAudioData: ALL DONE\n"));
		 
 *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_turnVUMeterOn(VFE vfe, UInt receiverId, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Turn amplitude reporting on
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  VBX_DEBUG(VBX_print("VFE_turnVUMeterOn: CALLED\n"));
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) VINTSIZEINMELS, _VUMETERON, COMMAND_PRIORITY, &vstatus)) {
    MSG_writeUns(vfe->message, (V_Uns) receiverId);
    if (MSG_closeWrite(vfe->message, &vstatus) &&
        MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
      vstatus = VFE_verifyAck(vfe->message, command, _VUMETERON);
      MSG_closeRead(vfe->message, &cstatus);
    }
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


Bool
VFE_turnVUMeterOff(VFE vfe, Int * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Turn amplitude reporting off
 *---------------------------------------------------------------------------*/
{
  V_Int    command;
  V_Uns    senderId;
  V_ULong  length;
  V_Err    vstatus, cstatus;

  /* VBX_print("VFE_turnVUMeterOff: CALLED\n"); */
  if (MSG_openWrite(vfe->message, vfe->feMBoxId, (V_ULong) 0, _VUMETEROFF, COMMAND_PRIORITY, &vstatus) &&
      MSG_closeWrite(vfe->message, &vstatus) &&
      MSG_openRead(vfe->message, &senderId, &length, &command, TRUE, &vstatus)) {
    vstatus = VFE_verifyAck(vfe->message, command, _VUMETEROFF);
    MSG_closeRead(vfe->message, &cstatus);
  }

  *status = (Int) vstatus;
  return(!(Bool) vstatus);
}


static V_Err
VFE_verifyAck(VBXMSG message, V_Int command, V_Int cmdExpected)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Verify the reply
 *---------------------------------------------------------------------------*/
{
  V_Int   vstatus;

  /* Verify that the message is an acknowledgment */
  if (command != _ACKNOWLEDGMENT) {
    VBX_DEBUG(VBX_print("VFE_verifyAck: message is not an ACKNOWLEDGMENT (command = %d)\n", command));
    vstatus = (V_Int) UNTIMELYCOMMAND;

  /* Get the command being acknowledged */
  } else if (!MSG_readInt(message, &command)) {
    VBX_DEBUG(VBX_print("VFE_verifyAck: cannot read command acknowledged\n"));
    vstatus = (V_Int) MESSAGETOOSHORT;

  /* Verify that the command is the one expected */
  } else if (command != cmdExpected) {
    VBX_DEBUG(VBX_print("VFE_verifyAck: command acknowledged (%d) is not that expected (%d)\n", command, cmdExpected));
    vstatus = (V_Int) UNTIMELYCOMMAND;

  /* Get the message status */
  } else if (!MSG_readInt(message, &vstatus)) {
    VBX_DEBUG(VBX_print("VFE_verifyAck: cannot read status\n"));
    vstatus = (V_Int) MESSAGETOOSHORT;
  }

  return((V_Err) vstatus);
}
