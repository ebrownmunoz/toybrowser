#ifndef _FE_H_
#define _FE_H_

#include "vbxtypes.h"
#include "viseerr.h"
#include "ifaces.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--- Configuration parameters defined according to available hardware -----*/
/* Uncomment following line if target processor has FPU */

#define VBX_USE_FLOATING_POINT

/* Uncomment the following line the single file test built */
/* #define FILE_TEST */

/*--------------------------------------------------------------------------*/
/* Front End Object pointer */

typedef struct fe_s *FE;

/*--------------------------------------------------------------------------*/
/* Exported functions */

extern FE    FE_create(IAudioIn *, IMail *, IMemory *, V_Err * status);
extern void  FE_run(FE fe);
extern void  FE_destroy(FE fe);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _FE_H_ */
