#include "pthr.h"
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "types.h"
#include "feversion.h"
#include "mmaudsrc.h"
#include "charsrc.h"
#include "queue.h"
#include "mkcep.h"
#include "mkjin.h"
#include "txtframe.h"
#include "objsnk.h"
#include "fe.h"
#include "visecomp.h"
#include "viseerr.h"
#include "viseparm.h"
#include "visecmds.h"
#include "visemsg.h"
#include "siesta.h"
#include "vbx.h"

#ifdef DEBUG
#define VBX_DEBUG(P) P
#define FE_PRINT_FRAMES
#else
#define VBX_DEBUG(P)
#endif

#define AUDIO_CLASSSIZE 5           /* Number of OBJT's for "audio results" in the audClass */
#define HDR_SIZE        512         /* NIST Header size (in V_Mel's) reserved at start of audio OBJT's */
#define DFLT_AUDIOQLEN  0           /* Default Audio Queue length */
#define MAXAMPS         8           /* 0.1s intervals of reporting */
#define DFLT_MAXSIGBITS 13          /* The default maximum number of significant bits in a sample */

#define OOPS_badException(INTERFACE, SNUM, REQSNUM, DIRECTION)          \
  FATAL_BRA_ "Unexpected exception %d (sn = %d) getting frame %d %s",   \
             IFACE_error((IFACE) (INTERFACE)), (SNUM), (REQSNUM),       \
             DIR_SYM(DIRECTION) _KET

#define FE_useExternalDSP(x)   ((x) >= 10000)

/* Feature Extractor Object */

typedef struct fe_s {
  /* External Interfaces */
  IAudioIn     *IAudioPtr;
  IMail        *IMailPtr;
  IMBox        *IMBoxPtr;
  IMemory      *imem;
  VBXMSG        msg;

  /* Feature Extractor Attributes */
  Int           sigBits;
  Bool          useExternalDSP;
  Bool          initialized;
  Bool          contiguousSN;

  /* OOPS Components */
  IGEN          igenMMAudSrc;      /* MMAudio Interface and Attributes */
  IGEN          igenMMAudSrcAttr;
  IMMAUDSRCATTR iMMAudSrcAttr;
  ISRC          isrcMMAudSrc;
  IGEN          igenCharSrc;       /* Character Source Interface and Attributes */
  IGEN          igenCharSrcAttr;
  ICHARSRCATTR  iCharSrcAttr;
  ICHARSRC      iCharSrc;
  ISRC          isrcCharSrc;
  IGEN          igenAudioQ;        /* Audio Queue Object and Attributes */
  IGEN          igenAudioQAttr;
  IQATTR        iAudioQAttr;
  IQUEUE        iqAudioQ;
  ISRC          isrcAudioQ;
  ISRC          isrcIqAudioQ;
  IGEN          igenMkcep;         /* Cep Front End Object and Attributes */
  IGEN          igenMkcepAttr;
  IMKCEPATTR    iMkcepAttr;
  IMKCEP        iMkcep;
  ISRC          isrcMkcep;
  IGEN          igenMkjin;         /* JIN Maker Object and Attributes */
  IGEN          igenMkjinAttr;
  IMKJIN        iMkjin;
  IMKJINATTR    iMkjinAttr;
  ISRC          isrcMkjin;
  IGEN          igenTxtFrame;      /* FRAME FROM TEXT Maker Object and Attributes */
  IGEN          igenTxtFrameAttr;
  ITXTFRAME     iTxtFrame;
  ITXTFRAMEATTR iTxtFrameAttr;
  ISRC          isrcTxtFrame;
  IGEN          igenSink;          /* The ultimate sink of frames */
  IOBJSINK      iSink;

  Int           audQueueLen;
  CLASS         audClass;          /* CLASS for returning audio to applications */
  Int           audFrameLen;       /* Current length of the frames in the audClass */
  UShort        amplitude;

  /* Components for VUMETER functionality */
  UShort        avamp;             /* Amplitude averaged over the last 8 frames */
  UShort        numAmps;           /* # of amplitudes currently in the buffer */
  UShort        amps[MAXAMPS];     /* Amplitude buffer */

} fe_t;

/* My internal function prototypes */
static V_Bool FE_msgReplyStatus(FE fe, V_Uns recvID, V_Int cmd, V_Err rstat);
static V_Err  FE_init(FE fe, V_Bool initAudio);
static V_Err  FE_getParameter(FE fe, V_Uns index, V_Int * value);
static V_Err  FE_setParameter(FE fe, V_Uns index, V_Int value);
static void   FE_reset(FE fe);
static V_Bool FE_addAmp(FE fe);
static void   FE_configOops(FE fe, V_Int version);
static void   FE_linkOops(IGEN source, IGEN sink);
static Bool   FE_deallocate(FE fe);

FE
FE_create(IAudioIn * iAudio, IMail * iMail, IMemory * imem, V_Err * status)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Create and initialize an FE object with default configuration
 * Return:  Pointer to initialized FE object in case of success, NULL 
 *          otherwise with error code set in status variable.
 * iAudio == NULL means text mode recognizer asking for FE generated from text
 *---------------------------------------------------------------------------*/
{
  FE  fe;
  V_Uns feID;
  V_Err stat;

  /* Allocate object. */
  if (!(fe = (FE) (imem->Calloc(imem, 1, sizeof(fe_t), FAST_MEMORY)))) {
    *status = OUTOFMEMORY;
    return NULL;
  }
  /* Initialize object */
  fe->IAudioPtr = iAudio;
  fe->IMailPtr = iMail;
  fe->imem = imem;
  fe->useExternalDSP = FALSE;
  fe->initialized = FALSE;
  fe->audQueueLen = DFLT_AUDIOQLEN;
  fe->sigBits = DFLT_MAXSIGBITS;

  /* Get unprioritized IMBox */
  if (!(fe->IMBoxPtr = iMail->CreateMBox(iMail, VISECOMP_FE, 1, status))) {
    fe->imem->Free(fe->imem, fe);
    return NULL;
  }
  /* Get msg object */
  if (!(fe->msg = MSG_create(fe->imem, fe->IMBoxPtr))) {
    *status = OUTOFMEMORY;
    fe->IMBoxPtr->Destroy(fe->IMBoxPtr);
    fe->imem->Free(fe->imem, fe);
    return NULL;
  }

  /* DEBUG */
  feID = iMail->MailBoxId(iMail, VISECOMP_FE, &stat);

  if (fe->IAudioPtr) {
    /* Create an MMAUDSRC */
    fe->igenMMAudSrcAttr = MMAUDSRCATTR_create(fe->imem);
    fe->iMMAudSrcAttr = (IMMAUDSRCATTR) IGEN_interface(fe->igenMMAudSrcAttr, IMMAUDSRCATTRID);
    fe->igenMMAudSrc = MMAUDSRC_create(fe->imem);
 
    /* Set the MMAUDSRC's input to be the given IAudioIn */
    IMMAUDSRCATTR_setIAudioIn(fe->iMMAudSrcAttr, (Ptr) iAudio);
    VBX_DEBUG(VBX_print("FE_create: MMAUDSRC created\n"));
    assert(fe->igenMMAudSrcAttr);
    assert(fe->iMMAudSrcAttr);
    assert(fe->igenMMAudSrc);

    /* Get its ISRC (output) interface */
    fe->isrcMMAudSrc = (ISRC) IGEN_interface(fe->igenMMAudSrc, ISRCID);
    assert(fe->isrcMMAudSrc);
  } else {
    /* Create an CHARSRC */
    fe->igenCharSrcAttr = CHARSRCATTR_create(fe->imem);
    assert(fe->igenCharSrcAttr);
    fe->iCharSrcAttr = (ICHARSRCATTR)IGEN_interface(fe->igenCharSrcAttr, ICHARSRCATTRID);
    assert(fe->iCharSrcAttr);
    fe->igenCharSrc = CHARSRC_create(fe->imem);
    assert(fe->igenCharSrc);
    fe->iCharSrc = (ICHARSRC)IGEN_interface(fe->igenCharSrc, ICHARSRCID);
    assert(fe->iCharSrc);
    fe->isrcCharSrc = (ISRC)IGEN_interface(fe->igenCharSrc, ISRCID);
    assert(fe->isrcCharSrc);
  }

  /* Create a MKCEPATTR where feVersion will reside */
  fe->igenMkcepAttr = MKCEPATTR_create(fe->imem);
  assert(fe->igenMkcepAttr);
  fe->iMkcepAttr = (IMKCEPATTR) IGEN_interface(fe->igenMkcepAttr, IMKCEPATTRID);
  assert(fe->iMkcepAttr);
  /* Set the default cepVersion */
  /* NOTE: Default is for FlexVISE! (no MKJIN created) */
  IMKCEPATTR_setCepVersion(fe->iMkcepAttr, FE_FVISE8KHzRastaOldNorm);
  /* This is for TEXTVISE */
  fe->contiguousSN = TRUE;

  if (fe->IAudioPtr) {
    /* Assume no Audio Queue (i.e., link the audio source directly to the cep maker) */
    fe->igenMkcep = MKCEP_create(fe->imem, fe->isrcMMAudSrc);
    VBX_DEBUG(VBX_print("FE_create: MKCEP created\n"));
    assert(fe->igenMkcep);

    /* Get its IMKCEP interface */
    fe->iMkcep = (IMKCEP) IGEN_interface(fe->igenMkcep, IMKCEPID);
    assert(fe->iMkcep);

    /* Get its ISRC (output) interface */
    fe->isrcMkcep = (ISRC) IGEN_interface(fe->igenMkcep, ISRCID);
    assert(fe->isrcMkcep);

    /* Create an OBJSINK, passing it the MKCEP's ISRC as source */
    fe->igenSink = OBJSINK_create(fe->imem, fe->isrcMkcep);
    assert(fe->igenSink);
  } else {
    /* Without audio configure it as producing text frames for VISE or FLEXVise according to cepVersion */
    fe->igenTxtFrameAttr = TXTFRAMEATTR_create(fe->imem);
    assert(fe->igenTxtFrameAttr);
    fe->iTxtFrameAttr = (ITXTFRAMEATTR)IGEN_interface(fe->igenTxtFrameAttr, ITXTFRAMEATTRID);
    assert(fe->iTxtFrameAttr);
    fe->igenTxtFrame = TXTFRAME_create(fe->imem, fe->isrcCharSrc);
    assert(fe->igenTxtFrame);
    fe->iTxtFrame = (ITXTFRAME)IGEN_interface(fe->igenTxtFrame, ITXTFRAMEID);
    assert(fe->iTxtFrame);
    fe->isrcTxtFrame = (ISRC)IGEN_interface(fe->igenTxtFrame, ISRCID);
    assert(fe->isrcTxtFrame);
    /* Create an OBJSINK, passing it the TXTFRAME's ISRC as source */
    fe->igenSink = OBJSINK_create(fe->imem, fe->isrcTxtFrame);
    assert(fe->igenSink);
  }
  /* Get an interface to the OBJSINK */
  fe->iSink = (IOBJSINK) IGEN_interface(fe->igenSink, IOBJSINKID);
  assert(fe->iSink);

  return fe;
}

void
FE_destroy(FE fe)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Shut down FE_run and free dynamically allocated resources
 *---------------------------------------------------------------------------*/
{
  IMBox *mailbox;
  V_Uns feMboxId;
  VBXMSG message;
  V_Int command, dstatus;
  V_Uns senderId;
  V_ULong length;
  V_Err status;

  /* If FE is running, send a _DESTROY command to it */
  if (fe) {
    if ((feMboxId = fe->IMailPtr->MailBoxId(fe->IMailPtr, VISECOMP_FE, &status))) {
      if ((mailbox = fe->IMailPtr->CreateMBox(fe->IMailPtr, VISECOMP_FE "_DESTROY", 1, &status))) {
        if (message = MSG_create(fe->imem, mailbox)) {
          if (MSG_openWrite(message, feMboxId, (V_ULong) 0, _DESTROY, COMMAND_PRIORITY, &status) &&
              MSG_closeWrite(message, &status) &&
              MSG_openRead(message, &senderId, &length, &command, TRUE, &status)) {
            MSG_readInt(message, &command);
            MSG_readInt(message, &dstatus);
            MSG_closeRead(message, &status);
          }
          MSG_destroy(message);
        }
        mailbox->Destroy(mailbox);
      }
      /* If _DESTROY reaches FE, FE_run destroys the FE descriptor; otherwise, do it here */
      if (status) {
        VBX_DEBUG(VBX_print("FE::INFO: Calling FE_deallocate() from FE_destroy() [1]"));
        FE_deallocate(fe);
      }
      /* Otherwise, FE is not running, so just destroy the FE descriptor */
    } else {
      FE_deallocate(fe);
      VBX_DEBUG(VBX_print("FE::INFO: Calling FE_deallocate() from FE_destroy() [2]"));
    }
  }
}

static Bool
FE_deallocate(FE fe) {
  Bool rval;

  MSG_destroy(fe->msg);
  rval = IGEN_annihilate(fe->igenSink);
  assert(rval);

  if (fe->igenMkjin != NULL) {
    rval = IGEN_annihilate(fe->igenMkjin);
    assert(rval);
    fe->igenMkjin = NULL;
    fe->iMkjin = NULL;
    fe->isrcMkjin = NULL;
  }
  if (fe->igenMkjinAttr != NULL) {
    rval = IGEN_annihilate(fe->igenMkjinAttr);
    assert(rval);
    fe->igenMkjinAttr = NULL;
    fe->iMkjinAttr = NULL;
  }

  if (fe->IAudioPtr) {
    rval = IGEN_annihilate(fe->igenMkcep);
    assert(rval);
  } else {
    rval = IGEN_annihilate(fe->igenTxtFrame);
    assert(rval);
    rval = IGEN_annihilate(fe->igenTxtFrameAttr);
    assert(rval);
  }

  rval = IGEN_annihilate(fe->igenMkcepAttr);
  assert(rval);

  if (fe->igenAudioQ != NULL) {
    rval = IGEN_annihilate(fe->igenAudioQ);
    assert(rval);
    fe->igenAudioQ = NULL;
  }
  if (fe->igenAudioQAttr != NULL) {
    rval = IGEN_annihilate(fe->igenAudioQAttr);
    assert(rval);
    fe->igenAudioQAttr = NULL;
  }

  if (fe->IAudioPtr) {
    rval = IGEN_annihilate(fe->igenMMAudSrc);
    assert(rval);
    rval = IGEN_annihilate(fe->igenMMAudSrcAttr);
    assert(rval);
  } else {
    rval = IGEN_annihilate(fe->igenCharSrc);
    assert(rval);
    rval = IGEN_annihilate(fe->igenCharSrcAttr);
    assert(rval);
  }
  fe->imem->Free(fe->imem, fe);
  return TRUE;
}

#define EXIT { goto Exit; }

void
FE_run(FE fe)
{
  V_Uns  searchID = 0;
  V_Uns  sourceID = 0;
  V_Uns  sendAmpID = 0;
  V_Bool audioOk;
  V_SN   seqNumber = (V_SN) 0;
  V_ULong length, lword;
  V_Int  command;
  V_Err  status;
  V_Err  retStatus;
  V_Bool feRunning = FALSE;
  V_Uns  index;
  V_Int  value;
  CLASS  frameClass = NULL;
  OBJT   frame = NULL;
  V_Int *iPtr;
  SN     startSN = 0, endSN = 0, frameCnt = 0;
  SN     sn, snReq;
  Int    frameShift;
  Int    frameLen;
  Int    audObjLen;
  Int    cepVecLen;
  Int    msgDataLen;
  Int    i, exc, rval;
  IGEN   igenIqSnk;
  IOBJSINK iosIqSnk;
  V_Byte *bPtr;
  V_Mel  mel;
  OBJT   objt;
  UShort *blk;

  while (TRUE) {
    VBX_DEBUG(VBX_print("FE: awaiting message\n"));

    /* Get message, blocking on mailbox */
    if (!MSG_openRead(fe->msg, &sourceID, &length, &command, TRUE, &status)) {
      WARN_BRA_ "FE exiting..." _KET;
      EXIT;
    }

    /* Get searchID, if have not got one yet */
    if (!searchID && !(searchID = fe->IMailPtr->MailBoxId(fe->IMailPtr, VISECOMP_SE, &status))) {
      WARN_BRA_ "FE exiting..." _KET;
      EXIT;
    }

    /* Decipher the message */
    VBX_DEBUG(VBX_print("FE: (idle) RECEIVED COMMAND %d\n", command));
    switch (command) {

    case _STARTLISTENING:

      /* Set running state */
      feRunning = TRUE;

      if (fe->IAudioPtr) {
        /* Reset FE and try to start the Audio source */
        FE_reset(fe);
        fe->IAudioPtr->Start(fe->IAudioPtr);
        status = fe->IAudioPtr->Status(fe->IAudioPtr);
      } else {
        ICHARSRC_Start(fe->iCharSrc);
        status = VISESUCCESS;
      }

      if (!FE_msgReplyStatus(fe, sourceID, command, status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* If the Audio has failed to start successfully, go back to the blocking MSG_openRead() */
      if (status)
        continue;

      /* Otherwise, go to the feature production */
      else
        break;

    case _STOPLISTENING:

      /* No action needed. */
      if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _STARTCALIBRATION:

      if (!MSG_readUns(fe->msg, (V_Uns *) &value)) {
        /* Complain and Go back to the blocking MSG_openRead() */
        if (!FE_msgReplyStatus(fe, sourceID, command, MESSAGETOOSHORT)) {
          WARN_BRA_ "FE exiting..." _KET;
          EXIT;
        }

        continue;
      }
      if (fe->IAudioPtr)
        IMKCEP_startCalibration(fe->iMkcep, (Int)value);
      if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _STOPCALIBRATION:

      if (fe->IAudioPtr)
        IMKCEP_stopCalibration(fe->iMkcep);
      if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _GETPARAMETERS:
      /* Read index */
      if (!MSG_readUns(fe->msg, &index)) {
        /* Complain and Go back to the blocking MSG_openRead() */
        if (!FE_msgReplyStatus(fe, sourceID, command, MESSAGETOOSHORT)) {
          WARN_BRA_ "FE exiting..." _KET;
          EXIT;
        }
        continue;
      }
      MSG_closeRead(fe->msg, &status);
      /* Do the job */
      retStatus = FE_getParameter(fe, index, &value);

      /* Report results */
      if (!MSG_openWrite(fe->msg, sourceID, (V_ULong) 4, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Write in command, retStatus, value. */
      MSG_writeInt(fe->msg, command);
      MSG_writeInt(fe->msg, (V_Int) retStatus);
      MSG_writeUns(fe->msg, index);
      MSG_writeInt(fe->msg, value);

      if (!MSG_closeWrite(fe->msg, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _GETSEQUENCENUMBER:
      MSG_closeRead(fe->msg, &status);

      if (!fe->contiguousSN)
        fe->IAudioPtr->TotalGet(fe->IAudioPtr, &seqNumber);

      /* Report */
      if (!MSG_openWrite(fe->msg, sourceID, (V_ULong) (2 + VSNSIZE / VAMESIZE), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Write in command, Status, seqNumber. */
      MSG_writeInt(fe->msg, command);
      MSG_writeInt(fe->msg, VISESUCCESS);
      MSG_writeSN(fe->msg, seqNumber);

      if (!MSG_closeWrite(fe->msg, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _SETPARAMETERS:
      /* Read index */
      MSG_readUns(fe->msg, &index);

      /* Read value, which is to be unsigned for now !!! */
      if (!MSG_readUns(fe->msg, (V_Uns *) & value)) {
        /* Complain and Go back to the blocking MSG_openRead() */
        if (!FE_msgReplyStatus(fe, sourceID, command, MESSAGETOOSHORT)) {
          WARN_BRA_ "FE exiting..." _KET;
          EXIT;
        }

        continue;
      }

      /* Do the job */
      retStatus = FE_setParameter(fe, index, value);
      if (!FE_msgReplyStatus(fe, sourceID, command, retStatus)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _DESTROY:
      /* Try to notify Sender */
      FE_msgReplyStatus(fe, sourceID, command, DESTROYED);
      WARN_BRA_ "FE exiting..." _KET;
      EXIT;

    case _GETAUDIOFRAMES:
      MSG_readSN(fe->msg, &startSN);
      MSG_readSN(fe->msg, &endSN);

      MSG_closeRead(fe->msg, &status);

      if (fe->audQueueLen > 0) {
        assert(fe->igenAudioQ != NULL);
        if (startSN == 0 && endSN == 0) IQUEUE_range(fe->iqAudioQ, &startSN, &endSN);
        IQUEUE_frameCount(fe->iqAudioQ, &startSN, &endSN, &frameCnt);
        frameCnt = (frameCnt > fe->audQueueLen) ? fe->audQueueLen : frameCnt;
        VBX_DEBUG(VBX_print("FE_run: _GETAUDIOFRAMES startSN %ld endSN %ld frameCnt %ld\n", startSN, endSN, frameCnt));
        IMMAUDSRCATTR_getFrameLen(fe->iMMAudSrcAttr, &frameLen);
      } else {
        VBX_print("FE_run: WARNING - GETAUDIOFRAMES requested w/o QUEUE configured\n");
        startSN = endSN = frameCnt = 0;
        frameLen = 0;
      }

      if (frameCnt > 0) {
        audObjLen = ((fe->audQueueLen + 1) * frameLen + HDR_SIZE) * sizeof(V_Mel);
        if (fe->audClass == NULL || audObjLen > CLASS_objectSize(fe->audClass)) {
          if (fe->audClass != NULL) {
            VBX_print("FE: INFO - recreating audio results CLASS (%d>%d)\n", audObjLen, CLASS_objectSize(fe->audClass));
            CLASS_destroy(fe->audClass, NULL, NULL);
          }
          fe->audClass = CLASS_create(fe->imem, AUDIO_CLASSSIZE, audObjLen, NULL, NULL);    
          fe->audFrameLen = frameLen;
        }

        objt = OBJT_new(fe->audClass);
        if (objt != NULL) {
          blk = (UShort *) &OBJT_contents(objt);
        } else {
          VBX_print("FE: WARNING - audio results CLASS exhausted!\n");
          continue;
        }
      }
      VBX_DEBUG(VBX_print("FE: _GETAUDIOFRAMES frameCnt %d objt 0x%x\n", frameCnt, objt));

      if (!fe->audQueueLen || blk == NULL) 
        continue;

      /* Create an OBJSINK and connect to IQUEUE's SRC */
      igenIqSnk = OBJSINK_create(fe->imem, fe->isrcIqAudioQ);
      assert(igenIqSnk);

      /* Get an interface to the OBJSINK */
      iosIqSnk = (IOBJSINK) IGEN_interface(igenIqSnk, IOBJSINKID);
      assert(iosIqSnk);

      /* Copy the audio frames into the OBJT */
      sn = startSN;
      if (frameCnt > 0) {
        VBX_DEBUG(VBX_print("FE: assembling %d frames for xmit\n", frameCnt));
        for (i = 0; i < frameCnt; i++) {
          if (!IOBJSINK_getFrame(iosIqSnk, FORWARD, &sn, &frame)) {
            /* OOPS_badException(iosIqSnk, sn, sn, FORWARD); */
            VBX_print("FE: frame processing loop incomplete due to OOPS exception\n");
            break;
          }
          memcpy(&blk[i * frameLen], &OBJT_contents(frame), frameLen * sizeof(UShort));
          OBJT_free(frame);
          sn = -1;
        }
        VBX_DEBUG(VBX_print("FE: frame assembly complete, tearing down sink..\n"));

        /* Annihilate the aux sink */
        rval = IGEN_annihilate(igenIqSnk);
        assert(rval);
      }

      /* Announce what is going to be sent */
      if (!MSG_openWrite(fe->msg, sourceID, (V_ULong) (3 + 3 * VSNSIZE / VAMESIZE + VLONGSIZEINMELS), _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Send the command, status, starting and ending SN's, frame count, frame length and a pointer to the OBJT */
      /* The latter MAY NOT BE PORTABLE!!! */
      MSG_writeInt(fe->msg, command);
      MSG_writeInt(fe->msg, VISESUCCESS);

      MSG_writeSN(fe->msg, startSN);
      MSG_writeSN(fe->msg, endSN);
      MSG_writeSN(fe->msg, frameCnt);
      MSG_writeInt(fe->msg, (V_Int) frameLen);
      MSG_writeULong(fe->msg, (V_ULong) objt);

      if (!MSG_closeWrite(fe->msg, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      VBX_DEBUG(VBX_print("FE: _GETAUDIOFRAMES ALL DONE\n"));
      continue;

    case _VUMETERON:
      /* Read sendAmpID */
      if (!MSG_readUns(fe->msg, &sendAmpID)) {
        /* Complain and Go back to the blocking MSG_openRead() */
        if (!FE_msgReplyStatus(fe, sourceID, command, MESSAGETOOSHORT)) {
          WARN_BRA_ "FE exiting..." _KET;
          EXIT;
        }

        continue;
      }
      /* No action needed. */

      if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;

    case _VUMETEROFF:
      /* Turn VUmeter off */
      sendAmpID = 0;

      if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;
    default:
      if (!FE_msgReplyStatus(fe, sourceID, command, ILLEGALCOMMAND)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Go back to the blocking MSG_openRead() */
      continue;
    }

    /* FE has received a _STARTLISTENING command and has turned on the audio source */

    /* Process incoming data */
    IMKCEPATTR_getCepVecLen(fe->iMkcepAttr, &cepVecLen);
    IMKCEPATTR_getFrameShift(fe->iMkcepAttr, &frameShift);
    if (fe->useExternalDSP) {
      frameClass = CLASS_create(fe->imem, 1, cepVecLen * sizeof(Float), NULL, NULL);
      assert(frameClass != NULL);
    }

    do {
      if (!fe->useExternalDSP) {
        sn = -1;

        snReq = sn;
        frame = NULL;
        if (!(audioOk = IOBJSINK_getFrame(fe->iSink, FORWARD, &sn, &frame))) {
          exc = IFACE_error((IFACE) fe->iSink);
          if (exc == EXC_EOU) {
            VBX_DEBUG(VBX_print("FE_run: ABORT on EXC_EOU\n"));
            if (frame)
              OBJT_free(frame);
            feRunning = FALSE;
            break;
          } else {
            OOPS_badException(fe->iSink, sn, snReq, FORWARD);
          }
        }
        if (fe->contiguousSN)
          sn = seqNumber++;
        else
          seqNumber = sn;
      } else {
        frame = OBJT_new(frameClass);
        assert(frame != NULL);
        iPtr = (V_Int *) &OBJT_contents(frame);
        if (!(audioOk = fe->IAudioPtr->Read(fe->IAudioPtr, iPtr, cepVecLen * 2))) {
          feRunning = FALSE;
          OBJT_free(frame);
          CLASS_destroy(frameClass, NULL, NULL);
          frameClass = NULL;
          break;
        }
        seqNumber++;   /*!! THIS BRANCH IS NOW BROKEN - FIX !!*/
      }

      if (frame != NULL) {
        /* Got frame. Send it to the Search: sn, frame packed */
        if (!MSG_openWrite(fe->msg, searchID, (V_ULong) (VSNSIZE / VAMESIZE + OBJT_size(frame)/2), _ACOUSTICFEATURES, DATA_PRIORITY, &status)) {
          /*!! CAV This is the site of a long-term failure */
          WARN_BRA_ "FE exiting because MSG_openWrite FAILS with status %s",  VISEERR_string(status) _KET;
          EXIT;
        }
        
        MSG_writeSN(fe->msg, sn);
        MSG_writeMels(fe->msg, (V_Mel *) &OBJT_contents(frame), OBJT_size(frame)/2);

        /* Define amplitude for possible use in VUMeter */
        bPtr = (V_Byte *) &OBJT_contents(frame);
        fe->amplitude = bPtr[0];

#if defined(FE_PRINT_FRAMES) && !defined(VXWORKS)
        VBX_print("  FE::SNJIN: " QuadFmt(-) "; ", sn);
        for (i = 0; i < OBJT_size(frame)/2; i++) {
          VBX_print("%3u %3u ", bPtr[i << 1], bPtr[(i << 1) + 1]);
        }
        VBX_print("\n");
#endif
        
        if (!MSG_closeWrite(fe->msg, &status)) {
          WARN_BRA_ "FE exiting because MSG_closeWrite FAILS with status %s",  VISEERR_string(status) _KET;
          EXIT;
        }

        /* If VUmeterOn and time is right, send volume to specified sink */
        if (fe->IAudioPtr && sendAmpID && FE_addAmp(fe) &&
        MSG_openWrite(fe->msg, sendAmpID, (V_ULong) (1 + VSNSIZE / VAMESIZE), _AMPLITUDE, DATA_PRIORITY, &status)) {
          MSG_writeUns(fe->msg, fe->avamp);
          MSG_writeSN(fe->msg, seqNumber);
          if (!MSG_closeWrite(fe->msg, &status)) {
            if (MAILNOSUCHMBOX == status) {
              sendAmpID = 0;
            } else {
              WARN_BRA_ "FE exiting because MSG_closeWrite FAILS with status %s",  VISEERR_string(status) _KET;
              EXIT;
            }
          }
        }
      }
      OBJT_free(frame);

      /* Check mail */
      if (MSG_numUnread(fe->msg)) {
        /* Get message. Don't block on the mailbox */
        if (!MSG_openRead(fe->msg, &sourceID, &length, &command, FALSE, &status)) {
          WARN_BRA_ "FE exiting..." _KET;
          EXIT;
        }

        /* Decipher the message */
        VBX_DEBUG(VBX_print("FE: (fpl) RECEIVED COMMAND %d\n", command));
        switch (command) {

        case _STOPLISTENING:    /* Must be from Search */
          /* Shut down feature production */
          feRunning = FALSE;

          if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
            WARN_BRA_ "FE exiting..." _KET;
            EXIT;
          }

          /* Go back to the blocking MSG_openRead() */
          break;

        case _GETSEQUENCENUMBER:
          MSG_closeRead(fe->msg, &status);

          /* Report */
          if (!MSG_openWrite(fe->msg, sourceID, (V_ULong) (2 + VSNSIZE / VAMESIZE),
                             _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status)) {
            WARN_BRA_ "FE exiting..." _KET;
            EXIT;
          }

          /* Write in command, Status, seqNumber. */
          MSG_writeInt(fe->msg, command);
          MSG_writeInt(fe->msg, VISESUCCESS);
          MSG_writeSN(fe->msg, seqNumber);

          if (!MSG_closeWrite(fe->msg, &status)) {
            WARN_BRA_ "FE exiting..." _KET;
            EXIT;
          }

          /* Continue feature production */
          break;

        case _DESTROY:
          /* Try to notify Sender */
          FE_msgReplyStatus(fe, sourceID, command, DESTROYED);
          WARN_BRA_ "FE exiting..." _KET;
          EXIT;

        case _VUMETERON:        /* May come from Search or Proxy */
          /* Read sendAmpID */
          if (!MSG_readUns(fe->msg, &sendAmpID)) {
            /* Complain and Continue feature production */
            if (!FE_msgReplyStatus(fe, sourceID, command, MESSAGETOOSHORT)) {
              WARN_BRA_ "FE exiting..." _KET;
              EXIT;
            }

            break;
          }

          /* No action needed. */
          if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
            WARN_BRA_ "FE exiting..." _KET;
            EXIT;
          }

          /* Continue feature production */
          break;

        case _VUMETEROFF:       /* May come from Search or Proxy */
          /* Turn VUmeter off */
          sendAmpID = 0;

          if (!FE_msgReplyStatus(fe, sourceID, command, VISESUCCESS)) {
            WARN_BRA_ "FE exiting..." _KET;
            EXIT;
          }

          /* Continue feature production */
          break;

        /* Process untimely sent commands from any source. Complain */
        case _STARTLISTENING:  
        case _STARTCALIBRATION:
        case _STOPCALIBRATION:
        case _GETPARAMETERS:
        case _SETPARAMETERS:
          FE_msgReplyStatus(fe, sourceID, command, UNTIMELYCOMMAND);

          /* Continue feature production */
          break;
        default:
          FE_msgReplyStatus(fe, sourceID, command, ILLEGALCOMMAND);

          /* Continue feature production */
        }
      }
    } while (feRunning);
 
    /* Clean up allocations */
    CLASS_destroy(frameClass, NULL, NULL);

    /* Stop Audio */
    if (fe->IAudioPtr)
      fe->IAudioPtr->Stop(fe->IAudioPtr);
    else
      ICHARSRC_Stop(fe->iCharSrc);
      
    if (!audioOk) {
#ifdef FILE_TEST
      WARN_BRA_ "FE exiting..." _KET;
      EXIT;
#else

      /* Inform the Search about the exception. */

      if (!MSG_openWrite(fe->msg, searchID, (V_ULong) 1, _EXCEPTION, DATA_PRIORITY, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }

      /* Write in reporting status */
      MSG_writeInt(fe->msg, ENDOFDATA);

      if (!MSG_closeWrite(fe->msg, &status)) {
        WARN_BRA_ "FE exiting..." _KET;
        EXIT;
      }
#endif
    }
    /* Go back to the blocking MSG_openRead() */
  }
Exit:
  INFO_BRA_ "FE_Run: Calling FE_deallocate() on EXIT" _KET;
  FE_deallocate(fe);
  siesta(100000);
}

static V_Bool
FE_msgReplyStatus(FE fe, V_Uns receiverID, V_Int cmd, V_Err retStatus)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reply to specified command with _ACKNOWLEDGMENT command and retStatus
 * Return: TRUE upon success, FALSE if MSG routines fail
 *---------------------------------------------------------------------------*/
{
  V_Err status;

  MSG_closeRead(fe->msg, &status);
  if (!MSG_openWrite(fe->msg, receiverID, (V_ULong) 2, _ACKNOWLEDGMENT, COMMAND_PRIORITY, &status))
    return FALSE;

  MSG_writeInt(fe->msg, cmd);
  MSG_writeInt(fe->msg, (V_Int) retStatus);

  if (!MSG_closeWrite(fe->msg, &status) && (OUTOFMEMORY == status || MAILBREAKDOWN == status))
    return FALSE;

  return TRUE;
}

static V_Err
FE_init(FE fe, V_Bool initAudio)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Initialize the specified FE object for the currently configured cepVersion.
 * The FE object must already have valid interfaces.
 * Return: VISESUCCESS in case of success, otherwise an error code
 *---------------------------------------------------------------------------*/
{
  Int     sampleRate;
  Int     frameShift;
  UShort  detritus;

  if (fe->IAudioPtr) {
    /* Initialize the MKCEP */
    if (!IGEN_initialize(fe->igenMkcep, (IATTR) fe->iMkcepAttr)) {
      WARN_BRA_ "Can't initialize MKCEP: error %d", IFACE_error((IFACE) fe->igenMkcep) _KET;
      return BADFEVERSION;
    }
    if (!IMKCEP_setSigBits(fe->iMkcep, fe->sigBits))
      return BADPARVALUE;

    IMKCEPATTR_getSampleRate(fe->iMkcepAttr, &sampleRate);
    IMKCEPATTR_getFrameShift(fe->iMkcepAttr, &frameShift);
    IMKCEP_reset(fe->iMkcep);
    
    if (fe->useExternalDSP)
      frameShift = sizeof(Float);

    /* Reinitialize the audio source if requested */
    if (initAudio) {
      if (!fe->IAudioPtr->Init(fe->IAudioPtr, sampleRate, frameShift, &detritus))
        return CANTINITIALIZEAUDIO;
    }
  } else {
    IGEN_initialize(fe->igenCharSrc, (IATTR) fe->iCharSrcAttr);
  }
  if (fe->audQueueLen > 0) {
    if (fe->igenAudioQ == NULL) {
       /* Create a QUEUE for audio data frames, passing it the MMAUDSRC's ISRC as source */
      fe->igenAudioQAttr = QATTR_create(fe->imem);
      assert(fe->igenAudioQAttr);
      fe->iAudioQAttr = (IQATTR) IGEN_interface(fe->igenAudioQAttr, IQATTRID);
      assert(fe->iAudioQAttr);
      fe->igenAudioQ = QUEUE_create(fe->imem, fe->isrcMMAudSrc);
      assert(fe->igenAudioQ);
      VBX_DEBUG(VBX_print("FE_init: QUEUE created\n"));
      fe->isrcAudioQ = (ISRC) IGEN_interface(fe->igenAudioQ, ISRCID);
      assert(fe->isrcAudioQ);
      fe->iqAudioQ = (IQUEUE) IGEN_interface(fe->igenAudioQ, IQUEUEID);
      assert(fe->iqAudioQ);
      fe->isrcIqAudioQ = (ISRC) IGEN_interface(fe->iqAudioQ, ISRCID);
      assert(fe->isrcIqAudioQ);
    }
    FE_linkOops(fe->igenAudioQ, fe->igenMkcep);
     /* (Re)initialize the audio QUEUE */
    IQATTR_setSize(fe->iAudioQAttr, fe->audQueueLen);
    IQATTR_setFwdTol(fe->iAudioQAttr, SN_MAX);
    IQATTR_setBwdTol(fe->iAudioQAttr, SN_MAX);
    IQATTR_setFAQ(fe->iAudioQAttr, FALSE);
    if (!IGEN_initialize(fe->igenAudioQ, (IATTR) fe->iAudioQAttr))
      FATAL_BRA_ "Can't initialize audio QUEUE: error %d", IFACE_error((IFACE) fe->igenAudioQ) _KET;
  } else {
    if (fe->igenAudioQ != NULL) {
      IGEN_annihilate(fe->igenAudioQ);
      fe->igenAudioQ = NULL;
      fe->isrcAudioQ = NULL;
      fe->iqAudioQ = NULL;
      fe->isrcIqAudioQ = NULL;
      IGEN_annihilate(fe->igenAudioQAttr);
      fe->igenAudioQAttr = NULL;
      fe->iAudioQAttr = NULL;
    }
    if (fe->IAudioPtr)
      FE_linkOops(fe->igenMMAudSrc, fe->igenMkcep);
  }

  if (fe->IAudioPtr) {
    /* (Re)initialize the MMAUDSRC with the frame size gotten from mkcep and a large enough CLASS of audio frames to fill the audio QUEUE */
    IMMAUDSRCATTR_setFrameLen(fe->iMMAudSrcAttr, frameShift);
    IMMAUDSRCATTR_setClassSize(fe->iMMAudSrcAttr, fe->audQueueLen + 3);
    if (!IGEN_initialize(fe->igenMMAudSrc, (IATTR) fe->iMMAudSrcAttr))
      FATAL_BRA_ "Can't initialize MMAUDSRC: error %d", IFACE_error((IFACE) fe->igenMkcep) _KET;

    /* (Re)initialize MKJIN, if extant */
    if (fe->iMkjinAttr != NULL && fe->igenMkjin != NULL) {
      IMKJINATTR_setFrameBytes(fe->iMkjinAttr, 2*frameShift);
      if (!IGEN_initialize(fe->igenMkjin, (IATTR) fe->iMkjinAttr))
        FATAL_BRA_ "Can't initialize the MKJIN: error %d", IFACE_error((IFACE) fe->igenMkjin) _KET;
      IMKJIN_reset(fe->iMkjin);
    }
  } else if (fe->iTxtFrameAttr != NULL && fe->igenTxtFrame != NULL) {
    /* (Re)initialize TXTFRAME */
    Int cepVersion;
    IMKCEPATTR_getCepVersion(fe->iMkcepAttr, &cepVersion);
    ITXTFRAMEATTR_setFrameSize(fe->iTxtFrameAttr, cepVersion);
    IGEN_initialize(fe->igenTxtFrame, (IATTR) fe->iTxtFrameAttr);
  }
  fe->initialized = fe->initialized || initAudio;
  VBX_DEBUG(VBX_print("FE_init(%d) successful\n", initAudio));
  return VISESUCCESS;
}

static V_Err
FE_getParameter(FE fe, V_Uns index, V_Int * value)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Report the value of the parameter specified by "index"
 * Return: VISESUCCESS for success, otherwise a an error code
 *---------------------------------------------------------------------------*/
{
  Int ival;

  switch (index) {
  case AUDIOLEVEL:
    if (!fe->IAudioPtr || !fe->IAudioPtr->LevelGet(fe->IAudioPtr, (V_Uns *) value))
      return NOTSUPPORTED;
  case AUDQUEUELEN:
   *value = fe->audQueueLen;
  case FEVERSION:
    IMKCEPATTR_getCepVersion(fe->iMkcepAttr, &ival);
   *value = (V_Int) ival;
    break;
  case FRAMESHIFT:
    IMKCEPATTR_getFrameShift(fe->iMkcepAttr, &ival);
   *value = (V_Int) ival;
    break;
  case SAMPLERATE:
    IMKCEPATTR_getSampleRate(fe->iMkcepAttr, &ival);
   *value = (V_Int) ival;
    break;
  case SIGBITS:
   *value = (V_Int) fe->sigBits;
    break;
  case NOISETRACKING:
   *value = (V_Int)IMKCEP_getNoiseTracking(fe->iMkcep);
    break;
  default:
    return BADPARINDEX;
  }

  return VISESUCCESS;
}

static V_Err
FE_setParameter(FE fe, V_Uns index, V_Int value)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Set the value of the parameter specified by "index".  The parameter
 * specified must be in the repertoire.
 * Return: VISESUCCESS for success, otherwise a an error code
 *---------------------------------------------------------------------------*/
{
  Int   oldFeVersion;
  V_Int status = VISESUCCESS;

  switch (index) {

  case FEVERSION:
    IMKCEPATTR_getCepVersion(fe->iMkcepAttr, &oldFeVersion);
    if (value != oldFeVersion || !fe->initialized) {
      fe->useExternalDSP = FE_useExternalDSP(value);

      /* Save important settings for restoring previous state */
      IMKCEPATTR_setCepVersion(fe->iMkcepAttr, (Int) value);
      if (fe->IAudioPtr)
        FE_configOops(fe, value);

      if (VISESUCCESS != (status = FE_init(fe, TRUE))) {
        /* Try to recover */
        IMKCEPATTR_setCepVersion(fe->iMkcepAttr, oldFeVersion);
        if (fe->IAudioPtr)
          FE_configOops(fe, oldFeVersion);

        /* If it was bad version, FE is now in the previous state */
        if (BADFEVERSION != status) {
          /* IAudioIn::Init failed. Try to reinitialize FE */
          if (VISESUCCESS == FE_init(fe, TRUE)) {
            /* Restore other settings, which may have been changed long ago. */
          } else {
            /* Disaster !!! */
            return VISEFAILURE;
          }
        }
      }
    }
    break;

  case AUDQUEUELEN:
    if (value < 0)
      return BADPARVALUE;

    if (!fe->IAudioPtr || fe->audQueueLen == (Int) value)
      break;

    fe->audQueueLen = (Int) value;

    VBX_DEBUG(VBX_print("FE_setParameter: Reconfiguring for AudioQLen %d\n", fe->audQueueLen));
    if (VISESUCCESS != (status = FE_init(fe, FALSE))) {
      VBX_print("FE_setParameter: SEVERE ERROR - FE_init returns %d\n", status);
      return VISEFAILURE;
    }
    break;

  case SIGBITS:
    if (fe->IAudioPtr) {
      /* Initialize the MKCEP */
      if (!IGEN_initialize(fe->igenMkcep, (IATTR) fe->iMkcepAttr)) {
        VBX_print("FE_setParameter: SEVERE ERROR - Can't initialize MKCEP: error %d\n", IFACE_error((IFACE) fe->igenMkcep));
        return BADFEVERSION;
      }
      if (!IMKCEP_setSigBits(fe->iMkcep, (Int) value)) {
        VBX_print("FE_setParameter: SEVERE ERROR - Can't set sigbits to %d\n", (Int) value);
        return BADPARVALUE;
      }
    }
    fe->sigBits = (Int) value;
    break;

  case AUDIOLEVEL:
    if (!fe->IAudioPtr || !fe->IAudioPtr->LevelSet(fe->IAudioPtr, (V_Uns) value))
      return NOTSUPPORTED;
    break;

  case NOISETRACKING:
    IMKCEP_setNoiseTracking(fe->iMkcep, (Int) value);
    break;

  default:
    return BADPARINDEX;
  }

  return status;
}

static void
FE_configOops(FE fe, V_Int version)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reconfigure the OOPS front end chain for the specified FEVersion. 
 * If necessary, a MKJIN jin feature maker is created (or destroyed) 
 * to achieve the front-end configuration required for "version".
 * Return: VISESUCCESS for success, otherwise a an error code
 * Should not be called when fe->IAudioPtr == NULL (TEXTVISE)!!!
 *---------------------------------------------------------------------------*/
{
  Bool rval;

  switch (version) {
 
  /* New config is for FlexVISE.  Unlink/delete MKJIN, if extant */
  case FE_FVISE16KHzOldNorm:
  case FE_FVISE8KHzRastaOldNorm:
  case FE_FVISE16KHzExternalDSP:
  case FE_FVISE8KHzExternalDSP:
    fe->contiguousSN = FALSE;
    if (fe->igenMkjin != NULL) {
      FE_linkOops(fe->igenMkcep, fe->igenSink);
      rval = IGEN_annihilate(fe->igenMkjin);
      assert(rval);
      fe->igenMkjin = NULL;
      fe->iMkjin = NULL;
      fe->isrcMkjin = NULL;
    }
    if (fe->igenMkjinAttr != NULL) {
      rval = IGEN_annihilate(fe->igenMkjinAttr);
      assert(rval);
      fe->igenMkjinAttr = NULL;
      fe->iMkjinAttr = NULL;
    }
    break;

  /* New config is for VISE.  Create/link a new MKJIN, if needed */
  case FE_VISE8KHz:
  case FE_VISE10KHz:
  case FE_VISE11KHz:
  case FE_VISE8KHzNSS:
  case FE_VISE11KHzNSS:
    fe->contiguousSN = FALSE;
    if (fe->igenMkjinAttr == NULL) {
      fe->igenMkjinAttr = MKJINATTR_create(fe->imem);
      fe->iMkjinAttr = (IMKJINATTR) IGEN_interface(fe->igenMkjinAttr, IMKJINATTRID);
    }
    if (fe->igenMkjin == NULL) {
      fe->igenMkjin = MKJIN_create(fe->imem, fe->isrcMkcep);
      fe->iMkjin = (IMKJIN) IGEN_interface(fe->igenMkjin, IMKJINID);
      fe->isrcMkjin = (ISRC) IGEN_interface(fe->igenMkjin, ISRCID);
      FE_linkOops(fe->igenMkjin, fe->igenSink);
    }
    break;
  default:
    VBX_print("FE_configOops: ERROR - unsupported FE %d\n", version);
  }
}


static void
FE_linkOops(IGEN source, IGEN sink)
/*---------------------------------------------------------------------------*
 * DESCRIPTION:
 * Link the isrc (output) interface of the source to the isnk (input)
 * interface of the sink
 *---------------------------------------------------------------------------*/
{
  ISRC  isrc = (ISRC) IGEN_interface(source, ISRCID);
  ISNK  isnk = (ISNK) IGEN_interface(sink, ISNKID);

  ISRC_setSink(isrc, isnk);
  ISNK_setSource(isnk, isrc);
}
   

static void
FE_reset(FE fe)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Reset any history here in preparation for processing a new frame sequence
 * Should not be called when fe->IAudioPtr == NULL (TEXTVISE)!!!
 *---------------------------------------------------------------------------*/
{
  if (fe->iMkjin) {
    IMKJIN_reset(fe->iMkjin);
  }

  IMKCEP_reset(fe->iMkcep);
  if (fe->iqAudioQ) {
    IQUEUE_reset(fe->iqAudioQ);
  }
}


static V_Bool
FE_addAmp(FE fe)
/*---------------------------------------------------------------------------*
 * DESCRIPTION
 * Every 8 frames (0.1s) compute average volume for elapsed interval
 * Should not be called when fe->IAudioPtr == NULL (TEXTVISE)!!!
 *---------------------------------------------------------------------------*/
{
  fe->amps[fe->numAmps++] = fe->amplitude;
  if (MAXAMPS == fe->numAmps) {
    /* Compute average, reseting counter */
    for (fe->avamp = 0; fe->numAmps;)
      fe->avamp += fe->amps[--(fe->numAmps)];

    /* Division with rounding. Here assume MAXAMPS == 8 !!! */
    fe->avamp = (fe->avamp + 4) >> 3;

    return TRUE;
  }
  return FALSE;
}

