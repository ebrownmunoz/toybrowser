/* nij.c - converts nist wav file into jin array
 *---------------------------------------------------------------------------*
 * DESCRIPTION
 * Creates jin frames from nist file audio input using regular FE object
 *---------------------------------------------------------------------------*/
#include "pthr.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "ifaces.h"
#include "imemory.h"
#include "imail.h"
#include "vbx.h"
#include "viseerr.h"
#include "visecmds.h"
#include "visecomp.h"
#include "visemsg.h"
#include "viseparm.h"

#include "feversion.h"
#include "nistaud.h"
#include "vfe.h"
#include "sn.h"
#include "nij.h"

/* Debug 
#define VBXDEBUG 1
*/

#ifdef VBXDEBUG
#define VBX_DEBUG(P)  P
#else
#define VBX_DEBUG(P)
#endif

/* NIJ object */
typedef struct nij_s {
  IMemory   * imem;
  IMail     * iMail;
  IMBox     * iMBoxSE;
  VBXMSG      msgSE;
  NISTAUDIN   nistaudin;
  VFE         vfe;
  Int         feVersion;
  Int         frameLen;
  Int         frameShift;
  Int         vecLen;
  Int         numBytes;
  Int         calibrationLength;
} nij_t;

/* Constructor */
NIJ
NIJ_create(IMemory * imem)
{
  NIJ   nij;
  V_Err s;

  if (
      imem && (nij = (NIJ)imem->Calloc(imem, 1, sizeof(nij_t), FAST_MEMORY)) &&
      (nij->iMail = IMAIL_Create(imem, VUNSMAX)) &&
      (nij->iMBoxSE = nij->iMail->CreateMBox(nij->iMail, VISECOMP_SE, 1, &s)) &&
      (nij->msgSE = MSG_create(imem, nij->iMBoxSE)) &&
      (nij->nistaudin = NISTAUDIN_create(imem)) &&
      (nij->vfe = VFE_create(imem, nij->iMail, (IAudioIn *)nij->nistaudin))
     ) {
    nij->imem = imem;
    nij->vecLen = 0;
    return nij;
  }

  return NULL;
}

/* Destructor */
void
NIJ_destroy(NIJ nij)
{
  if (nij) {
    VFE_destroy(nij->vfe);
    NISTAUDIN_destroy(nij->nistaudin);
    MSG_destroy(nij->msgSE);
    nij->iMBoxSE->Destroy(nij->iMBoxSE);
    IMAIL_Destroy(nij->iMail);
    nij->imem->Free(nij->imem, nij);
  }
}

/* Initializers */
Bool
NIJ_init(NIJ nij, Int feVersion)
{
  Int   istatus;
  Int   vecLen;

  /* jinvector is 16 bytes or 8 Shorts long */
  if (!nij || !FEVER_getCepVecLen(feVersion, &vecLen) || 8 != vecLen) {
    SEVERE_BRA_ "FEVersion (%d) is not supported\n", feVersion _KET;
    return FALSE;
  }

  if (!VFE_setParameter(nij->vfe, FEVERSION, feVersion, &istatus)) {
    SEVERE_BRA_ "VFE_setParameter FAILS for FEVERSION, istatus %d\n", istatus _KET;
    return FALSE;
  }
  FEVER_getFrameLen(feVersion, &nij->frameLen);
  FEVER_getFrameShift(feVersion, &nij->frameShift);
  nij->feVersion = feVersion;
  nij->vecLen = vecLen;
  nij->numBytes = 2 * vecLen;
  nij->calibrationLength = 0; /* Means using all frames available */
  return TRUE;
}

Bool
NIJ_setDefaultDir(NIJ nij, Char * defaultDir)
{
  if (nij && defaultDir && NISTAUDIN_setDefaultDir(nij->nistaudin, defaultDir))
    return TRUE;
  else if (nij && defaultDir)
    SEVERE_BRA_ "Can't set default directory (%s)\n", defaultDir _KET;

  return FALSE;
}

void
NIJ_setCalibrationLength(NIJ nij, Int maxFrames)
{
  if (nij)
    nij->calibrationLength = maxFrames;
}

Int
NIJ_setNoise(NIJ nij, Char * fullFN)
{
  if (nij)
    return NISTAUDIN_setNoise(nij->nistaudin, fullFN);

  return 0;
}

/* Calibrator */
Bool
NIJ_calibrate(NIJ nij, Char * calFileName)
{
  Int        istatus;
  V_ULong    msgLen;
  V_Int      command;
  V_Uns      word;
  V_Uns      sourceID;
  V_Err      status;

  if (!nij || !nij->vecLen) {
    SEVERE_BRA_ "Uninitialized module\n" _KET;
    return FALSE;
  }

  if (!NISTAUDIN_load(nij->nistaudin, calFileName))
    return FALSE;

  if (!VFE_startCalibration(nij->vfe, nij->calibrationLength, &istatus)) {
    SEVERE_BRA_ "VFE_startCalibration FAILS istatus %d\n", istatus _KET;
    return FALSE;
  }

  if (!VFE_startListening(nij->vfe, &istatus)) {
    SEVERE_BRA_ "VFE_startListening FAILS istatus %d\n", istatus _KET;
    return FALSE;
  }

  do {
    if (!MSG_openRead(nij->msgSE, &sourceID, &msgLen, &command, TRUE, &status)) {
      SEVERE_BRA_ "MSG_openRead FAILS! status = %d\n", status _KET;
      return FALSE;
    }

    while (msgLen--)
      MSG_readUns(nij->msgSE, &word);

    if (!MSG_closeRead(nij->msgSE, &status))
      WARN_BRA_ "MSG_closeRead FALSE! status = %d\n", status _KET;
  } while (_EXCEPTION != command && word != ENDOFDATA);

  if (!VFE_stopCalibration(nij->vfe, &istatus)) {
    SEVERE_BRA_ "VFE_stopCalibration FAILS! istatus = %d\n", istatus _KET;
    return FALSE;
  }

  if (!VFE_stopListening(nij->vfe, &istatus))
    WARN_BRA_ "VFE_stopListening FAILS istatus %d\n", istatus _KET;

  return TRUE;
}

/* Converter */
#define MINMAXSIGBITS      9
#define MINUTTLENGTH       10

Int
NIJ_convert(NIJ nij, UChar ** jbuf, Char ** transcription,
            Int * inSampleRate, Char * uttFileName, Int shift)
{
  Int        istatus;
  V_ULong    msgLen;
  V_Int      command, retStatus = 0;
  V_Uns      word;
  V_Uns      sourceID;
  V_Err      status;
  Int      * ip, len, i;
  Int        sampleCount, jinCount = 0;
  V_SN       seqNumber;
  UChar    * jinptr;
  Char     * str;

  if (!nij || !nij->vecLen || !jbuf) {
    SEVERE_BRA_ "Uninitialized module\n" _KET;
    return 0;
  }

  if (!NISTAUDIN_load(nij->nistaudin, uttFileName))
    return 0;

  if (shift < 0) {
    i = NISTAUDIN_peakPower(nij->nistaudin, nij->frameLen, nij->frameShift, 4, &shift);
    if (!i) {
      SEVERE_BRA_ "Failure finding peakPower\n" _KET;
      return 0;
    }
  }
  if (shift > 7) {
    WARN_BRA_ "Hardlimit shift %d to 7\n", shift _KET;
    shift = 7;
  }
  if (!VFE_setParameter(nij->vfe, SIGBITS, MINMAXSIGBITS + shift, &istatus)) {
    SEVERE_BRA_ "VFE_setParameter FAILS for SIGBITS, istatus %d\n", istatus _KET;
    return 0;
  }

  if (transcription) {
    if (
        (str = (Char *)NISTAUDIN_getHdrEntry(nij->nistaudin, "transcription")) ||
        (str = (Char *)NISTAUDIN_getHdrEntry(nij->nistaudin, "prompt_code"))
       ) {
      if (*transcription = (Char *)nij->imem->Calloc(nij->imem, strlen(str) + 1, 1, FAST_MEMORY))
        strcpy(*transcription, str);
    }
    else
      *transcription = NULL;
  }

  if (inSampleRate)
    if (ip = (Int *)NISTAUDIN_getHdrEntry(nij->nistaudin, "sample_rate"))
      *inSampleRate = *ip;

  sampleCount = NISTAUDIN_sampleCount(nij->nistaudin);

  /* Estimate length of jin array */
  if ((len = sampleCount / nij->frameShift) < MINUTTLENGTH) {
    SEVERE_BRA_ "Too short utterance (%s)\n", uttFileName _KET;
    return 0;
  }

  /* Allocate jin buffer */
  *jbuf = (UChar *)nij->imem->Calloc(nij->imem, len, nij->numBytes, FAST_MEMORY);
  if (NULL == *jbuf) {
    SEVERE_BRA_ "Out of memory allocating %d bytes\n", len * nij->numBytes _KET;
    return 0;
  }

  /* Run the show */
  if (!VFE_startListening(nij->vfe, &istatus)) {
    SEVERE_BRA_ "VFE_startListening FAILS istatus %d\n", istatus _KET;
    nij->imem->Free(nij->imem, *jbuf);
    *jbuf = NULL;
    return 0;
  }

  /* Get jins */
  jinptr = *jbuf;
  do {
    if (!MSG_openRead(nij->msgSE, &sourceID, &msgLen, &command, TRUE, &status)) {
      SEVERE_BRA_ "MSG_openRead FAILS! status = %d\n", status _KET;
      nij->imem->Free(nij->imem, *jbuf);
      *jbuf = NULL;
      return 0;
    }

    if (_ACOUSTICFEATURES != command && _EXCEPTION != command) {
    /* Some _ACKNOWLEDGMENTs may have come, just skip it all */
      while (msgLen--)
        MSG_readUns(nij->msgSE, &word);
    } else if (_ACOUSTICFEATURES == command) {
      /* These are frames */
      if (!MSG_readSN(nij->msgSE, &seqNumber)) {
        SEVERE_BRA_ "MSG_readSN FAILS!\n" _KET;
        nij->imem->Free(nij->imem, *jbuf);
        *jbuf = NULL;
        return 0;
      }
      VBX_DEBUG(VBX_print(""QuadFmt(-19)"  ", seqNumber));

      msgLen -= VSNSIZE/VAMESIZE;
      assert(msgLen == nij->vecLen);
      MSG_readMels(nij->msgSE, (V_Mel *)jinptr, msgLen);
      VBX_DEBUG(for (i = 0; i < nij->numBytes; i++))
        VBX_DEBUG(VBX_print("%3u ", jinptr[i]));
      VBX_DEBUG(VBX_print("\n"));
      jinptr += nij->numBytes;
      jinCount++;
    } else { /* Exception */
      assert(msgLen == 1);
      if (!MSG_readInt(nij->msgSE, &retStatus)) {
        SEVERE_BRA_ "MSG_readInt FAILS!\n" _KET;
        nij->imem->Free(nij->imem, *jbuf);
        *jbuf = NULL;
        return 0;
      }
      if (ENDOFDATA != retStatus)
        WARN_BRA_ "EXCEPTION: %s\n", VISEERR_string((V_Err)retStatus) _KET;
    }

    if (!MSG_closeRead(nij->msgSE, &status))
      WARN_BRA_ "MSG_closeRead FALSE! status = %d\n", status _KET;
  } while (!(_EXCEPTION == command && retStatus == ENDOFDATA));

  if (!VFE_stopListening(nij->vfe, &istatus))
    WARN_BRA_ "VFE_stopListening FAILS istatus %d\n", istatus _KET;

  return jinCount;
}

Int
NIJ_mix(NIJ nij, UChar ** jbuf, Char ** transcription, Char * uttFileName,
        Int noiPowerMix, Int msAdd, Int noiPowerAdd, Int shift)
{
  Int        istatus;
  V_ULong    msgLen;
  V_Int      command, retStatus = 0;
  V_Uns      word;
  V_Uns      sourceID;
  V_Err      status;
  Int        len, i;
  Int        sampleCount, jinCount = 0;
  V_SN       seqNumber;
  UChar    * jinptr;
  Char     * str;

  if (!nij || !nij->vecLen || !jbuf) {
    SEVERE_BRA_ "Uninitialized module\n" _KET;
    return 0;
  }

  if (!NISTAUDIN_mix(nij->nistaudin, uttFileName, noiPowerMix, msAdd, noiPowerAdd))
    return 0;

  if (shift < 0) {
    i = NISTAUDIN_peakPower(nij->nistaudin, nij->frameLen, nij->frameShift, 4, &shift);
    if (!i) {
      SEVERE_BRA_ "Failure finding peakPower\n" _KET;
      return 0;
    }
  }
  if (shift > 7) {
    WARN_BRA_ "Hardlimit shift %d to 7\n", shift _KET;
    shift = 7;
  }
  if (!VFE_setParameter(nij->vfe, SIGBITS, MINMAXSIGBITS + shift, &istatus)) {
    SEVERE_BRA_ "VFE_setParameter FAILS for SIGBITS, istatus %d\n", istatus _KET;
    return 0;
  }

  if (transcription) {
    if (
        (str = (Char *)NISTAUDIN_getHdrEntry(nij->nistaudin, "transcription")) ||
        (str = (Char *)NISTAUDIN_getHdrEntry(nij->nistaudin, "prompt_code"))
       ) {
      if (*transcription = (Char *)nij->imem->Calloc(nij->imem, strlen(str) + 1, 1, FAST_MEMORY))
        strcpy(*transcription, str);
    }
    else
      *transcription = NULL;
  }

  sampleCount = NISTAUDIN_sampleCount(nij->nistaudin);

  /* Estimate length of jin array */
  if ((len = sampleCount / nij->frameShift) < MINUTTLENGTH) {
    SEVERE_BRA_ "Too short utterance (%s)\n", uttFileName _KET;
    return 0;
  }

  /* Allocate jin buffer */
  *jbuf = (UChar *)nij->imem->Calloc(nij->imem, len, nij->numBytes, FAST_MEMORY);
  if (NULL == *jbuf) {
    SEVERE_BRA_ "Out of memory allocating %d bytes\n", len * nij->numBytes _KET;
    return 0;
  }

  /* Run the show */
  if (!VFE_startListening(nij->vfe, &istatus)) {
    SEVERE_BRA_ "VFE_startListening FAILS istatus %d\n", istatus _KET;
    nij->imem->Free(nij->imem, *jbuf);
    *jbuf = NULL;
    return 0;
  }

  /* Get jins */
  jinptr = *jbuf;
  do {
    if (!MSG_openRead(nij->msgSE, &sourceID, &msgLen, &command, TRUE, &status)) {
      SEVERE_BRA_ "MSG_openRead FAILS! status = %d\n", status _KET;
      nij->imem->Free(nij->imem, *jbuf);
      *jbuf = NULL;
      return 0;
    }

    if (_ACOUSTICFEATURES != command && _EXCEPTION != command) {
    /* Some _ACKNOWLEDGMENTs may have come, just skip it all */
      while (msgLen--)
        MSG_readUns(nij->msgSE, &word);
    } else if (_ACOUSTICFEATURES == command) {
      /* These are frames */
      if (!MSG_readSN(nij->msgSE, &seqNumber)) {
        SEVERE_BRA_ "MSG_readSN FAILS!\n" _KET;
        nij->imem->Free(nij->imem, *jbuf);
        *jbuf = NULL;
        return 0;
      }
      VBX_DEBUG(VBX_print(""QuadFmt(-19)"  ", seqNumber));

      msgLen -= VSNSIZE/VAMESIZE;
      assert(msgLen == nij->vecLen);
      MSG_readMels(nij->msgSE, (V_Mel *)jinptr, msgLen);
      VBX_DEBUG(for (i = 0; i < nij->numBytes; i++))
        VBX_DEBUG(VBX_print("%3u ", jinptr[i]));
      VBX_DEBUG(VBX_print("\n"));
      jinptr += nij->numBytes;
      jinCount++;
    } else { /* Exception */
      assert(msgLen == 1);
      if (!MSG_readInt(nij->msgSE, &retStatus)) {
        SEVERE_BRA_ "MSG_readInt FAILS!\n" _KET;
        nij->imem->Free(nij->imem, *jbuf);
        *jbuf = NULL;
        return 0;
      }
      if (ENDOFDATA != retStatus)
        WARN_BRA_ "EXCEPTION: %s\n", VISEERR_string((V_Err)retStatus) _KET;
    }

    if (!MSG_closeRead(nij->msgSE, &status))
      WARN_BRA_ "MSG_closeRead FALSE! status = %d\n", status _KET;
  } while (!(_EXCEPTION == command && retStatus == ENDOFDATA));

  if (!VFE_stopListening(nij->vfe, &istatus))
    WARN_BRA_ "VFE_stopListening FAILS istatus %d\n", istatus _KET;

  return jinCount;
}
