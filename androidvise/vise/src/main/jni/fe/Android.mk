LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := fe
LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/fe/*.c)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/../vise2/include $(LOCAL_PATH)/../vbx/include 

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := oops
LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/oops/*.c)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/../vbx/include 

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := cep
LOCAL_SRC_FILES := cep/agcsub.c cep/ceparray.c cep/cepattr.c cep/cepbuf.c cep/cepmax.c cep/cepmbox.c cep/cepmsg.c cep/cepsn.c cep/feversion.c cep/fft.c cep/ham.c cep/ness.c cep/kaiser.c cep/mel.c cep/mkcep.c cep/rasta.c cep/txtframe.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/../vbx/include
include $(BUILD_STATIC_LIBRARY)
