 // Generic inclusions
#include "pthr.h"
#include "android/log.h"
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include "vbx.h"

// CNI inclusions
#include "vise_jni.h"
#include "cniviseresult.hh"
#include "voxware_engine_recognition_sapivise_ViseResult.h"

#define VBX_DEBUG(P) P

#define SMAX   256

using namespace java::lang;
using namespace voxware::engine::recognition::sapivise;

#define VBX_print(...) __android_log_print(ANDROID_LOG_INFO, "com_voxware_vise_engine_recognition_sapivise_ViseResult.cpp", __VA_ARGS__)

#define DECLARE_RESULT_ALIAS(aliasName, ret) \
jobject result = env->GetObjectField(thiz, ViseResult.result); \
if (!result) { \
	env->ThrowNew(NullPointerException.clazz, "recognizer is null"); \
	return ret; \
}\
LPUNKNOWN aliasName = (LPUNKNOWN) env->GetDirectBufferAddress(result);

jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1rescorrection(JNIEnv *env, jobject thiz, jstring transcription) {
	//LPUNKNOWN         resUnknwnPtr = (LPUNKNOWN) result;
	DECLARE_RESULT_ALIAS(resUnknwnPtr, JNI_FALSE)
	PVISEGrammarObj gramPtr = ((PVISEResultsObj) resUnknwnPtr)->Grammar();
	IVbxGrammar *IVbxGrammarPtr = NULL;
	ISRResCorrection *ISRResCorrectionPtr = NULL;
	HRESULT hres;
	Char utftrans[SMAX];

	VBX_DEBUG(VBX_print("ViseResult::n_rescorrection: gramPtr = %p\n", gramPtr));

	utftrans[0] = '\0';
	int len;
	if (transcription != NULL) {
		if (!JavaToUTF8String(env, transcription, utftrans, SMAX)) {
			env->ThrowNew(IllegalArgumentException.clazz, "transcription too long");
			return JNI_FALSE;
		}
	}

	// Get the ISRGramCommon interface to the grammar object
	if (hres = gramPtr->QueryInterface(IID_IVbxGrammar, (PVOID *) &IVbxGrammarPtr)) {
		VBX_print("ViseResult::n_rescorrection ERROR: QI for IVbxGrammarPtr FAILS\n");
		return JNI_FALSE;
	}

	if (hres = resUnknwnPtr->QueryInterface(IID_ISRResCorrection, (PVOID *) &ISRResCorrectionPtr)) {
		VBX_print("ViseResult::n_rescorrection ERROR: QI for ISRResCorrection FAILS\n");
		return JNI_FALSE;
	}

	if (IVbxGrammarPtr == NULL || ISRResCorrectionPtr == NULL) {
		VBX_print("ViseResult::n_rescorrection: ERROR IVbxGrammarPtr/ISRResCorrectionPtr == NULL\n");
		return JNI_FALSE;
	}

	PSRPHRASE srPhrasePtr = NULL;
	DWORD needed;

	srPhrasePtr = (PSRPHRASE) new Char[SMAX];
	if (!(hres = IVbxGrammarPtr->Tokenize(NULL, utftrans, srPhrasePtr, (UInt) SMAX, &needed))) {
		ISRResCorrectionPtr->Correction(srPhrasePtr, SRCORCONFIDENCE_VERY);
	} else if (hres == SRERR_VALUEOUTOFRANGE && needed > SMAX) {
		delete srPhrasePtr;
		srPhrasePtr = (PSRPHRASE) new Char[needed];
		if (!(hres = IVbxGrammarPtr->Tokenize(NULL, utftrans, srPhrasePtr, needed, &needed))) {
			ISRResCorrectionPtr->Correction(srPhrasePtr, SRCORCONFIDENCE_VERY);
		} else
			VBX_print("ViseResult::n_rescorrection: ERROR - Cannot tokenize \"%s\"\n", utftrans);
	} else {
		VBX_print("ViseResult::n_rescorrection: ERROR - Cannot tokenize \"%s\"\n", utftrans);
	}
	delete srPhrasePtr;

	ISRResCorrectionPtr->Release();
	IVbxGrammarPtr->Release();
	return JNI_TRUE;
}

jint JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1resrelease(JNIEnv *env, jobject thiz) {
	DECLARE_RESULT_ALIAS(resUnknwnPtr, 0)
	HRESULT hres;

	if (resUnknwnPtr == NULL) {
		VBX_print("ViseResult::n_resrelease: ERROR resUnknwnPtr == NULL\n");
		return (jint) 0;
	} else {
		VBX_DEBUG(VBX_print("ViseResult::n_resrelease: resUnknwnPtr = %p\n", resUnknwnPtr));
	}

	ULong refCount = resUnknwnPtr->Release();
	VBX_DEBUG(VBX_print("ViseResult::n_resrelease: refCount = %ul\n", refCount));

	return (jint) refCount;
}

jboolean JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1resvalidate(JNIEnv *env, jobject thiz) {
	DECLARE_RESULT_ALIAS(resUnknwnPtr, JNI_FALSE)
	ISRResCorrection *ISRResCorrectionPtr = NULL;
	HRESULT hres;

	if (hres = resUnknwnPtr->QueryInterface(IID_ISRResCorrection, (PVOID *) &ISRResCorrectionPtr)) {
		VBX_print("ViseResult::n_validate ERROR: QI for ISRResCorrection FAILS\n");
		return JNI_FALSE;
	}

	if (ISRResCorrectionPtr == NULL) {
		VBX_print("ViseResult::n_resvalidate: ERROR ISRResCorrectionPtr == NULL\n");
		return JNI_FALSE;
	}

	VBX_DEBUG(VBX_print("ViseResult::n_resvalidate: Calling ISRResBasic::Validate\n"));
	hres = ISRResCorrectionPtr->Validate(SRCORCONFIDENCE_VERY);
	ISRResCorrectionPtr->Release();

	return (!hres);
}



jbyteArray JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1getwav(JNIEnv *env, jobject thiz) {
	DECLARE_RESULT_ALIAS(resUnknwnPtr, NULL)
	ISRResAudio *ISRResAudioPtr = NULL;
	SDATA sdata = { NULL, 0 };
	HRESULT hres;

	if (resUnknwnPtr == NULL) {
		VBX_print("ViseResult::n_getwav ERROR: resUnknwnPtr == NULL\n");
		return NULL;
	} else {
		VBX_DEBUG(VBX_print("ViseResult::n_getwav: INFO - started, resUnknwnPtr appears OK\n"));
	}

	if (hres = resUnknwnPtr->QueryInterface(IID_ISRResAudio, (PVOID *) &ISRResAudioPtr)) {
		VBX_print("ViseResult::n_getwav ERROR: QI for ISRResAudio FAILS with hres = %x\n", hres);
		return NULL;
	}

	if (ISRResAudioPtr == NULL) {
		VBX_print("ViseResult::n_getwav: ERROR ISRResAudioPtr == NULL\n");
		return NULL;
	}

	VBX_DEBUG(VBX_print("ViseResult::n_getwav: Calling ISRResAudio::GetWAV\n"));

	if (hres = ISRResAudioPtr->GetWAV(&sdata)) {
		VBX_print("ViseResult::n_getwav: ERROR: ISRResAudio::GetWAV FAILS with hres = %x. Is data collection configured?\n", hres);
		ISRResAudioPtr->Release();
		return NULL;
	}

	if (sdata.pData == NULL || sdata.dwSize == 0) {
		VBX_print("ViseResult::n_getwav: ERROR: no WAV data - is data collection configured?\n");
		ISRResAudioPtr->Release();
		return NULL;
	}

	jbyteArray wavData;
	wavData = env->NewByteArray(sdata.dwSize);

	if (wavData == NULL) {
		VBX_print("ViseResult::n_getwav: ERROR - NULL result for wave data array allocation\n");
		ISRResAudioPtr->Release();
		return NULL;
	} else {
		VBX_DEBUG(VBX_print("ViseResult::n_getwav: INFO - wave data array allocation is OK\n"));
	}

	env->SetByteArrayRegion(wavData, 0, (jsize)sdata.dwSize, (jbyte*)sdata.pData);
	//!! CAV Do not free this, it's coming from an OBJT managed by FE now!!
	/* VBX_free(sdata.pData); */
	ISRResAudioPtr->Release();

	return wavData;
}

jint JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1pathscore(JNIEnv *env, jobject thiz) {
	DECLARE_RESULT_ALIAS(resUnknwnPtr, 0)
	HRESULT hres;
	jint pathScore = 0;
	ISRResGraph * ISRResGraphPtr = NULL;

	if (resUnknwnPtr == NULL) {
		VBX_print("ViseResult::n_pathscore: ERROR resUnknwnPtr == NULL\n");
		return (jint) 0;
	} else {
		VBX_DEBUG(VBX_print("ViseResult::n_pathscore: resUnknwnPtr = %p\n", resUnknwnPtr));
	}

	if (hres = resUnknwnPtr->QueryInterface(IID_ISRResGraph, (PVOID *) &ISRResGraphPtr)) {
		VBX_print("ViseResult::n_pathscore ERROR: QI for ISRResGraph FAILS\n");
		return (jint) 0;
	}

	if (ISRResGraphPtr == NULL) {
		VBX_print("ViseResult::n_pathscore ERROR: ISRResGraphPtr == NULL\n");
		return (jint) 0;
	}

	hres = ISRResGraphPtr->PathScoreWord(NULL, 0, (LONG *) &pathScore);
	ISRResGraphPtr->Release();

	if (hres)
		return (jint) 0;

	return pathScore;
}

jint JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1releasetraininginfo(JNIEnv *env, jobject thiz) {
	DECLARE_RESULT_ALIAS(resUnknwnPtr, 0)
	ISRResMemory * ISRResMemoryPtr = NULL;
	HRESULT hres;
	ULong refCount = 0L;

	if (resUnknwnPtr == NULL) {
		VBX_print("ViseResult::n_releasetraininginfo: ERROR - resUnknwnPtr == NULL\n");
	} else {
		VBX_DEBUG(VBX_print("ViseResult::n_releasetraininginfo: resUnknwnPtr = %p\n", resUnknwnPtr));
		if (hres = resUnknwnPtr->QueryInterface(IID_ISRResMemory, (PVOID *) &ISRResMemoryPtr)) {
			VBX_print("ViseResult::n_releasetraininginfo: ERROR - QueryInterface for ISRResMemory FAILS with hres = %x\n", hres);
		} else if (ISRResMemoryPtr == NULL) {
			VBX_print("ViseResult::n_releasetraininginfo: ERROR - ISRResMemoryPtr == NULL\n");
		} else {
			ISRResMemoryPtr->Free(SRRESMEMKIND_CORRECTION);
			ISRResMemoryPtr->Release();
		}
		refCount = resUnknwnPtr->Release();
		VBX_DEBUG(VBX_print("ViseResult::n_resrelease: refCount = %ul\n", refCount));
	}

	return (jint) refCount;
}

jint JNICALL Java_voxware_engine_recognition_sapivise_ViseResult_n_1releaseaudio(JNIEnv *env, jobject thiz) {
	DECLARE_RESULT_ALIAS(resUnknwnPtr, 0)
	ISRResMemory * ISRResMemoryPtr = NULL;
	HRESULT hres;
	ULong refCount = 0L;

	if (resUnknwnPtr == NULL) {
		VBX_print("ViseResult::n_releaseaudio: ERROR - resUnknwnPtr == NULL\n");
	} else {
		VBX_DEBUG(VBX_print("ViseResult::n_releaseaudio: resUnknwnPtr = %p\n", resUnknwnPtr));
		if (hres = resUnknwnPtr->QueryInterface(IID_ISRResMemory, (PVOID *) &ISRResMemoryPtr)) {
			VBX_print("ViseResult::n_releaseaudio: ERROR - QueryInterface for ISRResMemory FAILS with hres = %x\n", hres);
		} else if (ISRResMemoryPtr == NULL) {
			VBX_print("ViseResult::n_releaseaudio: ERROR - ISRResMemoryPtr == NULL\n");
		} else {
			ISRResMemoryPtr->Free(SRRESMEMKIND_AUDIO);
			ISRResMemoryPtr->Release();
		}
		refCount = resUnknwnPtr->Release();
		VBX_DEBUG(VBX_print("ViseResult::n_resrelease: refCount = %ul\n", refCount));
	}

	return (jint) refCount;
}

