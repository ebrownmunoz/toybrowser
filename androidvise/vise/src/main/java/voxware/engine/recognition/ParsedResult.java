package voxware.engine.recognition;

import javax.speech.*;
import javax.speech.recognition.*;

public class ParsedResult extends BaseResult implements Cloneable {

  protected RuleParse parse;

  public ParsedResult() {
    super(null);
  }

  public ParsedResult(Grammar grammar, String[] nBest, RuleParse parse) {
    super(grammar, nBest, false);
    this.setParse(parse);
  }

  public ParsedResult(Grammar grammar, String best, RuleParse parse) {
    super(grammar, best, false);
    this.setParse(parse);
  }

  public RuleParse getParse() {
    return parse;
  }

  public void setParse(RuleParse parse) {
    this.parse = parse;
    tags = parse.getTags();
    ruleName = parse.getRuleName().getSimpleRuleName();
  }
}
