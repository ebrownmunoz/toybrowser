package voxware.engine.recognition;

import javax.speech.SpeechEvent;

/**
 * The class of token events. Token recognizers implement
 * the <code>TokenListener</code> interface to receive these.
 * <P>
 * 
 * @see TokenListener
 */

public class TokenEvent extends SpeechEvent {

  public String token;

  public TokenEvent(Object source, String token) {
    super(source);
    this.token = token;
  }

  public String token() {
    return token;
  }

  public String toString() {
    return token;
  }
}
