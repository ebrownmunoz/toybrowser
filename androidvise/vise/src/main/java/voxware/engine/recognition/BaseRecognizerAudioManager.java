/*
 * Copyright (c) 1998 Sun Microsystems, Inc. All Rights Reserved.
 */
package voxware.engine.recognition;

import voxware.engine.*;
import javax.speech.*;
import javax.speech.recognition.*;
import java.util.*;

/**
 * Skeletal Implementation of the JSAPI AudioManager interface for
 * Recognizers.  Merely provides convenience function for calling
 * the listeners.
 * 
 * <P>Actual JSAPI implementations might want to extend or
 * modify this implementation.
 * <P>
 *
 * @author Willie Walker
 * @version 1.1 12/14/98 16:06:12
 */
public class BaseRecognizerAudioManager extends BaseAudioManager {
    protected Vector listeners;
    
//////////////////////
// Begin convenience methods for calling RecognizerAudioListeners
//////////////////////
    /**
     * Utility function to generate AUDIO_LEVEL event and send it to all
     * RecognizerAudioListeners.
     * @param source the Recognizer causing the event
     * @param audioLevel the audio level between 0.0 and 1.0
     */
    public void audioLevel(Recognizer source, float audioLevel) {
	if (listeners == null) {
	    return;
	}
        RecognizerAudioEvent ev = new RecognizerAudioEvent(
            source,
            RecognizerAudioEvent.AUDIO_LEVEL,
            audioLevel);
        Enumeration E = listeners.elements();
        while (E.hasMoreElements()) {
            AudioListener al = (AudioListener) E.nextElement();
            if (al instanceof RecognizerAudioListener) {
                ((RecognizerAudioListener) al).audioLevel(ev);
            }
        }
    }
    

    /**
     * Utility function to generate SPEECH_STARTED event and send it to all
     * RecognizerAudioListeners.
     * @param source the Recognizer causing the event
     */
    public void speechStarted(Recognizer source) {
	if (listeners == null) {
	    return;
	}
        RecognizerAudioEvent ev = new RecognizerAudioEvent(
            source,
            RecognizerAudioEvent.SPEECH_STARTED);
        Enumeration E = listeners.elements();
        while (E.hasMoreElements()) {
            AudioListener al = (AudioListener) E.nextElement();
            if (al instanceof RecognizerAudioListener) {
                ((RecognizerAudioListener) al).speechStarted(ev);
            }
        }
    }

    /**
     * Utility function to generate SPEECH_STOPPED event and send it to all
     * RecognizerAudioListeners.
     * @param source the Recognizer causing the event
     */
    public void speechStopped(Recognizer source) {
	if (listeners == null) {
	    return;
	}
        RecognizerAudioEvent ev = new RecognizerAudioEvent(
            source,
            RecognizerAudioEvent.SPEECH_STOPPED);
        Enumeration E = listeners.elements();
        while (E.hasMoreElements()) {
            AudioListener al = (AudioListener) E.nextElement();
            if (al instanceof RecognizerAudioListener) {
                ((RecognizerAudioListener) al).speechStarted(ev);
            }
        }
    }   
//////////////////////
// End convenience methods for calling RecognizerAudioListeners
//////////////////////
}

