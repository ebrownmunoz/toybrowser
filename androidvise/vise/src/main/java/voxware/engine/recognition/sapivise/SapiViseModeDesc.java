package voxware.engine.recognition.sapivise;

import java.lang.*;
import java.util.Locale;

import javax.speech.*;
import javax.speech.recognition.*;

public class SapiViseModeDesc extends RecognizerModeDesc implements EngineCreate {

  public SapiViseModeDesc(String mode) {
    super("VISE",          // engine name
           mode,           // mode name
           Locale.US,      // locale
           Boolean.FALSE,  // running?
           Boolean.FALSE,  // dictationGrammarSupported
           null);          // profile[]
  }

  public Engine createEngine() throws IllegalArgumentException, EngineException, SecurityException {
    Recognizer recognizer = new SapiVise(this);
    if (recognizer == null) throw new EngineException("Cannot construct SapiVise engine");
    return recognizer;
  }
}
