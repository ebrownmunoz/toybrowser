package voxware.engine.recognition.sapivise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

import javax.speech.recognition.Grammar;
import javax.speech.recognition.ResultStateError;
import javax.speech.recognition.ResultToken;

import voxware.engine.recognition.BaseResult;

public class ViseResult extends BaseResult {
    private static final Logger log = LoggerFactory.getLogger(ViseResult.class);

    private native int n_resrelease();

    private native int n_releasetraininginfo();

    private native int n_releaseaudio();

    private native boolean n_rescorrection(String transcription);

    private native boolean n_resvalidate();

    private native byte[] n_getwav();

    private native int n_pathscore();

    ByteBuffer result = null;
    boolean training = false;
    boolean audio = false;
    private String hostTrans = "";
    private String[] correctTokens;

    public ViseResult(Grammar grammar, ByteBuffer result, String[] nBest, boolean parse) {
        super(grammar, nBest, parse);
        this.result = result;
        log.debug("ViseResult: constructed for \"" + toString() + "\"");
    }

    public void finalize() throws Throwable {
        try {
            log.debug("ViseResult.finalize: called on \"" + toString() + "\"");
            releaseTrainingInfo();
            releaseAudio();
        } finally {
            super.finalize();
        }
    }

    public void tokenCorrection(String[] correctTokens, ResultToken from, ResultToken to, int type) throws ResultStateError, IllegalArgumentException {

        if (result == null)
            throw new ResultStateError("ViseResult.tokenCorrection: No underlying VISEResults object");
        if (from != null || to != null)
            throw new IllegalArgumentException("ViseResult.tokenCorrection: To/from NOT SUPPORTED");

        switch (type) {
            case DONT_KNOW:
                this.correctTokens = this.nBest[0];
                if (!n_resvalidate())
                    throw new ResultStateError("ViseResult.tokenCorrection: n_resvalidate FAILS");
                break;
            case MISRECOGNITION:
                this.correctTokens = correctTokens;
                StringBuffer transcription = new StringBuffer("");
                if (correctTokens.length > 0) {
                    transcription.append(correctTokens[0]);
                    for (int i = 1; i < correctTokens.length; i++)
                        transcription.append(" ").append(correctTokens[i]);
                }
                if (!n_rescorrection(transcription.toString()))
                    throw new ResultStateError("ViseResult.tokenCorrection: n_rescorrection FAILS");
                break;
            default:
                throw new IllegalArgumentException("ViseResult.tokenCorrection: unsupported correction type (" + type + ")");
        }
    }

    public synchronized void releaseTrainingInfo() {
        if (training) {
            if (result == null)
                throw new ResultStateError("ViseResult.releaseTrainingInfo: no underlying VISEResults object");
            log.debug("ViseResult.releaseTrainingInfo: reducing ref count of \"" + this.toString() + "\"");
            int refCount = n_releasetraininginfo();
            log.debug("ViseResult.releaseTrainingInfo: reduced ref count of \"" + this.toString() + "\" to " + refCount);
            training = false;
        }
    }

    public synchronized void releaseAudio() {
        if (audio) {
            if (result == null)
                throw new ResultStateError("ViseResult.releaseAudio: no underlying VISEResults object");
            log.debug("ViseResult.releaseAudio: reducing ref count of \"" + this.toString() + "\"");
            int refCount = n_releaseaudio();
            log.debug("ViseResult.releaseAudio: reduced ref count of \"" + this.toString() + "\" to " + refCount);
            audio = false;
        }
    }

    public String[] correctTokens() {
        if (this.correctTokens == null)
            throw new ResultStateError("ViseResult.tokenCorrection: no correct tokens");
        return this.correctTokens;
    }

    public byte[] getWAV() throws ResultStateError {
        byte[] wavfile = null;
        if (audio) {
            if (result == null)
                throw new ResultStateError("ViseResult.getWAV: no underlying VISEResults object");
            wavfile = n_getwav();
        }
        return wavfile;
    }

    public void hostTrans(String hostTrans) {
        if (hostTrans != null)
            this.hostTrans = hostTrans;
    }

    public String hostTrans() {
        return hostTrans;
    }

    public int pathScore() throws ResultStateError {
        if (result == null)
            throw new ResultStateError("ViseResult.pathScore: no underlying VISEResults object");
        return n_pathscore();
    }
}
