/**
 * Rudimentary implementation of JSAPI Result, FinalResult, FinalRuleResult and FinalDictationResult
 *
 * @version 1.6 12/14/98 16:16:19
 */
package voxware.engine.recognition;

import javax.speech.*;
import javax.speech.recognition.*;

import java.util.*;

import voxware.util.ObjectSet;

public class BaseResult implements Result, FinalResult, FinalRuleResult, FinalDictationResult, java.io.Serializable, Cloneable {

    protected String[][] nBest;
    protected Vector unfinalizedTokens = null;
    protected int state = UNFINALIZED;
    protected String[] tags = null;
    protected String ruleName = null;

    protected transient Set<ResultListener> resultListeners = null;
    protected transient Grammar grammar = null;

    /**
     * Create an empty result
     */
    public BaseResult(Grammar grammar) {
        this.grammar = grammar;
    }

    /**
     * Create a result from an array of the nBest result strings
     */
    public BaseResult(Grammar grammar, String[] nBest, boolean parse) {
        this(grammar);
        construct(grammar, nBest, parse);
    }

    /**
     * Create a result from the best result string
     */
    public BaseResult(Grammar grammar, String best, boolean parse) {
        this(grammar);
        construct(grammar, new String[]{best}, parse);
    }

    protected void construct(Grammar grammar, String[] nBest, boolean parse) {
        this.nBest = new String[nBest.length][];
        for (int i = 0; i < nBest.length; i++) {
            int j = 0;
            StringTokenizer tokenizer = new StringTokenizer(nBest[i]);
            this.nBest[i] = new String[tokenizer.countTokens()];
            while (tokenizer.hasMoreTokens()) this.nBest[i][j++] = tokenizer.nextToken();
        }
        if (parse && grammar instanceof RuleGrammar) {
            try {
                RuleParse ruleParse = ((RuleGrammar) grammar).parse(nBest[0], null);
                tags = ruleParse.getTags();
                ruleName = ruleParse.getRuleName().getSimpleRuleName();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Copy a result. If the original is a BaseResult, clone it; otherwise, create a BaseResult and copy the tokens into it.
     */
    static BaseResult copyResult(Result result) {
        BaseResult copy = null;
        if (result instanceof BaseResult) {
            try {
                copy = (BaseResult) ((BaseResult) result).clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException("BaseResult.copyResult: UNEXPECTED " + e);
            }
            return copy;
        } else {
            copy = new BaseResult(result.getGrammar());
            BaseResult baseResult = (BaseResult) result;
            copy.nBest = new String[baseResult.nBest.length][];
            for (int i = 0; i < baseResult.nBest.length; i++) {
                int length = baseResult.nBest[i].length;
                copy.nBest[i] = new String[length];
                for (int j = 0; j < length; j++) {
                    copy.nBest[i][j] = new String(baseResult.nBest[i][j]);
                }
            }
            return copy;
        }
    }

//////////////////////
// Begin Result Methods
//////////////////////

    /**
     * Return the current state of the Result object.
     * From javax.speech.recognition.Result.
     */
    public int getResultState() {
        return state;
    }

    /**
     * Return the grammar that goes with this Result.
     * From javax.speech.recognition.Result.
     */
    public Grammar getGrammar() {
        return grammar;
    }

    /**
     * Return the number of finalized tokens in the best Result.
     * From javax.speech.recognition.Result.
     */
    public int numTokens() {
        return this.nBest != null ? this.nBest[0].length : 0;
    }

    /**
     * Return the best guess for the nth token.
     * From javax.speech.recognition.Result.
     */
    public ResultToken getBestToken(int nth) throws IllegalArgumentException {
        if (this.nBest == null || nth < 0 || nth >= this.nBest[0].length)
            throw new IllegalArgumentException("BaseResult.getBestToken: token index (" + Integer.toString(nth) + ") out of range");
        return (new BaseResultToken(this.nBest[0][nth]));
    }

    /**
     * Return the best guess tokens for the Result.
     * From javax.speech.recognition.Result.
     */
    public ResultToken[] getBestTokens() {
        int size = numTokens();
        ResultToken[] result = new ResultToken[size];
        while (size-- > 0) result[size] = new BaseResultToken(this.nBest[0][size]);
        return result;
    }

    /**
     * Return the current guess of the tokens following the unfinalized tokens.
     * From javax.speech.recognition.Result.
     */
    public ResultToken[] getUnfinalizedTokens() {
        int size = (unfinalizedTokens != null) ? unfinalizedTokens.size() : 0;
        ResultToken[] result = new ResultToken[size];
        while (size-- > 0)
            result[size] = new BaseResultToken((String) unfinalizedTokens.elementAt(size));
        return result;
    }

    /**
     * Add a ResultListener to this Result.
     * From javax.speech.recognition.Result.
     */
    public void addResultListener(ResultListener listener) {
        if (resultListeners == null) resultListeners = new LinkedHashSet<>();
        resultListeners.add(listener);
    }

    /**
     * Remove a ResultListener from this Result.
     * From javax.speech.recognition.Result.
     */
    public void removeResultListener(ResultListener listener) {
        if (resultListeners != null) resultListeners.remove(listener);
    }

//////////////////////
// End Result Methods
//////////////////////

//////////////////////
// Begin FinalResult Methods
//////////////////////

    /**
     * Returns true if the Recognizer has training information available for this result.
     * From javax.speech.recognition.FinalResult.
     */
    public boolean isTrainingInfoAvailable() throws ResultStateError {
        checkResultState(UNFINALIZED);
        return false;
    }

    /**
     * Release training info for this FinalResult.
     * From javax.speech.recognition.FinalResult.
     */
    public void releaseTrainingInfo() throws ResultStateError {
        checkResultState(UNFINALIZED);
    }

    /**
     * Inform the recognizer of a correction to one or more tokens in a FinalResult to the recognizer can re-train itself.
     * From javax.speech.recognition.FinalResult.
     */
    public void tokenCorrection(String correctTokens[], ResultToken fromToken, ResultToken toToken, int correctionType)
            throws ResultStateError, IllegalArgumentException {
        checkResultState(UNFINALIZED);
    }

    /**
     * Determine if audio is available for this FinalResult.
     * From javax.speech.recognition.FinalResult.
     */
    public boolean isAudioAvailable() throws ResultStateError {
        checkResultState(UNFINALIZED);
        return false;
    }

    /**
     * Release the audio for this FinalResult.
     * From javax.speech.recognition.FinalResult.
     */
    public void releaseAudio() throws ResultStateError {
        checkResultState(UNFINALIZED);
    }
//////////////////////
// End FinalResult Methods
//////////////////////

//////////////////////
// Begin FinalRuleResult Methods
//////////////////////

    /**
     * Return the number of guesses for this FinalRuleResult.
     * From javax.speech.recognition.FinalRuleResult.
     */
    public int getNumberGuesses() throws ResultStateError {
        checkResultState(UNFINALIZED);
        if (!(grammar instanceof RuleGrammar))
            throw new ResultStateError("Result is not a FinalRuleResult");
        return this.nBest.length;
    }

    /**
     * Get the nBest token sequence for this FinalRuleResult.
     * From javax.speech.recognition.FinalRuleResult.
     */
    public ResultToken[] getAlternativeTokens(int rank) throws ResultStateError {
        checkResultState(UNFINALIZED);
        if (!(grammar instanceof RuleGrammar))
            throw new ResultStateError("Result is not a FinalRuleResult");
        int size = this.nBest[rank].length;
        ResultToken[] result = new ResultToken[size];
        while (size-- > 0) result[size] = new BaseResultToken(this.nBest[rank][size]);
        return result;
    }

    /**
     * Get the RuleGrammar matched by the nBest guess for this FinalRuleResult.
     * From javax.speech.recognition.FinalRuleResult.
     */
    public RuleGrammar getRuleGrammar(int rank) throws ResultStateError {
        checkResultState(UNFINALIZED);
        if (!(grammar instanceof RuleGrammar))
            throw new ResultStateError("Result is not a FinalRuleResult");
        return (RuleGrammar) grammar;
    }

    /**
     * Get the name of the Rule matched by the nBest guess for this FinalRuleResult.
     * From javax.speech.recognition.FinalRuleResult.
     */
    public String getRuleName(int nBest) throws ResultStateError {
        checkResultState(UNFINALIZED);
        if (!(grammar instanceof RuleGrammar))
            throw new ResultStateError("Result is not a FinalRuleResult");
        return ruleName;
    }

    /**
     * Return the list of tags matched by the best-guess token sequence.
     * From javax.speech.recognition.FinalRuleResult.
     */
    public String[] getTags() throws ResultStateError {
        checkResultState(UNFINALIZED);
        if (!(grammar instanceof RuleGrammar))
            throw new ResultStateError("Result is not a FinalRuleResult");
        return tags;
    }

//////////////////////
// End FinalRuleResult Methods
//////////////////////

//////////////////////
// Begin FinalDictationResult Methods
//////////////////////

    /**
     * NOT IMPLEMENTED YET.
     * Get a set of alternative token guesses for a single known token or sequence of tokens.
     * From javax.speech.recognition.FinalDictationResult.
     */
    public ResultToken[][] getAlternativeTokens(ResultToken from, ResultToken to, int max) throws ResultStateError, IllegalArgumentException {
        checkResultState(UNFINALIZED);
        if (!(grammar instanceof DictationGrammar))
            throw new ResultStateError("Result is not a FinalDicationResult");
        return null;
    }

//////////////////////
// End FinalDictationResult Methods
//////////////////////

//////////////////////
// Begin utility methods for sending ResultEvents
//////////////////////

    /**
     * Utility function to generate RESULT_UPDATED event and send it to all result listeners.
     * The Result listeners will be notified first, followed by the grammar listeners and then the recognizer listeners.
     */
    public void resultUpdated(BaseRecognizer recognizer, BaseGrammar grammar) {
        resultUpdated(recognizer, grammar, false, false);
    }

    public void resultUpdated(BaseRecognizer recognizer, BaseGrammar grammar, boolean tokenFinalized, boolean unfinalizedTokensChanged) {
        ResultEvent event = new ResultEvent(this, ResultEvent.RESULT_UPDATED, tokenFinalized, unfinalizedTokensChanged);
        Enumeration listeners;
        if (resultListeners != null) {
            for (ResultListener resultListener : resultListeners) {
                resultListener.resultUpdated(event);
            }
        }
        if ((grammar != null) && (grammar.resultListeners != null)) {
            for (ResultListener resultListener : grammar.resultListeners) {
                resultListener.resultUpdated(event);
            }
        }
        if ((recognizer != null) && (recognizer.resultListeners != null)) {
            for (ResultListener resultListener : recognizer.resultListeners) {
                resultListener.resultUpdated(event);
            }
        }
    }

    /**
     * Utility function to generate GRAMMAR_FINALIZED event and send it to all result listeners.
     * The Result listeners will be notified first, followed by the grammar listeners and then the recognizer listeners.
     */
    public void grammarFinalized(BaseRecognizer recognizer, BaseGrammar grammar) {
        grammarFinalized(recognizer, grammar, false, false);
    }

    public void grammarFinalized(BaseRecognizer recognizer, BaseGrammar grammar, boolean tokenFinalized, boolean unfinalizedTokensChanged) {
        ResultEvent event = new ResultEvent(this, ResultEvent.GRAMMAR_FINALIZED, tokenFinalized, unfinalizedTokensChanged);
        Enumeration listeners;
        if (resultListeners != null) {
            for (ResultListener resultListener : resultListeners) {
                resultListener.grammarFinalized(event);
            }
        }
        if ((grammar != null) && (grammar.resultListeners != null)) {
            for (ResultListener resultListener : grammar.resultListeners) {
                resultListener.grammarFinalized(event);
            }
        }
        if ((recognizer != null) && (recognizer.resultListeners != null)) {
            for (ResultListener resultListener : recognizer.resultListeners) {
                resultListener.grammarFinalized(event);
            }
        }
    }

    /**
     * Utility function to generate RESULT_ACCEPTED event and send it to all result listeners.
     * The Result listeners will be notified first, followed by the grammar listeners and then the recognizer listeners.
     */
    public void resultAccepted(BaseRecognizer recognizer, BaseGrammar grammar) {
        resultAccepted(recognizer, grammar, false, false);
    }

    public void resultAccepted(BaseRecognizer recognizer, BaseGrammar grammar, boolean tokenFinalized, boolean unfinalizedTokensChanged) {
        ResultEvent event = new ResultEvent(this, ResultEvent.RESULT_ACCEPTED, tokenFinalized, unfinalizedTokensChanged);
        Enumeration listeners;
        if (resultListeners != null) {
            for (ResultListener resultListener : new LinkedHashSet<>(resultListeners)) {
                resultListener.resultAccepted(event);
            }
        }
        if ((grammar != null) && (grammar.resultListeners != null)) {
            for (ResultListener resultListener : new LinkedHashSet<>(grammar.resultListeners)) {
                resultListener.resultAccepted(event);
            }
        }
        if ((recognizer != null) && (recognizer.resultListeners != null)) {
            for (ResultListener resultListener : new LinkedHashSet<>(recognizer.resultListeners)) {
                resultListener.resultAccepted(event);
            }
        }
    }

    /**
     * Utility function to generate RESULT_REJECTED event and send it to all result listeners.
     * The Result listeners will be notified first, followed by the grammar listeners and then the recognizer listeners.
     */
    public void resultRejected(BaseRecognizer recognizer, BaseGrammar grammar) {
        resultRejected(recognizer, grammar, false, false);
    }

    public void resultRejected(BaseRecognizer recognizer, BaseGrammar grammar, boolean tokenFinalized, boolean unfinalizedTokensChanged) {
        ResultEvent event = new ResultEvent(this, ResultEvent.RESULT_REJECTED, tokenFinalized, unfinalizedTokensChanged);
        Enumeration listeners;
        if (resultListeners != null) {
            for (ResultListener resultListener : new LinkedHashSet<>(resultListeners)) {
                resultListener.resultRejected(event);
            }
        }
        if ((grammar != null) && (grammar.resultListeners != null)) {
            for (ResultListener resultListener : new LinkedHashSet<>(grammar.resultListeners)) {
                resultListener.resultRejected(event);
            }
        }
        if ((recognizer != null) && (recognizer.resultListeners != null)) {
            for (ResultListener resultListener : new LinkedHashSet<>(recognizer.resultListeners)) {
                resultListener.resultRejected(event);
            }
        }
    }

    /**
     * Utility function to generate AUDIO_RELEASED event and send it to all result listeners.
     * The Result listeners will be notified first, followed by the grammar listeners and then the recognizer listeners.
     */
    public void audioReleased(BaseRecognizer recognizer, BaseGrammar grammar) {
        ResultEvent event = new ResultEvent(this, ResultEvent.AUDIO_RELEASED);
        Enumeration listeners;
        if (resultListeners != null) {
            for (ResultListener resultListener : resultListeners) {
                resultListener.audioReleased(event);
            }
        }
        if ((grammar != null) && (grammar.resultListeners != null)) {
            for (ResultListener resultListener : grammar.resultListeners) {
                resultListener.audioReleased(event);
            }
        }
        if ((recognizer != null) && (recognizer.resultListeners != null)) {
            for (ResultListener resultListener : recognizer.resultListeners) {
                resultListener.audioReleased(event);
            }
        }
    }

    /**
     * Utility function to generate TRAINING_INFO_RELEASED event and send it to all result listeners.
     * The Result listeners will be notified first, followed by the grammar listeners and then the recognizer listeners.
     */
    public void trainingInfoReleased(BaseRecognizer recognizer, BaseGrammar grammar) {
        ResultEvent event = new ResultEvent(this, ResultEvent.RESULT_CREATED);
        Enumeration listeners;
        if (resultListeners != null) {
            for (ResultListener resultListener : resultListeners) {
                resultListener.trainingInfoReleased(event);
            }
        }
        if ((grammar != null) && (grammar.resultListeners != null)) {
            for (ResultListener resultListener : grammar.resultListeners) {
                resultListener.trainingInfoReleased(event);
            }
        }
        if ((recognizer != null) && (recognizer.resultListeners != null)) {
            for (ResultListener resultListener : recognizer.resultListeners) {
                resultListener.trainingInfoReleased(event);
            }
        }
    }

//////////////////////
// End utility methods for sending ResultEvents
//////////////////////

    /**
     * Concatenate the best tokens in the Result.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer(numTokens() > 0 ? getBestToken(0).getWrittenText() : "");
        for (int i = 1; i < numTokens(); i++)
            sb.append(" " + getBestToken(i).getWrittenText());
        return sb.toString();
    }

    protected class BaseResultToken implements ResultToken {
        String token;

        BaseResultToken(String t) {
            token = t;
        }

        public Result getResult() {
            return BaseResult.this;
        }

        public int getAttachmentHint() {
            return 0;
        }

        public int getCapitalizationHint() {
            return 0;
        }

        public String getWrittenText() {
            return token;
        }

        public String getSpokenText() {
            return token;
        }

        public long getStartTime() {
            return 0;
        }

        public long getEndTime() {
            return 0;
        }

        public int getSpacingHint() {
            return ResultToken.SEPARATE;
        }
    }

    public void addUnfinalizedToken(String token) {
        if (unfinalizedTokens == null) unfinalizedTokens = new Vector();
        unfinalizedTokens.addElement(token);
    }

    public void finalizeTokens(String token) {
        if (unfinalizedTokens != null) {
            int size = unfinalizedTokens.size();
            this.nBest = new String[1][];
            this.nBest[0] = new String[size];
            while (size-- > 0) this.nBest[0][size] = (String) unfinalizedTokens.elementAt(size);
            unfinalizedTokens.removeAllElements();
        }
    }

    public void addFinalizedToken(String token) {
        if (this.nBest == null) this.nBest = new String[1][];
        if (this.nBest[0] == null) {
            this.nBest[0] = new String[1];
            this.nBest[0][0] = token;
        } else {
            String[] oldFinals = this.nBest[0];
            this.nBest[0] = new String[oldFinals.length + 1];
            System.arraycopy(oldFinals, 0, this.nBest[0], 0, oldFinals.length);
            this.nBest[0][oldFinals.length] = token;
        }
    }

    /**
     * Utility function to set the grammar.
     */
    public void setGrammar(Grammar grammar) {
        this.grammar = grammar;
    }

    /**
     * Utility function to set the result state.
     */
    public void setResultState(int state) {
        this.state = state;
    }

    /**
     * If the result is in the given state, throw a ResultStateError.
     */
    protected void checkResultState(int state) throws ResultStateError {
        if (getResultState() == state)
            throw new ResultStateError("BaseResult.checkResultState: invalid state " + getResultState());
    }
}
