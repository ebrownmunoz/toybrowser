/*
 * Copyright (c) 1997-8 Sun Microsystems, Inc. All Rights Reserved.
 */
package voxware.engine.recognition;

import javax.speech.recognition.*;

import java.io.PrintStream;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

/**
 * Utilities methods.
 */
public class RecognizerUtilities {

    static public PrintStream errOutput = System.err;

    /**
     * Copy all RuleGrammars from recFrom to recTo.
     */
    public static void copyGrammars(Recognizer recFrom, Recognizer recTo)
        throws GrammarException 
    {
        RuleGrammar gramFrom[] = recFrom.listRuleGrammars();

        for(int i=0; i<gramFrom.length; i++) {
            String name = gramFrom[i].getName();
            RuleGrammar gramTo = recTo.getRuleGrammar(name);

            // Create a new grammar - if necessary
            if (gramTo == null) {
                try {
                    gramTo = recTo.newRuleGrammar(name);
                } catch(IllegalArgumentException gse) {
                    throw new GrammarException("copyGrammars: " + gse.toString(), null);
                }
            }

            // Copy imports
            RuleName imports[] = gramFrom[i].listImports();
            for(int j=0; j<imports.length; j++) {
                gramTo.addImport(imports[j]);
            }

            // Copy each rule
            String rnames[] = gramFrom[i].listRuleNames();
            for(int j=0; j<rnames.length; j++) {
                String ruleName = rnames[j];
                boolean isPublic = false;
                try { 
                    isPublic = gramFrom[i].isRulePublic(ruleName);
                } catch (IllegalArgumentException nse) {
                    throw new GrammarException(nse.toString(), null);
                }

                Rule rule = gramFrom[i].getRule(ruleName);
                try {
                    gramTo.setRule(ruleName, rule, isPublic);
                    if (gramFrom[i].isEnabled(ruleName)) {
                        gramTo.setEnabled(ruleName, true); 
                    }
                } catch (IllegalArgumentException E) {
                    throw new GrammarException(E.toString(), null);
                }

                // copy sample sentences if appropriate
                if ((gramTo instanceof BaseRuleGrammar) && (gramFrom[i] instanceof BaseRuleGrammar)) {
                    Vector a = ((BaseRuleGrammar)gramFrom[i]).getSampleSentences(ruleName);
                    ((BaseRuleGrammar)gramTo).setSampleSentences(ruleName,a);
                }
            }
        }
    }
    

    static Hashtable xrefs = null;

    /**
     * For a specified recognizer, build a table that for each rule R 
     * lists all rules in all grammars that reference R.
     *
     * @see getRuleNameRefs
     * @see getRefsToRuleName
     */
    static public void buildXrefTable(Recognizer rec) {
        xrefs = new Hashtable();
        RuleGrammar[] grams = rec.listRuleGrammars();

        if (grams == null) {
            return;
        }

        for (int i=0; i<grams.length; i++) {
            String[] names = grams[i].listRuleNames();
            if (names == null) {
                continue;
            }
            for (int j=0; j<names.length; j++) {
                // Get the definition of rule name[j] in gram[i]
                Rule r = grams[i].getRule(names[j]);

                // Build a fully-qualified RuleName for rule name[j] in gram[i]
                RuleName rn = new RuleName(grams[i].getName() + "." + names[j]);
                
                // Identify all rules referenced in r
                Vector refs = new Vector();
                getRuleNameRefs(r, refs);

                for (int k=0; k<refs.size(); k++) {
                    RuleName ref = (RuleName)(refs.elementAt(k));

                    // Get a fully-qualified reference
                    RuleName fullref;
                    try {
                        fullref = ((BaseRuleGrammar)(grams[i])).resolve(ref);
                    } catch(GrammarException e) {
                        fullref = null;
                    }

                    if (fullref != null) {
                        Hashtable h = (Hashtable)(xrefs.get(fullref.toString().intern()));

                        if (h == null) {
                            h = new Hashtable();
                        }
                        
                        h.put(rn.toString().intern(), "dummy");
                        xrefs.put(fullref.toString().intern(), h);
                    }
                    else {
                        debugMessageOut("Warning: unresolved rule " + ref.toString() +
                                        " in grammar " + grams[i].getName());
                    }
                }
            }
        }
    }

    /**
     * Return an array of references to rule r in grammar g.
     */
    static public RuleName[] getRefsToRuleName(RuleGrammar g, RuleName r) {
        // Ensure we have a fully qualified rulename
        // (that's how the xref table works)
        r = new RuleName(g.getName() + "." + r.getSimpleRuleName());

        if (xrefs == null) {
            return null;
        }

        Hashtable h = (Hashtable)(xrefs.get(r.toString().intern()));

        if (h == null) {
            return null;
        }

        RuleName[] rulenames = new RuleName[h.size()];
        
        int i=0;
        for (Enumeration e = h.keys(); e.hasMoreElements();) {
            String name = (String)(e.nextElement());
            rulenames[i++] = new RuleName(name);
        }

        return rulenames;
    }


    static protected void getRuleNameRefs(Rule r, Vector refs)
    {
        if (r instanceof RuleAlternatives || r instanceof RuleSequence) {
            Rule[] array;
            if (r instanceof RuleAlternatives) {
                array = ((RuleAlternatives)r).getRules();
            } else {
                array = ((RuleSequence)r).getRules();
            }

            if (array != null) {
                for (int i=0; i<array.length; i++) {
                    getRuleNameRefs(array[i], refs);
                }
            }
        } else if (r instanceof RuleTag) {
            getRuleNameRefs(((RuleTag)r).getRule(), refs);
        } else if (r instanceof RuleCount) {
            getRuleNameRefs(((RuleCount)r).getRule(), refs);
        } else if (r instanceof RuleName) {
            // Put a copy in the Xref list (avoid side-effects of linking)
            RuleName tmp = (RuleName)r;
            refs.addElement(new RuleName(tmp.getRuleName()));
        }
    }
  
    static private Hashtable oldMessages = new Hashtable();

    /**
     * Print out a message - don't ever repeat it.
     */
    static protected void debugMessageOut(String message) {
        message = message.intern();
        if (oldMessages.get(message) != null) {
            return;
        }
        errOutput.println(message);
        oldMessages.put(message, "dummy");
    }
}




