package voxware.engine.recognition.sapivise;

public class ViseResultMessage {
  public long nObj;
  public String[] nbest;
  public String hostTrans;
  public boolean accepted;

  public ViseResultMessage(long nObj, String[] nbest, String hostTrans, boolean accepted) {
    this.nObj = nObj;
    this.nbest = nbest;
    this.hostTrans = hostTrans;
    this.accepted = accepted;
  }
}





  