package voxware.engine.recognition.sapivise;

import java.io.*;

public class ParameterResult {
  boolean CMStatus = true;
  int     CMIntVal = 0;
  float   CMFloatVal = (float) 0.;

  public ParameterResult() {
  }

  public ParameterResult(boolean status, int intVal, float floatVal) {
    CMStatus = status;
    CMIntVal = intVal;
    CMFloatVal = floatVal;
  }

  public boolean Status() { return CMStatus; }
  public int     IntVal() { return CMIntVal; }
  public float   FloatVal() { return CMFloatVal; }
}