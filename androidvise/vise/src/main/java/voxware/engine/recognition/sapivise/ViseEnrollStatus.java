package voxware.engine.recognition.sapivise;

public class ViseEnrollStatus {

  private int CMStatus;
  private int CMCount;
  private int CMNeeded;

  public ViseEnrollStatus(int status, int count, int needed) {
    CMStatus = status;
    CMCount  = count;
    CMNeeded = needed;
  }

  public int Status() {
    return CMStatus;
  }

  public int Count() {
    return CMCount;
  }

  public int Needed() {
    return CMNeeded;
  }
}
  