package voxware.engine.recognition.sapivise;

import android.support.annotation.Keep;

import java.lang.*;
import java.nio.ByteBuffer;
import java.util.*;

public class RawResult {

    boolean accepted = false;
    boolean training = false;
    boolean audio = false;
    ByteBuffer result = null;
    String grammarName = "";
    String[] nbestArray = null;
    String hostTrans = "";

    public RawResult() {
    }
}
