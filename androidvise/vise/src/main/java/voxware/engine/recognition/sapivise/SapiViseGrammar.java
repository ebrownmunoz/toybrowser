package voxware.engine.recognition.sapivise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.speech.recognition.Rule;
import javax.speech.recognition.RuleName;

import voxware.engine.recognition.BaseRecognizer;
import voxware.engine.recognition.BaseRuleGrammar;

public class SapiViseGrammar extends BaseRuleGrammar {
    private static final Logger log = LoggerFactory.getLogger(BaseRuleGrammar.class);
    public static final String EnrollmentRule = "<ANY_WORD>";
    public static final String CalibrationRule = "<CALIBRATION>";

    static {
        System.loadLibrary("vise");
    }

    private native ByteBuffer newGrammar(ByteBuffer sapivise);

    private native int n_release();

    private native void n_load(byte[] recImage);

    public native void n_setactive(String name, boolean state);

    public native String n_grammarname();

    public native String n_defaultrulename();

    private native String[] n_parse(String phrase);

    private native String[] n_rulenames();

    private native String[] n_trainphrase(int extent);

    private native boolean n_startpass();

    private native boolean n_endpass(String[] words, boolean finalPass);

    private native boolean n_savemodels(String[] words, int minCount);

    public static final int RECOMMENDED = 1;
    public static final int REQUIRED = 2;

    private byte recImage[];  // Kept around pending future versions that can switch RuleGrammars
    private String defaultRuleName;

    ByteBuffer grammar;                 // A reference to the underlying CNISapiViseGram
    ByteBuffer sapivise;                // the owning CNISapiVise engine

    public SapiViseGrammar(SapiVise sapiVise, String name) {
        super((BaseRecognizer) sapiVise, name);
        grammar = newGrammar(sapiVise.recognizer);

        log.info("SapiViseGrammar constructor CALLED");

    }

    SapiViseGrammar(SapiVise sapiVise, InputStream recStream) throws IOException {
        // The grammar (application) name is supplied by the load(); no name is yet known here
        this(sapiVise, (String) null);
        // Read the grammar image from recStream into the recImage buffer
        int recSize = recStream.available();            // Throws IOException
        this.recImage = new byte[recSize];
        recStream.read(this.recImage);                  // Throws IOException
        // Load the grammar into SAPIVISE and get its name and the names of its rules
        this.load();
    }

    public void deallocate() {
        int refCount = n_release();
        if (refCount == 0) {
            grammar = null;
            log.info("SapiViseGrammar.deallocate reduced CNISapiViseGram refCount to " + refCount);
        } else {
            log.warn("SapiViseGrammar.deallocate: UNEXPECTED - refCount (" + refCount + ") non-zero after deallocation");
        }
    }

    protected void finalize() throws Throwable {
        try {
            if (grammar != null) {
                log.warn("SapiViseGrammar.finalize: UNEXPECTED - underlying CNISapiViseGram not deallocated");
            }
        } finally {
            super.finalize();
        }
    }

    public boolean isResultAudioProvided() {
        return (((SapiVise) recognizer).getParameter(ViseProperties.VISE_AUDQUEUELEN, 0).IntVal() > 0);
    }

    public boolean isTrainingProvided() {
        return ((SapiVise) recognizer).trainingProvided;
    }

    // Load SAPIVISE with the recfile image in recImage
    protected void load() {

        // Load SAPIVISE with the recfile image in recImage
        n_load(recImage);

        // Get the names of things
        myName = n_grammarname();

        String[] ruleNames = n_rulenames();
        defaultRuleName = n_defaultrulename();
        // If there is no default rule name, use the first rule name as the default
        if (defaultRuleName == null && ruleNames.length > 0) {
            defaultRuleName = ruleNames[0];
        }
        log.info("SapiViseGrammar.load: the default rule for grammar \"" + myName + "\" is \"" + getDefaultRuleName() + "\"");

        // Add the enrollment rule
        RuleName rn = new RuleName(null, getName(), EnrollmentRule);
        setRule(EnrollmentRule, (Rule) rn, true);

        // Add the calibration rule
        rn = new RuleName(null, getName(), CalibrationRule);
        setRule(CalibrationRule, (Rule) rn, true);

        // Add all the grammar's rules to the superclass's rule hash
        for (int i = 0; i < ruleNames.length; i++) {
            rn = new RuleName(null, getName(), ruleNames[i]);
            setRule(ruleNames[i], (Rule) rn, true);
            log.info("SapiViseGrammar.load: rule[" + i + "] is \"" + ruleNames[i] + "\"");
        }
    }

    public String getDefaultRuleName() {
        return defaultRuleName;
    }

    // Overrides activate() in BaseGrammar to add actual SAPIVISE activate/deactivate call
    public boolean activate(boolean active) {
        String rnames[] = listRuleNames();
        log.debug("SapiViseGrammar.activate: " + (active ? "activating" : "deactivating") + " grammar \"" + this.getName() + "\"");
        for (int j = 0; j < rnames.length; j++) {
            if (isRulePublic(rnames[j])) {
                boolean activate = active && isEnabled(rnames[j]);
                log.debug("  Rule \"{}\" {}", rnames[j], (activate ? "activated" : "deactivated"));
                this.n_setactive(rnames[j], activate);
            }
        }
        return super.activate(active);
    }

    public int addRule(RuleName rn) {
        int i = ((BaseRuleGrammar) ((SapiVise) recognizer).currentGrammar).getRuleID(rn.getSimpleRuleName());
        if (i == -1) {
            String gname = rn.getSimpleGrammarName();
            SapiViseGrammar SG = (SapiViseGrammar) (recognizer.getRuleGrammar(gname));
            if (SG == null) {
                return 0;
            }
            i = SG.getRuleID(rn.getSimpleRuleName());
        }
        return i;
    }

    public String[] parse(String phrase) {
        String[] ruleNames = null;

        if (phrase == null) {
            log.error("SapiViseGrammar.parse: ERROR - null phrase");
        } else {
            ruleNames = n_parse(phrase);
        }
        return ruleNames;
    }

    public String[] trainPhrase(int extent) {
        String[] prompts = null;

        switch (extent) {
            case RECOMMENDED:
            case REQUIRED:
                prompts = n_trainphrase(extent);
                break;
            default:
                log.error("SapiViseGrammar.trainPhrase: ERROR - unknown/unsupported extent " + extent);
        }
        return prompts;
    }

    public boolean startTrainingPass() {
        return n_startpass();
    }

    public boolean endTrainingPass() {
        return endTrainingPass(null);
    }

    public boolean endTrainingPass(String[] words) {
        return n_endpass(words, false);
    }

    public boolean completeTraining() {
        return completeTraining(null);
    }

    public boolean completeTraining(String[] words) {
        return n_endpass(words, true);
    }

    public boolean saveModels(int minTrainingCount) {
        return n_savemodels(null, minTrainingCount);
    }

    public boolean saveModels(String[] words, int minTrainingCount) {
        return n_savemodels(words, minTrainingCount);
    }

    public String toString() {
        return getClass().getName() + '@' + Integer.toHexString(hashCode());
    }
}
