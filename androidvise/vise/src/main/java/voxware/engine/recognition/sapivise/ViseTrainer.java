package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;

public class ViseTrainer {

    SapiVise CMSapiVise;
    ByteBuffer nativePeer = null;
    Boolean released = null;
//    ByteBuffer IVbxSpkrTrainPtr = null;
//    ByteBuffer IVbxTrainPtr = null;
//    ByteBuffer TrainerPtr = null;

    public static final int DATAFILEERROR = 100;
    public static final int NOTENOUGHDATA = 101;
    public static final int VALUEOUTOFRANGE = 102;

    private native boolean n_initialize(ByteBuffer sapiViseObj);

    private native boolean n_release();

    private native boolean n_insertcorrection(String transcription, ViseResult result);

    private native ViseEnrollStatus n_enrollword();

    // Need this to prevent premature finalization of ViseResult objects,
    // while they still are processed in the "native" warld
    private Vector refHolder = null;

    public ViseTrainer(SapiVise sapiVise) {
        CMSapiVise = sapiVise;
        n_initialize(sapiVise.recognizer);
        refHolder = new Vector();
    }

    public boolean insertCorrection(String transcription, ViseResult result) {
        boolean ret = n_insertcorrection(transcription, result);
        // Save reference
        if (ret) refHolder.add(result);
        return ret;
    }

    public ViseEnrollStatus enrollWord() {
        return n_enrollword();
    }

    public synchronized boolean release() {
        if (released == null) {
            // Relieve results objects of training info not waiting for their finalization
            for (int i = 0; i < refHolder.size(); i++)
                ((ViseResult) refHolder.elementAt(i)).releaseTrainingInfo();
            // Allow finalization of java results object
            refHolder.removeAllElements();
            released = n_release();
        }
        return released;
    }

    // If somebody forgets explicitly to call release() in application
    public void finalize() {
        release();
    }
}
