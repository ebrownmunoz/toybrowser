package voxware.engine.recognition.sapivise;

import javax.speech.*;
import javax.speech.recognition.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class ViseTrainerFactory {
  SapiVise CMSapiVise;

  public ViseTrainerFactory(SapiVise sapiVise) {
    CMSapiVise = sapiVise;
  }

  public ViseTrainer getTrainer() {
    return new ViseTrainer(CMSapiVise);
  }
}
    