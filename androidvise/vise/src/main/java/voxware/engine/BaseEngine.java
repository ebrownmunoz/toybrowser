/*
 * Copyright (c) 1997-8 Sun Microsystems, Inc. All Rights Reserved.
 */
package voxware.engine;

import javax.speech.*;
import javax.speech.recognition.*;

import java.net.*;
import java.io.*;
import java.util.*;

import voxware.util.Mailbox;
import voxware.util.ObjectSet;

/**
 * Skeletal Implementation of the JSAPI Engine interface.
 * <p/>
 * <P>Actual JSAPI implementations might want to extend or
 * modify this implementation.
 * <p/>
 *
 * @author Stuart Adams
 * @author Willie Walker
 * @version 1.3 12/14/98 16:03:44
 */
public class BaseEngine implements Engine {

    protected long engineState;
    protected Set<EngineListener> engineListeners;
    protected AudioManager audioManager = null;
    protected EngineModeDesc engineModeDesc = null;

//////////////////////
// Begin Constructors
//////////////////////

    /**
     * Create a new Engine in the DEALLOCATED state.
     */
    public BaseEngine(EngineModeDesc mode) {
        engineModeDesc = mode;
        engineListeners = new LinkedHashSet<>();
        engineState = DEALLOCATED;
    }

//////////////////////
// End Constructors
//////////////////////

//////////////////////
// Begin Engine Methods
//////////////////////

    /**
     * Get the current state of the Engine.
     * From javax.speech.Engine.
     */
    public long getEngineState() {
        return engineState;
    }

    /**
     * Wait until the Engine state has the current state.
     * From javax.speech.Engine.
     *
     * @param state the state to wait for.
     * @throws InterruptedException
     * @throws IllegalArgumentException
     */
    public synchronized void waitEngineState(long state) throws InterruptedException, IllegalArgumentException {
        StateChangeListener listener = new StateChangeListener(state);
        engineListeners.add(listener);
        listener.waitForTargetState();      // Throws InterruptedException
        engineListeners.remove(listener);
    }

    /**
     * Test to see if the Engine state has the current state.
     * From javax.speech.Engine.
     *
     * @param state the state to test.
     * @throws IllegalArgumentException
     */
    public synchronized boolean testEngineState(long state) throws IllegalArgumentException {
        return ((engineState & state) == state);
    }

    /**
     * Allocate the resources for the Engine and put it in the ALLOCATED state.
     * From javax.speech.Engine.
     *
     * @throws EngineException
     * @throws EngineStateError if called on an engine in the DEALLOCATING_RESOURCES state
     */
    public void allocate() throws EngineException, EngineStateError {

        // If not already in the ALLOCATED state, enter the ALLOCATING_RESOURCES state
        synchronized (this) {
            checkEngineState(DEALLOCATING_RESOURCES);               // Throws EngineStateError
            if (testEngineState(ALLOCATED)) return;
            long oldEngineState = engineState;
            engineState = ALLOCATING_RESOURCES;
            engineAllocatingResources(oldEngineState, engineState);
        }

        // Do the real work of allocation
        allocateEngine();                                         // Throws EngineException

        // Enter the ALLOCATED state.  Use | because something could have called pause(), etc.
        synchronized (this) {
            long oldEngineState = engineState;
            engineState &= ~ALLOCATING_RESOURCES;
            engineState |= ALLOCATED;
            engineAllocated(oldEngineState, engineState);
        }
    }

    /**
     * Deallocate the resources for the Engine and put it in the DEALLOCATED state.
     * From javax.speech.Engine.
     *
     * @throws EngineException
     * @throws EngineStateError if called on an engine in the ALLOCATING_RESOURCES state
     */
    public void deallocate() throws EngineException, EngineStateError {

        // If not already in the DEALLOCATED state, enter the DEALLOCATING_RESOURCES state
        synchronized (this) {
            checkEngineState(ALLOCATING_RESOURCES);                 // Throws EngineStateError
            if (testEngineState(DEALLOCATED)) return;
            long oldEngineState = engineState;
            engineState = DEALLOCATING_RESOURCES;
            engineDeallocatingResources(oldEngineState, engineState);
        }

        // Do the real work of deallocation
        deallocateEngine();                                       // Throws EngineException

        // Enter the DEALLOCATED state
        synchronized (this) {
            long oldEngineState = engineState;
            engineState = DEALLOCATED;
            engineDeallocated(oldEngineState, engineState);
        }
    }

    /**
     * Pause the audio stream for the Engine and put the Engine into the PAUSED state.
     * From javax.speech.Engine.
     *
     * @throws EngineStateError if called on an engine in the DEALLOCATED or DEALLOCATING_RESOURCES state
     */
    public synchronized void pause() throws EngineStateError {

        checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);   // Throws EngineStateError
        if (testEngineState(PAUSED)) return;

        pauseEngine();                                            // Throws EngineStateError

        long oldEngineState = engineState;
        engineState &= ~RESUMED;
        engineState |= PAUSED;
        enginePaused(oldEngineState, engineState);
    }

    /**
     * Resume the audio stream for the Engine and put the Engine into the RESUMED state.
     * From javax.speech.Engine.
     *
     * @throws AudioException
     * @throws EngineStateError if called on an engine in the DEALLOCATED or DEALLOCATING_RESOURCES state
     */
    public synchronized void resume() throws AudioException, EngineStateError {

        checkEngineState(DEALLOCATED | DEALLOCATING_RESOURCES);   // Throws EngineStateError
        if (testEngineState(RESUMED)) return;

        resumeEngine();                                           // Throws AudioException, EngineStateError

        long oldEngineState = engineState;
        engineState &= ~PAUSED;
        engineState |= RESUMED;
        engineResumed(oldEngineState, engineState);
    }

    /**
     * Return an object that provides management of the audio input.
     * or output of the Engine.
     * From javax.speech.Engine.
     */
    public synchronized AudioManager getAudioManager() {
        if (audioManager == null) audioManager = new BaseAudioManager();
        return audioManager;
    }

    /**
     * Return an object that provides management of the vocabulary for the Engine.
     * From javax.speech.Engine.
     *
     * @throws EngineStateError
     */
    public VocabManager getVocabManager() throws EngineStateError {
        return null;
    }

    /**
     * NOT IMPLEMENTED YET.
     * Get the EngineProperties of this Engine.
     * From javax.speech.Engine.
     */
    public EngineProperties getEngineProperties() {
        throw new RuntimeException("BaseEngine.getEngineProperties: not yet implemented");
    }

    /**
     * NOT IMPLEMENTED YET.
     * Get the EngineModeDesc of this Engine.
     * From javax.speech.Engine.
     *
     * @throws SecurityException
     */
    public EngineModeDesc getEngineModeDesc() throws SecurityException {
        return engineModeDesc;
    }

    /**
     * Request notification of Engine events from the Engine.
     * From javax.speech.Engine.
     *
     * @param listener the listener to add.
     */
    public void addEngineListener(EngineListener listener) {
        engineListeners.add(listener);
    }

    /**
     * Remove an EngineListener from the list of EngineListeners.
     * From javax.speech.Engine.
     *
     * @param listener the listener to remove.
     */
    public void removeEngineListener(EngineListener listener) {
        engineListeners.remove(listener);
    }

//////////////////////
// End Engine Methods
//////////////////////

//////////////////////
// Begin utility methods for sending EngineEvents
//////////////////////

    /**
     * Utility function to generate ENGINE_ALLOCATING_RESOURCES event and send it to all engine listeners.
     */
    protected synchronized void engineAllocatingResources(long oldState, long newState) {
        if (engineListeners == null) return;
        EngineEvent event = new EngineEvent(this, EngineEvent.ENGINE_ALLOCATING_RESOURCES, oldState, newState);
        for (EngineListener engineListener : engineListeners) {
            engineListener.engineAllocatingResources(event);
        }
    }

    /**
     * Utility function to generate ENGINE_ALLOCATED event and send it to all engine listeners.
     */
    protected synchronized void engineAllocated(long oldState, long newState) {
        if (engineListeners == null) return;
        EngineEvent event = new EngineEvent(this, EngineEvent.ENGINE_ALLOCATED, oldState, newState);
        for (EngineListener engineListener : engineListeners) {
            engineListener.engineAllocated(event);
        }
    }

    /**
     * Utility function to generate ENGINE_DEALLOCATING_RESOURCES event and send it to all engine listeners.
     */
    protected synchronized void engineDeallocatingResources(long oldState, long newState) {
        if (engineListeners == null) return;
        EngineEvent event = new EngineEvent(this, EngineEvent.ENGINE_DEALLOCATING_RESOURCES, oldState, newState);
        for (EngineListener engineListener : engineListeners) {
            engineListener.engineDeallocatingResources(event);
        }
    }

    /**
     * Utility function to generate ENGINE_DEALLOCATED event and send it to all engine listeners.
     */
    protected synchronized void engineDeallocated(long oldState, long newState) {
        if (engineListeners == null) return;
        EngineEvent event = new EngineEvent(this, EngineEvent.ENGINE_DEALLOCATED, oldState, newState);
        for (EngineListener engineListener : engineListeners) {
            engineListener.engineDeallocated(event);
        }
    }

    /**
     * Utility function to generate ENGINE_PAUSED event and send it to all engine listeners.
     */
    protected synchronized void enginePaused(long oldState, long newState) {
        if (engineListeners == null) return;
        EngineEvent event = new EngineEvent(this, EngineEvent.ENGINE_PAUSED, oldState, newState);
        for (EngineListener engineListener : engineListeners) {
            engineListener.enginePaused(event);
        }
    }

    /**
     * Utility function to generate ENGINE_RESUMED event and send it to all engine listeners.
     */
    protected synchronized void engineResumed(long oldState, long newState) {
        if (engineListeners == null) return;
        EngineEvent event = new EngineEvent(this, EngineEvent.ENGINE_RESUMED, oldState, newState);
        for (EngineListener engineListener : engineListeners) {
            engineListener.engineResumed(event);
        }
    }

//////////////////////
// End utility methods for sending EngineEvents
//////////////////////

/////////////////////
// Other non-JSAPI methods
/////////////////////

    /**
     * Throw an EngineStateError if ANY of the bits in the specified state are set in the Engine.
     *
     * @throws EngineStateError if ANY of the bits in the specified state are set in the Engine
     */
    protected synchronized void checkEngineState(long state) throws EngineStateError {
        if ((getEngineState() & state) != 0)
            throw new EngineStateError("Invalid EngineState: 0x" + Long.toHexString(getEngineState()));
    }

    /**
     * Set up the engine
     *
     * @throws EngineException
     */
    protected void allocateEngine() throws EngineException {
    }

    /**
     * Tear down the engine
     *
     * @throws EngineException
     */
    protected void deallocateEngine() throws EngineException {
    }

    /**
     * Pause the engine
     *
     * @throws EngineStateError
     */
    protected void pauseEngine() throws EngineStateError {
    }

    /**
     * Resume the engine
     *
     * @throws AudioException
     * @throws EngineStateError
     */
    protected void resumeEngine() throws AudioException, EngineStateError {
    }

    protected class StateChangeListener implements RecognizerListener {

        private Mailbox mailbox = new Mailbox();
        private long targetState;

        private StateChangeListener(long targetState) {
            this.targetState = targetState;
        }

        protected long waitForTargetState() throws InterruptedException {
            while ((engineState & targetState) != targetState) {
                // Don't really need to acquire the EngineEvent, but it may be useful for debugging
                EngineEvent event = (EngineEvent) mailbox.remove();  // Throws InterruptedException
            }
            return engineState;
        }

        // EngineListener Implementation
        public void engineAllocated(EngineEvent e) {
            mailbox.insert(e);
        }  // ENGINE_ALLOCATED

        public void engineAllocatingResources(EngineEvent e) {
            mailbox.insert(e);
        }  // ENGINE_ALLOCATING_RESOURCES

        public void engineDeallocated(EngineEvent e) {
            mailbox.insert(e);
        }  // ENGINE_DEALLOCATED

        public void engineDeallocatingResources(EngineEvent e) {
            mailbox.insert(e);
        }  // ENGINE_DEALLOCATING_RESOURCES

        public void engineError(EngineErrorEvent e) {
            mailbox.insert(e);
        }

        public void enginePaused(EngineEvent e) {
            mailbox.insert(e);
        }  // ENGINE_PAUSED

        public void engineResumed(EngineEvent e) {
            mailbox.insert(e);
        }  // ENGINE_RESUMED

        // RecognizerListener Implementation
        public void changesCommitted(RecognizerEvent e) {
            mailbox.insert(e);
        }  // CHANGES_COMMITTED

        public void focusGained(RecognizerEvent e) {
            mailbox.insert(e);
        }  // FOCUS_GAINED

        public void focusLost(RecognizerEvent e) {
            mailbox.insert(e);
        }  // FOCUS_LOST

        public void recognizerProcessing(RecognizerEvent e) {
            mailbox.insert(e);
        }  // RECOGNIZER_PROCESSING

        public void recognizerSuspended(RecognizerEvent e) {
            mailbox.insert(e);
        }  // RECOGNIZER_SUSPENDED
    }
}
