/*
 * Copyright (c) 1998 Sun Microsystems, Inc. All Rights Reserved.
 */
package voxware.engine;

import javax.speech.*;
import java.util.*;

/**
 * Skeletal Implementation of the JSAPI AudioManager interface.
 * 
 * <P>Actual JSAPI implementations might want to extend or
 * modify this implementation.
 * <P>
 *
 * @author Willie Walker
 * @version 1.2 12/14/98 16:15:01
 */
public class BaseAudioManager implements AudioManager {
    protected Vector listeners;
    
//////////////////////
// Begin Constructors
//////////////////////
    /** 
     * Create a new AudioManager.
     */
    public BaseAudioManager() {
        listeners = new Vector();
    }
//////////////////////
// End Constructors
//////////////////////

//////////////////////
// Begin AudioManager Methods
//////////////////////
    /**
     * Request notification of AudioEvents from the AudioManager.
     * @param listener the listener to add
     */
    public void addAudioListener(AudioListener listener) {
        if (!listeners.contains(listener)) {
            listeners.addElement(listener);
        }
    }
    
    /**
     * Remove an AudioListener from the list of AudioListeners.
     * @param listener the listener to remove.
     */
    public void removeAudioListener(AudioListener listener) {
        listeners.removeElement(listener);
    }
//////////////////////
// End AudioManager Methods
//////////////////////
}

