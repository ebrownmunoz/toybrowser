/*
 * Copyright © 2015. Voxware, Inc. All Rights Reserved.
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto.
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license
 * keys to control access to the Licensed Software.
 *
 *
 */

package com.voxware.vise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.speech.recognition.Result;
import javax.speech.recognition.ResultStateError;
import javax.speech.recognition.ResultToken;

import voxware.engine.recognition.sapivise.ViseResult;

/**
 * Created by edh on 8/21/2015.
 */
public class RecognitionResult extends com.voxware.browser.io.RecognitionResult {

    private Integer score;

    public static RecognitionResult from(ViseResult viseResult) {
        if (viseResult != null) {
            String hostTrans = viseResult.hostTrans();
            if (viseResult.getResultState() == Result.ACCEPTED) {
                return new RecognitionResult(viseResult);
            } else {
                // viseResult.getResultState() == Result.REJECTED
                // that means a no-hear
                byte[] audio = viseResult.getWAV();
                viseResult.releaseAudio();
                return new RecognitionResult(viseResult, Status.EVENT, "voice", "", null, audio, com.voxware.browser.interpreter.Result.event("noinput", ""));
            }
        } else {
            // speech was suppressed, rejected via other means
            return new RecognitionResult(null, com.voxware.browser.io.RecognitionResult.Status.EVENT, "voice", "", null, null, com.voxware.browser.interpreter.Result.event("noinput", ""));
        }
    }

    protected static List<List<String>> getAlternates(ViseResult viseResult) {
        List<List<String>> alternates = null;
        if (viseResult.getNumberGuesses() > 0) {
            alternates = new ArrayList<List<String>>(viseResult.getNumberGuesses());
            for (int i = 0; i < viseResult.getNumberGuesses(); i++) {
                ResultToken[] tokens = viseResult.getAlternativeTokens(i);
                List<String> tokenStrings = new ArrayList<String>(tokens.length);
                for (ResultToken token : tokens) {
                    tokenStrings.add(token.getSpokenText());
                }
                alternates.add(tokenStrings);
            }
            return alternates;
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    protected RecognitionResult(ViseResult viseResult) {
        this(viseResult, Status.SUCCESS, "voice", viseResult.hostTrans(), getAlternates(viseResult), viseResult.getWAV(), null);
        try {
            if (viseResult != null) {
                score = viseResult.pathScore();
            }
        } catch (ResultStateError e) {
            score = null;
        }
        viseResult.releaseAudio();
    }

    public RecognitionResult(Object recognizerSpecific, Status status, String inputMode, String result, List<List<String>> alternates, byte[] audio, com.voxware.browser.interpreter.Result event) {
        super(recognizerSpecific, status, inputMode, result == null ? "" : result, alternates, audio, event);
    }

    public String getText() {
        String text = super.getResult();
        if (text == null || text.isEmpty()) {
            return "<noinput>";
        }
        return text;
    }

    public Integer getScore() {
        return score;
    }
}
