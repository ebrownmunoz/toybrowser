package com.voxware.vise;

import android.util.Log;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Created by edh on 5/18/2015.
 */
public class AndroidAudioSource {
    private static final int LOG2_PLACES = 2;
    /* (( 1 << LOG2_PLACES ) - 1 ) */
    private static final int LOG2_PLACEMASK = 3;

    private static class SingletonHolder {
        private static final AndroidAudioSource INSTANCE = new AndroidAudioSource();
    }

    public static synchronized final AndroidAudioSource getInstance() {
        return SingletonHolder.INSTANCE;
    }

    static {
        System.loadLibrary("vise");
    }


    // this field will be overwritten in the native code
    private final ByteBuffer ptr = null;
    private TextView textView;

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    private AndroidAudioSource() {
        this.initialize();
    }

    protected void finalize() throws Throwable {
        this.destroy();
        super.finalize();
    }

    public void power() {
        power(null);
    }

    public void power(File audioFile) {
        int frameLen = 256;
        int frameShift = 141;
        int window = 4;
        int i, j, d;
        long e;
        int winPower, minPower, maxPower;

        // the * 2 is because we're storing 16 bit samples
        ByteBuffer buffer = ByteBuffer.allocateDirect(5 * 22050);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        int sampleCount = npower(buffer);
        ShortBuffer src = buffer.asShortBuffer();
        int offset = 10 * frameShift;
        src.position(offset);
        int countLeft = sampleCount - 10 * frameShift;
        int n = countLeft / frameShift;
        int[] amp = new int[n];
        n = 0;
        int minSample = Short.MAX_VALUE;
        int maxSample = Short.MIN_VALUE;
        e = 0;
        for (j = 0; j < countLeft; j++) {
            if (src.get(offset + j) > maxSample) maxSample = src.get(offset + j);
            if (src.get(offset + j) < minSample) minSample = src.get(offset + j);
            e += src.get(offset + j);
        }
        int avgSample = (int) (e / countLeft);

        while (countLeft >= frameLen) {
         /* Preemphasize and compute frame energy. We have src[-1] defined */
            for (j = 0, e = 0; j < frameLen; j++) {
                d = (int) src.get(offset + j) - (int) src.get(offset + j - 1);
                e += (long) d * (long) d;
            }
         /* Compute sample variance, convert it to DB scale */
            e /= frameLen;
            if (e >= Integer.MAX_VALUE) {
                e = Integer.MAX_VALUE - 1;
            }
            d = (int) e;
            d = (3 * log2b(1 + d)) >> LOG2_PLACES;
        /* Save logPower */
            amp[n++] = d;
            offset += frameShift;
            countLeft -= frameShift;
        }

        for (i = 0, maxPower = 0; i < n - window; i++) {
            for (j = i, winPower = 0; j < i + window; j++) {
                winPower += amp[j];
            }
            if (winPower > maxPower) {
                maxPower = winPower;
            }
        }
        maxPower /= window;
        if (textView != null) {
            textView.append(String.format("Max Power(DB): %d; minSample = %d; maxSample = %d; dcOffset = %d", maxPower, minSample, maxSample, avgSample));
        }
        Log.i("AndroidAudioSource", String.format("Max Power(DB): %d; minSample = %d; maxSample = %d; dcOffset = %d", maxPower, minSample, maxSample, avgSample));

        if (audioFile != null) {
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(audioFile);
                if (audioFile.getName().endsWith("wav")) {
                    fos.getChannel().write(encodeToWav(buffer));
                } else {
                    fos.getChannel().write(buffer);
                }
            } catch (IOException e1) {

            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (Exception e2) {

                    }
                }
            }
        }
    }

    private int log2b(int v) {
        int c = v, msb = 0;

  /* Hack acceptable for LOG2_PLACES = 2.
     Assures nonnegative shift right operand in the last statement */
        if (v <= LOG2_PLACEMASK)
            return ((v - 1) << 1);

  /* Find the most significant bit msb >= LOG2_PLACES */
        while ((c = c >> 1) > 0) {
            msb++;
        }

        return (msb << LOG2_PLACES) | ((v >> (msb - LOG2_PLACES)) & LOG2_PLACEMASK);
    }

    private ByteBuffer encodeToWav(ByteBuffer rawData) {
        ByteBuffer buffer = ByteBuffer.allocate(rawData.capacity() + 44).order(ByteOrder.LITTLE_ENDIAN);
        try {
            buffer.put("RIFF".getBytes("US-ASCII"));
            buffer.putInt(rawData.capacity() + 44);
            buffer.put("WAVE".getBytes("US-ASCII"));
            buffer.put("fmt ".getBytes("US-ASCII"));
            buffer.putInt(16); // previous size
            buffer.putShort((short) 1); // 1 = pcm
            buffer.putShort((short) 1); // channels
            buffer.putInt(11025); // sample rate
            buffer.putInt((11025 * 16 * 1) / 8); // sample rate * bits per sample * channels / 8
            buffer.putShort((short) (16 * 1 / 8)); // bits per sample * channels / 8
            buffer.putShort((short) 16); // bits per sample
            buffer.put("data".getBytes("US-ASCII"));
            buffer.putInt(rawData.capacity());
            buffer.put(rawData);
            buffer.rewind();
        } catch (UnsupportedEncodingException e) {
            // should not happen
            Log.wtf("", "US-ASCII not supported?");
        }
        return buffer;
    }

    private native void initialize();
    private native void destroy();
    public native int npower(ByteBuffer buffer);
}
