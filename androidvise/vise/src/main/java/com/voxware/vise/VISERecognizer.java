package com.voxware.vise;

import android.content.Intent;
import android.os.IBinder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.speech.Engine;
import javax.speech.EngineException;
import javax.speech.PropertyVetoException;
import javax.speech.VendorDataException;
import javax.speech.recognition.GrammarException;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiVise;
import voxware.engine.recognition.sapivise.SapiViseGrammar;
import voxware.engine.recognition.sapivise.SapiViseModeDesc;
import voxware.engine.recognition.sapivise.ViseProperties;

/**
 * Interface to the VISE recognizer
 * Created by edh on 9/30/2015.
 */
public class VISERecognizer {
    private static final Logger log = LoggerFactory.getLogger(VISERecognizer.class);


    private static final List<String> VISE_WCTHRESHOLD_NAMES = Arrays.asList("vise.wildcard.threshold");
    private static final List<String> VISE_ABSWILDCARD_NAMES = Arrays.asList("vise.wildcard.absolute");
    private static final List<String> VISE_RELWILDCARD_NAMES = Arrays.asList("vise.wildcard.relative");
    private static final List<String> VISE_THRESHOLD_MUTHA_NAMES = Arrays.asList("vise.threshold.mutha");
    private static final List<String> VISE_THRESHOLD_LOOSE_NAMES = Arrays.asList("vise.threshold.loose");
    private static final List<String> VISE_THRESHOLD_TIGHT_NAMES = Arrays.asList("vise.threshold.tight");
    private static final List<String> VISE_NUMHYPOTHESES_NAMES = Arrays.asList("vise.numhypotheses", "maxnbest");
    private static final List<String> VISE_BOUSILMSECS_NAMES = Arrays.asList("vise.audiodata.bousilmsecs");
    private static final List<String> VISE_EOUSILMSECS_NAMES = Arrays.asList("vise.audiodata.eousilmsecs");
    private static final List<String> TIMEOUT_NAMES = Arrays.asList("vise.audiodata.maxduration", "timeout");
    private static final List<String> SUPPRESS_SPEECH_INPUT = Arrays.asList("suppressspeechinput");
    private static final List<String> VISE_SAVERESULTS_NAMES = Arrays.asList("vise.saveresults");


    public static final int DEFAULT_NHYPOTHESES = 1;
    public static final int DEFAULT_SILMAXDWELL = 384;
    public static final int DEFAULT_ENDMINDWELL = 448;
    public static final float DEFAULT_INPUT_LEVEL = (float) 22.5;

    // Set this to true if VU reporting should be ON by default
    public static final boolean DEFAULT_VUONOFFSWCH = true;

    private static final AtomicBoolean nativeLoaded = new AtomicBoolean(false);
    private static final class ViseRecognizerHolder {
        private static final VISERecognizer INSTANCE = new VISERecognizer();
    }

    public static VISERecognizer getInstance() {
        return ViseRecognizerHolder.INSTANCE;
    }
    private Semaphore semaphore = new Semaphore(1);

    private final SapiViseModeDesc sapiViseDesc = new SapiViseModeDesc("voice");
    private final ScheduledExecutorService delayedExecutor = Executors.newSingleThreadScheduledExecutor();
    private SapiVise sapiVise;

    private String currentGrammarId;
    private byte[] currentGrammarBytes;
    private SapiViseGrammar currentGrammar;
    private byte[] currentSpeakerBytes;
    private String currentSpeaker;
    private int timeout;
    private boolean suppressed = false;

    private volatile RecognitionRequest currentRequest;

    private VISERecognizer() {
        initialize();
    }

    public void destroy() {
        if (sapiVise != null) {
            if (currentRequest != null && !currentRequest.isDone()) {
                currentRequest.cancel(true);
                currentRequest = null;
            }
            // we still leak something in native, so singleton
//            try {
//                semaphore.acquireUninterruptibly();
//                System.gc();
//                sapiVise.deallocate();
//                sapiVise.waitEngineState(Engine.DEALLOCATED);
//            } catch (EngineException e) {
//                log.error("Error shutting down VISE", e);
//            } catch (InterruptedException e) {
//
//            } finally {
//                semaphore.release();
//            }
//            sapiVise = null;
        }
        // this should only be done when vise is unloaded (which probably never happens since the classloader never goes away on Android)
        //NativeLogSource.INSTANCE.stopPumping();
    }

    public SapiViseGrammar getCurrentGrammar() {
        return currentGrammar;
    }

    /**
     * Loads a grammar file into the recognizer.
     * @param grammar
     */
    public void loadGrammar(String id, byte[] grammar, boolean forceUpdate) {
        if (semaphore.availablePermits() < 1) {
            log.warn("loadGrammar: Semaphore blocking");
        }

        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            return;
        }
        try {
            if (currentGrammarId == null || !currentGrammarId.equals(id) || forceUpdate) {
                log.info("Sending new grammar to SapiVise");
                currentGrammar = (SapiViseGrammar) sapiVise.readVendorGrammar(new ByteArrayInputStream(grammar));
                currentGrammarBytes = Arrays.copyOf(grammar, grammar.length);
                currentGrammarId = id;
            }
        } catch (VendorDataException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            // should not happen with a ByteArrayInputStream
        } finally {
            semaphore.release();
        }
    }

    /**
     * Loads a speaker(voi file) into the recognizer. The name identifies the voi file and is used like a key.
     * Multiple speakers can be sent to the recognizer. An existing speaker can be overwritten if the name is reused.
     *
     * @param name    the name of the speaker
     * @param grammar the file bytes
     * @return true if the speaker was successfully loaded, false otherwise
     */
    public boolean loadSpeaker(String name, byte[] grammar, boolean forceUpdate) {
        if (semaphore.availablePermits() < 1) {
            log.warn("loadSpeaker: Semaphore blocking");
        }

        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            return false;
        }
        try {
            if (currentSpeaker == null || !currentSpeaker.equals(name) || forceUpdate) {
                log.info("Sending new speaker profile to SapiVise");
                final boolean ret = sapiVise.readVendorSpeakerProfile(name, grammar);
                if (ret) {
                    currentSpeaker = name;
                    sapiVise.setCurrentSpeaker(new SpeakerProfile(currentSpeaker, currentSpeaker, ""));
                    currentSpeakerBytes = Arrays.copyOf(grammar, grammar.length);
                }
                return ret;
            } else {
                return true;
                //return sapiVise.setCurrentSpeaker(new SpeakerProfile(currentSpeaker, currentSpeaker, ""));
            }
        } finally {
            semaphore.release();
        }
    }

    public RecognitionRequest recognize() {
        if (semaphore.availablePermits() < 1) {
            log.warn("recognize: Semaphore blocking");
        }

        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            return null;
        }

        // we want to release the semaphore after the engine is truly done
        currentRequest = new RecognitionRequest(timeout, new RecognitionRequest.CompletionFunction() {
            @Override
            public void complete() {
                semaphore.release();
            }
        });
        final RecognitionRequest req = currentRequest;

        delayedExecutor.schedule(new Runnable() {
            @Override
            public void run() {
                if (!req.isDone()) {
                    req.resultRejected(null);
                }
            }
        }, timeout, TimeUnit.MILLISECONDS);

        return currentRequest;

    }

    /**
     * Recognizes speech based on the given rule. Assumes the timeout has already been set via <code>setParameter(String, Object)</code>.
     * @param rule the rule
     * @return a RecognitionRequest
     * @throws GrammarException
     */
    public RecognitionRequest recognize(String rule) throws GrammarException {
        if (suppressed) {
            if (rule.equals("CALIBRATION")) {
                log.warn("Tried calibration with speechinputsuppressed");
            } else {
                return recognize();
            }
        }

        if (semaphore.availablePermits() < 1) {
            log.warn("recognize2: Semaphore blocking");
        }

        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            return null;
        }

//        if (currentGrammar != sapiVise.getCurrentGrammar()) {
//            currentGrammar = (SapiViseGrammar) sapiVise.getCurrentGrammar();
//        }
        // we want to release the semaphore after the engine is truly done
        currentRequest = new RecognitionRequest(sapiVise, currentGrammar, rule, timeout, new RecognitionRequest.CompletionFunction() {
            @Override
            public void complete() {
                semaphore.release();
            }
        });
        ViseProperties properties = sapiVise.getRecognizerProperties();

        try {
            currentGrammar.setEnabled(rule, true);
        } catch (IllegalArgumentException e) {
            semaphore.release();
            throw e;
        }
        sapiVise.addResultListener(currentRequest);
        sapiVise.commitChanges();
        return currentRequest;
    }
    /**
     * Recognizes speech based on the given rule with the give timeout.
     * @param rule the rule
     * @param timeout timeout in millis
     * @return a RecognitionRequest
     * @throws PropertyVetoException if the timeout is invalid
     * @throws GrammarException
     */
    public RecognitionRequest recognize(String rule, int timeout) throws PropertyVetoException, GrammarException {
        if (suppressed) {
            return recognize();
        }

        if (semaphore.availablePermits() < 1) {
            log.warn("recognize3: Semaphore blocking");
        }

        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            return null;
        }

        // we want to release the semaphore after the engine is truly done
        currentRequest = new RecognitionRequest(sapiVise, currentGrammar, rule, timeout, new RecognitionRequest.CompletionFunction() {
            @Override
            public void complete() {
                semaphore.release();
            }
        });
        ViseProperties properties = sapiVise.getRecognizerProperties();
        properties.setMaxDuration(timeout);

        currentGrammar.setEnabled(rule, true);
        sapiVise.addResultListener(currentRequest);
        sapiVise.commitChanges();
        return currentRequest;
    }

    protected void initialize() {
        if (nativeLoaded.compareAndSet(false, true)) {
            log.info("Loading native library");
            System.loadLibrary("vise");
            log.info("Successfully loaded native library");
            NativeLogSource.INSTANCE.startPumping();
        }
        try {
            log.info("Creating SapiVise");
            sapiVise = (SapiVise) sapiViseDesc.createEngine();
            sapiVise.allocate();
            sapiVise.waitEngineState(javax.speech.Engine.ALLOCATED);
            sapiVise.requestFocus();
            sapiVise.waitEngineState(javax.speech.recognition.Recognizer.FOCUS_ON);
            log.info("SapiVise is ready");
        } catch (Exception e) {
            log.error("Failed to instantiate VISE", e);
            //throw new RuntimeException("Failed to instantiate recognizer", e);
        }
        log.info("Setting default properties");
        ViseProperties properties = (ViseProperties) sapiVise.getRecognizerProperties();
        try {
            properties.setNHypotheses(DEFAULT_NHYPOTHESES);
            properties.setSilMaxDwell(DEFAULT_SILMAXDWELL);
            properties.setEndMinDwell(DEFAULT_ENDMINDWELL);
            properties.setVUOnOffSwitch(DEFAULT_VUONOFFSWCH);
            log.info("Default properties are set");
            //setInputGain(DEFAULT_INPUT_LEVEL);
        } catch (PropertyVetoException e) {
            log.error("Failed to set VISE defaults", e);
            //throw new RuntimeException("Failed to set defaults", e);
        }
    }

    public ViseProperties getViseProperties() {
        return sapiVise.getRecognizerProperties();
    }

    public void setParameter(String s, Object o) {
        Object oldValue = null;
        try {
            if (VISE_WCTHRESHOLD_NAMES.contains(s)) {
                oldValue = getViseProperties().getWCThreshold();
                getViseProperties().setWCThreshold(coerceInt(o));
            } else if (VISE_ABSWILDCARD_NAMES.contains(s)) {
                oldValue = getViseProperties().getAbsWildcard();
                getViseProperties().setAbsWildcard(coerceInt(o));
            } else if (VISE_RELWILDCARD_NAMES.contains(s)) {
                oldValue = getViseProperties().getRelWildcard();
                getViseProperties().setRelWildcard(coerceInt(o));
            } else if (VISE_THRESHOLD_MUTHA_NAMES.contains(s)) {
                oldValue = getViseProperties().getMuthaThresh();
                getViseProperties().setMuthaThresh(coerceInt(o));
            } else if (VISE_THRESHOLD_LOOSE_NAMES.contains(s)) {
                oldValue = getViseProperties().getLooseThresh();
                getViseProperties().setLooseThresh(coerceInt(o));
            } else if (VISE_THRESHOLD_TIGHT_NAMES.contains(s)) {
                oldValue = getViseProperties().getTightThresh();
                getViseProperties().setTightThresh(coerceInt(o));
            } else if (VISE_NUMHYPOTHESES_NAMES.contains(s)) {
                oldValue = getViseProperties().getNHypotheses();
                getViseProperties().setNHypotheses(coerceInt(o));
            } else if (VISE_BOUSILMSECS_NAMES.contains(s)) {
                oldValue = getViseProperties().getBouSilMSecs();
                getViseProperties().setBouSilMSecs(coerceInt(o));
            } else if (VISE_EOUSILMSECS_NAMES.contains(s)) {
                oldValue = getViseProperties().getEouSilMSecs();
                getViseProperties().setEouSilMSecs(coerceInt(o));
            } else if (TIMEOUT_NAMES.contains(s)) {
                oldValue = getViseProperties().getMaxDuration();
                getViseProperties().setMaxDuration(coerceInt(o));
                //sapiVise.setTimeout(coerceInt(o));
                timeout = coerceInt(o);
            } else if (SUPPRESS_SPEECH_INPUT.contains(s)) {
                oldValue = suppressed;
                suppressed = coerceBoolean(o);
            } else if (VISE_SAVERESULTS_NAMES.contains(s)) {
                oldValue = getViseProperties().isTrainingProvided();
                getViseProperties().setTrainingProvided(coerceBoolean(o));
            } else {
                log.warn("Tried to set unsupported property " + s + " to " + o);
                return;
            }
            if (log.isDebugEnabled()) {
                log.debug("Changed " + s + " from " + oldValue + " to " + o);
            }
        } catch (PropertyVetoException e) {
            throw new IllegalArgumentException("Failed to set parameter " + s, e);
        }
    }

    public Object getParameter(String s) {
        if (VISE_WCTHRESHOLD_NAMES.contains(s)) {
            return getViseProperties().getWCThreshold();
        } else if (VISE_ABSWILDCARD_NAMES.contains(s)) {
            return getViseProperties().getAbsWildcard();
        } else if (VISE_RELWILDCARD_NAMES.contains(s)) {
            return getViseProperties().getRelWildcard();
        } else if (VISE_THRESHOLD_MUTHA_NAMES.contains(s)) {
            return getViseProperties().getMuthaThresh();
        } else if (VISE_THRESHOLD_LOOSE_NAMES.contains(s)) {
            return getViseProperties().getLooseThresh();
        } else if (VISE_THRESHOLD_TIGHT_NAMES.contains(s)) {
            return getViseProperties().getTightThresh();
        } else if (VISE_NUMHYPOTHESES_NAMES.contains(s)) {
            return getViseProperties().getNHypotheses();
        } else if (VISE_BOUSILMSECS_NAMES.contains(s)) {
            return getViseProperties().getBouSilMSecs();
        } else if (VISE_BOUSILMSECS_NAMES.contains(s)) {
            return getViseProperties().getEouSilMSecs();
        } else if (TIMEOUT_NAMES.contains(s)) {
            return timeout;
        } else if (SUPPRESS_SPEECH_INPUT.contains(s)) {
            return suppressed;
        } else if (VISE_SAVERESULTS_NAMES.contains(s)) {
            return getViseProperties().isTrainingProvided();
        } else {
            throw new IllegalArgumentException("Unknown parameter: " + s);
        }
    }

    public void interrupt(com.voxware.browser.io.RecognitionResult recognitionResult) {
        if (currentRequest != null) {
            currentRequest.interrupt(recognitionResult);
        } else {
            throw new IllegalStateException("No pending requests");
        }
    }

    public int getPower() {
        return sapiVise.getVU();
    }

    /**
     * Gets the underlying SapiVise object.
     * @return SapiVise
     */
    public SapiVise getSapiVise() {
        return sapiVise;
    }

    protected Boolean coerceBoolean(Object o) {
        if (o == null) {
            return false;
        } else if (o instanceof Boolean) {
            return (Boolean)o;
        } else if (o instanceof String) {
            String v = (String)o;
            return Boolean.valueOf(v);
        } else if (o instanceof Float || o instanceof Double) {
            return (o instanceof Float) ? (((Float)o).floatValue() != 0f) : (((Double)o).doubleValue() != 0d);
        } else if (o instanceof Number) {
            return ((Number)o).intValue() != 0;
        } else {
            throw new UnsupportedOperationException("Cannot convert " + o.getClass() + " to Boolean");
        }
    }
    protected Integer coerceInt(Object o) {
        if (o == null) {
            return 0;
        } else if (o instanceof String) {
            String v = (String)o;
            if (v.contains(".") || v.contains("E")) {
                return Double.valueOf(v).intValue();
            } else {
                return Integer.valueOf(v);
            }
        } else if (o instanceof Number) {
            return ((Number)o).intValue();
        } else if (o instanceof Boolean) {
            return ((Boolean)o) ? 1 : 0;
        } else {
            throw new UnsupportedOperationException("Cannot convert " + o.getClass() + " to Integer");
        }
    }

    protected Float coerceFloat(Object o) {
        if (o == null) {
            return 0.0f;
        } else if (o instanceof String) {
            return Float.valueOf((String) o);
        } else if (o instanceof Number) {
            return ((Number)o).floatValue();
        } else if (o instanceof Boolean) {
            return ((Boolean)o) ? 1.0f : 0.0f;
        } else {
            throw new UnsupportedOperationException("Cannot convert " + o.getClass() + " to Float");
        }
    }

}
