package com.voxware.vise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by edh on 3/16/2016.
 */
public class NativeLogSource {
    // singleton, because of JNI related stuff (static variable)
    public static final NativeLogSource INSTANCE = new NativeLogSource();
    private static final Logger log = LoggerFactory.getLogger(NativeLogSource.class);
    private Thread messagePumper;

    private NativeLogSource() {
    }

    public synchronized void startPumping() {
        if (messagePumper == null) {
            messagePumper = new Thread(new Runnable() {
                public void run() {
                    try {
                        while (!Thread.interrupted()) {
                            String message = pumpMessage();
                            log.info(message);
                        }
                    } catch (InterruptedException e) {

                    }
                }
            }, "NativeLogSource");
            messagePumper.start();
        }
    }

    public synchronized void stopPumping() {
        if (messagePumper != null) {
            messagePumper.interrupt();
            try {
                messagePumper.join();
                // don't null if we couldn't join, the other thread might still be alive
                messagePumper = null;
            } catch (InterruptedException e) {

            }
        }
    }

    private native String pumpMessage() throws InterruptedException;
}
