/*
 *
 *  * Copyright © 2015. Voxware, Inc. All Rights Reserved.
 *  * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 *  * title and interest to the Licensed Software, including all patents, copyrights,
 *  * trademarks, trade secrets, and other proprietary rights thereto.
 *  * All copies of the Licensed Software are subject to the terms and conditions of the
 *  * executed License Agreement on file with Voxware that has the right to use license
 *  * keys to control access to the Licensed Software.
 *
 *
 */

package com.voxware.vise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import javax.speech.recognition.GrammarException;
import javax.speech.recognition.ResultEvent;
import javax.speech.recognition.ResultListener;

import voxware.engine.recognition.sapivise.SapiVise;
import voxware.engine.recognition.sapivise.SapiViseGrammar;
import voxware.engine.recognition.sapivise.ViseResult;

/**
 * Represents a submitted request for recognition.
 * @author edh
 * @see java.util.concurrent.Future
 */
public class RecognitionRequest implements Future<com.voxware.browser.io.RecognitionResult>, ResultListener {

    public interface CompletionFunction {
        void complete();
    }
    private static final int STARTED = 0;
    private static final int COMPLETE = 1;
    private static final int CANCELLED = 2;

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private final SapiVise sapiVise;
    private final SapiViseGrammar grammar;
    private final String ruleName;
    private final CompletionFunction notifier;
    private final int timeout;

    private final CountDownLatch doneSignal = new CountDownLatch(1);
    //private ReentrantLock lock = new ReentrantLock();
    private volatile AtomicInteger state = new AtomicInteger(STARTED);

    private volatile com.voxware.browser.io.RecognitionResult result;

    public RecognitionRequest(int timeout, CompletionFunction notifier) {
        this.sapiVise = null;
        this.grammar = null;
        this.ruleName = null;
        this.notifier = notifier;
        this.timeout = timeout;
    }


    public RecognitionRequest(SapiVise sapiVise, SapiViseGrammar grammar, String ruleName, int timeout, CompletionFunction notifier) {
        this.sapiVise = sapiVise;
        this.grammar = grammar;
        this.ruleName = ruleName;
        this.notifier = notifier;
        this.timeout = timeout;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        if (!state.compareAndSet(STARTED, CANCELLED)) {
            // it was already completed
            return false;
        } else {
            if (mayInterruptIfRunning) {
                if (grammar != null) {
                    grammar.setEnabled(ruleName, false);
                }
                try {
                    if (sapiVise != null) {
                        sapiVise.commitChanges();
                    }
                } catch (GrammarException e) {
                    // we log this, but shouldn't happen when cancelling
                    log.warn("Caught exception cancelling recognition", e);
                }
                // this will wake anyone waiting in any of the get calls
                // we do this AFTER cancelling the recognition in case callers want to submit new recognition requests
                complete();
            }
            return true;
        }
    }

    @Override
    public boolean isCancelled() {
        return state.get() == CANCELLED;
    }

    @Override
    public boolean isDone() {
        return state.get() != STARTED;
    }

    @Override
    public com.voxware.browser.io.RecognitionResult get() throws InterruptedException, ExecutionException {
        doneSignal.await();
        switch (state.get()) {
            case COMPLETE :
                return result;
            case CANCELLED :
                throw new CancellationException("Recognition was cancelled by request");
            default:
                // this should not happen!!!!
                return null;
        }
    }

    @Override
    public com.voxware.browser.io.RecognitionResult get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        doneSignal.await(timeout, unit);
        switch (state.get()) {
            case COMPLETE :
                return result;
            case CANCELLED :
                throw new CancellationException("Recognition was cancelled by request");
            default:
                throw new TimeoutException("Timed out without a recognition result");
        }
    }

    @Override
    public void resultCreated(ResultEvent e) {
        // do nothing
    }

    @Override
    public void resultUpdated(ResultEvent e) {
        // do nothing
    }

    @Override
    public void grammarFinalized(ResultEvent e) {
        // do nothing
    }

    @Override
    public synchronized void resultAccepted(ResultEvent e) {
        if (state.compareAndSet(STARTED, COMPLETE)) {
            // normal recognition
            result = RecognitionResult.from((ViseResult) e.getSource());
        }
        teardown();
        // signal anyway, just in case
        complete();
    }

    @Override
    public synchronized void resultRejected(ResultEvent e) {
        // this is a noinput event btw
        if (state.compareAndSet(STARTED, COMPLETE)) {
            // normal recognition
            if (e != null && e.getSource() != null && e.getSource() instanceof ViseResult) {
                result = RecognitionResult.from((ViseResult) e.getSource());
            } else {
                // we only reach here if we timeout from a suppressspeechinput==true recognition
                result = RecognitionResult.from(null);
            }
        }
        teardown();
        // signal anyway, just in case
        complete();
    }

    @Override
    public void audioReleased(ResultEvent e) {
        // do nothing
    }

    @Override
    public void trainingInfoReleased(ResultEvent e) {
        // do nothing
    }

    protected void teardown() {
        if (grammar != null) {
            grammar.setEnabled(ruleName, false);
        }
        try {
            if (sapiVise != null) {
                sapiVise.commitChanges();
            }
        } catch (GrammarException e) {
            // we log this, but shouldn't happen when cancelling
            log.warn("Caught exception cleaning up recognition", e);
        } finally {
            if (sapiVise != null) {
                sapiVise.removeResultListener(this);
            }
        }
    }

    protected void complete() {
        doneSignal.countDown();
        notifier.complete();
    }

    public synchronized void interrupt(com.voxware.browser.io.RecognitionResult recognitionResult) {
        if (state.compareAndSet(STARTED, COMPLETE)) {
            result = recognitionResult;
            teardown();
            complete();
        }
    }
}
