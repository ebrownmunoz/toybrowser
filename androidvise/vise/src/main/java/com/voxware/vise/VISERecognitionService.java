package com.voxware.vise;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.speech.EngineException;
import javax.speech.PropertyVetoException;
import javax.speech.VendorDataException;
import javax.speech.recognition.GrammarException;
import javax.speech.recognition.Result;
import javax.speech.recognition.ResultEvent;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.BaseResult;
import voxware.engine.recognition.sapivise.SapiVise;
import voxware.engine.recognition.sapivise.SapiViseGrammar;
import voxware.engine.recognition.sapivise.SapiViseModeDesc;
import voxware.engine.recognition.sapivise.ViseProperties;
import voxware.engine.recognition.sapivise.ViseResult;

/**
 * Created by edh on 5/18/2015.
 */
public class VISERecognitionService extends Service {

    private static final Logger log = LoggerFactory.getLogger(VISERecognitionService.class);

    private enum ViseState {
        UNKNOWN,
        CREATED,
        STARTING,
        STARTED,
        STOPPING,
        STOPPED,
        DESTROYED
    };

    private AtomicReference<ViseState> state = new AtomicReference<>(ViseState.UNKNOWN);
    private IBinder binder = new Binder();
    private VISERecognizer recognizer;
    public class Binder extends android.os.Binder {
        public VISERecognizer getService() {
            return recognizer;
        }
    }

    public VISERecognitionService() {
        super();
    }

    @Override
    public IBinder onBind(Intent intent) {
        log.info("Service was bound");
        return binder;
    }

    @Override
    public void onCreate() {
        log.info("Creating VISERecognitionService");
        try {
            System.loadLibrary("vise");
        } catch (Throwable t) {
            log.error("Failed to load VISE", t);
            throw t;
        }

        state.compareAndSet(ViseState.UNKNOWN, ViseState.CREATED);
        log.info("Service created");
        //Toast.makeText(this, "Service created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        state.set(ViseState.DESTROYED);
        recognizer.destroy();
        recognizer = null;
        super.onDestroy();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            stopSelf();
            log.error("Not expecting intent to be null");
            //throw new IllegalArgumentException("Not expecting intent to be null");
            return START_NOT_STICKY;
        }

        if (!state.compareAndSet(ViseState.CREATED, ViseState.STARTING)) {
            stopSelf();
            log.error("Expected state to be CREATED, but was " + state.get());
            //throw new IllegalStateException("Expected state to be CREATED, but was " + state.get());
            return START_NOT_STICKY;
        }

        try {
            recognizer = VISERecognizer.getInstance();
        } catch (Exception e) {
            log.error("Failed to instantiate VISE", e);
            //throw new RuntimeException("Failed to instantiate recognizer", e);
            return START_NOT_STICKY;
        }
        state.compareAndSet(ViseState.STARTING, ViseState.STARTED);
        return START_NOT_STICKY;
    }
}
