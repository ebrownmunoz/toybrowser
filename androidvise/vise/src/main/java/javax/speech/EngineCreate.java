/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech;


/**
 * The <code>EngineCreate</code> interface is implemented by
 * <code>EngineModeDesc</code> objects obtained through calls to
 * the <code>EngineCentral</code> objects of each 
 * speech engine registered with the <code>Central</code> class.
 * <P>
 *
 * <B>Note:</B> most applications do not need to use this interface.
 * <P>
 *
 * Each engine implementation must sub-class either
 * <code>RecognizerModeDesc</code> or <code>SynthesizerModeDesc</code>
 * to implement the <code>EngineCreate</code> interface.
 * For example:
 *
 * <pre>
 *   public MyRecognizerModeDesc extends RecognizerModeDesc implements EngineCreate
 *   ...
 *     public Engine createEngine() {
 *       // Use mode desc propoerties to create an appropriate engine
 *     }
 * </pre>
 *
 * This implementation mechanism allows the engine to embed additional
 * mode information (engine-specific mode identifiers, GUIDs etc)
 * that simplify creation of the engine if requested by the <code>Central</code> class.
 * The engine-specific mode descriptor may need to override equals and 
 * other methods if engine-specific features are defined.
 * <P>
 *
 * The engine must perform the same security checks on access to 
 * speech engines as the <code>Central</code> class. 
 * <P>
 *
 * @see Central
 * @see EngineCentral
 * @see EngineModeDesc
 * @see javax.speech.recognition.RecognizerModeDesc
 * @see javax.speech.synthesis.SynthesizerModeDesc
 */


public interface EngineCreate
{
  /**
   * Create an engine with the properties specified by this object.
   * A new engine should be created in the <code>DEALLOCATED</code> state.
   * <P>
   *
   * @see Engine#DEALLOCATED
   * @exception IllegalArgumentException 
   *   The properties of the <code>EngineModeDesc</code> do not refer to a known 
   *   engine or engine mode.
   * @exception EngineException 
   *   The engine defined by this <code>EngineModeDesc</code> could not be properly created. 
   * @exception SecurityException 
   *   if the caller does not have <code>createRecognizer</code> permission but
   *   is attempting to create a <code>Recognizer</code>
   */

  public Engine createEngine()
    throws IllegalArgumentException, EngineException, SecurityException;
}

