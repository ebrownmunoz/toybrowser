/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import javax.speech.synthesis.*;
import javax.speech.recognition.*;


/**
 * The <code>Engine</code> interface is the parent interface for all 
 * speech engines including <code>Recognizer</code> and 
 * <code>Synthesizer</code>.  A speech engine is a generic entity
 * that either processes speech input or produces speech output.
 * Engines - recognizers and synthesizers - derive the following
 * functionality from the <code>Engine</code> interface:
 * <P>
 *
 * <UL>
 *   <LI><code>allocate</code> and <code>deallocate</code> methods.
 *   <LI><code>pause</code> and <code>resume</code> methods.
 *   <LI>Access to a <code>AudioManager</code> and <code>VocabManager</code>.
 *   <LI>Access to <code>EngineProperties</code>.
 *   <LI>Access to the engine's <code>EngineModeDesc</code>.
 *   <LI>Methods to add and remove <code>EngineListener</code> objects.
 * </UL>
 * <P>
 *
 * Engines are located, selected and created through methods of the 
 * <code>Central</code> class.
 * <P>
 *
 * <B>Engine State System: Allocation</B>
 * <P>
 *
 * Each type of speech engine has a well-defined set of states
 * of operation, and well-defined behavior for moving between
 * states.  These states are defined by constants of the
 * <code>Engine</code>, <code>Recognizer</code> and 
 * <code>Synthesizer</code> interfaces.
 * <P>
 *
 * The <code>Engine</code> interface defines three
 * methods for viewing and monitoring states: <code>getEngineState</code>,
 * <code>waitEngineState</code> and <code>testEngineState</code>.
 * An <code>EngineEvent</code> is issued to <code>EngineListeners</code>
 * each time an <code>Engine</code> changes state.
 * <P>
 * 
 * The basic states of any speech engine (<code>Recognizer</code>
 * or <code>Synthesizer</code>) are <code>DEALLOCATED</code>,
 * <code>ALLOCATED</code>, <code>ALLOCATING_RESOURCES</code> and
 * <code>DEALLOCATING_RESOURCES</code>.  An engine in the <code>ALLOCATED</code> 
 * state has acquired all the resources it requires to perform
 * its core functions.  
 * <P>
 *
 * Engines are created in the <code>DEALLOCATED</code> state
 * and a call to <code>allocate</code> is required to prepare
 * them for usage.  The <code>ALLOCATING_RESOURCES</code> state is an
 * intermediate state between <code>DEALLOCATED</code> and
 * <code>ALLOCATED</code> which an engine occupies during 
 * the resource allocation process (which may be a very short
 * period or takes 10s of seconds).
 * <P>
 *
 * Once an application finishes using a speech engine it should
 * always explicitly free system resources by calling the 
 * <code>deallocate</code> method.  This call transitions the
 * engine to the <code>DEALLOCATED</code> state via some period
 * in the <code>DEALLOCATING_RESOURCES</code> state.
 * <P>
 * 
 * The methods of <code>Engine</code>, <code>Recognizer</code> and
 * <code>Synthesizer</code> perform differently according to the
 * engine's allocation state.  Many methods cannot be performed
 * when an engine is in either the <code>DEALLOCATED</code> or 
 * <code>DEALLOCATING_RESOURCES</code> state.  Many methods block (wait)
 * for an engine in the <code>ALLOCATING_RESOURCES</code> state until 
 * the engine reaches the <code>ALLOCATED</code> state.  This
 * blocking/exception behavior is defined separately for each
 * method of <code>Engine</code>, <code>Synthesizer</code> and
 * <code>Recognizer</code>.
 * <P>
 *
 * <B>Engine State System: Sub-states of <code>ALLOCATED</code></B>
 * <P>
 *
 * The <code>ALLOCATED</code> states has sub-states.
 * (The <code>DEALLOCATED</code>, <code>ALLOCATING_RESOURCES</code> and
 * <code>DEALLOCATING_RESOURCES</code> states do not have any sub-states.)
 * <P>
 *
 * <UL>
 *  <LI>
 *    Any <code>ALLOCATED</code> engine (<code>Recognizer</code> or
 *    <code>Synthesizer</code>) is either <code>PAUSED</code>
 *    or <code>RESUMED</code>.  These state indicates whether
 *    audio input/output is stopped or running.
 *  <LI>
 *    An <code>ALLOCATED</code> <code>Synthesizer</code> has
 *    additional sub-states for <code>QUEUE_EMPTY</code> and 
 *    <code>QUEUE_NOT_EMPTY</code> that indicate the status of
 *    its speech output queue.  These two states are independent
 *    of the <code>PAUSED</code> and <code>RESUMED</code> states.
 *  <LI>
 *    An <code>ALLOCATED</code> <code>Recognizer</code> has 
 *    additional sub-states for <code>LISTENING</code>, 
 *    <code>PROCESSING</code> and <code>SUSPENDED</code>
 *    that indicate the status of the recognition process.
 *    These three states are independent of the <code>PAUSED</code>
 *    and <code>RESUMED</code> states (with the exception of
 *    minor interactions documented with <code>Recognizer</code>).
 *  <LI>
 *    An <code>ALLOCATED</code> <code>Recognizer</code> also has 
 *    additional sub-states for <code>FOCUS_ON</code> and 
 *    <code>FOCUS_OFF</code>.  Focus determines when most of an
 *    application's grammars are active or deactive for recognition.
 *    The focus states are independent of the <code>PAUSED</code>
 *    and <code>RESUMED</code> states and of the 
 *    <code>LISTENING/PROCESSING/SUSPENDED</code> states.
 *    (Limited exceptions are discussed in the documentation for
 *    <code>Recognizer</code>).
 * </UL>
 * <P>
 *
 * The <code>pause</code> and <code>resume</code> methods are used 
 * to transition an engine between the <code>PAUSED</code> and 
 * <code>RESUMED</code> states.  The <code>PAUSED</code> and 
 * <code>RESUMED</code> states are shared by all applications that
 * use the underlying engine.  For instance, pausing a recognizer
 * pauses all applications that use that engine.
 * <P>
 *
 * <A NAME="values"><B>Engine State System: get/test/wait</B></A>
 * <P>
 *
 * The current state of an <code>Engine</code> is returned
 * by the <code>getEngineState</code> method.  The 
 * <code>waitEngineState</code> method blocks the calling
 * thread until the <code>Engine</code> reaches a specified
 * state.  The <code>testEngineState</code> tests whether
 * an <code>Engine</code> is in a specified state.
 *
 * <P>
 * The state values can be bitwise OR'ed (using the Java "|" operator).
 * For example, for an allocated, resumed synthesizer with
 * items in its speech output queue, the state is
 *
 * <PRE>    Engine.ALLOCATED | Engine.RESUMED | Synthesizer.QUEUE_NOT_EMPTY </PRE>
 * <P>
 *
 * The states and sub-states defined above put constraints upon
 * the state of an engine.  The following are examples of illegal
 * states:
 *
 * <DL>
 *   <DT>Illegal <code>Engine</code> states:
 *     <DD><code>Engine.DEALLOCATED | Engine.RESUMED</code>
 *     <DD><code>Engine.ALLOCATED | Engine.DEALLOCATED</code>
 *   <DT>Illegal <code>Synthesizer</code> states:
 *     <DD><code>Engine.DEALLOCATED | Engine.QUEUE_NOT_EMPTY</code>
 *     <DD><code>Engine.QUEUE_EMPTY | Engine.QUEUE_NOT_EMPTY</code>
 *   <DT>Illegal <code>Recognizer</code> states:
 *     <DD><code>Engine.DEALLOCATED | Engine.PROCESSING</code>
 *     <DD><code>Engine.PROCESSING | Engine.SUSPENDED</code>
 * </DL>
 *
 * Calls to the <code>testEngineState</code> and <code>waitEngineState</code>
 * methods with illegal state values cause an exception to be
 * thrown.
 * <P>
 *
 * @see Central
 * @see Synthesizer
 * @see Recognizer
 */


public interface Engine
{
  /**
   * Bit of state that is set when an <code>Engine</code> is
   * in the deallocated state.  A deallocated engine does
   * not have the resources necessary for it to carry out
   * its basic functions.
   * <P>
   * 
   * In the <code>DEALLOCATED</code> state, many of the methods 
   * of an <code>Engine</code> throw an exception when called.  
   * The <code>DEALLOCATED</code> state has no sub-states.
   * <P>
   *
   * An <code>Engine</code> is always created in the 
   * <code>DEALLOCATED</code> state.  A <code>DEALLOCATED</code>
   * can transition to the <code>ALLOCATED</code> state
   * via the <code>ALLOCATING_RESOURCES</code> state following a
   * call to the <code>allocate</code> method.  An <code>Engine</code>
   * returns to the <code>DEALLOCATED</code> state via the 
   * <code>DEALLOCATING_RESOURCES</code> state with a call to the
   * <code>deallocate</code> method.
   * <P>
   *
   * @see #allocate
   * @see #deallocate
   * @see #getEngineState
   * @see #waitEngineState
   */

  public long DEALLOCATED = 1L << 0;


  /**
   * Bit of state that is set when an <code>Engine</code> is
   * being allocated - the transition state between <code>DEALLOCATED</code>
   * to <code>ALLOCATED</code> following a call to the <code>allocate</code>
   * method.  The <code>ALLOCATING_RESOURCES</code> state has no sub-states.  
   * In the <code>ALLOCATING_RESOURCES</code> state, many of the methods
   * of <code>Engine</code>, <code>Recognizer</code>, and
   * <code>Synthesizer</code> will block until the <code>Engine</code>
   * reaches the <code>ALLOCATED</code> state and the action can
   * be performed.
   * <P>
   *
   * @see #getEngineState
   * @see #waitEngineState
   */

  public long ALLOCATING_RESOURCES = 1L << 1;


  /**
   * Bit of state that is set when an <code>Engine</code> is
   * in the allocated state.  An engine in the <code>ALLOCATED</code>
   * state has acquired the resources required for it to
   * carry out its core functions.
   * <P>
   *
   * The <code>ALLOCATED</code> states has sub-states for
   * <code>RESUMED</code> and <code>PAUSED</code>.  Both
   * <code>Synthesizer</code> and <code>Recognizer</code> 
   * define additional sub-states of <code>ALLOCATED</code>.
   * <P>
   * 
   * An <code>Engine</code> is always created in the 
   * <code>DEALLOCATED</code> state.  It reaches the 
   * <code>ALLOCATED</code> state via the <code>ALLOCATING_RESOURCES</code>
   * state with a call to the <code>allocate</code> method.
   * <P>
   *
   *
   * @see Synthesizer
   * @see Recognizer
   * @see #getEngineState
   * @see #waitEngineState
   */

  public long ALLOCATED = 1L << 2;


  /**
   * Bit of state that is set when an <code>Engine</code> is
   * being deallocated - the transition state between <code>ALLOCATED</code>
   * to <code>DEALLOCATED</code>.  The <code>DEALLOCATING_RESOURCES</code>
   * state has no sub-states.  In the <code>DEALLOCATING_RESOURCES</code>
   * state, most methods of <code>Engine</code>, <code>Recognizer</code>
   * and <code>Synthesizer</code> throw an exception.
   * <P>
   *
   * @see #getEngineState
   * @see #waitEngineState
   */

  public long DEALLOCATING_RESOURCES = 1L << 3;


  /**
   * Bit of state that is set when an <code>Engine</code> is
   * is in the <code>ALLOCATED</code> state and is <code>PAUSED</code>.
   * In the <code>PAUSED</code> state, audio input or output
   * stopped.
   * <P>
   *
   * An <code>ALLOCATED</code> engine is always in either in 
   * the <code>PAUSED</code> or <code>RESUMED</code>.  The 
   * <code>PAUSED</code> and <code>RESUMED</code> states are
   * sub-states of the <code>ALLOCATED</code> state.
   * <P>
   *
   * @see #RESUMED
   * @see #ALLOCATED
   * @see #getEngineState
   * @see #waitEngineState
   */

  public long PAUSED = 1L << 8;


  /**
   * Bit of state that is set when an <code>Engine</code> is
   * is in the <code>ALLOCATED</code> state and is <code>RESUMED</code>.
   * In the <code>RESUMED</code> state, audio input or output active.
   * <P>
   *
   * An <code>ALLOCATED</code> engine is always in either in 
   * the <code>PAUSED</code> or <code>RESUMED</code>.  The 
   * <code>PAUSED</code> and <code>RESUMED</code> states are
   * sub-states of the <code>ALLOCATED</code> state.
   * <P>
   *
   * @see #RESUMED
   * @see #ALLOCATED
   * @see #getEngineState
   * @see #waitEngineState
   */

  public long RESUMED = 1L << 9;


  /** 
   * Returns a or'ed set of flags indicating the current state of
   * an <code>Engine</code>.  The format of the returned 
   * <code>state</code> value is <A HREF="#values">described above</A>.
   * <P>
   *
   * An <code>EngineEvent</code> is issued each time the
   * <code>Engine</code> changes state.
   * <P>
   *
   * The <code>getEngineState</code> method can be called successfully
   * in any <code>Engine</code> state.
   * <P>
   *
   * @see #getEngineState
   * @see #waitEngineState
   * @see EngineEvent#getNewEngineState
   * @see EngineEvent#getOldEngineState
   */

  public long getEngineState();


  /** 
   * Blocks the calling thread until the <code>Engine</code>
   * is in a specified state.  The format of the <code>state</code>
   * value is <A HREF="#values">described above</A>.
   * <P>
   *
   * All state bits specified in the <code>state</code> parameter
   * must be set in order for the method to return, as defined
   * for the <code>testEngineState</code> method.  If the <code>state</code>
   * parameter defines an unreachable state (e.g. <code>PAUSED | RESUMED</code>)
   * an exception is thrown.
   * <P>
   *
   * The <code>waitEngineState</code> method can be called successfully
   * in any <code>Engine</code> state.
   * <P>
   *
   * @see #testEngineState
   * @see #getEngineState
   * @exception InterruptedException
   *   if another thread has interrupted this thread.
   * @exception IllegalArgumentException
   *   if the specified state is unreachable
   */

  public void waitEngineState(long state)
    throws InterruptedException, IllegalArgumentException;


  /** 
   * Returns true if the current engine state matches the
   * specified state.  The format of the <code>state</code>
   * value is <A HREF="#values">described above</A>.
   * <P>
   *
   * The test performed is not an exact match to the current
   * state.  Only the specified states are tested.  For
   * example the following returns true only if the
   * <code>Synthesizer</code> queue is empty, irrespective
   * of the pause/resume and allocation states.
   * 
   * <PRE>
   *    if (synth.testEngineState(Synthesizer.QUEUE_EMPTY)) ... </PRE>
   *
   * The <code>testEngineState</code> method is equivalent to:
   * <P>
   * 
   * <PRE>
   *      if ((engine.getEngineState() & state) == state) </PRE>
   *
   * The <code>testEngineState</code> method can be called 
   * successfully in any <code>Engine</code> state.
   * <P>
   *
   * @exception IllegalArgumentException
   *   if the specified state is unreachable
   */

  public boolean testEngineState(long state)
    throws IllegalArgumentException;


  /** 
   * Allocate the resources required for the <code>Engine</code> and put it
   * into the <code>ALLOCATED</code> state. 
   * When this method returns successfully the <code>ALLOCATED</code>
   * bit of engine state is set, and the
   * <code>testEngineState(Engine.ALLOCATED)</code> method returns <code>true</code>.
   * During the processing of the method, the <code>Engine</code> is
   * temporarily in the <code>ALLOCATING_RESOURCES</code> state.
   * <P>
   *
   * When the <code>Engine</code> reaches the <code>ALLOCATED</code> state 
   * other engine states are determined:
   * <P>
   *
   * <UL>
   *   <LI><code>PAUSED</code> or <code>RESUMED</code>: the pause state
   *       depends upon the existing state of the engine.  In a multi-app
   *       environment, the pause/resume state of the engine is shared
   *       by all apps.
   *   <LI>A <code>Recognizer</code> always starts in the 
   *       <code>LISTENING</code> state when newly allocated but may
   *       transition immediately to another state.
   *   <LI>A <code>Recognizer</code> may be allocated in either the 
   *       <code>HAS_FOCUS</code> state or <code>LOST_FOCUS</code> state
   *       depending upon the activity of other applications.
   *   <LI>A <code>Synthesizer</code> always starts in the 
   *       <code>QUEUE_EMPTY</code> state when newly allocated.
   * </UL>
   * <P>
   *
   * While this method is being processed events are issued to 
   * any <code>EngineListeners</code> attached to the <code>Engine</code>
   * to indicate state changes.  First, as the <code>Engine</code> 
   * changes from the <code>DEALLOCATED</code> to the 
   * <code>ALLOCATING_RESOURCES</code> state, an 
   * <code>ENGINE_ALLOCATING_RESOURCES</code> event is issued.
   * As the allocation process completes, the engine moves from
   * the <code>ALLOCATING_RESOURCES</code> state to the
   * <code>ALLOCATED</code> state and an <code>ENGINE_ALLOCATED</code>
   * event is issued.
   * <P>
   *
   * The <code>allocate</code> method should be called for an
   * <code>Engine</code> in the <code>DEALLOCATED</code> state.
   * The method has no effect for an <code>Engine</code> is either
   * the <code>ALLOCATING_RESOURCES</code> or <code>ALLOCATED</code> states.
   * The method throws an exception in the <code>DEALLOCATING_RESOURCES</code>
   * state. 
   * <P>
   *
   * If any problems are encountered during the allocation process
   * so that the engine cannot be allocated, the engine returns
   * to the <code>DEALLOCATED</code> state (with an
   * <code>ENGINE_DEALLOCATED</code> event), and an <code>EngineException</code>
   * is thrown.
   * <P>
   *
   * <A NAME="inner"></A>
   * Allocating the resources for an engine may be fast (less than
   * a second) or slow (several 10s of seconds) depending upon a range
   * of factors.  Since the <code>allocate</code> method does not 
   * return until allocation is completed applications may want to perform 
   * allocation in a background thread and proceed with other activities.  
   * The following code uses an inner class implementation of <code>Runnable</code>
   * to create a background thread for engine allocation:
   *
   * <PRE>
   *     static Engine engine;
   *   
   *     public static void main(String argv[])
   *     {
   *       engine = Central.createRecognizer();
   *   
   *       new Thread(new Runnable() {
   *           public void run() {
   *             engine.allocate();
   *           }
   *         }).start();
   *   
   *       // Do other stuff while allocation takes place
   *       ...
   *   
   *       // Now wait until allocation is complete
   *       engine.waitEngineState(Engine.ALLOCATED);
   *      }</PRE>
   * <P>
   *
   * @see #getEngineState
   * @see #deallocate
   * @see #ALLOCATED
   * @see EngineEvent#ENGINE_ALLOCATED
   * @exception EngineException 
   *    if an allocation error occurred or the engine is not operational.
   * @exception EngineStateError
   *    if called for an engine in the <code>DEALLOCATING_RESOURCES</code> state
   */

  public void allocate()
    throws EngineException, EngineStateError;


  /** 
   * Free the resources of the engine that were acquired during
   * allocation and during operation and return the engine
   * to the <code>DEALLOCATED</code>.  When this method returns
   * the <code>DEALLOCATED</code> bit of engine state is set so the
   * <code>testEngineState(Engine.DEALLOCATED)</code> method returns 
   * <code>true</code>.  During the processing of the method, the 
   * <code>Engine</code> is temporarily in the <code>DEALLOCATING_RESOURCES</code> state.
   * <P>
   *
   * A deallocated engine can be re-started with a subsequent
   * call to <code>allocate</code>.
   * <P>
   *
   * Engines need to clean up current activities before being deallocated.
   * A <code>Synthesizer</code> must be in the <code>QUEUE_EMPTY</code>
   * state before being deallocated.  If the queue is not empty,
   * any objects on the speech output queue must be cancelled 
   * with appropriate events issued.  
   * A <code>Recognizer</code> cannot be in the <code>PROCESSING</code>
   * state when being deallocated.  If necessary, there must be a
   * <code>forceFinalize</code> of any unfinalized result.
   * <P>
   *
   * While this method is being processed events are issued to any
   * <code>EngineListeners</code> attached to the <code>Engine</code>
   * to indicate state changes. First, as the <code>Engine</code>
   * changes from the <code>ALLOCATED</code> to the
   * <code>DEALLOCATING_RESOURCES</code> state, an
   * <code>ENGINE_DEALLOCATING_RESOURCES</code> event is issued. 
   * As the deallocation process completes, the engine moves from 
   * the <code>DEALLOCATING_RESOURCES</code> state to the
   * <code>DEALLOCATED</code> state and an <code>ENGINE_DEALLOCATED</code>
   * event is issued. 
   * <P>
   *
   * The <code>deallocate</code> method should only be called for an
   * <code>Engine</code> in the <code>ALLOCATED</code> state.
   * The method has no effect for an <code>Engine</code> is either
   * the <code>DEALLOCATING_RESOURCES</code> or <code>DEALLOCATED</code> states.
   * The method throws an exception in the <code>ALLOCATING_RESOURCES</code>
   * state.
   * <P>
   *
   * Deallocating resources for an engine is not always immediate.
   * Since the deallocate method does not return until complete, 
   * applications may want to perform deallocation in a separate thread. 
   * The documentation for the <code>allocate</code> method shows an
   * example of an <A HREF="#inner">inner class implementation of 
   * <code>Runnable</code></A> that creates a separate thread.
   * <P>
   *
   * @see #allocate
   * @see EngineEvent#ENGINE_DEALLOCATED
   * @see Synthesizer#QUEUE_EMPTY
   * @exception EngineException 
   *    if a deallocation error occurs
   * @exception EngineStateError
   *    if called for an engine in the <code>ALLOCATING_RESOURCES</code> state
   */

  public void deallocate()
    throws EngineException, EngineStateError;


  /**
   * Pause the audio stream for the engine and put the <code>Engine</code>
   * into the <code>PAUSED</code> state.  Pausing an engine
   * pauses the underlying engine for <em>all applications</em> that 
   * are connected to that engine.  Engines are typically paused and resumed
   * by request from a user.
   * <P>
   *
   * Applications may pause an engine indefinately.  When an engine
   * moves from the <code>RESUMED</code> state to the <code>PAUSED</code> 
   * state, an <code>ENGINE_PAUSED</code> event is issued to each
   * <code>EngineListener</code> attached to the <code>Engine</code>.  
   * The <code>PAUSED</code> bit of the engine state is set to
   * <code>true</code> when paused, and can be tested by the
   * <code>getEngineState</code> method and other engine state
   * methods.
   * <P>
   *
   * The <code>PAUSED</code> state is a sub-state of the 
   * <code>ALLOCATED</code> state.  An <code>ALLOCATED</code> 
   * <code>Engine</code> is always in either the <code>PAUSED</code> 
   * or the <code>RESUMED</code> state.
   * <P>
   *
   * It is not an exception to pause an <code>Engine</code>
   * that is already paused.
   * <P>
   * 
   * The <code>pause</code> method operates as defined for engines in the
   * <code>ALLOCATED</code> state.  When pause is called for an
   * engine in the <code>ALLOCATING_RESOURCES</code> state, the method
   * blocks (waits) until the <code>ALLOCATED</code> state is
   * reached and then operates normally.  An error is thrown
   * when pause is called for an engine is either the 
   * <code>DEALLOCATED</code> is <code>DEALLOCATING_RESOURCES</code> states.
   * state.
   * <P>
   *
   * The <code>pause</code> method does not always return immediately.
   * Some applications need to execute pause in a separate
   * thread.  The documentation for the <code>allocate</code>
   * method includes an <A HREF="#inner">example implementation 
   * of <code>Runnable</code> with inner classes</A> that can perform
   * <code>pause</code> in a separate thread.
   * <P>
   *
   * <b>Pausing a <A HREF="synthesis/Synthesizer.html"><code>Synthesizer</code></A></b>
   * <P>
   *
   * The pause/resume mechanism for a synthesizer is analogous
   * to pause/resume on a tape player or CD player.
   * The audio output stream is paused.  The speaking
   * queue is left intact and a subsequent resume continues output from
   * the point at which the pause took effect.
   * <P>
   * 
   * <b>Pausing a <A HREF="recognition/Recognizer.html"><code>Recognizer</code></A></b>
   * <P>
   *
   * Pause and resume for a recognizer are analogous to turning
   * a microphone off and on.
   * Pausing stops the input audio input stream as close as
   * possible to the time of the call to pause.  The incoming
   * audio between the pause and the resume calls is <em>ignored</em>.
   * <P>
   *
   * Anything a user says while the recognizer is paused will not be
   * heard by the recognizer.
   * Pausing a recognizer during the middle of user speech forces
   * the recognizer to finalize or reject processing of that 
   * incoming speech - a recognition result cannot cross a pause/resume
   * boundary.
   * <P>
   *
   * Most recognizers have some amount of internal audio buffering.
   * This means that some recognizer processing may continue after the
   * pause.  For example, results can be created and finalized.
   * <P>
   *
   * Note: recognizers add a special <code>suspend</code> method
   * that allows applications to temporarily stop the recognizer to 
   * modify grammars and grammar activation.  Unlike a paused 
   * recognizer, a suspended recognizer buffers incoming audio input
   * to be processed once it returns to a listening state, so
   * no audio is lost.
   * <P>
   *
   * @see #resume
   * @see #getEngineState
   * @see EngineEvent#ENGINE_PAUSED
   * @see Recognizer#suspend
   * @exception EngineStateError
   *    if called for an engine in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void pause()
    throws EngineStateError;



  /**
   * Put the <code>Engine</code> in the <code>RESUMED</code> state to
   * resume audio streaming to or from a paused engine.
   * Resuming an engine resuming the underlying engine for 
   * <em>all applications</em> that are connected to that engine.  
   * Engines are typically paused and resumed by request from a user. 
   * <P>
   *
   * The specific pause/resume behavior of recognizers and synthesizers
   * is defined in the documentation for the 
   * <A HREF="#pause()"><code>pause</code></A> method.
   * <P>
   *
   * When an engine moves from the <code>PAUSED</code> state
   * to the <code>RESUMED</code> state, an <code>ENGINE_RESUMED</code>
   * event is issued to each <code>EngineListener</code> attached to
   * the <code>Engine</code>.  The <code>RESUMED</code> bit of the 
   * engine state is set to <code>true</code> when resumed, and
   * can be tested by the <code>getEngineState</code> method and other
   * engine state methods. 
   * <P>
   *
   * The <code>RESUMED</code> state is a sub-state of the 
   * <code>ALLOCATED</code> state. An <code>ALLOCATED</code> 
   * <code>Engine</code> is always in either the <code>PAUSED</code>
   * or the <code>RESUMED</code> state. 
   * <P>
   *
   * It is not an exception to resume a <code>engine</code>
   * that is already in the <code>RESUMED</code> state.  An exception 
   * may be thrown if the audio resource required by the engine 
   * (audio input or output) is not available.
   * <P>
   *
   * The <code>resume</code> method operates as defined for engines in the
   * <code>ALLOCATED</code> state.  When resume is called for an
   * engine in the <code>ALLOCATING_RESOURCES</code> state, the method
   * blocks (waits) until the <code>ALLOCATED</code> state is
   * reached and then operates normally.  An error is thrown
   * when resume is called for an engine is either the 
   * <code>DEALLOCATED</code> is <code>DEALLOCATING_RESOURCES</code> states.
   * state.
   * <P>
   *
   * The <code>resume</code> method does not always return immediately.
   * Some applications need to execute resume in a separate
   * thread.  The documentation for the <code>allocate</code>
   * method includes an <A HREF="#inner">example implementation 
   * of <code>Runnable</code> with inner classes</A> that could
   * also perform <code>resume</code> in a separate thread.
   * <P>
   *
   * @see #pause
   * @see #getEngineState
   * @see EngineEvent#ENGINE_RESUMED
   * @exception AudioException
   *    if unable to gain access to the audio channel
   * @exception EngineStateError
   *    if called for an engine in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void resume()
    throws AudioException, EngineStateError;


  /**
   * Return an object which provides management of the audio
   * input or output for the <code>Engine</code>.
   * <P>
   *
   * The <code>AudioManager</code> is available in any
   * state of an <code>Engine</code>.
   * <P>
   *
   * @return
   *  the <code>AudioManager</code> for the engine
   */

  public AudioManager getAudioManager();


  /**
   * Return an object which provides management of the 
   * vocabulary for the <code>Engine</code>.  See the 
   * <code>VocabManager</code> documentation for a description 
   * of vocabularies and their use with speech engines.
   * Returns <code>null</code> if the <code>Engine</code> 
   * does not provide vocabulary management capabilities.
   * <P>
   *
   * The <code>VocabManager</code> is available for engines
   * in the <code>ALLOCATED</code> state.  The call blocks
   * for engines in the <code>ALLOCATING_RESOURCES</code>.  An 
   * error is thrown for engines in the <code>DEALLOCATED</code>
   * or <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see Word
   * @return
   *   the <code>VocabManager</code> for the engine or <code>null</code>
   *   if it does not have a <code>VocabManager</code>
   * @exception EngineStateError
   *    if called for an engine in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public VocabManager getVocabManager()
    throws EngineStateError;


  /**
   * Return the <code>EngineProperties</code> object (a JavaBean).
   * <P>
   *
   * A <code>Recognizer</code> returns a <code>RecognizerProperties</code> object.
   * The <code>Recognizer</code> interface also defines a
   * <code>getRecognizerProperties</code> method that returns the same
   * object as <code>getEngineProperties</code>, but without requiring a cast
   * to be useful.
   * <P>
   *
   * A <code>Synthesizer</code> returns a <code>SynthesizerProperties</code> object.
   * The <code>Synthesizer</code> interface also defines a
   * <code>getSynthesizerProperties</code> method that returns the same
   * object as <code>getEngineProperties</code>, but without requiring a cast
   * to be useful.
   * <P>
   *
   * The <code>EngineProperties</code> are available in any
   * state of an <code>Engine</code>.  However, changes only 
   * take effect once an engine reaches the <code>ALLOCATED</code> state.
   * <P>
   *
   * @see Recognizer#getRecognizerProperties
   * @see RecognizerProperties
   * @see Synthesizer#getSynthesizerProperties
   * @see SynthesizerProperties
   * @return
   *   the <code>EngineProperties</code> object for this engine
   */

  public EngineProperties getEngineProperties();


  /**
   * Return a mode descriptor that defines the operating properties
   * of the engine.  For a <code>Recognizer</code> the return value
   * is a <code>RecognizerModeDesc</code>.  For a <code>Synthesizer</code>
   * the return value is a <code>SynthesizerModeDesc</code>.
   * <P>
   *
   * The <code>EngineModeDesc</code> is available in any
   * state of an <code>Engine</code>.
   * <P>
   *
   * @return 
   *   an <code>EngineModeDesc</code> for the engine.
   * @exception SecurityException
   *   if the application does not have <code>accessEngineModeDesc</code> permission
   */

  public EngineModeDesc getEngineModeDesc()
    throws SecurityException;


  /**
   * Request notifications of events of related to the <code>Engine</code>.
   * An application can attach multiple listeners to an <code>Engine</code>.
   * A single listener can be attached to multiple engines.
   * <P>
   *
   * The <code>EngineListener</code> is extended for both recognition
   * and synthesis.  Typically, a <code>RecognizerListener</code> is
   * attached to a <code>Recognizer</code> and a <code>SynthesizerListener</code>
   * is attached to a <code>Synthesizer</code>.
   * <P>
   *
   * An <code>EngineListener</code> can be attached or removed in any
   * state of an <code>Engine</code>.
   * <P>
   *
   * @see Recognizer
   * @see RecognizerListener
   * @see Synthesizer
   * @see SynthesizerListener
   * @param listener
   *   the listener that will receive <code>EngineEvents</code>
   */

  public void addEngineListener(EngineListener listener);


  /**
   * Remove a listener from this <code>Engine</code>.
   * An <code>EngineListener</code> can be attached or removed in any
   * state of an <code>Engine</code>.
   * <P>
   *
   * @param listener
   *   the listener to be removed
   */

  public void removeEngineListener(EngineListener listener);

}

