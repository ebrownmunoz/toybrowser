/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.util.*;

import javax.speech.synthesis.*;
import javax.speech.recognition.*;


/**
  * <code>EngineErrorEvent</code> is an asynchronous notification
  * of an internal error in the engine which prevents normal 
  * behavior of that engine.  The event encapsulates a <code>Throwable</code>
  * object that provides details about the error.
  * <P>
  *
  * @see EngineListener#engineError
  */

public class EngineErrorEvent extends EngineEvent
{
  /**
   * Identifier for event issued when engine error occurs.
   * <P>
   *
   * @see EngineListener#engineError
   */
  
  public static final int ENGINE_ERROR = 550;



  /**
   * <code>Throwable</code> object (<code>Exception</code> or 
   * <code>Error</code>) that describes the detected engine problem.
   * <P>
   *
   * @see #getEngineError
   * @serial
   */

  protected Throwable problem;


  /**
   * Constructs an <code>EngineErrorEvent</code> with an event identifier, 
   * throwable, old engine state and new engine state.  The old and
   * new states are zero if the engine states are unknown or undefined.
   * <P>
   *
   * @param source
   *   the object that issued the event
   * @param id
   *   the identifier for the event type
   * @param throwable
   *   description of the detected error 
   * @param oldEngineState
   *   engine state prior to this event
   * @param newEngineState
   *   engine state following this event
   * @see Engine#getEngineState
   */

  public EngineErrorEvent(Engine source, int id, Throwable throwable, 
		     long oldEngineState, long newEngineState)
  {
    super(source, id, oldEngineState, newEngineState);
  }


  /**
   * Return the <code>Throwable</code> object
   * (<code>Exception</code> or <code>Error</code>) 
   * that describes the engine problem.
   * <P>
   */

  public Throwable getEngineError() { return problem; }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch(id) {
    case ENGINE_ERROR:
      return "ENGINE_ERROR: " + problem.getMessage();
    default:
      return super.paramString();
    }
  }
}


