/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;


/**
 * The <code>Word</code> class provides a standard representation of 
 * speakable words for speech engines.  A <code>Word</code> object
 * provides the following information:
 * 
 * <UL>
 *   <LI>"Written form" string: text that can be used to present the 
 *       <code>Word</code> visually. 
 *   <LI>"Spoken form" text: printable string that indicates how the
 *       word is spoken.
 *   <LI>Pronunciation: a string of phonetic characters indicating
 *       how the word is spoken.
 *   <LI>Grammatical categories: flags indicating grammatical 
 *       "part-of-speech" information.
 * </UL>
 *
 * The written form string is required.  The other properties are
 * optional.  Typically, one or more of the optional properties are
 * specified.  The <code>Word</code> class allows the specification
 * of multiple pronunciations and multiple grammatical categories.  
 * Each pronunciation must be approporiate to each category.  If not,
 * separate <code>Word</code> objects should be created.
 * <P>
 *
 * All the optional properties of a word are hints to the speech engine.
 * Speech engines will use the information as appropriate for their
 * internal design.
 * <P>
 *
 * @see VocabManager
 * @see Engine#getVocabManager
 */


public class Word
{
  private long categories;
  private String writtenForm;
  private String spokenForm;
  private String[] pronunciations;

  /**
   * Grammatical category of word is unknown.
   * The value is zero - <code>0</code> - and implies
   * that no other category flag is set.
   */

  public final static long UNKNOWN = 0;


  /**
   * Grammatical category of word doesn't matter.
   */

  public final static long DONT_CARE = 1L << 0;


  /**
   * Other grammatical category of word not specified elsewhere in this class.
   */

  public final static long OTHER = 1L << 1;


  /**
   * Grammatical category of word is noun.
   * English examples: "car", "house", "elephant".
   */

  public final static long NOUN = 1L << 2;


  /**
   * Grammatical category of word is proper noun.
   * English examples: "Yellowstone", "Singapore".
   */

  public final static long PROPER_NOUN = 1L << 3;


  /**
   * Grammatical category of word is pronoun.
   * English examples: "me", "I", "they".
   */

  public final static long PRONOUN = 1L << 4;


  /**
   * Grammatical category of word is verb.
   * English examples: "run", "debug", "integrate".
   */

  public final static long VERB = 1L << 5;


  /**
   * Grammatical category of word is adverb.
   * English examples: "slowly", "loudly", "barely", "very", "never".
   */

  public final static long ADVERB = 1L << 6;


  /**
   * Grammatical category of word is adjective.
   * English examples: "red", "mighty", "very", "first", "eighteenth".
   */

  public final static long ADJECTIVE = 1L << 7;


  /**
   * Grammatical category of word is proper adjective.
   * English examples: "British", "Brazilian".
   */

  public final static long PROPER_ADJECTIVE = 1L << 8;


  /**
   * Grammatical category of word is auxiliary.
   * English examples: "have", "do", "is", "shall", "must", "cannot".
   */

  public final static long AUXILIARY = 1L << 9;


  /**
   * Grammatical category of word is determiner.
   * English examples: "the", "a", "some", "many", "his", "her".
   */

  public final static long DETERMINER = 1L << 10;


  /**
   * Grammatical category of word is cardinal.
   * English examples: "one", "two", "million".
   */

  public final static long CARDINAL = 1L << 11;


  /**
   * Grammatical category of word is conjunction.
   * English examples: "and", "or", "since", "if".
   */

  public final static long CONJUNCTION = 1L << 12;


  /**
   * Grammatical category of word is preposition.
   * English examples: "of", "for".
   */

  public final static long PREPOSITION = 1L << 13;

  /**
   * Grammatical category is contraction.
   * English examples: "don't", "can't".
   */

  public final static long CONTRACTION = 1L << 14;


  /**
   * Word is an abbreviation or acronynm.
   * English examples: "Mr.", "USA".
   */

  public final static long ABBREVIATION = 1L << 15;



  /**
   * Set the "written form" of the <code>Word</code>.
   * The written form text should be a string that could be
   * used to present the <code>Word</code> visually. 
   */
  
  public void setWrittenForm(String text) { this.writtenForm = text; }


  /**
   * Get the written form of the <code>Word</code>.
   */

  public String getWrittenForm() { return writtenForm; }


  /**
   * Set the "spoken form" of the <code>Word</code>.
   * May be <code>null</code>.
   * <P>
   *
   * The spoken form of a word is useful for mapping the written form
   * to words that are likely to be handled by a speech recognizer or
   * synthesizer.  For example, "JavaSoft" to "java soft", 
   * "toString" -> "to string", "IEEE" -> "I triple E".
   */

  public void setSpokenForm(String text) { this.spokenForm = text; }


  /**
   * Get the "spoken form" of the <code>Word</code>.
   * Returns <code>null</code> if the spoken form is not defined.
   */

  public String getSpokenForm() { return spokenForm; }


  /**
   * Set the pronunciation of the <code>Word</code> as
   * an array containing a phonetic character <code>String</code>
   * for each pronunciation of the word.
   * <P>
   *
   * The pronunciation string uses the IPA subset of Unicode.
   * <P>
   *
   * The string should be <code>null</code> if no pronunciation is 
   * available.  Speech engines should be expected to handle
   * most words of the language they support.
   * <P>
   *
   * Recognizers can use pronunciation information to improve 
   * recognition accuracy.  Synthesizers use the information to
   * accurately speak unusual words (e.g., foreign words).
   */

  public void setPronunciations(String pron[]) { this.pronunciations = pron; }


  /**
   * Get the pronunciations of the <code>Word</code>.  
   * The pronunciation string uses the Unicode IPA subset.
   * Returns <code>null</code> if no pronunciations are specified.
   */

  public String[] getPronunciations() { return pronunciations; }


  /**
   * Set the categories of the <code>Word</code>.  
   * The categories may be <code>UNKNOWN</code> or may be an 
   * OR'ed set of the defined categories such as <code>NOUN</code>, 
   * <code>VERB</code>, <code>PREPOSITION</code>.  For example:
   *
   * <PRE>
   *     Word w = new Word("running");
   *     w.setCategories(Word.NOUN | Word.VERB);
   * </PRE>
   *
   * The category information is a guide to the word's grammatical role.
   * Speech synthesizers can use
   * this information to improve phrasing and accenting.
   */

  public void setCategories(long cat) { this.categories = cat; }


  /**
   * Get the categories of the <code>Word</code>.
   * Value may be <code>UNKNOWN</code> or an OR'ed set
   * of the categories defined by this class.
   */

  public long getCategories() { return categories; }
}

