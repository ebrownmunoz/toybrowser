/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech;

import java.util.*;

import javax.speech.synthesis.*;
import javax.speech.recognition.*;


/**
 * Interface defining methods to be called when state-change events
 * for a speech engine occur.  To receive engine events an
 * application attaches a listener by calling the 
 * <code>addEngineListener</code> method of an <code>Engine</code>.
 * A listener is removed by a call to the 
 * <code>removeEngineListener</code> method.
 * <P>
 * 
 * The event dispatch policy is defined in the documentation
 * for the <code>SpeechEvent</code> class.
 * <P>
 *
 * This interface is extended by the <code>RecognizerListener</code> 
 * and <code>SynthesizerListener</code> interfaces to handle the
 * specialized events of speech recognizers and synthesizers.
 * <P>
 *
 * A trivial implementation of <code>EngineListener</code> is 
 * provided by the <code>EngineAdapter</code> class.  
 * <code>RecognizerAdapter</code> and <code>SynthesizerAdapter</code>
 * classes are also available.
 * <P>
 *
 * @see SpeechEvent
 * @see EngineAdapter
 * @see RecognizerListener
 * @see RecognizerAdapter
 * @see SynthesizerListener
 * @see SynthesizerAdapter
 */


public interface EngineListener extends EventListener
{
  /**
   * The <code>Engine</code> has been paused.
   * <P>
   *
   * @see EngineEvent#ENGINE_PAUSED
   */

  public void enginePaused(EngineEvent e);


  /**
   * The <code>Engine</code> has been resumed.
   * <P>
   *
   * @see EngineEvent#ENGINE_RESUMED
   */

  public void engineResumed(EngineEvent e);


  /**
   * The <code>Engine</code> has been allocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_ALLOCATED
   */

  public void engineAllocated(EngineEvent e);


  /**
   * The <code>Engine</code> has been deallocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_DEALLOCATED
   */

  public void engineDeallocated(EngineEvent e);



  /**
   * The <code>Engine</code> is being allocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_ALLOCATING_RESOURCES
   */

  public void engineAllocatingResources(EngineEvent e);


  /**
   * The <code>Engine</code> is being deallocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_DEALLOCATING_RESOURCES
   */

  public void engineDeallocatingResources(EngineEvent e);


  /**
   * An <code>EngineErrorEvent</code> has occurred and the
   * <code>Engine</code> is unable to continue normal operation.
   * <P>
   *
   * @see EngineErrorEvent
   */

  public void engineError(EngineErrorEvent e);
}



