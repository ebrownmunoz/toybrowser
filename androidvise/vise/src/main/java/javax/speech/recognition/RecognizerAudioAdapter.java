/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Adaptor for a audio events of a recognizer.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 */


public class RecognizerAudioAdapter extends AudioAdapter implements RecognizerAudioListener
{
  /**
   * The recognizer has detected a possible start of speech.
   * <P>
   *
   * @see RecognizerAudioEvent#SPEECH_STARTED
   */

  public void speechStarted(RecognizerAudioEvent e) {}


  /**
   * The recognizer has detected a possible end of speech.
   * <P>
   *
   * @see RecognizerAudioEvent#SPEECH_STOPPED
   */

  public void speechStopped(RecognizerAudioEvent e) {}


  /**
   * <code>AUDIO_LEVEL</code> event has occurred indicating a change in the 
   * current volume of the audio input to a recognizer. 
   * <P>
   *
   * @see RecognizerAudioEvent#AUDIO_LEVEL
   */

  public void audioLevel(RecognizerAudioEvent e) {}
}
