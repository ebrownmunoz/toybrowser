/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Extends the set of audio event of an engine for a recognizer
 * by adding a audio level event. 
 * <P>
 *
 * The <code>RecognizerAudioAdapter</code> class provided a
 * trivial implementation of this listener.
 * <P>
 *
 * @see RecognizerAudioAdapter
 */


public interface RecognizerAudioListener extends AudioListener
{
  /**
   * The recognizer has detected a possible start of speech.
   * <P>
   *
   * @see RecognizerAudioEvent#SPEECH_STARTED
   */

  public void speechStarted(RecognizerAudioEvent e);


  /**
   * The recognizer has detected a possible end of speech.
   * <P>
   *
   * @see RecognizerAudioEvent#SPEECH_STOPPED
   */

  public void speechStopped(RecognizerAudioEvent e);


  /**
   * <code>AUDIO_LEVEL</code> event has occurred indicating a change in the 
   * current volume of the audio input to a recognizer. 
   * <P>
   *
   * @see RecognizerAudioEvent#AUDIO_LEVEL
   */

  public void audioLevel(RecognizerAudioEvent e);
}


