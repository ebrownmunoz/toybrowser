/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * A <code>ResultEvent</code> is issued by a <code>Result</code> object 
 * to indicate changes in the recognized tokens and changes in state.
 * An event is issued to all appropriate <code>ResultListener</code> objects.
 * <code>ResultListeners</code> can be attached in three places:
 * <P>
 *
 * <UL>
 *   <LI>A <code>ResultListener</code> attached to the <code>Recognizer</code>
 *       receives all events for all results produced by the
 *       recognizer.
 *   <LI>A <code>ResultListener</code> attached to a <code>Grammar</code>
 *       receives all events for all results that have been 
 *       finalized for that <code>Grammar</code>: the 
 *       <code>GRAMMAR_FINALIZED</code> event and following events.
 *       (Note: it never receives a <code>RESULT_CREATED</code> event
 *       because that event always precedes <code>GRAMMAR_FINALIZED</code>).
 *   <LI>A <code>ResultListener</code> attached to a <code>Result</code>
 *       receives all events for that result starting from the
 *       time at which the listener is attached.  (Note: it never
 *       receives a <code>RESULT_CREATED</code> event because a listener
 *       can only be attached to a <code>Result</code> once a
 *       <code>RESULT_CREATED</code> event has already been issued.)
 * </UL>
 * <P>
 *
 * In all three forms of listener attachment, one listener can be 
 * attached to multiple objects, and multiple listeners can be
 * attached to a single object.
 * <P>
 *
 * The <A HREF="Result.html#states">three states of a recognition result</A> 
 * are described in the documentation for the <code>Result</code> interface
 * and can be tested by the <code>getResultState</code> method of that interface.
 * The <code>RESULT_CREATED</code>, <code>RESULT_ACCEPTED</code> and
 * <code>RESULT_REJECTED</code> events indicate result state changes.
 * <P>
 *
 * In brief, the three states are:
 * <P>
 *
 * <UL>
 *  <LI><code>UNFINALIZED</code> state: initial state of result indicating that
 *      recognition is still taking place.  A new result is created
 *      in the <code>UNFINALIZED</code> state with a <code>RESULT_CREATED</code>
 *      event.
 *  <LI><code>ACCEPTED</code> state: the result is finalized and the recognizer
 *      is confident it has recognized the result correctly.  The 
 *      <code>RESULT_ACCEPTED</code> event indicates a state change
 *      from the <code>UNFINALIZED</code> state to the <code>ACCEPTED</code> state.
 *  <LI><code>REJECTED</code> state: the result is finalized but the recognizer
 *      is not confident it has recognized the result correctly.  The 
 *      <code>RESULT_REJECTED</code> event indicates a state change
 *      from the <code>UNFINALIZED</code> state to the <code>REJECTED</code> state.
 * </UL>
 * <P>
 *
 * The <A HREF="Result.html#events">sequence of <code>ResultEvents</code>
 * associated with a recognition result</A>
 * are described in the documentation for the <code>Result</code> interface.
 * <P>
 *
 * In brief, the events that occur depend upon the <code>Result</code> state:
 * <P>
 *
 * <UL>
 *  <LI>A <code>RESULT_CREATED</code> event creates a <code>Result</code>
 *      object.  A new result is starts in the <code>UNFINALIZED</code>
 *      state.
 *  <LI><code>UNFINALIZED</code> state: <code>RESULT_UPDATED</code> 
 *      events indicate a change in finalized and/or unfinalized tokens;
 *      a <code>GRAMMAR_FINALIZED</code> event indicates that the
 *      <code>Grammar</code> matched by this result has been identified.
 *  <LI>The <code>RESULT_ACCEPTED</code> event finalizes a result by
 *      indicating a change in state from <code>UNFINALIZED</code> 
 *      to <code>ACCEPTED</code>.
 *  <LI>The <code>RESULT_REJECTED</code> event finalizes a result by
 *      indicating a change in state from <code>UNFINALIZED</code> 
 *      to <code>REJECTED</code>.
 *  <LI>In the finalized states - <code>ACCEPTED</code> and <code>REJECTED</code> -
 *      the <code>AUDIO_RELEASED</code> and <code>TRAINING_INFO_RELEASED</code> 
 *      may be issued.
 * </UL>
 *
 * <P>
 * 
 * @see Recognizer
 * @see Result
 * @see ResultListener
 * @see FinalResult
 * @see FinalRuleResult
 * @see FinalDictationResult
 */


public class ResultEvent extends SpeechEvent
{
  /**
   * <code>RESULT_CREATED</code> is issued when a new <code>Result</code>
   * is created.  The event is received by each <code>ResultListener</code>
   * attached to the <code>Recognizer</code>.
   * <P>
   * 
   * When a result is created, it is in the <code>UNFINALIZED</code>
   * state.  When created the result may have zero or more
   * finalized tokens and zero or more unfinalized tokens.  The
   * presence of finalized and unfinalized tokens is indicated
   * by the <code>isTokenFinalized</code> and 
   * <code>isUnfinalizedTokensChanged</code> flags.
   * <P>
   *
   * The <code>RESULT_CREATED</code> event follows the 
   * <code>RECOGNIZER_PROCESSING</code> event which transitions the
   * <code>Recognizer</code> from the <code>LISTENING</code> state to
   * the <code>PROCESSING</code> state.
   * <P> 
   *
   * @see ResultListener#resultCreated
   * @see Result#UNFINALIZED
   * @see Recognizer#PROCESSING
   * @see RecognizerEvent#RECOGNIZER_PROCESSING
   * @see #isTokenFinalized
   * @see #isUnfinalizedTokensChanged
   */

  public final static int RESULT_CREATED = 801;


  /**
   * <code>RESULT_UPDATED</code> is issued when one or more tokens of 
   * a <code>Result</code> are finalized or when the unfinalized tokens 
   * of a result are changed.  The <code>isTokenFinalized</code> and
   * <code>isUnfinalizedTokensChanged</code> flags are set appropriately.
   * <P>
   * 
   * The <code>RESULT_UPDATED</code> event only occurs when a 
   * <code>Result</code> is in the <code>UNFINALIZED</code> state.
   * <P>
   * 
   * @see #isTokenFinalized
   * @see #isUnfinalizedTokensChanged
   * @see Result#UNFINALIZED
   * @see ResultListener#resultUpdated
   */

  public final static int RESULT_UPDATED = 802;


  /**
   * <code>GRAMMAR_FINALIZED</code> is issued when the <code>Grammar</code>
   * matched by a <code>Result</code> is identified and finalized.
   * Before this event the <code>getGrammar</code> method of a 
   * <code>Result</code> returns <code>null</code>. Following the
   * event it is guaranteed to return non-null and the grammar
   * is guaranteed not to change.
   * <P>
   * 
   * The <code>GRAMMAR_FINALIZED</code> event only occurs for a 
   * <code>Result</code> is in the <code>UNFINALIZED</code> state.
   * <P>
   *
   * A <code>GRAMMAR_FINALIZED</code> event does not affect finalized
   * or unfinalized tokens.
   * <P>
   * 
   * @see #isTokenFinalized
   * @see #isUnfinalizedTokensChanged
   * @see Result#UNFINALIZED
   * @see ResultListener#grammarFinalized
   */

  public final static int GRAMMAR_FINALIZED = 803;


  /**
   * <code>RESULT_ACCEPTED</code> event is issued when a <code>Result</code>  
   * is successfully finalized and indicates a state change from
   * <code>UNFINALIZED</code> to <code>ACCEPTED</code>.  
   * <P>
   *
   * In the finalization transition, zero or more tokens may be finalized 
   * and the unfinalized tokens are set to <code>null</code>.  
   * The <code>isTokenFinalized</code> and <code>isUnfinalizedTokensChanged</code>
   * flags are set appropriately.
   * <P>
   *
   * Since the <code>Result</code> is finalized (accepted),
   * the methods of <code>FinalResult</code> and either
   * <code>FinalRuleResult</code> or <code>FinalDictationResult</code>
   * can be used.  (Use the <code>getGrammar</code> method of <code>Result</code>
   * to determine the type of grammar matched.)  
   * Applications should use type casting to ensure that only
   * the appropriate interfaces and method are used.
   * <P>
   *
   * The <code>RESULT_ACCEPTED</code> event is issued after the
   * <code>Recognizer</code> issues a <code>RECOGNIZER_SUSPENDED</code> 
   * event to transition from the <code>PROCESSING</code> state to
   * the <code>SUSPENDED</code> state.  Any changes made
   * to grammars or the enabled state of grammars during the processing
   * of the <code>RESULT_ACCEPTED</code> event are automatically
   * committed once the <code>RESULT_ACCEPTED</code> event has been
   * processed by all <code>ResultListeners</code>.  Once those 
   * changes have been committed, the <code>Recognizer</code> returns
   * to the <code>LISTENING</code> state with a <code>CHANGES_COMMITTED</code>
   * event.  A call to <code>commitChanges</code> is not required.
   * (Except, if there is a call to <code>suspend</code> without
   * a subsequent call to <code>commitChanges</code>, the <code>Recognizer</code>
   * defers the commit until the <code>commitChanges</code> call is
   * received.)
   * <P>
   *
   * @see ResultListener#resultAccepted
   * @see #isTokenFinalized
   * @see #isUnfinalizedTokensChanged
   * @see Result#getResultState
   * @see Result#getGrammar
   * @see Recognizer#SUSPENDED
   * @see RecognizerEvent#RECOGNIZER_SUSPENDED
   */

  public final static int RESULT_ACCEPTED = 804;


  /**
   * <code>RESULT_REJECTED</code> event is issued when a <code>Result</code>  
   * is unsuccessfully finalized and indicates a change from the
   * <code>UNFINALIZED</code> state to the <code>REJECTED</code> state. 
   * <P>
   *
   * In the state transition, zero or more tokens may be finalized 
   * and the unfinalized tokens are set to <code>null</code>.  
   * The <code>isTokenFinalized</code> and <code>isUnfinalizedTokensChanged</code>
   * flags are set appropriately.  However, because the result is
   * rejected, the tokens are quite likely to be incorrect.
   * <P>
   *
   * Since the <code>Result</code> is finalized (rejected),
   * the methods of <code>FinalResult</code> can be used.
   * If the grammar is known (<code>GRAMMAR_FINALIZED</code> event
   * was issued and the <code>getGrammar</code> methods returns
   * non-null) then the <code>FinalRuleResult</code> or 
   * <code>FinalDictationResult</code> interface can also be used
   * depending upon whether the matched grammar was a 
   * <code>RuleGrammar</code> or <code>DictationGrammar</code>.
   * <P>
   * 
   * Other state transition behavior for <code>RESULT_REJECTED</code>
   * is the same as for the <code>RESULT_ACCEPTED</code> event.
   * <P>
   *
   *
   * @see #isTokenFinalized
   * @see #isUnfinalizedTokensChanged
   * @see Result#getResultState
   * @see Result#getGrammar
   * @see ResultListener#resultRejected
   */

  public final static int RESULT_REJECTED = 805;


  /**
   * <code>AUDIO_RELEASED</code> event is issued when the audio information 
   * associated with a <code>FinalResult</code> object is released.  
   * The release may have been requested by an application call to 
   * <code>releaseAudio</code> in the <code>FinalResult</code> interface
   * or may be initiated by the recognizer to reclaim memory.  
   * The <code>FinalResult.isAudioAvailable</code> 
   * method returns <code>false</code> after this event.
   * <P>
   * 
   * The <code>AUDIO_RELEASED</code> event is only issued for results
   * in a finalized state (<code>getResultState</code> returns either 
   * <code>ACCEPTED</code> or <code>REJECTED</code>).
   * <P>
   *
   * @see FinalResult#releaseAudio
   * @see FinalResult#isAudioAvailable
   * @see Result#getResultState
   * @see ResultListener#audioReleased
   */

  public final static int AUDIO_RELEASED = 806;


  /**
   * <code>TRAINING_INFO_RELEASED</code> event is issued when the 
   * training information for a finaliized result is released.  
   * The release may have been requested by an application call to the
   * <code>releaseTrainingInfo</code> method in the <code>FinalResult</code>
   * interface or may be initiated by the recognizer to reclaim memory.
   * The <code>isTrainingInfoAvailable</code> method of <code>FinalResult</code>
   * returns <code>false</code> after this event.
   * <P>
   * 
   * The <code>TRAINING_INFO_RELEASED</code> event is only issued for 
   * results in a finalized state (<code>getResultState</code> returns either 
   * <code>ACCEPTED</code> or <code>REJECTED</code>).
   * <P>
   *
   * @see FinalResult#releaseTrainingInfo
   * @see FinalResult#isTrainingInfoAvailable
   * @see Result#getResultState
   * @see ResultListener#trainingInfoReleased
   */

  public final static int TRAINING_INFO_RELEASED = 807;


  /**
   * True if the <code>ResultEvent</code> indicates that one or more 
   * tokens have been finalized.  Tokens may be finalized with
   * any of the following result events:
   * <code>RESULT_CREATED</code>, <code>RESULT_UPDATED</code>,
   * <code>RESULT_ACCEPTED</code>, <code>RESULT_REJECTED</code>.
   * <P>
   * 
   * @see #isTokenFinalized
   * @serial
   */

  protected boolean tokenFinalized = false;


  /**
   * True if the <code>ResultEvent</code> indicates that the 
   * unfinalized tokens have changed.  The unfinalized tokens
   * may change with the following result events:
   * <code>RESULT_CREATED</code>, <code>RESULT_UPDATED</code>,
   * <code>RESULT_ACCEPTED</code>, <code>RESULT_REJECTED</code>.
   * <P>
   * 
   * @see #isUnfinalizedTokensChanged
   * @serial
   */

  protected boolean unfinalizedTokensChanged = false;


  /**
   * Constructs a <code>ResultEvent</code> for a specified source 
   * <code>Result</code> and result event id.  The two boolean flags 
   * indicating change in tokens should be set appropriately for 
   * <code>RESULT_CREATED</code>, <code>RESULT_UPDATED</code>,
   * <code>RESULT_ACCEPTED</code> and <code>RESULT_REJECTED</code> events.  
   * (For other event types these flags should be <code>false</code>).
   * <P>
   *
   * @see #RESULT_CREATED
   * @see #GRAMMAR_FINALIZED
   * @see #RESULT_UPDATED
   * @see #RESULT_ACCEPTED
   * @see #RESULT_REJECTED
   * @see #AUDIO_RELEASED
   * @see #TRAINING_INFO_RELEASED
   *
   * @param source
   *   the <code>Result</code> object that issued the event
   * @param id
   *   the identifier for the event type
   * @param isTokenFinalized
   *   true if any token is finalized with this event
   * @param isUnfinalizedTokensChanged
   *   true if the unfinalized text is changed with this event
   */

  public ResultEvent(Result source, int id, 
		     boolean isTokenFinalized, 
		     boolean isUnfinalizedTokensChanged)
  {
    super(source, id);
    this.tokenFinalized = isTokenFinalized;
    this.unfinalizedTokensChanged = isUnfinalizedTokensChanged;
  }


  /**
   * Constructs a <code>ResultEvent</code> with an event type identifier.
   * The <code>isTokenFinalized</code> and <code>isUnfinalizedTokensChanged</code>
   * flags are set to <code>false</code>.
   * <P>
   *
   * @param source
   *   the object that issued the event
   * @param id
   *   the identifier for the event type
   */

  public ResultEvent(Result source, int id)
  {
    super(source, id);

    this.tokenFinalized = false;
    this.unfinalizedTokensChanged = false;
  }


  /**
   * For <code>RESULT_CREATED</code>, <code>RESULT_UPDATED</code>, 
   * <code>RESULT_ACCEPTED</code> and <code>RESULT_REJECTED</code> events 
   * returns <code>true</code> if any tokens were finalized.  
   * For other events, return <code>false</code>.
   * If true, the number of tokens returned by <code>numTokens</code>
   * and <code>getBestTokens</code> has increased.
   * <P>
   *
   * @see Result#numTokens
   * @see Result#getBestTokens
   */

  public boolean isTokenFinalized()
    { return tokenFinalized; }


  /**
   * For <code>RESULT_CREATED</code>, <code>RESULT_UPDATED</code>,
   * <code>RESULT_ACCEPTED</code> and <code>RESULT_REJECTED</code> 
   * events returns <code>true</code> if the unfinalized tokens changed. 
   * For other events, return <code>false</code>.
   * If true, the value returned by <code>getUnfinalizedTokens</code>
   * has changed.
   * <P>
   * 
   * Note that both <code>RESULT_ACCEPTED</code> and <code>RESULT_REJECTED</code> 
   * events implicitly set the unfinalized text to <code>null</code>.  The
   * <code>isUnfinalizedTokensChanged</code> method should return <code>true</code>
   * only if the unfinalized text was non-null prior to finalization.
   * <P>
   *
   * @see Result#getUnfinalizedTokens
   */

  public boolean isUnfinalizedTokensChanged()
    { return unfinalizedTokensChanged; }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    StringBuffer buf = new StringBuffer();

    switch(id) {
    case RESULT_CREATED:           buf.append("RESULT_CREATED");           break;
    case RESULT_UPDATED:           buf.append("RESULT_UPDATED");           break;
    case GRAMMAR_FINALIZED:        buf.append("GRAMMAR_FINALIZED");        break;
    case RESULT_ACCEPTED:          buf.append("RESULT_ACCEPTED");          break;
    case RESULT_REJECTED:          buf.append("RESULT_REJECTED");          break;
    case AUDIO_RELEASED:           buf.append("AUDIO_RELEASED");           break;
    case TRAINING_INFO_RELEASED:   buf.append("TRAINING_INFO_RELEASED");   break;
    default:
      return super.paramString();
    }

    switch(id) {
    case RESULT_CREATED:
    case RESULT_UPDATED:
    case RESULT_ACCEPTED:
    case RESULT_REJECTED:
      if (tokenFinalized) buf.append(": token finalized");
      if (unfinalizedTokensChanged) buf.append(": unfinalized tokens changed");
    }

    return buf.toString();
  }
}


