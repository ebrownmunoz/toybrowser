/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;

import java.util.*;


/** 
  * A <code>RuleName</code> is a reference to a named rule.
  * A <code>RuleName</code> is equivalent to the various forms
  * of rulename syntax in the 
  * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSGF/index.html">
  * Java Speech Grammar Format (JSGF)</A>.
  * <A NAME="#rules"></A><P>
  *
  * A <em>fully-qualified rulename</em> consists of a <em>full grammar name</em>
  * plus the <em>simple rulename</em>.  A full grammar name consists of
  * a <em>package name</em> and a <em>simple grammar name</em>.  The three legal
  * forms of a rulename allowed by the Java Speech Grammar Format 
  * and in the <code>RuleName</code> object are:
  * 
  * <UL>
  *  <LI>Simple rulename: <code>&lt;simpleRulename&gt;</code>
  *      <BR>e.g. <code>&lt;digits&gt;</code>, <code>&lt;date&gt;</code>
  *  <LI>Qualified rulename: <code>&lt;simpleGrammarName.simpleRulename&gt;</code>
  *      <BR>e.g. <code>&lt;numbers.digits&gt;</code>, <code>&lt;places.cities&gt;</code>
  *  <LI>Fully-qualified rulename: <code>&lt;packageName.simpleGrammarName.simpleRulename&gt;</code>
  *      <BR>e.g. 
  *      <code>&lt;com.sun.numbers.digits&gt;</code>
  *      <code>&lt;com.acme.places.zipCodes&gt;</code>
  * </UL>
  *
  * The full grammar name is the following portion of the 
  * fully-qualified rulename: <code>packageName.simpleGrammarName</code>.
  * For example, 
  *      <code>&lt;com.sun.numbers&gt;</code>
  *      <code>&lt;com.acme.places&gt;</code>.
  * <P>
  *
  * There are two special rules are defined in JSGF, 
  * <CODE>&lt;NULL&gt;</CODE> and <CODE>&lt;VOID&gt;</CODE>.
  * Both have static instances in this class for convenience.  
  * These rulenames can be referenced in any grammar without an import statement.  
  * <P>
  *
  * There is a special case of using a <code>RuleName</code> to declare
  * and manage imports of a <code>RuleGrammar</code>.  This form is used
  * with the <code>addImport</code> and <code>removeImport</code> methods
  * of a <code>RuleGrammar</code>.  It requires a full grammar name plus
  * the string <code>"*"</code> for the simple rulename.  For example:
  * <code>&lt;com.acme.places.*&gt;</code>
  * <P>
  *
  * The angle brackets placed around rulenames are syntactic constructs in JSGF
  * but are not a part of the rulename.  For clarity of code, the angle brackets
  * may be included in calls to this class but they are automatically stripped.
  * <P>
  *
  * The following referencing and name resolution conditions of JSGF apply.
  * <UL>
  *  <LI>Any rule in a local grammar may be referenced with a simple rulename,
  *      qualified rulename or fully-qualified rulename.  
  *  <LI>A public rule of another grammar may be referenced by its simple rulename
  *      or qualified rulename if the rule is imported and the name is not
  *      ambiguous with another imported rule.
  *  <LI>A public rule of another grammar may be referenced by its
  *      fully-qualified rulename with or without a corresponding import
  *      statement.
  * </UL>
  *
  * @see Rule
  * @see RuleGrammar#addImport
  */


public class RuleName extends Rule
{
  /**
   * The complete specified rulename.  Maybe a fully-qualified rulename,
   * qualified rulename, or simple rulename depending upon how the
   * object is constructed.
   *
   * @serial
   */

  protected String fullRuleName;

  /**
   * The rule's package name or <code>null</code> if not specified.
   *
   * @serial
   */

  protected String packageName;

  
  /**
   * The rule's simple grammar name or <code>null</code> if not specified.
   *
   * @serial
   */

  protected String simpleGrammarName;


  /**
   * The simple rulename.
   * @serial
   */
  protected String simpleRuleName;


  /**
   * Special <CODE>&lt;NULL&gt;</CODE> rule of JSGF 
   * defining a rule that is always matched.
   */

  public static RuleName NULL = new RuleName("NULL");


  /**
   * Special <CODE>&lt;VOID&gt;</CODE> rule of JSGF 
   * defining a rule that can never be matched.
   */

  public static RuleName VOID = new RuleName("VOID");


  /**
   * Construct a <code>RuleName</code> from a string.
   * Leading and trailing angle brackets are stripped if found.
   * The rulename may be a simple rulename, qualified rulename
   * or full-qualified rulename.
   */

  public RuleName(String ruleName) {
    setRuleName(ruleName);
  }


  /**
   * Empty constructor which sets the rule to <CODE>&lt;NULL&gt;</CODE>.
   */

  public RuleName() {
    setRuleName("NULL");
  }


  /**
   * Construct a <code>RuleName</code> from its package-, grammar- 
   * and simple-name components.  Leading and trailing angle brackets 
   * are stripped from <code>ruleName</code> if found.
   * The package name may be <code>null</code>.  The grammar name may be <code>null</code>
   * only if <code>packageName</code> is <code>null</code>.
   * <P>
   *
   * @exception IllegalArgumentException 
   *  null <code>simpleGrammarName</code> with non-null <code>packageName</code>
   * @param packageName
   *  the package name of a fully-qualified rulename or <code>null</code>
   * @param simpleGrammarName
   *  the grammar name of a fully-qualified or qualified rule or <code>null</code>
   * @param simpleRuleName
   *  the simple rulename
   */

  public RuleName(String packageName, String simpleGrammarName, String simpleRuleName)
    throws IllegalArgumentException {
    setRuleName(packageName, simpleGrammarName, simpleRuleName);
  }


  /**
   * Get therulename including the package and grammar name
   * if they are non-null.  The return value may be a fully-qualified 
   * rulename, qualified rulename, or simple rulename.
   */

  public String getRuleName() {return fullRuleName;}


  /**
   * Set the rulename.
   * The rulename may be a simple-, qualified- or fully-qualified rulename.
   * Leading and trailing angle brackets are stripped if found.
   */

  public void setRuleName(String ruleName)
    {
      String n = stripRuleName(ruleName);
      this.fullRuleName = n;

      int pidx, gidx;  // Index of '.' for package and grammar.

      gidx = n.lastIndexOf('.');

      if (gidx < 0)
	{
	  // Simple rule reference.
	  this.packageName = null;
	  this.simpleGrammarName = null;
	  this.simpleRuleName = n;
	}
      else
	{
	  pidx = n.lastIndexOf('.', gidx-1);

	  if (pidx < 0)
	    {
	      // Qualified name: simpleGrammarName.ruleName
	      this.packageName = null;
	      this.simpleGrammarName = n.substring(0, gidx);
	      this.simpleRuleName = n.substring(gidx+1);
	    }
	  else
	    {
	      // Fully-qualified name: packageName.simpleGrammarName.ruleName
	      this.packageName = n.substring(0, pidx);
	      this.simpleGrammarName = n.substring(pidx+1, gidx);
	      this.simpleRuleName = n.substring(gidx+1);
	    }
	}
    }


  /**
   * Set the rule's name with package name, simple grammar name
   * and simple rulename components.
   * Leading and trailing angle brackets are stripped from <code>ruleName</code>.
   * The package name may be <code>null</code>.
   * The simple grammar name may be null only if <code>packageName</code> is <code>null</code>.
   * <P>
   *
   * @exception IllegalArgumentException 
   *  null <code>simpleGrammarName</code> with non-null <code>packageName</code>
   */

  public void setRuleName(String packageName, String simpleGrammarName, String simpleRuleName)
    throws IllegalArgumentException
    {
      if (simpleGrammarName == null && !(packageName == null))
	throw new IllegalArgumentException("null simpleGrammarName with non-null packageName");

      this.packageName = packageName;
      this.simpleGrammarName = simpleGrammarName;
      this.simpleRuleName = stripRuleName(simpleRuleName);

      StringBuffer tmp = new StringBuffer();

      if (packageName != null)
	tmp.append(packageName + '.');

      if (simpleGrammarName != null)
	tmp.append(simpleGrammarName + '.');

      tmp.append(simpleRuleName);

      fullRuleName = tmp.toString();
    }




  /**
   * Get the simple rulename.
   */

  public String getSimpleRuleName() {return simpleRuleName;}


  /**
   * Get the simple grammar name.
   * May be <code>null</code>.
   */

  public String getSimpleGrammarName() {return simpleGrammarName;}


  /**
   * Get the full grammar name.
   * If the <code>packageName</code> is <code>null</code>, 
   * the return value is the simple grammar name.
   * May return <code>null</code>.
   */

  public String getFullGrammarName()
  {
    if (packageName != null)
      return packageName + "." + simpleGrammarName;
    else
      return simpleGrammarName;
  }


  /**
   * Get the rule's package name.
   */

  public String getPackageName() {return packageName;}


  /**
   * Tests whether this <code>RuleName</code> is a legal JSGF rulename.
   * The <code>isLegalRuleName(java.lang.String)</code> method defines
   * legal rulename forms.
   * <P>
   *
   * @see #isLegalRuleName(java.lang.String)
   * @see #isRuleNamePart(char)
   */

  public boolean isLegalRuleName()
  {
    return isLegalRuleName(fullRuleName);
  }


  /**
   * Tests whether a string is a legal JSGF rulename format.
   * The <A HREF="#rules">legal patterns</A> for rulenames are defined above.
   * The method does not test whether the rulename exists or 
   * is resolvable (see the <code>resolve</code> method of
   * <code>RuleGrammar</code>).
   * <P>
   *
   * An import string (e.g. "com.acme.*") is considered legal
   * even though the "*" character is not a legal rulename character.
   * If present, starting and ending angle brackets are ignored.
   * <P>
   *
   * @see #isLegalRuleName()
   * @see #isRuleNamePart(char)
   */

  public static boolean isLegalRuleName(String name)
  {
    if (name == null)
      return false;

    name = stripRuleName(name);

    // Check for import
    if (name.endsWith(".*"))
      name = name.substring(0, name.length()-2);

    // Zero-length strings fail
    if (name.length() == 0)
      return false;

    // Work-around limitations of StringTokenizer
    // It doesn't return null tokens for the following
    //   ".name" // period at start
    //   "name." // period at end
    //   "a..b"  // two periods together

    if (name.startsWith(".") || name.endsWith(".") || name.indexOf("..") >= 0)
      return false;

    // Check parts in turn
    StringTokenizer toks = new StringTokenizer(name, ".");

    while (toks.hasMoreTokens()) {
      String part = toks.nextToken();

      int l = part.length();

      if (l == 0) return false;

      for (int i=0; i<l; i++)
	if (!isRuleNamePart(part.charAt(i)))
	  return false;
    }

    return true;
  }


  /**
   * Tests whether a character is a legal part of a 
   * Java Speech Grammar Format rulename.
   * <P>
   *
   * @see #isLegalRuleName()
   * @see #isLegalRuleName(java.lang.String)
   */

  public static boolean isRuleNamePart(char c)
    {
      if (java.lang.Character.isJavaIdentifierPart(c))
	return true;

      if (c == '!' || c == '#' || c == '%' || c == '&' || c == '('
	  || c == ')' || c == '+' || c == ',' || c == '-' || c == '/'
	  || c == ':' || c == ';' || c == '=' || c == '@' || c == '['
	  || c == '\\' || c == ']' || c == '^' || c == '|' || c == '~')
	return true;

      return false;
    }


  /**
   * Remove a pair of leading/trailing angle brackets <>.
   */

  private static String stripRuleName(String n)
    {
      if (n.startsWith("<") && n.endsWith(">"))
	return n.substring(1, n.length()-1);

      return n;
    }


  /**
   * Return a deep copy of this rule.
   */

  public Rule copy()
  {
    return new RuleName(packageName, simpleGrammarName, simpleRuleName);
  }


  /**
   * Return a String representing the <code>RuleName</code> as
   * partial Java Speech Grammar Format text.
   * The return value is <code>&lt;RuleName&gt;</code>.
   */

  public String toString()
    {
      return "<" + fullRuleName + ">";
    }
}

