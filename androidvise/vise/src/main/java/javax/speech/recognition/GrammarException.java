/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;

/**
 * Thrown if a problem is found with a Java Speech Grammar Format (JSGF)
 * file or with a <code>RuleGrammar</code> object derived from JSGF.
 * <P>
 *
 * Grammar problems are typically identified and fixed during
 * application development.  This class provides information that allows
 * a debugging environment to handle the error.
 * <P>
 *
 * The exception message is a printable string.  Recognizers may
 * optionally provide details of each syntax problem.
 */

public class GrammarException extends SpeechException
{
  /**
   * @serial
   */

  private GrammarSyntaxDetail[] details;


  /**
   * Constructs a <code>GrammarException</code> with no detail message.
   */

  public GrammarException()
  {
    super();
    details = null;
  }


  /**
   * Constructs a <code>GrammarException</code> with the specified detail message.
   * <P>
   *
   * @param s a printable detail message
   */

  public GrammarException(String s)
  {
    super(s);
    this.details = null;
  }


  /**
   * Constructs a <code>GrammarException</code> with the specified detail message
   * and an optional programmatic description of each error.
   * <P>
   *
   * @param s a printable detail message
   * @param detail detail of each error encountered or <code>null</code>
   */

  public GrammarException(String s, GrammarSyntaxDetail[] details)
  {
    super(s);
    this.details = details;
  }


  /**
   * Return the list of grammar syntax problem descriptions.
   */

  public GrammarSyntaxDetail[] getDetails() {return details;}


  /**
   * Set the grammar syntax problem descriptions.
   */

  public void setDetails(GrammarSyntaxDetail[] details) {this.details = details;}


  /**
   * Add a syntax error description (appended to the existing array of details).
   */

  public void addDetail(GrammarSyntaxDetail detail)
    {
      GrammarSyntaxDetail[] tmp = details;
      if (tmp == null)
	tmp = new GrammarSyntaxDetail[0];

      this.details = new GrammarSyntaxDetail[tmp.length + 1];
      System.arraycopy(tmp, 0, this.details, 0, tmp.length);

      this.details[tmp.length] = detail;
    }
}

