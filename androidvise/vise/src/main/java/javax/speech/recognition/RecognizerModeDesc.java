/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.util.*;


/**
 * <code>RecognizerModeDesc</code> extends the <code>EngineModeDesc</code>
 * with properties that are specific to speech recognizers.  
 * A <code>RecognizerModeDesc</code> inherits engine name,
 * mode name and <code>Locale</code> from <code>EngineModeDesc</code>.
 * <code>RecognizerModeDesc</code> adds three features:
 * <P>
 *
 * <UL>
 *  <LI>A <code>Boolean</code> indicating whether dictation is supported
 *  <LI>List of the recognizer's speaker profiles 
 *  <LI>Speaker profile that  will be loaded when
 *      a recognizer is started (not used in selection)
 * </UL>
 * <P>
 *
 * Like <code>EngineModeDesc</code>, there are two types of
 * <code>RecognizerModeDesc</code>: those created by an application
 * for use in engine selection, and those created by an
 * engine which descibe a particular mode of operation of the engine.
 * Descriptors provided by engines are obtained through the
 * <code>availableRecognizers</code> method of the <code>Central</code>
 * class.  These descriptors must have all their features
 * defined.  A descriptor created by an application may make
 * any or all of the features <code>null</code> to indictae a "don't care"
 * value (null features are ignored in engine selection).
 * <P>
 *
 * [Note: the <code>Boolean</code> <em>"is running"</em> feature
 * is a full object rather than a <code>boolean</code> primitive
 * so that it can represent true, false and "don't care".]
 * <P>
 *
 * Applications can modify application-created descriptors in
 * any way.  Applications should never modify the features of
 * a <code>RecognizerModeDesc</code> provided by an engine 
 * (i.e. returned by the <code>availableRecognizers</code> 
 * method).
 * <P>
 * 
 * Engine creation is described in the documentation for the
 * <code>Central</code> class.
 * <P>
 *
 * @see Central
 * @see SpeakerManager
 * @see SpeakerProfile
 */


public class RecognizerModeDesc extends EngineModeDesc
{
  private Boolean          dictationGrammarSupported;
  private SpeakerProfile[] profiles;


  /**
   * Construct a descriptor with <code>null</code> values ("don't care")
   * for all recognizer features.
   */

  public RecognizerModeDesc()
    {
      super();
      dictationGrammarSupported = null;
      profiles = null;
    }


  /**
   * Create a <code>RecognizerModeDesc</code> given a <code>Locale</code>
   * and the dictation flag.  The speaker profiles array
   * and other features are all <code>null</code>.
   */

  public RecognizerModeDesc(Locale locale, Boolean dictationGrammarSupported)
    {
      super(locale);
      this.dictationGrammarSupported = dictationGrammarSupported;
      profiles = null;
    }


  /**
   * Create a fully-specified descriptor.
   */

  public RecognizerModeDesc(String engineName, String modeName, Locale locale, Boolean running,
			    Boolean dictationGrammarSupported,
			    SpeakerProfile[] profiles)
    {
      super(engineName, modeName, locale, running);
      this.dictationGrammarSupported = dictationGrammarSupported;
      this.profiles = profiles;
    }


  /** 
   * Test whether this engine mode provides a <code>DictationGrammar</code>.
   * The value may be <code>TRUE</code>, <code>FALSE</code> or <code>null</code>.
   * A <code>null</code> value means "don't care".
   */

  public Boolean isDictationGrammarSupported() {return dictationGrammarSupported;}


  /**
   * Set the dictationGrammarSupported parameter.
   * The value may be <code>TRUE</code>, <code>FALSE</code> or <code>null</code>.
   * A <code>null</code> value means "don't care".
   */

  public void setDictationGrammarSupported(Boolean dictationGrammarSupported) 
    {this.dictationGrammarSupported = dictationGrammarSupported;}


  /**
   * Returns the list of speaker profiles known to this mode
   * of this recognition engine.  Returns <code>null</code> if speaker training
   * is not supported (<code>SpeakerManager</code> not implemented).  Returns
   * zero-length array if speaker training is supported but no
   * speaker profiles have been constructed yet.
   * <P>
   *
   * The list of speaker profiles is the same as returned by the
   * the <code>listKnownSpeakers</code> method of <code>SpeakerManager</code>
   * if this engine is running.
   * <P>
   *
   * @see SpeakerManager#listKnownSpeakers
   * @exception SecurityException
   *   if the application does not have <code>accessSpeakerProfiles</code> permission
   */

  public final SpeakerProfile[] getSpeakerProfiles()
    throws SecurityException
  {
    // Check security permission
    // try {
    //   SpeechPermission perm = new SpeechPermission("accessSpeakerProfiles");
    //   java.security.AccessController.checkPermission(perm);
    // }
    // catch (NoClassDefFoundError e) {
    //   e.printStackTrace();
    // }
    // If we reach here we have "accessSpeakerProfiles" permission

    return getSpeakerProfilesImpl();
  }


  /**
   * Version of <code>getSpeakerProfiles</code> that performs the
   * operation.  This method can be overridden in 
   * sub-classes.  However, application can only call
   * the <code>getSpeakerProfiles</code> method which
   * does a security check.
   * <P>
   *
   * @see #getSpeakerProfiles
   */

  protected SpeakerProfile[] getSpeakerProfilesImpl() {
    return profiles;
  }


  /**
   * Set the list of speaker profiles.
   * May be <code>null</code>.
   */

  public void setSpeakerProfiles(SpeakerProfile[] speakers) {this.profiles = profiles;}


  /**
   * Add a speaker profile to the <code>SpeakerProfile</code> array.
   */

  public synchronized void addSpeakerProfile(SpeakerProfile profile) 
    {
      if (profiles == null)
	{
	  profiles = new SpeakerProfile[1];
	  profiles[0] = profile;
	  return;
	}
      
      SpeakerProfile newProfiles[] = new SpeakerProfile[profiles.length+1];

      System.arraycopy(profiles, 0, newProfiles, 0, profiles.length);
      newProfiles[newProfiles.length-1] = profile;
      profiles = newProfiles;
    }


  /**
   * Determine whether a <code>RecognizerModeDesc</code> has all the features
   * defined in the <code>require</code> object.  Strings in
   * <code>require</code> which are either <code>null</code> or
   * zero-length ("") are not tested, including those in the
   * <code>Locale</code>.  All string comparisons are exact (case-sensitive).
   * <P>
   *
   * The parameters are used as follows:
   * <P>
   *
   * <UL>
   *   <LI> If the require parameter is an EngineModeDesc then
   *        only the <code>EngineModeDesc</code> features are tested
   *        (engine name, mode name, locale).
   *   <LI> If the require parameter is a <code>RecognizerModeDesc</code> (or sub-class) then
   *        the grammar supported flags and required speakers are tested as follows.
   *   <UL>
   *     <LI> Every speaker profile in the required set must be matched by 
   *          a profile in the tested object.  A <code>null</code> require speakers
   *          value is ignored.
   *     <LI> Match dictation supported <code>Boolean</code> value if 
   *          the required value is <code>null</code> or if exact boolean match.
   *   </UL>
   * </UL>
   * <P>
   */

  public boolean match(EngineModeDesc require)
    {
      //
      // Test EngineModeDesc features.
      //

      if (super.match(require) == false)
	return false;


      //
      //  If require is only an EngineModeDesc object then
      //  don't test additional RecognizerModeDesc features.
      //

      if (!(require instanceof RecognizerModeDesc))
	return true;


      //
      // Now test recognizer features.
      //

      RecognizerModeDesc r = (RecognizerModeDesc)require;

      if (r.dictationGrammarSupported != null)
	if (!r.dictationGrammarSupported.equals(dictationGrammarSupported))
	  return false;


      //
      // Each profile in the require object must be matched
      // by a profile in this RecognizerModeDesc object.
      //

      if (r.profiles != null && r.profiles.length > 0)
	{
	  if (profiles == null) return false;

	  for (int i = 0; i < r.profiles.length; i++)
	    {
	      boolean isMatched = false;
	      
	      for (int j = 0; !isMatched && j < profiles.length; j++)
		{
		  if (profiles[j].match(r.profiles[i]))
		    isMatched = true;
		}
		  
	      if (!isMatched)
		return false;
	    }
	}

      return true;
    }


  /**
   * True if and only if the input parameter is not <CODE>null</CODE>
   * and is a <code>RecognizerModeDesc</code> with equal values of
   * dictationGrammarSupported and all speaker profiles.
   */

  public boolean equals(Object anObject) {
    if ((anObject == null) || !(anObject instanceof RecognizerModeDesc))
      return false;

    RecognizerModeDesc anotherDesc = (RecognizerModeDesc)anObject;

    if (!super.equals(anObject))
      return false;

    if ((dictationGrammarSupported == null) != (anotherDesc.dictationGrammarSupported == null))
      return false;
    else if (dictationGrammarSupported != null)
      if (!dictationGrammarSupported.equals(anotherDesc.dictationGrammarSupported))
	return false;

    if ((profiles == null) != (anotherDesc.profiles == null))
      return false;

    if (profiles != null) {
      if (profiles.length != anotherDesc.profiles.length)
	return false;

      for (int i = 0; i < profiles.length; i++)
	if (profiles[i] == null) {
	  if (anotherDesc.profiles[i] != null)
	    return false;
	}
	else {
	  if (!profiles[i].equals(anotherDesc.profiles[i]))
	    return false;
	}
    }

    return true;
  }
}

