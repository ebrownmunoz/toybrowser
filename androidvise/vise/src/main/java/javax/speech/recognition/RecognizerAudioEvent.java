/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Event issued to indicate detection of speech in the incoming
 * audio stream or to periodically indicate the audio input level.
 * <code>RecognizerAudioEvents</code> are issued to each
 * <code>RecognizerAudioListener</code> attached to the
 * <code>AudioManager</code> for a <code>Recognizer</code>.
 * <P>
 *
 * <code>RecognizerAudioListener</code> events are timed 
 * with the input audio.  In most architectures this
 * indicates that the events occur in near to real time.
 * <P>
 *
 * <A NAME="startStop"></A>
 * <B><code>SPEECH_STARTED</code> and <code>SPEECH_STOPPED</code></B>
 * <P>
 *
 * The <code>SPEECH_STARTED</code> and <code>SPEECH_STOPPED</code>
 * events are used to indicate possible detection of speech 
 * in the incoming audio.  Applications may use this event to
 * display visual feedback to a user indicating that the recognizer 
 * is listening - for example, by maintaining a icon indicating
 * microphone status.
 * <P>
 *
 * It is sometimes difficult to quickly distinguish between
 * speech and other noises (e.g. coughs, microphone bumps), 
 * so the <code>SPEECH_STARTED</code> event is not always
 * followed by a <code>Result</code>.
 * <P>
 *
 * If a <code>RESULT_CREATED</code> is issued for the detected speech,
 * it will usually occur soon after the <code>SPEECH_STARTED</code>
 * event but may be delayed for a number of reasons.
 * <P>
 *
 * <UL>
 *  <LI>
 *   The recognizer may be slower than real time and lag audio input.
 *
 *  <LI>
 *   The recognizer may defer issuing a <code>RESULT_CREATED</code> 
 *   until it is confident that it has detected speech that matches
 *   one of the active grammars - in some cases the <code>RESULT_CREATED</code>
 *   may be issued at the end of the spoken sentence.
 *
 *  <LI>
 *   The recognizer may be delayed because it is in the
 *   <code>SUSPENDED</code> state causing it to buffer audio
 *   and "catch up" once it returns to the <code>LISTENING</code> state.
 *
 *  <LI>
 *   Many other reasons.
 * </UL>
 * <P>
 *
 * Some recognizers will allow a user to speak more than one commands 
 * without a break.  In these cases, a single <code>SPEECH_STARTED</code>
 * event may be followed by more than one <code>RESULT_CREATED</code> event
 * and result finalization before the <code>SPEECH_STOPPED</code> event
 * is issued.
 * <P>
 *
 * In longer speech (e.g. dictating a paragraph), short pauses in
 * the user's speech may lead to a <code>SPEECH_STOPPED</code> event
 * followed by a <code>SPEECH_STARTED</code> event as the user resumes
 * speaking.  These events do not always indicate that the current
 * result will be finalized.
 * <P>
 *
 * In short, applications should treat the <code>SPEECH_STARTED</code>
 * and <code>SPEECH_STOPPED</code> events as operating entirely 
 * independently from the result system of a <code>Recognizer</code>.
 * <P>
 *
 * <A NAME="level"></A>
 * <B>Audio Level Events</B>
 * <P>
 *
 * An <code>AUDIO_LEVEL</code> event indicates a change in the volume of the 
 * audio input to a recognizer.  The level is defined on a scale from
 * 0 to 1.  0.0 represents silence.  0.25 represents quiet input.
 * 0.75 represents loud input. 1.0 indicates the maximum level.
 * <P>
 *
 * Audio level events are suitable for display of a visual "VU meter" 
 * (like the bar on stereo systems which goes up and down with the volume).
 * Different colors are often used to indicate the different levels:
 * for example, red for loud, green for normal, and blue for background.
 * <P>
 * 
 * Maintaining audio quality is important for reliable recognition.
 * A common problem is a user speaking too loudly or too quietly.
 * The color on a VU meter is one way to provide feedback to the user.
 * Note that a quiet level (below 0.25) does not necessarily indicate
 * that the user is speaking too quietly.  The input is also quiet
 * when the user is not speaking.
 * <P>
 *
 * @see Recognizer
 * @see AudioManager
 * @see RecognizerAudioListener 
 * @see ResultEvent#RESULT_CREATED
 */

public class RecognizerAudioEvent extends AudioEvent
{
  /**
   * The recognizer has detected the possible start of speech 
   * in the incoming audio.  Applications may use this event to
   * display visual feedback to a user indicating that the recognizer 
   * is listening.
   * <P>
   *
   * A <A HREF="#startStop">detailed description of <code>SPEECH_STARTED</code> and
   * <code>SPEECH_STOPPED</code> events</A> is provided above.
   * <P>
   *
   * @see RecognizerAudioListener#speechStarted
   * @see EngineEvent#getId
   * @see #SPEECH_STOPPED
   */

  public static final int SPEECH_STARTED = 1100;


  /**
   * The recognizer has detected the end of speech or noise
   * in the incoming audio that it previously indicated by a
   * <code>SPEECH_STARTED</code> event.
   * This event always follows a <code>SPEECH_STARTED</code> event.
   * <P>
   *
   * A <A HREF="#startStop">detailed description of <code>SPEECH_STARTED</code> and
   * <code>SPEECH_STOPPED</code> events</A> is provided above.
   * <P>
   *
   * @see RecognizerAudioListener#speechStopped
   * @see #SPEECH_STARTED
   * @see EngineEvent#getId
   */

  public static final int SPEECH_STOPPED = 1101;


  /**
   * <code>AUDIO_LEVEL</code> event indicates a change in the
   * volume level of the incoming audio.
   * A <A HREF="#level">detailed description of the <code>AUDIO_LEVEL</code> 
   * event</A> is provided above.
   * <P>
   *
   * @see RecognizerAudioListener#audioLevel
   */

  public static final int AUDIO_LEVEL = 1102;


  /**
   * Audio level defined on a scale from 0 to 1.  
   * <P>
   *
   * @see #getAudioLevel
   * @serial
   */

  protected float audioLevel = 0.0f;


  /**
   * Constructs an <code>RecognizerAudioEvent</code> with a specified
   * event identifier and audio level.
   * The <code>audioLevel</code> should be <code>0.0</code>
   * for <code>SPEECH_STARTED</code> and <code>SPEECH_STOPPED</code>
   * events.
   *
   * @param source
   *   the <code>Recognizer</code> that issued the event
   * @param id
   *   the identifier for the event type
   * @param audioLevel
   *   the audio level for this event
   */

  public RecognizerAudioEvent(Recognizer source, int id, float audioLevel)
  {
    super(source, id);

    this.audioLevel = audioLevel;
  }


  /**
   * Constructs an <code>RecognizerAudioEvent</code> with a specified
   * event identifier.
   * The <code>audioLevel</code> is set to <code>0.0</code>.
   *
   * @param source
   *   the <code>Recognizer</code> that issued the event
   * @param id
   *   the identifier for the event type
   */

  public RecognizerAudioEvent(Recognizer source, int id)
  {
    super(source, id);

    this.audioLevel = 0.0f;
  }

 
  /**
   * Get the audio input level in the range 0 to 1.
   * A value below 0.25 indicates quiet input with 0.0 being silence.
   * A value above 0.75 indicates loud input with 1.0 indicating the maximum level.
   * <P>
   *
   * The level is provided only for the <code>AUDIO_LEVEL</code> event type.
   * The level should be ignored for <code>SPEECH_STARTED</code>
   * and <code>SPEECH_STOPPED</code> events.
   * <P>
   *
   * @see #AUDIO_LEVEL
   */
 
  public float getAudioLevel()
  {
    return audioLevel;
  }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch(id) {
    case SPEECH_STARTED:
      return "SPEECH_STARTED";
    case SPEECH_STOPPED:
      return "SPEECH_STOPPED";
    case AUDIO_LEVEL:
      return "AUDIO_LEVEL: " + audioLevel;
    default:
      return super.paramString();
    }
  }
}

