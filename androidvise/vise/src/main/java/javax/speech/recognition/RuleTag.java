/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
  * <code>RuleTag</code> attaches a tag to a contained <code>Rule</code> object.  
  * A tag is a string attached to any <code>Rule</code> entity.  The tag
  * does not affect the recognition of a <code>RuleGrammar</code> in which
  * it is used.  Instead tags are used to embed information into a grammar
  * that helps with processing of recognition results.
  * Tags are
  *
  * <UL>
  *   <LI>Used in the definition of a <code>RuleGrammar</code>, 
  *   <LI>Included in parse output (<code>RuleParse</code> objects).
  * </UL>
  *
  * The tag string in the Java Speech Grammar Format allows the 
  * backslash character to escape the curly brace character '}' or backslash.
  * The <code>RuleTag</code> class assumes that all such string
  * handling is handled separately.  The exception is <code>toString</code>
  * which is required to produce a JSGF-compliant string, and so escapes
  * special characters as required.
  * <PP>
  *
  * An empty tag in JSGF is <code>"{}"</code>.
  * This tag is defined to be the zero-length string,  <code>""</code>.
  * A <code>null</code> tag is converted to a zero-length string.
  * <P>
  *
  * @see Rule
  * @see RuleAlternatives
  * @see RuleCount
  * @see RuleGrammar
  * @see RuleGrammar#parse
  * @see RuleName
  * @see RuleParse
  * @see RuleSequence
  * @see RuleToken
  */


public class RuleTag extends Rule
{
  /**
   * The tagged rule.
   * <P>
   *
   * @see #getRule
   * @serial
   */

  protected Rule rule;


  /**
   * The tag string for the rule.
   * <P>
   *
   * @see #getTag
   * @serial
   */

  protected String tag;


  /**
   * Construct a <code>RuleTag</code> with for <code>Rule</code> object with a tag string.
   * The method assumes that pre-processing of JSGF tags is complete (the leading
   * and trailing curly braces are removed, escape characters are removed).
   * <P>
   *
   * @param rule the rule being tagged
   * @param tag the tag string
   */

  public RuleTag(Rule rule, String tag)
    {
      setRule(rule);
      setTag(tag);
    }


  /**
   * Empty constructor sets the rule and tag to <code>null</code>.
   */

  public RuleTag()
    {
      setRule(null);
      setTag(null);
    }


  /**
   * Returns the <code>Rule</code> object being tagged.
   */

  public Rule getRule() {return rule;}


  /**
   * Set the <code>Rule</code> object to be tagged.
   */

  public void setRule(Rule rule) {this.rule = rule;}


  /**
   * Returns the tag string.
   */

  public String getTag() {return tag;}


  /**
   * Set the tag string for the Rule.
   * A zero-length string is legal.
   * A null tag is converted to "".
   */

  public void setTag(String tag) 
    {
      if (tag == null)
	this.tag = "";
      else
	this.tag = tag;
    }



  /**
   * Return a deep copy of this rule.
   * See the <A HREF="Rule.html#copy()"><code>Rule.copy</code></A>
   * documentation for an explanation of deep copy.
   */

  public Rule copy()
  {
    return new RuleTag(rule.copy(), tag);
  }


  /**
   * Return a <code>String</code> representing the <code>RuleTag</code> object in
   * partial Java Speech Grammar Format.
   * <P>
   *
   * Any backslash or closing angle brackets within the tag will be 
   * properly escaped by a backslash.  If required, the rule contained
   * within the <code>RuleTag</code> will enclosed by parentheses.
   */

  public String toString()
    {
      String outTag = " {" + escapeTag(tag) + "}";

      // RuleToken and RuleName have higher precedence than RuleTag
      // so there's no need to use ().
      // RuleCount, RuleTag require () because of single unary operator
      // per expansion condition.
      // RuleSequence, RuleAlternatives, RuleParse are lower precedence so need ()

      if ((rule instanceof RuleToken) || (rule instanceof RuleName))
	return rule.toString() + outTag;
      else
	return "(" + rule.toString() + ")" + outTag;
    }


  private String escapeTag(String tag) {
    StringBuffer buf = new StringBuffer(tag);

    if (tag.indexOf('}') >= 0 || tag.indexOf('\\') >= 0) {
      for (int i = buf.length()-1; i >= 0; i--) {
	char ch = buf.charAt(i);
	if (ch == '}' || ch == '\\')
	  buf.insert(i, '\\');
      }
    }

    return buf.toString();
  }
}

