/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Provides access to the dictation capabilities of a 
 * <code>Recognizer</code>.  If a recognizer supports dictation, 
 * it provides a <code>DictationGrammar</code> which is obtained 
 * through the <code>getDictationGrammar</code> method of the
 * <code>Recognizer</code>.
 * <P>
 *
 * A <code>DictationGrammar</code> is named with the same
 * convention as a <code>RuleGrammar</code> and will typically
 * reflect the language and domain it supports.  The grammar
 * name is obtained through the <code>Grammar.getName</code>
 * method.  For example, the general general dictation for
 * US English from Acme speech company might be called:
 *
 * <PRE>      com.acme.dictation.english.us.general</PRE>
 *
 * A <code>DictationGrammar</code> is characterized by:
 *
 *   <UL>
 *     <LI>Typically large vocabulary.
 *     <LI>Grammar is built-into the recognizer,
 *     <LI>General purpose or domain-specific (e.g. legal, radiology, medical),
 *     <LI>May support continuous or discrete speech,
 *   </UL>
 *
 * A dictation grammar is built into a recognizer (if supported).
 * Moreover, a recognizer provides a single dictation grammar.
 * Applications cannot create or directly modify the grammar beyond
 * the relatively simple methods provided by this interface.  By comparison,
 * an application can change any part of a <code>RuleGrammar</code>.
 * (Some vendors provide tools for constructing dictation grammars, 
 * but these tools operate separate from the Java Speech API.)
 * <P>
 *
 * A recognizer that supports dictation:
 *
 *   <UL>
 *     <LI>Returns <code>true</code> for the 
 *         <code>RecognizerModeDesc.isDictationGrammarSupported</code> method
 *         This value can be used to select a dictation recognizer.
 *     <LI>Typically resource intensive (CPU, disk, memory),
 *     <LI>Often requires training by user 
 *         (supports the <code>SpeakerManager</code> interface).
 *   </UL>
 *
 * <B>DictationGrammar Extends Grammar</B>
 * <P>
 *
 * The <code>DictationGrammar</code> interface extends the 
 * <code>Grammar</code> interface.  Thus, a <code>DictationGrammar</code> 
 * provides all the <code>Grammar</code> functionality:
 *
 * <UL>
 *  <LI>The dictation grammar name is returned by the 
 *      <code>Grammar.getName</code> method.
 *  <LI>The dictation grammar is enabled and disabled for recognition 
 *      by the <code>setEnabled</code> method inherited from <code>Grammar</code>.
 *  <LI>The activation mode is set through the
 *      <code>setActivationMode</code> method of <code>Grammar</code>.
 *      Note: a <code>DictationGrammar</code> should <em>never</em>
 *      have <code>GLOBAL</code> activation mode.
 *  <LI>The current activation state of the <code>Grammar</code>
 *      is tested by the <code>isActive</code> method and the
 *      <code>GRAMMAR_ACTIVATED</code> and
 *      <code>GRAMMAR_DEACTIVATED</code> events.
 *  <LI>Grammar listeners are attached and removed by the 
 *      <code>Grammar.addGrammarListener</code> and 
 *      <code>Grammar.removeGrammarListener</code> methods.
 * </UL>
 * 
 * <B><A NAME="context">Context</a></B>
 * <P>
 *
 * Dictation recognizers can use the current textual context to improve
 * recognition accuracy.  Applications should use either of the 
 * <code>setContext</code> methods to inform the recognizer each time
 * the context changes.  For example, when editing <em>this</em> 
 * sentence with the cursor after the word "sentence", the preceding context is 
 * "When editing this sentence" and the following context is "with the cursor after...".
 * Any time the text context changes, the application should inform the recognizer.
 * <P>
 *
 * Applications should provide a minimum of 3 or 4 words of context (when it's
 * available).  Different recognizers process context differently. 
 * Some recognizers can take advantage of several paragraphs of context
 * others look only at a few words.
 * <P>
 *
 * One form of <code>setContext</code> takes one string each for preceding
 * and following context (as in the example above).  The other form of
 * <code>setContext</code> takes an array each of strings for preceding and 
 * following context.  Arrays should be provided if the surrounding context
 * is made of tokens from previous recognition results.
 * When possible, providing tokens is the preferred method 
 * because recognizers are able to use token information more
 * reliably and more efficiently.
 *
 * <P>
 * <B>Word Lists</B>
 * <P>
 *
 * Words can be added to and removed from the <code>DictationGrammar</code> 
 * using the <code>addWord</code> and <code>removeWord</code> methods.  
 * Lists of the added and removed words are available through the 
 * <code>listAddedWords</code> and <code>listRemovedWords</code> methods.
 * In a speaker-adaptive system (<code>SpeakerManager</code> supported) 
 * word data is stored as part of the speaker's data.
 * <P>
 *
 * Adding a word allows a recognizer to learn new words.  Removing
 * a word is useful when a recognizer consistently misrecognizes
 * similar sounding words.  For example, if each time a user says
 * "grammar", the recognizer hears "grandma", the user can remove
 * grandma (assuming they don't want to use the word "grandma").
 * <P>
 *
 * @see SpeakerManager
 * @see Recognizer
 * @see Grammar
 */


public interface DictationGrammar extends Grammar
{
  /**
   * Provide the recognition engine with the current textual context.
   * Dictation recognizers use the context information to improve 
   * recognition accuracy.  (Context is <A HREF="#context">discussed above.</A>)
   * <P>
   *
   * When dictating a sequence of words, the recognizer updates its
   * context.  The app does not need to inform the recognizer when
   * results arrive.  Instead it should call <code>setContext</code> for
   * events such as cursor movement, cut, paste etc.
   * <P>
   *
   * The preceding or following context may be <code>null</code>
   * if there is no preceding or following context.  This is appropriate
   * for a new document or in situations where context is not clear.
   * <P>
   *
   * The alternative <code>setContext</code>, that accepts 
   * arrays of tokens for context, should be used when the 
   * current context includes tokens from previous results.
   * <P>
   *
   * @see #setContext(String[], String[])
   */

  public void setContext(String preceding, String following);


  /**
   * Provide the recognition engine with the current textual context
   * with arrays of the previous and following tokens.  Dictation
   * recognizers use the context information to improve recognition
   * accuracy.  (Context is <A HREF="#context">discussed above.</A>)
   * <P>
   *
   * When dictating a sequence of words, the recognizer updates its
   * context.  The app does not need to inform the recognizer when
   * results arrive.  Instead it should call <code>setContext</code> for
   * events such as cursor movement, cut, paste etc.
   * <P>
   *
   * The preceding or following context may be <code>null</code>
   * if there is no preceding or following context.  This is appropriate
   * for a new document or in situations where context is not clear.
   * <P>
   *
   * @see #setContext(String, String)
   */

  public void setContext(String preceding[], String following[]);


  /**
   * Add a word to the <code>DictationGrammar</code>.  
   * The <code>addWord</code> method can undo
   * the effects of an <code>removeWord</code> call.
   * <P>
   *
   * A change in a <code>DictationGrammar</code> is applied
   * to the recognition process only after 
   * <A HREF="Grammar.html#commit">changes have been committed</A>.
   * <P>
   *
   * @see Recognizer#commitChanges
   */

  public void addWord(String word);


  /**
   * Remove a word from the <code>DictationGrammar</code>.  
   * The <code>removeWord</code> method can undo
   * the effects of an <code>addWord</code> call.
   * <P>
   *
   * A change in a <code>DictationGrammar</code> is applied
   * to the recognition process only after 
   * <A HREF="Grammar.html#commit">changes have been committed</A>.
   * <P>
   *
   * @see Recognizer#commitChanges
   */

  public void removeWord(String word);


  /**
   * List the words that have been added to the <code>DictationGrammar</code>.
   */

  public String[] listAddedWords();


  /**
   * List the words that have been removed from the <code>DictationGrammar</code>.
   */

  public String[] listRemovedWords();
}
