/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.util.*;
import java.io.Serializable;


/**
  * A <code>Rule</code> object is the basic component of a
  * <code>RuleGrammar</code> and represents anything that may appear 
  * on the right-hand side of a rule definition in
  * Java Speech Grammar Format.  Technically a <code>Rule</code> 
  * represents a JSGF "expansion".
  * <P>
  *
  * <code>Rule</code> is an abstract class that is sub-classed by:
  * <UL>
  *   <LI><code>RuleAlternatives</code>: set of alternatives <code>Rule</code> objects
  *   <LI><code>RuleCount</code>: contains a <code>Rule</code> that may occur optionally, 
  *       zero or more times, or one or more times.
  *   <LI><code>RuleName</code>: reference to a <code>Rule</code>
  *   <LI><code>RuleSequence</code>: set of rules that occur in sequence
  *   <LI><code>RuleTag</code>: contains a <code>Rule</code> tagged by a <code>String</code>
  *   <LI><code>RuleToken</code>: reference to a token that may be spoken.
  * </UL>
  *
  * Another sub-class of <code>Rule</code> is <code>RuleParse</code> which 
  * is returned by the parse method of <code>RuleGrammar</code> to
  * represent the structure of parsed text.
  * <P>
  *
  * Any <code>Rule</code> object can be converted to a partial 
  * Java Speech Grammar Format String using its <code>toString</code> method.
  * <P>
  *
  * @see RuleAlternatives
  * @see RuleCount
  * @see RuleGrammar
  * @see RuleName
  * @see RuleParse
  * @see RuleSequence
  * @see RuleTag
  * @see RuleToken
  */


public abstract class Rule implements Serializable
{
  /**
   * Return a deep copy of a <code>Rule</code>.
   * A deep copy implies that for a rule that contains
   * other rules (i.e. <code>RuleAlternatives</code>, 
   * <code>RuleCount</code>, <code>RuleParse</code>, 
   * <code>RuleSequence</code>, <code>RuleTag</code>)
   * the sub-rules are also copied.
   * Note: copy differs from the typical use of <code>clone</code>
   * because a clone is not normally a deep copy.
   */

  public abstract Rule copy();


  /**
   * Return a string representing the <code>Rule</code> in
   * partial Java Speech Grammar Format.
   * The <code>String</code> represents a portion of 
   * Java Speech Grammar Format that could appear
   * on the right hand side of a rule definition.
   * <P>
   *
   * @return printable Java Speech Grammar Format string
   */

  public abstract String toString();
}

