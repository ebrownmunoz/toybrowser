/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/** 
  * <code>RuleToken</code> represents speakable text in a <code>RuleGrammar</code>.  
  * It is the primitive type of a <code>Rule</code> (eventually any rule must break 
  * down into a sequence of <code>RuleTokens</code> that may be spoken).  It is also the 
  * primitive type of a <code>RuleParse</code>.
  *
  * @see Rule
  * @see RuleAlternatives
  * @see RuleCount
  * @see RuleGrammar
  * @see RuleName
  * @see RuleParse
  * @see RuleSequence
  * @see RuleTag
  */


public class RuleToken extends Rule
{
  /**
   * The token text.
   * <P>
   *
   * @see #getText
   * @serial
   */

  protected String text;


  /**
   * Construct a <code>RuleToken</code> with the speakable string.
   * The string should not include the surrounding quotes
   * or escapes of JSGF tokens (except as necessary to
   * properly format a Java string).
   */

  public RuleToken(String text) {
    setText(text);
  }


  /**
   * Empty constructor sets token text to <code>null</code>.
   */

  public RuleToken() {
    setText(null);
  }


  /**
   * Get the text of the token.
   * The returned string is not in JSGF format (backslash and quote 
   * characters are not escaped and surrounding quote characters 
   * are not included).
   * Use <code>toString</code> to obtain a JSGF-compliant string.
   * <P>
   *
   * @see #toString
   */

  public String getText() {return text;}


  /**
   * Set the text.
   */

  public void setText(String text) {this.text = text;}

  
  /**
   * Return a deep copy of this rule.
   */

  public Rule copy()
  {
    return new RuleToken(this.text);
  }


  /**
   * Return a string representing the <code>RuleToken</code> in partial 
   * Java Speech Grammar Format.  (It returns a String that could appear 
   * as the right hand side of a rule definition.)  The string is quoted 
   * if necessary (contains whitespace or escaped characters)
   * and the quote and backslash characters are escaped.
   */

  public String toString()
    {
      if (containsWhiteSpace(text) || text.indexOf('\\') >= 0 || text.indexOf('\"') >= 0)
	{
	  StringBuffer buf = new StringBuffer(text);
	  char ch;

	  for (int i = buf.length()-1; i >= 0; i--)
	      if ((ch = buf.charAt(i)) == '\"' || ch == '\\')
		buf.insert(i, '\\');

	  // add leading and trailing quotes
	  buf.insert(0, '\"');
	  buf.append('\"');

	  return buf.toString();
	}
      else return text;
    }


  private boolean containsWhiteSpace(String str)
    {
      for (int i = 0; i < str.length(); i++)
	if (Character.isWhitespace(str.charAt(i)))
	  return true;

      return false;
    }
}

