/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.util.*;


/**
 * Represents the output of a parse of a <code>Result</code> 
 * or a string against a <code>RuleGrammar</code>.  The <code>RuleParse</code>
 * object indicates how the result or text matches to the rules of the
 * <code>RuleGrammar</code> and the rules imported by that grammar.
 * <P>
 *
 * The <code>RuleParse</code> structure returned by parsing closely matches
 * the structure of the grammar it is parsed against: if the grammar
 * contains <code>RuleTag</code>, <code>RuleSequence</code>, <code>RuleToken</code>
 * objects and so on, the returned <code>RuleParse</code> will contain paired objects.
 * <P>
 *
 * The <code>RuleParse</code> object itself represents the match of text to 
 * a named rule or to any rule referenced within a rule.  The rulename field
 * of the <code>RuleParse</code> is the fully-qualified name of the
 * rule being matched.  The <code>Rule</code> field of the <code>RuleParse</code>
 * represents the parse structure (how that rulename is matched).
 * <P>
 *
 * The expansion (or logical structure) of a <code>RuleParse</code> matches
 * the structure of the definition of the rule being parsed.  The following
 * indicates the mapping of an entity in the rule being parsed to the
 * paired object in the <code>RuleParse</code>.
 *
 * <UL>
 *   <LI><code>RuleAlternatives</code>: 
 *       maps to a <code>RuleAlternatives</code> object containing a
 *       single <code>Rule</code> object for the one entity in the 
 *       set of alternatives that is matched.
 *   <LI><code>RuleCount</code>: maps to a <code>RuleSequence</code>
 *       containing a <code>Rule</code> for each match of the rule
 *       contained by <code>RuleCount</code>.  The sequence may contain
 *       zero, one or multiple rule matches (for optional, zero-or-more
 *       or one-or-more operators).
 *   <LI><code>RuleName</code>: maps to a <code>RuleParse</code> 
 *       indicating how the referenced rule was matched.  The rulename
 *       field of the <code>RuleParse</code> is the matched <code>RuleName</code>.
 *       The exception for <code>NULL</code> is described below.
 *   <LI><code>RuleSequence</code>: maps to a <code>RuleSequence</code> with 
 *       a matching rule for each rule in the original sequence.
 *   <LI><code>RuleTag</code>: maps to a matching <code>RuleTag</code>
 *       (same tag) with a match of the contained rule.
 *   <LI><code>RuleToken</code>: maps to an identical <code>RuleToken</code> object.
 * </UL>
 * 
 * [Note: the <code>RuleParse</code> object is never used in defining
 * a <code>RuleGrammar</code> so it doesn't need to be matched.]
 *
 * If a <code>RuleName</code> object in a grammar is <code>&lt;NULL&gt;</code>,
 * then the <code>RuleParse</code> contains the <code>&lt;NULL&gt;</code>
 * object too.
 * <P>
 * 
 * <B>Example</B>
 * <P>
 *
 * Consider a simple grammar:
 *
 * <pre>
 *   public &lt;command&gt; = &lt;action&gt; &lt;object&gt; [&lt;polite&gt;]
 *   &lt;action&gt; = open {OP} | close {CL} | move {MV};
 *   &lt;object&gt; = [&lt;this_that_etc&gt;] (window | door);
 *   &lt;this_that_etc&gt; = a | the | this | that | the current;
 *   &lt;polite&gt; = please | kindly;
 * </pre>
 *
 * <P>
 * We will analyze the parse of <code>"close that door please"</code> against 
 * <code>&lt;command&gt;</code> rule which is returned by the <code>parse</code>
 * method of the <code>RuleGrammar</code> against the <code>&lt;command&gt;</code>
 * rule:
 *
 * <pre>
 *    ruleParse = ruleGrammar.parse("close that door please", "command");
 * </pre>
 *
 * The call returns a <code>RuleParse</code> that is the match of
 * "close that door please" against <code>&lt;command&gt;</code>.
 * <P>
 *
 * Because <code>&lt;command&gt;</code> is defined as a sequence of
 * 3 entities (action, object and optional polite), the <code>RuleParse</code>
 * will contain a <code>RuleSequence</code> with length 3.
 * <P>
 *
 * The first two entities in <code>&lt;command&gt;</code> are
 * <code>RuleNames</code>, so the first two entities in the parse's
 * <code>RuleSequence</code> will be <code>RuleParse</code> objects with
 * rulenames of "action" and "object".
 * <P>
 *
 * The third entity in <code>&lt;command&gt;</code> is an optional
 * <code>RuleName</code> (a <code>RuleCount</code> containing
 * a <code>RuleName</code>), so the third entity in the sequence is a
 * <code>RuleSequence</code> containing a single <code>RuleParse</code>
 * indicating how the <code>&lt;polite&gt;</code> rule is matched.
 * (Recall that a <code>RuleCount</code> object maps to a <code>RuleSequence</code>).
 * <P>
 *
 * The <code>RuleParse</code> for <code>&lt;polite&gt;</code> will contain a
 * <code>RuleAlternatives</code> object with the single entry which is a
 * <code>RuleToken</code> set to "please".  Skipping the rest of the structure,
 * the entire <code>RuleParse</code> object has the following structure.
 *
 * <!-- RAGGED SPACING GETS HTML TO LOOK RIGHT -->
 * <PRE>
 * RuleParse(<em>&lt;command&gt;</em> =                    // Match &lt;command&gt;
 *   RuleSequence(                          //  by a sequence of 3 entities
 *     RuleParse(<em>&lt;action&gt;</em> =                 // First match &lt;action&gt;
 *       RuleAlternatives(                  // One of a set of alternatives
 *         RuleTag(                         // matching the tagged 
 *           RuleToken("<b>close</b>"), "CL")))    //   token "close"
 *     RuleParse(<em>&lt;object&gt;</em> =                 // Now match &lt;object&gt;
 *       RuleSequence(                      //   by a sequence of 2 entities
 *         RuleSequence(                    // RuleCount becomes RuleSequence
 *           RuleParse(<em>&lt;this_that_etc&gt;</em> =    // Match &lt;this_that_etc&gt;
 *             RuleAlternatives(            // One of a set of alternatives
 *               RuleToken("<b>that</b>"))))       //   is the token "that"
 *         RuleAlternatives(                // Match "window | door"
 *           RuleToken("<b>door</b>"))))           //   as the token "door"
 *     RuleSequence(                        // RuleCount becomes RuleSequence
 *       RuleParse(<em>&lt;polite&gt;</em> =               // Now match &lt;polite&gt;
 *         RuleAlternatives(                //   by 1 of 2 alternatives
 *           RuleToken("<b>please</b>"))))         // The token "please"
 *   )
 * )
 * </PRE>
 *
 * (Parse structures are hard to read and understand but can be easily 
 * processed by recursive method calls.)
 * <P>
 *
 * @see Rule
 * @see RuleAlternatives
 * @see RuleCount
 * @see RuleGrammar
 * @see RuleGrammar#parse
 * @see RuleName
 * @see RuleSequence
 * @see RuleTag
 * @see RuleToken
 */


public class RuleParse extends Rule
{
  /**
   * The <code>RuleName</code> matched by the parse structure.
   * <P>
   *
   * @see #getRuleName
   * @serial
   */

  protected RuleName ruleName;


  /**
   * The <code>Rule</code> structure matching the <code>RuleName</code>.
   * <P>
   *
   * @see #getRule
   * @serial
   */

  protected Rule rule;


  /**
   * Construct a <code>RuleParse</code> object for a named rule and 
   * a <code>Rule</code> object that represents the parse structure.
   * The structure of the rule object is described above.
   * The rulename should be a fully-qualified name.
   */

  public RuleParse(RuleName ruleName, Rule rule)
    {
      setRuleName(ruleName);
      setRule(rule);
    }


  /**
   * Empty constructor for <code>RuleParse</code> object with
   * rulename and rule set to <code>null</code>.
   */

  public RuleParse()
    {
      setRuleName(null);
      setRule(null);
    }


  /**
   * Return the matched <code>RuleName</code>.
   * Should be a fully-qualified rulename.
   */

  public RuleName getRuleName() {return ruleName;}



  /**
   * Set the matched <code>RuleName</code>.
   * Should be a fully-qualified rulename.
   */

  public void setRuleName(RuleName ruleName)
    {
      this.ruleName = ruleName;
    }


  /**
   * Return the <code>Rule</code> matched by the <code>RuleName</code>.
   */

  public Rule getRule() {return rule;}



  /**
   * Set the <code>Rule</code> object matched to the <code>RuleName</code>.
   */

  public void setRule(Rule rule)
    {
      this.rule = rule;
    }


  /**
   * List the tags matched in this parse structure.
   * Tags are listed in the order of tokens (from start
   * to end) and from the lowest to highest attachment.
   * (See the <code>FinalRuleResult.getTags</code> method for an example.)
   * <P>
   * 
   * @see FinalRuleResult#getTags
   */

  public String[] getTags()
    {
      Vector vec = new Vector();

      getTags(rule, vec);

      return vectorToStringArray(vec);
    }


  private void getTags(Rule rule, Vector vec)
    {
      if (rule instanceof RuleToken)
	{
	  return;
	}
      else if (rule instanceof RuleParse)
	{
	  getTags(((RuleParse)rule).getRule(), vec);
	  return;
	}
      else if (rule instanceof RuleTag)
	{
	  RuleTag r = (RuleTag)rule;
	  getTags(r.getRule(), vec);
	  vec.addElement(r.tag);
	  return;
	}
      else if (rule instanceof RuleSequence)
	{
	  RuleSequence seq = (RuleSequence)rule;
	  for (int i=0; i < seq.rules.length; i++)
	    getTags(seq.rules[i], vec);
	  return;
	}
      else if (rule instanceof RuleAlternatives)
	{
	  // Should contain a single rule only, but just in case...
	  RuleAlternatives alt = (RuleAlternatives)rule;
	  for (int i=0; i < alt.rules.length; i++)
	    getTags(alt.rules[i], vec);
	  return;
	}
      else if (rule instanceof RuleName)
	{
	  return;
	}
      else
	throw new IllegalArgumentException(rule.getClass().getName() + 
					   " is not a legal object in a RuleParse");
    }


  private String[] vectorToStringArray(Vector vec)
    {
      if (vec == null || vec.size() == 0) return null;

      String[] array = new String[vec.size()];
      for (int i = 0; i < vec.size(); i++)
	array[i] = (String)vec.elementAt(i);

      return array;
    }


  /**
   * Return a deep copy of this rule.
   * See the <A HREF="Rule.html#copy()"><code>Rule.copy</code></A>
   * documentation for an explanation of deep copy.
   */

  public Rule copy()
  {
    RuleName rncopy = null;

    if (ruleName != null)
      rncopy = (RuleName)(ruleName.copy());

    return new RuleParse(rncopy, rule.copy());
  }


  /**
   * Convert a <code>RuleParse</code> to a string with a similar style 
   * to the Java Speech Grammar Format.
   * For example,
   *
   * <PRE>
   *     "(&lt;command&gt; = (&lt;verb&gt; = open) this)"
   * </PRE>
   *
   * Notes:
   * <UL>
   * <LI>The Java Speech Grammar Format does not define a representation
   *     of parse structures.  A similar style is used for familiarity.
   *
   * <LI>A sequence of zero entries can be produced by a parse of a 
   *     <code>RuleCount</code> object.  This is printed as <code>&lt;NULL&gt;</code>.
   *
   * <LI>A <code>RuleAlternatives</code> is parsed to a <code>RuleAlternatives</code> 
   *     containing only one entry.  There is no explicit representation of this
   *     form in JSGF so <code>RuleAlternatives</code> structure is lost when printed.
   * </UL>
   */

  public String toString()
    {
      StringBuffer buf = new StringBuffer("(");

      if (ruleName == null)
 	buf.append("<???>");
      else
	buf.append(ruleName.toString());

      // No need to group the sub-rule.  
      // The format is sufficiently explicit.

      buf.append(" = " + rule.toString() + ')');

      return buf.toString();
    }
}


