/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * A <code>GrammarEvent</code> is issued to each
 * <code>GrammarListener</code> attached to a <code>Grammar</code>
 * when major events associated with that <code>Grammar</code> occur.
 * <P>
 *
 * The source for a <code>GrammarEvent</code> is always a 
 * <code>Grammar</code> object.
 * <P>
 *
 * @see Grammar
 * @see GrammarListener
 */

public class GrammarEvent extends SpeechEvent
{
  /**
   * A <code>GRAMMAR_CHANGES_COMMITTED</code> event is issued when a 
   * <code>Recognizer</code> completes <A HREF="Grammar.html#commit">
   * committing changes</A> to a <code>Grammar</code>.  The event
   * is issued immediately following the <code>CHANGES_COMMITTED</code>
   * event that is issued to <code>RecognizerListeners</code>.  That 
   * event indicates that changes have been applied to all grammars 
   * of a <code>Recognizer</code>.  The <code>GRAMMAR_CHANGES_COMMITTED</code>
   * event is specific to each individual grammar.
   * <P>
   *
   * The event is issued when the definition of the grammar is 
   * changed, when its <code>enabled</code> property is changed, or both.
   * The <code>enabledChanged</code> and <code>definitionChanged</code>
   * flags are set accordingly.
   * <P>
   *
   * A <code>GRAMMAR_CHANGES_COMMITTED</code> event can triggered without
   * an explicit call to <code>commitChanges</code> - there is usually an
   * implicit <code>commitChanges</code> at the completion of 
   * result finalization event processing.  If any syntactic or
   * logical errors are detected for a <code>Grammar</code> during the
   * commit, the generated <code>GrammarException</code> is included with 
   * this event.  If no problem is found the value is <code>null</code>.
   * <P>
   *
   * @see GrammarListener#grammarChangesCommitted
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see Recognizer#commitChanges
   * @see #getDefinitionChanged
   * @see #getEnabledChanged
   * @see #getGrammarException
   */

  public static final int GRAMMAR_CHANGES_COMMITTED = 200;


  /**
   * A <code>GRAMMAR_ACTIVATED</code> event is issued when a
   * grammar changes state from deactivated to activated.  The 
   * <code>isActive</code> method of the <code>Grammar</code>
   * will now return <code>true</code>.
   * <P>
   *
   * Grammar activation changes follow one of two <code>RecognizerEvents</code>:
   * (1) a <code>CHANGES_COMMITTED</code> event in which a grammar's
   * <code>enabled</code> flag is set <code>true</code> or (2) a 
   * <code>FOCUS_GAINED</code> event.  The full details of the
   * <A HREF="Grammar.html#activation">activation conditions</A> under which a
   * <code>Grammar</code> is activated are described in the documentation 
   * for the <code>Grammar</code> interface.  
   * <P>
   *
   * @see GrammarListener#grammarActivated
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see RecognizerEvent#FOCUS_GAINED
   */

  public static final int GRAMMAR_ACTIVATED = 201;


  /**
   * A <code>GRAMMAR_DEACTIVATED</code> event is issued when a
   * grammar changes state from activated to deactivated.  
   * The <code>isActive</code> method of the <code>Grammar</code>
   * will now return <code>false</code>.
   * <P>
   *
   * Grammar deactivation changes follow one of two <code>RecognizerEvents</code>:
   * (1) a <code>CHANGES_COMMITTED</code> event in which a grammar's
   * <code>enabled</code> flag is set <code>false</code> or (2) a 
   * <code>FOCUS_LOST</code> event.  The full details of the
   * <A HREF="Grammar.html#activation">activation conditions</A> under which a
   * <code>Grammar</code> is deactivated are described in the documentation 
   * for the <code>Grammar</code> interface.  
   * <P>
   *
   * @see GrammarListener#grammarDeactivated
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see RecognizerEvent#FOCUS_LOST
   */

  public static final int GRAMMAR_DEACTIVATED = 202;


  /**
   * True if the grammar's <code>enabled</code> property has changed with a 
   * <code>GRAMMAR_CHANGES_COMMITTED</code> event.  False for other
   * event types.
   * <P>
   *
   * @see #getEnabledChanged
   * @serial
   */

  protected boolean enabledChanged;


  /**
   * True if the grammar's definition has changed with a 
   * <code>GRAMMAR_CHANGES_COMMITTED</code> event.  False for other
   * event types.
   * <P>
   *
   * @see #getDefinitionChanged
   * @serial
   */

  protected boolean definitionChanged;


  /**
   * Non-null if any error is detected in a grammar's definition while
   * producing a <code>GRAMMAR_CHANGES_COMMITTED</code> event.  
   * <code>null</code> for other event types.
   * <P>
   *
   * @see #getGrammarException
   * @serial
   */

  protected GrammarException grammarException;


  /**
   * Constructs a <code>GrammarEvent</code> event with a specified
   * event identifier plus state change and exception values.
   * For a <code>GRAMMAR_CHANGES_COMMITTED</code> event, the
   * <code>enabledChanged</code> and <code>definitionChanged</code>
   * parameters should indicate what properties of the <code>Grammar</code>
   * has changed, otherwise they should be <code>false</code>.
   * For a <code>GRAMMAR_CHANGES_COMMITTED</code> event, the
   * <code>grammarException</code> parameter should be non-null
   * only if an error is encountered in the grammar definition.
   * <P>
   *
   * @param source
   *   the object that issued the event
   * @param id
   *   the identifier for the event type
   * @param enabledChanged
   *   <code>true</code> if the grammar's <code>enabled</code> property changed
   * @param definitionChanged
   *   <code>true</code> if the grammar's definition has changed
   * @param grammarException
   *   non-null if an error is detected in a grammar's definition
   */

  public GrammarEvent(Grammar source, int id, 
		      boolean enabledChanged, boolean definitionChanged,
		      GrammarException grammarException)
  {
    super(source, id);

    this.enabledChanged = enabledChanged;
    this.definitionChanged = definitionChanged;
    this.grammarException = grammarException;
  }


  /**
   * Constructs a <code>GrammarEvent</code> event with a specified
   * event identifier.  The <code>enabledChanged</code> and
   * <code>definitionChanged</code> fields are set to <code>false</code>.
   * The <code>grammarException</code> field is set to <code>null</code>.
   * <P>
   *
   * @param source
   *   the object that issued the event
   * @param id
   *   the identifier for the event type
   */

  public GrammarEvent(Grammar source, int id)
  {
    super(source, id);

    this.enabledChanged = false;
    this.definitionChanged = false;
    this.grammarException = null;
  }


  /**
   * Returns <code>true</code> for a <code>GRAMMAR_CHANGES_COMMITTED</code>
   * event if the <code>enabled</code> property of the <code>Grammar</code> changed.
   */

  public boolean getEnabledChanged() { return enabledChanged; }


  /**
   * Returns <code>true</code> for a <code>GRAMMAR_CHANGES_COMMITTED</code>
   * event if the definition of the source <code>Grammar</code>
   * has changed.
   */

  public boolean getDefinitionChanged() { return definitionChanged; }


  /**
   * Returns non-null for a <code>GRAMMAR_CHANGES_COMMITTED</code>
   * event if an error is found in the grammar definition.
   */

  public GrammarException getGrammarException() { return grammarException; }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch(id) {
    case GRAMMAR_CHANGES_COMMITTED:
      StringBuffer s = new StringBuffer();
      s.append("GRAMMAR_CHANGES_COMMITTED");
      if (enabledChanged) s.append(": enabledChanged");
      if (definitionChanged) s.append(": definitionChanged");
      if (grammarException != null) s.append(": " + grammarException.getMessage());
      return s.toString();
    case GRAMMAR_ACTIVATED:
      return "GRAMMAR_ACTIVATED";
    case GRAMMAR_DEACTIVATED:
      return "GRAMMAR_DEACTIVATED";
    default:
      return super.paramString();
    }
  }
}

