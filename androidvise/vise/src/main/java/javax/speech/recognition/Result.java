/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * A <code>Result</code> is issued by a <code>Recognizer</code> as it recognizes 
 * an incoming utterance that matches an active <code>Grammar</code>.
 * The <code>Result</code> interface provides the application with access
 * to the following information about a recognized utterance:
 * <P>
 * 
 * <OL>
 *  <LI>A sequence of finalized tokens (words) that have been recognized,
 *  <LI>A sequence of unfinalized tokens,
 *  <LI>Reference to the grammar matched by the result,
 *  <LI>The result state: <code>UNFINALIZED</code>, 
 *      <code>ACCEPTED</code> or <code>REJECTED</code>.
 * </OL>
 * <P>
 *
 * <B>Multiple Result Interfaces</B>
 * <P>
 *
 * <strong>
 * Every <code>Result</code> object provided by a <code>Recognizer</code> implements
 * both the <code>FinalRuleResult</code> and <code>FinalDictationResult</code>
 * interfaces.  Thus, by extension every result also implements the 
 * <code>FinalResult</code> and <code>Result</code> interfaces.
 * </strong>
 * <P>
 *
 * These multiple interfaces are designed to explicitly indicate (a) what 
 * information is available at what times in the result life-cycle and
 * (b) what information is available for different types of results.
 * Appropriate casting of results allows compile-time checking of 
 * result-handling code and fewer bugs.
 * <P>
 *
 * The <code>FinalResult</code> extends the <code>Result</code> interface.
 * It provides access to the additional information about a result
 * that is available once it has been finalized (once it is in either
 * of the <code>ACCEPTED</code> or <code>REJECTED</code> states).
 * Calling any method of the <code>FinalResult</code> interface for a 
 * result in the <code>UNFINALIZED</code> state causes a
 * <code>ResultStateError</code> to be thrown.
 * <P>
 *
 * The <code>FinalRuleResult</code> extends the <code>FinalResult</code>
 * interface.  It provides access to the additional information about
 * a finalized result that matches a <code>RuleGrammar</code>.  Calling
 * any method of the <code>FinalRuleResult</code> interface for a 
 * non-finalized result or a result that matches a <code>DictationGrammar</code>
 * causes a <code>ResultStateError</code> to be thrown.
 * <P>
 *
 * The <code>FinalDictationResult</code> also extends the <code>FinalResult</code>
 * interface.  It provides access to the additional information about
 * a finalized result that matches a <code>DictationGrammar</code>.  Calling
 * any method of the <code>FinalDictationResult</code> interface for a 
 * non-finalized result or a result that matches a <code>RuleGrammar</code>
 * causes a <code>ResultStateError</code> to be thrown.
 * <P>
 *
 * Note: every result implements both the <code>FinalRuleResult</code>
 * and <code>FinalDictationResult</code> interfaces even though the result
 * will match either a <code>RuleGrammar</code> or <code>DictationGrammar</code>,
 * but <em>never both</em>.  The reason for this is that when the result is 
 * created (<code>RESULT_CREATED</code> event), the grammar is not always known.
 *
 * <P>
 * <B><A NAME="states">Result States</A></B>
 * <P>
 *
 * The separate interfaces determine what information is available for a result 
 * in the different stages of its life-cycle.  The state of a <code>Result</code>
 * is determined by calling the <code>getResultState</code> method.  The
 * three possible states are <code>UNFINALIZED</code>, 
 * <code>ACCEPTED</code> and <code>REJECTED</code>.  
 * <P>
 *
 * A new result starts in the <code>UNFINALIZED</code> state.  When
 * the result is <em>finalized</em> is moves to either the
 * <code>ACCEPTED</code> or <code>REJECTED</code> state.  
 * An accepted or rejected result is termed a <em>finalized</em> result.
 * All values and information regarding a finalized result are fixed
 * (excepting that audio and training information may be released).
 * <P>
 *
 * Following are descriptions of a result object in each of the three states
 * including information on which interfaces can be used in each state.
 * <P>
 *
 * <B><CODE>getResultState() == Result.UNFINALIZED</CODE></B>
 * <P>
 *
 * <UL>
 *  <LI>Recognition of the result is in progress.
 *  <LI>A new result is created with a <code>RESULT_CREATED</code> event 
 *      that is issued to each <code>ResultListener</code> attached
 *      to a <code>Recognizer</code>.  The new result is created in
 *      in the <code>UNFINALIZED</code> state.
 *  <LI>A result remains in the <code>UNFINALIZED</code> state until
 *      it is <em>finalized</em> by either a <code>RESULT_ACCEPTED</code> 
 *      or <code>RESULT_REJECTED</code> event.
 *  <LI>Applications should only call the methods of the <code>Result</code>
 *      interface.  A <code>ResultStateError</code> is issued on calls to the 
 *      methods of <code>FinalResult</code>, <code>FinalRuleResult</code>
 *      and <code>FinalDictationResult</code> interfaces.
 *  <LI>Events 1: zero or more <code>RESULT_UPDATED</code> events 
 *      may be issued as (a) tokens are finalized, or (b) as the
 *      unfinalized tokens changes.
 *  <LI>Events 2: one <code>GRAMMAR_FINALIZED</code> event must be
 *      issued in the <code>UNFINALIZED</code> state before result
 *      finalization by an <code>RESULT_ACCEPTED</code> event.
 *      (Not required if a result is rejected.)
 *  <LI>Events 3: the <code>GRAMMAR_FINALIZED</code> event is optional
 *      if the result is finalized by a <code>RESULT_REJECTED</code>
 *      event.  (It is not always possible for a recognizer to identify
 *      a best-match grammar for a rejected result.)
 *  <LI>Prior to the <code>GRAMMAR_FINALIZED</code> event, the
 *      <code>getGrammar</code> returns <code>null</code>.  
 *      Following the <code>GRAMMAR_FINALIZED</code> event the
 *      <code>getGrammar</code> method returns a non-null reference to
 *      the active <code>Grammar</code> that is matched by this result.  
 *  <LI><code>numTokens</code> returns the number of finalized tokens.
 *      While in the <code>UNFINALIZED</code> this number may increase
 *      as <code>ResultEvent.RESULT_UPDATED</code> events are issued.
 *  <LI>The best guess for each finalized token is available through 
 *      <CODE>getBestToken(int num)</CODE>.  The best guesses are guaranteed
 *      not to change through the remaining life of the result.
 *  <LI><code>getUnfinalizedTokens</code> may return zero or more tokens and
 *      these may change at any time when a <code>ResultEvent.RESULT_UPDATED</code> 
 *      event is issued.
 * </UL>
 * <P>
 *
 * <B><CODE>getResultState() == Result.ACCEPTED</CODE></B>
 * <P>
 *
 * <UL>
 *  <LI>Recognition of the <code>Result</code> is complete and the recognizer
 *      is confident it has the correct result (not a rejected result).
 *      Non-rejection is not a guarantee of a correct result - only sufficient
 *      confidence that the guess is correct.
 *  <LI>Events 1: a result transitions from the <code>UNFINALIZED</code> state to
 *      the <code>ACCEPTED</code> state when an <code>RESULT_ACCEPTED</code> 
 *      event is issued.
 *  <LI>Events 2: <code>AUDIO_RELEASED</code> and <code>TRAINING_INFO_RELEASED</code>
 *      events may occur optionally (once) in the <code>ACCEPTED</code> state.
 *  <LI><code>numTokens</code> will return 1 or greater (there must be at least
 *      one finalized token) and the number of finalized tokens will not change.
 *      [Note: A rejected result may have zero finalized tokens.]
 *  <LI>The best guess for each finalized token is available through the
 *      <CODE>getBestToken(int tokNum)</CODE> method.  The best guesses will
 *      not change through the remaining life of the result.  
 *  <LI><code>getUnfinalizedTokens</code> method returns <code>null</code>.
 *  <LI>The <code>getGrammar</code> method returns the grammar matched by
 *      this result.  It may be either a <code>RuleGrammar</code> or
 *      <code>DictationGrammar</code>.
 *  <LI>For either a <code>RuleGrammar</code> or <code>DictationGrammar</code>
 *      the methods of <code>FinalResult</code> may be used to access
 *      audio data and to perform correction/training.
 *  <LI>If the result matches a <code>RuleGrammar</code>, the methods
 *      of <code>FinalRuleResult</code> may be used to get alternative
 *      guesses for the complete utterance and to get tags and other
 *      information associated with the <code>RuleGrammar</code>.
 *      (Calls to any methods of the <code>FinalDictationResult</code>
 *      interface cause a <code>ResultStateError</code>.)
 *  <LI>If the result matches a <code>DictationGrammar</code>, the methods
 *      of <code>FinalDictationResult</code> may be used to get alternative
 *      guesses for tokens and token sequences.
 *      (Calls to any methods of the <code>FinalRuleResult</code>
 *      interface cause a <code>ResultStateError</code>.)
 * </UL>
 * <P>
 *
 * <B><CODE>getResultState() == Result.REJECTED</CODE></B>
 * <P>
 *
 * <UL>
 *  <LI>Recognition of the <code>Result</code> is complete but the recognizer
 *      believes it does not have the correct result.  Programmatically, 
 *      an accepted and rejected result are very similar but the contents
 *      of a rejected result must be treated differently - they are likely to be wrong.
 *  <LI>Events 1: a result transitions from the <code>UNFINALIZED</code> state to
 *      the <code>REJECTED</code> state when an <code>RESULT_REJECTED</code> 
 *      event is issued.
 *  <LI>Events 2: (same as for the <code>ACCEPTED</code> state)
 *      <code>AUDIO_RELEASED</code> and <code>TRAINING_INFO_RELEASED</code>
 *      events may occur optionally (once) in the <code>REJECTED</code> state.
 *  <LI><code>numTokens</code> will return 0 or greater.
 *      The number of tokens will not change for the remaining life
 *      of the result.  [Note: an accepted result always has at least
 *      one finalized token.]
 *  <LI>As with an accepted result, 
 *      the best guess for each finalized token is available through the
 *      <CODE>getBestToken(int num)</CODE> method and the tokens are guaranteed
 *      not to change through the remaining life of the result.  Because
 *      the result has been rejected the guesses are not likely to be correct.
 *  <LI><code>getUnfinalizedTokens</code> method returns <code>null</code>.
 *  <LI>If the <code>GRAMMAR_FINALIZED</code> was issued during recognition
 *      of the result, the <code>getGrammar</code> method returns the 
 *      grammar matched by this result otherwise it returns <code>null</code>.
 *      It may be either a <code>RuleGrammar</code> or <code>DictationGrammar</code>.
 *      For rejected results, there is a greater chance that this
 *      grammar is wrong.
 *  <LI>The <code>FinalResult</code> interface behaves the same as 
 *      for a result in the <code>ACCEPTED</code> state expect that the information
 *      is less likely to be reliable.
 *  <LI>If the grammar is known, the <code>FinalRuleResult</code> and
 *      <code>FinalDictationResult</code> interfaces behave the same as 
 *      for a result in the <code>ACCEPTED</code> state expect that the information
 *      is less likely to be reliable.  If the grammar is unknown, then
 *      a <code>ResultStateError</code> is thrown on calls to the methods of both
 *      <code>FinalRuleResult</code> and <code>FinalDictationResult</code>.
 * </UL>
 *
 * <P>
 * <B>Result State and Recognizer States</B>
 * <P>
 *
 * The state system of a <code>Recognizer</code> is linked to the
 * state of recognition of the current result.  The <code>Recognizer</code>
 * interface documents the <A HREF="Recognizer.html#normalEvents">normal
 * event cycle</A> for a <code>Recognizer</code> and for <code>Results</code>.
 * The following is an overview of the ways in which the two state
 * systems are linked:
 * <P>
 *
 * <UL>
 *  <LI>The <code>ALLOCATED</code> state of a <code>Recognizer</code> has
 *      three sub-states.  In the <code>LISTENING</code> state, the 
 *      recognizer is listening to background audio and there is
 *      no result being produced.  In the <code>SUSPENDED</code>
 *      state, the recognizer is temporarily buffering audio input
 *      while its grammars are updated.  In the <code>PROCESSING</code>
 *      state, the recognizer has detected incoming audio that 
 *      may match an active grammar and is producing a <code>Result</code>.
 *  <LI>The <code>Recognizer</code> moves from the <code>LISTENING</code>
 *      state to the <code>PROCESSING</code> state with a 
 *      <code>RECOGNIZER_PROCESSING</code> event immediately prior to
 *      issuing a <code>RESULT_CREATED</code> event.
 *  <LI>The <code>RESULT_UPDATED</code> and <code>GRAMMAR_FINALIZED</code>
 *      events are produced while the <code>Recognizer</code> is in
 *      the <code>PROCESSING</code> state.
 *  <LI>The <code>Recognizer</code> finalizes a <code>Result</code> with
 *      <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code>
 *      event immediately after it transitions from the <code>PROCESSING</code> 
 *      state to the <code>SUSPENDED</code> state with a
 *      <code>RECOGNIZER_SUSPENDED</code> event.
 *  <LI>Unless there is a pending <code>suspend</code>, the
 *      <code>Recognizer</code> commits grammar changes with a
 *      <code>CHANGES_COMMITTED</code> event as soon as the 
 *      result finalization event is processed.
 *  <LI>The <code>TRAINING_INFO_RELEASED</code> and <code>AUDIO_RELEASED</code>
 *      events can occur in any state of an <code>ALLOCATED</code> 
 *      <code>Recognizer</code>.
 * </UL>
 *
 * <P>
 * <B>Accept or Reject?</B>
 * <P>
 *
 * Rejection of a result indicates that the recognizer is not confident that it 
 * has accurately recognized what a user says.  Rejection can be controlled
 * through the <code>RecognizerProperties</code> interface with the 
 * <code>setConfidenceLevel</code> method.  Increasing the confidence level
 * requires the recognizer to have greater confident to accept a result, so
 * more results are likely to be rejected.
 * <P>
 *
 * <EM>Important</EM>: the acceptance of a result (an <code>RESULT_ACCEPTED</code> 
 * event rather than a <code>RESULT_REJECTED</code> event) does not mean the 
 * result is correct.  Instead acceptance implies that the recognizer has a 
 * sufficient level of confidence that the result is correct.
 * <P>
 *
 * It is difficult for recognizers to reliably determine when they make mistakes.  
 * Applications need to determine the cost of incorrect recognition of any
 * particular results and take appropriate actions.  For example, confirm
 * with a user that they said "delete all files" before deleting anything.
 *
 * <P>
 * <B><A NAME="events">Result Events</A></B>
 * <P>
 *
 * Events are issued when a new result is created and when there is any change
 * in the state or information content of a result.  The following 
 * describes the event sequence for an accepted result.  It provides the
 * same information as above for result states, but focusses on legal event
 * sequences.
 * <P>
 *
 * Before a new result is created for incoming speech, a recognizer
 * usually issues a 
 * <A HREF="RecognizerAudioEvent.html#SPEECH_STARTED">SPEECH_STARTED</a>
 * event to the
 * <A HREF="RecognizerAudioListener.html#speechStarted(javax.speech.recognition.RecognizerEvent)">
 * speechStarted</a> method of
 * <A HREF="RecognizerAudioListener.html">RecognizerAudioListener</A>.
 * The 
 * <P>
 *
 * A newly created <code>Result</code> is provided to the application
 * by calling the <code>resultCreated</code> method of each <code>ResultListener</code>
 * attached to the <code>Recognizer</code> with a <code>RESULT_CREATED</code>
 * event.  The new result may or may not have any finalized tokens
 * or unfinalized tokens.
 * <P>
 *
 * At any time following the <code>RESULT_CREATED</code> event, an application
 * may attach a <code>ResultListener</code> to an individual result.
 * That listener will receive all subsequent events associated with 
 * that <code>Result</code>.
 * <P>
 *
 * A new <code>Result</code> is created in the <code>UNFINALIZED</code> 
 * state.  In this state, zero or more <code>RESULT_UPDATED</code> events
 * may be issued to each <code>ResultListener</code> attached to the
 * <code>Recognizer</code> and to each <code>ResultListener</code>
 * attached to that <code>Result</code>.  The <code>RESULT_UPDATED</code>
 * indicates that one or more tokens have been finalized, or that 
 * the unfinalized tokens have changed, or both.
 * <P>
 *
 * When the recognizer determines which grammar is the best match for
 * incoming speech, it issues a <code>GRAMMAR_FINALIZED</code> event.
 * This event is issued to each <code>ResultListener</code> attached to the
 * <code>Recognizer</code> and to each <code>ResultListener</code>
 * attached to that <code>Result</code>.
 * <P>
 *
 * The <code>GRAMMAR_FINALIZED</code> event is also issued to each 
 * <code>ResultListener</code> attached to the matched <code>Grammar</code>.
 * This is the first <code>ResultEvent</code> received by 
 * <code>ResultListeners</code> attached to the <code>Grammar</code>.
 * All subsequent result events are issued to all 
 * <code>ResultListeners</code> attached to the matched <code>Grammar</code>
 * (as well as to <code>ResultListeners</code> attached to the
 * <code>Result</code> and to the <code>Recognizer</code>).
 * <P>
 *
 * Zero or more <code>RESULT_UPDATED</code> events may be
 * issued after the <code>GRAMMAR_FINALIZED</code> event but before
 * the result is finalized.
 * <P>
 *
 * Once the recognizer completes recognition of the <code>Result</code>
 * that it choses to accept, it finalizes the result with an 
 * <code>RESULT_ACCEPTED</code> event that is issued to the
 * <code>ResultListeners</code> attached to the <code>Recognizer</code>,
 * the matched <code>Grammar</code>, and the <code>Result</code>.
 * This event may also indicate finalization of zero or more tokens,
 * and/or the reseting of the unfinalized tokens to <code>null</code>.
 * The result finalization event occurs immediately after the
 * <code>Recognizer</code> makes a transition from the <code>PROCESSING</code>
 * state to the <code>SUSPENDED</code> state with a
 * <code>RECOGNIZER_SUSPENDED</code> event.
 * <P>
 *
 * A finalized result (accepted or rejected state) may issue a
 * <code>AUDIO_RELEASED</code> or <code>TRAINING_INFO_RELEASED</code>
 * event.  These events may be issued in response to relevant release
 * methods of <code>FinalResult</code> and <code>FinalDictationResult</code>
 * or may be issued when the recognizer independently determines to
 * release audio or training information.
 * <P>
 *
 * When a result is rejected some of the events described above may be skipped.
 * A result may be rejected with the <code>RESULT_REJECTED</code>
 * event at any time after a <code>RESULT_CREATED</code> event instead
 * of an <code>RESULT_ACCEPTED</code> event.
 * A result may be rejected with or without any unfinalized or finalized 
 * tokens being created (no <code>RESULT_UPDATED</code> events), and
 * with or without a <code>GRAMMAR_FINALIZED</code> event.
 * <P>
 *
 * <B>When does a <code>Result</code> start and end?</B>
 * <P>
 *
 * A new result object is created when a recognizer has detected
 * possible incoming speech which may match an active grammar.  
 * <P>
 *
 * To accept the result (i.e. to issue a <code>RESULT_ACCEPTED</code> event),
 * the best-guess tokens of the result must match the token patterns
 * defined by the matched grammar.  For a  <code>RuleGrammar</code>
 * this implies that a call to the <code>parse</code>
 * method of the matched <code>RuleGrammar</code> must return successfully.
 * (Note: the parse is not guaranteed if the grammar has been changed.)
 * <P>
 *
 * Because there are no programmatically defined constraints upon word 
 * patterns for a <code>DictationGrammar</code>, a single result may
 * represent a single word, a short phrase or sentence, or possibly many
 * pages of text. 
 * <P>
 * 
 * The set of conditions that may cause a result matching a
 * <code>DictationGrammar</code> to be finalized includes:
 * <P>
 *
 * <UL>
 *   <LI> The user pauses for a period of time (a timeout).
 *   <LI> A call to the 
 *        <A HREF="Recognizer.html#forceFinalize(boolean)">forceFinalize</A>
 *        method of the recognizer.
 *   <LI> User has spoken text matching an active <code>RuleGrammar</code>
 *        (the dictation result is finalized and a new <code>Result</code> 
 *        is issued for the <code>RuleGrammar</code>).
 *   <LI> The engine is paused.
 * </UL>
 * <P>
 *
 * The following conditions apply to all finalized results:
 * <P>
 *
 * <UL>
 *  <LI>N-best alternative token guesses available through the 
 *      <code>FinalRuleResult</code> and <code>FinalDictationResult</code>
 *      interfaces cannot cross result boundaries.
 *  <LI>Correction/training is only possible within a single result object.
 * </UL>
 * <P>
 *
 * @see FinalResult
 * @see FinalRuleResult
 * @see FinalDictationResult
 * @see ResultEvent
 * @see ResultListener
 * @see ResultAdapter
 * @see Grammar
 * @see RuleGrammar
 * @see DictationGrammar
 * @see Recognizer#forceFinalize
 * @see RecognizerEvent
 * @see RecognizerProperties#setConfidenceLevel
 */


public interface Result
{
  /**
   * <code>getResultState</code> returns <code>UNFINALIZED</code> while a result 
   * is still being recognized.  A <code>Result</code> is in the
   * <code>UNFINALIZED</code> state when the <code>RESULT_CREATED</code>
   * event is issued.  <A HREF="#states">Result states</A> are
   * described above in detail.
   * <P>
   *
   * @see #getResultState
   * @see ResultEvent#RESULT_CREATED
   */

  public static int UNFINALIZED = 300;


  /**
   * <code>getResultState</code> returns <code>ACCEPTED</code> once 
   * recognition of the result is completed and the <code>Result</code> 
   * object has been finalized by being accepted.  When a <code>Result</code> 
   * changes to the <code>ACCEPTED</code> state a
   * <code>RESULT_ACCEPTED</code> event is issued. 
   * <A HREF="#states">Result states</A> are described above in detail.
   * <P>
   *
   * @see #getResultState
   * @see ResultEvent#RESULT_ACCEPTED
   */

  public static int ACCEPTED = 301;


  /**
   * <code>getResultState</code> returns <code>REJECTED</code> once 
   * recognition of the result complete and the <code>Result</code> object 
   * has been finalized by being rejected.  When a <code>Result</code> 
   * changes to the <code>REJECTED</code> state a
   * <code>RESULT_REJECTED</code> event is issued.
   * <A HREF="#states">Result states</A> are described above in detail.
   * <P>
   *
   * @see #getResultState
   * @see ResultEvent#RESULT_REJECTED
   */

  public static int REJECTED = 302;


  /**
   * Returns the current state of the Result object: <code>UNFINALIZED</code>,
   * <code>ACCEPTED</code> or <code>REJECTED</code>.  The details of
   * a <code>Result</code> in each state are <A HREF="#states">described above</A>.
   * <P>
   *
   * @see #UNFINALIZED
   * @see #ACCEPTED
   * @see #REJECTED
   */

  public int getResultState();


  /**
   * Return the <code>Grammar</code> matched by the best-guess finalized
   * tokens of this result or <code>null</code> if the grammar
   * is not known.  The return value is <code>null</code> before
   * a <code>GRAMMAR_FINALIZED</code> event and non-null afterwards.
   * <P>
   *
   * The grammar is guaranteed to be non-null for an accepted
   * result.  The grammar may be null or non-null for a rejected
   * result, depending upon whether a <code>GRAMMAR_FINALIZED</code>
   * event was issued prior to finalization.
   * <P>
   *
   * For a finalized result, an application should determine the
   * type of matched grammar with an <code>instanceof</code> test.
   * For a result that matches a <code>RuleGrammar</code>, the
   * methods of <code>FinalRuleResult</code> can be used (the
   * methods of <code>FinalDictationResult</code> throw an error).
   * For a result that matches a <code>DictationGrammar</code>, the
   * methods of <code>FinalDictationResult</code> can be used (the
   * methods of <code>FinalRuleResult</code> throw an error).
   * The methods of <code>FinalResult</code> can be used for
   * a result matching either kind of grammar.
   * <P>
   *
   * Example:
   *
   * <pre>
   *    Result result;
   *    if (result.getGrammar() instanceof RuleGrammar) {
   *      FinalRuleResult frr = (FinalRuleResult)result;
   *      ...
   *    }
   * </pre>
   * <P>
   *
   * @see #getResultState
   */

  public Grammar getGrammar();



  /**
   * Returns the number of finalized tokens in a <code>Result</code>.
   * Tokens are numbered from <CODE>0</CODE> to <CODE>numTokens()-1</CODE>
   * and are obtained through the <code>getBestToken</code> and 
   * <code>getBestTokens</code> method of this (<code>Result</code>)
   * interface and the <code>getAlternativeTokens</code> methods
   * of the <code>FinalRuleResult</code> and <code>FinalDictationResult</code>
   * interfaces for a finalized result.
   * <P>
   *
   * Starting from the <code>RESULT_CREATED</code> event and while the
   * result remains in the <code>UNFINALIZED</code> state, the number
   * of finalized tokens may be zero or greater and can increase as 
   * tokens are finalized.  When one or more tokens are finalized
   * in the <code>UNFINALIZED</code> state, a <code>RESULT_UPDATED</code> 
   * event is issued with the <code>tokenFinalized</code> flag set <code>true</code>.
   * The <code>RESULT_ACCEPTED</code> and <code>RESULT_REJECTED</code>
   * events which finalize a result can also indicate that one or
   * more tokens have been finalized.
   * <P>
   *
   * In the <code>ACCEPTED</code> and <code>REJECTED</code> states, 
   * <code>numTokens</code> indicates the total number of tokens that
   * were finalized.  The number of finalized tokens never changes in 
   * these states.  An <code>ACCEPTED</code> result must have 
   * one or more finalized token.  A <code>REJECTED</code> result 
   * may have zero or more tokens.
   * <P>
   *
   * @see ResultEvent#RESULT_UPDATED
   * @see #getBestToken
   * @see #getBestTokens
   * @see FinalRuleResult#getAlternativeTokens
   * @see FinalDictationResult#getAlternativeTokens
   */

  public int numTokens();


  /**
   * Returns the best guess for the <code>tokNum<sup>th</sup></code> token.  
   * <code>tokNum</code> must be in the range <CODE>0</CODE> to
   * <CODE>numTokens()-1</CODE>.  
   * <P>
   *
   * If the result has zero tokens (possible in both the
   * <code>UNFINALIZED</code> and <code>REJECTED</code>
   * states) an exception is thrown.
   * <P>
   *
   * If the result is in the <CODE>REJECTED</CODE> state, 
   * then the returned tokens are likely to be incorrect.  In the 
   * <CODE>ACCEPTED</CODE> state (not rejected) the recognizer 
   * is confident that the tokens are correct but applications should 
   * consider the possibility that the tokens are incorrect.
   * <P>
   *
   * The <code>FinalRuleResult</code> and <code>FinalDictationResult</code>
   * interfaces provide <code>getAlternativeTokens</code> methods that
   * return alternative token guesses for finalized results.
   * <P>
   * 
   * @see #getUnfinalizedTokens
   * @see #getBestTokens
   * @see FinalRuleResult#getAlternativeTokens
   * @see FinalDictationResult#getAlternativeTokens
   * @exception IllegalArgumentException
   *   if <code>tokNum</code> is out of range.
   */

  public ResultToken getBestToken(int tokNum)
    throws IllegalArgumentException;


  /**
   * Returns all the best guess tokens for this result.
   * If the result has zero tokens, the return value is <code>null</code>.
   */

  public ResultToken[] getBestTokens();


  /**
   * In the <code>UNFINALIZED</code> state, return the current guess of 
   * the tokens following the finalized tokens.
   * Unfinalized tokens provide an indication of what a recognizer
   * is considering as possible recognition tokens for speech
   * following the finalized tokens.
   * <P>
   *
.  * Unfinalized tokens can provide users with feedback on the recognition
   * process.  The array may be any length (zero or more tokens),  the
   * length may change at any time, and successive calls to 
   * <code>getUnfinalizedTokens</code> may return different
   * tokens or even different numbers of tokens.  When the
   * unfinalized tokens are changed, a <code>RESULT_UPDATED</code>
   * event is issued to the <code>ResultListener</code>.
   * The <code>RESULT_ACCEPTED</code> and <code>RESULT_REJECTED</code>
   * events finalize a result and always guarantee that the 
   * return value is <code>null</code>.  A new result created
   * with a <code>RESULT_CREATED</code> event may have a null or non-null
   * value.
   * <P>
   *
   * The returned array is null if there are currently no unfinalized tokens,
   * if the recognizer does not support unfinalized tokens,
   * or after a <code>Result</code> is finalized (in the <code>ACCEPTED</code> or
   * <code>REJECTED</code> state).
   * <P>
   *
   * @see ResultEvent#isUnfinalizedTokensChanged
   * @see ResultEvent#RESULT_UPDATED
   * @see ResultEvent#RESULT_ACCEPTED
   * @see ResultEvent#RESULT_REJECTED
   */

  public ResultToken[] getUnfinalizedTokens();


  /* ********************* RESULT LISTENER **************************** */


  /**
   * Request notifications of events of related to this <code>Result</code>.
   * An application can attach multiple listeners to a <code>Result</code>.
   * A listener can be removed with the <code>removeResultListener</code> method.
   * <P>
   *
   * <code>ResultListener</code> objects can also be attached to 
   * a <code>Recognizer</code> and to any <code>Grammar</code>.
   * A listener attached to the <code>Recognizer</code> receives
   * all events for all results produced by that <code>Recognizer</code>.
   * A listener attached to a <code>Grammar</code> receives
   * all events for all results that have been finalized for
   * that <code>Grammar</code> (all events starting with and including
   * the <code>GRAMMAR_FINALIZED</code> event).
   * <P>
   *
   * A <code>ResultListener</code> attached to a <code>Result</code>
   * only receives events following the point in time at which the
   * listener is attached.  Because the listener can only be attached
   * during or after the <code>RESULT_CREATED</code>, it will not receive
   * the <code>RESULT_CREATED</code> event.  Only <code>ResultListeners</code>
   * attached to the <code>Recognizer</code> receive <code>RESULT_CREATED</code>
   * events.
   * <P>
   *
   * @see #removeResultListener
   * @see Recognizer#addResultListener
   * @see Grammar#addResultListener
   */

  public void addResultListener(ResultListener listener);


  /**
   * Remove a listener from this <code>Result</code>.
   * <P>
   *
   * @see #addResultListener
   * @see Recognizer#removeResultListener
   * @see Grammar#removeResultListener
   */

  public void removeResultListener(ResultListener listener);
}

