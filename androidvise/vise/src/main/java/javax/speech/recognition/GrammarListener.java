/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.util.*;


/**
 * A <code>GrammarListener</code> receives notifications 
 * of status change events for a <code>Grammar</code>. 
 * <P>
 *
 * A <code>GrammarListener</code> is attached to and removed
 * from a <code>Grammar</code> with its <code>addGrammarListener</code> 
 * and <code>removeGrammarListener</code> methods.  
 * Multiple grammars can share a <code>GrammarListener</code> object 
 * and one grammar can have multiple
 * <code>GrammarListener</code> objects attached.
 * <P>
 *
 * The <code>GrammarAdapter</code> provides a trivial implementation
 * of this class.
 * <P>
 * 
 * @see Grammar
 * @see DictationGrammar
 * @see RuleGrammar
 * @see Grammar#addGrammarListener
 * @see Grammar#removeGrammarListener
 * @see GrammarAdapter
 */


public interface GrammarListener extends EventListener
{
  /**
   * A <code>GRAMMAR_CHANGES_COMMITTED</code> event is issued when a
   * <code>Recognizer</code> has <A HREF="Grammar.html#commit">
   * committed changes</A> to a <code>Grammar</code>.
   * <P>
   *
   * The <code>GRAMMAR_CHANGES_COMMITTED</code> immediately follows
   * the <code>CHANGES_COMMITTED</code> event issued to 
   * <code>RecognizerListeners</code> after changes to all
   * grammars have been committed.  The circumstances in which
   * <A HREF="Grammar.html#commit">changes are committed</A>
   * are described in the documentation for <code>Grammar</code>.
   * <P>
   *
   * @see GrammarEvent#GRAMMAR_CHANGES_COMMITTED
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see Grammar
   */

  public void grammarChangesCommitted(GrammarEvent e);


  /**
   * A <code>GRAMMAR_ACTIVATED</code> event is issued when a
   * grammar changes from deactivated to activated.
   * <P>
   *
   * @see GrammarEvent#GRAMMAR_ACTIVATED
   * @see Grammar
   */

  public void grammarActivated(GrammarEvent e);


  /**
   * A <code>GRAMMAR_DEACTIVATED</code> event is issued when a
   * grammar changes from activated to deactivated.
   * <P>
   *
   * @see GrammarEvent#GRAMMAR_DEACTIVATED
   * @see Grammar
   */

  public void grammarDeactivated(GrammarEvent e);
}


