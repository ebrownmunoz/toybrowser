/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import java.net.*;
import javax.speech.*;

/**
 * Description of a problem found in a grammar usually bundled
 * with a <code>GrammarException</code>.  The grammar
 * may have been created programmatically or by loading a 
 * Java Speech Grammar Format document.  Multiple
 * <code>GrammarSyntaxDetail</code> objects may be encapsulated
 * by a single <code>GrammarException</code>.
 * <P>
 *
 * Depending on the type of error and the context in
 * which the error is identified, some or all of the
 * following information may be provided: 
 *
 * <UL>
 *   <LI>Grammar name,
 *   <LI>Grammar location (URL or system resource),
 *   <LI>Rule name in which error is found,
 *   <LI>Import name,
 *   <LI>Line number of error in JSGF,
 *   <LI>Character number (within line) in JSGF, 
 *   <LI>A printable description string.
 * </UL>
 *
 * The following problems may be encountered when loading JSGF 
 * from a <code>URL</code> or <code>Reader</code>, or through the
 * <code>ruleForJSGF</code> method of the <code>RuleGrammar</code> interface.
 *
 * <UL>
 *   <LI>Missing or illegal grammar name declaration, or 
 *       grammar name doesn't match URL or file name.
 *   <LI>missing URL or URL does not contain JSGF file.
 *   <LI>Illegal import declaration.
 *   <LI>Illegal rule name or token.
 *   <LI>Missing semi-colon.
 *   <LI>Redefinition of a rule.
 *   <LI>Definition of a reserved rule name 
 *      (<code>&lt;NULL&gt;</code>, <code>&lt;VOID&gt;</code>).
 *   <LI>Unclosed quotes, tags, comment, or rule name.
 *   <LI>Unclosed grouping "()" or "[]".
 *   <LI>Empty rule definition or empty alternative.
 *   <LI>Missing or illegal weight on alternatives.
 *   <LI>Illegal attachment of unary operators (count and tag).
 *   <LI>Some other error.
 * </UL>
 *
 * When the <code>commitChanges</code> method of a <code>Recognizer</code>
 * is called, it performs addition checks to ensure all loaded grammars are
 * legal.  The following problems may be encountered:
 *
 * <UL>
 *   <LI>Unable to resolve import because grammar or rule is not defined.
 *   <LI>Reference to an undefined rule name.
 *   <LI>Illegal recursion: a rule refers to itself illegally.
 *       Only right recursion is allowed (defined by JSGF).
 *   <LI>Ambiguous reference to imported rule.
 * </UL>
 * 
 * @see GrammarException
 * @see Recognizer#loadJSGF(java.io.Reader)
 * @see Recognizer#loadJSGF(java.net.URL, java.lang.String)
 * @see RuleGrammar#ruleForJSGF
 * @see Recognizer#commitChanges
 */

public class GrammarSyntaxDetail
{
  /**
   * Name of grammar in which problem is encountered.
   * May be <code>null</code>.
   */

  public String grammarName = null;


  /**
   * URL location of grammar in which problem is encountered.
   * May be <code>null</code>.
   */

  public URL grammarLocation = null;


  /**
   * Name of rule within grammar in which problem is encountered.
   * May be <code>null</code>.
   */

  public String ruleName = null;


  /**
   * Name in grammar import declaration in which problem is encountered.
   * May be <code>null</code>.
   */

  public RuleName importName = null;


  /**
   * Line number in JSGF file for problem.
   * Negative values indicate that the line number unknown.
   */

  public int lineNumber = -1;


  /**
   * Character number in line in JSGF file for problem.
   * Negative values indicate that the line number unknown.
   */

  public int charNumber = -1;


  /**
   * Printable string describing problem.
   * May be <code>null</code>.
   */

  public String message = null;


  /**
   * Empty constructor.
   */

  public GrammarSyntaxDetail() {}


  /**
   * Complete constructor describing a syntax problem.
   */

  public GrammarSyntaxDetail(String grammarName, 
			     URL grammarLocation,
			     String ruleName,
			     RuleName importName,
			     int lineNumber,
			     int charNumber,
			     String message)
  {
     this.grammarName = grammarName;
     this.grammarLocation = grammarLocation;
     this.ruleName = ruleName;
     this.importName = importName;
     this.lineNumber = lineNumber;
     this.charNumber = charNumber;
     this.message = message;
  }
}

