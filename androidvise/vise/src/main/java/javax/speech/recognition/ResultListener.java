/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.util.*;


/**
 * The methods of a <code>ResultListener</code> receive notifications of 
 * events related to a <code>Result</code> object.  A <code>ResultListener</code>
 * may be attached to any of three entities:
 * <P>
 *
 * <UL>
 *   <LI><B><code>Recognizer</code></B>: a <code>ResultListener</code> attached  
 *       to a <code>Recognizer</code> receives notification of all 
 *       <code>ResultEvents</code> for all results produced by that
 *       recognizer.
 *   <LI><B><code>Grammar</code></B>: a <code>ResultListener</code> attached to a
 *       <code>Grammar</code> receives all events for all results that have
 *       been finalized for that grammar.  Specifically, it receives the
 *       <code>GRAMMAR_FINALIZED</code> event and following events.
 *       (Note: it never receives a <code>RESULT_CREATED</code> event because 
 *       that event always precedes the <code>GRAMMAR_FINALIZED</code> event). 
 *   <LI><B><code>Result</code></B>: a <code>ResultListener</code> attached to
 *       a <code>Result</code> receives all events for that result starting
 *       from the time at which the listener is attached. 
 *       (Note: it never receives a <code>RESULT_CREATED</code> event
 *       because a listener can only be attached to a <code>Result</code> 
 *       once a <code>RESULT_CREATED</code> event has already been issued.) 
 * </UL>
 * <P>
 *
 * The events are issued to the listeners in the order of most specific
 * to least specific: <code>ResultListeners</code> attached to the
 * <code>Result</code> are notified first, then listeners attached
 * to the matched <code>Grammar</code>, and finally to listeners
 * attached to the <code>Recognizer</code>.
 * <P>
 *
 * A single <code>ResultListener</code> may be attached to multiple objects 
 * (<code>Recognizer</code>, <code>Grammar</code> or <code>Result</code>),
 * and multiple <code>ResultListeners</code> may be attached to a single 
 * object.
 * <P>
 *
 * The source for all <code>ResultEvents</code> issued to a 
 * <code>ResultListener</code> is the <code>Result</code> that 
 * generated the event.  The full state system for results and the
 * associated events are described in the documentation
 * for <code>ResultEvent</code>.
 * <P>
 *
 * A trivial implementation of the <code>ResultListener</code> 
 * interface is provided by the <code>ResultAdapter</code> class.
 * <P>
 *
 * @see ResultEvent
 * @see ResultAdapter
 * @see Result#addResultListener(ResultListener)
 * @see Grammar#addResultListener(ResultListener)
 * @see Recognizer#addResultListener(ResultListener)
 */


public interface ResultListener extends EventListener
{
  /**
   * A <code>RESULT_CREATED</code> event is issued when a
   * <code>Recognizer</code> detects incoming speech that
   * may match an active grammar of an application.
   * <P>
   *
   * The event is issued to each <code>ResultListener</code> 
   * attached to the <code>Recognizer</code>.
   * (<code>ResultListeners</code> attached to a <code>Grammar</code>
   * or to a <code>Result</code> never receive a <code>RESULT_CREATED</code>
   * event.)
   * <P>
   *
   * The <code>RESULT_CREATED</code> follows the
   * <code>RECOGNIZER_PROCESSING</code> event that is issued
   * <code>RecognizerListeners</code> to indicate that the 
   * <code>Recognizer</code> has changed from the <code>LISTENING</code>
   * to the <code>PROCESSING</code> state.
   * <P>
   *
   * @see ResultEvent#RESULT_CREATED
   * @see RecognizerEvent#RECOGNIZER_PROCESSING
   */

  public void resultCreated(ResultEvent e);


  /**
   * A <code>RESULT_UPDATED</code> event has occured because a token has
   * been finalized and/or the unfinalized text of a result has changed.
   * <P>
   *
   * The event is issued to each <code>ResultListener</code> attached
   * to the <code>Recognizer</code>, to each <code>ResultListener</code>
   * attached to the <code>Result</code>, and if the <code>GRAMMAR_FINALIZED</code>
   * event has already been released to each <code>ResultListener</code>
   * attached to the matched <code>Grammar</code>.
   * <P>
   *
   * @see ResultEvent#RESULT_UPDATED
   */

  public void resultUpdated(ResultEvent e);


  /**
   * A <code>GRAMMAR_FINALIZED</code> event has occured because 
   * the <code>Recognizer</code> has determined which <code>Grammar</code>
   * is matched by the incoming speech.
   * <P>
   *
   * The event is issued to each <code>ResultListener</code> attached
   * to the <code>Recognizer</code>, <code>Result</code>, and 
   * matched <code>Grammar</code>.
   * <P>
   *
   * @see ResultEvent#GRAMMAR_FINALIZED
   */

  public void grammarFinalized(ResultEvent e);


  /**
   * An <code>RESULT_ACCEPTED</code> event has occured indicating
   * that a <code>Result</code> has transitioned from the
   * <code>UNFINALIZED</code> state to the <code>ACCEPTED</code>
   * state.  
   * <P>
   *
   * Since the <code>Result</code> source for this event is 
   * finalized, the <code>Result</code> object can be safely cast
   * to the <code>FinalResult</code> interface.
   * <P>
   *
   * Because the result is accepted, the matched <code>Grammar</code>
   * for the result is guaranteed to be non-null.  If the matched
   * <code>Grammar</code> is a <code>RuleGrammar</code>, then the
   * result object can be safely cast to <code>FinalRuleResult</code>
   * (methods of <code>FinalDictationResult</code> throw an exception).
   * If the matched <code>Grammar</code> is a <code>DictationGrammar</code>, 
   * then the result object can be safely cast to <code>FinalDictationResult</code>
   * (methods of <code>FinalRuleResult</code> throw an exception).
   * For example:
   * <P>
   *
   * <PRE>
   *      void resultAccepted(ResultEvent e) {
   *         Object source = e.getSource();
   *
   *         // always safe
   *         Result result = (Result)source;
   *         FinalResult fr = (FinalResult)source;
   *
   *         // find the grammar-specific details
   *         // the null test is only useful for rejected results
   *         if (result.getGrammar() != null) {
   *           if (result.getGrammar() instanceof RuleGrammar) {
   *             FinalRuleResult frr = (FinalRuleResult)source;
   *             ...
   *           }
   *           else if (result.getGrammar() instanceof DictationGrammar) {
   *             FinalDictationResult fdr = (FinalDictationResult)source;
   *             ...
   *           }
   * </PRE>
   *
   * The event is issued to each <code>ResultListener</code> attached
   * to the <code>Recognizer</code>, <code>Result</code>, and 
   * matched <code>Grammar</code>.
   * <P>
   *
   * The <code>RESULT_ACCEPTED</code> event is issued following
   * the <code>RECOGNIZER_SUSPENDED</code> <code>RecognizerEvent</code>
   * and while the <code>Recognizer</code> is in the <code>SUSPENDED</code>
   * state.  Once the <code>RESULT_ACCEPTED</code> event has been
   * processed by all listeners, the <code>Recognizer</code> automatically
   * commits all changes to grammars and returns to the <code>LISTENING</code>
   * state.  The only exception is when a call has been made to 
   * <code>suspend</code> without a following call to <code>commitChanges</code>.
   * In this case the <code>Recognizer</code> remains <code>SUSPENDED</code>
   * until <code>commitChanges</code> is called.
   * <P>
   *
   * @see ResultEvent#RESULT_ACCEPTED
   * @see FinalResult
   * @see FinalRuleResult
   * @see FinalDictationResult
   * @see RecognizerEvent
   * @see Recognizer#commitChanges
   */

  public void resultAccepted(ResultEvent e);


  /**
   * An <code>RESULT_REJECTED</code> event has occured indicating
   * that a <code>Result</code> has transitioned from the
   * <code>UNFINALIZED</code> state to the <code>REJECTED</code> state.  
   * <P>
   *
   * The casting behavior of a rejected result is the same as for
   * a <code>RESULT_ACCEPTED</code> event.  The exception is that if the 
   * grammar is not known (no <code>GRAMMAR_FINALIZED</code> event),
   * then the result cannot be cast to either 
   * <code>FinalRuleResult</code> or <code>FinalDictationResult</code>.
   * <P>
   *
   * The state behavior and grammar committing actions are the
   * same as for the <code>RESULT_ACCEPTED</code> event.
   * <P>
   *
   * The event is issued to each <code>ResultListener</code> attached
   * to the <code>Recognizer</code> and to the <code>Result</code>.
   * If a <code>GRAMMAR_FINALIZED</code> event was issued, then
   * the matched <code>Grammar</code> is known, and the 
   * event is also issued to each <code>ResultListener</code> attached
   * to that <code>Grammar</code>.
   * <P>
   *
   * @see ResultEvent#RESULT_REJECTED
   * @see FinalResult
   * @see FinalRuleResult
   * @see FinalDictationResult
   */

  public void resultRejected(ResultEvent e);


  /**
   * A <code>AUDIO_RELEASED</code> event has occured.
   * This event is only issued to finalized results.
   * See the documentation of the <code>isAudioAvailable</code> method
   * the <code>FinalResult</code> interface for details.
   * <P>
   *
   * The event is issued to each <code>ResultListener</code> attached
   * to the <code>Recognizer</code> and to the <code>Result</code>.
   * If a <code>GRAMMAR_FINALIZED</code> event was issued, then
   * the matched <code>Grammar</code> is known, and the 
   * event is also issued to each <code>ResultListener</code> attached
   * to that <code>Grammar</code>.
   * <P>
   *
   * @see ResultEvent#AUDIO_RELEASED
   * @see FinalResult#isAudioAvailable
   */

  public void audioReleased(ResultEvent e);


  /**
   * A <code>TRAINING_INFO_RELEASED</code> event has occured.
   * This event is only issued to finalized results.
   * See the documentation of the <code>isTrainingInfoAvailable</code> method
   * the <code>FinalResult</code> interface for details.
   * <P>
   *
   * The event is issued to each <code>ResultListener</code> attached
   * to the <code>Recognizer</code> and to the <code>Result</code>.
   * If a <code>GRAMMAR_FINALIZED</code> event was issued, then
   * the matched <code>Grammar</code> is known, and the 
   * event is also issued to each <code>ResultListener</code> attached
   * to that <code>Grammar</code>.
   * <P>
   *
   * @see ResultEvent#TRAINING_INFO_RELEASED
   * @see FinalResult#isTrainingInfoAvailable
   */

  public void trainingInfoReleased(ResultEvent e);
}


