/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Parent interface supported by all recognition grammars
 * including <code>DictationGrammar</code> and <code>RuleGrammar</code>.
 * A <code>Grammar</code> defines a set of tokens (words) that may be
 * spoken and the patterns in which those tokens may be spoken.  
 * Different grammar types (dictation vs. rule) define the words and the
 * patterns in different ways.
 * <P>
 *
 * The core functionality provided through the <code>Grammar</code> interface
 * includes:
 * <P>
 *
 * <UL>
 *  <LI>Naming: each grammar of a <code>Recognizer</code> has a unique name.
 *  <LI>Enabling: grammars may be enabled or disabled for activation of recognition.
 *  <LI>Activation: the activation mode can be set and the activation state tested.
 *  <LI>Adding and removing <code>GrammarListeners</code> for grammar-related events.
 *  <LI>Adding and removing <code>ResultListeners</code> to receive result events
 *      for results matching the <code>Grammar</code>.
 *  <LI>Access the <code>Recognizer</code> that owns the grammar
 * </UL>
 * <P>
 *
 * Each <code>Grammar</code> is associated with a specific <code>Recognizer</code>. 
 * All grammars are located, created and managed through the <code>Recognizer</code>
 * interface.  The basic steps in using any <code>Grammar</code> are:
 * <P>
 *
 * <OL>
 *  <LI> Create a new <code>Grammar</code> or get a reference to an existing grammar
 *       through the <code>Recognizer</code> interface.
 *  <LI> Attach a <code>ResultListener</code> to get results.
 *  <LI> As necessary, setup or modify the grammar for the application's context.
 *  <LI> Enabled and disable recognition of the <code>Grammar</code> as required
 *       by the application's context.
 *  <LI> Commit grammar changes and enabled state into the recognition process.
 *  <LI> Repeat the update, enable and commit steps as required.
 *  <LI> For application-created grammars,
 *       delete the <code>Grammar</code> when it is no longer needed.
 * </OL>
 * <P>
 *
 * <B>Grammar Types</B>
 * <P>
 *
 * There are two types of <code>Grammar</code>: 
 * <A HREF="DictationGrammar.html"><code>DictationGrammar</code></A>
 * and <A HREF="RuleGrammar.html"><code>RuleGrammar</code></A>
 * Both are defined by interfaces that extend the <code>Grammar</code> 
 * interface. All recognizers support <code>RuleGrammars</code>.  
 * A recognizer may optionally support a <code>DictationGrammar</code>.
 * <P>
 *
 * The <code>RuleGrammar</code> and <code>DictationGrammar</code>
 * interfaces define specialized mechanisms for controlling and modifying
 * those types of grammar.
 * <P>
 *
 * <A NAME="activation"></A>
 * <B>Grammar Activation</B>
 * <P>
 *
 * When a <code>Grammar</code> is active, the <code>Recognizer</code>
 * listens to incoming audio for speech that matches that <code>Grammar</code>.
 * To be activated, an application must first set the <code>enabled</code>
 * property to <code>true</code> (enable activation) and set the
 * <code>activationMode</code> property to indicate the 
 * <em>activation conditions</em>.
 * <P>
 *
 * There are three activation modes: <code>RECOGNIZER_FOCUS</code>,
 * <code>RECOGNIZER_MODAL</code> and <code>GLOBAL</code>.  For each
 * mode a certain set of activation conditions must be met for the
 * grammar to be activated for recognition.  The activation 
 * conditions are determined by <code>Recognizer</code> focus and
 * possibly by the activation of other grammars.  Recognizer
 * focus is managed with the
 * <A HREF="Recognizer.html#requestFocus">requestFocus</A> and
 * <A HREF="Recognizer.html#releaseFocus">releaseFocus</A> 
 * methods of a <code>Recognizer</code>.
 * <P>
 *
 * The modes are listed here by priority.  Always use the lowest 
 * priority mode possible.
 * <P>
 * 
 * <UL>
 *   <LI><code>GLOBAL</code> activation mode: if <code>enabled</code>,
 *       the <code>Grammar</code> is always active irrespective of
 *       whether the <code>Recognizer</code> of this application
 *       has focus.
 *
 *   <LI><code>RECOGNIZER_MODAL</code> activation mode: if <code>enabled</code>,
 *       the <code>Grammar</code> is always active when the
 *       <code>Recognizer</code> of this application has focus.
 *       Furthermore, enabling a modal grammar deactivates
 *       any grammars in the same <code>Recognizer</code> with the
 *       <code>RECOGNIZER_FOCUS</code> activation mode.
 *
 *   <LI><code>RECOGNIZER_FOCUS</code> activation mode (default): 
 *       if <code>enabled</code>, the <code>Grammar</code> is active when 
 *       the <code>Recognizer</code> of this application has focus.  The 
 *       exception is that if any other grammar of this application is 
 *       enabled with <code>RECOGNIZER_MODAL</code> activation mode, then 
 *       this grammar is not activated.
 * </UL>
 * <P>
 *
 * The current activation state of a grammar can be tested with the
 * <code>isActive</code> method.  Whenever a grammar's activation changes
 * either a <code>GRAMMAR_ACTIVATED</code> or
 * <code>GRAMMAR_DEACTIVATED</code> event is issued to each
 * attached <code>GrammarListener</code>.
 * <P>
 *
 * An application may have zero, one or many grammar enabled at
 * any time.  As the conventions below indicate, well-behaved
 * applications always minimize the number of active grammars.
 * <P>
 *
 * The activation and deactivation of grammars is independent of
 * <code>PAUSED</code> and <code>RESUMED</code> states of the
 * <code>Recognizer</code>.  However, when a <code>Recognizer</code> 
 * is <code>PAUSED</code>, audio input to the <code>Recognizer</code> 
 * is turned off, so speech can't be detected.  Note that just after
 * pausing a recognizer there may be some remaining audio in the input
 * buffer that could contain recognizable speech and thus an active 
 * grammar may continue to receive result events.
 * <P>
 *
 * Well-behaved applications adhere to the following conventions
 * to minimize impact on other applications and other components
 * of the same application:
 * <P>
 *
 * <UL>
 *   <LI>Never apply the <code>GLOBAL</code> activation mode to
 *       a <code>DictationGrammar</code>.
 *   <LI>Always use the default activation mode <code>RECOGNIZER_FOCUS</code> 
 *       unless there is a good reason to use another mode.
 *   <LI>Only use the <code>RECOGNIZER_MODAL</code> when certain
 *       that deactivating <code>RECOGNIZER_FOCUS</code> grammars
 *       will not adversely affect the user interface.
 *   <LI>Minimize the complexity and the number of <code>RuleGrammars</code> 
 *       with <code>GLOBAL</code> activation mode.
 *   <LI>Only enable a grammar when it is appropriate for a 
 *       user to say something matching that grammar.  Otherwise
 *       disable the grammar to improve recognition response time
 *       and recognition accuracy for other grammars.
 *   <LI>Only request focus when confident that the user's speech focus
 *       (attention) is directed to grammars of your application.
 *       Release focus when it is not required.
 * </UL>
 *
 * The general principal underlying these conventions is that
 * increasing the number of active grammars and/or increasing the 
 * complexity of those grammars can lead to slower response time, greater CPU
 * load and reduced recognition accuracy (more mistakes).
 * <P>
 *
 * <A NAME="commit"></A>
 * <B>Committing Changes</B>
 * <P>
 *
 * Grammars can be dynamically changed and enabled and disabled.
 * Changes may be necessary as the application changes context,
 * as new information becomes available and so on.  As with graphical
 * interface programming most grammar updates occur during processing
 * of events.  Very often grammars are updated in response to
 * speech input from a user (a <code>ResultEvent</code>).
 * Other asynchronous events (e.g., <code>AWTEvents</code>)
 * are another common trigger of grammar changes.  Changing 
 * grammars during normal operation of a recognizer is usually
 * necessary to ensure natural and usable speech-enabled applications.
 * <P>
 *
 * Different grammar types allow different types of run-time change.
 * <code>RuleGrammars</code> can be created and deleted  
 * during normal recognizer operation.  Also, any aspect of a created 
 * <code>RuleGrammar</code> can be redefined: rules can be modified, 
 * deleted or added, imports can be changed an so on.   Certain
 * properties of a <code>DictationGrammar</code> can be changed:
 * the context can be updated and the vocabulary modified.
 * <P>
 *
 * For any grammar changes to take effect they must be <em>committed</em>.
 * Committing changes builds the grammars into a format that can be used
 * by the internal processes of the recognizer and applies those changes to
 * the processing of incoming audio.
 * <P>
 *
 * There are two ways in which a commit takes place:
 * <P>
 * 
 * <OL>
 *   <LI>
 *     An explicit call by an application to the 
 *     <A HREF="Recognizer.html#commitChanges()"><code>commitChanges</code></A>
 *     method of the <code>Recognizer</code>.  The documentation for the
 *     method describes when and how changes are committed when called.
 *     (The <code>commitChanges</code> method is typically used in
 *     conjunction with the <code>suspend</code> method of
 *     <code>Recognizer</code>.)
 *   <LI>
 *     Changes to all grammars are implicitly committed at the completion
 *     of processing of a result finalization event (either a
 *     <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code> event).
 *     This simplifies the common scenario in which grammars are changed
 *     during result finalization process because the user's input 
 *     has changed the application's state.  (This implicit commit
 *     is deferred following a call to <code>suspend</code> 
 *     and until a call to <code>commitChanges</code>.)
 * </OL>
 *
 * The <code>Recognizer</code> issues a <code>CHANGES_COMMITTED</code> 
 * event to signal that changes have been committed.  This event signals
 * a transition from the <code>SUSPENDED</code> state to the
 * <code>LISTENING</code> state.  Once in the <code>LISTENING</code> state
 * the <code>Recognizer</code> resumes the processing of incoming
 * audio with the newly committed grammars.
 * <P>
 *
 * Also each changed <code>Grammar</code> receives a 
 * <code>GRAMMAR_CHANGES_COMMITTED</code> through attached
 * <code>GrammarListeners</code> whenever changes to it are
 * committed.
 * <P>
 *
 * The commit changes mechanism has two important properties:
 * <P>
 * 
 * <UL>
 *  <LI> Updates to grammar definations and the enabled property take
 *       effect atomically (all changes take effect at once).  There
 *       are no intermediate states in which some, but not all, 
 *       changes have been applied.
 *  <LI> It is a method of <code>Recognizer</code> so all changes to all 
 *       <code>Grammars</code> are committed at once.  Again, there
 *       are no intermediate states in which some, but not all, 
 *       changes have been applied.
 * </UL>
 * <P>
 *
 * <B>Grammar and Result Listeners</B>
 * <P>
 * 
 * An application can attach one or more <code>GrammarListeners</code> 
 * to any <code>Grammar</code>.  The listener is notified of 
 * status changes for the <code>Grammar</code>: when changes are
 * committed and when activation changes.
 * <P>
 *
 * An application can attach one or more <code>ResultListener</code>
 * objects to each <code>Grammar</code>.  The listener is notified of 
 * all events for all results that match this grammar.  The listeners
 * receive <code>ResultEvents</code> starting with the 
 * <code>GRAMMAR_FINALIZED</code> event (not the preceding 
 * <code>RESULT_CREATED</code> or <code>RESULT_UPDATED</code>
 * events).
 * <P>
 *
 * @see RuleGrammar
 * @see DictationGrammar
 * @see GrammarListener
 * @see ResultListener
 * @see Result
 * @see Recognizer
 * @see Recognizer#commitChanges
 */


public interface Grammar
{
  /**
   * Default value of activation mode that requires the <code>Recognizer</code>
   * have focus and that there be no enabled grammars with
   * <code>RECOGNIZER_MODAL</code> mode for the grammar to be
   * activated.  This is the lowest priority activation mode
   * and should be used unless there is a user interface design
   * reason to use another mode.
   * <P>
   *
   * The <A HREF="#activation">activation conditions</A>
   * for the <code>RECOGNIZER_FOCUS</code> mode are describe above.
   * <P>
   *
   * @see #setActivationMode
   * @see #RECOGNIZER_MODAL
   * @see #GLOBAL
   */

  public static int RECOGNIZER_FOCUS = 900;


  /**
   * Value of activation mode that requires the <code>Recognizer</code>
   * have focus for the grammar to be activated.
   * <P>
   *
   * The <A HREF="#activation">activation conditions</A>
   * for the <code>RECOGNIZER_MODAL</code> mode are describe above.
   * <P>
   *
   * @see #setActivationMode
   * @see #RECOGNIZER_FOCUS
   * @see #GLOBAL
   */

  public static int RECOGNIZER_MODAL = 901;


  /**
   * Value of activation mode in which the <code>Grammar</code>
   * is active for recognition irrespective of the focus state
   * of the <code>Recognizer</code>.
   * <P>
   *
   * The <A HREF="#activation">activation conditions</A>
   * for the <code>GLOBAL</code> mode are describe above.
   * <P>
   *
   * @see #setActivationMode
   * @see #RECOGNIZER_FOCUS
   * @see #RECOGNIZER_MODAL
   */

  public static int GLOBAL = 902;


  /** 
   * Returns a reference to the <code>Recognizer</code> that 
   * owns this grammar.
   */

  public Recognizer getRecognizer();


  /** 
   * Get the name of a grammar.  A grammar's name must be unique for 
   * a recognizer.  Grammar names use a similar naming convention to
   * Java classes.  The naming convention are defined in the 
   * Java Speech Grammar Format Specification.
   * <P>
   *
   * Grammar names are used with a <code>RuleGrammar</code> 
   * for resolving imports and references between grammars.
   * The name of a <code>RuleGrammar</code> is set when the grammar
   * is created (either by loading a JSGF document or creating
   * a new <code>RuleGrammar</code>).
   * <P>
   *
   * The name of a <code>DictationGrammar</code> should reflect the language 
   * domain it supports.  For example: <code>com.acme.dictation.us.general</code> 
   * for general US Dictation from Acme corporation.  Since a <code>DictationGrammar</code>
   * is built into a <code>Recognizer</code>, its name is determined by
   * the <code>Recognizer</code>.
   * <P>
   * 
   * @see DictationGrammar
   * @see RuleGrammar
   */

  public String getName();


  /**
   * Set the enabled property of a <code>Grammar</code>.  A change 
   * in the enabled property takes effect only after 
   * <A HREF="#commit">grammar changes are committed</A>.
   * Once a grammar is enabled and when the 
   * <A HREF="#activation">activation conditions</A> are met, it
   * is activated for recognition.  When a grammar is activated, the
   * <code>Recognizer</code> listens to incoming audio for speech
   * that matches the grammar and produces a <code>Result</code> when 
   * matching speech is detected. 
   * <P>
   *
   * The enabled property of a grammar is tested with the <code>isEnabled</code> method.
   * The activation state of a grammar is tested with the <code>isActive</code> method.
   * <P>
   *
   * The <code>RuleGrammar</code> interface extends the enabling property
   * to allow individual rules to be enabled and disabled.
   * <P>
   *
   * @see Recognizer#commitChanges
   * @see RuleGrammar#setEnabled(boolean)
   * @see RuleGrammar#setEnabled(java.lang.String, boolean)
   * @see RuleGrammar#setEnabled(java.lang.String[], boolean)
   * @see RuleGrammar#isEnabled
   */

  public void setEnabled(boolean enabled);


  /**
   * Return the <code>enabled</code> property of a <code>Grammar</code>.  
   * More specialized behaviour is specified by the
   *  <code>RuleGrammar</code> interface.
   * <P>
   *
   * @see #setEnabled
   * @see RuleGrammar#isEnabled()
   * @see RuleGrammar#isEnabled(java.lang.String)
   */

  public boolean isEnabled();


  /**
   * Set the activation mode of a <code>Grammar</code> as
   * <code>RECOGNIZER_FOCUS</code>, <code>RECOGNIZER_MODAL</code>, 
   * or <code>GLOBAL</code>.  The role of the activation mode
   * in the <A HREF="#activation">activation conditions</A>
   * for a <code>Grammar</code> are described above.
   * The default activation mode - <code>RECOGNIZER_FOCUS</code> -
   * should be used unless there is a user interface design
   * reason to use another mode.
   * <P>
   *
   * The individual rules of a <code>RuleGrammar</code> can
   * be separately enabled and disabled.  However, all rules
   * share the same <code>ActivationMode</code> since the
   * mode is a property of the complete <code>Grammar</code>.
   * A consequence is that all enabled rules of a <code>RuleGrammar</code>
   * are activated and deactivated together.
   * <P>
   *
   * A change in activation mode only takes effect once
   * <A HREF="#commit">changes are committed</A>.  For some
   * recognizers changing the activation mode is computationally
   * expensive.
   * <P>
   *
   * The activation mode of a grammar can be tested by the 
   * <code>getActivationMode</code> method.
   * <P>
   *
   * [Note: future releases may modify the set of activation modes.]
   * <P>
   *
   * @see #getActivationMode
   * @see #RECOGNIZER_FOCUS
   * @see #RECOGNIZER_MODAL
   * @see #GLOBAL
   * @see #setEnabled
   * @see #isActive
   * @see Recognizer#commitChanges
   * @exception IllegalArgumentException
   *   if an attempt is made to set <code>GLOBAL</code> mode for a <code>DictationGrammar</code>
   */

  public void setActivationMode(int mode)
    throws IllegalArgumentException;


  /**
   * Return the current activation mode for a <code>Grammar</code>.
   * The default value for a grammar is <code>RECOGNIZER_FOCUS</code>.
   * <P>
   *
   * @see #setActivationMode
   * @see #setEnabled
   * @see #isActive
   */

  public int getActivationMode();


  /**
   * Test whether a <code>Grammar</code> is currently active for recognition. 
   * When a grammar is active, the recognizer is matching incoming audio
   * against the grammar (and other active grammars) to detect speech
   * that matches the grammar.
   * <P>
   *
   * A <code>Grammar</code> is activated for recognition if the
   * <code>enabled</code> property is set to <code>true</code> and
   * the <A HREF="#activation">activation conditions</A> are met.
   * Activation is not directly controlled by applications and so can
   * only be tested (there is no <code>setActive</code> method).
   * <P>
   *
   * Rules of a <code>RuleGrammar</code> can be individuallly enabled
   * and disabled.  However all rules share the same <code>ActivationMode</code>
   * and the same activation state.  Thus, when a <code>RuleGrammar</code> is
   * active, all the enabled rules of the grammar are active for recognition.
   * <P>
   *
   * Changes in the activation state are indicated by 
   * <code>GRAMMAR_ACTIVATED</code> and <code>GRAMMAR_DEACTIVATED</code>
   * events issued to the <code>GrammarListener</code>.  A change in
   * activation state can follow these <code>RecognizerEvents</code>:
   * <P>
   *
   * <UL>
   *  <LI>A <code>CHANGES_COMMITTED</code> event that applies a
   *      change in the <code>enabled</code> state or 
   *      <code>ActivationMode</code> of this or another <code>Grammar</code>.
   *  <LI>A <code>FOCUS_GAINED</code> or <code>FOCUS_LOST</code>
   *      event.
   * </UL>
   * <P>
   *
   * @see #setEnabled
   * @see #setActivationMode
   * @see GrammarEvent#GRAMMAR_ACTIVATED
   * @see GrammarEvent#GRAMMAR_DEACTIVATED
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see RecognizerEvent#FOCUS_GAINED
   * @see RecognizerEvent#FOCUS_LOST
   */

  public boolean isActive();


  /**
   * Request notifications of events of related to this <code>Grammar</code>.
   * An application can attach multiple listeners to a <code>Grammar</code>.
   * <P>
   *
   * @see #removeGrammarListener
   */

  public void addGrammarListener(GrammarListener listener);


  /**
   * Remove a listener from this <code>Grammar</code>.
   * <P>
   *
   * @see #addGrammarListener
   */

  public void removeGrammarListener(GrammarListener listener);

  /* ********************* RESULT LISTENER **************************** */


  /**
   * Request notifications of events from any <code>Result</code>
   * that matches this <code>Grammar</code>.  An application
   * can attach multiple ResultListeners to a <code>Grammar</code>.
   * A listener is removed with the <code>removeResultListener</code> method.
   * <P>
   *
   * A <code>ResultListener</code> attached to a <code>Grammar</code>
   * receives result events starting from the <code>GRAMMAR_FINALIZED</code>
   * event - the event which indicates that the matched grammar is
   * known.  A <code>ResultListener</code> attached to a
   * <code>Grammar</code> will never receive a <code>RESULT_CREATED</code>
   * event and does not receive any <code>RESULT_UPDATED</code>
   * events that occurred before the <code>GRAMMAR_FINALIZED</code>
   * event.  A <code>ResultListener</code> attached to a <code>Grammar</code>
   * is guaranteed to receive a result finalization event - 
   * <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code> - 
   * some time after the <code>GRAMMAR_FINALIZED</code> event.
   * <P>
   *
   * <code>ResultListener</code> objects can also be attached to 
   * a <code>Recognizer</code> and to any <code>Result</code>.
   * A listener attached to the <code>Recognizer</code> receives
   * all events for all results produced by that <code>Recognizer</code>.
   * A listener attached to a <code>Result</code> receives
   * all events for that result from the time at which the listener
   * is attached.
   * <P>
   *
   * @see #removeResultListener
   * @see Recognizer#addResultListener
   * @see Result#addResultListener
   * @see ResultEvent#RESULT_CREATED
   * @see ResultEvent#GRAMMAR_FINALIZED
   * @see ResultEvent#RESULT_ACCEPTED
   * @see ResultEvent#RESULT_REJECTED
   */

  public void addResultListener(ResultListener listener);


  /**
   * Remove a <code>ResultListener</code> from this <code>Grammar</code>.
   * <P>
   *
   * @see #addResultListener
   * @see Recognizer#removeResultListener
   * @see Grammar#removeResultListener
   */

  public void removeResultListener(ResultListener listener);
}

