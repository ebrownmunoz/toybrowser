/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * The adapter which receives grammar events.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 */


public class GrammarAdapter implements GrammarListener
{
  /**
   * Event issued when a <code>Recognizer</code> has committed
   * changes to a <code>Grammar</code>.  The <code>Grammar</code>
   * interface documents how and when
   * <A HREF="Grammar.html#commit">changes are committed</A>.
   * <P>
   *
   * @see GrammarEvent#GRAMMAR_CHANGES_COMMITTED
   */

  public void grammarChangesCommitted(GrammarEvent e) {}


  /**
   * A <code>GRAMMAR_ACTIVATED</code> event occurred.
   * <P>
   *
   * @see GrammarEvent#GRAMMAR_ACTIVATED
   */

  public void grammarActivated(GrammarEvent e) {}


  /**
   * A <code>GRAMMAR_DEACTIVATED</code> event occurred.
   * <P>
   *
   * @see GrammarEvent#GRAMMAR_DEACTIVATED
   */

  public void grammarDeactivated(GrammarEvent e) {}
}

