/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * A <code>SpeakerProfile</code> object is used to identify each enrollment by a user
 * to a <code>Recognizer</code>.  <code>SpeakerProfile</code> objects are used in 
 * management of speaker data through the <code>SpeakerManager</code> interface
 * for a <code>Recognizer</code> and in selection of recognizers through the
 * <code>RecognizerModeDesc</code> class.
 * <P>
 *
 * A user may have a single or multiple profiles stored in recognizer.  Examples of
 * multiple profiles include a user who enrolls and trains the recognizer
 * separately for different microphones or for different application domains
 * (e.g. romance novels and business email).
 * <P>
 *
 * Each <code>SpeakerProfile</code> object has a unique identifier
 * (unique to the <code>Recognizer</code>), plus a user name
 * and optionally a variant name that identifies each separate profile
 * for a user (per-user unique).  All three identifying properties should be 
 * human-readable strings.  (The identifier is often the concatenation of the
 * user name and variant.)
 * <P>
 *
 * The user name may be the same as the <code>"user.name"</code> property
 * stored in the <code>java.lang.System</code> properties. (Note: access 
 * to system properties is restricted by Java's <code>SecurityManager</code>.)  
 * Appropriate naming of profiles is the joint responsibility of users and
 * recognizers.
 * <P>
 *
 * Calls to the <code>setXXX</code> methods of a <code>SpeakerProfile</code>
 * make persistent changes to the speaker data stored by the recognizer.  These
 * changes are persistent across sessions with the recognizer.
 * <P>
 *
 * <code>SpeakerProfiles</code> are created and managed by the
 * <code>SpeakerManager</code> for a <code>Recognizer</code>.  
 * <P>
 *
 * <B>Speaker Data</B>
 * <P>
 *
 * A <code>SpeakerProfile</code> object identifies all the stored data 
 * the recognizer has about a speaker in a particular enrollment.
 * The contents of the profile are controlled by the recognizer.  Except
 * for the properties of the <code>SpeakerProfile</code>, this data is
 * not accessibile to an application.  The profile may include:
 * <P>
 *
 * <UL>
 *   <LI> Speaker data: full name, age, gender etc.
 *   <LI> Speaker preferences: including settings of the <code>RecognizerProperties</code>
 *   <LI> Language models: data about the words and word patterns of the speaker
 *   <LI> Word models: data about the pronunciation of words by the speaker
 *   <LI> Acoustic models: data about the speaker's voice
 *   <LI> Training information and usage history
 * </UL>
 *
 * The key role of stored profiles maintaining information that
 * enables a recognition to adapt to characteristics of the speaker.
 * The goal of this adaptation is to improve the performance and
 * accuracy of speech recognition.  
 * <P>
 *
 * <B>Speakers in Recognizer Selection</B>
 * <P>
 *
 * When selecting a <code>Recognizer</code> (through <code>javax.speech.Central</code>) a user 
 * will generally prefer to select an engine which they have already trained.
 * The <code>getSpeakerProfiles</code> method of a <code>RecognizerModeDesc</code> 
 * should return the list of speaker profiles known to a recognizer (or 
 * <code>null</code> for speaker-independent recognizers and new recognizers).
 * For that selection process to be effective, a recognizer is responsible for 
 * ensuring that the <code>getSpeakerProfiles</code> method of its 
 * <code>RecognizerModeDesc</code> includes its complete list of known speakers.
 * <P>
 *
 * @see SpeakerManager
 * @see SpeakerManager#newSpeakerProfile
 * @see Recognizer#getSpeakerManager
 * @see RecognizerModeDesc#getSpeakerProfiles
 */


public class SpeakerProfile
{
  /**
   * Unique identifier for a <code>SpeakerProfile</code>.  
   * (Unique for a <code>Recognizer</code>.)
   *
   * @see #getId
   */
  
  protected String id;


  /**
   * Name of user identified by the profile.
   *
   * @see #getName
   */
  
  protected String name;


  /**
   * Name of variant enrollment of a user.
   * Should be unique for a user.
   *
   * @see #getName
   */
  
  protected String variant;


  /**
   * Null constructor sets all properties too <code>null</code>.
   * <P>
   *
   * Applications can create <code>SpeakerProfile</code> objects 
   * for use in selection of engines and when requesting an engine
   * to build a new profile.  A <code>SpeakerProfile</code>
   * created by an application using a <code>SpeakerProfile</code>
   * constructor does not reference a recognizer's profile.
   */

  public SpeakerProfile() {
    id = null;
    name = null;
    variant = null;
  }


  /**
   * Constructor a profile object with all properties specified.
   */

  public SpeakerProfile(String id, String name, String variant) {
    this.id = id;
    this.name = name;
    this.variant = variant;
  }


  /**
   * Return the <code>SpeakerProfile</code> identifier.
   */

  public String getId() {return id;}


  /**
   * Set the <code>SpeakerProfile</code> identifier.
   * The identifier should be a human-readable string.
   * The identifier string must be unique for a recognizer.
   * The identifier is sometimes the concatenation of the
   * user name and variants strings.
   * <P>
   *
   * If the <code>SpeakerProfile</code> object is one returned
   * from a recognizer's <code>SpeakerManager</code>, setting
   * the identifier changes the persistent speaker data of
   * the recognizer.
   * <P>
   *
   * @exception IllegalArgumentException
   *   if the speaker id is already being used by this recognizer
   */

  public void setId(String identifier)
    throws IllegalArgumentException
    {
      this.id = id;
    }


  /**
   * Return the speaker name.
   */

  public String getName() {return name;}


  /**
   * Set the speaker name.  The speaker name should be a human-readable string.
   * The speaker name does not need to be unique for
   * a recognizer.  (A speaker with more than
   * one profile must have a separate variant for each).
   * <P>
   *
   * If the <code>SpeakerProfile</code> object is one returned
   * from a recognizer's <code>SpeakerManager</code>, setting
   * the name changes the persistent speaker data of
   * the recognizer.
   */

  public void setName(String name) {
    this.name = name;
  }



  /**
   * Get the variant description.
   */

  public String getVariant() {return variant;}


  /**
   * Get the variant description.
   * The variant should be a human-readable string.
   * A speaker with more than one <code>SpeakerProfile</code>
   * should have a different variant description for each profile.
   * If a speaker has only one profile, the variant description
   * may be <code>null</code>.
   * <P>
   *
   * If the <code>SpeakerProfile</code> object is one returned
   * from a recognizer's <code>SpeakerManager</code>, setting
   * the variant changes the persistent speaker data of
   * the recognizer.
   */

  public void setVariant(String variant) {
    this.variant = variant;
  }


  /**
   * Returns <code>true</code> if this object matches the 
   * <code>require</code> object.  A match requires that each non-null
   * or non-zero-length string property of the required object
   * be an exact string match to the properties of this object.
   */

  public boolean match(SpeakerProfile require) {
    if (require.id != null)
      if (!require.id.equals(this.id))
	return false;

    if (require.name != null)
      if (!require.name.equals(this.name))
	return false;

    if (require.variant != null)
      if (!require.variant.equals(this.variant))
	return false;

    return true;
  }


  /**
   * True if and only if the input parameter is not <CODE>null</CODE>
   * and is a <code>SpeakerProfile</code> with equal values of
   * all properties.
   */

  public boolean equals(Object anObject) {
    if ((anObject == null) || !(anObject instanceof SpeakerProfile))
      return false;

    SpeakerProfile anotherProfile = (SpeakerProfile)anObject;

    if (id == null) {
      if (anotherProfile.id != null)
	return false;
    }
    else if (!id.equals(anotherProfile.id))
      return false;

    if (name == null) {
      if (anotherProfile.name != null)
	return false;
    }
    else if (!name.equals(anotherProfile.name))
      return false;

    if (variant == null) {
      if (anotherProfile.variant != null)
	return false;
    }
    else if (!variant.equals(anotherProfile.variant))
      return false;

    return true;
  }
}

