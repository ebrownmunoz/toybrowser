/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.lang.*;


/**
 * <code>RuleGrammar</code> interface describes a <code>Grammar</code> 
 * that defines what users may say by a set rules.  The rules may be 
 * defined as <code>Rule</code> objects that represent the rule in a 
 * data structure or defined in the 
 * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSGF/index.html">
 * Java Speech Grammar Format (JSGF)</A>.
 * <P>
 *
 * All <code>RuleGrammars</code> are created and managed through the
 * <code>Recognizer</code> interface.  A <code>RuleGrammar</code>
 * may be created with the <code>newRuleGrammar</code> method.  
 * A <code>RuleGrammar</code> is also created when JSGF text is 
 * loaded with the <code>loadJSGF</code> methods either from
 * a <code>Reader</code> or from a <code>URL</code>.
 * <P>
 *
 * A <code>RuleGrammar</code> contains the same information as a
 * grammar definition in the 
 * <A HREF="http://java.sun.com/products/java-media/speech/">Java Speech Grammar Format</A>.
 * That information includes:
 * <P>
 *
 * <UL>
 *  <LI>The name of the grammar (inherited from <code>Grammar</code> interface), 
 *  <LI>A set of imports: each import references a single public rule of another
 *      <code>RuleGrammar</code> or all public rules of another <code>RuleGrammar</code>.
 *  <LI>A set of defined rules: each definition is identified by a unique
 *      rulename (unique within the <code>RuleGrammar</code>), a boolean
 *      flag indicating whether the rule is public, and a <code>Rule</code>
 *      object that provides the logical expansion of the rule (how it spoken).
 * </UL>
 * <P>
 *
 * The set of imports and the rule definitions can be changed by 
 * applications.  For any change to take effect the application must
 * call the <code>commitChanges</code> method of the <code>Recognizer</code>.
 * <P>
 *
 * A <code>RuleGrammar</code> can be printed in Java Speech Grammar Format
 * using the <code>toString</code> method.  Individual <code>Rule</code>
 * objects can be converted to JSGF with their <code>toString</code> methods.
 * <P>
 * 
 * In JSGF a rulename is surrounded by angle brackets (e.g. 
 * <code>&lt;rulename&gt;</code>).  The angle brackets are ignored
 * in calls to the methods of a <code>RuleGrammar</code> - they
 * may be included but are stripped automatically and are not
 * included in rulenames returned by <code>RuleGrammar</code> methods.
 * <P>
 *
 * The rules defined in a <code>RuleGrammar</code> are either <code>public</code>
 * or <code>private</code>.  A <code>public</code> rule may be:
 * <P>
 *
 * <OL>
 *  <LI>Imported into other <code>RuleGrammars</code>,
 *  <LI>Enabled for recognition,
 *  <LI>Or both.
 * </OL>
 * <P>
 *
 * When a <code>RuleGrammar</code> is enabled and when the
 * activation conditions are appropriate (as described in the
 * documentation for <code>Grammar</code>) then the <code>Grammar</code>
 * is activated and any of the public rules of the grammar may be spoken.  
 * The <code>RuleGrammar</code> interface extends the enable methods
 * of the <code>Grammar</code> interface to allow individual rules
 * to be enabled and disabled independently.
 * <P>
 * 
 * A public rule may reference private rules and imported rules.  Only
 * the top public rule needs to be enabled for it to be spoken.  The
 * referenced rules (local private or imported public rules) do not
 * need to be enabled to be spoken as part of the enabled rule.
 * <P>
 *
 * @see Recognizer
 * @see Rule
 * @see Grammar
 * @see <A HREF="http://java.sun.com/products/java-media/speech/">Java Speech Grammar Format</A>
 */


public interface RuleGrammar extends Grammar
{
  /**
   * Convert a <code>String</code> in partial Java Speech Grammar Format (JSGF)
   * to a <code>Rule</code> object.  The string can be any legal 
   * <em>expansion</em> from JSGF: i.e. anything that can appear
   * on the right hand side of a  JSGF <em>rule definition</em>.  
   * For example,
   *
   * <pre>
   *   Rule r = ruleGrammar.ruleForJSGF("[please] &lt;action&gt; all files");
   * </pre>
   *
   * @exception GrammarException
   *   if the JSGF text contains any errors
  * @see <A HREF="http://java.sun.com/products/java-media/speech/">Java Speech Grammar Format</A>
   */

  public Rule ruleForJSGF(String JSGFText)
    throws GrammarException;


  /**
   * Set a rule in the grammar either by creating a new rule or
   * updating an existing rule.  The rule being set is identified by its
   * <code>"ruleName"</code> and defined by the <code>Rule</code> object
   * and its <code>isPublic</code> flag.  The <code>setRule</code>
   * method is equivalent to a <em>rule definition</em>
   * in the Java Speech Grammar Format (JSGF).
   * <P>
   *
   * The change in the <code>RuleGrammar</code> takes effect when
   * <A HREF="Grammar.html#commit">grammar changes are committed</A>.
   * <P>
   *
   * The rule object represents the <em>expansion</em> of a
   * JSGF definition (the right hand side).  It may be a 
   * <code>RuleToken</code>,
   * <code>RuleName</code>,
   * <code>RuleAlternatives</code>,
   * <code>RuleSequence</code>,
   * <code>RuleCount</code> or
   * <code>RuleTag</code>.  
   * Each of these 6 object types is an extension of the <code>Rule</code> object.
   * (The rule object cannot be an instance of <code>RuleParse</code> which
   * is also an extension of <code>Rule</code>.)
   * <P>
   *
   * A rule is most easily created from Java Speech Grammar Format text  
   * using the <code>ruleForJSGF</code> method.  e.g.
   * <pre>
   *    gram.setRule(ruleName, gram.ruleForJSGF("open the &lt;object&gt;"), true);
   * </pre>
   *
   * The <code>isPublic</code> flag defines whether this rule 
   * may be enabled and active for recognition and/or imported into other 
   * rule grammars.  It is equivalent to the <code>public</code>
   * declaration in JSGF.
   * <P>
   *
   * If the <code>Rule</code> object contains a fully-qualified reference
   * to a rule of another <code>RuleGrammar</code>, an import is 
   * automatically generated for that rule if it is not already imported.
   * A subsequent call to <code>listImports</code> will return that
   * import statement.
   * <P>
   *
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then they are
   * automatically stripped.
   * <P>
   * 
   * The <code>Rule</code> object passed to the <code>RuleGrammar</code>
   * is copied before being applied to recognizer.  Thus, an application
   * can modify and re-use a <code>rule</code> object without
   * unexpected side-effects.  Also, a different object will be returned by the
   * <code>getRule</code> and <code>getRuleInternal</code> methods
   * (although it will contain the same information).
   * <P>
   *
   * @see #ruleForJSGF
   * @see #getRule
   * @see #getRuleInternal
   * @see Recognizer#commitChanges
   * @see RuleAlternatives
   * @see RuleCount
   * @see RuleName
   * @see RuleSequence
   * @see RuleTag
   * @see RuleToken
   * @param ruleName
   *   unique name of rule being defined (unique for this <code>RuleGrammar</code>)
   * @param rule
   *   logical expansion for the rulename
   * @param isPublic
   *   true if this rule can be imported into other <code>RuleGrammars</code> or enabled
   * @exception IllegalArgumentException 
   *   if rule is not a legal instance of Rule
   * @exception NullPointerException 
   *   if ruleName or rule are null
   */

  public void setRule(String ruleName, Rule rule, boolean isPublic)
    throws NullPointerException, IllegalArgumentException;


  /**  
   * Returns a <code>Rule</code> object for a specified rulename. 
   * Returns <code>null</code> if the rule is unknown.
   * The returned object is a copy of the recognizer's internal object
   * so it can be modified in any way without affecting the recognizer.
   * The <code>setRule</code> method should be called when a change
   * to the returned rule object needs to be applied to the recognizer.
   * <P>
   *
   * The <code>Rule.toString</code> method can be used to convert the return object 
   * to a printable String in Java Speech Grammar Format.
   * <P>
   *
   * If there is a rule structure currently pending for a <code>commitChanges</code>
   * that structure is returned.  Otherwise, the current structure being
   * used by the recognizer on incoming speech is returned. 
   * <P>
   * 
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * If fast, read-only access to the rule object is required (e.g. in
   * parsing), then the application may use <code>getRuleInternal</code>.
   * <P>
   *
   * @see #setRule
   * @see #getRuleInternal
   * @see Recognizer#commitChanges
   * @param ruleName
   *   the <code>rulename</code> to be returned
   * @return
   *   the definition of <code>ruleName</code> or <code>null</code>
   */

  public Rule getRule(String ruleName);


  /**  
   * Returns a reference to a recognizer's internal rule object identified
   * by a rule name.  The application should <em>never</em> modify the returned
   * object.  This method is intented for use by parsers and other 
   * software that needs to quickly analyse a recognizer's grammars
   * without modifying them (without the overhead of making copies,
   * as required by <code>getRule</code>).  If the returned object is ever modified
   * in any way, <code>getRule</code> and <code>setRule</code> should be used.
   * <P>
   *
   * Returns <code>null</code> if the rule is unknown.
   * <P>
   * 
   * If there is a <code>Rule</code> structure currently pending for a 
   * <code>commitChanges</code> that structure is returned.  Otherwise, 
   * the current structure being used by the recognizer on incoming speech 
   * is returned.
   * <P>
   * 
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * @see #setRule
   * @see #getRule
   * @see Recognizer#commitChanges
   */

  public Rule getRuleInternal(String ruleName);


  /**
   * Test whether a rule is public.  Public rules may be enabled to be
   * activated for recognition and/or may be imported into other 
   * <code>RuleGrammars</code>.
   * <P>
   *
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * @param ruleName
   *   the rulename being tested
   * @return true if <code>ruleName</code> is public
   * @exception IllegalArgumentException 
   *   if ruleName is unknown
   */

  public boolean isRulePublic(String ruleName)
    throws IllegalArgumentException;


  /**  
   * List the names of all rules defined in this <code>RuleGrammar</code>.
   * If any rules are pending deletion they are not listed (between a
   * call to <code>deleteRule</code> and a <code>commitChanges</code>
   * taking effect).
   * <P>
   *
   * The returned names do not include the &lt;&gt; symbols.
   * <P>
   */

  public String[] listRuleNames();


  /**  
   * Delete a rule from the grammar.  The deletion only takes effect when
   * <A HREF="Grammar.html#commit">grammar changes are next committed</A>.
   * <P>
   * 
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * @see Recognizer#commitChanges
   * @param ruleName
   *   name of the defined rule to be deleted
   * @exception IllegalArgumentException 
   *   if ruleName is unknown
   */

  public void deleteRule(String ruleName)
    throws IllegalArgumentException;


  /**
   * Set the <code>enabled</code> property for a <code>RuleGrammar</code>.
   * When a grammar is enabled and when the activation conditions
   * are appropriate, the <code>RuleGrammar</code> is activated for
   * recognition and users may speak the commands/words/phrases defined by 
   * the grammar and results will be generated.  The enabled state of a grammar
   * can be tested by the <code>isEnabled</code> method.  The activation 
   * state of a grammar can be tested by the <code>isActive</code> methods.
   * <P>
   *
   * Recognizers can have multiple <code>Grammars</code> enabled and active 
   * at any time.  A change in enabled status of the <code>Grammar</code> 
   * only takes effect when
   * <A HREF="Grammar.html#commit">grammar changes are next committed</A>.
   * and the <code>CHANGES_COMMITTED</code> event is issued.
   * The grammar is only activated once all the activation
   * conditions are met (see documentation for <code>Grammar</code>).
   * <P>
   *
   * All enabled rules of a <code>RuleGrammar</code> share the same
   * <code>ActivationMode</code>.  Thus, when a <code>RuleGrammar</code>
   * is activated or deactivated for recognition, all the enabled
   * rules of the <code>RuleGrammar</code> are  activated or
   * deactivated together.
   * <P>
   *
   * This method is inherited from the <code>Grammar</code> interface.  For
   * a <code>RuleGrammar</code>, <code>setEnabled(true)</code> enables
   * all <em>public</em> rules of the grammar.  <code>setEnabled(false)</code>
   * disables all public rules of the <code>RuleGrammar</code>.
   * A <code>RuleGrammar</code> also provides <code>setEnabled</code>
   * methods for enabling and disabling individual public rules or sets
   * of public rules.
   * <P>
   *
   * It is not an error to enable an enabled grammar 
   * or disable an disable grammar.
   * <P>
   *
   * @see #setEnabled(String, boolean)
   * @see #setEnabled(String[], boolean)
   * @see #isEnabled
   * @see Grammar#setActivationMode
   */

  public void setEnabled(boolean enabled);


  /**
   * Set the enabled property for a specified public rule.
   * This method behaves the same as the 
   * <A HREF="#setEnabled(boolean)">setEnabled(boolean)</A> method
   * except that it affects only a single public rule.  The
   * enabled state of other rules is unchanged.
   * <P>
   *
   * Individual rules of a <code>RuleGrammar</code> may be individually
   * enabled and disabled.  Once any rule is enabled, the
   * <code>RuleGrammar</code> is considered to be enabled.
   * If all rules are disabled, then the <code>RuleGrammar</code> 
   * is considered disabled.
   * <P>
   *
   * A change in the enabled property of a <code>Rule</code> or 
   * a <code>RuleGrammar</code> only takes effect when
   * <A HREF="Grammar.html#commit">grammar changes are next committed</A>.
   * <P>
   *
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * @see #setEnabled(boolean)
   * @see #setEnabled(String[], boolean)
   * @see Recognizer#commitChanges
   * @param ruleName
   *   name of the rule to be enabled or disabled
   * @param enabled
   *   <code>true</code> to enable <code>ruleName</code>, <code>false</code> to disable
   * @exception IllegalArgumentException 
   *    if <code>ruleName</code> is unknown or if it is a non-public rule
   */

  public void setEnabled(String ruleName, boolean enabled)
    throws IllegalArgumentException;


  /**
   * Set the enabled property for a set of public rules of a 
   * <code>RuleGrammar</code>.
   * This method behaves the same as the 
   * <A HREF="#setEnabled(boolean)">setEnabled(boolean)</A> method
   * except that it only affects a defined single public rule.
   * This call does not affect the enabled state of other 
   * public rules of the <code>RuleGrammar</code>.
   * <P>
   *
   * If any one or more rules are enabled, the
   * <code>RuleGrammar</code> is considered to be enabled.
   * If all rules are disabled, then the <code>RuleGrammar</code> 
   * is considered disabled.
   * <P>
   *
   * A change in the enabled property of <code>Rules</code> or 
   * a <code>RuleGrammar</code> only takes effect when
   * <A HREF="Grammar.html#commit">grammar changes are next committed</A>.
   * <P>
   *
   * If any rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * @see Recognizer#commitChanges
   * @param ruleNames
   *   the set of rulenames to be enabled or disabled
   * @param enabled
   *   <code>true</code> to enabled rulenames, <code>false</code> to disable
   * @exception IllegalArgumentException 
   *    if one or more <code>ruleNames</code> is unknown or if any is a non-public rule
   */

  public void setEnabled(String ruleNames[], boolean enabled)
    throws IllegalArgumentException;


  /**
   * Returns true if any public rule of a <code>RuleGrammar</code> is enabled.
   * <P>
   *
   * @return <code>true</code> if <code>RuleGrammar</code> is enabled, otherwise <code>false</code>
   */

  public boolean isEnabled();


  /**
   * Test whether recognition of a specified rule of this 
   * <code>RuleGrammar</code> is enabled.
   * If <code>ruleName</code> is null, the method is equivalent to 
   * <code>isEnabled()</code>.
   * <P>
   *
   * If the rule name contains both start/end angle brackets 
   * (e.g. <CODE>"&lt;ruleName&gt;"</CODE>), then the brackets are ignored.
   * <P>
   *
   * @exception IllegalArgumentException 
   *    if ruleName is unknown or if it is a non-public rule
   * @param ruleName
   *   name of the rule being tested
   * @return <code>true</code> if <code>ruleName</code> is enabled, otherwise <code>false</code>
   */

  public boolean isEnabled(String ruleName)
    throws IllegalArgumentException;


  /**
   * Resolve a rulename reference within a <code>RuleGrammar</code> to a
   * fully-qualified rulename.  The input rulename may be a simple rulename,
   * qualified or fully-qualified.  If the rulename cannot be resolved,
   * the method returns <code>null</code>.
   * <P>
   *
   * If the rulename being resolved is a local reference, the return
   * value is a fully-qualified rulename with its grammar part being
   * the name of this <code>RuleGrammar</code>.
   * <P>
   *
   * Example: in a grammar that imports the rule &lt;number&gt; from
   * the grammar <code>"com.sun.examples"</code>, the following 
   * would return the fully-qualified rulename 
   * <code>com.sun.examples.number</code>.
   *
   * <pre>
   *   gram.resolve(new RuleName("number")); </pre>
   *
   * If the input rulename is a fully-qualified rulename, then the method
   * checks whether that rulename exists (and could therefore be successfully
   * referenced in this <code>RuleGrammar</code>).  If the rulename exists, then
   * the retun value is the same as the input value, otherwise the
   * method returns <code>null</code>.
   * <P>
   *
   * @param ruleName
   *   reference to rulename to be resolved
   * @return fully-qualified reference to a rulename
   * @exception GrammarException
   *   if an error is found in the definition of the <code>RuleGrammar</code>
   *   or if <code>rulename</code> is an ambiguous reference
   */

  public RuleName resolve(RuleName ruleName)
    throws GrammarException;


  /**
   * Import all rules or a specified rule from another grammar.
   * The <code>RuleName</code> should be in one these forms:
   *
   * <PRE>
   *   &lt;package.grammar.ruleName&gt;
   *   &lt;package.grammar.*&gt;
   *   &lt;grammar.ruleName&gt; </PRE>
   *   &lt;grammar.*&gt; </PRE>
   *
   * which are equivalent to the following declarations in the
   * Java Speech Grammar Format.
   *
   * <PRE>
   *   // import all public rules of a grammar
   *   import &lt;package.grammar.*&gt;        
   *   import &lt;grammar.*&gt;        
   *
   *   // import a specific public rule name of a grammar
   *   import &lt;package.grammar.ruleName&gt;
   *   import &lt;grammar.ruleName&gt; </PRE>
   *
   * The forms without a package are only legal when the
   * <em>full grammar name</em> does not include a package name.
   * <P>
   *
   * The <code>addImport</code> takes effect when 
   * <A HREF="Grammar.html#commit">grammar changes are committed</A>.
   * When changes are committed, all imports must be resolvable.  
   * Specifically, every <code>RuleGrammar</code> listed
   * in an import must exist, and every fully qualified rulename
   * listed in an import must exist.  If any ommissions are found,
   * the <code>commitChanges</code> method throws an exception and
   * the changes do not take effect.
   * <P>
   *
   * It is not an exception to import a rule or set of rules and not
   * reference them.
   * <P>
   * 
   * @see #listImports
   * @see #removeImport
   * @see Recognizer#commitChanges
   */

  public void addImport(RuleName importName);


  /**
   * Remove an import.  The name follows the format of <code>addImport</code>.
   * <P>
   *
   * The change in imports only takes effect when
   * <A HREF="Grammar.html#commit">grammar changes are committed</A>.
   * <P>
   *
   * @see #addImport
   * @see Recognizer#commitChanges
   * @exception IllegalArgumentException 
   *    if importName is not currently imported
   */

  public void removeImport(RuleName importName)
    throws IllegalArgumentException;


  /**
    * Return a list of the current imports.
    * Returns zero length array if no <code>RuleGrammars</code> are imported.
    * <P>
    *
    * @see #addImport
    * @see #removeImport
    */

  public RuleName[] listImports();


  /**
   * Parse a <code>String</code> against a <code>RuleGrammar</code>.  
   * Parsing is the process of matching the text against the rules that 
   * are defined in the grammar.
   * The text is tokenized as a Java Speech Grammar Format string
   * (white-spacing and quotes are significant).
   * <P>
   *
   * The string is parsed against the rule identified by <code>ruleName</code>,
   * which may identify any defined rule of this rule grammar 
   * (public or private, enabled or disabled).
   * If the <code>ruleName</code> is <code>null</code>, the string is
   * parsed against <em>all enabled</em> rules of the grammar.  
   * <P>
   *
   * The method returns a <code>RuleParse</code> object.  The documentation
   * <code>RuleParse</code> describes how the parse structure
   * and grammar structure correspond.
   * <P>
   *
   * If parse fails, then the return value is <code>null</code>.
   * To succeed, the parse must match the complete input string.
   * <P>
   *
   * For some grammars and strings, multiple parses are legal.
   * The parse method returns only one.  It is not defined 
   * which of the legal parses should be returned.
   * Development tools will help to analyse grammars for such
   * ambiguities.  Also grammar libraries can be used to parse
   * results to check for these ambiguities.
   * <P>
   * 
   * @see #parse(String[], String)
   * @see #parse(FinalRuleResult, int, String)
   * @exception GrammarException 
   *   if an error is found in the definition of the <code>RuleGrammar</code>
   */

  public RuleParse parse(String text, String ruleName)
    throws GrammarException;


  /**
   * Parse a sequence of tokens against a <code>RuleGrammar</code>.    
   * In all other respects this parse method is equivalent to the
   * <code>parse(String text, String ruleName)</code> method 
   * except that the string is pre-tokenized.
   * <P>
   *
   * @see #parse(String, String)
   * @see #parse(FinalRuleResult, int, String)
   * @exception GrammarException 
   *   if an error is found in the definition of the <code>RuleGrammar</code>
   */

  public RuleParse parse(String tokens[], String ruleName)
    throws GrammarException;


  /**
   * Parse the nth best result of a <code>FinalRuleResult</code> against
   * a <code>RuleGrammar</code>.  In other respects this parse method is
   * equivalent to the <code>parse(String text, String ruleName)</code>
   *  method described above.
   * <P>
   *
   * A rejected result (<code>REJECTED</code> state) is not guaranteed to parse.
   * Also, if the <code>RuleGrammar</code> has been modified since the result
   * was issued, parsing is not guaranteed to succeed.
   * <P>
   *
   * @see #parse(String, String)
   * @see #parse(String[], String)
   * @exception GrammarException 
   *   if an error is found in the definition of the <code>RuleGrammar</code>
   */

  public RuleParse parse(FinalRuleResult finalRuleResult, int nBest, String ruleName)
    throws GrammarException;


  /**
   * Return a string containing a specification for this <code>RuleGrammar</code>
   * in Java Speech Grammar Format.  The string includes the grammar name
   * declaration, import statements and all the rule definitions.
   * When sending JSGF to a stream (e.g. a file) the application should prepend 
   * the header line with the appropriate character encoding information:
   *
   * <pre>
   *    #JSGF V1.0 encoding-format;
   * </pre>
   * <P>
   *
   * @see <A HREF="http://java.sun.com/products/java-media/speech/">Java Speech Grammar Format</A>
   */

  public String toString();
}



