/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Defines an extension to the <code>EngineListener</code> interface 
 * for specific events associated with a <code>Recognizer</code>.  
 * A <code>RecognizerListener</code> object is attached to and
 * removed from a <code>Recognizer</code> by calls to the 
 * <code>addEngineListener</code> and <code>removeEngineListener</code>
 * methods (which <code>Recognizer</code> inherits from the
 * <code>Engine</code> interface).
 * <P>
 * 
 * The <code>RecognizerAdapter</code> class provides a trivial 
 * implementation of this interface.
 * <P>
 *
 * @see RecognizerAdapter
 * @see RecognizerEvent
 * @see Recognizer
 * @see Engine
 * @see Engine#addEngineListener
 * @see Engine#removeEngineListener
 */

public interface RecognizerListener extends EngineListener
{
  /**
   * A <code>RECOGNIZER_PROCESSING</code> event has been issued
   * as a <code>Recognizer</code> changes from the <code>LISTENING</code>
   * state to the <code>PROCESSING</code> state.
   * <P>
   *
   * @see RecognizerEvent#RECOGNIZER_PROCESSING
   */

  public void recognizerProcessing(RecognizerEvent e);


  /**
   * A <code>RECOGNIZER_SUSPENDED</code> event has been issued as a
   * <code>Recognizer</code> changes from either the <code>LISTENING</code>
   * state or the <code>PROCESSING</code> state to the
   * <code>SUSPENDED</code> state.
   * <P>
   *
   * A <code>Result</code> finalization event (either a
   * <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code> event)
   * is issued immediately following the <code>RECOGNIZER_SUSPENDED</code> event.
   * <P>
   *
   * @see RecognizerEvent#RECOGNIZER_SUSPENDED
   */

  public void recognizerSuspended(RecognizerEvent e);


  /**
   * A <code>CHANGES_COMMITTED</code> event has been issued as a
   * <code>Recognizer</code> changes from the <code>SUSPENDED</code>
   * state to the <code>LISTENING</code> state and resumed recognition.
   * <P>
   *
   * The <code>GRAMMAR_CHANGES_COMMITTED</code> event is issued
   * to the <code>GrammarListeners</code> of all changed grammars
   * immediately following the <code>CHANGES_COMMITTED</code> event. 
   * <P>
   *
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see GrammarEvent#GRAMMAR_CHANGES_COMMITTED
   */

  public void changesCommitted(RecognizerEvent e);


  /**
   * <code>FOCUS_GAINED</code> event has been issued as a
   * <code>Recognizer</code> changes from the <code>FOCUS_OFF</code>
   * state to the <code>FOCUS_ON</code> state.  A 
   * <code>FOCUS_GAINED</code> event typically follows
   * a call to <code>requestFocus</code> on a <code>Recognizer</code>.
   * <P>
   *
   * The <code>GRAMMAR_ACTIVATED</code> event is issued
   * to the <code>GrammarListeners</code> of all activated grammars
   * immediately following this <code>RecognizerEvent</code>.
   * <P>
   *
   * @see RecognizerEvent#FOCUS_GAINED
   * @see GrammarEvent#GRAMMAR_ACTIVATED
   * @see Recognizer#requestFocus
   */

  public void focusGained(RecognizerEvent e);


  /**
   * <code>FOCUS_LOST</code> event has been issued as a
   * <code>Recognizer</code> changes from the <code>FOCUS_ON</code>
   * state to the <code>FOCUS_OFF</code> state.  A 
   * <code>FOCUS_LOST</code> event may follow
   * a call to <code>releaseFocus</code> on a <code>Recognizer</code>
   * or follow a request for focus by another application.
   * <P>
   *
   * The <code>GRAMMAR_DEACTIVATED</code> event is issued
   * to the <code>GrammarListeners</code> of all deactivated grammars
   * immediately following this <code>RecognizerEvent</code>.
   * <P>
   *
   * @see RecognizerEvent#FOCUS_LOST
   * @see GrammarEvent#GRAMMAR_DEACTIVATED
   * @see Recognizer#releaseFocus
   */

  public void focusLost(RecognizerEvent e);
}


