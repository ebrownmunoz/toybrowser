/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech.recognition;

import javax.speech.*;


/**
 * Signals an error caused by an illegal call to a method
 * of <code>FinalResult</code>, <code>FinalRuleResult</code> 
 * or <code>FinalDictationResult</code>.
 * <P>
 *
 * Methods of these three interfaces of a result can only
 * be called for a finalized <code>Result</code>.  The
 * <code>getResultState</code> method of the <code>Result</code>
 * interface tests the result state and must return
 * either <code>ACCEPTED</code> or <code>REJECTED</code>.
 * Furthermore, the methods of <code>FinalRuleResult</code>
 * should only be called for a finalized result matching
 * a <code>RuleGrammar</code>.  Similarly, the methods of
 * <code>FinalDictationResult</code> should only 
 * be called for a finalized result matching
 * a <code>DictationGrammar</code>.
 */

public class ResultStateError extends SpeechError
{
  /**
   * Construct a <code>ResultStateError</code> with a message string.
   */

  public ResultStateError(String s)
  {
    super(s);
  }


  /**
   * Empty constructor for <code>ResultStateError</code>.
   */

  public ResultStateError()
  {
    super();
  }
}

