/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;

/**
 * <code>FinalResult</code> is an extension to the <code>Result</code>
 * interface that provides information about a result that has been
 * finalized - that is, recognition is complete.  A finalized result
 * is a <code>Result</code> that has received either a
 * <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code>
 * <code>ResultEvent</code> that puts it in either the <code>ACCEPTED</code>
 * or <code>REJECTED</code> state (indicated by the <code>getResultState</code>
 * method of the <code>Result</code> interface).
 * <P>
 *
 * The <code>FinalResult</code> interface provides information for
 * finalized results that match either a <code>DictationGrammar</code>
 * or a <code>RuleGrammar</code>.
 * <P>
 *
 * Any result object provided by a recognizer implements both the
 * <code>FinalRuleResult</code> and <code>FinalDictationResult</code>
 * interfaces.  Because both these interfaces extend the <code>FinalResult</code>
 * interface, which in turn extends the <code>Result</code> interface,
 * all results implement <code>FinalResult</code>.
 * <P>
 *
 * The methods of the <code>FinalResult</code> interface provide information
 * about a <em>finalized</em> result (<code>ACCEPTED</code> or
 * <code>REJECTED</code> state).  If any method of the <code>FinalResult</code>
 * interface is called on a result in the <code>UNFINALIZED</code> state, a
 * <code>ResultStateError</code> is thrown.
 * <P>
 *
 * Three capabilities can be provided by a finalized result:
 * training/correction, access to audio data, and access to alternative guesses.
 * All three capabilities are optional because they are not all relevant
 * to all results or all recognition environments, and they are
 * not universally supported by speech recognizers.
 * Training and access to audio data are provided by the
 * <code>FinalResult</code> interface.  Access to alternative guesses
 * is provided by the <code>FinalDictationResult</code> and
 * <code>FinalRuleResult</code> interfaces (depending upon the type
 * of grammar matched).
 * <P>
 *
 * <B>Training / Correction</B>
 * <P>
 *
 * Because speech recognizers are not always correct, applications need
 * to consider the possibility that a recognition error has occurred.
 * When an application detects an error (e.g. a user updates a result),
 * the application should inform the recognizer so that it can learn
 * from the mistake and try to improve future performance.
 * The <code>tokenCorrection</code> is provided for an application to provide
 * feedback from user correction to the recognizer.
 * <P>
 *
 * Sometimes, but certainly not always, the correct result is
 * selected by a user from amongst the N-best alternatives for a
 * result obtained through either the <code>FinalRuleResult</code>
 * or <code>FinalDictationResult</code> interfaces.  In other cases,
 * a user may type the correct result or the application may infer
 * a correction from following user input.
 * <P>
 *
 * Recognizers must store considerable information to support
 * training from results.  Applications need to be involved in
 * the management of that information so that it is not stored
 * unnecessarily.  The <code>isTrainingInfoAvailable</code> method
 * tests whether training information is available for a finalized result.
 * When an application/user has finished correction/training for a result
 * it should call <code>releaseTrainingInfo</code> to free up
 * system resources.  Also, a recognizer may choose at any time to free
 * up training information.  In both cases, the application is
 * notified of the the release with a <code>TRAINING_INFO_RELEASED</code>
 * event to <code>ResultListeners</code>.
 * <P>
 *
 * <B>Audio Data</B>
 * <P>
 *
 * Audio data for a finalized result is optionally provided by recognizers.
 * In dictation systems, audio feedback to users can remind them of
 * what they said and is useful in correcting and proof-reading documents.
 * Audio data can be stored for future use by an application or user and
 * in certain circumstances can be provided by one recognizer to another.
 * <P>
 *
 * Since storing audio requires substantial system resources,
 * audio data requires special treatment.  If an application wants to
 * use audio data, it should set the <code>setResultAudioProvided</code>
 * property of the <code>RecognizerProperties</code> to <code>true</code>.
 * <P>
 *
 * Not all recognizers provide access to audio data.  For those
 * recognizers, <code>setResultAudioProvided</code> has no effect,
 * the <code>FinalResult.isAudioAvailable</code> always returns
 * <code>false</code>, and the <code>getAudio</code>
 * methods always return <code>null</code>.
 * <P>
 *
 * Recognizers that provide access to audio data cannot always provide
 * audio for every result.  Applications should test audio availability
 * for every <code>FinalResult</code> and should always test for
 * <code>null</code> on the <code>getAudio</code> methods.
 * <P>
 *
 * @see Result
 * @see Result#getResultState
 * @see Result#ACCEPTED
 * @see Result#REJECTED
 * @see FinalDictationResult
 * @see FinalRuleResult
 * @see DictationGrammar
 * @see RuleGrammar
 * @see RecognizerProperties#setResultAudioProvided
 */


public interface FinalResult extends Result
{
  /* **************************** TRAINING **************************** */

  /**
   * The <code>MISRECOGNITION</code> flag is used in a call to
   * <code>tokenCorrection</code> to indicate that the change is
   * a correction of an error made by the recognizer.
   * <P>
   *
   * @see #tokenCorrection
   */

  public static int MISRECOGNITION = 400;


  /**
   * The <code>USER_CHANGE</code> flag is used in a call to
   * <code>tokenCorrection</code> to indicate that the user has
   * modified the text that was returned by the recognizer to
   * something different from what they actually said.
   * <P>
   *
   * @see #tokenCorrection
   */

  public static int USER_CHANGE = 401;


  /**
   * The <code>DONT_KNOW</code> flag is used in a call to <code>tokenCorrection</code>
   * to indicate that the application does not know whether a
   * change to a result is because of <code>MISRECOGNITION</code>
   * or <code>USER_CHANGE</code>.
   * <P>
   *
   * @see #tokenCorrection
   */

  public static int DONT_KNOW = 402;



  /**
   * Returns <code>true</code> if the <code>Recognizer</code>
   * has training information available for this result.
   * Training is available if the following conditions are met:
   * <P>
   *
   * <UL>
   *   <LI>The <code>isTrainingProvided</code> property of the
   *       <code>RecognizerProperties</code> is set to <code>true</code>.
   *   <LI>And, the training information for this result has not been released
   *       by the application or by the recognizer.
   *      (The <code>TRAINING_INFO_RELEASED</code> event has not been issued.)
   * </UL>
   * <P>
   *
   * Calls to <code>tokenCorrection</code> have no effect if the training
   * information is not available.
   * <P>
   *
   * @see RecognizerProperties#setTrainingProvided(boolean)
   * @see #releaseTrainingInfo()
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public boolean isTrainingInfoAvailable()
    throws ResultStateError;


  /**
   * Release training information for this <code>FinalResult</code>.
   * The release frees memory used for the training information --
   * this information can be substantial.
   * <P>
   *
   * It is not an error to call the method when training information
   * is not available or has already been released.
   * <P>
   *
   * This method is asynchronous - the training info is not
   * necessarily released when the call returns.
   * A <code>TRAINING_INFO_RELEASED</code> event is issued to
   * the <code>ResultListener</code> once the information is released.
   * The <code>TRAINING_INFO_RELEASED</code> event is also issued if the
   * recognizer releases the training information for any other reason
   * (e.g. to reclaim memory).
   * <P>
   *
   * @see ResultEvent#TRAINING_INFO_RELEASED
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public void releaseTrainingInfo()
    throws ResultStateError;


  /**
   * Inform the recognizer of a correction to one of more tokens in
   * a finalized result so that the recognizer can re-train itself.
   * Training the recognizer from its mistakes allows it to improve
   * its performance and accuracy in future recognition.
   * <P>
   *
   * The <code>fromToken</code> and <code>toToken</code> parameters
   * indicate the inclusive sequence of best-guess or alternative
   * tokens that are being trained or corrected.  If <code>toToken</code> is
   * <code>null</code> or if <code>fromToken</code> and <code>toToken</code>
   * are the same, the training applies to a single recognized token.
   * <P>
   *
   * The <code>correctTokens</code> token sequence may have the
   * same of a different length than the token sequence being corrected.
   * Setting <code>correctTokens</code> to <code>null</code> indicates
   * the deletion of tokens.
   * <P>
   *
   * The <code>correctionType</code> parameter must be one of <code>MISRECOGNITION</code>,
   * <code>USER_CHANGE</code>, <code>DONT_KNOW</code>.
   * <P>
   *
   * <em>Note:</em> <code>tokenCorrection</code> does not change the result object.
   * So, future calls to the <code>getBestToken</code>, <code>getBestTokens</code>
   * and <code>getAlternativeTokens</code> method return exactly the same values as
   * before the call to <code>tokenCorrection</code>.
   * <P>
   *
   * @see #MISRECOGNITION
   * @see #USER_CHANGE
   * @see #DONT_KNOW
   *
   * @param correctTokens
   *   sequence of correct tokens to replace <code>fromToken</code> to <code>toToken</code>
   * @param fromToken
   *   first token in the sequence being corrected
   * @param toToken
   *   last token in the sequence being corrected
   * @param correctionType
   *   type of correction: <code>MISRECOGNITION</code>, <code>USER_CHANGE</code>,
   *    <code>DONT_KNOW</code>
   *
   * @exception IllegalArgumentException
   *   either token is not from this <code>FinalResult</code>
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public void tokenCorrection(String correctTokens[],
			  ResultToken fromToken, ResultToken toToken,
			  int correctionType)
    throws ResultStateError, IllegalArgumentException;


  /* **************************** AUDIO **************************** */

  /**
   * Test whether result audio data is available for this result.
   * Result audio is only available if:
   * <P>
   *
   * <UL>
   *   <LI>The <code>ResultAudioProvided</code> property of
   *       <code>RecognizerProperties</code> was set to <code>true</code>
   *       when the result was recognized.
   *   <LI>The <code>Recognizer</code> was able to collect result audio for
   *       the current type of <code>FinalResult</code>
   *       (<code>FinalRuleResult</code> or <code>FinalDictationResult</code>).
   *   <LI>The result audio has not yet been released.
   * </UL>
   * <P>
   *
   * The availability of audio for a result does not mean that all
   * <code>getAudio</code> calls will return an <code>AudioClip</code>.
   * For example, some recognizers might provide audio data only for
   * the entire result or only for individual tokens, or not for
   * sequences of more than one token.
   * <P>
   *
   * @see #getAudio
   * @see RecognizerProperties#setResultAudioProvided
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public boolean isAudioAvailable()
    throws ResultStateError;


  /**
   * Release the result audio for the result.  After audio is
   * released, <code>isAudioAvailable</code> will return <code>false</code>.
   * This call is ignored if result audio is not available or
   * has already been released.
   * <P>
   *
   * This method is asynchronous - audio data is not necessarily
   * released immediately.  A <code>AUDIO_RELEASED</code> event
   * is issued to the <code>ResultListener</code> when the audio is released
   * by a call to this method.  A <code>AUDIO_RELEASED</code> event is also
   * issued if the recognizer releases the audio for some other reason
   * (e.g. to reclaim memory).
   * <P>
   *
   * @see ResultEvent#AUDIO_RELEASED
   * @see ResultListener#audioReleased
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public void releaseAudio()
    throws ResultStateError;
}
