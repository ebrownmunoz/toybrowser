/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
  * <code>RuleSequence</code> is a <code>Rule</code> composed of a sequence of 
  * sub-rules that must each be spoken in order.  If there are zero rules in the
  * sequence, the sequence is equivalent to 
  * <code>&lt;NULL&gt;</code>.
  *
  * @see Rule
  * @see RuleAlternatives
  * @see RuleCount
  * @see RuleGrammar
  * @see RuleName
  * @see RuleParse
  * @see RuleTag
  * @see RuleToken
  * @see RuleName#NULL
  */


public class RuleSequence extends Rule
{
  /**
   * Set of rules to be spoken in sequence.
   * <P>
   *
   * @see #getRules
   * @serial
   */

  protected Rule[] rules;


  /**
   * Construct a <code>RuleSequence</code> object with an array of sub-rules.
   */

  public RuleSequence(Rule[] rules)
    {
      setRules(rules);
    }


  /**
   * Construct a <code>RuleSequence</code> object containing a single <code>Rule</code>.
   */

  public RuleSequence(Rule rule)
    {
      Rule[] tmp = new Rule[1];
      tmp[0] = rule;

      setRules(tmp);
    }


  /**
   * Empty constructor creates a sequence with zero rules.
   * A sequence with zero rules is equivalent to 
   * <code>&lt;NULL&gt;</code>.
   * <P>
   *
   * @see RuleName#NULL
   */

  public RuleSequence()
    {
      setRules(null);
    }


  /**
   * Constructor for <code>RuleSequence</code> that is a sequence of 
   * strings that are converted to <code>RuleTokens</code>.
   * <P>
   *
   * A string containing multiple words (e.g. "san francisco") is treated as 
   * a single token. If appropriate, an application should parse such strings 
   * to produce separate tokens. 
   * <P>
   *
   * The token list may be zero-length or null. This will produce a zero-length
   * sequence which is equivalent to <code>&lt;NULL&gt;</code>.
   * <P>
   *
   * @see RuleToken
   */

  public RuleSequence(String tokens[])
    {
      if (tokens == null) tokens = new String[0];

      rules = new Rule[tokens.length];

      for (int i = 0; i < tokens.length; i++)
	rules[i] = new RuleToken(tokens[i]);
    }
  


  /**
   * Return the array of rules in the sequence.
   */

  public Rule[] getRules() {return rules;}


  /**
   * Set the array of rules in the sequence.
   * The array may be zero-length or null. This will produce a zero-length
   * sequence which is equivalent to <code>&lt;NULL&gt;</code>.
   * <P>
   */

  public void setRules(Rule[] rules)
    {
      if (rules == null) rules = new Rule[0];

      this.rules = rules;
    }


  /**
   * Append a single rule to the end of the sequence.
   */

  public void append(Rule rule)
    {
      if (rule == null)
	throw new NullPointerException("null rule to append");

      int len = rules.length;

      Rule[] newRules = new Rule[len + 1];
      System.arraycopy(rules, 0, newRules, 0, len);
      newRules[len] = rule;

      this.rules = newRules;
    }


  /**
   * Return a deep copy of this rule.
   * See the <A HREF="Rule.html#copy()"><code>Rule.copy</code></A>
   * documentation for an explanation of deep copy.
   */

  public Rule copy()
  {
    Rule[] rulesCopy = null;

    if (rules != null) {
      rulesCopy = new Rule[rules.length];
      for (int i=0; i<rules.length; i++)
	rulesCopy[i] = rules[i].copy();
    }

    return new RuleSequence(rulesCopy);
  }


  /**
   * Return a String representing this <code>RuleSequence</code> object as partial 
   * Java Speech Grammar Format.
   */

  public String toString()
    {
      if (rules.length == 0)
	return "<NULL>";

      StringBuffer buf = new StringBuffer();

      for (int i = 0; i < rules.length; i++)
	{
	  if (i > 0) buf.append(' ');

	  // Group RuleAlternatives because they have lower precedence
	  // Group RuleSequence to preserve heirarchy.

	  if (rules[i] instanceof RuleAlternatives || rules[i] instanceof RuleSequence)
	    buf.append("( " + rules[i].toString() + " )");
	  else
	    buf.append(rules[i].toString());
	}

      return buf.toString();
    }
}


