/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * A token (usually a word) contained by a <code>Result</code>
 * representing something heard by a recognizer.
 * For a result of a <code>RuleGrammar</code> a <code>ResultToken</code>
 * contains the same string as the defining <code>RuleToken</code> in the
 * <code>RuleGrammar</code>.  For a result of a <code>DictationGrammar</code>,
 * the tokens are defined by the recognizer.
 * <P>
 *
 * For any result, best guess finalized tokens are obtained from
 * the <code>getBestToken</code> and <code>getBestTokens</code>
 * methods of the <code>Result</code> interface.  For a finalized
 * result matching a <code>RuleGrammar</code> or a finalized
 * result matching a <code>DictationGrammar</code> alternative
 * guesses are available through the <code>getAlternativeTokens</code>
 * methods of the <code>FinalRuleResult</code> and
 * <code>FinalDictationResult</code> interfaces respectively.
 * <P>
 *
 * The <code>ResultToken</code> provides the following information:
 * <UL>
 *   <LI>Required: Spoken-form text
 *   <LI>Required: reference to the <code>Result</code> that contains this token
 *   <LI>Optional: Start and end time
 *   <LI>Dictation only: Written-form text
 *   <LI>Dictation only: Presentation hints (capitalization and spacing)
 * </UL>
 *
 * <A NAME="Spoken_Written"><b>Spoken vs. Written Form</B></A>
 * <P>
 *
 * The distinction between spoken and written forms of a
 * <code>ResultToken</code> applies only to results for a
 * <code>DictationGrammar</code>.  For a result matching a
 * <code>RuleGrammar</code>, the written and spoken forms are identical.
 * <P>
 *
 * The <em>spoken form</em> is a printable string that indicates what the
 * recognizer heard the user say.  In a dictation application,
 * the spoken form is typically used when displaying unfinalized tokens.
 * <P>
 *
 * The <em>written form</em> is a printable string that indicates how
 * the recognizer thinks the token should be printed in text.
 * In a dictation application, the written form is typically used
 * when displaying finalized tokens.
 * <P>
 *
 * For example, in English, a recognizer might return "twenty" for the
 * spoken form and "20" for the written form.
 * <P>
 *
 * Recognizers perform the conversion from spoken to written
 * form on a per-token basis.  For example, "nineteen thirty
 * eight" is three tokens.  The written form would also be
 * three tokens: "19 30 8".  Applications may use additional
 * processing to convert such token sequences to "1938".
 * <P>
 *
 * For some <code>ResultTokens</code>, the written form is a single Unicode
 * character.  Amongst these are the following important
 * formatting characters (shown here as spoken form for US English, written
 * form as a Unicode character number):
 *
 * <UL>
 *   <LI>New line character is "\u005Cu000A" and equals the
 *       static string <code>NEW_LINE</code>.
 *       <BR>In English, it might be spoken as "new line", "line break" or similar.
 *   <LI>New paragraph character is "\u005Cu2029" and equals the
 *       static string <code>NEW_PARAGRAPH</code>.
 *       <BR>In English, it might be spoken as "new paragraph",
 *       "start paragraph" or something similar.
 * </UL>
 *
 * The following are examples of punctuation characters given with
 * sample spoken forms and the corresponding written form.
 * The spoken form may vary between recognizers and one recognizer
 * may even allow one punctuation character to be spoken multiple ways.
 * Also the set of characters may be engine-specific and language-specific.
 *
 * <UL>
 *   <LI>"space bar" -&gt; "\u0020" (u0020)
 *   <LI>"exclamation mark", "exclamation point" -&gt; "\u0021" (u0021)
 *   <LI>"open quote", "begin quote", "open-\"" -&gt; "\u005C\u0022" (u0022) (single quote char)
 *   <LI>"dash", "hyphen", "minus" -&gt; "\u002D" (u002D)
 *   <LI>"pound sign" -&gt; "\u00A3" (u00A3)
 *   <LI>"yen sign" -&gt; "\u00A5" (u00A5)
 * </UL>
 *
 *
 * <A NAME="Presentation"><b>Presentation Hints</B></A>
 * <P>
 *
 * Note: results for rule grammars do not provide presentation hints.
 * Default values are returns for both <code>SpacingHint</code> and
 * <code>CapitalizationHint</code>.
 * <P>
 *
 * Applications use the presentation hints as a guide to
 * rendering the written-form tokens into complete strings.
 * The two hints are <code>SpacingHint</code> and
 * <code>CapitalizationHint</code>.
 * <P>
 *
 * <code>SpacingHint</code> is an <code>int</code> with several
 * flags indicating how the token should be spaced.
 *
 * <UL>
 *   <LI> <code>SpacingHint</code>==<code>SEPARATE</code>
 *        (value of 0) when all the flags are false.
 *        The token should be surrounding by preceding and following
 *        spaces.
 *   <LI> <code>ATTACH_PREVIOUS</code>:
 *        Flag is <code>true</code> if the token should be attached
 *        to the previous token: i.e. no space between this token and the previous token.
 *   <LI> <code>ATTACH_FOLLOWING</code>:
 *        Flag is <code>true</code> if the token should be attached
 *        to the following token: i.e. no space between this token and the following token.
 *   <LI> <code>ATTACH_GROUP</code>:
 *        If this flag is <code>true</code> and if the <code>ATTACH_GROUP</code>
 *        flag for a previous and/or following token is <code>true</code>,
 *        then override the other spacing flags and put no space between
 *        the tokens in the group.
 * </UL>
 *
 * The <code>ATTACH_GROUP</code> is the least obvious flag.  In English,
 * a common use of this flag is for sequence of digits.  If the user says
 * four tokens "3" "point" "1" "4" (point='.'), and these tokens
 * all have the <code>ATTACH_GROUP</code> flag set, then they are joined.
 * The presentation string is "3.14".  The name of the flag implies that
 * tokens in the same "group" should have no spaces between them.
 * <P>
 *
 * "previous" means the previously spoken token in the sequence of tokens -
 * that is, previous in time.  For left-to-right languages (e.g. English, German)
 * <code>ATTACH_PREVIOUS</code> means left attachment - no space to the left.
 * For right-to-left languages (e.g. Arabic, Hebrew) <code>ATTACH_PREVIOUS</code>
 * means right attachment - no space to the right.
 * The converse is true for <code>ATTACH_FOLLOWING</code>.
 * <P>
 *
 * The spacing flags are OR'ed (Java '|' operator on integers).
 * A legal value might be <code>(ATTACH_PREVIOUS | ATTACH_FOLLOWING)</code>.
 * The <code>SEPARATE</code> value is 0 (zero).
 * A flag can be tested by the following code:
 *
 * <pre>
 *   // if attach previous ...
 *   if ((token.getSpacingHint() & ResultToken.ATTACH_PREVIOUS) != 0)
 *     ...
 * </pre>
 *
 * <code>capitalizationHint</code> indicates how the written form
 * of the <em>following</em> token should be capitalized.  The options are
 *
 * <UL>
 *   <LI> <code>CAP_AS_IS</code>:
 *        As-is indicates the capitalization of the spoken form of
 *        the following should not be changed
 *   <LI> <code>CAP_FIRST</code>:
 *        Capitalize first character of the spoken form of the following token
 *   <LI> <code>UPPERCASE</code>: All uppercase following token
 *   <LI> <code>LOWERCASE</code>: All lowercase following token
 * </UL>
 *
 * The Internationalized case conversion methods of the <code>java.lang.String</code>
 * are recommended for implementing the capitalization hints.
 * <P>
 *
 * <A NAME="Null Written Form"><b>Null Written Form</B></A>
 * <P>
 *
 * Some spoken directives to recognizers produce tokens that have
 * no printable form.  These tokens return null for the written form.
 * Typically, these directives give explicit capitalization or
 * spacing hints.  The spoken forms of these tokens should be
 * non-null (to allow the application to provide appropriate feedback
 * to a user.  Example directives for English might include:
 *
 * <UL>
 *   <LI> "Capitalize next", "Cap next", "Upper case"
 *   <LI> "Lowercase"
 *   <LI> "Uppercase"
 *   <LI> "No space"
 * </UL>
 *
 * For these tokens, the interpretation of the capitalization and
 * spacing hints is specialized.  If the spacing hint
 * differs from the default, <code>SEPARATE</code>, it overrides
 * the spacing hint of the next non-null token.
 * If the capitalization hint differs from the default,
 * <code>CAP_AS_IS</code>, it overrides the capitalization hints of
 * previous non-null token (which in fact applies to the following
 * token also).
 * <P>
 *
 * <A NAME="Example"><b>Example</B></A>
 * <P>
 *
 * This example shows how a string of result tokens should
 * be processed to produce a single printable string.
 * The following is a sequence of tokens in a <code>FinalDictationResult</code>
 * shown as spoken form, written form, and spacing and capitalization hints.
 *
 * <OL>
 *   <LI>"NEW_LINE", "\u005Cu000A", SEPARATE, CAP_FIRST
 *   <LI>"the", "the", SEPARATE, CAP_AS_IS
 *   <LI>"UPPERCASE_NEXT", "", SEPARATE, UPPERCASE
 *   <LI>"index", "index", SEPARATE, CAP_AS_IS
 *   <LI>"is", "is" SEPARATE, CAP_AS_IS
 *   <LI>"seven", "7", ATTACH_GROUP, CAP_AS_IS
 *   <LI>"-", "-", ATTACH_GROUP, CAP_AS_IS
 *   <LI>"two", "2", ATTACH_GROUP, CAP_AS_IS
 *   <LI>"period", ".", ATTACH_PREVIOUS, CAP_FIRST
 * </OL>
 *
 * that could be converted to <code>"\nThe INDEX is 7-2."</code>
 * <P>
 *
 * @see Result
 * @see FinalResult
 * @see FinalDictationResult
 * @see FinalRuleResult
 */


public interface ResultToken
{
  /* **************************** SPECIAL TOKENS **************************** */

  /**
   * Special token representing the written form of
   * the "New Paragraph" directive.  Equal to "\u005Cu2029".
   * The spoken form of a "New Paragraph" directive
   * may vary between recognizers.
   */

  public static final String NEW_PARAGRAPH = "\u2029";


  /**
   * Special token representing the written form of
   * the "New Line" directive.  Equal to "\\n".
   * The spoken form of a "New Line" directive
   * may vary between recognizers.
   */

  public static final String NEW_LINE = "\n";



  /* **************************** DICTATION HINTS **************************** */

  /**
   * A <code>SpacingHint</code> returned when a token should be
   * presented separately from surrounding tokens (preceding and
   * following spaces).  Returned when
   * <code>ATTACH_PREVIOUS</code>,
   * <code>ATTACH_FOLLOWING</code>,
   * and <code>ATTACH_GROUP</code> are all false.
   * (See the <A HREF="#Presentation">description</A> above.)
   * <P>
   *
   * <code>SEPARATE</code> is the default spacing hint value.
   * <P>
   *
   * Example:
   * <pre>
   *     if (resultToken.getSpacingHint() == ResultToken.SEPARATE)
   *       ...;
   * </pre>
   *
   * @see #getSpacingHint
   */

  public static final int SEPARATE = 0;


  /**
   * A <code>SpacingHint</code> flag set true when a token should be
   * attached to the preceding token.
   * (See the <A HREF="#Presentation">description</A> above.)
   * <P>
   *
   * Example:
   * <pre>
   *     if ((resultToken.getSpacingHint() & ResultToken.ATTACH_PREVIOUS) != 0)
   *       ...;
   * </pre>
   *
   * @see #getSpacingHint
   */

  public static final int ATTACH_PREVIOUS = 1;


  /**
   * A <code>SpacingHint</code> flag set true when a token should be
   * attached to the following token.
   * (See the <A HREF="#Presentation">description</A> above.)
   * <P>
   *
   * Example:
   * <pre>
   *     if ((resultToken.getSpacingHint() & ResultToken.ATTACH_FOLLOWING) != 0)
   *       ...;
   * </pre>
   *
   * @see #getSpacingHint
   */

  public static final int ATTACH_FOLLOWING = 2;


  /**
   * A <code>SpacingHint</code> flag set true when a token should be
   * attached to preceding and/or following tokens which also have the
   * <code>ATTACH_GROUP</code> flag set true.
   * (See the <A HREF="#Presentation">description</A> above.)
   * <P>
   *
   * Example:
   * <pre>
   *     if (((thisToken.getSpacingHint() & ResultToken.ATTACH_GROUP) != 0)
   *         && ((prevToken.getSpacingHint() & ResultToken.ATTACH_GROUP) != 0))
   *       ...;
   * </pre>
   *
   * @see #getSpacingHint
   */

  public static final int ATTACH_GROUP = 4;


  /**
   * A <code>CapitalizationHint</code> indicating that the following word
   * should be presented without changes in capitalization.
   * This is the default value.
   * (See the <A HREF="#Presentation">description</A> above.)
   */

  public static final int CAP_AS_IS = 10;


  /**
   * A <code>CapitalizationHint</code> indicating that the following word
   * should be presented with the first character in uppercase.
   * (See the <A HREF="#Presentation">description</A> above.)
   */

  public static final int CAP_FIRST = 11;


  /**
   * A <code>CapitalizationHint</code> indicating that the following word
   * should be presented in uppercase.
   * (See the <A HREF="#Presentation">description</A> above.)
   */

  public static final int UPPERCASE = 12;


  /**
   * A <code>CapitalizationHint</code> indicating that the following word
   * should be presented in lowercase.
   * (See the <A HREF="#Presentation">description</A> above.)
   */

  public static final int LOWERCASE = 13;


  /**
   * Return a reference to the result that contains this token.
   */

  public Result getResult();


  /**
   * Get the spoken text of a token.  In dictation, the spoken form
   * is typically used when displaying unfinalized tokens.
   * The <a href="#Spoken_Written">difference between spoken and written forms</A>
   * is discussed above.
   */

  public String getSpokenText();


  /**
   * Get the written form of a spoken token.
   * <a href="#Spoken_Written">Spoken and written forms</A>
   * are discussed above.
   */

  public String getWrittenText();


  /**
   * Get the capitalization hint. (See <A HREF="#Presentation">description</A> above.)
   * Values are <code>CAP_AS_IS</code> (the default),
   * <code>CAP_FIRST</code>,
   * <code>UPPERCASE</code>,
   * <code>LOWERCASE</code>.
   * Tokens from a <code>RuleGrammar</code> result always return <code>CAP_AS_IS</code>.
   */

  public int getCapitalizationHint();


  /**
   * Get the spacing hints. (See <A HREF="#Presentation">description</A> above.)
   * The value equals <code>SEPARATE</code> (the default) if
   * the token should be presented with surrounding spaces.
   * Otherwise any or all of the following flags can be true:
   * <code>ATTACH_FOLLOWING</code>,
   * <code>ATTACH_PREVIOUS</code>,
   * <code>ATTACH_GROUP</code>.
   * Tokens from a <code>RuleGrammar</code> result always return <code>SEPARATE</code>.
   */

  public int getSpacingHint();


  /* **************************** TIMING **************************** */

  /**
   * Get the approximate start time for the token.
   * The value is matched to the
   * <code>System.currentTimeMillis()</code> time.
   * <P>
   *
   * The start time of a token is always greater than or equal to the
   * the end time of a preceding token.  The values will be different
   * if the tokens are separated by a pause.
   * <P>
   *
   * Returns <code>-1</code> if timing information is not available.
   * Not all recognizers provide timing information.
   * Timing information is not usually available for unfinalized or
   * finalized tokens in a <code>Result</code> that is not yet finalized.
   * Even if timing information is available for the best-guess tokens,
   * it might not be available for alternative tokens.
   * <P>
   *
   * @see java.lang.System#currentTimeMillis
   */

  public long getStartTime();


  /**
   * Get the approximate end time for the token.
   * The value is matched to the
   * <code>System.currentTimeMillis()</code> time.
   * <P>
   *
   * The end time of a token is always less than or equal to the
   * the end time of a preceding token.  The values will be different
   * if the tokens are separated by a pause.
   * <P>
   *
   * Returns <code>-1</code> if timing information is not available.
   * Not all recognizers provide timing information.
   * Timing information is not usually available for unfinalized or
   * finalized tokens in a <code>Result</code> that is not yet finalized.
   * Even if timing information is available for the best-guess tokens,
   * it might not be available for alternative tokens.
   * <P>
   *
   * @see java.lang.System#currentTimeMillis
   */

  public long getEndTime();
}


