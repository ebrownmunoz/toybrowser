/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
  * <code>RuleAlternatives</code> represents a <code>Rule</code> composed of a 
  * set of alternative sub-rules.  <code>RuleAlternatives</code> are used to 
  * construct <code>RuleGrammar</code> objects.  A <code>RuleAlternatives</code> 
  * object is spoken by saying one, and only one, of of its sub-rules.
  * <P>
  *
  * A <code>RuleAlternatives</code> object contains a set of zero or more
  * <code>Rule</code> objects.  A set of zero alternatives is equivalent to 
  * <code>&lt;VOID&gt;</code> (it is unspeakable).
  * <P>
  *
  * Weights may be (optionally) assigned to each alternative rule.
  * The weights indicate the chance of each <code>Rule</code> being spoken.
  * The <code>setWeights</code> method defines the constraints upon weights.
  * If no weights are defined, then all alternatives are considered
  * equally likely.
  * <P>
  *
  * @see Rule
  * @see RuleCount
  * @see RuleGrammar
  * @see RuleName
  * @see RuleParse
  * @see RuleSequence
  * @see RuleTag
  * @see RuleToken
  * @see RuleName#VOID
  */


public class RuleAlternatives extends Rule
{
  /**
   * Set of alternative <code>Rule</code> objects.
   *
   * @serial
   */

  protected Rule[] rules;


  /**
   * Array of weights for each alternative <code>Rule</code>
   * or <code>null</code> if the rules are equally likely.
   * If non-null, the <code>weights</code> array must have an 
   * identical length to the <code>rules</code> array.
   *
   * @serial
   */

  protected float[] weights;


  /**
   * Construct a <code>RuleAlternatives</code> object with an array of 
   * sub-rules and an array of weights.  The rules array and weights 
   * array may be <code>null</code>.  If the weights array is non-null, 
   * it must have identical length to the rules array.
   *
   * @exception IllegalArgumentException
   *   Error in length of array, or the weight values (see <code>setWeights</code>).
   * @see #setWeights
   * @param rules
   *   the set of alternative sub-rules
   * @param set of weights for each rule or <code>null</code>
   */

  public RuleAlternatives(Rule[] rules, float weights[])
    throws IllegalArgumentException
    {
      setRules(rules);
      setWeights(weights);
    }


  /**
   * Construct a <code>RuleAlternatives</code> object with an array of sub-rules.
   * The weights are set to <code>null</code>.
   * <P>
   *
   * @param rules
   *   the set of alternative sub-rules
   */

  public RuleAlternatives(Rule[] rules)
    {
      setRules(rules);
      weights = null;
    }


  /**
   * Construct a <code>RuleAlternatives</code> object containing a single sub-rule.
   * The weights array is set to <code>null</code>.
   */

  public RuleAlternatives(Rule rule)
    {
      Rule[] tmp = new Rule[1];
      tmp[0] = rule;

      setRules(tmp);
      weights = null;
    }


  /**
   * Empty constructor creates zero-length list of alternatives.
   * Use the <code>setRules</code> method or append method to 
   * add alternatives.
   * <P>
   *
   * A zero-length set of alternatives is equivalent to 
   * <code>&lt;VOID&gt;</code> (i.e. unspeakable).
   */

  public RuleAlternatives()
    {
      setRules(null);
      weights = null;
    }


  /**
   * Constructor for <code>RuleAlternatives</code> that produces a 
   * phrase list from an array of <code>String</code> objects.  Each 
   * string is used to create a single <code>RuleToken</code> object.  
   * <P>
   *
   * A string containing multiple words (e.g. "san francisco") is treated 
   * as a single token.  If appropriate, an application should parse such
   * strings to produce separate tokens.
   * <P>
   *
   * The phrase list may be zero-length or null.  This will produce an
   * empty set of alternatives which is equivalent to <code>&lt;VOID&gt;</code>
   * (i.e. unspeakable).
   * <P>
   *
   * @param tokens
   *  a set of alternative tokens
   * @see RuleName#VOID
   */

  public RuleAlternatives(String tokens[])
    {
      if (tokens == null) {
	tokens = new String[0];
	weights = null;
	return;
      }

      rules = new Rule[tokens.length];

      for (int i = 0; i < tokens.length; i++)
	rules[i] = new RuleToken(tokens[i]);

      weights = null;
    }
  

  /**
   * Return the array of alternative sub-rules.
   */

  public Rule[] getRules() {return rules;}


  /**
   * Set the array of alternative sub-rules.
   * <P>
   *
   * If the weights are non-null and the number of rules is
   * not equal to the number of weights, the weights are set to <code>null</code>.
   * To change the number of rules and weights, call <code>setRules</code> before
   * <code>setWeights</code>.
   *
   * @see #setWeights
   */

  public void setRules(Rule[] rules) {
    if (rules == null)
      rules = new Rule[0];
    
    if (weights != null && rules.length != weights.length)
      weights = null;

    this.rules = rules;
  }


  /**
   * Append a single rule to the set of alternatives.
   * The weights are set to <code>null</code>.
   */

  public void append(Rule rule)
    {
      if (rule == null)
	throw new NullPointerException("null rule to append");

      int len = rules.length;

      Rule[] newRules = new Rule[len + 1];
      System.arraycopy(rules, 0, newRules, 0, len);
      newRules[len] = rule;

      this.rules = newRules;

      this.weights = null;
    }



  /**
    * Return the array of weights.  May return <code>null</code>.
    * If non-null, the length of the weights array is guaranteed to be 
    * the same length as the array of rules.
    */

  public float[] getWeights() {return weights;}


  /** 
    * Set the array of weights for the rules.  
    * The <code>weights</code> array may be <code>null</code>.
    * If the weights are <code>null</code>, then all alternatives are
    * assumed to be equally likely.
    * <P>
    *
    * The length of the weights array must be the same length as
    * the array of rules.  The weights must all be greater than
    * or equal to 0.0 and at least one must be non-zero.
    * <P>
    *
    * To change the number of rules and weights, first call
    * <code>setRules</code>.
    * <P>
    *
    * @see #setRules
    * @exception IllegalArgumentException
    *   Error in length of array or value of weights
    */

  public void setWeights(float[] weights)
    throws IllegalArgumentException
    {
      if (weights == null || weights.length == 0)
	{
	  this.weights = null;
	  return;
	}

      if (weights.length != rules.length)
	throw (new IllegalArgumentException("weights/rules array length mismatch"));

      float sum = 0.0f;

      for (int i = 0; i < weights.length; i++)
	{
	  if (java.lang.Float.isNaN(weights[i]))
	    throw (new IllegalArgumentException("illegal weight value: NaN"));
	  if (java.lang.Float.isInfinite(weights[i]))
	    throw (new IllegalArgumentException("illegal weight value: infinite"));
	  if (weights[i] < 0.0)
	    throw (new IllegalArgumentException("illegal weight value: negative"));

	  sum += weights[i];
	}

      if (sum <= 0.0)
	    throw (new IllegalArgumentException("illegal weight values: all zero"));

      this.weights = weights;
    }


  /**
   * Return a deep copy of this rule.
   * See the <A HREF="Rule.html#copy()"><code>Rule.copy</code></A>
   * documentation for an explanation of deep copy.
   */

  public Rule copy()
  {
    float[] weightsCopy = null;

    if (weights != null) {
      weightsCopy = new float[weights.length];
      System.arraycopy(weights, 0, weightsCopy, 0, weights.length);
    }

    Rule[] rulesCopy = null;

    if (rules != null) {
      rulesCopy = new Rule[rules.length];
      for (int i=0; i<rules.length; i++)
	rulesCopy[i] = rules[i].copy();
    }

    return new RuleAlternatives(rulesCopy, weightsCopy);
  }


  /**
   * Return a <code>String</code> representing this object as partial 
   * Java Speech Grammar Format.  The string is a legal right hand side
   * of a rule definition.
   */

  public String toString()
    {
      if (rules == null || rules.length == 0)
	return "<VOID>";

      StringBuffer buf = new StringBuffer();

      for (int i = 0; i < rules.length; i++)
	{
	  if (i > 0) buf.append(" | ");

	  if (weights != null) buf.append("/" + weights[i] + "/ ");

	  //  Preserve hierarchy of alternatives within alternatives

	  if (rules[i] instanceof RuleAlternatives)
	    buf.append("( " + rules[i].toString() + " )");
	  else
	    buf.append(rules[i].toString());
	}

      return buf.toString();
    }
}


