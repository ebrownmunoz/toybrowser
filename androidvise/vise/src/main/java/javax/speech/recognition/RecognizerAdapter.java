/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * The adapter which receives events for a <code>Recognizer</code>.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 */

public class RecognizerAdapter extends EngineAdapter implements RecognizerListener
{
  /**
   * A <code>RECOGNIZER_PROCESSING</code> event has been issued
   * as a <code>Recognizer</code> changes from the <code>LISTENING</code>
   * state to the <code>PROCESSING</code> state.
   * <P>
   *
   * @see RecognizerEvent#RECOGNIZER_PROCESSING
   */

  public void recognizerProcessing(RecognizerEvent e) {}


  /**
   * <code>RECOGNIZER_SUSPENDED</code> event has been issued as a
   * <code>Recognizer</code> changes from either the 
   * <code>LISTENING</code> state or the <code>PROCESSING</code> 
   * state to the <code>SUSPENDED</code> state.
   * <P>
   *
   * @see RecognizerEvent#RECOGNIZER_SUSPENDED
   */

  public void recognizerSuspended(RecognizerEvent e) {}


  /**
   * <code>CHANGES_COMMITTED</code> event has been issued as a
   * <code>Recognizer</code> changes from the <code>SUSPENDED</code>
   * state to the <code>LISTENING</code> state.
   * <P>
   *
   * Following the <code>CHANGES_COMMITTED</code> event, a
   * <code>GRAMMAR_CHANGES_COMMITTED</code> event is issued
   * to the <code>GrammarListeners</code> of each grammar for
   * which a change in its definition or its enabled flag has
   * been committed.
   * <P>
   *
   * @see RecognizerEvent#CHANGES_COMMITTED
   * @see GrammarEvent#GRAMMAR_CHANGES_COMMITTED
   */

  public void changesCommitted(RecognizerEvent e) {}


  /**
   * <code>FOCUS_GAINED</code> event has been issued as a
   * <code>Recognizer</code> changes from the <code>FOCUS_OFF</code>
   * state to the <code>FOCUS_ON</code> state.
   * <P> 
   *
   * @see RecognizerEvent#FOCUS_GAINED
   */

  public void focusGained(RecognizerEvent e) {}


  /**
   * <code>FOCUS_LOST</code> event has been issued as a
   * <code>Recognizer</code> changes from the <code>FOCUS_ON</code>
   * state to the <code>FOCUS_OFF</code> state.
   * <P>
   *
   * @see RecognizerEvent#FOCUS_LOST
   */

  public void focusLost(RecognizerEvent e) {}
}


