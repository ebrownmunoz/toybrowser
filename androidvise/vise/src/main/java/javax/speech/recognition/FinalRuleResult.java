/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Provides information on a finalized result for an utterance that matches 
 * a <code>RuleGrammar</code>.  A finalized result is one that is in
 * either the <code>ACCEPTED</code> or <code>REJECTED</code> state
 * (tested by the <code>getResultState</code> method of the 
 * <code>Result</code> interface).
 * <P>
 *
 * This interface provides the following information for 
 * a finalized result for a <code>RuleGrammar</code> (in addition
 * to the information provided through the <code>Result</code> and
 * <code>FinalResult</code> interfaces):
 * <P>
 *
 * <UL>
 *   <LI>N-best alternatives for the entire result.  For each alternative
 *       guess the interface provides the token sequence and the names
 *       of the grammar and rule matched by the tokens.
 *   <LI>Tags for the best-guess result.
 * </UL>
 * <P>
 *
 * Every <code>Result</code> object provided by a <code>Recognizer</code>
 * implements the <code>FinalRuleResult</code> and the 
 * <code>FinalDictationResult</code> interfaces (and by inheritence the 
 * <code>FinalResult</code> and <code>Result</code> interfaces).  
 * However, the methods of <code>FinalRuleResult</code>
 * should only be called if the <code>Result.getGrammar</code>
 * method returns a <code>RuleGrammar</code> and once the
 * <code>Result</code> has been finalized with either an
 * <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code> event.
 * Inappropriate calls will cause a <code>ResultStateError</code>.
 * <P>
 *
 * @see RuleGrammar
 * @see Result
 * @see FinalResult
 * @see FinalDictationResult
 * @see Result#getResultState
 * @see Result#getGrammar
 */


public interface FinalRuleResult extends FinalResult
{
  /**
   * Return the number of guesses for this result.  The guesses
   * are numbered from 0 up.  The 0th guess is the best guess for
   * the result and provides the same tokens as the <code>getBestTokens</code>
   * method of the <code>Result</code> interface.
   * <P>
   *
   * If only the best guess is available (no alternatives) the return
   * value is 1.  If the result is was rejected (<code>REJECTED</code>
   * state), the return value may be 0 if no tokens are available.  
   * If a best guess plus alternatives are available, the return value 
   * is greater than 1.
   * <P>
   *
   * The integer parameter to the <code>getAlternativeTokens</code>, 
   * <code>getRuleGrammar</code> and <code>getRuleName</code> methods 
   * are indexed from <code>0</code> to <code>(getNumberGuesses()-1)</code>.
   * <P>
   *
   * @see Result#getBestTokens
   * @see Result#REJECTED
   * @see #getAlternativeTokens
   * @see #getRuleGrammar
   * @see #getRuleName
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public int getNumberGuesses()
    throws ResultStateError;


  /**
   * Get the N-best token sequence for this result.  The <code>nBest</code>
   * value should be in the range of <code>0</code> to 
   * <code>(getNumberGuesses()-1)</code> inclusive.  For out-of-range
   * values, the method returns <code>null</code>.
   * <P>
   *
   * If <code>nBest==0</code>, the method returns the best-guess sequence 
   * of tokens - identical to the token sequence returned by the 
   * <code>getBestTokens</code> method of the <code>Result</code> interface
   * for the same object.
   * <P>
   *
   * If <code>nBest==1</code> (or 2, 3...) the method returns the 1st (2nd, 3rd...) 
   * alternative to the best guess.
   * <P>
   *
   * The number of tokens for the best guess and the alternatives do 
   * not need to be the same.
   * <P>
   *
   * The <code>getRuleGrammar(int)</code> and <code>getRuleName(int)</code>
   * methods indicate which <code>RuleGrammar</code> and <code>ruleName</code>
   * are matched by each alternative result sequence.
   * <P>
   *
   * If the <code>Result</code> is in the <code>ACCEPTED</code> state (not rejected),
   * then the best guess and all the alternatives are accepted.  Moreover, 
   * each alternative set of tokens must be a legal match of the 
   * <code>RuleGrammar</code> and <code>RuleName</code> returned by
   * the <code>getRuleGrammar</code> and <code>getRuleName</code> methods.
   * <P>
   *
   * If the <code>Result</code> is in the <code>REJECTED</code> state (not accepted),
   * the recognizer is not confident that the best guess or any of the
   * alternatives are what the user said.  Rejected guesses do not need
   * to match the corresponding <code>RuleGrammar</code> and rule name.
   * <P>
   *
   * <B>Example</B>
   * <P>
   *
   * Say we have two simple grammars loaded and active for recognition.
   * The first is <code>grammar.numbers</code> with a public rule
   * <code>digits</code> that matches spoken digit sequences 
   * (e.g. "1 2 3 4").  The grammar is <code>grammar.food</code>
   * with the rule <code>whoAteWhat</code>
   * which matches statements about eating (e.g. "he ate mine").
   * [Yes, this is artificial!]
   * <P>
   *
   * The user says "two eight nine" and the recognizer
   * correctly recognizes the speech, but also provides 
   * 2 alternatives.  The 
   *
   * <pre>
   *  FinalRuleResult result = ...;
   *  result.getNumberGuesses() -&gt; 3
   *
   *  result.getAlternativeTokens(0) -&gt; two eight nine // array of tokens
   *  result.getRuleGrammar(0) -&gt; [reference to <code>grammar.numbers</code>]
   *  result.getRuleName(0) -&gt; "digits"
   *
   *  result.getAlternativeTokens(1) -&gt; you ate mine
   *  result.getRuleGrammar(1) -&gt; [reference to <code>grammar.food</code>]
   *  result.getRuleName(1) -&gt; "whoAteWhat"

   *  result.getAlternativeTokens(2) -&gt; two eight five
   *  result.getRuleGrammar(2) -&gt; [reference to <code>grammar.numbers</code>]
   *  result.getRuleName(2) -&gt; "digits"
   * </pre>
   *
   * @see #getRuleGrammar
   * @see #getRuleName
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public ResultToken[] getAlternativeTokens(int nBest)
    throws ResultStateError;


  /**
   * Return the <code>RuleGrammar</code> matched by the nth guess.
   * Return <code>null</code> if <code>nBest</code> is out-of-range.
   * <P>
   *
   * <code>getRuleName</code> returns the rule matched in the 
   * <code>RuleGrammar</code>.  See the documentation for
   * <code>getAlternativeTokens</code> for a description of how
   * tokens, grammars and rules correspond.
   * <p>
   *
   * An application can use the parse method of the
   * matched grammar to analyse a result.  e.g.
   *
   * <pre>
   *   int nBest = 2;
   *   FinalRuleResult res;
   *   RuleParse parse = res.getRuleGrammar(nbest).parse(
   *                              res.getAlternativeTokens(nBest), 
   *                              res.getRuleName(nBest));
   * </pre>
   *
   * @see RuleGrammar#parse
   * @see #getNumberGuesses
   * @see #getAlternativeTokens
   * @see #getRuleName
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public RuleGrammar getRuleGrammar(int nBest)
    throws ResultStateError;


  /**
   * Return the <code>RuleName</code> matched by the nth guess.
   * Return <code>null</code> if <code>nBest</code> is out-of-range.
   * Typically used in combination with <code>getAlternativeTokens</code>
   * and <code>getRuleGrammar</code> which return the corresponding
   * tokens and grammar.
   * <P>
   *
   * The documentation for <code>getAlternativeTokens</code> shows
   * and example result with alternatives.
   * <P>
   *
   * @see RuleGrammar#parse
   * @see #getAlternativeTokens
   * @see #getRuleGrammar
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public String getRuleName(int nBest)
    throws ResultStateError;


  /**
   * Return the list of tags matched by the best-guess token sequence.
   * The tags in the array are ordered strictly in the left to right
   * order in which they would appear in JSGF.
   * <P>
   *
   * For example, if the following
   * simple Java Speech Grammar Format (JSGF) rule is active:
   *
   * <pre>
   *   public <cmd> = (open {ACT_OPEN} | close {ACT_CLOSE}) [(it{WHAT} now) {NOW}];
   * </pre>
   *
   * and the user says "close it now", then getTags returns
   * an array containing <code>{"ACT_CLOSE", "WHAT", "NOW"}</code>.
   * Note how both the <code>{WHAT}</code> and <code>{NOW}</code> tags
   * are attached starting from the word "it" but that <code>{TAG}</code>
   * appears first in the array.  In effect, when tags start at the 
   * same token, they are listed "bottom-up".
   * <P>
   * 
   * <code>getTags</code> does not indicate which tokens are matched by 
   * which tags.  To obtain this information use the <code>parse</code> 
   * method of the <code>RuleGrammar</code>.  Also, <code>getTags</code>
   * only provides tags for the best guess.  To get tags for the
   * alternative guesses using parsing through the <code>RuleGrammar</code>.
   * <P>
   *
   * The string array returned by the <code>getTags</code> method of the 
   * <code>RuleParse</code> object returned by parsing the best-guess
   * token sequence will be the same as returned by this method.
   * <P>
   *
   * @see RuleGrammar#parse
   * @see RuleParse#getTags
   * @exception ResultStateError
   *   if called before a result is finalized
   */

  public String[] getTags()
    throws ResultStateError;
}
