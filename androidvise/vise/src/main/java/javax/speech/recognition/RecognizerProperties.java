/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.PropertyVetoException;
import javax.speech.EngineProperties;
import javax.speech.PropertyVetoException;

import java.beans.*;


/**
 * Enables control of the properties of a <code>Recognizer</code>.
 * The <code>RecognizerProperties</code> object is obtained by
 * calling the <code>getEngineProperties</code> method of the
 * <code>Recognizer</code> (inherited from the <code>Engine</code> interface).
 * <P>
 *
 * Because <code>RecognizerProperties</code> extends the 
 * <code>EngineProperties</code> interface, it inherits the
 * following behavior (described in detail in the <code>EngineProperties</code>
 * documentation):
 * <P>
 *
 * <UL>
 *   <LI>Each property has a get and set method.  (JavaBeans property method patterns)
 *   <LI>Engines may ignore changes to properties or apply maximum 
 *       and minimum limits.  If an engine does not apply a property change
 *       request, the set method throws a <code>PropertyVetoException</code>.
 *   <LI>Calls to set methods may be asynchronous (they may return
 *       before the property change takes effect).  The <code>Engine</code>
 *       will apply a change as soon as possible.
 *   <LI>The get methods return the current setting - not a pending value.
 *   <LI>A <code>PropertyChangeListener</code> may be attached to 
 *       receive property change events.
 *   <LI>Where appropriate, property settings are persistent across sessions.
 * </UL>
 * <P>
 *
 * <B>Per-Speaker Properties</B>
 * <P>
 *
 * For recognizers that maintain speaker data (recognizers that implement the
 * <code>SpeakerManager</code> interface) the <code>RecognizerProperties</code> 
 * should be stored and loaded as part of the speaker data.
 * Thus, when the current speaker is changed through the 
 * <code>SpeakerManager</code> interface, the properties of the
 * new speaker should be loaded.
 * <P>
 *
 * @see EngineProperties
 * @see SpeakerManager
 */

public interface RecognizerProperties extends EngineProperties
{
  /**
   * Get the recognizer's "ConfidenceLevel" property.
   * <P>
   *
   * @see #setConfidenceLevel
   */

  public float getConfidenceLevel();


  /**
   * Set the recognizer's "ConfidenceLevel" property.
   * The confidence level value can very between 0.0 and 1.0.
   * A value of 0.5 is the default for the recognizer.
   * A value of 1.0 requires the recognizer to have maximum confidence
   * in its results or otherwise reject them.  A value of 0.0 
   * requires only low confidence so fewer results are rejected.
   * <P>
   *
   * @see #getConfidenceLevel
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setConfidenceLevel(float confidenceLevel)
    throws PropertyVetoException;



  /**
   * Get the "Sensitivity" property.
   * <P>
   *
   * @see #setSensitivity
   */

  public float getSensitivity();


  /**
   * Set the "Sensitivity" property.
   * The sensitivity level can vary between 0.0 and 1.0.
   * A value of 0.5 is the default for the recognizer.
   * A value of 1.0 gives maximum sensitivity and makes the
   * recognizer sensitive to quiet input but more sensitive
   * to noise.  A value of 0.0 gives minimum sensitivity requiring
   * the user to speak loudly and making the recognizer less
   * sensitive to background noise.
   * <P>
   *
   * <b>Note</b>: some recognizers set the gain automatically during use,
   * or through a setup "Wizard".  On these recognizers the sensitivity 
   * adjustment should be used only in extreme cases where the automatic
   * settings are not adequate.
   * <P>
   *
   * @see #getSensitivity
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setSensitivity(float sensitivity)
    throws PropertyVetoException;



  /**
   * Get the "SpeedVsAccuracy" property.
   * <P>
   *
   * @see #setSpeedVsAccuracy
   */

  public float getSpeedVsAccuracy();


  /**
   * Get the "SpeedVsAccuracy" property.
   * The default value is 0.5 and is the factory-defined 
   * compromise of speed and accuracy.
   * A value of 0.0 minimizes response time.
   * A value of 1.0 maximizes recognition accuracy.
   * <P>
   *
   * Why are speed and accuracy a trade-off?  A recognizer determines
   * what a user says by testing different possible sequence of words
   * (with legal sequences being defined by the active grammars).  If the
   * recognizer tests more sequences it is more likely to
   * find the correct sequence, but there is additional processing so
   * it is slower.  Increasing grammar complexity and decreasing the
   * computer power both make this trade-off more important.  Conversely,
   * a simpler grammar or more powerful computer make the trade-off
   * less important.
   * <P>
   *
   * @see #getSpeedVsAccuracy
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setSpeedVsAccuracy(float speedVsAccuracy)
    throws PropertyVetoException;



  /**
   * Get the "CompleteTimeout" property.
   *
   * @see #setCompleteTimeout
   */

  public float getCompleteTimeout();


  /**
   * Set the <code>CompleteTimeout</code> property in seconds.
   * This timeout value determines the length of silence required 
   * following user speech before the recognizer finalizes a result
   * (with an <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code>
   * event).  The complete timeout is used when the speech is a complete 
   * match an active <code>RuleGrammar</code>.  By contrast, the incomplete
   * timeout is used when the speech is an incomplete match to an active grammar.
   * <P>
   *
   * A long complete timeout value delays the result completion
   * and therefore makes the computer's response slow.  A short
   * complete timeout may lead to an utterance being broken up
   * inappropriately.  Reasonable complete timeout values are
   * typically in the range of 0.3 seconds to 1.0 seconds.
   * <P>
   *
   * @see #getCompleteTimeout
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setCompleteTimeout(float timeout)
    throws PropertyVetoException;



  /**
   * Get the "IncompleteTimeout" property.
   * <P>
   *
   * @see #setIncompleteTimeout
   */

  public float getIncompleteTimeout();


  /**
   * Set the <code>IncompleteTimeout</code> property in seconds.
   * The timeout value determines the required length of silence
   * following user speech after which a recognizer finalizes
   * a result.
   * <P>
   *
   * The incomplete timeout applies when the speech prior to the
   * silence is an incomplete match of the active <code>RuleGrammars</code>.
   * In this case, once the timeout is triggered, the partial
   * result is rejected (with a <code>RESULT_REJECTED</code> event).
   * <P>
   *
   * The incomplete timeout also applies when the speech prior to
   * the silence is a complete match of an active grammar, but 
   * where it is possible to speak further and still match the
   * grammar.  For example, in a grammar for digit sequences for
   * telephone numbers it might be legal to speak either 7 or 10 digits.
   * If the user pauses briefly after speaking 7 digits then the
   * incomplete timeout applies because the user may then continue
   * with a further 3 digits.
   * <P>
   *
   * By contrast, the complete timeout is used when the speech
   * is a complete match to an active <code>RuleGrammar</code>
   * and no further words can be spoken.
   * <P>
   *
   * A long complete timeout value delays the result completion
   * and therefore makes the computer's response slow.  A short
   * incomplete timeout may lead to an utterance being broken up
   * inappropriately.
   * <P>
   * 
   * The incomplete timeout is usually longer than the complete timeout
   * to allow users to pause mid-utterance (for example, to breathe).
   * <P>
   *
   * @see #getIncompleteTimeout
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setIncompleteTimeout(float timeout)
    throws PropertyVetoException;


  /**
   * Get the "NumResultAlternatives" property.
   *
   * @see #setNumResultAlternatives
   */

  public int getNumResultAlternatives();


  /**
   * Set the <code>"NumResultAlternatives"</code> property.
   * This property indicates the preferred maximum number of
   * N-best alternatives in <code>FinalDictationResult</code> and 
   * <code>FinalRuleResult</code> objects.
   * A value of 0 or 1 indicates that the application does not want
   * alternatives; that is, it only wants the best guess.
   * <P>
   *
   * Recognizers are not required to provide this number of
   * alternatives for every result and the number of alternatives
   * may vary from result to result.  Recognizers should only provide
   * alternative tokens which are considered reasonable guesses: that is,
   * the alternatives should be above the <code>ConfidenceLevel</code>
   * set through this interface (unless the <code>Result</code> is rejected).
   * <P>
   *
   * Providing alternatives requires additional computing power.
   * Applications should only request the number of alternatives
   * that they are likely to use.
   * <P>
   *
   * @see #getNumResultAlternatives
   * @see FinalDictationResult#getAlternativeTokens(ResultToken, ResultToken, int)
   * @see FinalRuleResult#getAlternativeTokens(int)
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setNumResultAlternatives(int num)
    throws PropertyVetoException;


  /**
   * Get the <code>"ResultAudioProvided"</code> property.
   * <P>
   *
   * @see #setResultAudioProvided
   */

  public boolean isResultAudioProvided();


  /**
   * Set the <code>"ResultAudioProvided"</code> property.
   * If set to <code>true</code>, the recognizer is requested
   * to provide audio with <code>FinalResult</code> objects.  
   * If available, the audio is provided through the
   * <code>getAudio</code> methods of the <code>FinalResult</code> interface.
   * <P>
   *
   * Some recognizers that can provide audio for a <code>FinalResult</code>
   * cannot provide audio for all results.  Applications need
   * test audio available individually for results.  (For example,
   * a recognizer might only provide audio for dictation results.)
   * <P>
   *
   * A <code>Recognizer</code> that does not provide audio
   * for any results throws a <code>PropertyVetoException</code>
   * when an app attempts to set the value to <code>true</code>.
   *
   * @see #isResultAudioProvided
   * @see FinalResult#getAudio()
   * @see FinalResult#getAudio(ResultToken, ResultToken)
   * @exception PropertyVetoException
   *   if the recognizer rejects or limits the new value
   */

  public void setResultAudioProvided(boolean audio)
    throws PropertyVetoException;


  /**
   * Get the <code>TrainingProvided</code> property.
   * <P>
   * 
   * @see #setTrainingProvided
   */

  public boolean isTrainingProvided();


  /**
   * Set the <code>TrainingProvided</code> property.
   * If <code>true</code>, request a recognizer to provide
   * training information for <code>FinalResult</code> objects
   * through the <code>tokenCorrection</code> method.
   * <P>
   *
   * Not all recognizers support training.  Also, recognizers
   * that do support training are not required to support
   * training data for all results.  For example, a recognizer
   * might only produce training data with dictation results.
   * A <code>Recognizer</code> that does not support training 
   * throws a <code>PropertyVetoException</code> when an app
   * attempts to set the value to <code>true</code>.
   * <P>
   *
   * @see #isTrainingProvided
   * @see FinalResult#tokenCorrection
   * @exception PropertyVetoException
   *   if the recognizer rejects the new value
   */

  public void setTrainingProvided(boolean trainingProvided)
    throws PropertyVetoException;
}

