/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import java.lang.System;
import javax.speech.*;


/**
 * Provides control of <code>SpeakerProfiles</code> for a <code>Recognizer</code>.
 * The <code>SpeakerManager</code> for a <code>Recognizer</code>
 * is obtained through its <code>getSpeakerManager</code> method.
 * Recognizers that do not maintain speaker profiles - known as
 * speaker-independent recognizers return <code>null</code> for this method.
 * <P>
 *
 * Each <code>SpeakerProfile</code> stored with a <code>Recognizer</code>
 * stored information about an enrollment of a user with the recognizer.
 * The user information allows the recognizer to adapt to the
 * characteristic of the user with the goal of improving performance
 * and recognition accuracy.  For example, the recognizer might adjust
 * to vocabulary preferences and accent.
 * <P>
 *
 * The role of the <code>SpeakerManager</code> is provide access to the
 * known <code>SpeakerProfiles</code>, to enable storage and loading of
 * the profiles once a recognizer is running, and to provide other
 * management functions (storage to file, deletion etc).  The <code>SpeakerManager</code>
 * has a "current speaker" - the profile which is currently being used
 * by the recognizer.
 * <P>
 *
 * <B>Storing and Loading</B>
 * <P>
 *
 * Speaker data is typically persistent - a user will want their profile
 * to be available from session to session.  An application must explicitly
 * request a recognizer to save a speaker profile.  It is good practice
 * to check with a user before storing their speaker profile in case it
 * has become corrupted.
 * <P>
 *
 * The <code>SpeakerManager</code> interface provides a revert method
 * which requests the engine to restore the last saved profile (possibly the
 * profile loaded at the start of a session).
 * <P>
 *
 * The speaker profile is potentially a large amount of data
 * (often up to several MByte).  So loading and storing profiles,
 * reverting to an old profile and changing speakers may all be
 * slow operations.
 * <P>
 *
 * The <code>SpeakerManager</code> provides methods to load and
 * store speaker profiles to and from streams (e.g. files, URLs).
 * Speaker profiles contain engine-specific and often proprietary
 * information, so a speaker profile from one recognizer can not usually
 * be loaded into a recognizer from a different company.
 * <P>
 *
 * The <code>SpeakerManager</code> for a <code>Recognizer</code> can
 * be obtained from the <code>Recognizer</code> in any state of
 * the recognizer.  However, most methods of the <code>SpeakerManager</code>
 * operate correctly only when the <code>Recognizer</code> is in
 * the <code>ALLOCATED</code>.
 * <P>
 *
 * The <code>getCurrentSpeaker</code>, <code>setCurrentSpeaker</code>
 * and <code>listKnownSpeakers</code> methods operate in any
 * <code>Recognizer</code> state.  This allows the initial speaker
 * profile for a <code>Recognizer</code> to be loaded at allocation
 * time.
 * <P>
 *
 * @see SpeakerProfile
 * @see RecognizerModeDesc#getSpeakerProfiles
 * @see Central
 * @see System
 */


public interface SpeakerManager
{
  /**
   * Set the current <code>SpeakerProfile</code>.  The <code>SpeakerProfile</code>
   * object should be one of the objects returned by the <code>listKnownSpeakers</code>
   * method.
   * <P>
   *
   * Because a <code>SpeakerProfile</code> may store preferred user
   * settings for the <code>RecognizerProperties</code>, those properties
   * may change as a result of this call.
   *
   * @exception IllegalArgumentException
   *   if the speaker is not known
   */

  void setCurrentSpeaker(SpeakerProfile speaker)
    throws IllegalArgumentException;


  /**
   * Get the current <code>SpeakerProfile</code>.
   * Returns <code>null</code> if there is no current speaker.
   */

  SpeakerProfile getCurrentSpeaker();


  /**
   * List the <code>SpeakerProfiles</code> known to this <code>Recognizer</code>.
   * Returns <code>null</code> if there is no known speaker.
   */

  SpeakerProfile[] listKnownSpeakers();


  /**
   * Create a new <code>SpeakerProfile</code> for this <code>Recognizer</code>.
   * The <code>SpeakerProfile</code> object returned by this method
   * is <em>different</em> from the object passed to the method.
   * The input profile contains the new id, name and variant.  The
   * returned object is a reference to a recognizer-internal profile with
   * those settings but with all the additional recognizer-specific
   * information associated with a profile.
   * <P>
   *
   * This method does not change the current speaker.
   * <P>
   *
   * If the input profile's identifier or user name is not specified (is <code>null</code>),
   * the recognizer should assign a unique temporary identifier.
   * The application should request that the user update the id.
   * <P>
   *
   * If the input profile is <code>null</code>, the recognizer should
   * assign a temporary id and user name.  The application should
   * query the user for details.
   * <P>
   *
   * @exception IllegalArgumentException
   *   if the speaker id is already being used
   */

  SpeakerProfile newSpeakerProfile(SpeakerProfile profile)
    throws IllegalArgumentException;


  /**
   * Delete a <code>SpeakerProfile</code>.
   * If the deleted speaker is the current speaker,
   * the current speaker is set to <code>null</code>.
   * <P>
   *
   * @exception IllegalArgumentException
   *   if the speaker is not known
   */

  void deleteSpeaker(SpeakerProfile speaker)
    throws IllegalArgumentException;


  /**
   * Save the speaker profile for the current speaker.
   * The speaker profile is stored internally by the recognizer
   * and should be available for future sessions.
   * <P>
   *
   * Because of the large potential size of the speaker profile,
   * this may be a slow operation.
   * <P>
   *
   * @see #revertCurrentSpeaker
   * @see #writeVendorSpeakerProfile
   */

  void saveCurrentSpeakerProfile();


  /**
   * Restore the speaker profile for the current speaker to the last
   * saved version.  If the speaker profile has not been saved during
   * the session, the restored version will be the version loaded at
   * the start of the session.
   * <P>
   *
   * Because of the large potential size of the speaker profile,
   * this may be a slow operation.
   * <P>
   *
   * @see #saveCurrentSpeakerProfile
   * @see #readVendorSpeakerProfile
   */

  void revertCurrentSpeaker();


  /**
   * Write the speaker profile of the named speaker to a stream.
   * This method allows speaker data to be stored and to be
   * transferred between machines.
   * <P>
   *
   * The speaker profile is stored in a vendor-specific
   * format, so it can only be loaded into a recognizer that understands
   * that format - typically a recognizer from the same provider.
   * Speaker profiles are loaded with the <code>readVendorSpeakerProfile</code>
   * method.
   * <P>
   *
   * Note: The speaker profile is potentially large (up to several MByte).
   * <P>
   *
   * @see #readVendorSpeakerProfile
   * @exception java.io.IOException if an I/O error occurs
   */

  void writeVendorSpeakerProfile(java.io.OutputStream out, SpeakerProfile speaker)
    throws java.io.IOException;


  /**
   * Read a <code>SpeakerProfile</code> from a stream and return a
   * reference to it.  This method loads data that may have
   * been stored previously with the <code>writeVendorSpeakerProfile</code>
   * method.
   * <P>
   *
   * If the speaker profile contained in the input stream
   * already exists, the recognizer should create a modified name.
   * An application should inform the user of the name that is loaded and
   * consider giving them an option to modify it.
   * <P>
   *
   * Since speaker profiles are stored in vendor-specific
   * formats, they can only be loaded into a recognizer that understands
   * that format - typically a recognizer from the same provider.
   * <P>
   *
   * Note: The speaker profile is potentially large (up to several MByte).
   * <P>
   *
   * @see #writeVendorSpeakerProfile
   * @exception VendorDataException if the data format is not known to the recognizer
   * @exception java.io.IOException if an I/O error occurs
   */

  SpeakerProfile readVendorSpeakerProfile(java.io.InputStream in)
    throws java.io.IOException, VendorDataException;
}

