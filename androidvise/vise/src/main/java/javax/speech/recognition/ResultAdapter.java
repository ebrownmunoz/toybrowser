/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;
import java.util.*;


/**
 * The adapter which receives events for a <code>Result</code> object.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 */


public class ResultAdapter implements ResultListener
{
  /**
   * A <code>RESULT_CREATED</code> event has occured.
   * <P>
   *
   * @see ResultEvent#RESULT_CREATED
   */

  public void resultCreated(ResultEvent e) {}


  /**
   * A <code>RESULT_UPDATED</code> event has occured.
   * <P>
   *
   * @see ResultEvent#RESULT_UPDATED 
   */

  public void resultUpdated(ResultEvent e) {}


  /**
   * A <code>GRAMMAR_FINALIZED</code> event has occured.
   * <P>
   *
   * @see ResultEvent#GRAMMAR_FINALIZED
   */

  public void grammarFinalized(ResultEvent e) {}


  /**
    * A <code>RESULT_ACCEPTED</code> event has occured.
    * <P>
    *
    * @see ResultEvent#RESULT_ACCEPTED
    */

  public void resultAccepted(ResultEvent e) {}


  /**
    * A <code>RESULT_REJECTED</code> event has occured.
    * <P>
    *
    * @see ResultEvent#RESULT_REJECTED 
    */

  public void resultRejected(ResultEvent e) {}


  /**
    * A <code>AUDIO_RELEASED</code> event has occured.
    * <P>
    *
    * @see ResultEvent#AUDIO_RELEASED 
    */

  public void audioReleased(ResultEvent e) {}


  /**
    * A <code>TRAINING_INFO_RELEASED</code> event has occured.
    * <P>
    *
    * @see ResultEvent#TRAINING_INFO_RELEASED
    */

  public void trainingInfoReleased(ResultEvent e) {}
}

