/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Provides information on a finalized result
 * for an utterance that matches a <code>DictationGrammar</code>.
 * A finalized result is a result that is in either the
 * <code>ACCEPTED</code> or <code>REJECTED</code> state
 * (tested by the <code>getResultState</code> method of the
 * <code>Result</code> interface).
 * <P>
 *
 * The <code>FinalDictationResult</code> interface extends the
 * <code>Result</code> and <code>FinalResult</code> interfaces
 * with a single method.  The <code>getAlternativeTokens</code>
 * method provides access to alternative guesses for tokens
 * in a dictation result.
 * <P>
 *
 * Every <code>Result</code> object provided by a <code>Recognizer</code>
 * implements the <code>FinalDictationResult</code> and the
 * <code>FinalRuleResult</code> interfaces (and by inheritence the
 * <code>FinalResult</code> and <code>Result</code> interfaces).
 * However, the methods of <code>FinalDictationResult</code>
 * should only be called if the <code>Result.getGrammar</code>
 * method returns a <code>DictationGrammar</code> and once the
 * <code>Result</code> has been finalized with either an
 * <code>RESULT_ACCEPTED</code> or <code>RESULT_REJECTED</code> event.
 * Inappropriate calls will cause a <code>ResultStateError</code>.
 * <P>
 *
 * @see Result#ACCEPTED
 * @see Result#REJECTED
 * @see Result#getResultState
 * @see DictationGrammar
 * @see Result
 * @see FinalResult
 * @see FinalRuleResult
 * @see ResultStateError
 */


public interface FinalDictationResult extends FinalResult
{
  /**
   * Return a set of alternative token guesses for a single known
   * token or sequence of tokens.  In a dictation application
   * the alternative guesses are typically provided to a user to
   * facilitate efficient correction of dictated text.  The assumption is that
   * if a recognizer does not correctly hear a user (a mis-recognition)
   * the correct tokens are likely to be amongst the top alternatives guesses.
   * <P>
   *
   * Typically when a user selects one of the alternative guesses
   * as the correct token sequence, the <code>tokenCorrection</code>
   * method of the <code>FinalResult</code> interface is called
   * with those tokens.  That call allows the <code>Recognizer</code>
   * to learn from recognition errors and can improve future
   * recognition accuarcy.
   * <P>
   *
   * The <code>fromToken</code> and <code>toToken</code> parameters
   * define an inclusive set of tokens for which alternatives
   * are required.  <code>fromToken</code> and <code>toToken</code>
   * are typically included in the set of alternative guesses.
   * If <code>toToken</code> is <code>null</code> or if
   * <code>fromToken</code> and <code>toToken</code> are the same, then
   * alternatives are provided for the single <code>toToken</code> token.
   * <P>
   *
   * The tokens passed to this method must be <code>ResultToken</code>
   * objects provided by this result through previous calls to
   * <code>getAlternativeTokens</code> or the <code>getBestToken</code>
   * and <code>getBestTokens</code> methods of the <code>Result</code>
   * interface.
   * <P>
   *
   * <B>Returned Array Structure</B>
   * <P>
   *
   * The return value is a <em>ragged two-dimension array</em> with indices
   * being <code>altTokens[guessNumber][tokenNumber]</code>.
   * The guesses are ordered by from best guess to least likely
   * (determined by the recognizer): <code>altTokens[0]</code> is the
   * best guess for the span of <code>fromToken</code> to <code>toToken</code>,
   * <code>altTokens[1]</code> is the first alternative guess and so on.
   * <P>
   *
   * The number of tokens may be different in each alternative guess.
   * This means that the length of <code>altTokens[0]</code> may be
   * different from the length of <code>altTokens[1]</code> and so on.
   * The length is never zero or less.  (This point is illustrated in
   * the example below.)
   * <P>
   *
   * The <code>max</code> parameter indicates the maximum number of alternative
   * guesses to be returned.  The number of guesses returned may be less than
   * or equal to <code>max</code>.  The number of alternatives
   * returned is also less than or equal to the <code>NumResultAlternatives</code>
   * property set in the <code>RecognizerProperties</code> at the time of
   * recognition.
   * <P>
   *
   * The number of alternative guesses and the number of tokens in each
   * guess can vary between results.  The numbers can vary for different
   * values of <code>fromToken</code> and <code>toToken</code> for the
   * calls to the same result.
   * <P>
   *
   * The returned alternative guess is always an array of length one or
   * greater.  If there is only one guess, it may be the sequence
   * of <code>fromToken</code> to <code>toToken</code> tokens.
   * Each guess always contains one or more tokens.  If the result is
   * <code>ACCEPTED</code> then the recognizer is confident that
   * all the alternatives guesses are reasonable guesses of what the
   * user said.
   * <P>
   *
   * <B>Example</B>
   * <P>
   *
   * Assume the user says "You can recognize speech" and that
   * the recognizer accepts a result and hears the utterance correctly.
   * The result will have four tokens (<code>Result.numTokens() == 4</code>)
   * and the best guess tokens returned by the <code>Result.getBestTokens()</code>
   * method returns and array with <code>{"you", "can", "recognize", "speech"}</code>.
   * (The <code>Result.getBestToken(int index)</code> method will return the
   * tokens one at a time.)
   * <P>
   *
   * Let's take the second and last tokens ("can" and "speech") and request
   * five alternative guesses for the sequence including those tokens:
   *
   * <pre>
   *      FinalDictationResult result;
   *      ResultToken secondToken = result.getBestToken(1);
   *      ResultToken lastToken = result.getBestToken(result.numTokens());
   *      ResultToken[][] altTokens = result.getAlternativeTokens(secondToken, lastToken, 5);
   * </pre>
   *
   * The return value might look like the following (representing each token
   * by its written-form string):
   *
   * <PRE>
   *   altTokens = { {"can", "recognize", "speech"},
   *                 {"can", "wreck", "a", "nice", "beach"},
   *                 {"in", "recognize", "beach"},
   *                 {"recognize", "speech"} }
   * </PRE>
   * <P>
   *
   * Some observations from this example which illustrate the points
   * described above.
   * <P>
   *
   * <UL>
   *  <LI>Only 4 guesses are returned, less than 5 guesses requested in
   *      the <code>getAlternativeTokens</code> call.
   *  <LI>The first token sequence is the same as the best guess as returned
   *      by the <code>getBestTokens</code> method.  (As we expected,
   *      "can recognize speech" is the recognizer's best guess for
   *      that segment of incoming speech.)
   *  <LI>The number of tokens is different between guesses: 3, 5, 3, 2 tokens in each.
   * </UL>
   * <P>
   *
   * @see Result
   * @see Result#numTokens
   * @see Result#getBestToken
   * @see Result#getBestTokens
   * @see FinalResult#tokenCorrection
   * @see RecognizerProperties#setNumResultAlternatives
   * @exception ResultStateError
   *   if called before a result is finalized or if the matched
   *   grammar is not a <code>DictationGrammar</code>
   * @exception IllegalArgumentException
   *   if passed a <code>ResultToken</code> not obtained from this result
   */

  public ResultToken[][] getAlternativeTokens(ResultToken fromToken,
					      ResultToken toToken, int max)
    throws ResultStateError, IllegalArgumentException;
}

