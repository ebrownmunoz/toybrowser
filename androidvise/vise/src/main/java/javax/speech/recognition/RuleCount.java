/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Attaches a count to a contained <code>Rule</code> object to
 * indicate the number of times it may occur.  The contained 
 * rule may occur optionally (zero or one times), one or more times, 
 * or zero or more times.  The three count are equivalent to 
 * the "[]", "+" and "*" operators of the Java Speech Grammar Format.  
 * <P>
 *
 * Any <code>Rule</code> not contained by a <code>RuleCount</code> object
 * occurs once only.
 * <P>
 *
 * @see Rule
 * @see RuleAlternatives
 * @see RuleGrammar
 * @see RuleName
 * @see RuleParse
 * @see RuleSequence
 * @see RuleTag
 * @see RuleToken
 */


public class RuleCount extends Rule
{
  /**
   * The rule to which the count applies.
   *
   * @serial
   */

  protected Rule rule;
  

  /**
   * Identifier for the rule count.
   * <P>
   *
   * @see #OPTIONAL
   * @see #ONCE_OR_MORE
   * @see #ZERO_OR_MORE
   * @serial
   */

  protected int count;


  /**
   * <code>OPTIONAL</code> indicates that the <code>Rule</code> is optional: 
   * zero or one occurrences.  An optional <code>Rule</code> is surrounded 
   * by "[]" in Java Speech Grammar Format.
   *
   * @see #getCount
   */

  public static int OPTIONAL = 2;


  /**
   * <code>ONCE_OR_MORE</code> indicates that the <code>Rule</code> may be 
   * spoken one or more times.  This is indicated by the "+" operator in 
   * Java Speech Grammar Format.
   *
   * @see #getCount
   */

  public static int ONCE_OR_MORE = 3;


  /**
   * <code>ZERO_OR_MORE</code> indicates that the <code>Rule</code> may be
   * spoken zero or more times.  This is indicated by the "*" operator in 
   * Java Speech Grammar Format.
   *
   * @see #getCount
   */

  public static int ZERO_OR_MORE = 4;


  /**
   * <code>RuleCount</code> constructor with contained rule and count.
   */

  public RuleCount(Rule rule, int count)
    {
      setRule(rule);
      setCount(count);
    }


  /**
   * Empty constructor sets rule to <code>null</code> and count to <code>OPTIONAL</code>.
   */

  public RuleCount()
    {
      setRule(null);
      setCount(OPTIONAL);
    }


  /**
   * Returns the contained <code>Rule</code> object.
   */

  public Rule getRule() {return rule;}


  /**
   * Set the contained <code>Rule</code> object.
   */

  public void setRule(Rule rule) {this.rule = rule;}


  /**
   * Returns the count: <code>OPTIONAL</code>, <code>ZERO_OR_MORE</code>, 
   * <code>ONCE_OR_MORE</code>.
   * <P>
   *
   * @see #OPTIONAL
   * @see #ZERO_OR_MORE
   * @see #ONCE_OR_MORE
   */

  public int getCount() {return count;}


  /**
   * Set the count.  If count is not one of the defined values
   * <code>OPTIONAL</code>, <code>ZERO_OR_MORE</code>, <code>ONCE_OR_MORE</code>)
   * the call is ignored.
   * <P>
   *
   * @see #OPTIONAL
   * @see #ZERO_OR_MORE
   * @see #ONCE_OR_MORE
   */

  public void setCount(int count)
    {
      if (count != OPTIONAL && count != ZERO_OR_MORE && count != ONCE_OR_MORE)
	return;

      this.count = count;
    }


  /**
   * Return a deep copy of this rule.
   * See the <A HREF="Rule.html#copy()"><code>Rule.copy</code></A>
   * documentation for an explanation of deep copy.
   */

  public Rule copy()
  {
    return new RuleCount(rule.copy(), count);
  }


  /**
   * Return a string representing the <code>RuleCount</code> object in
   * partial Java Speech Grammar Format.  The String represents the portion 
   * of Java Speech Grammar Format that could appear on the right hand side
   * of a rule definition.  Parenthesis will be placed 
   * around the contained <code>Rule</code> object if required.
   * The output appears as one of:
   *
   * <PRE>
   *   [ruleString]   // OPTIONAL
   *   ruleString *   // ZERO_OR_MORE
   *   ruleString +   // ONCE_OR_MORE
   * </PRE>
   */

  public String toString()
    {
      // Simple case is [] which groups the contained rule
      
      if (count == OPTIONAL)
	return '[' + rule.toString() + ']';

      String base = null;

      // RuleToken and RuleName have higher precedence than RuleCount
      // so there's no need to use ().
      // RuleCount, RuleTag require () because of single unary operator
      // per expansion condition.
      // RuleSequence, RuleAlternatives, RuleParse are lower precedence so need ()

      if ((rule instanceof RuleToken) || (rule instanceof RuleName))
	base = rule.toString();
      else
	base = '(' + rule.toString() + ')';

      // Finally add the unary operator.

      if (count == ZERO_OR_MORE)
	return base + " *";
      else if (count == ONCE_OR_MORE)
	return base + " +";
      else
	return base + "???";
    }
}

