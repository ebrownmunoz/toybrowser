/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.recognition;

import javax.speech.*;


/**
 * Event issued by <code>Recognizer</code> through <code>RecognizerListener</code>.
 * Inherits the following event types from <code>EngineEvent</code>:
 *  <code>ENGINE_ALLOCATED</code>,
 *  <code>ENGINE_DEALLOCATED</code>,
 *  <code>ENGINE_ALLOCATING_RESOURCES</code>,
 *  <code>ENGINE_DEALLOCATING_RESOURCES</code>,
 *  <code>ENGINE_PAUSED</code>,
 *  <code>ENGINE_RESUMED</code>.
 * <P>
 *
 * The source object for any <code>RecognizerEvent</code> is the 
 * <code>Recognizer</code>.
 * <P>
 *
 * @see EngineEvent
 * @see Recognizer
 * @see RecognizerListener
 */


public class RecognizerEvent extends EngineEvent
{
  /**
   * <code>RECOGNIZER_PROCESSING</code> event is issued when a
   * <code>Recognizer</code> changes from the <code>LISTENING</code>
   * state to the <code>PROCESSING</code> state to indicate that
   * it is actively processing a recognition <code>Result</code>.
   * The transition is triggered when the recognizer detects
   * speech in the incoming audio stream that may match
   * and active grammar.  The transition occurs immediately 
   * before the <code>RESULT_CREATED</code> is issued to 
   * <code>ResultListeners</code>.
   * <P>
   *
   * @see RecognizerListener#recognizerProcessing
   * @see Recognizer#LISTENING
   * @see Recognizer#PROCESSING
   * @see ResultEvent#RESULT_CREATED
   * @see Engine#getEngineState
   * @see EngineEvent#getId
   */

  public final static int RECOGNIZER_PROCESSING = 1200;


  /**
   * <code>RECOGNIZER_SUSPENDED</code> event is issued when a
   * <code>Recognizer</code> changes from either the <code>LISTENING</code>
   * state or the <code>PROCESSING</code> state to the 
   * <code>SUSPENDED</code> state.
   * <P>
   *
   * A transition from the <code>LISTENING</code> state to the
   * <code>SUSPENDED</code> state is triggered by a call to either the
   * <code>suspend</code> method or the <code>commitChanges</code> method.
   * <P>
   *
   * A transition from the <code>PROCESSING</code> state to the
   * <code>SUSPENDED</code> state is triggered by the finalization
   * of the result currently being recognized.
   * In this instance, the <code>RECOGNIZER_SUSPENDED</code> event is
   * followed immediately by either the <code>RESULT_ACCEPTED</code>
   * or <code>RESULT_REJECTED</code> event that finalizes the result.
   * <P>
   *
   * @see RecognizerListener#recognizerSuspended
   * @see Recognizer#LISTENING
   * @see Recognizer#PROCESSING
   * @see Recognizer#SUSPENDED
   * @see Recognizer#suspend
   * @see Recognizer#commitChanges
   * @see Engine#getEngineState
   * @see EngineEvent#getId
   * @see ResultEvent#RESULT_ACCEPTED
   * @see ResultEvent#RESULT_REJECTED
   */

  public final static int RECOGNIZER_SUSPENDED = 1202;


  /**
   * <code>CHANGES_COMMITTED</code> event is issued when a
   * <code>Recognizer</code> changes from the <code>SUSPENDED</code>
   * state to the <code>LISTENING</code> state.
   * This state transition takes place when changes to the definition
   * and enabled state of all a recognizer's grammars have been applied.
   * The new grammar definitions are used as incoming speech is 
   * recognized in the <code>LISTENING</code> and <code>PROCESSING</code> 
   * states of the <code>Recognizer</code>.
   * <P>
   *
   * Immediately following the <code>CHANGES_COMMITTED</code> event,
   * <code>GRAMMAR_CHANGES_COMMITTED</code> events are issued to
   * the <code>GrammarListeners</code> of each changed <code>Grammar</code>.
   * <P>
   *
   * If any errors are detected in any grammar's definition during
   * the commit, a <code>GrammarException</code> is provided with this
   * event.  The <code>GrammarException</code> is also included with
   * the <code>GRAMMAR_CHANGES_COMMITTED</code> event to <code>Grammar</code>
   * with the error.  The <code>GrammarException</code> has the same
   * function as the <code>GrammarException</code> thrown on the
   * <code>commitChanges</code> method.
   * <P>
   *
   * The causes and timing of the <code>CHANGES_COMMITTED</code> event
   * are described with the <A HREF="Recognizer.html#normalEvents">
   * state transition documentation</A> for a <code>Recognizer</code>
   * with the the <A HREF="Grammar.html#commit">committing changes
   * documentation</A> for a <code>Grammar</code>.
   * <P>
   *
   * @see RecognizerListener#changesCommitted
   * @see Recognizer#commitChanges
   * @see Recognizer#LISTENING
   * @see Recognizer#SUSPENDED
   * @see GrammarEvent#GRAMMAR_CHANGES_COMMITTED
   * @see Engine#getEngineState
   * @see EngineEvent#getId
   */

  public final static int CHANGES_COMMITTED = 1203;


  /**
   * <code>FOCUS_GAINED</code> event is issued when a
   * <code>Recognizer</code> changes from the <code>FOCUS_OFF</code>
   * state to the <code>FOCUS_ON</code> state.  This event
   * typically occurs as a result of a call the the
   * <code>requestFocus</code> method of the <code>Recognizer</code>.
   * <P>
   *
   * The event indicates that the <code>FOCUS_ON</code> bit
   * of the engine state is set.
   * <P>
   *
   * Since recognizer focus is a key factor in the activation
   * policy for grammars, a <code>FOCUS_GAINED</code> event is followed
   * by a <code>GRAMMAR_ACTIVATED</code> event to the
   * <code>GrammarListeners</code> of each <code>Grammar</code> 
   * that is activated.  Activation conditions and the role
   * of recognizer focus are detailed in the documentation for
   * the <code>Grammar</code> interface.
   * <P>
   *
   * @see RecognizerListener#focusGained
   * @see #FOCUS_LOST
   * @see Recognizer#FOCUS_ON
   * @see Recognizer#requestFocus
   * @see GrammarEvent#GRAMMAR_ACTIVATED
   * @see Grammar
   */

  public final static int FOCUS_GAINED = 1204;


  /**
   * <code>FOCUS_LOST</code> event is issued when a
   * <code>Recognizer</code> changes from the <code>FOCUS_ON</code>
   * state to the <code>FOCUS_OFF</code> state.  This event
   * may occur as a result of a call to the <code>releaseFocus</code>
   * method of the <code>Recognizer</code> or because another application 
   * has requested recognizer focus.
   * <P>
   *
   * The event indicates that the <code>FOCUS_OFF</code> bit
   * of the engine state is set.
   * <P>
   *
   * Since recognizer focus is a key factor in the activation
   * policy for grammars, a <code>FOCUS_LOST</code> event is followed
   * by a <code>GRAMMAR_DEACTIVATED</code> event to the
   * <code>GrammarListeners</code> of each <code>Grammar</code> 
   * that loses activatation.  Activation conditions and the role
   * of recognizer focus are detailed in the documentation for
   * the <code>Grammar</code> interface.
   * <P>
   *
   * @see RecognizerListener#focusLost
   * @see #FOCUS_GAINED
   * @see Recognizer#FOCUS_OFF
   * @see Recognizer#releaseFocus
   * @see GrammarEvent#GRAMMAR_DEACTIVATED
   * @see Grammar
   */

  public final static int FOCUS_LOST = 1205;


  /**
   * Non-null if any error is detected in a grammar's definition while
   * producing a <code>CHANGES_COMMITTED</code> event.  
   * <code>null</code> for other event types.
   * The exception serves the same functional role as the 
   * <code>GrammarException</code> thrown on the <code>commitChanges</code>
   * method.
   * <P>
   *
   * @see #getGrammarException
   * @see Recognizer#commitChanges
   * @serial
   */

  protected GrammarException grammarException;


  /**
   * Construct a <code>RecognizerEvent</code> with a specified event source,
   * event identifier, old and new states, and optionally a 
   * <code>GrammarException</code> for a <code>CHANGES_COMMITTED</code> event.
   * <P>
   *
   * @param source
   *   the <code>Recognizer</code> that issued the event
   * @param id
   *   the identifier for the event type
   * @param oldEngineState
   *   engine state prior to this event
   * @param newEngineState
   *   engine state following this event
   * @param grammarException
   *   non-null if an error is detected during <code>CHANGES_COMMITTED</code>
   */

  public RecognizerEvent(Recognizer source, int id, 
			 long oldEngineState, long newEngineState,
			 GrammarException grammarException)
    {
      super(source, id, oldEngineState, newEngineState);

      this.grammarException = grammarException;
    }


  /**
   * Returns non-null for a <code>CHANGES_COMMITTED</code>
   * event if an error is found in the grammar definition.
   * The exception serves the same functional role as the 
   * <code>GrammarException</code> thrown on the <code>commitChanges</code>
   * method.
   * <P>
   *
   * @see Recognizer#commitChanges
   */

  public GrammarException getGrammarException() { return grammarException; }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch (id) {
    case CHANGES_COMMITTED:
      if (grammarException != null)
	return "CHANGES_COMMITTED: " + grammarException.getMessage();
      else
	return "CHANGES_COMMITTED";

    case RECOGNIZER_PROCESSING:  return "RECOGNIZER_PROCESSING";
    case RECOGNIZER_SUSPENDED:   return "RECOGNIZER_SUSPENDED";
    case FOCUS_GAINED:           return "FOCUS_GAINED";
    case FOCUS_LOST:             return "FOCUS_LOST";
    default: return super.paramString();
    }
  }
}

