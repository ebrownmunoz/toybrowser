/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.lang.Exception;


/**
 * Signals that a Java Speech API exception has occurred.
 * <code>SpeechException</code> is the super class of all exceptions
 * in the <code>javax.speech</code> packages.
 * <P>
 *
 * In addition to exceptions that inherit from <code>SpeechException</code> 
 * some calls throw other Java <code>Exceptions</code> and <code>Errors</code>
 * such as <code>IllegalArgumentException</code> and <code>SecurityException</code>.
 * <P>
 *
 * @see SpeechError
 */

public class SpeechException extends Exception
{
  /**
   * Constructs a <code>SpeechException</code> with no detail message.
   */

  public SpeechException()
  {
    super();
  }

  /**
   * Constructs a <code>SpeechException</code> with the specified detail message.
   * A detail message is a <code>String</code> that describes this particular exception.
   *
   * @param s the detail message
   */

  public SpeechException(String s)
  {
    super(s);
  }
}

