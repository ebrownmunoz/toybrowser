/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

/**
 * Signals that an error occurred while trying to create or access
 * a speech synthesis engine, speech recognition engine or 
 * <code>EngineCentral</code> object.
 */

public class EngineException extends SpeechException
{
  /**
   * Construct an <code>EngineException</code> with no detail message.
   */

  public EngineException() 
  {
    super();
  }

  /**
   * Construct an <code>EngineException</code> with a detail message.
   * A detail message is a <code>String</code> that describes this particular exception.
   */

  public EngineException(String s) 
  {
    super(s);
  }
}

