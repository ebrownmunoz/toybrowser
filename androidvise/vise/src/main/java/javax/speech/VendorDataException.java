/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;


/**
 * Signals that a problem has been encountered loading or saving
 * some type of vendor-specific data.
 * <P>
 *
 * @see javax.speech.recognition.Recognizer#readVendorGrammar(java.io.InputStream)
 * @see javax.speech.recognition.Recognizer#readVendorResult(java.io.InputStream)
 * @see javax.speech.recognition.SpeakerManager#readVendorSpeakerProfile(java.io.InputStream)
 */

public class VendorDataException extends SpeechException
{
  /**
   * Construct a <code>VendorDataException</code> with no detail message.
   */

  public VendorDataException() 
  {
    super();
  }

  /**
   * Construct a <code>VendorDataException</code> with the specified detail message.
   * A detail message is a <code>String</code> that describes this particular exception.
   *
   * @param s the detail message
   */

  public VendorDataException(String s) 
  {
    super(s);
  }
}

