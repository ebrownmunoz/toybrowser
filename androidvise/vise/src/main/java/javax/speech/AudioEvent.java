/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.util.*;


/**
 * Describes events associated with audio input/output for an <code>Engine</code>.
 * The event source is an <code>Engine</code> object.
 * <P>
 *
 * Extended by the <code>RecognizerAudioEvent</code> class
 * that provides specialized events for a <code>Recognizer</code>.
 * <P>
 *
 * <STRONG>Note:</STRONG> until the Java Sound API is finalized,
 * the <code>AudioManager</code> and other audio classes and 
 * interfaces will remain as placeholders for future expansion.
 * Only the <code>Recognizer</code> audio events are functional in this release.
 * <P>
 *
 * @see Engine
 * @see javax.speech.recognition.RecognizerAudioEvent
 */


public class AudioEvent extends SpeechEvent
{
  /**
   * Constructs an AudioEvent with a specified id.
   * <P>
   *
   * @param source <code>Engine</code> that produced the event
   * @param id type of audio event
   */

  public AudioEvent(Engine source, int id)
  {
    super(source, id);
  }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch(id) {
    default:
      return super.paramString();
    }
  }
}

