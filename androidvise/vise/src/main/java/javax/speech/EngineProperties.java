/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.beans.*;


/**
 * An <code>EngineProperties</code> object defines the set of run-time
 * properties of an <code>Engine</code>.  This interface is extended for
 * each type of speech engine.  <code>SynthesizerProperties</code> and
 * <code>RecognizerProperties</code> define the additional run-time
 * properties of synthesizers and recognizers respectively.
 * The <code>EngineProperties</code> object for an <code>Engine</code> is
 * obtained from the <code>getEngineProperties</code> method of the engine,
 * and should be cast appropriately for the type of engine.  For example:
 *
 * <PRE>
 *    Synthesizer synth = ...;
 *    SynthesizerProperties props = (SynthesizerProperties)synth.getEngineProperties();
 * </PRE>
 *
 * Each property of an engine has a set and get method.  The method
 * signatures follow the JavaBeans design patterns (outlined below).
 * <P>
 *
 * The run-time properties of an engine affect the behavior of a
 * running engine.  Technically, properties affect engines in the
 * <code>ALLOCATED</code> state.  Normally, property changes are made
 * on an <code>ALLOCATED</code> engine and take effect immediately
 * or soon after the change call.
 * <P>
 *
 * The <code>EngineProperties</code> object for an engine is, however,
 * available in all states of an <code>Engine</code>.  Changes
 * made to the properties of engine in the <code>DEALLOCATED</code> or
 * the <code>ALLOCATING_RESOURCES</code> state take effect when the
 * engine next enters the <code>ALLOCATED</code> state.  A typical
 * scenario for setting the properties of a non-allocated is determining
 * the initial state of the engine.  For example, setting the initial
 * voice of a <code>Synthesizer</code>, or the initial <code>SpeakerProfile</code>
 * of a <code>Recognizer</code>.  (Setting these properties prior to
 * allocation is desirable because allocating the engine and then changing
 * the voice or the speaker can be computationally expensive.)
 * <P>
 *
 * When setting any engine property:
 * <P>
 *
 * <UL>
 *  <LI>The engine may choose to ignore a set value either because
 *      it does not support changes in a property or because it is out-of-range.
 *  <LI>The engine will apply the property change as soon as possible, but
 *      the change is not necessarily immediate.  Thus, all set methods are
 *      asynchronous (call may return before the effect takes place).
 *  <LI>All properties of an engine are <em>bound</em> properties - JavaBeans
 *      terminology for a property for which an event is issued when the
 *      property changes.  A <code>PropertyChangeListener</code> can be
 *      attached to the <code>EngineProperties</code> object to receive a
 *      property change event when a change takes effect.
 * </UL>
 * <P>
 *
 * For example, a call to the <code>setPitch</code> method of the
 * <code>SynthesizerProperties</code> interface to change  pitch from 120Hz to 200Hz
 * might fail because the value is out-of-range.  If it does succeed, the
 * pitch change might be deferred until the synthesizer can make the
 * change by waiting to the end of the current word, sentence, paragraph
 * or text object.  When the change does take effect, a <code>PropertyChangeEvent</code>
 * is issued to all attached listeners with the name of the changed
 * property ("Pitch"), the old value (120) and the new value (200).
 * <P>
 *
 * Set calls take effect in the order in which they are received.
 * If multiple changed are requested are requested for a single property,
 * a separate event is issued for each call, even if the multiple changes
 * take place simultaneously.
 * <P>
 *
 * The properties of an engine are persistent across sessions where possible.
 * It is the engine's responsibility to store and restore the property
 * settings.  In multi-user and client-server environments the store/restore
 * policy is at the discretion of the engine.
 * <P>
 *
 * <B>Control Component</B>
 * <P>
 *
 * An engine may provide a control object through the <code>getControlComponent</code>
 * method of its <code>EngineProperties</code> object.
 * The control object is an AWT <code>Component</code>.  If provided, that
 * component can be displayed to a user for customization of the engine.
 * Because the component is implemented by the engine, the display may
 * support customization of engine-specific properties that are not
 * accessible through the standard properties interfaces.
 * <P>
 *
 * <B>JavaBeans Properties</B>
 * <P>
 *
 * The JavaBeans property patterns are followed for engine properties.
 * A property is defined by its name and its property type (for example,
 * "Pitch" is a float).  The property is accessed through get and set
 * methods.  The signature of the property accessor methods are:
 *
 * <PRE>
 *       public void set&lt;PropertyName&gt;(&lt;PropertyType&gt; value);
 *       public &lt;PropertyType&gt; get&lt;PropertyName&gt;();
 * </PRE>
 *
 * For boolean-valued properties, the <CODE> get&lt;PropertyName&gt;() </CODE>
 * may also be <CODE> is&lt;PropertyName&gt;() </CODE>
 * <P>
 *
 * For indexed properties (arrays) the signature of the
 * property accessor methods are:
 *
 * <PRE>
 *       public void set&lt;PropertyName&gt;(&lt;PropertyType&gt[]; value);
 *       public void set&lt;PropertyName&gt;(int i, &lt;PropertyType&gt; value);
 *       public &lt;PropertyType&gt;[] get&lt;PropertyName&gt;();
 *       public &lt;PropertyType&gt; get&lt;PropertyName&gt;(int i);
 * </PRE>
 *
 * For example speaking rate (for a Synthesizer) is a floating value
 * and has the following methods:
 *
 * <PRE>
 *       public void setSpeakingRate(float value);
 *       public float getSpeakingRate();
 * </PRE>
 *
 *
 * @see Engine
 * @see Engine#getEngineProperties
 * @see javax.speech.recognition.RecognizerProperties
 * @see javax.speech.synthesis.SynthesizerProperties
 * @see PropertyChangeListener
 * @see PropertyChangeEvent
 */


public interface EngineProperties
{
  /**
   * The reset method returns all properties to reasonable defaults
   * for the <code>Engine</code>.  A property change event is issued
   * for each engine property that changes as the reset takes effect.
   */

  void reset();


  /**
   * Add a <code>PropertyChangeListener</code> to the listener list.
   * The listener is registered for all properties of the engine..
   * <P>
   *
   * A <code>PropertyChangeEvent</code> is fired in response to setting
   * any bound property.
   *
   * @param listener - The <code>PropertyChangeListener</code> to be added
   */

  void addPropertyChangeListener(PropertyChangeListener listener);


  /**
   * Remove a <code>PropertyChangeListener</code> from the listener list.
   *
   * @param listener - The <code>PropertyChangeListener</code> to be removed
   */

  void removePropertyChangeListener(PropertyChangeListener listener);
}

