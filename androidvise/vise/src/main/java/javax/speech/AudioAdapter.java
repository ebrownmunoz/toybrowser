/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;


/**
 * A trivial implementation of the <code>AudioListener</code> interface
 * that receives an engine's audio events.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 * <P>
 *
 * Extended by the <code>RecognizerAudioAdapter</code> that adds
 * specialized audio events for a <code>Recognizer</code>.
 * <P>
 *
 * <STRONG>Note:</STRONG> until the Java Sound API is finalized,
 * the <code>AudioManager</code> and other audio classes and 
 * interfaces will remain as placeholders for future expansion.
 * Only the <code>Recognizer</code> audio events are functional in this release.
 * <P>
 *
 * @see javax.speech.recognition.RecognizerAudioAdapter
 */


public class AudioAdapter implements AudioListener
{
}


