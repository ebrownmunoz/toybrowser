/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.io.*;
import java.util.*;
//import java.security.AccessController;

import javax.speech.synthesis.*;
import javax.speech.recognition.*;

/**
 * The <code>Central</code> class is the initial access point to all
 * speech input and output capabilities.
 * <code>Central</code> provides the ability to locate, select and
 * create speech recognizers and speech synthesizers
 * <P>
 *
 * <A NAME="creatingEngines"></A>
 * <B>Creating a <code>Recognizer</code> or <code>Synthesizer</code></B>
 * <P>
 *
 * The <code>createRecognizer</code> and <code>createSynthesizer</code>
 * methods are used to create speech engines.  Both methods accept
 * a single parameter that defines the required properties for
 * the engine to be created.  The parameter is an <code>EngineModeDesc</code>
 * and may be one of the sub-classes: <code>RecognizerModeDesc</code>
 * or <code>SynthesizerModeDesc</code>.
 * <P>
 *
 * A mode descriptor defines a set of required properties for an engine.
 * For example, a <code>SynthesizerModeDesc</code> can describe
 * a <code>Synthesizer</code> for Swiss German that has a male voice.
 * Similarly, a <code>RecognizerModeDesc</code> can describe a
 * <code>Recognizer</code> that supports dictation for Japanese.
 * <P>
 *
 * An application is responsible for determining its own functional 
 * requirements for speech input/output and providing an appropriate
 * mode descriptor.  There are three cases for mode descriptors:
 * <P>
 *
 * <OL>
 *  <LI><code>null</code>
 *  <LI>Created by the application
 *  <LI>Obtained from the <code>availableRecognizers</code>
 *      or <code>availableSynthesizers</code> methods of <code>Central</code>. 
 * </OL>
 * <P>
 *
 * The mode descriptor is passed to the <code>createRecognizer</code>
 * or <code>createSynthesizer</code> methods of <code>Central</code> to create 
 * a <code>Recognizer</code> or <code>Synthesizer</code>.  The created
 * engine matches all the engine properties in the mode descriptor passed
 * to the create method.  If no suitable speech engine is available, 
 * the create methods return <code>null</code>.
 * <P>
 *
 * The create engine methods operate differently for the three cases.
 * That is, engine selection depends upon the type of the mode descriptor:
 * <P>
 *
 * <OL>
 *  <LI><code>null</code> mode descriptor: the <code>Central</code> class
 *      selects a suitable engine for the default <code>Locale</code>.
 *  <LI>Application-created mode descriptor: the <code>Central</code>
 *      class attempts to locate an engine with all application-specified
 *      properties.
 *  <LI>Mode descriptor from <code>availableRecognizers</code>
 *      or <code>availableSynthesizers</code>: descriptors returned
 *      by these two methods identify a specific engine with a
 *      specific operating mode. <code>Central</code> creates an instance
 *      of that engine.  (Note: these mode descriptors are distinguished
 *      because they implement the <code>EngineCreate</code> interface.)
 * </OL>
 * <P>
 *
 * <B>Case 1: Example</B>
 * <P>
 *
 * <pre>
 *    // Create a synthesizer for the default Locale
 *    Synthesizer synth = Central.createSynthesizer(null);
 * </pre>
 *
 * <B>Case 2: Example</B>
 * <P>
 *
 * <pre>
 *    // Create a dictation recognizer for British English
 *    // Note: the UK locale is English spoken in Britain
 *    RecognizerModeDesc desc = new RecognizerModeDesc(Locale.UK, Boolean.TRUE);
 *    Recognizer rec = Central.createRecognizer(desc);
 * </pre>
 *
 * <B>Case 3: Example</B>
 * <P>
 *
 * <pre>
 *    // Obtain a list of all German recognizers
 *    RecognizerModeDesc desc = new RecognizerModeDesc(Locale.GERMAN);
 *    EngineList list = Central.availableRecognizers(desc);
 *    // select amongst by other desired engine properties
 *    RecognizerModeDesc chosen = ...
 *    // create an engine from "chosen" - an engine-provided descriptor
 *    Recognizer rec = Central.createRecognizer(chosen);
 * </pre>
 *
 * <P>
 *
 * <B>Engine Selection Procedure: Cases 1 &amp; 2</B>
 * <P>
 *
 * For cases 1 and 2 there is a defined procedure for selecting 
 * an engine to be created.  (For case 3, the application
 * can apply it's own selection procedure.)
 * <P>
 *
 * <code>Locale</code> is treated specially in the selection to ensure 
 * that language is always considered when selecting an engine.
 * If a locale is not provided, the default locale 
 * (<code>java.util.Locale.getDefault</code>) is used.
 * <P>
 *
 * The selection procedure is:
 *
 * <OL type=1>
 *  <LI>
 *   If the locale is undefined add the language of the
 *   default locale to the required properties.
 *
 *  <LI>
 *   If a <code>Recognizer</code> or <code>Synthesizer</code>
 *   has been created already and it has the required properties,
 *   return a reference to it.  (The last created engine is checked.)
 *
 *  <LI>
 *   Obtain a list of all recognizer or synthesizer modes that
 *   match the required properties.
 *
 *  <LI>
 *   Amongst the matching engines, give preference to:
 *
 *  <UL>
 *    <LI>A running engine (<code>EngineModeDesc.getRunning</code> is <code>true</code>),
 *    <LI>An engine that matches the default locale's country.
 *  </UL>
 *
 * </OL>
 *
 * When more than one engine is a legal match in the final step, the engines
 * are ordered as returned by the <code>availableRecognizers</code> 
 * or <code>availableSynthesizers</code> method.
 * <P>
 *
 * <B>Security</B>
 * <P>
 *
 * Access to speech engines is restricted by Java's security system.
 * This is to ensure that malicious applets don't use the speech
 * engines inappropriately.  For example, a recognizer should
 * not be usable without explicit permission because it could be used to
 * monitor ("bug") an office.
 * <P>
 *
 * A number of methods throughout the API throw <code>SecurityException</code>.
 * Individual implementations of <code>Recognizer</code> and <code>Synthesizer</code>
 * may throw <code>SecurityException</code> on additional methods as required
 * to protect a client from malicious applications and applets.
 * <P>
 *
 * The <A HREF="SpeechPermission.html"><code>SpeechPermission</code></A>
 * class defines the types of permission that can be granted or
 * denied for applications.  This permission system is based on
 * the JDK 1.2 fine-grained security model.
 * <P>
 *
 * <B>Engine Registration</B>
 * <P>
 *
 * The <code>Central</code> class locates, selects and creates
 * speech engines from amongst a list of registered engines.
 * Thus, for an engine to be used by Java applications, the engine
 * must register itself with <code>Central</code>.  There are
 * two registration mechanisms: (1) add an <code>EngineCentral</code>
 * class to a speech properties file, (2) temporarily register
 * an engine by calling the <code>registerEngineCentral</code> method.
 * <P>
 *
 * The speech properties files provide <em>persistent</em> registration
 * of speech engines.  When <code>Central</code> is first called, it 
 * looks for properties in two files:
 *
 * <pre>
 *    &lt;user.home&gt;/speech.properties
 *    &lt;java.home&gt;/lib/speech.properties </pre>
 *
 * where the <code>&lt;user.home&gt;</code> and <code>&lt;java.home&gt;</code>
 * are the values obtained from the <code>System</code> properties object.
 * (The '/' separator will vary across operating systems.)
 * Engines identified in either properties file are made
 * available through the methods of <code>Central</code>.
 * <P>
 *
 * The property files must contain data in the format
 * that is read by the <code>load</code> method of the
 * <code>Properties</code> class.  <code>Central</code>
 * looks for properties of the form
 *
 * <pre>
 *    com.acme.recognizer.EngineCentral=com.acme.recognizer.AcmeEngineCentral </pre>
 *
 * This line is interpreted as "the <code>EngineCentral</code>
 * object for the <code>com.acme.recognizer</code> engine
 * is the class called <code>com.acme.recognizer.AcmeEngineCentral</code>.
 * When it is first called, the <code>Central</code> class will
 * attempt to create an instance of each <code>EngineCentral</code>
 * object and will ensure that it implements the <code>EngineCentral</code>
 * interface.
 * <P>
 *
 * <B>Note</B> to engine providers:
 * <code>Central</code> calls each <code>EngineCentral</code>
 * for each call to <code>availableRecognizers</code> or 
 * <code>availableSynthesizers</code> and sometimes
 * <code>createRecognizer</code> and <code>createSynthesizer</code>
 * The results are not stored.  The <code>EngineCentral.createEngineList</code>
 * method should be reasonably efficient.
 */


public class Central extends Object
{
  private static boolean loadedProperties = false;
  private static Hashtable centralList = new Hashtable();

  private static Recognizer  lastRecognizer = null;
  private static Synthesizer lastSynthesizer = null;


  /**
   * Create a <code>Recognizer</code> with specified required properties.
   * If there is no <code>Recognizer</code> with the required properties
   * the method returns <code>null</code>.
   * <P>
   *
   * The required properties defined in the input parameter may be
   * provided as either an <code>EngineModeDesc</code> object or
   * a <code>RecognizerModeDesc</code> object.  The input parameter
   * may also be <code>null</code>, in which case an engine is 
   * selected that supports the language of the default locale.
   * <P>
   *
   * A non-null mode descriptor may be either application-created 
   * or a mode descriptor returned by the <code>availableRecognizers</code> method.
   * <P>
   *
   * The mechanisms for <A HREF="creatingEngines">creating a <code>Recognizer</code></A>
   * are described above in detail.  
   * <P>
   *
   * @see #availableRecognizers
   * @see javax.speech.recognition.RecognizerModeDesc
   * @param require
   *   required engine properties or <code>null</code> for
   *   default engine selection
   * @return 
   *   a recognizer matching the required properties or <code>null</code> 
   *   if none is available
   * @exception IllegalArgumentException 
   *   if the properties of the <code>EngineModeDesc</code> do not refer to a known 
   *   engine or engine mode.
   * @exception EngineException 
   *   if the engine defined by this <code>RecognizerModeDesc</code> could not be properly created. 
   * @exception SecurityException 
   *   if the caller does not have <code>createRecognizer</code> permission 
   */

  public final static synchronized javax.speech.recognition.Recognizer 
    createRecognizer(EngineModeDesc require)
    throws IllegalArgumentException, EngineException, SecurityException
  {
    // Check security permission
    // try {
    //   SpeechPermission perm = new SpeechPermission("createRecognizer");
    //   AccessController.checkPermission(perm);
    // }
    // catch (NoClassDefFoundError e) {
    //   e.printStackTrace();
    // }

    // If we reach here we have "createRecognizer" permission


    // Make sure we have a non-null require property
    if (require == null) require = new RecognizerModeDesc();


    //////////////////////////////////////////////////////////////
    // Test first for mode descriptor from availableRecognizers
    //////////////////////////////////////////////////////////////

    // If this EngineModeDesc was provided by a speech engine,
    // immediately try to create the engine.

    if (require instanceof EngineCreate) {
      EngineCreate create = (EngineCreate)require;
      return (Recognizer)(create.createEngine());
    }


    //////////////////////////////////////////////////////////////
    // Perform complete engine selection procedure
    //////////////////////////////////////////////////////////////


    boolean useDefault = false;
    String defLanguage = Locale.getDefault().getLanguage();
    String defCountry = Locale.getDefault().getCountry();

    if (require.getLocale() == null) {
      require.setLocale(new Locale(defLanguage, ""));
      useDefault = true;
    }


    // Check the lastRecognizer to see if it matches
    if (lastRecognizer != null) {
      EngineModeDesc mode = lastRecognizer.getEngineModeDesc();
      if (mode != null && mode.match(require)) {
         // Clean up the require object - no unwanted side-effects
        if (useDefault) require.setLocale(null);
        return lastRecognizer;
      }
    }

    // Find all the matching engines
    EngineList list = availableRecognizers(require);

    if (list.isEmpty())
      return null;


    // Clean up the require object - no unwanted side-effects
    if (useDefault) require.setLocale(null);


    // If using default locale, order list with preference
    // for engines matching both language and country.
    // Don't throw out any matches because 
    // createEngine might fail below on some engines.

    if (useDefault)
      list.orderByMatch(new EngineModeDesc(new Locale(defLanguage, defCountry)));


    // If the require parameter specified running is "don't care"
    // then sort the running engines to the top of the list.
    // Note: running sort is second because it is higher priority (last to sort)

    if (require.getRunning() == null) {
      EngineModeDesc desc = new EngineModeDesc();
      desc.setRunning(Boolean.TRUE);
      list.orderByMatch(desc);
    }

    for (int i = 0; i < list.size(); i++) {
      try {
        EngineCreate create = (EngineCreate)(list.elementAt(i));
        Recognizer rec = (Recognizer)create.createEngine();
        lastRecognizer = rec;
        return rec;
      } catch(IllegalArgumentException e) {
      } catch(EngineException e) {
      }
    }

    return null;
  }


  /**
   * List <code>EngineModeDesc</code> objects for available recognition 
   * engine modes that match the required properties.  
   * If the <code>require</code> parameter is <code>null</code>, then all 
   * known recognizers are listed.
   * <P>
   *
   * Returns a zero-length list if no engines are available or if no
   * engines have the required properties.  (The method never returns null).
   * <P>
   *
   * The order of the <code>EngineModeDesc</code> objects in the list
   * is partially defined.  For each registered engine (technically,
   * each registered <code>EngineCentral</code> object) the order
   * of the descriptors is preserved.  Thus, each installed speech
   * engine should order its descriptor objects with the most
   * useful modes first, for example, a mode that is already loaded
   * and running on a desktop.
   * <P>
   *
   * @param require
   *   an <code>EngineModeDesc</code> or <code>RecognizerModeDesc</code>
   *   defining the required features of the mode descriptors in the returned list
   * @return 
   *   list of mode descriptors with the required properties
   * @exception SecurityException 
   *   if the caller does not have permission to use speech recognition
   */

  public final static synchronized EngineList availableRecognizers(EngineModeDesc require)
    throws SecurityException 
  {
    // Check security permission
    // try {
    //   SpeechPermission perm = new SpeechPermission("accessEngineModeDesc");
    //   AccessController.checkPermission(perm);
    // } catch (NoClassDefFoundError e) {
    //   e.printStackTrace();
    // }

    // If we reach here we have "accessEngineModeDesc" permission

     // Make sure that we pass a RecognizerModeDesc to EngineCentral
     // Helps synthesizers ignore searches for recognizers and vice versa
    if (require == null) {
      require = new RecognizerModeDesc();
    } else if (!(require instanceof RecognizerModeDesc)) {
      require = new RecognizerModeDesc(require.getEngineName(), require.getModeName(), require.getLocale(), require.getRunning(), null, null);
    }
     // Load speech.properties
    loadProps();

    EngineList list = new EngineList();

    Enumeration engineCentrals = centralList.elements();

    while (engineCentrals.hasMoreElements()) {
      EngineCentral ec = (EngineCentral)engineCentrals.nextElement();

      EngineList l = ec.createEngineList(require);

      if (l != null) {
        for (int j = 0; j < l.size(); j++)
          if ((l.elementAt(j) instanceof RecognizerModeDesc) && (l.elementAt(j) instanceof EngineCreate))
            list.addElement(l.elementAt(j));
      }
    }
    
    return list;
  }


  /**
   * Create a <code>Synthesizer</code> with specified required properties.
   * If there is no <code>Synthesizer</code> with the required properties
   * the method returns <code>null</code>.
   * <P>
   *
   * The required properties defined in the input parameter may be
   * provided as either an <code>EngineModeDesc</code> object or
   * a <code>SynthesizerModeDesc</code> object.  The input parameter
   * may also be <code>null</code>, in which case an engine is 
   * selected that supports the language of the default locale.
   * <P>
   *
   * A non-null mode descriptor may be either application-created 
   * or a mode descriptor returned by the <code>availableSynthesizers</code> method.
   * <P>
   *
   * The mechanisms for <A HREF="creatingEngines">creating a <code>Synthesizer</code></A>
   * are described above in detail.  
   * <P>
   *
   * @see #availableSynthesizers
   * @see javax.speech.synthesis.SynthesizerModeDesc
   * @param require
   *   required engine properties or <code>null</code> for
   *   default engine selection
   * @return 
   *   a <code>Synthesizer</code> matching the required properties or <code>null</code> 
   *   if none is available
   * @exception IllegalArgumentException 
   *   if the properties of the <code>EngineModeDesc</code> do not refer to a known 
   *   engine or engine mode.
   * @exception EngineException 
   *   if the engine defined by this <code>SynthesizerModeDesc</code> could not be properly created. 
   */

  public final static synchronized javax.speech.synthesis.Synthesizer createSynthesizer(EngineModeDesc require)
    throws IllegalArgumentException, EngineException
  {
    // Make sure we have a non-null require property
    if (require == null) require = new SynthesizerModeDesc();


    //////////////////////////////////////////////////////////////
    // Test first for mode descriptor from availableSynthesizers
    //////////////////////////////////////////////////////////////

    // If this EngineModeDesc was provided by a speech engine,
    // immediately try to create the engine.

    if (require instanceof EngineCreate) {
      EngineCreate create = (EngineCreate)require;
      return (Synthesizer)(create.createEngine());
    }


    //////////////////////////////////////////////////////////////
    // Perform complete engine selection procedure
    //////////////////////////////////////////////////////////////


    boolean useDefault = false;
    String defLanguage = Locale.getDefault().getLanguage();
    String defCountry = Locale.getDefault().getCountry();

    if (require.getLocale() == null) {
      require.setLocale(new Locale(defLanguage, ""));
      useDefault = true;
    }


    // Check the lastSynthesizer to see if matches
    if (lastSynthesizer != null) {
      EngineModeDesc mode = lastSynthesizer.getEngineModeDesc();
      if (mode != null && mode.match(require)) {
         // Clean up the require object - no unwanted side-effects
        if (useDefault) require.setLocale(null);
        return lastSynthesizer;
      }
    }

    // Find all the matching engines
    EngineList list = availableSynthesizers(require);

    if (list.isEmpty())
      return null;


    // Clean up the require object - no unwanted side-effects
    if (useDefault) require.setLocale(null);


    // If using default locale, order list with preference
    // for engines matching both language and country.
    // Don't throw out any matches because 
    // createEngine might fail below on some engines.

    if (useDefault)
      list.orderByMatch(new EngineModeDesc(new Locale(defLanguage, defCountry)));


    // If the require parameter specified running is "don't care"
    // then sort the running engines to the top of the list.
    // Note: running sort is second because it is higher priority (last to sort)

    if (require.getRunning() == null) {
      EngineModeDesc desc = new EngineModeDesc();
      desc.setRunning(Boolean.TRUE);
      list.orderByMatch(desc);
    }

    for (int i = 0; i < list.size(); i++) {
      try {
        EngineCreate create = (EngineCreate)(list.elementAt(i));
        Synthesizer synth = (Synthesizer)create.createEngine();
        lastSynthesizer = synth;
        return synth;
      } catch(IllegalArgumentException e) {
      } catch(EngineException e) {
      }
    }

    return null;
  }



  /**
   * List <code>EngineModeDesc</code> objects for available synthesis
   * engine modes that match the required properties.  
   * If the <code>require</code> parameter is <code>null</code>, then all available
   * known synthesizers are listed.
   * <P>
   *
   * Returns an empty list (rather than null) if no engines are available or if no
   * engines have the required properties.
   * <P>
   *
   * The order of the <code>EngineModeDesc</code> objects in the list
   * is partially defined.  For each speech installation (technically,
   * each registered <code>EngineCentral</code> object) the order
   * of the descriptors is preserved.  Thus, each installed speech
   * engine should order its descriptor objects with the most
   * useful modes first, for example, a mode that is already loaded
   * and running on a desktop.
   * <P>
   *
   * @exception SecurityException 
   *   if the caller does not have permission to use speech engines
   */

  public final static synchronized EngineList availableSynthesizers(EngineModeDesc require)
    throws SecurityException 
  {
    // Check security permission
    // try {
    //   SpeechPermission perm = new SpeechPermission("accessEngineModeDesc");
    //   AccessController.checkPermission(perm);
    // }
    // catch (NoClassDefFoundError e) {
    //  e.printStackTrace();
    // }

    // If we reach here we have "accessEngineModeDesc" permission


    // Make sure that we pass a SynthesizerModeDesc to EngineCentral.
    if (require == null)
      require = new SynthesizerModeDesc();
    else if (!(require instanceof SynthesizerModeDesc))
      require = new SynthesizerModeDesc(require.getEngineName(), require.getModeName(), require.getLocale(), require.getRunning(), null);

    loadProps();

    EngineList list = new EngineList();

    Enumeration engineCentrals = centralList.elements();

    while (engineCentrals.hasMoreElements()) {
      EngineCentral ec = (EngineCentral)engineCentrals.nextElement();

      EngineList l = ec.createEngineList(require);

      if (l != null) {
        for (int j = 0; j < l.size(); j++)
          if ((l.elementAt(j) instanceof SynthesizerModeDesc) && (l.elementAt(j) instanceof EngineCreate))
            list.addElement(l.elementAt(j));
      }
    }
    
    return list;
  }


  /**
   * Register a speech engine with the <code>Central</code> class
   * for use by the current application. This call adds the specified
   * class name to the list of <code>EngineCentral</code> objects.
   * The registered engine is <b>not</b> stored persistently in the 
   * properties files.
   * If <code>className</code> is already registered, the call has no effect.
   * <P>
   *
   * The class identified by <code>className</code> must have an
   * empty constructor.
   * <P>
   *
   * @param className name of a class that implements the <code>EngineCentral</code>
   *   interface and provides access to an engine implementation
   * @exception EngineException
   *   if <code>className</code> is not a legal class or it does not 
   *   implement the <code>EngineCentral</code> interface
   */

  public final static synchronized void registerEngineCentral(String className)
    throws EngineException {
    // Skip if we've already got it
    if (centralList.containsKey(className))
      return;

    Class cl;
    EngineCentral engineCentral;

    try { 
      cl = Class.forName(className); 
    }
    catch(ClassNotFoundException e) {
      throw new EngineException("javax.speech.Central: no class found for " + className);
    }
    
    // Create instance and cast it to EngineCentral to check it's type
    try {
      engineCentral = (EngineCentral)cl.newInstance();
      centralList.put(className, engineCentral);
    } catch(Exception e) {
      throw new EngineException("javax.speech.Central: error creating EngineCentral from " + className);
    }
  }


  /**
   * Load in the speech properties files to create the register
   * of <code>EngineCentral</code> objects.
   */

  static final private synchronized void loadProps() {
    if (loadedProperties) return;

    loadedProperties = true;

    String sep = File.separator;

    // Props in the first files take priority

    String filenames[] = new String[2];
    filenames[0] = System.getProperty("user.home") + sep + "speech.properties";
    filenames[1] = System.getProperty("java.home") + sep + "lib" + sep + "speech.properties";


    for (int i = 0; i < filenames.length; i++) {
      String fn = filenames[i];

      Properties properties = new Properties();

      File propsFile = new File(fn);

      // Load the file
      try {
        FileInputStream in = new FileInputStream(propsFile);
        properties.load(new BufferedInputStream(in));
        in.close();
      } catch (FileNotFoundException e) {
        // do nothing if a prop file don't exist
      } catch (IOException e) {
        // do nothing if a prop file can't be read
      }

      //  Register the class name of each property ending with EngineCentral
      //  e.g. com.acme.speech.EngineCentral=com.acme.speech.AcmeSynthesizer.AcmeEngineCentral

      for (Enumeration names = properties.propertyNames() ; names.hasMoreElements() ; ) {
        String propName = (String)names.nextElement();

        if (!propName.endsWith("EngineCentral"))
          continue;

         // Get the name of the class and create a Class object
        String className = properties.getProperty(propName);

        try {
          registerEngineCentral(className);
        } catch (EngineException e) {
          // ignore entry if class does not exist or is not an EngineCentral
        }
      }
    }
  }
}
