/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

/**
 * Problem encountered connecting audio to/from a speech engine.
 */

public class AudioException extends SpeechException
{
  /**
   * Constructs a <code>AudioException</code> with no detail message.
   */

  public AudioException()
  {
    super();
  }

  /**
   * Construct an <code>AudioException</code> with the specified detail message.
   * The string describes this particular exception.
   * <P>
   *
   * @param s the detail message
   */

  public AudioException(String s)
  {
    super(s);
  }
}

