/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * Represents an object on the speech output queue of a <code>Synthesizer</code>.
 * The item is described by the source object, the speakable text,
 * a boolean value indicating whether it is a plain text object, and 
 * the <code>SpeakableListener</code> for the object.
 * <P>
 *
 * The source object is the object provided to a <code>speak</code>
 * method (a <code>Speakable</code> objects, a <code>URL</code>, or
 * a <code>String</code>).  The text is the Java Speech
 * Markup Language string or plain text obtained from the source object.
 * The listener is the <code>SpeakableListener</code> object passed
 * to the <code>speak</code> method, or <code>null</code>.
 * <P>
 *
 * The <code>enumerateQueue</code> method of a <code>Synthesizer</code>
 * provides a snapshot of the speech output queue.  It returns an
 * enumeration object that is <code>null</code> if the queue is empty
 * or contains a set of <code>SynthesizerQueueItems</code>.  The 
 * <code>Synthesizer's</code> queue cannot be manipulated through this
 * enumeration object.
 * <P>
 *
 * @see Synthesizer
 * @see Synthesizer#enumerateQueue
 */


public class SynthesizerQueueItem
{
  /** 
   * The soure object for an item on the speech output queue.
   * <P>
   *
   * @see #getSource
   */

  protected Object source;


  /** 
   * The speakable text for an item on the speech output queue.
   * <P>
   *
   * @see #getText
   */

  protected String text;


  /** 
   * True if the text object is plain text (not Java Speech Markup Language).
   * <P>
   *
   * @see #isPlainText
   */

  protected boolean plainText;


  /** 
   * The listener for this object passed to the <code>speak</code> method 
   * or <code>null</code>.
   * <P>
   *
   * @see #getSpeakableListener
   */

  protected SpeakableListener listener;


  /**
   * Construct a <code>SynthesizerQueueItem</code> with the
   * source object and speakable text.
   */

  public SynthesizerQueueItem(Object source, String text, boolean plainText,
			      SpeakableListener listener)
    {
      this.source = source;
      this.text = text;
      this.plainText = plainText;
      this.listener = listener;
    }


  /**
   * Return the source object for an item on the speech output
   * queue of a <code>Synthesizer</code>.  The source is one
   * of the three object types passed to the <code>speak</code>
   * or <code>speakPlainText</code> methods of <code>Synthesizer</code>:
   * a <code>Speakable</code> objects, a <code>URL</code>, or
   * a <code>String</code>.
   */

  public Object getSource() {return source;}


  /**
   * Return the speakable text for an item on the speech output
   * queue of a <code>Synthesizer</code>.  The text is either a
   * Java Speech Markup Language string or a plain text string that
   * was obtained from source object.
   */

  public String getText() {return text;}


  /**
   * Return <code>true</code> if the item contains plain text
   * (not Java Speech Markup Language text).
   */

  public boolean isPlainText() {return plainText;}


  /**
   * Return the <code>SpeakableListener</code> object for this
   * speech output queue item, or <code>null</code> if none
   * was provided to the <code>speak</code> method.
   */

  public SpeakableListener getSpeakableListener() {return listener;}
}



