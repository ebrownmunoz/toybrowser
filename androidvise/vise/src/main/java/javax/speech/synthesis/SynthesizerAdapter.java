/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * Adapter that receives events associated with a <code>Synthesizer</code>.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 */


public class SynthesizerAdapter extends EngineAdapter implements SynthesizerListener
{
  /**
   * The synthesizer text output queue has emptied.
   * <P>
   *
   * @see SynthesizerEvent#QUEUE_EMPTIED
   */

  public void queueEmptied(SynthesizerEvent e) {}


  /**
   * The speaking queue has changed.
   * <P>
   *
   * @see SynthesizerEvent#QUEUE_UPDATED
   */

  public void queueUpdated(SynthesizerEvent e) {}
}

