/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * An extension to the <code>EngineListener</code> interface for receiving
 * notification of events associated with a <code>Synthesizer</code>. 
 * <code>SynthesizerListener</code> objects are attached to and removed
 * from a <code>Synthesizer</code> by calling the <code>addEngineListener</code> 
 * and <code>removeEngineListener</code> methods (which <code>Synthesizer</code>
 * inherits from the <code>Engine</code> interface).
 * <P>
 *
 * The source for all <code>SynthesizerEvents</code> provided to a
 * <code>SynthesizerListener</code> is the <code>Synthesizer</code>.
 * <P>
 *
 * The <code>SynthesizerAdapter</code> class provides a trivial implementation
 * of this interface.
 * <P>
 *
 * @see SynthesizerListener
 * @see Synthesizer
 * @see Engine
 * @see Engine#addEngineListener
 * @see Engine#removeEngineListener
 */
 
public interface SynthesizerListener extends EngineListener
{
  /**
   * An <code>QUEUE_EMPTIED</code> event has occurred indicating that
   * the text output queue of the <code>Synthesizer</code> has emptied.
   * The <code>Synthesizer</code> is in the <code>QUEUE_EMPTY</code> state.
   * <P>
   *
   * @see SynthesizerEvent#QUEUE_EMPTIED
   * @see Synthesizer#QUEUE_EMPTY
   */

  void queueEmptied(SynthesizerEvent e);


  /**
   * An <code>QUEUE_UPDATED</code> event has occurred indicating that
   * the speaking queue has changed.
   * The <code>Synthesizer</code> is in the <code>QUEUE_NOT_EMPTY</code> state.
   * <P>
   *
   * @see SynthesizerEvent#QUEUE_UPDATED
   * @see Synthesizer#QUEUE_NOT_EMPTY
   */

  void queueUpdated(SynthesizerEvent e);
}


