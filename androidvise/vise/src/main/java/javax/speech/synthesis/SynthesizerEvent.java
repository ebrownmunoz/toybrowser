/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * Event issued by <code>Synthesizer</code> to indicate a change in
 * state or other activity.  A <code>SynthesizerEvent</code> is 
 * issued to each <code>SynthesizerListener</code> attached to a
 * <code>Synthesizer</code> using the <code>addEngineListener</code>
 * method it inherits from the <code>Engine</code> interface.
 * <P>
 *
 * The <code>SynthesizerEvent</code> class extends the 
 * <code>EngineEvent</code> class.  Similarly, the 
 * <code>SynthesizerListener</code> interface extends the
 * <code>EngineListener</code> interface.
 * <P>
 *
 * <code>SynthesizerEvent</code> extends <code>EngineEvent</code>
 * with several events that are specialized for speech synthesis.
 * It also inherits several event types from <code>EngineEvent</code>:
 *  <code>ENGINE_ALLOCATED</code>,
 *  <code>ENGINE_DEALLOCATED</code>,
 *  <code>ENGINE_ALLOCATING_RESOURCES</code>,
 *  <code>ENGINE_DEALLOCATING_RESOURCES</code>,
 *  <code>ENGINE_PAUSED</code>,
 *  <code>ENGINE_RESUMED</code>.
 * <P>
 *
 * @see Synthesizer
 * @see SynthesizerListener
 * @see Engine#addEngineListener
 * @see EngineEvent
 */


public class SynthesizerEvent extends EngineEvent
{
  /**
   * The speaking queue of the <code>Synthesizer</code> has emptied
   * and the <code>Synthesizer</code> has changed to the
   * <code>QUEUE_EMPTY</code> state.
   * The queue may become empty because speech output of all 
   * items in the queue is completed, or because the items
   * have been cancelled.
   * <P>
   *
   * The <code>QUEUE_EMPTIED</code> event follows the <code>SPEAKABLE_ENDED</code>
   * or <code>SPEAKABLE_CANCELLED</code> event that removed the last item
   * from the speaking queue.
   * <P>
   *
   * @see SynthesizerListener#queueEmptied
   * @see Synthesizer#cancel()
   * @see Synthesizer#cancel(Object)
   * @see Synthesizer#cancelAll()
   * @see SpeakableEvent#SPEAKABLE_ENDED
   * @see SpeakableEvent#SPEAKABLE_CANCELLED
   * @see Synthesizer#QUEUE_EMPTY
   */

  public static final int QUEUE_EMPTIED = 700;


  /**
   * The speech output queue has changed.  This event may indicate
   * a change in state of the <code>Synthesizer</code> from 
   * <code>QUEUE_EMPTY</code> to <code>QUEUE_NOT_EMPTY</code>.
   * The event may also occur in the <code>QUEUE_NOT_EMPTY</code>
   * state without changing state.
   * the <code>enumerateQueue</code> method of <code>Synthesizer</code>
   * will return a changed list. 
   * <P>
   *
   * The speech output queue changes when (a) a new item is placed
   * on the queue with a call to one of the <code>speak</code> methods,
   * (b) when an item is removed from the queue with one of the
   * cancel methods (without emptying the queue), or (c) when output 
   * of the top item of the queue is completed (again, without leaving
   * an empty queue).
   * <P>
   *
   * The <code>topOfQueueChanged</code> boolean parameter is set
   * to true if the top item on the queue has changed.
   * <P>
   *
   * @see SynthesizerListener#queueUpdated
   * @see #getTopOfQueueChanged
   * @see SpeakableEvent#SPEAKABLE_ENDED
   * @see Synthesizer#QUEUE_NOT_EMPTY
   */

  public static final int QUEUE_UPDATED = 701;


  /**
   * <code>topOfQueueChanged</code> is <code>true</code> for
   * <code>QUEUE_UPDATED</code> event when the top item in the
   * speech output queue has changed.
   * <P>
   * 
   * @see #getTopOfQueueChanged
   * @serial
   */

  protected boolean topOfQueueChanged = false;


  /**
   * Construct a <code>SynthesizerEvent</code> with a specified event id
   * and <code>topOfQueueChanged</code> flag.
   * <P>
   *
   * @param source
   *   the <code>Synthesizer</code> that issued the event
   * @param id
   *   the identifier for the event type
   * @param topOfQueueChanged
   *   <code>true</code> if top item on speech output queue changed
   * @param oldEngineState
   *   engine state prior to this event
   * @param newEngineState
   *   engine state following this event
   */

  public SynthesizerEvent(Synthesizer source, int id, boolean topOfQueueChanged,
			  long oldEngineState, long newEngineState)
    {
      super(source, id, oldEngineState, newEngineState);

      this.topOfQueueChanged = topOfQueueChanged;
    }
  

  /**
   * Return the <code>topOfQueueChanged</code> value.  The value
   * is <code>true</code> for a <code>QUEUE_UPDATED</code> event
   * when the top item in the speech output queue has changed.
   * <P>
   * 
   * @see #topOfQueueChanged
   */

  public boolean getTopOfQueueChanged() {return topOfQueueChanged;}


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch (id) {
    case QUEUE_EMPTIED:
      return "QUEUE_EMPTIED";
    case QUEUE_UPDATED:
      if (topOfQueueChanged)
	return "QUEUE_UPDATED (topOfQueue changed)";
      else
	return "QUEUE_UPDATED";
    default: return super.paramString();
    }
  }
}

