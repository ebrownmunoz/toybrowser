/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech.synthesis;

import javax.speech.*;


/**
 * Adapter that receives events associated with spoken output of a
 * <code>Speakable</code> object.  The methods in this class are empty; 
 * this class is provided as a convenience for easily creating listeners 
 * by extending this class and overriding only the methods of interest.
 */


public class SpeakableAdapter implements SpeakableListener
{
  /**
   * A <code>TOP_OF_QUEUE</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#TOP_OF_QUEUE
   */

  public void topOfQueue(SpeakableEvent e) {}


  /**
   * A <code>SPEAKABLE_STARTED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_STARTED
   */

  public void speakableStarted(SpeakableEvent e) {}


  /**
   * A <code>SPEAKABLE_ENDED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_ENDED
   */

  public void speakableEnded(SpeakableEvent e) {}


  /**
   * A <code>SPEAKABLE_PAUSED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_PAUSED
   */

  public void speakablePaused(SpeakableEvent e) {}


  /**
   * A <code>SPEAKABLE_RESUMED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_RESUMED
   */

  public void speakableResumed(SpeakableEvent e) {}


  /**
   * A <code>SPEAKABLE_CANCELLED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_CANCELLED
   */

  public void speakableCancelled(SpeakableEvent e) {}


  /**
   * A <code>WORD_STARTED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#WORD_STARTED
   */

  public void wordStarted(SpeakableEvent e) {}


  /**
   * A <code>MARKER_REACHED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#MARKER_REACHED
   */

  public void markerReached(SpeakableEvent e) {}
}


