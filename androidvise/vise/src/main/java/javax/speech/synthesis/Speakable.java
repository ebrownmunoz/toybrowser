/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * An object implementing the <code>Speakable</code> interface can
 * be provided to the <code>speak</code> method of a
 * <code>Synthesizer</code> to be spoken.  
 * The text is accessed through the <code>getJSMLText</code> method 
 * making it the spoken equivalent of the <code>toString</code> method
 * of a Java object.  
 * <P>
 *
 * Applications can extend (nearly) any Java object to implement
 * the <code>Speakable</code> interface (strictly speaking, any non-final object).
 * Examples might include graphical objects or database entries.
 * <P>
 *
 * The <code>getJSMLText</code> method returns text formatted
 * for the Java Speech Markup Language -- defined in the 
 * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSML/index.html">
 * Java Speech Markup Language specification</A>.
 * JSML allows structural information (paragraphs and sentences), 
 * production information (pronunciations, emphasis, breaks, 
 * and prosody), and other miscellaneous markup.  Appropriate
 * use of this markup improves the quality and understandability
 * of the synthesized speech.
 * <P>
 *
 * The JSML text is a Unicode string and is assumed to contain
 * text of a single language (the language of the <code>Synthesizer</code>).
 * The text is treated as independent of other text output
 * on the synthesizer's text output queue, so, a sentence or
 * other important structure should be contained within a 
 * single speakable object.
 * <P>
 *
 * The standard XML header is optional for software-created 
 * JSML documents.  Thus, the <code>getJSMLText</code> method
 * is not required to provide the header.
 * <P>
 *
 * A <code>SpeakableListener</code> can be attached to the
 * <code>Synthesizer</code> with the <code>addSpeakableListener</code>
 * method to receive all <code>SpeakableEvents</code> for
 * all <code>Speakable</code> objects on the output queue.
 * <P>
 *
 * @see SpeakableListener
 * @see SpeakableEvent
 * @see Synthesizer
 * @see Synthesizer#speak(Speakable, SpeakableListener)
 * @see Synthesizer#addSpeakableListener
 */

public interface Speakable
{
  /** 
   * Return text to be spoken formatted for the Java Speech Markup Language.  
   * This method is called immediately when a <code>Speakable</code>
   * object is passed to the <code>speak</code> method of a 
   * <code>Synthesizer</code>.  The text placed on the speaking queue
   * can be inspected through the <code>SynthesizerQueueItem</code>
   * on the speech output queue available through the synthesizer's
   * <code>enumerateQueue</code> method.
   * <P>
   *
   * @see Synthesizer#speak(Speakable, SpeakableListener)
   * @see Synthesizer#enumerateQueue
   * @see SynthesizerQueueItem
   * @return 
   *    a string containing Java Speech Markup Language text
   */

  public String getJSMLText();
}
