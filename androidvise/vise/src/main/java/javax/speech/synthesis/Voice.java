/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * A description of one output voice of a speech synthesizer.
 * <code>Voice</code> objects can be used in selection of
 * synthesis engines (through the <code>SynthesizerModeDesc</code>).
 * The current speaking voice of a <code>Synthesizer</code>  
 * can be changed during operation with the setVoice method
 * of the <code>SynthesizerProperties</code> object.
 * <P>
 *
 * @see SynthesizerModeDesc
 * @see SynthesizerProperties#setVoice
 * @see Synthesizer
 */


public class Voice implements Cloneable
{
  /* ------------------- GENDER VALUES ------------------- */

  /**
   * Ignore gender when performing a match of voices. 
   * Synthesizers never provide a voice with <code>GENDER_DONT_CARE</code>.
   * <P>
   *
   * @see #getGender
   * @see #setGender
   */

  public static final int GENDER_DONT_CARE = 0xFFFF;
  

  /** 
   * Female voice.
   * <P>
   *
   * @see #getGender
   * @see #setGender
   */

  public static final int GENDER_FEMALE = 1;
  

  /** 
   * Male voice. 
   * <P>
   *
   * @see #getGender
   * @see #setGender
   */

  public static final int GENDER_MALE = 2;
  

  /**
   * Neutral voice that is neither male or female 
   * (for example, artificial voices, robotic voices).
   * <P>
   *
   * @see #getGender
   * @see #setGender
   */

  public static final int GENDER_NEUTRAL = 4;


  /* ------------------- AGE VALUES ------------------- */


  /**
   * Ignore age when performing a match. 
   * Synthesizers never provide a voice with <code>AGE_DONT_CARE</code>.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_DONT_CARE = 0xFFFF;


  /** 
   * Age roughly up to 12 years.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_CHILD = 0x0001 << 0;


  /** 
   * Age roughly 13 to 19 years.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_TEENAGER = 0x0001 << 1;


  /** 
   * Age roughly 20 to 40 years.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_YOUNGER_ADULT = 0x0001 << 2;


  /** 
   * Age roughly 40 to 60 years.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_MIDDLE_ADULT = 0x0001 << 3;


  /** 
   * Age roughly 60 years and up.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_OLDER_ADULT = 0x0001 << 4;


  /** 
   * Voice with age that is indeterminate.
   * For example, artificial voices, robotic voices.
   * <P>
   *
   * @see #getAge
   * @see #setAge
   */

  public static final int AGE_NEUTRAL = 0x0001 << 5;


  private String name;
  private int gender;
  private int age;
  private String style;


  /**
   * Empty constructor sets voice name and style to <code>null</code>, and 
   * age and gender to "don't care" values.
   */

  public Voice()
    {
      name = null;
      gender = GENDER_DONT_CARE;
      age = AGE_DONT_CARE;
      style = null;
    }


  /**
   * Constructor provided with voice name, gender, age and style.
   */

  public Voice(String name, int gender, int age, String style)
    {
      this.name = name;
      this.gender = gender;
      this.age = age;
      this.style = style;
    }


  /**
   * Get the voice name.  May return <code>null</code>.
   */

  public String getName() {return name;}


  /**
   * Set the voice name.
   * A null or "" string in voice match means don't care.
   */

  public void setName(String name) {this.name = name;}


  /**
   * Get the voice gender.  Gender values are OR'able.  For example,
   * to test whether a voice is male and/or neutral:
   *
   * <pre>
   *    Voice voice = ...;
   *    Voice test = new Voice();
   *    test.setGender(Voice.GENDER_MALE | Voice.GENDER_NEUTRAL);
   *    if (voice.match(test)) ...
   * </pre>
   *
   * @see #GENDER_FEMALE
   * @see #GENDER_MALE
   * @see #GENDER_NEUTRAL
   * @see #GENDER_DONT_CARE
   */

  public int getGender() {return gender;}


  /**
   * Set the voice gender.
   * <P>
   *
   * @see #GENDER_FEMALE
   * @see #GENDER_MALE
   * @see #GENDER_NEUTRAL
   * @see #GENDER_DONT_CARE
   */

  public void setGender(int gender) {this.gender = gender;}


  /**
   * Get the voice age.  Age values are OR'able.  For example,
   * to test whether a voice is child or teenager (less than 20):
   *
   * <pre>
   *    Voice voice = ...;
   *    Voice test = new Voice();
   *    test.setAge(Voice.AGE_CHILD | Voice.AGE_TEENAGER);
   *    if (voice.match(test)) ...
   * </pre>
   *
   * @see #AGE_CHILD
   * @see #AGE_TEENAGER
   * @see #AGE_YOUNGER_ADULT
   * @see #AGE_MIDDLE_ADULT
   * @see #AGE_OLDER_ADULT
   * @see #AGE_NEUTRAL
   * @see #AGE_DONT_CARE
   */

  public int getAge() {return age;}


  /**
   * Set the voice age.
   * <P>
   *
   * @see #AGE_CHILD
   * @see #AGE_TEENAGER
   * @see #AGE_YOUNGER_ADULT
   * @see #AGE_MIDDLE_ADULT
   * @see #AGE_OLDER_ADULT
   * @see #AGE_NEUTRAL
   * @see #AGE_DONT_CARE
   */

  public void setAge(int age) {this.age = age;}


  /**
   * Set the voice style.  This parameter is designed for human
   * interpretation.  Values might include "business", "casual",
   * "robotic", "breathy".
   */
  
  public String getStyle() {return style;}


  /**
   * Set the voice style.
   */

  public void setStyle(String style) {this.style = style;}


  /**
   * Create a copy of this <code>Voice</code>.
   * @return A copy of this
   */

  public Object clone()
  {
    try {
      return super.clone();
    }
    catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }


  /**
   * Determine whether a <code>Voice</code> has all the features
   * defined in the <code>require</code> object.  Strings in
   * <code>require</code> which are either <code>null</code> or
   * zero-length ("") are ignored.
   * All string comparisons are exact matches (case-sensitive).
   * <P>
   *
   * <code>GENDER_DONT_CARE</code> and <code>AGE_DONT_CARE</code> 
   * values in the <CODE>require</CODE> object are ignored.
   * The age and gender parameters are OR'ed: e.g. the required age
   * can be <code>AGE_TEENAGER | AGE_CHILD</code>.
   */

  public boolean match(Voice require)
    {
      if (require.name != null && !require.name.equals(""))
	{
	  if (this.name == null) return false;
	  if (!name.equals(require.name)) return false;
	}

      if (require.style != null && !require.style.equals(""))
	{
	  if (this.style == null) return false;
	  if (!style.equals(require.style)) return false;
	}

      if ((require.gender & this.gender) == 0)
	return false;

      if ((require.age & this.age) == 0)
	return false;

      return true;
    }


  /**
    * Returns true if and only if the parameter is not <CODE>null</CODE>
    * and is a <code>Voice</code> with equal values of name, age, gender,
    * and style.
    */

  public boolean equals(Object anObject)
    {
      if ((anObject == null) || !(anObject instanceof Voice))
	return false;

      Voice anotherVoice = (Voice)anObject;

      if (name == null)
	{
	  if (anotherVoice.name != null)
	    return false;
	}
      else if (!name.equals(anotherVoice.name))
	return false;

      if (style == null)
	{
	  if (anotherVoice.style != null)
	    return false;
	}
      else if (!style.equals(anotherVoice.style))
	return false;

      if ((gender != anotherVoice.gender) || (age != anotherVoice.age))
	return false;

      return true;
    }
}

