/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;
import java.util.*;


/**
 * <code>SynthesizerModeDesc</code> extends the <code>EngineModeDesc</code>
 * with properties that are specific to speech synthesizers.  
 * A <code>SynthesizerModeDesc</code> inherits engine name,
 * mode name, locale and running properties from <code>EngineModeDesc</code>.
 * <code>SynthesizerModeDesc</code> adds two properties:
 * <P>
 *
 * <UL>
 *  <LI>List of voices provided by the synthesizer
 *  <LI>Voice to be loaded when the synthesizer is started
 *      (not used in selection)
 * </UL>
 * <P>
 *
 * Like <code>EngineModeDesc</code>, there are two types of
 * <code>SynthesizerModeDesc</code>: those created by an application
 * which are used in engine selection, and those created by an
 * engine which descibe a particular mode of operation of the engine.
 * Descriptor provided engines are obtained through the
 * <code>availableSynthesizers</code> method of the <code>Central</code>
 * class and must have all their features
 * defined.  A descriptor created by an application may make
 * any or all of the features <code>null</code> which means "don't care"
 * (null features are ignored in engine selection).
 * <P>
 *
 * Applications can modify application-created descriptors in
 * any way.  Applications should never modify a <code>SynthesizerModeDesc</code>
 * provided by an engine (i.e. returned by the <code>availableSynthesizers</code> 
 * method.
 * <P>
 * 
 * Engine creation is described in the documentation for the
 * <code>Central</code> class.
 * <P>
 *
 * @see Central
 * @see Central#createSynthesizer
 * @see Voice
 */



public class SynthesizerModeDesc extends EngineModeDesc
{
  private Voice voices[];


  /**
   * Construct a descriptor with all features set to <code>null</code>.
   */

  public SynthesizerModeDesc()
    {
      super();
      voices = null;
    }


  /**
   * Create a <code>SynthesizerModeDesc</code> with a given <code>Locale</code> 
   * and other features set to <code>null</code>.
   */

  public SynthesizerModeDesc(Locale locale)
    {
      super(locale);
      voices = null;
    }


  /**
   * Create a fully-specified descriptor.
   * Any of the features may be <code>null</code>.
   */

  public SynthesizerModeDesc(String engineName, String modeName, Locale locale, Boolean running,
			     Voice voices[])
    {
      super(engineName, modeName, locale, running);
      this.voices = voices;
    }


  /**
   * Returns the list of voices available in this synthesizer mode.
   */

  public Voice[] getVoices() {return voices;}


  /**
   * Set the list of synthesizer voices.
   */

  public void setVoices(Voice v[]) { voices = v; }


  /**
   * Append a voice to the list of voices.
   */
    
  public void addVoice(Voice v)
    {
      int n = 0;
      if (voices != null)
	n = voices.length;

      Voice newArray[] = new Voice[n+1];

      if (n > 0)
	System.arraycopy(voices, 0, newArray, 0, n);

      newArray[n] = v;
      voices = newArray;
    }


  /**
   * Determine whether a <code>SynthesizerModeDesc</code> has all the features
   * specified by the <code>require</code> object.  Features in
   * <code>require</code> which are either <code>null</code> or
   * zero-length strings ("") are not tested (including those contained by
   * <code>Locale</code>).  All string comparisons are exact (case-sensitive).
   * <P>
   *
   * The parameters are used as follows:
   * <P>
   *
   * <UL>
   *   <LI>First, all features of the <code>EngineModeDesc</code> class
   *       are compared.  If any test fails, the method returns false.
   *   <LI> If the require parameter is a <code>SynthesizerModeDesc</code> 
   *        (or sub-class) then the required voice list is tested as follows.
   *        <BR>
   *        Each voice defined in the required set must match one of the voices
   *        in the tested object.  (See <code>Voice.match()</code> for details.)
   * </UL>
   * <P>
   *
   * Note: if is possible to compare an <code>EngineModeDesc</code> 
   * against a <code>SynthesizerModeDesc</code> and vice versa.
   * 
   * @see EngineModeDesc#match
   * @see Voice#match
   */

  public boolean match(EngineModeDesc require)
    {
       //
      // Test EngineModeDesc features.
      //

      if (super.match(require) == false)
	return false;


      //
      //  If require is only an EngineModeDesc object then
      //  don't test additional SynthesizerModeDesc features.
      //

      if (!(require instanceof SynthesizerModeDesc))
	return true;


      //
      // Now test synthesizer features.
      //

      SynthesizerModeDesc r = (SynthesizerModeDesc)require;

     if (super.match(r) == false)
	return false;

     if (r.voices != null)
       {
	 if (voices == null) return false;

	 for (int i = 0; i < r.voices.length; i++)
	   {
	     boolean isMatched = false;

	     if (r.voices[i] != null)
	       {
		 for (int j = 0; !isMatched && j < voices.length; j++)
		   {
		     if (voices[j].match(r.voices[i]))
		       isMatched = true;
		   }

		 if (!isMatched)
		   return false;
	       }
	   }
       }

      return true;
    }


  /**
    * Returns true if and only if the parameter is not <CODE>null</CODE>
    * and is a <code>SynthesizerModeDesc</code> with equal values of
    * engine name, mode name, locale, running, and all voices.
    */

  public boolean equals(Object anObject)
    {
      if ((anObject == null) || !(anObject instanceof SynthesizerModeDesc))
	return false;

      SynthesizerModeDesc anotherDesc = (SynthesizerModeDesc)anObject;

      if (!super.equals(anObject))
	return false;

      if ((voices == null) != (anotherDesc.voices == null))
	return false;

      if (voices != null)
	{
	  if (voices.length != anotherDesc.voices.length)
	    return false;

	  for (int i = 0; i < voices.length; i++)
	    if (voices[i] == null)
	      {
		if (anotherDesc.voices[i] != null)
		  return false;
	      }
	    else
	      {
		if (!voices[i].equals(anotherDesc.voices[i]))
		  return false;
	      }
	}

      return true;
    }
}

