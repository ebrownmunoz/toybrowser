/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech.synthesis;

import javax.speech.*;
import java.util.*;


/**
 * The listener interface for receiving notification of events
 * during spoken output of a <code>Speakable</code>.
 * Events are requested by either:
 * 
 * <UL>
 *   <LI>Providing a <code>SpeakableListener</code> object when calling
 *       one of the <code>speak</code> or <code>speakPlainText</code>
 *       methods of a <code>Synthesizer</code>.
 *   <LI>Attaching a <code>SpeakableListener</code> to a <code>Synthesizer</code>
 *       with its <code>addSpeakableListener</code> method.
 * </UL>
 *
 * The speakable events and the sequencing of events is defined 
 * in the documentation for <code>SpeakableEvent</code>.  The
 * source of each <code>SpeakableEvent</code> is the object from
 * which JSML text was derived: a <code>Speakable</code> object,
 * a <code>URL</code>, or a <code>String</code>.
 * <P>
 *
 * The <code>SpeakableAdapter</code> class provides a trivial implementation
 * of this interface.
 * <P>
 *
 * @see Speakable
 * @see SpeakableEvent
 * @see Synthesizer
 * @see Synthesizer#addSpeakableListener
 * @see Synthesizer#speak(javax.speech.synthesis.Speakable, javax.speech.synthesis.SpeakableListener)
 * @see Synthesizer#speak(java.net.URL, javax.speech.synthesis.SpeakableListener)
 * @see Synthesizer#speak(java.lang.String, javax.speech.synthesis.SpeakableListener)
 * @see Synthesizer#speakPlainText(java.lang.String, javax.speech.synthesis.SpeakableListener)
 */


public interface SpeakableListener extends EventListener
{
  /**
   * A <code>TOP_OF_QUEUE</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#TOP_OF_QUEUE
   */

  void topOfQueue(SpeakableEvent e);


  /**
   * A <code>SPEAKABLE_STARTED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_STARTED
   */

  void speakableStarted(SpeakableEvent e);


  /**
   * A <code>SPEAKABLE_ENDED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_ENDED
   */

  void speakableEnded(SpeakableEvent e);


  /**
   * A <code>SPEAKABLE_PAUSED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_PAUSED
   */

  void speakablePaused(SpeakableEvent e);


  /**
   * A <code>SPEAKABLE_RESUMED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_RESUMED
   */

  void speakableResumed(SpeakableEvent e);


  /**
   * A <code>SPEAKABLE_CANCELLED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#SPEAKABLE_CANCELLED
   */

  void speakableCancelled(SpeakableEvent e);


  /**
   * A <code>WORD_STARTED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#WORD_STARTED
   */

  void wordStarted(SpeakableEvent e);


  /**
   * A <code>MARKER_REACHED</code> event has occurred.
   * <P>
   *
   * @see SpeakableEvent#MARKER_REACHED
   */

  void markerReached(SpeakableEvent e);
}
