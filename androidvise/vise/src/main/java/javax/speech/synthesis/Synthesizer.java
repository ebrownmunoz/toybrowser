/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech.synthesis;

import javax.speech.*;

import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Enumeration;


/**
 * The <code>Synthesizer</code> interface provides primary access to speech 
 * synthesis capabilities.  The <code>Synthesizer</code> interface extends
 * the <code>Engine</code> interface.  Thus, any <code>Synthesizer</code>
 * implements basic speech engine capabilities plus the specialized capabilities
 * required for speech synthesis.  
 * <P>
 *
 * The primary functions provided by the <code>Synthesizer</code> interface 
 * are the ability to speak text, speak Java Speech Markup Language text,
 * and control an output queue of objects to be spoken.
 * <P>
 *
 * <B>Creating a Synthesizer</B>
 * <P>
 *
 * Typically, a Synthesizer is created by a call to the
 * <code>Central.createSynthesizer</code> method.
 * The procedures for locating, selecting, creating and
 * initializing a <code>Synthesizer</code> are described in the documentation for
 * <code><A HREF="../Central.html">Central</A></code> class.
 * <P>
 *
 * <B>Synthesis Package: Inherited and Extended Capabilities</B>
 * <P>
 *
 * A <code>synthesis</code> package inherits many of its important capabilities from 
 * the <code>Engine</code> interface and its related support classes and interfaces.
 * The <code>synthesis</code> package adds specialized functionality for 
 * performing speech synthesis.
 * <P>
 *
 * <UL>
 *  <LI>Inherits location mechanism by
 *      <code><A HREF="../Central.html#availableSynthesizers(javax.speech.EngineModeDesc)">
 *      Central.availableSynthesizers</A> </code>
 *      method and <code><A HREF="../EngineModeDesc.html">EngineModeDesc</A></code>.
 *
 *  <LI>Extends
 *      <code><A HREF="../EngineModeDesc.html">EngineModeDesc</A></code> as
 *      <code><A HREF="SynthesizerModeDesc.html">SynthesizerModeDesc</A></code>.
 *
 *  <LI>Inherits 
 *      <code><A HREF="../Engine.html#allocate()">allocate</A></code> and
 *      <code><A HREF="../Engine.html#deallocate()">deallocate</A></code> methods from the
 *      <code><A HREF="../Engine.html">Engine</A></code> interface.
 *
 *  <LI>Inherits <code><A HREF="../Engine.html#pause()">pause</A></code> and
 *      <code><A HREF="../Engine.html#resume()">resume</A></code> methods from the
 *      <code><A HREF="../Engine.html">Engine</A></code> interface.
 *
 *  <LI>Inherits 
 *      <code><A HREF="../Engine.html#getEngineState()">getEngineState</A></code>,
 *      <code><A HREF="../Engine.html#waitEngineState(long)">waitEngineState</A></code> and
 *      <code><A HREF="../Engine.html#testEngineState(long)">testEngineState</A></code> 
 *      methods from the
 *      <code><A HREF="../Engine.html">Engine</A></code> interface.
 *
 *  <LI>Inherits the
 *      <code><A HREF="../Engine.html#DEALLOCATED">DEALLOCATED</A></code>,
 *      <code><A HREF="../Engine.html#ALLOCATED">ALLOCATED</A></code>,
 *      <code><A HREF="../Engine.html#ALLOCATING_RESOURCES">ALLOCATING_RESOURCES</A></code>,
 *      <code><A HREF="../Engine.html#DEALLOCATING_RESOURCES">DEALLOCATING_RESOURCES</A></code>,
 *      <code><A HREF="../Engine.html#PAUSED">PAUSED</A></code> and
 *      <code><A HREF="../Engine.html#RESUMED">RESUMED</A></code>
 *      states from the
 *      <code><A HREF="../Engine.html">Engine</A></code> interface.
 *
 *  <LI>Adds <code><A HREF="#QUEUE_EMPTY">QUEUE_EMPTY</A></code> and
 *      <code><A HREF="#QUEUE_NOT_EMPTY">QUEUE_NOT_EMPTY</A></code> 
 *      sub-states to the 
 *      <code><A HREF="../Engine.html#ALLOCATED">ALLOCATED</A></code> state.
 *
 *  <LI>Inherits audio management:
 *      see <code><A HREF="../Engine.html#getAudioManager()">Engine.getAudioManager</A></code>
 *      and <code><A HREF="../AudioManager.html">AudioManager</A></code>.
 *
 *  <LI>Inherits vocabulary management:
 *      see <A HREF="../Engine.html#getVocabManager()">Engine.getVocabManager</A>
 *      and <A HREF="../VocabManager.html">VocabManager</A>.
 *
 *  <LI>Inherits 
 *      <code><A HREF="../Engine.html#addEngineListener(javax.speech.EngineListener)">
 *            addEngineListener</A></code> and
 *      <code><A HREF="../Engine.html#removeEngineListener(javax.speech.EngineListener)">
 *            removeEngineListener</A></code> 
 *      methods and uses the
 *      <code><A HREF="../EngineListener.html">EngineListener</A></code> interface.
 *
 *  <LI><code>Extends <A HREF="../EngineListener.html">EngineListener</A></code> interface
 *      to <code><A HREF="SynthesizerListener.html">SynthesizerListener</A></code>.
 *
 *  <LI>Adds 
 *      <code><A HREF="#speak(javax.speech.synthesis.Speakable, javax.speech.synthesis.SpeakableListener)">
 *            speak(Speakable, Listener)</A></code>,
 *      <code><A HREF="#speak(java.net.URL, javax.speech.synthesis.SpeakableListener)">
 *            speak(URL, Listener)</A></code>,
 *      <code><A HREF="#speak(java.lang.String, javax.speech.synthesis.SpeakableListener)">
 *            speak(String, Listener)</A></code> 
 *      and
 *      <code><A HREF="#speakPlainText(java.lang.String, javax.speech.synthesis.SpeakableListener)">
 *            speakPlainText(String)</A></code> 
 *      methods to place text on the output queue of the synthesizer.
 *
 *  <LI>Adds 
 *      <code><A HREF="#phoneme(java.lang.String)"> phoneme(String)</A></code>
 *      method that converts text to phonemes.
 *
 *  <LI>Adds
 *      <code><A HREF="#enumerateQueue()">enumerateQueue</A></code>,
 *      <code><A HREF="#cancel()">cancel()</A></code>,
 *      <code><A HREF="#cancel(java.lang.Object)">cancel(Object)</A></code> and
 *      <code><A HREF="#cancelAll()">cancelAll</A></code> methods 
 *      for management of output queue.
 *
 * </UL>
 * <P>
 *
 * <B>Speaking Text</B>
 * <P>
 *
 * The basic function of a <code>Synthesizer</code> is to speak text provided to
 * it by an application.  This text can be plain Unicode text in a <code>String</code>
 * or can be  marked up using the 
 * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSML/index.html">
 * Java Speech Markup Language (JSML)</A>.
 * <P>
 *
 * Plain text is spoken using the <code>speakPlainText</code> method.  
 * JSML text is spoken using one of the three <code>speak</code> methods.  The
 * <code>speak</code> methods obtain the JSML text for a <code>Speakable</code>
 * object, from a <code>URL</code>, or from a <code>String</code>.
 * <P>
 *
 * [Note: JSML text provided programmatically (by a <code>Speakable</code> object
 * or a <code>String</code>) does not require the full XML header.  JSML
 * text obtained from a URL requires the full XML header.]
 * <P>
 *
 * A synthesizer is mono-lingual (it speaks a single language) so the text
 * should contain only the single language of the synthesizer.
 * An application requiring output of more than one language needs to
 * create multiple <code>Synthesizer</code> object through <code>Central</code>.
 * The language of the <code>Synthesizer</code> should be selected at
 * the time at which it is created.  The language for a created 
 * <code>Synthesizer</code> can be checked through the <code>Locale</code>
 * of its <code>EngineModeDesc</code> (see <code>getEngineModeDesc</code>).
 * <P>
 *
 * Each object provided to a synthesizer is spoken independently.
 * Sentences, phrases and other structures should not span multiple
 * call to the <code>speak</code> methods.
 * <P>
 *
 * <B>Synthesizer State System</B>
 * <P>
 *
 * <code>Synthesizer</code> extends the state system of the generic
 * <code>Engine</code> interface.  It inherits the four basic 
 * allocation states, plus the <code>PAUSED</code> and <code>RESUMED</code>
 * states. 
 * <P>
 *
 * <code>Synthesizer</code> adds a pair of sub-states to the
 * <code>ALLOCATED</code> state to represent the state of the
 * speech output queue (queuing is described in more detail below).
 * For an <code>ALLOCATED</code> <code>Synthesizer</code>, the
 * speech output queue is either empty or not empty: represented
 * by the states <code>QUEUE_EMPTY</code> and <code>QUEUE_NOT_EMPTY</code>.
 * <P>
 *
 * The queue status is independent of the pause/resume status.
 * Pausing or resuming a synthesizer does not effect the queue.
 * Adding or removing objects from the queue does not effect the
 * pause/resume status.  The only form of interaction between 
 * these state systems is that the <code>Synthesizer</code> only
 * speaks in the <code>RESUMED</code> state, and therefore, 
 * a transition from <code>QUEUE_NOT_EMPTY</code> to <code>QUEUE_EMPTY</code>
 * because of completion of speaking an object is only possible
 * in the <code>RESUMED</code> state.  (A transition from
 * <code>QUEUE_NOT_EMPTY</code> to <code>QUEUE_EMPTY</code> is
 * possible in the <code>PAUSED</code> state only through a call
 * to one of the <code>cancel</code> methods.)
 * <P>
 *
 * <B>Speech Output Queue</B>
 * <P>
 * A synthesizer implements a queue of items provided to it 
 * through the <code>speak</code> and <code>speakPlainText</code> methods.  
 * The queue is "first-in, first-out (FIFO)" -- the objects are 
 * spoken in exactly he order in which they are received.
 * The object at the top of the queue is the object that is
 * currently being spoken or about to be spoken.
 * <P>
 *
 * The <code>QUEUE_EMPTY</code> and <code>QUEUE_NOT_EMPTY</code> states
 * of a <code>Synthesizer</code> indicate the current state of
 * of the speech output queue.  The state handling methods
 * inherited from the <code>Engine</code> interface 
 * (<code>getEngineState</code>, <code>waitEngineState</code> and
 * <code>testEngineState</code>) can be used to test the queue state.
 * <P>
 *
 * The items on the queue can be checked with the <code>enumerateQueue</code>
 * method which returns a snapshot of the queue.
 * <P>
 *
 * The <code>cancel</code> methods allows an application to (a) stop
 * the output of item currently at the top of the speaking queue, 
 * (b) remove an arbitrary item from the queue, or (c) remove
 * all items from the output queue.
 * <P>
 *
 * Applications requiring more complex queuing mechanisms (e.g. a prioritized
 * queue) can implement their own queuing objects that control the synthesizer.
 * <P>
 *
 *
 * <B>Pause and Resume</B>
 * <P>
 *
 * The pause and resume methods (inherited from the <code>javax.speech.Engine</code>
 * interface) have behavior like a "tape player".  Pause stops audio output
 * as soon as possible.  Resume restarts audio output from the point of the pause.  
 * Pause and resume may occur within words, phrases or unnatural points
 * in the speech output.
 * <P>
 *
 * Pause and resume do not affect the speech output queue.
 * <P>
 *
 * In addition to the <code>ENGINE_PAUSED</code> and <code>ENGINE_RESUMED</code>
 * events issued to the <code>EngineListener</code> (or <code>SynthesizerListener</code>),
 * <code>SPEAKABLE_PAUSED</code> and <code>SPEAKABLE_RESUMED</code> events are
 * issued to appropriate <code>SpeakableListeners</code> for the <code>Speakable</code>
 * object at the top of the speaking queue.  (The <code>SpeakableEvent</code>
 * is first issued to any <code>SpeakableListener</code> provided with the
 * <code>speak</code> method, then to each <code>SpeakableListener</code>
 * attached to the <code>Synthesizer</code>.  Finally, the <code>EngineEvent</code>
 * is issued to each <code>SynthesizerListener</code> and <code>EngineListener</code>
 * attached to the <code>Synthesizer</code>.)
 * <P>
 *
 * Applications can determine the approximate point at which a pause occurs by
 * monitoring the <code>WORD_STARTED</code> events.
 * <P>
 *
 * @see Central
 * @see Speakable
 * @see SpeakableListener
 * @see EngineListener
 * @see SynthesizerListener
 */


public interface Synthesizer extends Engine
{
  /**
   * Bit of state that is set when the speech output
   * queue of a <code>Synthesizer</code> is empty.
   * The <code>QUEUE_EMPTY</code> state is a sub-state
   * of the <code>ALLOCATED</code> state.  An allocated
   * <code>Synthesizer</code> is always in either the
   * <code>QUEUE_NOT_EMPTY</code> or <code>QUEUE_EMPTY</code> state.
   * <P>
   * 
   * A <code>Synthesizer</code> is always allocated in the 
   * <code>QUEUE_EMPTY</code> state.  The <code>Synthesizer</code>
   * transitions from the <code>QUEUE_EMPTY</code> state
   * to the <code>QUEUE_NOT_EMPTY</code> state when a
   * call to one of the <code>speak</code> methods places
   * an object on the speech output queue.  A <code>QUEUE_UPDATED</code>
   * event is issued to indicate this change in state.
   * <P>
   *
   * A <code>Synthesizer</code> returns from the 
   * <code>QUEUE_NOT_EMPTY</code> state to the <code>QUEUE_EMPTY</code>
   * state once the queue is emptied because of completion
   * of speaking all objects or because of a <code>cancel</code>.
   * <P>
   *
   * The queue status can be tested with the <code>waitQueueEmpty</code>,
   * <code>getEngineState</code> and <code>testEngineState</code> methods.
   * To block a thread until the queue is empty:
   *
   * <pre>
   *     Synthesizer synth = ...;
   *     synth.waitEngineState(QUEUE_EMPTY);
   * </pre>
   *
   * @see #QUEUE_NOT_EMPTY
   * @see Engine#ALLOCATED
   * @see Engine#getEngineState
   * @see Engine#waitEngineState
   * @see Engine#testEngineState
   * @see SynthesizerEvent#QUEUE_UPDATED
   */

  public long QUEUE_EMPTY = 1L << 16;


  /**
   * Bit of state that is set when the speech output
   * queue of a <code>Synthesizer</code> is not empty.
   * The <code>QUEUE_NOT_EMPTY</code> state is a sub-state
   * of the <code>ALLOCATED</code> state.  An allocated
   * <code>Synthesizer</code> is always in either the
   * <code>QUEUE_NOT_EMPTY</code> or <code>QUEUE_EMPTY</code> state.
   * <P>
   * 
   * A <code>Synthesizer</code> enters the <code>QUEUE_NOT_EMPTY</code>
   * from the <code>QUEUE_EMPTY</code> state when one of the
   * <code>speak</code> methods is called to place an object
   * on the speech output queue.  A <code>QUEUE_UPDATED</code> 
   * event is issued to mark this change in state.
   * <P>
   *
   * A <code>Synthesizer</code> returns from the 
   * <code>QUEUE_NOT_EMPTY</code> state to the <code>QUEUE_EMPTY</code>
   * state once the queue is emptied because of completion
   * of speaking all objects or because of a <code>cancel</code>.
   * <P>
   *
   * @see #QUEUE_EMPTY
   * @see Engine#ALLOCATED
   * @see Engine#getEngineState
   * @see Engine#waitEngineState
   * @see Engine#testEngineState
   * @see SynthesizerEvent#QUEUE_UPDATED
   */

  public long QUEUE_NOT_EMPTY = 1L << 17;


  /**
   * Speak an object that implements the <code>Speakable</code> interface
   * and provides text marked with the 
   * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSML/index.html">
   * Java Speech Markup Language</A>.
   * The <code>Speakable</code> object is added to the end of
   * the speaking queue and will be spoken once it reaches the top
   * of the queue and the synthesizer is in the <code>RESUMED</code> state.
   * <P>
   *
   * The synthesizer first requests the text of the <code>Speakable</code>
   * by calling its <code>getJSMLText</code> method.  It then checks the
   * syntax of the JSML markup and throws a <code>JSMLException</code>
   * if any problems are found.  If the JSML text is legal, the text
   * is placed on the speech output queue.
   * <P>
   *
   * When the speech output queue is updated, a <code>QUEUE_UPDATE</code>
   * event is issued to <code>SynthesizerListeners</code>.
   * <P>
   *
   * Events associated with the <code>Speakable</code> object are issued
   * to the <code>SpeakableListener</code> object.  The listener 
   * may be <code>null</code>.  A listener attached with this method
   * cannot be removed with a subsequent remove call.  The source
   * for the <code>SpeakableEvents</code> is the <code>JSMLtext</code> object.
   * <P>
   *
   * <code>SpeakableEvents</code> can also be received by attaching a
   * <code>SpeakableListener</code> to the <code>Synthesizer</code> with the
   * <code>addSpeakableListener</code> method.  A <code>SpeakableListener</code>
   * attached to the <code>Synthesizer</code> receives all <code>SpeakableEvents</code>
   * for all speech output items of the synthesizer (rather than for a
   * single <code>Speakable</code>).
   * <P>
   *
   * The speak call is asynchronous: it returns once the text for the
   * <code>Speakable</code> has been obtained, checked for syntax,
   * and placed on the synthesizer's speech output queue.  
   * An application needing to know when the <code>Speakable</code> has 
   * been spoken should wait for the <code>SPEAKABLE_ENDED</code>
   * event to be issued to the <code>SpeakableListener</code> object.  
   * The <code>getEngineState</code>, <code>waitEngineState</code> and
   * <code>enumerateQueue</code> methods can be used to determine
   * the speech output queue status.
   * <P>
   *
   * An object placed on the speech output queue can be removed
   * with one of the <code>cancel</code> methods.
   * <P>
   *
   * The <code>speak</code> methods operate as defined only when a
   * <code>Synthesizer</code> is in the <code>ALLOCATED</code> state.  
   * The call blocks if the <code>Synthesizer</code> in the 
   * <code>ALLOCATING_RESOURCES</code> state and completes when the engine
   * reaches the <code>ALLOCATED</code> state.  An error is thrown
   * for synthesizers in the <code>DEALLOCATED</code> or 
   * <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #speak(String, SpeakableListener)
   * @see #speak(URL, SpeakableListener)
   * @see #speakPlainText(String, SpeakableListener)
   * @see SpeakableEvent
   * @see #addSpeakableListener
   * @param JSMLText
   *   object implementing the Speakable interface that provides 
   *   Java Speech Markup Language text to be spoken
   * @param listener
   *   receives notification of events as synthesis output proceeds
   * @exception JSMLException
   *    if any syntax errors are encountered in <code>JSMLtext</code>
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void speak(Speakable JSMLtext, SpeakableListener listener)
    throws JSMLException, EngineStateError;


  /**
   * Speak text from a URL formatted with the 
   * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSML/index.html">
   * Java Speech Markup Language</A>.  The text is obtained from the
   * URL, checked for legal JSML formatting, and placed at the end of
   * the speaking queue.  It is spoken once it reaches the top
   * of the queue and the synthesizer is in the <code>RESUMED</code> state.
   * In other respects is it identical to the <code>speak</code>
   * method that accepts a <code>Speakable</code> object.
   * <P>
   *
   * The source of a <code>SpeakableEvent</code> issued to the 
   * <code>SpeakableListener</code> is the <code>URL</code>.
   * <P>
   *
   * Because of the need to check JSML syntax, this <code>speak</code>
   * method returns only once the complete URL is loaded, or until
   * a syntax error is detected in the URL stream.  Network delays
   * will cause the method to return slowly.
   * <P>
   *
   * Note: the full XML header is required in the JSML text provided
   * in the URL.  The header is optional on programmatically generated
   * JSML (ie. with the <code>speak(String, Listener)</code> and 
   * <code>speak(Speakable, Listener)</code> methods.
   * <P>
   *
   * The <code>speak</code> methods operate as defined only when a
   * <code>Synthesizer</code> is in the <code>ALLOCATED</code> state.  
   * The call blocks if the <code>Synthesizer</code> in the 
   * <code>ALLOCATING_RESOURCES</code> state and completes when the engine
   * reaches the <code>ALLOCATED</code> state.  An error is thrown
   * for synthesizers in the <code>DEALLOCATED</code> or 
   * <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #speak(Speakable, SpeakableListener)
   * @see #speak(String, SpeakableListener)
   * @see #speakPlainText(String, SpeakableListener)
   * @see SpeakableEvent
   * @see #addSpeakableListener
   * @param JSMLurl
   *   URL containing Java Speech Markup Language text to be spoken
   * @param JSMLException
   *   if any syntax errors are encountered in <code>JSMLtext</code>
   * @param listener
   *   receives notification of events as synthesis output proceeds
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void speak(URL JSMLurl, SpeakableListener listener)
    throws JSMLException, MalformedURLException, IOException, EngineStateError;


  /**
   * Speak a string containing text formatted with the 
   * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSML/index.html">
   * Java Speech Markup Language</A>.  The JSML text is checked for
   * formatting errors and a <code>JSMLException</code> is thrown
   * if any are found.  If legal, the text is placed at the end of
   * the speaking queue and will be spoken once it reaches the top
   * of the queue and the synthesizer is in the <code>RESUMED</code> state.
   * In all other respects is it identical to the <code>speak</code>
   * method that accepts a <code>Speakable</code> object.
   * <P>
   *
   * The source of a <code>SpeakableEvent</code> issued to the 
   * <code>SpeakableListener</code> is the <code>String</code>.
   * <P>
   *
   * The <code>speak</code> methods operate as defined only when a
   * <code>Synthesizer</code> is in the <code>ALLOCATED</code> state.  
   * The call blocks if the <code>Synthesizer</code> in the 
   * <code>ALLOCATING_RESOURCES</code> state and completes when the engine
   * reaches the <code>ALLOCATED</code> state.  An error is thrown
   * for synthesizers in the <code>DEALLOCATED</code> or 
   * <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #speak(Speakable, SpeakableListener)
   * @see #speak(URL, SpeakableListener)
   * @see #speakPlainText(String, SpeakableListener)
   * @param JSMLText
   *   String contains Java Speech Markup Language text to be spoken
   * @param listener
   *   receives notification of events as synthesis output proceeds
   * @param JSMLException
   *   if any syntax errors are encountered in <code>JSMLtext</code>
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void speak(String JSMLText, SpeakableListener listener)
    throws JSMLException, EngineStateError;


  /**
   * Speak a plain text string.  The text is not interpreted as
   * containing the Java Speech Markup Language so JSML elements
   * are ignored.  The text is placed at the end of the speaking queue
   * and will be spoken once it reaches the top of the queue and the
   * synthesizer is in the <code>RESUMED</code> state.
   * In other respects it is similar to the <code>speak</code>
   * method that accepts a <code>Speakable</code> object.
   * <P>
   *
   * The source of a <code>SpeakableEvent</code> issued to the 
   * <code>SpeakableListener</code> is the <code>String</code> object.
   * <P>
   *
   * The <code>speak</code> methods operate as defined only when a
   * <code>Synthesizer</code> is in the <code>ALLOCATED</code> state.  
   * The call blocks if the <code>Synthesizer</code> in the 
   * <code>ALLOCATING_RESOURCES</code> state and completes when the engine
   * reaches the <code>ALLOCATED</code> state.  An error is thrown
   * for synthesizers in the <code>DEALLOCATED</code> or 
   * <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #speak(Speakable, SpeakableListener)
   * @see #speak(URL, SpeakableListener)
   * @see #speak(String, SpeakableListener)
   * @param JSMLText
   *   String contains plaing text to be spoken
   * @param listener
   *   receives notification of events as synthesis output proceeds
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void speakPlainText(String text,  SpeakableListener listener)
    throws EngineStateError;


  /** 
   * Returns the phoneme string for a text string.  The return string
   * uses the International Phonetic Alphabet subset of Unicode.  
   * The input string is expected to be simple text (for example,
   * a word or phrase in English).  The text is not expected to
   * contain punctuation or JSML markup.
   * <P>
   *
   * If the <code>Synthesizer</code> does not support text-to-phoneme 
   * conversion or cannot process the input text it will return <code>null</code>.
   * <P>
   *
   * If the text has multiple pronunciations, there is no way to
   * indicate which pronunciation is preferred.
   * <P>
   *
   * The <code>phoneme</code> method operate as defined only when a
   * <code>Synthesizer</code> is in the <code>ALLOCATED</code> state.  
   * The call blocks if the <code>Synthesizer</code> in the 
   * <code>ALLOCATING_RESOURCES</code> state and completes when the engine
   * reaches the <code>ALLOCATED</code> state.  An error is thrown
   * for synthesizers in the <code>DEALLOCATED</code> or 
   * <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @param text
   *   plain text to be converted to phonemes
   * @return
   *   phonemic representation of text or <code>null</code>
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public String phoneme(String text)
    throws EngineStateError;


  /**
   * Return an <code>Enumeration</code> containing a snapshot of
   * all the objects currently on the speech output queue.  The
   * first item is the top of the queue.  An empty queue returns a
   * null object.
   * <P>
   *
   * If the return value is non-null then each object it contains
   * is guaranteed to be a <code>SynthesizerQueueItem</code> object
   * representing the source object (<code>Speakable</code> object, 
   * <code>URL</code>, or a <code>String</code>) and the JSML or
   * plain text obtained from that object.
   * <P>
   *
   * A <code>QUEUE_UPDATED</code> event is issued to each
   * <code>SynthesizerListener</code> whenever the speech output 
   * queue changes.  A <code>QUEUE_EMPTIED</code> event is issued
   * whenever the queue the emptied.
   * <P>
   *
   * This method returns only the items on the speech queue 
   * placed there by the current application or applet.  For
   * security reasons, it is not possible to inspect items placed
   * by other applications.
   * <P>
   *
   * The items on the speech queue cannot be modified by changing
   * the object returned from this method.
   * <P>
   *
   * The <code>enumerateQueue</code> method works in the
   * <code>ALLOCATED</code> state.  The call blocks if the
   * <code>Synthesizer</code> in the <code>ALLOCATING_RESOURCES</code> 
   * state and completes when the engine reaches the <code>ALLOCATED</code> 
   * state.  An error is thrown for synthesizers in the 
   * <code>DEALLOCATED</code> or <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see SynthesizerQueueItem
   * @see SynthesizerEvent#QUEUE_UPDATED
   * @see SynthesizerEvent#QUEUE_EMPTIED
   * @see Engine#addEngineListener
   * @return
   *    an <code>Enumeration</code> of the speech output queue or null
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public Enumeration enumerateQueue()
    throws EngineStateError;


  /**
   * Cancel output of the current object at the top of the output queue.  
   * A <code>SPEAKABLE_CANCELLED</code> event is issued to 
   * appropriate <code>SpeakableListeners</code>.
   * <P>
   *
   * If there is another object in the speaking queue, it is moved to top
   * of queue and receives the <code>TOP_OF_QUEUE</code> event.  If the
   * <code>Synthesizer</code> is not paused, speech output continues
   * with that object.  To prevent speech output continuing with the
   * next object in the queue, call <code>pause</code> before calling
   * <code>cancel</code>.
   * <P>
   *
   * A <code>SynthesizerEvent</code> is issued to indicate 
   * <code>QUEUE_UPDATED</code> (if objects remain on the queue) or
   * <code>QUEUE_EMPTIED</code> (if the cancel leaves the queue empty).
   * <P>
   *
   * It is not an exception to call cancel if the speech output
   * queue is empty.
   * <P>
   *
   * The <code>cancel</code> methods work in the
   * <code>ALLOCATED</code> state.  The calls blocks if the
   * <code>Synthesizer</code> in the <code>ALLOCATING_RESOURCES</code> 
   * state and complete when the engine reaches the <code>ALLOCATED</code> 
   * state.  An error is thrown for synthesizers in the 
   * <code>DEALLOCATED</code> or <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #cancel(Object)
   * @see #cancelAll()
   * @see SynthesizerEvent#QUEUE_UPDATED
   * @see SynthesizerEvent#QUEUE_EMPTIED
   * @see SpeakableEvent#TOP_OF_QUEUE
   * @see SpeakableEvent#SPEAKABLE_CANCELLED
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void cancel()
    throws EngineStateError;


  /**
   * Remove a specified item from the speech output queue.  The
   * source object must be one of the items passed to a <code>speak</code>
   * method.  A <code>SPEAKABLE_CANCELLED</code> event is issued to 
   * appropriate <code>SpeakableListeners</code>.
   * <P>
   *
   * If the source object is the top item in the queue, the behavior
   * is the same as the <code>cancel()</code> method.
   * <P>
   *
   * If the source object is not at the top of the queue, it is
   * removed from the queue without affecting the current top-of-queue
   * speech output.  A <code>QUEUE_UPDATED</code>
   * is then issued to <code>SynthesizerListeners</code>.
   * <P>
   *
   * If the source object appears multiple times in the queue, only
   * the first instance is cancelled.
   * <P>
   *
   * Warning: cancelling an object just after the synthesizer has
   * completed speaking it and has removed the object from the queue
   * will cause an exception.  In this instance, the exception can
   * be ignored.
   * <P>
   *
   * The <code>cancel</code> methods work in the
   * <code>ALLOCATED</code> state.  The calls blocks if the
   * <code>Synthesizer</code> in the <code>ALLOCATING_RESOURCES</code> 
   * state and complete when the engine reaches the <code>ALLOCATED</code> 
   * state.  An error is thrown for synthesizers in the 
   * <code>DEALLOCATED</code> or <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #cancel()
   * @see #cancelAll()
   * @see SynthesizerEvent#QUEUE_UPDATED
   * @see SynthesizerEvent#QUEUE_EMPTIED
   * @see SpeakableEvent#SPEAKABLE_CANCELLED
   * @exception IllegalArgumentException
   *    if the source object is not found in the speech output queue.
   * @param source
   *    object to be removed from the speech output queue
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void cancel(Object source)
    throws IllegalArgumentException, EngineStateError;


  /**
   * Cancel all objects in the synthesizer speech output queue and stop
   * speaking the current top-of-queue object.
   * <P>
   *
   * The <code>SpeakableListeners</code> of each cancelled item on
   * the queue receive a <code>SPEAKABLE_CANCELLED</code> event.
   * A <code>QUEUE_EMPTIED</code> event is issued to attached
   * <code>SynthesizerListeners</code>.  
   * <P>
   *
   * A <code>cancelAll</code> is implictly performed before
   * a <code>Synthesizer</code> is deallocated.
   * <P>
   *
   * The <code>cancel</code> methods work in the
   * <code>ALLOCATED</code> state.  The calls blocks if the
   * <code>Synthesizer</code> in the <code>ALLOCATING_RESOURCES</code> 
   * state and complete when the engine reaches the <code>ALLOCATED</code> 
   * state.  An error is thrown for synthesizers in the 
   * <code>DEALLOCATED</code> or <code>DEALLOCATING_RESOURCES</code> states.
   * <P>
   *
   * @see #cancel()
   * @see #cancel(Object)
   * @see SynthesizerEvent#QUEUE_EMPTIED
   * @see SpeakableEvent#SPEAKABLE_CANCELLED
   * @exception EngineStateError
   *    if called for a synthesizer in the <code>DEALLOCATED</code> or 
   *    <code>DEALLOCATING_RESOURCES</code> states
   */

  public void cancelAll()
    throws EngineStateError;


  /**
   * Return the <code>SynthesizerProperties</code> object (a JavaBean). 
   * The method returns exactly the same object as the
   * <code>getEngineProperties</code> method in the <code>Engine</code>
   * interface.  However, with the <code>getSynthesizerProperties</code>
   * method, an application does not need to cast the return value.
   * <P>
   *
   * The <code>SynthesizerProperties</code> are available in any state of
   * an <code>Engine</code>. However, changes only take effect once an
   * engine reaches the <code>ALLOCATED</code> state. 
   * <P>
   *
   * @return the <code>SynthesizerProperties</code> object for this engine
   * @see Engine#getEngineProperties
   */

  public SynthesizerProperties getSynthesizerProperties();


  /**
   * Request notifications of all <code>SpeakableEvents</code> for
   * all speech output objects for this <code>Synthesizer</code>.
   * An application can attach multiple <code>SpeakableListeners</code>
   * to a <code>Synthesizer</code>.
   * A single listener can be attached to multiple synthesizers.
   * <P>
   *
   * When an event effects more than one item in the speech output
   * queue (e.g. <code>cancelAll</code>), the <code>SpeakableEvents</code>
   * are issued in the order of the items in the queue starting with
   * the top of the queue.
   * <P>
   *
   * A <code>SpeakableListener</code> can also provided for an
   * indivudal speech output item by providing it as a parameter
   * to one of the <code>speak</code> or <code>speakPlainText</code>
   * methods.
   * <P>
   *
   * A <code>SpeakableListener</code> can be attached or removed in any
   * <code>Engine</code> state.
   * <P>
   *
   * @see #removeSpeakableListener
   * @param listener
   *   the listener that will receive <code>SpeakableEvents</code>
   */

  public void addSpeakableListener(SpeakableListener listener);


  /**
   * Remove a <code>SpeakableListener</code> from this <code>Synthesizer</code>.
   * <P>
   *
   * A <code>SpeakableListener</code> can be attached or removed in any
   * <code>Engine</code> state.
   * <P>
   *
   * @see #addSpeakableListener
   */

  public void removeSpeakableListener(SpeakableListener listener);
}




