/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.PropertyVetoException;
import javax.speech.*;

import java.beans.*;


/**
 * Provides control of the run-time properties of a <code>Synthesizer</code>.
 * The <code>SynthesizerProperties</code> object is obtained by
 * calling the <code>getEngineProperties</code> method of the
 * <code>Synthesizer</code> (inherited from the <code>Engine</code> interface).
 * <P>
 *
 * Because <code>SynthesizerProperties</code> extends the 
 * <code>EngineProperties</code> interface to provide synthesizer-specific
 * properties.  It also inherits the following properties and 
 * conventions from the <code>EngineProperties</code> interface.
 *
 * <UL>
 *   <LI>Each property has a get and set method.  (JavaBeans property method patterns)
 *   <LI>Engines may ignore calls to change properties, for example
 *       by applying maximum and minimum setttings.
 *   <LI>Calls to set methods may be asynchronous (they may return
 *       before the property change takes effect).  The <code>Engine</code>
 *       will apply a change as soon as possible.  A <code>PropertyChangeEvent</code>
 *       is issued when the change takes effect.  For example, a change
 *       in the speaking rate might take place immediately, or at the
 *       end of the current word, sentence or paragraph.
 *   <LI>The get methods return the current setting - not a pending value.
 *   <LI>A <code>PropertyChangeListener</code> may be attached to 
 *       receive property change events.
 *   <LI>Where appropriate, property settings are persistent across sessions.
 * </UL>
 *
 * The properties of a synthesizer are:

 * <UL>
 *   <LI>Speaking voice,
 *   <LI>Baseline pitch,
 *   <LI>Pitch range,
 *   <LI>Speaking rate,
 *   <LI>Volume.
 * </UL>
 *
 * Setting these properties should be considered as a <em>hint</em> 
 * to the synthesizer.  A synthesizer may choose to ignore out-of-range
 * values.  A synthesizer may have some properties that are unchangeable
 * (e.g. a single voice synthesizer).
 * Reasonable values for baseline pitch, pitch range and speaking rate 
 * may vary between synthesizers, between languages and or between voices.
 * <P>
 *
 * A change in voice may lead to change in other properties.
 * For example, female and young voices typically have higher
 * pitches than male voices.  When a change in voice leads to
 * changes in other properties, a separate <code>PropertyChangeEvent</code>
 * is issued for each property changed.
 * <P>
 *
 * Whenever possible, property changes should be persistent for a voice.  
 * For instance, after changing from voice A to voice B and back, the previous
 * property settings for voice A should return.
 * <P>
 *
 * Changes in pitch, speaking rate and so on in the Java Speech Markup Language
 * text provided to the synthesizer affect the get values but
 * do not lead to a property change event.  Applications needing an
 * event at the time these changes should include a <code>MARK</code>
 * property with the appropriate JSML element.
 * <P>
 *
 * @see Synthesizer
 * @see Engine#getEngineProperties
 */



public interface SynthesizerProperties extends EngineProperties
{

  //////////////////////////////////////////////
  //
  //  Parameters relating to volume
  //
  //////////////////////////////////////////////

  /**
   * Get the current synthesizer voice.  Modifications to the returned
   * voice do not affect the <code>Synthesizer</code> voice - a call
   * to <code>setVoice</code> is required for a change to take effect.
   * <P>
   *
   * @see #setVoice
   */

  public Voice getVoice();


  /**
   * Set the current synthesizer voice.
   * <P>
   *
   * The list of available voices for a <code>Synthesizer</code> 
   * is returned by the getVoices method of the synthesizer's
   * <code>SynthesizerModeDesc</code>.  Any one of the voices
   * returned by that method can be passed to <code>setVoice</code>
   * to set the current speaking voice.
   * <P>
   *
   * Alternatively, the voice parameter may be an application-created
   * partially specified <code>Voice</code> object.
   * If there is no matching voice, the call is ignored.
   * For example, to select a young female voice:
   *
   * <pre>
   *   Voice voice = new Voice(null
   *                           GENDER_FEMALE, 
   *                           AGE_CHILD | AGE_TEENAGER,
   *                           null);
   *   synthesizerProperties.setVoice(voice);
   * </pre>
   *
   * @see #getVoice
   * @see SynthesizerModeDesc
   * @see Engine#getEngineModeDesc
   * @exception PropertyVetoException
   *   if the synthesizer rejects or limits the new value
   */

  public void setVoice(Voice voice)
    throws PropertyVetoException;


  //////////////////////////////////////////////
  //
  //  Parameters relating to pitch
  //
  //////////////////////////////////////////////

  /**
   * Get the baseline pitch for synthesis.
   * <P>
   *
   * @see #setPitch
   */

  public float getPitch();


  /**
   * Set the baseline pitch for the current synthesis voice.  Out-of-range
   * values may be ignored or restricted to engine-defined limits.
   * Different voices have different natural sounding ranges of pitch.
   * Typical male voices are between 80 and 180 Hertz.  Female pitches
   * typically vary from 150 to 300 Hertz.
   * <P>
   *
   * @see #getPitch
   * @exception PropertyVetoException
   *   if the synthesizer rejects or limits the new value
   */

  public void setPitch(float hertz)
    throws PropertyVetoException;


  /**
   * Get the pitch range for synthesis.
   * <P>
   *
   * @see #setPitchRange
   */

  public float getPitchRange();


  /**
   * Set the pitch range for the current synthesis voice.  A narrow pitch range provides
   * monotonous output while wide range provide a more lively voice.
   * This setting is a hint to the synthesis engine.  
   * Engines may choose to ignore unreasonable requests.  
   * Some synthesizers may not support pitch variability.
   * <P>
   * The pitch range is typically between 20% and 80% of the baseline pitch.
   * <P>
   *
   * @see #getPitchRange
   * @exception PropertyVetoException
   *   if the synthesizer rejects or limits the new value
   */

  public void setPitchRange(float hertz)
    throws PropertyVetoException;



  //////////////////////////////////////////////
  //
  //  Parameters relating to speaking rate
  //
  //////////////////////////////////////////////

  /**
   * Get the current target speaking rate.  
   * <P>
   *
   * @see #setSpeakingRate
   */

  public float getSpeakingRate();


  /**
   * Set the target speaking rate for the synthesis voice
   * in words per minute.
   * <P>
   *
   * Reasonable speaking rates depend upon the synthesizer and
   * the current voice (some voices sound better at higher or lower
   * speed than others).
   * <P>
   *
   * Speaking rate is also dependent upon the language because
   * of different conventions for what is a "word".
   * A reasonable speaking rate for English is 200 words per minute.
   * <P>
   *
   * @see #getSpeakingRate
   * @exception PropertyVetoException
   *   if the synthesizer rejects or limits the new value
   */

  public void setSpeakingRate(float wpm)
    throws PropertyVetoException;



  //////////////////////////////////////////////
  //
  //  Parameters relating to volume
  //
  //////////////////////////////////////////////

  /**
   * Get the current volume.  
   * <P>
   *
   * @see #setVolume
   */

  public float getVolume();


  /**
   * Set the volume for the synthesizer's speech output as 
   * a value between 0.0 and 1.0.  A value of 0.0 indicates
   * silence.  A value of 1.0 is maximum volume and is usually
   * the synthesizer default.
   * <P>
   * 
   * A synthesizer may change the voice's style with volume.
   * For example, a quiet volume might produce whispered output
   * and loud might produce shouting.  Most synthesizer <em>do not</em>
   * make this type of change.
   * <P>
   *
   * @see #getVolume
   * @exception PropertyVetoException
   *   if the synthesizer rejects or limits the new value
   */

  public void setVolume(float volume)
    throws PropertyVetoException;
}

