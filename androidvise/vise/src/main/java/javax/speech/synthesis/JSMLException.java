/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;


/**
 * Thrown if a syntax problem is found with text in the marked up with the 
 * <A HREF="http://java.sun.com/products/java-media/speech/forDevelopers/JSML/index.html">
 * Java Speech Markup Language</A>.  
 * <P>
 *
 * The exception message is a printable string.
 */

public class JSMLException extends SpeechException
{
  /**
   * Constructs a <code>JSMLException</code> with no detail message.
   */

  public JSMLException()
  {
    super();
  }


  /**
   * Constructs a <code>JSMLException</code> with the specified detail message.
   * <P>
   *
   * @param s a printable detail message
   */

  public JSMLException(String s)
  {
    super(s);
  }
}

