/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech.synthesis;

import javax.speech.*;

/**
 * Event issued during spoken output of text. 
 * <P>
 *
 * <code>SpeakableEvents</code> are issued to <code>SpeakableListeners</code>.  
 * A single <code>SpeakableListener</code> can be provided to any of the
 * <code>speak</code> and <code>speakPlainText</code> methods of
 * a <code>Synthesizer</code> to monitor progress of a single
 * item on the speech output queue.  Any number of 
 * <code>SpeakableListener</code> objects can be attached to a
 * <code>Synthesizer</code> with the <code>addSpeakableListener</code>
 * method.  These listeners receive every <code>SpeakableEvent</code>
 * for every item on the speech output queue of the <code>Synthesizer</code>.
 * The <code>SpeakableListener</code> attached to an individual 
 * item on the speech output queue is notified before the 
 * <code>SpeakableListeners</code> attached to the <code>Synthesizer</code>.
 * <P>
 *
 * The source for a <code>SpeakableEvent</code> is the object
 * from which the JSML text was obtained: a <code>Speakable</code> object,
 * a <code>URL</code>, or a <code>String</code>.
 * <P>
 *
 * The normal sequence of events during output of the item of the
 * top of the synthesizer's speech output is:
 * <P>
 *
 * <UL>
 *  <LI><code>TOP_OF_QUEUE</code>
 *  <LI><code>SPEAKABLE_STARTED</code>
 *  <LI>Any number of <code>WORD_STARTED</code> and <code>MARKER_REACHED</code> events
 *  <LI><code>SPEAKABLE_ENDED</code>
 * </UL>
 * <P>
 *
 * A <code>SPEAKABLE_PAUSED</code> may occur any time after the 
 * <code>TOP_OF_QUEUE</code> but before the <code>SPEAKABLE_ENDED</code>
 * event.  A <code>SPEAKABLE_PAUSED</code> event can only be 
 * followed by a <code>SPEAKABLE_RESUMED</code> or <code>SPEAKABLE_CANCELLED</code>.
 * <P>
 *
 * A <code>SPEAKABLE_CANCELLED</code> event can occur at any time before
 * an <code>SPEAKABLE_ENDED</code> (including before a <code>TOP_OF_QUEUE</code>
 * event).  No other events can follow the <code>SPEAKABLE_CANCELLED</code> event
 * since the item has been removed from the speech output queue.
 * <P>
 *
 * A <code>SPEAKABLE_CANCELLED</code> event can be issued for items
 * that are not at the top of the speech output queue.  The other events
 * are only issued for the top-of-queue item.
 * <P>
 *
 * @see SpeakableListener
 * @see Synthesizer#addSpeakableListener
 * @see Speakable
 * @see Synthesizer#speak(javax.speech.synthesis.Speakable, javax.speech.synthesis.SpeakableListener)
 * @see Synthesizer#speak(java.net.URL, javax.speech.synthesis.SpeakableListener)
 * @see Synthesizer#speak(java.lang.String, javax.speech.synthesis.SpeakableListener)
 * @see Synthesizer#speakPlainText(java.lang.String, javax.speech.synthesis.SpeakableListener)
 */

public class SpeakableEvent extends SpeechEvent
{
  /**
   * Issued when an item on the synthesizer's speech output queue reaches 
   * the top of the queue.  If the <code>Synthesizer</code>
   * is not paused, the <code>TOP_OF_QUEUE</code> event will be followed
   * immediately by the <code>SPEAKABLE_STARTED</code> event.  If the
   * <code>Synthesizer</code> is paused, the <code>SPEAKABLE_STARTED</code>
   * event will be delayed until the <code>Synthesizer</code> is resumed.
   * <P>
   *
   * A <code>QUEUE_UPDATED</code> is also issued when the speech
   * output queue changes (e.g. a new item at the top of the queue).
   * The <code>SpeakableEvent</code> is issued prior to the
   * <code>SynthesizerEvent</code>.
   * <P>
   *
   * @see SpeakableListener#topOfQueue
   * @see SynthesizerEvent#QUEUE_UPDATED
   * @see javax.speech.Engine#PAUSED
   */

  public static final int TOP_OF_QUEUE = 601;


  /**
   * Issued at the start of audio output of an item on the speech output queue.
   * This event immediately follows the <code>TOP_OF_QUEUE</code>
   * unless the <code>Synthesizer</code> is paused when the speakable
   * text is promoted to the top of the output queue.
   * <P>
   *
   * @see SpeakableListener#speakableStarted
   */

  public static final int SPEAKABLE_STARTED = 602;


  /**
   * Issued with the completion of audio output of an object on
   * the speech output queue as the object is removed from the queue.
   * <P>
   *
   * A <code>QUEUE_UPDATED</code> or <code>QUEUE_EMPTIED</code> event is also
   * issued when the speech output queue changes because the speech output
   * of the item at the top of queue is completed.
   * The <code>SpeakableEvent</code> is issued prior to the
   * <code>SynthesizerEvent</code>.
   * <P>
   *
   * @see SpeakableListener#speakableEnded
   * @see SynthesizerEvent#QUEUE_EMPTIED
   * @see SynthesizerEvent#QUEUE_UPDATED
   */

  public static final int SPEAKABLE_ENDED = 603;


  /**
   * Issued when audio output of the item at the top of a
   * synthesizer's speech output queue is paused.
   * The <code>SPEAKABLE_PAUSED</code> <code>SpeakableEvent</code>
   * is issued prior to the <code>ENGINE_PAUSED</code> event
   * that is issued to the <code>SynthesizerListener</code>.
   * <P>
   *
   * @see SpeakableListener#speakablePaused
   * @see EngineEvent#ENGINE_PAUSED
   */

  public static final int SPEAKABLE_PAUSED = 604;


  /**
   * Issued when audio output of the item at the top of a
   * synthesizer's speech output queue is resumed after a previous pause.
   * The <code>SPEAKABLE_RESUMED</code> <code>SpeakableEvent</code>
   * is issued prior to the <code>ENGINE_RESUMED</code> event
   * that is issued to the <code>SynthesizerListener</code>.
   * <P>
   *
   * @see SpeakableListener#speakableResumed
   * @see EngineEvent#ENGINE_RESUMED
   */

  public static final int SPEAKABLE_RESUMED = 605;


  /**
   * Issued when an item on the synthesizer's speech output queue is
   * cancelled and removed from the queue.  A speech output queue item 
   * may be cancelled at any time following a call to <code>speak</code>.
   * An item can be cancelled even if it is not at the top of the
   * speech output queue (other <code>SpeakableEvents</code> are
   * issued only to the top-of-queue item).
   * Once cancelled, the listener for the cancelled object receives
   * no further <code>SpeakableEvents</code>.
   * <P>
   *
   * The <code>SPEAKABLE_CANCELLED</code> <code>SpeakableEvent</code>
   * is issued prior to the <code>QUEUE_UPDATED</code> or
   * <code>QUEUE_EMPTIED</code> event that is issued to the
   * <code>SynthesizerListener</code>.
   * <P>
   *
   * @see SpeakableListener#speakableCancelled
   * @see Synthesizer#cancel()
   * @see Synthesizer#cancel(Object)
   * @see Synthesizer#cancelAll()
   * @see SynthesizerEvent#QUEUE_UPDATED
   * @see SynthesizerEvent#QUEUE_EMPTIED
   */

  public static final int SPEAKABLE_CANCELLED = 606;


  /**
   * Issued when a synthesis engine starts the audio output of 
   * a word in the speech output queue item.  The <code>text</code>,
   * <code>wordStart</code> and <code>wordEnd</code> parameters
   * defines the segment of the speakable's string which is now being spoken.
   * <P>
   *
   * @see SpeakableListener#wordStarted
   * @see #getText
   * @see #getWordStart
   * @see #getWordEnd
   */

  public static final int WORD_STARTED = 607;


  /**
   * Issued when audio output reaches a marker 
   * contained in the JSML text of a speech output queue item.
   * The event text is the string of the <code>MARK</code> attribute.
   * The <code>markerType</code> indicates whether the mark is
   * at the openning or close of a JSML element or
   * is an attribute of an empty element (no close).
   * <P>
   *
   * @see SpeakableListener#markerReached
   * @see #getText
   * @see #getMarkerType
   * @see #ELEMENT_OPEN
   * @see #ELEMENT_CLOSE
   * @see #ELEMENT_EMPTY
   */

  public static final int MARKER_REACHED = 608;



  /**
   * The type of <code>MARKER_REACHED</code> event issued at the openning 
   * of a JSML container element with a <code>MARK</code> attribute.
   * An <code>ELEMENT_OPEN</code> event is followed by an <code>ELEMENT_CLOSE</code>
   * event for the closing of the element (unless the <code>Speakable</code>
   * is cancelled).
   * <P>
   *
   * Example: the event for the <code>MARK</code> attribute on the
   * openning <code>SENT</code> tag will be issued before the start
   * of the word "Testing" in:
   *
   * <pre>
   *     &lt;SENT MARK="open"&gt;Testing one, &lt;MARKER MARK="here"/&gt; two, three.&lt;/SENT&gt;
   * </pre>
   * <P>
   *
   * @see #MARKER_REACHED
   * @see #getMarkerType
   */

  public static final int ELEMENT_OPEN = 620;


  /**
   * The type of <code>MARKER_REACHED</code> event issued at the close 
   * of a JSML container element that has a <code>MARK</code> attribute on
   * the matching openning tag.   The <code>ELEMENT_CLOSE</code> event
   * always follows a matching <code>ELEMENT_OPEN</code> event for the
   * matching openning tag.
   * <P>
   *
   * Example: the event for the closing <code>SENT</code> tag for the 
   * <code>MARK</code> attribute at the openning of the <code>SENT</code>
   * element.  The event will be issued after the word "three" is spoken.
   *
   * <pre>
   *     &lt;SENT MARK="open"&gt;Testing one, &lt;MARKER MARK="here"/&gt; two, three.&lt;/SENT&gt;
   * </pre>
   * <P>
   *
   * @see #MARKER_REACHED
   * @see #getMarkerType
   */

  public static final int ELEMENT_CLOSE = 621;


  /**
   * The type of <code>MARKER_REACHED</code> event issued when an empty
   * JSML element with a <code>MARK</code> attribute is reached. 
   * (An empty JSML has no closing tag and is
   * indicated by a slash ('/') before the '&gt;' character.)
   * <P>
   *
   * Example: the <code>MARKER</code> tag below is empty event so 
   * an <code>ELEMENT_EMPTY</code> type of <code>MARKER_REACHED</code> event
   * is issued before the word "two" is spoken in:
   *
   * <pre>
   *     &lt;SENT MARK="open"&gt;Testing one, &lt;MARKER MARK="here"/&gt; two, three.&lt;/SENT&gt;
   * </pre>
   * <P>
   *
   * @see #MARKER_REACHED
   * @see #getMarkerType
   */

  public static final int ELEMENT_EMPTY = 622;


  /**
   * The text associated with the <code>SpeakableEvent</code>.
   *
   * @serial
   */

  protected String text;


  /**
   * Marker type for a <code>MARKER_REACHED</code> event.
   * <P>
   *
   * @see #getMarkerType
   * @see #ELEMENT_OPEN
   * @see #ELEMENT_CLOSE
   * @see #ELEMENT_EMPTY
   * @serial
   */

  protected int markerType;


  /**
   * Index of first character of of word in JSML text for a <code>WORD_STARTED</code> event.
   * <P>
   *
   * @see #WORD_STARTED
   * @see #getWordStart
   * @serial
   */
  protected int wordStart;



  /**
   * Index of last character of of word in JSML text for a <code>WORD_STARTED</code> event.
   * <P>
   *
   * @see #WORD_STARTED
   * @see #getWordEnd
   * @serial 
   */

  protected int wordEnd;


  /**
   * Constructs an <code>SpeakableEvent</code> with a specified source and identifier.
   */

  public SpeakableEvent(Object source, int id)
  {
    super(source, id);

    this.text = null;

    this.markerType = -1;

    this.wordStart = -1;
    this.wordEnd = -1;
  }


  /**
   * Constructs an <code>SpeakableEvent</code> with a specified source, identifier,
   * text and marker type (used for a <code>MARKER_REACHED</code> event).
   */

  public SpeakableEvent(Object source, int id, String text, int markerType)
  {
    super(source, id);

    this.text = text;

    this.markerType = markerType;

    this.wordStart = -1;
    this.wordEnd = -1;
  }


  /**
   * Constructor for a specified source, identifier,
   * text, wordStart and wordEnd (called for a <code>WORD_STARTED</code> event).
   */

  public SpeakableEvent(Object source, int id, String text, int wordStart, int wordEnd)
  {
    super(source, id);

    this.text = text;

    this.markerType = -1;

    this.wordStart = wordStart;
    this.wordEnd = wordEnd;
  }

  /**
   * Return the type of a <code>MARKER_REACHED</code> event.
   * <P>
   *
   * @see #MARKER_REACHED
   * @see #ELEMENT_OPEN
   * @see #ELEMENT_CLOSE
   * @see #ELEMENT_EMPTY
   */

  public int getMarkerType()
  {
    return markerType;
  }


  /**
   * Get the text associated with the event.
   * <P>
   *
   * For <code>WORD_STARTED</code>, the text is the next word to be spoken.
   * This text may differ from the text between the <code>wordStart</code>
   * and <code>wordEnd</code> points is the original JSML text.
   * <P>
   *
   * For <code>MARKER_REACHED</code>, the text is the <code>MARK</code> 
   * attribute in the JSML text.
   * <P>
   *
   * @see #WORD_STARTED
   * @see #MARKER_REACHED
   */

  public String getText()
  {
    return text;
  }


  /**
   * For a <code>WORD_STARTED</code> event, return the index
   * of the first character of the word in the JSML text.  
   */

  public int getWordStart()
  {
    return wordStart;
  }


  /**
   * For a <code>WORD_STARTED</code> event, return the index
   * of the last character of the word in the JSML text.  
   */

  public int getWordEnd()
  {
    return wordEnd;
  }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch (id) {
    case TOP_OF_QUEUE:        return "TOP_OF_QUEUE";
    case SPEAKABLE_STARTED:   return "SPEAKABLE_STARTED";
    case SPEAKABLE_ENDED:     return "SPEAKABLE_ENDED"; 
    case SPEAKABLE_PAUSED:    return "SPEAKABLE_PAUSED";
    case SPEAKABLE_RESUMED:   return "SPEAKABLE_RESUMED";
    case SPEAKABLE_CANCELLED: return "SPEAKABLE_CANCELLED";
    case WORD_STARTED:
      return "WORD_STARTED \"" + text + "\" from: " + wordStart + " to: " + wordEnd;
    case MARKER_REACHED:
      StringBuffer buf = new StringBuffer("MARKER_REACHED: ");
      buf.append("\"" + text + "\" at ");
      switch (markerType) {
      case ELEMENT_OPEN:  buf.append("ELEMENT_OPEN"); break;
      case ELEMENT_CLOSE: buf.append("ELEMENT_CLOSE"); break;
      case ELEMENT_EMPTY: buf.append("ELEMENT_EMPTY"); break;
      default: buf.append("unknown type"); break;
      }
      return buf.toString();
    default: return super.paramString();
    }
  }
}

