/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;


/**
 * Trivial implementation of the <code>EngineListener</code> interface
 * that receives a <code>EngineEvents</code>.
 * The methods in this class are empty;  this class is provided as a
 * convenience for easily creating listeners by extending this class
 * and overriding only the methods of interest.
 * <P>
 *
 * Extended by <code>RecognizerAdapter</code> and <code>SynthesizerAdapter</code>.
 *
 * @see javax.speech.recognition.RecognizerAdapter
 * @see javax.speech.synthesis.SynthesizerAdapter
 */


public class EngineAdapter implements EngineListener
{
  /**
   * The <code>Engine</code> has been paused.
   * <P>
   *
   * @see EngineEvent#ENGINE_PAUSED
   */

  public void enginePaused(EngineEvent e) {}


  /**
   * The <code>Engine</code> has been resumed.
   * <P>
   *
   * @see EngineEvent#ENGINE_RESUMED
   */

  public void engineResumed(EngineEvent e) {}


  /**
   * The <code>Engine</code> has been allocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_ALLOCATED
   */

  public void engineAllocated(EngineEvent e) {}


  /**
   * The <code>Engine</code> has been deallocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_DEALLOCATED
   */

  public void engineDeallocated(EngineEvent e) {}


  /**
   * The <code>Engine</code> is being allocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_ALLOCATING_RESOURCES
   */

  public void engineAllocatingResources(EngineEvent e) {}


  /**
   * The <code>Engine</code> is being deallocated.
   * <P>
   *
   * @see EngineEvent#ENGINE_DEALLOCATING_RESOURCES
   */

  public void engineDeallocatingResources(EngineEvent e) {}


  /**
   * An <code>EngineErrorEvent</code> has occurred and the
   * <code>Engine</code> is unable to continue normal operation.
   * <P>
   */

  public void engineError(EngineErrorEvent e) {}
}


