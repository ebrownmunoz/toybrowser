package javax.speech;

public class PropertyVetoException extends SpeechException
{
  /**
   * Construct a <code>PropertyVetoException</code> with no detail message.
   */

  public PropertyVetoException() 
  {
    super();
  }

  /**
   * Construct a <code>PropertyVetoException</code> with the specified detail message.
   * A detail message is a <code>String</code> that describes this particular exception.
   *
   * @param s the detail message
   */

  public PropertyVetoException(String s) 
  {
    super(s);
  }
}

