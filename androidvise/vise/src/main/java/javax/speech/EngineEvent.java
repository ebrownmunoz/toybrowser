/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.util.*;

import javax.speech.synthesis.*;
import javax.speech.recognition.*;

/**
  * <code>EngineEvent</code> notifies changes in state of a
  * speech synthesis or recognition engine.  <code>EngineEvents</code>
  * are issued to each <code>EngineListener</code> attached to an engine.
  * The <code>RecognizerEvent</code> and <code>SynthesizerEvent</code>
  * classes both extend <code>EngineEvent</code> to provide specific
  * events for recognizers and synthesizers.
  * <P>
  *
  * @see EngineListener
  * @see RecognizerEvent
  * @see SynthesizerEvent
  */

public class EngineEvent extends SpeechEvent
{
  /**
   * Identifier for event issued when engine allocation is complete.
   * The <code>ALLOCATED</code> flag of the <code>newEngineState</code> is set.
   * <P>
   *
   * @see #getNewEngineState
   * @see SpeechEvent#getId
   * @see Engine#allocate
   * @see EngineListener#engineAllocated
   */
  
  public static final int ENGINE_ALLOCATED = 501;


  /**
   * Identifier for event issued when engine deallocation is complete.
   * The <code>DEALLOCATED</code> flag of the <code>newEngineState</code> is set.
   * <P>
   *
   * @see #getNewEngineState
   * @see SpeechEvent#getId
   * @see Engine#allocate
   * @see EngineListener#engineDeallocated
   */
  
  public static final int ENGINE_DEALLOCATED = 502;


  /**
   * Identifier for event issued when engine allocation has commenced.
   * The <code>ALLOCATING_RESOURCES</code> flag of the <code>newEngineState</code> is set.
   * <P>
   *
   * @see #getNewEngineState
   * @see SpeechEvent#getId
   * @see Engine#allocate
   * @see EngineListener#engineAllocatingResources
   */
  
  public static final int ENGINE_ALLOCATING_RESOURCES = 503;


  /**
   * Identifier for event issued when engine deallocation has commenced.
   * The <code>DEALLOCATING_RESOURCES</code> flag of the <code>newEngineState</code> is set.
   * <P>
   *
   * @see #getNewEngineState
   * @see SpeechEvent#getId
   * @see Engine#allocate
   * @see EngineListener#engineDeallocatingResources
   */
  
  public static final int ENGINE_DEALLOCATING_RESOURCES = 504;


  /**
   * Identifier for event issued when engine is paused.
   * The <code>PAUSED</code> flag of the <code>newEngineState</code> is set.
   * <P>
   *
   * @see #getNewEngineState
   * @see SpeechEvent#getId
   * @see Engine#pause
   * @see EngineListener#enginePaused
   */
  
  public static final int ENGINE_PAUSED = 505;


  /**
   * Identifier for event issued when engine is resumed.
   * The <code>RESUMED</code> flag of the <code>newEngineState</code> is set.
   * <P>
   *
   * @see #getNewEngineState
   * @see SpeechEvent#getId
   * @see Engine#resume
   * @see EngineListener#engineResumed
   */
  
  public static final int ENGINE_RESUMED = 506;


  /**
   * Engine state following this event.
   * <P>
   *
   * @see #getNewEngineState
   * @serial
   */

  protected long newEngineState;


  /**
   * Engine state following prior to this event.
   * <P>
   *
   * @see #getOldEngineState
   * @serial
   */

  protected long oldEngineState;



  /**
   * Constructs an <code>EngineEvent</code> with an event identifier, old
   * engine state and new engine state.
   * <P>
   *
   * @param source
   *   the object that issued the event
   * @param id
   *   the identifier for the event type
   * @param oldEngineState
   *   engine state prior to this event
   * @param newEngineState
   *   engine state following this event
   * @see Engine#getEngineState
   */

  public EngineEvent(Engine source, int id, long oldEngineState, long newEngineState)
  {
    super(source, id);
    this.oldEngineState = oldEngineState;
    this.newEngineState = newEngineState;
  }


  /**
   * Return the state following this <code>EngineEvent</code>.
   * The value matches the <code>getEngineState</code> method.
   * <P>
   *
   * @see Engine#getEngineState
   */

  public long getNewEngineState() { return newEngineState; }


  /**
   * Return the state prior to this <code>EngineEvent</code>.
   * The value matches the <code>getEngineState</code> method.
   * <P>
   *
   * @see Engine#getEngineState
   */

  public long getOldEngineState() { return oldEngineState; }


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() {
    switch(id) {
    case ENGINE_ALLOCATED:
      return "ENGINE_ALLOCATED";
    case ENGINE_DEALLOCATED:
      return "ENGINE_DEALLOCATED";
    case ENGINE_ALLOCATING_RESOURCES:
      return "ENGINE_ALLOCATING_RESOURCES";
    case ENGINE_DEALLOCATING_RESOURCES:
      return "ENGINE_DEALLOCATING_RESOURCES";
    case ENGINE_PAUSED:
      return "ENGINE_PAUSED";
    case ENGINE_RESUMED:
      return "ENGINE_RESUMED";
    default:
      return super.paramString();
    }
  }
}


