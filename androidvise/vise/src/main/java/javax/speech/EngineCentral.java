/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

/**
 * Provides a list of <code>EngineModeDesc</code> objects that define the 
 * available operating modes of a speech engine.
 * <P>
 *
 * Each speech engine registers an <code>EngineCentral</code> object with the
 * <code>javax.speech.Central</code> class.  When requested by the <code>Central</code>
 * class, each registered <code>EngineCentral</code> object provides a list
 * with an <code>EngineModeDesc</code> object that describes each available
 * operating mode of the engine.
 * <P>
 *
 * The <code>EngineModeDesc</code> objects returned by <code>EngineCentral</code>
 * in its list must implement the <code>EngineCreate</code> interface and
 * be a sub-class of either <code>RecognizerModeDesc</code> or
 * <code>SynthesizerModeDesc</code>.  The <code>Central</code>
 * class calls the <code>createEngine</code> method of the <code>EngineCentral</code>
 * interface when it is requested to create an engine.  (See <code>EngineCreate</code>
 * for more detail.)
 * <P>
 *
 * The engine must perform the same security checks on access to speech engines
 * as the <code>Central</code> class. 
 * <P>
 *
 * <B>Note:</B> Application developers do not need to use this interface.
 * <code>EngineCentral</code> is used internally by
 * <code>Central</code> and speech engines.
 * <P>
 * 
 * @see Central
 * @see EngineCreate
 * @see EngineModeDesc
 */

public interface EngineCentral
{
  /** 
   * Create an <code>EngineList</code> containing an <code>EngineModeDesc</code>
   * for each mode of operation of a speech engine that matches a set
   * of required features.  Each object in the list must be a sub-class
   * of either <code>RecognizerModeDesc</code> or <code>SynthesizerModeDesc</code>
   * and must implement the <code>EngineCreate</code> interface.
   * <P>
   *
   * The <code>Central</code> class ensures that the <code>require</code>
   * parameter is an instance of either <code>RecognizerModeDesc</code>
   * or <code>SynthesizerModeDesc</code>.  This enables the <code>EngineCentral</code>
   * to optimize its search for either recognizers or synthesizers.
   * <P>
   *
   * Returns <code>null</code> if no engines are available or if none
   * meet the specified requirements.
   * <P>
   *
   * The returned list should indicate the list of modes available at the time
   * of the call (the list may change over time).  The engine can create the
   * list at the time of the call or it may be pre-stored.
   * <P>
   *
   * @see EngineCreate
   * @see EngineModeDesc
   * @see javax.speech.recognition.RecognizerModeDesc
   * @see javax.speech.synthesis.SynthesizerModeDesc
   * @exception SecurityException 
   *   if the caller does not have <code>accessEngineModeDesc</code> permission
   */

  public EngineList createEngineList(EngineModeDesc require)
    throws SecurityException;
}

