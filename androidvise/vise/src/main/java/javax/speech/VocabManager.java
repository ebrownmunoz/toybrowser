/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.util.*;


/**
 * Interface for management of words used by a speech <code>Engine</code>.
 * The <code>VocabManager</code> for an <code>Engine</code> is
 * returned by the <code>getVocabManager</code> method of
 * the <code>Engine</code> interface.  Engines are not required
 * support the <code>VocabManager</code> - the <code>getVocabManager</code>
 * manager may return <code>null</code>.
 * <P>
 *
 * Words, technically known as tokens, are provided to the vocabulary
 * manager with optional information about their pronunciation,
 * grammatical role and spoken form.  
 * <P>
 *
 * The <code>VocabManager</code> is typically used to provide a 
 * speech engine with information on problematic words - usually 
 * words for which the engine is unable to guess a pronunciation.
 * For debugging purposes, an <code>Engine</code> may provide a
 * list of words it finds difficult through the 
 * <code>listProblemWords</code> method.
 * <P>
 *
 * Words in the vocabulary manager can be used as tokens in
 * rule grammars for recognizers.
 * <P>
 *
 * @see Engine#getVocabManager
 * @see Word
 */

public interface VocabManager
{
  /** 
   * Add a word to the vocabulary. 
   */

  public void addWord(Word w);


  /** 
   * Add an array of words to the vocabulary. 
   */

  public void addWords(Word[] w);


  /** 
   * Remove a word from the vocabulary. 
   *
   * @exception IllegalArgumentException 
   *   Word is not known to the VocabManager.
   */

  public void removeWord(Word w)
    throws IllegalArgumentException;


  /** 
   * Remove an array of words from the vocabulary. 
   * To remove a set of words it is often useful to
   * <pre>
   *    removeWords(getWords("matching"));
   * </pre>
   *
   * @exception IllegalArgumentException 
   *   Word is not known to the VocabManager.
   */

  public void removeWords(Word w[])
    throws IllegalArgumentException;


  /**
   * Get all words from the vocabulary manager matching text. 
   * Returns <code>null</code> if there are no matches.
   * If text is <code>null</code> all words are returned.
   * This method only returns words that have been added
   * by the <code>addWord</code> methods - it does not provide
   * access to the engine's internal word lists.
   *
   * @param text
   *   word requested from <code>VocabManager</code>
   * @return list of <code>Words</code> matching <code>text</code>
   */

  public Word[] getWords(String text);


  /**
   * Returns a list of problematic words encountered during
   * a session of using a speech recognizer or synthesizer.
   * This return information is intended for development use
   * (so the application can be enhanced to provide vocabulary 
   * information).  An engine may return null.
   * <P>
   *
   * If a pronunciation for a problem word is provided through the
   * <code>addWord</code> methods, the engine may remove
   * the word from the problem list.
   * <P>
   *
   * An engine may (optionally) include its best-guess pronunciations
   * for problem words in the return array allowing the developer
   * to fix (rather than create) the pronunciation.
   */

  public Word[] listProblemWords();
}

