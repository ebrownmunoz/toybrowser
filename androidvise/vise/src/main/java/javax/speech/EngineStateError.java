/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

/**
 * Signals an error caused by an illegal call to a method
 * of a speech engine.  For example, it is illegal to request
 * a deallocated <code>Synthesizer</code> to <code>speak</code>,
 * or to request a deallocated <code>Recognizer</code> to 
 * create or new grammar, or to request any deallocated
 * engine to pause or resume.
 */

public class EngineStateError extends SpeechError
{
  /**
   * Construct an <code>EngineStateError</code> with no detail message.
   */

  public EngineStateError() 
  {
    super();
  }

  /**
   * Construct an <code>EngineStateError</code> with a detail message.
   * A detail message is a <code>String</code> that describes this particular exception.
   */

  public EngineStateError(String s) 
  {
    super(s);
  }
}

