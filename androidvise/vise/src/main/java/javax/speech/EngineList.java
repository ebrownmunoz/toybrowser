/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech;

import java.util.*;


/**
 * <code>EngineList</code> is a container for a set of <code>EngineModeDesc</code> objects.
 * An <code>EngineList</code> is used in the selection of speech
 * engines in conjuction with the methods of the <code>Central</code> class.
 * It provides convenience methods for the purpose of testing and manipulating the
 * <code>EngineModeDesc</code> objects it contains.
 * <P>
 *
 * An <code>EngineList</code> object is typically obtained through the
 * <code>availableSynthesizers</code> or <code>availableRecognizers</code> 
 * methods of the <code>javax.speech.Central</code> class.
 * The <code>orderByMatch</code>, <code>anyMatch</code>, <code>requireMatch</code> and 
 * <code>rejectMatch</code> methods are used to prune the
 * list to find the best match given multiple criteria.
 * <P>
 *
 * @see EngineModeDesc
 * @see Central
 * @see Central#availableRecognizers
 * @see Central#availableSynthesizers
 */


public class EngineList extends java.util.Vector
{
  /**
   * Return true if one or more <code>EngineModeDesc</code> 
   * in the <code>EngineList</code> match
   * the required properties.  The <code>require</code> object
   * is tested with the <code>match</code> method of each
   * <code>EngineModeDesc</code> in the list.  If any match call returns
   * true then this method returns <code>true</code>.
   * <P>
   *
   * <code>anyMatch</code> is often used to test whether 
   * pruning a list (with <code>requireMatch</code> or
   * <code>rejectMatch</code>) would leave the list empty.
   * <P>
   *
   * @see EngineModeDesc#match
   */

  public synchronized boolean anyMatch(EngineModeDesc require)
  {
    for (int i = 0; i < this.size(); i++)
      {
	EngineModeDesc d = (EngineModeDesc)elementAt(i);

	if (d.match(require))
	  return true;
      }

    return false;
  }


  /**
   * Remove <code>EngineModeDesc</code> entries from the list
   * that do not match <code>require</code>.  The <code>match</code>
   * method for each <code>EngineModeDesc</code> in the list is
   * called: if it returns <code>false</code> it is removed from
   * the list.  Example:
   *
   * <pre>
   *   // Remove engine mode descriptors from a list if they
   *   // don't support US English.
   *   EngineList list = ....;
   *   EngineModeDesc desc = new EngineModeDesc(Locale.US);
   *   list.requireMatch(desc);
   * </pre>
   *
   * @see EngineModeDesc#match
   */

  public synchronized void requireMatch(EngineModeDesc require)
  {
    //  Must go backwards because size changes whenever we remove elements

    for (int i = this.size()-1; i >= 0; i--)
      {
	EngineModeDesc d = (EngineModeDesc)elementAt(i);

	if (!d.match(require))
	  removeElementAt(i);
      }
  }


  /**
   * Remove <code>EngineModeDesc</code> entries from the list
   * that do match <code>require</code>.  The <code>match</code>
   * method for each <code>EngineModeDesc</code> in the list is
   * called: if it returns <code>true</code> it is removed from
   * the list.  Example:
   *
   * <pre>
   *   // Remove engine mode descriptors if they support US English.
   *   EngineList list = ....;
   *   EngineModeDesc desc = new EngineModeDesc(Locale.US);
   *   list.rejectMatch(desc);
   * </pre>
   *
   * @see EngineModeDesc#match
   */

  public synchronized void rejectMatch(EngineModeDesc reject)
  {
    //  Must go backwards because size changes whenever we remove elements

    for (int i = this.size()-1; i >= 0; i--)
      {
	EngineModeDesc d = (EngineModeDesc)elementAt(i);

	if (d.match(reject))
	  removeElementAt(i);
      }
  }

  
  /**
   * Order the list so that elements matching the required
   * features are at the head of the list, and others are
   * at the end.  Within categories, the original order of
   * the list is preserved.  Example:
   * 
   * <pre>
   *   // Put running engines at the head of the list.
   *   EngineList list = ....;
   *   EngineModeDesc desc = new EngineModeDesc();
   *   desc.setRunning(true);
   *   list.orderByMatch(desc);
   * </pre>
   */

  public synchronized void orderByMatch(EngineModeDesc require)
  {
    EngineList yes = new EngineList();
    EngineList no = new EngineList();

    for (int i = 0; i < this.size(); i++)
      {
	EngineModeDesc d = (EngineModeDesc)elementAt(i);

	if (d.match(require))
	  yes.addElement(d);
	else
	  no.addElement(d);
      }

    removeAllElements();

    for (int i = 0; i < yes.size(); i++)
      this.addElement(yes.elementAt(i));

    for (int i = 0; i < no.size(); i++)
      this.addElement(no.elementAt(i));
  }
}

