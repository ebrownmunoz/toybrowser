/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.util.*;


/**
 * The listener interface for receiving events associated
 * with the audio input or output of an <code>Engine</code>.
 * An <code>AudioListener</code> is attached to an <code>Engine</code>
 * by the <code>addAudioListener</code> method of the engine's
 * <code>AudioManager</code>.
 * <P>
 *
 * <code>RecognizerAudioListener</code> extends this interface to support 
 * <code>RecognizerAudioEvents</code> provided by a <code>Recognizer</code>.
 * <P>
 *
 * <STRONG>Note:</STRONG> until the Java Sound API is finalized,
 * the <code>AudioManager</code> and other audio classes and 
 * interfaces will remain as placeholders for future expansion.
 * Only the <code>Recognizer</code> audio events are functional in this release.
 * <P>
 *
 * @see AudioManager#addAudioListener
 * @see javax.speech.recognition.RecognizerAudioEvent
 * @see javax.speech.recognition.RecognizerAudioListener
 */


public interface AudioListener extends EventListener
{
}


