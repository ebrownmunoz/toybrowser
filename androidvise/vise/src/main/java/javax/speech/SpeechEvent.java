/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech;

import java.util.EventObject;


/**
 * The root event class for all speech events.
 * All events from a speech engine (recognizer or synthesizer)
 * are synchronized with the AWT event queue.  This allows an
 * application to mix speech and AWT events with being concerned
 * with multi-threading problems.
 * <P>
 *
 * <b>Note to Engine Developers</b>
 * <P>
 * 
 * The AWT event queue is obtained through the AWT Toolkit:
 *
 * <pre>
 *    import java.awt.*;
 *    ...
 *    EventQueue q = Toolkit.getDefaultToolkit().getSystemEventQueue();
 * </pre>
 *
 * An engine should create a sub-class of <code>AWTEvent</code>
 * that can be placed on the AWT event queue.  The engine also
 * needs to create a non-visual AWT <code>Component</code> to
 * receive the engine's <code>AWTEvent</code>.  When the
 * AWT event is notified to the engine's component, the engine
 * should issue the approporiate speech event.  The speech event
 * can be issued either from the AWT event thread or from a 
 * separate thread created by the speech engine.  
 * (Note that <code>SpeechEvent</code> is not a sub-class
 * of <code>AWTEvent</code> so speech events can not be placed
 * directly onto the AWT event queue.)
 */

public class SpeechEvent extends EventObject
{
  /**
   * Event identifier.  Id values are defined for each sub-class 
   * of <code>SpeechEvent</code>.
   * <P>
   *
   * @see #getId
   * @serial
   */

  protected int id;


  /**
   * Constructs a <code>SpeechEvent</code> with a specified source.
   * The source must be non-null.
   */

  protected SpeechEvent(Object source)
  {
    super(source);
  }


  /**
   * Constructs a <code>SpeechEvent</code>.
   *
   * @param source
   *   the object that issued the event
   * @param id
   *   the identifier for the event type
   */

  protected SpeechEvent(Object source, int id)
  {
    super(source);
    this.id = id;
  }


  /**
   * Return the event identifier.  Id values are defined for each sub-class 
   * of <code>SpeechEvent</code>.
   */

  public int getId() {return id;}


  /**
   * Returns a parameter string identifying this  event.
   * This method is useful for event-logging and for debugging.
   * <P>
   *
   * @return a string identifying the event
   */

  public String paramString() { return "unknown type"; }


  /**
   * Return a printable String.  Useful for event-logging and debugging.
   */

  public String toString() {
    return getClass().getName() + "[" + paramString() + "] on " + source;
  }
}

