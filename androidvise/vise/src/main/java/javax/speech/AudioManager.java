/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech;


/**
 * The <code>AudioManager</code> is provided by a speech <code>Engine</code> 
 * - a <code>Recognizer</code> or <code>Synthesizer</code> - to
 * allow an application to control audio input/output
 * and to monitor audio-related events.
 * The <code>AudioManager</code> for an engine is obtained by
 * calling its <code>getAudioManager</code> method.
 * <P>
 *
 * <STRONG>Note:</STRONG> until the Java Sound API is finalized,
 * the <code>AudioManager</code> and other audio classes and 
 * interfaces will remain as placeholders for future expansion.
 * Only the <code>Recognizer</code> audio events are functional in this release.
 * <P>
 *
 * @see Engine#getAudioManager
 */

public interface AudioManager
{
  /**
   * Request notifications of audio events to an <code>AudioListener</code>.
   * An application can attach multiple audio listeners to an
   * <code>AudioManager</code>.
   * If the engine is a <code>Recognizer</code>, a <code>RecognizerAudioListener</code>
   * may be attached since the <code>RecognizerAudioListener</code>
   * interface extends the <code>AudioListener</code> interface.
   * <P>
   *
   * @see AudioListener
   * @see javax.speech.recognition.RecognizerAudioListener
   */

  public void addAudioListener(AudioListener listener);


  /**
   * Detach an audio listener from this <code>AudioManager</code>.
   */

  public void removeAudioListener(AudioListener listener);
}

