/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */


package javax.speech;

import java.lang.Error;


/**
 * Signals that an error has occurred in the javax.speech package.
 * <code>SpeechError</code> is the super class of all errors in
 * the Java Speech API.
 * <P>
 *
 * @see SpeechException
 */

public class SpeechError extends Error
{
  /**
   * Constructs a <code>SpeechException</code> with a detail message.
   *
   * @param s the detail message
   */

  public SpeechError(String s) {
    super(s);
  }


  /**
   * Empty constructor for <code>SpeechException</code> with no detail message.
   *
   * @param s the detail message
   */

  public SpeechError() {
    super();
  }
}

