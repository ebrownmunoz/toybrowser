/*
 * Copyright (c) 1996-8 Sun Microsystems, Inc. All Rights Reserved.
 */

package javax.speech;

import java.util.*;


/**
 * <code>EngineModeDesc</code> provides information about a 
 * <EM>specific operating mode</EM> of a speech engine.  
 * The <code>availableRecognizers</code> and <code>availableSynthesizers</code>
 * methods of the <code>Central</code> class provide a list of mode 
 * descriptors for all operating modes of registered engines.
 * Applications may also create <code>EngineModeDescs</code>
 * for use in selecting and creating engines.  Examples of uses
 * mode descriptors are provided in the documentation for the
 * <A HREF="Central.html"><code>Central</code></A> class.
 * <P>
 *
 * The properties defined in the <code>EngineModeDesc</code> class
 * apply to all speech engines including speech recognizers
 * and speech synthesizers.  The <code>RecognizerModeDesc</code> 
 * and <code>SynthesizerModeDesc</code> classes extend the 
 * <code>EngineModeDesc</code> class to define specialized 
 * properties for recognizers and synthesizers.
 * <P>
 *
 * The <code>EngineModeDesc</code> and its sub-classes follow the 
 * Java Beans set/get property patterns.  The list of properties
 * is outlined below.
 * <P>
 *
 * The properties of <code>EngineModeDesc</code> and its sub-classes
 * are all object references.  All properties are defined so that a
 * <code>null</code> value means "don't care" when selecting an engine
 * or matching <code>EngineModeDesc</code> and its sub-classes.
 * For example, a <code>Boolean</code> value for a property means
 * that its three values are true, false and don't care (<code>null</code>).
 * <P>
 *
 * The basic properties of an engine defined by <code>EngineModeDesc</code>
 * are:
 *
 * <DL>
 *   <DT><B>engine name</B>
 *   <DD>A string that uniquely identifies a speech engine.
 *       e.g. "Acme Recognizer"
 *
 *   <DT><B>mode name</B>
 *   <DD>A string that uniquely identifies a mode of operation of the speech engine.
 *       e.g. "Spanish Dictator"
 *
 *   <DT><B>Locale</B>
 *   <DD>A <CODE>java.util.Locale</CODE> object representing the
 *       language supported by the engine mode.  The country code
 *       may be optionally defined for an engine.  The Locale
 *       variant is typically ignored.
 *       e.g. Locale("fr", "CA") represent French spoken in Canada.
 *       ("fr" and "CA" are standard ISO codes).
 *
 *   <DT><B>Running</B>
 *   <DD>A <code>Boolean</code> value indicating whether a speech engine
 *       is already running.  This allows for the selection of engines that
 *       already running so that system resources are conserved.
 * </DL>
 *
 * <B>Selection</B>
 * <P>
 *
 * There are two types of <code>EngineModeDesc</code> object (and its sub-classes):
 * those created by a speech engine and those created by an application.
 * Engine-created descriptors are obtained through the 
 * <code>availableRecognizers</code> and <code>availableSynthesizers</code>
 * methods of the <code>Central</code> class and must have all features
 * set to non-null values.
 * <P>
 *
 * Applications can create descriptors using the constructors of
 * the descriptor classes.  Applications may leave any or all of the
 * feature values <code>null</code> to indicate "don't care".
 * <P>
 *
 * Typically, application-created descriptors are used to test the
 * engine-created descriptors to select an appropriate engine for creation.
 * For example, the following code tests whether an engine mode supports 
 * Swiss German:
 *
 * <PRE>
 *    EngineModeDesc fromEngine = ...;
 *    // "de" is the ISO 639 language code for German
 *    // "CH" is the ISO 3166 country code for Switzerland
 *    // (see locale for details)
 *    EngineModeDesc require = new EngineModeDesc(new Locale("de", "CH"));
 *    // test whether the engine mode supports Swiss German.
 *    if (fromEngine.match(require)) ...   
 * </PRE>
 * <P>
 *
 * An application can create a descriptor and pass it to the
 * <code>createRecognizer</code> or <code>createSynthesizer</code>
 * methods of <code>Central</code>.  In this common approach, 
 * the <code>Central</code> performs the engine selection.  

 * <PRE>
 *    // Create a mode descriptor that requires French
 *    EngineModeDesc desc = new EngineModeDesc(Locale.FRENCH);
 *    // Create a synthesizer that supports French
 *    Synthesizer synth = Central.createSynthesizer(desc);
 * </PRE>
 * <P>
 *
 * Applications that need advanced selection criterion will
 * <OL>
 *  <LI>Request a list of engine mode descriptors from
 *      <code>availableRecognizers</code> or <code>availableSynthesizers</code>, 
 *  <LI>Select one of the descriptors using the methods of 
 *      <code>EngineList</code> and <code>EngineModeDesc</code> and its sub-classes,
 *  <LI>Pass the selected descriptor to the <code>createRecognizer</code> or
 *      <code>createSynthesizer</code> method of <code>Central</code>.
 * </OL>
 * <P>
 *
 * @see javax.speech.recognition.RecognizerModeDesc
 * @see javax.speech.synthesis.SynthesizerModeDesc
 * @see Central
 */


public class EngineModeDesc
{
  private String engineName;
  private String modeName;
  private Locale locale;
  private Boolean running;


  /**
   * Empty constructor sets engine name, mode name, Locale and
   * running all to <code>null</code>.
   * <P>
   */

  public EngineModeDesc()
    {
      engineName = null;
      modeName = null;
      locale = null;
      running = null;
    }


  /**
   * Construct an <code>EngineModeDesc</code> for a locale.
   * The engine name, mode name and running are set to <code>null</code>.
   */

  public EngineModeDesc(Locale locale)
    {
      this.engineName = null;
      this.modeName = null;
      this.locale = locale;
      this.running = null;
    }


  /**
   * Constructor provided with engine name, mode name, locale and running.
   * Any parameter may be <code>null</code>.
   */

  public EngineModeDesc(String engineName, String modeName, Locale locale, Boolean running)
    {
      this.engineName = engineName;
      this.modeName = modeName;
      this.locale = locale;
      this.running = running;
    }



  /**
   * Get the engine name. The engine name should be a unique string
   * across the provider company and across companies.
   */

  public String getEngineName() {return engineName;}


  /**
   * Set the engine name.
   * May be null.
   */

  public void setEngineName(String engineName) {this.engineName = engineName;}


  /**
   * Get the mode name. The mode name that should uniquely identify a
   * single mode of operation of a speech engine (per-engine unique).
   */

  public String getModeName() {return modeName;}


  /**
   * Set the mode name.
   * May be null.
   */

  public void setModeName(String modeName) {this.modeName = modeName;}


  /**
   * Get the Locale.  The locale for an engine mode must have
   * the language defined but the country may be undefined.
   * (e.g. <code>Locale.ENGLISH</code> indicates the English
   * language spoken in any country).  The locale variant is
   * typically ignored.
   */

  public Locale getLocale() {return locale;}


  /**
   * Set the Locale.
   * May be null.
   */

  public void setLocale(Locale locale) {this.locale = locale;}


  /**
   * Get the running feature.
   * Values may be <code>TRUE</code>, <code>FALSE</code> or
   * <code>null</code> (null means "don't care").
   */

  public Boolean getRunning() {return running;}


  /**
   * Set the running feature.
   * Values may be <code>TRUE</code>, <code>FALSE</code> or
   * <code>null</code> (null means "don't care").
   */

  public void setRunning(Boolean running) {this.running = running;}


  /**
   * Determine whether an <code>EngineModeDesc</code> has all the features
   * defined in the <code>require</code> object.  Strings in
   * <code>require</code> which are either <code>null</code> or
   * zero-length ("") are not tested, including those in the
   * Locale.  All string comparisons are exact (case-sensitive).
   */

  public boolean match(EngineModeDesc require)
    {
      //
      //  Test match for Locale
      //

      if (require.locale != null)
	{
	  if (this.locale == null) return false;

	  if (require.locale.getLanguage() != null && !require.locale.getLanguage().equals(""))
	    if (!require.locale.getLanguage().equals(locale.getLanguage()))
	      return false;

	  if (require.locale.getCountry() != null && !require.locale.getCountry().equals(""))
	    if (!require.locale.getCountry().equals(locale.getCountry()))
	      return false;
	}

      if (require.modeName != null && !require.modeName.equals(""))
	{
	  if (this.modeName == null) return false;
	  if (!modeName.equals(require.modeName)) return false;
	}

      if (require.engineName != null && !require.engineName.equals(""))
	{
	  if (this.engineName == null) return false;
	  if (!engineName.equals(require.engineName)) return false;
	}

      if (require.running != null)
	if (!require.running.equals(running))
	  return false;

      return true;
    }


  /**
    * True if and only if the parameter is not <CODE>null</CODE>
    * and is a <code>EngineModeDesc</code> with equal values of 
    * Locale, engineName and modeName.
    */

  public boolean equals(Object anObject)
    {
      if ((anObject == null) || !(anObject instanceof EngineModeDesc))
	return false;

      EngineModeDesc anotherDesc = (EngineModeDesc)anObject;

      if (engineName == null)
	{
	  if (anotherDesc.engineName != null)
	    return false;
	}
      else if (!engineName.equals(anotherDesc.engineName))
	return false;

      if (modeName == null)
	{
	  if (anotherDesc.modeName != null)
	    return false;
	}
      else if (!modeName.equals(anotherDesc.modeName))
	return false;

      if (locale == null)
	{
	  if (anotherDesc.locale != null)
	    return false;
	}
      else if (!locale.equals(anotherDesc.locale))
	return false;

      if ((running == null) != (anotherDesc.running == null))
	return false;
      else if (running != null)
	if (!running.equals(anotherDesc.running))
	  return false;

      return true;
    }
}

