package com.voxware.browser.android.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.voxware.browser.android.BuildConfig;
import com.voxware.browser.android.R;
import com.voxware.browser.android.deployment.DeploymentManager;
import com.voxware.browser.android.deployment.DeploymentStatus;
import com.voxware.browser.android.service.BrowserServiceClient;
import com.voxware.browser.android.utils.VoxBrowserSettings;
import com.voxware.browser.security.key.Verifier;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

/**
 * A placeholder fragment containing a simple view.
 */
public class VoxbrowserStartActivityFragment extends Fragment {
    static final int UPDATE_REQUEST = 1;
    private static final Logger log = LoggerFactory.getLogger(VoxbrowserStartActivityFragment.class);
    private VoxBrowserSettings settings;
    private BrowserServiceClient client;

    private Button startButton;
    public VoxbrowserStartActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_voxbrowser_start, container, false);
        settings = new VoxBrowserSettings(getActivity());
        startButton = (Button)view.findViewById(R.id.startButton);
        startButton.setOnClickListener(this::onClickStart);
        // if we get created, but the browser is already running, then go straight to the running activity
        client = new BrowserServiceClient(getActivity(), () -> {
            try {
                if (client.isBrowserStarted()) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(() -> {
                        transitionToRunning();
                    });
                }
            } catch (Exception e) {
                log.warn("Caught error trying to check browser service status", e);
            } finally {
                client.close();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateAppInfo();
        File startImmediatelyFile = new File(getActivity().getFilesDir(), "startImmediately");
        boolean startImmediately = startImmediatelyFile.exists();
        if (startImmediately) {
            startImmediatelyFile.delete();
            onClickStart(startButton);
        }
    }

    private void updateAppInfo() {
        VoxBrowserSettings settings = new VoxBrowserSettings(getActivity());
        TextView appText = (TextView)getView().findViewById(R.id.appText);
        URI serverURI;

        try {
            serverURI = new URI(settings.getServerUrl());
        } catch (URISyntaxException e) {
            serverURI = URI.create("http://localhost:8081");
        }
        String environment = settings.getEnvironment();
        String macAddress = settings.getMacAddress();


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        String info = "Server:" + serverURI +"\n" +
                "Environment:" + environment +"\n" +
                "Mac:" + macAddress +"\n" +
                "Version:" + BuildConfig.VERSION_NAME +"\n";

        log.info("Info: " + info);
        log.debug("Debug check");

        appText.setText(info);
    }
    /**
     * Handler for the start button onClick event
     * @param view the button?
     */
    public void onClickStart(View view) {
        Intent intent = new Intent(getActivity(), UpdateActivity.class);
        startActivityForResult(intent, UPDATE_REQUEST);
        startButton.setEnabled(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UPDATE_REQUEST && resultCode != Activity.RESULT_CANCELED && data != null) {
            DeploymentStatus status = DeploymentStatus.valueOf(data.getStringExtra(DeploymentManager.EXTRA_DEPLOYMENT_STATUS));
            switch (status) {
                case SUCCESS:
                    transitionToRunning();
                    break;
                case BROWSER_UPDATE:
                    new AlertDialog.Builder(getActivity()).setMessage(R.string.browser_update_required).setPositiveButton(R.string.ok, null).show();
                    break;
                case FAILED_REGISTRATION_CONNECT:
                case FAILED_REGISTRATION_GENERIC:
                    new AlertDialog.Builder(getActivity()).setMessage(R.string.deployment_failed_connect).setPositiveButton(R.string.ok, null).show();
                    break;
                case FAILED_REGISTRATION_ERROR:
                    new AlertDialog.Builder(getActivity()).setMessage(R.string.deployment_failed_registration_error).setPositiveButton(R.string.ok, null).show();
                    break;
                case FAILED_LICENSE_KEY:
                    showInvalidLicenseKeyAlert();
                    break;
                case FAILED_MANIFEST:
                    new AlertDialog.Builder(getActivity()).setMessage(R.string.deployment_failed_manifest_error).setPositiveButton(R.string.ok, null).show();
                    break;
                case FAILED_DOWNLOAD:
                    new AlertDialog.Builder(getActivity()).setMessage(R.string.deployment_failed_download).setPositiveButton(R.string.ok, null).show();
                    break;
                default:
                    new AlertDialog.Builder(getActivity()).setMessage("Error.").setPositiveButton(R.string.ok, null).show();
                    break;
            }
        }
        startButton.setEnabled(true);
    }

    private boolean isValidLicenseKey(String licenseKey) {
        if (licenseKey == null) {
            return false;
        }
        return licenseKey.toLowerCase().startsWith("v") ||
                Verifier.verify(settings.getMacAddress().replaceAll("[^A-Fa-f0-9]", ""),
                        licenseKey);
    }
    private void showInvalidLicenseKeyAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).setMessage(R.string.invalid_license_key).setPositiveButton(R.string.ok, null).show();
    }

    protected void transitionToRunning() {
        cleanOldLogFiles();
        String licenseKey = settings.getLicenseKey();

        if (settings.isDevMode()) {
            Intent nextActivityIntent = new Intent(getActivity(), BrowserIOActivity.class);
            nextActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            startActivity(nextActivityIntent);
        } else if (!isValidLicenseKey(licenseKey)) {
            showInvalidLicenseKeyAlert();
        } else {
            Intent nextActivityIntent = new Intent(getActivity(), VoxbrowserRunningActivity.class);
            nextActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            startActivity(nextActivityIntent);
        }
    }

    private void cleanOldLogFiles() {
        int days = settings.getDaysToKeep();
        int num = settings.getNumberToKeep();
        num = num < 1 ? 1 : num;
        long cutOffDate = System.currentTimeMillis() - (days * 86400000);
        File logRootDir = getActivity().getExternalFilesDir("logs");
        File[] logDirs = logRootDir.listFiles((pathname) -> pathname.isDirectory() && pathname.getName().matches("\\d{8}T\\d{6}"));

        // sort ascending, names are the same order lexicographically
        Arrays.sort(logDirs, (lhs, rhs) -> lhs.getName().compareTo(rhs.getName()));

        for (int i = 0; i < logDirs.length - num; i++) {
            try {
                FileUtils.deleteDirectory(logDirs[i]);
                log.info("Removed {}", logDirs[i].getAbsolutePath());
            } catch (IOException e) {
                log.error("Failed to deleted " + logDirs[i].getAbsolutePath(), e);
            }
        }

        for (File logDir : logDirs) {
            if (logDir.exists() && logDir != logDirs[logDirs.length - 1]) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd'T'hhmmss");
                try {
                    Date logDirDate = sdf.parse(logDir.getName());
                    if (logDirDate.getTime() < cutOffDate) {
                        FileUtils.deleteDirectory(logDir);
                        log.info("Removed {}", logDir.getAbsolutePath());
                    }
                } catch (ParseException e) {
                    // should not happen since we create our own dirs, but if it does, just skip the dir
                    log.error("Failed to parse {}", logDir.getName(), e);
                } catch (IOException e) {
                    log.error("Failed to delete {}", logDir.getAbsolutePath(), e);
                }
            }
        }
    }
}
