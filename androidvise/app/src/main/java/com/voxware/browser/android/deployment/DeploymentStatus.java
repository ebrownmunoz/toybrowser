package com.voxware.browser.android.deployment;

/**
 * Created by ehom on 11/1/16.
 */
public enum DeploymentStatus {
    NOT_STARTED,
    REGISTERING,
    DEPLOYING,
    SUCCESS,
    FAILURE, // general failure
    FAILED_REGISTRATION_GENERIC, // failed register
    FAILED_REGISTRATION_CONNECT, // failed register due to connection error/timeout
    FAILED_REGISTRATION_ERROR, // failed register (server returned error
    FAILED_LICENSE_KEY, // register returned bad license key
    FAILED_MANIFEST, // failed to get manifest
    FAILED_DOWNLOAD, // failed to download a component
    BROWSER_UPDATE// special instructions
}
