package com.voxware.browser.applets;

import com.voxware.browser.interpreter.Result;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ScriptableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import voxware.engine.recognition.sapivise.ViseResult;

/**
 * Created by edh on 12/2/2015.
 */
public class TrainUtterance extends AbstractApplet {
    private static final Logger log = LoggerFactory.getLogger(TrainUtterance.class);

    @Inject
    public TrainUtterance() {
    }
    @Override
    public Result run() throws MissingParameterException {
        ScriptableObject scope = getContext().initStandardObjects();
        Object score;

        // Get the parameters
        ViseResult result = require("result").as(ViseResult.class);
        String prompt = parameter("prompt").asString();
        boolean retain = parameter("retain", false).asBoolean();

        // Interpret the parameters

        if (prompt == null) {
            result.tokenCorrection(null, null, null, ViseResult.DONT_KNOW);                 // Throws ResultStateError, IllegalArgumentException
            log.warn("TrainUtterance.run: called ViseResult.tokenCorrection() without correction");
        } else {
            if (prompt.length() == 0)
                return Result.event("error.applet.illegalparameter", "\"prompt\" parameter of TrainUtterance is empty");
            String[] correctTokens = prompt.split("\\s+");
            result.tokenCorrection(correctTokens, null, null, ViseResult.MISRECOGNITION);   // Throws ResultStateError, IllegalArgumentException
            log.info("TrainUtterance.run: called ViseResult.tokenCorrection() with correction to \"" + prompt + "\"");
        }
        // Return the result's path score
        score = Context.javaToJS(result.pathScore(), scope);
        // Release the training information in the result if the "retain" parameter is not true
        if (!retain) {
            result.releaseTrainingInfo();
            log.debug("TrainUtterance.run: released training info");
        }

        // Return the score
        Result ret = Result.Ok();
        ret.setValue(score);
        return ret;
    }
}
