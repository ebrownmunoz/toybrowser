package com.voxware.browser.applets;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiVise;
import voxware.voxfile.VoxFormatException;
import voxware.voxfile.VoxReport;

/**
 * Created by edh on 12/7/2015.
 */
public class Vocabulary extends AbstractApplet {
    public static final boolean BATCH_DEFAULT = false;
    public static final int COUNT_DEFAULT = 1;

    private SapiVise sapiVise;
    private DocumentServer documentServer;

    @Inject
    public Vocabulary(VISERecognizer recognizer, DocumentServer documentServer) {
        this.sapiVise = recognizer.getSapiVise();
        this.documentServer = documentServer;
    }

    @Override
    public Result run() throws Exception {
        String grammarUrl = parameter("grammar", null).asString();
        String[] script = parameter("script", null).asStringArray();
        String profileUrl = parameter("profile", null).asString();
        String strip = parameter("strip", null).asString();
        String type = parameter("type", null).asString();

        CacheControl caching = CacheControl.FOREVER_CACHE;
        String cachingParam = parameter("caching", null).asString();

        // Interpret the parameters
        boolean all = !"unenrolled".equalsIgnoreCase(type);

        try {
            if (cachingParam != null && "safe".equalsIgnoreCase(cachingParam)) {
                caching = CacheControl.NO_CACHE;
            }
        } catch (Exception e) {

        }

        // Acquire the necessary images
        byte[] recImage = null;
        InputStream is = null;
        if (grammarUrl != null) {
            try {
                is = documentServer.getDocument(new URI(grammarUrl), null, caching);
                recImage = IOUtils.toByteArray(is);
            } finally {
                IOUtils.closeQuietly(is);
                is = null;
            }
        }
        byte[] voiImage = null;
        if ((recImage == null || !all) && profileUrl != null) {
            SpeakerProfile[] speakers = sapiVise.listKnownSpeakers();
            for (SpeakerProfile speaker : speakers) {
                if (profileUrl.equals(speaker.getName())) {
                    // we found one
                    voiImage = sapiVise.writeVendorSpeakerProfile(profileUrl);
                    break;
                }
            }
            if (voiImage == null) {
                try {
                    is = documentServer.getDocument(new URI(profileUrl), null, caching);
                    voiImage = IOUtils.toByteArray(is);
                    sapiVise.readVendorSpeakerProfile(profileUrl, voiImage);
                } finally {
                    IOUtils.closeQuietly(is);
                    is = null;
                }
            }
        }

        String[] words = null;
        if (script == null && recImage == null && voiImage == null) {
            return Result.event("error.applet.missingparameter", "\"script\", \"grammar\" and \"profile\" parameters of Vocabulary applet are all missing or empty");
        } else if (!all) {
            if (script == null && recImage == null)
                return Result.event("error.applet.missingparameter", "\"script\" and \"grammar\" parameter of Vocabulary applet are both missing or empty");
            else if (voiImage == null)
                return Result.event("error.applet.missingparameter", "\"profile\" parameter of Vocabulary applet is missing or empty");
            if (script != null)
                words = missingWords(script, voiImage);
            else
                words = VoxReport.missingWords(new ByteArrayInputStream(recImage), new ByteArrayInputStream(voiImage));  // Throws IOException, VoxFormatException
        } else {
            if (script != null) words = allWords(script);
            else if (recImage != null)
                words = VoxReport.allWords(new ByteArrayInputStream(recImage));  // Throws IOException, VoxFormatException
            else if (voiImage != null)
                words = VoxReport.allWords(new ByteArrayInputStream(voiImage));  // Throws IOException, VoxFormatException
        }
        if (words == null) words = new String[0];

        if (strip != null) {
            for (int i = 0; i < words.length; i++) {
                int stripIndex = words[i].indexOf(strip);
                if (stripIndex > 0) words[i] = words[i].substring(0, stripIndex);
            }
            // Eliminate duplicate words
            words = (String[]) new HashSet<String>(Arrays.asList(words)).toArray(new String[0]);
        }

        // Wrap the words in a JavaScript Array
        Result result = Result.Ok();
        result.setValue(getContext().newArray(getContext().initStandardObjects(), Arrays.copyOf(words, words.length, Object[].class)));
        return result;
    }

    private String[] missingWords(String[] script, byte[] voiImage) throws IOException, VoxFormatException {
        HashSet<String> wordSet = new HashSet<>(script.length);
        for (int index = 0; index < script.length; index++) {
            StringTokenizer tokens = new StringTokenizer(script[index]);
            while (tokens.hasMoreTokens()) {
                String token = tokens.nextToken();
                // Deal with double-quoted compound words
                if (token.startsWith("\"")) {
                    StringBuffer tokenBuffer = new StringBuffer(token);
                    tokenBuffer.deleteCharAt(0);
                    int length = 0;
                    boolean unterminated = true;
                    while (((length = tokenBuffer.length()) == 0 || (unterminated = tokenBuffer.charAt(length - 1) != '\"')) && tokens.hasMoreTokens())
                        tokenBuffer.append(" ").append(tokens.nextToken());
                    if (!unterminated) tokenBuffer.deleteCharAt(length - 1);
                    token = tokenBuffer.toString().trim();
                }
                if (token.length() > 0) wordSet.add(token);
            }
        }
        if (voiImage != null) {
            String[] enrolledWords = VoxReport.allWords(new ByteArrayInputStream(voiImage));  // Throws IOException, VoxFormatException
            for (int index = 0; index < enrolledWords.length; index++)
                wordSet.remove(enrolledWords[index]);
        }
        return (String[]) wordSet.toArray(new String[0]);
    }

    private String[] allWords(String[] script) throws IOException, VoxFormatException {
        return missingWords(script, null);  // Throws IOException, VoxFormatException
    }
}
