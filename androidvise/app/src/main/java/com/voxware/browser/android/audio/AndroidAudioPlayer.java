package com.voxware.browser.android.audio;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.io.ByteArrayPlayer;
import com.voxware.browser.io.StringPlayer;
import com.voxware.browser.io.UriPlayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Created by edh on 3/24/2016.
 */
public class AndroidAudioPlayer extends AudioPlayer {
    private static final Logger log = LoggerFactory.getLogger(AndroidAudioPlayer.class);
    private final Context context;
    private final AudioManager audioManager;

    @Inject
    public AndroidAudioPlayer(Context context, StringPlayer tts, ByteArrayPlayer bytePlayer, UriPlayer uriPlayer) {
        super(tts, bytePlayer, uriPlayer);
        this.context = context;
        this.audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }


    /**
     * Gets the stream id to use. For instance, STREAM_MUSIC is usually ok except when the bluetooth headset is connected. Then we want to use STREAM_VOICE_CALL.
     * @return STREAM_VOICE_CALL if BT headset is connected and in use, otherwise STREAM_MUSIC
     */
    protected int getStreamId() {
        Intent btIntent = context.registerReceiver(null, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED));
        if (audioManager.isBluetoothScoAvailableOffCall() &&
                (audioManager.isBluetoothA2dpOn() || audioManager.isBluetoothScoOn()) &&
                btIntent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, AudioManager.SCO_AUDIO_STATE_DISCONNECTED) == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
            //return AudioManager.STREAM_VOICE_CALL;
            // this is AudioManager.STREAM_BLUETOOTH_SCO, but is hidden from the compiler
            return 6;
        } else {
            return AudioManager.STREAM_MUSIC;
        }
    }

    @Override
    public void setParameter(String name, Object value) {
        if (name.equals("speakervolume")) {
            if (value instanceof Number) {
                audioManager.setStreamVolume(getStreamId(), mapVolume(new Double(((Number)value).doubleValue())), 0);
            } else if (value instanceof String) {
                audioManager.setStreamVolume(getStreamId(), mapVolume(Double.valueOf(value.toString())), 0);
            } else {
                log.error("Unknown type: " + value.getClass());
            }
        } else {
            super.setParameter(name, value);
        }
    }

    @Override
    public Object getParameter(String name) {
        if (name.equals("speakervolume")) {
            audioManager.getStreamVolume(getStreamId());
        }
        return super.getParameter(name);
    }

    /**
     * Tries mapping old style volumes to new style volume
     */
    private int mapVolume(Double volume) {
        int maxVolume = audioManager.getStreamMaxVolume(getStreamId());
        int newVolume = (int)Math.ceil((volume / 50.0d) * maxVolume);
        if (newVolume < 0) {
            newVolume = 0;
        }
        if (newVolume > maxVolume) {
            newVolume = maxVolume;
        }
        if (newVolume == 0 && volume > 0.0d) {
            newVolume = 1;
        }
        return newVolume;
    }
}
