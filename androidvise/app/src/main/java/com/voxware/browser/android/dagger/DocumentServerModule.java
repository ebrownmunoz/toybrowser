package com.voxware.browser.android.dagger;

import android.content.Context;

import com.voxware.browser.android.documentserver.AndroidDocumentServer;
import com.voxware.browser.android.utils.VoxBrowserSettings;
import com.voxware.browser.documentserver.DocumentServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by edh on 10/13/2016.
 */
@Module(includes = AndroidModule.class)
public class DocumentServerModule {
    private static final Logger log = LoggerFactory.getLogger(DocumentServerModule.class);

    @Provides
    @Singleton
    public DocumentServer providesDocumentServer(Context context, VoxBrowserSettings settings) {
        try {
            URI serverUri = new URI(settings.getServerUrl());
            if (!serverUri.getPath().endsWith("/")) {
                serverUri = new URI(serverUri.getScheme(), serverUri.getUserInfo(), serverUri.getHost(), serverUri.getPort(),
                        serverUri.getPath() + "/", null, null);
            }
            return new AndroidDocumentServer(serverUri, context);
        } catch (URISyntaxException e) {
            log.warn("Error processing root URI \"" + settings.getServerUrl().toString() + "\"", e);
        }

        return null;
    }
}
