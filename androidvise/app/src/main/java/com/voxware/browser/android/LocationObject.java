package com.voxware.browser.android;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by edh on 9/13/2016.
 */
public class LocationObject {
    private final static Logger log = LoggerFactory.getLogger(LocationObject.class);
    private final Context context;
    private final LocationManager locationManager;
    private final LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            lastLocation = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
    private Location lastLocation;
    private boolean listenerRegistered = false;

    public LocationObject(Context context) {
        this.context = context;
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        registerListener();
    }

    public void close() {
        unregisterListener();
    }

    private void registerListener() {
        List<String> providers = locationManager.getProviders(true);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String bestProvider = locationManager.getBestProvider(criteria, true);
        if (bestProvider != null) {
            try {
                locationManager.requestLocationUpdates(bestProvider, 500l, 0.5f, listener, Looper.getMainLooper());
                listenerRegistered = true;
            } catch (IllegalArgumentException e) {
                // updates are unavailable
                log.warn("Error requesting location updates", e);
            }
        }
    }

    private void unregisterListener() {
        if (listenerRegistered) {
            locationManager.removeUpdates(listener);
        }
    }

    public float getAccuracy() {
        return lastLocation != null ? lastLocation.getAccuracy() : 0.0f;
    }

    public double getAltitude() {
        return lastLocation != null ? lastLocation.getAltitude() : 0.0d;
    }

    public float getBearing() {
        return lastLocation != null ? lastLocation.getBearing() : 0.0f;
    }

    public Bundle getExtras() {
        return lastLocation != null ? lastLocation.getExtras() : null;
    }

    public double getLatitude() {
        return lastLocation != null ? lastLocation.getLatitude() : 0.0d;
    }

    public double getLongitude() {
        return lastLocation != null ? lastLocation.getLongitude() : 0.0d;
    }

    public String getProvider() {
        return lastLocation != null ? lastLocation.getProvider() : null;
    }

    public float getSpeed() {
        return lastLocation != null ? lastLocation.getSpeed() : 0.0f;
    }

    public long getTime() {
        return lastLocation != null ? lastLocation.getTime() : 0;
    }

    public boolean hasAccuracy() {
        return lastLocation != null ? lastLocation.hasAccuracy() : false;
    }

    public boolean hasAltitude() {
        return lastLocation != null ? lastLocation.hasAltitude() : false;
    }

    public boolean hasBearing() {
        return lastLocation != null ? lastLocation.hasBearing() : false;
    }

    public boolean hasSpeed() {
        return lastLocation != null ? lastLocation.hasSpeed() : false;
    }
}
