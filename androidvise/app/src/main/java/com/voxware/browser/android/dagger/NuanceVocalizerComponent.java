package com.voxware.browser.android.dagger;

import com.voxware.browser.audioplayer.AudioPlayer;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by edh on 10/13/2016.
 */
@Singleton
@Component(modules = {NuanceVocalizerModule.class})
public interface NuanceVocalizerComponent {
    AudioPlayer audioPlayer();
}
