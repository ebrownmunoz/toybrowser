package com.voxware.browser.android.deployment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.voxware.browser.android.BuildConfig;
import com.voxware.browser.android.activity.InstallAPKActivity;
import com.voxware.browser.android.utils.VoxBrowserSettings;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.DocumentServerInputStream;
import com.voxware.browser.security.key.Verifier;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.AndroidManifestPropertiesUtil;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import static android.R.attr.name;

/**
 * Does most of the work of deploying the manifest.
 * Created by eric on 7/4/16.
 */
public class DeploymentManager implements Callable<DeploymentStatus> {
    public static final String ACTION_DEPLOYMENT_PROGRESS = "com.voxware.browser.android.action.deployment_progress";
    public static final String EXTRA_DEPLOYMENT_STEP_NAME = "com.voxware.browser.android.extra.deployment_step_name";
    public static final String EXTRA_DEPLOYMENT_STEP = "com.voxware.browser.android.extra.deployment_step";
    public static final String EXTRA_TOTAL_DEPLOYMENT_STEPS = "com.voxware.browser.android.extra.total_deployment_steps";

    public static final String ACTION_ACQUISITION_PROGRESS = "com.voxware.browser.android.action.acquisition_progress";
    public static final String EXTRA_CURRENT_BYTES = "com.voxware.browser.android.extra.current_bytes";
    public static final String EXTRA_TOTAL_BYTES = "com.voxware.browser.android.extra.total_bytes";

    public static final String ACTION_DEPLOYMENT_COMPLETE = "com.voxware.browser.android.action.deployment_complete";
    public static final String EXTRA_DEPLOYMENT_STATUS = "com.voxware.browser.android.extra.deployment_status";

    public static final String ACTION_DEPLOY_APK = "com.voxware.browser.android.action.deploy_apk";
    public static final String ACTION_APK_DEPLOYED = "com.voxware.browser.android.action.apk_deployed";
    public static final String EXTRA_APK_FILE = "com.voxware.browser.android.extra.apk_file";

    public static final String ACTION_UPDATE_UI_PAUSING = "com.voxware.browser.android.action.update_ui_pausing";
    public static final String ACTION_UPDATE_UI_RESUMING = "com.voxware.browser.android.action.update_ui_resuming";

    private static final String NUANCE_VOICE_FILE = "Android/obb/com.voxware.browser.android/main." + BuildConfig.VERSION_CODE + ".com.voxware.browser.android.obb";
    private static final String NUANCE_VOICE_FILE_NAME = "main." + BuildConfig.VERSION_CODE + ".com.voxware.browser.android.obb";
    private static final boolean USE_OBB_FILE_FOR_VOICE = true;
    private static final char[] hexTable = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final Logger log = LoggerFactory.getLogger(DeploymentManager.class);

    private static final String HEADER_LICENSE_KEY = "license-key";
    private static final String HEADER_ENVIRONMENT = "environment";
    private static final String HEADER_CLIENT_GROUP = "clientGroup";

    private static final URI MANIFEST_URI = URI.create("vmserver/logon/manifest"); // the 'logon'is filler
    private static final URI RESOURCE_URI = URI.create("vmserver/logon/resource"); // the 'logon'is filler
    private static final URI VOICE_URI = URI.create("vmserver/logon/ttsvoice"); // the 'logon'is still filler
    private static final String DEPLOYMENT_DIR = "deployment";
    private static final String MANIFEST = "manifest.json";
    private static final String CHECKSUM_ALGORITHM = "MD5";


    private final Context context;
    private final DocumentServer documentServer;
    private final VoxBrowserSettings settings;
    private final String macAddress;
    private final AtomicReference<DeploymentStatus> status = new AtomicReference<>(DeploymentStatus.NOT_STARTED);
    private final LocalBroadcastManager localBroadcastManager;

    @Inject
    public DeploymentManager(Context context, DocumentServer documentServer) {
        this.context = context;
        this.settings = new VoxBrowserSettings(context);
        this.documentServer = documentServer;
        String macAddress = settings.detectMacAddress();
        if (macAddress != null) {
            macAddress = macAddress.toLowerCase().replaceAll("[.:-]", "");
        }
        this.macAddress = macAddress;
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
    }

    public DeploymentStatus getStatus() {
        return status.get();
    }

    @Override
    public DeploymentStatus call() {
        try {
            status.set(DeploymentStatus.REGISTERING);
            if (sendRegistrationMessage()) {
                status.set(DeploymentStatus.DEPLOYING);
                updateDeployment(macAddress);
            }
        } catch (Exception e) {
            log.error("Deployment error", e);
            status.set(DeploymentStatus.FAILURE);
        }
        Intent result = new Intent(ACTION_DEPLOYMENT_COMPLETE);
        result.putExtra(EXTRA_DEPLOYMENT_STATUS, status.get().name());
        localBroadcastManager.sendBroadcast(result);
        return status.get();
    }

    private boolean sendRegistrationMessage() {
        String fullUrlString = settings.getServerUrl() +
                "/vmserver/logon/register" +
                "?name=Platform&clientType=" + Uri.encode(settings.getClientType()) +
                "&oemVariant=" + Uri.encode(settings.getOemVariant())+
                "&cfgVariant=" + Uri.encode(settings.getCfgVariant()) +
                "&macAddress=" + Uri.encode(macAddress) +
                "&logicalName=" + Uri.encode(settings.getLogicalName()) +
                "&headsetType=Voxware/Standard" +
                "&environment=" + Uri.encode(settings.getEnvironment()) +
                "&clientGroup=" + Uri.encode(settings.getClientGroup()) +
                "&crc=beef";

        log.info("Registration request : " + fullUrlString);

        URLConnection conn = null;
        InputStream instream = null;

        try {
            URL fullUrl = new URL(fullUrlString);
            conn = fullUrl.openConnection();
            if (conn instanceof HttpURLConnection) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) conn;
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setReadTimeout(12000);
                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode != 200 && responseCode != 204) {
                    status.set(DeploymentStatus.FAILED_REGISTRATION_ERROR);
                    return false;
                }
            }
            instream = conn.getInputStream();
            String licenseKey = conn.getHeaderField(HEADER_LICENSE_KEY);
            String environment = conn.getHeaderField(HEADER_ENVIRONMENT);
            String clientGroup = conn.getHeaderField(HEADER_CLIENT_GROUP);

            // we need to update the preferences too
            settings.setLicenseKey(licenseKey);
            settings.setEnvironment(environment);
            settings.setClientGroup(clientGroup);

            if (!isValidLicenseKey(licenseKey)) {
                status.set(DeploymentStatus.FAILED_LICENSE_KEY);
                return false;
            }

            byte[] buffer = new byte[4096];

            //noinspection StatementWithEmptyBody
            while (instream.read(buffer) >= 0) {
                // this data is thrown away
            }
            return true;
        } catch (ConnectException e) {
            log.error("Exception during Client Registration", e);
            status.set(DeploymentStatus.FAILED_REGISTRATION_CONNECT);
            return false;
        } catch (Exception e) {
            log.error("Exception during Client Registration", e);
            status.set(DeploymentStatus.FAILED_REGISTRATION_GENERIC);
            return false;
        } finally {
            IOUtils.closeQuietly(instream);
            if (conn != null && conn instanceof HttpURLConnection) {
                ((HttpURLConnection)conn).disconnect();
            }
        }
    }

    private boolean isValidLicenseKey(String licenseKey) {
        return (licenseKey != null) && (licenseKey.toLowerCase().startsWith("v") ||
                Verifier.verify(settings.getMacAddress().replaceAll("[^A-Fa-f0-9]", ""),
                        licenseKey));
    }

    private void updateDeployment(String macaddress) {
        status.set(DeploymentStatus.DEPLOYING);
        String newBrowser = null;
        String newPrompts = null;
        String newLogback = null;
        VoxManifest savedManifest = getManifestFromInternalStorage();
        VoxManifest newManifest = getManifestFromServer(macaddress);
        if (savedManifest != null) {
            for (VoxManifest.Item resource : savedManifest.getResources()) {
                if (resource.getName().endsWith(".apk")) {
                    File apkFile = new File(getDeploymentDirectory(), resource.getName());
                    if (isBrowserApk(apkFile)) {
                        if (installBrowserApk(resource.getName())) {
                            status.set(DeploymentStatus.BROWSER_UPDATE);
                            return;
                        }
                    }
                }
            }
        }
        if (newManifest == null) {
            status.set(DeploymentStatus.FAILED_MANIFEST);
            return;
        }
        List<DeploymentAction> actions = generateDifferences(savedManifest, newManifest);
        log.info("ToDo:");
        for (DeploymentAction action : actions) {
            log.info(action.toString());
        }
        int actionCount = 0;
        for (DeploymentAction action : actions) {
            updateActionProgress(action.getName(), actionCount++, actions.size());
            if (!action.call()) {
                status.set(DeploymentStatus.FAILED_DOWNLOAD);
                return;
            }
            if (action.getName().startsWith("logback")) {
                newLogback = action.getName();
            }
            if (action.getName().startsWith("prompts")) {
                newPrompts = action.getName();
            }
            // TODO: this needs to apply only to new browsers, not voice APKs
            if (action.getName().endsWith(".apk")) {https://www.youtube.com/watch?v=xT9pygVQwgk
                newBrowser = action.getName();
            }
        }
        updateActionProgress("Finalizing", actionCount++, actions.size());
        writeManifestToInternalStorage(newManifest);
        if (newPrompts != null) {
            File source = new File(getDeploymentDirectory(), newPrompts);
            File dest = new File(context.getFilesDir(), "audio/prompts.zip");
            try {
                FileUtils.copyFile(source, dest);
            } catch (IOException e) {
                log.error("Error moving prompts.zip", e);
                status.set(DeploymentStatus.FAILURE);
                return;
            }
        }
        if (newLogback != null) {
            File dest = new File(context.getFilesDir(), "logback.xml");
            if (dest.exists() && !dest.delete()) {
                log.error("Failed to delete {}", dest.getAbsolutePath());
                status.set(DeploymentStatus.FAILURE);
                return;
            }
            File source = new File(getDeploymentDirectory(), newLogback);
            try {
                if (source.exists()) {
                    FileUtils.copyFile(source, dest);
                }
            } catch (IOException e) {
                log.error("Error moving logback config file", e);
                status.set(DeploymentStatus.FAILURE);
                return;
            }
        }

        if (newBrowser != null) {
            // new manifest has a new browser
            if (installBrowserApk(newBrowser)) {
                status.set(DeploymentStatus.BROWSER_UPDATE);
                return;
            }
        }
        status.set(DeploymentStatus.SUCCESS);
    }

    public static void resetLogger(File dest) {
        try {
            resetLogger(new FileInputStream(dest));
        } catch (FileNotFoundException e) {
            log.error("Error reading log file " + dest, e);
        }
    }

    private static void resetLogger(InputStream instream) {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        context.reset();
        try {
            AndroidManifestPropertiesUtil.setAndroidProperties(context);
            configurator.doConfigure(instream);
        } catch (JoranException e) {
            log.error("Error configuring log file", e);
        } finally {
            IOUtils.closeQuietly(instream);
        }
    }

    /**
     * Delete the file from the deployment directory
     *
     * @param name the name of the resource to delete
     * @return true if the file did not exist or was deleted succesfully, false otherwise
     */
    private boolean deleteResource(@NonNull String name) {
        File outFile = new File(getDeploymentDirectory(), name);
        return !(outFile.exists() && !outFile.delete());
    }

    private boolean deleteVoice(@NonNull String name) {
        if (USE_OBB_FILE_FOR_VOICE) {
            return deleteObbs();
        } else {
            // we don't support this yet, but for Nuance Vocalizer voice APKs, should probably involve uninstalling that apk
            throw new UnsupportedOperationException();
        }

    }

    /**
     * Acquires a resource and returns the checksum
     * @return the checksum of the download or null if download failed
     */
    private byte[] acquireResource(String resourceName) {
        return acquireResource(resourceName, new File(getDeploymentDirectory(), resourceName));
    }

    private byte[] acquireResource(String resourceName, File destinationFile) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", resourceName);
        return acquire(RESOURCE_URI, parameters, destinationFile);
    }

    private byte[] acquireVoice(String voiceName, String language) {
        if (language == null) {
            log.error("Null language, defaulting to en-US (but this should be fixed");
            language = "en-US";
        }

        if (USE_OBB_FILE_FOR_VOICE) {
            return acquireVoiceObb(voiceName, "Nuance", language);
        } else {
            return acquireVoice(voiceName, "Nuance", language);
        }
    }

    private byte[] acquireVoiceObb(String voiceName, String engineName, String language) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", voiceName);
        parameters.put("engine", engineName);
        parameters.put("language", language);
        parameters.put("platform", "Android");
        return acquire(VOICE_URI, parameters, new File(getDeploymentDirectory(), NUANCE_VOICE_FILE_NAME));
    }

    private byte[] acquireVoice(String voiceName, String engineName, String language) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", voiceName);
        parameters.put("engine", engineName);
        parameters.put("language", language);
        parameters.put("platform", "Android");
        return acquire(VOICE_URI, parameters, new File(getDeploymentDirectory(), voiceName+"_"+language + ".apk"));
    }


    private byte[] acquire(URI uri, Map<String, Object> parameters, File destinationFile) {
        File parentDir = destinationFile.getParentFile();
        if (!parentDir.exists() && !parentDir.mkdirs()) {
            log.error("Failed to create parent dirs {} to store {}", parentDir.getAbsolutePath());
            return null;
        }
        MessageDigest checksum;
        try {
            checksum = MessageDigest.getInstance(CHECKSUM_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            log.error("Exception calculating Checksum" + name, e);
            return null;
        }
        DocumentServerInputStream instream = null;
        FileOutputStream fos = null;
        try {
            instream = documentServer.getDocument(uri, parameters, CacheControl.FOREVER_CACHE, 12000, 20000);
            fos = new FileOutputStream(destinationFile);
            int len;
            long totalBytes = 0;
            updateAcquisitionProgress(totalBytes, instream.getContentLength());
            byte[] buffer = new byte[128 * 1024];
            while ((len = instream.read(buffer)) >= 0) {
                totalBytes += len;
                fos.write(buffer, 0, len);
                checksum.update(buffer, 0, len);
                updateAcquisitionProgress(totalBytes, instream.getContentLength());
            }
            byte[] ret = checksum.digest();
            log.info("Acquired {} as {} with checksum {}", uri, destinationFile.getAbsolutePath(), encodeHexString(ret));
            log.info("Statistics: {}", instream.getStatistics());
            return ret;
        } catch (Exception e) {
            log.error("Error acquiring URI {}", uri.toString(), e);
            return null;
        } finally {
            IOUtils.closeQuietly(instream);
            IOUtils.closeQuietly(fos);
        }
    }



    private void installApk(File apkFile) {
        final CountDownLatch latch = new CountDownLatch(1);
        final BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                latch.countDown();
            }
        };
        localBroadcastManager.registerReceiver(receiver, new IntentFilter(ACTION_APK_DEPLOYED));
        Intent intent = new Intent(ACTION_DEPLOY_APK);
        intent.putExtra(EXTRA_APK_FILE, apkFile);
        if (localBroadcastManager.sendBroadcast(intent)) {
            // somone is listening
            try {
                latch.await();
            } catch (InterruptedException e) {

            }
        } else {
            // no one is registered, we need to do this ourselves?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    /**
     * Installs the browser apk by copying the apk to external cache dir/browser.apk, then issuing intent
     * @param apkName name of the browser apk file
     */
    private boolean installBrowserApk(@NonNull String apkName) {
        File apkFile = new File(getDeploymentDirectory(), apkName);
        File cacheCopy = new File(context.getExternalCacheDir(), "browser.apk");

        if (!apkFile.exists()) {
            log.error("APK doesn't exist {}", apkFile.getAbsolutePath());
            return false;
        }
        if (cacheCopy.exists() && !cacheCopy.delete()) {
            log.error("Failed to delete {}", cacheCopy);
            return false;
        }

        String newApkVersionName = getVersionName(apkFile);
        if (BuildConfig.VERSION_NAME.equals(newApkVersionName)) {
            // we matched
            log.info("Not installing browser, version names match {}", newApkVersionName);
            return false;
        } else {
            log.info("Installing browser version {}", getVersionName(apkFile));
            try {
                FileUtils.copyFile(apkFile, cacheCopy);
            } catch (IOException e) {
                log.error("Error copying file", e);
            }
            Intent intent = new Intent(context, InstallAPKActivity.class);
            intent.putExtra(EXTRA_APK_FILE, cacheCopy);
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_NEW_TASK);
            final CountDownLatch latch = new CountDownLatch(1);
            final BroadcastReceiver receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    latch.countDown();
                }
            };
            localBroadcastManager.registerReceiver(receiver, new IntentFilter(ACTION_APK_DEPLOYED));
            context.startActivity(intent);
            try {
                latch.await();
                return true;
            } catch (InterruptedException e) {
                return false;
            }
        }
    }

    /**
     * Get the Vox manifest from VMServer. This comes down as Json and is
     * parsed by VoxManifest.
     *
     * @return a manifest or null on error
     */
    public VoxManifest getManifestFromServer(String macaddress) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("macaddress", macaddress);
        InputStream instream = null;
        try {
            instream = documentServer.getDocument(MANIFEST_URI, parameters, CacheControl.NO_CACHE, 20000, 20000);
            return VoxManifest.parseVoxManifest(instream);
        } catch (DocumentServerException e) {
            log.error("Document Server Exception getting manifest", e);
            return null;
        } finally {
            IOUtils.closeQuietly(instream);
        }
    }

    /**
     * Get the priviously saved Manifest from the Deployment directory
     * in Internal storage. If there is no saved manifest, it returns
     * null
     *
     * @return previously saved manifest of null if none.
     */
    public VoxManifest getManifestFromInternalStorage() {
        File deploymentDir = getDeploymentDirectory();
        File manifestFile = new File(deploymentDir, MANIFEST);

        if (!manifestFile.exists()) {
            return null;
        }

        InputStream instream = null;
        try {
            instream = new FileInputStream(manifestFile);
            return VoxManifest.parseVoxManifest(instream);
        } catch (FileNotFoundException e) {
            log.error("Error getting manifest", e);
            return null;
        } finally {
            IOUtils.closeQuietly(instream);
        }
    }

    /**
     * Write the given manifest to Internal Storage as Json array.
     *
     * @param manifest the manifest to write
     */
    public void writeManifestToInternalStorage(VoxManifest manifest) {
        File deploymentDir = getDeploymentDirectory();
        File manifestFile = new File(deploymentDir, MANIFEST);
        FileOutputStream outstream = null;

        try {
            outstream = new FileOutputStream(manifestFile);
            outstream.write(manifest.toString().getBytes());
        } catch (IOException e) {
            log.error("Failed to write manifest to internal storage", e);
        } finally {
            IOUtils.closeQuietly(outstream);
        }
    }

    private File getDeploymentDirectory() {
        File deploymentmentDirectory = new File(context.getFilesDir(), DEPLOYMENT_DIR);
        if (!deploymentmentDirectory.exists() && !deploymentmentDirectory.mkdirs()) {
            log.error("Failed to create deployment directory {}", deploymentmentDirectory.getAbsolutePath());
        }
        return deploymentmentDirectory;
    }

    private boolean deleteObbs() {
        boolean success = true;
        File obbDirectory = new File(Environment.getExternalStorageDirectory(), "Android/obb/com.voxware.browser.android");
        File[] obbs = obbDirectory.listFiles((dir, s) -> {
            return s.matches("main\\.\\d+\\.com\\.voxware\\.browser\\.android\\.obb");
        });
        if (obbs != null) {
            for (File obb : obbs) {
                if (!obb.delete()) {
                    log.error("Failed to delete {}", obb.getAbsolutePath());
                    success = false;
                }
            }
        }
        return success;
    }

    /**
     * Generates a list of Actions that would change this manifest to <code>newManifest</code>
     * @param oldManifest the current manifest
     * @param newManifest the target manifest
     * @return a List of DeploymentActions to bring the deployment up to date
     */
    private List<DeploymentAction> generateDifferences(@Nullable VoxManifest oldManifest, @NonNull VoxManifest newManifest) {
        List<DeploymentAction> actions = new LinkedList<>();
        if (oldManifest == null) {
            for (VoxManifest.Item resource : newManifest.getResources()) {
                actions.add(new GetResourceAction(resource.getName(), resource.getMd5()));
            }
            for (VoxManifest.Item voice : newManifest.getVoices()) {
                actions.add(new GetVoiceAction(voice.getName(), voice.getLanguage(), voice.getMd5()));
            }
            return actions;
        }

        // find any deletions or updates
        for (VoxManifest.Item resource : oldManifest.getResources()) {
            VoxManifest.Item possiblyNewResource = newManifest.findResourceByName(resource.getName());
            if (possiblyNewResource == null) {
                // this resource is not listed in the other manifest, so remove it
                actions.add(new DeleteResourceAction(resource.getName()));
            } else if (!resource.getMd5().equals(possiblyNewResource.getMd5())) {
                // this resource has a different checksum in the other manifest, so update it
                actions.add(new GetResourceAction(possiblyNewResource.getName(), possiblyNewResource.getMd5()));
            }
        }
        // find anything new
        for (VoxManifest.Item possiblyNewItem : newManifest.getResources()) {
            if (oldManifest.findResourceByName(possiblyNewItem.getName()) == null) {
                // possiblyNewItem did not exist in the old manifest, so get it
                actions.add(new GetResourceAction(possiblyNewItem.getName(), possiblyNewItem.getMd5()));
            }
            // otherwise, it was handled above
        }
        for (VoxManifest.Item voice : oldManifest.getVoices()) {
            VoxManifest.Item possiblyNewVoice = newManifest.findVoiceByName(voice.getName());
            if (possiblyNewVoice == null) {
                // this resource is not listed in the other manifest, so remove it
                actions.add(new DeleteVoiceAction(voice.getName()));
            } else if (!voice.getMd5().equals(possiblyNewVoice.getMd5())) {
                // this resource has a different checksum in the other manifest, so update it
                actions.add(new GetVoiceAction(possiblyNewVoice.getName(), possiblyNewVoice.getLanguage(), possiblyNewVoice.getMd5()));
            }
        }

        for (VoxManifest.Item possiblyNewVoice : newManifest.getVoices()) {
            if (oldManifest.findVoiceByName(possiblyNewVoice.getName()) == null) {
                actions.add(new GetVoiceAction(possiblyNewVoice.getName(), possiblyNewVoice.getLanguage(), possiblyNewVoice.getMd5()));
            }
        }
        return actions;
    }


    private String encodeHexString(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte aByte : bytes) {
            builder.append(hexTable[(aByte & 240) >>> 4]);
            builder.append(hexTable[(aByte & 15)]);
        }
        return builder.toString();
    }

    private boolean isBrowserApk(File apkFile) {
        PackageInfo info = context.getPackageManager().getPackageArchiveInfo(apkFile.getAbsolutePath(), 0);
        return BuildConfig.APPLICATION_ID.equals(info.packageName);
    }

    private String getVersionName(File apkFile) {
        PackageInfo info = context.getPackageManager().getPackageArchiveInfo(apkFile.getAbsolutePath(), 0);
        return info.versionName;
    }

    private void updateActionProgress(String name, int currentAction, int totalActions) {
        Intent intent = new Intent(ACTION_DEPLOYMENT_PROGRESS);
        intent.putExtra(EXTRA_DEPLOYMENT_STEP_NAME, name);
        intent.putExtra(EXTRA_DEPLOYMENT_STEP, currentAction);
        intent.putExtra(EXTRA_TOTAL_DEPLOYMENT_STEPS, totalActions);
        localBroadcastManager.sendBroadcastSync(intent);
    }

    private void updateAcquisitionProgress(long totalBytes, long contentLength) {
        Intent intent = new Intent(ACTION_ACQUISITION_PROGRESS);
        intent.putExtra(EXTRA_CURRENT_BYTES, totalBytes);
        intent.putExtra(EXTRA_TOTAL_BYTES, contentLength);
        localBroadcastManager.sendBroadcastSync(intent);
    }

    interface DeploymentAction {
        String getName();
        boolean call();
    }

    /**
     * Deletes a resource
     */
    private class DeleteResourceAction implements DeploymentAction {
        private final String resourceName;
        DeleteResourceAction(String resourceName) {
            this.resourceName = resourceName;
        }

        public String getName() {
            return resourceName;
        }
        public boolean call() {
            deleteResource(resourceName);
            return true;
        }
        public String toString() {
            return String.format("Delete resource {}", resourceName);
        }
    }

    private class GetResourceAction implements DeploymentAction {
        private final String resourceName;
        private final String checksum;
        GetResourceAction(String resourceName, String checksum) {
            this.resourceName = resourceName;
            this.checksum = checksum;
        }
        public String getName() {
            return resourceName;
        }
        public boolean call() {
            byte[] checksum = acquireResource(resourceName);
            if (checksum == null) {
                log.error("Failed to download file");
                status.compareAndSet(DeploymentStatus.DEPLOYING, DeploymentStatus.FAILED_DOWNLOAD);
                return false;
            }
            if (this.checksum != null && !encodeHexString(checksum).equals(this.checksum)) {
                log.error("Checksum does not match, was {}, expected {}", encodeHexString(checksum), this.checksum);
                return false;
            }
            return true;
        }
        @Override
        public String toString() {
            return String.format("Download resource {}", resourceName);
        }
    }

    private class DeleteVoiceAction implements DeploymentAction {
        private final String voiceName;
        DeleteVoiceAction(String voiceName) {
            this.voiceName = voiceName;
        }
        public String getName() {
            return voiceName;
        }
        public boolean call() {
            deleteVoice(voiceName);
            return true;
        }
        @Override
        public String toString() {
            return String.format("Download voice {}", voiceName);
        }
    }

    private class GetVoiceAction implements DeploymentAction {
        private final String voiceName;
        private final String checksum;
        private final String language;
        GetVoiceAction(String voiceName, String language, String checksum) {
            this.voiceName = voiceName;
            this.checksum = checksum;
            this.language = language;
        }

        public String getName() {
            return voiceName;
        }

        public boolean call() {
            log.info("Acquiring voice " + voiceName + " language: " + language);
            byte[] checksum = acquireVoice(voiceName, language);
            if (checksum == null) {
                log.error("Failed to download file");
                status.compareAndSet(DeploymentStatus.DEPLOYING, DeploymentStatus.FAILED_DOWNLOAD);
                return false;
            }
            if (this.checksum != null && !encodeHexString(checksum).equals(this.checksum)) {
                log.error("Checksum does not match, was {}, expected {}", encodeHexString(checksum), this.checksum);
                status.compareAndSet(DeploymentStatus.DEPLOYING, DeploymentStatus.FAILED_DOWNLOAD);
                return false;
            }
            if (USE_OBB_FILE_FOR_VOICE) {
                // copy the obb file
                File sourceFile = new File(getDeploymentDirectory(), NUANCE_VOICE_FILE_NAME);
                File destinationFile = new File(Environment.getExternalStorageDirectory(), NUANCE_VOICE_FILE);
                try {
                    FileUtils.copyFile(sourceFile, destinationFile);
                } catch (IOException e) {
                    status.compareAndSet(DeploymentStatus.DEPLOYING, DeploymentStatus.FAILED_DOWNLOAD);
                    return false;
                }
            }
            return true;
        }

        @Override
        public String toString() {
            return String.format("Download voice %s", voiceName);
        }
    }

    static interface ApkInstaller {
        void installApk(File apk, boolean expectRestart);
    }
}
