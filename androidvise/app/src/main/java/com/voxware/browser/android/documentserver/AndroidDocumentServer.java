package com.voxware.browser.android.documentserver;

import android.content.Context;
import android.net.http.HttpResponseCache;

import com.voxware.browser.documentserver.BasicDocumentServer;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.DocumentServerInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Created by edh on 10/28/2015.
 */
public class AndroidDocumentServer extends BasicDocumentServer {

    public static final String ASSET = "asset";
    public static final String FILE = "file";
    private final Logger log = LoggerFactory.getLogger(AndroidDocumentServer.class);
    private final HttpResponseCache hrc = HttpResponseCache.getInstalled();


    public AndroidDocumentServer(URI rootURI, Context context) throws URISyntaxException {
        super(rootURI);
        registerSchemeHandler(ASSET, new AssetSchemeHandler(context.getAssets()));
        registerSchemeHandler(FILE, new FileSchemeHandler(context));
    }

    @Override
    public DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters, CacheControl cacheControl, int connectTimeout, int readTimeout) throws DocumentServerException {
        log.info("Android.getDocument hitCount = " + ((hrc == null) ? "NA" : hrc.getHitCount()));
        log.info("CacheControl " + cacheControl + " cachable=" + cacheControl.isCacheable());

        DocumentServerInputStream instream;
        if (ASSET.equals(uri.getScheme())) {
            AssetSchemeHandler schemeHandler = (AssetSchemeHandler) findSchemeHandler(ASSET);
            instream =  schemeHandler.handleScheme(uri, null);
        } else {
            log.info("URI is " + uri);
            instream = super.getDocument(uri, parameters, cacheControl, connectTimeout, readTimeout);
        }
        return instream;
    }
}
