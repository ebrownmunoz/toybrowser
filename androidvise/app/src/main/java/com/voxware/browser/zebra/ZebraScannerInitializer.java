package com.voxware.browser.zebra;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.ProfileManager;
import com.symbol.emdk.VersionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;

/**
 * Created by eric on 3/3/16.
 */
public class ZebraScannerInitializer implements  EMDKListener, Closeable {

    private static final String DEFAULT_PROFILE_NAME = "Voxware";
    private static final String PRELOLLIPOP_PROFILE_NAME = "Voxware_PreLollipop";
    private final Context context;
    private VersionManager versionManager;
    private ProfileManager profileManager;
    private EMDKManager emdkManager;

    private Logger log = LoggerFactory.getLogger(ZebraScannerInitializer.class);

    //Assign the profile name used in EMDKConfig.xml
    private final String PROFILE_NAME;

    public ZebraScannerInitializer(Context context) {
        this.context = context;
        PROFILE_NAME = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? DEFAULT_PROFILE_NAME : PRELOLLIPOP_PROFILE_NAME;
    }

    public EMDKResults initialize () {
        //The EMDKManager object will be created and returned in the callback.
        EMDKResults results = EMDKManager.getEMDKManager(context, this);

        //Check the return status of processProfile
        if(results.statusCode == EMDKResults.STATUS_CODE.SUCCESS) {
            //EMDKManager object creation success
        } else {
            //EMDKManager object creation failed
        }
        return results;
    }

    @Override
    public void close() {
        if (emdkManager != null) {
            EMDKManager emdkManager = this.emdkManager;
            this.emdkManager = null;
            emdkManager.release();
        }
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        this.versionManager = (VersionManager)emdkManager.getInstance(EMDKManager.FEATURE_TYPE.VERSION);
        log.info("EMDK Version: {}", versionManager.getVersion(VersionManager.VERSION_TYPE.EMDK));
        log.info("MX Version: {}", versionManager.getVersion(VersionManager.VERSION_TYPE.MX));
        log.info("Barcode Scanner Framework Version: {}", versionManager.getVersion(VersionManager.VERSION_TYPE.BARCODE));
        String[] emdkVersionSegments = versionManager.getVersion(VersionManager.VERSION_TYPE.EMDK).split("\\.|");

        //Get the ProfileManager object to process the profiles
        this.profileManager = (ProfileManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.PROFILE);
        String[] modifyData = new String[1];
        new ProcessProfileInitTask().execute(modifyData[0]);
    }

    @Override
    public void onClosed() {
        if (emdkManager != null) {
            emdkManager.release();
        }
    }

    private class ProcessProfileInitTask extends AsyncTask<String, Void, EMDKResults> {

        @Override
        protected EMDKResults doInBackground(String... params) {
            //Call processPrfoile with profile name and SET flag to create the profile. The params can be null.
            EMDKResults results = profileManager.processProfile(PROFILE_NAME, ProfileManager.PROFILE_FLAG.SET, params);
            return results;
        }

        @Override
        protected void onPostExecute(EMDKResults results) {
            super.onPostExecute(results);

            String resultString;
            //Check the return status of processProfile
            if(results.statusCode == EMDKResults.STATUS_CODE.SUCCESS) {
                log.info("Profile initialization success.");
            }else {
                log.warn("Profile initialization failed.");
            }
        }
    }
}
