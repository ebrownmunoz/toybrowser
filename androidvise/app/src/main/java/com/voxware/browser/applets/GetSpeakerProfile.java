package com.voxware.browser.applets;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiVise;

/**
 * Created by edh on 12/3/2015.
 */
public class GetSpeakerProfile extends AbstractApplet {

    private final SapiVise sapiVise;
    private final DocumentServer documentServer;

    @Inject
    public GetSpeakerProfile(VISERecognizer recognizer, DocumentServer documentServer) {
        this.sapiVise = recognizer.getSapiVise();
        this.documentServer = documentServer;
    }
    @Override
    public Result run() throws MissingParameterException {
        Scriptable scope = getContext().initStandardObjects();
        String url = require("speaker").asString();
        boolean load = parameter("load", false).asBoolean();
        int timeout = parameter("timeout", 10000).asInteger();

        // Get the image of the speaker profile in a byte[]
        InputStream is = null;
        try {
            URI uri = new URI(url);
            String name = "";
            byte[] bytes = null;
            SpeakerProfile[] speakers = sapiVise.listKnownSpeakers();
            for (SpeakerProfile speaker : speakers) {
                if (name.equals(speaker.getName())) {
                    // we found one
                    bytes = sapiVise.writeVendorSpeakerProfile(name);
                    break;
                }
            }

            if (bytes == null && load) {
                is = documentServer.getDocument(uri, null, CacheControl.NO_CACHE, timeout, timeout);
                bytes = IOUtils.toByteArray(is);
                sapiVise.readVendorSpeakerProfile(name, bytes);
            }

            if (bytes != null) {
                Result ret = Result.Ok();
                Scriptable retScriptable = getContext().newObject(scope);
                retScriptable.put("name", retScriptable, name);
                retScriptable.put("image", retScriptable, bytes);
                ret.setValue(retScriptable);
                return ret;
            } else {
                Result ret = Result.Ok();
                Scriptable retScriptable = getContext().newObject(scope);
                retScriptable.put("name", retScriptable, Undefined.instance);
                retScriptable.put("image", retScriptable, Undefined.instance);
                ret.setValue(retScriptable);
                return ret;
            }
        } catch (URISyntaxException e) {
            return Result.event("error.badfetch.malformedurl", "The url " + url + " is malformed");
        } catch (DocumentServerException e) {
            return Result.event("error.badfetch.ioerror", e.getMessage());
        } catch (IOException e) {
            return Result.event("error.badfetch.ioerror", e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
}
