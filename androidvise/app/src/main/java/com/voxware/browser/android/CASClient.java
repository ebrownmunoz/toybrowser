package com.voxware.browser.android;

import com.voxware.browser.android.service.Browser;
import com.voxware.browser.log.LogService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import voxware.cas.commands.NameValuePairsCommand;
import voxware.cas.commands.SourcedInterruptCommand;
import voxware.util.StringLiteralSupport;
import voxware.vjml.VJML;
import voxware.vjml.VJMLAttributes;
import voxware.vjml.VJMLChannel;
import voxware.vjml.VJMLListener;
import voxware.vjml.VJMLMessage;

/**
 * Created by edh on 12/14/2015.
 */
public class CASClient implements Runnable, LogService, VJMLListener {
    private static final Logger log = LoggerFactory.getLogger(CASClient.class);

    public String CASChannelNamePrefix = "CAS_";
    public int    ReceivePort = 4451;
    public long   CASWaitInterval = 10;   // Milliseconds
    public long   CASMaxWait = 1000;      // Milliseconds

    protected VJML vjml;
    protected VJMLChannel channel = null;

    private Browser browser;

    public CASClient(Browser browser) throws IOException {
        this.browser = browser;
        VJMLAttributes attr = new VJMLAttributes();
        attr.defaultReceivePort(ReceivePort);
        vjml = new VJML(attr);
        log.info("AppServerAccess: instantiated VJML " + vjml.toString());
    }

    /// Runnable Implementation ///

    public void run() {
        // Install the AppServer SysMonitor
        try {
            log.info("AppServerAccess.run: starting up");
            VJMLChannel oldestChannel = null;
            // Wait for the CAS to make contact
            do {
                oldestChannel = vjml.oldestChannel();
                log.debug("Got oldest channel as {}", oldestChannel.name());
            } while (!oldestChannel.name().startsWith(CASChannelNamePrefix));
            channel = oldestChannel;
            log.info("AppServerAccess.run: installed " + this.getClass().getName() + " as AppServer");
            message(oldestChannel);
            // Don't use the VJMLListener mechanism here, since it is too hard to guarantee synchronization
            while (!Thread.interrupted()) {
                oldestChannel = vjml.oldestChannel();
                message(oldestChannel);
            }
        } catch (InterruptedException e) {
            log.error("Caught signal to terminate CAS client");
        } catch (Exception e) {
            log.error("AppServerAccess.run: caught error", e);
        } finally {
            log.warn("AppServerAccess.run: shutting down");
            vjml.shutdown();
            return;
        }
    }

    @Override
    public void send(String text) {

        NameValuePairsCommand nvpCommand = new NameValuePairsCommand(text);
        // Make sure there is a channel to the CAS before attempting to use it
        if (channel == null) {
            int waitCount = 0;
            do {
                try {
                    Thread.sleep(CASWaitInterval);
                } catch (InterruptedException e) {
                    log.error("AppServerAccess.logEvent: unexpectedly caught " + e + " waiting for CAS contact");
                }
            } while (channel == null && (++waitCount * CASWaitInterval) < CASMaxWait);
        }
        if (channel != null) {
            channel.putMessage(new VJMLMessage(nvpCommand, VJMLMessage.HighestPriority));
            channel.sendNow();
        } else {
            log.warn("AppServerAccess.logEvent: WARNING -- timed out waiting for contact from CAS");
        }
    }

    /// VJMLListener Implementation ///

    public void exception(Throwable exception) {
        log.error("AppServerAccess.exception: " + exception.toString());
    }

    public void message(VJMLChannel channel) {
        if (channel.name().startsWith(CASChannelNamePrefix)) {
            VJMLMessage message = null;
            try {
                message = channel.getMessage();
            } catch (InterruptedException ie) {
                log.info("Interrupted", ie);
            }

            Object content = message.content();
            if (content instanceof NameValuePairsCommand) {
                NameValuePairsCommand nvpCommand = (NameValuePairsCommand) content;
                log.info("AppServerAccess.message: \"" + StringLiteralSupport.stringLiteralFromUnicode(nvpCommand.toString()) + "\"");
                // Generate a browser interrupt
                browser.sendInterrupt("appserver", nvpCommand.toString());
            } else if (content instanceof SourcedInterruptCommand) {
                SourcedInterruptCommand siCommand = (SourcedInterruptCommand) content;
                log.info("AppServerAccess.message: \"" + siCommand.toString() + "\"");
                // Generate a browser interrupt
                browser.sendInterrupt("appserver.irq." + siCommand.getSource(), siCommand.getContent().toString());
            } else {
                log.error("AppServerAccess.message ERROR -- unrecognized command type " + content.getClass().getName());
            }
        } else {
            log.error("AppServerAccess.message: ERROR -- invalid channel name \"" + channel.name() + "\"");
        }
    }
}
