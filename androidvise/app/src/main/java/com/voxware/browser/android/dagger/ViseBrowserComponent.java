package com.voxware.browser.android.dagger;

import com.voxware.browser.android.service.Browser;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ehom on 10/14/16.
 */
@Singleton
@Component(modules = {AndroidTextToSpeechModule.class, ViseModule.class, BaseBrowserModule.class})
public interface ViseBrowserComponent {
    Browser getBrowser();
}
