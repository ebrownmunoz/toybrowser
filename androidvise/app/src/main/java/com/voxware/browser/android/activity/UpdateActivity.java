package com.voxware.browser.android.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.voxware.browser.android.R;
import com.voxware.browser.android.deployment.DeploymentManager;
import com.voxware.browser.android.service.deployment.DeploymentService;
import com.voxware.browser.android.utils.VoxBrowserSettings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateActivity extends AppCompatActivity {
    private static Logger log = LoggerFactory.getLogger(UpdateActivity.class);
    private static final String DEPLOYMENT_STARTED = "com.voxware.browser.android.activity.UpdateActivity.DEPLOYMENT_STARTED";
    private static final String STATUS_TEXT = "com.voxware.browser.android.activity.UpdateActivity.STATUS_TEXT";
    private static final String OVERALL_PROGRESS = "com.voxware.browser.android.activity.UpdateActivity.OVERALL_PROGRESS";
    private boolean deploymentStarted = false;
    private LocalBroadcastManager localBroadcastManager;

    private VoxBrowserSettings settings;
    private Handler handler;
    private TextView statusText;
    private ProgressBar overallBar;
    private ProgressBar componentBar;
    private final BroadcastReceiver actionProgressListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DeploymentManager.ACTION_DEPLOYMENT_PROGRESS.equals(intent.getAction())) {
                if (overallBar != null) {
                    String name = intent.getStringExtra(DeploymentManager.EXTRA_DEPLOYMENT_STEP_NAME);
                    int component = intent.getIntExtra(DeploymentManager.EXTRA_DEPLOYMENT_STEP, 1);
                    int totalComponents = intent.getIntExtra(DeploymentManager.EXTRA_TOTAL_DEPLOYMENT_STEPS, 1);
                    handler.post(() -> {
                        statusText.setText("Deploying " + name);
                        overallBar.setMax(totalComponents);
                        overallBar.setProgress(component);
                    });
                }
            } else if (DeploymentManager.ACTION_ACQUISITION_PROGRESS.equals(intent.getAction())) {
                if (componentBar != null) {
                    long bytesAcquired = intent.getLongExtra(DeploymentManager.EXTRA_CURRENT_BYTES, 0);
                    long acquisitionSize = intent.getLongExtra(DeploymentManager.EXTRA_TOTAL_BYTES, 1);
                    int intBytesAcquired;
                    int intAcquisitionSize;
                    if (acquisitionSize > Integer.MAX_VALUE) {
                        int scaleFactor = 33 - Long.numberOfLeadingZeros(acquisitionSize);
                        intBytesAcquired = (int) (bytesAcquired >>> scaleFactor);
                        intAcquisitionSize = (int) (acquisitionSize >>> scaleFactor);
                    } else {
                        intBytesAcquired = (int) bytesAcquired;
                        intAcquisitionSize = (int) acquisitionSize;
                    }
                    handler.post(() -> {
                        componentBar.setMax(intAcquisitionSize);
                        componentBar.setProgress(intBytesAcquired);
                    });
                }
            } else if (DeploymentManager.ACTION_DEPLOYMENT_COMPLETE.equals(intent.getAction())) {
                // we're done
                UpdateActivity.this.setResult(RESULT_OK, new Intent(intent));
                deploymentStarted = false;
                finish();
            } else if (DeploymentManager.ACTION_DEPLOY_APK.equals(intent.getAction())) {

            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        settings = new VoxBrowserSettings(this);
        statusText = (TextView)findViewById(R.id.statusText);
        overallBar = (ProgressBar)findViewById(R.id.overallProgressBar);
        componentBar = (ProgressBar)findViewById(R.id.componentProgressBar);
        handler = new Handler(Looper.getMainLooper());
        if (savedInstanceState != null) {
            deploymentStarted = savedInstanceState.getBoolean(DEPLOYMENT_STARTED, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!deploymentStarted) {
            IntentFilter progressIntentFilter = new IntentFilter();
            progressIntentFilter.addAction(DeploymentManager.ACTION_ACQUISITION_PROGRESS);
            progressIntentFilter.addAction(DeploymentManager.ACTION_DEPLOYMENT_PROGRESS);
            progressIntentFilter.addAction(DeploymentManager.ACTION_DEPLOYMENT_COMPLETE);
            progressIntentFilter.addAction(DeploymentManager.ACTION_DEPLOY_APK);
            localBroadcastManager.registerReceiver(actionProgressListener, progressIntentFilter);
            Intent startDeployment = new Intent(this, DeploymentService.class);
            startService(startDeployment);
            deploymentStarted = true;
        }

        Intent intent = new Intent(DeploymentManager.ACTION_UPDATE_UI_PAUSING);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        localBroadcastManager.unregisterReceiver(actionProgressListener);
        Intent intent = new Intent(DeploymentManager.ACTION_UPDATE_UI_PAUSING);
        localBroadcastManager.sendBroadcast(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(DEPLOYMENT_STARTED, deploymentStarted);
        outState.putString(STATUS_TEXT, statusText.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        statusText.setText(savedInstanceState.getString(STATUS_TEXT));
    }

    public void handleBrowserUpdate() {
        log.warn("The browser needs to be updated");
        new AlertDialog.Builder(this).setMessage("The browser needs to be updated. Please click \"INSTALL\" when prompted. You may then click \"OPEN\" when the installation is complete to automatically relaunch VoxBrowser").setPositiveButton(R.string.ok, null).show();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(actionProgressListener);
        super.onDestroy();
    }
}
