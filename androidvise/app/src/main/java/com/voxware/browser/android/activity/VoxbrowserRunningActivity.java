package com.voxware.browser.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.voxware.browser.android.R;
import com.voxware.browser.android.service.BrowserService;

public class VoxbrowserRunningActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voxbrowser_running);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        startBrowserService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void startBrowserService() {
        Intent browserServiceIntent = new Intent(getApplicationContext(), BrowserService.class);
        browserServiceIntent.putExtra(BrowserService.ACTIVITY_EXTRA, this.getClass());
        startService(browserServiceIntent);
    }
}
