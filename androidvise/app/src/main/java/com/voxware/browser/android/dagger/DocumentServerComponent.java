package com.voxware.browser.android.dagger;

import com.voxware.browser.documentserver.DocumentServer;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by edh on 10/12/2016.
 */
@Singleton
@Component(modules = {DocumentServerModule.class})
public interface DocumentServerComponent {
    DocumentServer getDocumentServer();
}
