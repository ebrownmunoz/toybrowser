package com.voxware.browser.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.net.ConnectivityManagerCompat;

/**
 * Created by edh on 7/26/2016.
 */
public class NetworkObject {
    private final ConnectivityManager connectivityManager;
    private final WifiManager wifiManager;

    public NetworkObject(Context context) {
        connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
    }

    public NetworkInfo getActiveNetworkInfo() {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    public WifiInfo getWifiInfo() {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo;
    }
}
