package com.voxware.browser.android;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.voxware.browser.android.service.Browser;
import com.voxware.browser.model.InterruptNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by edh on 4/19/2016.
 */
public class BatteryObserver {
    private static final Logger log = LoggerFactory.getLogger(BatteryObserver.class);
    public static final String BATTERY_CHARGE = "batterycharge";
    private static final int BATTERY_CHECK_INTERVAL = 10;


    private final Context context;
    private final Browser browser;

    private int currentLevel = -1; // This should only be read or modified from BatteryObserver Thread

    private final ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "BatteryObserver");
        }
    });

    private final ScheduledFuture<?> future;

    public BatteryObserver(Context context, Browser browser) {
        this.context = context;
        this.browser = browser;

        future = executorService.scheduleAtFixedRate(this::checkBattery, 0, BATTERY_CHECK_INTERVAL, TimeUnit.SECONDS);
    }

    private void checkBattery() {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_UNKNOWN);
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);


        log.debug("BatteryObserver Checking Battery: level = " + level + "status = " + status);

        if (level != currentLevel) {
            int maxLevel = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            if (maxLevel == -1) {
                log.error("max level is -1, won't be able to acquire battery information");
                return;
            }
            Double doubleLevel = ((double) level / (double) maxLevel) * 100.0d;
            if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
                browser.sendInterrupt(BATTERY_CHARGE, InterruptNode.FALL_TO, doubleLevel);
            } else if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
                browser.sendInterrupt(BATTERY_CHARGE, InterruptNode.RISE_TO, doubleLevel);
            }

            currentLevel = level;
        }
    }

    public void stop() {
        future.cancel(true);
        executorService.shutdown();
    }
}
