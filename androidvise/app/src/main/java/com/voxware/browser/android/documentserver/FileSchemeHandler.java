package com.voxware.browser.android.documentserver;

import android.content.Context;

import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.DocumentServerInputStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by edh on 3/2/2016.
 */
public class FileSchemeHandler extends com.voxware.browser.documentserver.FileSchemeHandler {
    private List<File> roots = new ArrayList<File>();
    public FileSchemeHandler(Context context) {
        roots.add(context.getFilesDir());
        roots.add(context.getExternalFilesDir(null)); // We no longer need th external directory (left in for debugging)
    }
    /*
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.SchemeHandler#handleScheme(java.net.URI, java.lang.Object)
     */
    @Override
    public DocumentServerInputStream handleScheme(URI uri, Void parameters) throws DocumentServerException {
        File file = null;
        String path = uri.getPath();
        if (path.startsWith("/opt/voxware/")) {
            path = path.substring("/opt/voxware/".length());
            try {
                for (File root : roots) {
                    file = new File(root, path);
                    if (file.exists() && !file.isDirectory()) {
                        return new DocumentServerInputStream(file);
                    }
                }
                throw new FileNotFoundException("No file matches " + uri.getPath());
            } catch (FileNotFoundException e) {
                throw new DocumentServerException("Unable to open file: " + file.getAbsolutePath(), e);
            }
        } else {
            throw new DocumentServerException("Don't know how to deref file: " + uri);
        }
    }
}
