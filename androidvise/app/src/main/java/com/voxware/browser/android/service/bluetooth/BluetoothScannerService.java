package com.voxware.browser.android.service.bluetooth;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.support.annotation.Nullable;

import com.voxware.browser.android.ScannerAdapter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by edh on 3/18/2016.
 */
public class BluetoothScannerService extends Service {
    public static final String LOG_DIR_EXTRA = "com.voxware.browser.android.extra.log_dir";
    public static final UUID SERIAL_PORT_PROFILE_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static final Logger log = LoggerFactory.getLogger("bluetooth");

    private static final String ACTION_DATA_WEDGE_ENUMERATE_SCANNERS = "com.motorolasolutions.emdk.datawedge.api.ACTION_ENUMERATESCANNERS";
    private static final String ACTION_DATA_WEDGE_ENUMERATED_SCANNERS_LIST = "com.motorolasolutions.emdk.datawedge.api.ACTION_ENUMERATEDSCANNERLIST";
    private static final String KEY_ENUMERATED_SCANNER_LIST = "DataWedgeAPI_KEY_ENUMERATEDSCANNERLIST";

    // need to support jellybean, so use the default adapter
    //private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 5, 30, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new ThreadPoolExecutor.AbortPolicy());
    private Map<BluetoothDevice, Future<?>> currentPumpers = Collections.synchronizedMap(new HashMap<BluetoothDevice, Future<?>>());

    /**
     * Dispatch since we don't want to make the onReceive too unwieldy
     */
    private final BroadcastReceiver BLUETOOTH_BOND_STATE_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleDeviceBondEvent(context, intent);
        }
    };

    private boolean registered = false;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //bluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = getBluetoothAdapter();
        registerBroadcastReceiver();
        executor.submit(new ScanCurrentDevicesTask());
        executor.submit(new ListenForConnectionsTask());
        return new Binder();
    }


    @Override
    public boolean onUnbind(Intent intent) {
        unregisterBroadcastReceiver();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Registers all braodcast receivers. Synchronized but probably not necessary since lifecycle methods are called from main UI thread
     */
    protected synchronized void registerBroadcastReceiver() {
        if (!registered) {
            registerReceiver(BLUETOOTH_BOND_STATE_RECEIVER, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
            registered = true;
        }
    }

    /**
     * Unregisters all braodcast receivers. Synchronized but probably not necessary since lifecycle methods are called from main UI thread
     */
    protected synchronized void unregisterBroadcastReceiver() {
        if (registered) {
            this.unregisterReceiver(BLUETOOTH_BOND_STATE_RECEIVER);
            registered = false;
        }
    }

    /**
     * Handles device bond notification. When device pairing is created, check it for serial port profile
     *
     * @param context the context
     * @param intent  the intent
     */
    protected void handleDeviceBondEvent(Context context, Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

        int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1);
        if (bondState == BluetoothDevice.BOND_BONDED) {
            log.info("Device bonded: {} [{}]", device.getName(), device.getAddress());
            if (!isDataWedgeAvailable() || !isDataWedgeHandlingDevice(device)) {
                submitBluetoothSPPTask(device);
            }
        } else if (bondState == BluetoothDevice.BOND_NONE) {
            // existing device disconnected
            log.info("Device unbonded: {} [{}]", device.getName(), device.getAddress());
        }
    }

    protected void submitBluetoothSPPTask(BluetoothDevice device) {
        Future<?> pumper = currentPumpers.get(device);
        if (pumper == null || pumper.isDone()) {
            if (supportsSerialPortProfile(device)) {
                BluetoothSocket socket = null;
                try {
                    socket = device.createInsecureRfcommSocketToServiceRecord(SERIAL_PORT_PROFILE_UUID);
                    socket.connect();
                    currentPumpers.put(device, executor.submit(new MessagePumper(socket)));
                } catch (IOException e) {
                    log.error("Error creating socket", e);
                    IOUtils.closeQuietly(socket);
                }
            } else {
                log.info("Serial Port Profile not supported, not connecting to {}", device.getAddress());
            }
        }
    }

    protected void submitBluetoothSPPTask(BluetoothSocket socket) {
        BluetoothDevice device = socket.getRemoteDevice();
        Future<?> pumper = currentPumpers.get(device);
        if (pumper == null || pumper.isDone()) {
            currentPumpers.put(device, executor.submit(new MessagePumper(socket)));
        }
    }

    private BluetoothAdapter getBluetoothAdapter() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                return viaService();
            }
        } catch (NoSuchFieldError e) {
            // fall through
        } catch (Exception e) {
            // fall through
        }
        return BluetoothAdapter.getDefaultAdapter();
    }

    @TargetApi(18)
    private BluetoothAdapter viaService() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> bluetoothManagerServiceClass = Class.forName("android.bluetooth.BluetoothManager");
        Object bluetoothManagerService = getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManagerService != null && bluetoothManagerServiceClass.isAssignableFrom(bluetoothManagerService.getClass())) {
            Method getAdapterMethod = bluetoothManagerServiceClass.getMethod("getAdapter", new Class<?>[0]);
            return (BluetoothAdapter) getAdapterMethod.invoke(bluetoothManagerService);
        } else {
            throw new NullPointerException("BLUETOOTH_SERVICE does not exist");
        }
    }

    private boolean supportsSerialPortProfile(BluetoothDevice device) {
        ParcelUuid[] uuids = device.getUuids();
        for (ParcelUuid uuid : uuids) {
            if (uuid.getUuid().equals(SERIAL_PORT_PROFILE_UUID)) {
                return true;
            }
        }
        return false;
    }

    private boolean isDataWedgeAvailable() {
        Intent intent = new Intent(ACTION_DATA_WEDGE_ENUMERATE_SCANNERS);
        PackageManager manager = getPackageManager();
        List<ResolveInfo> list = manager.queryIntentActivities(intent, 0);

        if (list != null && list.size() > 0) {
            //You have at least one activity to handle the intent
            return true;
        } else {
            return false;
        }
    }

    private boolean isDataWedgeHandlingDevice(BluetoothDevice device) {
        Intent intent = new Intent(ACTION_DATA_WEDGE_ENUMERATE_SCANNERS);
        IntentFilter filter = new IntentFilter(ACTION_DATA_WEDGE_ENUMERATED_SCANNERS_LIST);
        final List<String> attachedScanners = new LinkedList<>();
        final Semaphore semaphore = new Semaphore(0);
        final BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle b = intent.getExtras();
                String[] scanner_list = b.getStringArray(KEY_ENUMERATED_SCANNER_LIST);
                attachedScanners.addAll(Arrays.asList(scanner_list));
                semaphore.release();
            }
        };
        registerReceiver(receiver, filter);
        sendBroadcast(intent);
        // now wait for the message
        try {
            if (!semaphore.tryAcquire(30, TimeUnit.SECONDS)) {
                log.warn("DataWedge did not respond within the 30s timeout, assuming it is NOT handling {}", device.getAddress());
                return false;
            }
            if (attachedScanners.contains(device.getName())) {
                return true;
            }
        } catch (InterruptedException e) {
            log.warn("Interrupted waiting for DataWedge response");
        } finally {
            unregisterReceiver(receiver);
        }
        return false;
    }

    protected class MessagePumper implements Runnable {
        private final BluetoothDevice device;
        private final BluetoothSocket socket;
        private InputStream inputStream;
        private InputStreamReader inputStreamReader;
        private BufferedReader bufferedReader;

        public MessagePumper(BluetoothSocket socket) {
            this.socket = socket;
            this.device = socket.getRemoteDevice();
            try {
                inputStream = socket.getInputStream();
                inputStreamReader = new InputStreamReader(inputStream);
                bufferedReader = new BufferedReader(inputStreamReader);
            } catch (IOException e) {
                log.error("Error opening input stream", e);
            }
        }

        public BluetoothDevice getDevice() {
            return device;
        }

        public void run() {
            String threadName = Thread.currentThread().getName();
            try {
                Thread.currentThread().setName("BluetoothHandler " + device.getName() + " [" + device.getAddress() + "]");
                try {
                    String s = readMessage();
                    while (s != null) {
                        log.info("Read {}", s);
                        Intent broadcastIntent = new Intent(ScannerAdapter.ACTION);
                        broadcastIntent.putExtra(ScannerAdapter.DATA_STRING_TAG, s);
                        sendOrderedBroadcast(broadcastIntent, "com.symbol.emdk.permission.EMDK");
                        s = readMessage();
                    }
                } catch (Exception e) {
                    log.error("Caught exception", e);
                } finally {
                    IOUtils.closeQuietly(bufferedReader);
                    IOUtils.closeQuietly(inputStreamReader);
                    IOUtils.closeQuietly(inputStream);
                    IOUtils.closeQuietly(socket);
                    currentPumpers.remove(device);
                }
            } finally{
                Thread.currentThread().setName(threadName);
            }
        }

        protected String readMessage() {
            if (bufferedReader == null) {
                return null;
            }
            try {
                return bufferedReader.readLine();
            } catch (IOException e) {
                log.error("Caught IOException", e);
                return null;
            }
        }
    }

    protected class ScanCurrentDevicesTask implements Runnable {

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            try {
                Thread.currentThread().setName("Bluetooth Device Scanner");
                Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
                for (BluetoothDevice device : bondedDevices) {
                    ParcelUuid[] uuids = device.getUuids();
                    for (ParcelUuid uuid : uuids) {
                        if (uuid.getUuid().equals(SERIAL_PORT_PROFILE_UUID)) {
                            submitBluetoothSPPTask(device);
                        }
                    }
                }
            } finally {
                Thread.currentThread().setName(threadName);
            }
        }
    }

    protected class ListenForConnectionsTask implements Runnable {
        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            try {
                Thread.currentThread().setName("Bluetooth Server Socket Listener");
                BluetoothServerSocket serverSocket = null;
                try {
                    serverSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("Voxware Serial Port Server", SERIAL_PORT_PROFILE_UUID);
                } catch (IOException e) {
                    log.error("Error creating listener socket", e);
                }
                try {
                    while (!Thread.interrupted()) {
                        try {
                            BluetoothSocket socket = serverSocket.accept(5000);
                            log.info("Connected by {}, [{}]", socket.getRemoteDevice().getName(), socket.getRemoteDevice().getAddress());
                            submitBluetoothSPPTask(socket.getRemoteDevice());
                        } catch (IOException e) {
                            if (!e.getMessage().equals("Try again")) {
                                log.error("Error occurred accepting socket");
                                return;
                            }
                        }
                    }
                } finally {
                    IOUtils.closeQuietly(serverSocket);
                }
            } finally {
                Thread.currentThread().setName(threadName);
            }
        }
    }
}
