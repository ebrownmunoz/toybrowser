package com.voxware.browser.android.audio;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioTrack;

import com.voxware.browser.audioplayer.AudioPlayerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ThreadFactory;

import javax.inject.Inject;

/**
 * Created by eric on 1/29/16.
 */
public class ByteArrayPlayer extends AbstractAndroidPlayer<byte[]> implements com.voxware.browser.io.ByteArrayPlayer {

    private static final Logger log = LoggerFactory.getLogger(ByteArrayPlayer.class);

    @Inject
    public ByteArrayPlayer(Context context) {
        super(context);
    }

    @Override
    protected ThreadFactory createThreadFactory() {
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r,  "ByteArrayPlayer");
            }
        };
    }

    @Override
    protected Callable<Void> createCallable(byte[] playable) {
        return new Track(playable, 11025);
    }

    /**
     * Represents a track to be played. The AudioTrack object is not created until the call method is invoked to avoid needing cleanup if the track is never played.
     */
    protected class Track implements Callable<Void> {
        private AudioTrack audioTrack;
        private final short[] shorts;
        private final int frequency;
        /**
         * content length in ms, rounded upwards to nearest integer
         */
        private final int contentLengthInMillis;
        private final int totalFrames;
        private int totalFramesPlayed = 0;

        public Track(byte[] content, int frequency) {
            this.frequency = frequency;
            shorts = new short[content.length / 2];
            ByteBuffer.wrap(content).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            totalFrames = shorts.length;
            contentLengthInMillis = calculateLengthInMillis(totalFrames);
        }

        private int calculateLengthInMillis(int frames) {
            return ((frames * 1000) / frequency) + (frames % frequency == 0 ? 0 : 1);
        }

        public Void call() throws Exception {
            int framesWritten = 0;
            audioTrack = new AudioTrack(getStreamId(), frequency, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
                    shorts.length * 2, AudioTrack.MODE_STATIC);

            try {
                while (framesWritten != totalFrames) {
                    int writeResult = audioTrack.write(shorts, framesWritten, totalFrames - framesWritten);
                    String errorMsg = null;
                    if (writeResult < 0) {
                        errorMsg = String.format("Error writing shorts to audio track: %d", writeResult);
                        log.error(errorMsg);
                        throw new AudioPlayerException(errorMsg);
                    } else if (writeResult > 0){
                        framesWritten += writeResult;
                    } else {
                        log.warn("Wrote 0 frames to the audio track");
                    }
                }
                log.info("Rendering audio samples");
                audioTrack.play();
                waitForCompletion();
            } catch (InterruptedException e) {
                // we must have been cancelled?
                throw new CancellationException();
            } finally {
                log.info("Rendering complete");
                audioTrack.stop();
                audioTrack.release();
            }
            return null;
        }

        protected void waitForCompletion() throws InterruptedException {
            final long start = System.currentTimeMillis();
            final long estimatedEnd = start + contentLengthInMillis;
            int oldCurrentPosition = 0;
            int currentPosition = audioTrack.getPlaybackHeadPosition();
            int noFramesCount = 0;
            while (!Thread.currentThread().isInterrupted() && audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING && currentPosition < totalFrames) {
                if (oldCurrentPosition == currentPosition) {
                    final long now = System.currentTimeMillis();
                    if (now <= estimatedEnd) {
                        noFramesCount++;
                    } else if ((noFramesCount > 3) || (now > (estimatedEnd + 1000))) {
                        log.warn("Playback is stalled, frame counter hasn't moved " + noFramesCount + " times");
                        return;
                    } else {
                        log.warn("Playback is stalled, frame counter hasn't moved " + noFramesCount + " times");
                    }
                }
                oldCurrentPosition = currentPosition;
                currentPosition = audioTrack.getPlaybackHeadPosition();
                int framesRemaining = totalFrames - currentPosition;
                // divide by framesPerMillis, add 1ms means waiting just a little longer
                int millisLeft = calculateLengthInMillis(framesRemaining);
                // precision is an issue, hence the loop
                Thread.sleep(millisLeft);
            }
        }

    } // track
}
