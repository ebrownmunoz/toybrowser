package com.voxware.browser.android.audio;

import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.LocalBroadcastManager;

import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.io.StringPlayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

/**
 * Created by edh on 10/29/2015.
 */
public class DefaultAndroidStringPlayer extends AbstractAndroidPlayer<String> implements StringPlayer {
    public static final String ACTION_SPEAK_START = "com.voxware.browser.android.action.SPEAK_START";
    public static final String ACTION_SPEAK_STOP = "com.voxware.browser.android.action.SPEAK_STOP";
    public static final String ACTION_SPEAK_ERROR = "com.voxware.browser.android.action.SPEAK_ERROR";
    public static final String EXTRA_TTS_TEXT = "com.voxware.browser.android.extra.TTS_TEXT";
    public static final String EXTRA_TTS_ERROR = "com.voxware.browser.android.extra.TTS_ERROR";

    private static final Logger log = LoggerFactory.getLogger(DefaultAndroidStringPlayer.class);

    private TextToSpeech textToSpeech;

    private final AtomicInteger count = new AtomicInteger(0);

    private float pitch = 1.0f;
    private float speechRate = 1.0f;
    private float volume = 1.0f;

    public DefaultAndroidStringPlayer(Context context) throws AudioPlayerException {
        super(context);
        getExecutorService().submit(new InitRequest());
        try {
            Future<TextToSpeech> initTask = getExecutorService().submit(new InitRequest());
            textToSpeech = initTask.get();
        } catch (InterruptedException e) {
            throw new AudioPlayerException("Failed to initialize engine, was interrupted", e);
        } catch (ExecutionException e) {
            throw new AudioPlayerException("Failed to initialize engine", e);
        }
    }

    protected ThreadFactory createThreadFactory() {
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "DefaultAndroidStringPlayer Thread");
            }
        };
    }

    @Override
    public synchronized void cancel() {
        textToSpeech.stop();
        super.cancel();
    }

    @Override
    public void setParameter(String name, Object value) {
        if (name.equals("tts.pitch")) {
            pitch = Float.parseFloat(value.toString());
        } else if (name.equals("tts.speed")) {
            speechRate = Float.parseFloat(value.toString());
        } else if (name.equals("tts.volume")) {
            volume = Float.parseFloat(value.toString());
        }
    }

    @Override
    public Object getParameter(String name) {
        if (name.equals("tts.pitch")) {
            return new Float(pitch);
        } else if (name.equals("tts.speed")) {
            return new Float(speechRate);
        } else if (name.equals("tts.volume")) {
            return new Float(volume);
        }
        return null;
    }

    @Override
    public void shutdown() {
        super.shutdown();
        textToSpeech.shutdown();
    }

    @Override
    protected Callable<Void> createCallable(String playable) {
        String id = Integer.toString(count.getAndIncrement());
        return new TtsRequest(id, playable, this.pitch, this.speechRate, this.volume);
    }

    protected class InitRequest implements Callable<TextToSpeech> {
        public TextToSpeech call() throws AudioPlayerException {
            final CountDownLatch ttsWait = new CountDownLatch(1);
            final AtomicBoolean ttsOkay = new AtomicBoolean(false);

            TextToSpeech tts = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        ttsOkay.set(true);
                    }
                    ttsWait.countDown();
                }
            });
            try {
                ttsWait.await(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                log.warn("Caught InterruptedException", e);
            }
            if (ttsOkay.get()) {
                return tts;
            } else {
                throw new AudioPlayerException("Failed to initialize TextToSpeech after 30s");
            }
        }
    }

    protected class TtsRequest implements Callable<Void> {
        private final String id;
        private final String utterance;
        private final float pitch;
        private final float speechRate;
        private final float volume;
        private final CountDownLatch latch = new CountDownLatch(1);
        private final AtomicInteger status = new AtomicInteger(0);
        private final UtteranceProgressListener utteranceProgressListener = new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                TtsRequest.this.onStart(utteranceId);
            }

            @Override
            public void onDone(String utteranceId) {
                TtsRequest.this.onDone(utteranceId);
            }

            @Override
            public void onError(String utteranceId) {
                TtsRequest.this.onError(utteranceId);
            }
        };

        public TtsRequest(final String id, final String utterance, final float pitch, final float speechRate, final float volume) {
            this.id = id;
            this.utterance = utterance;
            this.pitch = pitch;
            this.speechRate = speechRate;
            this.volume = volume;
        }

        public Void call() throws Exception {
            // sending an empty string to TextToSpeech has weird behavior (no callbacks get invoked, seems to hang)
            String ttsUtterance = utterance == null ? null : fixup(utterance);
            if (ttsUtterance == null || ttsUtterance.isEmpty()) {
                onStart(id);
                onDone(id);
                return null;
            }

            try {
                log.info("Sending \"{}\" to TTS engine with (id, pitch, rate, volume) = ({}, {}, {}, {})", ttsUtterance, id, pitch, speechRate, volume);
                //textToSpeech.speak(output, TextToSpeech.QUEUE_FLUSH, Bundle.EMPTY, output);
                HashMap<String, String> args = new HashMap<String, String>();
                args.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, id);
                args.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(getStreamId()));
                args.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, Float.toString(volume));
                int result = textToSpeech.setOnUtteranceProgressListener(utteranceProgressListener);
                if (result != TextToSpeech.SUCCESS) {
                    throw new TtsException("Failed to register listener: " + result);
                }
                result = textToSpeech.setPitch(pitch);
                if (result != TextToSpeech.SUCCESS) {
                    throw new TtsException("Failed to set pitch: " + result);
                }
                result = textToSpeech.setSpeechRate(speechRate);
                if (result != TextToSpeech.SUCCESS) {
                    throw new TtsException("Failed to set rate: " + result);
                }
                result = textToSpeech.speak(ttsUtterance, TextToSpeech.QUEUE_ADD, args);
                if (result != TextToSpeech.SUCCESS) {
                    throw new TtsException("Failed to queue \"" + utterance + "\": " + result);
                }
                // now we wait till we are notified of completion, or we get interrupted (cancelled)
                latch.await();
                if (status.intValue() == 0) {
                    return null;
                } else {
                    throw new TtsException("Error: " + status.intValue());
                }
            } catch (InterruptedException e) {
                log.info("Playback of {}\"{}\" cancelled", id, utterance);
                textToSpeech.stop();
                throw new CancellationException("Playback of \"" + utterance + "\" cancelled");
            }
        }

        private String fixup(final String utterance) {
            String oldUtterance = utterance;
            String newUtterance = utterance.replaceAll("( ' )|( '$)|(^' )", " ");
            while (!newUtterance.equals(oldUtterance)) {
                // changes happened, keep trying until no changes
                oldUtterance = newUtterance;
                newUtterance = oldUtterance.replaceAll("( ' )|( '$)|(^' )", " ");
            }
            return newUtterance.trim().toLowerCase();
        }


        public void onStart(String utteranceId) {
            log.debug("Started playing {}[{}]", utteranceId, utterance);
            sendNotification(ACTION_SPEAK_START);
        }

        public void onDone(String utteranceId) {
            log.debug("Finished playing {}[{}]", utteranceId, utterance);
            sendNotification(ACTION_SPEAK_STOP);
            latch.countDown();
        }

        public void onError(String utteranceId) {
            // we use a generic error
            onError(utteranceId, -1);
        }

        public void onError(String utteranceId, int errorCode) {
            log.debug("Error playing {}[{}]: {}", utteranceId, utterance, errorCode);
            sendNotification(ACTION_SPEAK_ERROR, errorCode);
            status.set(errorCode);
            latch.countDown();
        }

        protected void sendNotification(String action) {
            Intent intent = new Intent();
            intent.setAction(action);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        }

        protected void sendNotification(String action, int errorCode) {
            Intent intent = new Intent();
            intent.setAction(action);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            intent.putExtra(EXTRA_TTS_ERROR, errorCode);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        }
    }
}
