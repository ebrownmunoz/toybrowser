package com.voxware.browser.applets;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import javax.inject.Inject;
import javax.speech.recognition.FinalResult;
import javax.speech.recognition.ResultStateError;
import javax.speech.recognition.SpeakerManager;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiViseGrammar;
import voxware.engine.recognition.sapivise.ViseResult;

/**
 * Created by edh on 12/2/2015.
 */
public class TrainUtterances extends AbstractApplet {

    private static Logger log = LoggerFactory.getLogger(TrainingPrompts.class);

    private final VISERecognizer recognizer;
    private final DocumentServer documentServer;

    @Inject
    public TrainUtterances(VISERecognizer recognizer, DocumentServer documentServer) {
        this.recognizer = recognizer;
        this.documentServer = documentServer;
    }


    @Override
    public Result run() throws MissingParameterException, IOException {
        Scriptable scope = getContext().initStandardObjects(null);
        String grammarURL = parameter("grammar").asString();
        Object[] results = require("results").asObjectArray();
        String[] words = parameter("words").asStringArray();
        long score = parameter("score", Long.MAX_VALUE).asLong();
        int passCount = parameter("passcount", Integer.MAX_VALUE).asInteger();
        int trainCount = parameter("traincount", 1).asInteger();
        boolean logSummary = parameter("log", true).asBoolean();
        boolean punt = parameter("punt", false).asBoolean();

        // Interpret the parameters
        if (grammarURL.isEmpty())
            return Result.event("error.applet.missingparameter", "\"grammar\" parameter of TrainUtterances applet empty");
        if (score < 0)
            return Result.event("error.applet.illegalparameter", "\"score\" parameter of TrainUtterances (" + Long.toString(score) + ") is less than zero");
        if (passCount < 0)
            return Result.event("error.applet.illegalparameter", "\"passcount\" parameter of TrainUtterances (" + passCount + ") is not greater than zero");
        if (trainCount <= 0)
            return Result.event("error.applet.illegalparameter", "\"traincount\" parameter of TrainUtterances (" + trainCount + ") is not greater than zero");

        InputStream is = null;
        byte[] bytes = null;
        Map<Object, Object> cache = getSessionContext().getCache();
        if (cache.containsKey(grammarURL)) {
            bytes = (byte[]) cache.get(grammarURL);
            recognizer.loadGrammar(grammarURL, bytes, false);
        } else {
            try {
                is = documentServer.getDocument(new URI(grammarURL), null, CacheControl.NO_CACHE);
                bytes = IOUtils.toByteArray(is);
                recognizer.loadGrammar(grammarURL, bytes, true);
                cache.put(grammarURL, bytes);
            } catch (URISyntaxException e) {
                return Result.event("error.badfetch.malformedurl", "The url " + grammarURL + " is malformed");
            } catch (IOException e) {
                return Result.event("error.badfetch.ioerror", e.getMessage());
            } catch (DocumentServerException e) {
                return Result.event("error.badfetch.ioerror", e.getMessage());
            } finally {
                IOUtils.closeQuietly(is);
            }

        }
        // Get the SapiViseGrammar
        javax.speech.recognition.Grammar grammar = recognizer.getCurrentGrammar();

        if (!(grammar instanceof SapiViseGrammar))
            return Result.event("error.badgrammar", "TrainUtterances: \"" + grammarURL + "\" is a " + grammar.getClass().getName() + ", not a SapiViseGrammar");
        SapiViseGrammar viseGrammar = (SapiViseGrammar) grammar;

        // End the current training pass
        if (!viseGrammar.endTrainingPass())
            return Result.event("error.applet.recognition.training", "TrainUtterances cannot end the initial training pass for grammar \"" + grammarURL + "\"");

        if (logSummary) {
            log.info("======= SUMMARY OF TRAINING PASS 0 =======");
            log.info("       Total Utterances: " + results.length);
            log.info("            Total Score: " + score);
            log.info("========= END OF TRAINING PASS 0 =========");
        }

        // Iterate over the results until the passCount is exhausted or the total score begins to increase
        int pass = 0;

        while (pass < passCount) {

            long previousScore = score;
            int index;
            int phraseCount = 0;

            score = 0;
            pass++;

            if (!viseGrammar.startTrainingPass())
                return Result.event("error.applet.recognition.training", "TrainUtterances cannot start training pass " + pass + " for grammar \"" + grammarURL + "\"");
            else if (logSummary)
                log.info("======== STARTING TRAINING PASS " + pass + " ========");

            // RECOGNITION LOOP ON RESULTS collected on the first pass of BATCH TRAINING
            for (index = 0; index < results.length; index++) {
                if (results[index] != null) {
                    try {
                        ViseResult result = (ViseResult) results[index];
                        result.tokenCorrection(result.correctTokens(), null, null, FinalResult.MISRECOGNITION);
                    } catch (ResultStateError e) {
                        if (logSummary)
                            log.info("TrainUtterances: result[" + index + "] does not support correction");
                        // Optionally throw a VXMLEvent if the Result does not support tokenCorrection() and correctTokens()
                        if (punt)
                            return Result.event("error.applet.recognition.training", "TrainUtterances: result[" + index + "] does not support correction");
                        results[index] = null;
                        continue;
                    } catch (ClassCastException e) {
                        if (logSummary)
                            log.info("TrainUtterances: result[" + index + "] is not a ViseResult");
                        // Optionally throw a VXMLEvent if the Result is not a ViseResult
                        if (punt)
                            return Result.event("error.applet.recognition.training", "TrainUtterances: result[" + index + "] is not a ViseResult");
                        results[index] = null;
                        continue;
                    } catch (IllegalArgumentException e) {
                        return Result.event("error.applet.recognition.training", "TrainUtterances: result[" + index + "]");
                    }
                }
            }
            // Compute new templates. This synchronized command makes sure that all corrections took place
            if (!viseGrammar.endTrainingPass(words))
                return Result.event("error.applet.recognition.training", "TrainUtterances cannot end training pass " + pass + " for grammar \"" + grammarURL + "\"");

            // Now sum the path scores of all the results
            for (index = 0; index < results.length; index++) {
                if (results[index] != null) {
                    try {
                        ViseResult result = (ViseResult) results[index];
                        score += result.pathScore();
                        phraseCount++;
                    } catch (ResultStateError e) {
                        if (logSummary)
                            log.info("TrainUtterances: result[" + index + "] does not support pathscore()");
                        // Optionally throw a VXMLEvent if the Result does not support pathScore()
                        if (punt)
                            return Result.event("error.applet.recognition.training", "TrainUtterances: result[" + index + "] does not support pathscore()");
                        results[index] = null;
                        continue;
                    } catch (ClassCastException e) {
                        if (logSummary)
                            log.info("TrainUtterances: result[" + index + "] it is not a ViseResult");
                        // Optionally throw a VXMLEvent if the Result is not a ViseResult
                        if (punt)
                            return Result.event("error.applet.recognition.training", "TrainUtterances: result[" + index + "] is not a ViseResult");
                        results[index] = null;
                        continue;
                    }
                }
            }

            if (logSummary) {
                log.info("======= SUMMARY OF TRAINING PASS " + pass + " =======");
                log.info("  Total Usable Utterances: " + phraseCount);
                log.info("              Total Score: " + score);
                log.info("              Delta Score: " + (score - previousScore));
                log.info("========= END OF TRAINING PASS " + pass + " =========");
            }

            // Decide whether we're finished
            if (score >= previousScore) break;
        }

        // Tighten the min/max-dwells in word models
        if (!viseGrammar.completeTraining(words))
            return Result.event("error.applet.recognition.training", "TrainUtterances failed to complete Training");
        else if (!viseGrammar.saveModels(words, trainCount))
            return Result.event("error.applet.recognition.training", "TrainUtterances failed to save word models");

        // Get the image of the current speaker profile in a byte[]
        SpeakerManager speakerManager = recognizer.getSapiVise().getSpeakerManager();
        SpeakerProfile profile = speakerManager.getCurrentSpeaker();
        if (profile != null) {
            ByteArrayOutputStream profileStream = new ByteArrayOutputStream();
            speakerManager.writeVendorSpeakerProfile(profileStream, profile); // Throws SecurityException, MalformedURLException, IOException
            Scriptable retScriptable = getContext().newObject(scope);
            retScriptable.put("name", retScriptable, profile.getName());
            retScriptable.put("image", retScriptable, getContext().javaToJS(profileStream.toByteArray(), scope));
            Result ret = Result.Ok();
            ret.setValue(retScriptable);
            return ret;
        } else {
            return Result.event("error.vise.speaker.nospeaker", "TrainUtterances cannot get the current speaker profile from the SpeakerManager");
        }
    }
}
