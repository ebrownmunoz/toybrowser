package com.voxware.browser.android.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.voxware.browser.android.R;
import com.voxware.browser.android.audio.DefaultAndroidStringPlayer;
import com.voxware.browser.android.service.BrowserService;
import com.voxware.browser.android.service.BrowserServiceClient;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.voxware.browser.android.dagger.ViseModule.log;

/**
 * A placeholder fragment containing a simple view.
 */
public class VoxbrowserRunningActivityFragment extends Fragment {

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        }
    };

    private BroadcastReceiver browserStoppedReceiver;

    private BrowserServiceClient client;
    private BrowserService.BrowserServiceBinder browserServiceBinder;
    private ProgressBar progressBar;
    private ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
    private Semaphore vuStateLock = new Semaphore(1);
    private VUPoller vuPoller;
    private ScheduledFuture vuPollerFuture;
    private ProcessRestarter processRestarter;
    private boolean stopClicked;

    public VoxbrowserRunningActivityFragment() {
        executor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
        executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
    }

    /*
     * The 'Stop' button on the screen will cleanly terminate the browser task.
     */
    private Button stopButton;
    private final View.OnClickListener stopButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!stopClicked) {
                stopClicked = true;
                pauseVUPoller();
                client.close();
                getActivity().stopService(new Intent(getActivity(), BrowserService.class));
            }
        }
    };


    /*
     * The 'Sample Noise' button currently just shows a message.  It is disabled in CE because of a bug; if it does not
     * work in Android either, could be disabled or removed here too.
     */
    private Button sampleNoiseButton;
    private final View.OnClickListener sampleNoiseButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CharSequence samplingNoise = "Sampling noise, please wait...";
            Toast toast = Toast.makeText(getActivity().getApplicationContext(), samplingNoise, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 35);
            toast.show();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // we set up the broadcast receiver here because we never want to lose a message
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter(DefaultAndroidStringPlayer.ACTION_SPEAK_START));
        processRestarter = new ProcessRestarter();

        browserStoppedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                processRestarter.restart();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(browserStoppedReceiver, new IntentFilter(BrowserService.BROWSER_STOPPED));

        View v = inflater.inflate(R.layout.fragment_voxbrowser_running, container, false);


        stopButton = (Button) v.findViewById(R.id.stopButton);
        stopButton.setOnClickListener(stopButtonClickListener);

        sampleNoiseButton = (Button) v.findViewById(R.id.sampleNoiseButton);
        sampleNoiseButton.setOnClickListener(sampleNoiseButtonClickListener);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (client == null) {
            client = new BrowserServiceClient(getActivity(), () -> {
                vuStateLock.acquireUninterruptibly();
                // schedule the creation of the VUPoller object immediately
                executor.execute(() -> {
                    try {
                        vuPoller = new VUPoller(client);
                        vuPollerFuture = executor.scheduleAtFixedRate(vuPoller, 0, 100, TimeUnit.MILLISECONDS);
                    } finally {
                        vuStateLock.release();
                    }
                });

            });
        }
        // we need to resume the VU poller if applicable
        resumeVUPoller();
    }

    @Override
    public void onResume() {
        if (!stopClicked) {
            resumeVUPoller();
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        // we need to pause the VU poller
        pauseVUPoller();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        client.close();
        executor.shutdown();
        super.onDestroy();
    }

    protected synchronized void resumeVUPoller() {
        vuStateLock.acquireUninterruptibly();
        try {
            if (vuPoller != null) {
                if (vuPollerFuture != null) {
                    vuPollerFuture.cancel(true);
                }
                vuPollerFuture = executor.scheduleAtFixedRate(vuPoller, 0, 100, TimeUnit.MILLISECONDS);
            }
        } finally {
            vuStateLock.release();
        }

    }

    protected synchronized void pauseVUPoller() {
        vuStateLock.acquireUninterruptibly();
        try {
            if (vuPollerFuture != null) {
                vuPollerFuture.cancel(true);
                vuPollerFuture = null;
            }
        } finally {
            vuStateLock.release();
        }
    }

    private class VUPoller implements Runnable {

        private final BrowserServiceClient client;

        public VUPoller(BrowserServiceClient client) {
            this.client = client;
        }

        @Override
        public void run() {
            int power = 0;
            if (client != null) {
                try {
                    power = client.getVU();
                } catch (Exception e) {
                    log.error("Error retrieving VU", e);
                }
            }
            final int fpower = power;
            getActivity().runOnUiThread(() -> {
                progressBar.setProgress(fpower >> 9);
            });
        }
    }

    private class ProcessRestarter {
        private final PendingIntent restartIntent;
        private final AlarmManager alarmManager;

        public ProcessRestarter() {
            restartIntent = PendingIntent.getActivity(getActivity(), 123456, new Intent(getActivity(), VoxbrowserStartActivity.class), PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        }

        public void restart() {
            alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 200, restartIntent);
            System.exit(0);
        }
    }

}
