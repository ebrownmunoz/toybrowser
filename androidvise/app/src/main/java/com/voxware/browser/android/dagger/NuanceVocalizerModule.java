package com.voxware.browser.android.dagger;

import android.content.Context;

import com.voxware.browser.android.audio.AndroidAudioPlayer;
import com.voxware.browser.android.audio.NuanceVocalizerStringPlayer;
import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.io.StringPlayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by edh on 10/13/2016.
 */
@Module(includes = {AndroidModule.class, StreamPlayerModule.class})
public class NuanceVocalizerModule {
    private static final Logger log = LoggerFactory.getLogger(NuanceVocalizerModule.class);
    @Provides
    public AudioPlayer providesAudioPlayer(AndroidAudioPlayer androidAudioPlayer) {
        return androidAudioPlayer;
    }

    @Provides
    public StringPlayer providesNuanceVocalizerStringPlayer(Context context) {
        NuanceVocalizerStringPlayer player = null;
        try {
            player = new NuanceVocalizerStringPlayer(context);
        } catch (AudioPlayerException e) {
            log.error("Failed to initialize NuanceVocalizerStringPlayer", e);
        }
        return player;
    }
}
