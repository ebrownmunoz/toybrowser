package com.voxware.browser.android.audio;

import com.voxware.browser.audioplayer.PromptResolver;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.inject.Inject;

/**
 * Created by edh on 2/26/2016.
 */
public class AndroidFragmentProviderFactory implements PromptResolver.FragmentProviderFactory {
    private static final Logger log = LoggerFactory.getLogger(AndroidFragmentProviderFactory.class);

    private final DocumentServer documentServer;
    private final Map<String, PromptResolver.FragmentProvider> fragmentProviders = new HashMap<String, PromptResolver.FragmentProvider>();

    @Inject
    public AndroidFragmentProviderFactory(DocumentServer documentServer) {
        this.documentServer = documentServer;

    }

    @Override
    public PromptResolver.FragmentProvider getFragmentProvider(String source) {
        if (fragmentProviders.containsKey(source)) {
            return fragmentProviders.get(source);
        } else {
            InputStream is = null;
            try {
                is = documentServer.getDocument(new URI(source), null, CacheControl.FOREVER_CACHE);
                PromptResolver.FragmentProvider fragmentProvider = new AndroidFragmentProvider(is);
                fragmentProviders.put(source, fragmentProvider);
                return fragmentProvider;
            } catch (URISyntaxException e) {
                log.error("Bad URI: " + source, e);
                return null;
            } catch (DocumentServerException e) {
                log.error("Error retrieving " + source, e);
                return null;
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }

    private class AndroidFragmentProvider implements PromptResolver.FragmentProvider {
        private final Map<String, byte[]> fragments = new LinkedHashMap<String, byte[]>();

        public AndroidFragmentProvider(InputStream inputStream) {
            BufferedInputStream bis = null;
            ZipInputStream zis = null;
            ZipEntry entry;
            List<String> fragmentList = null;
            try {
                bis = new BufferedInputStream(inputStream);
                zis = new ZipInputStream(bis);
                while ((entry = zis.getNextEntry()) != null) {
                    if (!entry.isDirectory() && entry.getName().toUpperCase().endsWith(".WAV")) {
                        String filePath = entry.getName();
                        int lastPathSeparator = filePath.lastIndexOf("/");
                        String fileName = entry.getName().substring(lastPathSeparator >= 0 ?  lastPathSeparator + 1 : 0, filePath.length() - 4);

                        int count = 0;
                        ByteArrayOutputStream baos = new ByteArrayOutputStream((int)entry.getSize());
                        byte[] buffer = new byte[(int)entry.getSize()];
                        while ((count = zis.read(buffer)) != -1) {
                            baos.write(buffer, 0, count);
                        }

                        // this wouldn't be necessary if we left the spaces in the filename instead of making them +
                        fileName = fileName.replace('+', ' ');
                        fileName = fileName.replaceAll("\\s+", " ");
                        // strip the wave header, replace with better code that can read a wav file
                        fragments.put(fileName, Arrays.copyOfRange(baos.toByteArray(), 44, baos.size()));
                    }
                    zis.closeEntry();
                }
            } catch (IOException e) {
                log.error("Error reading prompts.zip", e);
            } finally {
                IOUtils.closeQuietly(zis);
                IOUtils.closeQuietly(bis);
            }
        }
        @Override
        public List<String> getAllFragments() {
            return new ArrayList<String>(fragments.keySet());
        }

        @Override
        public Object getFragment(String fragment) {
            return fragments.get(fragment);
        }
    }
}
