package com.voxware.browser.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;

import com.voxware.browser.android.R;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

/**
 * Created by edh on 9/14/2016.
 */
public class VoxBrowserSettings {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private Context context;
    private SharedPreferences preferences;

    /**
     * Constructs a VoxBrowserSettings object. Always updates the macAddress to the detected macAddress.
     * @param context
     */
    public VoxBrowserSettings(Context context) {
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        setMacAddress(detectMacAddress());
    }

    /**
     * Returns the mac address of this device.
     * @return a mac address or null if there is neither a stored mac address or a detectable one
     */
    public String getMacAddress() {
        return preferences.getString(context.getString(R.string.pref_mac_address_key), null);
    }

    /**
     * Stores a macAddress in the preferences. If
     * @param macAddress
     */
    public void setMacAddress(String macAddress) {
        updateSharedPreference(context.getString(R.string.pref_mac_address_key), macAddress);
    }

    public String getServerUrl() {
        return preferences.getString(context.getString(R.string.pref_server_address_key), "http://localhost:8081/");
    }

    public void setServerUrl(String url) {
        updateSharedPreference(context.getString(R.string.pref_server_address_key), url);
    }

    public String getEnvironment() {
        return preferences.getString(context.getString(R.string.pref_environment_key), "WH101");
    }

    public void setEnvironment(String environment) {
        updateSharedPreference(context.getString(R.string.pref_environment_key), environment);
    }

    public String getLogicalName() {
        String logicalName = preferences.getString(context.getString(R.string.pref_unit_id_key), null);
        if (logicalName == null || logicalName.trim().isEmpty()) {
            return getMacAddress();
        } else {
            return logicalName.trim();
        }
    }

    public void setLogicalName(String logicalName) {
        if (logicalName != null) {
            logicalName = logicalName.trim();
        }
        if (logicalName != null && logicalName.isEmpty()) {
            logicalName = null;
        }
        updateSharedPreference(context.getString(R.string.pref_unit_id_key), logicalName);
    }

    public String getLicenseKey() {
        return preferences.getString(context.getString(R.string.pref_license_key_key), null);
    }

    public void setLicenseKey(String licenseKey) {
        updateSharedPreference(context.getString(R.string.pref_license_key_key), licenseKey);
    }

    public String getClientType() {
        if (Build.MODEL.equals("MC32N0")) {
            return "Zebra MC32N0";
        } else {
            return Build.MANUFACTURER + " " + Build.MODEL;
        }
    }

    public String getOemVariant() {
        return "Android";
    }

    public String getCfgVariant() {
        return "Flash";
    }

    public String getClientGroup() {
        return preferences.getString(context.getString(R.string.pref_client_group_key), getEnvironment());
    }

    public void setClientGroup(String clientGroup) {
        updateSharedPreference(context.getString(R.string.pref_client_group_key), clientGroup);
    }

    protected void deleteSharedPreference(String name) {
        final SharedPreferences.Editor edit = preferences.edit();
        edit.remove(name);
        edit.apply();
    }

    protected void updateSharedPreference(String name, String value) {
        final SharedPreferences.Editor edit = preferences.edit();
        if (value == null || value.isEmpty()) {
            edit.remove(name);
        } else {
            edit.putString(name, value);
        }
        edit.apply();
    }

    protected void updateSharedPreference(String name, Boolean value) {
        final SharedPreferences.Editor edit = preferences.edit();
        if (value == null) {
            edit.remove(name);
        } else {
            edit.putBoolean(name, value);
        }
        edit.apply();
    }

    public boolean isAutoStart() {
        return preferences.getBoolean(context.getString(R.string.pref_auto_start), false);
    }

    public void setAutoStart(boolean autoStart) {
        updateSharedPreference(context.getString(R.string.pref_auto_start), autoStart);
    }

    public int getDaysToKeep() {
        // because the ListPreference control uses a string array only
        return Integer.parseInt(preferences.getString(context.getString(R.string.pref_logging_days_to_keep_key), "-1"));
    }

    public void setDaysToKeep(int daysToKeep) {
        // because the ListPreference control uses a string array only
        updateSharedPreference(context.getString(R.string.pref_logging_days_to_keep_key), Integer.toString(daysToKeep));
    }

    public int getNumberToKeep() {
        return Integer.parseInt(preferences.getString(context.getString(R.string.pref_logging_number_to_keep_key), String.valueOf(Integer.MAX_VALUE)));
    }

    public void setNumberToKeep(int numberToKeep) {
        updateSharedPreference(context.getString(R.string.pref_logging_number_to_keep_key), Integer.toString(numberToKeep));
    }

    public boolean isDevMode() {
        return preferences.getBoolean("com.voxware.browser.android.pref_developerMode", false);
    }

    public void setDevMode(boolean on) {
        updateSharedPreference("com.voxware.browser.android.pref_developerMode", on);
    }

    public String getLanguageId() {
        return preferences.getString(context.getString(R.string.pref_language_id_key), "en-US");
    }

    public void setLanguageId(String languageId) {
        updateSharedPreference(context.getString(R.string.pref_language_id_key), languageId);
    }

    /**
     * @return
     */

    public String detectMacAddress() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            String mac = wInfo.getMacAddress();
            return mac;
        } else {
            try {
                List<NetworkInterface> ifaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface iface : ifaces) {
                    if (iface.isUp() && iface.getName().startsWith("wlan")) {
                        byte[] mac = iface.getHardwareAddress();
                        return String.format("%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
                    }
                }
            } catch (SocketException e) {
                log.error("Error trying to enumerate interfaces", e);
            }
            return "fe:00:00:00:00:00";
        }
    }
}
