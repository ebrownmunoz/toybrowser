package com.voxware.browser.android.audio;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.nuance.android.vocalizer.VocalizerEngine;
import com.nuance.android.vocalizer.VocalizerEngineListener;
import com.nuance.android.vocalizer.VocalizerSpeechItem;
import com.nuance.android.vocalizer.VocalizerVoice;
import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.io.StringPlayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by bsantosuosso on 5/19/16.
 */
public class NuanceVocalizerStringPlayer extends AbstractAndroidPlayer<String> implements StringPlayer {

    public static final String ACTION_SPEAK_START = "com.voxware.browser.android.action.SPEAK_START";
    public static final String ACTION_SPEAK_STOP = "com.voxware.browser.android.action.SPEAK_STOP";
    public static final String ACTION_SPEAK_ERROR = "com.voxware.browser.android.action.SPEAK_ERROR";
    public static final String EXTRA_TTS_TEXT = "com.voxware.browser.android.extra.TTS_TEXT";
    public static final String EXTRA_TTS_ERROR = "com.voxware.browser.android.extra.TTS_ERROR";

    private static final int MIN_TTS_SPEED = 50; // Default 100
    private static final int MAX_TTS_SPEED = 400;
    private static final int MIN_TTS_PITCH = 50; // Default 100
    private static final int MAX_TTS_PITCH = 200;
    private static final int MAX_TTS_VOLUME = 100; // Default 80
    private static final int MIN_TTS_VOLUME = 0;

    private static final Logger log = LoggerFactory.getLogger(NuanceVocalizerStringPlayer.class);

    private Handler handler = null;
    private VocalizerEngine textToSpeech;
    private final AtomicInteger count = new AtomicInteger(0);
    final AtomicBoolean ttsOkay = new AtomicBoolean(false);
    final CountDownLatch engineThreadReady = new CountDownLatch(1);
    final CountDownLatch ttsWait = new CountDownLatch(1);

    private Thread engineThread = new Thread(() -> {
        Looper.prepare();
        handler = new Handler();
        engineThreadReady.countDown();
        Looper.loop();
    });

    // The default values
    private int pitch = 100;
    private int speechRate = 100;
    private int volume = 80;


    public NuanceVocalizerStringPlayer(Context context) throws AudioPlayerException {
        super(context);
        try {
            Future<Void> initTask = getExecutorService().submit(new InitRequest());
            initTask.get();
        } catch (InterruptedException e) {
            throw new AudioPlayerException("Failed to initialize engine, was interrupted", e);
        } catch (ExecutionException e) {
            throw new AudioPlayerException("Failed to initialize engine", e);
        }
    }

    protected ThreadFactory createThreadFactory() {
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "NuanceVocalizerStringPlayer Thread");
            }
        };
    }

    @Override
    public synchronized void cancel() {
        handler.post(textToSpeech::stop);
        super.cancel();
    }

    @Override
    public void setParameter(String name, Object value) {
        if (name.equals("tts.pitch")) {
            pitch = mapPitch((Double.parseDouble(value.toString())));
        } else if (name.equals("tts.speed")) {
            speechRate = mapSpeed((Double.parseDouble(value.toString())));
        } else if (name.equals("tts.volume")) {
            volume = mapVolume(Double.parseDouble(value.toString()));
        }
    }

    @Override
    public Object getParameter(String name) {
        if (name.equals("tts.pitch")) {
            double retPitch = pitch/MAX_TTS_PITCH;
            return new Double(retPitch);
        } else if (name.equals("tts.speed")) {
            double retSpeed = speechRate/MAX_TTS_SPEED;
            return new Double(retSpeed);
        } else if (name.equals("tts.volume")) {
            double retVolume = volume/MAX_TTS_VOLUME;
            return new Double(retVolume);
        }
        return null;
    }

    @Override
    public void shutdown() {
        super.shutdown();
        textToSpeech.release();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            handler.getLooper().quitSafely();
        } else {
            handler.getLooper().quit();
        }
    }

    @Override
    protected Callable<Void> createCallable(String playable) {
        String id = Integer.toString(count.getAndIncrement());
        return new TtsRequest(id, playable, this.pitch, this.speechRate, this.volume);
    }


    /**
     * Maps old style volumes to new style volume with TTS engine volume max set to be 1.0
     */
    private int mapVolume(Double volume) {
        int newVolume = (int)((volume / 1.0d) * MAX_TTS_VOLUME);
        if (newVolume < MIN_TTS_VOLUME) {
            newVolume = MIN_TTS_VOLUME;
        }
        if (newVolume > MAX_TTS_VOLUME) {
            newVolume = MAX_TTS_VOLUME;
        }
        return newVolume;
    }

    /**
     * Maps old style pitches to new style pitches with TTS engine pitch default set to be 1.0
     */
    private int mapPitch(Double pitch) {
        int newPitch = (int)((pitch / 2.0d) * MAX_TTS_PITCH);
        if (newPitch < MIN_TTS_PITCH) {
            newPitch = MIN_TTS_PITCH;
        }
        if (newPitch > MAX_TTS_PITCH) {
            newPitch = MAX_TTS_PITCH;
        }
        return newPitch;
    }

    /**
     * Maps old style speed to new style speed with TTS engine speed default set to be 1.0
     */
    private int mapSpeed(Double speed) {
        int newSpeed = (int)((speed / 4.0d) * MAX_TTS_SPEED);
        if (newSpeed < MIN_TTS_SPEED) {
            newSpeed = MIN_TTS_SPEED;
        }
        if (newSpeed > MAX_TTS_SPEED) {
            newSpeed = MAX_TTS_SPEED;
        }
        return newSpeed;
    }

    protected class InitRequest implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            // we do this from here so that the engineThread is considered the "main" thread
            engineThread.start();
            engineThreadReady.await();
            handler.post(() -> {
                textToSpeech = new VocalizerEngine(getContext());
                textToSpeech.setListener(new VocalizerEngineListener() {
                    @Override
                    public void onStateChanged(int newState) {
                        if (newState == VocalizerEngine.STATE_INITIALIZED) {
                            // Retrieve the available voices and use the first one
                            VocalizerVoice[] availableVoices=textToSpeech.getVoiceList();
                            boolean result = textToSpeech.loadVoice(availableVoices[0]);
                            if (result == false) {
                                log.warn("Loading TTS voice failed");
                                throw new RuntimeException("Loading TTS voice failed");
                            }
                            ttsOkay.set(true);
                        }
                        else if (newState == VocalizerEngine.STATE_INIT_ERROR) {
                            log.error("Initializing TTS failed");
                            //throw new RuntimeException("Initializing TTS failed");
                        }
                        ttsWait.countDown();
                    }
                    @Override
                    public void onSpeakElementStarted(String s) {}
                    @Override
                    public void onSpeakElementCompleted(String s) {}
                    @Override
                    public void onVoiceListChanged() {}
                });
                // Initialize the TTS Engine
                textToSpeech.initialize();
            });
            ttsWait.await();
            if (ttsOkay.get()) {
                return null;
            } else {
                throw new AudioPlayerException("Failed to initialize TTS");
            }
        }
    }

    protected class TtsRequest implements Callable<Void> {
        private final String id;
        private final String utterance;
        private final int pitch;
        private final int speechRate;
        private final int volume;
        private final CountDownLatch latch = new CountDownLatch(1);
        private final AtomicInteger status = new AtomicInteger(0);
        private VocalizerSpeechItem speechItem = null;

        private final VocalizerEngineListener utteranceProgressListener = new VocalizerEngineListener() {
            @Override
            public void onStateChanged(int i) {

            }

            @Override
            public void onSpeakElementStarted(String utteranceId) {
                TtsRequest.this.onStart(utteranceId);
            }

            @Override
            public void onSpeakElementCompleted(String utteranceId) {
                TtsRequest.this.onDone(utteranceId);
            }

            @Override
            public void onVoiceListChanged() {

            }
        };

        public TtsRequest(final String id, final String utterance, final int pitch, final int speechRate, final int volume) {
            this.id = id;
            this.utterance = utterance;
            this.pitch = pitch;
            this.speechRate = speechRate;
            this.volume = volume;
        }

        public Void call() throws Exception {
            // sending an empty string to TextToSpeech has weird behavior (no callbacks get invoked, seems to hang)
            if (utterance == null || utterance.isEmpty()) {
                onStart(id);
                onDone(id);
                return null;
            }
            final String ttsUtterance = utterance.trim().toLowerCase();
            try {
                log.info("Sending \"{}\" to TTS engine with (id, pitch, rate, volume) = ({}, {}, {}, {})", ttsUtterance, id, pitch, speechRate, volume);
                handler.post(() -> {
                    textToSpeech.setListener(utteranceProgressListener);
                    textToSpeech.setAudioStream(getStreamId());
                    textToSpeech.setSpeechPitch(pitch);
                    textToSpeech.setSpeechRate(speechRate);
                    textToSpeech.setSpeechVolume(volume);
                    speechItem = textToSpeech.speak(ttsUtterance, false, id);
                });
                //speechItem.waitForComplete();
                // Now we wait till we are notified of completion, or we get interrupted (cancelled)
                latch.await();
                if (status.intValue() == 0) {
                    return null;
                } else {
                    throw new TtsException("Error: " + status.intValue());
                }
            } catch (InterruptedException e) {
                log.info("Playback of {}\"{}\" cancelled", id, utterance);
                textToSpeech.stop();
                throw new CancellationException("Playback of \"" + utterance + "\" cancelled");
            }
        }

        public void onStart(String utteranceId) {
            log.debug("Started playing {}[{}]", utteranceId, utterance);
            sendNotification(ACTION_SPEAK_START);
        }

        public void onDone(String utteranceId) {
            log.debug("Finished playing {}[{}]", utteranceId, utterance);
            sendNotification(ACTION_SPEAK_STOP);
            latch.countDown();
        }

        protected void sendNotification(String action) {
            Intent intent = new Intent();
            intent.setAction(action);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        }

        protected void sendNotification(String action, int errorCode) {
            Intent intent = new Intent();
            intent.setAction(action);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            intent.putExtra(EXTRA_TTS_ERROR, errorCode);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        }
    }
}
