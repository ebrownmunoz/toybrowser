package com.voxware.browser.android.dagger;

import com.voxware.browser.io.Recognizer;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by edh on 10/13/2016.
 */
@Singleton
@Component(modules = {ViseModule.class})
public interface ViseComponent {
    Recognizer recognizer();
}
