package com.voxware.browser.android.dagger;

import android.content.Context;

import com.voxware.browser.android.utils.VoxBrowserSettings;

import dagger.Module;
import dagger.Provides;

/**
 * Created by edh on 10/12/2016.
 */
@Module
public class AndroidModule {
    private final Context context;
    public AndroidModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides
    public VoxBrowserSettings provideSettings() {
        return new VoxBrowserSettings(context);
    }
}
