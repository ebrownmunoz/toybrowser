package com.voxware.browser.android.service.bluetooth;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by edh on 3/18/2016.
 */
public class BluetoothHeadsetService extends Service {
    public static final String LOG_DIR_EXTRA = "com.voxware.browser.android.extra.log_dir";
    private static final Logger log = LoggerFactory.getLogger("bluetooth");
    ;

    /**
     * Convenience
     *
     * @param context
     * @param serviceConnection
     */
    public static void startBluetoothHeadsetService(Context context, ServiceConnection serviceConnection) {
        Intent intent = new Intent(context, BluetoothHeadsetService.class);
        context.bindService(intent, serviceConnection, Context.BIND_IMPORTANT | Context.BIND_AUTO_CREATE);
    }

    public static void stopBluetoothHeadsetService(Context context, ServiceConnection serviceConnection) {
        context.unbindService(serviceConnection);
    }

    // need to support jellybean, so use the default adapter
    //private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private AudioManager audioManager;
    private BluetoothHeadset handsFreeProfile;

    /**
     * Dispatch since we don't want to make the onReceive too unwieldy
     */
    private final BroadcastReceiver BLUETOOTH_CONNECTION_STATE_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleDeviceConnectionEvent(context, intent);
        }
    };

    /**
     * Dispatch since we don't want to make the onReceive too unwieldy
     */
    private final BroadcastReceiver BLUETOOTH_SCO_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleBluetoothScoUpdate(context, intent);
        }
    };

    private boolean registered = false;

    private BluetoothProfile.ServiceListener bluetoothServiceListener = new BluetoothProfile.ServiceListener() {

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            if (profile == BluetoothProfile.HEADSET) {
                handsFreeProfile = (BluetoothHeadset) proxy;
                registerBroadcastReceiver();
                // seems that pre lollipop may not deliver that initial intent
                List<BluetoothDevice> devices = handsFreeProfile.getConnectedDevices();
                boolean scoConnected = false;
                for (BluetoothDevice device : devices) {
                    if (handsFreeProfile.isAudioConnected(device)) {
                        scoConnected = true;
                    }
                }
                if (devices.size() > 0 && !scoConnected) {
                    audioManager.startBluetoothSco();
                }
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
            if (profile == BluetoothProfile.HEADSET) {
                handsFreeProfile = null;
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //bluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = getBluetoothAdapter();
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        bluetoothAdapter.getProfileProxy(this, bluetoothServiceListener, BluetoothProfile.HEADSET);
        return new Binder();
    }


    @Override
    public boolean onUnbind(Intent intent) {
        audioManager.stopBluetoothSco();
        audioManager.setBluetoothScoOn(false);
        unregisterBroadcastReceiver();
        BluetoothAdapter.getDefaultAdapter().closeProfileProxy(BluetoothProfile.HEADSET, handsFreeProfile);
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Registers all braodcast receivers. Synchronized but probably not necessary since lifecycle methods are called from main UI thread
     */
    protected synchronized void registerBroadcastReceiver() {
        if (!registered) {
            registerReceiver(BLUETOOTH_CONNECTION_STATE_RECEIVER, new IntentFilter(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED));
            registerReceiver(BLUETOOTH_SCO_RECEIVER, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED));
            registered = true;
        }
    }

    /**
     * Unregisters all braodcast receivers. Synchronized but probably not necessary since lifecycle methods are called from main UI thread
     */
    protected synchronized void unregisterBroadcastReceiver() {
        if (registered) {
            this.unregisterReceiver(BLUETOOTH_SCO_RECEIVER);
            this.unregisterReceiver(BLUETOOTH_CONNECTION_STATE_RECEIVER);
            registered = false;
        }
    }

    /**
     * Handles device connection notification. If a device is connected and is handsfree, start the Bluetooth SCO connection
     *
     * @param context the context
     * @param intent  the intent
     */
    protected void handleDeviceConnectionEvent(Context context, Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        int state = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1);
        if (state == BluetoothHeadset.STATE_CONNECTED) {
            // new device connection, intent filter says this should be handsfree profile only, but check anyway
            log.warn("Device connected: {} [{}]", device.getName(), device.getAddress());
            if (handsFreeProfile.getConnectedDevices().contains(device)) {
                // it is handsfree
                log.warn("Device is a handsfree device, connecting SCO audio: {} [{}]", device.getName(), device.getAddress());
                audioManager.startBluetoothSco();
            }
        } else if (state == BluetoothHeadset.STATE_DISCONNECTED) {
            // existing device disconnected
            log.warn("Device disconnected: {} [{}]", device.getName(), device.getAddress());
            audioManager.stopBluetoothSco();
        }
    }

    /**
     * Monitors the Bluetooth SCO state for diagnostic purposes.
     *
     * @param context the context
     * @param intent  the intent
     */
    protected void handleBluetoothScoUpdate(Context context, Intent intent) {
        // It is possible to call setBluetoothScoOn(boolean) method here instead of onBind. You
        // would call setBluetoothScoOn(true) and setBluetoothScoOn(false) for
        // SCO_AUDIO_STATE_CONNECTED and SCO_AUDIO_STATE_DISCONNECTED respectively
        int currentState = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, -1);
        int previousState = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_PREVIOUS_STATE, -1);
        String previousStateName = "unknown";
        switch (previousState) {
            case AudioManager.SCO_AUDIO_STATE_CONNECTED:
                previousStateName = "SCO_AUDIO_STATE_CONNECTED";
                break;
            case AudioManager.SCO_AUDIO_STATE_CONNECTING:
                previousStateName = "SCO_AUDIO_STATE_CONNECTING";
                break;
            case AudioManager.SCO_AUDIO_STATE_DISCONNECTED:
                previousStateName = "SCO_AUDIO_STATE_DISCONNECTED";
                break;
            case AudioManager.SCO_AUDIO_STATE_ERROR:
                previousStateName = "SCO_AUDIO_STATE_ERROR";
                break;
            default:
                previousStateName = "unknown(" + previousState + ")";
        }
        if (currentState == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
            log.info("SCO audio connected, was {}", previousStateName);
            //audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setBluetoothScoOn(true);
        } else if (currentState == AudioManager.SCO_AUDIO_STATE_CONNECTING) {
            log.info("SCO audio connecting, was {}", previousStateName);
        } else if (currentState == AudioManager.SCO_AUDIO_STATE_DISCONNECTED) {
            log.info("SCO audio disconnected, was {}", previousStateName);
            audioManager.setBluetoothScoOn(false);
            // if we are disconnected, it means either the headset shutdown
            // or a call came in?
        } else if (currentState == AudioManager.SCO_AUDIO_STATE_ERROR) {
            log.info("SCO audio error, was {}", previousStateName);
        }
    }

    private BluetoothAdapter getBluetoothAdapter() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                return viaService();
            }
        } catch (NoSuchFieldError e) {
            // fall through
        } catch (Exception e) {
            // fall through
        }
        return BluetoothAdapter.getDefaultAdapter();
    }

    @TargetApi(18)
    private BluetoothAdapter viaService() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> bluetoothManagerServiceClass = Class.forName("android.bluetooth.BluetoothManager");
        Object bluetoothManagerService = getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManagerService != null && bluetoothManagerServiceClass.isAssignableFrom(bluetoothManagerService.getClass())) {
            Method getAdapterMethod = bluetoothManagerServiceClass.getMethod("getAdapter", new Class<?>[0]);
            return (BluetoothAdapter) getAdapterMethod.invoke(bluetoothManagerService);
        } else {
            throw new NullPointerException("BLUETOOTH_SERVICE does not exist");
        }
    }
}
