package com.voxware.browser.android.audio;

/**
 * Represents the default values for TTS properties. Can be fed values to allow exteranl sources, like preferences.
 * Created by edh on 4/14/2016.
 */
public class TtsDefaults {

    private final double volume;
    private final double speed;
    private final double pitch;
    public TtsDefaults() {
        volume = 1.0d;
        speed = 1.0d;
        pitch = 1.0d;
    }

    public TtsDefaults(final double volume, final double speed, final double pitch) {
        this.volume = volume;
        this.speed = speed;
        this.pitch = pitch;
    }

    public double getVolume() {
        return volume;
    }

    public double getSpeed() {
        return speed;
    }

    public double getPitch() {
        return pitch;
    }
}
