package com.voxware.browser.android.dagger;

import com.voxware.browser.android.audio.ByteArrayPlayer;
import com.voxware.browser.android.audio.UriPlayer;

import dagger.Binds;
import dagger.Module;

/**
 * Created by edh on 10/12/2016.
 */
@Module
public abstract class StreamPlayerModule {
    @Binds
    public abstract com.voxware.browser.io.ByteArrayPlayer provideBytePlayer(ByteArrayPlayer byteArrayPlayer);

    @Binds
    public abstract com.voxware.browser.io.UriPlayer provideUriPlayer(UriPlayer uriPlayer);
}
