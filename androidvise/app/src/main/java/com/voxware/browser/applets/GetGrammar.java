package com.voxware.browser.applets;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Scriptable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;

/**
 * Created by edh on 12/7/2015.
 */
public class GetGrammar extends AbstractApplet {
    private final DocumentServer documentServer;

    @Inject
    public GetGrammar(DocumentServer documentServer) {
        this.documentServer = documentServer;
    }

    @Override
    public Result run() throws MissingParameterException {
        String url = require("grammar").asString();
//        boolean load = parameter("load", false).asBoolean();
//        int timeout = parameter("timeout", -1).asInteger();

        // Interpret them
        if (url.isEmpty()) {
            return Result.event("error.applet.missingparameter", "\"grammar\" parameter of GetGrammar applet missing or empty");
        }

        // Acquire the grammar image
        byte[] recImage = null;
        InputStream is = null;
        try {
            is = documentServer.getDocument(new URI(url), null, CacheControl.NO_CACHE);
            recImage = IOUtils.toByteArray(is);
            Result result = Result.Ok();
            Scriptable scriptable = getContext().newObject(getContext().initStandardObjects());
            scriptable.put("name", scriptable, url);
            scriptable.put("image", scriptable, recImage);
            result.setValue(scriptable);
            return result;
        } catch (URISyntaxException e) {
            return Result.event("error.badfetch.malformedurl", e.getMessage());
        } catch (IOException e) {
            return Result.event("error.badfetch.ioerror", e.getMessage());
        } catch (DocumentServerException e) {
            return Result.event("error.badfetch.ioerror", e.getMessage());
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
}
