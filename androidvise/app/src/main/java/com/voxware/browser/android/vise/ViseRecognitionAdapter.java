package com.voxware.browser.android.vise;

import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.vise.RecognitionRequest;
import com.voxware.vise.VISERecognizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Future;

import javax.speech.recognition.GrammarException;

/**
 * Created by edh on 10/26/2015.
 */
public class ViseRecognitionAdapter implements Recognizer {
    public static final String RULE = "rule";
    private VISERecognizer recognizer;
    private RecognitionRequest request;
    private String rule = null;

    private final Logger log = LoggerFactory.getLogger(ViseRecognitionAdapter.class);

    public ViseRecognitionAdapter(VISERecognizer recognizer) {
        this.recognizer = recognizer;
    }

    @Override
    public Future<RecognitionResult> recognize(String rule) throws BrowserEvent{
        try {
            request = recognizer.recognize(rule);
        } catch (GrammarException e) {
            request = null;
            log.error("Something is wrong with the grammar. Sending event", e);
            throw new BrowserEvent("error.grammar.exception", "Grammar Exception");

        } catch (IllegalArgumentException illarg) {
            request = null;
            log.info("Illegal Argument exception (expected in training). Sending event", illarg);
            throw new BrowserEvent("error.grammar.nosuchrule", "Grammar Exception");
        }
        return request;
    }

    @Override
    public Future<RecognitionResult> recognizeWithInterruptsOnly() {
        return recognizer.recognize();
    }

    @Override
    public void cancel() {
        if (request != null && !request.isDone()) {
            request.cancel(true);
            request = null;
        }
    }

    @Override
    public void loadGrammar(String id, byte[] bytes, boolean forceUpdate) {
        recognizer.loadGrammar(id, bytes, forceUpdate);
    }

    @Override
    public void loadVoiceModel(String id, byte[] bytes, boolean forceUpdate) {
        recognizer.loadSpeaker(id, bytes, forceUpdate);
    }

    @Override
    public void setParameter(String s, Object o) {
        if (RULE.equals(s)) {
            rule = o.toString();
        } else {
            recognizer.setParameter(s, o);
        }
    }

    @Override
    public Object getParameter(String s) {
        return recognizer.getParameter(s);
    }

    @Override
    public void sendInterrupt(RecognitionResult result) {
        recognizer.interrupt(result);
    }

    @Override
    public void shutdown() {
        cancel();
        recognizer.destroy();
    }

    public VISERecognizer getRecognizer() {
        return recognizer;
    }


}
