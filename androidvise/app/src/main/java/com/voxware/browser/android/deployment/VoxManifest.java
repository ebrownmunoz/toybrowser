package com.voxware.browser.android.deployment;

import android.util.JsonReader;
import android.util.JsonToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eric on 7/4/16.
 */
public class VoxManifest {

    private static final Logger log = LoggerFactory.getLogger(VoxManifest.class);

    protected static class Item implements Comparable<Item> {
        private String name;
        private String md5;
        private String language;

        public Item(String name, String md5) {
            this.name = name;
            this.md5 = md5;
            this.language = null;
        }

        public Item(String name, String language, String md5) {
            this(name, md5);
            this.language = language;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMd5() {
            return md5;
        }

        public void setMd5(String md5) {
            this.md5 = md5;
        }

        public String getLanguage() {return language;}

        @Override
        public int compareTo(Item another) {
            int retval = name.compareTo(another.name);

            if (retval == 0) {
                return md5.compareTo(another.md5);
            } else {
                return retval;
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Item) {
                Item another = (Item)obj;
                return ((name == another.name) && (md5 == another.md5));
            } else {
                return false;
            }
        }

        public int hashCode() {
            return name.hashCode();
        }


    }

    private List<Item> resources = new ArrayList<Item>();
    private List<Item> voices = new ArrayList<Item>();

    public void addResource(String name, String md5) {
        resources.add(new Item(name, md5));

    }

    public void addVoice(String name, String md5) {
        voices.add(new Item(name, md5));

    }
    public String toString() {
        boolean first = true;

        StringBuilder builder = new StringBuilder("{ resources : [\n");

        for (Item item : resources) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("{\"name\": \"" + item.getName() +"\"," );
            builder.append("\"md5\": \"" + item.getMd5() +"\"}\n" );
        }

        first = true;
        builder.append("], \nttsvoices : [");

        for (Item item : voices) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("{\"name\": \"" + item.getName() +"\"," );
            builder.append("\"md5\": \"" + item.getMd5() +"\"}\n" );
        }


        builder.append("]}");

        return builder.toString();
    }

    /**
     * Create a VoxManifest from an input stream.
     * @param instream
     * @return
     */
    public static VoxManifest parseVoxManifest(InputStream instream) {
        VoxManifest manifest = new VoxManifest();

        if (instream == null) {
            return manifest;
        }


        JsonReader reader = new JsonReader(new InputStreamReader(instream));
        reader.setLenient(true);

        try {
            reader.beginObject();

            while(reader.hasNext()) {
                String lname = reader.nextName();

                if ("resources".equals(lname)) {
                    manifest.resources.addAll(parseArray(reader));
                } else if ("ttsvoices".equals(lname)) {
                    manifest.voices.addAll(parseArray(reader));
                } else {
                    log.warn("Field " + lname + " is not implemented.");
                    reader.skipValue();
                }
            }




            reader.endObject();
        } catch (IOException e) {
            log.error("Error reading manifest", e);
        }

        return manifest;

    }

    private static List<Item> parseArray (JsonReader reader) {
        List<Item> items = new ArrayList<Item>();
        try {
            reader.beginArray();
            while (reader.hasNext()) { // array
                String oname = null;
                String omd5 = null;
                String language = null;
                reader.beginObject();
                while (reader.hasNext()) { // object
                    String lname = reader.nextName();
                    if ("name".equals(lname)) {
                        oname = reader.nextString();
                    } else if ("md5".equals(lname) || "crc".equals(lname)) {
                        JsonToken token = reader.peek();
                        if (token.equals(JsonToken.STRING)) {
                            omd5 = reader.nextString();
                        } else {
                            reader.skipValue();
                        }
                    } else if ("language".equals(lname)) {
                        language = reader.nextString();
                    } else {
                        log.warn("Unknown field " + lname + "=");
                        reader.skipValue();
                    }
                }
                reader.endObject();
                if (oname == null) {
                    log.error("null name. Ignoring");
                } else {
                    items.add(new Item(oname, language, omd5));
                }
            }
            reader.endArray();
        } catch (IOException e) {
            log.error("Error parsing manifest", e);
        }
        return items;
    }

    public List<Item> getResources() {
        return resources;
    }

    public Item findResourceByName(String name) {
        for (Item item : resources) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return null;
    }

    public List<Item> getVoices() {
        return voices;
    }

    public Item findVoiceByName(String name) {
        for (Item item : voices) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return null;
    }
}
