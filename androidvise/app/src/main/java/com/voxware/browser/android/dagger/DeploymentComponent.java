package com.voxware.browser.android.dagger;

import com.voxware.browser.android.deployment.DeploymentManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by edh on 10/26/2016.
 */
@Component(modules = {DocumentServerModule.class, AndroidModule.class})
@Singleton
public interface DeploymentComponent {
    DeploymentManager getDeploymentClient();
}
