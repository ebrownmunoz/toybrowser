/**
 *
 */
package com.voxware.browser.android.service;

import android.content.Context;

import com.voxware.browser.android.BatteryObserver;
import com.voxware.browser.android.CASClient;
import com.voxware.browser.android.InterruptHandler;
import com.voxware.browser.android.LocationObject;
import com.voxware.browser.android.NetworkObject;
import com.voxware.browser.android.audio.TtsDefaults;
import com.voxware.browser.android.utils.VoxBrowserSettings;
import com.voxware.browser.applets.Applet;
import com.voxware.browser.applets.AppletRegistry;
import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.audioplayer.PromptResolver;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Interpreter;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.interrupt.InterruptManager;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.log.LogService;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.parser.VxmlParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author eric
 */
public class Browser implements Runnable, InterruptHandler {
    private static final Logger log = LoggerFactory.getLogger(Browser.class);
    private final Context context;
    private final URI start;
    private final BatteryObserver batteryObserver;
    private final VxmlParser vxmlParser = DaggerVxmlParser.create();
    private final AudioPlayer audioPlayer;
    private final Recognizer recognizer;
    private final DocumentServer docserver;
    private final AppletRegistry appletRegistry = new AppletRegistry();
    private final Map<String, LogService> loggers = new LinkedHashMap<>();
    private final PropertyList defaultProperties = new PropertyList();
    private InterruptManager interruptManager;
    private final Map<String, Object> sessionProperties = new HashMap<>();
    private final PromptResolver.FragmentProviderFactory fragmentProviderFactory;
    private final LocationObject locationObject;

    // Yuck, can't we get rid of this??
    private CASClient cas;
    private Thread casThread;

    private Thread thread = null;

    private Interpreter interpreter;

    @Inject
    public Browser(Context context,
                   @Named("bootUri") URI start,
                   DocumentServer documentServer,
                   AudioPlayer audioPlayer,
                   Recognizer recognizer,
                   PromptResolver.FragmentProviderFactory fragmentProviderFactory,
                   @Named("defaultApplets") Map<String, Applet> defaultApplets,
                   @Named("defaultRecognizerApplets") Map<String, Applet> defaultRecognizerApplets,
                   @Named("defaultRecognizerSessionVariables") Map<String, Object> defaultSessionVariables) {
        this.context = context;
        this.start = start;
        this.docserver = documentServer;
        this.audioPlayer = audioPlayer;
        this.recognizer = recognizer;
        this.fragmentProviderFactory = fragmentProviderFactory;
        batteryObserver = new BatteryObserver(context, this);
        locationObject = new LocationObject(context);
        appletRegistry.registerApplets(defaultApplets);
        appletRegistry.registerApplets(defaultRecognizerApplets);
        sessionProperties.putAll(defaultSessionVariables);
    }

    public Recognizer getRecognizer() {
        return recognizer;
    }

    public Map<String, LogService> getLoggerMap() {
        return loggers;
    }

    public PropertyList getDefaultProperties() {
        return defaultProperties;
    }

    public void sendInterrupt(String source, String message) {
        if (interruptManager != null) {
            interruptManager.raise(source, message);
        }
    }

    public void sendInterrupt(String source, String sense, Double level) {
        if (interruptManager != null) {
            interruptManager.raise(source, sense, level);
        }
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(Thread.currentThread().getThreadGroup(), this, "Browser", 262144);
            thread.start();
        }
    }

    public Map<String, Object> getSessionProperties() {
        return sessionProperties;
    }

    protected void startAndBindCas() {
        casThread = new Thread(() -> {
            try {
                cas = new CASClient(Browser.this);
                loggers.put("appsrv", cas);
                cas.run();
            } catch (IOException e) {
                throw new IllegalStateException("Failed to start CAS client", e);
            }
        }, "CAS-Client");
        casThread.start();
    }

    protected void stopCas() {
        if (casThread != null) {
            casThread.interrupt();
            try {
                casThread.join(30000);
            } catch (InterruptedException e) {

            }

        }
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        //TODO handle exceptions for null tts, recognizer and docserver.
        try {
            startAndBindCas();
            setDefaultProperties();

            IoManager iomanager = new IoManager();
            iomanager.setAudioPlayer(audioPlayer);
            iomanager.setRecognizer(recognizer);
            SessionContext sessionContext = new SessionContext(null, sessionProperties, defaultProperties, iomanager, docserver);

            for (Map.Entry<String, LogService> logService : loggers.entrySet()) {
                sessionContext.getLogManager().register(logService.getKey(), logService.getValue());
            }
            interruptManager = sessionContext.getInterruptManager();
            sessionContext.getPromptResolver().setFragmentProviderFactory(fragmentProviderFactory);

            DocumentParser parser = vxmlParser.getDocumentParser();
            Document document = parser.parse(sessionContext, start, docserver.getDocument(start));
            interpreter = new Interpreter(sessionContext, docserver, appletRegistry);
            interpreter.loadDocument(document);
            Result result = interpreter.processDocument();
            log.info("Exitting!!!");
        } catch (DocumentServerException e) {
            log.error("Error", e);
        } catch (ParserException e) {
            log.error("Error", e);
        } catch (InterpreterException e) {
            log.error("Error", e);
        } catch (Throwable t) {
            log.error("Error", t);
        } finally {
            stopCas();
            audioPlayer.shutdown();
            recognizer.shutdown();
        }
    }

    public void stop() {
        batteryObserver.stop();
        interpreter.exit();
        try {
            thread.join(30000);
        } catch (InterruptedException e) {
            thread = null;
        }
        if (locationObject != null) {
            locationObject.close();
        }
    }

    private void setDefaultProperties() {
        VoxBrowserSettings settings = new VoxBrowserSettings(context);
        sessionProperties.put("macaddress", settings.getMacAddress());
        sessionProperties.put("environmentid", settings.getEnvironment());
        sessionProperties.put("languageId", settings.getLanguageId());
        sessionProperties.put("memory", new AndroidMemoryObject());
        sessionProperties.put("tts", new TtsDefaults());
        sessionProperties.put("platform", settings.getClientType());
        sessionProperties.put("network", new NetworkObject(context));
        sessionProperties.put("location", locationObject);
        // default properties
        defaultProperties.put("bargein", makeProperty("bargein", "true"));
        defaultProperties.put("tts.pitch", makeProperty("tts.pitch", "1.0"));
        defaultProperties.put("tts.speed", makeProperty("tts.speed", "1.0"));
        defaultProperties.put("tts.volume", makeProperty("tts.volume", "1.0"));
        defaultProperties.put("speakervolume", makeProperty("speakervolume", "34.5"));
        defaultProperties.put("suppressspeechinput", makeProperty("suppressspeechinput", "false"));
    }

    protected Property makeProperty(String name, String value) {
        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put(Property.NAME, name);
        attributes.put(Property.VALUE, value);
        return new Property(attributes, new NodeLocation(URI.create("builtin://defaults/" + name)));
    }

    /**
     * Created by edh on 12/24/2015.
     */

    public static class AndroidMemoryObject {
        public static class System {
            public static class Statistics {
                public long getTotalbytes() {
                    return Runtime.getRuntime().totalMemory();
                }
                public long getBytesfree() {
                    return Runtime.getRuntime().freeMemory();
                }
                public double getPercentused() {
                    return 1.0d - ((double)Runtime.getRuntime().freeMemory() / (double)Runtime.getRuntime().totalMemory());
                }
            }
            private final Statistics statistics = new Statistics();
            public Statistics getStats() {
                return statistics;
            }
        }
        private final System system = new System();
        public System getSystem() {
            return system;
        }
    }
}
