package com.voxware.browser.android;

/**
 * Created by eric on 2/10/16.
 */
public interface InterruptHandler {

    public void sendInterrupt(String source, String message);
}
