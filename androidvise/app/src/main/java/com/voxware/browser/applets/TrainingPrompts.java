package com.voxware.browser.applets;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;

import javax.inject.Inject;

import voxware.engine.recognition.sapivise.SapiViseGrammar;

/**
 * Created by edh on 11/19/2015.
 */
public class TrainingPrompts extends AbstractApplet {
    public static final int COUNT_DEFAULT = 1;
    public static final boolean BATCH_DEFAULT = false;
    private static Logger log = LoggerFactory.getLogger(TrainingPrompts.class);

    //private final SapiVise sapiVise;
    private final VISERecognizer recognizer;
    private final DocumentServer documentServer;

    @Inject
    public TrainingPrompts(VISERecognizer recognizer, DocumentServer documentServer) {
        this.recognizer = recognizer;
        this.documentServer = documentServer;
    }

    @Override
    public Result run() throws MissingParameterException {

        ScriptableObject scope = getContext().initStandardObjects();

        Scriptable ret = getContext().newObject(scope);

        String grammarParam = parameter("grammar").asString();
        String[] script = parameter("script").asStringArray();
        String[] words = parameter("words").asStringArray();
        String stripParam = parameter("strip").asString();
        int count = parameter("count", COUNT_DEFAULT).asInteger();
        boolean batch = parameter("batch", BATCH_DEFAULT).asBoolean();
        String type = parameter("type", "null").asString();

        // Interpret the parameters
        URI grammarUrl = null;
        if (grammarParam != null) {
            try {
                grammarUrl = new URI(grammarParam);
            } catch (URISyntaxException e) {
                return Result.event("error.badfetch", grammarParam + " is not a valid URI");
            }
        }

        // Optimize variant matching by using a simple char match if the suffix link is one character long
        String linkString;
        char linkChar = 0;
        if (stripParam == null || stripParam.isEmpty()) {
            linkString = null;
        } else {
            linkString = stripParam;
            linkChar = stripParam.charAt(0);
            if (stripParam.length() == 1) linkString = null;
        }

        int typeIndex = (type.equalsIgnoreCase("required") || type.equalsIgnoreCase("unenrolled")) ? SapiViseGrammar.REQUIRED : SapiViseGrammar.RECOMMENDED;

        if (grammarUrl == null) {
            if (script == null)
                return Result.event("error.applet.missingparameter", "\"grammar\" and \"script\" parameters of TrainingPrompts applet are both missing or empty");

            if (batch || (typeIndex == SapiViseGrammar.REQUIRED))
                return Result.event("error.applet.missingparameter", "\"grammar\" parameter of TrainingPrompts applet is missing or empty");

        }

        // Get the SapiViseGrammar if batch is true or there is no script or if type index is SapiViseGrammar.REQUIRED
        SapiViseGrammar viseGrammar = null;
        if ((batch || (script == null) || (typeIndex == SapiViseGrammar.REQUIRED))) {
            javax.speech.recognition.Grammar grammar = null;  // Throws MalformedURLException, VXMLEvent
            InputStream is = null;
            byte[] bytes = null;
            Map<Object, Object> cache = getSessionContext().getCache();
            if (cache.containsKey(grammarParam)) {
                bytes = (byte[]) cache.get(grammarParam);
                recognizer.loadGrammar(grammarParam, bytes, false);
            } else {
                try {
                    is = documentServer.getDocument(grammarUrl, null, CacheControl.NO_CACHE);
                    bytes = IOUtils.toByteArray(is);
                    recognizer.loadGrammar(grammarParam, bytes, true);
                    cache.put(grammarParam, bytes);
                } catch (IOException e) {
                    return Result.event("error.badfetch.ioerror", e.getMessage());
                } catch (DocumentServerException e) {
                    return Result.event("error.badfetch.ioerror", e.getMessage());
                } finally {
                    IOUtils.closeQuietly(is);
                }
            }

            grammar = recognizer.getCurrentGrammar();
            if (!(grammar instanceof SapiViseGrammar))
                return Result.event("error.badgrammar", "\"" + grammarUrl + "\" is a " + grammar.getClass().getName() + ", not a SapiViseGrammar");
            viseGrammar = (SapiViseGrammar) grammar;
        }

        // Get the prompt strings from the script, if present, otherwise from the grammar
        String[] phrases = null;
        if (script != null) {
            phrases = (typeIndex == SapiViseGrammar.REQUIRED) ? viseGrammar.trainPhrase(typeIndex) : script;
        } else if (viseGrammar != null) {
            phrases = viseGrammar.trainPhrase(typeIndex);
        }
        if (phrases == null) phrases = new String[0];

        // Prune the prompts to those containing enough occurrences of words from the word list
        if (words != null) {
            TreeSet wordSet = new TreeSet(Arrays.asList(words));
            if (phrases.length > 0) {
                // Null out the phrases in phrases[] that do not contain enough instances of the specified words or their variants
                int phraseCount = 0;
                int phraseIndex;
                int phraseLength;
                String phrase;
                for (phraseIndex = 0; phraseIndex < phrases.length; phraseIndex++) {
                    phrase = phrases[phraseIndex];
                    if (phrase != null) {
                        phraseLength = phrase.length();
                        int wordIndex = 0;
                        int instanceCount = 0;
                        int wordStart;
                        int wordLength;
                        int wordEnd;
                        int suffixEnd;
                        String word;
                        while (wordIndex < words.length) {
                            word = words[wordIndex];
                            if (word != null) {
                                wordStart = 0;
                                wordLength = word.length();
                                wordEnd = 0;
                                // Add the number of occurrences of this word to the overall instance count for this phrase
                                while ((linkChar != 0 || instanceCount < count) && (wordStart = phrase.indexOf(word, wordEnd)) >= 0) {
                                    wordEnd = wordStart + wordLength;
                                    if (wordStart > 0 && phrase.charAt(wordStart - 1) != ' ' && phrase.charAt(wordStart - 1) != '\"')
                                        continue;
                                    if (wordEnd < phraseLength && phrase.charAt(wordEnd) != ' ' && phrase.charAt(wordEnd) != '\"') {
                                        if (phrase.charAt(wordEnd) != linkChar) continue;
                                        if (linkString != null && !phrase.startsWith(linkString, wordEnd))
                                            continue;
                                        // The word in the phrase is a variant of this word; add it to the wordSet
                                        if ((suffixEnd = phrase.indexOf(' ', wordEnd)) > 0 || (suffixEnd = phrase.indexOf('\"', wordEnd)) > 0)
                                            wordEnd = suffixEnd;
                                        else wordEnd = phraseLength;
                                        if (wordSet.add(phrase.substring(wordStart, wordEnd)))
                                            log.info("TrainingPrompts: added word \"" + phrase.substring(wordStart, wordEnd) + "\" to the word set"); // DEBUG
                                    }
                                    instanceCount++;
                                }
                                // If we've found enough instances of the specified words or their variants in this phrase, leave it in phrases[]
                                if (instanceCount >= count) {
                                    // Strip double quotes from the phrase
                                    int index = phrase.indexOf('\"');
                                    if (index > -1) {
                                        StringBuffer buffer = new StringBuffer(phrase);
                                        while (index < buffer.length()) {
                                            if (buffer.charAt(index) == ('\"'))
                                                buffer.deleteCharAt(index);
                                            index++;
                                        }
                                        phrases[phraseIndex] = buffer.toString();
                                    }
                                    phraseCount++;
                                    break;
                                }
                            }
                            // If we've exhausted the word list, then the phrase doesn't contain enough of the words; remove it from phrases[]
                            if (++wordIndex == words.length) {
                                phrases[phraseIndex] = null;
                                break;
                            }
                        }
                    }
                }
                // Prune phrases[] of any null phrases
                if (phraseCount < phrases.length) {
                    int prunedPhraseIndex = 0;
                    String[] prunedPhrases = new String[phraseCount];
                    for (phraseIndex = 0; phraseIndex < phrases.length && prunedPhraseIndex < phraseCount; phraseIndex++) {
                        if (phrases[phraseIndex] != null)
                            prunedPhrases[prunedPhraseIndex++] = phrases[phraseIndex];
                    }
                    phrases = prunedPhrases;
                }
            }
            // Wrap the wordSet in a JavaScript Array and put it in the results object as the "words" property
            ret.put("words", ret, getContext().newArray(scope, wordSet.toArray(new Object[wordSet.size()])));
            //flatReturn.putProperty("words", context.newArray(scope, wordSet.toArray(new String[0])));
        }

        // Wrap the prompts in a JavaScript Array and put it in the results object as the "prompts" property
        ret.put("prompts", ret, getContext().newArray(scope, Arrays.copyOf(phrases, phrases.length, Object[].class)));
        //flatReturn.putProperty("prompts", context.newArray(scope, phrases));

        // Optionally start the training pass
        if (batch && !viseGrammar.startTrainingPass())
            return Result.event("error.applet.recognition.training", "Cannot start training pass for grammar \"" + grammarUrl + "\"");

        Result result = Result.Ok();
        result.setValue(ret);
        return result;
    }
}
