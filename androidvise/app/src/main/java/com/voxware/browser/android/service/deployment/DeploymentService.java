package com.voxware.browser.android.service.deployment;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.voxware.browser.android.dagger.AndroidModule;
import com.voxware.browser.android.dagger.DaggerDeploymentComponent;
import com.voxware.browser.android.deployment.DeploymentManager;

import java.util.concurrent.atomic.AtomicBoolean;

public class DeploymentService extends Service {

    private AtomicBoolean started = new AtomicBoolean(false);
    private DeploymentManager deploymentManager;
    private LocalBroadcastManager broadcastManager;
    private Thread thread;

    @Override
    public void onCreate() {
        super.onCreate();
        broadcastManager = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!started.get()) {
            deploymentManager = DaggerDeploymentComponent.builder().androidModule(new AndroidModule(this)).build().getDeploymentClient();
            thread = new Thread(() -> {
                deploymentManager.call();
                started.set(false);
            },"ClientRegistrationAndDeployment");
            thread.start();
            started.set(true);
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        started.set(false);
    }
}
