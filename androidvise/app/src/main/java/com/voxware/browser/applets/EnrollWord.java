package com.voxware.browser.applets;

import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.speech.recognition.SpeakerManager;
import javax.speech.recognition.SpeakerProfile;

import voxware.engine.recognition.sapivise.SapiVise;
import voxware.engine.recognition.sapivise.ViseEnrollStatus;
import voxware.engine.recognition.sapivise.ViseResult;
import voxware.engine.recognition.sapivise.ViseTrainer;
import voxware.engine.recognition.sapivise.ViseTrainerFactory;

/**
 * Created by edh on 12/4/2015.
 */
public class EnrollWord extends AbstractApplet {
    private static final Logger log = LoggerFactory.getLogger(EnrollWord.class);


    private final SapiVise sapiVise;
    private ViseTrainerFactory factory;
    private ViseTrainer trainer;
    private String prevWordName = null;

    @Inject
    public EnrollWord(VISERecognizer recognizer) {
        this.sapiVise = recognizer.getSapiVise();
        this.factory = sapiVise.getTrainerFactory();
    }

    @Override
    public Result run() throws MissingParameterException {
        // Get the parameters
        ViseResult result = require("result").as(ViseResult.class);
        String wordName = require("word").asString();
        boolean request = parameter("request", false).asBoolean();

        Scriptable scope = getContext().initStandardObjects();
        Map<String, Object> ret = new HashMap<String, Object>();

        // When first called or when a new word comes in, get a new Trainer
        if (prevWordName == null || !wordName.equals(prevWordName)) {
            prevWordName = wordName;
            trainer = factory.getTrainer();
            log.info("EnrollWord: started with Word \"" + wordName + "\"");
        }
        if (trainer.insertCorrection(wordName, result)) {
            ViseEnrollStatus enrollmentStatus = trainer.enrollWord();
            log.info("  EnrollWord: have " + enrollmentStatus.Count() + " utterances; need a total of " + enrollmentStatus.Needed());
            ret.put("count", enrollmentStatus.Count());
            if (enrollmentStatus.Status() == 0) {
                log.info("EnrollWord: success on Word \"" + wordName + "\"");
                trainer.release();
                ret.put("more", 0);
                // Force new trainer for next utterance, so backing to this word would work from scratch
                prevWordName = null;
                if (request) {
                    // Send back voice file
                    SpeakerManager speakerManager = sapiVise.getSpeakerManager();
                    SpeakerProfile profile = speakerManager.getCurrentSpeaker();
                    if (profile != null) {
                        ByteArrayOutputStream profileStream = new ByteArrayOutputStream();
                        try {
                            speakerManager.writeVendorSpeakerProfile(profileStream, profile); // Throws SecurityException, MalformedURLException, IOException
                        } catch (IOException e) {
                            return Result.event("error.applet.exception", e.getMessage());
                        }
                        ret.put("name", profile.getName());
                        ret.put("image", Context.javaToJS(profileStream.toByteArray(), scope));
                    } else {
                        return Result.event("error.vise.speaker.nospeaker", "EnrollWord: cannot get the current speaker profile from the SpeakerManager");
                    }
                }
            } else if (enrollmentStatus.Status() == ViseTrainer.NOTENOUGHDATA) {
                // Signal that more utterances are needed
                int num = enrollmentStatus.Needed() - enrollmentStatus.Count();
                if (num <= 0) num = 1;
                ret.put("more", num);
            } else {
                return Result.event("error.enrollment", "enrollment status is " + enrollmentStatus.Status());
            }
        } else {
            return Result.event("error.enrollment.insertion", "cannot insert ViseResult for \"" + wordName + "\"");
        }

        Result appletResult = Result.Ok();
        Scriptable retScriptable = getContext().newObject(scope);
        for (Map.Entry<String, Object> entry : ret.entrySet()) {
            retScriptable.put(entry.getKey(), retScriptable, entry.getValue());
        }
        appletResult.setValue(retScriptable);
        return appletResult;
    }
}
