package com.voxware.browser.android.audio;

/**
 * Created by edh on 2/3/2016.
 */
public class TtsException extends Exception {
    public TtsException() {
        super();
    }

    public TtsException(String detailMessage) {
        super(detailMessage);
    }

    public TtsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public TtsException(Throwable throwable) {
        super(throwable);
    }
}
