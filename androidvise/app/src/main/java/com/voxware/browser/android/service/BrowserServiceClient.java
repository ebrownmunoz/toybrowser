package com.voxware.browser.android.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by edh on 11/2/2016.
 */
public class BrowserServiceClient  implements Handler.Callback, Closeable {
    private static final Logger log = LoggerFactory.getLogger(BrowserServiceClient.class);
    private static final int DISCONNECTED = -1;
    private static int count = 0;
    private static Map<BrowserServiceClient, StackTraceElement[]> clients = new HashMap<>();
    private SynchronousQueue<Object> pendingMessage = new SynchronousQueue<>();
    private Context context;
    private OnReadyListener onReadyListener;
    private boolean serviceBound = false;
    private Messenger browserService;
    private Handler handler;
    private Messenger messenger;

    private final HandlerThread thread = new HandlerThread("BrowserServiceClient-" + count++) {
        @Override
        protected void onLooperPrepared() {
            handler = new Handler(getLooper(), BrowserServiceClient.this);
            messenger = new Messenger(handler);
            if (onReadyListener != null) {
                new Thread(onReadyListener::onReady).start();
            }
        }
    };

    // Used if the constructor that takes a Context is used
    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceBound = true;
            browserService = new Messenger(service);
            thread.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
            thread.quit();
        }
    };


    public BrowserServiceClient(Context context) {
        this(context, null);
    }

    public BrowserServiceClient(Context context, OnReadyListener onReadyListener) {
        this.context = context;
        this.onReadyListener = onReadyListener;
        context.bindService(new Intent(context, BrowserService.class), connection, Context.BIND_AUTO_CREATE | Context.BIND_ABOVE_CLIENT);
        clients.put(this, new Throwable().getStackTrace());
    }

    public synchronized void close() {
        if (serviceBound) {
            context.unbindService(connection);
            handler.sendEmptyMessage(DISCONNECTED);
            serviceBound = false;
            clients.remove(this);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    public boolean handleMessage(Message msg) {
        try {
            switch (msg.what) {
                case BrowserService.MSG_GET_BROWSER_STATUS_REPLY:
                    Boolean status = (msg.arg1 == BrowserService.BROWSER_STATUS_RUNNING);
                    pendingMessage.put(status);
                case BrowserService.MSG_GET_RECOGNIZER_VU_REPLY:
                    Integer vu = msg.arg1;
                    pendingMessage.put(vu);
                    break;
                case DISCONNECTED:
                    close();
                    break;
                default:
                    return false;
            }
        } catch (InterruptedException e) {
            // shutting down?
        }
        return true;
    }

    public synchronized boolean isBrowserStarted() throws InterruptedException, RemoteException {
        Message m = Message.obtain();
        m.what = BrowserService.MSG_GET_BROWSER_STATUS;
        m.replyTo = messenger;
        browserService.send(m);
        return (Boolean)getResponse();
    }

    public synchronized int getVU() throws InterruptedException, RemoteException {
        Message m = Message.obtain();
        m.what = BrowserService.MSG_GET_RECOGNIZER_VU;
        m.replyTo = messenger;
        browserService.send(m);
        return (Integer)getResponse();
    }

    public synchronized void stopBrowser() throws RemoteException {
        Message m = Message.obtain();
        m.what = BrowserService.MSG_STOP_BROWSER;
        browserService.send(m);
    }

    private Object getResponse() throws InterruptedException {
        Object reply = pendingMessage.take();
        return reply;
    }

    @FunctionalInterface
    public interface OnReadyListener {
        void onReady();
    }
}
