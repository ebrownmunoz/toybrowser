package com.voxware.browser.android.dagger;

import com.voxware.browser.android.audio.AndroidFragmentProviderFactory;
import com.voxware.browser.android.utils.VoxBrowserSettings;
import com.voxware.browser.applets.Applet;
import com.voxware.browser.applets.ConfigurePeripheral;
import com.voxware.browser.audioplayer.PromptResolver;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ehom on 10/15/16.
 */
@Module(includes = {AndroidModule.class})
public class BaseBrowserModule {
    @Provides
    @Named("serverUrl")
    public URI provideServerUrl(VoxBrowserSettings settings) {
        String url = settings.getServerUrl();
        if (!url.endsWith("/")) {
            url += "/";
        }
        return URI.create(url);
    }

    @Provides
    @Named("bootUri")
    public URI provideBootUri() {
        return URI.create("asset:///localboot.vxml");
    }

    @Provides
    @Named("defaultApplets")
    public Map<String, Applet> providesDefaultApplets() {
        Map<String, Applet> map = new HashMap<>();
        map.put("voxware.browser.applets.ConfigurePeripheral", new ConfigurePeripheral());
        return map;
    }
    @Provides
    @Singleton
    public PromptResolver.FragmentProviderFactory provideFragmentProvider(AndroidFragmentProviderFactory factory) {
        return factory;
    }
}
