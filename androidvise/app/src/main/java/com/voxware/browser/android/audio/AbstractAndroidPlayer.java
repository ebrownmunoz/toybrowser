package com.voxware.browser.android.audio;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;

import com.voxware.browser.audioplayer.AbstractPlayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * Created by edh on 6/7/2016.
 */
public abstract class AbstractAndroidPlayer<T> extends AbstractPlayer<T> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final Context context;
    private final AudioManager audioManager;

    public AbstractAndroidPlayer(@NonNull Context context) {
        this.context = context;
        this.audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
    }

    /**
     * Gets the stream id to use. For instance, STREAM_MUSIC is usually ok except when the bluetooth headset is connected. Then we want to use STREAM_VOICE_CALL.
     * @return STREAM_VOICE_CALL if BT headset is connected and in use, otherwise STREAM_MUSIC
     */
    protected int getStreamId() {
        Intent btIntent = context.registerReceiver(null, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED));
        if (audioManager.isBluetoothScoAvailableOffCall() &&
                (audioManager.isBluetoothA2dpOn() || audioManager.isBluetoothScoOn()) &&
                btIntent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, AudioManager.SCO_AUDIO_STATE_DISCONNECTED) == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
            //return AudioManager.STREAM_VOICE_CALL;
            // this is AudioManager.STREAM_BLUETOOTH_SCO, but is hidden from the compiler
            return 6;
        } else {
            return AudioManager.STREAM_MUSIC;
        }
    }

    /**
     * Gets the context used when creating this player
     * @return a context
     */
    public Context getContext() {
        return context;
    }

    @Override
    protected PlayerTask createPlayerTask(T playable) {
        return super.createPlayerTask(playable);
    }

    protected class AndroidPlayerTask extends PlayerTask implements AudioManager.OnAudioFocusChangeListener{
        // it is possible that done is called but run is not (cancellation)
        protected boolean changeListenerRegistered = false;
        /**
         * Constructs a new <code>PlayerTask</code> instance.
         *
         * @param callable
         */
        public AndroidPlayerTask(Callable<Void> callable) {
            super(callable);
        }

        @Override
        public void run() {
            audioManager.requestAudioFocus(this, getStreamId(), AudioManager.AUDIOFOCUS_GAIN);
            changeListenerRegistered = true;
            super.run();
        }

        @Override
        protected void done() {
            super.done();
            if (changeListenerRegistered) {
                audioManager.abandonAudioFocus(this);
            }
        }

        public void onAudioFocusChange(int focusChange) {
            log.warn("audio focus changed: " + focusChange);
        }
    }
}
