package com.voxware.browser.applets;

import com.voxware.browser.interpreter.Result;

import javax.inject.Inject;

/**
 * Created by edh on 12/7/2015.
 */
public class ModifyAudioHeader extends AbstractApplet {

    @Inject
    public ModifyAudioHeader() {
        super();
    }
    @Override
    public Result run() throws MissingParameterException {
        return Result.event("error.applet.unsupported", "");
//        // Get the parameters
//        byte[] image = require("image").as(byte[].class);
//        Scriptable[] map = require("map").as(Scriptable[].class);
//
//
//        // Acquire the old header
//        ByteArrayInputStream inputStream = new ByteArrayInputStream(image);
//        NISTHeader header = new NISTHeader(1);
//        if (!header.read(inputStream)) return Result.event("error.nist.badheader", "");
//        // Calculate the length of the image minus the header
//        int dataLength = image.length - header.length();
//
//        // Process the map
//        Object[] ids = map.getIds();
//        for (int index = 0; index < ids.length; index++) {
//            Object fieldParam = map.get(ids[index], map);
//            if (fieldParam != Undefined.instance && fieldParam instanceof FlattenedObject) {
//                FlattenedObject flatField = (FlattenedObject) fieldParam;
//                Object oldnameParam = flatField.getProperty("oldname");
//                Object newnameParam = flatField.getProperty("newname");
//                Object valueParam = flatField.getProperty("value");
//                Object typeParam = flatField.getProperty("type");
//
//                String oldname = (oldnameParam != Undefined.instance) ? Context.toString(oldnameParam) : null;
//                String newname = (newnameParam != Undefined.instance) ? Context.toString(newnameParam) : null;
//                if (oldname == null && newname == null)
//                    return Result.event("error.applet.missingparameter", "Both \"oldname\" and \"newname\" parameters missing from ModifyAudioHeader map");
//                String type = (typeParam != Undefined.instance) ? Context.toString(typeParam).toLowerCase() : null;
//                Object value = null;
//                if (valueParam != Undefined.instance) {
//                    if (type != null) {
//                        if (type.startsWith("i"))
//                            value = new Integer((int) Context.toNumber(valueParam));
//                        else if (type.startsWith("r"))
//                            value = new Float((float) Context.toNumber(valueParam));
//                        else if (type.startsWith("s")) value = Context.toString(valueParam);
//                        else
//                            return Result.event("error.applet.illegalparameter", "Unknown \"type\" parameter (\"" + type + "\") in ModifyAudioHeader map");
//                    } else {
//                        // Default to String
//                        value = Context.toString(valueParam);
//                    }
//                }
//
//                if (oldname != null) {
//                    if (newname != null) header.changeFieldName(oldname, newname);
//                    else if (value == null) header.remove(oldname);
//                    else newname = oldname;
//                } else if (newname != null && value == null) {
//                    return Result.event("error.applet.missingparameter", "\"value\" parameter for \"newname\" \"" + newname + "\" missing from ModifyAudioHeader map");
//                }
//                if (newname != null && value != null) header.addField(newname, value);
//            }
//        }
//
//        ByteArrayOutputStream headerImage = header.bytes();
//
//        // Construct a ByteArrayOutputStream with a buffer big enough to hold the image with its new header
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(dataLength + header.length());
//
//        // Write the header into it
//        outputStream.write(headerImage.toByteArray());
//        // Write the rest of the image into it
//        byte[] buffer = new byte[ChunkSize];
//        while (true) {
//            int byteCount = inputStream.read(buffer);
//            if (byteCount < 0) break;
//            outputStream.write(buffer, 0, byteCount);
//        }
//
//        results = outputStream.toByteArray();
//
//        inputStream.close();
//        outputStream.close();
    }
}
