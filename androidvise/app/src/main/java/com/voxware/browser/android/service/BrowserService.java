/**
 *
 */
package com.voxware.browser.android.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.http.HttpResponseCache;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.voxware.browser.android.R;
import com.voxware.browser.android.ScannerAdapter;
import com.voxware.browser.android.activity.VoxbrowserRunningActivity;
import com.voxware.browser.android.audio.DefaultAndroidStringPlayer;
import com.voxware.browser.android.dagger.AndroidModule;
import com.voxware.browser.android.dagger.DaggerNuanceViseBrowserComponent;
import com.voxware.browser.android.dagger.DaggerViseBrowserComponent;
import com.voxware.browser.android.service.bluetooth.BluetoothHeadsetService;
import com.voxware.vise.VISERecognizer;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author eric
 */
public class BrowserService extends Service {
    //private static final String TAG = "VOXWARE_BROWSER_SERVICE";
    private static final Logger log = LoggerFactory.getLogger(BrowserService.class);
    public static final String TAG = "VOXWARE_VOXBROWSER";
    public static final int NOTIFICATION_ID = 1001;
    public static final String ACTIVITY_EXTRA = "com.voxware.browser.android.service.BrowserService.extra.ACTIVITY";
    private boolean browserRunning = false;

    public static final String BROWSER_STOPPED = "com.voxware.browser.android.intent.action.BROWSER_STOPPED";
    private static final String LOGBACK_CONFIG = "logback.xml";
    private static final String DEFAULT_SERVER_ADDRESS = "http://localhost:8080/";

    public static final int MSG_IS_REPLY = 0x80000000;
    public static final int MSG_GET_BROWSER_STATUS       = 0x00000000;
    public static final int MSG_GET_BROWSER_STATUS_REPLY = MSG_GET_BROWSER_STATUS | MSG_IS_REPLY;

    public static final int MSG_GET_RECOGNIZER_VU       = 0x00000001;
    public static final int MSG_GET_RECOGNIZER_VU_REPLY = MSG_GET_RECOGNIZER_VU | MSG_IS_REPLY;

    public static final int MSG_STOP_BROWSER = 0x00000002; // void, no reply

    public static final int BROWSER_STATUS_NOT_RUNNING = 0;
    public static final int BROWSER_STATUS_RUNNING = 1;

    private final Messenger messenger = new Messenger(new BrowserHandler());

    public static final URI BOOT_URI = URI.create("asset:///localboot.vxml");
    private ScannerAdapter scannerReceiver;

    public enum BrowserState {
        STOPPED,
        STARTING,
        STARTED,
        STOPPING
    }

    private AtomicReference<BrowserState> state = new AtomicReference<>(BrowserState.STOPPED);
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private WifiManager wifiManager;
    private WifiManager.WifiLock wifiLock;
    private NotificationManager notificationManager;
    private PendingIntent notificationIntent;
    private Notification notification;
    private final BroadcastReceiver ttsListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DefaultAndroidStringPlayer.ACTION_SPEAK_START.equals(intent.getAction())) {
                if (notificationIntent != null) {
                    notification = buildNotification(notificationIntent, intent.getStringExtra(DefaultAndroidStringPlayer.EXTRA_TTS_TEXT));
                    notificationManager.notify(NOTIFICATION_ID, notification);
                }
            }
        }
    };

    private Browser browser;
    private Thread browserStartThread;
    private Closeable zebraScannerInitializer;

    private static final ServiceConnection HEADSET_CONNECTION =  new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // do nothing
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // also do nothing
        }
    };

    private static final ServiceConnection SCANNER_CONNECTION =  new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // do nothing
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // also do nothing
        }
    };

    public class BrowserServiceBinder extends android.os.Binder {
        public Browser getBrowser() {
            try {
                browserStartThread.join();
            } catch (InterruptedException e) {

            }
            return browser;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        wifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "VoxBrowser");
        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private Closeable runZebraScannerInitializer() {
        try {
            Class<?> klazz = Class.forName("com.voxware.browser.zebra.ZebraScannerInitializer");
            Constructor<?> constructor = klazz.getConstructor(Context.class);
            Object o = constructor.newInstance(this);
            Method method = klazz.getMethod("initialize");
            method.invoke(o);
            return (Closeable)o;
        } catch (Exception e) {
            log.debug("Failed to load ZebraScannerInitializer");
        }
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if (intent == null) {
            //stopSelf();
            return START_NOT_STICKY;
        }

        if (!state.compareAndSet(BrowserState.STOPPED, BrowserState.STARTING)) {
            log.info("Expected state to be STOPPED, but was " + state.get());
            return START_NOT_STICKY;
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(ttsListener, new IntentFilter(DefaultAndroidStringPlayer.ACTION_SPEAK_START));

        // scanner support
        zebraScannerInitializer = runZebraScannerInitializer();

        log.info("Setting up Scanning receiver");
        scannerReceiver = new ScannerAdapter();
        registerReceiver(scannerReceiver, ScannerAdapter.getIntentFilter());

        bindService(new Intent(this, BluetoothHeadsetService.class), HEADSET_CONNECTION, Context.BIND_IMPORTANT | Context.BIND_AUTO_CREATE);
        //bindService(new Intent(this, BluetoothScannerService.class), SCANNER_CONNECTION, Context.BIND_IMPORTANT | Context.BIND_AUTO_CREATE);
        installCache();

        Class<?> activityClass = (Class<?>) intent.getSerializableExtra(ACTIVITY_EXTRA);
        if (activityClass == null || !Activity.class.isAssignableFrom(activityClass)) {
            activityClass = VoxbrowserRunningActivity.class;
        }
        notificationIntent = PendingIntent.getActivity(this, 0, new Intent(this, activityClass), PendingIntent.FLAG_UPDATE_CURRENT);
        notification = buildNotification(notificationIntent, "browser is active");

        browserStartThread = new Thread(this::startBrowser, "Browser");
        browserStartThread.start();

        if (!state.compareAndSet(BrowserState.STARTING, BrowserState.STARTED)) {
            log.warn("Expected state to be {}, but was {}", BrowserState.STARTING, state.get());
        }
        return START_NOT_STICKY;
    }

    /**
     * The Android system will only call this method once.
     * If addition components bind, it will return the
     * cached Binder.
     */
    @Override
    public IBinder onBind(Intent arg0) {
        return messenger.getBinder();
    }

    @Override
    public void onDestroy() {
        if (state.compareAndSet(BrowserState.STARTED, BrowserState.STOPPING)) {
            log.info("Stopping browser service");
            browser.stop();
            IOUtils.closeQuietly(zebraScannerInitializer);
            //unbindService(SCANNER_CONNECTION);
            unbindService(HEADSET_CONNECTION);

            LocalBroadcastManager.getInstance(this).unregisterReceiver(ttsListener);
            //PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), BrowserIOActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
            //pi.cancel();
            notificationIntent.cancel();
            if (wakeLock != null && wakeLock.isHeld()) {
                wakeLock.release();
                wifiLock.release();
                stopForeground(true);
            }

            if (scannerReceiver != null) {
                unregisterReceiver(scannerReceiver);
            }
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(BROWSER_STOPPED));
            state.compareAndSet(BrowserState.STOPPING, BrowserState.STOPPED);
        } else {
            log.info("Browser service is being destroyed, but was never started, so nothing to do");
        }
        super.onDestroy();
    }

    protected void installCache() {
        if (HttpResponseCache.getInstalled() != null) {
            return;
        }
        File cacheDir = new File(getCacheDir(), "http-cache");
        deleteRecursive(cacheDir); // clear the existing cache files
        int cacheSize = 50 * 1024 * 1024;
        try {
            HttpResponseCache cache = HttpResponseCache.install(cacheDir, cacheSize);
            cache.flush();
            log.info("Installed cache at " + cacheDir.getAbsolutePath() + " with size " + cacheSize);
        } catch (IOException e) {
            log.warn("Error installing HttpResponseCache, caching may not work", e);
        }
    }

    private void deleteRecursive(File dir) {
        if (dir.isDirectory()) {
            for (File child : dir.listFiles()) {
                deleteRecursive(child);
            }
        }
        dir.delete();
    }

    protected void startBrowser() {
        if (!wakeLock.isHeld()) {
            wakeLock.acquire();
            wifiLock.acquire();
            startForeground(NOTIFICATION_ID, notification);
        }

        try {
            browser = DaggerNuanceViseBrowserComponent.builder().androidModule(new AndroidModule(this)).build().getBrowser();
        } catch (Exception e) {
            log.error("Failed to initialize with Vocalizer, defaulting to built-in", e);
            browser = DaggerViseBrowserComponent.builder().androidModule(new AndroidModule(this)).build().getBrowser();
        }
        scannerReceiver.setInterruptHandler(browser);
        browser.start();
        log.info("Browser started");
        state.set(BrowserState.STARTED);
    }

    protected void stopBrowser() {
        browser.stop();
    }

    protected Notification buildNotification(PendingIntent intent, String text) {
        return new NotificationCompat.Builder(this)
                .setContentTitle("Voxware Browser")
                .setContentText(text)
                .setSmallIcon(R.drawable.recognizing_stub)
                .setOngoing(true)
                .setContentIntent(intent)
                .build();
    }

    protected class BrowserHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Message reply = null;
            switch (msg.what) {
                case MSG_GET_BROWSER_STATUS:
                    if (msg.replyTo != null) {
                        reply = Message.obtain();
                        reply.what = MSG_GET_BROWSER_STATUS_REPLY;
                        reply.arg1 = state.get() == BrowserState.STARTED ? BROWSER_STATUS_RUNNING : BROWSER_STATUS_NOT_RUNNING;
                        try {
                            msg.replyTo.send(reply);
                        } catch (RemoteException e) {
                            log.error("RemoteException sending reply for MSG_GET_BROWSER_STATUS", e);
                        }
                    }
                    break;
                case MSG_GET_RECOGNIZER_VU:
                    if (msg.replyTo != null) {
                        reply = Message.obtain();
                        reply.what = MSG_GET_RECOGNIZER_VU_REPLY;
                        reply.arg1 = VISERecognizer.getInstance().getPower();
                        try {
                            msg.replyTo.send(reply);
                        } catch (RemoteException e) {
                            log.error("RemoteException sending reply for MSG_GET_RECOGNIZER_VU", e);
                        }
                    }
                    break;
                case MSG_STOP_BROWSER:
                    stopBrowser();
                    break;
                default:
                    super.handleMessage(msg);
            }

        }
    }

}
