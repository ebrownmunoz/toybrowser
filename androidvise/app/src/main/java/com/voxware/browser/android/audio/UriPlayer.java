package com.voxware.browser.android.audio;

import android.content.Context;
import android.media.MediaPlayer;

import com.voxware.browser.audioplayer.AudioPlayerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URI;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

/**
 * Created by edh on 6/2/2016.
 */
public class UriPlayer extends AbstractAndroidPlayer<URI> implements com.voxware.browser.io.UriPlayer {
    private static final Logger log = LoggerFactory.getLogger(UriPlayer.class);

    @Inject
    public UriPlayer(Context context) {
        super(context);
    }

    @Override
    protected ThreadFactory createThreadFactory() {
        return new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "UriPlayer");
            }
        };
    }

    @Override
    protected Callable<Void> createCallable(URI playable) {
        if (playable.getScheme().equals("file") && playable.getPath().startsWith("/opt/voxware")) {
            String path = playable.getPath().substring("/opt/voxware/".length());
            File audioFile = new File(getContext().getExternalFilesDir(null), path);
            if (!audioFile.exists()) {
                audioFile = new File(getContext().getFilesDir(), path);
            }
            return new MediaCallable(audioFile.toURI());
        }
        return new MediaCallable(playable);
    }

    protected class MediaCallable implements Callable<Void>, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
        private final android.net.Uri uri;
        private final CountDownLatch signal = new CountDownLatch(1);
        private final AtomicInteger status = new AtomicInteger(0);
        private MediaPlayer mediaPlayer;

        /**
         * Added in case android.net.Uri is ever supported
         * @param uri
         */
        public MediaCallable(android.net.Uri uri) {
            this.uri = uri;
        }

        public MediaCallable(URI uri) {
            this.uri = android.net.Uri.parse(uri.toString());
        }
        @Override
        public Void call() throws Exception {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
            mediaPlayer.setDataSource(getContext(), this.uri);
            mediaPlayer.setAudioStreamType(getStreamId());
            mediaPlayer.prepare();
            mediaPlayer.setLooping(false);
            mediaPlayer.start();
            try {
                signal.await();
            } catch (InterruptedException e) {
                mediaPlayer.stop();
            } finally {
                mediaPlayer.release();
            }
            if (status.get() != 0) {
                throw new AudioPlayerException("Error occurred during playback: " + status.get());
            }
            return null;
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            signal.countDown();
        }

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            log.error("MediaPlayer reports error {}, {}", what, extra);
            status.set(extra);
            signal.countDown();
            return true;
        }
    }
}
