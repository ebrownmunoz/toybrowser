package com.voxware.browser.android.dagger;

import android.content.Context;

import com.voxware.browser.android.vise.VisePropertiesBridge;
import com.voxware.browser.android.vise.ViseRecognitionAdapter;
import com.voxware.browser.applets.Applet;
import com.voxware.browser.applets.Calibrate;
import com.voxware.browser.applets.EnrollWord;
import com.voxware.browser.applets.GetGrammar;
import com.voxware.browser.applets.GetSpeakerProfile;
import com.voxware.browser.applets.GrammarRules;
import com.voxware.browser.applets.ModifyAudioHeader;
import com.voxware.browser.applets.TrainUtterance;
import com.voxware.browser.applets.TrainUtterances;
import com.voxware.browser.applets.TrainingPrompts;
import com.voxware.browser.applets.Vocabulary;
import com.voxware.browser.io.Recognizer;
import com.voxware.vise.VISERecognizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.speech.PropertyVetoException;

import dagger.Module;
import dagger.Provides;
import voxware.engine.recognition.sapivise.ViseProperties;

/**
 * Created by edh on 10/13/2016.
 */
@Module(includes = {AndroidModule.class, DocumentServerModule.class})
public class ViseModule {

    public static final Logger log = LoggerFactory.getLogger(ViseModule.class);

    @Provides
    @Singleton
    public Recognizer providesRecognizer(VISERecognizer vise) {
        return new ViseRecognitionAdapter(vise);
    }

    @Provides
    @Singleton
    public ViseProperties providesViseProperties(Context context, VISERecognizer vise) {
        return vise.getViseProperties();
    }

    @Provides
    @Named("defaultRecognizerSessionVariables")
    public Map<String, Object> providesViseSessionObject(ViseProperties props) {
        Map<String, Object> ret =  new HashMap<>();
        ret.put("vise", new VisePropertiesBridge(props));
        return ret;
    }

    @Provides
    @Named("defaultRecognizerApplets")
    public Map<String, Applet> providesDefaultRecognizerApplets(
            Calibrate calibrate,
            EnrollWord enrollWord,
            GetGrammar getGrammar,
            GetSpeakerProfile getSpeakerProfile,
            GrammarRules grammarRules,
            ModifyAudioHeader modifyAudioHeader,
            TrainingPrompts trainingPrompts,
            TrainUtterance trainUtterance,
            TrainUtterances trainUtterances,
            Vocabulary vocabulary) {
        Map<String, Applet> ret = new HashMap<>();
        ret.put("voxware.browser.applets.Calibrate", calibrate);
        ret.put("voxware.browser.applets.EnrollWord", enrollWord);
        ret.put("voxware.browser.applets.GetGrammar", getGrammar);
        ret.put("voxware.browser.applets.GetSpeakerProfile", getSpeakerProfile);
        ret.put("voxware.browser.applets.GrammarRules", grammarRules);
        ret.put("voxware.browser.applets.ModifyAudioHeader", modifyAudioHeader);
        ret.put("voxware.browser.applets.TrainingPrompts", trainingPrompts);
        ret.put("voxware.browser.applets.TrainUtterance", trainUtterance);
        ret.put("voxware.browser.applets.TrainUtterances", trainUtterances);
        ret.put("voxware.browser.applets.Vocabulary", vocabulary);
        return ret;
    }

    @Provides
    @Singleton
    public VISERecognizer providesVise(Context context) {
        VISERecognizer recognizer = VISERecognizer.getInstance();
        ViseProperties props = recognizer.getViseProperties();
        try {
            props.setSpeakerPath(context.getFilesDir().getAbsolutePath());
            // this is vise.saveresults
            props.setTrainingProvided(true);
            props.setAudQueueLen(20000);
            return recognizer;
        } catch (PropertyVetoException e) {
            log.error("Failed to set property", e);
            throw new RuntimeException(e);
        }
    }
}
