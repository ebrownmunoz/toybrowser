package com.voxware.browser.android.documentserver;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;

import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.DocumentServerInputStream;
import com.voxware.browser.documentserver.SchemeHandler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;

/**
 * Created by edh on 10/28/2015.
 */
public class AssetSchemeHandler implements SchemeHandler {

    private final AssetManager assetManager;

    public AssetSchemeHandler(AssetManager assetManager) {
        this.assetManager = assetManager;
    }
    @Override
    public DocumentServerInputStream handleScheme(URI uri, Object parameters) throws DocumentServerException {
        String path = uri.getPath();
        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        AssetFileDescriptor fd = null;
        long length = -1;
        try {
            fd = assetManager.openFd(path);
            length = fd.getLength();
            fd.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            if (fd != null) {
                try {
                    fd.close();
                } catch (IOException e) {}
            }
        }

        try {
            return new DocumentServerInputStream(assetManager.open(path), length);
        } catch (IOException e) {
            throw new DocumentServerException("Error opening asset " + path, e);
        }
    }
}
