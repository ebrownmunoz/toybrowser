package com.voxware.browser.android.vise;

import com.voxware.vise.VISERecognitionService;

import voxware.engine.recognition.sapivise.ViseProperties;

/**
 * Created by edh on 2/5/2016.
 */
public class VisePropertiesBridge {
    public class WildcardBridge {
        public int getAbsolute() {
            return props.getAbsWildcard();
        }

        public int getRelative() {
            return props.getRelWildcard();
        }
    }
    public class ThresholdBridge {
        public int getMutha() {
            return props.getMuthaThresh();
        }

        public int getLoose() {
            return props.getLooseThresh();
        }

        public int getTight() {
            return props.getTightThresh();
        }
    }
    private final ViseProperties props;
    private final WildcardBridge wildcardBridge = new WildcardBridge();
    private final ThresholdBridge thresholdBridge = new ThresholdBridge();

    public VisePropertiesBridge(ViseProperties visePropertiers) {
        props = visePropertiers;
    }

    public WildcardBridge getWildcard() {
        return wildcardBridge;
    }

    public ThresholdBridge getThreshold() {
        return thresholdBridge;
    }
}
