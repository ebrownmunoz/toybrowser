package com.voxware.browser.applets;

import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import javax.inject.Inject;

import voxware.engine.recognition.sapivise.SapiVise;

/**
 * Created by edh on 11/19/2015.
 */
public class Calibrate extends AbstractApplet {
    private final SapiVise vise;

    @Inject
    public Calibrate(VISERecognizer recognizer) {
        this.vise = recognizer.getSapiVise();
    }

    @Override
    public Result run() throws Exception {
        boolean success = vise.calibrate();
        Result result = Result.Ok();
        result.setValue(success);
        return result;
    }
}
