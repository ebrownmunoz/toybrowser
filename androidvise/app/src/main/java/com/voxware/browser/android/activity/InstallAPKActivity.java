package com.voxware.browser.android.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.voxware.browser.android.R;
import com.voxware.browser.android.deployment.DeploymentManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

import static com.voxware.browser.android.deployment.DeploymentManager.EXTRA_APK_FILE;

public class InstallAPKActivity extends AppCompatActivity {

    //public static final String EXTRA_APK_FILE = "com.voxware.browser.android.extra.apk_file";
    private static final Logger log = LoggerFactory.getLogger(InstallAPKActivity.class);

    private File apkFile;
    private boolean checkInstall = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install_apk);
        Intent intent = getIntent();
        apkFile = (File)intent.getSerializableExtra(DeploymentManager.EXTRA_APK_FILE);
        if (apkFile == null) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_APK_FILE, apkFile);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkInstall) {
            if (verifyInstall()) {
                installSuccessful();
            } else {
                new AlertDialog.Builder(this)
                        .setMessage("You must allow the APK to install to continue.")
                        .setPositiveButton("OK", (dialog, which) -> {
                           doInstall();
                        })
                        .create().show();
            }
        } else {
            checkInstall = true;
            doInstall();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            installSuccessful();
//        } else {
//            // failed?
//            doInstall();
//        }
    }

    private boolean verifyInstall() {
        PackageInfo info = getPackageManager().getPackageArchiveInfo(apkFile.getAbsolutePath(), 0);
        List<PackageInfo> installedPackages = getPackageManager().getInstalledPackages(0);
        for (PackageInfo installedPackage : installedPackages) {
            if (equals(info, installedPackage)) {
                return true;
            }
        }
        return false;
    }

    private void doInstall() {
        // do the install
        if (isZebra()) {

        } else {
            doRegularInstall();
        }

    }

    private boolean isZebra() {
        return false;
    }

    protected void doRegularInstall() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Compares two PackageInfo objects for equality. Equality is currently defined to us as same packageName, versionName, and versionCode.
     * @param a the first
     * @param b the second
     * @return true if the A equals B, false otherwise
     */
    private boolean equals(PackageInfo a, PackageInfo b) {
        if (a.versionCode != b.versionCode) {
            return false;
        }
        if (!a.packageName.equals(b.packageName)) {
            return false;
        }
        if (!a.versionName.equals(b.versionName)) {
            return false;
        }
        return true;
    }

    private void installSuccessful() {
        Intent result = new Intent();
        result.setAction(DeploymentManager.ACTION_APK_DEPLOYED);
        result.putExtra(EXTRA_APK_FILE, apkFile);
        LocalBroadcastManager.getInstance(this).sendBroadcast(result);
        finish();
    }
}
