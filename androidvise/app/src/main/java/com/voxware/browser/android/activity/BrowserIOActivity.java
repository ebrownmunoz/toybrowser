package com.voxware.browser.android.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import com.voxware.browser.android.R;
import com.voxware.browser.android.service.Browser;
import com.voxware.browser.android.service.BrowserService;
import com.voxware.browser.io.RecognitionResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Activity to run the browser. This provides a scroll
 * pane for prompts (i.e. TTS) and a Text box to enter data (i.e.
 * recognizer).
 *
 * @author eric
 */
public class BrowserIOActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(BrowserIOActivity.class);
    private Browser browser = null;
    private EditText editText = null;


    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log.info("Creating Activity");
        setContentView(R.layout.activity_main);
        editText = (EditText)this.findViewById(R.id.editText1);
        Intent bindIntent = new Intent(this, BrowserService.class);
        this.bindService(bindIntent, browserConnection, Context.BIND_ABOVE_CLIENT);
        startBrowserService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.unbindService(browserConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private final ServiceConnection browserConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof BrowserService.BrowserServiceBinder) {
                BrowserService.BrowserServiceBinder browserServiceBinder = (BrowserService.BrowserServiceBinder)service;
                browser = browserServiceBinder.getBrowser();
            }
            // otherwise, who called us?
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            browser = null;
        }
    };


    public void sendBurp(View view) {
        if (browser != null) {
            String text = editText.getText().toString();
            browser.getRecognizer().sendInterrupt(RecognitionResult.success(text));
        }
    }

    protected void startBrowserService() {
        Intent browserServiceIntent = new Intent(getApplicationContext(), BrowserService.class);
        browserServiceIntent.putExtra(BrowserService.ACTIVITY_EXTRA, this.getClass());
        startService(browserServiceIntent);
    }

}

