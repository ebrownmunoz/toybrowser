package com.voxware.browser.applets;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.interpreter.Result;
import com.voxware.vise.VISERecognizer;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;

import javax.inject.Inject;

import voxware.engine.recognition.sapivise.SapiViseGrammar;

/**
 * Created by edh on 12/7/2015.
 */
public class GrammarRules extends AbstractApplet {

    private final VISERecognizer viseRecognitionService;
    private final DocumentServer documentServer;

    @Inject
    public GrammarRules(VISERecognizer vise, DocumentServer documentServer) {
        this.viseRecognitionService = vise;
        this.documentServer = documentServer;
    }

    @Override
    public Result run() throws Exception {

        String url = require("grammar").asString();
        String text = require("text").asString();

        // Interpret the parameters
        if (url.isEmpty())
            return Result.event("error.applet.missingparameter", "\"grammar\" parameter of GrammarRules applet missing or empty");
        if (text.isEmpty())
            return Result.event("error.applet.missingparameter", "\"text\" parameter of GrammarRules applet missing or empty");
        if (url.contains("#")) {
            url = url.substring(0, url.indexOf("#"));
        }
        // Get the SapiViseGrammar
        InputStream is = null;
        byte[] bytes = null;
        Map<Object, Object> cache = getSessionContext().getCache();
        if (cache.containsKey(url)) {
            bytes = (byte[]) cache.get(url);
            viseRecognitionService.loadGrammar(url, bytes, false);
        } else {
            try {
                is = documentServer.getDocument(new URI(url), null, CacheControl.NO_CACHE);
                bytes = IOUtils.toByteArray(is);
                viseRecognitionService.loadGrammar(url, bytes, true);
                cache.put(url, bytes);
            } catch (URISyntaxException e) {
                return Result.event("error.badfetch.malformedurl", "The url " + url + " is malformed");
            } catch (IOException e) {
                return Result.event("error.badfetch.ioerror", e.getMessage());
            } finally {
                IOUtils.closeQuietly(is);
            }

        }

        javax.speech.recognition.Grammar grammar = viseRecognitionService.getCurrentGrammar();
        if (!(grammar instanceof SapiViseGrammar))
            return Result.event("error.badgrammar", "\"" + url + "\" is a " + grammar.getClass().getName() + ", not a SapiViseGrammar");
        SapiViseGrammar viseGrammar = (SapiViseGrammar) grammar;

        // Get the names of the rules that parse the given text
        String[] ruleNames = viseGrammar.parse(text);
        if (ruleNames == null) ruleNames = new String[0];

        // Wrap the rule names in a JavaScript Array
        Result ret = Result.Ok();
        ret.setValue(getContext().newArray(getContext().initStandardObjects(), Arrays.copyOf(ruleNames, ruleNames.length, Object[].class)));
        return ret;
    }
}
