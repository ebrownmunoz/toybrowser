package com.voxware.browser.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.voxware.browser.android.activity.VoxbrowserStartActivity;

import java.io.File;
import java.io.IOException;

/**
 * Created by edh on 11/18/2016.
 */

public class PackageReplacedListener extends BroadcastReceiver {
    public static final String TAG = "VOXWARE";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED) &&
                intent.getDataString().contains("com.voxware.browser.android")) {
            Log.d(TAG, "Received intent ACTION_PACKAGE_REPLACED");
            Log.d(TAG, "Data is " + intent.getDataString());
            Bundle extras = intent.getExtras();
            if (extras != null) {
                for (String key : extras.keySet()) {
                    Object value = extras.get(key);
                    Log.d(TAG, String.format("Extra[%s] = %s", key, value == null ? "null" : value.toString()));
                }
            }

            File startImmediatelyFile = new File(context.getFilesDir(), "startImmediately");
            try {
                Log.d(TAG, String.format("Creating semaphore %s", startImmediatelyFile.getAbsolutePath()));
                startImmediatelyFile.createNewFile();
            } catch (IOException e) {
                Log.d(TAG, "Error creating semaphore file", e);
            }
            Intent startIntent = new Intent(context, VoxbrowserStartActivity.class);
            //startIntent.putExtra(VoxbrowserStartActivity.EXTRA_START_IMMEDIATELY, true);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startIntent);
        }
    }
}
