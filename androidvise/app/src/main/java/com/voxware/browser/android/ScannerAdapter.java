package com.voxware.browser.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by eric on 2/4/16.
 */
public class ScannerAdapter extends BroadcastReceiver {

    private final static String DEFAULT_ACTION = "";
    public final static String ACTION = "com.voxware.scan";
    public final static String DATA_STRING_TAG = "com.motorolasolutions.emdk.datawedge.data_string";
    private final static String SCANNER_INTERRUPT_SOURCE = "scanner";

    private static Logger log = LoggerFactory.getLogger(ScannerAdapter.class);

    // @guardedBy(this)
    private InterruptHandler interruptHandler=null;


    public static IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION);
        return intentFilter;

    }


    public synchronized void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String data = intent.getStringExtra(DATA_STRING_TAG);
        log.info("received scan! " + data + " action " + action);

        if (interruptHandler != null) {
            log.info("raising interrupt");
            interruptHandler.sendInterrupt(SCANNER_INTERRUPT_SOURCE, data);
        } else {
            log.error("null interruptManager!");
        }
    }

    public synchronized void setInterruptHandler(InterruptHandler interruptHandler) {
        this.interruptHandler = interruptHandler;
    }


}
