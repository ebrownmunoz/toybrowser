package com.voxware.browser.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.voxware.browser.android.BuildConfig;
import com.voxware.browser.android.R;
import com.voxware.browser.android.deployment.DeploymentManager;
import com.voxware.cas.CasService;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class VoxbrowserStartActivity extends AppCompatActivity {

    public static final String EXTRA_START_IMMEDIATELY = "com.voxware.browser.android.extra.start_immediately";
    private Logger log = LoggerFactory.getLogger(VoxbrowserStartActivity.class);
    private boolean startImmediately = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        log.info("Version: {}", BuildConfig.VERSION_NAME);
        log.info("Build Date: {} ORG", sdf.format(BuildConfig.BUILD_DATE));
        log.info("Commit: {}", BuildConfig.GIT_COMMIT);

        setContentView(R.layout.voxbrowser_start);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        //Configure log (if available)
        File logFileConfig = new File (getFilesDir(), "logback.xml");
        if (logFileConfig.exists()) {
            try {
                DeploymentManager.resetLogger(logFileConfig);
            } catch (Exception e) {
                log.error("Error resetting Logger", e);
            }
        }

        /*
        Remove the app title in the toolbar, will replace in layout with the Voxware logo.
         */
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        expandConfirmationWav();
        startService(new Intent(getApplicationContext(), CasService.class));
//        startImmediately = getIntent().getBooleanExtra(EXTRA_START_IMMEDIATELY, false);
        // clear the intent
//        getIntent().putExtra(EXTRA_START_IMMEDIATELY, false);
//        log.info("Start Immediately: {}", startImmediately);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_voxbrowser_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, BrowserSettings.class);
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void expandConfirmationWav() {
        InputStream is = null;
        FileOutputStream fos = null;
        try {
            is = getAssets().open("audio/confirmation.wav");
            File destDir = new File(getFilesDir(), "audio");
            if (!destDir.exists()) {
                destDir.mkdirs();
            }
            File destFile = new File(destDir, "confirmation.wav");
            fos = new FileOutputStream(destFile);
            IOUtils.copy(is, fos);
        } catch (IOException e) {
            log.error("Failed to expand confirmation.wav");
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(fos);
        }
    }
}
