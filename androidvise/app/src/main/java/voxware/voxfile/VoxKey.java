// VoxKey -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxKey extends VoxBlock {

   // Instance Fields and their access method
  private long ulMask;
  public long getMask() { return ulMask;}
  public void setMask(long m) {ulMask = m;}

   // Constructors
  public VoxKey() {
    blockID = KEY;
    ulMask = 0xF;    // Our current default
  }

   // Instance methods

   // Package scope
  long bodySize() {return 4;} // size of V_ULong

   // Package scope. blockID must have been read !!!
  VoxKey read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Early check on the fixed length object
      if (blockSize != bodySize()) throw new VoxFormatException("VoxKey.read: expected size = 4, but read " + blockSize);
      // Read object body
      ulMask = vis.readULong();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxKey.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    vos.writeULong(ulMask);
  }

  public String toString () {
    return super.toString() + "VoxKey: Mask = " + ulMask + "\n\n";
  }
}
