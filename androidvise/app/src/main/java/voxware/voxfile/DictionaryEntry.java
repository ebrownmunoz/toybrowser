// DictionaryEntry - component of VoxDictionary block

package voxware.voxfile;

import java.io.*;

public class DictionaryEntry {

   // Class Methods. Package scope.
  static DictionaryEntry read(VoxInputStream vis) throws IOException, VoxFormatException {
    DictionaryEntry entry = new DictionaryEntry();
    try {
      entry.loWordName = LenOff.read(vis);
      entry.loPhoneString = LenOff.read(vis);
      entry.slAltPronProb = vis.readSLong();
      entry.usUseContext = vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("DictionaryEntry.read: Unexpected EOF");
    }
    return entry;
  }

  static long bodySize() {return 2 * LenOff.bodySize() + 4 + 2;}

   // Instance Fields and their access method
  private LenOff loWordName;
  public  LenOff getWordName() {return loWordName;}
  public  void   setWordName(LenOff lo) {loWordName = lo;}

  private LenOff loPhoneString;
  public  LenOff getPhoneString() {return loPhoneString;}
  public  void   setPhoneString(LenOff lo) {loPhoneString = lo;}

  private int    slAltPronProb;
  public  int    getAltPronProb() {return slAltPronProb;}
  public  void   setAltPronProb(int i) {slAltPronProb = i;}

  private int    usUseContext;
  public  int    getUseContext() {return usUseContext;}
  public  void   setUseContext(int i) {usUseContext = i;}

   // Constructor
  public  DictionaryEntry() {} 

   // Package scope
  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    loWordName.write(vos);
    loPhoneString.write(vos);
    vos.writeSLong(slAltPronProb);
    vos.writeUns(usUseContext);
  }

  public String toString () {
    StringBuffer buffer = StringFormatter.appendTo("  Word: ", 32, VoxBlock.getText(loWordName), ";");
    buffer.append("Transcription: " + VoxBlock.getText(loPhoneString) + "\n");
    return buffer.toString();
  }
}
