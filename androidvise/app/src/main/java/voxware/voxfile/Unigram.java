// Unigram - component of LanguageModel

package voxware.voxfile;

import java.io.*;

public class Unigram {

   // Class Methods. Package scope
  static Unigram read(VoxInputStream vis) throws IOException, VoxFormatException {
    Unigram ug = new Unigram();
    try {
      ug.slProbability = vis.readSLong();
      ug.slBackoffWeight = vis.readSLong();
      ug.usWordID = vis.readUns();
      ug.usNodeID = vis.readUns();
      int n = vis.readUns();
      ug.bgaFollowers = new Bigram[n];
      for (int i = 0; i < n; i++)
        ug.bgaFollowers[i] = Bigram.read(vis);
    } catch (EOFException e) {
      throw new VoxFormatException("Unigram.read: Unexpected EOF");
    }
    return ug;
  }

   // Instance Fields and their access methods

  private int  slProbability;
  public  int  getProbability() {return slProbability;}
  public  void setProbability(int i) {slProbability = i;}

  private int  slBackoffWeight;
  public  int  getBackoffWeight() {return slBackoffWeight;}
  public  void setBackoffWeight(int i) {slBackoffWeight = i;}

  private int  usWordID;
  public  int  getWordID() {return usWordID;}

  private int  usNodeID;
  public  int  getNodeID() {return usNodeID;}

  private Bigram[] bgaFollowers;
  public  Bigram[] getFollowers() {return bgaFollowers;}

  public  int  getNumSuccessors() {return bgaFollowers.length;}

   // Constructor. Package scope for read
  Unigram() {}

   // Public constructor
  public Unigram(int wordID, int nodeID, Bigram[] followers) {
    usWordID = wordID;
    usNodeID = nodeID;
    bgaFollowers = followers;
  }

   // Instance methods

   // Package scope
  long bodySize() {return 2*4 + 3*2 + Bigram.bodySize() * bgaFollowers.length;}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    vos.writeSLong(slProbability);
    vos.writeSLong(slBackoffWeight);
    vos.writeUns(usWordID);
    vos.writeUns(usNodeID);
    vos.writeUns(bgaFollowers.length);
    for (int i = 0; i < bgaFollowers.length; i++)
      bgaFollowers[i].write(vos);
  }
}
