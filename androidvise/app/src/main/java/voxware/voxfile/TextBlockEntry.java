package voxware.voxfile;

public class TextBlockEntry extends LenOff {
  private String string;

  public TextBlockEntry(String string) {
    super();
    this.string = string;
    try {
      super.setLen(string.getBytes(VoxFile.WORD_ENCODING).length);
    } catch (Exception e) {}
  }

  public String getString() {
    return this.string;
  }

  public void setString(String string) {
    super.setLen(string.length());
    this.string = string;
  }

  public String toString() {
    return string;
  }
}
