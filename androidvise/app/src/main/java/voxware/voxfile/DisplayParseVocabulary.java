// DisplayParseVocabulary -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class DisplayParseVocabulary extends ParseTable {

   // Constructor for VoxBlock.read()
  DisplayParseVocabulary() {blockID = DISPLAY_PARSE_VOCAB;}

   // Constructor for Java Convert writers. Should check arguments consistency.
  public DisplayParseVocabulary(int vocabularyID) {
    super(vocabularyID, null, null);
    blockID = DISPLAY_PARSE_VOCAB;
  }

  public DisplayParseVocabulary(int vocabularyID, long[] offsets, int[] data) {
    super(vocabularyID, offsets, data);
    blockID = DISPLAY_PARSE_VOCAB;
  }

  public String toString() {
    return super.toString() + "DisplayParseVocabulary: State Count = " + ulaOffsets.length + "\n\n";
  }
}
