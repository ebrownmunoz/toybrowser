// VoiceParseVocabulary -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class VoiceParseVocabulary extends ParseTable {

   // Constructor for VoxBlock.read()
  VoiceParseVocabulary() {blockID = VOICE_PARSE_VOCAB;}

   // Constructors for Java Convert writers. Should check arguments consistency.
  public VoiceParseVocabulary(int vocabularyID) {
    super(vocabularyID, null, null);
    blockID = VOICE_PARSE_VOCAB;
  }

  public VoiceParseVocabulary(int vocabularyID, long[] offsets, int[] data) {
    super(vocabularyID, offsets, data);
    blockID = VOICE_PARSE_VOCAB;
  }

  public String toString() {
    return super.toString() + "VoiceParseVocabulary: State Count = " + ulaOffsets.length + "\n\n";
  }
}
