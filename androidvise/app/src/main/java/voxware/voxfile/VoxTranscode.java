// Voice file word name recoding utility

package voxware.voxfile;

public class VoxTranscode {

   // Main
  public static void main(String argv[]) {
    int i, j;

    if (argv.length != 3) {
      System.out.println("Usage: java VoxTranscode SrcVoiceFileName SrcEncoding DstVoiceFileName");
      return;
    }

    VoxFile vf = new VoxFile();

    try {
      vf.read(argv[0]);
      if (!vf.isVoiceFile()) {
        System.out.println("Source File must be Voice File");
        return;
      }
      String defaultEncoding = VoxFile.WORD_ENCODING;
      System.out.println("Encoding words in " + defaultEncoding);
      UserPatterns up = vf.getUserPatterns();;
      VoxPattern[] vpa = up.getPatterns();
      for (i = 0; i < vpa.length; i++) {
        VoxFile.WORD_ENCODING = argv[1];
        String wordName = vpa[i].getWordName();
        System.out.print(wordName + " (" + VoxFile.WORD_ENCODING + "):");
        byte[] ba = vpa[i].getWordBytes();
        for (j = 0; j < ba.length; j++)
          System.out.print(" " + Integer.toOctalString(0xff & ba[j]));
        VoxFile.WORD_ENCODING = defaultEncoding;
        vpa[i].setWordName(wordName);
        System.out.print(" -> (" + VoxFile.WORD_ENCODING + "):");
        ba = vpa[i].getWordBytes();
        for (j = 0; j < ba.length; j++)
          System.out.print(" " + Integer.toOctalString(0xff & ba[j]));
        System.out.println(";");
      }
      vf.write(argv[2]);
    } catch (Throwable t) {
      System.out.println("VoxDump: caught Throwable " + t.toString() + ", reading file " + argv[0]);
      t.printStackTrace();
    }
  }
}
