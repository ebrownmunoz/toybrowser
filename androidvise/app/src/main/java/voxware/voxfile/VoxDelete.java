// Word model deletion service

package voxware.voxfile;

import java.io.*;
import java.util.*;

public class VoxDelete {

  /**
   * Method deletes from source all word models with wordNames comprising "names" (keys) in the specified Properties parameter.
   * Remaining models are saved in destination. "values" in the Properties parameter are irrelevant.
   * Returns the number of actually deleted words
   */
  public static int deleteWords(Properties names, InputStream source, OutputStream desination) throws IOException, VoxFormatException {
    int n = 0;

    if (names.size() == 0) return 0;
    // Source VoiceFile
    VoxFile vf = new VoxFile();
    vf.read(source);
    if (!vf.isVoiceFile()) throw new VoxFormatException("VoxDelete: source is not a Voice File");
    UserPatterns up = vf.getUserPatterns();;
    // Do patterns
    VoxPattern[] vpa = up.getPatterns();
    for (int i = 0; i < vpa.length; i++)
      if (names.containsKey(vpa[i].getWordName())) {
        vpa[i] = null;
        n++;
      }
    if (up.getNumberPatterns() > 0) {
      up.setWildcardSum(0);
      up.setWildcardSumSquares(0);
      up.setWildcardCount(0);
      // Write destination
      vf.write(desination);
    }
    return n;
  }

  /**
   * Method deletes from source word model with specified wordName
   */
  public static int deleteWords(String wordName, InputStream source, OutputStream desination) throws IOException, VoxFormatException {
    return deleteWords(new VoxProperties(wordName), source, desination);
  }

  /**
   * Method deletes from source all word models with wordNames from specified array.
   */
  public static int deleteWords(String[] wordNames, InputStream source, OutputStream desination) throws IOException, VoxFormatException {
    return deleteWords(new VoxProperties(wordNames), source, desination);
  }

  /**
   * Method deletes from source all word models with wordNames from specified collection.
   */
  public static int deleteWords(Collection wordNames, InputStream source, OutputStream desination) throws IOException, VoxFormatException {
    return deleteWords(new VoxProperties(wordNames), source, desination);
  }

  /**
   * Main. Deletes all word models of words coming from WordsListFile from SourceVoiceFile and saves remaining in DestinationVoiceFile.
   * If the DestinationVoiceFileName == null, the backed up SourceVoiceFile will be the DestinationVoiceFile.
   * WordsListFile must conform to Properties File standard: ISO 8859-1 (Latin 1) character encoding is used for WordNames spelling,
   * For characters outside the encoding Unicode escapes are used as is the case when writing grammar for Convert.
   */
  public static void main(String argv[]) {
    String source, destination;

    if (argv.length < 2 || argv.length > 3) {
      System.out.println("Usage: java VoxDelete WordListFileName SourceVoiceFileName [DestinationVoiceFileName]");
      return;
    }
    if (argv.length == 2) {
      source = argv[1] + "~";
      (new File(argv[1])).renameTo(new File(source));
      destination = argv[1];
    } else {
      source = argv[1];
      destination = argv[2];
    }
    Properties p = new Properties();
    try {
      p.load(new FileInputStream(argv[0]));
      System.out.println("VoxDelete: Deleted " + deleteWords(p, new FileInputStream(source), new FileOutputStream(destination)) + " words.");
    } catch (Throwable t) {
      System.out.println("VoxDelete: caught Throwable " + t.toString());
      t.printStackTrace();
    }
  }
}
