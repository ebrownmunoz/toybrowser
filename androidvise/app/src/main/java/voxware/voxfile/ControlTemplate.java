// ControlTemplate -> TemplateTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ControlTemplate extends TemplateTable {

   // Constructor for VoxBlock.read()
  ControlTemplate() {blockID = CONTROL_TEMPLATE;}

   // Constructor for Java Convert writers. Should check arguments consistency.
  public ControlTemplate(long[] offsets, int[] data) {
    super(offsets, data);
    blockID = CONTROL_TEMPLATE;
  }

  public String toString() {
    return super.toString() + "ControlTemplate: Template Count = " + ulaOffsets.length + "\n\n";
  }
}
