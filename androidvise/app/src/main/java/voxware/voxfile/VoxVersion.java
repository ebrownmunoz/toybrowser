// VoxVersion -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class VoxVersion extends VoxBlock {

   // Instance Fields and their access method
  private VoxName vnVersion;
  public String getVersion() { return vnVersion.getName();}
  public void setVersion(String s) throws VoxFormatException { vnVersion.setName(s);}

   // Constructors
  public VoxVersion() {
    blockID = VERSION;
    vnVersion = new VoxName(); // Empty, with MAX_NAME_LEN=255 limit
  }

  public VoxVersion(String s) {
    blockID = VERSION;
    vnVersion = new VoxName(0, s);
  }

   // Instance methods

   // Package scope
  long bodySize() {return vnVersion.bodySize();}

   // Package scope. blockID must have been read !!!
  VoxVersion read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      vnVersion = VoxName.read(vis);  // MAX_NAME_LEN=255 limit
      // Early object format check
      if (blockSize != vnVersion.bodySize()) throw new VoxFormatException("VoxVersion actual size != blockSize");
    } catch (EOFException e) {
      throw new VoxFormatException("VoxVersion.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    vnVersion.write(vos);
  }

  public String toString () {
    return super.toString() + "VoxVersion: " + vnVersion.toString() + "\n\n";
  }
}
