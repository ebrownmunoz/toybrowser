// HostParseVocabulary -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class HostParseVocabulary extends ParseTable {

   // Constructor for VoxBlock.read()
  HostParseVocabulary() {blockID = HOST_PARSE_VOCAB;}

   // Constructor for Java Convert writers. Should check arguments consistency.
  public HostParseVocabulary(int vocabularyID) {
    super(vocabularyID, null, null);
    blockID = HOST_PARSE_VOCAB;
  }

  public HostParseVocabulary(int vocabularyID, long[] offsets, int[] data) {
    super(vocabularyID, offsets, data);
    blockID = HOST_PARSE_VOCAB;
  }

  public String toString() {
    return super.toString() + "HostParseVocabulary: State Count = " + ulaOffsets.length + "\n\n";
  }
}
