/**------------------------------------------------------------*
 * Some general methods for justifying and concatenating Strings
 * in fixed-length fields separated by separator Strings
 * If the nth Field or its content is null, its separator and
 * the preceding separator are ignored. A positive/negative
 * minimum field length causes the content to be left/right
 * justified in the field. Content longer than the minimum field
 * length is NOT truncated.
 *------------------------------------------------------------**/

package voxware.voxfile;

public class StringFormatter {

   // A field descriptor
  public static class Field {

    public int     length;     // The minimum length of the field (negative for right-justification, positive for left)
    public String  content;    // The String to place in the field
    public String  separator;  // The string to separate this field from the following field

    public Field(String content) {
      this.content = content;
    }

    public Field(String content, String separator) {
      this.content = content;
      this.separator = separator;
    }

    public Field(int length, String content) {
      this.length = length;
      this.content = content;
    }

    public Field(int length, String content, String separator) {
      this.length = length;
      this.content = content;
      this.separator = separator;
    }

    public String toString() {
      return super.toString() + "[" + length + ", \"" + content + "\", \"" + separator + "\"]";
    }
  }

  public static StringBuffer appendTo(StringBuffer buffer, String prefix, int len, String content, String separator) {
    if (buffer == null) buffer = new StringBuffer("");
    if (prefix != null) buffer.append(prefix);
    if (len < 0) skip(-len - content.length(), buffer);
    buffer.append(content);
    if (separator != null) buffer.append(separator);
    if (len > 0) skip(len - content.length(), buffer);
    return buffer;
  }
  public static StringBuffer appendTo(String prefix, int len, String content, String separator) {
    return appendTo(null, prefix, len, content, separator);
  }
  public static StringBuffer appendTo(int len, String content, String separator) {
    return appendTo(null, null, len, content, separator);
  }
  public static StringBuffer appendTo(String prefix, int len, String content) {
    return appendTo(null, prefix, len, content, null);
  }
  public static StringBuffer appendTo(int len, String content) {
    return appendTo(null, null, len, content, null);
  }
  public static StringBuffer appendTo(StringBuffer buffer, int len, String content, String separator) {
    return appendTo(buffer, null, len, content, separator);
  }
  public static StringBuffer appendTo(StringBuffer buffer, String prefix, int len, String content) {
    return appendTo(buffer, prefix, len, content, null);
  }
  public static StringBuffer appendTo(StringBuffer buffer, int len, String content) {
    return appendTo(buffer, null, len, content, null);
  }
  private Field[]  fields;
  private String   defaultSeparator;

  public StringFormatter(Field[] fields) {
    this.fields = fields;
    this.defaultSeparator = " ";
  }

  public StringFormatter(Field[] fields, String defaultSeparator) {
    this.fields = fields;
    this.defaultSeparator = defaultSeparator;
  }

  public void defaultSeparator(String defaultSeparator) {
    this.defaultSeparator = defaultSeparator;
  }

  public String defaultSeparator() {
    return this.defaultSeparator;
  }

   // Append the formatted string to the given StringBuffer and return the StringBuffer
  public StringBuffer appendTo(StringBuffer buffer) {
    if (fields != null) {
      String separator = "";
      int skip = 0;
      for (int fi = 0; fi < fields.length; fi++) {
        Field field = fields[fi];
        if (field != null && field.content != null) {
          skip(skip, buffer);
          skip = 0;
          buffer.append(separator);
          if (field.length < 0) skip(- field.length - field.content.length(), buffer);   // Right-justify field.content
          else                  skip = field.length - field.content.length();            // Left-justify field.content
          buffer.append(field.content);
          separator = field.separator != null ? field.separator : "";
        } else {
          separator = defaultSeparator;
        }
        int finn = fi;
        for (finn = fi; finn < fields.length; finn++)
          if (fields[finn] != null && fields[finn].content != null) break;
        if (finn == fields.length) break;
      }
    }
    return buffer;
  }

   // Return the formatted string in a StringBuffer
  public StringBuffer toStringBuffer() {
    return appendTo(new StringBuffer(""));
  }

   // Return the formatted string
  public String toString() {
    return toStringBuffer().toString();
  }

  /**------------------------------------------------------------*
   * Append skip blanks to the given StringBuffer
   *------------------------------------------------------------**/
  private static void skip(int skip, StringBuffer buffer) {
    while (skip-- > 0) buffer.append(' ');
  }
}
