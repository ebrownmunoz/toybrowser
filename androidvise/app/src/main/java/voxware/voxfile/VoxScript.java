// VoxScript -> VoxBlock

package voxware.voxfile;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class VoxScript extends VoxBlock {

   // Instance Fields and their access method

  private int            ubDiscMinCount;
  public  int            getDiscreteMinCount() {return ubDiscMinCount;}

  private int            ubContMinCount;
  public  int            getContinuousMinCount() {return ubContMinCount;}

  private ArrayList      entries;
  public  ArrayList      getEntries() { return entries; }
  public  void           setEntries() { this.entries = entries; }

   // Constructors
   // Package scope for read()
  VoxScript() {blockID = SCRIPT;}

   // Public
  public VoxScript(int vocabularyId, int discreteMinCount, int continuousMinCount, ArrayList entries) {
    blockID = SCRIPT;
    classID = vocabularyId;
    ubDiscMinCount = discreteMinCount;
    ubContMinCount = continuousMinCount;
    this.entries = entries;
  }

   // Instance methods

   // Package scope
  long bodySize() {
    long l = 4; // usEntryCount, ubDiscMinCount, ubContMinCount
    for (Iterator it = entries.iterator(); it.hasNext(); ) {
      ScriptEntry entry = (ScriptEntry) it.next();
      l += entry.bodySize();
    }
    return l;
  }

   // Package scope. blockID must have been read !!!
  VoxScript read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Read object body
      int n = vis.readUns();
      if (n == 0) throw new VoxFormatException("VoxScript.read: Empty script array");
      ubDiscMinCount = vis.readUnsignedByte();
      ubContMinCount = vis.readUnsignedByte();
      if (entries == null)
        entries = new ArrayList();
      else 
        entries.clear();
      for (int i = 0; i < n; i++)
        entries.add(ScriptEntry.read(vis));
    } catch (EOFException e) {
      throw new VoxFormatException("VoxScript.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos); // Computes this.bodySize(), updating ubNumberParameters, usNumberPatterns
    vos.writeUns(entries.size());
    vos.writeByte(ubDiscMinCount);
    vos.writeByte(ubContMinCount);
    for (Iterator it = entries.iterator(); it.hasNext(); ) {
      ScriptEntry entry = (ScriptEntry) it.next();
      entry.write(vos);
    }
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer(super.toString() + "VoxScript: Discrete Min: " + ubDiscMinCount + "; Continuous Min: " + ubContMinCount + "\n");
    int i = 0;
    for (Iterator it = entries.iterator(); it.hasNext(); i++) {
      ScriptEntry entry = (ScriptEntry) it.next();
      buffer = StringFormatter.appendTo(buffer, -3, String.valueOf(i + 1), ". ");
      buffer.append(entry + "\n");      
    }
    return buffer.toString() + "\n";
  }

  public String toString(VoxFile vf) {
    StringBuffer buffer = new StringBuffer(super.toString() + "VoxScript: Discrete Min: " + ubDiscMinCount + "; Continuous Min: " + ubContMinCount + "\n");
    buffer.append("#vocabulary " + vf.getText(vf.getVocabularyTranslation().getVocabularyName()) + "\n");
    for (Iterator it = entries.iterator(); it.hasNext();) {
      ScriptEntry entry = (ScriptEntry) it.next();
      buffer.append(entry.toString(vf) + "\n");      
    }
    return buffer.toString() + "\n";
  }
}
