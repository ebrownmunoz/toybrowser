// ScriptEntry - component of VoxScript block

package voxware.voxfile;

import java.io.*;

public class ScriptEntry {

   // Class Methods. Package scope.
  static ScriptEntry read(VoxInputStream vis) throws IOException, VoxFormatException {
    ScriptEntry entry = new ScriptEntry();
    try {
      entry.usGrammarID = vis.readUns();
      entry.usStartNode = vis.readUns();
      entry.usEndNode   = vis.readUns();
      int n = vis.readUnsignedByte();
      if (n == 0) throw new VoxFormatException("ScriptEntry.read: Empty Word List");
      entry.usaWords = new int[n];
      for (int i = 0; i < n; i++)
        entry.usaWords[i] = vis.readUns();
    } catch (EOFException e) {
      throw new VoxFormatException("ScriptEntry.read: Unexpected EOF");
    }
    return entry;
  }

   // Instance Fields and their access method
  private int    usGrammarID;
  public  int    getGrammarID() {return usGrammarID;}

  private int    usStartNode;
  public  int    getStartNode() {return usStartNode;}

  private int    usEndNode;
  public  int    getEndNode() {return usEndNode;}

  private int[]  usaWords;
  public  int[]  getWords() {return usaWords;}

   // Constructor. Package scope for read
  ScriptEntry() {} 

   // Constructor
  public ScriptEntry(int grammarID, int startNode, int endNode, int[] words) {
    if (words.length == 0) throw new IllegalArgumentException("ScriptEntry: Empty Word List");
    usGrammarID = grammarID;
    usStartNode = startNode;
    usEndNode   = endNode;
    usaWords    = words;
  }

   // Instance methods
   // Package scope
  long bodySize() {return 3*2 + 1 + 2*usaWords.length;}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    vos.writeUns(usGrammarID);
    vos.writeUns(usStartNode);
    vos.writeUns(usEndNode);
    vos.writeByte(usaWords.length);
    for (int i = 0; i < usaWords.length; i++)
      vos.writeUns(usaWords[i]);
  }

  public String toString () {
    StringBuffer buffer = StringFormatter.appendTo("(", -2, String.valueOf(usGrammarID));
    buffer.append(" " + usStartNode + " " + usEndNode + ") ");
    for (int i = 0; i < usaWords.length; i++)
      StringFormatter.appendTo(buffer, -4, String.valueOf(usaWords[i]));
    return buffer.toString();
  }

  public String toString (VoxFile vf) {
    VoxGrammar grammar = (VoxGrammar)vf.getGrammars().get(usGrammarID-1);
    StringBuffer buffer = new StringBuffer("(" + vf.getText(grammar.getGrammarName()) + ") ");
    VocabularyEntry[] vea = vf.getVocabularyTranslation().getWords();
    for (int i = 0; i < usaWords.length; i++) {
      String wordName = vf.getText((vea[usaWords[i]-1]).getWordName());
      if (wordName.indexOf(' ') >= 0) buffer.append("\"" + wordName + "\" ");
      else                            buffer.append(wordName + " ");
    }
    return buffer.toString();
  }
}
