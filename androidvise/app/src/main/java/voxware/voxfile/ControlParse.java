// ControlParse -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ControlParse extends ParseTable {

   // Constructor for VoxBlock.read()
  ControlParse() {blockID = CONTROL_PARSE;}

   // Constructor for Java Convert writers. Should check arguments consistency.
  public ControlParse(int grammarID, long[] offsets, int[] data) {
    super(grammarID, offsets, data);
    blockID = CONTROL_PARSE;
  }

  public String toString() {
    return super.toString() + "ControlParse: State Count = " + ulaOffsets.length + "\n\n";
  }
}
