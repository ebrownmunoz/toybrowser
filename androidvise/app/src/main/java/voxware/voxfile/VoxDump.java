package voxware.voxfile;

import java.io.FileInputStream;

public class VoxDump {
  public static void main(String argv[]) {
    if (argv.length != 1) {
      System.out.println("VoxDump: usage: <voxdump> path");
    }

    FileInputStream fis = null;
    try {
      fis = new FileInputStream(argv[0]);
    } catch (Exception e) {
      e.printStackTrace();
    }
    VoxInputStream vis = new VoxInputStream(fis);
    
    // process blocks until exception occurs
    while (true) {
      RawBlock block = new RawBlock();
      try {
        block.read(vis);
        System.out.println("Block: " + block.getBlockID() + "; Class: " +
                           block.getClassID() + "; Size: " + block.getBodySize() +
                           "; Checksum: " + block.checksum());
      } catch (Exception e) {
        e.printStackTrace();
        System.exit(0);
      }
    }
  }
}