// VoiceParse -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class VoiceParse extends ParseTable {

   // Constructor for VoxBlock.read()
  VoiceParse() {
    blockID = VOICE_PARSE;
  }

   // Constructor for Java Convert writers. Should check arguments consistency.
  public VoiceParse(int grammarID) {
    super(grammarID, null, null);
    blockID = VOICE_PARSE;
  }

  public VoiceParse(int grammarID, long[] offsets, int[] data) {
    super(grammarID, offsets, data);
    blockID = VOICE_PARSE;
  }

  public String toString() {
    return super.toString() + "VoiceParse: State Count = " + ulaOffsets.length + "\n\n";
  }
}
