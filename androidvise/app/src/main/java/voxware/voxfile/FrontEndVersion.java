// Enforces Front End parameter consistency across FE(Cep) versions.

package voxware.voxfile;

public class FrontEndVersion {

  // Constants
  // Known FE versions
  public static final int FVISE16KHzOldNorm     = 1;
  public static final int FVISE8KHzRastaOldNorm = 2;
  public static final int VISE8KHz              = 813;
  public static final int VISE10KHz             = 850;
  public static final int VISE11KHz             = 900;
  public static final int VISE8KHzNSS           = 8;
  public static final int VISE11KHzNSS          = 11;

  //  modes
  public static final int VISE                  = 0;
  public static final int FLEX                  = 1;

  // Cepstrum Processing Option Bits
  public static final int RASTA                 = 1;
  public static final int NMASS                 = 2;

  // Class Methods
  public static boolean isValidFEVersion(int feVersion) {
    return feVersion == FVISE16KHzOldNorm || feVersion == FVISE8KHzRastaOldNorm ||
           feVersion == VISE8KHz || feVersion == VISE10KHz || feVersion == VISE11KHz ||
           feVersion == VISE8KHzNSS || feVersion == VISE11KHzNSS;
  }

  // Instance Fields and their access methods

  private int feVersion;
  public  int getFEVersion() {return feVersion;}

  private int sampleRate;
  public  int getSampleRate() {return sampleRate;}

  private int frameLength;
  public  int getFrameLength() {return frameLength;}

  private int frameShift;
  public  int getFrameShift() {return frameShift;}

  private int cepstrLength;
  public  int getCepstrLength() {return cepstrLength;}

  private int featureLength;;
  public  int getFeatureLength() {return featureLength;}

  private int optionBits;
  public  int getOptionBits() {return optionBits;}

  private int mode;
  public  int getMode() {return mode;}

  // Constructor
  public  FrontEndVersion(int version) {
    feVersion = version;
    switch(version) {

      case FVISE16KHzOldNorm:
        sampleRate    = 16000;
        frameLength   =   410;
        frameShift    =   160;
        cepstrLength  =    13;
        featureLength =    54;
        optionBits    =     0;
        mode          =  FLEX;
        break;

      case FVISE8KHzRastaOldNorm:
        sampleRate    =  8000;
        frameLength   =   205;
        frameShift    =    80;
        cepstrLength  =    13;
        featureLength =    54;
        optionBits    = RASTA;
        mode          =  FLEX;
        break;

      case VISE8KHz:
        sampleRate    =  8000;
        frameLength   =   256;
        frameShift    =   102;
        cepstrLength  =     8;
        featureLength =    16;
        optionBits    =     0;
        mode          =  VISE;
        break;

      case VISE10KHz:
        sampleRate    = 10000;
        frameLength   =   256;
        frameShift    =   128;
        cepstrLength  =     8;
        featureLength =    16;
        optionBits    =     0;
        mode          =  VISE;
        break;

      case VISE11KHz:
        sampleRate    = 11025;
        frameLength   =   256;
        frameShift    =   141;
        cepstrLength  =     8;
        featureLength =    16;
        optionBits    =     0;
        mode          =  VISE;
        break;

      case VISE8KHzNSS:
        sampleRate    =  8000;
        frameLength   =   256;
        frameShift    =   102;
        cepstrLength  =     8;
        featureLength =    16;
        optionBits    = NMASS;
        mode          =  VISE;
        break;

      case VISE11KHzNSS:
        sampleRate    = 11025;
        frameLength   =   256;
        frameShift    =   141;
        cepstrLength  =     8;
        featureLength =    16;
        optionBits    = NMASS;
        mode          =  VISE;
        break;

      default:
        throw new IllegalArgumentException("FrontEndVersion: Bad Front End Version " + version);
    }
  }
}
