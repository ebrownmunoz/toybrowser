// VoxName: byte array with optionaly fixed length

package voxware.voxfile;

import java.io.*;
import java.util.Arrays;

public class VoxName {

   // Constants

  public static final int MAX_NAME_LEN = 255;

   // Class Methods. Package scope
  static VoxName read(VoxInputStream vis, int size) throws IOException, VoxFormatException {
    VoxName vn = new VoxName(size);
    try {
      vn.nameLen = vis.readUnsignedByte();
      if (size != 0 && vn.nameLen > size) throw new VoxFormatException("VoxName.read: len > size");
      if (size == 0) vn.baName = new byte[vn.nameLen];
      if (vn.baName.length > 0) vis.readFully(vn.baName);
    } catch (EOFException e) {
      throw new VoxFormatException("VoxName.read: Unexpected EOF");
    }
    return vn;
  }

  static VoxName read(VoxInputStream vis) throws IOException, VoxFormatException {
    return read(vis, 0);
  }

   // Instance Fields and their access method
  private int baSize;
  public int getNameSize() { return baSize; }
  private int nameLen;
  public int getNameLen() { return nameLen; }
  private byte[] baName;
  public byte[] getBytes() { return baName; }
  public String getName() {
    try {
      return new String(baName, 0, nameLen, VoxFile.WORD_ENCODING);
    } catch (UnsupportedEncodingException e) {
      return "VoxName.getName: UnsupportedEncodingExceptionCaught";
    }
  }

  public void setName(String s) throws VoxFormatException {
    byte[] ba;
    try {
      ba = s.getBytes(VoxFile.WORD_ENCODING);
    } catch (UnsupportedEncodingException e) {
      throw new VoxFormatException("VoxName.setName: UnsupportedEncodingException caught");
    }
    if (ba.length > ((baSize > 0) ? baSize : MAX_NAME_LEN))
      throw new VoxFormatException("VoxName.setName: string too long");
    nameLen = ba.length;
    if (baSize > 0) {
      System.arraycopy(ba, 0, baName, 0, nameLen);
      Arrays.fill(baName, nameLen, baName.length, (byte)0);
    } else {
      baName = ba;
    }
  }

   // Constructors
  public VoxName(int size, String s) {
    byte[] ba;
    if (size < 0 || size > MAX_NAME_LEN)
      throw new IllegalArgumentException("VoxName: size must be within [0, " + MAX_NAME_LEN + "] range");
    baSize = size;
    try {
      ba = s.getBytes(VoxFile.WORD_ENCODING);
    } catch (UnsupportedEncodingException e) {
      throw new IllegalArgumentException("VoxName: UnsupportedEncodingException caught");
    }
    if (ba.length > ((baSize > 0) ? baSize : MAX_NAME_LEN))
      throw new IllegalArgumentException("VoxName: string too long");
    nameLen = ba.length;
    if (baSize > 0) {
      baName = new byte[baSize];
      System.arraycopy(ba, 0, baName, 0, nameLen);
      Arrays.fill(baName, nameLen, baName.length, (byte)0);
    } else {
      baName = ba;
    }
  }

  public VoxName(int size) {
    if (size < 0 || size > MAX_NAME_LEN)
      throw new IllegalArgumentException("VoxName: size must be within [0, " + MAX_NAME_LEN + "] range");
    baSize = size;
    baName = new byte[baSize];
  }

  public VoxName() {
    this(0);
  }

   // Package scope
  long bodySize() {return 1 + baName.length;}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    vos.writeByte(nameLen);
    if (baName.length > 0) vos.write(baName);
  }

  public String toString () {
    return getName();
  }
}
