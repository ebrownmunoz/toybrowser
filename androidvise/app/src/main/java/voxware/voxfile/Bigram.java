// Bigram - component of Unigram component of LanguageModel

package voxware.voxfile;

import java.io.*;

public class Bigram {

   // Class Methods. Package scope
  static Bigram read(VoxInputStream vis) throws IOException, VoxFormatException {
    Bigram bg = new Bigram();
    try {
      bg.usSuccessor = vis.readUns();
      bg.slProbability = vis.readSLong();
    } catch (EOFException e) {
      throw new VoxFormatException("Bigram.read: Unexpected EOF");
    }
    return bg;
  }

  static long bodySize() {return 6;} // size of V_Uns + V_Long

   // Instance Fields and their access methods

  private int  usSuccessor;
  public  int  getSuccessor() {return usSuccessor;}

  private int  slProbability;
  public  int  getProbability() {return slProbability;}

   // Constructor. Package scope for read
  Bigram() {}

   // Public constructor
  public Bigram(int successor, int probability) {
    usSuccessor = successor;
    slProbability = probability;
  }

   // Instance methods

  void write(VoxOutputStream vos) throws IOException {
    vos.writeUns(usSuccessor);
    vos.writeSLong(slProbability);
  }
}
