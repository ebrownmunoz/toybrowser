// GrammarEntry - component of GrammarTranslation block

package voxware.voxfile;

import java.io.*;

public class GrammarEntry {

   // Class Methods. Package scope.
  static GrammarEntry read(VoxInputStream vis) throws IOException, VoxFormatException {
    GrammarEntry entry = new GrammarEntry();
    try {
      entry.usWordID = vis.readUns();
      if (entry.usWordID == 0) throw new VoxFormatException("GrammarEntry.read: wordId == 0");
      entry.loHostTranslation = LenOff.read(vis);
      entry.loVoiceTranslation = LenOff.read(vis);
      entry.loDisplayTranslation = LenOff.read(vis);
    } catch (EOFException e) {
      throw new VoxFormatException("GrammarEntry.read: Unexpected EOF");
    }
    return entry;
  }

   // Instance Fields and their access method
  private int    usWordID;
  public  int    getWordID() {return usWordID;}
  public  void   setWordID(int i) {usWordID = i;}

  private LenOff loHostTranslation;
  public  LenOff getHostTranslation() {return loHostTranslation;}
  public  void   setHostTranslation(LenOff lo) {loHostTranslation = lo;}

  private LenOff loVoiceTranslation;
  public  LenOff getVoiceTranslation() {return loVoiceTranslation;}
  public  void   setVoiceTranslation(LenOff lo) {loVoiceTranslation = lo;}

  private LenOff loDisplayTranslation;
  public  LenOff getDisplayTranslation() {return loDisplayTranslation;}
  public  void   setDisplayTranslation(LenOff lo) {loDisplayTranslation = lo;}

   // Constructor
  public  GrammarEntry() {} 

   // Package scope
  static long bodySize() {return 2 + 3 * LenOff.bodySize();}

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    if (usWordID == 0) throw new VoxFormatException("GrammarEntry.write: wordId == 0");
    vos.writeUns(usWordID);
    loHostTranslation.write(vos);
    loVoiceTranslation.write(vos);
    loDisplayTranslation.write(vos);
  }

  public String toString () {
    StringBuffer buffer = StringFormatter.appendTo("Word ", -4, String.valueOf(usWordID), "\n");
    buffer.append("          host    translation:  " + VoxBlock.getText(loHostTranslation));
    if (loHostTranslation != null)
      buffer.append(" (" + new LenOff(loHostTranslation) + ")\n");
    else
      buffer.append("\n");
    buffer.append("          voice   translation:  " + VoxBlock.getText(loVoiceTranslation));
    if (loVoiceTranslation != null)
      buffer.append(" (" + new LenOff(loVoiceTranslation) + ")\n");
    else
      buffer.append("\n");
    buffer.append("          display translation:  " + VoxBlock.getText(loDisplayTranslation));
    if (loDisplayTranslation != null)
      buffer.append(" (" + new LenOff(loDisplayTranslation) + ")\n");
    else
      buffer.append("\n");
    return buffer.toString();
  }
}
