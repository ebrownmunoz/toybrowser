// Exception thrown when fatal inconsistency is encountered in data

package voxware.voxfile;

public class VoxFormatException extends Exception {
   // Constructor
  public VoxFormatException(String message) {
    super(message);
  }
}
