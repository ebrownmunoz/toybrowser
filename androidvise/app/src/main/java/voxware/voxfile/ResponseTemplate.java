// ResponseTemplate -> TemplateTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ResponseTemplate extends TemplateTable {

   // Constructor for Java Convert writers. Should check arguments consistency.
  public ResponseTemplate() {blockID = RESPONSE_TEMPLATE;}

  public ResponseTemplate(long[] offsets, int[] data) {
    super(offsets, data);
    blockID = RESPONSE_TEMPLATE;
  }

  public String toString() {
    return super.toString() + "ResponseTemplate: Template Count = " + ulaOffsets.length + "\n\n";
  }
}
