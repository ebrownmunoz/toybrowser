// ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class ParseTable extends VoxTable {

   // Constructors. Subclasses must setup blockID
  ParseTable() {
  }

  ParseTable(int classId, long[] offsets, int[] data) {
    super(offsets, data);
    classID = classId;
  }
}
