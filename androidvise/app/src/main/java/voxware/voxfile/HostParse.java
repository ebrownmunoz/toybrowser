// HostParse -> ParseTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class HostParse extends ParseTable {

   // Constructor for VoxBlock.read()
  HostParse() {
    blockID = HOST_PARSE;
  }

   // Constructors for Java Convert writers. Should check arguments consistency.
  public HostParse(int grammarID) {
    super(grammarID, null, null);
    blockID = HOST_PARSE;
  }

  public HostParse(int grammarID, long[] offsets, int[] data) {
    super(grammarID, offsets, data);
    blockID = HOST_PARSE;
  }

  public String toString() {
    return super.toString() + "HostParse: State Count = " + ulaOffsets.length + "\n\n";
  }
}
