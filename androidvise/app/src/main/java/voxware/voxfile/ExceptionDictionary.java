/**
 * ExceptionDictionary provides for orthograpy and/or phonetic translation and/or component translation
 * for atypical word names, difficult orthographies to translate, or components tweaking. It is assumed
 * to be small and unsorted. So search is simple linear.
 *
 * Exception Dictionary file is assumed to have the following format:
 *
 *    1)  Any line beginning with a '!' is a comment and is ignored.
 *    2)  All other lines contain: wordName, followed by
 *        wordDelimiter, followed by optional SPACE characters,
 *        followed by optional (orthography, followed by orthoDelimiter),
 *        followed by optional SPACE characters, followed by optional
 *        (phone sequence, followed by phoneDelimiter), followed by optional
 *        SPACE characters, followed by optional component sequence, delimeted by \n
 *    3)  Encoding is defined by public class field EXCEPTION_DICTIONARY_ENCODING (default is "UTF-8").
 *        If platform default CharSet is desirable assign null to this field.
 *    4)  word, ortho and phone Delimiters are defined by public class members (default are :;/ respectively)
 *        All delimiter symbols must be distinctive, and wordDelimiter and orthoDelimiter assumed NOT to be
 *        a part of any multibyte character for language in use, so regular
 *        string search routins work.
 *    5)  Kernel minimum dwell of 2 or 3 can be specified after component name using # symbol
 *    For example:
 *
 *        AT&T:  A.T.&T.;  EY T IY AE N DD T IY,  *>ey Ey eY ey<t ^t<t t<iy t>iy iy iy<ae iy>ae ae#3 ae<n n n<d ^d<d ^t<t t<iy t>iy iy#2 iy<*
 */

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class ExceptionDictionary {

  // Class fields
  public static char   EXCEPTION_DICTIONARY_WORD_DELIMITER = ':';
  public static char   EXCEPTION_DICTIONARY_ORTHO_DELIMITER = ';';
  public static char   EXCEPTION_DICTIONARY_PHONE_DELIMITER = '/';
  public static String EXCEPTION_DICTIONARY_ENCODING = "UTF-8";

  static class Xentry {
    String word;
    String ortho;
    String phone;
    String comps;

    Xentry(String w, String o, String p, String c) {
      word = w;
      ortho = o;
      phone = p;
      comps = c;
    }
  }

  // Instance Fields
  private Xentry[] entries;

  // Constructor
  public ExceptionDictionary(String fileName) {
    ArrayList list = new ArrayList(100);
    String    line, w, o, p, c;
    int       i, j;

    try {
      BufferedReader br = new BufferedReader(EXCEPTION_DICTIONARY_ENCODING == null ? new FileReader(fileName) :
                                             new InputStreamReader(new FileInputStream(fileName), EXCEPTION_DICTIONARY_ENCODING));
      while ((line = br.readLine()) != null) {
        line = line.trim();
        if (line.length() == 0 || line.charAt(0) == '!') continue;
        i = 0;
        // Search for must have word delimiter
        if ((j = line.indexOf(EXCEPTION_DICTIONARY_WORD_DELIMITER, i)) < 0) continue;
        w = line.substring(i, j).trim();
        if (w.length() == 0) continue;
        o = p = c = null;
        // Search for optional delimiters
        i = j + 1;
        if ((j = line.indexOf(EXCEPTION_DICTIONARY_ORTHO_DELIMITER, i)) > 0) {
          o = line.substring(i, j).trim();
          if (o.length() == 0) o = null;
          i = j + 1;
        }
        if ((j = line.indexOf(EXCEPTION_DICTIONARY_PHONE_DELIMITER, i)) > 0) {
          p = line.substring(i, j).trim();
          if (p.length() == 0) p = null;
          i = j + 1;
        }
        if (i < line.length()) c = line.substring(i).trim();
        if (c != null && c.length() == 0) c = null;
        // At least one of the last three should be there
        if (o == null && p == null && c == null) continue;
        list.add(new Xentry(w, o, p, c));
      }
      // Convert list to array
      if ((i = list.size()) == 0) throw new IllegalArgumentException("ExceptionDictionary: Empty Source File");
      entries = new Xentry[i];
      entries = (Xentry[])list.toArray(entries);
    } catch (IOException ioe) {
      throw new IllegalArgumentException("ExceptionDictionary: " + ioe.getMessage());
    }
  }

  // Instance Metods
  public int count() {return entries.length;}

/**
 * Performs case sensitive search of given word among entries.word.
 * Returns index of found entry, or -1 if search failed.
 */
  public int wordIndex(String word) {
    for (int i = 0; i < entries.length; i++)
      if (word.equalsIgnoreCase(entries[i].word))
        return i;
    return -1;
  }

/**
 * Performs case insensitive search of given orthography among entries.ortho.
 * Returns index of found entry, or -1 if search failed.
 */
  public int orthoIndex(String o) {
    for (int i = 0; i < entries.length; i++)
      if (entries[i].ortho != null)
        if (o.equalsIgnoreCase(entries[i].ortho))
          return i;
    return -1;
  }

/**
 * Performs search of given phoneString among entries.phone.
 * Returns index of found entry, or -1 if search failed.
 */
  public int phoneIndex(String p) {
    for (int i = 0; i < entries.length; i++)
      if (entries[i].phone != null)
        if (p.equals(entries[i].phone))
          return i;
    return -1;
  }

/**
 * For valid index returns corresponding entry's string, null otherwise.
 */
  public String getWordName(int i) {return (i >= 0 && i < entries.length) ? entries[i].word : null;}
  public String getOrthography(int i) {return (i >= 0 && i < entries.length) ? entries[i].ortho : null;}
  public String getPhones(int i) {return (i >= 0 && i < entries.length) ? entries[i].phone : null;}
  public String getComponents(int i) {return (i >= 0 && i < entries.length) ? entries[i].comps : null;}

  // Main. Dumps content
  public static void main(String argv[]) {
    int i;

    if (argv.length == 0) {
      System.out.println("Usage: java ExceptionDictionary fileName [wordName]");
      return;
    }

    try {
      ExceptionDictionary xd = new ExceptionDictionary(argv[0]);
      if (argv.length == 2) {
        if ((i = xd.wordIndex(argv[1])) > 0)
          System.out.println(argv[1] + ": " + xd.getOrthography(i) + "; " + xd.getPhones(i) + "/ " + xd.getComponents(i));
        if ((i = xd.orthoIndex(argv[1])) > 0)
          System.out.println(argv[1] + "; " + xd.getPhones(i) + "/ " + xd.getComponents(i));
        if ((i = xd.phoneIndex(argv[1])) > 0)
          System.out.println(argv[1] + "/ " + xd.getComponents(i));
      } else {
        System.out.println("Total " + xd.count() + " entries:");
        for (i = 0; i < xd.count(); i++)
          System.out.println(xd.getWordName(i) + ": " + xd.getOrthography(i) + "; " + xd.getPhones(i) + "/ " + xd.getComponents(i));
      }
    } catch (Throwable t) {
      System.out.println("ExceptionDictionary: caught Throwable " + t.toString() + " on file " + argv[0]);
      t.printStackTrace();
    }
  }
}
