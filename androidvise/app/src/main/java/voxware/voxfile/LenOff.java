// LenOff - Length/Offset pair for access to text in VoxText block

package voxware.voxfile;

import java.io.*;

public class LenOff {
  private byte ubLen;
  private long ulOffset;

  public static final int MAX_LEN = 255;

   // Class Methods. Package scope
  static LenOff read(VoxInputStream vis) throws IOException, VoxFormatException {
    LenOff lo = new LenOff();
    try {
      lo.ubLen = vis.readByte();
      lo.ulOffset = vis.readULong();
    } catch (EOFException e) {
      throw new VoxFormatException("VoxKey.read: Unexpected EOF");
    }
    return lo;
  }

  static long bodySize() {return 5;} // size of V_Byte + V_ULong

  // Accessors
  public int getLen() {
    // Unsigned byte returned
    return 0xff & (int)ubLen;
  }

  public void setLen(int len) {
    ubLen = (byte)len;
  }

  public long getOffset() {
    return ulOffset;
  }

  public void setOffset(long offset) {
    ulOffset = offset;
  }

   // Constructors
  public LenOff() {
  }

  public LenOff(LenOff that) {
    this.ubLen = that.ubLen;
    this.ulOffset = that.ulOffset;
  }

  public LenOff(int len, long offset) {
    ubLen = (byte)len;
    ulOffset = offset;
  }

   // Instance methods

  void write(VoxOutputStream vos) throws IOException {
    vos.writeByte(ubLen);
    vos.writeULong(ulOffset);
  }

  public String toString() {
    String string = new String("LenOff: len " + getLen() + " offset " + getOffset());
    return string;
  }
}
