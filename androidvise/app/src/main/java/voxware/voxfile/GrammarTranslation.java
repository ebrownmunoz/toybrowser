// GrammarTranslation -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class GrammarTranslation extends VoxBlock {

   // Instance Fields and their access method

  private int    usNumberWords;
  public  int    getNumberWords() {return usNumberWords;}
  public  void   setNumberWords(int i) {usNumberWords = i;}

  private LenOff loHRFInitiator;
  public  LenOff getHRFInitiator() {return loHRFInitiator;}
  public  void   setHRFInitiator(LenOff lo) {loHRFInitiator = lo;}

  private LenOff loHRFSeparator;
  public  LenOff getHRFSeparator() {return loHRFSeparator;}
  public  void   setHRFSeparator(LenOff lo) {loHRFSeparator = lo;}

  private LenOff loHRFTerminator;
  public  LenOff getHRFTerminator() {return loHRFTerminator;}
  public  void   setHRFTerminator(LenOff lo) {loHRFTerminator = lo;}

  private LenOff loVRFInitiator;
  public  LenOff getVRFInitiator() {return loVRFInitiator;}
  public  void   setVRFInitiator(LenOff lo) {loVRFInitiator = lo;}

  private LenOff loVRFSeparator;
  public  LenOff getVRFSeparator() {return loVRFSeparator;}
  public  void   setVRFSeparator(LenOff lo) {loVRFSeparator = lo;}

  private LenOff loVRFTerminator;
  public  LenOff getVRFTerminator() {return loVRFTerminator;}
  public  void   setVRFTerminator(LenOff lo) {loVRFTerminator = lo;}

  private LenOff loDRFInitiator;
  public  LenOff getDRFInitiator() {return loDRFInitiator;}
  public  void   setDRFInitiator(LenOff lo) {loDRFInitiator = lo;}

  private LenOff loDRFSeparator;
  public  LenOff getDRFSeparator() {return loDRFSeparator;}
  public  void   setDRFSeparator(LenOff lo) {loDRFSeparator = lo;}

  private LenOff loDRFTerminator;
  public  LenOff getDRFTerminator() {return loDRFTerminator;}
  public  void   setDRFTerminator(LenOff lo) {loDRFTerminator = lo;}

  private GrammarEntry[] geaWords;
  public  GrammarEntry[] getWords() {return geaWords;}

   // Constructors
   // Package scope for read()
  GrammarTranslation() {blockID = GRAMMAR_TRANS;}

  // Public
  public GrammarTranslation(int grammarID, int wordCount) {
    blockID = GRAMMAR_TRANS;
    classID = grammarID;
    if (wordCount <= 0 || wordCount > 2 * Short.MAX_VALUE)
      throw new IllegalArgumentException("GrammarTranslation: Bad Word Count");
    geaWords = new GrammarEntry[wordCount];
    usNumberWords = wordCount;
  }

  public GrammarTranslation(int grammarID, GrammarEntry[] gea) {
    blockID = GRAMMAR_TRANS;
    classID = grammarID;
    geaWords = gea;
    usNumberWords = geaWords.length;
    if (usNumberWords > 2 * Short.MAX_VALUE)
      throw new IllegalArgumentException("GrammarTranslation: Bad Word Count");
  }

   // Instance methods

   // Package scope
  long bodySize() {return 2 + 9*LenOff.bodySize() + geaWords.length * GrammarEntry.bodySize();}

   // Package scope. blockID must have been read !!!
  GrammarTranslation read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Read object body
      usNumberWords = vis.readUns();
      loHRFInitiator = LenOff.read(vis);
      loHRFSeparator = LenOff.read(vis);
      loHRFTerminator = LenOff.read(vis);
      loVRFInitiator = LenOff.read(vis);
      loVRFSeparator = LenOff.read(vis);
      loVRFTerminator = LenOff.read(vis);
      loDRFInitiator = LenOff.read(vis);
      loDRFSeparator = LenOff.read(vis);
      loDRFTerminator = LenOff.read(vis);
      geaWords = new GrammarEntry[usNumberWords];
      for (int i = 0; i < usNumberWords; i++)
        geaWords[i] = GrammarEntry.read(vis);
    } catch (EOFException e) {
      throw new VoxFormatException("GrammarTranslation.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos);
    vos.writeUns(usNumberWords);
    loHRFInitiator.write(vos);
    loHRFSeparator.write(vos);
    loHRFTerminator.write(vos);
    loVRFInitiator.write(vos);
    loVRFSeparator.write(vos);
    loVRFTerminator.write(vos);
    loDRFInitiator.write(vos);
    loDRFSeparator.write(vos);
    loDRFTerminator.write(vos);
    for (int i = 0; i < usNumberWords; i++)
      if (geaWords[i] == null)
        throw new VoxFormatException("GrammarTranslation.write: null Grammar Entry #" + i);
      else
        geaWords[i].write(vos);
  }

  public String toString() {
    StringBuffer buffer = new StringBuffer(super.toString() + "GrammarTranslation: #Words: " + usNumberWords + "\n");
    for (int i = 0; i < usNumberWords; i++) {
      if (geaWords[i] == null) 
        throw new NullPointerException("GrammarTranslation.toString: null Grammar Entry #" + i);
      buffer.append(geaWords[i]);
    }
    return buffer.toString() + "\n";
  }
}
