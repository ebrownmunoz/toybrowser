// Concrete VoxAdd subclass facilitating Japanese language support

package voxware.voxfile;

import java.util.*;
import java.io.*;

public class VoxAddJA extends VoxAdd {

  /** General Dictionary File to be found in Knowledge Base directory */
  public static String GeneralDictionaryFileName   = "japanese.dic";
  /** General Dictionary default orthography delimiter */
  public static char   GENERAL_DICTIONARY_ORTHO_DELIMITER = ' ';
  /** General Dictionary default encoding */
  public static String GENERAL_DICTIONARY_ENCODING = "SJIS";

  // Constants
  static Phone[] japanese = {
    new Phone("SIL", "*",             "*",  SPECS, 1),
    new Phone("A",   ">a a a<",       "a",  VOWEL, 3),
    new Phone("AA",  ">aa aa aa<",    "a",  VOWEL, 3),
    new Phone("B",   "^b<b b<",       "b",  STOPS, 2),
    new Phone("BD",  "^b<",           "b",  FLAPS, 1),
    new Phone("CH",  "^t<ch ch ch<",  "ch", AFRIC, 3),
    new Phone("D",   "^d<d d<",       "d",  STOPS, 2),
    new Phone("DD",  "^d<",           "d",  FLAPS, 1),
    new Phone("E",   ">e e e<",       "e",  VOWEL, 3),
    new Phone("EE",  ">ee ee ee<",    "e",  VOWEL, 3),
    new Phone("F",   "f f<",          "f",  UFRIC, 2),
    new Phone("G",   "^g<g g<",       "g",  STOPS, 2),
    new Phone("GD",  "^g<",           "g",  FLAPS, 1),
    new Phone("HH",  "hh hh<",        "hh", UFRIC, 2),
    new Phone("I",   ">i i i<",       "i",  VOWEL, 3),
    new Phone("II",  ">ii ii ii<",    "i",  VOWEL, 3),
    new Phone("IH",  ">ih ih<",       "i",  VOWEL, 2),
    new Phone("JH",  "^d<jh jh jh<",  "jh", AFRIC, 3),
    new Phone("K",   "^k<k k<",       "k",  STOPS, 2),
    new Phone("KD",  "^k<",           "k",  FLAPS, 1),
    new Phone("M",   "m m<",          "m",  NASAL, 2),
    new Phone("N",   "n n<",          "n",  NASAL, 2),
    new Phone("NG",  "ng ng<",        "ng", NASAL, 2),
    new Phone("NY",  "ny ny<",        "ny", NASAL, 2),
    new Phone("O",   ">o o o<",       "o",  VOWEL, 3),
    new Phone("OO",  ">oo oo oo<",    "o",  VOWEL, 3),
    new Phone("P",   "^p<p p<",       "p",  STOPS, 2),
    new Phone("PD",  "^p<",           "p",  FLAPS, 1),
    new Phone("R",   "r<",            "r",  GLIDE, 1),
    new Phone("S",   "s s<",          "s",  UFRIC, 2),
    new Phone("SH",  "sh sh<",        "sh", UFRIC, 2),
    new Phone("T",   "^t<t t<",       "t",  STOPS, 2),
    new Phone("TD",  "^t<",           "t",  FLAPS, 1),
    new Phone("TS",  "^t<ts ts ts<",  "ts", AFRIC, 3),
    new Phone("U",   ">u u u<",       "u",  VOWEL, 3),
    new Phone("UU",  ">uu uu uu<",    "u",  VOWEL, 3),
    new Phone("UH",  ">uh uh<",       "u",  VOWEL, 2),
    new Phone("W",   ">w w<",         "w",  SEMIV, 2),
    new Phone("Y",   ">y y<",         "y",  SEMIV, 2),
    new Phone("Z",   "z z<",          "z",  VFRIC, 2)
  };

  static final int MAXRULESETS = 26;

  static class Rule {
    String letter;
    String sound;

    Rule(String l, String s) {
      letter = l;
      sound  = s;
    }
  }

  static Rule ruleTable[][] = {
    {new Rule("[aa]", "/AA/"), new Rule("[a]", "/A/")},
    {new Rule("[bb]", "/BD B/"), new Rule("[b]", "/B/")},
    {
      new Rule("[cchaa]", "/TD CH Y AA/"), new Rule("[ccha]", "/TD CH Y A/"), new Rule("[chaa]", "/CH Y AA/"), new Rule("[cha]","/CH Y A/"),
      new Rule("[cchou]", "/TD CH Y OO/"), new Rule("[chou]", "/CH Y OO/"),   new Rule("[chuu]", "/CH Y UU/"), new Rule("[cho]", "/CH Y O/"),
      new Rule("[cch]", "/TD CH/"),        new Rule("[ch]", "/CH/")
    },
    {new Rule("[dd]", "/DD D/"), new Rule("[d]", "/D/")},
    {new Rule("[ee]", "/EE/"), new Rule("[eii]", "/E II/"), new Rule("[ei]", "/EE/"), new Rule("[e]", "/E/")},
    {new Rule("[ff]", "/F/"), new Rule("[f]", "/F/")},
    {new Rule("[gg]", "/GD G/"), new Rule("[g]", "/G/")},
    {
      new Rule("de[h]a_", "/W/"),    new Rule("de[h]aaru", "/W/"), new Rule("de[h]aatte", "/W/"), new Rule("de[h]anai", "/W/"),
      new Rule("de[h]anaku", "/W/"), new Rule("[h]ainai_", "/W/"), new Rule("[h]airuga_", "/W/"), new Rule("[h]anai_", "/W/"),
      new Rule("[h]a_", "/W/"),      new Rule("[h]eto_", "//"),    new Rule("[h]emo_", "//"),     new Rule("[h]eno_", "//"),
      new Rule("[h]e_", "//"),       new Rule("[h]", "/HH/")
    },
    {
      new Rule("_[i]u", "/Y/"),  new Rule("o[i]u", "/Y/"),  new Rule("u[i]u", "/Y/"),    new Rule("[ii]", "/II/"),
      new Rule("[i]ha_", "/I/"), new Rule("[i]he_", "/I/"), new Rule("[i]heno_", "/I/"), new Rule("[i]heto_", "/I/"),
      new Rule(",[i],", "/IH/"), new Rule("[i]", "/I/")
    },
    {
      new Rule("[jaa]", "/JH Y AA/"), new Rule("[ja]", "/JH Y A/"),   new Rule("[joo]", "/JH Y OO/"), new Rule("[jou]", "/JH Y OO/"),
      new Rule("[jo]", "/JH Y O/"),   new Rule("[juu]", "/JH Y UU/"), new Rule("[ju]", "/JH Y U/"),   new Rule("[j]", "/JH/")
    },
    {new Rule("[kk]", "/KD K/"), new Rule("[k]", "/K/")},
    {new Rule("[l]", "/L/")},
    {new Rule("[m]", "/M/")},
    {
      new Rule("[n]m", "/M/"),      new Rule("[n]b", "/M/"),       new Rule("[n]p", "/M/"),       new Rule("[n]k", "/NG/"),  new Rule("[n]g", "/NG/"),
      new Rule("[n]ha_", "/NG/"),   new Rule("[n]woshita", "/N/"), new Rule("[n]woshite", "/N/"), new Rule("[n]wo_", "/N/"), new Rule("[n]w", "/NG/"),
      new Rule("[nn]i", "/NY NY/"), new Rule("[nn]y", "/NY NY/"),  new Rule("[n]i", "/NY/"),      new Rule("[n]y", "/NY/"),  new Rule("[n]", "/N/")
    },
    {new Rule("[oo]", "/OO/"), new Rule("[ou]", "/OO/"), new Rule("[o]", "/O/")},
    {new Rule("[pp]", "/PD P/"), new Rule("[p]", "/P/")},
    null,
    {new Rule("[r]", "/R/")},
    {
      new Rule("[ssha]", "/SH Y A/"), new Rule("[sha]", "/SH Y A/"),   new Rule("[sshuu]", "/SH Y UU/"), new Rule("[shuu]", "/SH Y UU/"),
      new Rule("[sshu]", "/SH Y U/"), new Rule("[shu]", "/SH Y U/"),   new Rule("[sshou]", "/SH Y OO/"), new Rule("[shou]", "/SH Y OO/"),
      new Rule("[ssho]", "/SH Y O/"), new Rule("[shoo]", "/SH Y OO/"), new Rule("[sho]", "/SH Y O/"),    new Rule("[ssh]", "/SH/"),
      new Rule("[sh]", "/SH/"),       new Rule("[ss]", "/S/"),         new Rule("[s]", "/S/")
    },
    {new Rule("[tts]", "/TD TS/"), new Rule("[ts]", "/TS/"), new Rule("[tt]", "/TD T/"), new Rule("[t]", "/T/")},
    {
      new Rule("des[u]_", "//"),   new Rule("imas[u]_", "//"),  new Rule("[uu]", "/UU/"),  new Rule("[u]ha_", "/U/"), new Rule("[u]he_", "/U/"),
      new Rule("[u]heno_", "/U/"), new Rule("[u]heto_", "/U/"), new Rule(",[u],", "/UH/"), new Rule("[u]", "/U/")
    },
    null,
    {
      new Rule("_[w]o_", "//"), new Rule("[w]oenai", "//"), new Rule("[w]ohikeba", "//"), new Rule("[w]omotte", "//"),
      new Rule("[w]omo_", "//"), new Rule("[w]oshita", "//"), new Rule("[w]oshite", "//"), new Rule("[w]osuru", "//"),
      new Rule("[w]otoiu", "//"), new Rule("[w]otooshite", "//"), new Rule("[w]o_", "//"), new Rule("[w]", "/W/")
    },
    null,
    {new Rule("[y]", "/Y/")},
    {new Rule("[z]", "/Z/")}
  };

  // Constructors
  public VoxAddJA(String kbPath) {
    super(japanese);
    language = "JA";
    try {
      generalDictionary = new VoxProperties(new FileInputStream(kbPath + "/" + GeneralDictionaryFileName), GENERAL_DICTIONARY_ENCODING);
    } catch (IOException ioe) {
      throw new IllegalArgumentException("VoxAddJA: " + ioe.getMessage());
    }
  }

  public VoxAddJA(String kbPath, String gender, int feVersion) {
    this(kbPath);
    if (!FrontEndVersion.isValidFEVersion(feVersion)) throw new IllegalArgumentException("VoxAddJA: Bad Front End Version " + feVersion);
    String componentFileName = "jap" + feVersion + ".voi";
    if (gender.charAt(0) == 'm' || gender.charAt(0) == 'M')      componentFileName = "m" + componentFileName;
    else if (gender.charAt(0) == 'f' || gender.charAt(0) == 'F') componentFileName = "f" + componentFileName;
    else throw new IllegalArgumentException("VoxAddJA: Gender String \"" + gender + "\" does not start with 'f' or 'm'");
    try {
      load(new FileInputStream(kbPath + "/" + componentFileName));
    } catch (IOException ioe) {
      throw new IllegalArgumentException("VoxAddJA: " + ioe.getMessage());
    } catch (VoxFormatException vfe) {
      throw new IllegalArgumentException("VoxAddJA: " + vfe.getMessage());
    }
  }

  static final int HIRAGANA        = -126;
  static final int LARGE_KATAKANA  = -125;
  // Katakana -33 to -92, 60 total, inclu. comma
  static String[] kata = {
    "P", "NIGORI", "n", "wa",         /*-33*/
    "ro", "re", "ru", "ri", "ra",     /*-37*/
    "yo", "yu", "ya",                 /*-42*/
    "mo", "me", "mu", "mi", "ma",     /*-45*/
    "ho", "he", "fu", "hi", "ha",     /*-50*/
    "no", "ne", "nu", "ni", "na",     /*-55*/
    "to", "te", "tsu", "chi", "ta",   /*-60*/
    "so", "se", "su", "shi", "sa",    /*-65*/
    "ko", "ke", "ku", "ki", "ka",     /*-70*/
    "o", "e", "u", "i", "a",          /*-75*/
    "LONG", "TSU", "YO", "YU", "YA",  /*-80*/
    "O", "E", "U", "I", "A",          /*-85*/
    "wo", ".", ","                    /*-90*/
  };
  static final int kataOffset      = 33;
  // Hiragana -15 to -97, 83 total
  static String[] hira = {
    "n",  "wo", null, null, "wa", null, "ro", "re", "ru", "ri", "ra",    /*-15*/
    "yo", "YO", "yu", "YU", "ya", "YA",                                  /*-26*/
    "mo", "me", "mu", "mi", "ma",                                        /*-32*/
    "po", "bo", "ho", "pe", "be", "he", "pu", "bu", "fu",                /*-37*/
    "pi", "bi", "hi", "pa", "ba", "ha",                                  /*-46*/
    "no", "ne", "nu", "ni", "na",                                        /*-52*/
    "do", "to", "de", "te", "zu", "tsu", "TSU", "ji", "chi", "da", "ta", /*-57*/
    "zo", "so", "ze", "se", "zu", "su", "ji", "shi", "za", "sa",         /*-68*/
    "go", "ko", "ge", "ke", "gu", "ku", "gi", "ki", "ga", "ka",          /*-78*/
    "o",  "O",  "e",  "E",  "u",  "U",  "i",  "I",  "a",  "A"            /*-88*/
  };
  static final int hiraOffset      = 15;
  // Large Katakana 64 to 150, 87 total
  static String[] largeKata = {
    "A",  "a",  "I",   "i",  "U",   "u",   "E",  "e",  "O",  "o",        /* 64*/
    "ka", "ga", "ki",  "gi", "ku",  "gu",  "ke", "ge", "ko", "go",       /* 74*/
    "sa", "za", "shi", "ji", "su",  "zu",  "se", "ze", "so", "zo",       /* 84*/
    "ta", "da", "chi", "ji", "TSU", "tsu", "zu", "te", "de", "to", "do", /* 94*/
    "na", "ni", "nu",  "ne", "no",                                       /*105*/
    "ha", "ba", "pa",  "hi", "bi",  "pi",                                /*110*/
    "fu", "bu", "pu",  "he", "be",  "pe",  "ho", "bo", "po",             /*116*/
    "ma", "mi", "mu",  "mu", "me",  "mo",                                /*125*/
    "YA", "ya", "YU",  "yu", "YO",  "yo",                                /*131*/
    "ra", "ri", "ru",  "re", "ro",  "WA",  "wa", null, null, "wo", "n",  /*137*/
    null, "KA", "KE"                                                     /*148*/
  };
  static final int largeKataOffset = 64;

  static String[] iSeries = {
    "ki", "gi", "shi", "chi", "ji", "ni", "hi", "pi", "bi", "mi", "ri",
    "kki", "ggi", "sshi", "cchi", "jji", "nni","hhi", "ppi", "bbi", "mmi", "rri",
    "te", "de", "tte", "dde"
  };

  static String[] palatalObs = {
    "ch", "cch", "j", "jj", "sh", "ssh"
  };

  static String[] ySeries = {
    "YA", "YU", "YO"
  };

  static String[] smallVowels = {
    "A", "E", "I", "O", "U"
  };

  static String[] vl_consonants = {
    "p", "t", "k", "pd", "td", "kd", "ts", "ch", "f", "s", "sh", "hh", "h"
  };

  static boolean isMember(String s, String[] list) {
    for (int i = 0; i < list.length; i++)
      if (s.equals(list[i]))
        return true;
    return false;
  }

  /**
   * Returns phone string corresponding to valid kana input string. Throws VoxFormatException otherwise.
   */
  String applyRules(String input, boolean inDictionary) throws VoxFormatException {
    int  i, j, n;

    // kana2rom
    // Convet to byte sequence
    byte[] kana;
    try {
      kana = input.getBytes("SJIS");
    } catch (IOException ioe) {
      throw new VoxFormatException("VoxAddJA.applyRules: " + ioe.getMessage());
    }
    ArrayList romaji = new ArrayList(256);
    for (i = 0; i < kana.length; i++) {
      j = kana[i];
      if (j == HIRAGANA) { // Hiragana - 16-bit
        j = kana[++i];
        if (j < -14 && j > -98) romaji.add(hira[-j - hiraOffset]);
        else throw new VoxFormatException("VoxAddJA.applyRules: Bad HIRAGANA byte " + j);
      } else if (j == LARGE_KATAKANA) { // Large Katakana - 16-bit
        j = kana[++i];
        if (j < -105 && j > -130) j += 256;
        if (j > 63 && j < 151) romaji.add(largeKata[j - largeKataOffset]);
        else throw new VoxFormatException("VoxAddJA.applyRules: Bad LARGE_KATAKANA byte " + kana[i]);
      } else if (j < -32 && j > -93) { // Katakana - 8-bit
        romaji.add(kata[-j - kataOffset]);
      } else if (j < -96 && j > -121) { // Kanji
        throw new VoxFormatException("VoxAddJA.applyRules: Illegal KANJI byte " + j);
      } else {
        throw new VoxFormatException("VoxAddJA.applyRules: Unknown byte " + j);
      }
    }
    // Process special symbols
    for (i = 0; i < romaji.size() - 1; i++) {
      // Skip already processed special symbols
      String currentSymbol = (String)romaji.get(i);
      if (currentSymbol.length() == 0) continue;
      // Find next symbol, skipping over special symbols already processed.
      j = i + 1;
      while (((String)romaji.get(j)).length() == 0 && j < romaji.size() - 1) j++;
      String nextSymbol = (String)romaji.get(j);
      if (nextSymbol.equals("NIGORI")) { // Voice current symbol
        if (currentSymbol.equals("shi"))       romaji.set(i, "ji");
        else if (currentSymbol.equals("shi"))  romaji.set(i, "jji"); // Does not make sense. The same as above !!
        else if (currentSymbol.equals("tsu"))  romaji.set(i, "zu");
        else if (currentSymbol.equals("ttsu")) romaji.set(i, "zzu");
        else {
          StringBuffer symbol = new StringBuffer(currentSymbol);
          for (n = 0; n < 2 && n < currentSymbol.length(); n++) {
            if      (currentSymbol.charAt(n) == 's') symbol.setCharAt(n, 'z');
            else if (currentSymbol.charAt(n) == 'h') symbol.setCharAt(n, 'b');
            else if (currentSymbol.charAt(n) == 'f') symbol.setCharAt(n, 'b');
            else if (currentSymbol.charAt(n) == 't') symbol.setCharAt(n, 'd');
            else if (currentSymbol.charAt(n) == 'k') symbol.setCharAt(n, 'g');
          }
          romaji.set(i, symbol.toString());
        }
        romaji.set(j, "");
        i--;
      } else if (nextSymbol.equals("P")) { // P series
        StringBuffer symbol = new StringBuffer(currentSymbol);
        for (n = 0; n < 2 && n < currentSymbol.length(); n++) {
          if      (currentSymbol.charAt(n) == 'h') symbol.setCharAt(n, 'p');
          else if (currentSymbol.charAt(n) == 'f') symbol.setCharAt(n, 'p');
        }
        romaji.set(i, symbol.toString());
        romaji.set(j, "");
        i--;
      } else if (isMember(nextSymbol, ySeries) && isMember(currentSymbol, iSeries)) { // Ci followed by YA, YU, or YO
        romaji.set(i, currentSymbol.substring(0, currentSymbol.length() - 1)); // Deletes vowel
        currentSymbol = (String)romaji.get(i);
        if (isMember(currentSymbol, palatalObs)) romaji.set(j, nextSymbol.substring(1,2));
      } else if (isMember(nextSymbol, smallVowels)) { // te, fu followed by small vowels
        if (currentSymbol.equals("fu")  ||
            currentSymbol.equals("ffu") ||
            currentSymbol.equals("te")  ||
            currentSymbol.equals("tte") ||
            currentSymbol.equals("de")  ||
            currentSymbol.equals("dde") ||
            isMember(currentSymbol, iSeries))
          romaji.set(i, currentSymbol.substring(0, currentSymbol.length() - 1)); // Deletes vowel
        else if (currentSymbol.equals("u")) romaji.set(i, "w");
      } else if (nextSymbol.equals("LONG")) { // Long vowels
        n = currentSymbol.length();
        if (n > 0) romaji.set(i, currentSymbol + currentSymbol.substring(n - 1)); // Lengthen vowel
        romaji.set(j, "");
      } else if (currentSymbol.equals("TSU")) { // Geminate C
        romaji.set(j, nextSymbol.substring(0, 1) + nextSymbol);
        romaji.set(i, "");
      }
    }
    // Collect everything in single romanized string doing transPunc in process
    StringBuffer result = new StringBuffer(256);
    result.append('_');
    for (i = 0; i < romaji.size(); i++) {
      String s = (String)romaji.get(i);
      if (s.length() > 0) result.append(s.toLowerCase());
    }
    result.append('_');
    String text = result.toString();
    // End of kan2rom
    // Apply rules translating text to phones letter by letter
    result.setLength(0);
    for (i = 0; i < text.length(); i++) {
      char c = text.charAt(i);
      // Skip spaces
      if (c == '_') continue;
      int set = (int)c - (int)'a';
      if (set >= MAXRULESETS || set < 0) throw new VoxFormatException("VoxAddJA.applyRules: No rules for character '" + c + "'");
      Rule[] ruleSet = ruleTable[set];
      if (ruleSet == null) throw new VoxFormatException("VoxAddJA.applyRules: null rules for character '" + c + "'");
      int absorbed = 0;
      for (j = 0; j < ruleSet.length; j++) {
        Rule rule = ruleSet[j];
        if ((absorbed = match(text, i, rule.letter)) > 0) {
          // Concatenate core of a sound, stripping flanking //
          if ((n = rule.sound.length()) > 2)
            result.append(rule.sound.substring(1, n - 1) + " ");
          break; // Done with this letter
        }
      }
      // Skip no. of chars. already absorbed.
      if (absorbed > 1) i += absorbed - 1;
      else if (absorbed == 0) throw new VoxFormatException("VoxAddJA.applyRules: No rule match character '" + c + "'");
    }
    if (result.length() == 0) throw new VoxFormatException("VoxAddJA.applyRules: Empty result");
    return result.toString().trim();
  }

  final static String VOWELS        = "aeiou";
  final static String FR_VOWELS     = "eiy";
  final static String CONSONANTS    = "bcdfghjklmnprstwz";
  final static String VD_CONSONANTS = "bdgjlmnrwz";
  final static int    RIGHT         = 1;
  final static int    LEFT          = -1;

  // Match  characters beginning from startIndex against a rule; if match,
  // return the number of extra characters to advance, 0 otherwise.
  static int match(String text, int startIndex, String rule) {
    int currTextIndex, currRuleIndex, skip, absorbed = 1;
    // Index of the first letter after '[' in the rule, which must match current letter in text
    int ruleIndex = rule.indexOf(text.charAt(startIndex), rule.indexOf('['));
    if (ruleIndex < 0) return 0;
    if (ruleIndex > 1) {
      // Not 1st char in rule; look for left context.
      currTextIndex = startIndex - 1;
      // Compute scan index on rule, skipping over '['.
      currRuleIndex = ruleIndex - 2;
      // Match letter against target in rule leftward.
      while (currRuleIndex >= 0 && currTextIndex >= 0) {
        if ((skip = symbolEquald(text, currTextIndex, rule, currRuleIndex, LEFT)) > 0) currTextIndex -= skip;
        else                                                                           return 0;
        currRuleIndex--;
      }
    }
    if (ruleIndex + 1 < rule.length() - 1) {
      // Not single char in the rule; look for right context.
      currTextIndex = startIndex + 1;
      currRuleIndex = ruleIndex + 1;
      boolean passedBracket = false;
      while (currRuleIndex < rule.length() && currTextIndex < text.length()) {
        // Count letters inside the brackets, i.e. absorbed by the rule.
        if (rule.charAt(currRuleIndex) == ']')                                               passedBracket = true;
        else if ((skip = symbolEquald(text, currTextIndex, rule, currRuleIndex, RIGHT)) > 0) currTextIndex += skip;
        else                                                                                 return 0;
        if (!passedBracket) absorbed++;
        currRuleIndex++;
      }
    }
    return absorbed;
  }

  static int symbolEquald(String text, int textIndex, String rule, int ruleIndex, int direction) {
    if (rule.charAt(ruleIndex) == ',') {
      // Try to match one of voiceless consonants
      for (int i = 0; i < vl_consonants.length; i++) {
        String s = vl_consonants[i];
        int l = s.length();
        int index = direction == RIGHT ? textIndex : textIndex - l + 1;
        if (text.regionMatches(index, s, 0, l)) return l;
      }
    } else if (text.charAt(textIndex) == rule.charAt(ruleIndex)) return 1;
    // Original C-source handled following rule symbols "#.^+:", which are not present in current ruleTable
    return 0;
  }
}
