// TemplateTable -> VoxTable -> VoxBlock

package voxware.voxfile;

public class TemplateTable extends VoxTable {

  // Access methods
  public int     getTemplateCount() {return ulaOffsets.length;}
  public long[]  getTemplateOffsets() {return ulaOffsets;}
  public int[] getTemplateData() {return usaData;}

   // Constructor. Subclasses must setup blockID
  TemplateTable() {
  }

  TemplateTable(long[] offsets, int[] data) {
    super(offsets, data);
  }
}
