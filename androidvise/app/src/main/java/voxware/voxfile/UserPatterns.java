// UserPatterns -> VoxBlock

package voxware.voxfile;

import java.io.*;

public class UserPatterns extends VoxBlock {
   // Constants

  public static final int EMBEDDED_NAME_SIZE   = 38;
  public static final int MAX_PARAMETERS       = 16;
  public static final int MAX_SILENCE_PATTERNS = 13;
  public static final int MAX_WORD_PATTERNS    = Short.MAX_VALUE;

  public static final int GENDER_FEMALE        = 1;
  public static final int GENDER_MALE          = 2;
  public static final int GENDER_UNISEX        = 3;
  public static final int GENDER_GIRL          = 4;
  public static final int GENDER_BOY           = 5;
  public static final int GENDER_CHILD         = 6;

  public static final int TRAIN_SCHEME_INCRM   = 0;
  public static final int TRAIN_SCHEME_BATCH   = 1;
  public static final int TRAIN_SCHEME_MIXED   = 2;

   // Instance Fields and their access method

  private VoxName        vnVocabularyName;
  public  String         getVocabularyName() {return vnVocabularyName.getName();}
  public  void           setVocabularyName(String s) throws VoxFormatException {vnVocabularyName.setName(s);}

  private int            ubGender;
  public  int            getGender() {return ubGender;}
  public  void           setGender(int g) {ubGender = g;}

  private int            ubDiscreteMinCount;
  public  int            getDiscreteMinCount() {return ubDiscreteMinCount;}
  public  void           setDiscreteMinCount(int c) {ubDiscreteMinCount = c;}

  private int            ubContinuousMinCount;
  public  int            getContinuousMinCount() {return ubContinuousMinCount;}
  public  void           setContinuousMinCount(int c) {ubContinuousMinCount = c;}

  private int            usAlwaysFFFF;
  private int            usAlwaysZero;

  private long           ulWildcardSum;
  public  long           getWildcardSum() {return ulWildcardSum;}
  public  void           setWildcardSum(long l) {ulWildcardSum = l;}

  private long           ulWildcardSumSquares;
  public  long           getWildcardSumSquares() {return ulWildcardSumSquares;}
  public  void           setWildcardSumSquares(long l) {ulWildcardSumSquares = l;}

  private int            usWildcardCount;
  public  int            getWildcardCount() {return usWildcardCount;}
  public  void           setWildcardCount(int c) {usWildcardCount = c;}

  private int            usScriptOrigin;
  private int            usScriptOffset;

  private VoxName        vnDate;
  public  String         getDate() {return vnDate.getName();}
  public  void           setDate(String s) throws VoxFormatException {vnDate.setName(s);}

  private int            ubTrainScheme;
  public  int            getTrainScheme() {return ubTrainScheme;}
  public  void           setTrainScheme(int v) {ubTrainScheme = v;}

  private VoxName        vnUserName;
  public  String         getUserName() {return vnUserName.getName();}
  public  void           setUserName(String s) throws VoxFormatException {vnUserName.setName(s);}

  private int            ubReserved;
  private int            usNonAPSPassCount;

  private int            usNumberPatterns;
  public  int            getNumberPatterns() {
    usNumberPatterns = 0;
    for (int i = 0; i < patterns.length; i++)
      if (patterns[i] != null) usNumberPatterns++;
    return usNumberPatterns;
  }

  private int            ubNumberParameters;
  public  int            getNumberParameters() {return (ubNumberParameters = parameters.length);}

  private VoxParameter[] parameters; // <= 16. only first 10 are essential, the rest are to be 0-filled.
  public  VoxParameter[] getParameters() {return parameters;}
  public  void           setParameters(VoxParameter[] p) {parameters = p;}

  private VoxPattern[]   patterns;   // Some entries may be null. usNumberPatterns counts real patterns.
  public  VoxPattern[]   getPatterns() {return patterns;}
  public  void           setPatterns(VoxPattern[] p) {patterns = p;}

   // Constructors
   // Package scope for read()
  UserPatterns() {blockID = USER_PATTERNS;}

  // Public
  public UserPatterns(int classId, int feVersion, int wordCount) {
    FrontEndVersion fev = new FrontEndVersion(feVersion); // Throws IllegalArgumentException if version is bad
    if (fev.getMode() != FrontEndVersion.VISE) throw new IllegalArgumentException("UserPatterns: FLEX mode is not supported");
    blockID = USER_PATTERNS;
    classID = classId;
    if (classID == 0) { // Silences
      wordCount = MAX_SILENCE_PATTERNS;
      vnVocabularyName = new VoxName(EMBEDDED_NAME_SIZE, "Global Silences");
    } else {
      if (wordCount > MAX_WORD_PATTERNS) wordCount = MAX_WORD_PATTERNS;
      if (wordCount <= 0) wordCount = 1;
      vnVocabularyName = new VoxName(EMBEDDED_NAME_SIZE);
    }
    usAlwaysFFFF = 0xFFFF;
    vnDate = new VoxName(EMBEDDED_NAME_SIZE);
    vnUserName = new VoxName(EMBEDDED_NAME_SIZE);
    ubNumberParameters = MAX_PARAMETERS;
    parameters = new VoxParameter[ubNumberParameters];
    parameters[0] = new VoxParameter(VoxParameter.FEVERSION, feVersion, 1);
    for (int i = 1; i < ubNumberParameters; i++)
      parameters[i] = new VoxParameter();
    patterns = new VoxPattern[wordCount];
  }
   // For Silence UserPatterns
  public UserPatterns(int classId, int feVersion) {
    this(classId, feVersion, 1);
  }

   // Instance methods

   // Package scope
  long bodySize() { // Computes size, updates ubNumberParameters and usNumberPatterns, which may have changed
    long l = vnVocabularyName.bodySize() + 3 + 2*2 + 2*4 + 3*2 + vnDate.bodySize() + 1 + vnUserName.bodySize() + 1 + 2*2 + 1;
    l += (ubNumberParameters = parameters.length) * VoxParameter.bodySize();
    usNumberPatterns = 0;
    for (int i = 0; i < patterns.length; i++)
      if (patterns[i] != null) {
        usNumberPatterns++;
        l += patterns[i].bodySize();
      }
    return l;
  }

   // Package scope. blockID must have been read !!!
  UserPatterns read(VoxInputStream vis) throws IOException, VoxFormatException {
    try {
      readBlockHeader(vis);
      // Read object body
      vnVocabularyName = VoxName.read(vis, EMBEDDED_NAME_SIZE);
      ubGender = vis.readUnsignedByte();
      ubDiscreteMinCount = vis.readUnsignedByte();
      ubContinuousMinCount = vis.readUnsignedByte();
      usAlwaysFFFF = vis.readUns();
      if (usAlwaysFFFF != 0xFFFF) throw new VoxFormatException("UserPatterns.read: AlwaysFFFF != 0xFFFF");
      usAlwaysZero = vis.readUns();
      if (usAlwaysZero != 0) throw new VoxFormatException("UserPatterns.read: AlwaysZero != 0");
      ulWildcardSum = vis.readULong();
      ulWildcardSumSquares = vis.readULong();
      usWildcardCount = vis.readUns();
      usScriptOrigin = vis.readUns();
      usScriptOffset = vis.readUns();
      vnDate = VoxName.read(vis, EMBEDDED_NAME_SIZE);
      ubTrainScheme = vis.readUnsignedByte();
      vnUserName = VoxName.read(vis, EMBEDDED_NAME_SIZE);
      ubReserved = vis.readUnsignedByte();
      usNonAPSPassCount = vis.readUns();
      usNumberPatterns = vis.readUns();
      ubNumberParameters = vis.readUnsignedByte();
       // More checks
      if (ubNumberParameters > MAX_PARAMETERS) throw new VoxFormatException("UserPatterns.read: Too many parameters");
      if (classID == 0 && usNumberPatterns > MAX_SILENCE_PATTERNS) throw new VoxFormatException("UserPatterns.read: Too many silence patterns");
      parameters = new VoxParameter[ubNumberParameters];
      for (int i = 0; i < ubNumberParameters; i++)
        parameters[i] = VoxParameter.read(vis);
      patterns = new VoxPattern[(classID == 0) ? MAX_SILENCE_PATTERNS : usNumberPatterns];
      for (int i = 0; i < usNumberPatterns; i++)
        patterns[i] = VoxPattern.read(vis, classID);
    } catch (EOFException e) {
      throw new VoxFormatException("UserPatterns.read: Unexpected EOF");
    }
    return this;
  }

  void write(VoxOutputStream vos) throws IOException, VoxFormatException {
    writeBlockHeader(vos); // Computes this.bodySize(), updating ubNumberParameters, usNumberPatterns
    if (ubNumberParameters > MAX_PARAMETERS) throw new VoxFormatException("UserPatterns.write: Too many parameters");
    if (classID == 0 && usNumberPatterns > MAX_SILENCE_PATTERNS) throw new VoxFormatException("UserPatterns.write: Too many silence patterns");
    vnVocabularyName.write(vos);
    vos.writeByte(ubGender);
    vos.writeByte(ubDiscreteMinCount);
    vos.writeByte(ubContinuousMinCount);
    vos.writeUns(usAlwaysFFFF);
    vos.writeUns(usAlwaysZero);
    vos.writeULong(ulWildcardSum);
    vos.writeULong(ulWildcardSumSquares);
    vos.writeUns(usWildcardCount);
    vos.writeUns(usScriptOrigin);
    vos.writeUns(usScriptOffset);
    vnDate.write(vos);
    vos.writeByte(ubTrainScheme);
    vnUserName.write(vos);
    vos.writeByte(ubReserved);
    vos.writeUns(usNonAPSPassCount);
    vos.writeUns(usNumberPatterns);
    vos.writeByte(ubNumberParameters);
    for (int i = 0; i < ubNumberParameters; i++)
      parameters[i].write(vos);
    for (int i = 0; i < patterns.length; i++)
      if (patterns[i] != null) patterns[i].write(vos);
  }

  public String toString(String wordName) {
    int i, n;
    StringBuffer buffer = new StringBuffer(super.toString() + "UserPatterns:\n");
    buffer.append("Vocabulary: \"" + vnVocabularyName + "\", # Pats: " + usNumberPatterns + ", Count: " + usWildcardCount +
                  ", Mean: " + ((usWildcardCount > 0) ? ulWildcardSum/usWildcardCount : 0) + "; Gender: " + ubGender +
                  "; Scheme: " + ubTrainScheme + "\n");
    // Print parms only once
    if (classID == 0) {
      buffer.append("PAR#    PARV    PARC\n");
      for (i = 0; i < ubNumberParameters; i++)
        buffer.append(parameters[i]);
    }
    for (i = 0, n = 0; i < patterns.length; i++)
      if (patterns[i] != null) {
        if (wordName != null && !wordName.equals(patterns[i].getWordName())) continue;
        buffer = StringFormatter.appendTo(buffer, -4, String.valueOf(++n), "  ");
        buffer.append(patterns[i]);
      }
    return buffer.toString();
  }

  public String toString () {
    return toString(null);
  }
}

