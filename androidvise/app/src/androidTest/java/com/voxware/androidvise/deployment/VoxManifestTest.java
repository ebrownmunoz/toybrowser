package com.voxware.androidvise.deployment;


import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import com.voxware.browser.android.deployment.DeploymentManager;
import com.voxware.browser.android.deployment.VoxManifest;
import com.voxware.browser.android.documentserver.AndroidDocumentServer;
import com.voxware.browser.documentserver.DocumentServer;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by eric on 7/5/16.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class VoxManifestTest {

    private static final String TEST_URI =  "http://192.168.210.109:8080/";

    ByteArrayInputStream bais = new ByteArrayInputStream(
      ("{ resources : " +
              "[{" +
              "\"name\": \"foo\"," +
              "\"md5\" : \"12ABC\"" +
              "},{" +
              "name: \"bar\"," +
              "md5 : \"34DEF\"" +
              "}]" +
              "}").getBytes()
    );

    public VoxManifestTest() {
    }

    @Test
    public void testManifestTest() {
        Context context = InstrumentationRegistry.getTargetContext();;
        final File filesDir = context.getFilesDir();
        VoxManifest manifest = VoxManifest.parseVoxManifest(bais);
    }

    @Test
    public void testDeoplymentManagerTest() throws URISyntaxException {
        Context context = InstrumentationRegistry.getTargetContext();
        DocumentServer docServer = new AndroidDocumentServer(new URI (TEST_URI),context);

        DeploymentManager manager = new DeploymentManager(context, docServer);

        VoxManifest manifest = manager.getManifestFromServer("71");

        manager.writeManifestToInternalStorage(manifest);

        VoxManifest copy  = manager.getManifestFromInternalStorage();
    }
}
