# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

-dontpreverify
-dontobfuscate

-dontwarn java.awt.**
-dontwarn java.lang.management.**
-dontwarn javax.swing.**
-dontwarn javax.naming.**
-dontwarn org.apache.harmony.xnet.provider.jsse.**

-dontwarn org.mozilla.javascript.tools.**
-dontwarn org.mozilla.javascript.xml.**

-keep,includedescriptorclasses class ch.qos.** { *; }
-keep,includedescriptorclasses class org.slf4j.** { *; }
#-keep,includedescriptorclasses class com.symbol.emdk.** { *; }
#-keep,includedescriptorclasses class com.nuance.android.vocalyzer.** { *; }
-keepattributes *Annotation*

-keep,includedescriptorclasses class voxware.** { *; }
-keep,includedescriptorclasses class com.voxware.** { *; }

-printseeds
# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
