package voxware.util;

import java.util.*;

public class Mailbox {
  private int      numMessages = 0;
  private int      numPriorities;         // The highest priority is zero; the lowest is numPriorities - 1;
  private Vector[] mailbox;

   // Construct a single-priority mailbox
  public Mailbox() {
    this(1);
  }

   // Construct a prioritized mailbox
  public Mailbox(int numPriorities) {
    mailbox = new Vector[numPriorities];
    for (int priority = 0; priority < numPriorities; priority++)
      mailbox[priority] = new Vector();
    this.numPriorities = numPriorities;
  }

   // Return the number of priorities
  public int numPriorities() {
    return this.numPriorities;
  }

   // Insert an Object at the highest (lowest-numbered) priority
  public synchronized void insert(Object obj) {
    mailbox[0].addElement(obj);
    numMessages++;
    notify();
  }

   // Insert an Object at a specified priority
  public synchronized void insert(Object obj, int priority) {
    mailbox[priority].addElement(obj);
    numMessages++;
    notify();
  }

   // Remove and return the oldest Object at the highest available (lowest-numbered) priority, blocking if none is available
  public synchronized Object remove() throws InterruptedException {
    while (numMessages == 0)
      wait();                             // Throws InterruptedException
    Object object = null;
    for (int priority = 0; priority < numPriorities; priority++) {
      if (!mailbox[priority].isEmpty()) {
        object = mailbox[priority].firstElement();
        mailbox[priority].removeElementAt(0);
        numMessages--;
        break;
      }
    }
    return object;
  }

   // Remove and return the oldest Object at the specified priority, blocking if none is available
  public synchronized Object remove(int priority) throws InterruptedException {
    while (numMessages == 0 || mailbox[priority].isEmpty())
      wait();                             // Throws InterruptedException
    Object object = mailbox[priority].firstElement();
    mailbox[priority].removeElementAt(0);
    numMessages--;
    return object;
  }

   // Return a reference to the oldest Object at the highest available (lowest-numbered) priority, blocking if none is available
  public synchronized Object peek() throws InterruptedException {
    while (numMessages == 0)
      wait();                             // Throws InterruptedException
    Object object = null;
    for (int priority = 0; priority < numPriorities; priority++) {
      if (!mailbox[priority].isEmpty()) {
        object = mailbox[priority].firstElement();
        break;
      }
    }
    return object;
  }

   // Return a reference to the oldest Object at the specified priority, blocking if none is available
  public synchronized Object peek(int priority) throws InterruptedException {
    while (numMessages == 0 || mailbox[priority].isEmpty())
      wait();                             // Throws InterruptedException
    return mailbox[priority].firstElement();
  }

   // Remove and return the oldest Object at the highest available (lowest-numbered) priority, returning null after timeout msec if none is available
  public synchronized Object remove(long timeout) throws InterruptedException {
    while (numMessages == 0) {
      wait(timeout);                      // Throws InterruptedException
      if (numMessages == 0) return null;  // Return null if the wait has timed out
    }
    Object object = null;
    for (int priority = 0; priority < numPriorities; priority++) {
      if (!mailbox[priority].isEmpty()) {
        object = mailbox[priority].firstElement();
        mailbox[priority].removeElementAt(0);
        numMessages--;
        break;
      }
    }
    return object;
  }

   // Return a reference to the oldest Object at the highest available (lowest-numbered) priority, returning null after timeout msec if none is available
  public synchronized Object peek(long timeout) throws InterruptedException {
    while (numMessages == 0) {
      wait(timeout);                      // Throws InterruptedException
      if (numMessages == 0) return null;  // Return null if the wait has timed out
    }
    Object object = null;
    for (int priority = 0; priority < numPriorities; priority++) {
      if (!mailbox[priority].isEmpty()) {
        object = mailbox[priority].firstElement();
        break;
      }
    }
    return object;
  }

   // Remove and return the oldest Object at the highest available (lowest-numbered) priority, or null if none is available
  public synchronized Object removeNow() {
    Object object = null;
    if (numMessages > 0) {
      for (int priority = 0; priority < numPriorities; priority++) {
        if (!mailbox[priority].isEmpty()) {
          object = mailbox[priority].firstElement();
          mailbox[priority].removeElementAt(0);
          numMessages--;
          break;
        }
      }
    }
    return object;
  }

   // Return a reference to the oldest Object at the highest available (lowest-numbered) priority, or null if none is available
  public synchronized Object peekNow() {
    Object object = null;
    if (numMessages > 0) {
      for (int priority = 0; priority < numPriorities; priority++) {
        if (!mailbox[priority].isEmpty()) {
          object = mailbox[priority].firstElement();
          break;
        }
      }
    }
    return object;
  }

   // Remove and return the oldest Object at the specified priority, or null if none is available
  public synchronized Object removeNow(int priority) {
    Object object = null;
    if (!mailbox[priority].isEmpty()) {
      object = mailbox[priority].firstElement();
      mailbox[priority].removeElementAt(0);
      numMessages--;
    }
    return object;
  }

   // Return a reference to the oldest Object at the specified priority, or null if none is available
  public synchronized Object peekNow(int priority) {
    Object object = null;
    if (!mailbox[priority].isEmpty())
      object = mailbox[priority].firstElement();
    return object;
  }

   // Remove the oldest instance of the given Object at the highest available priority, returning false if the object is not in the mailbox
  public synchronized boolean removeInstance(Object object) {
    if (numMessages > 0) {
      for (int priority = 0; priority < numPriorities; priority++) {
        if (mailbox[priority].removeElement(object)) {
          numMessages--;
          return true;
        }
      }
    }
    return false;
  }

   // Remove the oldest instance of the given object at the specified priority, returning false if the object is not present at that priority
  public synchronized boolean removeInstance(Object object, int priority) {
    if (numMessages > 0 && mailbox[priority].removeElement(object)) {
      numMessages--;
      return true;
    } else {
      return false;
    }
  }

   // Return the total number of Objects in the mailbox
  public synchronized int numobj() {
    return numMessages;
  }

   // Return the number of Objects at the given priority in the mailbox
  public synchronized int numobj(int priority) {
    return mailbox[priority].size();
  }

   // Flush the mailbox and return a Vector of its contents in priority order
  public synchronized Vector flush() {
    Vector vector = (numMessages > 0) ? new Vector(numMessages) : null;
    for (int priority = 0; numMessages > 0 && priority < numPriorities; priority++) {
      Vector level = mailbox[priority];
      for (int i = 0; i < level.size(); i++)
        vector.addElement(level.elementAt(i));
      numMessages -= level.size();
      level.removeAllElements();
    }
    return vector;
  }

   // Flush the mailbox of Objects at the specified priority and return a Vector of the Objects flushed
  public synchronized Vector flush(int priority) {
    Vector vector = null;
    int size = mailbox[priority].size();
    if (size > 0) {
      vector = new Vector(size);
      Vector level = mailbox[priority];
      for (int i = 0; i < size; i++)
        vector.addElement(level.elementAt(i));
      numMessages -= size;
      level.removeAllElements();
    }
    return vector;
  }

   // Flush the mailbox and return an array of arrays of its contents indexed by priority
  public synchronized Object[][] priorityFlush() {
    Object[][] array = new Object[numPriorities][];
    for (int priority = 0; priority < numPriorities; priority++) {
      Vector level = mailbox[priority];
      array[priority] = new Object[level.size()];
      level.copyInto(array[priority]);
      numMessages -= level.size();
      level.removeAllElements();
    }
    return array;
  }
}
