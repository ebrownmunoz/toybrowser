package voxware.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Created by edh on 1/28/2016.
 */
public class WaveFile {
    private static final Charset US_ASCII = Charset.forName("US_ASCII");
    private static final int RIFF = 0x52494646; // 'RIFF' as int
    private static final int WAVE = 0x57415645; // 'WAVE' as int
    private static final int FMT_ = 0x666d7420; // 'fmt ' as int
    private static final int DATA = 0x64617461; // 'data' as int
    private final int sampleRate;
    private final short channels;
    private final short bitsPerSample;
    private final byte[] samples;

    /**
     * Constructs a WaveFile from the byte[].
     * @param wavefile the byte[] containing a RIFF header + audio header
     */
    public WaveFile(byte[] wavefile) {
        ByteBuffer data = ByteBuffer.wrap(wavefile).order(ByteOrder.LITTLE_ENDIAN).asReadOnlyBuffer();
        if (data.getInt(0) != RIFF) {
            throw new IllegalArgumentException("File does not start with RIFF header");
        }
        int size = data.getInt(4);
        if (size != wavefile.length) {
            throw new IllegalArgumentException("Declared wave file size does not match array length");
        }
        if (data.getInt(8) != WAVE) {
            throw new IllegalArgumentException("Not a WAVE file?");
        }
        if (data.getInt(12) != FMT_) {
            throw new IllegalArgumentException("FMT block expected");
        }
        if (data.getInt(16) != 16) {
            throw new IllegalArgumentException("Format error");
        }
        if (data.getShort(20) != 1) {
            throw new IllegalArgumentException("Not PCM data");
        }

        channels = data.getShort(22);
        sampleRate = data.getInt(24);
        int byteRate = data.getInt(28);
        short blockAlign = data.getShort(32);
        bitsPerSample = data.getShort(34);
        if (data.getInt(36) != DATA) {
            throw new IllegalArgumentException("Expecting 'data' at offset 36");
        }
        samples = new byte[data.getInt(40)];
        if (byteRate != sampleRate * bitsPerSample * channels / 8) {
            throw new IllegalArgumentException("Offset 28[" + byteRate + "] != " + (sampleRate * bitsPerSample * channels / 8));
        }
        if (blockAlign != (bitsPerSample * channels / 8)) {
            throw new IllegalArgumentException("Offset 32[" + blockAlign + "] != " + (bitsPerSample * channels / 8));
        }
        data.get(samples, 44, samples.length);
    }

    public WaveFile(int sampleRate, short channels, short bitsPerSample, ByteOrder byteOrder, byte[] audioData) {
        this.sampleRate = sampleRate;
        this.channels = channels;
        this.bitsPerSample = bitsPerSample;
        this.samples = Arrays.copyOf(audioData, audioData.length);
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public short getChannels() {
        return channels;
    }

    public short getBitsPerSample() {
        return bitsPerSample;
    }

    public InputStream getPcmData() {
        return new ByteArrayInputStream(samples);
    }

    public void writeTo(OutputStream os) throws IOException {
        Channels.newChannel(os).write(toByteBuffer());
    }

    private ByteBuffer toByteBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(samples.length + 44).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put("RIFF".getBytes(US_ASCII));
        buffer.putInt(samples.length + 44);
        buffer.put("WAVE".getBytes(US_ASCII));
        buffer.put("fmt ".getBytes(US_ASCII));
        buffer.putInt(16); // previous size
        buffer.putShort((short) 1); // 1 = pcm
        buffer.putShort(channels); // channels
        buffer.putInt(sampleRate); // sample rate
        buffer.putInt((sampleRate * bitsPerSample * channels) / 8); // sample rate * bits per sample * channels / 8
        buffer.putShort((short) (bitsPerSample * channels / 8)); // bits per sample * channels / 8
        buffer.putShort(bitsPerSample); // bits per sample
        buffer.put("data".getBytes(US_ASCII));
        buffer.putInt(samples.length);
        buffer.put(samples);
        buffer.rewind();
        return buffer;
    }
}
