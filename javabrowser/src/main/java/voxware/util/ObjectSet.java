/*
 *
 *  * Copyright © 2015. Voxware, Inc. All Rights Reserved.
 *  * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 *  * title and interest to the Licensed Software, including all patents, copyrights,
 *  * trademarks, trade secrets, and other proprietary rights thereto.
 *  * All copies of the Licensed Software are subject to the terms and conditions of the
 *  * executed License Agreement on file with Voxware that has the right to use license
 *  * keys to control access to the Licensed Software.
 *
 *
 */

package voxware.util;

import java.util.LinkedHashSet;

/**
 * Created by edh on 7/30/2015.
 */
public class ObjectSet extends LinkedHashSet<Object> {
}
