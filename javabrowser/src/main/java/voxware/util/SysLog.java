/*
 *
 *  * Copyright © 2015. Voxware, Inc. All Rights Reserved.
 *  * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 *  * title and interest to the Licensed Software, including all patents, copyrights,
 *  * trademarks, trade secrets, and other proprietary rights thereto.
 *  * All copies of the Licensed Software are subject to the terms and conditions of the
 *  * executed License Agreement on file with Voxware that has the right to use license
 *  * keys to control access to the Licensed Software.
 *
 *
 */

package voxware.util;

import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SysLog for backwards compatibility with old voxbrowser, new code should directly use slf4j. This class has been stripped down based on existing Java and JS LiveConnect calls.
 * Created by edh on 7/30/2015.
 */
@Deprecated
public class SysLog {
    // ALWAYS refer to PrintMasks symbolically, since the values may change
    // Browser PrintMasks
    public static final long AUDIOPLAYER_MASK = 1L;                            // 0x00000000001
    public static final long CATCH_MASK = AUDIOPLAYER_MASK << 1;         // 0x00000000002
    public static final long EXECUTABLECONTENT_MASK = CATCH_MASK << 1;               // 0x00000000004
    public static final long FILLED_MASK = EXECUTABLECONTENT_MASK << 1;   // 0x00000000008
    public static final long FORM_MASK = FILLED_MASK << 1;              // 0x00000000010
    public static final long GOTO_MASK = FORM_MASK << 1;                // 0x00000000020
    public static final long GRAMMAR_MASK = GOTO_MASK << 1;                // 0x00000000040
    public static final long HARDWARE_MASK = GRAMMAR_MASK << 1;             // 0x00000000080
    public static final long INTERPRET_MASK = HARDWARE_MASK << 1;            // 0x00000000100
    public static final long INTERRUPT_MASK = INTERPRET_MASK << 1;           // 0x00000000200
    public static final long OBJECT_MASK = INTERRUPT_MASK << 1;           // 0x00000000400
    public static final long PREPROCESS_MASK = OBJECT_MASK << 1;              // 0x00000000800
    public static final long PROPERTY_MASK = PREPROCESS_MASK << 1;          // 0x00000001000
    public static final long RADIOSTATUS_MASK = PROPERTY_MASK << 1;            // 0x00000002000
    public static final long RECOGNIZER_MASK = RADIOSTATUS_MASK << 1;         // 0x00000004000
    public static final long SCRIPT_MASK = RECOGNIZER_MASK << 1;          // 0x00000008000
    public static final long SPEAKER_MASK = SCRIPT_MASK << 1;              // 0x00000010000
    public static final long SUBDIALOG_MASK = SPEAKER_MASK << 1;             // 0x00000020000
    public static final long VARIABLES_MASK = SUBDIALOG_MASK << 1;           // 0x00000040000

    // JSAPI PrintMasks
    public static final long JSAPI_MASK = 1L << 20;                      // 0x00000100000
    public static final long SAPIVISE_MASK = JSAPI_MASK << 1;               // 0x00000200000
    public static final long KEYPAD_MASK = SAPIVISE_MASK << 1;            // 0x00000400000

    // VJAP PrintMask
    public static final long VJAP_MASK = 1L << 24;                      // 0x00001000000

    // VJML PrintMask
    public static final long VJML_MASK = 1L << 25;                      // 0x00002000000

    // Utility PrintMasks
    public static final long CACHE_MASK = 1L << 32;                      // 0x00100000000
    public static final long ALLOCATION_MASK = CACHE_MASK << 1;               // 0x00200000000
    public static final long NISTHEADER_MASK = ALLOCATION_MASK << 1;          // 0x00400000000
    public static final long PCFG_MASK = NISTHEADER_MASK << 1;          // 0x00800000000
    public static final long URLCONNECTION_MASK = PCFG_MASK << 1;                // 0x01000000000
    public static final long URLLOADER_MASK = URLCONNECTION_MASK << 1;       // 0x02000000000
    public static final long UTILITY_MASK = URLLOADER_MASK << 1;           // 0x04000000000
    public static final long XMLPARSER_MASK = UTILITY_MASK << 1;             // 0x08000000000

    // If uninitialized, log to System.out without pruning
    public static PrintStream stream = System.out;

    public static int printLevel = 5;
    public static long printMask = 0xffffffffffffffffl ^ SysLog.VJML_MASK;

    public static void setPrintLevel(int pl) {
        printLevel = pl;
    }

    public static int getPrintLevel() {
        return printLevel;
    }

    public static void setPrintMask(long pm) {
        printMask = pm;
    }

    public static long getPrintMask() {
        return printMask;
    }

    public static void setPrintMaskBits(long pm) {
        printMask |= pm;
    }

    public static void clearPrintMaskBits(long pm) {
        printMask &= ~pm;
    }

    public static boolean printMaskBitsSet(long pm) {
        return (printMask & pm ^ pm) == 0;
    }

    public static boolean printMaskBitsClear(long pm) {
        return (printMask & pm) == 0;
    }

    public static boolean shouldLog(int prio, long pm) {
        return prio <= printLevel && printMaskBitsSet(pm);
    }

    public static boolean shouldLog(int prio) {
        return prio <= printLevel;
    }

    public static boolean shouldLog(long pm) {
        return printMaskBitsSet(pm);
    }

    public static synchronized void println(String str) {
        StackTraceElement e = Thread.currentThread().getStackTrace()[1];
        LoggerFactory.getLogger(e.getClassName()).info(str);
    }

    public static synchronized void println(int prio, String str) {
        StackTraceElement e = Thread.currentThread().getStackTrace()[1];
        Logger log = LoggerFactory.getLogger(e.getClassName());
        switch (prio) {
            case 0:
            case 1:
                log.error(str);
                break;
            case 2:
                log.warn(str);
                break;
            case 3:
                log.info(str);
                break;
            case 4:
                log.debug(str);
            case 5:
            default:
                log.trace(str);
                break;
        }

    }
}
