package voxware.util;

import java.util.Properties;

/**
 * Created by edh on 12/11/2015.
 */
public class Environment {
    private static Environment environmentProvider = new Environment();
    public static void setEnvironmentProvider(Environment environmentProvider) {
        Environment.environmentProvider = environmentProvider;
    }
    
    public static String getEnvironment(String envVarName) {
        return environmentProvider.get(envVarName);
    }
    
    public static void setEnvironment(String envVarName, String value) {
        environmentProvider.set(envVarName, value);
    }
    
    private Properties props = new Properties();
    
    protected String get(String envVarName) {
        return props.getProperty(envVarName);
    }
    
    protected void set(String envVarName, String value) {
        props.setProperty(envVarName, value);
    }
}
