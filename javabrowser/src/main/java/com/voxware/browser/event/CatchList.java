/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;

/**
 * Handle a list of Events in a context. This is designed to implement the W3C spec. The events are searched by document
 * order by first match (specificity of match is irrelevant).
 */
public class CatchList {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

 
    private List<Catch> catches = new LinkedList<Catch>();
    private final CatchList parent;

    /**
     * Constructs a new <code>CatchList</code> instance.
     */
    public CatchList() {
        this.parent = null;
    }

    /**
     * Constructs a new <code>CatchList</code> instance.
     * @param parent the parent catch list
     */
    public CatchList(CatchList parent) {
        this.parent = parent;
    }
  
    protected List<Catch> getAllCandidates(CatchFinder catchFinder, SessionContext sessionContext, BaseContext currentContext, String name, int count) {
        List<Catch> candidates = new ArrayList<Catch>(catches.size());
        for (Catch katch : catches) {
            // if the catch count is higher than count, it is automatically ineligible
            // if the name does not match according to the algorithm, it is ineligible
            // if the cond evaluates to false, it is ineligible
            // we check count first because it is most efficient
            // the other two will depend on the vxml page
            if (katch.getCount() <= count && catchFinder.isCandidateEventName(name, katch.getEventNames()) && evaluateCatchCond(currentContext, katch)) {
                candidates.add(katch);
            }
        }
        return candidates;
    }
    
   
    /**
     * Add a catch
     * 
     * @param name
     * @param handler
     */
    public void addCatch(Catch katch) {
        catches.add(katch);
    }

    /**
     * Adds a list of catches
     * 
     * @param catches
     */
    public void addCatches(List<Catch> catches) {
        this.catches.addAll(catches);
    }


    /**
     * Returns the parent.
     * 
     * @return the parent.
     */
    public CatchList getParent() {
        return parent;
    }

    

    private boolean evaluateCatchCond(BaseContext currentContext, Catch katch) {
        return katch.getCond() == null || (currentContext.evaluateToBoolean(katch.getCond()));
    }
}
