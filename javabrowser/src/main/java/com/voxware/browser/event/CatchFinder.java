/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.util.List;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;

/**
 * CatchFinder
 * Strategy pattern interface for finding cacthes
 * 
 * @author edh
 */
public interface CatchFinder {
    /**
     * Finds the appropriate catch in the catch list.
     * @param sessionContext the sesion context
     * @param currentContext the current context
     * @param catchList the catch list to search
     * @param name the event name
     * @param count the event count
     * @return a catch or null if onse is not found
     */
    public Catch findCatch(SessionContext sessionContext, BaseContext currentContext, CatchList catchList, String name, int count);
    /**
     * Determines if any of the catch event names fulfills the candidate
     * @param eventName the event name
     * @param catchEventNames the catch event names
     * @return true if the catch event names match the event name
     */
    public boolean isCandidateEventName(String eventName, List<String> catchEventNames);
}
