/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.util.List;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;

/**
 * W3CCacthFinder
 *
 * @author edh
 */
public class LegacyCatchFinder implements CatchFinder {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /*
     * 
     * (non-Javadoc)
     * @see com.voxware.browser.event.CatchFinder#findCatch(com.voxware.browser.context.SessionContext, com.voxware.browser.context.AbstractContext, com.voxware.browser.event.CatchList, java.lang.String, int)
     */
    @Override
    public Catch findCatch(SessionContext sessionContext, BaseContext currentContext, CatchList catchList, String name, int count) {
        List<Catch> candidates = catchList.getAllCandidates(this, sessionContext, currentContext, name, count);
        CatchList parent = catchList.getParent();
        

        if (candidates.size() == 0) {
            // we have none, so delegate all work to the parent if any
            Catch parentCandidate = null;
            if (parent != null) {
                parentCandidate = findCatch(sessionContext, currentContext, parent, name, count);
            }
            if (parentCandidate == null) {
                if (name.isEmpty()) {
                    return null;
                } else {
                    int lastDot = name.lastIndexOf('.');
                    if (lastDot != -1) {
                        return findCatch(sessionContext, currentContext,catchList, name.substring(0, lastDot), count);
                    } else {
                        return findCatch(sessionContext, currentContext,catchList, "", count);
                    }
                }
            } else {
                return parentCandidate;
            }
        }
        // Find parent.
        List<Catch> parentCandidates = null;
        if (parent != null) {
            parentCandidates = parent.getAllCandidates(this, sessionContext, currentContext, name, count);
        }
        if (parentCandidates != null && parentCandidates.size() > 0) {
            candidates.addAll(parentCandidates);
        }

        // filter by count (find first candidate with highest count <= count)
        int highestCount = 0;
        Catch bestMatch = null;
        for (Catch candidate : candidates) {
            int candidateCount = candidate.getCount();
            if (candidateCount <= count && candidateCount > highestCount) {
                bestMatch = candidate;
                highestCount = candidate.getCount();
            }
        }

        return bestMatch;
    }
    
    public boolean isCandidateEventName(String eventName, List<String> catchEventNames) {
        boolean isCandidate = false;
        for (String catchEventName : catchEventNames) {
            isCandidate = isCandidateEventName(eventName, catchEventName);
            if (isCandidate) {
                break;
            }
        }
        return isCandidate;
    }

    /**
     * Compares the eventName and catchEventName to see if they are candidate matches. An eventName and catchEventName
     * match if each segment matches.
     * 
     * @param eventName the thrown event name
     * @param catchEventName the event name declared by a catch
     * @return true if the catch event name is a candidate match for event name
     */
    protected boolean isCandidateEventName(String eventName, String catchEventName) {
        if (catchEventName == null || catchEventName.isEmpty() || catchEventName.equals(".")) {
            return eventName.isEmpty();
        }
        return eventName.equals(catchEventName);
    }
}
