/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.model.form.InputItem;

/**
 * FilledList
 *
 * @author edh
 */
public class FilledList {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(FilledList.class);

    private List<FilledFilter> filleds = new LinkedList<FilledFilter>();

    public FilledList() {

    }

    public void addFilled(Filled filled) {
        filleds.add(new FilledFilter(filled));
    }

    public void addFilled(InputItem inputItem) {
        filleds.add(new FilledFilter(inputItem));
    }

    public List<Filled> findFilledByCriteria(FormContext context, FormItem filledElement) throws InterpreterException {
        return findFilledByCriteria(context, Arrays.asList(filledElement));
    }
    
    public List<Filled> findFilledByCriteria(FormContext context, List<FormItem> filledElement) throws InterpreterException {
        List<Filled> ret = new LinkedList<Filled>();
        for (FilledFilter filled : this.filleds) {
            if (filled.isFilledbyCriteria(context, filledElement)) {
                ret.addAll(filled.getFilleds());
            }
        }
        return ret;
    }

    protected class FilledFilter {
        private final Filled filled;
        private final InputItem inputItem;

        public FilledFilter(Filled filled) {
            this.filled = filled;
            this.inputItem = null;
        }

        public FilledFilter(InputItem inputItem) {
            this.filled = null;
            this.inputItem = inputItem;
        }

        // if inputItem return all filled handlers
        // else return filled (wrapped in array)
        public List<Filled> getFilleds() {
            if (inputItem != null) {
                return inputItem.getFilledHandlers();
            } else {
                return Arrays.asList(filled);
            }
        }
        /**
         * @param context
         * @param filledElements
         * @return
         * @throws InterpreterException
         */
        public boolean isFilledbyCriteria(FormContext context, List<FormItem> filledElements) throws InterpreterException {
            if (inputItem != null) {
                // we are input item filled, and the list of just filled elements contains inputItem
                return filledElements.contains(inputItem);
            } else {
                // we are form level filled
                if (Filled.MODE_ALL.equals(filled.getMode())) {
                    // W3C spec says in this mode, all mentioned input items need to be filled and at least one is filled by last user input
                    boolean found = false;
                    List<String> names = filled.getNameList();
                    for (String name : names) {
                        FormItem item = context.findInputItem(name);
                        // item should never be null if validation passed (validation should check all input items)
                        if (!context.isComplete(item)) {
                            // we found an element that isn't complete, so that fails MODE_ALL
                            return false;
                        }
                    }
                    for (FormItem filledItem : filledElements) {
                        if (names.contains(filledItem.getName())) {
                            found = true;
                        }
                    }
                    // if we found everything in our namelist, but no names in the namelist match anything in the filledElements, we're not matching the criteria
                    return found;
                } else if (Filled.MODE_ANY.equals(filled.getMode())) {
                    List<String> names = filled.getNameList();
                    for (FormItem filledItem : filledElements) {
                        if (names.contains(filledItem.getName())) {
                            return true;
                        }
                    }
                    return false;
                } else {
                    throw new InterpreterException("Unknown mode for <filled> element: " + filled.getMode(),
                            filled.getNodeLocation());
                }
            }

        }
    }
}
