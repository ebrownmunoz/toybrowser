/**
 * 
 */
package com.voxware.browser.event;

import org.mozilla.javascript.NativeJavaObject;

/**
 * @author eric
 *
 */
public class Event extends NativeJavaObject {

    private final String name;
    private final String description;
    private final Throwable cause;
    
    /**
     * 
     */
    public Event(String name, String message, Throwable cause) {
        this.name = name;
        this.description = message;
        this.cause = cause;
    }
    
    public Event (String name, String message) {
        this(name, message, null);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the cause
     */
    public Throwable getCause() {
        return cause;
    }
    
    public String toString() {
        return "Event: " + name + ": " + description + ", cause: " + 
            ((cause != null) ? cause.getMessage() : "[NA]");
    }

}
