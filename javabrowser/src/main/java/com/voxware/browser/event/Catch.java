/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeObject;

import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents a <catch> event handler or any of the shortcuts (such as <filled> or <help>)
 */
public class Catch extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    private final ExecutableList executables;
    /**
     * The event attribute is mostly for debugging purposes. It is only used to fill in eventNames variable.
     */
    private final String event;
    private final List<String> eventNames;
    private final Integer count;
    private final String cond;

    /**
     * Constructs a new <code>Catch</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     * @param executableElements 
     */
    public Catch(Map<String, String> attributes, NodeLocation nodeLocation, ExecutableList executableElements) {
        super(attributes, nodeLocation);
        this.executables = (executableElements != null) ? executableElements : ExecutableList.EMPTY_LIST;
        event = getAttribute("event");
        if (event != null) {
            String[] events = event.split("\\s+");
            eventNames = Collections.unmodifiableList(Arrays.asList(events));
        } else {
            eventNames = Collections.unmodifiableList(Arrays.asList(""));
        }
        String countStr = getAttribute("count");
        Integer countValue = null;
        if (countStr != null) {
            try {
                countValue = Integer.valueOf(countStr);
            } catch (NumberFormatException e) {
                /*
                 * Do nothing for now, validate will need to realize that if count is null, we hit an invalid value
                 */
            }
        } else {
            countValue = 1;
        }
        count = countValue;
        cond = getAttribute("cond");

    }

    public Result handleEvent(SessionContext scontext, FormContext context, Event thrownEvent, CatchList catchList) {
        
        AnonymousContext acontext = new AnonymousContext(context);
        
        BrowserScope scope = acontext.getScope();
        
        scope.put("_event", scope, thrownEvent.getName());
        scope.put("_message", scope, thrownEvent.getDescription());
        
        Context rhinoContext = scontext.getContext();
        
        NativeObject javaToJS = new NativeObject();
        javaToJS.put("name", javaToJS, thrownEvent.getName());
        javaToJS.put("description", javaToJS, thrownEvent.getDescription());
        
        scope.put("event$", scope, javaToJS);
    
        
        Result result = executables.execute(scontext, acontext);

        return result;
    }

    /**
     * Get an unmodifiable list of executables.
     * 
     * @return
     */
    public ExecutableList getExecutables() {
        return executables;
    }

    /**
     * Gets the value of the event attribute. This value is parsed for the list of event names. This may also be null.
     * 
     * @return the name.
     */
    public String getEvent() {
        return event;
    }

    /**
     * Gets all the event names.
     * 
     * @return a List of event names
     */
    public List<String> getEventNames() {
        return eventNames;
    }

    /**
     * Gets the count attribute.
     * 
     * @return the count attribute
     */
    public Integer getCount() {
        return count;
    }

    /**
     * Gets the cond expression.
     * 
     * @return a cond expression or null
     */
    public String getCond() {
        return cond;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (count == null) {
            throw new ValidationException("count is invalid", getNodeLocation());
        }
    }
}
