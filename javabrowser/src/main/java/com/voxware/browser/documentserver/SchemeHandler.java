/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.InputStream;
import java.net.URI;

/**
 * SchemeHandler
 *
 * @author edh
 */
public interface SchemeHandler<T> {
    /**
     * Handles a scheme, like http or https
     * @param uri an absolute uri
     * @param parameters parameters
     * @return an InputStream
     * @throws DocumentServerException on error
     */
    DocumentServerInputStream handleScheme(URI uri, T parameters) throws DocumentServerException;
}
