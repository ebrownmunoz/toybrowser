/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeObject;

import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.model.NodeLocation;


/**
 * AbstractDocumentServer
 *
 * @author edh
 */
public abstract class AbstractDocumentServer implements DocumentServer {

    public static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final int DEFAULT_CONNECT_TIMEOUT = 5000;
    public static final int DEFAULT_READ_TIMEOUT = 10000;
    public static final String DEFAULT_CONTENT_TYPE = "application/x-www-form-urlencoded";

    /**
     * Constructs a new <code>AbstractDocumentServer</code> instance.
     */
    public AbstractDocumentServer() {
        super();
    }

    @Override
    public DocumentServerInputStream getDocument(URI uri) throws DocumentServerException {
        return getDocument(uri, null, CacheControl.FOREVER_CACHE);
    }

    @Override
    public DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters, CacheControl cacheControl) throws DocumentServerException {
        return getDocument(uri, parameters, cacheControl, DEFAULT_CONNECT_TIMEOUT, DEFAULT_READ_TIMEOUT);
    }

    @Override
    public DocumentServerInputStream postDocument(URI uri, Map<String, Object> parameters) throws DocumentServerException {
        return postDocument(uri, parameters, null, 0, 0);
    }
    
    /**
     * This takes a map of parameters and maps it into multipart form content type
     * @param parameters
     * @return a byte array representing octets of a UTF-8 encoded string or raw octets
     * @throws InterpreterException 
     */
    protected byte[] getMultipartFormFromParameters(Map<String, Object> parameters, String boundary, 
        NodeLocation nodeLocation) throws InterpreterException {
//        StringBuffer output = new StringBuffer();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (Entry<String, Object> each : parameters.entrySet()) {
            String disposition;
            String type;
            String name = each.getKey();
            Object value = each.getValue();
            
            if ((value instanceof String) || 
                (value instanceof Integer) ||
                (value instanceof Double)) {
                type = "application/x-www-form-urlencoded;charset=UTF-8";
                disposition = "form-data";
            } else if (value instanceof NativeObject) {
                type = "application/x-www-form-urlencoded;charset=UTF-8";
                disposition = "form-data";
                value = value.toString();
            } else if (value == Context.getUndefinedValue() || value == null) {
                type = "application/x-www-form-urlencoded;charset=UTF-8";
                disposition = "form-data";
                value = "null";
            } else if (value instanceof byte[]) {
                type = "application/octet-stream";
                disposition = "form-data";
            } else {
                throw new InterpreterException("Unknown parameter name: " + name +  " type: " +
                    value.getClass().toString() + " value: " + each.getValue(), nodeLocation);
            }
            
            
//            output.append("--" + boundary + "\r\n");
//            output.append("Content-Type: " + type + "\r\n");
//            output.append("Content-Disposition: " + disposition + "; name =\"" + name + "\"\r\n\r\n");
//            output.append(value + "\r\n");
            try {
                baos.write(("--" + boundary + "\r\n").getBytes(UTF_8));
                if (value instanceof byte[]) {
                    baos.write(("Content-Disposition: " + disposition + "; name =\"" + name + "\"; filename =\"" + name + "\"\r\n").getBytes(UTF_8));
                } else {
                    baos.write(("Content-Disposition: " + disposition + "; name =\"" + name + "\"\r\n").getBytes(UTF_8));
                }
                baos.write(("Content-Type: " + type + "\r\n").getBytes(UTF_8));
                baos.write("\r\n".getBytes(UTF_8));
                if (value instanceof byte[]) {
                    baos.write((byte[])value);
                } else {
                    baos.write(value.toString().getBytes(UTF_8));
                }
                baos.write("\r\n".getBytes(UTF_8));
            } catch (IOException e) {
                // do nothing, since ByteArrayOutputStream never throws IOException on write
            }
        }
        
//        output.append("--" + boundary + "--\r\n");
        try {
            baos.write(("--" + boundary + "--\r\n").getBytes(UTF_8));
        } catch (IOException e) {
            // do nothing, since ByteArrayOutputStream never throws IOException on write
        }
        
        //return output.toString();
        return baos.toByteArray();
    }
    
    
}