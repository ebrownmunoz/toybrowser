/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <ul>
 * <li>Title: </li>
 * <li>Created: Apr 27, 2015 by: eric</li>
 * </ul>
 * 
 * This Document Server provides access to documents from the file
 * server. This is useful for testing, and perhaps more.
 * 
 * The host name and port will be stripped, and the file with the
 * path will be returned. For example an http request, 
 * such as "localhost:8080/Application/page.vxml will
 * return the file at [root]/Application/page.vxml."
 */
public class FileSystemDocumentServer implements DocumentServer{
    private static final String ROOT_VXML = "http://localhost:8080/";
    private Logger LOG = LoggerFactory.getLogger(FileSystemDocumentServer.class);

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = " (c) Copyright Voxware Inc. 2015.";
    
    
    public URI context ;
    /**
     * The directory where files are stored.
     */
    public final File directory;
    
    /**
     * Constructs a new <code>FileSystenDocumentServer</code> instance.
     * @param directory - the directory to find the resources.
     * @throws DocumentServerException 
     */
    public FileSystemDocumentServer(String directory) throws DocumentServerException {
        try {
            context = new URI(ROOT_VXML);
        } catch (URISyntaxException e) {
            throw new DocumentServerException();
        }
        this.directory = new File(directory);
        if (!this.directory.isDirectory()) {
            throw new DocumentServerException("No directory found at " + directory);
        }
    }
    
    public DocumentServerInputStream getDocument(URI uri) throws DocumentServerException {
        URL url;
        File file;
        DocumentServerInputStream instream;
        
        try {
            if (uri.isAbsolute()) {
                url = uri.toURL();
            } else {
                url = context.resolve(uri).toURL();
            }
            
            file = new File(directory, url.getPath());
            
            if (!file.exists()) {
                throw new DocumentServerException("A file for " + uri + " can not be found in " +
                    directory);
            }
            
            instream = new DocumentServerInputStream(new FileInputStream(file), file.length());
        } catch (MalformedURLException e) {
            throw new DocumentServerException("Can not open " + uri, e);
        } catch (FileNotFoundException e) {
            throw new DocumentServerException("File not found for " + uri, e);
        } 
        
        return instream;
    }
    
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.DocumentServer#postDocument(java.net.URI, java.util.Map)
     */
    @Override
    public DocumentServerInputStream postDocument (URI uri, Map<String, Object> arguments) throws DocumentServerException {
        if (arguments == null) {
            LOG.error("null argument list past to postDocument");
        } else {
            StringBuilder out = new StringBuilder("Post called with arguments :");
            for (String key : arguments.keySet()) {
                out.append(" " + key + "='" + arguments.get(key) + "'");
            }
            
            LOG.info(out.toString());
        }
        
        return getDocument(uri);
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.documentserver.DocumentServer#getDocument(java.net.URI, com.voxware.browser.documentserver.CacheControl)
     */
    public DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters,  CacheControl cacheControl) throws DocumentServerException {
        return getDocument(uri);
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.DocumentServer#getDocument(java.net.URI, com.voxware.browser.documentserver.CacheControl, int, int)
     */
    @Override
    public DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters,  CacheControl cacheControl, int connectTimeout, int readTimeout)
        throws DocumentServerException {
        // TODO Auto-generated method stub
        return null;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.DocumentServer#postDocument(java.net.URI, java.util.Map, java.lang.String, com.voxware.browser.documentserver.CacheControl, int, int)
     */
    @Override
    public DocumentServerInputStream postDocument(URI uri, Map<String, Object> parameters, CacheControl cacheControl,
        int connectTimeout, int readTimeout) throws DocumentServerException {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public void cacheDocument(URI url) throws DocumentServerException {
		// Do nothing (you don't cache file system documents)
		
	}
}
