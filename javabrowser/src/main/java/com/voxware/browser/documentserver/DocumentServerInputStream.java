/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.documentserver.http.DisconnectingInputStream;

/**
 * DocumentServerInputStream
 *
 * @author edh
 */
public class DocumentServerInputStream extends FilterInputStream {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2016.";
    private static final Logger log = LoggerFactory.getLogger(DocumentServerInputStream.class);
    private final Statistics stats; 
    private final long contentLength;
    private boolean firstRead = false;
    private boolean lastRead = false;
    private boolean closed = false;
    /**
     * Constructs a new <code>DocumentServerInputStream</code> instance.
     * @param in
     */
    public DocumentServerInputStream(InputStream in) {
        this(in, -1);
    }
    
    public DocumentServerInputStream(File file) throws FileNotFoundException {
        this(new FileInputStream(file), file.length());
    }
    
    public DocumentServerInputStream(InputStream in, long contentLength) {
        this(in, contentLength, new Statistics());
    }
    
    public DocumentServerInputStream(InputStream in, long contentLength, Statistics stats) {
        super(in);
        this.contentLength = contentLength;
        this.stats = stats;
    }
    
    public DocumentServerInputStream(URLConnection urlConnection, Statistics stats) throws IOException {
        super(null);
        in = getStream(urlConnection);
        contentLength = getContentLength(urlConnection);
        this.stats = stats;
    }
    
    private void checkFirstRead() {
        if (!firstRead) {
            firstRead = true;
            stats.setCheckPoint("FirstRead");
        }
    }
    
    private int checkLastRead(int ret) {
        if (!lastRead && ret == -1) {
            lastRead = true;
            stats.setCheckPoint("LastRead");
        }
        return ret;
    }
    /* 
     * (non-Javadoc)
     * @see java.io.FilterInputStream#read()
     */
    @Override
    public int read() throws IOException {
        checkFirstRead();
        return checkLastRead(super.read());
    }

    /* 
     * (non-Javadoc)
     * @see java.io.FilterInputStream#read(byte[])
     */
    @Override
    public int read(byte[] b) throws IOException {
        checkFirstRead();
        return checkLastRead(super.read(b));
    }

    /* 
     * (non-Javadoc)
     * @see java.io.FilterInputStream#read(byte[], int, int)
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        checkFirstRead();
        return checkLastRead(super.read(b, off, len));
    }

    /* 
     * (non-Javadoc)
     * @see java.io.FilterInputStream#close()
     */
    @Override
    public void close() throws IOException {
        super.close();
        if (!closed) {
            closed = true;
            stats.setCheckPoint("Closed");
            log.debug("Download statistic: {}", stats);
        }
    }

    /**
     * Returns the content length. A value of -1 means unspecified.
     * @return a content length or -1 if unspecified
     */
    public long getContentLength() {
        return contentLength;
    }

    /**
     * Extracts the InputStream from a URLConnection. Wraps the InputStream with DisconnectingInputStream if the URLConnection is an instance of HttpURLConnection.
     * @param connection the URL connection
     * @return the input stream
     * @throws IOException on error
     */
    protected InputStream getStream(URLConnection connection) throws IOException {
        if (connection instanceof HttpURLConnection) {
            return new DisconnectingInputStream((HttpURLConnection)connection);
        } else {
            return connection.getInputStream();
        }
    }
    
    /**
     * Gets the content length of the URLConnection. Tries to use getContentLengthLong() first, then falls back to getContentLength() if unavailable (like on Android targeting older platforms).
     * 
     * @param connection
     * @return
     * @throws IOException
     */
    protected long getContentLength(URLConnection connection) throws IOException {
        try {
            return connection.getContentLengthLong();
        } catch (NoSuchMethodError e) {
            return connection.getContentLength();
        }
    }
    
    public Statistics getStatistics() {
        return stats;
    }
}
