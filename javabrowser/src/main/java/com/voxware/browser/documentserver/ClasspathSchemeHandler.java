/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

/**
 * ClasspathSchemeHandler
 *
 * @author edh
 */
public class ClasspathSchemeHandler implements SchemeHandler<Void> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.SchemeHandler#handleScheme(java.net.URI, java.lang.Object)
     */
    @Override
    public DocumentServerInputStream handleScheme(URI uri, Void parameters) throws DocumentServerException {
        String path = uri.getPath();
        URL url = this.getClass().getResource(path);
        //InputStream is = this.getClass().getResourceAsStream(path);
        if (url != null) {
            URLConnection conn;
            try {
                conn = url.openConnection();
                return new DocumentServerInputStream(conn, new Statistics());
            } catch (IOException e) {
                throw new DocumentServerException("Error opening resource", e);
            }
            
        } else {
            throw new DocumentServerException("File not found: " + path);
        }
    }
    
}
