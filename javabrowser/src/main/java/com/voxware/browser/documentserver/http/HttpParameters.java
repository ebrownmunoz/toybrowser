/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.documentserver.http;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.NetworkParameters;

/**
 * HttpParameters Represents parameters to an http or https request
 *
 * @author edh
 */
public class HttpParameters extends NetworkParameters {
    public enum HttpMethod {
        GET, POST
    };

    private final CacheControl cacheControl;
    private final HttpMethod method;
    private final String payload;

    /**
     * Constructs a new <code>HttpNetworkSettings</code> instance.
     * 
     * @param connectTimeout the connect timeout
     * @param readTimeout the read timeout
     * @param cacheControl the cache control object
     * @param method the method
     */
    public HttpParameters(int connectTimeout, int readTimeout, CacheControl cacheControl, HttpMethod method) {
        super(connectTimeout, readTimeout);
        this.cacheControl = cacheControl;
        this.method = method;
        this.payload = null;
    }

    /**
     * Constructs a new <code>HttpNetworkSettings</code> instance. If <code>method</code> is GET and a payload is
     * defined, no error is thrown.
     * 
     * @param connectTimeout the connect timeout
     * @param readTimeout the read timeout
     * @param cacheControl the cache control object
     * @param method the method
     * @param payload the payload
     */
    public HttpParameters(int connectTimeout, int readTimeout, CacheControl cacheControl, HttpMethod method,
        String payload) {
        super(connectTimeout, readTimeout);
        this.cacheControl = cacheControl;
        this.method = method;
        this.payload = payload;
    }

    /**
     * Gets the {@link CacheControl} object
     * 
     * @return a {@link CacheControl} object
     */
    public CacheControl getCacheControl() {
        return cacheControl;
    }

    /**
     * Gets the {@link HttpMethod} object
     * 
     * @return a {@link HttpMethod}
     */
    public HttpMethod getMethod() {
        return method;
    }

    /**
     * Returns the payload.
     * @return the payload.
     */
    public String getPayload() {
        return payload;
    }
}