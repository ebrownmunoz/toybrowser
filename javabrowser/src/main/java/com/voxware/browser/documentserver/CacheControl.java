/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

public class CacheControl {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    public static final CacheControl NO_CACHE = new CacheControl(false);
    public static final CacheControl FOREVER_CACHE = new CacheControl(null, Long.MAX_VALUE);

    /**
     * Allows response to be cached
     */
    private final boolean cacheable;
    // use Object version because they can be "undefined"
    private final Long maxAge;
    private final Long maxStale;

    private CacheControl(boolean cacheable) {
        this.cacheable = cacheable;
        this.maxAge = null;
        this.maxStale = null;
    }

    /**
     * Creates a CacheControl object. This is generally passed to the ResourceServer as part of a request.
     * 
     * @param maxAge the max age in the cache
     * @param maxStale
     */
    public CacheControl(Long maxAge, Long maxStale) {
        this.cacheable = true;
        this.maxAge = maxAge;
        this.maxStale = maxStale;
    }

    /**
     * Returns true for cacheable, false otherwise
     * @return
     */
    public boolean isCacheable() {
        return cacheable;
    }

    public Long getMaxAge() {
        return maxAge;
    }

    public Long getMaxStale() {
        return maxStale;
    }

    /* 
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (cacheable ? 1231 : 1237);
        result = prime * result + ((maxAge == null) ? 0 : maxAge.hashCode());
        result = prime * result + ((maxStale == null) ? 0 : maxStale.hashCode());
        return result;
    }

    /* 
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CacheControl)) {
            return false;
        }
        CacheControl other = (CacheControl) obj;
        if (cacheable != other.cacheable) {
            return false;
        }
        if (maxAge == null) {
            if (other.maxAge != null) {
                return false;
            }
        } else if (!maxAge.equals(other.maxAge)) {
            return false;
        }
        if (maxStale == null) {
            if (other.maxStale != null) {
                return false;
            }
        } else if (!maxStale.equals(other.maxStale)) {
            return false;
        }
        return true;
    }

    /* 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (cacheable) {
            return "max-age=" + maxAge + ", max-stale=" + maxStale;
        } else {
            return "no-cache";
        }
    }
}
