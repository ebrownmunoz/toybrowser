/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.documentserver.http.HttpParameters;
import com.voxware.browser.documentserver.http.HttpParameters.HttpMethod;
import com.voxware.browser.documentserver.http.HttpSchemeHandler;

/**
 * AbstractDocumentServer
 *
 * @author edh
 */
public class BasicDocumentServer extends AbstractDocumentServer implements DocumentServer {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(BasicDocumentServer.class);
    public static final String HTTP = "http";
    public static final String HTTPS = "https";
    public static final String FILE = "file";
    public static final String CLASSPATH = "classpath";
    private static final String BOUNDARY = "1DB3FA5VAX";
    private final URI rootURI;

    static {
        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    }

    private Map<String, SchemeHandler<?>> schemeHandlers = new HashMap<String, SchemeHandler<?>>();

    /**
     * Constructs a new <code>BasicDocumentServer</code> instance.
     * 
     * @param rootURI
     * @throws URISyntaxException
     */
    public BasicDocumentServer(URI rootURI) throws URISyntaxException {
        if (rootURI.getPath().endsWith("/")) { // the root must end with "/" for paths to resolve correctly
            this.rootURI = rootURI;
        } else {
            this.rootURI = new URI(rootURI.getScheme(), rootURI.getUserInfo(), rootURI.getHost(), rootURI.getPort(),
                rootURI.getPath() + "/", null, null);
        }
        HttpSchemeHandler httpSchemeHandler = new HttpSchemeHandler();
        registerSchemeHandler(HTTP, httpSchemeHandler);
        registerSchemeHandler(HTTPS, httpSchemeHandler);
        registerSchemeHandler(FILE, new FileSchemeHandler());
        registerSchemeHandler(CLASSPATH, new ClasspathSchemeHandler());
    }

    /**
     * Registers
     * 
     * @param protocol
     * @param schemeHandler
     */
    protected void registerSchemeHandler(String protocol, SchemeHandler<?> schemeHandler) {
        schemeHandlers.put(protocol, schemeHandler);
    }

    /**
     * Finds
     * 
     * @param protocol
     * @return
     */
    protected SchemeHandler<?> findSchemeHandler(String protocol) {
        return schemeHandlers.get(protocol);
    }

    /**
     * Gets the document from the <code>uri</code>. Uses the {@link URLConnection} family of classes and APIs to perform
     * the actual work.
     * 
     * @param luri the uri
     * @param cacheControl cache control
     * @param connectTimeout the connect timeout
     * @param readTimeout the read timeout
     * @return an InputStream
     * @throws DocumentServerException on error
     */
    public DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters, CacheControl cacheControl,
        int connectTimeout, int readTimeout) throws DocumentServerException {
        URI luri;

        if (parameters != null) {
            try {
                luri = appendQueryParameters(resolve(uri), parameters);
            } catch (UnsupportedEncodingException e) {
                throw new DocumentServerException("Unable to resolve parameters", e);
            } catch (URISyntaxException e) {
                throw new DocumentServerException("Unable to resolve parameters", e);
            }
        } else {
            luri = resolve(uri);
        }

        final String protocol = luri.getScheme();

        log.info("BasicDocumentServer: getDocument(" + luri.toString() + ")");

        // dispatch on protocol
        // there must be a better way than this
        if (HTTP.equals(protocol) || HTTPS.equals(protocol)) {
            HttpSchemeHandler schemeHandler = (HttpSchemeHandler) schemeHandlers.get(protocol);
            return schemeHandler.handleScheme(luri,
                new HttpParameters(connectTimeout, readTimeout, cacheControl, HttpMethod.GET));
        } else if (schemeHandlers.containsKey(protocol)) {
            SchemeHandler<?> schemeHandler = schemeHandlers.get(protocol);
            return schemeHandler.handleScheme(luri, null);
        } else {
            throw new DocumentServerException("Unhandled scheme: " + protocol);
        }
    }
    
    /* (non-Javadoc)
     * @see com.voxware.browser.documentserver.DocumentServer#cacheDocument(java.net.URI)
     */
    @Override
    public void cacheDocument(URI uri) throws DocumentServerException {
    	final URI luri = resolve(uri);
    	final String protocol = luri.getScheme();
    	InputStream instream = null;
    	byte[] buffer = new byte[64 * 1024];
    	
    	if (HTTP.equals(protocol) || HTTPS.equals(protocol)) {
            HttpSchemeHandler schemeHandler = (HttpSchemeHandler) schemeHandlers.get(protocol);
            try {
            	instream = schemeHandler.handleScheme(luri,
            			new HttpParameters(1000, 1000, CacheControl.FOREVER_CACHE, HttpMethod.GET));
            	
            	while (instream.read(buffer) > -1) { // read all the bytes, and throw them away
            		// do nothing
            	}
            	
            	log.info("cacheDocument(): Cached " + luri);
            } catch (IOException e) {
            	log.error("Error reading caches");
            } finally {
                IOUtils.closeQuietly(instream);
            }
        }
    }

    /**
     * @param uri
     * @param parameters
     * @param payload
     * @param cacheControl
     * @param connectTimeout
     * @param readTimeout
     * @return
     * @throws DocumentServerException
     * @throws InterpreterException
     */
    public DocumentServerInputStream postDocument(URI uri, Map<String, Object> parameters, CacheControl cacheControl,
        int connectTimeout, int readTimeout) throws DocumentServerException {
        URI luri;
        String responseMessage = null;
        DocumentServerInputStream returnStream = null;

        luri = resolve(uri);

        log.info("BasicDocumentServer: postDocument(" + luri.toString() + ")");
        log.info(makeCurlLine(parameters, luri));

        final String protocol = luri.getScheme();
        // dispatch on protocol
        if (protocol.equals(HTTP) || protocol.equals(HTTPS)) {
            HttpURLConnection connection = null;
            OutputStream os = null;

            try {
                connection = (HttpURLConnection) luri.toURL().openConnection();
                connection.setConnectTimeout(connectTimeout);

                connection.setRequestMethod("POST");
                connection.setRequestProperty("Accept", "*/*");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
                connection.setDoOutput(true);

                byte[] payload;
                try {
                    payload = getMultipartFormFromParameters(parameters, BOUNDARY, null);                    
                } catch (InterpreterException e) {
                    throw new DocumentServerException("Error payload for document " + uri, e);
                }
                connection.setRequestProperty("Content-Length", "" + payload.length);
                os = connection.getOutputStream();
                os.write(payload);
                os.flush();
            } catch (MalformedURLException e) {
                throw new DocumentServerException("Unhandled scheme: " + protocol);
            } catch (IOException e) {
                log.info(getErrorResponse(connection));
                throw new DocumentServerException("Exception sending POST: ", e);
            } finally {
                IOUtils.closeQuietly(os);
            }

            try {
                String key;
                System.out.println("Headers-------start-----");
                Statistics stats = new Statistics();
                    
                for (int i = 1; (key = connection.getHeaderFieldKey(i)) != null; i++) {
                    System.out.println(key + ":" + connection.getHeaderField(i));
                }
                System.out.println("Headers-------end-----");
                returnStream = new DocumentServerInputStream(connection, stats);
            } catch (IOException e) {
                log.info(getErrorResponse(connection));
                throw new DocumentServerException("Exception sending POST: ", e);
            }
        } else {
            throw new DocumentServerException("Unhandled scheme: " + protocol);
        }

        
        return returnStream;

    }

    private String getErrorResponse(HttpURLConnection connection) {
        ByteArrayOutputStream baos;
        try {
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];

            InputStream error = connection.getErrorStream();
            if (error == null) {
                return null;
            }
            int iii;

            while ((iii = error.read(buffer)) > -1) {
                baos.write(buffer, 0, iii);
            }

            return baos.toString();
        } catch (IOException e) {
            log.error("Error", e);
        }

        return null;

    }

    private String makeCurlLine(Map<String, Object> parameters, URI uri) {
        StringBuffer buffer = new StringBuffer("curl ");
        for (String name : parameters.keySet()) {
            Object value = parameters.get(name);
            if (value == Context.getUndefinedValue()) {
                value = null;
            }
            buffer.append("-F " + name + "=\"" + value + "\" ");
        }

        buffer.append(uri.toString());
        return buffer.toString();
    }

    /**
     * Resolves the uri. Returns <code>uri</code> if it is already an absolute URI. The default behavior is to resolve
     * against the rootURI, but this can be overridden by subclasses.
     * 
     * @param uri the uri to resolve
     * @return a resolved URI, or <code>uri</code> if it is already absolute
     * @throws DocumentServerException if rootURI is null and the given URI is not absolute
     */
    protected URI resolve(URI uri) throws DocumentServerException {
        if (uri.isAbsolute()) {
            return uri;
        } else if (rootURI != null) {
            return rootURI.resolve(uri);
        } else {
            throw new DocumentServerException("uri is not absolute and no root URI is set");
        }
    }

    /**
     * Appends query parameters to <code>uri</code>
     * 
     * @param uri original uri
     * @param parameters the parameters to append
     * @return a new uri
     * @throws UnsupportedEncodingException if UTF-8 encoding is not supported
     * @throws URISyntaxException if the resulting uri is invalid
     */
    protected URI appendQueryParameters(URI uri, Map<String, Object> parameters)
        throws UnsupportedEncodingException, URISyntaxException {
        Map<String, String> queryArguments = new LinkedHashMap<String, String>();
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            queryArguments.put(entry.getKey(), entry.getValue().toString());
        }
        StringBuilder queryStringBuilder = new StringBuilder();
        String query = null;
        if (uri.getQuery() != null) {
            if (parameters.size() > 0) {
                queryStringBuilder.append(uri.getQuery());
                for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                    queryStringBuilder.append("&").append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
                }
                query = queryStringBuilder.toString();
            } else {
                query = uri.getQuery();
            }

        } else {
            if (parameters.size() > 0) {
                for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                    queryStringBuilder.append("&").append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
                }
                query = queryStringBuilder.substring(1, queryStringBuilder.length());
            }
        }

        StringBuffer newUri = new StringBuffer(uri.getScheme() + "://");
        newUri.append(uri.getHost());
        if (uri.getPort() > 0) {
            newUri.append(":" + uri.getPort());
        }
        newUri.append(uri.getPath());
        
        if (query != null) {
        	newUri.append("?" + query);
        }

        if (uri.getFragment() != null) {
            newUri.append("#" + uri.getFragment());
        }
        return new URI(newUri.toString());
    }
}
