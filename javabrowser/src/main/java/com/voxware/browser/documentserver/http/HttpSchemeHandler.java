/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver.http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.DocumentServerInputStream;
import com.voxware.browser.documentserver.SchemeHandler;
import com.voxware.browser.documentserver.Statistics;
import com.voxware.browser.documentserver.http.HttpParameters.HttpMethod;

/**
 * HttpSchemeHandler
 *
 * @author edh
 */
public class HttpSchemeHandler implements SchemeHandler<HttpParameters> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final Logger log = LoggerFactory.getLogger(HttpSchemeHandler.class);

    private static final Charset UTF_8 = Charset.forName("UTF-8");

    public static final String DEFAULT_CONTENT_TYPE = "application/x-www-form-urlencoded";
    
    /**
     * Constructs a new <code>HttpSchemeHandler</code> instance.
     */
    public HttpSchemeHandler() {
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.SchemeHandler#handleScheme(java.net.URI, java.lang.Object)
     */
    @Override
    public DocumentServerInputStream handleScheme(URI uri, HttpParameters parameters) throws DocumentServerException {
        try {
        	Statistics stats = new Statistics();
    		String uriString = uri.toString();
    		String clippedString;
    		
    		if (uriString.contains("#")) {
    			clippedString = uriString.substring(0, uriString.lastIndexOf('#'));
    		} else {
    			clippedString = uriString;
    		}
    		
        	URI luri = new URI(clippedString);

        	URL url = luri.toURL();
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setAllowUserInteraction(false);
            applyHttpSettings(connection, parameters);
            performHttpConnect(connection, parameters, stats);
            return new DocumentServerInputStream(connection, stats);
        } catch (MalformedURLException e) {
            throw new DocumentServerException("The URL is malformed", e);
        } catch (IOException e) {
            throw new DocumentServerException("Error during IO. Uri=" + uri, e);
        } 
        catch (URISyntaxException e) {
        	throw new DocumentServerException("Error during IO. Uri=" + uri, e);
		}
    }
    
    /**
     * Handles a connection
     * 
     * @param protocol
     * @param connection
     * @param parameters
     * @throws DocumentServerException
     * @throws IOException
     */
    protected void applyHttpSettings(HttpURLConnection httpConnection, HttpParameters parameters)
        throws DocumentServerException {
        setCacheControlDirectives(httpConnection, parameters.getCacheControl());
        log.debug("Cache-Control: {}", parameters.getCacheControl());
        setTimeouts(httpConnection, parameters.getConnectTimeout(), parameters.getReadTimeout());
        log.debug("Timeouts: connect: {}, read: {}", parameters.getConnectTimeout(), parameters.getReadTimeout());
    }

    /**
     * Handles a HTTP or HTTPS connection
     * 
     * @param httpConnection the http connection
     * @param parameters request parameters
     * @throws DocumentServerException if http server did not respond with a success
     * @throws IOException on other errors
     */
    protected void performHttpConnect(HttpURLConnection httpConnection, HttpParameters parameters, Statistics stats)
        throws DocumentServerException, IOException {
        final HttpMethod method = parameters.getMethod();
        try {
            httpConnection.setRequestMethod(parameters.getMethod().toString());
            if (parameters.getPayload() != null) {
                httpConnection.setDoOutput(true);
                attachPayload(httpConnection, parameters.getPayload());
            }
        } catch (ProtocolException e) {
            // swallow it because we're using constants that should work, but log the error
            log.error("Unexpected ProtocolException setting request method to " + method, e);
        }
        httpConnection.getInputStream();
        stats.setCheckPoint("BeforeConnect");
        int responseCode = httpConnection.getResponseCode();
        stats.setCheckPoint("AfterConnect");
        if (!isHttpSuccess(responseCode)) {
            throw new DocumentServerException("Server returned " + responseCode + " response with message" + httpConnection.getResponseMessage());
        }
    }
    
    /**
     * Attaches a String payload as UTF-8 encoded bytes
     * @param httpConnection the connection
     * @param payload the payload
     * @throws IOException on error
     */
    protected void attachPayload(HttpURLConnection httpConnection, String payload) throws IOException {
        byte[] bytes = payload.getBytes(UTF_8);
        httpConnection.setRequestProperty("Content-Type", DEFAULT_CONTENT_TYPE);
        httpConnection.setRequestProperty("Content-Length", Integer.toString(bytes.length));
        final OutputStream os = httpConnection.getOutputStream();
        try {
            os.write(bytes);
        } finally {
            try {
                os.flush();
            } finally {
                IOUtils.closeQuietly(os);
            }
        }
    }

    /**
     * Sets the Cache-Control
     * 
     * @param connection
     * @param cacheControl
     */
    protected void setCacheControlDirectives(HttpURLConnection connection, CacheControl cacheControl) {
        if (!cacheControl.isCacheable()) {
            connection.setUseCaches(false);
            connection.setRequestProperty("Cache-Control", "no-cache");
        } else {
            connection.setUseCaches(true);
            List<String> cacheControlElements = new ArrayList<String>(2);
            if (cacheControl.getMaxAge() != null) {
                cacheControlElements.add("max-age=" + cacheControl.getMaxAge());
            }
            if (cacheControl.getMaxStale() != null) {
                cacheControlElements.add("max-stale=" + cacheControl.getMaxStale());
            }
            connection.setRequestProperty("Cache-Control", join(",", cacheControlElements));
        }
    }

    /**
     * Sets the timeout values on the connection
     * 
     * @param connection the connection
     * @param connectTimeout the connect timeout
     * @param readTimeout the read timeout
     */
    protected void setTimeouts(HttpURLConnection connection, int connectTimeout, int readTimeout) {
        connection.setConnectTimeout(connectTimeout);
        connection.setReadTimeout(readTimeout);
    }

    /**
     * Returns true if the code is a successful HTTP response (2xx)
     * 
     * @param code the code
     * @return true if the code is 2xx, false otherwise
     */
    protected boolean isHttpSuccess(int code) {
        if (code >= 200 && code < 300) {
            return true;
        }
        return false;
    }
    /**
     * Joins the strings with the delimiter. Use String.join if you are on Java 1.8
     * 
     * @param delimiter the delimiter
     * @param elements the string like elements
     * @return a String
     */
    protected String join(CharSequence delimiter, Iterable<? extends CharSequence> elements) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (CharSequence element : elements) {
            if (first) {
                sb.append(element);
                first = false;
            } else {
                sb.append(delimiter).append(element);
            }
        }
        return sb.toString();
    }
}
