/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;

/**
 * FileSchemeHandler
 *
 * @author edh
 */
public class FileSchemeHandler implements SchemeHandler<Void> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.documentserver.SchemeHandler#handleScheme(java.net.URI, java.lang.Object)
     */
    @Override
    public DocumentServerInputStream handleScheme(URI uri, Void parameters) throws DocumentServerException {
        File file = new File(uri.getPath());
        try {
            return new DocumentServerInputStream(file);
        } catch (FileNotFoundException e) {
            throw new DocumentServerException("File not found: " + file.getAbsolutePath(), e);
        }
    }

}
