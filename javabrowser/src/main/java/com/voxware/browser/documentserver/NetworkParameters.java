/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

/**
 * NetworkSettings, used to package up network setting arguments to avoid a proliferation of method arguments.
 *
 * @author edh
 */
public class NetworkParameters {
    private final int connectTimeout;
    private final int readTimeout;

    /**
     * Constructs a new <code>NetworkSettings</code> instance.
     * 
     * @param connectTimeout the connection timeout
     * @param readTimeout the read timeout
     */
    public NetworkParameters(int connectTimeout, int readTimeout) {
        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
    }

    /**
     * Gets the connect timeout in millis. Cannot be negative.
     * 
     * @return an int >= 0
     */
    public int getConnectTimeout() {
        return connectTimeout;
    }

    /**
     * Gets the read timeout in millis. Cannot be negative
     * 
     * @return an int >= 0
     */
    public int getReadTimeout() {
        return readTimeout;
    }
}