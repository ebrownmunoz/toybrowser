/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.net.URI;
import java.util.Map;

/**
 * <ul>
 * <li>Title: Document Server </li>
 * <li>Description: Interface to fetch documents</li>
 * <li>Created: Apr 27, 2015 by: eric</li>
 * </ul>
 */
public interface DocumentServer {
    /**
     * Fetches a document at the given URI
     * @param uri the URI
     * @return content
     * @throws DocumentServerException on error
     */
    DocumentServerInputStream getDocument(URI uri) throws DocumentServerException;
    
    /**
     * Requests that a document be cached by the DocumentServer if possible.
     * @param url
     * @throws DocumentServerException
     */
    void cacheDocument(URI url) throws DocumentServerException;
    
    /**
     * Fetches a document at the given URI
     * @param uri the URI
     * @param cacheControl cache control parameters
     * @return content
     * @throws DocumentServerException on error
     */
    DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters,  CacheControl cacheControl) throws DocumentServerException;
    
    /**
     * Fetches a document at the given URI
     * @param uri the URI
     * @param cacheControl cache control parameters
     * @param connectTimeout the connect timeout (if applicable)
     * @param readTimeout the read timeout (if applicable)
     * @return content
     * @throws DocumentServerException on error
     */
    DocumentServerInputStream getDocument(URI uri, Map<String, Object> parameters,  CacheControl cacheControl, int connectTimeout, int readTimeout) throws DocumentServerException;

    /**
     * Supports post with given parameters, returns content
     * @param uri
     * @param parameters
     * @return
     * @throws DocumentServerException 
     */
    DocumentServerInputStream postDocument(URI uri, Map<String, Object> parameters) throws DocumentServerException;
    
    /**
     * Supports post with given parameters, returns content
     * @param uri
     * @param parameters
     * @param payload
     * @param cacheControl
     * @param connectTimeout
     * @param readTimeout
     * @return
     * @throws DocumentServerException
     */
    DocumentServerInputStream postDocument(URI uri, Map<String, Object> parameters, CacheControl cacheControl, int connectTimeout, int readTimeout) throws DocumentServerException;    
}
