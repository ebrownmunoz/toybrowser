/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver.http;

import java.io.FilterInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * DisconnectingInputStream disconnects the HttpURLConnection when the InputStream is closed.
 *
 * @author edh
 */
public class DisconnectingInputStream extends FilterInputStream {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2016.";

    private final HttpURLConnection connection;
    
    /**
     * Constructs a new <code>DisconnectingInputStream</code> instance.
     * @param in
     * @throws IOException 
     */
    public DisconnectingInputStream(HttpURLConnection connection) throws IOException {
        super(connection.getInputStream());
        this.connection = connection;
    }
    /** 
     * Closes the input stream and calls disconnect on the HttpURLConnection.
     * @see java.io.FilterInputStream#close()
     */
    @Override
    public void close() throws IOException {
        super.close();
        connection.disconnect();
    }

    
}
