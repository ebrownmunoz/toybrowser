/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Created: Apr 27, 2015 by: eric</li>
 * </ul>
 */
public class DocumentServerException extends Exception {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>DocumentServerException</code> instance.
     */
    public DocumentServerException() {
        super();
    }

    /**
     * Constructs a new <code>DocumentServerException</code> instance.
     * @param description
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public DocumentServerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * Constructs a new <code>DocumentServerException</code> instance.
     * @param description
     * @param cause
     */
    public DocumentServerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new <code>DocumentServerException</code> instance.
     * @param description
     */
    public DocumentServerException(String message) {
        super(message);
    }

    /**
     * Constructs a new <code>DocumentServerException</code> instance.
     * @param cause
     */
    public DocumentServerException(Throwable cause) {
        super(cause);
    }
}
