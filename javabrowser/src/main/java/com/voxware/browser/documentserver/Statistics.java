/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.documentserver;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Statistics
 *
 * @author edh
 */
public class Statistics {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2016.";

    private final Map<Long, String> checkPoints = new TreeMap<Long, String>();
    
    /**
     * Constructs a new <code>Statistics</code> instance.
     */
    public Statistics() {
        setCheckPoint("START");
    }
    
    public synchronized void setCheckPoint(String id) {
        long now = System.currentTimeMillis();
        checkPoints.put(now, id);
    }
    
    public Collection<String> getCheckPoints() {
        return checkPoints.values();
    }
    
    public Long getCheckPointTime(String id) {
        for (Entry<Long, String> entry : checkPoints.entrySet()) {
            if (entry.getValue().equals(id)) {
                return entry.getKey();
            }
        }
        return null;
    }

    /* 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Statistics [checkPoints=" + checkPoints + "]";
    }
}
