/**
 * 
 */
package com.voxware.browser.log;

/**
 * @author eric
 *
 */
public interface LogService {

    /**
     * Send a log description to this service
     * @param description
     */
    public void send(String message);
    
}
