/**
 * 
 */
package com.voxware.browser.log;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author eric
 *
 */
/**
 * @author eric
 *
 */
public class LogManager {

    private final LogService defaultLogService;;
    
    private Logger log = LoggerFactory.getLogger(LogManager.class);
    
    private final Map<String, LogService> logservices = new HashMap<String, LogService>();
    
    /**
     * Construct a new LogManager
     */
    public LogManager(LogService defaultLogService) {
        if (defaultLogService != null) {
            this.defaultLogService = defaultLogService;
        } else {
            this.defaultLogService = new LogService() {
                
                @Override
                public void send(String message) {
                    log.info("LOG " + message);
                }
            };
        }
    }
    
    /**
     * Register a Log Service for the given name
     * @param name
     * @param service
     */
    public void register(String name, LogService service) {
        logservices.put(name, service);
    }
    
    /**
     * Send a description to the service of the given name
     * @param name
     * @param description
     */
    public void sendLog(String name, String message ) {
        if (name == null) {
            defaultLogService.send(message);
        } else {
            LogService service = logservices.get(name);
            if (service == null) {
                throw new NullPointerException("Unknown Log service" + name);
            } else {
                service.send(message);
            }
        }
    }

}
