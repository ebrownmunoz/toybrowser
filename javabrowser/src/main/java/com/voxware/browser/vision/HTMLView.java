package com.voxware.browser.vision;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

public abstract class HTMLView {

    /**
     * Render the HTML in the appropriate view.
     * @param viewName
     * @param model
     */
    public void render(String viewName, Object model) {
        Template template = Mustache.compiler().compile(new InputStreamReader(getTemplateStreamForName(viewName)));
        OutputStream outStream = getOutputStream();
        Writer outWriter = new OutputStreamWriter(outStream);
        template.execute(model, outWriter);
        onDone();
    }
    
    abstract InputStream getTemplateStreamForName(String templateName);
    abstract OutputStream getOutputStream();
    abstract void onDone();

}
