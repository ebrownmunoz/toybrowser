package com.voxware.browser.util.nist;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NISTFile {
    
    private final Map<String, String> headers;
    private final byte[] pcmbytes;
    private final int pcmStart;
    
    private static final Logger log = LoggerFactory.getLogger(NISTFile.class);
    
    protected NISTFile(byte[] bytes, Map<String, String> headers, int pcmStart) {
        this.pcmbytes = bytes;
        this.headers = headers;
        this.pcmStart = pcmStart;
    }
    
    public static NISTFile parse(byte[] bytes) {
        NISTParser that = new NISTParser(bytes);
        int length;
        
        String nist = that.getLine();
        
        if (!"NIST_1A".equals(nist)) {
            log.warn("Invalid NIST data (no NIST header)");
            return null;
        }
        
        try {
            length = Integer.parseInt(that.getLine().trim());
        } catch (NumberFormatException e) {
            log.error("Invalid NIST data (no length)", e);
            return null;
        }
        
        Map<String, String> headers = that.getHeaders(length);
        
        byte[] pcmdata = new byte[bytes.length -length];
        System.arraycopy(bytes, length, pcmdata, 0, bytes.length - length);
        
        
        return new NISTFile(pcmdata, headers, length);
    }
    
    public String getHeader(String key) {
        return headers.get(key);
    }
    
    public int getPcmStart() {
        return pcmStart;
    }

    public byte[] getPcmData() {
        
        return pcmbytes;
    }

}
