package com.voxware.browser.util.nist;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NISTParser {

    private static final Logger log = LoggerFactory.getLogger(NISTParser.class);
 
    private final byte[] bytes;
    private int pos = 0;
    
    NISTParser(byte[] bytes) {
        this.bytes = bytes;
    }
    
    public String getLine() {
        StringBuilder builder = new StringBuilder();
        
        while ((pos < bytes.length) && (bytes[pos] != 10)) {
            builder.append((char)bytes[pos]);
            pos++;
        }
        
        pos++; // eat end of line
        
        return builder.toString();
    }
    
    public Map<String, String> getHeaders(int length) {
        String key = null;
        String value;
        Map<String, String> retval = new HashMap<String,String>();
        
        while((!"end_head".equals(key)) && (pos <= length)) {
            String line = getLine();
            String[] parts = line.split(" ", 3); // ignoring type parameter for now
            
            key = parts[0];
            
            if (parts.length > 1) {
                value = parts[2];
            } else {
                value = null;
            }
            
            if (!"end_head".equals(key)) {
                retval.put(key, value);
            }
        } // while
        
        return retval;
    }

}
