/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.audioplayer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.io.Player;

/**
 * Abstract is a base class to implement one or more Players.
 *
 * @author edh
 * @param <T> the type of thing being played
 */
public abstract class AbstractPlayer<T> implements Player<T> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2016.";

    private static final Logger log = LoggerFactory.getLogger(AbstractPlayer.class);
    private List<PlayerTask> tasks = new ArrayList<PlayerTask>(1);
    private ExecutorService executor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), createThreadFactory());
    /**
     * Constructs a ThreadFactory for the executor service. This should return a ThreadFactory that at the very least appropriately names the Thread used by the ExecutorService.
     * @return a ThreadFactory
     */
    protected abstract ThreadFactory createThreadFactory();

    /**
     * Exposes the ExecutorService to allow subclasses to directly submit jobs.
     * @return
     */
    protected ExecutorService getExecutorService() {
        return executor;
    }
    /*
     * (non-Javadoc)
     * @see com.voxware.browser.io.Player#play(java.lang.Object)
     */
    @Override
    public synchronized Future<Void> play(T output) throws AudioPlayerException {
        final PlayerTask task = createPlayerTask(output);
        try {
            executor.submit(task);
        } catch (RejectedExecutionException e) {
            throw new AudioPlayerException(e);
        }
        return task;
    }
    
    /**
     * Cancels all tasks pending and executing.
     */
    @Override
    public void cancel() {
        List<PlayerTask> allTasks = new ArrayList<PlayerTask>(tasks);
        for (PlayerTask task : allTasks) {
            task.cancel(true);
        }
        tasks.clear();
    }
    
    /**
     * Shuts down the player, override to release resources, but please call super.shutdown() if you do.
     */
    @Override
    public void shutdown() {
        executor.shutdown();        
        cancel();
        try {
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.warn("Interrupted while waiting for shutdown");
        }
    }
    
    /**
     * Sets a parameter
     * @param name the parameter name
     * @param value the parameter value
     */
    @Override
    public void setParameter(String name, Object value) {
        
    }
    
    /**
     * Gets a parameter
     * @param name the parameter name
     * @return a value which may be null or null if no such parameter
     */
    @Override
    public Object getParameter(String name) {
        return null;
    }
    
    /**
     * Returns the Callable that handles playing <code>playable</code>. The Callable must not return until the playable has completed playing.
     * @param playable the thing to play
     * @return a Callable
     */
    protected abstract Callable<Void> createCallable(T playable);
    
    /**
     * Creates the player task.
     * @param playable the playable
     * @return a PlayerTask
     */
    protected PlayerTask createPlayerTask(T playable) {
        return new PlayerTask(createCallable(playable));
    }
    
    protected class PlayerTask extends FutureTask<Void> {
        /**
         * Constructs a new <code>PlayerTask</code> instance.
         * @param callable
         */
        public PlayerTask(Callable<Void> callable) {
            super(callable);
        }

        @Override
        protected void done() {
            synchronized (AbstractPlayer.this) {
                tasks.remove(this);
            }
        }
    }
}
