/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.audioplayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PromptResolver
 *
 * @author edh
 */
public class PromptResolver {
    /**
     * FragmentProviderFactory a SPI for creating/retrieving FragmentProviders
     *
     * @author edh
     */
    public interface FragmentProviderFactory {
        /**
         * Returns a FragmentProvider for the given source. The source may represent a single fragment or a collection
         * of fragments.
         * 
         * @param source the source, likely to be a URI
         * @return a FragmentProvider
         */
        FragmentProvider getFragmentProvider(String source);
    }

    public interface FragmentProvider {
        /**
         * Returns the names of all the fragments provided
         * 
         * @return the fragment names
         */
        List<String> getAllFragments();

        /**
         * Returns an object representing the fragment. This object may be byte[] or a URI.
         * 
         * @param fragment the fragment name
         * @return
         */
        Object getFragment(String fragment);
    }

    private FragmentProviderFactory fragmentProviderFactory;
    private Map<String, FragmentProviderWrapper> fragmentProviders = new HashMap<String, FragmentProviderWrapper>();

    public void setFragmentProviderFactory(FragmentProviderFactory factory) {
        fragmentProviderFactory = factory;
    }

    public List<Object> resolvePrompt(String source, String text) {
        if (fragmentProviderFactory == null) {
            if (!fragmentProviders.containsKey(source)) {
                List<Object> ret = new ArrayList<Object>();
                ret.add(text);
                return ret;
            } else {
                return fragmentProviders.get(source).resolvePrompt(text);
            }
        } else {
            if (!fragmentProviders.containsKey(source)) {
                FragmentProvider fragmentProvider = fragmentProviderFactory.getFragmentProvider(source);
                if (fragmentProvider != null) {
                    fragmentProviders.put(source, new FragmentProviderWrapper(fragmentProvider));
                } else {
                    List<Object> ret = new ArrayList<Object>();
                    ret.add(text);
                    return ret;
                }
            }
            return fragmentProviders.get(source).resolvePrompt(text);
        }
    }

    private class FragmentProviderWrapper {
        private final Logger log = LoggerFactory.getLogger(FragmentProviderWrapper.class);
        private final FragmentProvider fragmentProvider;
        private final Map<String, List<Object>> resolvedCache = new HashMap<String, List<Object>>();

        public FragmentProviderWrapper(final FragmentProvider fragmentProvider) {
            this.fragmentProvider = fragmentProvider;
        }

        public List<Object> resolvePrompt(String text) {
            text = text.trim();
            if (resolvedCache.containsKey(text)) {
                log.debug("Returning prior resolution for \"{}\"", text);
                return resolvedCache.get(text);
            } else {
                log.debug("Resolving \"{}\"", text);
                List<Object> resolution = findBestResolution(text);
                resolvedCache.put(text, resolution);
                return resolution;
            }
        }

        protected List<Object> findBestResolution(final String text) {
            List<String> fragments = fragmentProvider.getAllFragments();
            String fragment = getIgnoreCase(fragments, text);
            if (null != fragment) {
                log.debug("Found direct match for: " + text);
                List<Object> ret = new ArrayList<Object>(1);
                ret.add(fragmentProvider.getFragment(fragment));
                return ret;
            } else {
                // now we need to find some combination of fragments that will match text as best as possible (maybe even constructing new fragments for TTS)
                // first remove all strings in the fragments longer than the given text,
                for (Iterator<String> iter = fragments.iterator(); iter.hasNext();) {
                    String s = iter.next();
                    if (s.length() > text.length()) {
                        iter.remove();
                        continue;
                    }
                }
                // sort by length, then alphabetical?
                Collections.sort(fragments, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        int cmp = Integer.compare(o2.length(), o1.length());
                        if (cmp == 0) {
                            return o1.compareTo(o2);
                        } else {
                            return cmp;
                        }
                    }
                });
                String tokenizedText = text.replaceAll("[,.:;]", " ").replaceAll("\\s+", " ").trim();
                List<Object> ret = findBestResolution(tokenizedText, fragments);
                if (ret == null) {
                    ret = new ArrayList<Object>(1);
                    ret.add(text);
                }
                return ret;
            }
        }

        protected List<Object> findBestResolution(String text, List<String> fragments) {
            String match = getIgnoreCase(fragments, text);
            if (null != match) {
                log.debug("Found full match for: " + text);
                List<Object> ret = new ArrayList<Object>(1);
                ret.add(fragmentProvider.getFragment(match));
                return ret;
            } else {
                for (String fragment : fragments) {
                    if (startsWithIgnoreCase(text, fragment)) {
                        log.debug("Found partial match for: " + text);
                        List<Object> rest = findBestResolution(text.substring(fragment.length()).trim(), fragments);
                        if (rest != null) {
                            rest.add(0, fragmentProvider.getFragment(fragment));
                            return rest;
                        }
                    }
                }
                return null;
            }
        }

        /**
         * Implementation of {@link Collection#contains(Object)}, but specialized for Strings to case insensitive search
         * @param fragments the Collection of Strings
         * @param text the text to find
         * @return true 
         */
        protected boolean containsIgnoreCase(Collection<String> fragments, String text) {
            for (String fragment : fragments) {
                if (text.equalsIgnoreCase(fragment)) {
                    return true;
                }
            }
            return false;
        }
        
        protected String getIgnoreCase(Collection<String> fragments, String text) {
            for (String fragment : fragments) {
                if (text.equalsIgnoreCase(fragment)) {
                    return fragment;
                }
            }
            return null;
        }
        
        /**
         * Similar to {@link String#startsWith(String)}, this performs the operation in a case insensitive way.
         * @param source the source string
         * @param fragment the piece to check with
         * @return true if <code>source</code> starts with <code>fragment</code>
         */
        protected boolean startsWithIgnoreCase(String source, String fragment) {
            return source.toLowerCase().startsWith(fragment.toLowerCase());
        }
    }
}
