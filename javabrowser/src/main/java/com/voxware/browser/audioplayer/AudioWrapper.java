package com.voxware.browser.audioplayer;

public class AudioWrapper {
	private final String text;
	private final byte[] audiobytes;
	private final AudioPlayer.Type type;
	
	public AudioWrapper(AudioPlayer.Type type, String text, byte[] audiobytes) {
		this.text = text;
		this.audiobytes = audiobytes;
		this.type = type;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the audiobytes
	 */
	public byte[] getAudiobytes() {
		return audiobytes;
	}
	
	public AudioPlayer.Type getType() {
		return type;
	}
}
