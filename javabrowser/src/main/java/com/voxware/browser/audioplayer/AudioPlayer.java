package com.voxware.browser.audioplayer;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.io.Player;

/**
 * AudioPlayer
 *
 * @author edh
 */
public class AudioPlayer {
    private static final Logger log = LoggerFactory.getLogger(AudioPlayer.class);

    public enum Type {
        STRING, BYTES, URI
    }

    private final List<FutureTask<Void>> allTasks = new ArrayList<FutureTask<Void>>(1);
    private final ExecutorService executor = new ThreadPoolExecutor(1, 1, 10, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(1));

    private final Player<String> tts;
    private final Player<byte[]> bytePlayer;
    private final Player<URI> uriPlayer;

    public AudioPlayer(Player<String> tts, Player<byte[]> bytePlayer, Player<URI> uriPlayer) {
        this.tts = tts;
        this.bytePlayer = bytePlayer;
        this.uriPlayer = uriPlayer;
    }

    protected List<AudioWrapper> coalesce(List<AudioWrapper> audios) {
        // we know the new list will never exceed the old list in size
        List<AudioWrapper> coalesced = new ArrayList<AudioWrapper>(audios.size());
        AudioWrapper lastWrapper = null;
        for (AudioWrapper wrapper : audios) {
            if (wrapper.getType() == Type.BYTES) {
                if (lastWrapper == null || lastWrapper.getType() != Type.BYTES) {
                    coalesced.add(wrapper);
                    lastWrapper = wrapper;
                } else {
                    byte[] concatenated = new byte[lastWrapper.getAudiobytes().length + wrapper.getAudiobytes().length];
                    System.arraycopy(lastWrapper.getAudiobytes(), 0, concatenated, 0, lastWrapper.getAudiobytes().length);
                    System.arraycopy(wrapper.getAudiobytes(), 0, concatenated, lastWrapper.getAudiobytes().length, wrapper.getAudiobytes().length);
                    lastWrapper = new AudioWrapper(Type.BYTES, null, concatenated);
                    coalesced.set(coalesced.size() - 1, lastWrapper);
                }
            } else if (wrapper.getType() == Type.STRING) {
                if (lastWrapper == null || lastWrapper.getType() != Type.STRING) {
                    coalesced.add(wrapper);
                    lastWrapper = wrapper;
                } else {
                    lastWrapper = new AudioWrapper(Type.STRING, lastWrapper.getText() + " " + wrapper.getText(), null);
                    coalesced.set(coalesced.size() - 1, lastWrapper);
                }                
            } else if (wrapper.getType() == Type.URI) {
                lastWrapper = null;
                coalesced.add(wrapper);
            } else {
                // unknown type, so don't coalesce them
                lastWrapper = null;
                coalesced.add(wrapper);
            }
        }
        return coalesced;
    }

    public Future<Void> play(final List<AudioWrapper> audios) throws AudioPlayerException {
        synchronized (allTasks) {
            final List<AudioWrapper> compacted = coalesce(audios);
            final AudioCallable callable = new AudioCallable(compacted);
            final AudioTask task = new AudioTask(callable);
            try {
                // we don't return the executor's Future
                allTasks.add(task);
                executor.submit(task);
                return task;
            } catch (RejectedExecutionException e) {
                throw new AudioPlayerException("AudioPlayer is busy", e);
            }
        }
    }

    public boolean cancel() {
        synchronized (allTasks) {
            boolean cancel = true;
            List<FutureTask<Void>> copy = new ArrayList<FutureTask<Void>>(allTasks);
            // prevents work from being
            for (FutureTask<Void> t : copy) {
                cancel = cancel && t.cancel(true);
            }
            allTasks.clear();
            if (tts != null) {
                tts.cancel();
            }
            if (bytePlayer != null) {
                bytePlayer.cancel();
            }
            return cancel;
        }
    }
    
    public void setParameter(String name, Object value) {
        if (tts != null) {
            tts.setParameter(name, value);
        }
        if (bytePlayer != null) {
            bytePlayer.setParameter(name, value);
        }
        if (uriPlayer != null) {
            uriPlayer.setParameter(name, value);
        }
    }
    
    public Object getParameter(String name) {
        return null;
    }
    
    public void shutdown() {
        cancel();
        executor.shutdown();
        tts.shutdown();
        bytePlayer.shutdown();
    }

    protected class AudioCallable implements Callable<Void> {
        private final List<AudioWrapper> audios;

        public AudioCallable(final List<AudioWrapper> audios) {
            this.audios = Collections.unmodifiableList(audios);
        }

        /**
         * Plays the audio.
         * 
         * @throws CancellationException if cancelled
         */
        @Override
        public Void call() throws ExecutionException, AudioPlayerException {
            for (AudioWrapper audio : audios) {
                Future<Void> future = null;
                if (audio.getType() == Type.BYTES) {
                    future = bytePlayer.play(audio.getAudiobytes());
                } else if (audio.getType() == Type.STRING) {
                    future = tts.play(audio.getText());
                } else if (audio.getType() == Type.URI) {
                    future = uriPlayer.play(URI.create(audio.getText()));
                } else {
                    log.error("Unknown Audio Type " + audio.getType() + " (ignoring).");
                    continue;
                }
                try {
                    future.get();
                } catch (InterruptedException e) {
                    log.debug("Audio playback cancelled by request");
                    // we were interrupted, which probably means we were cancelled
                    future.cancel(true);
                    throw new CancellationException();
                }
            }
            return null;
        }
    } // AudioCallable

    protected class AudioTask extends FutureTask<Void> {

        public AudioTask(AudioCallable audioCallable) {
            super(audioCallable);
        }

        /*
         * (non-Javadoc)
         * @see java.util.concurrent.FutureTask#done()
         */
        @Override
        protected void done() {
            super.done();
            synchronized (allTasks) {
                allTasks.remove(this);   
            }
        }
    }
}
