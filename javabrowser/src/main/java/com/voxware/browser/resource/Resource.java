/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Oct 2, 2015 by: edh</li>
 * </ul>
 */
public abstract class Resource extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";
    
    private final CacheControl cacheControl;
    private final String src;
    private final String expr;
    
    private static Logger LOG = LoggerFactory.getLogger(Resource.class);
    
    public Resource(Map<String, String> attributes, NodeLocation nodeLocation, CacheControl cacheControl) {
        super(attributes, nodeLocation);
        this.cacheControl = cacheControl;
        src = getAttribute(SRC);
        expr = getAttribute(EXPR);
    }
    
    public CacheControl getCacheControl() {
        return cacheControl;
    }
    
    public String getSrc() {
        return src;
    }
    
    public String getExpr() {
        return expr;
    }
    
    public boolean isDynamic() {
        return src == null;
    }

    /**
     * Retrieves the resource.
     * @param sessionContext
     * @param currentContext
     * @throws InterpreterException 
     * @throws Exception
     */
    public byte[] fetch(SessionContext sessionContext, BaseContext currentContext) throws InterpreterException  {
        DocumentServer server = sessionContext.getDocumentServer();
        InputStream instream = null;
        
        try {
            instream = server.getDocument(new URI(getUrl(currentContext)), null, getCacheControl());
            return IOUtils.toByteArray(instream);
        } catch (DocumentServerException e) {
            LOG.error(e.getMessage(), e);
            throw new InterpreterException ("Initializing Speaker Tag ", e, getNodeLocation());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw new InterpreterException ("Initializing Speaker Tag ", e, getNodeLocation());
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage(), e);
            throw new InterpreterException ("Initializing Speaker Tag ", e, getNodeLocation());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * Get the url either from src, or from expr (evaluated in context).
     * @param context
     * @return
     * @throws InterpreterException
     */
    public String getUrl(BaseContext context) {
        if (src != null) {
            return src;
        } else {
             return context.evaluateToString(expr);
        } 
    }
}
