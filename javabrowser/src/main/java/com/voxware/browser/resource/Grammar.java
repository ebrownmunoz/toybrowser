/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.resource;

import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents a Grammar resource
 * Created: 2015-10-7
 * @author edh
 */
public class Grammar extends Resource {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "Â© Copyright Voxware Inc. 2015.";

    private final String content;
    /**
     * @param cacheControl
     * @param expr
     */
    public Grammar(Map<String, String> attributes, NodeLocation nodeLocation, CacheControl cacheControl, String content) {
        super(attributes, nodeLocation, cacheControl);
        this.content = content;
    }
    /**
     * Gets inline content if any exists
     * @return inline content or null if none exists
     */
    public String getContent() {
        return content;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (!context.getValidationUtils().exactlyOneNonNull(getSrc(), getExpr(), getContent())) {
            throw new ValidationException("only one of 'src', 'expr' or inline content can be defined",getNodeLocation());
        }        
    }
}
