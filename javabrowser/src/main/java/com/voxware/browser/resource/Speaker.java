/**
 * 
 */
package com.voxware.browser.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * @author eric
 *
 */
public class Speaker extends Resource {
    
    private static Logger LOG = LoggerFactory.getLogger(Speaker.class);
    
    private String src = null;
    private String expr = null;
    private String type = null;
    
    /**
     * @param attributes
     * @param nodeLocation
     */
    public Speaker(Map<String, String> attributes, NodeLocation nodeLocation, 
        CacheControl cacheControl) {
        super(attributes, nodeLocation, cacheControl);
        this.src = attributes.get("src");
        this.expr = attributes.get("expr");
        this.type = attributes.get("type");
    }
    
    /**
     *  Return the type.
     */
    public String getType() {
        return this.type;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (!context.getValidationUtils().exactlyOneNonNull(getSrc(), getExpr())) {
            throw new ValidationException("only one of 'src' or 'expr'  can be defined",getNodeLocation());
        }
    }
}
