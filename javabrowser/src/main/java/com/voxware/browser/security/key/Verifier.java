package com.voxware.browser.security.key;

import java.math.BigInteger;

public class Verifier {

  public static boolean verify(String uidString, String keyString) throws NumberFormatException {
    long uid = 0L;
    try {
      if (uidString.length() > Encryption.uidLength * 2) throw new NumberFormatException();
      uid = Long.parseLong(uidString, 16);
    } catch (NumberFormatException e) {
      throw new NumberFormatException(uidString + " is not a well-formed uid");
    }
    long key = 0L;
    try {
      if (keyString.length() > Encryption.keyLength * 2) throw new NumberFormatException();
      key = new BigInteger(keyString, 16).longValue();
    } catch (NumberFormatException e) {
      throw new NumberFormatException(keyString + " is not a well-formed key");
    }
    return verify(uid, key);
  }

  public static boolean verify(long uid, long key) {
    return (uid == Encryption.uid(key));
  }
}
