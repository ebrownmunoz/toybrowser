package com.voxware.browser.security.key;

import java.util.Random;
import java.security.NoSuchAlgorithmException;

public class ErgodicSequence {

  private int length;

  private int modulus;
  private int random;
  private int multiplier;

  /*
  private double modulus;
  private double random;
  private double multiplier;

  ErgodicSequence(int length, long seed) {
    this.length = length;
    double exponent = StrictMath.ceil(StrictMath.log(length) / StrictMath.log(2.0) + 2);
    modulus = StrictMath.pow(2, exponent);
    Random generator = new Random(seed);
    random = StrictMath.floor(generator.nextDouble() * (modulus - 1));
    if (random % 2 == 0) random++;
    multiplier = 8 * (StrictMath.floor(generator.nextDouble() * (StrictMath.pow(2, exponent - 3) - 2)) + 1) - 3;
  }
  */

  ErgodicSequence(int length, long seed) {
    this.length = length;
    for (modulus = 4; modulus < length; modulus *= 2);
    modulus *= 4;
    Random generator = new Random(seed);
    random = generator.nextInt(modulus);
    if (random % 2 == 0) random = (random + 1) % modulus;
    multiplier = 8 * (generator.nextInt(modulus / 8) + (modulus / 16)) - 3;
  }

  int next() {
    random = (multiplier * random) % modulus;
    int index = random / 4;
    return (index < length) ? index : next();
  }

  int[] sequence() {
    int[] sequence = new int[length];
    for (int index = 0; index < length; index++) sequence[index] = next();
    return sequence;
  }
}
