package com.voxware.browser.security.key;

import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;

class Encryption {

  static final int uidLength = 6; // Bytes
  static final int keyLength = 8; // Bytes
  private static final String prng = "SHA1PRNG";
  private static SecureRandom random;
  private static boolean test = false;

    static long longFromBytes(byte[] idBytes) {
    long uid = 0L;
    for (int index = 0; index < idBytes.length; index++)
      uid = (uid << 8) | (((long) idBytes[index]) & ((1L << 8) - 1));
    return uid;
  }

  static long seed() throws NoSuchAlgorithmException {
    if (random == null) random = SecureRandom.getInstance(prng);
    return longFromBytes(random.generateSeed(2));
  }

  static long key(long uid) throws NoSuchAlgorithmException {
    long key = 0L;
    long mask = uid;
    long seed;
    do {
      seed = seed();
      mask = seed << 32 | seed << 16 | seed;
    } while (mask == uid);
    if (test) System.out.print("; seedIn = " + Long.toHexString(seed) + "; maskIn = " + Long.toHexString(mask));
    uid = uid ^ mask;
    int[] sequence = new ErgodicSequence(uidLength * 8, seed).sequence();
    for (int keyBit = 0; keyBit < keyLength * 8; keyBit++) {
      if (keyBit % 4 == 0) {
         // Every fourth bit comes from the seed
        if ((seed & 1L << keyBit / 4) != 0L) key = key | (1L << keyBit);
      } else if ((uid & (1L << sequence[keyBit - keyBit / 4 - 1])) != 0L) {
        key = key | (1L << keyBit);
      }
    }
      return key;
  }

  static long uid(long key) {
     // Reconstitute the seed
    long seed = 0L;
    for (int keyBit = 0; keyBit < keyLength * 8; keyBit += 4)
      if ((key & (1L << keyBit)) != 0) seed = seed | (1L << keyBit / 4);
    long mask = seed << 32 | seed << 16 | seed;
    if (test) System.out.print("; seedOut = " + Long.toHexString(seed) + "; maskOut = " + Long.toHexString(mask));
     // Reconstitute the uid
    long uid = 0L;
    int[] sequence = new ErgodicSequence(uidLength * 8, seed).sequence();
    for (int keyBit = 0; keyBit < keyLength * 8; keyBit++) {
      if (keyBit % 4 != 0 && (key & (1L << keyBit)) != 0L)
        uid = uid | (1L << sequence[keyBit - keyBit / 4 - 1]);
    }
    return (uid ^ mask) & ((1L << (uidLength * 8)) - 1);
  }
}
