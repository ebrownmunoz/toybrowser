/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.event.Catch;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.resource.Speaker;

/**
 * Document
 *
 * @author ebrown
 */
public class Document extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final static Logger log = LoggerFactory.getLogger(Document.class);

    private final Form defaultForm;
    private final Map<String, Form> forms;
    private final List<Initializer> initializers;
    private final List<Catch> catches;
    private final List<InterruptLinkNode> links;
    private final Speaker speaker;
    
    /**
     * Constructs a new <code>Document</code> instance.
     * 
     * @param attributes the attributes
     * @param nodeLocation the node location
     * @param initializers the variables
     * @param forms the forms (forms and menus)
     */
    public Document(Map<String, String> attributes, NodeLocation nodeLocation, List<Initializer> initializers, List<Catch> catches, List<Form> forms, 
        List<InterruptLinkNode> links, Speaker speaker) {
        super(attributes, nodeLocation);
        this.initializers = makeFinal(initializers);
        this.catches = makeFinal(catches);
        if (forms != null) {
            if (forms.size() > 0) {
                defaultForm = forms.get(0);
            } else {
                defaultForm = null;
            }
            Map<String, Form> formMap = new LinkedHashMap<String, Form>();
            for (Form form : forms) {
                formMap.put(form.getId(), form);
            }
            this.forms = Collections.unmodifiableMap(formMap);
        } else {
            this.forms = Collections.emptyMap();
            defaultForm = null;
        }
        
        this.speaker = speaker;
        this.links = links;
    }

    /**
     * Returns a Form (Form or Menu) with the given id. If id is null, requests the default Form or Menu.
     * 
     * @param id the id or null for the default Form
     * @return a Form or null if one is not found
     */
    public Form getForm(String id) {
        if (id == null) {
            return defaultForm;
        } else {
            return forms.get(id);
        }
    }

    /**
     * return the list of initializers
     * 
     * @return
     */
    public List<Initializer> getInitializers() {
        return initializers;
    }

    /**
     * Returns the uri.
     * 
     * @return the uri.
     */
    public URI getUri() {
        return getNodeLocation().getUri();
    }

    /**
     * get the parent URI to this document
     * 
     * @return
     * @throws URISyntaxException
     */
    private URI getParentURI() throws URISyntaxException {
        URI uri = getNodeLocation().getUri();
        String thisPath = uri.getPath();

        // hack of the last path part
        int parentIndex = thisPath.lastIndexOf("/");
        if (parentIndex >= 0) {
            thisPath = thisPath.substring(0, parentIndex + 1);
        }

        return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), thisPath, null, null);
    }

    /**
     * Creates a new URI object relative to the parent of this document.
     * 
     * @param uri
     * @return
     * @throws URISyntaxException
     */
    public URI resolveRelativeURI(String uri) throws URISyntaxException {
        return getParentURI().resolve(uri);
    }

    /**
     * Creates a new URI object relative to the parent of this document.
     * 
     * @param uri
     * @return
     * @throws URISyntaxException
     */
    public URI resolveRelativeURI(URI uri) throws URISyntaxException {
        return getParentURI().resolve(uri);
    }

    /**
     * Provide a detailed dump of the document
     * 
     * @return
     */
    public String dump() {
        StringBuilder builder = new StringBuilder("dump: Document: " + getNodeLocation().getUri());
        builder.append("dump: INITIALIZERS:");
        for (Initializer each : initializers) {
            builder.append(each.toString());

        }
        builder.append("dump: FORMS:");
        for (String each : forms.keySet()) {
            Form form = forms.get(each);
            builder.append(form.toString() + ((form == defaultForm) ? "*" : ""));
        }
        return builder.toString();
    }
    
    

    /**
     * @return the speaker
     */
    public Speaker getSpeaker() {
        return speaker;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        context.validateChildren(this, forms.values());
        context.validateChildren(this, catches);
        context.validateChildren(this, links);
        context.validateChild(this, speaker);
    }

	/**
	 * @return the links
	 */
	public List<InterruptLinkNode> getLinks() {
		return links;
	}

    public List<Catch> getCatches() {
        return catches;
    }
}
