package com.voxware.browser.model;

import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.interrupt.Interrupt;
import com.voxware.browser.io.RecognitionResult;

public class InterruptLinkNode extends InterruptNode {

	private final String linkEvent;
	private final String linkMode;
	
	public InterruptLinkNode(Map<String, String> attributes, NodeLocation nodeLocation, String event, String mode) {
		super(attributes, nodeLocation, Type.EVENT);
		this.linkEvent = event;
		this.linkMode = mode != null ? mode : "soft";
	}

	@Override
	public void validate(ValidationContext context) throws ValidationException {
		// TODO Auto-generated method stub
	}

    public RecognitionResult processInterrupt(BaseContext context, Interrupt interrupt) {
        return RecognitionResult.event(Result.event(linkEvent, interrupt.getMessage()));
    }

    /**
     * @return the linkEvent
     */
    public String getLinkEvent() {
        return linkEvent;
    }

    /**
     * @return the linkMode
     */
    public String getLinkMode() {
        return linkMode;
    }
	
	
    public String toString() {
        return "InterruptLinkNode(linkEvent = " + linkEvent + ", linkMode = " + linkMode + ")";
    }
}
