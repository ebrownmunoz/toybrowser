/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.text;

import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.AbstractVxmlNode;

/**
 * A Simple immutable String to implement the Text Class
 */
public class TextString extends AbstractVxmlNode implements Text {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final String text;

    /**
     * Constructs a new <code>TextString</code> instance. Does not take an attribute map as text never contains
     * attributes.
     * 
     * @param nodeLocation the node location
     * @param text the text
     */
    public TextString(NodeLocation nodeLocation, String text) {
        super(null, nodeLocation);
        this.text = text;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.text.Text#getString()
     */
    public String getString(SessionContext context, Scriptable scope) {
        return text;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return text;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (text == null) {
            throw new ValidationException("text cannot be null", getNodeLocation());
        }
    }
}
