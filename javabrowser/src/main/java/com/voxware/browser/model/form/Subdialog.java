/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;

import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.Param;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

public class Subdialog extends InputItem {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";

    /**
     * Default method is get
     */
    public static final String DEFAULT_METHOD = "get";
    /**
     * Default encoding type (enctype) is application/x-www-form-urlencoded
     */
    public static final String DEFAULT_ENCODING_TYPE = "application/x-www-form-urlencoded";
    /**
     * Allowed method types
     */
    private static final List<String> ALLOWED_METHODS = Arrays.asList(DEFAULT_METHOD, "post");
    /**
     * Allowed encoding types
     */
    private static final List<String> ALLOWED_ENCTYPES = Arrays.asList(DEFAULT_ENCODING_TYPE, "multipart/form-data");

    private final String src;
    private final String srcExpr;
    private final List<String> nameList;
    private final String method;
    private final String encType;
    private final CacheControl cacheControl;
    private final List<Param> params;

    public Subdialog(Map<String, String> attributes, NodeLocation nodeLocation, List<Filled> filledHandlers,
        CacheControl cacheControl, List<Param> params, List<Catch> catches, List<Property> properties) {
        // we null the prompts because we don't support prompts in subdialogs
        // revisit if needed
        super(attributes, nodeLocation, null, catches, filledHandlers, properties);
        this.src = getAttribute(SRC);
        this.srcExpr = getAttribute(SRCEXPR);
        nameList = getNameListAttribute(NAMELIST);
        if (getAttribute(METHOD) != null) {
        	this.method = getAttribute(METHOD);
        } else {
        	this.method = DEFAULT_METHOD;
        }
        this.encType = getAttribute(ENCTYPE);
        this.cacheControl = cacheControl;
        this.params = makeFinal(params);
    }

    /**
     * Gets the source from src iff present or srcexpr. Will throw an exception if both are specified or if neither are
     * specified.
     * 
     * @param attributes
     * @return
     * @throws InterpreterException
     */
    public String getSrcFromAttributes(Context rhino, FormContext fcontext) throws InterpreterException {
        if (src != null) {
            return src;
        } else {
            final BrowserScope formScope = fcontext.getScope();
            final String location = getNodeLocation().getUri().toString();
            final int lineNumber = getNodeLocation().getLineNumber();
            return (String) rhino.evaluateString(formScope, srcExpr, location, lineNumber, null);
        }
    }

    /**
     * Return the subdialog params
     * 
     * @return
     */
    public List<Param> getParams() {
        return params;
    }

    public List<String> getNameList() {
        return nameList;
    }

    public String getMethod() {
        return method;
    }

    public String getEncType() {
        return encType;
    }

    public CacheControl getCacheControl() {
        return cacheControl;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        super.validate(context);
        if (!context.getValidationUtils().exactlyOneNonNull(src, srcExpr)) {
            throw new ValidationException("one and only one of 'src' or 'srcexpr' must be defined", getNodeLocation());
        }
        if (method != null && !ALLOWED_METHODS.contains(method)) {
            throw new ValidationException("method must be one of " + ALLOWED_METHODS, getNodeLocation());
        }

        if (encType != null && !ALLOWED_ENCTYPES.contains(encType)) {
            throw new ValidationException("enctype must be one of " + ALLOWED_METHODS, getNodeLocation());
        }

        for (Param param : params) {
            context.validateChild(this, param);
        }
    }
}
