/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Base class for all form items
 *
 * @author edh
 */
public abstract class FormItem extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(FormItem.class);
    
    private final String name;
    private final String expr;
    private final String cond;

    protected final List<Catch> catches;
    protected final List<Property> properties;

    /**
     * Constructs a new <code>FormItem</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     */
    public FormItem(Map<String, String> attributes, NodeLocation nodeLocation, List<Catch> catches, List<Property> properties) {
        super(attributes, nodeLocation);
        this.name = attributes.get(NAME);
        this.expr = attributes.get(EXPR);
        this.cond = attributes.get(COND);
        this.catches = makeFinal(catches);
        this.properties = makeFinal(properties);
    }

    /**
     * Returns true if the form item variable is stored in the scope.
     * 
     * @return
     */
    public boolean isScoped() {
        return ((name != null) && (name.length() > 0));
    }

    /**
     * Gets the name of the form item. Returns null if not defined.
     * 
     * @return a name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the expr attribute of the form item. If this is defined and does not resolve to undefined, the form item's
     * initial state is filled (does not trigger {@literal <filled>}).
     * 
     * @return an expression, or null
     */
    public final String getExpr() {
        return expr;
    }

    /**
     * Gets the cond for the visitation algorithm. Returns null if not defined.
     * 
     * @return
     */
    public String getCond() {
        return cond;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (name != null) {
            if (context.isEcmaScriptReservedWord(name)) {
                log.warn("variable name '" + name + "' is a reserved word", getNodeLocation());
            }
            if (context.isVxmlReservedSyntax(name)) {
                throw new ValidationException("variable names starting with '_' or ending with '$' are reserved",
                    getNodeLocation());
            }
        }
    }

    public List<Catch> getCatches() {
        return catches;
    }

    public List<Property> getProperties() {
        return properties;
    }
}
