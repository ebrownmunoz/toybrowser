/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.mozilla.javascript.ConsString;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.executable.Goto;

/**
 * Represents a Submit element.
 */
public class Submit extends Goto implements Executable {

	/**
	 * Legal copyright notice.
	 */
	public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

	/**
	 * Default method is get
	 */
	public static final String DEFAULT_METHOD = "get";
	/**
	 * Default encoding type (enctype) is application/x-www-form-urlencoded
	 */
	public static final String DEFAULT_ENCODING_TYPE = "application/x-www-form-urlencoded";
	/**
	 * Allowed method types
	 */
	private static final List<String> ALLOWED_METHODS = Collections.unmodifiableList(Arrays.asList(DEFAULT_METHOD, "post"));
	/**
	 * Allowed encoding types
	 */
	private static final List<String> ALLOWED_ENCTYPES = Collections.unmodifiableList(Arrays.asList(DEFAULT_ENCODING_TYPE, "multipart/form-data"));

	private static Logger log = LoggerFactory.getLogger(Submit.class);

	private final List<String> nameList;
	private final String method;
	private final String encType;

	/**
	 * Constructs a new <code>Submit</code> instance.
	 * @param attributes
	 * @param nodeLocation
	 */
	public Submit(Map<String, String> attributes, NodeLocation nodeLocation) {
		super(attributes, nodeLocation);
		nameList = getNameListAttribute(NAMELIST);
		// OPT: intern these to save a few bytes
		method = getAttribute(METHOD, DEFAULT_METHOD);
		encType = getAttribute(ENCTYPE, DEFAULT_ENCODING_TYPE);
	}

	/*
	 * (non-Javadoc)
	 * @see com.voxware.browser.model.executable.Goto#execute(com.voxware.browser.context.SessionContext,
	 * com.voxware.browser.context.AnonymousContext)
	 */
	public Result execute(SessionContext scontext, BaseContext context) {
		String nextURL = null;
		try {
			nextURL = evaluateTargetURI(scontext, context.getScope());
		} catch (InterpreterException e) {
			log.error("Exception in getNext", e);
			return Result.event("error.semantic.goto", "Exception in getNext");
		}

		Result result = new Result(Result.Type.SUBMIT, nextURL, ("post".equalsIgnoreCase(method)));
		if (nameList != null) {
			try {
				result.setParameters(expandNameList(nameList, context.getScope(), getNodeLocation()));
			} catch (InterpreterException e) {
				log.error("Exception in execute", e);
				return Result.event("error.semantic", e.getMessage());
			}
		}

		return result;
		
	}

	/**
	 * Returns the parsed namelist attribute. Returns null if it was not specified, but returns a single empty string
	 * element if it was empty.
	 * 
	 * @return
	 */
	public List<String> getNameList() {
		return nameList;
	}

	public String getMethod() {
		if (method == null) {
			return DEFAULT_METHOD;
		}
		return method;
	}

	public String getEncType() {
		if (encType == null) {
			return DEFAULT_ENCODING_TYPE;
		}
		return encType;
	}

	@Override
	public void validate(ValidationContext context) throws ValidationException {
		super.validate(context);
		if (nameList != null)  {
			// verify the namelist?
		}
		if (method == null && !ALLOWED_METHODS.contains(method)) {
			throw new ValidationException("method must be one of " + ALLOWED_METHODS, getNodeLocation());
		}
		if (encType == null && !ALLOWED_ENCTYPES.contains(encType)) {
			throw new ValidationException("enctype must be one of " + ALLOWED_METHODS, getNodeLocation());
		}
	}

	/**
	 * Given a namelist and a scope, create a map of <String, Object> that contains the each
	 * name in the namelist and the expanded value
	 * @throws InterpreterException 
	 */
	public static Map<String, Object> expandNameList(List<String> namelist, BrowserScope scope, NodeLocation nodeLocation) throws InterpreterException {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();

		List<String> parameterNames;

		if (namelist == null) {
			parameterNames = new ArrayList<String>();
			for (Object astring : scope.getAllIds()) {
				parameterNames.add((String)astring);
			}
		} else {
			parameterNames = namelist;
		}

		for (String name : parameterNames) {
			Object value = scope.getRecursive(name);
			
			// Change Integer Doubles into true Integers.
			if (value instanceof Double) {
				double d = ((Double)value).doubleValue();
				if ((d % 1) == 0) {
					value = Math.round(d);
				}
				
				// if it is really a double, just keep it.
			} else if (value instanceof String) {
				// keep it as is
			} else if (value instanceof ConsString) {
			    value = value.toString(); // Convert to Sting
			} else if (value == Scriptable.NOT_FOUND) {
				throw new InterpreterException("Can not get property " + name, nodeLocation);
			} else if (value instanceof NativeJavaObject) {
			    value = ((NativeJavaObject)value).unwrap();
			} else if (value instanceof Undefined) {
			    value = ""; // empty String will be put in args list.			    
			} else if (value instanceof Scriptable) {
				boolean first = true;
				StringBuffer sbuff = new StringBuffer("{");
				for (Entry<Object, Object> each : ((NativeObject)value).entrySet()) {
					if (first) {
						first = false;
					} else {
						sbuff.append(",");
					}
					
					Object valueobject = each.getValue();
					
					if (valueobject instanceof String){
						sbuff.append(each.getKey() + ":\"" + valueobject + "\"");
					} else {
						sbuff.append(each.getKey() + ":" + valueobject);
					}
				}
				
				sbuff.append("}");
				
				value = sbuff.toString();
			} else {
			    log.warn("Unknown type for value name: " + name + " type: "  + value.getClass().toString() + 
			        " with value: " + value + " (converting to String)");
			    value = value.toString();
			}
			parameters.put(name, value);
		}


		return parameters;
	} // expandNameList
}
