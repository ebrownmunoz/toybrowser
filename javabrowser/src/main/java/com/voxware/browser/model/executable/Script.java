/*
 * Copyright  2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.RhinoException;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents a Script tag
 */
public class Script extends AbstractVxmlNode implements Executable, Initializer {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final Logger log = LoggerFactory.getLogger(Script.class);

    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final boolean isInline;
    private final String src;
    private String content;
    private final Charset charSet;

    /**
     * Constructs a new non-inlined <code>Script</code> instance.
     * 
     * @param attributes node attributes
     * @param nodeLocation node location
     */
    public Script(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        isInline = false;
        src = getAttribute(SRC);
        content = null;
        String charSet = attributes.get("charset");
        this.charSet = (charSet != null) ? Charset.forName(charSet) : UTF_8;
    }

    /**
     * Constructs a Script instance that has inline content.
     * 
     * @param attributes node attributes
     * @param nodeLocation node location
     * @param content inline content
     */
    public Script(Map<String, String> attributes, NodeLocation nodeLocation, String content) {
        super(attributes, nodeLocation);
        isInline = true;
        src = getAttribute(SRC); // validation needs to check to make sure this is null
        this.content = content;
        String charSet = attributes.get("charset");
        this.charSet = (charSet != null) ? Charset.forName(charSet) : UTF_8;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.Initializer#initialize(com.voxware.browser.context.SessionContext,
     * com.voxware.browser.context.BaseContext)
     */
    public Result initialize(SessionContext scontext, BaseContext context) {
        Result result = runScript(scontext, context.getScope());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext,
     * com.voxware.browser.context.AnonymousContext)
     */
    public Result execute(SessionContext scontext, BaseContext context) {
        Result result = runScript(scontext, context.getScope());
        return result;
    }

    /**
     * Run the script (after fetching it if necessary)
     * 
     * @param scontext
     * @param context
     * @return
     */
    private synchronized Result runScript(SessionContext scontext, Scriptable scope) {
        if (content == null) {
            if (!isInline) {
                try {
                    fetchContent(scontext);
                } catch (DocumentServerException e) {
                    log.error("Fetch error", e);
                    return Result.event("error.badfetch", "Error Fetching inline Script");
                }
            } else {
                log.error("No content available for script at " + getNodeLocation());
                return Result.event("error.semantic", "No content available for script at " + getNodeLocation());
            }
        }

        Context rhino = scontext.getContext();
        try {
            rhino.evaluateString(scope, this.content, "<script>", 0, null);
        } catch (RhinoException e) {
            log.error("Error running script at " + getNodeLocation(), e);
            log.info("Script: " + this.content);
            return Result.event("error.semantic", "No content available for script at " + getNodeLocation());
        } catch (Exception e) {
            log.error("Error running script at " + getNodeLocation(), e);
            log.info("Script: " + this.content);
            return Result.event("error.semantic", "No content available for script at " + getNodeLocation());
        }

        return Result.Ok();
    }

    /**
     * Fetch the script content from the server.
     * 
     * @param scontext
     * @throws DocumentServerException
     */
    private synchronized void fetchContent(SessionContext scontext) throws DocumentServerException {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream instream = null;

        try {
            URI uri = URI.create(src);

            instream = scontext.getDocumentServer().getDocument(uri);
            int len;
            while ((len = instream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            content = new String(baos.toByteArray(), charSet);
        } catch (DocumentServerException e) {
            throw e;
        } catch (IOException e) {
            throw new DocumentServerException("fetch error", e);
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    log.warn("Error closing inputstream", e);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (src != null && isInline) {
            throw new ValidationException("attribute 'src' cannot be specified with an inline script", getNodeLocation());
        }
        if (src == null && !isInline) {
            throw new ValidationException("attribute 'src' must be specified if no inline script is declared", getNodeLocation());
        }
        if (src != null) {
            try {
                URI uri = new URI(src);
            } catch (URISyntaxException e) {
                throw new ValidationException("attribute 'src' is not a valid URI", getNodeLocation());
            }
        }
    }
}
