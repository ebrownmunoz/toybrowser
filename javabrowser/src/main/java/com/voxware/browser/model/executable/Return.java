/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.List;
import java.util.Map;

import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.event.Event;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Aug 3, 2015 by: eric</li>
 * </ul>
 */
public class Return extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(Return.class);

    private final List<String> nameList;
    private final String event;
    private final String eventExpr;
    private final String message;
    private final String messageExpr;
    
    /**
     * Constructs a new <code>Return</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public Return(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        nameList = getNameListAttribute(NAMELIST);
        event = getAttribute(EVENT);
        eventExpr = getAttribute(EVENTEXPR);
        message = getAttribute(MESSAGE);
        messageExpr = getAttribute(MESSAGEEXPR);
    }
    
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext, org.mozilla.javascript.Scriptable)
     */
    public Result execute(SessionContext scontext, BaseContext context) {
        Result result = new Result(Result.Type.RETURN, null, false);
        BrowserScope scope = context.getScope();
        Scriptable value = scontext.getContext().newObject(scope);
        
        if (nameList != null) {
            for (String name : nameList) {
                Scriptable containing = scope.findAncestorWithVariable(name);
                if (containing == null) {
                    return Result.event("error.semantic.return", 
                        "<return> contains name " + name + "which isn't declared in this scope.");
                } else {
                    value.put(name, value, containing.get(name, containing));
                }
            } // for
            result.setValue(value);
        } else {
            // throw event
            String eventName = event != null ? event : context.evaluateToString(eventExpr);
            String messageText = message != null ? message : (messageExpr == null ? null : context.evaluateToString(messageExpr));
            result.setEvent(new Event(eventName, messageText));
        }
        return result;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (!context.getValidationUtils().exactlyOneNonNull(event, eventExpr, nameList)) {
            throw new ValidationException("exactly one of 'event', 'eventexpr', 'namelist' must be defined", getNodeLocation());
        }
        if (message != null && messageExpr != null) {
            throw new ValidationException("only one of 'description' or 'messageexpr' may be defined", getNodeLocation());
        }
        if (nameList != null && message != null) {
            log.warn("namelist and a description is specified, but the description will never be visible");
        }
        if (nameList != null && messageExpr != null) {
            log.warn("namelist and a messageexpr is specified, but the description will never be visible");
        }
    }
}

