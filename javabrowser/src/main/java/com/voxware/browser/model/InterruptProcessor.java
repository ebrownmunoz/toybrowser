/**
 * 
 */
package com.voxware.browser.model;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.interrupt.Interrupt;

/**
 * @author eric
 *
 */

public interface InterruptProcessor {
    
    /**
     * Process an interrupt
     * @param interrupt
     * @return
     */
    Result processInterrupt(BaseContext context, Interrupt interrupt);
}
