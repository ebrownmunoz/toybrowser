/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.text;

import java.util.List;

import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.SessionContext;

/**
 * Represents a series of TextString and TextValue objects.
 * 
 * @author edh
 */
public class CompositeText implements Text {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private List<Text> textNodes;

    /**
     * Constructs a new <code>CompositePlayable</code> instance.
     * 
     * @param textNodes
     */
    public CompositeText(List<Text> textNodes) {
        if (textNodes == null) {
            throw new NullPointerException("textNodes cannot be null");
        }
        this.textNodes = textNodes;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.text.Text#getString(com.voxware.browser.context.SessionContext,
     * org.mozilla.javascript.Scriptable)
     */
    public String getString(SessionContext context, Scriptable scope) {
        StringBuilder sb = new StringBuilder();
        for (Text text : textNodes) {
            sb.append(text.getString(context, scope));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Text text : textNodes) {
            sb.append(text.toString());
        }
        return sb.toString();
    }
}
