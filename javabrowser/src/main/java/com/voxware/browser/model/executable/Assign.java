/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.Arrays;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Aug 7, 2015 by: EBrown-Munoz</li>
 * </ul>
 */
public class Assign extends Var {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * 
     * Constructs a new <code>Assign</code> instance.
     * @param attributes
     * @param nodeLocation 
     */
    public Assign(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
    }
    
    @Override
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.Initializer#initialize(com.voxware.browser.context.SessionContext, com.voxware.browser.context.scope.BrowserScope)
     */
    public Result execute(SessionContext scontext, BaseContext context) {
        Scriptable scope = null;
        String name;
        String[] parts = getName().split("\\.");
        name = parts[parts.length - 1];
        
        scope = context.getScope().findAncestorWithVariable(parts[0]);
        if (scope == null) {
            return Result.event("error.semantic", "Undeclared variable: " + name + " " + getNodeLocation());
        }

        if (parts.length > 1) {
        	String[] path = Arrays.copyOf(parts, parts.length-1);
        	
        	try {
				scope = BrowserScope.getRecursiveScope(scope, path, getNodeLocation());
			} catch (InterpreterException e) {
				return Result.event("error.semantic", "Unknown scope " + getName() + " " + getNodeLocation());
			}
            if (scope == null) {
                return Result.event("error.semantic", "Unknown scope " + getName() + " " + getNodeLocation());
            }
        }
        setValue(scontext.getContext(), scope, context.getScope(), name, getExpr());
        return Result.Ok();
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        String name = getName();
        String expr = getExpr();
        if (name == null || name.isEmpty()) {
            throw new ValidationException("'name' attribute must be defined'", getNodeLocation());
        }
        if (expr == null || expr.isEmpty()) {
            throw new ValidationException("'expr' attribute must be defined'", getNodeLocation());
        }
    }
}
