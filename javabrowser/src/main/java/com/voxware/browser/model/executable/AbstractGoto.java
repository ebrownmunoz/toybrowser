/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.AbstractVxmlNode;

/**
 * Represents a Goto tag
 */
public abstract class AbstractGoto extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(AbstractGoto.class);
    private final String next;
    private final String expr;

    /**
     * Constructs a new <code>Goto</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public AbstractGoto(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        next = attributes.get(NEXT);
        expr = attributes.get(EXPR);
    }

    /**
     * Gets the next attribute
     * 
     * @return a URI string, or null if not defined
     */

    public String getNext() {
        return next;
    }

    /**
     * Gets the expr attribute
     * 
     * @return an expression, or null if not defined
     */
    public String getExpr() {
        return expr;
    }
    
    /**
     * Evaluates the target URI. If <code>getNext()</code> is not null, it is the result of <code>getNext()</code>,
     * otherwise it is the result of evaluating the expression returned by <code>getExpr()</code>.
     * 
     * @param scontext
     * @param scope
     * @return
     * @throws InterpreterException
     */
    protected String evaluateTargetURI(SessionContext scontext, Scriptable scope) throws InterpreterException {
        if (next != null) {
            return next;
        } else {
            final Context context = scontext.getContext();
            final String source = getNodeLocation().getUri().toString();
            final int lineNumber = getNodeLocation().getLineNumber();
            String exprString = (String) context.evaluateString(scope, expr, source, lineNumber, null);
            return exprString;
        }
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser. model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (!context.getValidationUtils().exactlyOneNonNull(next, expr)) {
            throw new ValidationException("one and only one of 'next' or 'expr' must be defined", getNodeLocation());
        }
        try {
            if (next != null) {
                URI uri = new URI(next);
            }
        } catch (URISyntaxException e) {
            throw new ValidationException("'next' is not a valid URI", e, getNodeLocation());
        }
    }
}
