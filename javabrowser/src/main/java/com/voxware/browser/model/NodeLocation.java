/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.net.URI;

/**
 * Represents a node location. Line number and column number of 0 means they are unknown.
 * 
 * @author edh
 */
public class NodeLocation implements Comparable<NodeLocation> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final URI uri;
    private final int lineNumber;
    private final int columnNumber;
    private final String locationDescription;

    /**
     * Constructs a new <code>NodeLocation</code> instance.
     * 
     * @param uri the URI
     */
    public NodeLocation(URI uri) {
        this(uri, 0, 0, null);
    }

    /**
     * Constructs a new <code>NodeLocation</code> instance.
     * 
     * @param uri the uri
     * @param lineNumber the line number
     * @param columnNumber the column number
     * @param locationDescription a better description of the location
     */
    public NodeLocation(URI uri, int lineNumber, int columnNumber, String locationDescription) {
        super();
        this.uri = uri;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
        if (locationDescription != null) {
            this.locationDescription = locationDescription;
        } else {
            this.locationDescription = lineNumber + ":" + columnNumber + " in " + (uri != null ? uri : "unknown");
        }
    }

    /**
     * Gets the URI of the location
     * 
     * @return the uri
     */
    public URI getUri() {
        return uri;
    }

    /**
     * Gets the line number of the location
     * 
     * @return the line number
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * Gets the column number of the location
     * 
     * @return the column number
     */
    public int getColumnNumber() {
        return columnNumber;
    }

    /**
     * Gets the location description
     * 
     * @return the location description
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    public String toString() {
        return locationDescription;
    }

    /* 
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(NodeLocation o) {
        if (!getUri().equals(o.getUri())) {
            return getUri().compareTo(o.getUri());
        } else {
            if (getLineNumber() != o.getLineNumber()) {
                return Integer.compare(getLineNumber(), o.getLineNumber());
            } else {
                return Integer.compare(getColumnNumber(), o.getColumnNumber());
            }
        }
    }
}
