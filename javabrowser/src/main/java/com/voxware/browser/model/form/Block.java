/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.Map;

import com.voxware.browser.model.Executable;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.VxmlNode;

/**
 * Block
 *
 * @author edh
 */
public class Block extends ControlItem {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final ExecutableList executables;

    /**
     * Constructs a new <code>Block</code> instance.
     * @param attributes
     * @param nodeLocation
     * @param executables
     */
    public Block(Map<String, String> attributes, NodeLocation nodeLocation, ExecutableList executables) {
        // blocks do not have catches
        super(attributes, nodeLocation, null);
        this.executables = (executables != null) ? executables : ExecutableList.EMPTY_LIST;
    }

    /**
     * Gets the immutable list of Executables
     * 
     * @return
     */
    public ExecutableList getExecutableList() {
        return executables;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        super.validate(context);
        for (Executable executable : executables) {
            context.validateChild(this, (VxmlNode) executable);
        }
    }
}
