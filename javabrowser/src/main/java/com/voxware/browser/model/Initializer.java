/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;


/**
 * Used to Initialize either a Document or a Form. This will be
 * used for <var> or <script> tags that can be used to initialize
 * the scope. This means that we can create a new Context using the
 * information based in the model
 */
public interface Initializer {

    /**
     * Initialize the scope using the Rhino Context in scontext.
     * 
     * @param scontext
     * @param context 
     * @return 
     */
    public Result initialize(SessionContext scontext, BaseContext context);
}
