/*
 * Copyright © 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable, shall
 * retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks, trade
 * secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.FormItemVariable;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: Aug 7, 2015 by: EBrown-Munoz</li>
 * </ul>
 */
public class Clear extends AbstractVxmlNode implements Executable {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(Clear.class);
    private final List<String> nameList;

    /**
     * Constructs a new <code>Assign</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     */
    public Clear(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        nameList = getNameListAttribute(NAMELIST);
    }

    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
        if (nameList == null) { // clear all FIVs
            FormContext formContext = null;
            if (context instanceof AnonymousContext) {
                AnonymousContext acontext = (AnonymousContext) context;
                formContext = acontext.getFormContext();
            } else if (context instanceof FormContext) {
                formContext = (FormContext)context;
            } else {
                throw new IllegalArgumentException("Unknown context type: " + context.getClass());
            }
            
            for (FormItemVariable each : formContext.getFormItemVariablesList()) {
                if (log.isDebugEnabled()) {
                    log.debug("Clearing " + each.getName());
                }
                each.clear();
            }
        } else {
            BrowserScope currentScope = context.getScope();
            for (String name : nameList) {
                Scriptable declarationScope = currentScope.findAncestorWithVariable(name);
                if (name == null) {
                    return Result.event("error.semantic", "Invalid variable name " + name);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Clearing " + name);
                    }
                    declarationScope.put(name, declarationScope, Context.getUndefinedValue());
                }
            }
        }
        return Result.Ok();
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        /*
         * Not much to do except maybe check the names Not every variable may be known at this point (e.g. var
         * declarations in scripts)
         */
    }
}
