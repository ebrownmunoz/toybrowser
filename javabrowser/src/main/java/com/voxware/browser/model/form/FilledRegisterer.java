/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.Initializer;

/**
 * FilledRegistered
 *
 * @author edh
 */
public class FilledRegisterer implements Initializer {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final Filled filled;
    private final InputItem inputItem;
    /**
     * Constructs a new <code>FilledRegisterer</code> instance. Used for form level filleds.
     * @param filled the filled
     */
    public FilledRegisterer(Filled filled) {
        this.filled = filled;
        inputItem = null;
    }
    /**
     * Constructs a new <code>FilledRegisterer</code> instance. Used for input item level filleds.
     * @param inputItem the input item that contains some {@literal <filled>} elements
     */
    public FilledRegisterer(InputItem inputItem) {
        this.filled = null;
        this.inputItem = inputItem;
    }
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.Initializer#initialize(com.voxware.browser.context.SessionContext, com.voxware.browser.context.BaseContext, com.voxware.browser.interpreter.InterpreterState)
     */
    @Override
    public Result initialize(SessionContext scontext, BaseContext context) {
        if (inputItem == null) {
            context.getFilledList().addFilled(filled);
        } else {
            context.getFilledList().addFilled(inputItem);
        }
        return null;
    }

}
