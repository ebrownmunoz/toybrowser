/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import com.voxware.browser.BrowserException;

/**
 * ValidationException
 *
 * Created: Oct 15, 2015
 * @author edh
 */
public class ValidationException extends BrowserException {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ValidationException</code> instance.
     * @param nodeLocation
     */
    public ValidationException(NodeLocation nodeLocation) {
        super(nodeLocation);
    }

    /**
     * Constructs a new <code>ValidationException</code> instance.
     * @param description
     * @param nodeLocation
     */
    public ValidationException(String message, NodeLocation nodeLocation) {
        super(message, nodeLocation);
    }

    /**
     * Constructs a new <code>ValidationException</code> instance.
     * @param cause
     * @param nodeLocation
     */
    public ValidationException(Throwable cause, NodeLocation nodeLocation) {
        super(cause, nodeLocation);
    }

    /**
     * Constructs a new <code>ValidationException</code> instance.
     * @param description
     * @param cause
     * @param nodeLocation
     */
    public ValidationException(String message, Throwable cause, NodeLocation nodeLocation) {
        super(message, cause, nodeLocation);
    }
}
