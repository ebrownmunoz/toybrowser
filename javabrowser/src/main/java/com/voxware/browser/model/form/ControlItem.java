/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.List;
import java.util.Map;

import com.voxware.browser.event.Catch;
import com.voxware.browser.model.NodeLocation;

/**
 * ControlItem
 *
 * Created: Oct 19, 2015
 * @author edh
 */
public abstract class ControlItem extends FormItem {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ControlItem</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public ControlItem(Map<String, String> attributes, NodeLocation nodeLocation, List<Catch> catches) {
        super(attributes, nodeLocation, catches, null);
    }

}
