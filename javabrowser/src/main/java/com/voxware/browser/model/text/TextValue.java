/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.text;

import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.AbstractVxmlNode;

/**
 * Represents a <value> tag. The expr is evaluated with the given scope.
 */
public class TextValue extends AbstractVxmlNode implements Text {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final String expr;

    /**
     * Constructs a new <code>TextValue</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public TextValue(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        this.expr = getAttribute("expr");
    }

    public String getString(SessionContext context, Scriptable scope) {
        Context jscontext = context.getContext();
        String result = String.valueOf(jscontext.evaluateString(scope, this.expr, expr, 0, null));
        return result;
    }

    @Override
    public String toString() {
        return "<value expr=\"" + expr + "\"/>";
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (expr == null) {
            throw new ValidationException("<value> element must have 'expr' attribute'", getNodeLocation());
        }
    }
}
