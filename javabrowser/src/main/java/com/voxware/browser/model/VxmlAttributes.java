/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

/**
 * VxmlAttributes
 * 
 * @author edh
 */
public interface VxmlAttributes {
    /**
     * archive attribute (object tag)
     */
    static final String ARCHIVE = "archive";
    /**
     * classid attribute (object tag)
     */
    static final String CLASSID = "classid";
    /**
     * codebase attribute (object tag)
     */
    static final String CODEBASE = "codebase";
    /**
     * codetype attribute (object tag)
     */
    static final String CODETYPE = "codetype";
    /**
     * cond attribute
     */
    static final String COND = "cond";
    /**
     * count attribute
     */
    static final String COUNT = "count";
    /**
     * data attribute (object tag)
     */
    static final String DATA = "data";
    /**
     * enctype attribute
     */
    static final String ENCTYPE = "enctype";
    /**
     * event attribute
     */
    static final String EVENT = "event";
    /**
     * events attribute (used only for non-executable log element)
     */
    static final String EVENTS = "events";
    /**
     * eventexpr attribute
     */
    static final String EVENTEXPR = "eventexpr";
    /**
     * expr attribute
     */
    static final String EXPR = "expr";
    /**
     * id attribute
     */
    static final String ID = "id";
    /**
     * description attribute
     */
    static final String MESSAGE = "message";
    /**
     * messageexpr attribute
     */
    static final String MESSAGEEXPR = "messageexpr";
    /**
     * method attribute
     */
    static final String METHOD = "method";
    /**
     * modal attribute
     */
    static final String MODAL = "modal";
    /**
     * mode attribute
     */
    static final String MODE = "mode";
    /**
     * name attribute
     */
    static final String NAME = "name";
    /**
     * namelist attribute
     */
    static final String NAMELIST = "namelist";
    /**
     * next attribute
     */
    static final String NEXT = "next";
    /**
     * nextitem attribute
     */
    static final String NEXTITEM = "nextitem";
    /**
     * scope attribute
     */
    static final String SCOPE = "scope";
    /**
     * slot attribute
     */
    static final String SLOT = "slot";
    /**
     * src attribute
     */
    static final String SRC = "src";
    /**
     * srcexpr attribute
     */
    static final String SRCEXPR = "srcexpr";
    /**
     * timeout attribute
     */
    static final String TIMEOUT = "timeout";
    /**
     * type attribute
     */
    static final String TYPE = "type";
    /**
     * value attribute
     */
    static final String VALUE = "value";
    /**
     * valuetype attribute
     */
    static final String VALUE_TYPE = "valuetype";
}
