/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.Map;

import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: Jul 31, 2015 by: eric</li>
 * </ul>
 */
public class IfClause extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";
    private final String cond;

    private final ExecutableList executables;

    /**
     * Constructs a new <code>IfClause</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     * @param executables
     */
    public IfClause(Map<String, String> attributes, NodeLocation nodeLocation, ExecutableList executables) {
        super(attributes, nodeLocation);
        this.cond = getAttribute(COND);
        this.executables = (executables != null) ? executables : ExecutableList.EMPTY_LIST;
    }

    /**
     * Get an unmodifiable list of executables.
     * 
     * @return
     */
    public ExecutableList getExecutables() {
        return executables;
    }

    /**
     * Get the condition of the clause
     */
    public String getCond() {
        return cond;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        /*
         * Nothing to validate, since IfStatement validates everything
         */
    }
}
