package com.voxware.browser.model;

import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.interrupt.Interrupt;
import com.voxware.browser.io.RecognitionResult;

public class InterruptFieldNode extends InterruptNode {
	
	public InterruptFieldNode(Map<String, String> attributes, NodeLocation nodeLocation) {
		super(attributes, nodeLocation, Type.RESULT);
	}

	@Override
	public void validate(ValidationContext context) throws ValidationException {
		// TODO Auto-generated method stub
	}

    public RecognitionResult processInterrupt(BaseContext context, Interrupt interrupt) {
        return RecognitionResult.success(this.getSource(), interrupt.getMessage());
    }
	

}
