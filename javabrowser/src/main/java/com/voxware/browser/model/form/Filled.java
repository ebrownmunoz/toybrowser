/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Filled Created: Oct 19, 2015
 * 
 * @author edh
 */
public class Filled extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    public static final String MODE_ALL = "all";
    public static final String MODE_ANY = "any";
    /**
     * Default mode, all
     */
    public static final String DEFAULT_MODE = MODE_ALL;

    private static final List<String> ACCEPTED_MODES = Collections
        .unmodifiableList(Arrays.asList(DEFAULT_MODE, MODE_ANY));

    private final ExecutableList executables;
    private final String mode;
    private final List<String> nameList;

    /**
     * Constructs a new <code>Filled</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     * @param executables
     */
    public Filled(Map<String, String> attributes, NodeLocation nodeLocation, ExecutableList executables) {
        super(attributes, nodeLocation);
        this.executables = (executables != null) ? executables : ExecutableList.EMPTY_LIST;
        mode = getAttribute(MODE, DEFAULT_MODE);
        nameList = getNameListAttribute(NAMELIST);
    }

    /**
     * Get the executables to run when the appropriate conditions are met
     * 
     * @return an <code>ExecutableList</code>
     */
    public ExecutableList getExecutables() {
        return executables;
    }

    /**
     * Gets the mode. Must be null if the parent element is an input item.
     * 
     * @return the mode "any" or "all"
     */
    public String getMode() {
        return mode;
    }

    /**
     * Gets the parsed namelist attribute. Must be null if the parent element is an input item.
     * 
     * @return a list of names
     */
    public List<String> getNameList() {
        return nameList;
    }
    
    public String getNameListAsString() {
    	
    	StringBuffer nameListBuffer = new StringBuffer();

    	if (nameList == null) {
    		return "[empty]";
    	} else {
    		// This is just for the logfile
        	for (String name : nameList) {
        		nameListBuffer.append(" " + name);
        	}
    	}
    	
    	return nameListBuffer.toString();
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (context.getParentNode() instanceof Form) {
            // we're child of form
            if (mode != null && !ACCEPTED_MODES.contains(mode)) {
                throw new ValidationException("'mode' must be one of 'any' or 'all'", getNodeLocation());
            }
        } else {
            // we're child of input item
            // we use getAttribute because we default MODE internally to all when not specified
            if (getAttribute(MODE) != null) {
                throw new ValidationException("'mode' cannot be specified when <filled> is a child of an input item",
                    getNodeLocation());
            }
            if (nameList != null) {
                throw new ValidationException(
                    "'namelist' cannot be specified when <filled> is a child of an input item", getNodeLocation());
            }
        }
    }
}
