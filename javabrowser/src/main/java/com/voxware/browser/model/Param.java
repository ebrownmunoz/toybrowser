/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.model.form.Subdialog;

/**
 * Param
 *
 * Created: Oct 14, 2015
 * @author edh
 */
public class Param extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    public static final List<String> ACCEPTED_VALUE_TYPES = Arrays.asList("data", "ref"); 
    private static final Logger log = LoggerFactory.getLogger(Param.class);
    
    private final String name;
    private final String expr;
    private final String value;
    private final String valueType;
    private final String type;
    
    /**
     * Constructs a new <code>Param</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public Param(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        this.name = getAttribute(NAME);
        this.expr = getAttribute(EXPR);
        this.value = getAttribute(VALUE);
        this.valueType = getAttribute(VALUE_TYPE);
        this.type = getAttribute(TYPE);
    }

    /**
     * Gets the name attribute. This attribute is never null if validation is passed.
     * @return the name, never null
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the expr attribute
     * @return the expr or null
     */
    public String getExpr() {
        return expr;
    }

    public String getValue() {
        return value;
    }

    public String getValueType() {
        return valueType;
    }

    public String getType() {
        return type;
    }
    
    /**
     * Evaluates the value in a given scope
     * @param sessionContext
     * @param context
     * @return an object
     */
    public Object evaluateValue(SessionContext sessionContext, BaseContext context) {
        if (expr != null) {
            return context.evaluate(expr);
        } else {
            return this.value;
        }
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (name == null) {
            throw new ValidationException("'name' must be defined", getNodeLocation());
        }
        if (!context.getValidationUtils().exactlyOneNonNull(expr, value)) {
            throw new ValidationException("exactly one of 'expr' or 'value' must be defined", getNodeLocation());
        }
        // in subdialog, param's valuetype must be data
        if (context.getParentNode() instanceof Subdialog && valueType != null && !valueType.equals("data")) {
            throw new ValidationException("'valuetype' attribute in a <param ...> element must be 'data' for a subdialog", getNodeLocation());
        }
        if (valueType != null && !ACCEPTED_VALUE_TYPES.contains(valueType)){
            throw new ValidationException("attribute 'valuetype' must be 'data' or 'ref'", getNodeLocation());
        }
        if (valueType != null && !valueType.equals("ref") && type != null) {
            log.warn("attribute 'type' not relevant if attribute 'valuetype' is not 'ref'");
        }
        if (expr == null && value == null) {
            throw new ValidationException("expr and value can't both be null", getNodeLocation());
        }
        if (expr != null && value != null) {
            throw new ValidationException("expr and value can't both be defined", getNodeLocation());
        }
    }
    
    public String toString() {
        return name;
    }
}
