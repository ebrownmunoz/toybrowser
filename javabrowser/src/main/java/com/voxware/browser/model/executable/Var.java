/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its
 * licensors and/or suppliers, as applicable, shall retain all right, title and
 * interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. All copies
 * of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use
 * license keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.ExecutableElement;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents the <var> tag
 */
public class Var extends ExecutableElement implements Initializer {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";
    private final String name;
    private final String expr;

    /**
     * Constructs a new <code>Var</code> instance.
     * @param attributes 
     * @param nodeLocation 
     */
    public Var(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        this.name = getAttribute(NAME);
        this.expr = getAttribute(EXPR);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.model.Initializer#initialize(com.voxware.browser.
     * context.SessionContext, com.voxware.browser.context.scope.BrowserScope)
     */
    @Override
    public Result initialize(SessionContext scontext, BaseContext context) {
        return setValue(scontext.getContext(), context.getScope(), context.getScope(), name, expr);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.model.Executable#execute(com.voxware.browser.context.
     * SessionContext, com.voxware.browser.context.BaseContext,
     * com.voxware.browser.event.EventList)
     */
    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
        return setValue(scontext.getContext(), context.getScope(), context.getScope(), name, expr);
    }

    /**
     * Set the value of a variable
     * 
     * @param rhino the rhino context
     * @param declarationScope the scope where the variable is declared/to be declared
     * @param evaluationScope the scope to evaluate the expression
     * @param name the name of the variable to assign
     * @param expr the expression to be evaluated to get a value
     * @return ok result or error.semantic?
     */
    protected Result setValue(Context rhino, Scriptable declarationScope, Scriptable evaluationScope, String name, String expr) {
        Object value = Context.getUndefinedValue();
        if (expr != null) {
            value = rhino.evaluateString(evaluationScope, expr, getNodeLocation().getUri().toString(), getNodeLocation().getLineNumber(), null);
            if (value == null) {
                value = Context.getUndefinedValue();
            }
        }
        declarationScope.put(name, declarationScope, value);
        return Result.Ok();
    }

    /**
     * get the name attribute
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the expr attribute
     * 
     * @return
     */
    public String getExpr() {
        return expr;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (name == null || name.isEmpty()) {
            throw new ValidationException("'name' attribute must be defined'", getNodeLocation());
        }
        if (context.isEcmaScriptReservedWord(name)) {
            throw new ValidationException("variable name '" + name + "' is a reserved word", getNodeLocation());
        }
        if (context.isVxmlReservedSyntax(name)) {
            throw new ValidationException("variable names starting with '_' or ending with '$' are reserved", getNodeLocation());
        }
    }
}
