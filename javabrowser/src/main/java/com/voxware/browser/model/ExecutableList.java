/*
 * Copyright © 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable, shall
 * retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks, trade
 * secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description: Decorates a list of executables so that all can be executed with the event list handled.</li>
 * <li>Created: Jul 31, 2015 by: eric</li>
 * </ul>
 */
public class ExecutableList implements Executable, Iterable<Executable> {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";

    /**
     * log is not really a constant
     */
    private static final Logger log = LoggerFactory.getLogger(ExecutableList.class);

    /**
     * The empty executable list.
     */
    public static final ExecutableList EMPTY_LIST = new ExecutableList(null);

    private final List<Executable> executables;

    /**
     * Constructs a new <code>ExecutableList</code> instance. If <code>executables</code> is null, treats it like the
     * empty list. ExecutableList is immutable.
     * 
     * @param executables the executable elements
     */
    public ExecutableList(List<Executable> executables) {
        if (executables != null) {
            this.executables = Collections.unmodifiableList(new ArrayList<Executable>(executables));
        } else {
            this.executables = Collections.emptyList();
        }
    }

    /**
     * Execute all of the executables in the list, returning an aggregate result. If the result of any executable is
     * GOTO or SUBMIT it returns immediately. Any events are handled using the given Event List Any normal event should
     * be handled
     * 
     * @param scontext
     * @param context
     * @return
     */
    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
        Result result = Result.Ok();
        boolean reprompt = false;
        for (Executable each : executables) {
            result = executeOne(each, scontext, context);
            switch (result.getType()) {
            	case SUBMIT:
            		result = result.withReprompt(reprompt);
            		// this falls through to return result
            	case GOTO:
                case GOTO_NEXTITEM:
                case RETURN:
                case EVENT:
                    return result;

                case OK:
                    reprompt = reprompt || result.getReprompt();
                    break;
                default:
                    break;
            }
        } // for
        return result;
    } // executeAll

    /**
     * Execute one Executable returning a result. If this is an event, it is handled using the given event list.
     **/

    public Result executeOne(Executable each, SessionContext scontext, BaseContext context) {
        Result result = each.execute(scontext, context);
        while (result.getType() != Result.Type.OK) {
            switch (result.getType()) {
                case GOTO:
                    // fall through
                case GOTO_NEXTITEM:
                    // fall through
                case SUBMIT:
                    // fall through
                case RETURN:
                case EVENT:
                    return result;                
//                    final Event event = result.getEvent();
//                    final EventList eventList = context.getEventList();
//                    final EventHandler firstMatch = eventList.getFirstMatch(scontext, event.getName(), scontext.);
//                    if (firstMatch != null) {
//                        result = firstMatch.handleEvent(scontext, context, context.getEventList(), event);
//                    } else {
//                        // what do we do if we have no event handlers?
//                        return Result.exit(-1, "Unhandled event");
//                    }
//                    break;
                default:
                    log.error("Unexpected Result Type " + result.getType());
                    return Result.event("error.semantic.internal", "Unexpected Result Type");
            }
        } // while
        return result;
    }

    public int size() {
        return executables.size();
    }

    public boolean isEmpty() {
        return executables.isEmpty();
    }

    public boolean equals(Object o) {
        return executables.equals(o);
    }

    public int hashCode() {
        return executables.hashCode();
    }

    public Executable get(int index) {
        return executables.get(index);
    }

    /* 
     * (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<Executable> iterator() {
        return executables.iterator();
    }
}
