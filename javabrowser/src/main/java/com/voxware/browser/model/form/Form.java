/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.voxware.browser.event.Catch;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.InterruptLinkNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.VxmlNode;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * Represents a Form.
 */
public class Form extends AbstractVxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private final String id;
    private final String scope;
    private final List<FormItem> formItems;
    private final List<Initializer> initializers;
    private final List<Filled> filledHandlers;
    private final List<Catch> catches;
    private final List<InterruptLinkNode> links;

    private final Speaker speaker;
    private final Grammar grammar;

    /**
     * Constructs a new <code>Form</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     * @param initializers
     * @param formItems
     * @param filleds
     */
    public Form(Map<String, String> attributes, NodeLocation nodeLocation, List<Initializer> initializers,
        List<FormItem> formItems, List<Filled> filleds, List<Catch> catches, List<InterruptLinkNode> links, Speaker speaker, Grammar grammar) {
        super(attributes, nodeLocation);
        id = getAttribute(ID);
        scope = getAttribute(SCOPE);
        this.initializers = makeFinal(initializers);
        this.formItems = makeFinal(formItems);
        this.filledHandlers = makeFinal(filleds);
        this.catches = makeFinal(catches);
        this.speaker = speaker;
        this.grammar = grammar;
        this.links = links;
    }

    /**
     * @return
     */
    public List<FormItem> getFormItems() {
        return formItems;
    }

    /**
     * @return
     */
    public List<Initializer> getInitializers() {
        return initializers;
    }

    /**
     * Returns the id of the form
     * 
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the scope of the form
     * 
     * @return
     */
    public String getScope() {
        return scope;
    }

    /**
     * @return
     */
    public List<Filled> getFilledHandlers() {
        return filledHandlers;
    }

    /**
     * @return the speaker
     */
    public Speaker getSpeaker() {
        return speaker;
    }

    /**
     * @return the grammar
     */
    public Grammar getGrammar() {
        return grammar;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        final Set<String> formItemNames = new HashSet<String>();
        for (Initializer initalizer : initializers) {
            // validates var and catches
            // everything else is synthetic e.g. FilledRegisterer
            if (initalizer instanceof VxmlNode) {
                context.validateChild(this, (VxmlNode) initalizer);
            }
        }
        for (FormItem formItem : formItems) {
            // validates all form items
            context.validateChild(this, formItem);
            String name = formItem.getName();
            if (name != null) {
                if (formItemNames.contains(name)) {
                    throw new ValidationException("Duplicate form item names: " + name, getNodeLocation());
                } else {
                    formItemNames.add(name);
                }
            }
        }
        for (Filled filled : filledHandlers) {
            // validates <filled> elements
            context.validateChild(this, filled);
        }
        for (Catch katch : catches) {
            context.validateChild(this, katch);
        }
    }

	/**
	 * @return the links
	 */
	public List<InterruptLinkNode> getLinks() {
		return links;
	}

    /**
     * Returns the catches.
     * @return the catches.
     */
    public List<Catch> getCatches() {
        return catches;
    }
	
	
}
