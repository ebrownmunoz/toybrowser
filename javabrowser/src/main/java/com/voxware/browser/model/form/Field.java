/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.List;
import java.util.Map;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.InterruptFieldNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.executable.Log;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * Represents a Field element.
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: May 29, 2015 by: eric</li>
 * </ul>
 */
public class Field extends InputItem {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private final List<Grammar> grammars;
    private final List<InterruptFieldNode> interrupts;
    private final Speaker speaker;
    private final String slot;
    // this is to support very non standard VXML behavior
    private final List<Log> loggers;
    private final boolean modal;

    /**
     * Constructs a new <code>Field</code> instance.
     * @param attributes
     * @param nodeLocation
     * @param grammars
     * @param catches
     * @param filledHandlers
     */
    public Field(Map<String, String> attributes, NodeLocation nodeLocation, List<Prompt> prompts, Speaker speaker, List<Grammar> grammars,
        List<Catch> catches, List<Filled> filledHandlers, List<Property> properties, List<InterruptFieldNode> interrupts, List<Log> loggers) {
        super(attributes, nodeLocation, prompts, catches, filledHandlers, properties);
        this.grammars = makeFinal(grammars);
        this.interrupts = makeFinal(interrupts);
        this.speaker = speaker;
        this.slot = getAttribute(SLOT);
        this.loggers = makeFinal(loggers);
        this.modal = getBooleanAttribute(MODAL, false);
    }

    /**
     * Returns the grammars.
     * 
     * @return the grammars.
     */
    public List<Grammar> getGrammars() {
        return grammars;
    }
    
	/**
	 * @return the interrupts
	 */
	public List<InterruptFieldNode> getInterrupts() {
		return interrupts;
	}

	public String getSlot() {
	    return slot;
	}
	
	public List<Log> getLogs() {
	    return loggers;
	}

	public boolean isModal() {
	    return modal;
	}
	
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.form.InputItem#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        super.validate(context);
        context.validateChildren(this, grammars);
        context.validateChildren(this, properties);
        context.validateChildren(this, interrupts);
        context.validateChildren(this, loggers);
    }
}
