/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.audioplayer.AudioWrapper;
import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.io.Playable;
import com.voxware.browser.io.TextPlayable;
import com.voxware.browser.io.ValuePlayable;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents the <prompt> element. Note that this only supports CDATA and the <value> tag (there are more tags in the
 * W3C spec).
 */
public class Prompt extends AbstractVxmlNode implements Executable {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private static final String BARGEIN = "bargein";

    /*
     * The content of the prompt
     */
    private final CompositePlayable content;
    private final String src;
    private final Boolean bargeIn;
    private final String cond;
    private final Integer count;
    private final Integer timeout;

    private Logger log = LoggerFactory.getLogger(Prompt.class);

    /**
     * Constructs a new <code>Prompt</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     * @param content
     */
    public Prompt(Map<String, String> attributes, NodeLocation nodeLocation, CompositePlayable content) {
        super(attributes, nodeLocation);
        src = getAttribute(SRC);
        this.content = content;
        this.bargeIn = getBooleanAttribute(BARGEIN);
        this.cond = getAttribute(COND);
        String countAttr = getAttribute(COUNT);
        if (countAttr == null) {
            this.count = 1;
        } else {
            boolean parsed = false;
            int intCount = 0;
            try {
                intCount = Integer.parseInt(countAttr);
                parsed = true;
            } catch (NumberFormatException e) {

            }
            if (parsed) {
                this.count = intCount;
            } else {
                this.count = null;
            }
        }
        String timeoutString = getAttribute(TIMEOUT);
        if (timeoutString != null) {
            if (timeoutString.endsWith("ms")) {
                this.timeout = Integer.parseInt(timeoutString);
            } else if (timeoutString.endsWith("s")) {
                String seconds = timeoutString.substring(0, timeoutString.length() - 1);
                this.timeout = Integer.parseInt(seconds) * 1000;
            } else {
                this.timeout = Integer.parseInt(timeoutString);
            }
        } else {
            this.timeout = null;
        }
    }

    public String getCond() {
        return cond;
    }

    /**
     * Evaluates the cond expression in the given context
     * 
     * @param scontext
     * @param context
     * @return
     */
    public boolean evaluateCond(SessionContext scontext, BaseContext context) {
        if (cond != null) {
            return Context.toBoolean(context.evaluate(cond));
        } else {
            return true;
        }
    }

    /**
     * Gets the count. Never returns null.
     * 
     * @return the count (default is 1)
     */
    public Integer getCount() {
        return count;
    }

    public List<AudioWrapper> getAudios(SessionContext scontext, BaseContext context) throws InterpreterException {
        List<AudioWrapper> wrappers = new ArrayList<AudioWrapper>();
        StringBuffer logbuffer = new StringBuffer();

        List<Playable> playables = null;

        if (content != null) {
            playables = content.getPlayables();
        } else {
            log.info("empty prompt");
        }

        if (playables != null) {
            for (Playable playable : playables) {
                if (playable instanceof TextPlayable) {
                    wrappers.add(new AudioWrapper(AudioPlayer.Type.STRING, playable.getText(scontext, context), null));
                } else {
                    List<Object> elements = playable.getAudio(scontext, context);
                    if (elements != null) {
                        for (Object element : elements) {
                            if (element instanceof byte[]) {
                                wrappers.add(new AudioWrapper(AudioPlayer.Type.BYTES, null, (byte[]) element));
                            } else if (element instanceof String) {
                                wrappers.add(new AudioWrapper(AudioPlayer.Type.STRING, (String) element, null));
                            }
                        }
                    } else {
                        wrappers.add(new AudioWrapper(AudioPlayer.Type.STRING, playable.getText(scontext, context), null));
                    }
                    
                }
                
                if (logbuffer.length() > 0) {
                	logbuffer.append(" ,");
                }
                
                logbuffer.append(playable.toString());
            } // for
        }

        log.info("Prompt.getaudios() returning: " + logbuffer.toString());
        return wrappers;
    }

    /**
     * See the output. This is public because test cases need it.
     * 
     * @param scontext
     * @param context
     * @return
     */
    public String getOutput(SessionContext scontext, BaseContext context) {
        if (content != null) {
            return content.getString(scontext, context);
        } else {
            return "";
        }
    }

    /**
     * Returns the bargein attribute
     * 
     * @return the bargein attribute
     */
    public Boolean getBargeIn() {
        return bargeIn;
    }

    /**
     * Returns the timeout attribute
     * 
     * @return
     */
    public Integer getTimeout() {
        return timeout;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext,
     * com.voxware.browser.context.AnonymousContext)
     */
    public Result execute(SessionContext scontext, BaseContext context) {
        Result result = Result.Ok();
        
        AudioPlayer audioPlayer = scontext.getIoManager().getAudioPlayer();
        PropertyList properties = new PropertyList(context.getPropertyList());
        for (Property each : properties.getAll().values()) {
            String propertyValue = Context.toString(each.evaluate(context));
            audioPlayer.setParameter(each.getName(), propertyValue);
        }

        if (src != null) {
        	log.info("Prompt.execute; src:" + src);
            InputStream is = null;
            try {
                //is = scontext.getDocumentServer().getDocument(new URI(src));
                URI test = new URI(src);
                Future<Void> play = audioPlayer.play(Arrays.asList(new AudioWrapper(AudioPlayer.Type.URI, src, null)));
                play.get();
//            } catch (DocumentServerException e) {
//                log.error("Error acquiring audio", e);
                // handle the error better, if the file is not found
            } catch (URISyntaxException e) {
                log.error("Error acquiring audio", e);
                result = Result.event("error.badfetch.uri", "Error acquiring " + src);
//            } catch (IOException e) {
//                log.error("Error acquiring audio", e);
//                result = Result.event("error.badfetch", "Error acquiring " + src);
            } catch (AudioPlayerException e) {
                log.error("Error playing audio", e);
                result = Result.event("error.badfetch", "Error acquiring " + src);
            } catch (InterruptedException e) {
                log.error("Error playing audio", e);
                result = Result.event("error", src);
            } catch (ExecutionException e) {
                if (e.getCause() instanceof IOException) {
                    log.error("Error acquiring audio", e);
                    result = Result.event("error.badfetch", "Error acquiring " + src);
                } else {
                    log.error("Error playing audio", e);
                    result = Result.event("error", src);
                }
            } finally {
                IOUtils.closeQuietly(is);
            }
        } else {
            try {
                if (evaluateCond(scontext, context)) {
                    List<AudioWrapper> audios = getAudios(scontext, context);
                    Future<Void> play = scontext.getIoManager().getAudioPlayer().play(audios);
                    play.get();
                }
            } catch (InterpreterException e) {
                log.error("Exception executing prompt", e);
                result = Result.event("error", "Error acquiring " + src);
            } catch (AudioPlayerException e) {
                log.error("Exception executing prompt", e);
            } catch (InterruptedException e) {
                log.error("Exception executing prompt", e);
            } catch (ExecutionException e) {
                log.error("Exception executing prompt", e);
            }
        }

        return result;
    }

    public String toString() {
        return content.toString();
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (count == null) {
            throw new ValidationException("Count is not valid: " + getAttribute("count"), this.getNodeLocation());
        }
    }

}
