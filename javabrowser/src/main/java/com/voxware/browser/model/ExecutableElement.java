/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.util.Map;

/**
 * ExecutableElement
 *
 * Created: Oct 15, 2015
 * @author edh
 */
public abstract class ExecutableElement extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ExecutableElement</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public ExecutableElement(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
    }
}
