/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * <ul>
 * <li>Title: Represents if/then/elese constructs in VXML</li>
 * <li>Description:</li>
 * <li>Created: Jul 31, 2015 by: eric</li>
 * </ul>
 */
public class IfStatement extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    
    private static Logger log = LoggerFactory.getLogger(IfStatement.class);

    private final List<IfClause> clauses;

    /**
     * Constructs a new <code>IfStatement</code> instance.
     * @param attributes
     * @param nodeLocation
     * @param clauses
     */
    public IfStatement(Map<String, String> attributes, NodeLocation nodeLocation, List<IfClause> clauses) {
        super(attributes, nodeLocation);
        this.clauses = makeFinal(clauses);
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext,
     * org.mozilla.javascript.Scriptable)
     */
    public Result execute(SessionContext scontext, BaseContext context) {
        IfClause clause = findClause(scontext, context.getScope());
        
        if (clause == null) { // nothing matches
            return Result.Ok();
        } else {
            log.info("Running ifclause: " + clause.getCond() + "at " + clause.getNodeLocation());
            ExecutableList executables = clause.getExecutables();
            return executables.execute(scontext, context);
        }
    }

    private IfClause findClause(SessionContext scontext, Scriptable scope) {
        for (IfClause clause : clauses) {
            String cond = clause.getCond();

            if (cond == null) { // this is the else clause (if we get there, run it)
                return clause;
            }

            final String location = clause.getNodeLocation().getUri().toString();
            final int lineNumber = clause.getNodeLocation().getLineNumber();
            Object condresult = scontext.getContext().evaluateString(scope, cond, location, lineNumber, null);

            if ((condresult != Context.getUndefinedValue()) && Context.toBoolean(condresult)) {
                return clause;
            } // if condresult
        } // for IfClause       
        return null;
    }

    /**
     * Add a clause to the if statement
     * 
     * @param clause
     */
    public void addClause(IfClause clause) {
        clauses.add(clause);
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (clauses.size() == 0) {
            throw new ValidationException("no if clauses", getNodeLocation());
        }
        for (int index = 0; index < clauses.size(); index++) {
            if (clauses.get(index).getCond() == null && index != (clauses.size() - 1)) {
                throw new ValidationException("else clause must be the final clause", getNodeLocation());
            }
        }
    }

}
