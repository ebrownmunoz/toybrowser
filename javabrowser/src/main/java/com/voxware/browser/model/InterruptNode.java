package com.voxware.browser.model;

import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.interrupt.Interrupt;
import com.voxware.browser.io.RecognitionResult;

public abstract class InterruptNode extends AbstractVxmlNode {

    public static final String SOURCE = "source";
    public static final String SENSE = "sense";
    public static final String LEVEL = "level";

    public static final String RISE_TO = "riseto";
    public static final String FALL_TO = "fallto";
	
    private final String source;
	private final String sense;
	private final Double level;
	
	public enum Type {
	    EVENT,
	    GOTO,
	    RESULT
	}
	
	private final Type type;
	
	public InterruptNode(Map<String, String> attributes, NodeLocation nodeLocation, Type type) {
		super(attributes, nodeLocation);
		this.source = getAttribute(SOURCE);
		this.level = getDoubleAttribute("level", null);
		if (level == null) {
		    this.sense = getAttribute(SENSE);
		} else {
		    // backwards compatibility
            this.sense = getAttribute(SENSE, FALL_TO);
		}
		
		this.type = type;
	}

	@Override
	public void validate(ValidationContext context) throws ValidationException {
		if (level == null && sense != null) {
		    throw new ValidationException("sense defined without level being defined", getNodeLocation());
		}
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	
	/**
     * Returns the sense.
     * @return the sense.
     */
    public String getSense() {
        return sense;
    }

    /**
     * Returns the level.
     * @return the level.
     */
    public Double getLevel() {
        return level;
    }

    /**
	 * 
	 */
	public Type getType() {
		return type;
	}
	
	public abstract RecognitionResult processInterrupt(BaseContext context, Interrupt interrupt);
	
}
