/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.text;

import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.SessionContext;

/**
 * Handle either a basic text, or an expression that turns into a string.
 */
public interface Text {

    /**
     * Return the contents as a String based on given scope.
     * 
     * @return
     */
    public String getString(SessionContext context, Scriptable scope);
}
