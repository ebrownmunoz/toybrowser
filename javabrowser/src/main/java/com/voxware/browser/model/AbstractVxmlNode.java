/*
 * Copyright © 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable, shall
 * retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks, trade
 * secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: Jun 24, 2015 by: eric</li>
 * </ul>
 */
public abstract class AbstractVxmlNode implements VxmlNode {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "© Copyright Voxware Inc. 2015.";

    private final Map<String, String> attributes;
    private final NodeLocation nodeLocation;

    /**
     * Constructs a new <code>VxmlNode</code> instance.
     * 
     * @param attributes node attributes
     * @param nodeLocation node location
     */
    public AbstractVxmlNode(Map<String, String> attributes, NodeLocation nodeLocation) {
        if (attributes != null) {
            this.attributes = Collections.unmodifiableMap(attributes);
        } else {
            // this works because EMPTY_MAP is immutable
            this.attributes = Collections.emptyMap();
        }
        if (nodeLocation != null) {
            this.nodeLocation = nodeLocation;
        } else {
            this.nodeLocation = new NodeLocation(URI.create("#unknown"), 0, 0, null);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.VxmlNode#getAttribute(java.lang.String)
     */
    @Override
    public String getAttribute(String key) {
        return attributes.get(key);
    }

    /**
     * Gets an attribute. Returns <code>defaultValue</code> if not defined.
     * 
     * @param key the attribute name
     * @param defaultValue the value to return if the attribute is not defined
     * @return a String
     */
    public String getAttribute(String key, String defaultValue) {
        if (attributes.containsKey(key)) {
            return attributes.get(key);
        } else {
            return defaultValue;
        }
    }

    /**
     * Convenience method for getting a Boolean attribute. Follows the rule of {@link Boolean} except with regards to a
     * <code>null</code> value.
     * 
     * @param key the name of the attribute
     * @return true if the attribute value is true, false if the attribute value is anything else or null if the
     * attribute does not exist
     */
    public Boolean getBooleanAttribute(String key) {
        String val = getAttribute(key);
        if (val == null) {
            return null;
        }
        return Boolean.valueOf(getAttribute(key));
    }

    /**
     * Convenience method for getting a boolean attribute. Follows the rule of {@link Boolean} except with regards to a
     * <code>null</code> value. Returns <code>defaultValue</code> if the attribute was not declared.
     * 
     * @param key the name of the attribute
     * @param defaultValue the default value to return if the attribute does not exist
     * @return a boolean
     */
    public boolean getBooleanAttribute(String key, boolean defaultValue) {
        Boolean b = Boolean.valueOf(getAttribute(key));
        if (b == null) {
            return defaultValue;
        } else {
            return b;
        }
    }

    /**
     * Convenience method for extracting a whitespace separated namelist attribute
     * 
     * @param key the name of the attribute
     * @return a list of strings, null if the attribute is null
     */
    public List<String> getNameListAttribute(String key) {
        String value = getAttribute(key);
        if (value == null) {
            return null;
        } else {
            return makeFinal(Arrays.asList(value.split("\\s+")));
        }
    }
    
    /**
     * Convenience method for extracting an enum attribute
     * @param <T> the enum type
     * @param key the name of the attribute
     * @param enumType the enum class
     * @param defaultValue the default value if the attribute is not defined
     * @param errorValue the value if the value of the attribute does not match any enum value
     * @return an enum or null if the attribute does not map to an enum
     */
    public <T extends Enum<T>> T getEnumAttribute(String key, Class<T> enumType, T defaultValue, T errorValue) {
        String value = getAttribute(key);
        if (value == null) {
            return defaultValue;
        }
        try {
            return Enum.valueOf(enumType, value);
        } catch (IllegalArgumentException e) {
            return errorValue;
        }
    }
    
    public Double getDoubleAttribute(String key, Double defaultValue) {
        String value = getAttribute(key);
        if (value == null) {
            return defaultValue;
        } else {
            try {
                return Double.valueOf(value);
            } catch (NumberFormatException e) {
                return Double.NaN;
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.VxmlNode#getNodeLocation()
     */
    @Override
    public NodeLocation getNodeLocation() {
        return nodeLocation;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.VxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public abstract void validate(ValidationContext context) throws ValidationException;

    /**
     * Makes an immutable list initialized from the list <code>elements</code>. Treats null as an empty list.
     * 
     * @param <T> some type
     * @param elements the list
     * @return an immutable list
     */
    protected <T> List<T> makeFinal(List<T> elements) {
        if (elements != null) {
            return Collections.unmodifiableList(new ArrayList<T>(elements));
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * Makes an immutable map initialized from the map <code>elements</code>. Treats null as an empty map.
     * 
     * @param <K> key type
     * @param <V> value type
     * @param elements the map
     * @return an immutable map
     */
    protected <K, V> Map<K, V> makeFinal(Map<K, V> elements) {
        if (elements != null) {
            return Collections.unmodifiableMap(new LinkedHashMap<K, V>(elements));
        } else {
            return Collections.emptyMap();
        }
    }

    public Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }
}
