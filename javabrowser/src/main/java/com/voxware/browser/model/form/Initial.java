/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.List;
import java.util.Map;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.InterruptFieldNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.Prompt;

/**
 * Represents a Field element.
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: May 29, 2015 by: eric</li>
 * </ul>
 */
public class Initial extends InputItem {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private final List<InterruptFieldNode> interrupts;

    /**
     * Constructs a new <code>Field</code> instance.
     * @param attributes
     * @param nodeLocation
     * @param prompts
     * @param grammars
     * @param catches
     * @param filledHandlers
     */
    public Initial(Map<String, String> attributes, NodeLocation nodeLocation, List<Prompt> prompts,
        List<Catch> catches, List<Filled> filledHandlers, List<Property> properties, List<InterruptFieldNode> interrupts) {
        super(attributes, nodeLocation, prompts, catches, filledHandlers, properties);        
        this.interrupts = makeFinal(interrupts);
    }

    /**
	 * @return the interrupts
	 */
	public List<InterruptFieldNode> getInterrupts() {
		return interrupts;
	}
}
