/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.form;

import java.util.List;
import java.util.Map;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.executable.Prompt;

/**
 * InputItem
 *
 * @author edh
 */
public abstract class InputItem extends FormItem {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private final List<Prompt> prompts;
    private final List<Filled> filledHandlers;

    /**
     * Constructs a new <code>InputItem</code> instance.
     * 
     * @param attributes node attributes
     * @param nodeLocation node location
     * @param prompts the prompt nodes
     * @param catches the catch nodes
     * @param filledHandlers {@literal <filled>} elements
     */
    public InputItem(Map<String, String> attributes, NodeLocation nodeLocation, List<Prompt> prompts,
        List<Catch> catches, List<Filled> filledHandlers, List<Property> properties) {
        super(attributes, nodeLocation, catches, properties);
        this.prompts = makeFinal(prompts);
        this.filledHandlers = makeFinal(filledHandlers);
    }

    /**
     * Returns the prompt.
     * 
     * @return the prompt.
     */
    public List<Prompt> getPrompts() {
        return prompts;
    }

    /**
     * Gets the the {@literal <filled>} child elements
     * 
     * @return the filled child elements, never null
     */
    public List<Filled> getFilledHandlers() {
        return filledHandlers;
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        super.validate(context);
        for (Prompt prompt : getPrompts()) {
            context.validateChild(this, prompt);
        }
        for (Filled filled : getFilledHandlers()) {
            context.validateChild(this, filled);
        }
    }
}
