/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.audioplayer.AudioWrapper;
import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.text.Text;

/**
 * Audio
 *
 * Created: Oct 13, 2015
 * @author edh
 */
public class Audio extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(Audio.class);
    private final String source;
    private final Text text;

    /**
     * Constructs a new <code>Audio</code> instance.
     * @param attributes
     * @param nodeLocation
     * @param text 
     */
    public Audio(Map<String, String> attributes, NodeLocation nodeLocation, Text text) {
        super(attributes, nodeLocation);
        this.source = getAttribute(SRC);
        this.text = text;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext, com.voxware.browser.context.BaseContext, com.voxware.browser.event.EventList)
     */
    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
    	log.info("Audio.execute; text:" + text + ", source:" + source);
    	Result result = Result.Ok();
    	AudioWrapper audio = null;
    	if (source != null) {
    	    // resolve the file
    	    InputStream is = null;
            try {
                is = scontext.getDocumentServer().getDocument(new URI(source));
                audio = new AudioWrapper(AudioPlayer.Type.BYTES, null, IOUtils.toByteArray(is));
            } catch (DocumentServerException e) {
                log.error("Error acquiring audio", e);
                result = Result.event("error.badfetch", "Error acquiring " + source);
            } catch (URISyntaxException e) {
                log.error("Error acquiring audio", e);
                result = Result.event("error.badfetch.uri", "Error acquiring " + source);
            } catch (IOException e) {
                log.error("Error acquiring audio", e);
                result = Result.event("error.badfetch", "Error acquiring " + source);
            } finally {
                IOUtils.closeQuietly(is);
            }
    	} else {
            audio = new AudioWrapper(AudioPlayer.Type.STRING, text.getString(scontext, context.getScope()), null);
    	}
    	
    	if (result.getType() == Result.Type.OK) {
            try {
                Future<Void> play = scontext.getIoManager().getAudioPlayer().play(Arrays.asList(audio));
                play.get();
            } catch (AudioPlayerException e) {
                log.error("Error executing audio ", e);
                result = Result.event("error.audio", "Error playing audio");
            } catch (InterruptedException e) {
                log.error("Error executing audio ", e);
                result = Result.event("error.audio", "Error playing audio");
            } catch (ExecutionException e) {
                log.error("Error executing audio ", e);
                result = Result.event("error.audio", "Error playing audio");
            }
    	}

        return result;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (!context.getValidationUtils().exactlyOneNonNull(source, text)) {
            throw new ValidationException("Audio must have either a src attribute or children nodes", getNodeLocation());
        }
    }
}
