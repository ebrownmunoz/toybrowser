/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.form.Block;

/**
 * Log
 *
 * Created: Oct 13, 2015
 * @author edh
 */
public class Log extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Logger log = LoggerFactory.getLogger(Log.class);
    
    private final CompositePlayable content;
    private final String cond;
    private final String to;
    private final String label;
    private final String expr;
    private final List<String> events;
    /**
     * Constructs a new <code>Log</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public Log(Map<String, String> attributes, NodeLocation nodeLocation, CompositePlayable content) {
        super(attributes, nodeLocation);
        this.content = content;
        this.to = getAttribute("to");
        this.label = getAttribute("label");
        this.cond = getAttribute(COND);
        this.expr = getAttribute(EXPR);
        this.events = getNameListAttribute(EVENTS);
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext, com.voxware.browser.context.BaseContext, com.voxware.browser.event.EventList)
     */
    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
        if (cond != null && !context.evaluateToBoolean(cond)) {
            return Result.Ok();
        }
        StringBuilder output = new StringBuilder();
        
        if (label != null) {
            output.append(label);
        }
        if (expr != null) {
            output.append(context.evaluateToString(expr));
        }
        if (content != null) {
            output.append(content.getString(scontext, context));
        } 
        try {
            scontext.getLogManager().sendLog(to, output.toString());
        } catch (NullPointerException e) {
            return Result.event("error.semantic", "Invalid log server name " + to);
        }
        
        log.info(output.toString());
        return Result.Ok();
    }
    
    public List<String> getEvents() {
        return events;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (getEvents() == null && !(context.getParentNode() instanceof Block)) {
            throw new ValidationException("non-executable log elements must declare an events attribute", getNodeLocation());
        }
        if (getEvents() != null && context.getParentNode() instanceof Block) {
            throw new ValidationException("executable log elements must not declare an events attribute", getNodeLocation());
        }
    }
}
