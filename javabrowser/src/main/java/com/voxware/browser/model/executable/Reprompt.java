/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.AbstractVxmlNode;

/**
 * Reprompt
 *
 * Created: Oct 13, 2015
 * @author edh
 */
public class Reprompt extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>Reprompt</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public Reprompt(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext, com.voxware.browser.context.BaseContext, com.voxware.browser.event.EventList)
     */
    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
        return Result.reprompt();
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        // TODO Auto-generated method stub
        
    }

}
