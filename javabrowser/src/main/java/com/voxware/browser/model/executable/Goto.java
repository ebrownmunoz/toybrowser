/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.AbstractVxmlNode;

/**
 * Represents a Goto tag
 */
public class Goto extends AbstractGoto implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    private static final Logger log = LoggerFactory.getLogger(Goto.class);
    private final String nextitem;

    /**
     * Constructs a new <code>Goto</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public Goto(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        nextitem = attributes.get(NEXTITEM);
    }

    /**
     * Return a goto result
     */
    public Result execute(SessionContext scontext, BaseContext context) {
    	String next;
    	Scriptable scope = context.getScope();

    	if (nextitem != null) {
    		return new Result(Result.Type.GOTO_NEXTITEM, null, nextitem, false, false);
    	}  else {
    		try {
    			next = evaluateTargetURI(scontext, scope);
    		} catch (InterpreterException e) {
    			log.error("Exception in execute", e);
    			return Result.event("error.semantic.js", "Interpreter Exeception");
    		}
    		return new Result(Result.Type.GOTO, next, false);
    	}
    }
}
