/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents a throw tag. This simply returns an even response when executed. The response will be populated with the 'event' and 'description' tags. Created: Aug 5, 2015 by: eric
 */
public class Throw extends AbstractVxmlNode implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final String event;
    private final String eventExpr;
    private final String message;
    private final String messageExpr;

    /**
     * Constructs a new <code>Throw</code> instance.
     * @param attributes
     * @param nodeLocation
     */
    public Throw(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        this.event = getAttribute(EVENT);
        this.eventExpr = getAttribute(EVENTEXPR);
        this.message = getAttribute(MESSAGE);
        this.messageExpr = getAttribute(MESSAGEEXPR);
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext, org.mozilla.javascript.Scriptable)
     */
    public Result execute(SessionContext scontext, BaseContext context) {
        String event = this.event != null ? this.event : context.evaluateToString(eventExpr);
        String message = null;
        if (this.message != null) {
            message = this.message;
        } else if (this.messageExpr != null) {
            message = context.evaluateToString(messageExpr);
        }
        return Result.event(event, message);
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (!context.getValidationUtils().exactlyOneNonNull(event, eventExpr)) {
            throw new ValidationException("one and only one of 'event' or 'eventexpr' must be defined", getNodeLocation());
        }
        if (!context.getValidationUtils().atMostOneNonNull(message, messageExpr)) {
            throw new ValidationException("cannot define both 'description' and 'messageexpr'", getNodeLocation());
        }
    }
}
