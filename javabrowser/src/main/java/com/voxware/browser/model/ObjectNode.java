/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.util.List;
import java.util.Map;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.event.Catch;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.model.form.InputItem;

/**
 * Object
 *
 * @author edh
 */
public class ObjectNode extends InputItem implements Executable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final String classId;
    private final String codebase;
    private final String codeType;
    private final String data;
    private final String type;
    private final String archive;
    private final List<Param> params; 
    /**
     * Constructs a new <code>Object</code> instance.
     * 
     * @param attributes
     * @param nodeLocation
     */
    public ObjectNode(Map<String, String> attributes, NodeLocation nodeLocation, List<Filled> filledHandlers, CacheControl cacheControl,
        List<Param> params, List<Catch> catches, List<Property> properties) {
        // we do not support prompting for objects
        super(attributes, nodeLocation, null, catches, filledHandlers, properties);
        classId = getAttribute(CLASSID);
        codebase = getAttribute(CODEBASE);
        codeType = getAttribute(CODETYPE);
        data = getAttribute(DATA);
        type = getAttribute(TYPE);
        archive = getAttribute(ARCHIVE);
        this.params = makeFinal(params);
    }

    /**
     * Returns the classId.
     * @return the classId.
     */
    public String getClassId() {
        return classId;
    }

    /**
     * Returns the codebase.
     * @return the codebase.
     */
    public String getCodebase() {
        return codebase;
    }

    /**
     * Returns the codeType.
     * @return the codeType.
     */
    public String getCodeType() {
        return codeType;
    }

    /**
     * Returns the data.
     * @return the data.
     */
    public String getData() {
        return data;
    }

    /**
     * Returns the type.
     * @return the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the archive.
     * @return the archive.
     */
    public String getArchive() {
        return archive;
    }

    /**
     * Returns the params.
     * @return the params.
     */
    public List<Param> getParams() {
        return params;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.Executable#execute(com.voxware.browser.context.SessionContext,
     * com.voxware.browser.context.BaseContext)
     */
    @Override
    public Result execute(SessionContext scontext, BaseContext context) {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.AbstractVxmlNode#validate(com.voxware.browser.model.ValidationContext)
     */
    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (codebase != null) {
            throw new ValidationException("codebase is not supported", getNodeLocation());
        }
        if (codeType != null) {
            throw new ValidationException("codetype is not supported", getNodeLocation());
        }
        if (data != null) {
            throw new ValidationException("data is not supported", getNodeLocation());
        }
        if (type != null) {
            throw new ValidationException("type is not supported", getNodeLocation());
        }
        if (archive != null) {
            throw new ValidationException("archive is not supported", getNodeLocation());
        }
    }

}
