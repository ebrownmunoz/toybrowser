/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable, shall
 * retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks, trade
 * secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import javax.inject.Inject;

import com.voxware.browser.parser.ValidationUtils;

/**
 * ValidationContext Created: Oct 15, 2015
 * 
 * @author edh
 */
public class ValidationContext {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    /**
     * A list of words reserved by EcmaScript 6
     */
    public static final List<String> ECMASCRIPT_RESERVED_WORDS = Collections.unmodifiableList(
        Arrays.asList("abstract", "arguments", "boolean", "break", "byte", "case", "catch", "char", "class", "const",
            "continue", "debugger", "default", "delete", "do", "double", "else", "enum", "eval", "export", "extends",
            "false", "final", "finally", "float", "for", "function", "goto", "if", "implements", "import", "in",
            "instanceof", "int", "interface", "let", "long", "native", "new", "null", "package", "private", "protected",
            "public", "return", "short", "static", "super", "switch", "synchronized", "this", "throw", "throws",
            "transient", "true", "try", "typeof", "var", "void", "volatile", "while", "with", "yield"));

    private final Stack<VxmlNode> ancestorNodes = new Stack<VxmlNode>();
    private ValidationUtils validationUtils;

    /**
     * Constructs a new <code>ValidationContext</code> instance.
     * 
     * @param validationUtils
     */
    @Inject
    public ValidationContext(ValidationUtils validationUtils) {
        this.validationUtils = validationUtils;
    }

    /**
     * Gets the {@link ValidationUtils} object for the system.
     * 
     * @return a {@link ValidationUtils} instance
     */
    public ValidationUtils getValidationUtils() {
        return validationUtils;
    }

    /**
     * Gets the parent node of the current node being validated
     * 
     * @return
     */
    public VxmlNode getParentNode() {
        return ancestorNodes.peek();
    }

    /**
     * Validates a <code>child</code> of the <code>parent</code>. If all children validation calls go through this
     * method, then the <code>getParentNode()</code> call should be consistent.
     * 
     * @param parent the parent node
     * @param child this child node
     * @throws ValidationException if the child node's <code>validate(ValidationContext)</code> invocation throws an
     * exception
     */
    public void validateChild(VxmlNode parent, VxmlNode child) throws ValidationException {
        ancestorNodes.push(parent);
        if (child != null) {
            child.validate(this);
        }
        ancestorNodes.pop();
    }
    
    /**
     * Validates a list of children. Convenience method for calling <code>validateChild</code>.
     * @param parent the parent node
     * @param children a list of children nodes
     * @throws ValidationException if any child node's <code>validate(ValidationContext)</code> invocation throws an
     * exception
     */
    public void validateChildren(VxmlNode parent, Collection<? extends VxmlNode> children) throws ValidationException {
        ancestorNodes.push(parent);
        if (children != null) {
            for (VxmlNode child : children) {
                if (child != null) {
                    child.validate(this);
                }
            }
        }
        ancestorNodes.pop();
    }

    /**
     * Returns true if <code>word</code> is an EcmaScript reserved word.
     * 
     * @param word the word to check
     * @return true if the word is reserved, false otherwise
     */
    public boolean isEcmaScriptReservedWord(String word) {
        return ECMASCRIPT_RESERVED_WORDS.contains(word);
    }

    /**
     * Returns true if <code>name</code> is reserved by VXML. The name is reserved if it starts with '_' (underscore) or
     * ends with '$' (dollar sign).
     * 
     * @param name the name to check
     * @return true if the name starts with _ or ends with $
     */
    public boolean isVxmlReservedSyntax(String name) {
        return (name.startsWith("_") || name.endsWith("$"));
    }
}
