/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its
 * licensors and/or suppliers, as applicable, shall retain all right, title and
 * interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. All copies
 * of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use
 * license keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

/**
 * VxmlNode
 * 
 * @author edh
 */
public interface VxmlNode extends VxmlAttributes {
    /**
     * Gets an attribute. Returns null if the attribute is not defined.
     * 
     * @param key the attribute name
     * @return a String value or null if not defined
     */
    String getAttribute(String key);

    /**
     * Gets the NodeLocation for the node.
     * 
     * @return a NodeLocation
     */
    NodeLocation getNodeLocation();

    /**
     * Performs validation of this node. At least the attributes should be
     * validated if not the content. A node should not invoke this on children
     * nodes, but should call
     * {@link ValidationContext#validateChild(VxmlNode, VxmlNode)} instead.
     * 
     * @param context the validation context
     * @throws ValidationException on error
     */
    void validate(ValidationContext context) throws ValidationException;

}