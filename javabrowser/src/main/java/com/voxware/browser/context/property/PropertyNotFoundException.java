/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context.property;

/**
 * PropertyNotFoundException
 *
 * @author edh
 */
public class PropertyNotFoundException extends Exception {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2016.";

    /**
     * Constructs a new <code>PropertyNotFoundException</code> instance.
     */
    public PropertyNotFoundException() {
    }

    /**
     * Constructs a new <code>PropertyNotFoundException</code> instance.
     * @param message
     */
    public PropertyNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructs a new <code>PropertyNotFoundException</code> instance.
     * @param cause
     */
    public PropertyNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new <code>PropertyNotFoundException</code> instance.
     * @param message
     * @param cause
     */
    public PropertyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new <code>PropertyNotFoundException</code> instance.
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public PropertyNotFoundException(String message, Throwable cause, boolean enableSuppression,
        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
