/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */

package com.voxware.browser.context;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.event.FilledList;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.Initializer;

/**
 * Base class for contexts
 */
public class BaseContext {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    
    private static final Logger log = LoggerFactory.getLogger(BaseContext.class);
    
    /**
     * The Session Context
     */
    private final SessionContext sessionContext;
    private BrowserScope scope;
    private final CatchList eventList;
    private final PropertyList propertyList;
    private final FilledList filledList;
    
    /**
     * Constructs a new <code>DocumentContext</code> instance.
     * @param name
     * @param sessionContext
     * @param parentScope
     */
    protected BaseContext(SessionContext sessionContext, Scriptable parentScope, String scopeName, 
        CatchList eventList, PropertyList propertyList) {
        this.sessionContext = sessionContext;
        this.eventList = eventList;
        this.propertyList = propertyList;
        this.scope = new BrowserScope(scopeName);
        this.scope.setParentScope(parentScope);
        this.filledList = new FilledList();
    }
    
    /**
     * Apply a List of initializer to the context scope
     * @param initializers
     */
    public void applyInitializers(List<Initializer> initializers, InterpreterState state) {
        for (Initializer each : initializers) {
            each.initialize(sessionContext, this);
        }
    }
    
    /**
     * Sets an indexed property in the document scope object
     * TODO not tested (and not yet used)
     * @param index
     * @param start
     * @param value
     */
    public void put(int index, Scriptable start, Object value) {
        scope.put(index, start, value);
    }

    /**
     * Sets a named property in the document scope object
     * TODO not tested (and not yet used)
     * @param name
     * @param start
     * @param value
     */
    public void put(String name, Scriptable start, Object value) {
        scope.put(name, start, value);
    }
    
    /**
     * Put a named property in the top of the document scope object
     * @param name
     * @param value
     */
    public void put (String name, Object value) {
        scope.put(name, scope, value);
    }
    
    /**
     * Get property from scope
     * @param name
     * @return
     */
    public Object get (String name) {
        return scope.get(name);
    }
    
    /**
     * Evaluate a javascript string and return the result as a string.
     * @param javascript
     * @return
     * @throws Exception 
     */
    public String evaluateToString (String javascript) {
    	
    	String retval = null;
    	
        try {
        	retval = Context.toString(evaluate(javascript));
        } catch (Exception e) {
        	log.error("Error evaluating JS " + javascript, e);
        	//throw new InterpreterException("Exception parsing JavaScript", null);
        }
        
        return retval;
    }
    
    /**
     * Evaluate a javascript string and returns the result.
     * @param javascript
     * @return
     */
    public Object evaluate(String javascript) {
        return sessionContext.getContext().evaluateString(scope, javascript, "<Session>", 1, null);
    }
    
    public boolean evaluateToBoolean(String javascript) {
        Object value = evaluate(javascript);
        return Context.toBoolean(value);
    }
    
    /**
     * Return a reference to the document scope.
     * @return
     */
    public BrowserScope getScope() {
        return scope;
    }
    
    /**
     * Set the scope of this context
     * @param scope
     */
    protected void setScope(BrowserScope scope) {
        this.scope = scope;
    }
    
    /**
     * Return the sessionContext
     * @return
     */
    protected SessionContext getSessionContext() {
        return this.sessionContext;
    }
    
    /**
     * Gets the eventList. The contents of the event list can be modified.
     * @return
     */
    public CatchList getEventList() {
        return eventList;
    }
    
    
    public PropertyList getPropertyList() {
        return this.propertyList;
    }
    
    public FilledList getFilledList() {
        return filledList;
    }
}
