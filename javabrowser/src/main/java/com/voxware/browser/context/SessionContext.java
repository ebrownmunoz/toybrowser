/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.context;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

import com.voxware.browser.audioplayer.PromptResolver;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.interrupt.InterruptManager;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.log.LogManager;

/**
 * This creates a Session along with scope. The W3C VoiceXML documentation
 * says the data in immutable. This will contain user information
 * and system configuration.
 * 
 * Note: This starts a Rhino Session. You should close this with the "exit" method
 * when you are done.
 */
public class SessionContext {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    private final Context context;
    private final BrowserScope scope; // This is sealed and unchangeable
    private final IoManager ioManager;
    private final DocumentServer documentServer;
    private final CatchList sessionEvent = new CatchList();    
    private final InterruptManager interruptManager;    
    private final LogManager logmanager = new LogManager(null);
    private final PromptResolver promptResolver;
    private final PropertyList defaultProperties;
    private final LinkedHashMap<Object, Object> objectCache = new LinkedHashMap<Object, Object>() {
        /* 
         * (non-Javadoc)
         * @see java.util.LinkedHashMap#removeEldestEntry(java.util.Map.Entry)
         */
        @Override
        protected boolean removeEldestEntry(Entry<Object, Object> eldest) {
            return size() > 100;
        }
    };

    
    /**
     * 
     * Constructs a new <code>SessionContext</code> instance. As per the VoiceXML
     * specification, the resulting scope is sealed. This starts a new 
     * Context.
     * 
     * @param configuration
     */
    public SessionContext(Map<String, Object> configuration, IoManager iomanager, DocumentServer documentServer) {
       this(null, configuration, null, iomanager, documentServer);
    }

    /**
     * Constructs a new <code>SessionContext</code> instance.
     * @param context
     * @param configuration
     * @param iomanager
     * @param documentServer
     */
    public SessionContext(Context context, Map<String, Object> configuration, IoManager iomanager, DocumentServer documentServer) {
        this(context, configuration, null, iomanager, documentServer);
    }
    /**
     * 
     * Constructs a new <code>SessionContext</code> instance. As per the VoiceXML
     * specification, the resulting scope is sealed.
     * 
     * Note: values in configuration must be something that is allowed in Scriptable.put()
     * (hint: instead of Map, use NativeObject-- see JvmBrowser).
     * 
     * @param context
     * @param configuration
     */
    public SessionContext(Context context, Map<String, Object> configuration, PropertyList defaultProperties, IoManager iomanager, DocumentServer documentServer) {
        if (context == null ) {
            ContextFactory cf = createContextFactory();
            this.context = cf.enterContext();
        } else {
            this.context = context;
        }
        this.ioManager = iomanager;
        this.documentServer = documentServer;
        this.interruptManager = new InterruptManager();
        this.defaultProperties = defaultProperties;
        this.promptResolver = new PromptResolver();

        // created the sealed root with standard js objects
        Scriptable sealedRoot = this.context.initStandardObjects(null, true);
        // create the session object
        BrowserScope object = new BrowserScope("session");
        object.setParentScope(sealedRoot);
        
        if (configuration != null) {
            for (String name : configuration.keySet()) {
                Object value = configuration.get(name);
                ScriptableObject.putProperty(object, name, Context.javaToJS(value, object));
            }
        }
        
        object.put("session", object, object); // self reference
        object.sealObject();
        this.scope = object;
    }
    
    /**
     * Construct a context factory with set features. This
     * turns on STRICT_VARS and WARNING_AS_ERROR
     * @return
     */
    public static final  ContextFactory createContextFactory() {
        return new ContextFactory() {
            @Override
            public boolean hasFeature(Context cx, int feature) {
                if (feature == Context.FEATURE_STRICT_VARS ||
                    feature == Context.FEATURE_WARNING_AS_ERROR ||
                    feature == Context.FEATURE_TO_STRING_AS_SOURCE) {
                    return true;
                } else {
                    return super.hasFeature(cx, feature);
                }
            }

            /* 
             * (non-Javadoc)
             * @see org.mozilla.javascript.ContextFactory#makeContext()
             */
            @Override
            protected Context makeContext() {
                Context ctx =  super.makeContext();
                ctx.setOptimizationLevel(-1);
                return ctx;
            }
        };
        
    }

    /**
     * Evaluate a Javascript string and return a result string.
     * 
     * @param javascript
     * @return
     */
    public String evaluateToString(String javascript) {
        Object value =  context.evaluateString(scope, javascript, "Context", 
            1, null);
        
        return value.toString();
    }
    
    /**
     * Return a property from the Session scope.
     * 
     * @param name
     * @return
     */
    public Object getProperty(String name) {
        return ScriptableObject.getProperty(this.scope, name);
    }

    /**
     * Create a new Application Context
     * 
     * @param uri
     * @return
     */
    public ApplicationContext createApplicationContext(String uri) {
        return new ApplicationContext(uri, this, new CatchList(sessionEvent), new PropertyList(defaultProperties));
    }
    
    /**
     * Create a new Document Context
     * 
     * @param uri
     * @return
     */
    public DocumentContext createDocumentContext(ApplicationContext appContext) {
        if (appContext != null) {
            return new DocumentContext(this, appContext, 
                new CatchList(appContext.getEventList()),
                new PropertyList(appContext.getPropertyList()));
        } else {
            return new DocumentContext(this, "document", new CatchList(sessionEvent), new PropertyList(defaultProperties));
        }
    }
    
    /**
     * Return the Rhino Context.
     * TODO: this is not strictly typesafe
     * @return
     */
    public Context getContext() {
        return context;
    }
    
    /**
     * Return the Application Scope
     */
    public BrowserScope getScope() {
        return scope;
    }
    
    /**
     * Returns the ioManager.
     * @return the ioManager.
     */
    public IoManager getIoManager() {
        return ioManager;
    }
    
    /**
     * Returns the document server for this session
     * @return
     */
    public DocumentServer getDocumentServer() {
        return documentServer;
    }

    /**
     * Exits the Rhino session.
     * 
     * @throws IOException
     */
    public void exit() throws IOException {
        Context.exit();
    }

    /**
     * @return the interruptManager
     */
    public InterruptManager getInterruptManager() {
        return interruptManager;
    }
    
    /**
     * @return the LogManager
     */
    
   public LogManager getLogManager() {
       return this.logmanager;
   }
   
   /**
    * Gets the object cache. This is scoped to an application.
    * @return the object cache
    */
   public Map<Object, Object> getCache() {
       return objectCache;
   }
   
   public PromptResolver getPromptResolver() {
       return promptResolver;
   }
}


