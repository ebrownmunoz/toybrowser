/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

/**
 * A class for form item variables that are not set in the javascript scope. The w3c spec refers to these as "inaccessible".
 */
public class FormItemUnnamedVariable implements FormItemVariable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private boolean complete = false;
    private Object value = null;

    
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#getName()
     */
    @Override
    public String getName() {
        return "anonymous";
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    /**
     * Set complete
     * @param complete
     * @return
     */
    public void setComplete(Object value) {
        this.complete = true;
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#clear()
     */
    public void clear() {
        this.complete = false;
        this.value = null;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#getValue()
     */
    public Object getValue() {
        // TODO Auto-generated method stub
        return value;
    }

}
