/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import com.voxware.browser.BrowserException;
import com.voxware.browser.model.NodeLocation;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: May 17, 2015 by: eric</li>
 * </ul>
 */
public class InterpreterException extends BrowserException {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>InterpreterException</code> instance.
     * @param uri 
     * @param lineNumber 
     */
    public InterpreterException(NodeLocation nodeLocation) {
        super(nodeLocation);
        
    }

    /**
     * Constructs a new <code>InterpreterException</code> instance.
     * @param description
     */
    public InterpreterException(String message, NodeLocation nodeLocation) {
        super(message, nodeLocation);    
    }

    /**
     * Constructs a new <code>InterpreterException</code> instance.
     * @param cause
     */
    public InterpreterException(Throwable cause, NodeLocation nodeLocation) {
        super(cause, nodeLocation);
    }

    /**
     * Constructs a new <code>InterpreterException</code> instance.
     * @param description
     * @param cause
     */
    public InterpreterException(String message, Throwable cause, NodeLocation nodeLocation) {
        super(message, cause, nodeLocation);
    }

}
