/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import com.voxware.browser.context.property.PropertyList;

/**
 * The AnomymousContext for interpreting
 * blocks, filled etc.
 */
public class AnonymousContext extends BaseContext implements ExecutableContext {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final FormContext formContext;
    
    
    /**
     * Constructs a new <code>AnonymousContext</code> instance.
     * @param formContext
     * @param formScope
     * @param eventList
     */
    public AnonymousContext(FormContext formContext) {
        super(formContext.getSessionContext(), formContext.getScope(), null, formContext.getEventList(), new PropertyList(formContext.getPropertyList()));
        this.formContext = formContext;
    }
    
    /**
     * @return the formContext
     */
    public FormContext getFormContext() {
        return this.formContext;
    }
}
