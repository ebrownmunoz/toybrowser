/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Jun 4, 2015 by: eric</li>
 * </ul>
 */
public class FormItemNamedVariable implements FormItemVariable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private final FormContext formContext;
    private final String name;

    /**
     * Constructs a new <code>FormItemContextVariable</code> instance.
     */
    public FormItemNamedVariable(FormContext context, String name) {
        this.formContext = context;
        this.name = name;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#isComplete()
     */
    public boolean isComplete() {
        Scriptable scope = formContext.getScope();
        Object variable = scope.get(name, scope);
        
        if ((variable == Scriptable.NOT_FOUND) || (variable.equals(Context.getUndefinedValue()))) {
            return false;
        } else {
            return true;
        }
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#setComplete(boolean)
     */
    public void setComplete(Object value) {
        Scriptable scope = formContext.getScope();
        scope.put(name, scope, value);
    }
    
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#clear()
     */
    public void clear() {
        Scriptable scope = formContext.getScope();
        scope.put(name, scope, Context.getUndefinedValue());
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.context.FormItemVariable#getValue()
     */
    public Object getValue() {
        Scriptable scope = formContext.getScope();
        return scope.get(name, scope);
    }

}
