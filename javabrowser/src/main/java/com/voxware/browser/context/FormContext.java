/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.Param;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.model.form.InputItem;

/**
 * The Context for Interpreting a Form. For this implementation FormContext is what would normally be called
 * DialogContext since we are not implementing menus.
 */
public class FormContext extends BaseContext implements ExecutableContext {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String NAME = "name";

    private static final String EXPR = "expr";

    private Map<FormItem, FormItemVariable> formItemVariables = new HashMap<FormItem, FormItemVariable>();
    private Map<String, InputItem> namedInputItems = new HashMap<String, InputItem>();
    /**
     * Maps input items to an integer indicating how many times the input item has been visited for prompting purposes
     */
    private Map<InputItem, Integer> promptCounters = new HashMap<InputItem, Integer>();
    /**
     * Maps input items to a boolean indicating if prompt selection should be performed
     */
    private Map<InputItem, Boolean> promptSelection = new HashMap<InputItem, Boolean>();

    /**
     * Holds the event counters for forms and menus (which are specialized forms)
     */
    private Map<String, Integer> formEventCounters = new HashMap<String, Integer>();
    private Map<FormItem, Map<String, Integer>> formItemEventCounters = new HashMap<FormItem, Map<String, Integer>>();
    
    private byte[] voiFile;

    /**
     * Constructs a new <code>FormContext</code> instance.
     * 
     * @param sessionContext
     * @param documentScope
     */
    public FormContext(SessionContext sessionContext, DocumentContext documentContext, CatchList eventlist,
        PropertyList propertyList) {
        super(sessionContext, documentContext.getScope(), "dialog", eventlist, propertyList);
        this.voiFile = documentContext.getVoiFile(); // initialize to document
    }

    /**
     * Used when this form is called by a <subdialog>. The params that were specified are passed in. The params are
     * passed in as a Map so that each param's evaluated value can be passed in as well.
     * 
     * @param params
     * @throws InterpreterException
     */
    public void applyParams(Map<Param, Object> params) throws InterpreterException {

        if (params == null) {
            return;
        }

        // ARGGG! The W3C spec specifically says it is an error to pass in a parameter
        // that isn't defined in the callee scope. Yet we do exactly this.
        // So we will just ignore it.

        //        /*
        //         * First check that params already exist in scope.
        //         */
        //        for (Param each : params.keySet()) {
        //            if (!getScope().has(each.getName(), getScope())) {
        //                throw new InterpreterException("Param " + each.getName() + " is not declared.", each.getNodeLocation());
        //            }
        //        }

        /*
         * Now set each value in this scope
         */
        for (Map.Entry<Param, Object> each : params.entrySet()) {
            // each.execute(getSessionContext(), this, getEventList());
            // expands to
            // setValue(getSessionContext().getContext(), this.getScope(), name, expr)
            // expands to
            // if (expr != null) {
            // value = rhino.evaluateString(scope, expr, name, 0, null);
            // } else {
            // value = Context.getUndefinedValue();
            // }
            //
            // scope.put(name, scope, value);

            BrowserScope scope = getScope();

            // If it isn't defined, just ignore it (see above)
            if (scope.has(each.getKey().getName(), scope)) {
                scope.put(each.getKey().getName(), scope, each.getValue());
            }
        }
    }

    /**
     * Adds all form item variables from a form to this context
     * 
     * @param form the form
     */
    public void addFormItemVariablesFromForm(Form form) { // Add form item variables
        for (FormItem item : form.getFormItems()) {
            NodeLocation nodeLocation = item.getNodeLocation();
            Object value = null;
            String name = item.getName();
            String expr = item.getExpr();
            if (expr != null) {
                value = getSessionContext().getContext().evaluateString(this.getScope(), expr,
                    "expr attribute of " + nodeLocation.getUri().toString(), nodeLocation.getLineNumber(), null);
            }
            if (item.isScoped()) {
                this.addNamedFormItemVariable(item, name, value);
            } else {
                this.addUnnamedFormItemVariable(item, value);
            }
            if (item instanceof InputItem) {
                promptCounters.put((InputItem) item, 1);
                promptSelection.put((InputItem)item, true);
            }
            formItemEventCounters.put(item, new HashMap<String, Integer>());
        }
    }

    /**
     * Adds an unnamed form item to this context
     * 
     * @param formItem the form item
     * @param value
     */
    public void addUnnamedFormItemVariable(FormItem formItem, Object value) {
        FormItemUnnamedVariable formItemVariable = new FormItemUnnamedVariable();
        if (value != null) {
            formItemVariable.setComplete(value);
        }
        formItemVariables.put(formItem, formItemVariable);
    }

    /**
     * @param fi
     * @param name
     */
    public void addNamedFormItemVariable(FormItem fi, String name, Object value) {
        FormItemNamedVariable fiv = new FormItemNamedVariable(this, name);
        if (value != null) {
            fiv.setComplete(value);
        } else {
            // all named form item variables exist in the scope as undefined
            Scriptable scope = getScope();
            scope.put(name, getScope(), Context.getUndefinedValue());
        }
        formItemVariables.put(fi, fiv);
        if (fi instanceof InputItem) {
            namedInputItems.put(name, (InputItem) fi);
        }
    }

    /**
     * Clear the form item variable for the given form item.
     * 
     * @param fi
     */
    public void clearFormItemVariable(FormItem fi) {
        formItemVariables.get(fi).clear();
        Map<String, Integer> eventCounters = formItemEventCounters.get(fi);
        if (eventCounters != null) {
            eventCounters.clear();
        }
        if (fi instanceof InputItem) {
            promptCounters.put((InputItem)fi, 1);
            promptSelection.put((InputItem)fi, true);
        }
    }

    /**
     * Check if the given form Item is complete @param formItem
     * 
     * @return
     * @throws InterpreterException
     */
    public boolean isComplete(FormItem formItem) throws InterpreterException {
        FormItemVariable fiv = formItemVariables.get(formItem);
        if (fiv == null) {
            throw new InterpreterException("Unknown form Item", formItem.getNodeLocation());
        }

        return fiv.isComplete();
    }

    /**
     * Finds an input item with the given name
     * 
     * @param inputItem the input item name
     * @return a FormItem, or null if on does not exist
     */
    public FormItem findInputItem(String inputItem) {
        return namedInputItems.get(inputItem);
    }

    /**
     * Set the Form Item to be complete or clear form Item
     * if value is null.
     * 
     * @param formItem
     * @throws InterpreterException
     */
    public void setFormItemValue(FormItem formItem, Object value) throws InterpreterException {
        
        FormItemVariable fiv = formItemVariables.get(formItem);
        if (fiv == null) {
            throw new InterpreterException("Unknown form Item", formItem.getNodeLocation());
        }
        if (value == null) {
        	fiv.clear();
        } else {
        	fiv.setComplete(value);
        }
    }

    /**
     * @return
     */
    public List<FormItemVariable> getFormItemVariablesList() {
        return new ArrayList<FormItemVariable>(formItemVariables.values());
    }

    /**
     * Set the voiFile
     * 
     * @param voiFile
     */
    public void setVoiFile(byte[] voiFile) {
        this.voiFile = voiFile;
    }

    /**
     * Returns the input Item with that name
     * 
     * @param name
     * @return
     */
    public FormItem getInputItem(String name) {
        return namedInputItems.get(name);
    }

    /**
     * Returns the counter for an input item. Returns null if the input item is not found.
     * 
     * @param item the input item (field, initial, etc...)
     * @return a counter
     */
    public Integer getCounter(FormItem item) {
        if (promptCounters.containsKey(item)) {
            return promptCounters.get(item);
        } else {
            throw new IllegalArgumentException("Error, input item " + item + " is not tracked");
        }
    }

    /**
     * Increments the counter for an input item. Throws an {@link IllegalArgumentException} if this form context does
     * not manage the input item.
     * 
     * @param item the input item
     */
    public void incrementCounter(InputItem item) {
        if (promptCounters.containsKey(item)) {
            Integer counter = promptCounters.get(item);
            if (counter != null) {
                counter++;
                promptCounters.put(item, counter);
            } else {
                // counter should never be null
                log.warn("Warning, item " + item + " is tracked, but had a null counter, assuming it was 1");
                promptCounters.put(item, 2);
            }
        } else {
            throw new IllegalArgumentException("Error, input item " + item + " is not tracked");
        }

    }

    public void setShouldPerformPromptSelection(InputItem item, boolean shouldPerformPromptSelection) {
        if (promptSelection.containsKey(item)) {
            promptSelection.put(item, shouldPerformPromptSelection);
        }
    }
    
    public boolean getShouldPerformPromptSelection(FormItem item) {
        if (promptSelection.containsKey(item)) {
            // use the actual stored value to be compliant with W3C, return true for backwards compatibility
            return true;
            //return promptSelection.get(item);
        } else {
            throw new IllegalArgumentException("Error, input item " + item + " is not tracked");
        }
    }
    
    /**
     * Gets the form level event counter for the given event.
     * @param event the event
     * @return a positive integer >= 1
     */
    public int getEventCounter(String event) {
        if (formEventCounters.containsKey(event)) {
            return formEventCounters.get(event);
        } else {
            return 1;
        }
    }
    
    /**
     * Increments the form level event counter for the given event, and all parent events
     * @param event the event
     */
    public void incrementEventCounter(String event) {
        String[] segments = event.split("\\.");
        String eventName = "";
        for (String segment : segments) {
            eventName = eventName + segment;
            if (formEventCounters.containsKey(eventName)) {
                int count = formEventCounters.get(eventName);
                formEventCounters.put(eventName, count + 1);
            } else {
                formEventCounters.put(eventName, 2);
            }
        }
    }
    /**
     * Gets the event counter for a form item.
     * @param formItem the form item
     * @param event the event name
     * @return an int
     */
    public int getEventCounter(FormItem formItem, String event) {
        if (formItemEventCounters.containsKey(formItem)) {
            Map<String, Integer> eventCounters = formItemEventCounters.get(formItem);
            Integer counter = eventCounters.get(event);
            if (counter == null) {
                return 1;
            } else {
                return counter;
            }
        } else {
            throw new IllegalArgumentException("Error, input item " + formItem + " is not tracked");
        }
    }
    
    /**
     * Increments the event counters for a form item. For instance, the event a.b.c would get a.b.c, a.b, and a incremented by one for the given formItem.
     * @param formItem the form item
     * @param event the event
     */
    public void incrementEventCounter(FormItem formItem, String event) {
        if (formItemEventCounters.containsKey(formItem)) {
            Map<String, Integer> eventCounters = formItemEventCounters.get(formItem);
            String[] segments = event.split("\\.");
            String eventName = "";
            for (String segment : segments) {
                eventName = (eventName.isEmpty() ? "" : (eventName + ".")) + segment;
                if (eventCounters.containsKey(eventName)) {
                    int count = eventCounters.get(eventName);
                    eventCounters.put(eventName, count + 1);
                } else {
                    eventCounters.put(eventName, 2);
                }
            }
        } else {
            throw new IllegalArgumentException("Error, input item " + formItem + " is not tracked");
        }
    }
}
