/**
 * 
 */
package com.voxware.browser.context;

import com.voxware.browser.interpreter.Result;

/**
 * @author eric
 *
 */
public class BrowserEvent extends Throwable {

	private final String event;
	private final String message;
	
	public BrowserEvent(String event, String message) {
		this.event = event;
		this.message = message;
		
	}
	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Return an event Result
	 * @return
	 */
	public Result result() {
		return Result.event(event, message);
	}

	
	
}
