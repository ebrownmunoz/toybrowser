package com.voxware.browser.context.property;

import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeJSON;
import org.mozilla.javascript.json.JsonParser;
import org.mozilla.javascript.json.JsonParser.ParseException;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.AbstractVxmlNode;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;

/**
 * Represents a property. This is immutable (by design)
 */
public class Property extends AbstractVxmlNode implements Initializer {
    
    private final String name;
    private final String expr;
    private final String value;

    /**
     * Constructs a property
     * @param attributes 
     * @param nodeLocation 
     */
    public Property(Map<String, String> attributes, NodeLocation nodeLocation) {
        super(attributes, nodeLocation);
        this.name = attributes.get(NAME);
        this.expr = attributes.get(EXPR);
        this.value = attributes.get(VALUE);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the expr
     */
    public String getExpr() {
        return expr;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    @Override
    public Result initialize(SessionContext scontext, BaseContext context) {
        context.getPropertyList().put(name, this);
        return Result.Ok();
    }
    
    public Object evaluate(BaseContext context) {
        String value = getValue();
        if (value != null) {
            if (value.isEmpty() || value.trim().isEmpty()) {
                return value;
            } else if (value.trim().startsWith("{") || value.trim().startsWith("[")) {
                // maybe it is JSON?
                Context ctx = Context.enter();
                try {
                    JsonParser jsonParser = new JsonParser(ctx, context.getScope());
                    return jsonParser.parseValue(value);
                } catch (ParseException e) {
                    return value;
                } finally {
                    Context.exit();
                }
            } else if ("null".equals(value)) {
                return null;
            } else if ("undefined".equals(value)) {
                return Context.getUndefinedValue();
            } else if ("true".equalsIgnoreCase(value)) {
                return Boolean.TRUE;
            } else if ("false".equalsIgnoreCase(value)) {
                return Boolean.FALSE;
            } else if ("NaN".equals(value)) {
                return Double.NaN;
            } else if ("Infinity".equals(value) || "+Infinity".equals(value)) {
                return Double.POSITIVE_INFINITY;
            } else if ("-Infinity".equals(value)) {
                return Double.NEGATIVE_INFINITY;
            } else {
                try {
                    // see if it is fixed point type first
                    return Long.valueOf(value);
                } catch (NumberFormatException e) {
                    // maybe a floating point?
                    double val = Context.toNumber(value);
                    // we already checked for the NaN earlier, so a NaN here means it's not really a JS numeric literal
                    if (Double.isNaN(val)) {
                        return value;
                    } else {
                        return val;
                    }
                }
            }
        } else {
            return context.evaluate(getExpr());
        }
    }

    @Override
    public void validate(ValidationContext context) throws ValidationException {
        if (name == null) {
            throw new ValidationException("name is null", getNodeLocation());
        }
        if (!context.getValidationUtils().exactlyOneNonNull(expr, value)) {
            throw new ValidationException("one and only one of expr or value attribute should be defined", getNodeLocation());
        }
    }
}
