/**
 * 
 */
package com.voxware.browser.context.property;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.Context;

import com.voxware.browser.context.BaseContext;

/**
 * Holds the properties within a context
 */
public class PropertyList {
    private final HashMap<String, Property> properties = new HashMap<String, Property>();

    /**
     * Construct a property List with the given parent. All of the properties are copied into the parent.
     * 
     * @param parent
     */
    public PropertyList(PropertyList parent) {
        if (parent != null) {
            properties.putAll(parent.getAll());
        }
    }

    /**
     * Construct a property list with no parent
     */
    public PropertyList() {
        this(null);
    }

    /**
     * Get a specific property
     * 
     * @param key
     * @return
     */
    public Property get(String key) {
        return properties.get(key);
    }

    /**
     * Put a property
     * 
     * @return
     */
    public void put(String key, Property value) {
        properties.put(key, value);

    }

    /**
     * Return all of the properties (type safe)
     * 
     * @return
     */
    public Map<String, Property> getAll() {
        return Collections.unmodifiableMap(properties);
    }

    /**
     * Put all of the properties in a list into the property map
     * 
     * @param propList
     */
    public void putAll(List<Property> propList) {
        for (Property each : propList) {
            properties.put(each.getName(), each);
        }
    }

    /**
     * Evaluates a property. Throws {@link PropertyNotFoundException} if the property is not found.
     * @param context the context to evaluate expressions against, unused if value attribute was used
     * @param key the name of the property
     * @return a value
     * @throws PropertyNotFoundException if the property is not declared
     */
    public Object evaluate(BaseContext context, String key) throws PropertyNotFoundException {
        Property property = properties.get(key);
        if (property == null) {
            throw new PropertyNotFoundException("Property " + key + " not declared");
        } else {
            return property.evaluate(context);
        }
    }
    
    /**
     * Evaluates a property. Returns <code>missingPropertyValue</code> if the property is not found.
     * @param context the context to evaluate expressions against, unused if value attribute was used
     * @param key the name of the property
     * @param missingPropertyValue the value to return if the property does not exist
     * @return a value
     */
    public Object evaluate(BaseContext context, String key, Object missingPropertyValue) {
        try {
            return evaluate(context, key);
        } catch (PropertyNotFoundException e) {
            return missingPropertyValue;
        }
    }   
}
