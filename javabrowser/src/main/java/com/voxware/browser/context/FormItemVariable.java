/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: May 17, 2015 by: eric</li>
 * </ul>
 */
public interface FormItemVariable {
    /**
     * Gets the name of the form item variable.
     * @return the name of the form item variable
     */
    public String getName();
    /**
     * return true if the Form Item corresponding to this variable is
     * complete.
     * @return
     */
    public boolean isComplete();

    /**
     * Set the form item variable as complete. 
     * @param b
     */
    public void setComplete(Object value);
    
    /**
     * Delete the current valariable (so the form item will be visited again
     */
    public void clear();
    
    /**
     * return the value
     * @return
     */
    public Object getValue();
    
}
