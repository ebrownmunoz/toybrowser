/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */

package com.voxware.browser.context;

import org.mozilla.javascript.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.event.CatchList;

/**
 * Represents a document context.
 */
public class DocumentContext extends BaseContext {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "Â© Copyright Voxware Inc. 2015.";
    
    private static Logger LOG = LoggerFactory.getLogger(DocumentContext.class);

    private ApplicationContext applicationContext;
    
    private byte[] voiFile;
           
    /**
     * Constructs a new <code>DocumentContext</code> instance.
     * @param name
     * @param sessionContext
     * @param sessionScope
     */
    protected DocumentContext(SessionContext sessionContext, String name, CatchList eventlist, PropertyList propertyList) {
        super(sessionContext, sessionContext.getScope(), name, eventlist, propertyList);
    }
    
    /**
     * Constructs a new <code>DocumentContext</code> instance.
     * @param name
     * @param sessionContext
     * @param sessionScope
     */
    protected DocumentContext(SessionContext sessionContext, ApplicationContext appContext,
        CatchList eventlist, PropertyList propertyList) {
        super(sessionContext, appContext.getScope(), "document", eventlist, propertyList);
        this.voiFile = appContext.getVoiFile();
        
        BrowserScope scope = getScope();
        scope.put("backflag", scope, Context.getUndefinedValue());
        scope.put("selectedcontainerstring", scope, Context.getUndefinedValue());
        scope.put("skiplflag", scope, Context.getUndefinedValue());
        scope.put("skipaflag", scope, Context.getUndefinedValue());
        scope.put("mylocprompt", scope, Context.getUndefinedValue());
        scope.put("specialOperation", scope, Context.getUndefinedValue());
        scope.put("operationRequest", scope, Context.getUndefinedValue());
        scope.put("zoneId", scope, Context.getUndefinedValue());
        scope.put("chkdigit", scope, Context.getUndefinedValue());
        scope.put("assignprompt", scope, Context.getUndefinedValue());

        scope.put("palletId",scope, Context.getUndefinedValue());
        scope.put("productId",scope, Context.getUndefinedValue());
        scope.put("locOverride",scope, Context.getUndefinedValue());
        scope.put("stagingId",scope, Context.getUndefinedValue());
        scope.put("workTypeID",scope, Context.getUndefinedValue());
        scope.put("workTypeDesc",scope, Context.getUndefinedValue());
        
        scope.put("equipmentId",scope, Context.getUndefinedValue());
        scope.put("assignmentId",scope, Context.getUndefinedValue());
 
        scope.put("mileage",scope, Context.getUndefinedValue());
        
        scope.put("trailerId",scope, Context.getUndefinedValue());
        scope.put("confirmAll",scope, Context.getUndefinedValue());
        
        scope.put("postext",scope, Context.getUndefinedValue());
        scope.put("posid",scope, Context.getUndefinedValue());
        scope.put("reasondesc",scope, Context.getUndefinedValue());
        scope.put("repquantity",scope, Context.getUndefinedValue());
        
        scope.put("errmsg",scope, Context.getUndefinedValue());
        
        scope.put("laneId", scope, Context.getUndefinedValue());
        
        
        
    }

    /**
     * Set the voi file;
     * @return
     */
    public byte[] getVoiFile() {
        return this.voiFile;
    }
    
    /**
     * Return the voiFile
     * @param voiFile
     */
    public void setVoiFile(byte[] voiFile) {
        this.voiFile = voiFile;
    }
}
