/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.mozilla.javascript.Context;

import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.event.CatchList;

/**
 * Define the Context of an VoiceXML application.
 */
public class ApplicationContext extends DocumentContext {
    
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ApplicationContext</code> instance.
     * @param name
     * @param sessionContext
     * @param sessionScope
     */
    protected ApplicationContext(String name, SessionContext sessionContext, 
        CatchList eventlist, PropertyList propertyList) {
        super (sessionContext, "application", eventlist, propertyList);
        // A hack because NGV sets to this value in a <script> (and the original browser accepts it)
        getScope().put("promptstring", getScope(), Context.getUndefinedValue());
        getScope().put("id", getScope(), Context.getUndefinedValue());
        getScope().put("newid", getScope(), Context.getUndefinedValue());
        getScope().put("formatLocation", getScope(), Context.getUndefinedValue());
        getScope().put("locationId",getScope(), Context.getUndefinedValue());
        getScope().put("checkId",getScope(), Context.getUndefinedValue());
        getScope().put("zoneDesc",getScope(), Context.getUndefinedValue());
        getScope().put("taskStartTime",getScope(), Context.getUndefinedValue());
        getScope().put("pick",getScope(), Context.getUndefinedValue()); // Jira 4332 (weird case when root.js not run)
        
        getScope().put("pickedQty",getScope(), Context.getUndefinedValue());
        getScope().put("state_id",getScope(), Context.getUndefinedValue());
        getScope().put("startdiscon",getScope(), Context.getUndefinedValue());
    }
}
