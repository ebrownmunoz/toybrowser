/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context.scope;

import java.util.Arrays;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeJSON;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.ScriptRuntime;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.TopLevel;

import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.model.NodeLocation;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: Jun 18, 2015 by: eric</li>
 * </ul>
 */
public class BrowserScope extends NativeObject {
	/**
	 * Legal copyright notice.
	 */
	public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

	/*
	 * This string will work as a variable (i.e. if the scopeName is "document" child scopes will be able to refer to
	 * variable "x" in this scope as "document.x")
	 */
	private final String scopeName;

	public BrowserScope(String scopeName) {
		this.scopeName = scopeName;
		if ((scopeName != null) && (scopeName.length() > 0)) {
			put(scopeName, this, this);
		}

		Context ctx = Context.enter();
		ScriptRuntime.setBuiltinProtoAndParent(this, ctx.initStandardObjects(null),
				TopLevel.Builtins.Object);
		Context.exit();
	}

	/**
	 *  Get, this looks for the value up the ancestor chain.
	 */
	public Object getRecursive(String name) {
		if (name.equals(this.scopeName)) {
			return this;
		} else {
			Scriptable ancestor = findAncestorWithVariable(name);
			if (ancestor != null) {
				return ancestor.get(name, ancestor);
			} else {
				return Scriptable.NOT_FOUND;
			}
		}
	}

	/**
	 * This is for the case such as <assign name="foo.bar.betch" expr="1"/>. In this
	 * case both foo, and bar need to exist. call getRecursiveScope (["foo", 'bar] , nodeLocation) for this.
	 *
	 * 
	 * @param path
	 * @param value
	 * @throws InterpreterException 
	 */
	public Scriptable getRecursiveScope (String[] parts, NodeLocation nodeLocation) throws InterpreterException {
		return getRecursiveScope(this, parts, nodeLocation);
	}

	public static Scriptable getRecursiveScope (Scriptable start, String[] parts, NodeLocation nodeLocation) throws InterpreterException {

		String varName = parts[0];
		Object scope = start.get(varName, start);


		if (scope == null) {
			throw new InterpreterException("Non-existant scope path", nodeLocation); 
		} else if (!(scope instanceof Scriptable)) {
			throw new InterpreterException("Trying to set property of non-object", nodeLocation);
		}

		if (parts.length > 1) {

			return getRecursiveScope((Scriptable)scope, Arrays.copyOfRange(parts, 1, parts.length), nodeLocation);
		} else {
			return (Scriptable)scope;
		}
	}


	/**
	 * @return the scope name
	 */
	public String getScopeName() {
		return this.scopeName;
	}

	/**
	 * Find the ancestor scope that has the given variable or null if not found.
	 * 
	 * @param variableName
	 * @return
	 */
	public Scriptable findAncestorWithVariable(String variableName) {

		Scriptable bscope = this;

		while ((bscope != null)) {
			if (bscope.has(variableName, bscope)) {
				return bscope;
			}
			bscope = bscope.getParentScope();
		}

		return null;
	}

	/**
	 * Find scope by the given Scope name
	 * 
	 * @param scopeName
	 * @return
	 */
	public Scriptable findAncestorByScopeName(String scopeName) {

		BrowserScope bscope = this;

		while ((bscope.getScopeName() == null) || // Keeps going as long as bscope has a name that is not null
				(!bscope.getScopeName().equals(scopeName))) { // and equal to scopeName
			Object parent = bscope.getParentScope();
			if (!(parent instanceof BrowserScope)) {
				bscope = null;
				break; // this allows us to not check for bscope == null in the while condition
			}
			bscope = (BrowserScope) parent;
		}

		return bscope;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		Context ctx = Context.enter();
		try {
			boolean first = true;
			Object[] ids = getIds();
			for (Object id : ids) {
				if (!first) {
					sb.append(", ");    
				}
				sb.append("\"").append(id).append("\" : ");
				sb.append(Context.toString(NativeJSON.stringify(ctx, this, get(id), null, null)));
				first = false;
			}
			sb.append("}");
			return sb.toString();
		} finally {
			Context.exit();
		}
	}
}
