/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser;

import java.net.URI;

import com.voxware.browser.model.NodeLocation;

/**
 * BrowserException is the base class of exceptions thrown by the browser that isn't already covered by a builtin Java
 * exception.
 *
 * @author edh
 */
public class BrowserException extends Exception {
    /**
     * The serialVersionUID.
     */
    private static final long serialVersionUID = -4456439199978359213L;
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    private final NodeLocation nodeLocation;

    /**
     * Constructs a new <code>BrowserException</code> instance.
     * 
     * @param nodeLocation the node location
     */
    public BrowserException(NodeLocation nodeLocation) {
        this.nodeLocation = nodeLocation;
    }

    /**
     * Constructs a new <code>BrowserException</code> instance.
     * 
     * @param description the description
     * @param nodeLocation the node location
     */
    public BrowserException(String message, NodeLocation nodeLocation) {
        super(message);
        this.nodeLocation = nodeLocation;
    }

    /**
     * Constructs a new <code>BrowserException</code> instance.
     * 
     * @param cause the underlying cause
     * @param nodeLocation the node location
     */
    public BrowserException(Throwable cause, NodeLocation nodeLocation) {
        super(cause);
        this.nodeLocation = nodeLocation;
    }

    /**
     * @param description error description
     * @param cause underlying cause
     * @param nodeLocation the node location
     */
    public BrowserException(String message, Throwable cause, NodeLocation nodeLocation) {
        super(message, cause);
        this.nodeLocation = nodeLocation;
    }

    /**
     * return the VXML filename
     * 
     * @return a URI
     */
    public URI getURI() {
        return nodeLocation.getUri();
    }

    /**
     * Return the VXML line number
     * 
     * @return an int
     */
    public int getLineNumber() {
        return nodeLocation.getLineNumber();
    }

    /**
     * Return the VXML column number
     * 
     * @return an int
     */
    public int getColumnNumber() {
        return nodeLocation.getColumnNumber();
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        String locationDescription = (nodeLocation != null) ? nodeLocation.getLocationDescription() : "NA";
        return (message != null) ? (s + "@[" + locationDescription + "]: " + message)
            : s + "@[" + locationDescription + "]";
    }
}
