/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.interrupt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.InterruptLinkNode;
import com.voxware.browser.model.InterruptNode;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: Aug 24, 2015 by: eric</li>
 * </ul>
 */
public class InterruptManager {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    
    public static final Interrupt EXIT_INTERRUPT = new Interrupt(null, null);
    private static final Logger log = LoggerFactory.getLogger(InterruptManager.class);
    
    
    private Map<String, InterruptNode> activeInterrupts = new HashMap<String, InterruptNode>();
    private final BlockingQueue<Interrupt> interruptQueue = new LinkedBlockingQueue<Interrupt>();
    private final Set<Interrupt> ioInterrupts = new HashSet<Interrupt>();
    private Thread threadToInterrupt;
    private InterruptThread thread;
    private RecognitionResult resultInterrupt;
    private boolean exitting = false;

    public InterruptManager() {
    }

    /**
     * Starts processing of pending interrupts. Only hard interrupts are delivered when a modal field is active.
     * @param modal true if the current field is modal
     */
    public void recognizerStart(boolean modal) {
        recognizerStart(modal, Thread.currentThread());
    }
    
    public synchronized void recognizerStart(boolean modal, Thread threadToInterrupt) {
        this.threadToInterrupt = threadToInterrupt;
        if (exitting) {
            threadToInterrupt.interrupt();
            return;
        }
        if (thread != null) {
            log.warn("Old InterruptThread was found!");
            Thread oldThread = thread;
            recognizerStop();
            try {
                oldThread.join();
            } catch (InterruptedException e) {
                // we got interrupted? it's a possibility, so maybe we just don't start a new thread
                return;
            }
        }
        this.thread = new InterruptThread(modal);
        this.thread.start();
    }

    public synchronized void recognizerStop() {
        if (this.thread != null) {
            this.thread.sendStop();
            this.thread = null;
            this.threadToInterrupt = null;
        }
    }

    public synchronized void register(List<InterruptNode> interrupts) {
        Map<String, InterruptNode> ninterrupts = new HashMap<String, InterruptNode>();
        for (InterruptNode each : interrupts) {
            ninterrupts.put(each.getSource(), each);
        }
        activeInterrupts = ninterrupts;
    }

    /**
     * Take all of the interrupts from the queue that are served by InterruptNodes of type RESULTS. This also clears
     * interrupts that are not in the active_interrupts Map.
     */
    public synchronized void clearPendingIoInterrupts() {
        List<Interrupt> interrupts = new ArrayList<Interrupt>();
        interruptQueue.drainTo(interrupts); // this clears all interrupts
        for (Interrupt each : interrupts) {
            InterruptNode inode = activeInterrupts.get(each.getSource());
            if ((inode != null) && (inode.getType() != InterruptNode.Type.RESULT)) {
                interruptQueue.offer(each);
            }
        }
    }

    /**
     * Puts an interrupt in the queue
     * 
     * @param source
     * @param description
     */
    public synchronized void raise(String source, String message) {
        InterruptNode inode = activeInterrupts.get(source);
        if (inode != null) {
            Interrupt interrupt = new Interrupt(source, message);
            log.info("Queueing interrupt source = " + source + " message = " + message);
            interruptQueue.add(interrupt);

            if (inode.getType() == InterruptNode.Type.RESULT) {
                ioInterrupts.add(interrupt);
            }
        } else {
            log.info("Interrupt" + source + ":" + message  + " did not match any active interrupt. Active interrupts are: " + activeInterrupts.keySet());            
        }
    }
    
    /**
     * Puts an interrupt in the queue
     * 
     * @param source
     * @param description
     */
    public synchronized void raise(String source, String direction, Double value) {
        InterruptNode inode = activeInterrupts.get(source);
        if (inode != null) {
            Interrupt interrupt = new Interrupt(source, direction, value);
            log.info("Queueing interrupt source = " + source + " direction = " + direction + " value = " + value);
            interruptQueue.add(interrupt);

            if (inode.getType() == InterruptNode.Type.RESULT) {
                ioInterrupts.add(interrupt);
            }
        } else {
            log.info("Interrupt" + source + ":" + value  + " did not match any active interrupt. Active interrupts are: " + activeInterrupts.keySet());            
        }
    }
    
    public synchronized void raiseExit() {
        exitting = true;
        interruptQueue.add(EXIT_INTERRUPT);
    }
    
    public synchronized RecognitionResult getInterrupt() {
        return resultInterrupt;
    }
    
    public synchronized void clearInterrupt() {
        resultInterrupt = null;
    }
    
    protected synchronized void fireInterrupt(RecognitionResult interrupt) {
        if (threadToInterrupt != null) {
            this.resultInterrupt = interrupt;
            threadToInterrupt.interrupt();
        }
    }
    
    class InterruptThread extends Thread {
        private final boolean modal;
        private volatile boolean stopflag = false;

        public InterruptThread(boolean modal) {
            super("InterruptProcessor");
            this.modal = modal; 
        }
        
        @Override
        public void run() {
            while (!stopflag) {
                Interrupt interrupt;
                try {
                    if (exitting) {
                        interrupt = EXIT_INTERRUPT;
                        interruptQueue.clear();
                    } else {
                        interrupt = interruptQueue.take();
                    }
                    log.debug("Took interrupt " + interrupt + " from queue");
                    if (interrupt == EXIT_INTERRUPT) {
                        fireInterrupt(RecognitionResult.event(Result.exit(0, "Exitting by request")));
                        stopflag = true;
                        return;
                    }
                    InterruptNode interruptNode = null;
                    synchronized (InterruptManager.this) {
                        interruptNode = activeInterrupts.get(interrupt.getSource());
                        if (interruptNode != null && !matchSenseAndLevel(interrupt, interruptNode)) {
                            interruptNode = null;
                        }
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("interruptNode: " + interruptNode);
                        log.debug("modal: " + modal);
                        log.debug("stopflag: " + stopflag);
                    }
                    if ((interruptNode != null) && (stopflag == false)) {
                        if (modal) {
                            if (interruptNode instanceof InterruptLinkNode) {
                                InterruptLinkNode link = (InterruptLinkNode)interruptNode;
                                if (!link.getLinkMode().equals("hard")) {
                                    // field is modal, but interrupt mode is not hard, we keep looking
                                    continue;                                        
                                }
                            } else {
                                
                            }
                        }
                        RecognitionResult result = createRecognitionResult(interrupt, interruptNode);
                        if (result.getStatus() == RecognitionResult.Status.EVENT) {
                            Result event = result.getEvent();
                            log.info("Interrupting with " + event.getEvent());
                        } else {
                            log.info("Interrupting with " + result.getResult());    
                        }
                        fireInterrupt(result);
                        stopflag = true;
                    } else if (stopflag == false) {
                        log.info("Interrupt" + interrupt  + " did not match any active interrupt. Active interrupts are: " + activeInterrupts.keySet());            
                    }
                } catch (InterruptedException e) {
                    log.warn("InterruptThread was interrupted.");
                }
            }

        } // run()

        private RecognitionResult createRecognitionResult(Interrupt interrupt, InterruptNode interruptNode) {
            if (interruptNode.getType() == InterruptNode.Type.EVENT) {
                String eventSource = ((InterruptLinkNode) interruptNode).getLinkEvent();
                return RecognitionResult.event(Result.event(eventSource, interrupt.getMessage()));
            } else {
                return RecognitionResult.success("interrupt." + interruptNode.getSource(), interrupt.getMessage());
            }
        }

        public void sendStop() {
            stopflag = true;
            this.interrupt();
        }
        
        private boolean matchSenseAndLevel(Interrupt interrupt, InterruptNode interruptNode) {
            if (interrupt.getSense() == null && interruptNode.getSense() == null) {
                return true;
            } else if (interrupt.getSense() != null && interruptNode.getSense() != null && interrupt.getSense().equals(interruptNode.getSense()) && interrupt.getLevel() != null && interruptNode.getLevel() != null) {
                // sense is not null and matches
            	
                if (interrupt.getSense().equals(InterruptNode.FALL_TO)) {
                    return interrupt.getLevel() <= interruptNode.getLevel();
                }  else {
                    return interrupt.getLevel() >= interruptNode.getLevel();
                }
            }
            return false;
        }
    } // class InterruptThread

}
