/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.interrupt;

public class Interrupt {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final String source;
    private final String message;
    private final String sense;
    private final Double level;

    /**
     * Constructs a new <code>Interrupt</code> instance.
     * @param source the source
     * @param message the message
     */
    public Interrupt(String source, String message) {
        this.source = source;
        this.message = message;
        this.sense = null;
        this.level = null;
    }
    
    /**
     * Constructs a new <code>Interrupt</code> instance.
     * @param source the source
     * @param sense the direction fallto or risetoo
     * @param level the level
     */
    public Interrupt(String source, String sense, Double level) {
        this.source = source;
        this.message = source + ": " + sense + " " + level;
        this.sense = sense;
        this.level = level;
    }

    /**
     * Returns the source.
     * 
     * @return the source.
     */
    public String getSource() {
        return source;
    }

    /**
     * Returns the description.
     * 
     * @return the description.
     */
    public String getMessage() {
        return message;
    }
    
    /**
     * Returns the sense, "fallto", "riseto" or null
     * @return
     */
    public String getSense() {
        return sense;
    }
    
    /**
     * Returns the current level.
     * @return the level or null if not defined
     */
    public Double getLevel() {
        return level;
    }

    public String toString() {
        return "Interrupt(" + source + ", m=" + message + ", s=" + sense + ", l=" + level + ")"; 
    }
}
