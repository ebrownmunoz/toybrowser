/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.applets;

/**
 * SingletonAppletFactory
 *
 * @author edh
 */
public class SingletonAppletFactory implements AppletFactory {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Applet instance;
    
    /**
     * Constructs a new <code>SingletonAppletFactory</code> instance.
     * @param instance the instance
     */
    public SingletonAppletFactory(Applet instance) {
        this.instance = instance;
    }
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.applets.VoxwareAppletFactory#getAppletInstance()
     */
    @Override
    public Applet getAppletInstance() {
        return instance;
    }
}
