/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.applets;

import java.util.HashMap;
import java.util.Map;

/**
 * AppletRegistry
 *
 * @author edh
 */
public class AppletRegistry {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    private Map<String, AppletFactory> applets = new HashMap<String, AppletFactory>();

    /**
     * Registers an {@link AppletFactory} to this registry. The name should match the classid specified in an {@literal 
     * <object>} tag in VXML.
     * 
     * @param name the name
     * @param factory the factory
     */
    public void registerAppletFactory(String name, AppletFactory factory) {
        applets.put(name, factory);
    }

    /**
     * Registers multiple {@link AppletFactory}s to this registry. This is purely a convenience method.
     * @param appletFactories
     */
    public void registerAppletFactories(final Map<String, AppletFactory> appletFactories) {
        for (Map.Entry<String, AppletFactory> entry : appletFactories.entrySet()) {
            registerAppletFactory(entry.getKey(), entry.getValue());
        }
    }
    /**
     * Registers an {@link Applet} to this registry. The name should match the classid specified in an {@literal 
     * <object>} tag in VXML. Registering a single {@link Applet} with this method means a singleton instance of the
     * {@link Applet} is invoked.
     * 
     * @param name the name
     * @param applet the applet
     */
    public void registerApplet(final String name, final Applet applet) {
        applets.put(name, new SingletonAppletFactory(applet));
    }

    /**
     * Registers multiple {@link Applet}s to this registry. This is purely a convenience method.
     * @param applets
     */
    public void registerApplets(final Map<String, Applet> applets) {
        for (Map.Entry<String, Applet> entry : applets.entrySet()) {
            registerApplet(entry.getKey(), entry.getValue());
        }
    }
    /**
     * Gets an applet from the registry.
     * @param name the name
     * @return an Applet or null if the name is unknown
     */
    public Applet getApplet(String name) {
        AppletFactory factory = applets.get(name);
        if (factory == null) {
            return null;
        } else {
            return factory.getAppletInstance();
        }
    }
}
