/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.applets;

import java.util.HashMap;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.Param;

/**
 * AbstractApplet
 *
 * @author edh
 */
/**
 * AbstractApplet
 *
 * @author edh
 */
public abstract class AbstractApplet implements Applet {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private Context context;
    private Map<Param, Object> parameters;
    private SessionContext sessionContext;
    private Map<String, ParameterValue> indexedParameters = new HashMap<String, ParameterValue>();

    /*
     * 
     * (non-Javadoc)
     * @see com.voxware.browser.applets.Applet#run(org.mozilla.javascript.Context, com.voxware.browser.context.SessionContext, java.util.Map)
     */
    @Override
    public Result run(Context jsContext, SessionContext sessionContext, Map<Param, Object> parameters) {
        this.context = jsContext;
        this.parameters = parameters;
        this.sessionContext = sessionContext;
        indexedParameters.clear();
        try {
            Result r = run();
            return r;
        } catch (MissingParameterException e) {
            log.error("Missing parameter", e);
            return Result.event("error.applet.missingparameter", "The applet requires the parameter " + e.getName());
        } catch (IllegalParameterException e) {
            log.error("Bad parameter", e);
            return Result.event("error.applet.illegalparameter", e.getMessage());
        } catch (Exception e) {
            log.error("Exception occurred running applet", e);
            return Result.event("error.applet." + e.getClass().getName(), e.getMessage());
        }
    }

    protected Context getContext() {
        return context;
    }
    
    protected SessionContext getSessionContext() {
        return sessionContext;
    }

    /**
     * Returns a ParameterValue object if the parameter is found, even if the value is null or Undefined. Returns null if the parameter is not found.
     * @param name the name of the parameter
     * @return a ParameterValue or null if the parameter is missing
     */
    private ParameterValue getParameter(String name) {
        for (Map.Entry<Param, Object> entry : parameters.entrySet()) {
            if (entry.getKey().getName().equals(name)) {
                Object value = entry.getValue();
                if (value == Context.getUndefinedValue()) {
                    value = null;
                }
                ParameterValue pv = new ParameterValue(name, value);
                return pv;
            }
        }
        return null;
    }

    /**
     * Gets a parameter, return a ParameterValue of null if not found.
     * @param name the parameter name
     * @return a ParameterValue object
     */
    protected ParameterValue parameter(String name) {
        return parameter(name, null);
    }
    
    /**
     * Gets a parameter, return a ParameterValue of defValue if not found. 
     * @param name the parameter name
     * @param defValue the default value if not found
     * @return a ParameterValue object
     */
    protected ParameterValue parameter(String name, Object defValue) {
        ParameterValue pv = null;
        if (indexedParameters.containsKey(name)) {
            pv = indexedParameters.get(name);
        } else {
            pv = getParameter(name);
            indexedParameters.put(name, pv);
        }
        if (pv != null) {
            // was found
            return pv;
        } else {
            // was not found, but not required, so we return the default
            return new ParameterValue(name, defValue);
        }
    }

    /**
     * Gets a required parameter, throwing a MissingParameterException if not found.
     * @param name the name of the parameter
     * @return a ParameterValue object
     * @throws MissingParameterException if not found
     */
    protected ParameterValue require(String name) throws MissingParameterException {
        ParameterValue pv = null;
        if (indexedParameters.containsKey(name)) {
            pv = indexedParameters.get(name);
        } else {
            pv = getParameter(name);
            indexedParameters.put(name, pv);
        }
        if (pv != null) {
            // was found
            return pv;
        } else {
            // was not found, but required, so we throw exception
            throw new MissingParameterException(name);
        }
    }

    public abstract Result run() throws Exception;
}
