/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.applets;

/**
 * MissingParameterException
 *
 * @author edh
 */
public class MissingParameterException extends Exception {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final String name;
    /**
     * Constructs a new <code>MissingParameterException</code> instance.
     */
    public MissingParameterException(String name) {
        super();
        this.name = name;
    }

    /**
     * Constructs a new <code>MissingParameterException</code> instance.
     * @param description
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public MissingParameterException(String message, String name, Throwable cause, boolean enableSuppression,
        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.name = name;
    }

    /**
     * Constructs a new <code>MissingParameterException</code> instance.
     * @param description
     * @param cause
     */
    public MissingParameterException(String message, String name, Throwable cause) {
        super(message, cause);
        this.name = name;
    }

    /**
     * Constructs a new <code>MissingParameterException</code> instance.
     * @param description
     */
    public MissingParameterException(String message, String name) {
        super(message);
        this.name = name;
    }

    /**
     * Constructs a new <code>MissingParameterException</code> instance.
     * @param cause
     */
    public MissingParameterException(String name, Throwable cause) {
        super(cause);
        this.name = name;
    }

    /**
     * Returns the name.
     * @return the name.
     */
    public String getName() {
        return name;
    }
}
