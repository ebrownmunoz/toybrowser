/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.applets;

import java.lang.reflect.Array;
import java.util.LinkedHashMap;
import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.Scriptable;

/**
 * 
 * ParameterValue
 *
 * @author edh
 */
public class ParameterValue {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private final String name;
    private final Object value;

    /**
     * Constructs a new <code>ParameterValue</code> instance. Unwraps instances of NativeJavaObject.
     * @param name the name of the parameter (mostly for debugging purposes)
     * @param value the value of the parameter
     */
    public ParameterValue(String name, Object value) {
        this.name = name;
        if (Context.getUndefinedValue().equals(value)) {
            this.value = null;
        } else if (value instanceof NativeJavaObject) {
            this.value = Context.jsToJava(value, Object.class);
        } else {
            this.value = value;
        }
    }
    
    /**
     * Gets the raw value of the parameter
     * @return the raw value
     */
    public Object value() {
        return value;
    }
    
    /**
     * Gets the parameter value as a boolean. If the value is of number type, then only a 0, 0.0d, or NaN value is considered false.
     * @return
     */
    public Boolean asBoolean() {
        if (value instanceof Scriptable) {
            return (Boolean)Context.jsToJava(value, Boolean.class);
        } else if (value instanceof Integer) {
            return !((Integer)value).equals(0);
        } else if (value instanceof Double) {
            if (((Double)value).equals(0.0d) || ((Double)value).isNaN()) {
                return false;
            } else {
                return true;
            }
        } else if (value instanceof Boolean) {
            return (Boolean)value;
        } else if (value instanceof String) {
            return Boolean.valueOf((String)value);
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Boolean");
        }
    }
    
    /**
     * Gets the parameter value as an Integer.
     * @return
     */
    public Integer asInteger() {
        if (value instanceof Scriptable) {
            return (Integer) Context.jsToJava(value, Integer.class);
        } else if (value instanceof Integer) {
            return (Integer)value;
        } else if (value instanceof Number) { // Float or Double
            return ((Number)value).intValue();
        } else if (value instanceof String) {
            try {
                return Double.valueOf(value.toString()).intValue();
            } catch (NumberFormatException e) {
                throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Integer");    
            }
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Integer");
        }
    }
    
    public Long asLong() {
        if (value instanceof Scriptable) {
            return (Long) Context.jsToJava(value, Long.class);
        } else if (value instanceof Long) {
            return (Long)value;
        } else if (value instanceof Number) { // Float or Double
            return ((Number)value).longValue();
        } else if (value instanceof String) {
            try {
                return Double.valueOf(value.toString()).longValue();
            } catch (NumberFormatException e) {
                throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Long");    
            }
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Long");
        }
    }
    
    public Double asDouble() {
        if (value instanceof Scriptable) {
            return (Double) Context.jsToJava(value, Double.class);
        } else if (value instanceof Double) {
            return (Double)value;
        } else if (value instanceof Number) {
            return ((Number)value).doubleValue();
        } else if (value instanceof String) {
            try {
                return Double.valueOf(value.toString());
            } catch (NumberFormatException e) {
                throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Double");    
            }
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Double");
        }
    }
    
    public String asString() {
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }
    
    public String[] asStringArray() {
        if (value instanceof NativeArray) {
            NativeArray nArray = (NativeArray)value;
            long length = nArray.getLength();
            if (length > Integer.MAX_VALUE) {
                throw new IllegalArgumentException("Array too large");
            }
            String[] ret = new String[(int) length];
            for (int index = 0; index < length; index ++) {
                ret[index] = Context.toString(nArray.get(index));
            }
            return ret;
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to String[]");
        }
    }
    
    public Object[] asObjectArray() {
        if (value instanceof NativeArray) {
            return ((NativeArray)value).toArray();
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Object[]");
        }            
    }
    
    public Map<String, Object> asMap() {
        if (value instanceof Scriptable) {
            Map<String, Object> ret = new LinkedHashMap<String, Object>();
            Scriptable scriptable = (Scriptable)value;
            for (Object id: scriptable.getIds()) {
                if (id instanceof String) {
                    String key = (String)id;
                    ret.put(key, scriptable.get(key, scriptable));
                }
            }
            return ret;
        } else if (value == null) {
            return null;
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to Map<String, Object>");
        }
    }
    
    @SuppressWarnings("unchecked")
    public <T> T as(Class<T> klass) {
        if (value == null) {
            return null;
        } else if (klass.isAssignableFrom(value.getClass())) {
            return klass.cast(value);
        } else if (klass.isArray() && value instanceof NativeArray) {            
            return (T) ((NativeArray)value).toArray((Object[]) Array.newInstance(klass.getComponentType(), ((NativeArray)value).size()));
        } else {
            throw new IllegalParameterException("The parameter " + name + " cannot be converted from " + value.getClass() + " to " + klass.getName());
        }
    }
}