/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.applets;

/**
 * VoxwareAppletFactory
 *
 * @author edh
 */
public interface AppletFactory {
    /**
     * Returns an instance of the applet. This is called for every invocation of an object tag.
     * @return a VoxwareApplet instance
     */
    Applet getAppletInstance();
}
