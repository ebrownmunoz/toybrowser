/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.AnonymousContext;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: May 25, 2015 by: eric</li>
 * </ul>
 */
public class ExecutableProcessor {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private SessionContext scontext;
    private InterpreterState state;
    private AnonymousContext context;
    
    /**
     * Constructs a new <code>BlockProcessor</code> instance.
     */
    public ExecutableProcessor(SessionContext scontext, AnonymousContext context, InterpreterState state) {
        this.scontext = scontext;
        this.context = context;
        this.state = state;
    }
        
    
    public Result process() {
        return null;
    }

}
