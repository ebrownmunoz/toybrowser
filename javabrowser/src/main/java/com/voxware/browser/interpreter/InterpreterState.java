/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;

import com.voxware.browser.context.ApplicationContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;

/**
 * Maintain the State of the Interpreter
 */
public class InterpreterState {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    
    private final SessionContext sessionContext;
    
    private Document application;
    
    private ApplicationContext appContext;
    
    private Document document;
    
    private DocumentContext docContext;
    
    private FormContext formContext;
    
    private FormItem formItem = null;
    
    private Form form;
    
    private AtomicBoolean exitFlag = new AtomicBoolean(false);
    
    /**
     * Constructs a new <code>InterpreterState</code> instance.
     * @param sessionContext
     */
    public InterpreterState(SessionContext sessionContext) {
        this.sessionContext = sessionContext;
    }
    
    /**
     * Set the state to a new document. This nulls out the form and form
     * context.
     * @param doc
     * @param context
     */
    public void setState(Document doc, DocumentContext context) {
        this.document = doc;
        this.docContext = context;
        this.formContext = null;
        this.form = null;
    }
    
    /**
     * Returns the formContext.
     * @return the formContext.
     */
    public FormContext getFormContext() {
        return formContext;
    }

    /**
     * Sets the formContext.
     * @param formContext the formContext.
     */
    public void setFormContext(FormContext formContext) {
        this.formContext = formContext;
    }

    /**
     * Returns the form.
     * @return the form.
     */
    public Form getForm() {
        return form;
    }

    /**
     * Sets the form.
     * @param form the form.
     */
    public void setForm(Form form) {
        this.form = form;
    }

    /**
     * Returns the document.
     * @return the document.
     */
    public Document getDocument() {
        return document;
    }

    /**
     * Sets the document.
     * @param document the document.
     */
    public void setDocument(Document document) {
        this.document = document;
    }

    /**
     * Returns the docContext.
     * @return the docContext.
     */
    public DocumentContext getDocContext() {
        return docContext;
    }

    /**
     * Sets the docContext.
     * @param docContext the docContext.
     */
    public void setDocContext(DocumentContext docContext) {
        this.docContext = docContext;
    }

    /**
     * Returns the application.
     * @return the application.
     */
    public Document getApplication() {
        return application;
    }
    
    /**
     * Returns the URI for the current application
     * @return
     */
    public URI getApplicationURI() {
        if (application == null) {
            return null;
        } else {
            return application.getUri();
        }
    }

    /**
     * Sets the application.
     * @param application the application.
     */
    public void setApplication(Document application) {
        this.application = application;
    }

    /**
     * Returns the appContext.
     * @return the appContext.
     */
    public ApplicationContext getAppContext() {
        return appContext;
    }

    /**
     * Sets the appContext.
     * @param appContext the appContext.
     */
    public void setAppContext(ApplicationContext appContext) {
        this.appContext = appContext;
    }

    public FormItem getFormItem() {
        return formItem;
    }

    public void setFormItem(FormItem formItem) {
        this.formItem = formItem;
    }
    
    public void setExitFlag() {
        this.exitFlag.set(true);
    }
    
    public boolean isExitFlagSet() {
        return exitFlag.get();
    }
}
