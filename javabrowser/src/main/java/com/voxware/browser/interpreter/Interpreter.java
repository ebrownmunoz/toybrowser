/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.voxware.browser.applets.Applet;
import com.voxware.browser.applets.AppletRegistry;
import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.audioplayer.AudioWrapper;
import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.ApplicationContext;
import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.event.FilledList;
import com.voxware.browser.interpreter.Result.Type;
import com.voxware.browser.interrupt.InterruptManager;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.InterruptLinkNode;
import com.voxware.browser.model.InterruptNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ObjectNode;
import com.voxware.browser.model.Param;
import com.voxware.browser.model.Submit;
import com.voxware.browser.model.executable.Goto;
import com.voxware.browser.model.executable.Log;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.form.Block;
import com.voxware.browser.model.form.Field;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.model.form.Initial;
import com.voxware.browser.model.form.InputItem;
import com.voxware.browser.model.form.Subdialog;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.parser.VxmlParser;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: May 14, 2015 by: EBrown-Munoz</li>
 * </ul>
 */
public class Interpreter {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    public static final String FORM_URI = "formUri";
    public static final String SUPPRESS_SPEECH_INPUT = "suppressspeechinput";
    public static final String BARGE_IN = "bargein";

    private static final String APPLICATION = "application";
    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final InterpreterState state;
    private final SessionContext sessionContext;
    private final Logger log = LoggerFactory.getLogger(Interpreter.class);
    private final VxmlParser vxmlparser;// = DaggerVxmlParser.create();
    private final DocumentServer docServer;
    private final AppletRegistry appletRegistry;

    // this needs to be a Map, because the params need to be evaluated in the parent form context/interpreter
    private Map<Param, Object> subdialogParams = null;

    private final EventProcessor eventProcessor;
    

    /**
     * Constructs a new <code>DocumentInterpreter</code> instance. This instance has an empty applet registry.
     * 
     * @param context session context
     * @param docServer document server
     */
    public Interpreter(SessionContext context, DocumentServer docServer) {
        this(context, docServer, new AppletRegistry());
    }

    /**
     * Constructs a new <code>Interpreter</code> instance.
     * 
     * @param context the session context
     * @param docServer the document server
     * @param appletRegistry the applet registry
     */
    public Interpreter(SessionContext context, DocumentServer docServer, AppletRegistry appletRegistry) {
        this.sessionContext = context;
        this.state = new InterpreterState(context);
        this.eventProcessor = new EventProcessor(context, this.state);
        this.docServer = docServer;
        this.vxmlparser = DaggerVxmlParser.create();
        this.appletRegistry = appletRegistry;
    }
    
    /**
     * Informs the interpreter that it should exit. Once an interpreter is in a should exit state, it should not be reused.
     */
    public void exit() {
        state.setExitFlag();
        sessionContext.getInterruptManager().raiseExit();
    }

    /**
     * Load the document and set it in InterpreterState. This will also load the application of the document into state
     * (if one is supplied) and the form (default). At the end of this, the document and application will be parsed and
     * the models and the context objects will be set into state.
     * 
     * @param document the document
     * @return an Ok Result
     * @throws InterpreterException on error
     */
    public Result loadDocument(Document document) throws InterpreterException {
        return loadDocument(document, null);
    }

    /**
     * Load the document and set it in InterpreterState. This will also load the application of the document into state
     * (if one is supplied) and the form specified by <code>form</code>. At the end of this, the document and
     * application will be parsed and the models and the context objects will be set into state.
     * 
     * @param document the document
     * @param form the form name to start
     * @return an Ok Result
     * @throws InterpreterException on error
     */
    public Result loadDocument(Document document, String form) throws InterpreterException {
        String applicationURL = document.getAttribute(APPLICATION);
        DocumentContext documentContext;

        this.state.setForm(null); // this is important for event processing

        // Check for Application
        if (applicationURL != null) {
            URI appURI;
            try {
                appURI = new URI(applicationURL);
            } catch (URISyntaxException e) {
                throw new InterpreterException(e, this.state.getDocument().getNodeLocation());
            }
            if (!appURI.equals(this.state.getApplicationURI())) {
                loadApplication(appURI);
            }
            // Create document context
            documentContext = sessionContext.createDocumentContext(this.state.getAppContext());
        } else { // no application specified
            documentContext = sessionContext.createDocumentContext(null);
        }
        documentContext.applyInitializers(document.getInitializers(), state);
        documentContext.getEventList().addCatches(document.getCatches());

        this.state.setDocument(document);
        this.state.setDocContext(documentContext);
        this.state.setForm(document.getForm(form));
        this.state.setFormContext(null);

        return Result.Ok();

    }

    /**
     * Iterate over the children of the document.
     * 
     * @param document
     * @param context
     * @throws DocumentServerException
     * @throws ParserException
     * @throws InterpreterException
     * @throws URISyntaxException
     */
    public Result processDocument() {

        log.info("DOCUMENT: " + this.state.getDocument().getUri());
        Result result = null;

        result = processForm(this.state.getForm(), this.state.getDocContext());

        while (result.getType() != Result.Type.OK) {

            Result.Type type = result.getType();

            if (type == Result.Type.GOTO) {
                URI uri = null;
                try {
                    uri = new URI(result.getNextURL());
                    if ((uri.getFragment() != null) && (uri.getSchemeSpecificPart().length() == 0)) {
                        relativeGoto(state, uri);
                    } else {
                        absoluteGoto(state, uri);
                    }
                } catch (URISyntaxException e) {
                    // we should throw error.semantic
                    log.error("Error in goto", e);
                    result = Result.event("error.semantic", "Bad next");
                } catch (InterpreterException e) {
                    log.error("Could not transition to " + uri);
                    result = Result.event("error.badfetch.missingform",
                        "Could not transition to " + uri + e.getMessage());
                }

            } else if (type == Result.Type.SUBMIT) {
                try {
                    URI uri = new URI(result.getNextURL());
                    result = submit(state, uri, result.getParameters(), null, result.isPost());


                } catch (URISyntaxException e) {
                    result = Result.event("error.badfetch.uri", "Bad Fetch");
                }
            } else if (type == Result.Type.RETURN || type == Result.Type.EXIT) {
                return result;
            } else if (type == Result.Type.OK) {
                Form currentForm = this.state.getForm();
                log.error("Form {} at {} finished with nowhere to go", currentForm.getId(),
                    currentForm.getNodeLocation());
                break;
            } else if (type != Result.Type.EVENT) {
                log.error("Type " + type + " not handled.");
                break; // This an end condition
            }

            if (result.getType() == Result.Type.EVENT) {
                log.info("Event " + result.getEvent());
                result = eventProcessor.handleEvent(result);

                if (result.getType() == Result.Type.EVENT) {
                    log.error("No Event Handler. I quit!");
                    break;
                }

                if (result.getType() == Result.Type.OK) {
                    result = processForm(this.state.getForm(), this.state.getDocContext());
                }
            } else {
                result = processForm(this.state.getForm(), this.state.getDocContext());
            }

        } // While result != OK
        return Result.exit(0, "Reached end of interpretation");
    }

    /**
     * Process a relative Goto. This goes to the same document. The document and document context are preserved.
     * 
     * @param state
     * @param uri
     */
    private void relativeGoto(InterpreterState state, URI uri) throws InterpreterException {
        Document document = state.getDocument();
        String fragment = uri.getFragment();
        Form form = document.getForm(fragment);
        if (form == null) {
            throw new InterpreterException("Null form in relative Goto.", null);
        }
        state.setForm(form);
    }

    /**
     * Process an absolute Goto. The document is overwritten and the document context is re-initialized even if the
     * document is the same.
     * 
     * @param state
     * @param uri
     * @throws URISyntaxException
     * @throws ParserException
     * @throws DocumentServerException
     * @throws InterpreterException
     */
    private void absoluteGoto(InterpreterState state, URI uri) throws InterpreterException {
        DocumentParser parser = vxmlparser.getDocumentParser();

        InputStream instream = null;

        try {
            instream = docServer.getDocument(uri);
            Document document = parser.parse(sessionContext, uri, new InputStreamReader(instream, UTF_8));
            loadDocument(document);
            if (uri.getFragment() != null) {
                Form form = document.getForm(uri.getFragment());
                if (form == null) {
                    // Exception for non-existent form
                    throw new InterpreterException(null);
                } else {
                    state.setForm(form);
                }
            }
        } catch (DocumentServerException e) {
            Result event = Result.event("error.badfetch", e.getMessage());
            eventProcessor.handleEvent(event);
        } catch (ParserException e) {
            Result event = Result.event("error.semantic", e.getMessage());
            eventProcessor.handleEvent(event);
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Send a submit. This sends up the the names in the namelist (if availabe) or all of the properties of the form
     * scope. This reaches down into the form scope (through the state) to get the properties.
     * 
     * @param state
     * @param uri
     */
    private Result submit(InterpreterState state, URI uri, Map<String, Object> parameters, NodeLocation nodeLocation,
        boolean isPost) {

        try {
            Document document;
            if (isPost) {
                document = postDocument(uri, parameters, nodeLocation);
            } else {
                document = getDocument(uri, parameters, nodeLocation);
            }

            loadDocument(document);
            if (uri.getFragment() != null) {
                Form form = document.getForm(uri.getFragment());
                if (form == null) {
                    // Exception for non-existent form
                } else {
                    state.setForm(form);
                }
            }
        } catch (DocumentServerException e) {
            log.error("Error fetching resource", e);
            return Result.event("error.badfetch", "Bad Fetch");
        } catch (ParserException e) {
            log.error("Error on submit", e);
            return Result.event("error.semantic.parser", "Parser error on submit");
        } catch (InterpreterException e) {
            log.error("Error on submit", e);
            return Result.event("error.semantic.interpreter", "Interpreter error on submit.");
        } // try
        return Result.Ok();
    }

    private Document postDocument(URI uri, Map<String, Object> parameters, NodeLocation nodeLocation)
        throws InterpreterException, ParserException, DocumentServerException {

        Document document = null;
        InputStream instream = null;
        try {
            instream = docServer.postDocument(uri, parameters);
            DocumentParser parser = vxmlparser.getDocumentParser();
            document = parser.parse(sessionContext, uri, new InputStreamReader(instream, UTF_8));
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return document;
    }

    private Document getDocument(URI uri, Map<String, Object> parameters, NodeLocation nodeLocation)
        throws InterpreterException, ParserException, DocumentServerException {

        Document document = null;
        InputStream instream = null;
        try {
            instream = docServer.getDocument(uri, parameters, CacheControl.NO_CACHE);
            DocumentParser parser = vxmlparser.getDocumentParser();
            document = parser.parse(sessionContext, uri, new InputStreamReader(instream, UTF_8));
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return document;
    }

    /**
     * This is to allow test suite access to context
     */
    public InterpreterState getInterpreterState() {
        return this.state;
    }

    /**
     * Load the application for the document and store state.
     * 
     * @param attribute
     * @return
     */
    private URI loadApplication(URI uri) throws InterpreterException {
        InputStream stream = null;
        Document application = null;
        ApplicationContext appContext = null;

        try {
            stream = docServer.getDocument(uri);
            DocumentParser appParser = vxmlparser.getDocumentParser();
            application = appParser.parse(sessionContext, uri, new InputStreamReader(stream, UTF_8));
            appContext = sessionContext.createApplicationContext(uri.toString());
            appContext.applyInitializers(application.getInitializers(), state);
            appContext.getEventList().addCatches(application.getCatches());
        } catch (DocumentServerException e) {
            throw new InterpreterException(e, new NodeLocation(uri));
        } catch (ParserException e) {
            throw new InterpreterException(e, new NodeLocation(uri));
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.state.setApplication(application);
        this.state.setAppContext(appContext);
        return uri;
    }

    /**
     * @param form
     * @param documentContext
     * @return
     * @throws URISyntaxException
     * @throws ParserException
     * @throws DocumentServerException
     * @throws InterpreterException
     */
    private Result processForm(Form form, DocumentContext documentContext) {
        MDC.put(FORM_URI, createFormUri(form.getNodeLocation().getUri(), form.getId()));
        log.info("FORM: " + form.getId() + ": " + form.getNodeLocation());
        FormContext formContext = new FormContext(sessionContext, documentContext,
            new CatchList(documentContext.getEventList()), new PropertyList(documentContext.getPropertyList()));
        final InterruptManager interruptManager = sessionContext.getInterruptManager();
        interruptManager.register(createInterruptNodeList(form));
        formContext.getEventList().addCatches(form.getCatches());
        formContext.applyInitializers(form.getInitializers(), state);
        formContext.addFormItemVariablesFromForm(form);
        try {
            formContext.applyParams(subdialogParams);
            // we only apply them once per form!!!
            subdialogParams = null;
        } catch (InterpreterException e1) {
            return Result.event("Interpreter Event", e1.getMessage());
        }
        this.state.setForm(form);
        this.state.setFormContext(formContext);
        this.state.setFormItem(null); // reset on new form for blocks etc.

        // Now process
        try {
            FormItem nextItem = getNextFormItem(formContext, form);
            while (nextItem != null) {
                this.state.setFormItem(nextItem);
                Result result = processFormItem(nextItem);
                if (result.getType() == Result.Type.GOTO_NEXTITEM) {
                    nextItem = getNextFormItemByName(formContext, form, result.getNextItem());
                } else if (result.isGotoOrSubmit() || (result.getType() == Result.Type.RETURN) || result.getType() == Result.Type.EXIT) {
                    return result;
                } else if (result.getType() == Result.Type.EVENT) {
                    result = eventProcessor.handleEvent(result);
                    if (result.getType() != Result.Type.OK) {
                        return result;
                    } else {
                        nextItem = getNextFormItem(formContext, form);
                    }
                } else {
                    nextItem = getNextFormItem(formContext, form);
                }
            }
        } catch (InterpreterException e) {
            return Result.event("error.semantic", e.getMessage());
        }

        this.state.setFormItem(null);

        return Result.Ok();
    }

    /**
     * Get the next Form Item. If there is none for this form it will return null.
     * 
     * @param result
     * @param state
     * @return
     * @throws InterpreterException
     */
    protected FormItem getNextFormItem(FormContext fcontext, Form form) throws InterpreterException {

        Context rhino = sessionContext.getContext();

        for (FormItem item : form.getFormItems()) {

            if (!fcontext.isComplete(item)) {
                String cond = item.getCond();
                if (cond != null) {

                    Object condresult = rhino.evaluateString(fcontext.getScope(), cond, "<formItem cond>", 0, null);
                    if (condresult == Context.getUndefinedValue()) {
                        return item;
                    } else {
                        if (Context.toBoolean(condresult)) {
                            return item;
                        }
                    }
                } else {
                    return item; // cond == null
                } // cond != null
            } // isComplete().
        } // for FormItem

        return null;
    }

    /**
     * Get the next Form Item. If there is none for this form it will return null.
     * 
     * @param result
     * @param state
     * @return
     * @throws InterpreterException
     */
    protected FormItem getNextFormItemByName(FormContext fcontext, Form form, String formItemName)
        throws InterpreterException {

        for (FormItem item : form.getFormItems()) {

            if (formItemName.equals(item.getName())) {
                return item;
            }
        } // for FormItem

        return null;
    }

    /**
     * Process the current form item and return the result.
     * 
     * @param fi
     * @return
     * @throws InterpreterException
     * @throws URISyntaxException
     * @throws ParserException
     * @throws DocumentServerException
     */
    private Result processFormItem(FormItem fi) {
        FormContext formContext = state.getFormContext();
        Result result = null;
        if (state.isExitFlagSet()) {
            return Result.exit(0, "Exitting due to request");
        }
        try {            
            if (fi instanceof Block) {
                result = processBlock((Block) fi);
            } else if (fi instanceof Field) {
                result = processField((Field) fi);
            } else if (fi instanceof Subdialog) {
                result = processSubdialog((Subdialog) fi, this.state.getFormContext().getScope());
            } else if (fi instanceof ObjectNode) {
                result = processObject((ObjectNode) fi);
            } else if (fi instanceof Initial) {
                result = processInitial((Initial) fi);
            }
        } catch (InterpreterException e) {
            if (e.getCause() instanceof DocumentServerException || e.getCause() instanceof IOException) {
                result = Result.event("error.badfetch", e.getCause().getMessage());
            } else if (e.getCause() != null) {
                result = Result.event("error.semantic", e.getCause().getMessage());
            } else {
                result = Result.event("error.semantic", "No description");
            }
        } catch (ParserException e) {
            log.error("parser exception", e);
            result = Result.event("error.semantic", e.getMessage());
        } catch (DocumentServerException e) {
            log.error("Document Server exception", e);
            result = Result.event("error.badfetch", e.getMessage());
        } catch (URISyntaxException e) {
            log.error("URI Syntax exception", e);
            result = Result.event("error.badfetch.urisyntax", e.getMessage());
        }
        if (result == null) {
            return null;
        }

        result = eventProcessor.handleEvent(result);

        // we special case initial here because processInitialResult(...) calls the filleds
        if (result.getType() == Result.Type.OK && !(fi instanceof Initial)) {
            try {
                if (formContext.isComplete(fi)) {
                    List<Filled> filleds = formContext.getFilledList().findFilledByCriteria(formContext, fi);
                    // we need to find Filled from the form
                    if (!filleds.isEmpty()) {
                        result = processFilled(filleds);
                    } else {
                        result = Result.Ok();
                    }
                }
            } catch (InterpreterException e) {
                log.error("Exception", e);
                return Result.event("error.semantic", e.getMessage());
            }
        }

        log.info("Process Form Item, returning result " + result);
        return result;
    }

    /**
     * Process a subdialog (as form Item). This starts a new instance of Interpreter. All processing of the subdialog
     * happens in this new Interpreter (which ensure that the context is kept separate). The parameters are passed in as
     * parameters
     * 
     * @param subdialog
     * @return
     * @throws InterpreterException
     * @throws DocumentServerException
     * @throws ParserException
     * @throws URISyntaxException
     */
    private Result processSubdialog(Subdialog subdialog, BrowserScope scope)
        throws InterpreterException, DocumentServerException, ParserException, URISyntaxException {
        String src = subdialog.getSrcFromAttributes(sessionContext.getContext(), state.getFormContext());

        URI uri = new URI(src);

        Interpreter subdialogInterpreter = new Interpreter(sessionContext, docServer, appletRegistry);

        // we need to evaluate all the params in the current context to get their values first
        Map<Param, Object> subdialogParams = evaluateParams(subdialog.getParams());
        subdialogInterpreter.setSubdialogParams(subdialogParams);

        Document document;

        if (subdialog.getMethod().equals("post")) {
            document = postDocument(uri,
                Submit.expandNameList(subdialog.getNameList(), scope, subdialog.getNodeLocation()),
                subdialog.getNodeLocation());
        } else {
            document = getDocument(uri,
                Submit.expandNameList(subdialog.getNameList(), scope, subdialog.getNodeLocation()),
                subdialog.getNodeLocation());
        }

        subdialogInterpreter.loadDocument(document);
        log.info("Process subdalog " + subdialog.getName() + ", document" + document.getUri());
        Result result = subdialogInterpreter.processDocument();

        if (result.getType() == Type.RETURN) {
            log.info("Returned from subdialog. " + result.getValue());
            if (result.getEvent() != null) {
                String eventName = result.getEvent().getName();
                String eventMessage = result.getEvent().getDescription();
                result = Result.event(eventName, eventMessage);
            } else {
                scope.put(subdialog.getName(), scope, result.getValue());
                result = Result.Ok();
            }
        }
        return result;
    }

    private Result processObject(ObjectNode object) throws InterpreterException {
        Map<Param, Object> parameters = evaluateParams(object.getParams());
        String classId = object.getClassId();

        Applet applet = appletRegistry.getApplet(classId);
        FormContext formContext = state.getFormContext();
        BrowserScope scope = formContext.getScope();
        Scriptable objectValue = sessionContext.getContext().newObject(scope);
        // we do this prior to running the applet
        formContext.setFormItemValue(object, objectValue);
        int counter = formContext.getCounter(object);

        if (applet == null) {
            log.error("Could not find class: " + classId);
            return Result.event("error.unsupported.objectname", "Could not find " + classId);
        }

        log.info("Invoking " + classId);
        Result appletResult = invokeApplet(applet, parameters);
        if (appletResult.getType().equals(Result.Type.OK)) {
            if (object.getName() != null) {
                Object value = appletResult.getValue();
                if (value != null) {
                    scope.put(object.getName(), scope, value);
                }
            }
            //return processFilled(formContext.getFilledList().findFilledByCriteria(formContext, object));
        }
        return appletResult;
    }

    /**
     * Process a block
     * 
     * @param block
     * @return the results
     * @throws InterpreterException
     */
    private Result processBlock(Block block) throws InterpreterException {
        Result result;
        FormContext formContext = state.getFormContext();
        AnonymousContext acontext = new AnonymousContext(formContext);
        cacheGotoTargets(block);
        // as per spec, we set the completion right before we execute
        state.getFormContext().setFormItemValue(block, true);
        result = block.getExecutableList().execute(sessionContext, acontext);
        return result;
    }
    
    /**
     * Ensures all goto statements in this block are cached.
     * @param block
     */
    private void cacheGotoTargets(Block block) {
    	for (Executable each : block.getExecutableList()) {
    		if (each instanceof Goto) {
    			Goto go = ((Goto)each);
    			
    			String uristring = go.getNext();
    			if (uristring != null) {
    				URI uri;
					try {
						uri = new URI(uristring);
						if (uri.getSchemeSpecificPart().length() > 0) {
							sessionContext.getDocumentServer().cacheDocument(uri);
						}
					} catch (URISyntaxException e) {
						log.error("Error caching (ignored but may impact recovery)", e);
					} catch (DocumentServerException e) {
						log.error("Error caching (ignored but may impact recovery)", e);
					}
    				
    			}
    			
    		}
    	}
    }

    /**
     * Process a field
     * 
     * @param fi
     * @return the results
     */
    private Result processField(Field fi) throws InterpreterException {
        log.info("FIELD: " + fi.getName() + ": " + fi.getNodeLocation());
        RecognitionResult rresult = null;
        FormContext formContext = state.getFormContext();
        IoManager iomanager = sessionContext.getIoManager();
        PropertyList properties = new PropertyList(formContext.getPropertyList());
        properties.putAll(fi.getProperties()); // Merge the field properties into the form context
        Recognizer recognizer = iomanager.getRecognizer();
        // initialize the shadow variable
        formContext.put(fi.getName() + "$", Context.getUndefinedValue());
        final InterruptManager interruptManager = sessionContext.getInterruptManager();
        interruptManager.register(createInterruptNodeList(fi));

        // TODO should this be asynchronous while we play the prompt?

        Object suppressSpeechInput = properties.evaluate(formContext, SUPPRESS_SPEECH_INPUT, Boolean.FALSE);
        // W3C spec says default bargein is true
        Object bargeIn = properties.evaluate(formContext, BARGE_IN, Boolean.TRUE);

        if (suppressSpeechInput != null && !(suppressSpeechInput instanceof Boolean)) {
            return Result.event("error.semantic", "Value of " + SUPPRESS_SPEECH_INPUT + " must be boolean");
        }
        if (bargeIn != null && !(bargeIn instanceof Boolean)) {
            return Result.event("error.semantic", "Value of " + BARGE_IN + " must be boolean");
        }

        Grammar grammar = getGrammar(fi);
        Speaker speaker = getSpeaker();

        if (suppressSpeechInput == null || !((Boolean) suppressSpeechInput)) {
            prepareRecognizer(formContext, recognizer, grammar, speaker);
        }

        for (Property each : properties.getAll().values()) {
            Object propertyValue = each.evaluate(formContext);
            recognizer.setParameter(each.getName(), propertyValue);
            iomanager.getAudioPlayer().setParameter(each.getName(), propertyValue);
        }

        try {
            interruptManager.recognizerStart(fi.isModal());
            // a single prompt is always prompted
            try {
                List<Prompt> allPrompts = fi.getPrompts();
                if (allPrompts.size() == 1
                    || (allPrompts.size() > 1 && formContext.getShouldPerformPromptSelection(fi))) {
                    List<Prompt> prompts = performPromptSelection(fi, (Boolean) bargeIn);
                    if (prompts.size() > 0) {
                        Integer timeout = prompts.get(prompts.size() - 1).getTimeout();
                        if (timeout != null) {
                            recognizer.setParameter("timeout", timeout);
                        }
                    }
                    formContext.setShouldPerformPromptSelection(fi, false);
                }
            } catch (InterruptedException e) {
                iomanager.getAudioPlayer().cancel();
                rresult = interruptManager.getInterrupt();
                interruptManager.recognizerStop();
                interruptManager.clearInterrupt();
            }

            Future<RecognitionResult> recognize = null;
            if (rresult == null) {
                if (grammar != null && speaker != null) {
                    List<Log> recogStartLoggers = new LinkedList<Log>();
                    for (Log logger : fi.getLogs()) {
                        if (logger.getEvents().contains("recogstart")) {
                            recogStartLoggers.add(logger);
                        }
                    }
                    try {
                        log.info("processField: recognizing");
                        recognize = recognizer.recognize(getGrammarRule(grammar));
                        if (recognize == null) {
                            rresult = interruptManager.getInterrupt();
                        }
                    } catch (BrowserEvent e) {
                        return e.result();
                    }
                    for (Log logger : recogStartLoggers) {
                        logger.execute(sessionContext, formContext);
                    }
                } else {
                    log.warn("No grammar or speaker specified, voice modality is disabled");
                    recognize = recognizer.recognizeWithInterruptsOnly();
                }
            }

            try {
                if (recognize != null) {
                    log.info("processField: getting result(blocking)");
                    try {
                        rresult = recognize.get();
                    } catch (InterruptedException e) {
                        iomanager.getAudioPlayer().cancel();
                        recognize.cancel(true);
                        rresult = interruptManager.getInterrupt();
                        Thread.interrupted();
                        interruptManager.recognizerStop();
                        interruptManager.clearInterrupt();
                    }
                }
                log.info("processField: got result " + rresult);
                Scriptable shadow = createRecognitionShadowVariable(formContext, rresult);
                formContext.put(fi.getName() + "$", shadow);
            } catch (ExecutionException e) {
                log.error("Recognize exception", e);
                rresult = RecognitionResult.event(Result.event("error.recognize.execution", "Recognize exception"));
            } finally {
                // we stop any playing prompts
                sessionContext.getIoManager().getAudioPlayer().cancel();
                interruptManager.recognizerStop();
                interruptManager.clearPendingIoInterrupts();
            }
        } finally {
            interruptManager.recognizerStop();
        }

        log.info("RECOGNITION RESULT: " + rresult);
        FormContext context = state.getFormContext();

        if (rresult.getStatus() == RecognitionResult.Status.EVENT) {
            if (rresult.getEvent() == null) {
                throw new InterpreterException("Null event", fi.getNodeLocation());
            }

            context.setFormItemValue(fi, null);

            return rresult.getEvent();
        } else if (rresult.getStatus() == RecognitionResult.Status.SUCCESS) {
            log.info("ERK Success");
            String slot = fi.getSlot();
            String result = rresult.getResult();
            boolean matchRequired = true;
            Property matchRequiredProperty = properties.get("matchrequired");
            if (matchRequiredProperty != null) {
                matchRequired = Context.toBoolean(matchRequiredProperty.evaluate(formContext));
            }
            Map<String, String> slottedResult = splitResult(result);

            if (matchRequired && slottedResult != null && !slottedResult.containsKey(slot)) {
                return Result.event("nomatch", "slotted result with no matching slot: " + slot);
            } else {
                if (slottedResult != null && slottedResult.containsKey(slot)) {
                    context.setFormItemValue(fi, slottedResult.get(slot));
                } else {
                    context.setFormItemValue(fi, result);
                }
            }
            if (slot == null) {
                context.setFormItemValue(fi, result);
            } else {
                if (slottedResult != null && slottedResult.containsKey(slot)) {
                    context.setFormItemValue(fi, rresult.getResult());
                } else {
                    // what do we do here ?!
                }
            }
            return Result.Ok();
        } else {
            //TODO Handle this more intelligently.
            throw new InterpreterException("Unknown recognition results " + rresult.getStatus(), fi.getNodeLocation());
        }
    }

    /**
     * @param rresult
     * @return
     */
    private Scriptable createRecognitionShadowVariable(FormContext formContext, RecognitionResult rresult) {
        Scriptable shadow = sessionContext.getContext().newObject(formContext.getScope());
        shadow.put("inputmode", shadow, rresult.getInputMode());
        if (rresult.getAlternates().size() > 0) {
            shadow.put("utterance", shadow, join(rresult.getAlternates().get(0)));
        } else {
            shadow.put("utterance", shadow, "");
        }
        shadow.put("interpretation", shadow, rresult.getResult());
        Scriptable audio = sessionContext.getContext().newObject(shadow);
        byte[] audioData = rresult.getAudio();
        if (audioData != null) {
            audio.put("data", audio, Context.javaToJS(audioData, audio));
            audio.put("size", audio, audioData.length);
        }
        shadow.put("audio", shadow, audio);
        shadow.put("result", shadow, Context.javaToJS(rresult.getRecognizerSpecific(), shadow));
        return shadow;
    }

    /**
     * Process a field
     * 
     * @param fi
     * @return the results
     */
    private Result processInitial(Initial fi) throws InterpreterException {
        RecognitionResult rresult = null;
        FormContext formContext = state.getFormContext();
        IoManager iomanager = sessionContext.getIoManager();
        PropertyList properties = new PropertyList(formContext.getPropertyList());
        properties.putAll(fi.getProperties()); // Merge the field properties into the form context
        Recognizer recognizer = iomanager.getRecognizer();
        final InterruptManager interruptManager = sessionContext.getInterruptManager();

        Object suppressSpeechInput = properties.evaluate(formContext, SUPPRESS_SPEECH_INPUT, Boolean.FALSE);
        // W3C spec says default bargein is true
        Object bargeIn = properties.evaluate(formContext, BARGE_IN, Boolean.TRUE);

        if (suppressSpeechInput != null && !(suppressSpeechInput instanceof Boolean)) {
            return Result.event("error.semantic", "Value of " + SUPPRESS_SPEECH_INPUT + " must be boolean");
        }
        if (bargeIn != null && !(bargeIn instanceof Boolean)) {
            return Result.event("error.semantic", "Value of " + BARGE_IN + " must be boolean");
        }
        // TODO should this be asynchronous while we play the prompt?
        Grammar grammar = getGrammar(fi);
        Speaker speaker = getSpeaker();

        if (suppressSpeechInput == null || !((Boolean) suppressSpeechInput)) {
            prepareRecognizer(formContext, recognizer, grammar, speaker);
        }

        for (Property each : properties.getAll().values()) {
            Object propertyValue = each.evaluate(formContext);
            recognizer.setParameter(each.getName(), propertyValue);
            iomanager.getAudioPlayer().setParameter(each.getName(), propertyValue);
        }

        try {
            interruptManager.recognizerStart(false);
            try {
                if (formContext.getShouldPerformPromptSelection(fi)) {
                    performPromptSelection(fi, (Boolean) bargeIn);
                    formContext.setShouldPerformPromptSelection(fi, false);
                }
            } catch (InterruptedException e) {
                rresult = interruptManager.getInterrupt();
                Thread.interrupted();
                interruptManager.recognizerStop();
                interruptManager.clearInterrupt();
            }

            if (rresult == null) {
                Future<RecognitionResult> recognize;
                try {
                    recognize = iomanager.getRecognizer().recognize(getGrammarRule(fi));
                    if (recognize == null) {
                        rresult = interruptManager.getInterrupt();
                    }
                } catch (BrowserEvent e1) {
                    return e1.result(); // this returns the event as a result.
                }

                try {
                    if (rresult == null) {
                        rresult = recognize.get();
                    }
                } catch (InterruptedException e) {
                    rresult = interruptManager.getInterrupt();
                    Thread.interrupted();
                    interruptManager.recognizerStop();
                    interruptManager.clearInterrupt();
                } catch (ExecutionException e) {
                    log.error("Recognize exception", e);
                    rresult = RecognitionResult.event(Result.event("error.recognize.execution", "Recognize exception"));
                } finally {
                    // we stop any playing prompts
                    sessionContext.getIoManager().getAudioPlayer().cancel();
                    interruptManager.recognizerStop();
                    interruptManager.clearPendingIoInterrupts();
                }
            }
        } finally {
            interruptManager.recognizerStop();
        }

        if (rresult.getStatus() == RecognitionResult.Status.EVENT) {
            return rresult.getEvent();
        } else if (rresult.getStatus() == RecognitionResult.Status.SUCCESS) {
            log.info("INITIAL REC SUCCESS!: got: " + rresult.getResult());
            FormContext context = state.getFormContext();
            context.setFormItemValue(fi, rresult.getResult());
            return processInitialResult(fi, rresult);
        } else {
            //TODO Handle this more intelligently.
            throw new InterpreterException("Unknown recognition results " + rresult.getStatus(), fi.getNodeLocation());
        }
    }

    /**
     * Process the result of the recognition for <initial>. This fills in the additional <form> fields. This is hard
     * coded with the assumption that the <initial> tag will only ever be used in the login application.
     * 
     * @param fi
     * @param rresult
     * @return
     * @throws InterpreterException
     */
    private Result processInitialResult(Initial fi, RecognitionResult rresult) throws InterpreterException {
        String result = rresult.getResult();
        String userId = null;
        String application = null;
        String action = "launch";
        String language = null;

        // Each param (separated by '&'
        for (String param : result.split("&")) {
            String[] parts = param.split("=");
            if (parts.length != 2) {
                throw new InterpreterException("Bad response from initial: " + result, fi.getNodeLocation());
            }

            //  userId=1&applicationId=PICKING&action=launch&languageId=en-US
            if ("userId".equals(parts[0])) {
                userId = parts[1];
            } else if ("applicationId".equals(parts[0])) {
                application = parts[1];
            } else if ("languageId".equals(parts[0])) {
                language = parts[1];
            } else if ("action".equals(parts[0])) {
                action = parts[1];
            } else {
                throw new InterpreterException("Bad response (unknown name) from initial: " + result,
                    fi.getNodeLocation());
            }
        }

        if ((userId == null) || (application == null)) {
            throw new InterpreterException("Bad response (null value) from initial: " + result, fi.getNodeLocation());
        }

        FormContext fcontext = state.getFormContext();

        FormItem userIdField = fcontext.getInputItem("userId");
        FormItem applicationIdField = fcontext.getInputItem("applicationId");
        FormItem actionField = fcontext.getInputItem("action");
        FormItem languageIdField = fcontext.getInputItem("languageId");
        fcontext.setFormItemValue(userIdField, userId);
        fcontext.setFormItemValue(applicationIdField, application);
        fcontext.setFormItemValue(actionField, action);

        if (language != null) {
            fcontext.setFormItemValue(languageIdField, language);
        }

        FilledList filledList = fcontext.getFilledList();
        List<Filled> filleds = new ArrayList<Filled>();
        filleds.addAll(
            filledList.findFilledByCriteria(fcontext, Arrays.asList(userIdField, applicationIdField, actionField)));
        Result fresult = processFilled(filleds);
        return fresult;
    }

    private Map<String, String> splitResult(String resultString) {
        Map<String, String> result = new LinkedHashMap<String, String>();
        for (String nameValuePair : resultString.split("&")) {
            String[] parts = nameValuePair.split("=");
            if (parts.length != 2) {
                return null;
            }
            result.put(parts[0], parts[1]);
        }
        return result;
    }

    private List<InterruptNode> createInterruptNodeList(Field fi) {
        List<InterruptNode> interruptNodes = new ArrayList<InterruptNode>(fi.getInterrupts());
        // we don't need to check field modality here because interrupts declared inside a modal field are always active
        interruptNodes.addAll(createInterruptNodeList(state.getForm(), fi.isModal()));
        return interruptNodes;
    }

    private List<InterruptNode> createInterruptNodeList(Form form) {
        return createInterruptNodeList(form, false);
    }

    /**
     * Creates a list of interrupt nodes for a form
     * 
     * @param form
     * @param hardOnly
     * @return
     */
    private List<InterruptNode> createInterruptNodeList(Form form, boolean hardOnly) {
        List<InterruptNode> interruptNodes = new ArrayList<InterruptNode>();
        List<InterruptLinkNode> linkedInterrupts = state.getForm().getLinks();
        for (InterruptLinkNode link : linkedInterrupts) {
            if (!hardOnly || link.getLinkMode().equals("hard")) {
                interruptNodes.add(link);
            }
        }
        linkedInterrupts = state.getDocument().getLinks();
        for (InterruptLinkNode link : linkedInterrupts) {
            if (!hardOnly || link.getLinkMode().equals("hard")) {
                interruptNodes.add(link);
            }
        }

        Document application = state.getApplication();
        if (application != null) {
            linkedInterrupts = application.getLinks();
            for (InterruptLinkNode link : linkedInterrupts) {
                if (!hardOnly || link.getLinkMode().equals("hard")) {
                    interruptNodes.add(link);
                }
            }
        }

        return interruptNodes;
    }

    private Grammar getGrammar(Field fi) {
        Grammar grammar = null;
        Form form = state.getForm();

        if ((fi.getGrammars() != null) && !fi.getGrammars().isEmpty()) {
            grammar = fi.getGrammars().get(0); // always return first item in list.
        } else {
            if (form.getGrammar() != null) {
                grammar = form.getGrammar();
            }
        }

        return grammar;
    }

    private Grammar getGrammar(Initial initial) {
        Grammar grammar = null;
        Form form = state.getForm();
        if (form.getGrammar() != null) {
            grammar = form.getGrammar();
        }
        return grammar;
    }

    private String getGrammarId(Grammar grammar) {
        String grammarUrl = grammar.getUrl(state.getFormContext());
        if (grammarUrl.contains("#")) {
            grammarUrl = grammarUrl.substring(0, grammarUrl.indexOf("#"));
        }
        return grammarUrl;
    }

    private String getGrammarRule(Grammar grammar) throws InterpreterException {
        try {
            return new URI(grammar.getUrl(state.getFormContext())).getFragment();
        } catch (URISyntaxException e) {
            throw new InterpreterException("Invalid URI", grammar.getNodeLocation());
        }
    }

    private String getSpeakerId(Speaker speaker) throws InterpreterException {
        return speaker.getUrl(state.getFormContext());
    }

    private String getGrammarRule(Initial fi) throws InterpreterException {
        Grammar grammar = null;
        Form form = state.getForm();

        if (form.getGrammar() != null) {
            grammar = form.getGrammar();
        }

        if (grammar == null) {
            throw new InterpreterException("No Grammar available", fi.getNodeLocation());
        }
        try {
            return new URI(grammar.getUrl(state.getFormContext())).getFragment();
        } catch (URISyntaxException e) {
            throw new InterpreterException("Invalid URI", fi.getNodeLocation());
        }
    }

    private Speaker getSpeaker() {
        Form form = state.getForm();

        if (form.getSpeaker() != null) {
            return form.getSpeaker();
        } else if (state.getDocument().getSpeaker() != null) {
            return state.getDocument().getSpeaker();
        } else if (state.getApplication().getSpeaker() != null) {
            return state.getApplication().getSpeaker();
        }

        return null;
    }

    private byte[] getSpeakerBytes(Initial fi) throws InterpreterException {
        Speaker speaker = null;
        Form form = state.getForm();

        if (form.getSpeaker() != null) {
            speaker = form.getSpeaker();
        } else if (state.getDocument().getSpeaker() != null) {
            speaker = state.getDocument().getSpeaker();
        } else if (state.getApplication().getSpeaker() != null) {
            speaker = state.getApplication().getSpeaker();
        } else {
            throw new InterpreterException("No Grammar available", fi.getNodeLocation());
        }

        return speaker.fetch(sessionContext, state.getFormContext());
    }

    private Result processFilled(List<Filled> filledList) {
        FormContext formContext = state.getFormContext();
        AnonymousContext acontext = new AnonymousContext(formContext);
        for (Filled filled : filledList) {
            log.info("FILLED!" + filled.getNameListAsString());

            Result result = filled.getExecutables().execute(sessionContext, acontext);
            if (result.getType() != Result.Type.OK) {
                // must be an event or a goto/submit?
                return result;
            }
        }
        return Result.Ok();
    }

    /**
     * Set the subdialog params
     * 
     * @param params
     */
    public void setSubdialogParams(Map<Param, Object> params) {
        this.subdialogParams = params;
    }

    /**
     * Evaluates a list of Params and puts the results into a Map.
     * 
     * @param params the params to evalute
     * @return a map of params to evaluated values
     * @throws InterpreterException on error
     */
    private Map<Param, Object> evaluateParams(List<Param> params) throws InterpreterException {
        Map<Param, Object> parameters = new HashMap<Param, Object>();
        for (Param param : params) {
            try {
                parameters.put(param, param.evaluateValue(sessionContext, state.getFormContext()));
            } catch (Exception e) {
                throw new InterpreterException(param.getNodeLocation());
            }
        }
        return parameters;
    }

    private Result invokeApplet(Applet applet, Map<Param, Object> parameters) {
        Context context = sessionContext.getContext();
        try {
            Result result = applet.run(context, sessionContext, parameters);
            return result;
        } catch (Exception e) {
            return Result.event("error.applet.uncaught" + e.getClass(), e.getLocalizedMessage());
        }
    }

    /**
     * Filters a prompt list by cond and count according to W3C spec for prompt selection
     * 
     * @param prompts the list of prompts for the input item
     * @param counter the current counter for the input item
     * @return a list of prompts
     */
    private List<Prompt> selectPrompts(List<Prompt> prompts, int counter) {
        List<Prompt> selected = new ArrayList<Prompt>(prompts.size());
        int highestCounter = 0;
        for (Prompt prompt : prompts) {
            // first, filter on cond on count <= counter
            if (prompt != null && prompt.evaluateCond(sessionContext, state.getFormContext())
                && prompt.getCount() <= counter) {
                // if the current count is higher than any previously seen count, clear the selected list and add this
                // one
                // if the current count is equal to the highest previously seen count, just add it
                // if the current count is lower, don't select it
                if (prompt.getCount() > highestCounter) {
                    highestCounter = prompt.getCount();
                    selected.clear();
                    selected.add(prompt);
                } else if (prompt.getCount() == highestCounter) {
                    selected.add(prompt);
                }
            }
        }
        return selected;
    }

    /**
     * Queues a prompt to be played and
     * 
     * @param prompt
     * @param defaultBargeIn
     * @throws InterpreterException
     */
    private void queuePrompts(List<Prompt> prompts, boolean defaultBargeIn)
        throws InterruptedException, InterpreterException {
        List<AudioWrapper> queuedAudios = new ArrayList<AudioWrapper>();
        IoManager iomanager = sessionContext.getIoManager();
        AudioPlayer audioPlayer = iomanager.getAudioPlayer();

        FormContext formContext = state.getFormContext();
        boolean bargein = false;

        for (Prompt prompt : prompts) {
            List<AudioWrapper> wrappers = prompt.getAudios(sessionContext, formContext);

            if (prompt.getBargeIn() == null) {
                if (defaultBargeIn) {
                    bargein = true;
                }
            } else if (prompt.getBargeIn().booleanValue()) {
                bargein = true;
            }

            if (!bargein) {
                try {
                    Future<Void> future = audioPlayer.play(wrappers);
                    future.get();
                } catch (AudioPlayerException e) {
                    log.error("Exception playing Audio", e);
                } catch (ExecutionException e) {
                    log.error("Exception playing Audio", e);
                }

            } else {
                queuedAudios.addAll(wrappers); // play them asynchronously
            }
        } // for

        if (!queuedAudios.isEmpty()) {
            try {
                audioPlayer.play(queuedAudios);
            } catch (AudioPlayerException e) {
                log.error("Exception playing Audio", e);
            }
        }

    }

    private List<Prompt> performPromptSelection(InputItem fi, boolean defaultBargeIn)
        throws InterruptedException, InterpreterException {
        FormContext formContext = state.getFormContext();
        List<Prompt> prompts = fi.getPrompts();
        prompts = selectPrompts(prompts, formContext.getCounter(fi));

        try {
            queuePrompts(prompts, defaultBargeIn);
        } finally {
            formContext.incrementCounter(fi);
        }
        return prompts;
    }

    private void prepareRecognizer(FormContext formContext, Recognizer recognizer, Grammar grammar, Speaker speaker)
        throws InterpreterException {
        if (formContext == null) {
            throw new NullPointerException("formContext is null");
        }
        if (recognizer == null) {
            throw new NullPointerException("recognizer is null");
        }
        Map<Object, Object> cache = sessionContext.getCache();
        byte[] fetch = null;
        
        if (grammar != null) {
        	String grammarUrl = grammar.getUrl(formContext);
            if (grammarUrl.contains("#")) {
                grammarUrl = grammarUrl.substring(0, grammarUrl.indexOf("#"));
            }

            if (grammar.getCacheControl().equals(CacheControl.NO_CACHE) || !cache.containsKey(grammarUrl)) {
                fetch = grammar.fetch(sessionContext, formContext);
                recognizer.loadGrammar(getGrammarId(grammar), fetch, true);
            } else {
                fetch = (byte[]) cache.get(grammarUrl);
                recognizer.loadGrammar(getGrammarId(grammar), fetch, false);
            }

            if (!grammar.getCacheControl().equals(CacheControl.NO_CACHE)) {
                cache.put(grammarUrl, fetch);
            }
        }

        if (speaker != null) {
            log.info("processField: loading voice model");
            String speakerUrl = speaker.getUrl(formContext);
            fetch = null;
            if (speaker.getCacheControl().equals(CacheControl.NO_CACHE) || !cache.containsKey(speakerUrl)) {
                fetch = speaker.fetch(sessionContext, formContext);
                recognizer.loadVoiceModel(getSpeakerId(speaker), fetch, true);
            } else {
                fetch = (byte[]) cache.get(speakerUrl);
                recognizer.loadVoiceModel(getSpeakerId(speaker), fetch, false);
            }

            if (!speaker.getCacheControl().equals(CacheControl.NO_CACHE)) {
                cache.put(speakerUrl, fetch);
            }
        }
    }

    private String join(List<String> strings) {
        StringBuilder sb = new StringBuilder();
        for (String string : strings) {
            sb.append(string).append(' ');
        }
        return sb.substring(0, sb.length() - 1);
    }

    private String createFormUri(URI documentUri, String formId) {
        if (documentUri == null) {
            return "#" + formId;
        } else {
            try {
                return new URI(documentUri.getScheme(), documentUri.getSchemeSpecificPart(), formId).toString();
            } catch (URISyntaxException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    private Boolean toBoolean(Object object) {
        if (object == null) {
            return null;
        } else {
            String string = object.toString();
            if (string.equalsIgnoreCase("false") || string.equals("0") || string.equals("0.0")) {
                return false;
            } else if (string.equalsIgnoreCase("true") || string.matches("\\d+(\\.\\d+)?([Ee]\\d+)")) {
                return true;
            } else {
                throw new IllegalArgumentException("Unable to interpret " + string + " as Boolean");
            }
        }
    }
}
