/**
 * 
 */
package com.voxware.browser.interpreter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.event.Catch;
import com.voxware.browser.event.CatchFinder;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.event.Event;
import com.voxware.browser.event.LegacyCatchFinder;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.model.form.InputItem;

/**
 * @author eric
 */
public class EventProcessor {

    private static final Logger log = LoggerFactory.getLogger(EventProcessor.class);

    private final InterpreterState state;
    private final SessionContext sessionContext;
    private final CatchFinder catchFinder;

    /**
     * 
     */
    public EventProcessor(SessionContext scontext, InterpreterState state) {
        this.state = state;
        this.sessionContext = scontext;
        this.catchFinder = new LegacyCatchFinder();
    }
    

    public Result handleEvent(Result result) {
        FormContext formContext = state.getFormContext();
        FormItem fi = state.getFormItem();
        CatchList formEventList;
        formEventList = new CatchList(state.getFormContext().getEventList());
        if (fi != null) {
            formEventList.addCatches(fi.getCatches());
        }

        while (result.getType() == Result.Type.EVENT && !state.isExitFlagSet()) {
            Event event = result.getEvent();
            String eventName = event.getName();
            int counter = 0;
            log.info("Processing Event " + event.toString());
            if (fi != null) {
                counter = formContext.getEventCounter(fi, event.getName());
                formContext.incrementEventCounter(fi, eventName);
            }

            Catch katch = catchFinder.findCatch(sessionContext, state.getFormContext(), formEventList, eventName,
                counter);

            if (katch == null) {
                // TODO the EventList should find parent and default events
                // This should never be null
                log.info("Found no event handler for " + eventName);
                if (eventName.equals("help") || eventName.equals("noinput") || eventName.equals("nomatch")
                    || eventName.equals("maxspeechtimeout")) {
                    result = Result.reprompt();
                } else {
                    return Result.event("error.semantic", "No default catch handler for " + eventName);
                }
            } else {
                log.info("Found event handler " + katch.getEvent() + " location " + katch.getNodeLocation());
                result = katch.handleEvent(sessionContext, formContext, event, formEventList);
            }
            if ((fi != null) && result.getReprompt() && fi instanceof InputItem) {
                formContext.setShouldPerformPromptSelection((InputItem) fi, true);
            }
        }

        if (state.isExitFlagSet()) {
            return Result.exit(0, "Exitting by request");
        }
        return result;
    }
}
