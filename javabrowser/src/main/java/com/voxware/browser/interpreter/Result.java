/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.voxware.browser.event.Event;

/**
 * The result of processing a FormItem. This class could be called event,
 * it represents a transition the FormItem state machine.
 */
public class Result {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    /**
     * If this is set, the interpreter will load the URL and use this
     * to determine the next FormItem.
     */
    private String nextURL = null;
    
    private Event event = null;
    
    private String message = null;
    
    private Map<String, Object> parameters;
    
    private Object value = null;
    
    private int status = 0;
    
    private final boolean isPost;
    
    private final boolean reprompt;
    
    private final String nextitem;
    
    public enum Type {
        INITIAL,
        OK,
        GOTO,
        GOTO_NEXTITEM,
        SUBMIT,
        EVENT,
        RETURN,
        EXIT
    }

    private Type type;
    
    /**
     * Get an OK Result
     * @return
     */
    public static Result Ok() {
        return new Result(Type.OK, null, false);
    }
    
    /**
     * Get an EVENT Result
     * @param event
     * @return
     */
    public static Result event(String event, String message) {
        Result result = new Result(Type.EVENT, null, false);
        result.setEvent(new Event(event, message));
        result.setMessage(message);
        return result;
    }
    
    /**
     * Get an EXIT result. This is different from a RETURN in that EXIT should propagate all the way to the top instead of just the session 
     * @return
     */
    public static Result exit(int status, String message) {
        Result result = new Result(Type.EXIT, null, false);
        result.setStatus(status);
        result.setMessage(message);
        return result;
    }
    
    public static Result reprompt() {
        Result result = new Result(Type.OK, null, null, false, true);
        result.setMessage("reprompt");
        return result;
    }
    
    /**
     * Set the description (on event)
     * @param description
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Constructs a new <code>Result</code> instance.
     * @param type
     * @param nextURL
     */
    public Result(Type type, String nextURL, boolean isPost) {
        this(type, nextURL, null, isPost, false);
    }
    
    public Result(Type type, String nextURL, String nextitem, boolean isPost, boolean reprompt) {
        this.type = type;
        this.nextURL = nextURL;
        this.isPost = isPost;
        this.reprompt = reprompt;
        this.nextitem = nextitem;
    }

    /**
     * Returns the nextURL.
     * @return the nextURL.
     */
    public String getNextURL() {
        return nextURL;
    }
    
    /**
     * True if type is Goto or Submit
     * @return
     */
    public boolean isGotoOrSubmit() {
        return ((type == Type.GOTO) || (type == Type.SUBMIT));
    }
    
    /**
     * Return the result type
     * @return
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the event.
     * @return the event.
     */
    public Event getEvent() {
        return event;
    }

    /**
     * Sets the event.
     * @param event the event.
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     * Returns the parameters.
     * @return the parameters.
     */
    public Map<String, Object> getParameters() {
        if (parameters == null) {
            return Collections.emptyMap();
        } else {
            return parameters;
        }
    }

    /**
     * Sets the parameters.
     * @param parameters the parameters.
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    /**
     * Returns the status.
     * @return the status.
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the status.
     * @param status the status.
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Returns the value.
     * @return the value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value.
     * @param value the value.
     */
    public void setValue(Object value) {
        this.value = value;
    }
    
    public String toString() {
    	if (type == Type.EVENT) {
    		return "EVENT: " + event;
    	} else if (type==Type.GOTO) {
    		return "GOTO: " + nextURL;
    	} else if (type==Type.GOTO_NEXTITEM) {
        		return "GOTO_NEXT_ITEM: " + nextitem;
    	} else {
    		return type + ":" + message;
    	}
    }
    /**
     * Return true if this is submit is for POST method.
     * @return
     */
    public boolean isPost() {
    	return this.isPost;
    }
	
    /**
     * Return true if this is submit is for POST method.
     * @return
     */
    public boolean getReprompt() {
        return reprompt;
    }
    
    /**
     * Creates a new Result that is a copy of this result, but with a new value for reprompt
     * @param reprompt the new reprompt value
     * @return a new result
     */
    public Result withReprompt(boolean reprompt) {
        Result result = new Result(type, nextURL, nextitem, isPost, reprompt);
        result.event = this.event;
        result.message = this.message;
        result.parameters = this.parameters;
        result.value = this.value;
        return result;
    }

	public String getNextItem() {
		return nextitem;
	}
}
