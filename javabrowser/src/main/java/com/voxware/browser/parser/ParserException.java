/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import com.voxware.browser.BrowserException;
import com.voxware.browser.model.NodeLocation;

/**
 * Exception while parsing VoiceXML Document
 */
public class ParserException extends BrowserException {
    /**
     * The serialVersionUID.
     */
    private static final long serialVersionUID = -6641248806276724518L;
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    public ParserException(NodeLocation nodeLocation) {
        super(nodeLocation);
    }

    public ParserException(String message, NodeLocation nodeLocation) {
        super(message, nodeLocation);
    }

    public ParserException(Throwable cause, NodeLocation nodeLocation) {
        super(cause, nodeLocation);
    }

    public ParserException(String message, Throwable cause, NodeLocation nodeLocation) {
        super(message, cause, nodeLocation);
    }
}
