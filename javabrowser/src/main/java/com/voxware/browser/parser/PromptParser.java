/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.text.Text;

/**
 * @author edh
 *
 */
public class PromptParser extends AbstractTagParser<Prompt> implements ExecutableParser<Prompt> {
    private final CompoundPlayableParser compoundPlayableParser;
    /**
     * 
     */
    @Inject
    public PromptParser(CompoundPlayableParser compoundPlayableParser) {
        this.compoundPlayableParser = compoundPlayableParser;
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "prompt";
    }
    
    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    public Prompt parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        CompositePlayable playable = null;
        int eventType = parser.next();
        // we don't use the usual while loop since there should only be text
        // any other child elements would be an error
        if (eventType != XmlPullParser.END_TAG) {
            // it should be either text or a <value...> tag
            // anything else would provoke an error from the parser
            playable = compoundPlayableParser.parse(context);
            // we want the current event type, not the next one
            // the text parser would have advanced passed the text to the END_TAG
            eventType = parser.getEventType();
        } else if (parser.getName().equals(getTagName())) {
            // we are at an end tag and it is </prompt>
            // handles the case of <prompt ...></prompt>
        } 
        Prompt prompt = new Prompt(attributes, nodeLocation, playable);
        return prompt;
    }
    
    @Override
    public Prompt parse(ParsingContext context) throws ParserException {
    	/*
    	 * Handle the special case where there is no prompt tag, only text.
    	 */
    	try {
			if (context.getXmlPullParser().getEventType() == XmlPullParser.TEXT) {
				CompositePlayable text = new CompositePlayable(context.getXmlPullParser().getText());
				return new Prompt(new HashMap<String, String>(), context.getNodeLocation(), text);
			} else {
				return super.parse(context);
			}
		} catch (XmlPullParserException e) {
			throw new ParserException(e, context.getNodeLocation());
		}
    }
}
