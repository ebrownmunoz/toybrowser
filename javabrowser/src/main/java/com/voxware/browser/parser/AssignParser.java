/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.model.executable.Assign;

/**
 * Parses an {@literal<assign...>} element.
 *
 * Created: Oct 13, 2015
 * @author edh
 */
public class AssignParser extends AbstractTagParser<Assign> implements ExecutableParser<Assign> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>AssignParser</code> instance.
     */
    @Inject
    public AssignParser() {
    }
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "assign";
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    @Override
    protected Assign parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        Assign assign = new Assign(attributes, context.getNodeLocation());
        return assign;
    }
}
