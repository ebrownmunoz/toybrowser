/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import javax.inject.Inject;

/**
 * ValidationUtils
 *
 * Created: Oct 14, 2015
 * @author edh
 */
public class ValidationUtils {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ValidationUtils</code> instance.
     */
    @Inject
    public ValidationUtils() {
    }

    /**
     * Checks the parameters and returns true iff one is not null. This 2 argument call should be more efficient than the varargs call. 
     * @param obj1 an object
     * @param obj2 another object
     * @return true iff one the two parameters is not null
     */
    public boolean exactlyOneNonNull(Object obj1, Object obj2) {
        return (obj1 != null && obj2 == null) || (obj1 == null && obj2 != null);
    }
    
    /**
     * Checks the parameters and returns true iff one is not null. This 3 argument call should be more efficient than the varargs call. 
     * @param obj1 an object
     * @param obj2 another object
     * @param obj3 yet another object
     * @return true iff one the two parameters is not null
     */
    public boolean exactlyOneNonNull(Object obj1, Object obj2, Object obj3) {
        return (obj1 != null && obj2 == null && obj3 == null) || (obj1 == null && obj2 != null && obj3 == null) || (obj1 == null && obj2 == null && obj3 != null);
    }
    
    /**
     * Checks the parameters and returns true iff one is not null 
     * @param objects the objects
     * @return true iff there is one non null value, false otherwise
     */
    public boolean exactlyOneNonNull(Object... objects) {
        boolean foundOneNull = false;
        for (Object object : objects) {
            if (object != null ) {
                if (foundOneNull) {
                    // we found more than one null
                    return false;
                } else {
                    foundOneNull = true;
                }
            }
        }
        // if foundNonNull is false, we found no nulls
        return foundOneNull;
    }
    
    /**
     * Checks the parameters and returns true iff there exists at most 1 non null value
     * @param obj1 an object
     * @param obj2 another object
     * @return true iff there exists at most 1 non null value
     */
    public boolean atMostOneNonNull(Object obj1, Object obj2) {
        return obj1 == null || obj2 == null;
    }
    
    /**
     * Checks the parameters and returns true iff there exists at most 1 non null value
     * @param objects the objects
     * @return true iff there exists at most 1 non null value
     */
    public boolean atMostOneNonNull(Object... objects) {
        boolean foundOneNull = false;
        for (Object object : objects) {
            if (object != null ) {
                if (foundOneNull) {
                    // we found more than one null
                    return false;
                } else {
                    foundOneNull = true;
                }
            }
        }
        // we found either 0 or 1 null
        return true;
    }
}
