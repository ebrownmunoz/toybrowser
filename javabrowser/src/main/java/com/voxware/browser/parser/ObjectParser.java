/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ObjectNode;
import com.voxware.browser.model.Param;
import com.voxware.browser.model.form.Filled;

/**
 * SubdialogParser
 * 
 * @author edh
 */
public class ObjectParser extends AbstractFetchableTagParser<ObjectNode> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private final ParamParser paramParser;
    private final CatchParser catchParser;
    private final FilledParser filledParser;
    private final PropertyParser propertyParser;

    /**
     * Constructs a new <code>SubdialogParser</code> instance.
     * @param paramParser the param parser
     * @param filledParser the filled parser
     */
    @Inject
    public ObjectParser(ParamParser paramParser, CatchParser catchParser, FilledParser filledParser, PropertyParser propertyParser) {
        this.paramParser = paramParser;
        this.catchParser = catchParser;
        this.filledParser = filledParser;
        this.propertyParser = propertyParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "object";
    }

    /*
     * 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.AbstractFetchableTagParser#parseTagContent(com.voxware.browser.parser.ParsingContext, java.util.Map)
     */
    @Override
    protected ObjectNode parseTagContent(ParsingContext context, Map<String, String> attributes, String fetchHint, Long fetchtimeout, CacheControl cacheControl) throws ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        List<Param> params = new LinkedList<Param>();
        List<Catch> catches = new LinkedList<Catch>();
        List<Filled> filleds = new LinkedList<Filled>();
        List<Property> properties = new LinkedList<Property>();
        try {
            int eventType = parser.next();
            while (eventType != XmlPullParser.END_TAG) {
                if (eventType == XmlPullParser.TEXT) {
                    if (!parser.isWhitespace()) {
                        log.warn("Unexpected text found in <field> (ignored");
                    }
                } else if (eventType == XmlPullParser.START_TAG) {
                    String tagName = parser.getName();
                    if ("param".equals(tagName)) {
                        Param param = paramParser.parse(context);
                        params.add(param);
                    } else if ("filled".equals(tagName)) {
                        Filled filled = filledParser.parse(context);
                        filleds.add(filled);
                    } else if ("catch".equals(tagName)) {
                        Catch katch = catchParser.parse(context);
                        catches.add(katch);
                    } else if ("property".equals(tagName)) {
                        Property property = propertyParser.parse(context);
                        properties.add(property);
                    } else {
                        log.warn("unhandled child tag " + tagName + " in <subdialog>.");
                    }
                } else {
                    log.warn("unhandled event type " + eventType + " name: " + parser.getName());
                }

                eventType = parser.next();
            } // while
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
            throw context.newParserException(e);
        }

        ObjectNode subdialog = new ObjectNode(attributes, nodeLocation, filleds, cacheControl, params, catches, properties);
        return subdialog;
    }
}
