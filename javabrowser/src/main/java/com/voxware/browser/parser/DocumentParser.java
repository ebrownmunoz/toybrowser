/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.InterruptLinkNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.model.executable.Script;
import com.voxware.browser.model.executable.Var;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * @author edh
 */
public class DocumentParser extends AbstractTagParser<Document> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private final CatchParser catchParser;
    private final FormParser formParser;
    private final ScriptParser scriptParser;
    private final VarParser varParser;
    private final SpeakerParser speakerParser;
    private final PropertyParser propertyParser;
    private final NoInputParser noinputParser;

	private LinkParser linkParser;

    /**
     * Constructs a new <code>DocumentParser</code> instance.
     * 
     * @param catchParser
     * @param formParser
     * @param scriptParser
     * @param varParser
     * @param speakerParser
     * @param grammarParser
     * @param propertyParser
     */
    @Inject
    public DocumentParser(CatchParser catchParser, FormParser formParser, ScriptParser scriptParser,
            VarParser varParser, SpeakerParser speakerParser, GrammarParser grammarParser,
            PropertyParser propertyParser, LinkParser linkParser, NoInputParser noinputParser) {
        this.catchParser = catchParser;
        this.formParser = formParser;
        this.scriptParser = scriptParser;
        this.varParser = varParser;
        this.speakerParser = speakerParser;
        this.propertyParser = propertyParser;
        this.linkParser = linkParser;
        this.noinputParser = noinputParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "vxml";
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.Parser#parse(com.voxware.browser.parser.v2.ParsingContext)
     */
    public Document parse(ParsingContext context) throws ParserException {
        XmlPullParser parser = context.getXmlPullParser();
        try {
            parser.require(XmlPullParser.START_DOCUMENT, null, null);
            parser.nextTag();

            Document document = super.parse(context);
            int eventType = parser.next();
            // this just in case there's some whitespace junk at the end of the xml document
            if (eventType == XmlPullParser.TEXT && parser.isWhitespace()) {
                eventType = parser.next();
            }
            if (eventType != XmlPullParser.END_DOCUMENT) {
                throw context.newParserException("Unexpected content at end of document");
            }
            document.validate(new ValidationContext(new ValidationUtils()));
            return document;
        } catch (IOException e) {
            throw context.newParserException(e);
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (ValidationException e) {
            throw context.newParserException(e);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.Parser#parse(com.voxware.browser.parser.v2.ParsingContext)
     */
    public Document parseTagContent(ParsingContext context, Map<String, String> attributes)
            throws ParserException, XmlPullParserException {
        XmlPullParser parser = context.getXmlPullParser();
        NodeLocation nodeLocation = context.getNodeLocation();
        List<Initializer> initializers = new LinkedList<Initializer>();
        // Menus are also forms
        List<Form> forms = new LinkedList<Form>();
        List<Catch> catches = new LinkedList<Catch>();
        List<InterruptLinkNode> links = new ArrayList<InterruptLinkNode>();
        
        Speaker speaker = null;
        Grammar grammar = null;

        try {
            int parseEvent = parser.next();
            while (parseEvent != XmlPullParser.END_TAG) {
                if (parseEvent == XmlPullParser.START_TAG) {
                    String name = parser.getName();
                    if ("meta".equals(name)) {
                        // NOT yet supported
                    } else if ("var".equals(name)) {
                        Var var = varParser.parse(context);
                        initializers.add(var);
                    } else if ("script".equals(name)) {
                        Script script = scriptParser.parse(context);
                        initializers.add(script);
                    } else if ("form".equals(name)) {
                        Form form = formParser.parse(context);
                        forms.add(form);
                    } else if ("catch".equals(name)) {
                        Catch katch = catchParser.parse(context);
                        catches.add(katch);
                    } else if ("speaker".equals(name)) {
                        speaker = speakerParser.parse(context);
                    } else if ("property".equals(name)) {
                        Property property = propertyParser.parse(context);
                        initializers.add(property);
                    }  else if ("link".equals(name)) {
                    	InterruptLinkNode link = linkParser.parse(context);
                    	links.add(link);
                    } else if ("noinput".equals(name)) {
                        Catch katch  = noinputParser.parse(context);
                        catches.add(katch);
                    } else {
                        throw context.newParserException("Unknown Element " + name);
                    }
                }
                parseEvent = parser.next();
            }
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
            throw context.newParserException(e);
        }
        Document document = new Document(attributes, nodeLocation, initializers, catches, forms, links, speaker);
        return document;
    }

    /**
     * Parses a document. Assumes <code>inputStream</code> uses UTF-8 encoding.
     * 
     * @param sessionContext the session context, possibly used for prefetching
     * @param uri the URI being parsed
     * @param inputStream the input stream
     * @return a Document
     * @throws ParserException on error
     */
    public Document parse(SessionContext sessionContext, URI uri, Reader reader) throws ParserException {
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            parser = factory.newPullParser();
            parser.setInput(reader);
        } catch (XmlPullParserException e) {
            if (factory == null) {
                throw new ParserException("Failed to acquire XmlPullParserFactory", e,
                        new NodeLocation(uri, 0, 0, null));
            } else if (parser == null) {
                throw new ParserException("Failed to acquire XmlPullParser", e, new NodeLocation(uri, 0, 0, null));
            } else {
                throw new ParserException("Failed to set input on XmlPullParser", e, new NodeLocation(uri, 0, 0, null));
            }
        }
        return parse(new ParsingContext(sessionContext, parser, uri));
    }

    /**
     * Parses a document.
     * 
     * @param sessionContext the session context, possibly used for prefetching
     * @param uri the URI being parsed
     * @param reader the reader
     * @return a Document
     * @throws ParserException on error
     */
    public Document parse(SessionContext sessionContext, URI uri, InputStream inputStream) throws ParserException {
        return parse(sessionContext, uri, new InputStreamReader(inputStream, UTF_8));
    }
}
