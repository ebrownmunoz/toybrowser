/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.resource.Grammar;

/**
 * @author edh
 */
public class GrammarParser extends AbstractFetchableTagParser<Grammar> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>GrammarParser</code> instance.
     * 
     * @param validationUtils
     */
    @Inject
    public GrammarParser() {
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractFetchableTagParser#parseTagContent(com.voxware.browser.parser.v2.
     * ParsingContext, java.util.Map, java.lang.String, java.lang.Long, com.voxware.browser.documentserver.CacheControl)
     */
    @Override
    protected Grammar parseTagContent(ParsingContext context, Map<String, String> attributes, String fetchHint, Long fetchtimeout, CacheControl cacheControl) throws ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();

        try {
            if (!parser.isEmptyElementTag()) {
                int eventType = parser.next();
                // this deviates from spec because we don't support inline grammars
                while (eventType != XmlPullParser.END_TAG) {
                    if (eventType == XmlPullParser.TEXT) {
                        if (!parser.isWhitespace()) {
                            throw context.newParserException("No content allowed in <grammar> tag. Found text " + parser.getText());
                        }
                    } else if (eventType == XmlPullParser.START_TAG) {
                        throw context.newParserException("No content allowed in <grammar> tag. Found start tag: " + parser.getName());
                    }
                    eventType = parser.next();
                }
            }
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
            throw context.newParserException(e);
        }
        Grammar grammar = new Grammar(attributes, nodeLocation, cacheControl, null);
        return grammar;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "grammar";
    }

}
