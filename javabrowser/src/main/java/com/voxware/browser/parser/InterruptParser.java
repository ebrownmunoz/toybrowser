package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.model.InterruptFieldNode;


public class InterruptParser extends AbstractTagParser<InterruptFieldNode> {

    @Inject
    public InterruptParser() {
    }
    
	@Override
	protected String getTagName() {
		return "interrupt";
	}

	@Override
	protected InterruptFieldNode parseTagContent(ParsingContext context, Map<String, String> attributes)
			throws XmlPullParserException, IOException, ParserException {
		return new InterruptFieldNode(attributes, context.getNodeLocation());
	}

}
