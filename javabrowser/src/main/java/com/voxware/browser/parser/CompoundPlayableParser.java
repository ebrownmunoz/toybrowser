/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.io.Playable;
import com.voxware.browser.io.TextPlayable;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.text.CompositeText;
import com.voxware.browser.model.text.Text;
import com.voxware.browser.model.text.TextString;

/**
 * Parser for CompositePlayable objects. Composite text is collection of TextString and TextValue objects. A caveat for
 * using this class is that it leaves the XmlPullParser on the element after the text, so generally you will want to use
 * <code>getEventType()</code> instead of <code>next()</code> after calling <code>parse()</code>. Most other parsers
 * will leave the parser on the ending tag.
 * 
 * @author edh
 */
public class CompoundPlayableParser implements Parser<CompositePlayable> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Logger log = LoggerFactory.getLogger(Parser.class);
    private final ValueParser valueParser;

    /**
     * 
     */
    @Inject
    public CompoundPlayableParser(ValueParser valueParser) {
        this.valueParser = valueParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.Parser#parse(com.voxware.browser.parser.v2.ParsingContext)
     */
    public CompositePlayable parse(ParsingContext context) throws ParserException {
        XmlPullParser parser = context.getXmlPullParser();
        List<Playable> content = new LinkedList<Playable>();
        try {
            // check the current event first, because it might be text
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_TAG) {
                if (eventType == XmlPullParser.TEXT) {
                    if (!parser.isWhitespace()) {
                        TextPlayable text = new TextPlayable(parser.getText());
                        content.add(text);
                    }
                } else if (eventType == XmlPullParser.START_TAG) {
                    if (parser.getName().equals("value")) {
                        Playable value = valueParser.parse(context);
                        content.add(value);
                    } else if (parser.getName().equals("audio")) {
                        parser.next(); // eat the audio tag
                        CompositePlayable audioPlayable = parse(context);
                        for (Playable each : audioPlayable.getPlayables()) {
                            if (each != null) {
                                content.add(each);
                            }
                        }                       
                    }
                } else {
                    log.info("unhandled event type " + eventType + " name: " + parser.getName());
                    throw context.newParserException("unhandled event type " + eventType + " name: " + parser.getName());
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
            throw context.newParserException(e);
        }
        CompositePlayable playables =  new CompositePlayable(content);
        return playables;
    }
}