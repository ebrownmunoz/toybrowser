package com.voxware.browser.parser;

import java.util.Map;

import javax.inject.Inject;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.resource.Speaker;

public class SpeakerParser extends AbstractFetchableTagParser<Speaker> {


    /**
     * Construct a speaker parser.
     * 
     * @param validationUtils
     */
    @Inject
    public SpeakerParser() {
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.impl.AbstractFetchableTagParser#parseTagContent(com.voxware.browser.parser.ParsingContext, java.util.Map, java.lang.String, java.lang.Long, com.voxware.browser.documentserver.CacheControl)
     */
    @Override    
    protected Speaker parseTagContent(ParsingContext context, Map<String, String> attributes, String fetchHint, Long fetchtimeout, CacheControl cacheControl) throws ParserException {
        Speaker speaker = new Speaker(attributes, context.getNodeLocation(), cacheControl);
        return speaker;
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.impl.AbstractTagParser#getTagName()
     */
    @Override    
    protected String getTagName() {
        return "speaker";
    }

}
