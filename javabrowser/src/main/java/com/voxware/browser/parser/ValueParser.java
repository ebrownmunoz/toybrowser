/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable, shall
 * retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks, trade
 * secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.util.Map;

import javax.inject.Inject;

import com.voxware.browser.io.ValuePlayable;
import com.voxware.browser.model.text.TextValue;

/**
 * ValueParser parses {@literal <value.../>} tags
 * @author edh
 */
public class ValueParser extends AbstractTagParser<ValuePlayable> implements Parser<ValuePlayable> {

    /**
     * Constructs a new <code>ValueParser</code> instance.
     */
    @Inject
    public ValueParser() {
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "value";
    }

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext,
     * java.util.Map)
     */
    @Override
    protected ValuePlayable parseTagContent(ParsingContext context, Map<String, String> attributes) {
        ValuePlayable value = new ValuePlayable(context.getXmlPullParser().getAttributeValue(null,"expr"), context.getXmlPullParser().getAttributeValue(null,"recsrc"));
        return value;
    }

}
