/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

/**
 * Interface for a parser.
 * 
 * @author edh
 */
public interface Parser<T> {
    /**
     * Parses an element
     * 
     * @param context
     * @return
     * @throws ParserException
     */
    T parse(ParsingContext context) throws ParserException;
}
