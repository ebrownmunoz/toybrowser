package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.InterruptLinkNode;
import com.voxware.browser.model.InterruptNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

public class LinkParser extends AbstractTagParser<InterruptLinkNode>{
	
	private InterruptParser interruptParser;

    @Inject
	public LinkParser (InterruptParser interruptParser) {
		this.interruptParser = interruptParser;
	}

	@Override
	protected String getTagName() {
		return "link";
	}

	@Override
	protected InterruptLinkNode parseTagContent(ParsingContext context, Map<String, String> attributes)
			throws XmlPullParserException, IOException, ParserException {
		NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        InterruptLinkNode link = null;
        
        String event = parser.getAttributeValue(null, "event");
        String mode = parser.getAttributeValue(null, "mode");

        int eventType = parser.next();
        while (eventType != XmlPullParser.END_TAG) { // process <form>
            if (eventType == XmlPullParser.TEXT) {
                if (!parser.isWhitespace()) {
                    throw context.newParserException("Unexpected text in form");
                }
            } else if (eventType == XmlPullParser.START_TAG) {
                if ("interrupt".equals(parser.getName())) {
                    InterruptNode interrupt = interruptParser.parse(context);
                    link = new InterruptLinkNode(interrupt.getAttributes(), 
                        interrupt.getNodeLocation(), event, mode);
                } else {
                    throw new ParserException("Unknown tag name " + parser.getName(), nodeLocation);
                }
            } 
                
            eventType = parser.next();
        } // while
        return link;

	}
	

}
