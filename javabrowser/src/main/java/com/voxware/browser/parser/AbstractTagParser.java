/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * Abstract parser class that assumes it is parsing content that starts with a tag. Verifies that the start tag is the
 * one returned by <code>getTagName()</code>. Also verifies that the end tag is the one returned by
 * <code>getTagName()</code>. Correctly handles degenerate tags too.
 * 
 * @author edh
 */
public abstract class AbstractTagParser<T> implements Parser<T> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.Parser#parse(com.voxware.browser.parser.v2.ParsingContext)
     */
    public T parse(ParsingContext context) throws ParserException {
        XmlPullParser parser = context.getXmlPullParser();
        try {
            parser.require(XmlPullParser.START_TAG, null, getTagName());
            Map<String, String> attributes = extractAttributes(context);
            context.mark();
            T obj = parseTagContent(context, attributes);
            // if the parser expects only attributes, advance the parser to the next element (should be the end tag)
            if (!context.positionChanged()) {
                int eventType = parser.next();
                // if it's basically ignorable whitespace, keep advancing
                while (eventType == XmlPullParser.TEXT && parser.isWhitespace()) {
                    eventType = parser.next();
                }
            }
            parser.require(XmlPullParser.END_TAG, null, getTagName());
            return obj;
        } catch (IOException e) {
            throw context.newParserException(e);
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        }
    }

    /**
     * Extracts the attributes of the start tag. Creates a Map with the attributes.
     * 
     * @param context the parsing context
     * @return a Map, possibly empty
     */
    protected Map<String, String> extractAttributes(ParsingContext context) {
        Map<String, String> attributes = new HashMap<String, String>();
        XmlPullParser parser = context.getXmlPullParser();
        int attributeCount = parser.getAttributeCount();
        try {
            for (int attributeIndex = 0; attributeIndex < attributeCount; attributeIndex++) {
                String name = parser.getAttributeName(attributeIndex);
                String value = parser.getAttributeValue(attributeIndex);
                attributes.put(name, value);
            }
        } catch (IndexOutOfBoundsException e) {

        }
        return attributes;
    }

    /**
     * Gets the tag name that this parser expects.
     * 
     * @return a tag name
     */
    protected abstract String getTagName();

    /**
     * Parses the inner content of the tag.
     * 
     * @param context the parsing context
     * @param attributes the attributes extracted via <code>extractAttributes(ParsingContext context)</code>
     * @return an object
     * @throws XmlPullParserException on error
     * @throws IOException on error
     * @throws ParserException on error
     */
    protected abstract T parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException;
}
