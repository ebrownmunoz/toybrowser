/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.InterruptLinkNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ObjectNode;
import com.voxware.browser.model.executable.Script;
import com.voxware.browser.model.executable.Var;
import com.voxware.browser.model.form.Field;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.model.form.FilledRegisterer;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.model.form.Initial;
import com.voxware.browser.model.form.Subdialog;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * @author edh
 */
public class FormParser extends AbstractTagParser<Form> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private final BlockParser blockParser;
    private final CatchParser catchParser;
    private final FieldParser fieldParser;
    private final FilledParser filledParser;
    private final SubdialogParser subdialogParser;
    private final ObjectParser objectParser;
    private final VarParser varParser;
    private final SpeakerParser speakerParser;
    private final GrammarParser grammarParser;
    private final PropertyParser propertyParser;

	private final LinkParser linkParser;

    private InitialParser initialParser;

    private ScriptParser scriptParser;

    /**
     * Constructs a new <code>FormParser</code> instance.
     * 
     * @param blockParser
     * @param catchParser
     * @param fieldParser
     * @param filledParser
     * @param subdialogParser
     * @param varParser
     * @param speakerParser
     * @param grammarParser
     * @param propertyParser
     */
    @Inject
    public FormParser(BlockParser blockParser, CatchParser catchParser, FieldParser fieldParser,
        FilledParser filledParser, SubdialogParser subdialogParser, ObjectParser objectParser, VarParser varParser,
        SpeakerParser speakerParser, GrammarParser grammarParser, PropertyParser propertyParser,
        LinkParser linkParser, InitialParser initialParser, ScriptParser scriptParser) {
        this.blockParser = blockParser;
        this.catchParser = catchParser;
        this.fieldParser = fieldParser;
        this.filledParser = filledParser;
        this.subdialogParser = subdialogParser;
        this.objectParser = objectParser;
        this.varParser = varParser;
        this.speakerParser = speakerParser;
        this.grammarParser = grammarParser;
        this.propertyParser = propertyParser;
        this.linkParser = linkParser;
        this.initialParser = initialParser;
        this.scriptParser = scriptParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "form";
    }

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext,
     * java.util.Map)
     */
    @Override
    protected Form parseTagContent(ParsingContext context, Map<String, String> attributes)
            throws XmlPullParserException, IOException, ParserException {
        try {
            final NodeLocation nodeLocation = context.getNodeLocation();
            XmlPullParser parser = context.getXmlPullParser();
            List<Initializer> initializers = new LinkedList<Initializer>();
            List<FormItem> formItems = new LinkedList<FormItem>();
            List<Filled> filleds = new LinkedList<Filled>();
            List<Catch> catches = new LinkedList<Catch>();
            List<InterruptLinkNode> links = new LinkedList<InterruptLinkNode>();
            
            Speaker speaker = null;
            Grammar grammar = null;

            int eventType = parser.next();
            while (eventType != XmlPullParser.END_TAG) { // process <form>
                if (eventType == XmlPullParser.TEXT) {
                    if (!parser.isWhitespace()) {
                        throw context.newParserException("Unexpected text in form");
                    }
                } else if (eventType == XmlPullParser.START_TAG) {
                    FormItem formItem = null;
                    String tagName = parser.getName();
                    if (tagName.equals("block")) {
                        formItem = blockParser.parse(context);
                        formItems.add(formItem);
                    } else if ("catch".equals(tagName)) {
                        Catch katch = catchParser.parse(context);
                        catches.add(katch);
                    } else if ("field".equals(tagName)) {
                        Field field = fieldParser.parse(context);
                        formItems.add(field);
                        initializers.add(new FilledRegisterer(field));
                    } else if ("filled".equals(tagName)) {
                        Filled filled = filledParser.parse(context);
                        filleds.add(filled);
                        initializers.add(new FilledRegisterer(filled));
                    } else if ("var".equals(tagName)) {
                        Var var = varParser.parse(context);
                        initializers.add(var);
                    } else if ("subdialog".equals(tagName)) {
                        Subdialog subdialog = subdialogParser.parse(context);
                        formItems.add(subdialog);
                        initializers.add(new FilledRegisterer(subdialog));
                    } else if ("object".equals(tagName)) {
                        ObjectNode object = objectParser.parse(context);
                        formItems.add(object);
                        initializers.add(new FilledRegisterer(object));
                    } else if ("initial".equals(tagName)) {
                        Initial initial = initialParser.parse(context);
                        formItems.add(initial);
                        initializers.add(new FilledRegisterer(initial));
                    }else if ("speaker".equals(tagName)) {
                        speaker = speakerParser.parse(context);
                    } else if ("grammar".equals(tagName)) {
                        grammar = grammarParser.parse(context);
                    } else if ("property".equals(tagName)) {
                        Property property = propertyParser.parse(context);
                        initializers.add(property);
                    } else if ("link".equals(tagName)) {
                    	InterruptLinkNode link = linkParser.parse(context);
                    	links.add(link);
                    } else if ("script".equals(tagName)) {
                        Script script = scriptParser.parse(context);
                        initializers.add(script);
                    }else {
                        log.warn("Unknown Tag " + tagName);
                    }
                }
                eventType = parser.next();
            }
            Form form = new Form(attributes, nodeLocation, initializers, formItems, filleds, catches, links, speaker, grammar);
            return form;
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
            throw context.newParserException(e);
        }
    }
}
