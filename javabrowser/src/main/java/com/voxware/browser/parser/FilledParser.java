/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.form.Filled;

/**
 * Parses {@literal <filled>} element. Currently returns an EventHandler object, but may be inappropriate.
 * 
 * @author edh
 */
public class FilledParser extends AbstractTagParser<Filled> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final ExecutableListParser executableParser;

    /**
     * @param executableParser
     */
    @Inject
    public FilledParser(ExecutableListParser executableParser) {
        this.executableParser = executableParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "filled";
    }

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext,
     * java.util.Map)
     */
    @Override
    protected Filled parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();        
        ExecutableList executables = executableParser.parse(context);
        Filled filled = new Filled(attributes, nodeLocation, executables);
        return filled;
    }
}
