/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.model.Executable;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.IfClause;
import com.voxware.browser.model.executable.IfStatement;

/**
 * It would be nice if we had multiple inheritance in this case, but we don't. This class 
 *
 * Created: Oct 13, 2015
 * @author edh
 */
public class IfParser extends AbstractTagParser<IfStatement> implements ExecutableParser<IfStatement> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    private final AudioParser audioParser;
    private final AssignParser assignParser;
    private final ClearParser clearParser;
    // disconnect not supported
    private final GotoParser gotoParser;
    private final LogParser logParser;
    private final PromptParser promptParser;
    private final RepromptParser repromptParser;
    private final ReturnParser returnParser;
    private final ScriptParser scriptParser;
    private final SubmitParser submitParser;
    private final ThrowParser throwParser;
    private final VarParser varParser;
    
    /**
     * Constructs a new <code>IfParser</code> instance.
     * @param audioParser
     * @param assignParser
     * @param clearParser
     * @param gotoParser
     * @param logParser
     * @param promptParser
     * @param repromptParser
     * @param returnParser
     * @param scriptParser
     * @param submitParser
     * @param throwParser
     * @param varParser
     */
    @Inject
    public IfParser(AudioParser audioParser, AssignParser assignParser, ClearParser clearParser, GotoParser gotoParser, LogParser logParser, PromptParser promptParser, RepromptParser repromptParser, ReturnParser returnParser, ScriptParser scriptParser, SubmitParser submitParser,
        ThrowParser throwParser, VarParser varParser) {
        this.audioParser = audioParser;
        this.assignParser = assignParser;
        this.clearParser = clearParser;
        this.gotoParser = gotoParser;
        this.logParser = logParser;
        this.promptParser = promptParser;
        this.repromptParser = repromptParser;
        this.returnParser = returnParser;
        this.scriptParser = scriptParser;
        this.submitParser = submitParser;
        this.throwParser = throwParser;
        this.varParser = varParser;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "if";
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    @Override
    protected IfStatement parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        // IfStatement's nodeLocation (probably same as the first IfClause?
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        boolean hasElse = false;
        NodeLocation start = context.getNodeLocation();
        List<IfClause> clauses = new LinkedList<IfClause>();
        List<Executable> executables = new LinkedList<Executable>();

        int eventType = parser.next();
        while (eventType != XmlPullParser.END_TAG) { // process <form>
            String tagName = parser.getName();
            if (isElseLikeTag(eventType, tagName)) {
                // start a new clause
                if (tagName.equals("else")) {
                    if (hasElse) {
                        throw context.newParserException("If statement has multiple else clauses.");
                    }
                    hasElse = true;
                    clauses.add(new IfClause(attributes, start, new ExecutableList(executables)));
                    attributes = extractAttributes(context);
                    start = context.getNodeLocation();
                    executables.clear();
                    eventType = parser.next();
                    parser.require(XmlPullParser.END_TAG, null, "else");
                } else if (tagName.equals("elseif")) {
                    if (hasElse) {
                        throw context.newParserException("If statement has multiple else clauses.");
                    }
                    clauses.add(new IfClause(attributes, start, new ExecutableList(executables)));
                    attributes = extractAttributes(context);
                    start = context.getNodeLocation();
                    executables.clear();
                    eventType = parser.next();
                    parser.require(XmlPullParser.END_TAG, null, "elseif");
                } else {
                    throw context.newParserException("Unknown tag" + parser.getName());
                }

            } else {
                // just some regular executable content
                if ((eventType != XmlPullParser.TEXT) || !parser.isWhitespace()) {
                    Executable exec = dispatchAndParse(context);
                    if (exec != null) {
                        executables.add(exec);
                    }
                } 
                
                              
            } // if elselike tag
            
            eventType = parser.next();
        } // while
        
        clauses.add(new IfClause(attributes, start, new ExecutableList(executables)));
        
        if (!hasElse) {
            // we have no else clause
            // do we add a dummy else clause?
        }
        IfStatement ifstatement = new IfStatement(attributes, nodeLocation, clauses);
        // caller will check if we're actually ending on a </if>
        return ifstatement;
    }

    /**
     * Returns true if the parse is positioned on an else-like tag (elseif and else)
     * @param eventType the event type
     * @param tagName the tag name
     * @return true if on an elseif or else tag, false otherwise
     */
    protected boolean isElseLikeTag(int eventType, String tagName) {
        if (eventType == XmlPullParser.START_TAG && (tagName.equals("else") || tagName.equals("elseif"))) {
            return true;
        }
        return false;
    }
    
    /**
     * Dispatches on the current parsing context and returns an Executable. Returns null if whitespace is swallowed.
     * @param context the parsing context
     * @return an Executable
     * @throws ParserException on error
     */
    protected Executable dispatchAndParse(ParsingContext context) throws ParserException {
        XmlPullParser parser = context.getXmlPullParser();
        int eventType;
        try {
            eventType = parser.getEventType();
            if (eventType == XmlPullParser.TEXT) {
                if (!parser.isWhitespace()) {
                    return promptParser.parse(context);
                } else {
                    return null; // this means it is whitespace
                }
            } else if (eventType == XmlPullParser.START_TAG) {
                ExecutableParser<?> subParser = selectParser(parser.getName());
                 
                if (subParser != null) {
                    return subParser.parse(context);
                } else {
                    throw context.newParserException("unknown tag encountered: " + parser.getName());
                }
            } 
            throw context.newParserException("tag must be one of " + " but was " + parser.getName());
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        }
    }
    
    /**
     * Returns a parser based on a <code>tagName</code>.
     * @param tagName the tag name
     * @return a parser, or null if there is none for the tag name
     */
    protected ExecutableParser<?> selectParser(String tagName) {
        if ("assign".equals(tagName)) {
            return assignParser;
        } else if ("audio".equals(tagName)) {
            return audioParser;
        } else if ("clear".equals(tagName)) {
            return clearParser;
        } else if ("goto".equals(tagName)) {
            return gotoParser;
        } else if ("if".equals(tagName)) {
            return this;
        } else if ("log".equals(tagName)) {
            return logParser;
        } else if ("prompt".equals(tagName)) {
            return promptParser;
        } else if ("reprompt".equals(tagName)) {
            return repromptParser;
        } else if ("return".equals(tagName)) {
            return returnParser;
        } else if ("script".equals(tagName)) {
            return scriptParser;
        } else if ("submit".equals(tagName)) {
            return submitParser;
        } else if ("throw".equals(tagName)) {
            return throwParser;
        } else if ("var".equals(tagName)) {
            return varParser;
        } else {
            //throw new ParserException("Unknown Executable Tag " + tagName, filename, parser.getLineNumber());
            return null;
        }
    }
}
