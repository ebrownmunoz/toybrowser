/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.Script;

/**
 * @author edh
 */
public class ScriptParser extends AbstractFetchableTagParser<Script> implements ExecutableParser<Script> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ScriptParser</code> instance.
     */
    @Inject
    public ScriptParser() {
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "script";
    }

    @Override
    protected Script parseTagContent(ParsingContext context, Map<String, String> attributes, String fetchHint, Long fetchtimeout, CacheControl cacheControl) throws ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        Script script;
        String src = attributes.get("src");
        String charSet = attributes.get("charset");
        if (charSet == null) {
            charSet = "UTF-8";
        }
        String content = null;
        try {
            // should be a leaf node
            content = parser.nextText();
        } catch (XmlPullParserException e) {
            throw context.newParserException("error reading script content", e);
        } catch (IOException e) {
            throw context.newParserException("error reading script content", e);
        }
        if (src != null && content != null && !content.isEmpty()) {
            throw context.newParserException("can not have both inline script and 'src' attribute");
        }
        
        if (content != null && !content.isEmpty()) {
            script = new Script(attributes, nodeLocation, content);
        } else {
            script = new Script(attributes, nodeLocation);
        }
        
        return script;
    }
}
