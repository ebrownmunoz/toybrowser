/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.io.Playable;
import com.voxware.browser.io.TextPlayable;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.text.Text;
import com.voxware.browser.model.text.TextValue;

/**
 * @author edh
 */
public class ExecutableListParser implements ExecutableParser<ExecutableList> {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private static Logger log = LoggerFactory.getLogger(ExecutableListParser.class);

    // notice elseif and else are not here, they never appear outside of an if clause
    private static final List<String> EXECUTABLE_TAGS = Arrays.asList("audio", "assign", "clear", "disconnect", "exit", "goto", "if", "log", "prompt", "reprompt", "return", "script", "submit", "throw", "var");

    private final AudioParser audioParser;
    private final AssignParser assignParser;
    private final ClearParser clearParser;
    // disconnect not supported
    private final GotoParser gotoParser;
    private final IfParser ifParser;
    private final LogParser logParser;
    private final PromptParser promptParser;
    private final RepromptParser repromptParser;
    private final ReturnParser returnParser;
    private final ScriptParser scriptParser;
    private final SubmitParser submitParser;
    private final CompoundPlayableParser textParser;
    private final ThrowParser throwParser;
    private final VarParser varParser;
    private boolean hold = false;

    /**
     * Constructs a new <code>ExecutableListParser</code> instance.
     * 
     * @param audioParser
     * @param assignParser
     * @param clearParser
     * @param gotoParser
     * @param ifParser
     * @param logParser
     * @param promptParser
     * @param repromptParser
     * @param returnParser
     * @param scriptParser
     * @param submitParser
     * @param throwParser
     * @param varParser
     */
    @Inject
    public ExecutableListParser(AudioParser audioParser, AssignParser assignParser, ClearParser clearParser, GotoParser gotoParser, IfParser ifParser, LogParser logParser, PromptParser promptParser, RepromptParser repromptParser, ReturnParser returnParser, ScriptParser scriptParser,
        SubmitParser submitParser, CompoundPlayableParser textParser, ThrowParser throwParser, VarParser varParser) {
        this.audioParser = audioParser;
        this.assignParser = assignParser;
        this.clearParser = clearParser;
        this.gotoParser = gotoParser;
        this.ifParser = ifParser;
        this.logParser = logParser;
        this.promptParser = promptParser;
        this.repromptParser = repromptParser;
        this.returnParser = returnParser;
        this.scriptParser = scriptParser;
        this.submitParser = submitParser;
        this.textParser = textParser;
        this.throwParser = throwParser;
        this.varParser = varParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.Parser#parse(com.voxware.browser.parser.v2.ParsingContext)
     */
    public ExecutableList parse(ParsingContext context) throws ParserException {
        List<Executable> executables = new LinkedList<Executable>();
        XmlPullParser parser = context.getXmlPullParser();
        try {
            int eventType = parser.next();
            while (eventType != XmlPullParser.END_TAG) { // process <form>
                Executable exec = dispatchAndParse(context);
                if (exec != null) {
                    executables.add(exec);
                }
                if (hold) {
                    hold = false;
                    eventType = parser.getEventType();
                } else {
                    eventType = parser.next();
                }
            } // while
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
            throw context.newParserException(e);
        }
        return new ExecutableList(executables);
    }

    /**
     * Dispatches on a tag name or event type to the appropriate parser.
     * 
     * NOTE: Set the hold flag is you grab the next eventType (e.g. if
     * you want to check for a value tag).
     * 
     * @param context the context
     * @return an element, or null if ignorable text was encountered
     * @throws ParserException on error
     */
    protected Executable dispatchAndParse(ParsingContext context) throws ParserException {
        XmlPullParser parser = context.getXmlPullParser();
        int eventType;
        try {
            eventType = parser.getEventType();
            if (eventType == XmlPullParser.TEXT) {
                if (!parser.isWhitespace()) {
                    CompositePlayable cp;
                    TextPlayable text = new TextPlayable(parser.getText());
                    
                    List<Playable> plist = new ArrayList<Playable> ();
                    plist.add(text);
                    
                    cp = new CompositePlayable(plist);
                    
                    Map<String, String> empty = Collections.emptyMap();
                    return new Prompt(empty, context.getNodeLocation(), cp);
                } else {
                    return null; // this means it is whitespace
                }
            } else if (eventType == XmlPullParser.START_TAG) {

            	/*
                 * TODO REMOVE THIS CODE: This is for backwards compatibility for a bug
                 * in VMS VXML (which is is accepted by the old browser). A <block>
                 * tag in an executable context (i.e. <catch>) will be ignored with
                 * all of its children.
                 */
            	if ("block".equals(parser.getName())) {
            		log.warn("<block> found in executable context. Ignored for 5.2 compatibility");
            		while ((eventType != XmlPullParser.END_TAG) || (!"block".equals(parser.getName()))) {
            			eventType = parser.next();
            		}
            		return null;
            	}

            	
                ExecutableParser<?> subParser = selectParser(parser.getName());             
                
                if (subParser != null) {
                    return subParser.parse(context);
                } else {
                    throw context.newParserException("unknown tag encountered: " + parser.getName());
                }
            }
            throw context.newParserException("tag must be one of " + EXECUTABLE_TAGS + " but was " + parser.getName());
        } catch (XmlPullParserException e) {
            throw context.newParserException(e);
        } catch (IOException e) {
			throw context.newParserException(e);
		}
    }

    /**
     * Selects a parser based on <code>tagName</code>.
     * @param tagName the tag name
     * @return a parser or null if unhandled
     */
    protected ExecutableParser<?> selectParser(String tagName) {
        if ("audio".equals(tagName)) {
            return audioParser;
        } else if ("assign".equals(tagName)) {
            return assignParser;
        } else if ("clear".equals(tagName)) {
            return clearParser;
        } else if ("goto".equals(tagName)) {
            return gotoParser;
        } else if ("if".equals(tagName)) {
            return ifParser;
        } else if ("log".equals(tagName)) {
            return logParser;
        } else if ("prompt".equals(tagName)) {
            return promptParser;
        } else if ("reprompt".equals(tagName)) {
            return repromptParser;
        } else if ("return".equals(tagName)) {
            return returnParser;
        } else if ("script".equals(tagName)) {
            return scriptParser;
        } else if ("submit".equals(tagName)) {
            return submitParser;
        } else if ("throw".equals(tagName)) {
            return throwParser;
        } else if ("var".equals(tagName)) {
            return varParser;
        } else {
            // throw new ParserException("Unknown Executable Tag " + tagName, filename, parser.getLineNumber());
            return null;
        }
    }
}
