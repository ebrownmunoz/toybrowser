/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.documentserver.CacheControl;

/**
 * @author edh
 * @param <T>
 */
public abstract class AbstractFetchableTagParser<T> extends AbstractTagParser<T> implements Parser<T> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    public static final String FETCH_HINT_SAFE = "safe";
    public static final String FETCH_HINT_PREFETCH = "prefetch";

    private static final List<String> FETCH_HINT_VALUES = Arrays.asList(FETCH_HINT_SAFE, FETCH_HINT_PREFETCH);

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext,
     * java.util.Map)
     */
    @Override
    protected T parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        String fetchHint = attributes.get("fetchhint");
        if (fetchHint == null) {
            fetchHint = FETCH_HINT_SAFE;
        }
        String fetchTimeoutStr = attributes.get("fetchtimeout");
        if (!FETCH_HINT_VALUES.contains(fetchHint)) {
            throw context.newParserException("fetchhint attribute must be 'safe' or 'prefetch'");
        }
        Long fetchTimeout = null;
        
        if (fetchTimeoutStr != null) {
        	fetchTimeout = parseFetchTimeout(context, fetchTimeoutStr);
        }

        return parseTagContent(context, attributes, fetchHint, fetchTimeout, extractCacheControl(context, attributes));
    }

    /**
     * @param fetchTimeoutStr
     * @return
     */
    protected Long parseFetchTimeout(ParsingContext context, String fetchTimeoutStr) throws ParserException {
        if (fetchTimeoutStr == null) {
            return null;
        }
        try {
            if (fetchTimeoutStr.endsWith("ms")) {
                return Long.valueOf(fetchTimeoutStr.substring(0, fetchTimeoutStr.length() - 1));
            } else if (fetchTimeoutStr.endsWith("s")) {
                return 1000 * Long.valueOf(fetchTimeoutStr.substring(0, fetchTimeoutStr.length() - 1));
            } else {
                return Long.valueOf(fetchTimeoutStr.substring(0, fetchTimeoutStr.length() - 1));
            }
        } catch (NumberFormatException e) {
            throw context.newParserException("error parsing timeout", e);
        }
    }

    /**
     * Extracts the cache control attributes and constructs a CacheControl object to represent them.
     * 
     * @param context the parsing context
     * @param attributes the attribute map
     * @return a CacheControl object
     * @throws ParserException on error
     */
    protected CacheControl extractCacheControl(ParsingContext context, Map<String, String> attributes) throws ParserException {
        String caching = attributes.get("caching");
        if (caching != null && caching.equals("safe")) {
            // never cache
            return CacheControl.NO_CACHE;
        } else {
            String maxAgeStr = attributes.get("maxage");
            String maxStaleStr = attributes.get("maxstale");
            Long maxAge = null;
            Long maxStale = null;
            try {
                if (maxAgeStr != null) {
                    maxAge = Long.valueOf(maxAgeStr);
                }
            } catch (NumberFormatException e) {
                throw context.newParserException("error parsing maxAge", e);
            }
            try {
                if (maxStaleStr != null) {
                    maxStale = Long.valueOf(maxStaleStr);
                }
            } catch (NumberFormatException e) {
                throw context.newParserException("error parsing maxStale", e);
            }
            return new CacheControl(maxAge, maxStale);
        }
    }

    /**
     * Handles the tag content for elements that can be fetched, like {@literal<grammar>}; or {@literal<speaker>};.
     * 
     * @param context the parsing context
     * @param attributes the attributes
     * @param fetchHint the parsed fetch hint, defaults to "safe" if one does not exist
     * @param fetchtimeout the parsed fetch timeout, defaults to null if one does not exist
     * @param cacheControl the parsed cache control values, defaults to CacheControl.NO_CACHE
     * @return
     * @throws ParserException
     */
    protected abstract T parseTagContent(ParsingContext context, Map<String, String> attributes, String fetchHint, Long fetchtimeout, CacheControl cacheControl) throws ParserException;
}
