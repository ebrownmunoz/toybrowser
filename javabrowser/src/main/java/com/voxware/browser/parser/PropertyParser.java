/**
 * 
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.context.property.Property;

/**
 * @author eric
 *
 */
public class PropertyParser extends AbstractTagParser<Property> {

    /**
     * 
     */
    @Inject
    public PropertyParser() {
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "property";
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.AbstractTagParser#parseTagContent(com.voxware.browser.parser.ParsingContext, java.util.Map)
     */
    @Override
    protected Property parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        Property property = new Property(attributes, context.getNodeLocation());
        return property;
    }
}
