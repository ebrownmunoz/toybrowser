/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.Log;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.text.Text;

/**
 * LogParser
 *
 * Created: Oct 14, 2015
 * @author edh
 */
public class LogParser extends AbstractTagParser<Log> implements ExecutableParser<Log> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final CompoundPlayableParser compoundPlayableParser;
    
    /**
     * Constructs a new <code>LogParser</code> instance.
     */
    @Inject
    public LogParser(CompoundPlayableParser textParser) {
        this.compoundPlayableParser = textParser;
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "log";
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    @Override
    protected Log parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        CompositePlayable text = null;
        int eventType = parser.next();
        // we don't use the usual while loop since there should only be text
        // any other child elements would be an error
        if (eventType != XmlPullParser.END_TAG) {
            // it should be either text or a <value...> tag
            // anything else would provoke an error from the parser
            text = compoundPlayableParser.parse(context);
            // we want the current event type, not the next one
            // the text parser would have advanced passed the text to the END_TAG
            eventType = parser.getEventType();
        } else if (parser.getName().equals(getTagName())) {
            // we are at an end tag and it is </prompt>
            // handles the case of <prompt ...></prompt>
        } 
//        if (!parser.getName().equals("prompt")) {
//            throw new XmlPullParserException("Unexpected END_TAG for " + parser.getName());
//        }
        
        
        Log log = new Log(attributes, nodeLocation, text);
        return log;
    }
}
