/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.context.property.Property;
import com.voxware.browser.event.Catch;
import com.voxware.browser.model.InterruptFieldNode;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.Log;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.form.Field;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * @author edh
 */
public class FieldParser extends AbstractTagParser<Field> {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private final CatchParser catchParser;
    private final FilledParser filledParser;
    private final GrammarParser grammarParser;
    private final PromptParser promptParser;
    private final PropertyParser propertyParser;
 	private final NoInputParser noinputParser;
 	private final NoMatchParser nomatchParser;
 	private final InterruptParser interruptParser;
 	private final SpeakerParser speakerParser;
 	private final LogParser logParser;
   
    /**
     * Constructs a new <code>FieldParser</code> instance.
     * 
     * @param catchParser catch parser
     * @param filledParser filled parser
     * @param grammarParser grammar parser
     * @param promptParser prompt parser
     * @param propertyParser property parser
     * @param noInputParser noinput parser
     * @param noMatchParser nomatch parser
     * @param interruptParser interrupt parser
     */
    @Inject
    public FieldParser(CatchParser catchParser, FilledParser filledParser, GrammarParser grammarParser, 
        PromptParser promptParser, PropertyParser propertyParser, NoInputParser noInputParser, NoMatchParser noMatchParser,
        InterruptParser interruptParser, SpeakerParser speakerParser, LogParser logParser) {
        this.catchParser = catchParser;
        this.filledParser = filledParser;
        this.grammarParser = grammarParser;
        this.promptParser = promptParser;
		this.propertyParser = propertyParser;
		this.noinputParser = noInputParser;
		this.nomatchParser = noMatchParser;
		this.interruptParser = interruptParser;
		this.speakerParser = speakerParser;
		this.logParser = logParser;
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "field";
    }

    /*
     * (non-Javadoc)
     * @see
     * com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext,
     * java.util.Map)
     */
    @Override
    protected Field parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        XmlPullParser parser = context.getXmlPullParser();
        List<Prompt> prompts = new LinkedList<Prompt>();
        List<Grammar> grammars = new LinkedList<Grammar>();
        List<Catch> catches = new LinkedList<Catch>();
        List<Filled> filleds = new LinkedList<Filled>();
        List<Property> properties = new LinkedList<Property>();
        List<InterruptFieldNode> interrupts = new LinkedList<InterruptFieldNode>();
        List<Log> loggers = new LinkedList<Log>();
        Speaker speaker = null;
        
        int eventType = parser.next();
        while (eventType != XmlPullParser.END_TAG) { // process <form>
            if (eventType == XmlPullParser.TEXT) {
                if (!parser.isWhitespace()) {
                    log.warn("Unexpected text found in <field> (ignored");
                }
            } else if (eventType == XmlPullParser.START_TAG) {
                String tagName = parser.getName();

                if ("prompt".equals(tagName)) {
                    Prompt prompt = promptParser.parse(context);
                    prompts.add(prompt);
                } else if ("grammar".equals(tagName)) {
                    Grammar grammar = grammarParser.parse(context);
                    grammars.add(grammar);
                } else if ("catch".equals(tagName)) {
                    Catch katch = catchParser.parse(context);
                    catches.add(katch);
                } else if ("filled".equals(tagName)) {
                    Filled filled = filledParser.parse(context);
                    filleds.add(filled);
                } else if ("noinput".equals(tagName)) {
                    Catch katch = noinputParser.parse(context);
                    catches.add(katch);
                } else if ("nomatch".equals(tagName)) {
                    Catch katch = nomatchParser.parse(context);
                    catches.add(katch);
                } else if ("property".equals(tagName)) {
                    Property property = propertyParser.parse(context);
                    properties.add(property);
                } else if ("interrupt".equals(tagName)) {
                    InterruptFieldNode interrupt= interruptParser.parse(context);
                    interrupts.add(interrupt);
                } else if ("speaker".equals(tagName)) { 
                    speaker = speakerParser.parse(context);
                } else if ("log".equals(tagName)) {
                    Log logger = logParser.parse(context);
                    loggers.add(logger);
                } else {
                    log.warn("unhandled child tag " + tagName + " in <field>.");
                }
            } else {
                log.warn("unhandled event type " + eventType + " name: " + parser.getName());
            }
            eventType = parser.next();
        } // while
        Field field = new Field(attributes, nodeLocation, prompts, speaker, grammars, catches, filleds, properties, interrupts, loggers);
        return field;
    }
}
