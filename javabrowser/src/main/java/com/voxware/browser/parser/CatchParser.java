/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.event.Catch;
import com.voxware.browser.model.ExecutableList;
import com.voxware.browser.model.NodeLocation;

/**
 * @author edh
 *
 */
public class CatchParser extends AbstractTagParser<Catch> {

    private final ExecutableListParser executableParser;
    /**
     * 
     */
    @Inject
    public CatchParser(ExecutableListParser executableParser) {
        this.executableParser = executableParser;
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "catch";
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    @Override
    protected Catch parseTagContent(ParsingContext context, Map<String, String> attributes) throws XmlPullParserException, IOException, ParserException {
        final NodeLocation nodeLocation = context.getNodeLocation();
        ExecutableList executables = executableParser.parse(context);
        return new Catch(attributes, nodeLocation, executables);
    }
}
