/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.util.Map;

import javax.inject.Inject;

import com.voxware.browser.model.executable.Reprompt;

/**
 * RepromptParser
 *
 * Created: Oct 14, 2015
 * @author edh
 */
public class RepromptParser extends AbstractTagParser<Reprompt> implements ExecutableParser<Reprompt> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>RepromptParser</code> instance.
     */
    @Inject
    public RepromptParser() {
    }
    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "reprompt";
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    @Override
    protected Reprompt parseTagContent(ParsingContext context, Map<String, String> attributes) {
        Reprompt reprompt =  new Reprompt(attributes, context.getNodeLocation());
        return reprompt;
    }

}
