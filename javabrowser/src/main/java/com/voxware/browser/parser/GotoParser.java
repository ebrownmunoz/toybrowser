/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.util.Map;

import javax.inject.Inject;

import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.model.executable.Goto;

/**
 * @author edh
 */
public class GotoParser extends AbstractFetchableTagParser<Goto> implements ExecutableParser<Goto> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    /**
     * Constructs a new <code>GotoParser</code> instance.
     */
    @Inject
    public GotoParser() {
    }
    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "goto";
    }
    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractFetchableTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map, java.lang.String, java.lang.Long, com.voxware.browser.documentserver.CacheControl)
     */
    @Override
    protected Goto parseTagContent(ParsingContext context, Map<String, String> attributes, String fetchHint, Long fetchtimeout, CacheControl cacheControl) throws ParserException {
        Goto retval = new Goto(attributes, context.getNodeLocation());
        return retval;
    }
}
