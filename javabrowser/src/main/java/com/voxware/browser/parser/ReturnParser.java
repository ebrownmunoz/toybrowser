/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.util.Map;

import javax.inject.Inject;

import com.voxware.browser.model.executable.Return;

/**
 * ReturnParser
 * 
 * @author edh
 */
public class ReturnParser extends AbstractTagParser<Return> implements ExecutableParser<Return> {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>ReturnParser</code> instance.
     */
    @Inject
    public ReturnParser() {
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "return";
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.v2.impl.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.
     * ParsingContext, java.util.Map)
     */
    @Override
    protected Return parseTagContent(ParsingContext context, Map<String, String> attributes) {
        Return ret = new Return(attributes, context.getNodeLocation());
        return ret;
    }
}
