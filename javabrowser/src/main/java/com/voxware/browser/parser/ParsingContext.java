/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.util.Arrays;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.VxmlNode;

/**
 * Represents a parsing context. Encapsulates a SessionContext, XmlPullParser, and URI.
 * 
 * @author edh
 */
public class ParsingContext {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * A position changing method is a method in XmlPullParser that changes
     * the tag we are parsing. This is used to support mark() and positionChanged()
     */
    private final SessionContext sessionContext;
    private final XmlPullParser xmlPullParser;
    private final URI uri;

    private int markedIndex = -1;
    private int eventIndex = 0;

    /**
     * Constructs a ParsingContext from the constituent parts
     * 
     * @param sessionContext the session context
     * @param xmlPullParser the parser
     * @param uri the URI of the document being parsed
     */
    public ParsingContext(SessionContext sessionContext, XmlPullParser xmlPullParser, URI uri) {
        try {
            xmlPullParser.setProperty("http://xmlpull.org/v1/doc/properties.html#location", uri);
        } catch (XmlPullParserException e) {
            // must not be supported in this implementation
        }
        this.sessionContext = sessionContext;
        this.xmlPullParser = wrap(xmlPullParser);
        this.uri = uri;
    }

    /**
     * Gets the session context
     * 
     * @return a SessionContext
     */
    public SessionContext getSessionContext() {
        return sessionContext;
    }

    /**
     * Gets the parser
     * 
     * @return an XmlPullParser
     */
    public XmlPullParser getXmlPullParser() {
        return xmlPullParser;
    }

    /**
     * Gets the URI location of the document being parsed.
     * 
     * @return a URI
     */
    public URI getURI() {
        return uri;
    }

    /**
     * Gets the node location.
     * 
     * @return a NodeLocation
     */
    public NodeLocation getNodeLocation() {
        NodeLocation nodeLocation = new NodeLocation(uri, xmlPullParser.getLineNumber(), xmlPullParser.getColumnNumber(), xmlPullParser.getPositionDescription());
        return nodeLocation;
    }

    /**
     * Convenience method for generating a ParserException. Erases the call to <code>newParserException</code> from the
     * stack trace so that it looks inlined.
     * 
     * @param description the exception description
     * @return a ParserException
     */
    public ParserException newParserException(String message) {
        ParserException e = new ParserException(message, getNodeLocation());
        // erase this particular call
        StackTraceElement[] stackTrace = e.getStackTrace();
        stackTrace = Arrays.copyOfRange(e.getStackTrace(), 1, stackTrace.length);
        e.setStackTrace(stackTrace);
        return e;
    }

    /**
     * Convenience method for generating a ParserException. Erases the call to <code>newParserException</code> from the
     * stack trace so that it looks inlined.
     * 
     * @param description the exception description
     * @param cause the underlying cause
     * @return a ParserException
     */
    public ParserException newParserException(String message, Throwable cause) {
        ParserException e = new ParserException(message, cause, getNodeLocation());
        // erase this particular call
        StackTraceElement[] stackTrace = e.getStackTrace();
        stackTrace = Arrays.copyOfRange(e.getStackTrace(), 1, stackTrace.length);
        e.setStackTrace(stackTrace);
        return e;
    }

    /**
     * Convenience method for generating a ParserException. Erases the call to <code>newParserException</code> from the
     * stack trace so that it looks inlined.
     * 
     * @param cause the underlying cause
     * @return a ParserException
     */
    public ParserException newParserException(Throwable cause) {
        ParserException e = new ParserException(cause, getNodeLocation());
        // erase this particular call
        StackTraceElement[] stackTrace = e.getStackTrace();
        stackTrace = Arrays.copyOfRange(e.getStackTrace(), 1, stackTrace.length);
        e.setStackTrace(stackTrace);
        return e;
    }

    /**
     * Marks the current position is the event stream.
     */
    public void mark() {
        markedIndex = eventIndex;
    }

    /**
     * Returns true if position has changed
     * @return true if position has changed, false otherwise
     */
    public boolean positionChanged() {
        return markedIndex != eventIndex;
    }

    private XmlPullParser wrap(final XmlPullParser xmlPullParser) {
        return new XmlPullParserInterceptor(xmlPullParser);
    }

    private class XmlPullParserInterceptor implements XmlPullParser {
        private final XmlPullParser delegate;

        public XmlPullParserInterceptor(final XmlPullParser delegate) {
            this.delegate = delegate;
        }

        public void defineEntityReplacementText(String arg0, String arg1) throws XmlPullParserException {
            delegate.defineEntityReplacementText(arg0, arg1);
        }

        public int getAttributeCount() {
            return delegate.getAttributeCount();
        }

        public String getAttributeName(int arg0) {
            return delegate.getAttributeName(arg0);
        }

        public String getAttributeNamespace(int arg0) {
            return delegate.getAttributeNamespace(arg0);
        }

        public String getAttributePrefix(int arg0) {
            return delegate.getAttributePrefix(arg0);
        }

        public String getAttributeType(int arg0) {
            return delegate.getAttributeType(arg0);
        }

        public String getAttributeValue(int arg0) {
            return delegate.getAttributeValue(arg0);
        }

        public String getAttributeValue(String arg0, String arg1) {
            return delegate.getAttributeValue(arg0, arg1);
        }

        public int getColumnNumber() {
            return delegate.getColumnNumber();
        }

        public int getDepth() {
            return delegate.getDepth();
        }

        public int getEventType() throws XmlPullParserException {
            return delegate.getEventType();
        }

        public boolean getFeature(String arg0) {
            return delegate.getFeature(arg0);
        }

        public String getInputEncoding() {
            return delegate.getInputEncoding();
        }

        public int getLineNumber() {
            return delegate.getLineNumber();
        }

        public String getName() {
            return delegate.getName();
        }

        public String getNamespace() {
            return delegate.getNamespace();
        }

        public String getNamespace(String arg0) {
            return delegate.getNamespace(arg0);
        }

        public int getNamespaceCount(int arg0) throws XmlPullParserException {
            return delegate.getNamespaceCount(arg0);
        }

        public String getNamespacePrefix(int arg0) throws XmlPullParserException {
            return delegate.getNamespacePrefix(arg0);
        }

        public String getNamespaceUri(int arg0) throws XmlPullParserException {
            return delegate.getNamespaceUri(arg0);
        }

        public String getPositionDescription() {
            return delegate.getPositionDescription();
        }

        public String getPrefix() {
            return delegate.getPrefix();
        }

        public Object getProperty(String arg0) {
            return delegate.getProperty(arg0);
        }

        public String getText() {
            return delegate.getText();
        }

        public char[] getTextCharacters(int[] arg0) {
            return delegate.getTextCharacters(arg0);
        }

        public boolean isAttributeDefault(int arg0) {
            return delegate.isAttributeDefault(arg0);
        }

        public boolean isEmptyElementTag() throws XmlPullParserException {
            return delegate.isEmptyElementTag();
        }

        public boolean isWhitespace() throws XmlPullParserException {
            return delegate.isWhitespace();
        }

        public int next() throws XmlPullParserException, IOException {
            eventIndex++;
            return delegate.next();
        }

        public int nextTag() throws XmlPullParserException, IOException {
            eventIndex++;
            return delegate.nextTag();
        }

        public String nextText() throws XmlPullParserException, IOException {
            eventIndex++;
            return delegate.nextText();
        }

        public int nextToken() throws XmlPullParserException, IOException {
            eventIndex++;
            return delegate.nextToken();
        }

        public void require(int arg0, String arg1, String arg2) throws XmlPullParserException, IOException {
            delegate.require(arg0, arg1, arg2);
        }

        public void setFeature(String arg0, boolean arg1) throws XmlPullParserException {
            delegate.setFeature(arg0, arg1);
        }

        public void setInput(InputStream arg0, String arg1) throws XmlPullParserException {
            delegate.setInput(arg0, arg1);
        }

        public void setInput(Reader arg0) throws XmlPullParserException {
            delegate.setInput(arg0);
        }

        public void setProperty(String arg0, Object arg1) throws XmlPullParserException {
            delegate.setProperty(arg0, arg1);
        }
    }
}
