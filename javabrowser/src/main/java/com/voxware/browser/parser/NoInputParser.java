/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.parser;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.xmlpull.v1.XmlPullParserException;

import com.voxware.browser.event.Catch;

/**
 * NoInputParser
 *
 * @author edh
 */
public class NoInputParser extends CatchParser {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    /**
     * Constructs a new <code>NoInputParser</code> instance.
     * 
     * @param executableParser dependency
     */
    @Inject
    public NoInputParser(ExecutableListParser executableParser) {
        super(executableParser);
    }

    /*
     * (non-Javadoc)
     * @see com.voxware.browser.parser.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "noinput";
    }

    /* 
     * (non-Javadoc)
     * @see com.voxware.browser.parser.CatchParser#parseTagContent(com.voxware.browser.parser.ParsingContext, java.util.Map)
     */
    @Override
    protected Catch parseTagContent(ParsingContext context, Map<String, String> attributes)
        throws XmlPullParserException, IOException, ParserException {
        Map<String, String> newAttributes = new LinkedHashMap<String, String>(attributes);
        newAttributes.put("event", "noinput");
        return super.parseTagContent(context, newAttributes);
    }
}
