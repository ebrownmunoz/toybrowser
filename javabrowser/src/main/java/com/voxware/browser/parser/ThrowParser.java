/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */

package com.voxware.browser.parser;

import java.util.Map;

import javax.inject.Inject;

import com.voxware.browser.model.executable.Throw;

/**
 * Parses a {@literal<throw>} tag.
 *
 * Created: Oct 13, 2015
 * @author edh
 */
public class ThrowParser extends AbstractTagParser<Throw> implements ExecutableParser<Throw> {
    
    /**
     * Constructs a new <code>ThrowParser</code> instance.
     */
    @Inject
    public ThrowParser() {
    }
    /* (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#getTagName()
     */
    @Override
    protected String getTagName() {
        return "throw";
    }

    /* (non-Javadoc)
     * @see com.voxware.browser.parser.v2.AbstractTagParser#parseTagContent(com.voxware.browser.parser.v2.ParsingContext, java.util.Map)
     */
    @Override
    protected Throw parseTagContent(ParsingContext context, Map<String, String> attributes) {
        Throw ret = new Throw(attributes, context.getNodeLocation());
        return ret;
    }
}
