/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;

/**
 * Represents a series of TextPlayable and ValuePlayable
 * 
 * @author edh
 */
public class CompositePlayable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";

    private List<Playable> playableNodes;

    /**
     * Constructs a new <code>CompositePlayable</code> instance.
     * 
     * @param textNodes
     */
    public CompositePlayable(List<Playable> playableNodes) {
        if (playableNodes == null) {
            throw new NullPointerException("textNodes cannot be null");
        }
        this.playableNodes = playableNodes;
    }

    /**
     * A convenience Constructor to construct a list out of the given string
     * @param text
     */
    public CompositePlayable(String text) {
        List<Playable> playables = new ArrayList<Playable>();
        
        playables.add(new TextPlayable(text));
        
        this.playableNodes = playables;
    }
    /*
     * (non-Javadoc)
     * @see com.voxware.browser.model.text.Text#getString(com.voxware.browser.context.SessionContext,
     * org.mozilla.javascript.Scriptable)
     */
    public String getString(SessionContext scontext, BaseContext context) {
        StringBuilder sb = new StringBuilder();
        for (Playable each : playableNodes) {
            String text = each.getText(scontext, context);
            
            if (text != null) {
                sb.append(text);
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Playable each : playableNodes) {
            String text = each.toString();
            
            if (text != null) {
                sb.append(text);
            }
        }
        return sb.toString();
    }
    
    public List<Playable> getPlayables() {
        return Collections.unmodifiableList(playableNodes);
    }
}
