/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.util.concurrent.Future;

import com.voxware.browser.context.BrowserEvent;

public interface Recognizer {
    
    public Future<RecognitionResult> recognize(String rule) throws BrowserEvent;
    
    /**
     * Creates a dummy RecognitionResult when only interrupts are allowed. 
     * @return
     */
    public Future<RecognitionResult> recognizeWithInterruptsOnly();
    /**
     * Cancels all recognition requests.
     */
    public void cancel();
    /**
     * Loads a grammar into the recognizer
     * @param id the id
     * @param grammarBytes the grammar bytes
     * @param forceUpdate force the grammar to update even if the id is the same
     */
    public void loadGrammar(String id, byte[] grammarBytes, boolean forceUpdate);
    public void loadVoiceModel(String id, byte[] voiBytes, boolean forceUpdate);
    public void setParameter(String name, Object value);
    public Object getParameter(String name);
    
    /**
     * Send a recognition result to interrupt the recognition. This
     * will cause the recognizer to stop and discard the current recognition
     * and set the result in the Future to the given result.
     * 
     * @param rresult
     */
    public void sendInterrupt(RecognitionResult rresult);
    
    /**
     * Stops the recognizer and releases resources. The recognizer instance should be discarded after this method is called.
     */
    public void shutdown();
}
