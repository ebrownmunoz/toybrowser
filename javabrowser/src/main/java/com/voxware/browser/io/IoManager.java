/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.util.List;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.resource.Grammar;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Jun 3, 2015 by: eric</li>
 * </ul>
 */
public class IoManager {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private Recognizer recognizer;
    private AudioPlayer audioPlayer;

    /**
     * Constructs a new <code>IoManager</code> instance.
     */
    public IoManager() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Returns the recognizer.
     * @return the recognizer.
     */
    public Recognizer getRecognizer() {
        return recognizer;
    }

    /**
     * Sets the recognizer.
     * @param recognizer the recognizer.
     */
    public void setRecognizer(Recognizer recognizer) {
        this.recognizer = recognizer;
    }

    /**
     * Returns the audioPlayer.
     * @return the audioPlayer.
     */
    public AudioPlayer getAudioPlayer() {
        return audioPlayer;
    }

    /**
     * Sets the audioPlayer
     * @param tts the audioPlayer.
     */
    public void setAudioPlayer(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;
    }

    /**
     * Set the active grammars
     * @param grammars
     */
    public void setGrammar(List<Grammar> grammars) {
        // TODO Auto-generated method stub
        // Grammars are no-ops
        
    }
    
    
    

}
