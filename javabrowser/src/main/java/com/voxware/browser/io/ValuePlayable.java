/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mozilla.javascript.NativeJavaArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.audioplayer.PromptResolver;
import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.util.nist.NISTFile;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Jun 3, 2015 by: eric</li>
 * </ul>
 */
public class ValuePlayable implements Playable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "(c) Copyright Voxware Inc. 2015.";
    
    private static Logger log = LoggerFactory.getLogger(ValuePlayable.class);
    private final String expr;
    private final String recsrc; // This will not be a String in its final form
    

    /**
     * Constructs a new <code>TextPlayable</code> instance.
     * @param text the text to play.
     */
    public ValuePlayable(String expr, String recsrc) {
        this.expr = expr;
        this.recsrc = recsrc;
    }


    @Override
    public String getText(SessionContext scontext, BaseContext context) {
        return context.evaluateToString(expr);
    }

    @Override
    public List<Object> getAudio(SessionContext scontext, BaseContext context) {
    	
    	List<Object> retval = null;
    	
    	if (recsrc != null) {
    	    String text = getText(scontext, context);
    	    // now find the nearest file(s) in the recsrc
    	    PromptResolver resolver = scontext.getPromptResolver();
    	    if (resolver == null) {
    	        return new ArrayList<Object>(Arrays.asList(text));
    	    } else {
    	        return resolver.resolvePrompt(recsrc, text);
    	    }
    	} else {
    		Object jsobject = context.evaluate(expr);
    		log.info("JSOBJECT " + jsobject);
    		if ((jsobject instanceof NativeJavaArray)) {
    			Object unwrappedObject = ((NativeJavaArray) jsobject).unwrap();
    			NISTFile nistFile = NISTFile.parse((byte[]) unwrappedObject);
    			if (nistFile != null) {
    				log.info("Prompt.getAudios() Adding Bytes as pcm"); 
    				retval = new ArrayList<Object>(1);
    				retval.add(nistFile.getPcmData());
    			}
    		} 
    	}
    	
    	return retval;
    } // getAudio
    
    public String toString() {
    	return "ValuePlayable: recsrc:" + recsrc + ", expr:" + expr;
    }
}
