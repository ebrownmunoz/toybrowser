/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.util.List;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;

public interface Playable {
    
    // TODO: refactor this to have only one method, since a List<Object> can contain strings or byte arraus etc
    public String getText(SessionContext scontext, BaseContext context);
    /**
     * Returns a list of playable elements. These include Strings, byte arrays (byte[]), or 
     * @param scontext
     * @param context
     * @return
     */
    public List<Object> getAudio (SessionContext scontext, BaseContext context);
}
