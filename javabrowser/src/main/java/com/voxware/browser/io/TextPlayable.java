/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Jun 3, 2015 by: eric</li>
 * </ul>
 */
public class TextPlayable implements Playable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private String text;

    /**
     * Constructs a new <code>TextPlayable</code> instance.
     * @param text the text to play.
     */
    public TextPlayable(String text) {
        this.text = text;
    }


    @Override
    public String getText(SessionContext scontext, BaseContext context) {
        return text;
    }

    @Override
    public List<Object> getAudio(SessionContext scontext, BaseContext context) {
        return new ArrayList<Object>(Arrays.asList(text));
    }

    @Override
    public String toString() {
        return "TextPlayable: " + text;
    }
}
