/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.voxware.browser.interpreter.Result;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Jun 3, 2015 by: eric</li>
 * </ul>
 */
public class RecognitionResult {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private static final String VISE_MODE = "voice";

    private static final String EVENT_MODE = "event";
    
    public enum Status {
        INITIALIZED,
        SUCCESS,
        EVENT     
    }
    
    private final Object recognizerSpecific;
    private final Status status;
    private final String inputMode;
    /**
     * Host translated result
     */
    private final String result;
    /**
     * Raw utterance tokens
     */
    private final List<List<String>> alternates;
    /**
     * Audio samples
     */
    private final byte[] audio;
    
    private final Result event;
  
    
    /**
     * Create a SUCCESS RecognitionResult
     * @param results
     * @return
     */
    public static RecognitionResult success(String result) {
        return new RecognitionResult(Status.SUCCESS, VISE_MODE, result, null, null, null);
    }
    
    public static RecognitionResult success(String source, String result) {
        return new RecognitionResult(Status.SUCCESS, source, result, null, null, null);
    }

    /**
     * Create an EVENT RecognitionResult
     * @param result
     * @return
     */

    public static RecognitionResult event(Result event) {
        return new RecognitionResult(EVENT_MODE, event);
    }
    
    
    /**
     * Constructs a new <code>RecognitionResult</code> instance.
     * @param recognizerSpecific
     * @param status
     * @param inputMode
     * @param result
     * @param alternates
     * @param audio
     * @param event
     */
    public RecognitionResult(Object recognizerSpecific, Status status, String inputMode, String result,
        List<List<String>> alternates, byte[] audio, Result event) {
        this.recognizerSpecific = recognizerSpecific;
        this.status = status;
        this.inputMode = inputMode;
        this.result = result;
        this.alternates = makeFinalAlternates(alternates);
        this.audio = audio;
        this.event = event;
    }

    /**
     * Constructs a new <code>RecognitionResult</code> instance.
     */
    protected RecognitionResult(Status status, String inputMode, String result, List<List<String>> alternates, byte[] audio, Object recognizerSpecific) {
        this.status = status;
        this.inputMode = inputMode;
        this.result = result;
        this.alternates = makeFinalAlternates(alternates);
        this.audio = audio;
        this.event = null;
        this.recognizerSpecific = recognizerSpecific;
    }
    
    protected RecognitionResult(String inputMode, Result event) {
        this.status = Status.EVENT;
        this.inputMode = inputMode;
        this.result = null;
        this.alternates = makeFinalAlternates(null);
        this.audio = null;
        this.event = event;
        this.recognizerSpecific = null;
    }

    /**
     * Returns the status.
     * @return the status.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the results.
     * @return the results.
     */
    public String getResult() {
        return result;
    }

    /**
     * Returns the event.
     * @return the event.
     */
    public Result getEvent() {
        return event;
    }
    
    /**
     * Returns the inputMode.
     * @return the inputMode.
     */
    public String getInputMode() {
        return inputMode;
    }

    /**
     * Returns the alternates.
     * @return the alternates.
     */
    public List<List<String>> getAlternates() {
        return alternates;
    }

    /**
     * Returns the audio.
     * @return the audio.
     */
    public byte[] getAudio() {
        return audio;
    }
    
    

    /**
     * Returns the recognizerSpecific.
     * @return the recognizerSpecific.
     */
    public Object getRecognizerSpecific() {
        return recognizerSpecific;
    }

    protected List<List<String>> makeFinalAlternates(List<List<String>> alternates) {
        if (alternates == null) {
            return Collections.emptyList();
        }
        List<List<String>> ret = new ArrayList<List<String>>(alternates.size());
        for (List<String> alternate : alternates) {
            if (alternate == null) {
                ret.add(null);
                continue;
            }
            List<String> retElem = new ArrayList<String>(alternate.size());
            retElem.addAll(alternate);
            ret.add(Collections.unmodifiableList(retElem));
        }
        return Collections.unmodifiableList(ret);
    }
    
    public String toString() {
    	
    	if (status == Status.EVENT) {
    		return "Result: Status=" + status + ", inputMode=" + inputMode + ", event=" + event + ", result=" + result;
    	} else {
    		return "Result: Status=" + status + ", inputMode=" + inputMode + ", result=" + result;
    	}
    }

}
