package com.voxware.browser.io;

import java.util.concurrent.Future;

import com.voxware.browser.audioplayer.AudioPlayerException;

public interface Player<T> {

    /**
     * Queues up content to be played
     * @param content to be played
     * @return a Future<Void>
     * @throws AudioPlayerException on queueing error, error during playback will be thrown as part of get()
     */
    public Future<Void> play(T content) throws AudioPlayerException;
    
    /**
     * Cancels current and pending speech
     */
    public void cancel();
    
    /**
     * Shuts down the player
     */
    public void shutdown();
    /**
     * Sets a TTS parameter
     * @param name the name of the parameter
     * @param value the value
     */
    public void setParameter(String name, Object value);
    /**
     * Gets a TTS parameter
     * @param name the name of the parameter
     * @return a value
     */
    public Object getParameter(String name);
}
