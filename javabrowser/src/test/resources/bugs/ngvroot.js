//application level prompts, used in root.vxml/disconnected.vxml/calibrate.vxml
//eventually will be set when user logs on based on user language
//default with english values so that messages can be played regardless of other errors

//globals
    var global_impossible='impossible!!';
    var global_special='special menu';
    var global_noinput='i heard nothing';
    var global_invalid_input='invalid input';
    var global_confirm=' confirm';
    var global_verify = 'verify ';
    var helpmileage ='meter number confirm';
    
    application.state = new Object();

//root.vxml
   var root_sysmon='Caught sysmonitor interrupt event';
   var root_generic='Caught generic event';
   var root_error='Caught Error';
   var root_forcelogoff='halted via vox console';
   
//special.vxml

   var special_special='special menu';
   var special_invalid='invalid option';
   //var special_invalidsay='invalid option. say special menu';

//calibrate.vxml
    var calibrate_prompt='Sampling background noise, please remain silent';
    var calibrate_error='Noise sampling failed!';
    var calibrate_success='Noise sampling complete.';

//disconscript.vxml
    var disconscript_verifycut='verify cut item';
    var disconscript_verifyskiploc='verify skip location';
    var disconscript_verifyskipaisle='verify skip aisle';
    var disconscript_verifyshort='verify short item';

    var disconscript_invalidcut='invalid cut request ';
    var disconscript_cutnotallow='cut not allowed ';
    var disconscript_invalidskip='invalid skip request ';
    var disconscript_shortnotallow='short not allowed ';
    var disconscript_skiplocnotallow='skip location not allowed ';
    var disconscript_skipaislenotallow='skip aisle not allowed ';


    var disconscript_checkdigit='check digit';
    var disconscript_invalidchk='invalid check digit ';
    var disconscript_cutreason='provide reason for CUT';
    var disconscript_skipreason='provide reason for SKIP';
    var disconscript_shortreason='provide reason for SHORT';

    var disconscript_invaliduom='Invalid product uom!';
    var disconscript_invalidbad='Invalid bad item!';
    var disconscript_invalidspecial='Invalid special instructions!';
    var disconscript_grabquantity='grab quantity';

    var disconscript_catchweight='catch weight of item';
    var disconscript_putwhatsay='say check position like check ALPHA or BRAVO or got it';
    var disconscript_cutwhatsay='say short item or cut item or got it';
    var disconscript_addverwhatsay='value for verification item';
    var disconscript_addcollwhatsay='value for collection attribute ';
    var disconscript_putwhatsay='say check position like check ALPHA or BRAVO or got it';
    var disconscript_outofrange='unit out of range please get in range';
    var disconscript_nobatch='unit out of range collections and catch weight not supported here';
    var disconscript_headsetwhatsay='headset volume up or down or calibrate';
    var disconscript_lunchwhatsay='lunch and break not allowed when unit out of range ! please get in range';

    var startServerCrash = null;

//logoff status
   var logoff_status='completed';    

    var dispatched = [];
    var fulfilled = void 0;
    
    var pick=null;
    var currentSessionID='';
    var back = [];
    var jsessionid = '';
    var warehouseID;
    var goaltime=0;
    var numtasks=0;
    var numitems=0;
    var startdiscon=0;
    var enddiscon=0;
    var prskcount=1;

    var promptsource='';

    var islogoffallowed='false';
    var isbreakallowed='false';
    var islunchallowed='false';
    var issuspendallowed='false';
    var isproductivityallowed='false';
    var ischangezoneallowed='false';
    var isaddcontainerallowed='false';
    var skipsallowed ='false';
    var ischangeequipmentallowed = 'false';
    var ischangecustomerallowed = 'false';
    var ischangeworktypeallowed = 'false';
    var islookaheadallowed = 'false';
    var islookbackallowed = 'false';
    var iscyclecountallowed = 'false';

    

    //pick flags
    var cutflag='false';
    var shortflag='false';
    var skipLflag='false';
    var skipAflag='false';
    var weightflag='false';
    var overpickflag='false';
    var cutcnt=0;
    var cutval='';
    var echopick='';

    //promt strings from vprompts
    var invalidgoback='';
    var  verifygoback='';
    var addnewcontprompt = '';
    var addnewcontwsay = '';
    var droplocwsay='';
    var closecontwsay ='';
    var locwsay='';
    var verifycut='';
    var invalidcut='';
    var verifyshort='';
    var invalidshort='';
    var verifyskipaisle='';
    var verifyskiplocation='';
    var invalidskipl='';
    var invalidskipa='';
    var pickwsay1='';
    var pickwsay2='';
    var pickstr='';
    var catchwsay='';
    var putwsay='';
    var skipwsay='';
    var cspromptlen='';
    var csprompt='';
         

	//prompt values
    var pickflagset=0;
    var catchprompt='';
    var putprompt='';
    var locprompt='';
    var usermode='';
    var pickprompt='';
    var pickqty='';
    var stageLocation='';
    var container='';
    var cfprompt='';
    var completionreason='';
    var weightprompt='';
    var callingprocess='';
    var closecontprompt='';
    var csusepos='';
    var csuseid='';
    var csusetype='';
    var cswsay='';
    var zwsay='';
    var wsay='';
    var prompt='';
    

    var selectedids = [];


    var speakervolume=42.0;

    //need to manipulate for location display
    var prevZone;
  	var prevAttrDesc = [];
  	var prevAttrValue = [];

  	var ttsspeed=1.15;
  	var ttspitch=0.1;
  	var ttsvolume=0.0;
     var sidetonevol = 0.0;
       	
     var state_id='';
     var pickedQty=0;
     var locchkdigit='';
     var dblocchk='';
     
     var locpromptnumber=0;
     var reason='';
     var status='';
     var completelocation='';
     var prodesc='';
     var totalpicked=0;
     var pickconf='';
     var picked='';
     var wcntitem=0;
     var prodWght=0.0;
     var verWghtflag=false;
     var reasonprompt='';
     var cutres='';
     var dropchk='';
     var pickoverpick='';
     var targetposition='';
	 var orgputprompt='';
     var specialinst='';
     var produom='';
     var addverprompt='';
     var orglocprompt='';
     var orgpickprompt='';


	var batchtasks = [];
    var nexttask=null;
    var batchtaskcount=0;
    var incrementqty=0;
    var verificationlist='';
    var collectionarray=[];
    var verifyarray = [];
    var dbverifyvalue='';
    var userverifyvalue='';
    var collectionlist='';
    var catchweightstr=null;
    var catchqty=0;
    var getweightqty=0;
    var itemspicked=0;	// items picked inthis pick ~~ pickedQty
    var taskwcnitem=0;	//individual count of task items
    var collectiondone='false';
    var collectionindex=0;
    var addattrvalue='';
    var attrarray=[];
    var attrresarray=[];
    var addattrstr=null;
    var collectionlength=0;
    var addattrprompt='';
    var addattrstr='';
    var gotweight='false';
    var targetposition='';
	var orgputprompt='';
    var promptcont='false';
    var activecontainer='';
    var putuowid='';
    var opencontainers='';
    var contconfirmation='';
    var putinfoarray=[];
    var request='';
    var invalidchk=0;
    var uom='';



    var confirmstr=' confirm';

    //vars and functions for vxml recognize subdialog
    var nohears = 0;
    var retries = 0;
    var echomisrecs = 0;
    var totalrecs = 0;
    
    function processFilledResponse(response)
    {
		application.nohears += response.nohears;
		application.retries += response.retries;
		application.echomisrecs += response.echomisrecs;
		application.totalrecs += 1;
    }

    function processNoHearResponse(nohearlimit)
    {
        application.nohears += nohearlimit;
        application.retries += nohearlimit;
    }
    
    //builds the default set of params. individual pages might need to set additional values
    function buildRecParams(grmStructure)
    {
		var paramObject = new Object();
		paramObject.profile=application.profile;
		paramObject.grammar=application.grammar+grmStructure;
		paramObject.prompt=application.prompt;
		paramObject.whatsay=application.whatsay;
		paramObject.headsetvolume=application.speakervolume;
		paramObject.calibrateprompt=application.calibrate_prompt;
		paramObject.nohearprompt=application.nohear_prompt;
		return paramObject;
     }

	 //get next task in list to process
	function nextPick() {
	 batchtasks.length=0;
	if(dispatched.length == 0 || state_id == 100)
	  {
	  	if (fulfilled == null) {
			state_id=100;
		}else {
			state_id=50;
		}
	  	return;
	  }
     while (dispatched.length > 0 ) {
        pick = dispatched.shift();
        if(typeof pick.taskid != "undefined")  { 
        	completelocation = pick.completelocation;
       	 	application.pickqty = pick.taskqty;
            uom=pick.uom;
			usermode=pick.usermode;
        	weightflag = pick.catchweighflag;
        	prodesc=pick.proddesc;
        	verificationlist = pick.verificationlist;
        	collectionlist = pick.collectionlist;
        	if(collectionlist != null && collectionlist != ''   )
        	{
        		var indexc = collectionlist.indexOf(",");
    			if(indexc > 0){
    				collectionarray = collectionlist.split(",");
    				collectionlength=collectionarray.length;
    			}else{
    				collectionlength=1;
				}
    		}
   	    	batchtaskcount = pick.numtasksatlocation;
			if (batchtaskcount > 0)
			{
				promptcont='true';
				//get the rows and make new array
				for (var i=0; i< batchtaskcount; i++)
				{
					nexttask = dispatched.shift();
					batchtasks.push(nexttask);
				}
				nexttask=null;
			} else { //single order
				//check if container needs to be prompted
				
				activecontainer=pick.activecontainer;
				opencontainers=pick.uowcontainer;
				contconfirmation=pick.contconfirmation;
				putuowid=pick.uowid;
				if(opencontainers != null && opencontainers.indexOf(",") > 0){
					promptcont='true';
				}else{
					promtcont='false';
				}
			}
        	break;
        }
  
      }
      
      if(pick.uowid == "0"){
		  state_id=100;	//no values set 
	  }else if (pick == null && fulfilled == null) {
      		state_id=100;	//not supported features
	  } else if( pick == null && fulfilled != null) {
      		state_id = 50;	//sned the data to server
	  }
      return pick;
    }
    
    function countTime() {
    	var now = new Date();
    	var endtm = now.getTime();
    	enddiscon = Math.round((endtm - startdiscon)*100/1000 )/100;
    	return;
    }

    function speakerdB(v) {
      return (48.0 * v) / 100.0;
    }

    function spacedout(s) {
      var t, r, i;
      t = "" + s; r = "";
      for (i = 0; i < t.length; i++) {
        r += t.charAt(i) + " ";
      }
      return r;
    }

	function convertAllToAlpha(s) {
        
        var result = "";

        for(i = 0; i < s.length; i++) {
            result = result + convertoAlpha(s.charAt(i));
        }
        
        return result;
    }
    function convertoAlpha(s) {
	 switch (s) {
		case 'A': 	return ("ALPHA"); break;
		case 'B': 	return ("BRAVO"); break;
		case 'C':	return ("CHARLIE"); break;
		case 'D':	return ("DELTA"); break;
		case 'E':	return ("ECHO"); break;
		case 'F':	return ("FOXTROT"); break;
		case 'G':	return ("GOLF"); break;
		case 'H':	return ("HOTEL"); break;
		case 'I':	return ("INDIA"); break;
		case 'J':	return ("JULIET"); break;
		case 'K':	return ("KILO"); break;
		case 'L':	return ("LIMA"); break;
		case 'M':	return ("MIKE"); break;
		case 'N':	return ("NOVEMBER"); break;
		case 'O':	return ("OSCAR"); break;
		case 'P':	return ("PAPA"); break;
		case 'Q':	return ("QUEBEC"); break;
		case 'R':	return ("ROMEO"); break;
		case 'S':	return ("SIERRA"); break;
		case 'T':	return ("TANGO"); break;
		case 'U':	return ("UNIFORM"); break;
		case 'V':	return ("VICTOR"); break;
		case 'W':	return ("WHISKEY"); break;
		case 'X':	return ("XRAY"); break;
		case 'Y':	return ("YANKEE"); break;
		case 'Z':	return ("ZULU"); break;

		default:
			return s;
			break;
	 }
    }




    function replaceNatoAlphabet(s) {
         s = s.replace(/ALPHA/g, "ANNA");
         s = s.replace(/BRAVO/g, "BERNHARD");
	     s = s.replace(/CHARLIE/g, "CORNELIS");
	     s = s.replace(/DELTA/g, "DIRK");
	     s = s.replace(/ECHO/g, "EDUARD");
	     s = s.replace(/FOXTROT/g, "FERDINAND");
	     s = s.replace(/GOLF/g, "GERARD");
	     s = s.replace(/HOTEL/g, "HENDRIK");
	     s = s.replace(/INDIA/g, "IZAAK");
	     s = s.replace(/JULIET/g, "JOHAN");
	     s = s.replace(/KILO/g, "KAREL");
	     s = s.replace(/LIMA/g, "LEONTIEN");
	     s = s.replace(/MIKE/g, "MARIA");
	     s = s.replace(/NOVEMBER/g, "NICOLET");
	     s = s.replace(/OSCAR/g, "OTTO");
	     s = s.replace(/PAPA/g, "PIETER");
	     s = s.replace(/QUEBEC/g, "QUOTA");
	     s = s.replace(/ROMEO/g, "RUDOLF");
	     s = s.replace(/SIERRA/g, "SIMON");
	     s = s.replace(/TANGO/g, "THOMAS");
	     s = s.replace(/UNIFORM/g, "UTRECHT");
	     s = s.replace(/VICTOR/g, "VICTOR");
	     s = s.replace(/WHISKEY/g, "WILLEM");
	     s = s.replace(/XRAY/g, "XANTIPPE");
	     s = s.replace(/YANKEE/g, "YPSILON");
	     s = s.replace(/ZULU/g, "ZWOLLE");
	     return s;
    }



    function resetTaskvar() {
    	state_id = 10;
    	totalpicked = 0;
    	locpromptnumber = 0;
    	locchkdigit='';
    	pick=null;
    	picked='';
    	status='';
    	reason='';
    	wcntitem=0;
    	weightprompt='';
    	completelocation ='';
        application.pickqty=0;
        weightflag='0';
        prodWght=0.0;
        reasonprompt='';
        dropchk='';
        batchtaskcount=0;
        batchtasks=[];
        nexttask=null;
        incrementqty=0;
        catchweightstr=null;
        catchqty=0;
        wcntitem=0;
        getweightqty=0;
        itemspicked=0;
        taskwcnitem=0;
        collectiondone='false';
    	addattrvalue='';
    	attrarray=[];
    	attrresarray=[];
    	addattrstr=null;
    	collectionlength=0;
    	addattrstr='';
        gotweight='false';
        activecontainer='';
    	putuowid='';
    	opencontainers='';
    	contconfirmation='';
        addverprompt='';
        invalidchk=0;
	    pickedQty=0;
        uom='';
        orglocprompt='';
        orgpickprompt='';
		targetposition='';
		orgputprompt='';
   }

    function resetCatchattr()
    {	
        catchqty=0;
        wcntitem=0;
        resetCollectattr();
        
    }
    
    function resetCollectattr() {
        wcntitem=0;
        collectionindex=0;
        collectiondone='false';
        
    }

    function makecatchweightstr(itemweight) {
    	if(catchweightstr == null || catchweightstr == ''){
    		catchweightstr=itemweight;
    	}else {
    		catchweightstr+=";"+itemweight;
		}
    	
    }


    function makeaddattrtstr() {
    	var addstr='';
    	if(attrarray == null){
    		addstr=addattrstr+"="+addattrvalue;
    		attrarray.unshift(addstr);	
    	} else {
    		var entrycnt=attrarray.length;
    		var entryindex=0;
    		var found='false';
    		while(entryindex < entrycnt ) 
    		{
    			addstr = attrarray.shift();
    			var indexE = addstr.indexOf("=");
    			
    			if(indexE > 0)
    			{
    				var namestr= addstr.substring(0,indexE);
    				if(namestr == addattrstr)
    				{
    					addstr +=";"+addattrvalue;
    					found='true';
    					entryindex=entrycnt;
    				}
    			}
    			attrarray.unshift(addstr);
    			attrarray.reverse();
    			entryindex++;
    		}
    		if(found == 'false')
    		{
    			var addstr=addattrstr+"="+addattrvalue;
    			attrarray.unshift(addstr);
    			attrarray.reverse();
    		}	
    			
    	}
    	    	
    }

    function getAdditionalstring(){
    	var resultstr=null;
    	
    	if( collectionlength >0){
    		var len=attrarray.length;
    		attrarray.reverse;
    		for(var i=0; i< len; i++){
    			var rstr=attrarray.pop();
    			if(resultstr == null || resultstr == ''){
    				resultstr = rstr;
    			}else{
    				resultstr+=","+rstr;
				}
    		}
    	}
    	return resultstr;
    
    }

    function fillPick(  thispick,  total,pos){
     		countTime();
     		
     		var addstrtosend=getAdditionalstring();

	    	picked = "{uowid:\'" + thispick.uowid + "\', taskid:\'" + thispick.taskid + "\', quantity:" + total + 
	    		", status:\'"+ status +"\',catchweightstr:\'"+catchweightstr+
	    		"\',additionalcollstr:\'"+addstrtosend +
	    		"\',targetposition:\'"+pos+"\', disconduration:\'"+enddiscon+
			"\', pickquantity:"+application.pickqty+",reason:\'"+reason+"\' }";



            if (fulfilled === (void 0)) {
				fulfilled = picked;
            }else  {            
              	 fulfilled += ", " + picked;
			}
            
            return;
              		
    }
    
    function submitPick() {
    	resetTaskvar();
		return "bugs/disconscript.vxml#submitTask";
    }
    	
    function processVerification() {
    	// make the prompt for the verification, and value to verifiy
    	// process all the verifications
    	// verificaton list is name=value,name=value
    	var indexc = verificationlist.indexOf(",");
    	if(indexc > 0){
    		verifyarray = verificationlist.split(",");
    		var verificationstr = verifyarray.shift();
    		var verifypromptinfo =  verificationstr.split("=");
    		var verifyprompt= verifypromptinfo.shift();
    		dbverifyvalue = verifypromptinfo.shift();
    		verificationlist = verificationlist.substring(indexc+1,verificationlist.length);
    		
    	} // only one
    	else {
    		var verifypromptinfo =  verificationlist.split("=");
    		var verifyprompt= verifypromptinfo.shift();
    		dbverifyvalue = verifypromptinfo.shift();
    		verificationlist=null;
    	}
    	
    	addverprompt="verify "+ verifyprompt;
    }
    
    function vercatchweight() {
		if(batchtaskcount > 0){
		    if( weightflag == '1' ){
		    	if( wcntitem++ < itemspicked){
		    		weightprompt = 'weight '+ wcntitem;
		    		collectiondone='false';
					if(collectionlength > 0) {
						collectionindex=1;
					}
					taskwcnitem++;
					//gotweight='true';
					return "bugs/disconscript.vxml#GetCatchWght";
		    	}else {
					gotweight='true';
			 		return distributePickqty(1);
		     	}
		    	
		    }else if(collectionlength > 0){
			 	//only collection
	 			collectiondone='false';
	 			taskwcnitem++;
	 			collectionindex=1;
	 			if(wcntitem++ < itemspicked) {
		 				return processCollectionAttrs(wcntitem);
		 		}else {
			 		return distributePickqty(1);
		     	}
			 			
			 }else if (putinfoarray.length > 0 ){
					return getputprompt();
		     }else {
			 	return distributePickqty(1);
		     }
		}else { //single order
		 	//catch weight and collection
			 if(weightflag == '1' ){
			 		if(wcntitem++ < pickedQty ){
			 			weightprompt = 'weight '+ wcntitem;
			    		collectiondone='false';
						if(collectionlength > 0){
			 				collectionindex=1;
						}
			    		return "bugs/disconscript.vxml#GetCatchWght";
			 		}
			 	if(pick.taskqty > 0) {
					 return getputprompt();
			  	}else {
					return getputprompt();
			   }
			 	
			 } else if(collectionlength > 0){
			 	//only collection
			 		collectiondone='false';
			 		collectionindex=1;
			 		if(wcntitem++ < pickedQty) {
			 			return processCollectionAttrs(wcntitem);
			 		}
					if(pick.taskqty > 0) {
		    			resetCatchattr();
						return getputprompt();
					}else {
						return getputprompt();
					   }
			}
		  if(pick.taskqty > 0) {
				 resetCatchattr();
				return getputprompt();
		  }else {
				return getputprompt();
		   }
		}
	
    }
    
    function processCatchweight(){
    		if(batchtaskcount > 0){
	 			 	 if ( taskwcnitem++ < getweightqty ) {	//individual task items 
	 			 	 	wcntitem++
	 			 	 	weightprompt = 'weight '+ wcntitem;
		    			return "bugs/disconscript.vxml#GetCatchWght";	
	 			 	 }
	 			 	return  distributePickqty(1);
	 			 	
			 }else {//single
              		 if (catchqty++ < pickedQty) {
							weightprompt = 'weight '+ catchqty;
							return "bugs/disconscript.vxml#GetCatchWght";
					 }else if(catchqty == pickedQty){
              			fillPick(pick,pick.taskqty,activecontainer); //what happens in over pick, incremental picjk, batch pick
              			catchweightstr=null;
              		
						return getputprompt();
					}else {
						return getputprompt();
				}
			 }       		 
    }

    function processCollectionAttrs(item){
   
    	// make the prompt for the add attr, 
    	// process all the verifications
    	// verificaton list is name1,name2
    	if(collectionlength > 1)
    	{
    		var indexc = collectionlist.indexOf(",");
    		if(indexc > 0) {
				collectionarray = collectionlist.split(",");
			    addattrstr = collectionarray.shift();
				collectionlist=collectionlist.substring(indexc+1,collectionlist.length)+","+addattrstr;
				if( collectionindex++ >= collectionlength)	
					collectiondone='true';
			}
    
    	} // only one
    	else {
    		 addattrstr = collectionlist;
    		 collectiondone='true';
    	}
    	
    	
    	addattrprompt="say "+ addattrstr +" "+ item;
		return "bugs/disconscript.vxml#GetAddAttr";
    	
    }


    function distributePickqty( processtype) {
     	//distribute to individual tasks
	//overpick not taken care at---------------7/6
	//catch weight and additioanl options and verifications need to be done
	//no discrete pick also done
      var lastputqty=0;  //with in the same pick
		if(processtype == 0) {
			     
			     if(pickedQty == pick.taskqty){
			        lastputqty=pick.taskqty;
			        pick.taskqty =0;
					pick.totalqty -= pickedQty;
					status='COMPLETE';
					if(uom == '' || uom == null || uom != null && uom.equals("null")){
					 orgpickprompt=pickprompt = "pick "+spacedout(pick.totalqty);
					}else{
					 orgpickprompt=pickprompt ="pick "+spacedout(pick.totalqty)+ uom;
					}
					incrementqty+=pickedQty;	//reset it
					if(catchqty ==0){
						catchqty=1;
					}
					getweightqty=pickedQty;//qty to prompt for catch weight 
					pickedQty=0;
					makeputobject(pick,lastputqty);
					return vercatchweight();
					
				}else if (pickedQty > pick.taskqty ) {
					
					getweightqty=pick.taskqty;
					
					lastputqty=pick.taskqty;
					incrementqty+=pick.taskqty;
					pick.totalqty -= pick.taskqty;
					pickedQty -= pick.taskqty;
					pick.taskqty = 0;
					status='COMPLETE';
					if(catchqty ==0){
						catchqty=1;
					}
					if(lastputqty > 0){
						makeputobject(pick,lastputqty);
						return vercatchweight();
					}
					
					
			       	if(uom == ''|| uom == null || uom != null && uom.equals("null")){
					 orgpickprompt=pickprompt = "pick "+spacedout(pick.totalqty);
					}else{
					 orgpickprompt=pickprompt ="pick "+spacedout(pick.totalqty)+ uom;
					}
					
					
					while(batchtasks.length > 0 && pickedQty > 0)
					{
						nexttask = batchtasks.pop();
						if(nexttask.taskqty == pickedQty){
							lastputqty=nexttask.taskqty;
							getweightqty=nexttask.taskqty+wcntitem;
							pick.totalqty -= nexttask.taskqty;
							incrementqty+=nexttask.taskqty;
							makeputobject(nexttask,lastputqty);
							nexttask.taskqty =0;
							status='COMPLETE';
							
			            	
							if(uom == ''||uom == null ||  uom != null && uom.equals("null")){

					 			orgpickprompt=pickprompt = "pick "+spacedout(pick.totalqty);
							}else{
					 			orgpickprompt=pickprompt ="pick "+spacedout(pick.totalqty)+ uom;
							}
							pickedQty=0;
							return vercatchweight();
							//pickedQty=0;
						}
						else if (pickedQty > nexttask.taskqty){
							lastputqty=nexttask.taskqty;
							getweightqty=nexttask.taskqty;
							status='COMPLETE';
							incrementqty+=nexttask.taskqty;
			            	
							pickedQty -= nexttask.taskqty;
							pick.totalqty -= nexttask.taskqty;
							makeputobject(nexttask,lastputqty);
							return vercatchweight();
							//nexttask.taskqty = 0;
							//incrementqty=0;
						}
						else if(pickedQty < nexttask.taskqty){
							lastputqty=pickedQty;
							incrementqty+=pickedQty;
							getweightqty=pickedQty;
							nexttask.taskqty -= pickedQty;
							pick.totalqty -= pickedQty;
							if(uom == ''|| uom == null || uom != null && uom.equals("null"))

					 			orgpickprompt=pickprompt = "pick "+spacedout(pick.totalqty);
							else
					 			orgpickprompt=pickprompt ="pick "+spacedout(pick.totalqty)+ uom;

							batchtasks.push(nexttask);
							pickedQty=0;
							makeputobject(nexttask,lastputqty);
							return vercatchweight();
							
						}
					}
					//if picked still more then assign to last item.
					if(pickedQty > 0){
						status='COMPLETE';
						lastputqty+=pickedQty;
						getweightqty=pickedQty;
						incrementqty+=pickedQty;
			            
			            pickedQty = 0;
						pick.totalqty =0;
						makeputobject(nexttask,lastputqty);
			            return vercatchweight();
					}
			     }else if (pickedQty < pick.taskqty){
			        lastputqty = pickedQty;
					pick.taskqty -= pickedQty;
					getweightqty=pickedQty;
					pick.totalqty -=pickedQty;
					if(uom == ''|| uom == null ||  uom != null && uom.equals("null")){
					 orgpickprompt=pickprompt = "pick "+spacedout(pick.totalqty);
					}else{
					 orgpickprompt=pickprompt ="pick "+spacedout(pick.totalqty)+ uom;
					}
					incrementqty+=pickedQty;
					pickedQty=0;
					makeputobject(pick,lastputqty);
					return vercatchweight();
				 }
		}else { // process fillpicks if task is complete
			
			    if(nexttask == null || nexttask == ''){
					nexttask=pick; //use pick task
				}
			    
				if(nexttask.taskqty == 0 && incrementqty > 0){
					fillPick(nexttask,incrementqty,nexttask.activecontainer);
					catchweightstr=null;
					incrementqty=0;
					nexttask.taskqty = 0;
					//taskwcnitem=0;
					
					if(pickedQty > 0){
						return distributePickqty(0);
					}
				}
				if(pickedQty > 0){
					return distributePickqty(0);
				}
					
				if (putinfoarray.length > 0 ){
					return getputprompt();
				}
		    	
					
			    if(pick.totalqty > 0){
			    	//catchweightstr=null;
			    	wcntitem=0;
			    	taskwcnitem=0;
					//return getputprompt();
					 setExpertPickPrompt();
					 targetposition='';
					return "bugs/disconscript.vxml#GetPickQty";
				}
				 else { // allpicked so submit..
					resetTaskvar();
					return "bugs/disconscript.vxml#submitTask";
				 }
				
		}
  
    }


    function makeputobject(thispick,lastputqty){
    	 var putinfo = new Object();
    	 putinfo.activecontainer = thispick.activecontainer ;
    	 putinfo.opencontainers = thispick.uowcontainer;
    	 putinfo.putuowid = thispick.uowid;
    	 putinfo.qty = lastputqty;
    	 putinfo.contconfirmation = thispick.contconfirmation;
    	 putinfoarray.push(putinfo);
    }


    function getputprompt(){
	//expert mode processing
    if(targetposition != null && targetposition != '') {
	 if( /^\d+$/.test( targetposition ) ) {
		state_id = 56;
	}else {
		state_id = 55;
        }
    }
    if(batchtaskcount > 0){
    	//putinfo-uowid,activecontainer,opencontainers,contconfirmation
    	if( putinfoarray.length > 0){ //get item
    		var putinfo= putinfoarray.pop();
    		activecontainer = putinfo.activecontainer;
    		opencontainers = putinfo.opencontainers;
    		putuowid = putinfo.putuowid;
    		var qty = putinfo.qty;
    		contconfirmation = putinfo.contconfirmation;
			if(activecontainer != null && !activecontainer.equals("null")){
    			putprompt = "Put " + spacedout(qty) + " "+ activecontainer;
				orgputprompt = putprompt;
				if(state_id == 55 || state_id == 56) {
					return processRequest();
				}else {
				  return "bugs/disconscript.vxml#PutPosition";
				}
			}else
				return distributePickqty(1);
		}else {
			if(pick.taskqty > 0) {
		 		resetCatchattr();
				setExpertPickPrompt();
				targetposition='';
	            return "bugs/disconscript.vxml#GetPickQty";
			}else {
				fillPick(pick,totalpicked,activecontainer);
				catchweightstr=null;
				resetTaskvar();
				return submitPick();
			}
		}
    }else{
    	if(promptcont == "true" && activecontainer != null && !activecontainer.equals("null")) { //contianer open prompt
    		putprompt = "Put " + spacedout(pickedQty) + " "+ activecontainer;
			orgputprompt = putprompt;
			if(state_id == 55 || state_id == 56) {
					return processRequest();
				}else {
				  return "bugs/disconscript.vxml#PutPosition";
				}
    	}else if(pick.taskqty > 0) {
		 		resetCatchattr();
				targetposition='';
				setExpertPickPrompt();
	            return "bugs/disconscript.vxml#GetPickQty";
		}else {
	    	fillPick(pick,totalpicked,activecontainer);
	    	catchweightstr=null;
			resetTaskvar();
	    	return submitPick();
	   }
	  }
    }



    function decideNextMove(){
    			
    	if(batchtaskcount > 0 ){ //if batch mode
    		 if  (weightflag == '1' ){
					if(wcntitem < getweightqty) {
						return vercatchweight();
					}else {
						return distributePickqty(1);
					}
    		 }else if (collectionlength > 0  ) { //&& collectiondone == 'false'
    			if ( taskwcnitem++ < getweightqty) {	//individual task items
	 			 	 	collectiondone='false';
	 					collectionindex=1;
		 				return processCollectionAttrs(taskwcnitem);
		 		}
		 		else {
			 		return distributePickqty(1);
		     	}
		     	
		      }else {
			 		return distributePickqty(1);
		     	}
    	} else {  // if sinlge order just go back to vercatchweight
    			if(weightflag == '1' || collectionlength > 0)
    					return vercatchweight();
    			if(pick.taskqty > 0) {
    				 resetCatchattr();
					setExpertPickPrompt();
					targetposition='';
			         return "bugs/disconscript.vxml#GetPickQty";
			  }else {
				return getputprompt();
			   }
    	}
    	
    }


    function moveAfterPut(){
		targetposition='';
		state_id = 0;	//reset
    	if(batchtaskcount > 0 ){ //if batch mode
			 		return distributePickqty(1);
		     	
    	} else {  // if sinlge order just go back to vercatchweight
    			if(pick.taskqty > 0) {
    				 resetCatchattr();
					 setExpertPickPrompt();
			         return "bugs/disconscript.vxml#GetPickQty";
			  	}else {
			    	fillPick(pick,totalpicked,activecontainer);
			    	catchweightstr=null;
			    	return submitPick();
			   }
    	}
    	
    }


   function processRequest() {
   
   
	switch (state_id) {
          
          	case 10:				//prompt first location
          			
          			orglocprompt=locprompt=pick.promptlist0;
          			return "bugs/disconscript.vxml#location";
          			break;
		case 11:				// verify chkdigit
				var dbchkdigit;
				switch (locpromptnumber){
					case 0:	
							if(invalidchk > 0){
								orglocprompt=pick.promptlist0;
								locprompt= spacedout(locchkdigit) + " "+disconscript_invalidchk + pick.promptlist0;
							} else{
								
								orglocprompt=locprompt=pick.promptlist0;
															}
							invalidchk=0;
          						return "bugs/disconscript.vxml#location";
          						break;
					case 1:
							dbchkdigit = pick.chkdigit0;
							if(pick.promptlen > 0){
							  
							   if(invalidchk > 0){
								orglocprompt=pick.promptlist0;
								locprompt=spacedout(locchkdigit) + " "+ disconscript_invalidchk + pick.promptlist0;
								invalidchk=0;
								locpromptnumber=0;
								return "bugs/disconscript.vxml#location";

							} else{
								
								orglocprompt=locprompt=pick.promptlist1;
																}
							}
							
							break;
					case 2:
							dbchkdigit = pick.chkdigit1;
							if(pick.promptlen > 1){
							  
							  if(invalidchk > 0){
								orglocprompt=pick.promptlist1;
								locprompt=spacedout(locchkdigit) + " "+disconscript_invalidchk + pick.promptlist1;
							  	invalidchk=0;
								locpromptnumber=1;
								return "bugs/disconscript.vxml#location";
							} else{
								orglocprompt=locprompt=pick.promptlist2;
								
							  }
							}
							
							break;
					case 3:
							dbchkdigit = pick.chkdigit2;
							if(pick.promptlen > 2){
								
							  if(invalidchk > 0){
								 orglocprompt=pick.promptlist2;
								 locprompt=spacedout(locchkdigit) + " "+ disconscript_invalidchk + pick.promptlist2;
								 invalidchk=0;
								 locpromptnumber=2;
								 return "bugs/disconscript.vxml#location";
							     } else{
								invalidchk=0;
								orglocprompt=locprompt=pick.promptlist3;
															   }
							}
							break;
					default:
							break;
				}

					
				if(locchkdigit == dbchkdigit ){ //plain check digit.
					invalidchk=0;
					if(locpromptnumber == pick.promptlen ){ //no more prompts go to pick
		  			  //check if need to veri additional attributes
		  			  if(verificationlist  != null && verificationlist != ''){
		  			  		 processVerification();
		  			  		if(addverprompt != null )
		  			  			return "bugs/disconscript.vxml#AddVerify";
		  			  }

					 		
					 if(batchtaskcount > 0){
							if(uom == ''|| uom == null || uom != null && uom.equals("null"))
					 			orgpickprompt=pickprompt = "pick "+spacedout(pick.totalqty) +" consolidated";
							else
					 			orgpickprompt=pickprompt ="pick "+spacedout(pick.totalqty)+ uom;

					  }else{
							orgpickpromt=pickprompt='pick '+spacedout(pick.taskqty);
							if(uom == ''|| uom == null || uom != null && uom.equals("null"))

					 		  orgpickprompt=pickprompt = "pick "+spacedout(pick.taskqty);
							else
					 		orgpickprompt=pickprompt ="pick "+spacedout(pick.taskqty)+ uom;

					  }
					   
					  if( batchtaskcount == 0 && usermode !=null && usermode.equals("EXPERT")) { //expert mode partially accepted check digit sopickput
						orgpickprompt += " Put "+ activecontainer;
						pickprompt = orgpickprompt;
					   }
						targetposition='';
					   return "bugs/disconscript.vxml#GetPickQty";
					  
					}else { // more to prompt
						return "bugs/disconscript.vxml#location";
					}
				}else { //not valid check digit
					if(locpromptnumber > pick.promptlen)
							locpromptnumber--;
					else if(locpromptnumber < 0)
							locpromptnumber=0;

					invalidchk=1;
					return processRequest();
				}
				break;
				
		case 12:				//pick_quanity
			
			if(application.overpick == "false" && pickedQty > pick.totalqty){
				if(uom == ''|| uom == null || uom != null && uom.equals("null")){
					 orgpickprompt= spacedout(pick.totalqty);
					 pickprompt = "overpick not allowed pick "+spacedout(pick.totalqty);

					}else{
					 pickprompt = "overpick not allowed pick "+spacedout(pick.totalqty)+ uom;
			    		 orgpickprompt= spacedout(pick.totalqty)+ uom;
					}
					setExpertPickPrompt();
					return "bugs/disconscript.vxml#GetPickQty";
			}
			if(application.underpick == "false" && pickedQty < pick.totalqty){
				if(uom == ''|| uom == null || uom != null && uom.equals("null")){
					orgpickprompt= spacedout(pick.totalqty);
			    		pickprompt = "underpick not allowed pick "+spacedout(pick.totalqty);
				}else{
					orgpickprompt= spacedout(pick.totalqty)+ uom;
					pickprompt = "underpick not allowed pick "+spacedout(pick.totalqty)+ uom;
				}
				setExpertPickPrompt();
				targetposition='';
			    	return "bugs/disconscript.vxml#GetPickQty";
			 }
				// batch pick 
				//if batch pick get total quantity to pick
			if(batchtaskcount > 0){
					
				itemspicked=pickedQty;
				return distributePickqty(0);
				

			}else { // single pick
				
					totalpicked += pickedQty;	//get total qty picked
					if( pickedQty < pick.taskqty ){
					
						pick.taskqty -= pickedQty;
						catchqty=1; //pickedqtyl
						if(uom == ''|| uom == null || uom != null && uom.equals("null"))

							orgpickprompt=pickprompt = "pick " + spacedout(pick.taskqty);
						else
							orgpickprompt=pickprompt = "pick " + spacedout(pick.taskqty)+ uom;
						return vercatchweight();
						
				 		//return "bugs/disconscript.vxml#GetPickQty";
				 	 }
				 	 else if (pickedQty > pick.taskqty ){	//overpick
				 	 
				 	 	if(overpickflag == "true"){
				 	 		status = 'COMPLETE';  
	              			catchqty=1;//pickedQty; 
	              			return vercatchweight();
	              			 
				 	 	}else {
				 	 		totalpicked -= pickedQty;	//adjust the qunatity just added..
				 	 		if(uom == ''|| uom == null || uom != null && uom.equals("null")){
							   orgpickprompt= spacedout(pick.totalqty);

							   pickprompt = "overpick not allowed pick "+spacedout(pick.taskqty);
				 	 		}else{
							      orgpickprompt= spacedout(pick.totalqty)+ uom;

								pickprompt = "overpick not allowed pick "+spacedout(pick.taskqty)+uom;
							}
							setExpertPickPrompt();
							return "bugs/disconscript.vxml#GetPickQty";
				 	 	}
				 	 }
				 	 else if(pickedQty == pick.taskqty) {
				 	 	
				 	 		status = 'COMPLETE';
				 	 		pick.taskqty=0;
				 	 		catchqty=1;
	              			return vercatchweight();
	              			  
				 	 }
				 	}
			 		break;
			 		
		case 20:			//process cuts
			 		
			 	if(batchtaskcount > 0){
			 				
			 			if(nexttask != null && pick.taskqty > 0){ //first item already picked
			 				fillPick(nexttask,getweightqty,activecontainer);
			 				nexttask=null;
			 				getweightqty=0;
			 			}else if (nexttask == null && pick.taskqty > 0){// fist item not picked
			 				fillPick(pick,getweightqty,activecontainer);
			 				nexttask=null; 
			 				getweightqty=0;
			 			}else if(nexttask !=null && nexttask.taskqty > 0){ //item picked and cut from batch lst
			 				fillPick(nexttask,getweightqty,activecontainer);
			 				nexttask=null;
			 				getweightqty=0;
			 			}
			 			catchweightstr=null;
					    addstrtosend=null;
			 			while(batchtasks.length > 0){
			 				nexttask = batchtasks.shift();
			 				fillPick(nexttask,0,activecontainer);
			 			}
						
			 		}
			 		else {
				 		//if(totalpicked >= pickedQty)
				 		//	totalpicked -= pickedQty;
				 		fillPick(pick,totalpicked,activecontainer);

	              	}
              		          
              	resetTaskvar();
			 	return "bugs/disconscript.vxml#submitTask";
		
			 	break;
		case 22:	//failed getting reason, not supported reasonin disconenct
				//hard coded to 0; partialskips not supported
			 			status='SKIP';
						reason="other";
         					state_id=20;
						fillPick(pick,0,activecontainer); 
						resetTaskvar();
			 			return "bugs/disconscript.vxml#submitTask";

		case 25:		//expert mode
			 			switch(pick.promptlen){
			 			//need to figureout how to get pick.xx1 from prompt len???
			 			
			 			case 1:dbchkdigit = pick.chkdigit0;
			 					break;
			 			case 2: dbchkdigit = pick.chkdigit1; break;
			 			case 3: dbchkdigit = pick.chkdigit2; break;
			 			default:
			 				dbchkdigit='confirm';break;
			 			}
			 			if(locchkdigit == dbchkdigit ) {//plain check digit.
		  						state_id=12;
		  						return processRequest();
						}else { //not valid check digit
							locpromptnumber=pick.promptlen;
							state_id = 11;
							return processRequest();
						}
					break;
			 			
			 			
		case 30:		//overpick reason
			 		return "bugs/disconscript.vxml#ovpreason";
			 	break;
		case 35:		//batch mode not supoprted
			 		return "bugs/disconscript.vxml#nobatchmode";
			 	break;
		case 40:		//get cut reason
      				if (cutres == "cutitem" ) {
      				 	status='CUT';
      				 	reasonprompt='Provide reason for CUT';
      				    return "bugs/disconscript.vxml#getreason";
      				}else if (cutres == "shortitem") {
      				 	status='SHORT';
      				 	reasonprompt='Provide reason for SHORT';
      				    return "bugs/disconscript.vxml#getreason";
      				}else if (cutres == "skiplocation" || cutres == "skipaisle") {
      				 	status='SKIP';
      				 	reasonprompt='Provide reason for SKIP';
      				    return "bugs/disconscript.vxml#getreason";
      				 }  else{
      				 }
      				   
			 	break;
		 case 45:			//catch weight processing
			 	if(prodWght == pick.weight) {
			 			 makecatchweightstr(prodWght);
			 			 if(collectionlength > 0)
			 			 	return processCollectionAttrs(wcntitem);
			 			 else
			 			 	return processCatchweight();
	              		 
			 	}else if( prodWght >= (pick.weight - pick.min) && prodWght <= (pick.weight + pick.max)){
			 			if(verWghtflag == "false") { //to verify the weight
			 				verWghtflag = true;
			 				weightprompt = 'verify weight ' + wcntitem;
			 				//change weight
			 				pick.weight = (pick.weight + prodWght)/2;
			 				return "bugs/disconscript.vxml#GetCatchWght";
			 			}else {
			 				verWghtflag = false;
			 				 makecatchweightstr(prodWght);
			 				 if(collectionlength > 0)
			 			 		return processCollectionAttrs(wcntitem);//-1
			 			 	else
			 			 		return processCatchweight();
			 			}
			 	}else {
			 			weightprompt = 'weight out of range, weight '+ wcntitem;
			 			return "bugs/disconscript.vxml#GetCatchWght";
			 	}
			 	break;

		case 50:			//no more tasks to process
			 		return "bugs/disconscript.vxml#submitTask";
			 		break;

		case 55:	//handle put prompt
			 		if(targetposition == "confirm"){ //accept the positon;
			 			targetposition = activecontainer;
			 		}else if(targetposition == activecontainer){
			 			targetposition= activecontainer;
			 		}else {
			 		     
			 			if(opencontainers == null || opencontainers != null && opencontainers.indexOf(targetposition) < 0){
							putprompt = "invalid container "+ orgputprompt;
			 				return "bugs/disconscript.vxml#PutPosition";
			 			}else{ //swich active contaienr
			 				activecontainer = targetposition;
			 			}
			 		}
			 		
					return moveAfterPut();
					
					break;
		case 56: //container id
			 		if(targetposition == contconfirmation)
			 		{
			 			targetposition = activecontainer;
			 		}else{
			 			return "bugs/disconscript.vxml#PutPosition";
			 		}
			 		
					return moveAfterPut();
					
			 		break;
		
		case 70:				// verify additional verification  
				
				
				if(userverifyvalue == dbverifyvalue ){
					 if(verificationlist  != null && verificationlist != ''){
					 		addverprompt=null;
		  			  		processVerification();
		  			  		if(addverprompt != null )
		  			  			return "bugs/disconscript.vxml#AddVerify";
		  			  }
					  if(batchtaskcount > 0)
							if(uom == '' || uom == null || uom != null && uom.equals("null"))

							  orgpickprompt=pickprompt = 'pick '+spacedout(pick.totalqty);
							else
							  orgpickprompt=pickprompt = 'pick '+spacedout(pick.totalqty)+uom;
					  else
							if(uom == ''|| uom == null || uom != null && uom.equals("null"))

								orgpickprompt=pickprompt='pick '+spacedout(pick.taskqty);
							else
								orgpickprompt=pickprompt='pick '+spacedout(pick.taskqty)+uom;

					  setExpertPickPrompt();	
					  targetposition='';
					  return "bugs/disconscript.vxml#GetPickQty";
					  
				}else {
					if(dbverifyvalue == '')
						processVerification();
					return "bugs/disconscript.vxml#AddVerify";
				}
				break;
				
		case 80:				// collect additional attributes  
				
				makeaddattrtstr();
				if (collectiondone == 'false'){
					return	processCollectionAttrs(wcntitem);
				}else {
					//gotweight='false';
					return decideNextMove();
					}
				
				break;
		case 100:			//no dispatched data display outof range message
			 			return "bugs/disconscript.vxml#outofrange";
			 		break;
		case 120:		//drop chk digit verification
			 			return "bugs/disconscript.vxml#dropchk";
			 			break;
		case 125:	//droplocation container
			 		return "bugs/disconscript.vxml#dropcont";
			 		break;
			 	
		default:
				break;
	}
}

function setExpertPickPrompt(){
	
	if( batchtaskcount == 0 && usermode !=null && usermode.equals("EXPERT") &&
		activecontainer != null && !activecontainer.equals("null")) { //expert mode partially accepted check digit sopickput
		orgpickprompt += " Put "+ activecontainer;
		pickprompt += " Put "+ activecontainer;
	}
}


function doLocationInit(jsessionid,putstr,promptVal,location,prodesc,uom,spinst,whatsay,cutproperty,
				skipLproperty,skipAproperty,shortproeprty,
				promptUomQty,qtyAllUom,needExactUom,expertPromptAllUom,dispatched) {
	application.jsessionid = jsessionid;
  		var putstr = putstr;
		application.putstring=putstr;
  		//application.locprompt=promptVal;
  		application.completelocation=location;
  		application.prodesc=prodesc;
	      
	      application.specialinst=spinst;
	      application.dispatched=dispatched;
  		//application.dispatched=0;
		application.fulfilled = void 0;
		application.locwsay=whatsay;

		application.cutflag=cutproperty;
  		application.skipLflag=skipLproperty;
  		application.skipAflag=skipAproperty;
		application.shortflag=shortproeprty;

	 	//create state object for recovery
		application.state.locprompt=promptVal;
		application.state.cutflag=cutproperty;
  		application.state.skipLflag=skipLproperty;
  		application.state.skipAflag=skipAproperty;
		application.state.shortflag=shortproeprty;
		application.numUom=1;
		application.packuom='';

		//uom
		if(qtyAllUom.indexOf('$') != 0 ){
			application.state.qtyAllUom=qtyAllUom;
		}else{
			application.state.qtyAllUom="";
		}
		application.state.needExactUom=needExactUom;
		application.state.exactuomqty=promptUomQty;
		application.state.expertPromptAllUom=expertPromptAllUom;
		

		if(promptUomQty.indexOf('$') != 0) 
		{
			if (promptUomQty.indexOf(",")>0) {
				var uomArray = promptUomQty.split(",");
				application.numUom=uomArray.length;
				application.packuom="";
			}else {
				var uomVals = promptUomQty.split("=");
				application.packuom=uomVals[0];
				application.numUom=1;
			}
			
		}

		if( uom.indexOf('$') != 0)
			application.produom=uom;
   		else
			application.produom="";

}



//promptuomqty-cASE=1,EACH=4 -- to prompt and match exact on require uom
//qtyalluom-EACH=28,CASE=1,PALLET=0 -- to convert
function doPickInit(jsessionid,qtydb,promptValue,contslot,pickstr,pickedQty,setProperty,outreason,
		putstr,wsay1,wsay2,uom,consolidated,promptUomQty,singledbUom,qtyAllUom,needExactUom,
			help3,help4) {
  		application.jsessionid =jsessionid;

  		var pickedQty;
  		var qty = qtydb;
  		var ask;
  		var prompv = parseInt(promptValue);
  		var contslot = contslot;
  		application.locpromptnumber=0;
  		application.fulfilled = void 0;
		application.pickstr =pickstr;
		application.pickedQty=pickedQty;
		application.pickoverpick=setProperty;
		application.pickwsay1=wsay1;
		application.pickwsay2=wsay2;
		application.pickwsay3=help3;
		application.pickwsay4=help4;
		application.pickwsay=wsay1;
		application.packuom='';
		
		application.state.needExactUom=needExactUom;
		application.state.exactuomqty=promptUomQty;
		application.state.expertpickadd="";
		
		if(qtyAllUom.indexOf('$') != 0 ){
			application.state.qtyAllUom=qtyAllUom;
		}else{
			application.state.qtyAllUom="";
		}
		
		application.numUom=1;


		var yesPack=false;
		var uomQty=null;
		


		if(promptUomQty.indexOf('$') != 0) 
		{
			yesPack=true;
			

			if (promptUomQty.indexOf(",")>0) {
				var uomArray = promptUomQty.split(",");
				application.numUom=uomArray.length;
				for(i=0; i< uomArray.length; i++)
				{
					if(uomQty == null)
					{
						var uomVals = uomArray[i].split("=");
						uomQty = spacedout(uomVals[1]) + " "+ uomVals[0]+ " ";
						
					}else {
						var uomVals = uomArray[i].split("=");
						uomQty = uomQty + spacedout(uomVals[1]) + " "+ uomVals[0]+ " ";
					}
					
				}
			}
			else {// single uom to prompt
				var uomVals = promptUomQty.split("=");
				uomQty = spacedout(uomVals[1]) + " "+ uomVals[0]+ " ";
				application.packuom=uomVals[0];
				
			}
			application.pickwsay=help3;
			
       	}

		application.uomqtyprompt=uomQty;	//used for repat quantity
		
  	
        if ( prompv == 1 ){
			if(yesPack==false)
			{
				ask=application.pickstr + spacedout(qty) ;
				if( uom.indexOf('$') != 0)
					ask += " "+uom + " ";
			}else {
				ask=application.pickstr+ uomQty;
			
			}
		}
		else if ( prompv == 2 || prompv == 6)
		{
			if(yesPack==false)
			{
				ask=outreason + application.pickstr + spacedout(qty) ;
				if( uom.indexOf('$') != 0)
					ask += " "+uom + " ";
			}else {
				ask=outreason + application.pickstr+ uomQty;
			
			}
			if(prompv == 6){
				ask+=" "+ consolidated;
			}
		}
		else if (prompv == 3 ){
			ask=outreason ;
		}else if (prompv == 4) {
			if(yesPack==false)
			{
				ask = application.pickstr + spacedout(qty) + consolidated;
				if( uom.indexOf('$') != 0)
					ask = application.pickstr + spacedout(qty) +" "+ uom+" "+ consolidated;
				}else {
					ask = application.pickstr+ uomQty + consolidated;		
				}


		}else if (prompv == 5) {
			if(yesPack==false)
			{
				ask =outreason+" "+ application.pickstr + spacedout(qty) + consolidated;
				if( uom.indexOf('$') != 0)
					ask = outreason+ " "+application.pickstr + spacedout(qty) +" "+ uom+" "+ consolidated;
			}else {
				ask =outreason+" "+ application.pickstr +" "+ uomQty + consolidated;
			
			}
		

		}

	if(prompv == 4 || prompv == 5){
		application.state.expertpickadd=" "+consolidated;
	}

  	application.contslot = contslot;
	application.putstring = putstr;
  	if (prompv != 3 && contslot.indexOf('$') != 0){
  		//change prompt
            if(contslot.match(/[0-9]/)){
				ask = ask + putstr+ spacedout(contslot);
				application.contslot = spacedout(contslot);
				application.state.expertpickadd+=" "+ putstr+ spacedout(contslot);
			}else{
				ask = ask + putstr+ contslot;
				application.state.expertpickadd+=" "+ putstr+ contslot;
			}
  	}
  	application.pickprompt=ask; //to prompt
	application.pickqty=qty;

	// remove overpick not allowed from prompt
	if(application.pickprompt.indexOf(outreason) >= 0){
		application.orginalPickPrompt=application.pickprompt.substring											(outreason.length,application.pickprompt.length);
	}else{
		application.orginalPickPrompt=application.pickprompt;	// wihtout overpick reasons
	}
			

}

//change normal mode give me each
function changeUom(uom){

	if( application.state.qtyAllUom.indexOf('$') != 0){
		if(application.state.qtyAllUom.indexOf(uom) >= 0)
		{
			var uomvalArray = application.state.qtyAllUom.split(uom+"=");
			
			if(uomvalArray[1].indexOf(",")>0)
			{
				var uomqtyArray = uomvalArray[1].split(",");
				if(uomqtyArray[0] != "0")
					return (spacedout(uomqtyArray[0]) +" "+ uom);
				else
					return false;
			}else{

				if(uomvalArray[1] != "0")
					return (spacedout(uomvalArray[1]) +" "+ uom);
				else
					return false;
			}
		}else
			return false;

	}else{

		return false;
	}

}

//changeto uom- give me each for expert mode
function expertChangeUom(uom)
{

	if( application.state.expertPromptAllUom.indexOf('$') != 0){
		if(application.state.expertPromptAllUom.indexOf(uom) >= 0)
		{
			var uomvalArray = application.state.expertPromptAllUom.split(uom+"=");
			if(uomvalArray[1].indexOf(",")>0)
			{
				var uomValue = uomvalArray[1].split(",");
				return(uomValue[0]);
			}else{
			
				return(uomvalArray[1]);
			}
		}else
			return false;

	}else{

		return false;
	}

}

function matchExactUomQty(pickedQty,rcvUom1,pickedQty2,rcvUom2)
{

	if( application.state.exactuomqty.indexOf('$') != 0){
	  if(application.state.exactuomqty.indexOf(rcvUom1) < 0 )
	   {
		return false;
	    }else if (rcvUom2 != null && !rcvUom2.equals("null") && application.state.exactuomqty.indexOf(rcvUom2) < 0)
	    {
			return false;
	     }
	     var uomArray = application.state.exactuomqty.split(",");
		// fidn uom and check value

		for(i=0; i< uomArray.length; i++)
		{
			if(uomArray[i].indexOf(rcvUom1) >= 0)
			{
				var uomValue = uomArray[i].split(rcvUom1+"=");
				if( pickedQty > parseInt(uomValue[1]))
					return false;
			}
			if(rcvUom2 != null && !rcvUom2.equals("null") && uomArray[i].indexOf(rcvUom2) >= 0)
			{
				var uomValue = uomArray[i].split(rcvUom2+"=");
				if( pickedQty2 > parseInt(uomValue[1]))
					return false;
			} 
					
		}
		return true;
	}
		
	return false;	
	

}

function repeatQuantity(uom)
{
	var value=false;
	
	if(uom != ""){
		value = changeUom(uom);

	}
	if( value === false  && application.uomqtyprompt != null){
		return application.uomqtyprompt;
	}else if(value == false ){
		return spacedout(application.pickqty)+" "+application.produom;
		
	}else {
		return value;
	}
}



function initPickVXML(){
  		jsessionid = application.jsessionid;
	
		var cutflag=application.cutflag;
  		var shortflag=application.shortflag;
  		var skipLflag=application.skipLflag;
  		var skipAflag=application.skipAflag;

  		
  		var prompt = application.pickprompt;
		var qty = application.pickqty;
		var backflag=application.goback;
		var backcnt=0;
		var prevprompt='';
		var cutval='';
		var cutcnt=0;
		var resptype=0;
        var putchkdigit='';
        var putchktype='';
		var setproperty='';
		var callingProcess='';

}

/**
used to  make assignments to prompt for repeat assignments
will never be called when the data is is null
**/
function makeActiveAssignments(uowidobj)
{
	var uowidstr='';
	application.numofassignments=uowidobj.length; 	

	while(uowidobj.length > 0) {
             		id = uowidobj.shift();
	    		if(id.uowID.length > 10) 
				newid = id.uowID.substring(id.uowID.length-5,id.uowID.length);
	    		else
				newid = id.uowID;

			if(uowidstr == '')
				uowidstr=newid;
			 else
				uowidstr+=","+newid;
	}
	if(uowidstr == ''){
			application.uowidlist=uowidstr;
	}else if(uowidstr.indexOf(',') > 0){
			application.uowidlist=uowidstr.split(",");
	}else{
			application.uowidlist=uowidstr;
	}

}

/**
used to make containers to repeat containers
**/

function makeActiveContainers(containerstr)
{
	if(containerstr.indexOf('$') == 0){
			application.containerlist=containerstr;
			application.containernumber=0;		
	}else if(containerstr.indexOf(',') > 0){
			application.containerlist=containerstr.split(",");
			application.containernumber=application.containerlist.length;
	}else {
			application.containerlist=containerstr;
			application.containernumber=1;
	}
}


    var benchStartTime;
    function markTime()
    {
    	benchStartTime=new Date();
    	return benchStartTime.getTime();
    }
    function getElapsedTime()
    {
    	var now = new Date();
    	return now.getTime()-benchStartTime;
    }
	
	var benchStartTime2;
    function markTime2()
    {
    	benchStartTime2=new Date();
    	return benchStartTime2.getTime();
    }
    function getElapsedTime2()
    {
    	var now = new Date();
    	return now.getTime()-benchStartTime2;
    }


