package com.voxware.browser.property;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.Property;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.interpreter.Interpreter;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.ValidationContext;
import com.voxware.browser.model.ValidationException;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.parser.ValidationUtils;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;

import static org.junit.Assert.*;

public class PropertyTest {

    private Context context;
    private ClassLoader cl;
    private FileSystemDocumentServer docserver;
    private SessionContext sessionContext;
    private IoManager iomanager;
    @Mock private Player<String> mocktts;
    @Mock private Recognizer mockrecognizer;
    private DocumentParser parser;
    private Document document;
    
    @Mock
    private AudioPlayer audioPlayer;
    
    @Mock
    private ValidationContext validationContext;

    @Before
    public void setUp() throws Throwable {
        MockitoAnnotations.initMocks(this);

        cl = this.getClass().getClassLoader();
        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        context = Context.enter();
        

        Map<String, Object> props = new HashMap<String, Object>();

        docserver = new FileSystemDocumentServer(file.getParent());
        
        iomanager = new IoManager();
        iomanager.setRecognizer(mockrecognizer);
        iomanager.setAudioPlayer(audioPlayer);

        sessionContext = new SessionContext(context, props, iomanager, docserver);
        
        parser = DaggerVxmlParser.create().getDocumentParser();
        
        URI documentURI = new URI("http://localhost:8080/properties/propdoc.vxml");
        
        InputStream docStream = docserver.getDocument(documentURI);
        
        document = parser.parse(sessionContext, documentURI, docStream );
        
        
        Future<RecognitionResult> result = new Future<RecognitionResult>() {
            RecognitionResult res = RecognitionResult.success("here is your result");

            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            public boolean isCancelled() {
                return false;
            }

            public boolean isDone() {
                return true;
            }

            public RecognitionResult get() throws InterruptedException, ExecutionException {
                return res;
            }

            public RecognitionResult get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return res;
            }
        };

        when(mockrecognizer.recognize(anyString())).thenReturn(result);
        iomanager.setRecognizer(mockrecognizer);
        
        when(validationContext.getValidationUtils()).thenReturn(new ValidationUtils());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() throws InterpreterException, ParserException, DocumentServerException, URISyntaxException {
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        //<property name="form.prop" value="formvalue"/>

        verify(mockrecognizer).setParameter("form.prop", "formvalue");
        //<property name="field.prop" value ="fieldvalue"/>
        verify(mockrecognizer).setParameter("field.prop", "fieldvalue");
        
        //<property name="app.prop" value="appvalue"/>
        verify(mockrecognizer, never()).setParameter("field.prop", "appvalue");
        
        // <property name="doc.prop" value="appvalue"/>
        verify(mockrecognizer, never()).setParameter("doc.prop", "appvalue");
        
        verify(mockrecognizer).setParameter("formvar.prop", "bleepbloop");
        verify(mockrecognizer).setParameter("fieldvar.prop", "bleepbloop");
    }

    @Test(expected=ValidationException.class)
    public void testNullNameValidation() throws Exception {
        Property property = makeProperty(null, "1");
        property.validate(validationContext);
    }
    
    @Test(expected=ValidationException.class)
    public void testNeitherValueNorExprValidation() throws Exception {
        Property property = makeProperty("test", null, null);
        property.validate(validationContext);
    }
    
    @Test(expected=ValidationException.class)
    public void testBothValueAndExprValidation() throws Exception {
        Property property = makeProperty("test", "foo", "'foo'");
        property.validate(validationContext);
    }
    
    @Test
    public void testLiteralEvaluation() throws Exception {
        BaseContext context = sessionContext.createDocumentContext(null);
        assertEquals("", makeProperty("test", "").evaluate(context));
        assertEquals("   ", makeProperty("test", "   ").evaluate(context));
        assertNull(makeProperty("test", "null").evaluate(context));
        assertEquals(Context.getUndefinedValue(), makeProperty("test", "undefined").evaluate(context));
        assertEquals(Boolean.TRUE,makeProperty("test", "true").evaluate(context));
        assertEquals(Boolean.FALSE, makeProperty("test", "false").evaluate(context));
        assertEquals(Double.NaN, makeProperty("test", "NaN").evaluate(context));
        assertEquals(Double.POSITIVE_INFINITY, makeProperty("test", "Infinity").evaluate(context));
        assertEquals(Double.POSITIVE_INFINITY, makeProperty("test", "+Infinity").evaluate(context));
        assertEquals(Double.NEGATIVE_INFINITY, makeProperty("test", "-Infinity").evaluate(context));
        assertEquals(new Long(7), makeProperty("test", "7").evaluate(context));
        assertEquals(new Double(40.5d), makeProperty("test", "40.5").evaluate(context));
        
        Scriptable scriptable = (Scriptable) makeProperty("test", "{ \"foo\":\"16\", \"bar\":\"test\"}").evaluate(context);
        assertEquals("16", scriptable.get("foo", scriptable));
        assertEquals("test", scriptable.get("bar", scriptable));
        scriptable = (Scriptable) makeProperty("test", "[\"a\", \"b\", \"c\"]").evaluate(context);        
        assertEquals(3, scriptable.getIds().length);
        assertEquals("a", scriptable.get(0, scriptable));
        assertEquals("b", scriptable.get(1, scriptable));
        assertEquals("c", scriptable.get(2, scriptable));
        assertEquals("{ \"foo\":\"16\"", makeProperty("test", "{ \"foo\":\"16\"").evaluate(context));
        assertEquals("foo", makeProperty("test", "foo").evaluate(context));
    }
    
    protected Property makeProperty(String name, String value) {
        return makeProperty(name, value, null);
    }
    
    protected Property makeProperty(String name, String value, String expr) {
        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put(Property.NAME, name);
        attributes.put(Property.VALUE, value);
        attributes.put(Property.EXPR, expr);
        return new Property(attributes, new NodeLocation(URI.create("builtin://defaults/" + name)));
    }
}
