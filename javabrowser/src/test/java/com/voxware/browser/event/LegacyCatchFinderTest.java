/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.voxware.browser.context.BaseContext;

import static org.mockito.Mockito.when;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Jun 4, 2015 by: eric</li>
 * </ul>
 */

public class LegacyCatchFinderTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private CatchList eventList;
    private Catch good = new Catch(null, null, null);
    private Catch bad = new Catch(null, null, null);
    
    @Mock
    private BaseContext dummy;
    
    private CatchFinder catchFinder = new LegacyCatchFinder();
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when (dummy.evaluateToBoolean("cat")).thenReturn(false);
        when (dummy.evaluateToBoolean("dog")).thenReturn(true);
        eventList = new CatchList();
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", "foo.good");
        good = new Catch(props, null, null);
        props.put("event", "foo.bad");
        bad = new Catch(props, null, null);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }
    
    @Test 
    public void testNull() {
        assertNull(catchFinder.findCatch(null, dummy, eventList, "bar", 1));
    }

    @Test
    public void testOnePartName() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", "foo");
        Catch one = new Catch(props, null, null);
        eventList.addCatch(one);
        
        assertNull(catchFinder.findCatch(null, dummy, eventList, "bar", 1));
        assertEquals(one, catchFinder.findCatch(null, dummy, eventList, "foo", 1));
    }
    
    @Test 
    public void testTwoPartName() {

        eventList.addCatch(bad);
        eventList.addCatch(good);
        
        assertEquals(good, catchFinder.findCatch(null, dummy, eventList, "foo.good", 1));
    }
    
    @Test 
    public void testSpecificFirst() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", "foo.bar.specific");
        Catch specific = new Catch(props, null, null);
        props.put("event",  "foo.bar");
        Catch notSpecific = new Catch(props, null, null);
        eventList.addCatch(specific);
        eventList.addCatch(notSpecific);
        
        assertEquals(specific, catchFinder.findCatch(null, dummy, eventList, "foo.bar.specific", 1));
        assertEquals(notSpecific, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
    }

    @Test 
    public void testGeneralFirst() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", "foo.bar.specific");
        Catch specific = new Catch(props, null, null);
        props.put("event",  "foo.bar");
        Catch notSpecific = new Catch(props, null, null);
        eventList.addCatch(notSpecific);
        eventList.addCatch(specific);
        
        assertEquals(specific, catchFinder.findCatch(null, dummy, eventList, "foo.bar.specific", 1));
        assertEquals(notSpecific, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
    }
    
    @Test 
    public void testNullCatchall() {
        Map<String, String> props = new HashMap<String, String>();
        Catch catchAll = new Catch(props, null, null);
        eventList.addCatch(catchAll);
        props.put("event", "foo.bar");
        Catch katch = new Catch(props, null, null);
        eventList.addCatch(katch);
        
        assertEquals(katch, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
        assertEquals(catchAll, catchFinder.findCatch(null, dummy, eventList, "baz", 1));
    }
    
    @Test 
    public void testEmptyCatchall() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", "");
        Catch catchAll = new Catch(props, null, null);
        eventList.addCatch(catchAll);
        props.put("event", "foo.bar");
        Catch katch = new Catch(props, null, null);
        eventList.addCatch(katch);
        
        assertEquals(katch, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
        assertEquals(catchAll, catchFinder.findCatch(null, dummy, eventList, "baz", 1));
    }
    
    @Test 
    public void testDotCatchall() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", ".");
        Catch catchAll = new Catch(props, null, null);
        eventList.addCatch(catchAll);
        props.put("event", "foo.bar");
        Catch katch = new Catch(props, null, null);
        eventList.addCatch(katch);
        
        assertEquals(katch, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
        assertEquals(catchAll, catchFinder.findCatch(null, dummy, eventList, "baz", 1));
    }
    
    @Ignore("pathalogical case?")
    @Test 
    public void testSpaceDotCatchall() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", " . ");
        Catch catchAll = new Catch(props, null, null);
        eventList.addCatch(catchAll);
        props.put("event", "foo.bar");
        Catch katch = new Catch(props, null, null);
        eventList.addCatch(katch);
        
        assertEquals(catchAll, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
    }
    
    @Test
    public void testCatchCount() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", ".");
        props.put("count", "1");
        Catch catchAll1 = new Catch(props, null, null);
        eventList.addCatch(catchAll1);
        props.put("count", "2");
        Catch catchAll2 = new Catch(props, null, null);
        eventList.addCatch(catchAll2);
        
        assertEquals(catchAll1, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
        assertEquals(catchAll2, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 2));
    }
    
    @Test
    public void testCatchCond() {
        Map<String, String> props = new HashMap<String, String>();
        props.put("event", ".");
        props.put("cond", "cat");
        Catch catchAll1 = new Catch(props, null, null);
        eventList.addCatch(catchAll1);
        props.put("cond", "dog");
        Catch catchAll2 = new Catch(props, null, null);
        eventList.addCatch(catchAll2);
        
        assertEquals(catchAll2, catchFinder.findCatch(null, dummy, eventList, "foo.bar", 1));
    }
}

