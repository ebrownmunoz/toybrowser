/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.event;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.common.io.Resources;
import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerInputStream;
import com.voxware.browser.interpreter.Interpreter;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.log.LogService;
import com.voxware.browser.model.Document;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

/**
 * WeirdLogEventTest
 *
 * @author edh
 */
public class WeirdLogEventTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final URI START_PAGE = URI.create("start_page");
    private DocumentParser documentParser = DaggerVxmlParser.create().getDocumentParser();
    @Mock
    private IoManager ioManager;
    @Mock
    private Recognizer recognizer;
    @Mock
    private Player<String> tts;
    @Mock
    private Player<byte[]> bytePlayer;
    @Mock
    private Player<URI> uriPlayer;
    @Mock
    private DocumentServer documentServer;
    @Mock
    private Future<Void> ttsFuture;
    @Mock
    private Future<RecognitionResult> recFuture;
    @Mock
    private RecognitionResult dummyResult;
    private SessionContext sessionContext;

    @Mock
    private LogService test;
    @Mock
    private LogService test2;

    @Before
    public void setup() throws Throwable {
        MockitoAnnotations.initMocks(this);

        sessionContext = new SessionContext(null, ioManager, documentServer);
        when(ioManager.getRecognizer()).thenReturn(recognizer);

        AudioPlayer audioPlayer = new AudioPlayer(tts, bytePlayer, uriPlayer);
        when(ioManager.getAudioPlayer()).thenReturn(audioPlayer);
        //when(documentServer.getDocument(START_PAGE)).thenReturn(getClass().getResourceAsStream("/vxml_events/document_weird_event.vxml"));
        when(documentServer.getDocument(any(URI.class))).thenAnswer(new Answer<DocumentServerInputStream>() {
            @Override
            public DocumentServerInputStream answer(InvocationOnMock invocation) throws Throwable {
                byte[] bytes = Resources.toByteArray(getClass().getResource("/vxml_events/document_weird_event.vxml"));
                return new DocumentServerInputStream(new ByteArrayInputStream(bytes), bytes.length);
            }
        });
        when(documentServer.getDocument(any(URI.class), any(Map.class), any(CacheControl.class)))
            .thenAnswer(new Answer<DocumentServerInputStream>() {
                @Override
                public DocumentServerInputStream answer(InvocationOnMock invocation) throws Throwable {
                    byte[] bytes = Resources
                        .toByteArray(getClass().getResource("/vxml_events/document_weird_event.vxml"));
                    return new DocumentServerInputStream(new ByteArrayInputStream(bytes), bytes.length);
                }
            });
        when(documentServer.getDocument(any(URI.class), any(Map.class), any(CacheControl.class), anyInt(), anyInt()))
            .thenAnswer(new Answer<DocumentServerInputStream>() {
                @Override
                public DocumentServerInputStream answer(InvocationOnMock invocation) throws Throwable {
                    byte[] bytes = Resources
                        .toByteArray(getClass().getResource("/vxml_events/document_weird_event.vxml"));
                    return new DocumentServerInputStream(new ByteArrayInputStream(bytes), bytes.length);
                }
            });
        when(recognizer.recognize(anyString())).thenReturn(recFuture);
        when(tts.play(anyString())).thenReturn(ttsFuture);
        when(recFuture.get()).thenReturn(dummyResult);
    }

    @Test
    public void testRecogStartEvent() throws Exception {
        sessionContext.getLogManager().register("test", test);
        sessionContext.getLogManager().register("test2", test2);
        Interpreter interpreter = new Interpreter(sessionContext, documentServer);
        Document document = documentParser.parse(sessionContext, START_PAGE, documentServer.getDocument(START_PAGE));
        interpreter.loadDocument(document);
        interpreter.processDocument();
        verify(test).send("Message");
        verify(test2).send("Dynamic!!!");
    }
}
