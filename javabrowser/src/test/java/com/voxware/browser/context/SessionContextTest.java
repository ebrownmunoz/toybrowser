/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.EvaluatorException;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.scope.BrowserScope;
import com.voxware.browser.io.IoManager;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Apr 28, 2015 by: eric</li>
 * </ul>
 */
public class SessionContextTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private SessionContext sessionContext;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("volume", "66");
        
        Context context = Context.enter();
        
        sessionContext = new SessionContext(context, props, new IoManager(), null);
    }
    
    @After
    public void teardown() throws Exception {
        Context.exit();
    }

    @Test
    public void testEvaluateSimple() {
        String results = sessionContext.evaluateToString("3 + 2");
        if (!("5".equals(results) || "5.0".equals(results))) {
            fail();
        }
    }
    
    @Test
    public void testEvaluateProperty() {
        String results = sessionContext.evaluateToString("volume + 2");
        // TODO make this better once we get more sophisticated evaluation
        assertEquals("662", results);
    }
    
    @Test
    public void testSessionScopeVariable() {
        BrowserScope scope = new BrowserScope(null);
        Scriptable sessionScope = sessionContext.getScope();
        
        scope.setParentScope(sessionContext.getScope());
        
        Object result = 
            sessionContext.getContext().evaluateString(scope, "session.volume", "test", 1, null);
        assertEquals("66", result);
        
    }
    
    @Test(expected=EvaluatorException.class)
    public void testChangeSealedParameter() {
        sessionContext.evaluateToString("volume = 12;");
        String results = sessionContext.evaluateToString("volume + 2");
        Object originalVolume = sessionContext.getProperty("volume");
        fail("This should not be hit " + results);
    }

    @Test
    public void testMapProperty() {
        Map<String, Object> props = new HashMap<String, Object>();
        NativeObject hierarchy = new NativeObject();
        NativeObject leaf = new NativeObject();
        
        leaf.put("foo", leaf, "brig");
        hierarchy.put("follow", hierarchy, leaf);
        props.put("system", hierarchy);
        
        Context context = Context.enter();
        
        sessionContext = new SessionContext(context, props, new IoManager(), null);
        String results = sessionContext.evaluateToString("system.follow.foo");
        
        assertEquals("brig", results);
    
    }
}
