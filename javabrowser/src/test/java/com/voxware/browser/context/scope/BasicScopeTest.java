/*******************************************************************************
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 *******************************************************************************/
package com.voxware.browser.context.scope;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.SessionContext;

public class BasicScopeTest {

    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private Scriptable parentscope;
    private BrowserScope underTest;

    private Context cx;
    
    @Before
    public void setUp() throws Exception {
        
        cx = SessionContext.createContextFactory().enterContext();
        parentscope = cx.initStandardObjects();
        parentscope.put("parentscopeglobal", parentscope, null);
        
        cx.evaluateString(parentscope, "var parentscopelocal = 'plocal'; parentscopeglobal = 'pglobal' ",
            "foo", 1, null);
        
        underTest = new BrowserScope(null);

        underTest.setParentScope(parentscope);
        
    }
    
    @After
    public void tearDown() {
        Context.exit();
    }


    @Ignore
    @Test
    /**
     * Neither locals nor globals are inherited from parents for get (they are in ECMA)
     */
    public void testBasic() {
        String value = (String) parentscope.get("parentscopeglobal", parentscope);
        assertEquals("pglobal", value);
        
        String value2 = (String)parentscope.get("parentscopelocal", parentscope);
        assertEquals("plocal", value2);
        
        Object value3 = underTest.get("parentscopelocal", underTest);
        assertEquals(Scriptable.NOT_FOUND, value3);
        
        Object value4 = underTest.get("parentscopeglobal", underTest);
        assertEquals(Scriptable.NOT_FOUND, value4);
        
        Object evaluateString = cx.evaluateString(underTest, "parentscopeglobal", "foo", 1, null);
        Object evaluateString2 = cx.evaluateString(underTest, "parentscopelocal", "foo", 1, null);

        assertEquals("pglobal", evaluateString);
        assertEquals("plocal", evaluateString2);
    }
    
    @Ignore
    @Test
    public void testGetScopeName() {
        BrowserScope bscope = new BrowserScope("loose");
        bscope.put("tiger", bscope, "cat");
        underTest.put("tiger", underTest, "shark");
        underTest.setParentScope(bscope);
        
        Object result = cx.evaluateString(underTest, "loose.tiger", "foo", 1, null);
        
        assertEquals("cat", result);
        
        Object result2 = cx.evaluateString(underTest, "tiger", "foo", 1, null);
        
        assertEquals("shark", result2);
    }
    
    @Test
    public void testGetPutScopeName() {
        BrowserScope bscope = new BrowserScope("loose");
        underTest.setParentScope(bscope);
        bscope.put("tiger", bscope, null);
        
        cx.evaluateString(underTest, "loose.tiger = 'shark'", "foo", 1, null);
        
        assertEquals("shark", bscope.get("tiger", bscope));
        assertEquals(Scriptable.NOT_FOUND, underTest.get("tiger", underTest));
        
        underTest.put("tiger", underTest, "dog");
        
        cx.evaluateString(underTest, "loose.tiger = 'cat'", "foo", 1, null);
        assertEquals("cat", bscope.get("tiger", bscope));
        assertEquals("dog", underTest.get("tiger", underTest));
    
        
        
    }


}
