/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.model.Initializer;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.form.Block;
import com.voxware.browser.model.form.Filled;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;
import com.voxware.browser.model.form.Form;

public class FormContextTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private SessionContext sessionContext;
    private DocumentContext documentContext;
    private FormContext formContext;
    private Form form;

    @Before
    public void setUp() throws Exception {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("volume", "33");

        ContextFactory cf = new ContextFactory() {
            @Override
            public boolean hasFeature(Context cx, int feature) {
                if (feature == Context.FEATURE_STRICT_VARS || feature == Context.FEATURE_WARNING_AS_ERROR) {
                    return true;
                } else {
                    return super.hasFeature(cx, feature);
                }
            }
        };

        Context context = cf.enterContext();

        sessionContext = new SessionContext(context, props, new IoManager(), null);
        documentContext = sessionContext.createApplicationContext("app");

        form = new Form(new HashMap<String, String>(), new NodeLocation(null, 0, 0, null), null, null, new ArrayList<Filled>(), null, null, null, null);
        formContext = new FormContext(sessionContext, documentContext, null, new PropertyList());

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBooleanFIV() throws InterpreterException {
        FormItem fi = new Block(Collections.<String, String> emptyMap(), new NodeLocation(null, 0, 0, null), null);
        form = new Form(null, new NodeLocation(null, 0, 0, null), null, Arrays.asList(fi), new ArrayList<Filled>(), null, null, null, null);
        formContext.addUnnamedFormItemVariable(fi, null);

        assertFalse(formContext.isComplete(fi));
        formContext.setFormItemValue(fi, true);
        assertTrue(formContext.isComplete(fi));
    }

    @Test
    public void testNamedFIV() throws InterpreterException {
        FormItem fi = new Block(Collections.<String, String> emptyMap(), new NodeLocation(null, 0, 0, null), null);
        form = new Form(null, new NodeLocation(null, 0, 0, null), null, Arrays.asList(fi), new ArrayList<Filled>(), null, null, null, null);

        formContext.addNamedFormItemVariable(fi, "foo", null);
        assertFalse(formContext.isComplete(fi));
        formContext.setFormItemValue(fi, true);
        assertTrue(formContext.isComplete(fi));
    }

    @Test
    public void testNamedFIVScopeFirst() throws InterpreterException {
        FormItem fi = new Block(Collections.<String, String> emptyMap(), new NodeLocation(null, 0, 0, null), null);
        form = new Form(null, new NodeLocation(null, 0, 0, null), null, Arrays.asList(fi), new ArrayList<Filled>(), null, null, null, null);

        formContext.addNamedFormItemVariable(fi, "foo", null);

        Scriptable scope = formContext.getScope();

        scope.put("foo", scope, 1);

        assertTrue(formContext.isComplete(fi));
    }

    @Test
    public void testNamedFIVSetFirst() throws InterpreterException {
        FormItem fi = new Block(Collections.<String, String> emptyMap(), new NodeLocation(null, 0, 0, null), null);
        form = new Form(null, new NodeLocation(null, 0, 0, null), null, Arrays.asList(fi), new ArrayList<Filled>(), null, null, null, null);

        formContext.addNamedFormItemVariable(fi, "foo", null);

        formContext.setFormItemValue(fi, true);
        assertTrue(formContext.isComplete(fi));

        Object result = formContext.get("foo");

        assertEquals(Boolean.TRUE, result);
    }

    @Test
    public void testNamedFIVScopeWeirdFalseCase() throws InterpreterException {
        // Setting the FIV to false means it is still complete
        FormItem fi = new Block(Collections.<String, String> emptyMap(), new NodeLocation(null, 0, 0, null), null);
        form = new Form(null, new NodeLocation(null, 0, 0, null), null, Arrays.asList(fi), new ArrayList<Filled>(), null, null, null, null);

        formContext.addNamedFormItemVariable(fi, "foo", null);

        Scriptable scope = formContext.getScope();

        scope.put("foo", scope, false);

        assertTrue(formContext.isComplete(fi));
    }
}
