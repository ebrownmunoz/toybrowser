/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.context;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.EvaluatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.voxware.browser.io.IoManager;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Apr 28, 2015 by: eric</li>
 * </ul>
 */
public class DocumentContextTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    private static final Logger LOG = LoggerFactory.getLogger(DocumentContext.class);

    private SessionContext sessionContext;
    private DocumentContext documentContext;
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("volume", "33");
        
        ContextFactory cf = new ContextFactory() {
            @Override
            public boolean hasFeature(Context cx, int feature) {
                if (feature == Context.FEATURE_STRICT_VARS ||
                    feature == Context.FEATURE_WARNING_AS_ERROR) {
                    return true;
                } else {
                    return super.hasFeature(cx, feature);
                }
            }
        };
        
        Context context = cf.enterContext();
        
        sessionContext = new SessionContext(context, props, new IoManager(), null);
        documentContext = sessionContext.createApplicationContext("app");
    }
    
    @After
    public void teardown() throws Exception {
        Context.exit();
    }

    @Test
    public void testEvaluateSimple() {
        String results = documentContext.evaluateToString("3 + 2");
        assertEquals("5", results);
    }
    
    @Test
    public void testEvaluateProperty() {
        String results = documentContext.evaluateToString("volume + 2");
        
        // the double isn't important as long as we get some form of 66 + 2;
        // TODO make this better once we get more sophisticated evaluation
        assertEquals("332", results);
    }
    
    @Test(expected=EvaluatorException.class)
    @Ignore // this doesn't seem to test anything valid (we might should fix that)
    public void testChangeSealedParameter() {
        documentContext.evaluateToString("volume = 12;");
        String results = documentContext.evaluateToString("volume + 2");
        Object originalVolume = sessionContext.getProperty("volume");
        fail("This should not be hit " + results);
    }
    
    @Test
    public void testChangeParameter() {
        documentContext.evaluateToString("var dog = 44");
        String results = documentContext.evaluateToString("dog + 2");
        assertEquals("46", results);
    }
    
    @Ignore("This should throw an 'error.semantic' event based on strict.")
    @Test
    public void testCreateGlobalParameter() {
        String foo = documentContext.evaluateToString("dog = 55");
        String results = documentContext.evaluateToString("dog + 2");
        assertEquals("46.0", results);
    }
    
    @Test
    public void testPut() {
        documentContext.put("bert", "cat");
        String results = documentContext.evaluateToString("bert + 'meow'");
        
        assertEquals("catmeow", results);
    }
  

}
