/*
 * Copyright (c) 2016. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.audioplayer;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.voxware.browser.audioplayer.PromptResolver.FragmentProvider;
import com.voxware.browser.audioplayer.PromptResolver.FragmentProviderFactory;

import static org.mockito.Mockito.when;

import static org.mockito.Matchers.anyString;

import static org.junit.Assert.assertEquals;

/**
 * PromptResolverTest
 *
 * @author edh
 */
public class PromptResolverTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2016.";
    public static final String TEST_SOURCE = "test";
    
    private PromptResolver resolver = new PromptResolver();
    @Mock
    private FragmentProviderFactory factory;
    @Mock
    private FragmentProvider fragmentProvider;
    
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        resolver.setFragmentProviderFactory(factory);
        when(factory.getFragmentProvider(anyString())).thenReturn(fragmentProvider);
        
    }
    
    @Test
    public void exactMatchTest() {
        final String EXACT_MATCH = "exact match";
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(EXACT_MATCH));
        when(fragmentProvider.getFragment(EXACT_MATCH)).thenReturn(EXACT_MATCH);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, EXACT_MATCH);
        assertEquals(1, result.size());
        assertEquals(EXACT_MATCH, result.get(0));
    }
    
    @Test
    public void exactMatchTestWithWhitespace() {
        final String EXACT_MATCH = "I heard nothing";
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(EXACT_MATCH));
        when(fragmentProvider.getFragment(EXACT_MATCH)).thenReturn(EXACT_MATCH);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, " i heard nothing ");
        assertEquals(1, result.size());
        assertEquals(EXACT_MATCH, result.get(0));
    }

    
    @Test
    public void fragmentMatch() {
        final String FRAGMENT_1 = "A";
        final String FRAGMENT_2 = "B";
        final Object FRAGMENT_1_OBJ = new Object();
        final Object FRAGMENT_2_OBJ = new Object();
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(FRAGMENT_1, FRAGMENT_2));
        when(fragmentProvider.getFragment(FRAGMENT_1)).thenReturn(FRAGMENT_1_OBJ);
        when(fragmentProvider.getFragment(FRAGMENT_2)).thenReturn(FRAGMENT_2_OBJ);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, "A B");
        assertEquals(2, result.size());
        assertEquals(FRAGMENT_1_OBJ, result.get(0));
        assertEquals(FRAGMENT_2_OBJ, result.get(1));
    }
    
    @Test
    public void fragmentMatchSecondOnly() {
        final String FRAGMENT_2 = "B";
        final Object FRAGMENT_2_OBJ = new Object();
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(FRAGMENT_2));
        when(fragmentProvider.getFragment(FRAGMENT_2)).thenReturn(FRAGMENT_2_OBJ);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, "A B");
        assertEquals(1, result.size());
        assertEquals("A B", result.get(0));
    }    
    
    @Test
    public void fragmentMatchSecondOnly2() {
        final String FRAGMENT_2 = "B";
        final Object FRAGMENT_2_OBJ = new Object();
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(FRAGMENT_2));
        when(fragmentProvider.getFragment(FRAGMENT_2)).thenReturn(FRAGMENT_2_OBJ);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, "A B C");
        assertEquals(1, result.size());
        assertEquals("A B C", result.get(0));
    }
    
    @Test
    public void fragmentMatch2() {
        final String FRAGMENT_2 = "B";
        final Object FRAGMENT_2_OBJ = new Object();
        final String FRAGMENT_4 = "D";
        final Object FRAGMENT_4_OBJ = new Object();
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(FRAGMENT_2, FRAGMENT_4));
        when(fragmentProvider.getFragment(FRAGMENT_2)).thenReturn(FRAGMENT_2_OBJ);
        when(fragmentProvider.getFragment(FRAGMENT_4)).thenReturn(FRAGMENT_4_OBJ);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, "A B C D");
        assertEquals(1, result.size());
        assertEquals("A B C D", result.get(0));
    }
    
    @Test
    public void fragmentMatch121() {
        final String FRAGMENT_1 = "A";
        final Object FRAGMENT_1_OBJ = new Object();
        final String FRAGMENT_2 = "b c";
        final Object FRAGMENT_2_OBJ = new Object();
        final String FRAGMENT_3 = "D";
        final Object FRAGMENT_3_OBJ = new Object();
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(FRAGMENT_1, FRAGMENT_2, FRAGMENT_3));
        when(fragmentProvider.getFragment(FRAGMENT_1)).thenReturn(FRAGMENT_1_OBJ);
        when(fragmentProvider.getFragment(FRAGMENT_2)).thenReturn(FRAGMENT_2_OBJ);
        when(fragmentProvider.getFragment(FRAGMENT_3)).thenReturn(FRAGMENT_3_OBJ);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, "A B C D");
        assertEquals(3, result.size());
        assertEquals(FRAGMENT_1_OBJ, result.get(0));
        assertEquals(FRAGMENT_2_OBJ, result.get(1));
        assertEquals(FRAGMENT_3_OBJ, result.get(2));
    }
    
    @Test
    public void fragmentMatch121WithPunctuation() {
        final String FRAGMENT_1 = "A";
        final Object FRAGMENT_1_OBJ = new Object();
        final String FRAGMENT_2 = "B C";
        final Object FRAGMENT_2_OBJ = new Object();
        final String FRAGMENT_3 = "D";
        final Object FRAGMENT_3_OBJ = new Object();
        when(fragmentProvider.getAllFragments()).thenReturn(Arrays.asList(FRAGMENT_1, FRAGMENT_2, FRAGMENT_3));
        when(fragmentProvider.getFragment(FRAGMENT_1)).thenReturn(FRAGMENT_1_OBJ);
        when(fragmentProvider.getFragment(FRAGMENT_2)).thenReturn(FRAGMENT_2_OBJ);
        when(fragmentProvider.getFragment(FRAGMENT_3)).thenReturn(FRAGMENT_3_OBJ);
        List<Object> result = resolver.resolvePrompt(TEST_SOURCE, "A. B, C; D...");
        assertEquals(3, result.size());
        assertEquals(FRAGMENT_1_OBJ, result.get(0));
        assertEquals(FRAGMENT_2_OBJ, result.get(1));
        assertEquals(FRAGMENT_3_OBJ, result.get(2));
    }
}

