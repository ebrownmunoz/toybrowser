/**
 * 
 */
package com.voxware.browser.audioplayer;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.voxware.browser.io.Player;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.mockito.Matchers.any;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author eric
 *
 */
public class AudioPlayerTest {
	
	private class InnerFuture implements Future<Void> {
		
		@Override
		public boolean isDone() {
			return true;
		}
		
		@Override
		public boolean isCancelled() {
			return false;
		}
		
		@Override
		public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
			return null;
		}
		
		@Override
		public Void get() throws InterruptedException, ExecutionException {
			return null;
		}
		
		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			// TODO Auto-generated method stub
			return false;
		}
	}
	
	private AudioPlayer player;
	
	@Mock
	Player<String> tts;
	
	@Mock
	Player<byte[]> bytePlayer;
	
	@Mock
	Player<URI> uriPlayer;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		player=new AudioPlayer(tts, bytePlayer, uriPlayer);
	}

	/**
	 * Test method for {@link com.voxware.browser.audioplayer.AudioPlayer#play(java.util.List)}.
	 * @throws AudioPlayerException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testPlay() throws AudioPlayerException, InterruptedException, ExecutionException {
		
		when(tts.play(any(String.class))).thenReturn(new InnerFuture());
		Future<Void> play = player.play(wrappersFromStrings("one", "two", "three"));
		play.get();
		
		verify(tts).play("one two three");
		
	}
	
	private List<AudioWrapper> wrappersFromStrings(String...strings) {

		
		
		List<AudioWrapper> wrappers = new ArrayList<AudioWrapper>();
		for (String string : strings) {
			wrappers.add(new AudioWrapper(AudioPlayer.Type.STRING, string, null));			
		}
		
		return wrappers;
	}

	/**
	 * Test method for {@link com.voxware.browser.audioplayer.AudioPlayer#waitForCompletion()}.
	 * @throws AudioPlayerException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	@Test(timeout = 1200)
	public void testWaitForCompletion() throws AudioPlayerException, InterruptedException, ExecutionException {
		
		when(tts.play(any(String.class))).thenReturn(
				new InnerFuture() {
					public Void get() throws InterruptedException, ExecutionException {
						synchronized(this) {
							Thread.sleep(200);
						}
						return super.get();
					}
				});
		
		Future<Void> play = player.play(wrappersFromStrings("four", "five", "six"));
		assertFalse(play.isCancelled() || play.isDone());
		
		play.get();
		assertTrue(play.isDone());
		verify(tts).play("four five six");		
	}

	/**
	 * Test method for {@link com.voxware.browser.audioplayer.AudioPlayer#cancel()}.
	 * @throws AudioPlayerException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	@Test
	public void testCancel() throws AudioPlayerException, InterruptedException, ExecutionException {
		when(tts.play(any(String.class))).thenReturn(
				new InnerFuture() {
					public Void get() throws InterruptedException, ExecutionException {
						synchronized(this) {
							Thread.sleep(200);
						}
						return super.get();
					}
				});
		
		Future<Void> play = player.play(wrappersFromStrings("four", "five", "six"));
		assertFalse(play.isCancelled() || play.isDone());
		play.cancel(true);
		try {
		    play.get();
		    fail();
		} catch (CancellationException e) {
		    assertTrue(play.isCancelled());
		}
	}
	
	/**
	 * Test method for {@link com.voxware.browser.audioplayer.AudioPlayer#cancel()}.
	 * @throws AudioPlayerException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	@Test
	public void testPlayerCancel() throws AudioPlayerException, InterruptedException, ExecutionException {
		when(tts.play(any(String.class))).thenReturn(
				new InnerFuture() {
					public Void get() throws InterruptedException, ExecutionException {
						synchronized(this) {
							Thread.sleep(400);
						}
						return super.get();
					}
				});
		
		Future<Void> play = player.play(wrappersFromStrings("four", "five", "six"));
		assertFalse(play.isCancelled() || play.isDone());
		
		// Use player to cancel from another thread.
		new Thread(new Runnable() {

			@Override
			public void run() {
				synchronized(this) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					player.cancel();
				}
			}
			
		}).start();
		
		try {
		    play.get();
		    fail();
		} catch (CancellationException e) {
		    
		}
		assertTrue(play.isCancelled());
		
		
	}

}
