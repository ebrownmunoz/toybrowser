/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mozilla.javascript.Context;

import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.InterruptLinkNode;

public class DocumentParserTest {

    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final URI URL = URI.create("http://localhost:8080/vxmlparser/simple.vxml");
    private static final URI DOBBY_URL = URI.create("http://localhost:8080/vxmlparser/dobby.vxml");

    private DocumentParser parser;

    private FileSystemDocumentServer docserver;

    private ClassLoader cl = getClass().getClassLoader();

    private SessionContext sessionContext;

    Context context;

    @Before
    public void setUp() throws Exception {
        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        context = Context.enter();

        Map<String, Object> props = new HashMap<String, Object>();

        docserver = new FileSystemDocumentServer(file.getParent());

        sessionContext = new SessionContext(context, props, new IoManager(), docserver);

        parser = DaggerVxmlParser.create().getDocumentParser();
    }

    @Ignore
    @Test
    public void testParseVXML() throws ParserException, DocumentServerException {
        parser.parse(sessionContext, URL, docserver.getDocument(URL));
    }

    @Test
    public void testParseVar() throws ParserException, DocumentServerException {
        Document doc = parser.parse(sessionContext, DOBBY_URL, docserver.getDocument(DOBBY_URL));

        DocumentContext dcontext = sessionContext.createDocumentContext(null);
        dcontext.applyInitializers(doc.getInitializers(), null);

        assertEquals("dobby", dcontext.evaluate("dog"));
        assertEquals("dobby,alyosha", dcontext.evaluate("pets"));
        assertTrue(dcontext.getScope().has("lizard", dcontext.getScope()));
        assertFalse(dcontext.getScope().has("lksdfie", dcontext.getScope()));

        assertEquals(Context.getUndefinedValue(), dcontext.evaluate("lizard"));
    }
    
    @Test
    public void testParseLink() throws ParserException, DocumentServerException {
        Document doc = parser.parse(sessionContext, DOBBY_URL, docserver.getDocument(DOBBY_URL));
        List<InterruptLinkNode> links = doc.getLinks();
        InterruptLinkNode node = links.get(0);
        assertEquals("event.foo", node.getLinkEvent());
        assertEquals("foo", node.getSource());
    }

    @Test
    public void testParseBlock() throws ParserException, DocumentServerException {
        Document doc = parser.parse(sessionContext, URL, docserver.getDocument(URL));
    }
}
