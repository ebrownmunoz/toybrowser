/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.model.Executable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.executable.Prompt;
import com.voxware.browser.model.form.Block;
import com.voxware.browser.model.form.Field;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.resource.Grammar;
import com.voxware.browser.resource.Speaker;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: May 23, 2015 by: eric</li>
 * </ul>
 */
public class FormParserTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private FormParser parser; 
    /**
     * @throws java.lang.Exception
     */
    
    private static final String BLOCK1 = "<block>\n"
        + "This is some text</block>\n";
    
    private static final String BLOCKNAMED = "<form><block name=\"foo\">\n"
        + "This is some named text</block></form>\n";
    
    private static final String BLOCK2 = 
        "<block><prompt>This is some text</prompt>\n"
        + "<prompt>and more words to test</prompt></block>\n";
    
    private static final String FIELD1 = "<field>\n"
        + "<prompt>Hello there</prompt>\n"
        + "<grammar src=\"city.grm\"/>\n"
        + "<catch event=\"help\">foo bar bazz</catch>\n"
        + "</field>";
    
    private static final String FORM1 = "<form>\n"
        + "<grammar expr=\"ggram.grm\"/>\n"
        + "<speaker src=\"pryan.voi\"/>\n"
        + "</form>";
    
    private SessionContext scontext;
    private Scriptable sessionScope;

    private AnonymousContext acontext;

    private FormContext fcontext;
    
    @Before
    public void setUp() throws Exception {
        Context context = Context.enter();
        Map<String, Object> configuration = new HashMap<String, Object>();
        scontext = new SessionContext(context, configuration, new IoManager(), null);
        sessionScope = scontext.getScope();
        
        DocumentContext doccontext = scontext.createDocumentContext(null);
        
        Form form = new Form(new HashMap<String, String> (), new NodeLocation(URI.create("test")), null, null, null, null, null, null, null);
        fcontext = new FormContext(scontext, doccontext, new CatchList(), new PropertyList(doccontext.getPropertyList()));
        
        acontext = new AnonymousContext(fcontext);
        
        parser = DaggerTester.create().getFormParser();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link com.voxware.browser.parser.FormParser#parseVXML(org.xmlpull.v1.XmlPullParser)}.
     * @throws IOException 
     * @throws XmlPullParserException 
     * @throws ParserException 
     */
    @Test
    public void testParseBlockTest() throws XmlPullParserException, IOException, ParserException {
        XmlPullParser xmlparser = createPullParser(BLOCK1);
          
        Block block = DaggerTester.create().getBlockParser().parse(new ParsingContext(scontext, xmlparser, URI.create("test")));
        assertEquals(1, block.getExecutableList().size());
        
        Executable executable = block.getExecutableList().get(0);
        
        assertTrue(executable instanceof Prompt);
        
        assertTrue(((Prompt)executable).getOutput(scontext, acontext).contains("some text"));
        
        
    }
    
    @Test
    public void testParseBlockPrompt() throws XmlPullParserException, IOException, ParserException  {
        XmlPullParser xmlparser = createPullParser(BLOCK2);
        
        Block block = DaggerTester.create().getBlockParser().parse(new ParsingContext(scontext, xmlparser, URI.create("test")));
        assertEquals(2, block.getExecutableList().size());
        
        Executable executable = block.getExecutableList().get(0);
        
        assertTrue(executable instanceof Prompt);
        
        assertTrue(((Prompt)executable).getOutput(scontext, acontext).contains("some text"));
        
        executable = block.getExecutableList().get(1);
        
        assertTrue(executable instanceof Prompt);
        
        assertTrue(((Prompt)executable).getOutput(scontext, acontext).contains("more words"));
        
    }
    
    @Test
    public void testField1() throws XmlPullParserException, IOException, ParserException {
        XmlPullParser xmlparser = createPullParser(FIELD1);
        
        Field field = DaggerTester.create().getFieldParser().parse(new ParsingContext(scontext, xmlparser, URI.create("test")));
        
        List<Prompt> prompts = field.getPrompts();
        Prompt aprompt = prompts.get(0);
        
        assertEquals(1, prompts.size());
        assertEquals("TextPlayable: Hello there", aprompt.toString());
//        assertNull(field.findEvent(null, "foo", 1));
//        assertNotNull(field.findEvent(null, "help", 1));
        assertEquals("city.grm", field.getGrammars().get(0).getSrc());

    }
    
    @Test
    public void testBlockNamed() throws XmlPullParserException, IOException, ParserException, InterpreterException {
        XmlPullParser xmlparser = createPullParser(BLOCKNAMED);
        
        Form form = parser.parse(new ParsingContext(scontext, xmlparser, URI.create("test")));
        
        FormItem block = form.getFormItems().get(0);
        
        assertTrue(block.isScoped());
        
    }
    
    @Test
    public void testFormGrammar() throws Exception {
        XmlPullParser xmlparser = createPullParser(FORM1);
        Form form = parser.parse(new ParsingContext(scontext, xmlparser, URI.create("test")));
        
        Grammar grammar = form.getGrammar();
        Speaker speaker = form.getSpeaker();
        
        assertNull(grammar.getSrc());
        assertEquals("ggram.grm", grammar.getExpr());
        assertEquals("pryan.voi", speaker.getSrc());
        
    }
    
    private XmlPullParser createPullParser(String vxml) throws XmlPullParserException, IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(vxml.getBytes("UTF-8"));
        XmlPullParser xmlparser = (XmlPullParser) XmlPullParserFactory.newInstance().newPullParser();
        xmlparser.setInput(bais, "UTF-8"); 
        
        xmlparser.nextTag();
        
        return xmlparser;
    }

}
