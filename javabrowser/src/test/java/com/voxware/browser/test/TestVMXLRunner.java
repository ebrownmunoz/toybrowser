/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.test;

import java.io.Closeable;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.mozilla.javascript.Context;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.interpreter.Interpreter;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;

/**
 * <ul>
 * <li>Title: </li>
 * <li>Description: </li>
 * <li>Created: Aug 11, 2015 by: eric</li>
 * </ul>
 */
public class TestVMXLRunner implements Closeable {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    private DocumentServer docserver;
    private SessionContext sessionContext;
    private DocumentParser parser;
    
    private Recognizer recognizer = null;
    
    private Player<String> sysOutTts = getSysOutTTS();
    
    private static Player<String> getSysOutTTS() {
    	return new Player<String>() {
            
            @Override
            public Future<Void> play(String speech) {
                System.out.println(speech);
                return new Future<Void>() {
                    private boolean cancelled = false;
                    @Override
                    public boolean cancel(boolean mayInterruptIfRunning) {
                        return cancelled = true;
                    }

                    @Override
                    public boolean isCancelled() {
                        return cancelled;
                    }

                    @Override
                    public boolean isDone() {
                        return true;
                    }

                    @Override
                    public Void get() throws InterruptedException, ExecutionException {
                        if (cancelled) {
                            throw new CancellationException();
                        } else {
                            return null;
                        }
                    }

                    @Override
                    public Void get(long timeout, TimeUnit unit)
                        throws InterruptedException, ExecutionException, TimeoutException {
                        if (cancelled) {
                            throw new CancellationException();
                        } else {
                            return null;
                        }
                    }
                };
            }
            
            @Override
            public void cancel() {
            }

            @Override
            public void setParameter(String name, Object value) {
            }

            @Override
            public Object getParameter(String name) {
                return null;
            }

            @Override
            public void shutdown() {
                // TODO Auto-generated method stub
                
            }
            
    	};
    }
    
    /**
     * Constructs a new <code>TestVMXLRunner</code> instance with
     * a default FileDocumentServer
     * @throws DocumentServerException
     */
    public TestVMXLRunner() throws DocumentServerException {
    	 ClassLoader cl = getClass().getClassLoader();
         String path = cl.getResource("foo.txt").getPath();
         File file = new File(path);

         docserver = new FileSystemDocumentServer(file.getParent());
        
    }
    
    public TestVMXLRunner(DocumentServer docserver) {
    	this.docserver = docserver;
    }
    
    public void setTTs (Player<String> tts) {
    	this.sysOutTts = tts;
    }
    
    public void setRecognizer(Recognizer recognizer) {
    	this.recognizer = recognizer;
    }
    
    /**
     * @param url
     * @return
     * @throws DocumentServerException
     * @throws ParserException
     * @throws URISyntaxException
     * @throws InterpreterException
     */
    public InterpreterState runVXMLTest (String url) throws DocumentServerException, ParserException, URISyntaxException, InterpreterException {
    	
        Context context = Context.enter();
        Map<String, Object> props = new HashMap<String, Object>();
    	
    	IoManager ioManager = new IoManager();
        
        AudioPlayer audioPlayer = new AudioPlayer(sysOutTts, null, null);
        ioManager.setAudioPlayer(audioPlayer);
        ioManager.setRecognizer(recognizer);
        
        sessionContext = new SessionContext(context, props, ioManager, docserver);
        parser = DaggerVxmlParser.create().getDocumentParser();
        URI uri = new URI(url);
        Document document = parser.parse(sessionContext, uri, docserver.getDocument(uri));
        
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
    
        return interpreter.getInterpreterState();
    }
    
    /* 
     * (non-Javadoc)
     * @see java.io.Closeable#close()
     */
    public void close() {
        Context.exit();
    }
    
    

}
