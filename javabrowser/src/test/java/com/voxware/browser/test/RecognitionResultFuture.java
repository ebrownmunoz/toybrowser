package com.voxware.browser.test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.voxware.browser.io.RecognitionResult;

public class RecognitionResultFuture implements Future<RecognitionResult> {
	
	private RecognitionResult result;
	boolean isDone = false;
	boolean isCancelled = false;
	
	public RecognitionResultFuture(RecognitionResult result) {
		this.result = result;
	}
	
	@Override
	public synchronized boolean cancel(boolean mayInterruptIfRunning) {
		isCancelled = true;
		return true;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public boolean isDone() {
		return isDone;
	}

	@Override
	public RecognitionResult get() throws InterruptedException, ExecutionException {
		
		if (isDone) {
			return result;
		} else if (isCancelled) {
			return null;
		} else {
			synchronized(this) {
				Thread.sleep(500);
				isDone = true;
			}
			
			return result;
			
		}
	}

	@Override
	public RecognitionResult get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

}
