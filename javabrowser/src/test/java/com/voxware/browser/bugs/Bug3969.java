/**
 * 
 */
package com.voxware.browser.bugs;

import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.test.RecognitionResultFuture;
import com.voxware.browser.test.TestVMXLRunner;

import static org.mockito.Mockito.when;

import static org.mockito.Matchers.anyString;

import static org.junit.Assert.assertTrue;

/**
 * @author eric
 *
 */
public class Bug3969 {
	
	@Mock
	private Recognizer recognizer;
	
	private TestVMXLRunner runner;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		
		runner = new TestVMXLRunner();
		runner.setRecognizer(recognizer);
		
		
	}

	@Test
	public void test() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException, BrowserEvent {
		
		RecognitionResultFuture one = 
				new RecognitionResultFuture(RecognitionResult.success("cooler"));
		

		when(recognizer.recognize(anyString())).thenReturn(one);
		
		try {
		
		InterpreterState state = runner.runVXMLTest("http://localhost:8080/bugs/jira3969.vxml");
		
		assertTrue(true);
		} finally {
			//runner.close();
		}
	}

}
