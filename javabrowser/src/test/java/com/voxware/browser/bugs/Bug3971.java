/**
 * 
 */
package com.voxware.browser.bugs;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.test.RecognitionResultFuture;
import com.voxware.browser.test.TestVMXLRunner;

/**
 * @author eric
 *
 */
public class Bug3971 {
	
	@Mock
	private Recognizer recognizer;
	
	private TestVMXLRunner runner;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		
		runner = new TestVMXLRunner();
		runner.setRecognizer(recognizer);
		
		
	}

	@Test
	public void test() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException, BrowserEvent {
		
		RecognitionResultFuture one = 
				new RecognitionResultFuture(RecognitionResult.success("one"));
		RecognitionResultFuture two = 
				new RecognitionResultFuture(RecognitionResult.event(Result.event("noinput", "noinput")));
		RecognitionResultFuture three = 
				new RecognitionResultFuture(RecognitionResult.success("quit"));
		

		when(recognizer.recognize(anyString())).thenReturn(one).thenReturn(two).thenReturn(three);
		
		try {
		
		InterpreterState state = runner.runVXMLTest("http://localhost:8080/bugs/Jira3971.vxml");
		
		assertTrue(true);
		} finally {
			//runner.close();
		}
	}

}
