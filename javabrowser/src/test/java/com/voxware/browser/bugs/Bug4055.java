/**
 * 
 */
package com.voxware.browser.bugs;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.test.RecognitionResultFuture;
import com.voxware.browser.test.TestVMXLRunner;

/**
 * @author eric
 *
 */
public class Bug4055 {
	
	@Mock
	private Recognizer recognizer;
	
	private TestVMXLRunner runner;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		
		runner = new TestVMXLRunner();
		runner.setRecognizer(recognizer);
		
		
	}

	@Ignore // This test doesn't do anything, and fails on load?
	@Test
	public void test() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException, BrowserEvent {
		

		RecognitionResultFuture one = 
            new RecognitionResultFuture(RecognitionResult.success("load 123 confirm"));
		

		when(recognizer.recognize(anyString())).thenReturn(one);
		
		try {
		
		InterpreterState state = runner.runVXMLTest("http://localhost:8080/bugs/jira4055.vxml");
		
		assertTrue(true);
		} finally {
			//runner.close();
		}
	}

}
