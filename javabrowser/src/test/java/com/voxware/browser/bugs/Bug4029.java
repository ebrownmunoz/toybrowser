/**
 * 
 */
package com.voxware.browser.bugs;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.mockito.Mockito.*;

import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.test.RecognitionResultFuture;
import com.voxware.browser.test.TestVMXLRunner;

/**
 * @author eric
 *
 */
public class Bug4029 {
	
	@Mock
	private Recognizer recognizer;
	
	private TestVMXLRunner runner;
	
    private DocumentParser parser;

	private FileSystemDocumentServer docserver;
	
	private Logger log = LoggerFactory.getLogger(Bug4029.class);
	
	private Context context;
    
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
        ClassLoader cl = getClass().getClassLoader();
        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);
        
        docserver = new FileSystemDocumentServer(file.getParent());
		
		
		runner = new TestVMXLRunner();
		runner.setRecognizer(recognizer);
		context = Context.enter();
		
		
	}
	
	@After
	public void shutdDown() throws Exception {
		Context.exit();
	}

	@Test
	public void testParse() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException, BrowserEvent {
        
        Map<String, Object> props = new HashMap<String, Object>();
    	IoManager ioManager = new IoManager();
        
        SessionContext sessionContext = new SessionContext(context, props, ioManager, docserver);
        
		String url = "bugs/prefetch.vxml";
		
        parser = DaggerVxmlParser.create().getDocumentParser();
        URI uri = new URI(url);
        Document document = parser.parse(sessionContext, uri, docserver.getDocument(uri));
        log.info("here");

	}
	
	@Test
	public void testRun() throws ParserException, InterpreterException, DocumentServerException, URISyntaxException {
		InterpreterState state = runner.runVXMLTest("http://localhost:8080/bugs/prefetch.vxml#boot");
		assertEquals("skip", state.getAppContext().evaluateToString("application.state.prevPrompt"));

	}

}
