/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.ApplicationContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.model.NodeLocation;

public class GotoTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    Map<String, String> attributes;

    private SessionContext scontext;

    private Scriptable sessionScope;

    private ApplicationContext dcontext;

    private FormContext fcontext;

    @Before
    public void setUp() throws Exception {
        attributes = new HashMap<String, String>();

        Context context = Context.enter();
        Map<String, Object> configuration = new HashMap<String, Object>();

        String path = getClass().getClassLoader().getResource("foo.txt").getPath();
        File file = new File(path);

        FileSystemDocumentServer docserver = new FileSystemDocumentServer(file.getParent());
        scontext = new SessionContext(context, configuration, new IoManager(), docserver);

        dcontext = scontext.createApplicationContext("foo");
        fcontext = new FormContext(scontext, dcontext, new CatchList(), new PropertyList(dcontext.getPropertyList()));

        sessionScope = scontext.getScope();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testExecuteNext() throws InterpreterException {

        AnonymousContext acontext = new AnonymousContext(fcontext);

        attributes.put("next", "#foo");

        Goto underTest = new Goto(attributes, new NodeLocation(URI.create("test")));
        Result result = underTest.execute(scontext, acontext);

        assertEquals("#foo", result.getNextURL());
        assertEquals(Result.Type.GOTO, result.getType());

    }

    @Test
    public void testExecuteExpr() throws InterpreterException {

        Scriptable fscope = fcontext.getScope();
        fscope.put("dem", fscope, "bones");

        AnonymousContext acontext = new AnonymousContext(fcontext);

        attributes.put("expr", "'#' + dem");

        Goto underTest = new Goto(attributes, new NodeLocation(URI.create("test")));
        Result result = underTest.execute(scontext, acontext);

        assertEquals("#bones", result.getNextURL());
        assertEquals(Result.Type.GOTO, result.getType());

    }

//    @Ignore // we ignore this now because next or expr is part of the constructor and the parser should not pass in a null
//    public void testNoAttributes() throws InterpreterException {
//        AnonymousContext acontext = new AnonymousContext(fcontext, sessionScope, new EventList());
//
//        Goto underTest = new Goto(attributes, (String)null, new NodeLocation(URI.create("test")));
//        Result result = underTest.execute(scontext, acontext, null);
//
//        assertEquals(Result.Type.EVENT, result.getType());
//    }

//    @Ignore // we don't need this test anymore because next or expr is passed in via one of two constructors, so they wlil never have both defined
//    public void testBothAttributes() throws InterpreterException {
//        AnonymousContext acontext = new AnonymousContext(fcontext, sessionScope, new EventList());
//
//        attributes.put("next", "#foo");
//        attributes.put("expr", "'#' + dem");
//
//        Goto underTest = new Goto(attributes, "test", 0);
//        Result result = underTest.execute(scontext, acontext, null);
//
//        assertEquals(Result.Type.EVENT, result.getType());
//    }
}
