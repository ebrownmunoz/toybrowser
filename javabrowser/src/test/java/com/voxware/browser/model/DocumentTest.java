/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class DocumentTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testResolveRelativeURI() throws URISyntaxException {
        Map<String, String> attributes = new HashMap<String, String>();
        Document doc = new Document(attributes, new NodeLocation(new URI("http://localhost:8080/fish/foo.html"), 0, 0, null), null, null, null, null, null);

        assertEquals("http://localhost:8080/fish/bar.html", doc.resolveRelativeURI("bar.html").toString());
        assertEquals("http://localhost:8080/bar.html", doc.resolveRelativeURI("/bar.html").toString());

    }

    @Test
    public void testResolveRelativeURITop() throws URISyntaxException {
        Map<String, String> attributes = new HashMap<String, String>();
        Document doc = new Document(attributes, new NodeLocation(new URI("http://localhost:8080/foo.html"), 0, 0, null), null, null, null, null, null);

        assertEquals("http://localhost:8080/bar.html", doc.resolveRelativeURI("bar.html").toString());
        assertEquals("http://localhost:8080/bar.html", doc.resolveRelativeURI("/bar.html").toString());

    }

}
