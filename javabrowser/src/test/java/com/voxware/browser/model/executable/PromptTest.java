/*******************************************************************************
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 *******************************************************************************/
package com.voxware.browser.model.executable;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.audioplayer.AudioPlayerException;
import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.io.CompositePlayable;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.TextPlayable;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.text.TextString;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

public class PromptTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final String BLOCK = "<block><prompt>This is some text</prompt>\n" + "<prompt>and more words to test</prompt></block>\n";

    private SessionContext scontext;

    private Scriptable sessionScope;

    private AnonymousContext acontext;

    private DocumentContext documentContext;

    private FormContext formContext;
    
    @Mock
    private Player<String> tts;
    @Mock
    private Player<byte[]> bytePlayer;
    @Mock
    private Player<URI> uriPlayer;
    
    @Mock
    private IoManager ioManager;
    


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        
    	when(tts.play(anyString())).thenReturn(new Future<Void>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isCancelled() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDone() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public Void get() throws InterruptedException, ExecutionException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Void get(long timeout, TimeUnit unit)
					throws InterruptedException, ExecutionException, TimeoutException {
				// TODO Auto-generated method stub
				return null;
			}
		});
    	
        Context context = Context.enter();
        Map<String, Object> configuration = new HashMap<String, Object>();
        
        AudioPlayer audioPlayer = new AudioPlayer(tts, bytePlayer, uriPlayer);
        when(ioManager.getAudioPlayer()).thenReturn(audioPlayer);
        scontext = new SessionContext(context, configuration, ioManager, null);
        sessionScope = scontext.getScope();
        documentContext = scontext.createApplicationContext("app");

        Form form = new Form(new HashMap<String, String>(), new NodeLocation(URI.create("test")), null, null, null, null, null, null, null);
        formContext = new FormContext(scontext, documentContext, new CatchList(), new PropertyList(documentContext.getPropertyList()));
        acontext = new AnonymousContext(formContext);
    }

    @Test
    public void testExecutePromptText() throws AudioPlayerException {
        Prompt prompt = new Prompt(Collections.<String, String> emptyMap(), new NodeLocation(URI.create("test")), new CompositePlayable("This is a test"));
        prompt.execute(scontext, acontext);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(tts).play(captor.capture());

        assertEquals("This is a test", captor.getValue());
    }
}
