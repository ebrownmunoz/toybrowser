/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.parser.ParserException;

public class ScriptTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final String SCRIPT_FOO = "var foo = 'queen';";
    private static final String SCRIPT_DEFINED = "super = 'man';";
    private static final String SCRIPT_UNDEFINED = "bogus = 'blech';";
    private static final String SCRIPT_URL = "test_script.js";

    private SessionContext scontext;

    private Scriptable sessionScope;

    private AnonymousContext acontext;

    private DocumentContext documentContext;

    private FormContext formContext;

    private Map<String, String> attributes;

    @Before
    public void setUp() throws Exception {
        Context context = Context.enter();
        Map<String, Object> configuration = new HashMap<String, Object>();

        ClassLoader cl = getClass().getClassLoader();

        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        scontext = new SessionContext(context, configuration, new IoManager(), new FileSystemDocumentServer(file.getParent()));
        sessionScope = scontext.getScope();

        documentContext = scontext.createApplicationContext("app");

        Form form = new Form(new HashMap<String, String>(), new NodeLocation(URI.create("test")), null, null, null, null, null, null, null);

        formContext = new FormContext(scontext, documentContext, new CatchList(), new PropertyList(documentContext.getPropertyList()));

        acontext = new AnonymousContext(formContext);

        documentContext.getScope().put("supper", documentContext.getScope(), "dog");

        attributes = new HashMap<String, String>();
    }

    @Test
    public void testContent() throws ParserException, InterpreterException {
        Script script = new Script(attributes, new NodeLocation(URI.create("test")), SCRIPT_FOO);

        Result result = script.execute(scontext, acontext);

        assertEquals(Result.Type.OK, result.getType());
        assertEquals("queen", acontext.getScope().get("foo", acontext.getScope()));

    }

    @Test
    public void testSrc() throws InterpreterException {
        attributes.put("src", "test_script.js");
        Script script = new Script(attributes, new NodeLocation(URI.create("test")));
        Result result = script.execute(scontext, acontext);

        assertEquals(Result.Type.OK, result.getType());
        assertEquals("fish", documentContext.getScope().get("supper", documentContext.getScope()));

    }

//    this test is no longer valid
//    @Test(expected = ParserException.class)
//    public void testBoth() throws ParserException, InterpreterException {
//        attributes.put("src", "test_script.js");
//        Script script = new Script(attributes, URI.create("test_script.js"), new NodeLocation(URI.create("test")));
//        script.setContent(SCRIPT_FOO);
//
//    }

//    this test is no longer valid
//    @Test
//    public void testNeither() throws ParserException, InterpreterException {
//        Script script = new Script(attributes, "test", 0);
//
//        Result result = script.execute(scontext, acontext, null);
//
//        assertEquals(Result.Type.EVENT, result.getType());
//        assertEquals("error.semantic", result.getEvent());
//
//    }

}
