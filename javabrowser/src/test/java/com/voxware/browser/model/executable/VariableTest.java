/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.AnonymousContext;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.FormItemVariable;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.interpreter.Interpreter;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;

public class VariableTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final URI FORM_URL = URI.create("http://localhost:8080/vxmlparser/form_item_variables.vxml");

    private SessionContext scontext;

    private DocumentContext docContext;

    private FormContext formContext;

    private AnonymousContext aContext;

    private Map<String, String> attrs;
    
    private AudioPlayer audioPlayer;
    
    @Mock
    private Player<String> tts;
    
    @Mock
    private Player<byte[]> bytePlayer;
    @Mock
    private Player<URI> uriPlayer;

    @Before
    public void setUp() throws Exception {
    	MockitoAnnotations.initMocks(this);
        Map<String, Object> properties = new HashMap<String, Object>();
        
        audioPlayer = new AudioPlayer(tts, bytePlayer, uriPlayer);
        
    	when(tts.play(anyString())).thenReturn(new Future<Void>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isCancelled() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDone() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public Void get() throws InterruptedException, ExecutionException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Void get(long timeout, TimeUnit unit)
					throws InterruptedException, ExecutionException, TimeoutException {
				// TODO Auto-generated method stub
				return null;
			}
		});
        
        IoManager iomanager = new IoManager();
        iomanager.setAudioPlayer(audioPlayer);

        String path = this.getClass().getClassLoader().getResource("foo.txt").getPath();
        File file = new File(path);

        DocumentServer docServer = new FileSystemDocumentServer(file.getParent());

        scontext = new SessionContext(properties, iomanager, docServer);
        docContext = scontext.createDocumentContext(null);
        formContext = new FormContext(scontext, docContext, docContext.getEventList(), new PropertyList(docContext.getPropertyList()));
        aContext = new AnonymousContext(formContext);
        attrs = new HashMap<String, String>();
        attrs.put("name", "turtle");
        attrs.put("expr", "'myrtle'");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBasic() {
        Var var = new Var(attrs, new NodeLocation(URI.create("file")));
        var.execute(scontext, aContext);
        assertEquals("myrtle", aContext.getScope().get("turtle", aContext.getScope()));
    }

    @Test
    public void testDocVariable() {
        Scriptable dscope = docContext.getScope();
        dscope.put("turtle", dscope, "mcconnell");

        Var var = new Var(attrs, new NodeLocation(URI.create("file")));
        var.execute(scontext, aContext);
        assertEquals("myrtle", aContext.getScope().get("turtle", aContext.getScope()));
        assertEquals("mcconnell", docContext.getScope().get("turtle", docContext.getScope()));
    }

    @Test
    public void testAssignNotDefined() {
        Assign assign = new Assign(attrs, new NodeLocation(URI.create("file")));
        Result result = assign.execute(scontext, aContext);
        assertEquals(Result.Type.EVENT, result.getType());

    }

    @Test
    public void testAssigned() {
        Var var = new Var(attrs, new NodeLocation(URI.create("file")));
        var.execute(scontext, aContext);

        Map<String, String> attrsnew = new HashMap<String, String>();
        attrsnew.put("name", "turtle");
        attrsnew.put("expr", "'clyde'");

        Assign assign = new Assign(attrsnew, new NodeLocation(URI.create("file")));
        assign.execute(scontext, aContext);

        assertEquals("clyde", aContext.getScope().get("turtle", aContext.getScope()));

    }

    @Test
    public void testAssignParent() {

        Scriptable dscope = docContext.getScope();
        dscope.put("turtle", dscope, "mcconnell");

        Map<String, String> attrsnew = new HashMap<String, String>();
        attrsnew.put("name", "document.turtle");
        attrsnew.put("expr", "'clyde'");

        Assign assign = new Assign(attrsnew, new NodeLocation(URI.create("filenew")));
        @SuppressWarnings("unused")
        Result result = assign.execute(scontext, aContext);
        assertEquals("clyde", docContext.getScope().get("turtle", docContext.getScope()));

    }

    @Test
    public void testAssignParentNonExistantVar() {

        Scriptable dscope = docContext.getScope();
        dscope.put("turtle", dscope, "mcconnell");

        Map<String, String> attrsnew = new HashMap<String, String>();
        attrsnew.put("name", "document.purtle");
        attrsnew.put("expr", "'clyde'");

        Assign assign = new Assign(attrsnew, new NodeLocation(URI.create("filenew")));
        @SuppressWarnings("unused")
        Result result = assign.execute(scontext, aContext);
        assertEquals(Result.Type.OK, result.getType());
        assertEquals("clyde", docContext.getScope().get("purtle", docContext.getScope()));

    }
    
    @Test
    public void testAssignParentNonExistantBlankVar() {

        Scriptable dscope = docContext.getScope();
        dscope.put("turtle", dscope, "mcconnell");

        Map<String, String> attrsnew = new HashMap<String, String>();
        attrsnew.put("name", "document.purtle");
        attrsnew.put("expr", "");

        Assign assign = new Assign(attrsnew, new NodeLocation(URI.create("filenew")));
        @SuppressWarnings("unused")
        Result result = assign.execute(scontext, aContext);
        assertEquals(Result.Type.OK, result.getType());
        assertEquals(Context.getUndefinedValue(), docContext.getScope().get("purtle", docContext.getScope()));

    }

    @Test
    public void testClearNoNamelist() throws DocumentServerException, ParserException, URISyntaxException, InterpreterException {
        DocumentParser parser = DaggerVxmlParser.create().getDocumentParser();
        Document document = parser.parse(scontext, FORM_URL, scontext.getDocumentServer().getDocument(FORM_URL));
        Interpreter interpreter = new Interpreter(scontext, scontext.getDocumentServer());
        interpreter.loadDocument(document);
        interpreter.processDocument();

        InterpreterState istate = interpreter.getInterpreterState();
        FormContext fcontext = istate.getFormContext();

        int numberComplete = 0;
        for (FormItemVariable each : fcontext.getFormItemVariablesList()) {
            if (each.isComplete()) {
                numberComplete++;
            }
        }

        // 2 of 4 are completed during process. The other 2 have cond attrs.
        assertEquals(2, numberComplete);
        Clear clear = new Clear(new HashMap<String, String>(), new NodeLocation(URI.create("file")));

        clear.execute(scontext, new AnonymousContext(istate.getFormContext()));

        for (FormItemVariable each : fcontext.getFormItemVariablesList()) {
            assertFalse(each.isComplete());
        }

        System.out.println("foo");
    }

}
