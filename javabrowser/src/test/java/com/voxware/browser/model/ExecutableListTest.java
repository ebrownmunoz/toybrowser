/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.model;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.voxware.browser.context.BaseContext;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;

public class ExecutableListTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    @Before
    public void setUp() throws Exception {
    }

    @Ignore
    @Test
    public void test() {
        fail("Not yet implemented");
    }

    private Executable createExecutableWithResult(Result.Type type) {
        Executable retval = mock(Executable.class);

        if (type == Result.Type.OK) {
            when(retval.execute(any(SessionContext.class), any(BaseContext.class))).thenReturn(Result.Ok());
        } else if (type == Result.Type.GOTO) {
            Result result = new Result(Result.Type.GOTO, "foo.html", false);
            when(retval.execute(any(SessionContext.class), any(BaseContext.class))).thenReturn(result);
        }

        return retval;
    }

}
