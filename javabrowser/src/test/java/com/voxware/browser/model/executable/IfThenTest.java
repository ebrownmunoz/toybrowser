/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject
 * to the terms and conditions of the executed License Agreement on file with Voxware that has the right to use
 * license keys to control access to the Licensed Software.
 */
package com.voxware.browser.model.executable;

import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.interpreter.InterpreterState;
import com.voxware.browser.parser.ParserException;
import com.voxware.browser.test.TestVMXLRunner;

public class IfThenTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    public static final String IFTHEN = "http://localhost:8080/predicates/simple_if.vxml";
    public static final String IFELSE = "http://localhost:8080/predicates/if_else.vxml";
    public static final String IFELSEIF = "http://localhost:8080/predicates/if_else_if.vxml";

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testSimple() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException {
        TestVMXLRunner runner = null;

        try {
            runner = new TestVMXLRunner();
            runner.runVXMLTest(IFTHEN);
        } finally {
            if (runner != null) {
                runner.close();
            }
        }
    }
    
    @Test
    public void testElse() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException {
        TestVMXLRunner runner = null;

        try {
            runner = new TestVMXLRunner();
            InterpreterState state = runner.runVXMLTest(IFELSE);
            
            FormContext fcontext = state.getFormContext();
            Object result= fcontext.getScope().get("horse", fcontext.getScope());
            Object turtle= fcontext.getScope().get("turtle", fcontext.getScope());
            
            
            assertEquals("phil", turtle);
            assertEquals("burger", result);
            
            
        } finally {
            if (runner != null) {
                runner.close();
            }
        }
    }

    @Test
    public void testElseIf() throws DocumentServerException, ParserException, InterpreterException, URISyntaxException {
        TestVMXLRunner runner = null;

        try {
            runner = new TestVMXLRunner();
            InterpreterState state = runner.runVXMLTest(IFELSEIF);
            
            FormContext fcontext = state.getFormContext();
            Object result= fcontext.getScope().get("horse", fcontext.getScope());
            Object turtle= fcontext.getScope().get("turtle", fcontext.getScope());
            Object train= fcontext.getScope().get("train", fcontext.getScope());
            
            
            assertEquals("phil", result);
            assertEquals("sam", turtle);
            assertEquals("thomas", train);
            
            
        } finally {
            if (runner != null) {
                runner.close();
            }
        }
    }

}
