/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import static org.mockito.Mockito.mock;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;
import org.mozilla.javascript.Context;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: May 19, 2015 by: eric</li>
 * </ul>
 */
public class AppGotoTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final ClassLoader cl = getClass().getClassLoader();
    private Context context;
    private DocumentServer docserver;
    private DocumentParser parser;

    private IoManager iomanager;

    private static final String URL = "http://localhost:8080/vxmlparser/three_blocks.vxml";

    /**    	when(tts.play(anyString()).thenReturn(new Future<AudioPlayer.Status>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isCancelled() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDone() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public AudioPlayer.Status get() throws InterruptedException, ExecutionException {
				// TODO Auto-generated method stub
				return AudioPlayer.Status.SUCCESS;
			}

			@Override
			public AudioPlayer.Status get(long timeout, TimeUnit unit)
					throws InterruptedException, ExecutionException, TimeoutException {
				// TODO Auto-generated method stub
				return AudioPlayer.Status.SUCCESS;
			}
		});
     * The CREATE.
     */
    private static final URI THREE_BLOCKS = URI.create(URL);

    private static final String URL2 = "http://localhost:8080/vxmlparser/weather_service.vxml";

    private static final String URL_FIV = "http://localhost:8080/vxmlparser/form_item_variables.vxml";

    private SessionContext sessionContext;

    @Mock
    private Player<String> tts;
    
    @Mock
    private Player<byte[]> bytePlayer;
    
    @Mock
    private Player<URI> uriPlayer;
    
    
    private Recognizer mockrecognizer;
    
    private AudioPlayer audioPlayer;
    

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    	MockitoAnnotations.initMocks(this);
    	
    	when(tts.play(anyString())).thenReturn(new Future<Void>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isCancelled() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDone() {
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public Void get() throws InterruptedException, ExecutionException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Void get(long timeout, TimeUnit unit)
					throws InterruptedException, ExecutionException, TimeoutException {
				// TODO Auto-generated method stub
				return null;
			}
		});
    	
    	
        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);
        
        audioPlayer = new AudioPlayer(tts, bytePlayer, uriPlayer);

        context = Context.enter();

        Map<String, Object> props = new HashMap<String, Object>();

        docserver = new FileSystemDocumentServer(file.getParent());

        iomanager = new IoManager();
        iomanager.setAudioPlayer(audioPlayer);

        mockrecognizer = mock(Recognizer.class);
        iomanager.setRecognizer(mockrecognizer);
        sessionContext = new SessionContext(context, props, iomanager, docserver);
        parser = DaggerVxmlParser.create().getDocumentParser();
    }

    @After
    public void tearDown() {
        Context.exit();
    }

    @Test
    public void testDocument()
        throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {
        Document document = parser.parse(sessionContext, THREE_BLOCKS, docserver.getDocument(THREE_BLOCKS));

        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();

    }

}
