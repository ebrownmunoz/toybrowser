/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mozilla.javascript.Context;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.CacheControl;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerInputStream;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.RecognitionResult.Status;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;

import static org.mockito.Mockito.when;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * FilledTest
 *
 * @author edh
 */
public class FilledTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    public static final URI NO_FILLED = URI.create("classpath:///filled.vxml#noFilled");
    public static final URI FIELD_FILLED = URI.create("classpath:///filled.vxml#fieldFilled");
    public static final URI DOCUMENT_ORDER = URI.create("classpath:///filled.vxml#documentOrder");
    public static final URI FORM_ANY_FILLED = URI.create("classpath:///filled.vxml#formAnyFilled");
    
    private SessionContext sessionContext;
    @Mock
    private IoManager ioManager;
    @Mock
    private Recognizer recognizer;
    @Mock
    private Player<String> tts;
    @Mock
    private Player<byte[]> bytePlayer;
    @Mock
    private Player<URI> uriPlayer;
    @Mock
    private Future<Void> ttsResult;
    @Mock
    private Future<RecognitionResult> resultFuture;
    @Mock
    private RecognitionResult result;
    @Mock
    private DocumentServer documentServer;
     
    private DocumentParser documentParser;
    private Document document;
    
    private Interpreter interpreter;
    
    
    @Before
    public void setup() throws Throwable {
        MockitoAnnotations.initMocks(this);
        
        when(ioManager.getRecognizer()).thenReturn(recognizer);
        AudioPlayer audioPlayer = new AudioPlayer(tts, bytePlayer, uriPlayer);
        when(ioManager.getAudioPlayer()).thenReturn(audioPlayer);
        
        when(recognizer.recognize(anyString())).thenReturn(resultFuture);
        when(resultFuture.get()).thenReturn(result);
        when(result.getStatus()).thenReturn(Status.SUCCESS);
        when(result.getResult()).thenReturn("success");
        when(tts.play(anyString())).thenReturn(ttsResult);
        //documentServer = new BasicDocumentServer(null);
        
        sessionContext = new SessionContext(null, ioManager, documentServer);
        documentParser = DaggerVxmlParser.create().getDocumentParser();
        interpreter = new Interpreter(sessionContext, documentServer);
        when(documentServer.getDocument(any(URI.class))).then(answerWithEmptyInputStream());
        when(documentServer.getDocument(any(URI.class), any(Map.class), any(CacheControl.class))).then(answerWithEmptyInputStream());
        when(documentServer.getDocument(any(URI.class), any(Map.class), any(CacheControl.class), anyInt(), anyInt())).then(answerWithEmptyInputStream());
    }
    
    /**
     * @return
     */
    private Answer<?> answerWithEmptyInputStream() {
        return new Answer<DocumentServerInputStream>() {
            @Override
            public DocumentServerInputStream answer(InvocationOnMock invocation) throws Throwable {
                return new DocumentServerInputStream(new ByteArrayInputStream(new byte[0]), 0);
            }
        };
    }

    protected Document loadDocument(URI uri) throws Exception {
        Document document = documentParser.parse(sessionContext, uri, this.getClass().getResourceAsStream(uri.getPath()));
        Result result = interpreter.loadDocument(document);
        assertEquals(Result.Type.OK, result.getType());
        String formName = uri.getFragment();
        if (formName != null) {
            Form form = document.getForm(formName);
            assertNotNull(form);
            interpreter.getInterpreterState().setForm(form);
        }
        return document;
    }
    
    @Test
    public void testNoFilled() throws Exception {
        Document document = loadDocument(NO_FILLED);
        Result res = interpreter.processDocument();
        assertEquals(Result.Type.EXIT, res.getType());
        assertEquals(0, res.getStatus());
        assertEquals(0.0, interpreter.getInterpreterState().getDocContext().get("ok"));
    }
    
    @Test
    public void testFieldFilled() throws Exception {
        Document document = loadDocument(FIELD_FILLED);
        Result res = interpreter.processDocument();
        assertEquals(Result.Type.EXIT, res.getType());
        assertEquals(0, res.getStatus());
        assertEquals(1.0, interpreter.getInterpreterState().getDocContext().get("ok"));
    }
    
    @Test
    public void testDocumentOrder() throws Exception {
        Document document = loadDocument(DOCUMENT_ORDER);
        Result res = interpreter.processDocument();
        assertEquals(Result.Type.EXIT, res.getType());
        assertEquals(0, res.getStatus());
        assertEquals(2.0d, Context.toNumber(interpreter.getInterpreterState().getDocContext().get("ok")), 0.0d);
    }
    
    @Test
    public void testFormAnyFilled() throws Exception {
        Document document = loadDocument(FORM_ANY_FILLED);
        Result res = interpreter.processDocument();
        assertEquals(Result.Type.EXIT, res.getType());
        assertEquals(0, res.getStatus());
        assertEquals(1.0, interpreter.getInterpreterState().getDocContext().get("ok"));
    }
}
