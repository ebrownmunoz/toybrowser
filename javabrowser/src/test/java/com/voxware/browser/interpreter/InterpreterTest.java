/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.UniqueTag;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.ApplicationContext;
import com.voxware.browser.context.BrowserEvent;
import com.voxware.browser.context.DocumentContext;
import com.voxware.browser.context.FormContext;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.context.property.PropertyList;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.event.CatchList;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;

/**
 * <ul>
 * <li>Title:</li>
 * <li>Description:</li>
 * <li>Created: May 19, 2015 by: eric</li>
 * </ul>
 */
public class InterpreterTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private final ClassLoader cl = getClass().getClassLoader();
    private Context context;
    private DocumentServer docserver;

    private IoManager iomanager;

    private static final URI URL = URI.create("http://localhost:8080/vxmlparser/three_blocks.vxml");

    private static final URI URL2 = URI.create("http://localhost:8080/vxmlparser/weather_service.vxml");

    private static final URI URL_FIV = URI.create("http://localhost:8080/vxmlparser/form_item_variables.vxml");

    private static final URI URL_APP_FLAVOR = URI.create("http://localhost:8080/vxmlparser/application_goto/order_flavor.vxml");

    private static final URI URL_APP_FLAVOR2 = URI.create("http://localhost:8080/vxmlparser/application_goto/order_flavor2.vxml");

    private static final URI URL_APP_MULTIFORM = URI.create("http://localhost:8080/vxmlparser/application_goto/single_page_multiform.vxml");

    private static final URI URL_APP_MULTIFORM_ABSOLUTE = URI.create("http://localhost:8080/vxmlparser/application_goto/single_page_multiform_absolute.vxml");

    private static final URI URL_APP_SUBMIT = URI.create("http://localhost:8080/vxmlparser/application_submit/order_flavor.vxml");

    private static final URI URL_APP_FILLED = URI.create("http://localhost:8080/vxmlparser/field_filled.vxml");

    private static final URI URL_APP_SUBDIALOG = URI.create("http://localhost:8080/subdialogs/doc1.vxml");

    private SessionContext sessionContext;

    @Mock
    private AudioPlayer mockAudioPlayer;
    private Recognizer mockrecognizer;

    private DocumentParser parser = DaggerVxmlParser.create().getDocumentParser();
    
    public static Future<RecognitionResult> result = new Future<RecognitionResult>() {
        RecognitionResult res = RecognitionResult.success("here is your result");

        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        public boolean isCancelled() {
            return false;
        }

        public boolean isDone() {
            return true;
        }

        public RecognitionResult get() throws InterruptedException, ExecutionException {
            return res;
        }

        public RecognitionResult get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return res;
        }
    };

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Throwable {
    	
    	MockitoAnnotations.initMocks(this);

        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        context = Context.enter();

        Map<String, Object> props = new HashMap<String, Object>();

        docserver = new FileSystemDocumentServer(file.getParent());

        iomanager = new IoManager();
        iomanager.setAudioPlayer(mockAudioPlayer);

        mockrecognizer = mock(Recognizer.class);


        when(mockrecognizer.recognize(anyString())).thenReturn(result);
        iomanager.setRecognizer(mockrecognizer);
        sessionContext = new SessionContext(context, props, iomanager, docserver);
        
        @SuppressWarnings("unchecked")
        Future<Void> ttsResult = mock(Future.class);
        when(mockAudioPlayer.play(any(List.class))).thenReturn(ttsResult);

    }

    @After
    public void tearDown() {
        Context.exit();
    }

    @Ignore
    @Test
    public void test_three_blocks() throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {
        Document document = parser.parse(sessionContext, URL, docserver.getDocument(URL));
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        // interpreter.iterate(document, context);
    }

    @Ignore
    @Test
    public void test_weather_service() throws ParserException, DocumentServerException, InterpreterException, InterruptedException, ExecutionException, URISyntaxException, BrowserEvent {
        Document document = parser.parse(sessionContext, URL2, docserver.getDocument(URL2));

        @SuppressWarnings("unchecked")
        Future<RecognitionResult> futrr = mock(Future.class);

        RecognitionResult rr = RecognitionResult.success("foo");

        when(futrr.get()).thenReturn(rr);

        when(mockrecognizer.recognize(anyString())).thenReturn(futrr);

        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        // interpreter.iterate(document, context);

    }

    @Test
    public void testGetNextFormItem() throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {
        Document document = parser.parse(sessionContext, new URI("foo.txt"), docserver.getDocument(URL_FIV));

        DocumentContext dcontext = sessionContext.createDocumentContext(null);
        
        InterpreterState state = new InterpreterState(sessionContext);

        Form form = (Form) document.getForm(null);

        FormContext fcontext = new FormContext(sessionContext, dcontext, new CatchList(), new PropertyList(dcontext.getPropertyList()));

        Scriptable formScope = fcontext.getScope();

        dcontext.applyInitializers(document.getInitializers(), state);
        fcontext.applyInitializers(form.getInitializers(), state);
        fcontext.addFormItemVariablesFromForm(form);

        Context rhino = sessionContext.getContext();

        Interpreter interpreter = new Interpreter(sessionContext, docserver);

        FormItem next = interpreter.getNextFormItem(fcontext, form);

        assertEquals(form.getFormItems().get(0), next);

        fcontext.setFormItemValue(next, "booop");

        next = interpreter.getNextFormItem(fcontext, form);

        assertEquals(form.getFormItems().get(1), next);

        fcontext.setFormItemValue(next, "bleep");

        next = interpreter.getNextFormItem(fcontext, form);

        assertNull(next);

        formScope.put("foo", formScope, "bear");

        next = interpreter.getNextFormItem(fcontext, form);

        assertEquals(form.getFormItems().get(3), next);

        formScope.put("foo", formScope, "var");

        next = interpreter.getNextFormItem(fcontext, form);

        assertEquals(form.getFormItems().get(2), next);

        fcontext.clearFormItemVariable(form.getFormItems().get(1));

        next = interpreter.getNextFormItem(fcontext, form);
        assertEquals(form.getFormItems().get(1), next);

    }

    @Test
    public void testApplicatonContext() throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {
        Document document = parser.parse(sessionContext, URL_APP_FLAVOR, docserver.getDocument(URL_APP_FLAVOR));

        Interpreter interpreter = new Interpreter(sessionContext, docserver);

        interpreter.loadDocument(document);

        interpreter.processDocument();

        InterpreterState state = interpreter.getInterpreterState();

        // Application context is saved
        ApplicationContext appContext = state.getAppContext();

        Scriptable scope = appContext.getScope();

        Scriptable object = (Scriptable) scope.get("order", scope);

        assertEquals("doc1", object.get("flavor", object));
        assertEquals("topping", object.get("foo", object));

        // Document content is not.
        Scriptable docScope = state.getDocContext().getScope();

        assertEquals("carp", docScope.get("fish", docScope));
        assertEquals(Scriptable.NOT_FOUND, docScope.get("soup", docScope));
    }

    @Test
    public void testApplicatonContextOtherApp() throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {
        Document document = parser.parse(sessionContext, URL_APP_FLAVOR2, docserver.getDocument(URL_APP_FLAVOR2));
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        InterpreterState state = interpreter.getInterpreterState();

        // Application context is saved
        ApplicationContext appContext = state.getAppContext();
        Scriptable scope = appContext.getScope();
        Object object = scope.get("order", scope);
        assertEquals(Scriptable.NOT_FOUND, object);
    }

    @Test
    public void testMultiForm() throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {
        Document document = parser.parse(sessionContext, URL_APP_MULTIFORM, docserver.getDocument(URL_APP_MULTIFORM));

        Interpreter interpreter = new Interpreter(sessionContext, docserver);

        interpreter.loadDocument(document);

        interpreter.processDocument();

        InterpreterState state = interpreter.getInterpreterState();

        assertNull(state.getAppContext());

        DocumentContext docContext = state.getDocContext();
        Scriptable scope = docContext.getScope();

        assertEquals("carp", scope.get("fish", scope));
        assertEquals("nuts", scope.get("soup", scope));
        assertEquals(Scriptable.NOT_FOUND, scope.get("dummy", scope));

    }

    @Test
    public void testMultiFormAbsolute() throws ParserException, DocumentServerException, InterpreterException, URISyntaxException {

        Document document = parser.parse(sessionContext, URL_APP_MULTIFORM_ABSOLUTE, docserver.getDocument(URL_APP_MULTIFORM_ABSOLUTE));

        Interpreter interpreter = new Interpreter(sessionContext, docserver);

        interpreter.loadDocument(document);

        interpreter.processDocument();

        InterpreterState state = interpreter.getInterpreterState();

        assertNull(state.getAppContext());

        DocumentContext docContext = state.getDocContext();
        Scriptable scope = docContext.getScope();

        assertEquals("carp", scope.get("fish", scope));
        assertEquals(Scriptable.NOT_FOUND, scope.get("soup", scope));
        assertEquals(Scriptable.NOT_FOUND, scope.get("dummy", scope));

    }

    @Test
    public void testSubmit() throws ParserException, URISyntaxException, DocumentServerException, InterpreterException {
        Document document = parser.parse(sessionContext, URL_APP_SUBMIT, docserver.getDocument(URL_APP_SUBMIT));
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
    }

    @Test
    public void testFilled() throws ParserException, URISyntaxException, DocumentServerException, InterpreterException {
        Document document = parser.parse(sessionContext, URL_APP_FILLED, docserver.getDocument(URL_APP_FILLED));
        
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        Scriptable scope = interpreter.getInterpreterState().getDocContext().getScope();
        assertEquals("dog", scope.get("blog", scope));
    }

    @Test
    public void testSubdialog() throws ParserException, URISyntaxException, DocumentServerException, InterpreterException {
        Document document = parser.parse(sessionContext, URL_APP_SUBDIALOG, docserver.getDocument(URL_APP_SUBDIALOG));
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        Scriptable scope = interpreter.getInterpreterState().getFormContext().getScope();
        Object kool = scope.get("kool", scope);
        assertNotEquals(kool, UniqueTag.NOT_FOUND);
        assertNotEquals(kool, Context.getUndefinedValue());
        assertNotNull(kool);
        
        assertEquals("juice", ((Scriptable)kool).get("boo", (Scriptable) kool));
    }
}
