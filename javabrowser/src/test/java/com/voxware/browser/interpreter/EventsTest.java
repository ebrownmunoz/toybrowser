/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 */
package com.voxware.browser.interpreter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import com.voxware.browser.audioplayer.AudioPlayer;
import com.voxware.browser.context.InterpreterException;
import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServer;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.io.Player;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.io.Recognizer;
import com.voxware.browser.model.Document;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;

public class EventsTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private static final String VXML_EVENTS = "http://localhost:8080/vxml_events/document_event.vxml";
    private static final String VXML_EVENTS_SHADOW = "http://localhost:8080/vxml_events/document_event_shadow.vxml";
    private static final String VXML_EVENTS_SCOPING = "http://localhost:8080/vxml_events/application_catch.vxml";

    private Context context;
    private DocumentServer docserver;
    private IoManager iomanager;
    
    @Mock
    private AudioPlayer mockaudioPlayer;
    
    private Recognizer mockrecognizer;

    private SessionContext sessionContext;

    ClassLoader cl = EventsTest.class.getClassLoader();

    @Before
    public void setUp() throws Throwable {
    	
    	MockitoAnnotations.initMocks(this);

        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        context = Context.enter();

        Map<String, Object> props = new HashMap<String, Object>();

        docserver = new FileSystemDocumentServer(file.getParent());

        iomanager = new IoManager();

        iomanager.setAudioPlayer(mockaudioPlayer);

        mockrecognizer = mock(Recognizer.class);

        Future<RecognitionResult> result = new Future<RecognitionResult>() {
            RecognitionResult res = RecognitionResult.success("here is your result");

            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            public boolean isCancelled() {
                return false;
            }

            public boolean isDone() {
                return true;
            }

            public RecognitionResult get() throws InterruptedException, ExecutionException {
                return res;
            }

            public RecognitionResult get(long timeout, TimeUnit unit)
                throws InterruptedException, ExecutionException, TimeoutException {
                return res;
            }
        };

        when(mockrecognizer.recognize(anyString())).thenReturn(result);
        iomanager.setRecognizer(mockrecognizer);
        sessionContext = new SessionContext(context, props, iomanager, docserver);

    }

    @After
    public void tearDown() throws Exception {}

    @Test
    public void vxml_event() throws ParserException, DocumentServerException, URISyntaxException, InterpreterException {
        Document document = parseDocument(VXML_EVENTS);
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        InterpreterState state = interpreter.getInterpreterState();
        Scriptable formScope = state.getFormContext().getScope();
        assertEquals("potato", formScope.get("tit", formScope));
        Scriptable docScope = state.getDocContext().getScope();
        assertEquals("fibble:hello sweetie", docScope.get("caught", docScope).toString());


    }
    
    @Test
    public void vxml_event_shadow() throws ParserException, DocumentServerException, URISyntaxException, InterpreterException {
        Document document = parseDocument(VXML_EVENTS_SHADOW);
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        InterpreterState state = interpreter.getInterpreterState();
        Scriptable docScope = state.getDocContext().getScope();
        assertEquals("shadow1: voxware.fish", docScope.get("caught", docScope).toString());
    }
    
    @Test
    public void vxml_event_scoping() throws Exception {
        Document document = parseDocument(VXML_EVENTS_SCOPING);
        Interpreter interpreter = new Interpreter(sessionContext, docserver);
        interpreter.loadDocument(document);
        interpreter.processDocument();
        InterpreterState state = interpreter.getInterpreterState();
        Scriptable docScope = state.getDocContext().getScope();
        Scriptable formScope = state.getFormContext().getScope();
        assertEquals("document_local_ok", docScope.get("document_local", docScope).toString());
        assertEquals("form_local_ok", formScope.get("form_local", formScope).toString());
        assertEquals("document_local2_ok", docScope.get("document_local2", docScope).toString());
        assertEquals("form_local2_ok", formScope.get("form_local2", formScope).toString());
        assertEquals("document_local3_ok", docScope.get("document_local3", docScope).toString());
    }

    private Document parseDocument(String documentUrl)
        throws ParserException, DocumentServerException, URISyntaxException {
        DocumentParser parser = DaggerVxmlParser.create().getDocumentParser();
        URI uri = URI.create(documentUrl);
        Document document = parser.parse(sessionContext, uri, docserver.getDocument(uri));
        return document;
    }
}
