package com.voxware.browser.interpreter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.voxware.browser.interrupt.InterruptManager;
import com.voxware.browser.io.RecognitionResult;
import com.voxware.browser.model.InterruptFieldNode;
import com.voxware.browser.model.InterruptNode;

import static org.junit.Assert.*;

public class InterruptsTest {

    public static final String SOURCE = "tester";
    public static final String RESULT = "this is a test";

    private InterruptManager interruptManager = new InterruptManager();
    private Map<String, String> interruptAttributes = new HashMap<String, String>();
    private Thread interrupter = new Thread(new Runnable() {
        @Override
        public void run() {
            interruptManager.raise(SOURCE, RESULT);
        }
    });

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        interruptAttributes.put("source", SOURCE);
        List<InterruptNode> interrupts = new ArrayList<InterruptNode>();
        interrupts.add(new InterruptFieldNode(interruptAttributes, null));
        interruptManager.register(interrupts);
    }

    @After
    public void tearDown() throws Exception {}

    @Test
    public void test() throws Throwable {
        interruptManager.recognizerStart(false);
        interrupter.start();
        try {
            Thread.sleep(3000);
            fail();
        } catch (InterruptedException e) {
            RecognitionResult result = interruptManager.getInterrupt();
            assertEquals("interrupt." + SOURCE, result.getInputMode());
            assertEquals(RESULT, result.getResult());
        }
    }
}
