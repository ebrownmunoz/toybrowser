/*******************************************************************************
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. Voxware and its licensors and/or suppliers, as applicable,
 * shall retain all right, title and interest to the Licensed Software, including all patents, copyrights, trademarks,
 * trade secrets, and other proprietary rights thereto. All copies of the Licensed Software are subject to the terms and
 * conditions of the executed License Agreement on file with Voxware that has the right to use license keys to control
 * access to the Licensed Software.
 *******************************************************************************/
package com.voxware.browser.documentserver;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.junit.Before;
import org.junit.Test;

public class FileSystemDocumentServerTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    private ClassLoader cl = getClass().getClassLoader();

    DocumentServer docserver;

    @Before
    public void setUp() throws Exception {
        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        docserver = new FileSystemDocumentServer(file.getParent());
    }

    @Test
    public void testGetDocument() throws DocumentServerException, IOException {

        InputStream stream = null;
        BufferedReader reader;

        try {

            stream = docserver.getDocument(URI.create("http://localhost:8080/foo.txt"));
            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder sb = new StringBuilder(1024);
            String str = null;

            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }

            assertEquals("foobar", sb.toString());
        } catch (DocumentServerException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e1) {
            e1.printStackTrace();
            throw e1;
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

    @Test
    public void testGetRelative() throws Exception {
        InputStream stream = null;
        BufferedReader reader;

        try {

            stream = docserver.getDocument(URI.create("http://localhost:8080/foo.txt"));
            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder sb = new StringBuilder(1024);
            String str = null;

            while ((str = reader.readLine()) != null) {
                sb.append(str);
            }

            assertEquals("foobar", sb.toString());
        } catch (DocumentServerException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e1) {
            e1.printStackTrace();
            throw e1;
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

    /**
     * Test that an exception is thrown for file not exist
     * 
     * @throws DocumentServerException
     */
    @Test(expected = DocumentServerException.class)
    public void testGetDocumentNotExist() throws DocumentServerException {
        docserver.getDocument(URI.create("http://localhost:8080/notexists.txt"));
    }

}
