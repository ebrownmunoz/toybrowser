package com.voxware.browser.util;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;


import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Scratch {
	
	Logger log = LoggerFactory.getLogger(Scratch.class);
	
	private final String test_string = "http://alexs-8300:8081/ngv/workflow?workflow=workflow%2Fbootworkflow.xml&cmd=CheckRecoveryMAC&USER_ID=ERICB&DIALOG=PICKING&profile=vmserver%2Flogon%2Fspeaker%3FapplicationId%3DPICKING%26userId%3DERICB%26password%3D1iajhn5jlqgsuwh8nxo027vxd%26languageId%3Den-US%26environmentId%3DWH101&grammar=vmserver%2Flogon%2Fgrammar%3FapplicationId%3DPICKING%26userId%3DERICB%26password%3D1iajhn5jlqgsuwh8nxo027vxd%26languageId%3Den-US%26environmentId%3DWH101&macaddress=4083de8e8254&languageId=en-US&clientmem_percentused=0.9488204013745937&clientmem_totalbytes=15122432#beet";

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws URISyntaxException, MalformedURLException {
		URI testURI = new URI(test_string);
		log.info("orig :" + testURI.toString());
		
		String uriString = testURI.toString();
		String clippedString;
		
		if (uriString.contains("#")) {
			clippedString = uriString.substring(0, uriString.lastIndexOf('#'));
		} else {
			clippedString = uriString;
		}
		
		log.info("clip :" + clippedString);
		
		
		URI testURI_b = new URI(testURI.toString());
		log.info(testURI_b.toString());
		
		log.info("ssp: " + testURI.getRawSchemeSpecificPart());
		URI luri = new URI(clippedString);
		log.info("luri: " + luri.toString());
		
		java.net.URL url = luri.toURL();
		log.info("URL : " + url.toString());
		
	}

}
