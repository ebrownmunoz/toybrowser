/**
 * 
 */
package com.voxware.browser.util.nist;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.reflect.ClassPath;

/**
 * @author eric
 *
 */
public class NistFileTest {

    private ClassLoader cl = getClass().getClassLoader();
    private byte[] nistBytes;
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream instream = null; 
        int len = 0;
        byte[] buffer = new byte[1024];
        
        try {
            instream = cl.getResourceAsStream("nist/test_pcm.nist");
            while((len = instream.read(buffer)) >= 0) {
                baos.write(buffer, 0, len);
            }
        } finally {
            IOUtils.closeQuietly(instream);
        }
        
        nistBytes = baos.toByteArray();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link com.voxware.browser.util.nist.NISTFile#parse(byte[])}.
     */
    @Test
    public void testParse() {
        NISTFile nistFile = NISTFile.parse(nistBytes);
        assertEquals("78396", nistFile.getHeader("sample_count"));
        assertEquals(272, nistFile.getPcmStart());
    }

}
