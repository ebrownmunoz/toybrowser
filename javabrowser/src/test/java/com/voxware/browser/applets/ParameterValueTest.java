/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.applets;

import org.junit.Test;
import org.mozilla.javascript.Context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * ParameterValueTest
 *
 * @author edh
 */
public class ParameterValueTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";

    
    @Test
    public void testNullSourceConversions() {
        ParameterValue nullParameter = new ParameterValue("param", null);
        assertNull(nullParameter.value());
        assertNull(nullParameter.asBoolean());
        assertNull(nullParameter.asDouble());
        assertNull(nullParameter.asInteger());
        assertNull(nullParameter.asLong());
        assertNull(nullParameter.asMap());
        assertNull(nullParameter.asObjectArray());
        assertNull(nullParameter.asString());
        assertNull(nullParameter.asStringArray());
        assertNull(nullParameter.as(Object.class));
    }
    
    @Test
    public void testUndefinedSourceConversions() {
        ParameterValue nullParameter = new ParameterValue("param", Context.getUndefinedValue());
        assertNull(nullParameter.value());
        assertNull(nullParameter.asBoolean());
        assertNull(nullParameter.asDouble());
        assertNull(nullParameter.asInteger());
        assertNull(nullParameter.asLong());
        assertNull(nullParameter.asMap());
        assertNull(nullParameter.asObjectArray());
        assertNull(nullParameter.asString());
        assertNull(nullParameter.asStringArray());
        assertNull(nullParameter.as(Object.class));
    }
    
    @Test
    public void testStringSourceConversions() {
        ParameterValue stringParameter = new ParameterValue("param", "dog");
        assertEquals(false, stringParameter.asBoolean());
        assertEquals("dog", stringParameter.asString());
        assertEquals("dog", stringParameter.as(String.class));
        try {
            stringParameter.asDouble();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asInteger();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asLong();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asMap();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asObjectArray();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asStringArray();
            fail();
        } catch (IllegalParameterException e) {}
    }
    
    @Test
    public void testZeroSourceConversions() {
        ParameterValue stringParameter = new ParameterValue("param", 0);
        assertEquals(false, stringParameter.asBoolean());
        assertEquals("0", stringParameter.asString());
        assertEquals(new Double(0), stringParameter.asDouble());
        assertEquals(new Integer(0), stringParameter.asInteger());
        assertEquals(new Long(0), stringParameter.asLong());
        try {
            stringParameter.asMap();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asObjectArray();
            fail();
        } catch (IllegalParameterException e) {}
        try {
            stringParameter.asStringArray();
            fail();
        } catch (IllegalParameterException e) {}
    }
}
