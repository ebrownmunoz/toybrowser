/*
 * Copyright (c) 2015. Voxware, Inc. All Rights Reserved. 
 * Voxware and its licensors and/or suppliers, as applicable, shall retain all right,
 * title and interest to the Licensed Software, including all patents, copyrights,
 * trademarks, trade secrets, and other proprietary rights thereto. 
 * All copies of the Licensed Software are subject to the terms and conditions of the
 * executed License Agreement on file with Voxware that has the right to use license 
 * keys to control access to the Licensed Software.
 */
package com.voxware.browser.applets;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mozilla.javascript.Context;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.interpreter.Result;
import com.voxware.browser.model.NodeLocation;
import com.voxware.browser.model.Param;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * AbstractAppletTest
 *
 * @author edh
 */
public class AbstractAppletTest {
    /**
     * Legal copyright notice.
     */
    public static final String COPYRIGHT = "\u00a9 Copyright Voxware Inc. 2015.";
    
    public static final URI TEST_URI = URI.create("test");
    
    class TestApplet extends AbstractApplet {
        @Override
        public Result run() throws Exception {
            return null;
        }

        /* 
         * (non-Javadoc)
         * @see com.voxware.browser.applets.AbstractApplet#parameter(java.lang.String)
         */
        @Override
        public ParameterValue parameter(String name) {
            return super.parameter(name);
        }
    }

    @Mock private SessionContext sessionContext;
    private TestApplet applet = new TestApplet();
    private Map<Param, Object> parameters = new HashMap<Param, Object>();
    
    protected Param makeParam(String name) {
        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("name", name);
        return new Param(attributes, new NodeLocation(TEST_URI));
    }
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        parameters.put(makeParam("boolean"), true);
    }
    
    @Test
    public void testParameters() throws Exception {
        applet.run(Context.enter(), sessionContext, parameters);
        assertNotNull(applet.parameter("boolean"));
        assertTrue(applet.parameter("boolean").asBoolean());
    }
}
