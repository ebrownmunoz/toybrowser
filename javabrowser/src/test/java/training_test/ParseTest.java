package training_test;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mozilla.javascript.Context;

import com.voxware.browser.context.SessionContext;
import com.voxware.browser.documentserver.DocumentServerException;
import com.voxware.browser.documentserver.FileSystemDocumentServer;
import com.voxware.browser.io.IoManager;
import com.voxware.browser.model.Document;
import com.voxware.browser.model.form.Form;
import com.voxware.browser.model.form.FormItem;
import com.voxware.browser.parser.DaggerVxmlParser;
import com.voxware.browser.parser.DocumentParser;
import com.voxware.browser.parser.ParserException;

public class ParseTest {
	
    private static final URI TRAINING  = URI.create("http://localhost:8080/vmserver/vxml/training/training.vxml");
    				
    private DocumentParser parser;

    private FileSystemDocumentServer docserver;

    private ClassLoader cl = getClass().getClassLoader();

    private SessionContext sessionContext;
    
    Context context;
    
    


	@Before
	public void setUp() throws Exception {
        String path = cl.getResource("foo.txt").getPath();
        File file = new File(path);

        context = Context.enter();

        Map<String, Object> props = new HashMap<String, Object>();

        docserver = new FileSystemDocumentServer(file.getParent());

        sessionContext = new SessionContext(context, props, new IoManager(), docserver);

        parser = DaggerVxmlParser.create().getDocumentParser();
	}

	@Test
	public void test() throws ParserException, DocumentServerException {
		 Document training_document = parser.parse(sessionContext, TRAINING, docserver.getDocument(TRAINING));
		 Form getUtterance = training_document.getForm("getutterance");
		 List<FormItem> formItems = getUtterance.getFormItems();
		 
		 
		 assertTrue(true);
	}

}
